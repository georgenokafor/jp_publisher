
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAllExcursionIDsResult" type="{http://webapi.globusandcosmos.com/}ArrayOfInt" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllExcursionIDsResult"
})
@XmlRootElement(name = "GetAllExcursionIDsResponse")
public class GetAllExcursionIDsResponse {

    @XmlElement(name = "GetAllExcursionIDsResult")
    protected ArrayOfInt getAllExcursionIDsResult;

    /**
     * Gets the value of the getAllExcursionIDsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfInt }
     *     
     */
    public ArrayOfInt getGetAllExcursionIDsResult() {
        return getAllExcursionIDsResult;
    }

    /**
     * Sets the value of the getAllExcursionIDsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfInt }
     *     
     */
    public void setGetAllExcursionIDsResult(ArrayOfInt value) {
        this.getAllExcursionIDsResult = value;
    }

}
