package models.publisher;

import com.avaje.ebean.config.ServerConfig;
import com.avaje.ebean.event.ServerConfigStartup;

/**
 * Exactly as in https://www.playframework.com/documentation/2.1.0/JavaEbean
 * This is needed to reduce sequence gaps.
 */
public class MyServerConfigStartup
    implements ServerConfigStartup {
  @Override
  public void onStart(ServerConfig serverConfig) {
    serverConfig.setDatabaseSequenceBatchSize(1);
  }
}
