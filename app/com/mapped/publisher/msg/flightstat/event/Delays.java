package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

/**
 * Created by twong on 2015-02-03.
 */
public class Delays {
  private List<Delay> delay;

  public List<Delay> getDelay() {
    return delay;
  }

  public void setDelay(List<Delay> delay) {
    this.delay = delay;
  }
}
