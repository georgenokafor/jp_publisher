package com.umapped.persistence.reservation.cruise;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.umapped.persistence.reservation.UmDateTime;
import com.umapped.persistence.reservation.UmOrganization;
import com.umapped.persistence.reservation.UmPlace;
import com.umapped.persistence.reservation.UmThing;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmCruiseShip
    extends UmThing
    implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public String arrivalPier;

  public UmPlace arrivalPort;

  public UmDateTime arrivalTime;

  public String departPier;

  public UmPlace departPort;

  public UmDateTime departTime;

  public UmDateTime boardingTime;

  private UmOrganization provider;

  public UmOrganization getProvider() {
    if (provider == null) {
      provider = new UmOrganization();
    }
    return provider;
  }

  public void setProvider(UmOrganization provider) {
    this.provider = provider;
  }
}
