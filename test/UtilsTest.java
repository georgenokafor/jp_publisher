import com.mapped.publisher.utils.Utils;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Timestamp;

/**
 * Created by surge on 2016-08-05.
 */
public class UtilsTest
    extends Assert {

  @Test
  public void getISO8601DateTestLong()  {
    assertEquals("First day of the year. First Second", "2016-01-01",
                 Utils.getISO8601Date(1451606400000L));
    assertEquals("First day of the year. Last Second", "2016-01-01",
                 Utils.getISO8601Date(1451692799000L));
    assertEquals("Lear year day. First Second", "2016-02-29",
                 Utils.getISO8601Date(1456704000000L));
    assertEquals("Lear year day. Last Second", "2016-02-29",
                 Utils.getISO8601Date(1456790399000L));
  }

  @Test
  public void getISO8601DateTestInstant()  {
    assertEquals("First day of the year. First Second", "2016-01-01",
                 Utils.getISO8601Date(new Timestamp(1451606400000L)));
    assertEquals("First day of the year. Last Second", "2016-01-01",
                 Utils.getISO8601Date(new Timestamp(1451692799000L)));
    assertEquals("Lear year day. First Second", "2016-02-29",
                 Utils.getISO8601Date(new Timestamp(1456704000000L)));
    assertEquals("Lear year day. Last Second", "2016-02-29",
                 Utils.getISO8601Date(new Timestamp(1456790399000L)));
  }

}
