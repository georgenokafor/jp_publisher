package com.umapped.external.wetu.pins.list;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by george on 2016-06-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class Option extends SimpleItem {
  public ArrayList<ItemRate> optionRates;

  public ArrayList<ItemRate> getOptionRates() {
    return optionRates;
  }

  public void setOptionRates(ArrayList<ItemRate> optionRates) {
    this.optionRates = optionRates;
  }
}
