package models.publisher;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by twong on 16-01-12.
 */
@Embeddable
public class TripBrandingPK implements Serializable {
  @Column(name="tripid")
  public String tripid;

  @Column (name="cmpyid")
  public String cmpyid;

  public TripBrandingPK (String tripId, String cmpyId) {
    this.tripid = tripId;
    this.cmpyid = cmpyId;
  }

  public String getTripid() {
    return tripid;
  }

  public void setTripid(String tripid) {
    this.tripid = tripid;
  }

  public String getCmpyid() {
    return cmpyid;
  }

  public void setCmpyid(String cmpyid) {
    this.cmpyid = cmpyid;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    TripBrandingPK that = (TripBrandingPK) o;

    if (!cmpyid.equals(that.cmpyid)) {
      return false;
    }
    if (!tripid.equals(that.tripid)) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = tripid.hashCode();
    result = 31 * result + cmpyid.hashCode();
    return result;
  }
}
