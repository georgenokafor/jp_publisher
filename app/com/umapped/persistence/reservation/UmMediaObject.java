package com.umapped.persistence.reservation;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmMediaObject
    extends UmCreativeWork {
  /**
   * A NewsArticle associated with the Media Object. (IGNORED)
   */
  //public NewsArticle associatedArticle;

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  /**
   *  The bitrate of the media object.
   *  Text
   */
  public String bitrate;
  /**
   *  Text  File size in (mega/kilo) bytes.
   */
  public String contentSize;
  /**
   * Actual bytes of the media object, for example the image file or video file.
   * URL
   */
  public String contentUrl;

  /**
   * The duration of the item (movie, audio recording, event, etc.) in ISO 8601 date format.
   */
  //Duration duration;

  /**
   * A URL pointing to a player for a specific video. In general, this is the information in the src element of an embed tag and should not be the same as the content of the loc tag.
   *  URL
   */
  public String embedUrl;

  /**
   * The CreativeWork encoded by this media object. (IGNORED)
   */
  //CreativeWork encodesCreativeWork;

  /**
   * mp3, mpeg4, etc.
   *  Text
   */
  public String encodingFormat;
  /**
   * Date the content expires and is no longer useful or available. Useful for videos.
   * Date
   */
  public UmDate expires;

  /**
   * The height of the item.
   * Distance  or QuantitativeValue
   */
  //Distance height;

  /**
   * Date when this media object was uploaded to this site.
   * Date
   */
  public UmDate uploadDate;
}
