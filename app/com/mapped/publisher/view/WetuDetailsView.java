package com.mapped.publisher.view;

import java.util.List;

/**
 * Created by george on 2016-07-04.
 */
public class WetuDetailsView extends BaseView {

  public String type;
  public String identifier;
  public String identifierKey;
  public int days;
  public String name;
  public String referenceNumber;
  public String clientName;
  public String clientEmail;
  public String startDate;
  public String lastModified;
  public int accessCount;
  public boolean isDisabled;
  public List<String> categories;
  public String bookingStatus;
}
