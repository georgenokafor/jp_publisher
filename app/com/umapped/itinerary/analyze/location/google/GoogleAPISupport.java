package com.umapped.itinerary.analyze.location.google;

import com.google.maps.GeoApiContext;

/**
 * Created by wei on 2017-03-22.
 */
public class GoogleAPISupport {
  private static String API_KEY = "AIzaSyAY7RX8nj6vPo9YBtSAsKXSc9wBgUk8QXI";

  public static GeoApiContext getGeoApiContext() {
    GeoApiContext context = new GeoApiContext();
    context.setApiKey(API_KEY);
    return context;
  }
}
