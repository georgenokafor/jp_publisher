package com.umapped.persistence.accountprop;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by twong on 2016-10-18.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountBilling {
  public BraintreeBillingInfo braintree;
  public EligiblePlans eligiblePlans;

  public void prePersist() {

  }
}
