package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-14
 * Time: 1:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class TripAgentForm {

    private String inTripId;
    private String inGroupId;

    private String inEmail;
    private String inAgentName;
    private String inAgencyName;

    private String inComments;

    public String getInTripId() {
        return inTripId;
    }

    public void setInTripId(String inTripId) {
        this.inTripId = inTripId;
    }

    public String getInEmail() {
        return inEmail;
    }

    public void setInEmail(String inEmail) {
        this.inEmail = inEmail;
    }

    public String getInAgentName() {
        return inAgentName;
    }

    public void setInAgentName(String inAgentName) {
        this.inAgentName = inAgentName;
    }

    public String getInAgencyName() {
        return inAgencyName;
    }

    public void setInAgencyName(String inAgencyName) {
        this.inAgencyName = inAgencyName;
    }

    public String getInComments() {
        return inComments;
    }

    public void setInComments(String inComments) {
        this.inComments = inComments;
    }

    public String getInGroupId() {
        return inGroupId;
    }

    public void setInGroupId(String inGroupId) {
        this.inGroupId = inGroupId;
    }
}
