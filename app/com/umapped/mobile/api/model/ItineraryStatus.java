package com.umapped.mobile.api.model;

/**
 * Created by wei on 2017-07-17.
 */
public enum ItineraryStatus {
  PENDING,
  PUBLISHED,
  DELETED
}
