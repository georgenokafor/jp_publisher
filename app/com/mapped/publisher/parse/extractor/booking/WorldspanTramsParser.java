package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by twong on 2015-02-09.
 */
public class WorldspanTramsParser extends BookingExtractor {

  protected void initImpl(Map<String, String> params) {

  }


  protected TripVO extractDataImpl(InputStream input)  throws Exception {
    TripVO trip = getInfo(parsePDF(input));

    return trip;
  }

  public ArrayList<ParseError> getParseErrors() {
    return null;
  }

  @Override
  public void postProcessingImpl(String tripId, String userId) {
  }

  private static String[] dateTimeFormat = {"M/dd/yyyy", "M/dd/yyyy h:mm:ss a", "dd MMM yyyy", "dd MMM yyyy ", "E dd MMM yyyy h:mma",  "E dd MMM yyyy", "E dd MMM yyyy ", "E dd MMMMM yyyy HH:mm" , "dd MMM yyyy HH:mm a", "E dd MMM yyyy HH:mm" , "dd MMMMM yyyy HH:mm", "E, MMMMM dd, yyyy HH:mm", "E, MMMMM dd, yyyy", "E, MMMMM dd, yyyy hh:mm a", "EEE dd MMM yyyy h:mm a", "EEE dd MMM yyyy 0h:mm a"} ;

  private static DateUtils du = new DateUtils();

  private Pattern section1 = Pattern.compile("[A-Za-z]+/[A-Za-z]+\\s[0-9]+/[0-9]+/[0-9]{4}");
  private Pattern section2 = Pattern.compile("[A-Z]{3}\\s[A-Za-z\\s]+[A-Z]{3}[A-Za-z\\s]+[0-9]{1,5}\\s[A-Za-z0-9]+\\s[0-9]+/[0-9]+/[0-9]{4}\\s[0-9]+[:][0-9]{2}[:][0-9]{2}\\s(AM|PM)|Payments\\sApplied\\sTo\\sThis\\sInvoice|Invoice\\sTotal:");
  private Pattern section3 = Pattern.compile("[A-Za-z]+/[A-Za-z]+\\s[0-9]+/[0-9]+/[0-9]{4}|Payments\\sApplied\\sTo\\sThis\\sInvoice|Invoice\\sTotal:");
  private Pattern airportCodePattern = Pattern.compile("[A-Z]{3}");
  private Pattern airportNamePattern = Pattern.compile("[A-Za-z]+");
  private Pattern flightNumberPattern = Pattern.compile("[0-9]{1,5}");
  private Pattern flightCodePattern = Pattern.compile("[A-Z]{2}");
  private Pattern datePattern = Pattern.compile("[0-9]+/[0-9]+/[0-9]{4}");
  private Pattern timePattern = Pattern.compile("[0-9]+:[0-9]{2}:[0-9]{2}");
  private Pattern time2Pattern = Pattern.compile("(AM|PM)");


  public  String parsePDF(InputStream input) {
    PDFParser parser;
    String parsedText = "";
    PDFTextStripper pdfStripper;
    PDDocument pdDoc = null;
    COSDocument cosDoc = null;
    //PDDocumentInformation pdDocInfo;
    try {
      parser = new PDFParser(input);

      parser.parse();
      cosDoc = parser.getDocument();
      pdfStripper = new PDFTextStripper();
      pdDoc = new PDDocument(cosDoc);
      parsedText = pdfStripper.getText(pdDoc);

      pdDoc.close();
    } catch (IOException e) {
      Log.log(LogLevel.ERROR,"WorldspanExtractor:parsePDF - ",e);
      e.printStackTrace();

      try {
        if (cosDoc != null) cosDoc.close();
        if (pdDoc != null) pdDoc.close();
      } catch (Exception e1) {
        e.printStackTrace();
      }
      return null;
    }

    return parsedText;
  }

  public  TripVO getInfo(String info) {
    StringTokenizer st = new StringTokenizer(info, "\n");
    String travelInfo = "";

    while(st.hasMoreTokens()) {
      String next = st.nextToken();

      if(next.equals("From To Flight A/L Depart Arrive"))
        break;
    }

    List<String> details = new ArrayList<String>();

    while(st.hasMoreTokens()) {
      //travelInfo = travelInfo + st.nextToken() + "\n";
      details.add(st.nextToken());
      if(details.get(details.size() - 1).equals("Payments Applied To This Invoice"))
        break;
    }

    HashMap<String, String> nameToTravelType = new HashMap<String, String>();
    HashMap<String, List<String>> travelTypeToDetails = new HashMap<String, List<String>>();
    List<Timestamp> departureDate = new ArrayList<Timestamp>();
    List<Timestamp> arrivalDate = new ArrayList<Timestamp>();
    HashMap<String, String> car = new HashMap<String, String>();
    Matcher m;
    for(int i = 0; i < details.size(); i++) {
      m = section1.matcher(details.get(i));
      if(m.find()) {
        nameToTravelType.put(details.get(i), details.get(i+1));
      }
      else if (details.get(i).contains("Domestic Air")) {
        int j = i+1;
        Boolean found = true;
        while(found) {
          List<String> travelDetails = travelTypeToDetails.get(details.get(i));

          if(travelDetails == null)
            travelDetails = new ArrayList<String>();

          travelDetails.add(details.get(j));
          j++;
          m = section2.matcher(details.get(j));
          found = m.find();
          if(found)
            travelTypeToDetails.put(details.get(i), travelDetails);
        }
      }
      else if (details.get(i).contains("Hotel")) {
        int k = i+1;
        Boolean found = false;
        while(!found) {
          List<String> hotelDetails = travelTypeToDetails.get(details.get(i));

          if(hotelDetails == null)
            hotelDetails = new ArrayList<String>();

          hotelDetails.add(details.get(k));
          k++;
          m = section3.matcher(details.get(k));

          found = m.find();
          if(!found)
            travelTypeToDetails.put(details.get(i), hotelDetails);
        }
      }
      else if (details.get(i).contains("Car")){

      }
    }


    TripVO trip = new TripVO();


    for(String s : travelTypeToDetails.keySet()) {
      if(s.contains("Domestic Air")) {
        String [] tokens = s.split(" ");
        String flightConfirmation = "";
        if (tokens.length > 2) {
          flightConfirmation = tokens[2].trim();
          flightConfirmation = flightConfirmation.replaceAll("-","");
        }
        for(String s2 : travelTypeToDetails.get(s)) {
          FlightVO flight = new FlightVO();
          flight.setReservationNumber(flightConfirmation);
          flight.setArrivalAirport(new AirportVO());
          flight.setDepartureAirport(new AirportVO());

          String arriveAirportCode = "";
          String arriveAirportName = "";
          String departAirportCode = "";
          String departAirportName = "";
          String departDate = "";
          String departTime = "";
          String arriveDate = "";
          String arriveTime = "";
          String flightNum = "";
          String flightC = "";
          String country = "";


          StringTokenizer st2 = new StringTokenizer(s2);
          Boolean airportFound = true;
          Matcher airportCode = null;
          Matcher airportName = null;
          Matcher flightNumber = null;
          Matcher flightCode = null;
          Matcher date = null;
          Matcher time = null;
          Matcher time2;
          while(st2.hasMoreTokens()) {
            String nextToken = st2.nextToken();
            airportCode = airportCodePattern.matcher(nextToken);
            airportName = airportNamePattern.matcher(nextToken);
            flightNumber = flightNumberPattern.matcher(nextToken);
            flightCode = flightCodePattern.matcher(nextToken);
            date = datePattern.matcher(nextToken);
            time = timePattern.matcher(nextToken);
            time2 = time2Pattern.matcher(nextToken);

            if(departAirportCode.equals("") && airportCode.find())
              departAirportCode = airportCode.group();
            else if(!departAirportCode.equals("") && airportCode.find())
              arriveAirportCode = airportCode.group();

            if(flightNumber.find() && flightNum.equals(""))
              flightNum = flightNumber.group();

            if(flightCode.find()) {
              String tempFlightC = flightCode.group();
              if(!tempFlightC.equals("AM") && !tempFlightC.equals("PM"))
                flightC = flightCode.group();
            }

            if(departDate.equals("") && date.find())
              departDate = date.group();
            else if(!departDate.equals("") && date.find())
              arriveDate = date.group();

            if(departTime.equals("") && time.find())
              departTime = time.group();
            else if(!departTime.equals("") && time.find())
              arriveTime = time.group();

            if(!departTime.equals("") && time2.find() && !departTime.contains("AM") && !departTime.contains("PM"))
              departTime += " " + time2.group();
            else if (!arriveTime.equals("") && (nextToken.equals("PM") || nextToken.equals("AM")) && !arriveTime.contains("AM") && !arriveTime.contains("PM"))
              arriveTime += " " + time2.group();

            if(arriveAirportCode.equals("") && airportName.find() && !airportName.group().equals(departAirportCode) &&
               !airportName.group().equals(arriveAirportCode) && !airportName.group().equals(country) && !airportName.group().equals("AM")
               && !airportName.group().equals("PM"))
              departAirportName += airportName.group() + " ";
            else if(airportName.find() && !airportName.group().equals(departAirportCode) &&
                    !airportName.group().equals(arriveAirportCode) && !airportName.group().equals(country) && !airportName.group().equals("AM")
                    && !airportName.group().equals("PM"))
              arriveAirportName += airportName.group() + " ";

          }

          flight.setArrivalTime(getTimestamp(arriveDate, arriveTime));
          flight.setDepatureTime(getTimestamp(departDate, departTime));

          if (flightNum != null)
            flight.setNumber(flightNum);

          if (flightC != null)
            flight.setCode(flightC);

          if  (arriveAirportCode != null && arriveAirportCode.trim().length() == 3) {
            flight.getArrivalAirport().setCode(arriveAirportCode);
          }
          flight.getArrivalAirport().setCity(arriveAirportName);
          flight.getArrivalAirport().setCountry(country);
          if  (departAirportCode != null && departAirportCode.trim().length() == 3) {
            flight.getDepartureAirport().setCode(departAirportCode);
          }
          flight.getDepartureAirport().setCity(departAirportName);
          flight.getDepartureAirport().setCountry(country);

          if (flight.getDepartureAirport() != null && flight.getDepatureTime() != null && flight.getCode() != null) {
            trip.addFlight(flight);
          }
        }
      }
      else if (s.contains("Hotel")){
        HotelVO hotel = new HotelVO();
        StringBuilder notes = new StringBuilder();
        for(String s2 : travelTypeToDetails.get(s)) {
          notes.append(s2); notes.append("\n");
          String[] hotelName = s.split(" ");
          StringBuilder sb = new StringBuilder();
          for(int i = 2; i < hotelName.length; i++) {
            sb.append(hotelName[i] + " ");
          }

          hotel.setHotelName(sb.toString());

          String hotelDates = "";
          String hotelCheckin = "";
          String hotelCheckout = "";
          String hotelConfirmation = hotelName[1];

          for(String h : nameToTravelType.keySet()) {
            if(nameToTravelType.get(h).equals(s))
              hotelDates = h;
          }

          Matcher date = null;

          StringTokenizer st2 = new StringTokenizer(hotelDates);
          while(st2.hasMoreTokens()) {
            String nextToken = st2.nextToken();
            date = datePattern.matcher(nextToken);

            if(hotelCheckin.equals("") && date.find())
              hotelCheckin = date.group();
            else if (!hotelCheckin.equals("") && date.find())
              hotelCheckout = date.group();
          }

          hotel.setCheckin(getTimestamp(hotelCheckin,"12:00:00 PM"));
          hotel.setCheckout(getTimestamp(hotelCheckout,"12:00:00 PM"));
          hotel.setConfirmation(hotelConfirmation);
          hotel.setNote("");
        }
        hotel.setNote(notes.toString());
        trip.addHotel(hotel);
      }
    }

    for(String s : nameToTravelType.keySet()) {
      if (nameToTravelType.get(s).contains("Car")) {
        TransportVO transport = new TransportVO();
        Matcher carDate = datePattern.matcher(s);
        String pickupDate = "";
        String dropoffDate = "";

        while(carDate.find()) {
          if(pickupDate.equals(""))
            pickupDate = carDate.group();
          else
            dropoffDate = carDate.group();
        }

        String[] carName = nameToTravelType.get(s).split(" ");
        StringBuilder sb = new StringBuilder();
        for(int i = 2; i < carName.length; i++) {
          sb.append(carName[i] + " ");
        }

        String carConfirmation = carName[1];

        transport.setCmpyName(sb.toString());
        transport.setConfirmationNumber(carConfirmation);
        transport.setPickupDate(getTimestamp(pickupDate, "12:00:00 PM"));
        transport.setDropoffDate(getTimestamp(dropoffDate, "12:00:00 PM"));

        trip.addTransport(transport);
      }
    }

    trip.setImportSrc(BookingSrc.ImportSrc.WORLDSPAN_TRAMS);
    trip.setImportTs(System.currentTimeMillis());
    return trip;

  }

  private static Timestamp getTimestamp (String date, String time) {
    try {
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime() );
      return t;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }


}
