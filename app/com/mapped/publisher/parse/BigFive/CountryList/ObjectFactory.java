
package com.mapped.publisher.parse.BigFive.CountryList;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mapped.publisher.parse.BigFive.CountryList package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CountryFlight_QNAME = new QName("", "country_flight");
    private final static QName _CountryCurrency_QNAME = new QName("", "country_currency");
    private final static QName _CountryVoltage_QNAME = new QName("", "country_voltage");
    private final static QName _CountryDescription_QNAME = new QName("", "country_description");
    private final static QName _Link_QNAME = new QName("", "link");
    private final static QName _CountryWeather_QNAME = new QName("", "country_weather");
    private final static QName _Image_QNAME = new QName("", "image");
    private final static QName _CountryTime_QNAME = new QName("", "country_time");
    private final static QName _CountryName_QNAME = new QName("", "country_name");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mapped.publisher.parse.BigFive.CountryList
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CountryList }
     * 
     */
    public CountryList createCountryList() {
        return new CountryList();
    }

    /**
     * Create an instance of {@link Country }
     * 
     */
    public Country createCountry() {
        return new Country();
    }

    /**
     * Create an instance of {@link CountryImages }
     * 
     */
    public CountryImages createCountryImages() {
        return new CountryImages();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "country_flight")
    public JAXBElement<String> createCountryFlight(String value) {
        return new JAXBElement<String>(_CountryFlight_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "country_currency")
    public JAXBElement<String> createCountryCurrency(String value) {
        return new JAXBElement<String>(_CountryCurrency_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "country_voltage")
    public JAXBElement<String> createCountryVoltage(String value) {
        return new JAXBElement<String>(_CountryVoltage_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "country_description")
    public JAXBElement<String> createCountryDescription(String value) {
        return new JAXBElement<String>(_CountryDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "link")
    public JAXBElement<String> createLink(String value) {
        return new JAXBElement<String>(_Link_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "country_weather")
    public JAXBElement<String> createCountryWeather(String value) {
        return new JAXBElement<String>(_CountryWeather_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "image")
    public JAXBElement<String> createImage(String value) {
        return new JAXBElement<String>(_Image_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "country_time")
    public JAXBElement<String> createCountryTime(String value) {
        return new JAXBElement<String>(_CountryTime_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "country_name")
    public JAXBElement<String> createCountryName(String value) {
        return new JAXBElement<String>(_CountryName_QNAME, String.class, null, value);
    }

}
