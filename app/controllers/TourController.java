package controllers;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.audit.AuditModuleType;
import com.mapped.publisher.audit.event.*;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.*;
import com.mapped.publisher.js.BaseJsonResponse;
import com.mapped.publisher.persistence.PageAttachType;
import com.mapped.publisher.persistence.TourCmpyMgr;
import com.mapped.publisher.persistence.TripBrandingMgr;
import com.mapped.publisher.persistence.TripMgr;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.persistence.communicator.MessengerUpdateHelper;
import com.mapped.publisher.utils.*;
import com.mapped.publisher.view.*;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.helper.TripPublisherHelper;
import models.publisher.*;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.db.DB;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import static controllers.ImageController.uploadHelper;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-01
 * Time: 11:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class TourController
    extends Controller {

  @Inject
  FormFactory formFactory;

  public static TripsView buildTripsView(SessionMgr sessionMgr,
                                         String term,
                                         int searchType,
                                         String cmpyId,
                                         int startPos,
                                         String tableName,
                                         String tripSearchTerm) {
    TripsView view = new TripsView();

    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      view.message = msg;
    }

    view.searchType = searchType;
    view.tableName = tableName;
    view.isTour = true;

    Credentials cred = sessionMgr.getCredentials();

    Map<String, Company> cmpiesMap = new HashMap<>();

    ArrayList<String> cmpies = new ArrayList<>();

    HashMap<String, String> agencyList = new HashMap<>();
    if(cred.hasCompany()) {
      agencyList.put(cred.getCmpyId(), cred.getCmpyName());
      cmpies.add(cred.getCmpyId());
      cmpiesMap.put(cred.getCmpyId(), cred.getCompany());
    }

    view.tripAgencyList = agencyList;

    //filter cmpy
    if (cmpyId != null && !cmpyId.equalsIgnoreCase("All")) {
      //filter cmpies to only cmpy selected
      if (cmpies.contains(cmpyId)) {
        cmpies = new ArrayList<>();
        cmpies.add(cmpyId);
      }
    }
    view.searchTerm = tripSearchTerm;
    if (view.cmpyId == null || view.cmpyId.isEmpty()) {
      view.cmpyId = "All";
    }
    else {
      view.cmpyId = cmpyId;
    }


    view.searchResults = TourController.searchMyTours(sessionMgr, term, searchType, cmpyId, startPos);
    view.searchResultsCount = 0;

    view.nextPos = 0;
    view.prevPos = -1;
    if (startPos >= APPConstants.MAX_RESULT_COUNT) {
      view.prevPos = startPos - APPConstants.MAX_RESULT_COUNT + 1;
      if (view.prevPos < 0) {
        view.prevPos = -1;
      }
    }

    if (view.searchResults != null) {
      view.searchResultsCount = view.searchResults.size();

      if (view.searchResults.size() == APPConstants.MAX_RESULT_COUNT) {
        //there are possibly more records
        view.nextPos = startPos + APPConstants.MAX_RESULT_COUNT;
      }
    }

    //hide the company tabs if the user has no extra access
    if (cred.isCmpyAdmin() || cred.hasGroupies()) {
      view.displayCmpyTabs = true;
    }
    else {
      view.displayCmpyTabs = false;
    }

    return view;
  }

  @With({Authenticated.class, Authorized.class})
  public Result myTours() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    Form<TripSearchForm> tripForm = formFactory.form(TripSearchForm.class);
    TripSearchForm tripSearchForm = tripForm.bindFromRequest().get();

    String term = "";
    if (tripSearchForm.getInSearchTerm() != null && tripSearchForm.getInSearchTerm().length() > 0) {
      term = "%" + tripSearchForm.getInSearchTerm().replace(" ", "%") + "%";
    }
    else {
      term = "%";
    }

    int searchType = tripSearchForm.getSearchType();
    if (searchType != APPConstants.SEARCH_MY_PENDING_TRIPS && searchType != APPConstants.SEARCH_MY_TRIPS) {
      searchType = APPConstants.SEARCH_MY_TRIPS;
    }

    int startPos = tripSearchForm.getInStartPos();
    if (startPos < 0) {
      startPos = 0;
    }

    String cmpyId = tripSearchForm.getInCmpyId();

    TripsView view = buildTripsView(sessionMgr,
                                    term,
                                    searchType,
                                    cmpyId,
                                    startPos,
                                    tripSearchForm.getInTableName(),
                                    tripSearchForm.getInSearchTerm());

    return ok(views.html.trip.searchResults.render(view));
  }

  /* Search utility methods */
  public static List<TripInfoView> searchMyTours(SessionMgr sessionMgr,
                                                 String term,
                                                 int searchType,
                                                 String cmpyId,
                                                 int startPos) {
    int maxRows = APPConstants.MAX_RESULT_COUNT;

    Credentials cred = sessionMgr.getCredentials();

    Map<String, Company> cmpiesMap = new HashMap<>();

    ArrayList<String> cmpies = new ArrayList<>();

    HashMap<String, String> agencyList = new HashMap<>();

    if(cred.hasCompany()) {
      agencyList.put(cred.getCmpyId(), cred.getCmpyName());
      cmpies.add(cred.getCmpyId());
      cmpiesMap.put(cred.getCmpyId(), cred.getCompany());
    }

    //filter cmpy
    if (cmpyId != null && !cmpyId.equalsIgnoreCase("All")) {
      //filter cmpies to only cmpy selected
      if (cmpies.contains(cmpyId)) {
        cmpies = new ArrayList<>();
        cmpies.add(cmpyId);
      }
    }

    List<Trip> searchResults = new ArrayList<>();
    if (searchType == APPConstants.SEARCH_MY_PENDING_TRIPS) {
      searchResults = Trip.findPendingTourByUserId(sessionMgr.getUserId(), maxRows, cmpies, term, startPos);
    }
    else if (searchType == APPConstants.SEARCH_MY_TRIPS) {
      searchResults = Trip.findActiveTourByUserId(sessionMgr.getUserId(), maxRows, cmpies, term, startPos);
    }

    return TripController.buildTripView(searchResults, cmpiesMap, null);
  }

  @With({Authenticated.class, Authorized.class})
  public Result otherTours() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<TripSearchForm> tripForm = formFactory.form(TripSearchForm.class);
    TripSearchForm tripSearchForm = tripForm.bindFromRequest().get();

    String term = "";
    if (tripSearchForm.getInSearchTerm() != null && tripSearchForm.getInSearchTerm().length() > 0) {
      term = "%" + tripSearchForm.getInSearchTerm().replace(" ", "%") + "%";
    }
    else {
      term = "%";
    }

    int searchType = tripSearchForm.getSearchType();
    if (searchType != APPConstants.SEARCH_OTHER_PENDING_TRIPS && searchType != APPConstants.SEARCH_OTHER_TRIPS) {
      searchType = APPConstants.SEARCH_OTHER_TRIPS;
    }

    String cmpyId = tripSearchForm.getInCmpyId();

    int startPos = tripSearchForm.getInStartPos();
    if (startPos < 0) {
      startPos = 0;
    }

    TripsView view = new TripsView();

    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      view.message = msg;
    }

    view.searchType = searchType;
    view.tableName = tripSearchForm.getInTableName();
    view.isTour = true;


    Credentials cred = sessionMgr.getCredentials();

    Map<String, Company> cmpiesMap = new HashMap<String, Company>();

    ArrayList<String> cmpies = new ArrayList<String>();

    HashMap<String, String> agencyList = new HashMap<>();

    if(cred.hasCompany()) {
      agencyList.put(cred.getCmpyId(), cred.getCmpyName());
      cmpies.add(cred.getCmpyId());
      cmpiesMap.put(cred.getCmpyId(), cred.getCompany());
    }

    view.tripAgencyList = agencyList;

    //filter cmpy
    if (cmpyId != null && !cmpyId.equalsIgnoreCase("All")) {
      //filter cmpies to only cmpy selected
      if (cmpies.contains(cmpyId)) {
        cmpies = new ArrayList<String>();
        cmpies.add(cmpyId);
      }
    }
    view.searchTerm = tripSearchForm.getInSearchTerm();
    view.cmpyId = cmpyId;

    view.searchResults = TourController.searchOtherTours(sessionMgr, term, searchType, cmpyId, startPos);
    view.nextPos = 0;
    view.prevPos = -1;
    if (startPos >= APPConstants.MAX_RESULT_COUNT) {
      view.prevPos = startPos - APPConstants.MAX_RESULT_COUNT + 1;
      if (view.prevPos < 0) {
        view.prevPos = -1;
      }
    }

    if (view.searchResults != null) {
      view.searchResultsCount = view.searchResults.size();


      if (view.searchResults.size() == APPConstants.MAX_RESULT_COUNT) {
        //there are more records
        view.nextPos = startPos + APPConstants.MAX_RESULT_COUNT;
      }
    }

    return ok(views.html.trip.searchResults.render(view));
  }

  public static List<TripInfoView> searchOtherTours(SessionMgr sessionMgr,
                                                    String term,
                                                    int searchType,
                                                    String cmpyId,
                                                    int startPos) {


    int maxRows = APPConstants.MAX_RESULT_COUNT;

    Credentials cred = sessionMgr.getCredentials();

    Map<String, Company> cmpiesMap = new HashMap<String, Company>();

    ArrayList<String> cmpies = new ArrayList<String>();

    HashMap<String, String> agencyList = new HashMap<String, String>();

    if(cred.hasCompany()) {
      agencyList.put(cred.getCmpyId(), cred.getCmpyName());
      cmpies.add(cred.getCmpyId());
      cmpiesMap.put(cred.getCmpyId(), cred.getCompany());
    }

    //filter cmpy
    if (cmpyId != null && !cmpyId.equalsIgnoreCase("All")) {
      //filter cmpies to only cmpy selected
      if (cmpies.contains(cmpyId)) {
        cmpies = new ArrayList<String>();
        cmpies.add(cmpyId);
      }
    }

    List<Trip> searchResults = new ArrayList<Trip>();


    if (cred.isCmpyAdmin()) {
      //get all companies where the user is the admin
      List<String> adminCmpies = new ArrayList<>();
      adminCmpies.add(cred.getCmpyId());

      if (searchType == APPConstants.SEARCH_OTHER_PENDING_TRIPS) {
        List<Trip> pTrips = Trip.findOtherPendingTourByCmpyIds(sessionMgr.getUserId(),
                                                               maxRows,
                                                               adminCmpies,
                                                               term,
                                                               startPos);
        if (pTrips != null) {
          searchResults.addAll(pTrips);
        }
      }
      else if (searchType == APPConstants.SEARCH_OTHER_TRIPS) {
        List<Trip> pTrips = Trip.findActiveToursByCmpyIds(sessionMgr.getUserId(), maxRows, adminCmpies, term, startPos);
        if (pTrips != null) {
          searchResults.addAll(pTrips);
        }
      }
    }


    if (cred.hasGroupies()) {
      List<String> users = new ArrayList<>();
      List<String> memberCmpies = new ArrayList<>();

      List<String> groups = new ArrayList<>();
      groups.addAll(cred.getUserGroups().keySet());
      memberCmpies.add(cred.getCmpyId());

      for (String groupId : groups) {
        users.addAll(cred.getUserGroups().get(groupId));
      }

      if (users.size() > 0) {
        if (searchType == APPConstants.SEARCH_OTHER_PENDING_TRIPS) {
          List<Trip> pTrips = Trip.findPendingToursByUserIds(maxRows, users, term, startPos, memberCmpies);
          if (pTrips != null) {
            searchResults.addAll(pTrips);
          }
        }
        else {
          List<Trip> pTrips = Trip.findActiveToursByUserIds(maxRows, users, term, startPos, memberCmpies);
          if (pTrips != null) {
            searchResults.addAll(pTrips);
          }
        }
      }
    }


    HashMap<String, String> userProfiles = new HashMap<String, String>();

    if (searchResults != null) {
      ArrayList<String> userIds = new ArrayList<String>();
      for (Trip t : searchResults) {
        if (t.createdby != null && !userIds.contains(t.createdby)) {
          userIds.add(t.createdby);
        }
      }
      List<UserProfile> users = UserProfile.getUserProfiles(userIds);
      for (UserProfile u : users) {
        if (!userProfiles.containsKey(u.getUserid())) {
          if (u.getFirstname() != null) {
            userProfiles.put(u.getUserid(), u.getFirstname() + " " + u.getLastname());
          }
        }
      }
    }


    List<TripInfoView> tripViews = TripController.buildTripView(searchResults, cmpiesMap, userProfiles);

    for (TripInfoView t : tripViews) {
      if (cred.isCmpyAdmin(t.tripAgencyId)) {
        t.isCmpyAdmin = true;
      }
    }
    return tripViews;
  }


  public static TripReviewView reviewTourHelper(String tripId, String groupId, Trip trip, SessionMgr sessionMgr, SecurityMgr.AccessLevel accessLevel) {
    TripReviewView view = new TripReviewView();
    try {
      String msg = flash(SessionConstants.SESSION_PARAM_MSG);
      view.message = msg;
      view.tripId = tripId;
      view.tripCmpyId = trip.cmpyid;
      view.accessLevel = accessLevel;
      view.userId = sessionMgr.getUserId();

      TripGroup group = null;
      if (groupId != null) {
        group = TripGroup.findByPK(groupId);
        if (group == null || !group.getTripid().equals(trip.tripid)) {
          group = null;
          groupId = null;
        } else {
          view.groupId = groupId;
          view.groupName = group.getName();
        }
      }

      if (trip.starttimestamp > 0) {
        view.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
        view.tripStartDate = String.valueOf(trip.starttimestamp);
      }


      if (trip.endtimestamp > 0) {
        view.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
        view.tripEndDate = Utils.getDateString(trip.endtimestamp);
      }
      view.tripName = trip.name;

      //look for trip publish status at the group level if not default group
      //look for self registration at the group level if not default group
      if (group != null) {
        view.tripStatus = group.publishstatus;

        if (group.allowselfregistration == APPConstants.ALLOW_SELF_REGISTRATION) {
          view.allowSelfRegistration = true;
        } else {
          view.allowSelfRegistration = false;
        }

        view.compositeId = TourController.getCompositePK(tripId, group.groupid);
      } else {
        TripPublishHistory pubHistory = TripPublishHistory.getPublishedDefaultGroup(trip.tripid);
        if (pubHistory != null) {
          view.tripStatus = pubHistory.getStatus();
        } else {
          view.tripStatus = APPConstants.STATUS_PUBLISHED_REVIEW;
        }

        if (trip.visibility == APPConstants.ALLOW_SELF_REGISTRATION) {
          view.allowSelfRegistration = true;
        } else {
          view.allowSelfRegistration = false;
        }

        view.compositeId = tripId;
      }
      //get passengers and check if they are registered?
      List<AccountTripLink> tripLinks = null;

      //look for passengers at the group level if not default group
      if (group != null) {
        tripLinks = AccountTripLink.findByTripGroup(tripId, group.groupid);
      } else {
        tripLinks = AccountTripLink.findByTripNoGroup(tripId);
      }
      if (tripLinks != null && tripLinks.size() > 0) {
        List<TripPassengerInfoView> passengerInfoViews = new ArrayList<TripPassengerInfoView>();
        for (AccountTripLink tripLink : tripLinks) {
          Account traveler = Account.find.byId(tripLink.getPk().getUid());
          TripPassengerInfoView passengerInfoView = new TripPassengerInfoView();
          passengerInfoView.tripId = tripId;
          passengerInfoView.id = String.valueOf(traveler.getUid());
          passengerInfoView.email = traveler.getEmail();
          passengerInfoView.name = traveler.getFullName();
          passengerInfoView.msisdn = (traveler.getMsisdn() == null) ? "": traveler.getMsisdn();
          passengerInfoView.isUmappedUser = false;
          if (AccountSession.hasSession(traveler.getUid())) {
            passengerInfoView.isUmappedUser = true;
          }

          final long localTimeOffsetMillis = sessionMgr.getTimezoneOffsetMins() * 60 * 1000;
          WebItineraryStatsView statsView = new WebItineraryStatsView();
          statsView.uId = tripLink.getPk().getUid();
          statsView.tripId = tripId;
          statsView.clicks = tripLink.getClickCounter();
          if (tripLink.getFirstClickTs() != null) {
            statsView.firstClick = Utils.formatDateTimeSecondsPrint(tripLink.getFirstClickTs().getTime() + localTimeOffsetMillis);
          }
          if (tripLink.getLastClickTs() != null) {
            statsView.lastClick = Utils.formatDateTimeSecondsPrint(tripLink.getLastClickTs().getTime() + localTimeOffsetMillis);
          }
          passengerInfoView.webItineraryStats = statsView;
          passengerInfoViews.add(passengerInfoView);
        }
        view.passengers = passengerInfoViews;
      }

      view.tripEmailLogs = EmailController.buildEmailLogs(tripId, EmailLog.EmailTypes.TRAVELLER_ITINERARY);

          /* Build new CollaboratorsView if collaboration is allowed for this company */
      if (Company.isCollaborationEnabled(trip.cmpyid)) {
        view.collaborationEnabled = true;
        view.collabView = SharedTripsController.buildCollaboratorsView(trip,
            accessLevel,
            sessionMgr.getUserId());
        view.collabView.collabEmailLogs = view.tripEmailLogs;

      } else {
        view.collaborationEnabled = false;
      }



      //get any agents for this tour
      //get passengers and check if they are registered?
      List<TripAgent> agents = null;

      //look for passengers at the group level if not default group
      if (group != null) {
        agents = TripAgent.getByTripIdGroupId(tripId, group.groupid);
      } else {
        agents = TripAgent.getByTripIdDefaultGroup(tripId);
      }
      if (agents != null && agents.size() > 0) {
        List<TripAgentView> agentViews = new ArrayList<TripAgentView>();
        for (TripAgent agent : agents) {
          TripAgentView agentView = new TripAgentView();
          agentView.pk = agent.getPk();
          agentView.tripId = tripId;
          if (group != null) {
            agentView.groupId = group.getGroupid();
          }
          agentView.agentEmail = agent.email;
          agentView.agentName = agent.name;

          agentViews.add(agentView);
        }
        view.agents = agentViews;
      }

      HashMap<String, String> tours = new HashMap<String, String>();

      //get bookings in chronological order
      List<TripDetail> bookings = TripDetail.findActiveByTripId(tripId);

      //sort everything chronologically
      Collections.sort(bookings, TripDetail.TripDetailComparator);



      //get guides
      //build the guides
      //
      List<TripDestination> guideList = TripDestination.getActiveByTripId(tripId);
      for (String tourId : tours.keySet()) {
        List<TripDestination> tripDests = TripDestination.getActiveByTripId(tourId);
        if (tripDests != null) {
          guideList.addAll(tripDests);
        }
      }
      Map<String, TripDestination> guides = new HashMap<String, TripDestination>();
      for (TripDestination td : guideList) {
        if (td.getStatus() == APPConstants.STATUS_ACTIVE && !guides.containsKey(td.getDestinationid())) {
          guides.put(td.getDestinationid(), td);
        }
      }

      List<TripDestination> documents = TripDestination.getActiveByTripId(tripId);
      int numNotes = TripNote.tripNoteCount(tripId);
      int numAttachments = TripAttachment.activeCountByTripId(tripId);
      //if there are no bookings and no guides and no files - do not publish
      if ((bookings == null || bookings.size() == 0) && (documents == null || documents.size() == 0) &&
          numAttachments == 0 && numNotes == 0) {
        return null;
      }

      //get publishing history
      List<TripPublishHistory> publishHistories = null;
      //look for passengers at the group level if not default group
      if (group != null) {
        publishHistories = TripPublishHistory.findByTripGroupId(tripId, group.groupid);
      } else {
        publishHistories = TripPublishHistory.findByTripDefaultGroup(tripId);
      }

      if (publishHistories != null && publishHistories.size() > 0) {
        ArrayList<TripPublishingView> publishHistoryView = new ArrayList<TripPublishingView>();
        for (TripPublishHistory h : publishHistories) {
          TripPublishingView v = new TripPublishingView();
          v.pk = h.pk;
          v.comments = h.comments;
          //adjust for local timezone
          long localTimestamp = h.createdtimestamp + (sessionMgr.getTimezoneOffsetMins() * 60 * 1000);

          v.publishedDate = Utils.formatDateTimePrint(localTimestamp);

          v.tripId = h.tripid;
          if (h.status == APPConstants.STATUS_PUBLISHED) {
            v.status = APPConstants.STATUS_PUBLISHED_DESC;
          } else {
            v.status = APPConstants.STATUS_PUBLISHED_REVIEW_DESC;
          }

          publishHistoryView.add(v);

        }
        view.publishList = publishHistoryView;
      }

      //add all the groups for this trip.
      List<TripGroup> groups = TripGroup.findActiveByTripId(tripId);
      if (groups != null) {
        view.groups = new ArrayList<TripGroupView>();
        for (TripGroup g : groups) {
          TripGroupView groupView = new TripGroupView();
          groupView.groupName = g.name;
          groupView.groupId = g.getGroupid();
          view.groups.add(groupView);
        }
      }


      //check security to make sure we should display branding
      //restricted to users that belong to the same company
      if (accessLevel.ge(SecurityMgr.AccessLevel.APPEND_N_PUBLISH) || SecurityMgr.canAccessCmpy(trip.cmpyid, sessionMgr)) {


        //check for cobranding updates
        //delete first to handle any new parent cmpy id link properly
        TripBrandingMgr.deleteUnsharedCmpies(trip.tripid);
        TripBrandingMgr.addNewSharedCmpies(trip.tripid, trip.cmpyid);

        //build branding view
        view.tripBranding = TripBrandingMgr.findBrandedCmpies(trip);
      }

      //collaboration
                  /* Build new CollaboratorsView if collaboration is allowed for this company */
      if (Company.isCollaborationEnabled(trip.cmpyid)) {
        view.collabView = SharedTripsController.buildCollaboratorsView(trip, accessLevel, sessionMgr.getUserId());
        view.collaborationEnabled = true;
      } else {
        view.collaborationEnabled = false;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return view;
  }

  /*
   This method should take into account the default traveller group (no explicit group created) or a specified
   traveller group.
   */
  @With({Authenticated.class, Authorized.class})
  public Result reviewTour() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    String groupId = flash(SessionConstants.SESSION_PARAM_GROUPID);
    final DynamicForm form = formFactory.form().bindFromRequest();
    String tripId = form.get("tripId");

    if (tripId == null) {
      tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
    }

    if (groupId == null) {
      groupId = form.get("inGroupId");
    }


    if (tripId != null) {
      Trip trip = Trip.find.byId(tripId);
      SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
      if (accessLevel.ge(SecurityMgr.AccessLevel.READ)) {
        TripReviewView view = reviewTourHelper(tripId, groupId, trip, sessionMgr, accessLevel);
        if (view != null) {
          return ok(views.html.trip.review.render(view));
        } else {
          BaseView viewError = new BaseView();
          viewError.message = "No bookings or Documents to publish";
          return ok(views.html.common.message.render(viewError));
        }
      }
    }

    BaseView view = new BaseView();
    view.message = "System Error";
    return ok(views.html.common.message.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result simpleReviewTour() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    String groupId = flash(SessionConstants.SESSION_PARAM_GROUPID);
    final DynamicForm form = formFactory.form().bindFromRequest();
    String tripId = form.get("tripId");

    if (tripId == null) {
      tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
    }

    if (groupId == null) {
      groupId = form.get("inGroupId");
    }


    if (tripId != null) {
      Trip trip = Trip.find.byId(tripId);
      SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
      if (accessLevel.ge(SecurityMgr.AccessLevel.READ)) {
        TripReviewView view = reviewTourHelper(tripId, groupId, trip, sessionMgr, accessLevel);
        if (view != null) {
          return ok(views.html.trip.simpleReview.render(view));
        } else {
          BaseView viewError = new BaseView();
          viewError.message = "No bookings or Documents to publish";
          return ok(views.html.common.message.render(viewError));
        }
      }
    }

    BaseView view = new BaseView();
    view.message = "System Error";
    return ok(views.html.common.message.render(view));
  }

  /**
   * Checks to see if groups are allowed for a trip
   * @param trip - trip to check
   * @return
   */
  private static boolean areGroupsAllowed(Trip trip) {
    List<TripInvite> tripInvites = TripInvite.getTripInvites(trip);
    List<TripShare> tripShares = TripShare.getTripCollaborators(trip);

    if ((tripInvites != null && tripInvites.size() > 0) ||
        (tripShares != null && tripShares.size() > 0)) {
      return false;
    }

    return true;
  }

  public static String getCompositePK(String tripId, String groupId) {
    if (groupId == null || groupId.trim().length() == 0) {
      return tripId;
    }
    else {
      String s = tripId + APPConstants.TRIP_GROUP_SEPARATOR + groupId;
      return s;
    }

  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteTour() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    final DynamicForm form = formFactory.form().bindFromRequest();
    final String tripId = form.get("tripId");
    Trip tripModel = Trip.find.byId(tripId);

    if (SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).ge(SecurityMgr.AccessLevel.OWNER)) {

      /*if (tripModel.getStatus() == APPConstants.STATUS_PUBLISHED) {
        BaseView baseView = new BaseView();
        baseView.message = "Delete Error - This trip has already been published.";
        return ok(views.html.common.message.render(baseView));
      }

      //Check if there are any groups that have been published
      List<TripGroup> groups = TripGroup.findActiveByTripId(tripModel.tripid);
      if (groups != null) {
        for (TripGroup group : groups) {
          if (group.getPublishstatus() == APPConstants.STATUS_PUBLISHED) {
            BaseView baseView = new BaseView();
            baseView.message = "Delete Error - This trip has already been published.";
            return ok(views.html.common.message.render(baseView));
          }
        }
      }*/

            /*
            //check to see if this tour is part of any trip
            //if yes, do not delete and notify user
            try {
                int c = TourBookingMgr.countActiveBookings(t.tripid);
                if (c > 0) {
                    BaseView baseView = new BaseView();
                    baseView.message = "Delete Error. This tour is currently part of " + c + " active trips.";
                    return ok(views.html.common.message.render(baseView));
                }
            } catch (Exception e) {
              e.printStackTrace();
            }
            */
      Connection conn = null;
      try {
        tripModel.setStatus(APPConstants.STATUS_DELETED);
        tripModel.setLastupdatedtimestamp(System.currentTimeMillis());
        tripModel.setModifiedby(sessionMgr.getUserId());
        tripModel.save();

        MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId()).wipeTrip();

        //Create audit log
        audit:
        {
          TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP, AuditActionType.DELETE, AuditActorType.WEB_USER)
                                  .withTrip(tripModel)
                                  .withCmpyid(tripModel.cmpyid)
                                  .withUserid(sessionMgr.getUserId());

          ((AuditTrip) ta.getDetails()).withName(tripModel.getName())
                                       .withId(tripModel.getTripid())
                                       .withStatus(AuditTrip.TripStatus.DELETED);
          ta.save();;
        }

        conn = DBConnectionMgr.getConnection4Publisher();
        TourCmpyMgr.deleteByTourId(tripModel.tripid, conn);

        flash(SessionConstants.SESSION_PARAM_MSG, "Trip - " + tripModel.name + " - has been deleted.");
        return redirect(routes.Application.dashboard());
      }
      catch (Exception e) {
        e.printStackTrace();
      }
      finally {
        if (conn != null) {
          try {
            conn.close();
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }
      }

    }
    BaseView baseView = new BaseView();
    baseView.message = "Error Deleting the trip";
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result tours() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<TripSearchForm> tripForm = formFactory.form(TripSearchForm.class);
    TripSearchForm tripSearchForm = tripForm.bindFromRequest().get();

    String term = "";
    if (tripSearchForm.getInSearchTerm() != null && tripSearchForm.getInSearchTerm().length() > 0) {
      term = "%" + tripSearchForm.getInSearchTerm().replace(" ", "%") + "%";
    }
    else {
      term = "%";
    }

    int startPos = tripSearchForm.getInStartPos();
    if (startPos < 0) {
      startPos = 0;
    }

    String cmpyId = tripSearchForm.getInCmpyId();

    TripsView view = new TripsView();

    Credentials cred = sessionMgr.getCredentials();

    Map<String, Company> cmpiesMap = new HashMap<String, Company>();

    ArrayList<String> cmpies = new ArrayList<String>();

    HashMap<String, String> agencyList = new HashMap<String, String>();
    if(cred.hasCompany()) {
      agencyList.put(cred.getCmpyId(), cred.getCmpyName());
      cmpies.add(cred.getCmpyId());
      cmpiesMap.put(cred.getCmpyId(), cred.getCompany());
    }


    view.tripAgencyList = agencyList;

    //filter cmpy
    if (cmpyId != null && !cmpyId.equalsIgnoreCase("All")) {
      //filter cmpies to only cmpy selected
      if (cmpies.contains(cmpyId)) {
        cmpies = new ArrayList<String>();
        cmpies.add(cmpyId);
      }
    }
    view.searchTerm = tripSearchForm.getInSearchTerm();
    if (view.cmpyId == null || view.cmpyId.isEmpty()) {
      view.cmpyId = "All";
    }
    else {
      view.cmpyId = cmpyId;
    }
        /*
        view.publishedTrips = TourController.searchMyTours(sessionMgr,term, APPConstants.SEARCH_MY_TRIPS,cmpyId,
        startPos);

        view.nextPos = 0;
        view.prevPos = -1;
        if (startPos >= APPConstants.MAX_RESULT_COUNT) {
            view.prevPos = startPos - APPConstants.MAX_RESULT_COUNT + 1;
            if (view.prevPos < 0)
                view.prevPos = -1;
        }

        if (view.publishedTrips != null) {
          if (view.publishedTrips.size() == APPConstants.MAX_RESULT_COUNT) {
                //there are more records
                view.nextPos = startPos + APPConstants.MAX_RESULT_COUNT - 1;
            }
        }
        */
    //hide the company tabs if the user has no extra access
    view.displayCmpyTabs = (cred.isCmpyAdmin() || cred.hasGroupies());
    return ok(views.html.trip.trips.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result addAgent() {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      String comments = "";
      Form<TripAgentForm> agentForm = formFactory.form(TripAgentForm.class);
      TripAgentForm agentInfo = agentForm.bindFromRequest().get();


      String tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);


      if (tripId != null && agentInfo.getInEmail() != null &&
          !agentInfo.getInEmail().isEmpty() && agentInfo.getInAgentName() != null &&
          !agentInfo.getInAgentName().isEmpty()) {

        Trip trip = Trip.find.byId(tripId);

        if (TripAgent.isAgentInTrip(tripId, agentInfo.getInEmail())) {
          BaseView baseView = new BaseView();
          baseView.message = "This travel agent has already been added to this Trip";
          return ok(views.html.common.message.render(baseView));
        }


        //Only owner or above can add new agents to the trip
        if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND_N_PUBLISH)) {
          //check for group
          TripGroup group = null;
          if (agentInfo.getInGroupId() != null && agentInfo.getInGroupId().trim().length() > 0) {
            group = TripGroup.findByPK(agentInfo.getInGroupId());
            if (group == null || !group.tripid.equals(trip.tripid)) {
              BaseView view = new BaseView();
              view.message = "System Error - invalid group." + agentInfo.getInGroupId();
              return ok(views.html.common.message.render(view));
            }
          }


          TripAgent tripAgent = new TripAgent();
          tripAgent.setPk(DBConnectionMgr.getUniqueId());
          tripAgent.setAgencyname(agentInfo.getInAgencyName());
          tripAgent.setEmail(agentInfo.getInEmail().toLowerCase().trim());
          tripAgent.setComments(agentInfo.getInComments());
          tripAgent.setName(agentInfo.getInAgentName());
          if (group != null) {
            tripAgent.setGroupid(agentInfo.getInGroupId());
          }
          tripAgent.setTripid(tripId);
          tripAgent.setStatus(APPConstants.STATUS_ACTIVE);
          tripAgent.setModifiedby(sessionMgr.getUserId());
          tripAgent.setLastupdatedtimestamp(System.currentTimeMillis());
          tripAgent.setCreatedby(sessionMgr.getUserId());
          tripAgent.setCreatedtimestamp(System.currentTimeMillis());

          tripAgent.save();

          //Create audit log
          audit:
          {
            TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_AGENT,
                                                 AuditActionType.ADD,
                                                 AuditActorType.WEB_USER)
                                    .withTrip(trip)
                                    .withCmpyid(trip.cmpyid)
                                    .withUserid(sessionMgr.getUserId());

            ((AuditTripAgent) ta.getDetails()).withEmail(tripAgent.getEmail())
                                              .withName(tripAgent.getName())
                                              .withId(tripAgent.getPk())
                                              .withGroupId((group != null) ? group.getGroupid() : "")
                                              .withGroupName((group != null) ? group.getName() : "Default");
            ta.save();;
          }


          //if trip has already been published, send the email notification now
          //create share  + invite

          //if there is a group, use the composite key for trip/group combination


          if (TripPublishHistory.countPublished(trip.getTripid()) > 0) {
            UserProfile up = UserProfile.find.byId(trip.getCreatedby());
            ArrayList agents = new ArrayList<>();
            agents.add(tripAgent);
            int returnCode = TripController.sendUMappedAgentEmailNotitication(agents, trip, up, null, group);
            if (returnCode != 0) {
              flash(SessionConstants.SESSION_PARAM_MSG, "Error - cannot send notification email");
            }
          }

          flash(SessionConstants.SESSION_PARAM_MSG, "Travel Agent has been added - " + tripAgent.name);
          return redirect(routes.TourController.reviewTour());


        }
      }
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Trip Add Cmpy - ", e);
    }
    BaseView view = new BaseView();
    view.message = "System Error";
    return ok(views.html.common.message.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result removeAgent() {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      final DynamicForm form = formFactory.form().bindFromRequest();
      final String pk = form.get("inTripAgentId");
      final String groupId = form.get("inGroupId");


      String tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);


      if (tripId != null && pk != null && pk.length() > 0 && tripId.length() > 0) {

        Trip trip = Trip.find.byId(tripId);

        if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND_N_PUBLISH)) {
          //check for group
          TripGroup group = null;
          if (groupId != null && groupId.trim().length() > 0) {
            group = TripGroup.findByPK(groupId.trim());
            if (group == null || !group.tripid.equals(trip.tripid)) {
              BaseView view = new BaseView();
              view.message = "System Error - invalid group." + groupId;
              return ok(views.html.common.message.render(view));
            }
          }

          TripAgent tripAgent = TripAgent.find.byId(pk);

          if (tripAgent != null && tripAgent.getStatus() == 0) {
            if (group == null || group.getGroupid().equals(tripAgent.getGroupid())) {
              tripAgent.delete();
            }
          }

          //Create audit log
          audit:
          {
            TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_AGENT,
                                                 AuditActionType.DELETE,
                                                 AuditActorType.WEB_USER)
                                    .withTrip(trip)
                                    .withCmpyid(trip.cmpyid)
                                    .withUserid(sessionMgr.getUserId());

            ((AuditTripAgent) ta.getDetails()).withEmail(tripAgent.getEmail())
                                              .withName(tripAgent.getName())
                                              .withId(tripAgent.getPk())
                                              .withGroupId((group != null) ? group.getGroupid() : "")
                                              .withGroupName((group != null) ? group.getName() : "Default");

            ta.save();;
          }

          flash(SessionConstants.SESSION_PARAM_MSG, tripAgent.name + " - has been removed.");
          return redirect(routes.TourController.reviewTour());


        }
      }
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Trip Add Cmpy - ", e);
    }
    BaseView view = new BaseView();
    view.message = "System Error";
    return ok(views.html.common.message.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result addPassengerGroup() {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      Form<TripGroupForm> tripForm = formFactory.form(TripGroupForm.class);
      TripGroupForm formInfo = tripForm.bindFromRequest().get();

      if (formInfo.getInTripId() != null && formInfo.getInName() != null) {
        if (formInfo.getInName().trim().length() == 0) {
          BaseView view = new BaseView();
          view.message = "Mandatory field missing - Please enter a group name.";
          return ok(views.html.common.message.render(view));
        }
        Trip trip = Trip.findByPK(formInfo.getInTripId());
        if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.OWNER)) {
          List<TripGroup> groups = TripGroup.findActiveByName(formInfo.getInTripId(), formInfo.getInName());
          if (groups != null && groups.size() > 0) {
            BaseView view = new BaseView();
            view.message = "Duplicate Name - A group with this name already exists.";
            return ok(views.html.common.message.render(view));
          }
          /*
          if (!areGroupsAllowed(trip)) {
            BaseView view = new BaseView();
            view.message = "Groups are not allowed for shared collaborating agent trips.";
            return ok(views.html.common.message.render(view));
          }
          */

          TripGroup group = new TripGroup();
          group.setTripid(formInfo.getInTripId());
          group.setGroupid(DBConnectionMgr.getUniqueId());
          group.setName(formInfo.getInName().trim());
          group.setComments(formInfo.getInComments());
          group.setPublishstatus(APPConstants.STATUS_PUBLISH_PENDING);
          group.setStatus(APPConstants.STATUS_ACTIVE);
          group.setCreatedby(sessionMgr.getUserId());
          group.setModifiedby(sessionMgr.getUserId());
          group.setCreatedtimestamp(System.currentTimeMillis());
          group.setLastupdatedtimestamp(System.currentTimeMillis());
          group.save();


          //Create audit log
          audit:
          {
            TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_GROUP,
                                                 AuditActionType.ADD,
                                                 AuditActorType.WEB_USER)
                                    .withTrip(trip)
                                    .withCmpyid(trip.cmpyid)
                                    .withUserid(sessionMgr.getUserId());

            ((AuditTripGroup) ta.getDetails()).withName(group.getName()).withId(group.getGroupid());
            ta.save();;
          }

          flash(SessionConstants.SESSION_PARAM_MSG, " Group - " + formInfo.getInName() + ", created successfully");
          flash(SessionConstants.SESSION_PARAM_GROUPID, group.getGroupid());
          return redirect(routes.TourController.reviewTour());

        }
        else {
          BaseView view = new BaseView();
          view.message = "Authorization Error - cannot create a group.";
          return ok(views.html.common.message.render(view));
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    BaseView view = new BaseView();
    view.message = "System Error - cannot create a group.";
    return ok(views.html.common.message.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result deletePassengerGroup() {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      Form<TripGroupForm> tripForm = formFactory.form(TripGroupForm.class);
      TripGroupForm formInfo = tripForm.bindFromRequest().get();

      if (formInfo.getInTripId() != null && formInfo.getInGroupId() != null) {
        Trip trip = Trip.findByPK(formInfo.getInTripId());
        if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.OWNER)) {

          TripGroup group = TripGroup.findByPK(formInfo.getInGroupId());
          if (group == null || !group.tripid.equals(formInfo.getInTripId())) {
            BaseView view = new BaseView();
            view.message = "Authorization Error - cannot delete the group.";
            return ok(views.html.common.message.render(view));
          }

          Connection conn = null;
          try {
            Ebean.beginTransaction();
            conn = DB.getConnection("web");
            conn.setAutoCommit(false);
            int recCount = 0;


            List<AccountTripLink> tripLinks = AccountTripLink.findByTripGroup(trip.getTripid(), group.getGroupid());
            if (group.publishstatus == APPConstants.STATUS_PUBLISHED || group.publishstatus == APPConstants
                .STATUS_PUBLISHED_REVIEW) {
              String mapPK = TourController.getCompositePK(trip.tripid, group.groupid);
              //delete the trip from the mobile if the passenger has registered
              if (tripLinks != null) {
                for (AccountTripLink tripLink : tripLinks) {
                  Account traveler = Account.find.byId(tripLink.getPk().getUid());
                  tripLink.delete();
                }
              }
            }

            if (recCount > 0) {
              conn.commit();
            }



            //Create audit log
            audit:
            {
              TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_GROUP,
                                                   AuditActionType.DELETE,
                                                   AuditActorType.WEB_USER)
                                      .withTrip(trip)
                                      .withCmpyid(trip.cmpyid)
                                      .withUserid(sessionMgr.getUserId());

              ((AuditTripGroup) ta.getDetails()).withName(group.getName()).withId(group.getGroupid());
              ta.save();;
            }


            group.setStatus(APPConstants.STATUS_DELETED);
            group.setModifiedby(sessionMgr.getUserId());
            group.setLastupdatedtimestamp(System.currentTimeMillis());
            group.save();
            flash(SessionConstants.SESSION_PARAM_MSG, " Group - " + group.getName() + ", deleted successfully");
            flash(SessionConstants.SESSION_PARAM_GROUPID, "");
            Ebean.commitTransaction();
            conn.close();
            return redirect(routes.TourController.reviewTour());
          }
          catch (Exception e) {
            e.printStackTrace();
            if (conn != null) {
              conn.rollback();
            }

            Ebean.rollbackTransaction();
          }
          finally {
            if (conn != null) {
              try {
                conn.close();
              }
              catch (Exception e) {
                e.printStackTrace();
              }
            }
          }
          //if the trip has been published


        }
        else {
          BaseView view = new BaseView();
          view.message = "Authorization Error - cannot delete the group.";
          return ok(views.html.common.message.render(view));
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    BaseView view = new BaseView();
    view.message = "System Error - cannot delete the group.";
    return ok(views.html.common.message.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result addReviewPassenger() {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      Form<TourPassengerForm> tripForm = formFactory.form(TourPassengerForm.class);
      TourPassengerForm formInfo = tripForm.bindFromRequest().get();

      if (formInfo.getInEmail() != null &&
          formInfo.getInFirstName() != null &&
          formInfo.getInLastName() != null &&
          formInfo.getInTripId() != null) {
        Trip trip = Trip.findByPK(formInfo.getInTripId());
        if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND_N_PUBLISH)) {
          //check for group
          TripGroup group = null;
          if (formInfo.getInGroupId() != null && formInfo.getInGroupId().trim().length() > 0) {
            group = TripGroup.findByPK(formInfo.getInGroupId());
            if (group == null || !group.tripid.equals(trip.tripid)) {
              return UserMessage.message(ReturnCode.LOGICAL_ERROR,
                                         "System Error - invalid group." + formInfo.getInGroupId());
            }
          }

          List<AccountTripLink> tripLinks = AccountTripLink.findByEmailTrip(formInfo.getInEmail().trim(), formInfo.getInTripId().trim());

          if (tripLinks == null || tripLinks.size() == 0) {
            String groupId = null;
            if (group != null) {
              groupId = group.getGroupid();
            }

            String name = null;
            String firstName = null;
            String lastName = null;
            if (formInfo.getInFirstName() != null && formInfo.getInLastName() != null) {
              firstName = formInfo.getInFirstName();
              lastName = formInfo.getInLastName();
            } else if (formInfo.getInName() != null) {
              name = formInfo.getInName().trim();
            }
            else {
              String[] epart = formInfo.getInEmail().split("@");
              name = epart[0];
            }
            AccountTripLink tripLink = null;
            if (firstName != null || lastName != null) {
              tripLink = AccountController.addTravelerToTrip(sessionMgr.getUserId(),
                                                  groupId,
                                                  trip.tripid,
                                                  firstName,
                                                  lastName,
                                                  formInfo.getInEmail().toLowerCase().trim(),
                                                  formInfo.getInMSISDN());
            } else {
              tripLink = AccountController.addTravelerToTrip(sessionMgr.getUserId(),
                                                             groupId,
                                                             trip.tripid,
                                                             name,
                                                             formInfo.getInEmail().toLowerCase().trim(),
                                                             formInfo.getInMSISDN());
            }
            if (tripLink != null) {
              Account traveler = Account.find.byId(tripLink.getPk().getUid());
              //Republishing whole trip as we need to add new user to every booking
              MessengerUpdateHelper.build(trip, sessionMgr.getAccountId())
                                   .modifyUserTripAccessState(tripLink.getLegacyId(), true);

              //Create audit log
              audit:
              {
                TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_TRAVELLER,
                        AuditActionType.ADD,
                        AuditActorType.WEB_USER)
                        .withTrip(trip)
                        .withCmpyid(trip.cmpyid)
                        .withUserid(sessionMgr.getUserId());

                ((AuditTripTraveller) ta.getDetails()).withName(traveler.getFullName())
                        .withId(String.valueOf(traveler.getUid()))
                        .withEmail(traveler.getEmail())
                        .withGroupId((group != null) ? group.getGroupid() : "")
                        .withGroupName((group != null) ? group.getName() : "Default");

                ta.save();
              }



              if (TripPublishHistory.countPublished(trip.getTripid()) > 0) {
                try {
                  //send notification email
                  List<Account> passengers = new ArrayList<Account>();
                  passengers.add(traveler);
                  UserProfile up;
                  TripPublishHistory tp = TripPublishHistory.getLastPublished(trip.tripid, null);
                  if (tp != null) {
                    up = UserProfile.findByPK(tp.createdby);
                  } else {
                    up = UserProfile.findByPK(trip.getCreatedby());
                  }
                  int returnCode = TripController.sendUMappedEmailNotitication(passengers, trip, up, null, group);
                  if (returnCode != 0) {
                    flash(SessionConstants.SESSION_PARAM_MSG, "Error - cannot send notification emails");
                  }


                } catch (Exception e) {
                  Log.err("addReviewPassenger", e);
                }
              }
              flash(SessionConstants.SESSION_PARAM_MSG, formInfo.getInEmail() + " added successfully.");
              flash(SessionConstants.SESSION_PARAM_TRIPID, trip.tripid);
              if (group != null) {
                flash(SessionConstants.SESSION_PARAM_GROUPID, group.groupid);
              }
              if (formInfo.getInViewType() != null && formInfo.getInViewType().equals("SimpleTripInfo")) {
                return redirect(routes.TourController.simpleReviewTour());
              } else {
                return redirect(routes.TourController.reviewTour());
              }
            } else {
              BaseView view = new BaseView();
              view.message = "Cannot add this traveler, please contact support.";
              return ok(views.html.common.message.render(view));
            }
          }
          else {
            BaseView view = new BaseView();
            view.message = "This email is already registered.";
            return ok(views.html.common.message.render(view));
          }
        }
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }

    BaseView view = new BaseView();
    view.message = "System Error - cannot add the email.";
    return ok(views.html.common.message.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteReviewPassenger() {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      Form<PassengerForm> tripForm = formFactory.form(PassengerForm.class);
      PassengerForm formInfo = tripForm.bindFromRequest().get();

      if (formInfo.getInEmail() != null && formInfo.getInPassengerId() != null && formInfo.getInTripId() != null) {
        Trip trip = Trip.findByPK(formInfo.getInTripId());
        if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND_N_PUBLISH)) {
          TripGroup group = null;
          if (formInfo.getInGroupId() != null && formInfo.getInGroupId().trim().length() > 0) {
            group = TripGroup.findByPK(formInfo.getInGroupId());
            if (group == null || !group.tripid.equals(trip.tripid)) {
              return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND, "Invalid Group", formInfo.getInGroupId());
            }
          }

          AccountTripLink tripLink = AccountTripLink.findByUid(Long.parseLong(formInfo.getInPassengerId()), trip.getTripid());
          if (tripLink != null) {
            Account traveler = Account.find.byId(tripLink.getPk().getUid());
            tripLink.delete();

            MessengerUpdateHelper.build(trip, sessionMgr.getAccountId())
                                 .modifyUserTripAccessState(tripLink.getLegacyId(), false);

            //Create audit log
            audit:
            {
              TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_TRAVELLER,
                                                   AuditActionType.DELETE,
                                                   AuditActorType.WEB_USER)
                                      .withTrip(trip)
                                      .withCmpyid(trip.cmpyid)
                                      .withUserid(sessionMgr.getUserId());

              ((AuditTripTraveller) ta.getDetails()).withName(traveler.getFullName())
                                                    .withId(Communicator.getTravelerId(tripLink))
                                                    .withEmail(traveler.getEmail())
                                                    .withGroupId((group != null) ? group.getGroupid() : "")
                                                    .withGroupName((group != null) ? group.getName() : "Default");
              ta.save();
            }




            flash(SessionConstants.SESSION_PARAM_MSG, formInfo.getInEmail() + " removed successfully.");
            flash(SessionConstants.SESSION_PARAM_TRIPID, trip.tripid);
            if (group != null) {
              flash(SessionConstants.SESSION_PARAM_GROUPID, group.groupid);
            }
            if (formInfo.getInOriginPage() != null && formInfo.getInOriginPage().equals("SimpleTripInfo")) {
              return redirect(routes.TourController.simpleReviewTour());
            }
            return redirect(routes.TourController.reviewTour());
          }
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    BaseView view = new BaseView();
    view.message = "System Error - cannot add the email.";
    return ok(views.html.common.message.render(view));
  }

  public Result addPassenger() {
    try {
      SessionMgr sessionMgr = new SessionMgr(session());
      Form<TourPassengerForm> tripForm = formFactory.form(TourPassengerForm.class);
      TourPassengerForm formInfo = tripForm.bindFromRequest().get();

      if (formInfo.getInEmail() != null && formInfo.getInEndDate() != null &&
          formInfo.getInStartDate() != null &&
          formInfo.getInTripId() != null) {
        Trip trip = Trip.findByPK(formInfo.getInTripId());
        if (trip != null & trip.getStatus() != APPConstants.STATUS_DELETED &&
            String.valueOf(trip.starttimestamp).equals(formInfo.getInStartDate()) && String.valueOf(trip.endtimestamp)
                                                                                           .equals(formInfo.getInEndDate())) {
          TripGroup group = null;
          if (formInfo.getInGroupId() != null && formInfo.getInGroupId().trim().length() > 0) {
            group = TripGroup.findByPK(formInfo.getInGroupId());
            if (group == null || !group.tripid.equals(trip.tripid) ||
                (group.publishstatus != APPConstants.STATUS_PUBLISHED && group.publishstatus != APPConstants
                    .STATUS_PUBLISH_PENDING) ||
                group.allowselfregistration != APPConstants.ALLOW_SELF_REGISTRATION) {
              BaseView view = new BaseView();
              view.message = "System Error - invalid group." + formInfo.getInGroupId();
              return ok(views.html.common.message.render(view));
            }
          }
          else {
            if ((trip.getStatus() != APPConstants.STATUS_PUBLISHED && trip.getStatus() != APPConstants
                .STATUS_PUBLISHED_REVIEW) || trip.getVisibility() != APPConstants.ALLOW_SELF_REGISTRATION) {
              BaseView view = new BaseView();
              view.message = "System Error - invalid status.";
              return ok(views.html.common.message.render(view));
            }
          }

          TripPublisherHelper.TravelerResponse response = TripPublisherHelper.addPassenger(formInfo.getInFirstName(),
                  formInfo.getInLastName(),
                  formInfo.getInEmail(),
                  formInfo.getInMSISDN(),
                  sessionMgr.getUserId(),
                  trip,
                  group,
                  true, null);

          BaseView view = null;
          StringBuilder sb = new StringBuilder();
          switch (response) {
            case ERR_EMAIL:
              sb.append("Error - cannot send notification emails. ");
              //still ok - so we just add a message and return ok
            case OK:
              sb.append(formInfo.getInEmail() + " added to this trip.");
              flash(SessionConstants.SESSION_PARAM_MSG, sb.toString());
              flash(SessionConstants.SESSION_PARAM_TRIPID, trip.tripid);
              if (group != null) {
                flash(SessionConstants.SESSION_PARAM_GROUPID, group.groupid);
              }
              return this.getPassengers();
            case ERR_DUPLICATE:
              view = new BaseView();
              view.message = "This email is already registered.";
              return ok(views.html.common.message.render(view));
            case ERR_UNDEFINED:
              view = new BaseView();
              view.message = "Cannot add this traveler - please contact support";
              return ok(views.html.common.message.render(view));
          }
        }
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }

    BaseView view = new BaseView();
    view.message = "System Error - cannot add the email.";
    return ok(views.html.common.message.render(view));
  }

  public Result getPassengers() {
    try {
      SessionMgr sessionMgr = new SessionMgr(session());
      String tripId = flash(SessionConstants.SESSION_PARAM_TRIPID);
      String groupId = flash(SessionConstants.SESSION_PARAM_GROUPID);


      if (tripId != null) {
        Trip trip = Trip.findByPK(tripId);
        if (trip != null && trip.status != APPConstants.STATUS_DELETED) {
          TripGroup group = null;
          if (groupId != null && groupId.trim().length() > 0) {
            group = TripGroup.findByPK(groupId);
            if (group == null || !group.tripid.equals(trip.tripid) || (group.publishstatus != APPConstants
                .STATUS_PUBLISHED && group.publishstatus != APPConstants.STATUS_PUBLISHED_REVIEW) ||
                group.allowselfregistration != APPConstants.ALLOW_SELF_REGISTRATION) {
              BaseView view = new BaseView();
              view.message = "System Error - cannot get emails.";
              return ok(views.html.common.message.render(view));
            }
          }
          else if ((trip.getStatus() != APPConstants.STATUS_PUBLISHED && trip.getStatus() != APPConstants
              .STATUS_PUBLISHED_REVIEW) || trip.visibility != APPConstants.ALLOW_SELF_REGISTRATION) {
            BaseView view = new BaseView();
            view.message = "System Error - cannot get emails.";
            return ok(views.html.common.message.render(view));
          }


          List<AccountTripLink> tripLinks = null;

          String cacheId = trip.tripid;
          if (group != null) {
            tripLinks = AccountTripLink.findByTripGroup(tripId, groupId);
            cacheId = cacheId + group.groupid;
          }
          else {
            tripLinks = AccountTripLink.findByTripNoGroup(tripId);
          }



          TripPreviewView previewView = (TripPreviewView) CacheMgr.get(APPConstants.CACHE_WEB_ITINERARY_PREFX + cacheId);
          if (previewView == null) {
            previewView = new TripPreviewView();
          }
          previewView.passengers = new ArrayList<TripPassengerView>();
          previewView.tripId = trip.tripid;

          if (group != null) {
            previewView.groupId = group.groupid;
          }

          previewView.tripEndDateMs = String.valueOf(trip.endtimestamp);
          previewView.tripStartDateMs = String.valueOf(trip.starttimestamp);
          if (tripLinks != null && tripLinks.size() > 0) {
            List<TripPassengerView> passengerInfoViews = new ArrayList<TripPassengerView>();
            for (AccountTripLink traveler : tripLinks) {
              Account account = Account.find.byId(traveler.getPk().getUid());
              TripPassengerView passengerInfoView = new TripPassengerView();
              passengerInfoView.id = String.valueOf(account.getUid());
              passengerInfoView.email = account.getEmail();

              passengerInfoViews.add(passengerInfoView);
            }
            previewView.passengers = passengerInfoViews;
          }
          previewView.wasPublished = (TripPublishHistory.countPublished(trip.tripid) > 0); //George: toggle for draft B/G in PDF


          //handle cross timezone booking
          if (previewView.bookings != null && previewView.bookings.flights != null && previewView.bookings.flights.size() > 0) {
            previewView.bookings.handleCrosTimezoneFlights();
          }

          CacheMgr.set(APPConstants.CACHE_WEB_ITINERARY_PREFX+cacheId, previewView);

          previewView.message = flash(SessionConstants.SESSION_PARAM_MSG);

          return ok(views.html.webItinerary.mobile.render(previewView));

        }
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }

    BaseView view = new BaseView();
    view.message = "System Error - cannot get emails.";
    return ok(views.html.common.message.render(view));
  }

  public Result deletePassenger() {
    try {
      Form<PassengerForm> tripForm = formFactory.form(PassengerForm.class);
      PassengerForm formInfo = tripForm.bindFromRequest().get();

      if (formInfo.getInEmail() != null && formInfo.getInPassengerId() != null && formInfo.getInTripId() != null) {
        Trip trip = Trip.findByPK(formInfo.getInTripId());
        if (trip != null && trip.getStatus() != APPConstants.STATUS_DELETED) {
          TripGroup group = null;
          if (formInfo.getInGroupId() != null && formInfo.getInGroupId().trim().length() > 0) {
            group = TripGroup.findByPK(formInfo.getInGroupId());
            if (group == null || !group.tripid.equals(trip.tripid) || (group.publishstatus != APPConstants
                .STATUS_PUBLISHED && group.publishstatus != APPConstants.STATUS_PUBLISH_PENDING) ||
                group.allowselfregistration != APPConstants.ALLOW_SELF_REGISTRATION) {
              BaseView view = new BaseView();
              view.message = "System Error - invalid group." + formInfo.getInGroupId();
              return ok(views.html.common.message.render(view));
            }
          }
          else {
            if ((trip.getStatus() != APPConstants.STATUS_PUBLISHED && trip.getStatus() != APPConstants
                .STATUS_PUBLISHED_REVIEW) || trip.getVisibility() != APPConstants.ALLOW_SELF_REGISTRATION) {
              BaseView view = new BaseView();
              view.message = "System Error - invalid status.";
              return ok(views.html.common.message.render(view));
            }
          }

          AccountTripLink tripLink = AccountTripLink.findByUid(Long.parseLong(formInfo.getInPassengerId()), trip.getTripid());
          if (tripLink != null) {
            tripLink.delete();
            Account traveler = Account.find.byId(tripLink.getPk().getUid());

            //TODO: Here we don't know who the user is... so let's call it a system.
            MessengerUpdateHelper.build(trip, Account.AID_PUBLISHER).
                modifyUserTripAccessState(tripLink.getLegacyId(), false);

            //check to make sure tour is in umapped mobile
            //delete from the shared in umapped mobile

            flash(SessionConstants.SESSION_PARAM_MSG, formInfo.getInEmail() + " removed from this trip.");
            flash(SessionConstants.SESSION_PARAM_TRIPID, trip.tripid);

            if (group != null) {
              flash(SessionConstants.SESSION_PARAM_GROUPID, group.groupid);
            }
            return this.getPassengers();
          }
        }
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }

    BaseView view = new BaseView();
    view.message = "System Error - cannot add the email.";
    return ok(views.html.common.message.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result createFileAttachment() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    Form<FileUploadForm> form = formFactory.form(FileUploadForm.class).bindFromRequest();
    if(form.hasErrors()){
      return BaseJsonResponse.formError(form);
    }

    FileUploadForm destFileInfo = form.get();
    if (destFileInfo.getInTripId() == null || destFileInfo.getInFileName() == null) {
      return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_MISSING_DATA);
    }

    if (destFileInfo.getInFileName() == null || destFileInfo.getInFileName().length() == 0) {
      ObjectNode result = Json.newObject();
      result.put("msg", "Error - please resubmit1");
      return ok(result);
    }

    Trip trip = Trip.find.byId(destFileInfo.getInTripId());
    if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND)) {
      ObjectNode result = Json.newObject();
      result.put("msg", "Error - you have no permission to add files");
      return ok(result);
    }

    int count = TripAttachment.activeCountByTripId(trip.tripid);
    if (count > 60) {
      BaseView baseView = new BaseView();
      baseView.message = "Error - you have exceeded the maximum number of files.";
      return ok(views.html.common.message.render(baseView));
    }

    UserProfile userProfile = UserProfile.findByPK(sessionMgr.getUserId());

    try {

      Account a;
      if(sessionMgr.getAccount().isPresent()) {
        a = sessionMgr.getAccount().get();
      } else {
        a = Account.find.byId(sessionMgr.getAccountId());
      }

      TripAttachment tripAttachment = TripAttachment.buildTripAttachment(userProfile,
                                                                         trip,
                                                                         destFileInfo.getInFileName(),
                                                                         destFileInfo.getInFileName(),
                                                                         "",
                                                                         destFileInfo.getInDestFileCaption());
      String nextUrl = "/tours/newTour/uploadSuccess?inTripId=" + trip.tripid + "&inFileId=" + tripAttachment.getPk();
      tripAttachment.save();

      ObjectNode result = Json.newObject();

      FileInfo file = FileController.uploadHelper(tripAttachment, destFileInfo.getInFileName(), a, result, nextUrl);

      if (file != null) {
        tripAttachment.setName(destFileInfo.getInDestFileName());
        tripAttachment.setFileInfo(file);
        tripAttachment.setFileurl(file.getUrl());
        tripAttachment.setFilename(file.getFilename());
        tripAttachment.setOrigfilename(file.getFilename());
        tripAttachment.setStatus(APPConstants.STATUS_PENDING);
        tripAttachment.setAttachtype(APPConstants.TRIP_ATTACH_FILE);
        tripAttachment.setDescription(destFileInfo.getInDestFileCaption());
        tripAttachment.update();
        return ok(result);
      }


    }
    catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    return BaseJsonResponse.codeResponse(ReturnCode.STORAGE_S3_UPLOAD, "3rd party service provider error.");
  }

  @With({Authenticated.class, Authorized.class})
  public Result editFileAttachment(String tripId, String fileId) {
    SessionMgr sessionMgr = new SessionMgr(session());
    //bind html form to form bean
    Form<FileUploadForm> form = formFactory.form(FileUploadForm.class).bindFromRequest();
    if(form.hasErrors()){
      return BaseJsonResponse.formError(form);
    }

    FileUploadForm editFileInfo = form.get();

    if (editFileInfo.getInFileName() == null || editFileInfo.getInFileName().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - Missing File name";
      return ok(views.html.common.message.render(baseView));
    }

    Trip trip = Trip.find.byId(editFileInfo.getInTripId());
    if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "Error - you have no permission to add files";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();

      TripAttachment tripAttachment = TripAttachment.find.byId(editFileInfo.getInFileId());
      if (tripAttachment != null) {
        tripAttachment.setName(editFileInfo.getInFileName());
        tripAttachment.setDescription(editFileInfo.getInFileCaption());
        tripAttachment.save();
      }


      Ebean.commitTransaction();
      flash(SessionConstants.SESSION_PARAM_MSG, "File edited successfully.");
      return redirect(routes.TripController.guides(tripId, new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.ATTACHMENTS)));


    }
    catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }


  }

  @With({Authenticated.class, Authorized.class})
  public Result uploadSuccess() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    Form<FileUploadForm> form = formFactory.form(FileUploadForm.class).bindFromRequest();
    if (form.hasErrors()) {
      Log.err("TourController:uploadSuccess(): Form errors: ", form.errorsAsJson().toString());
      return UserMessage.formErrors(form);
    }

    FileUploadForm fileInfo = form.get();
    if (fileInfo.getInTripId() == null || fileInfo.getInFileId() == null) {
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    if (fileInfo.getInFileId() == null || fileInfo.getInFileId().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    Trip tripModel = Trip.find.byId(fileInfo.getInTripId());
    if (SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "System Error - You are not allowed to upload files";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      TripAttachment attach = TripAttachment.find.byId(fileInfo.getInFileId());
      if (attach == null || !attach.tripid.equals(fileInfo.getInTripId())) {
        BaseView baseView = new BaseView();
        baseView.message = "System Error - please resubmit";
        return ok(views.html.common.message.render(baseView));
      }
      Ebean.beginTransaction();

      attach.setStatus(APPConstants.STATUS_ACTIVE);
      attach.setLastupdatedtimestamp(System.currentTimeMillis());
      attach.setModifiedby(sessionMgr.getUserId());
      attach.save();
      Ebean.commitTransaction();

      //Create audit log
      audit:
      {
        TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_DOC_ATTACHMENT,
                                             AuditActionType.ADD,
                                             AuditActorType.WEB_USER)
                                .withTrip(tripModel)
                                .withCmpyid(tripModel.cmpyid)
                                .withUserid(sessionMgr.getUserId());

        ((AuditTripDocAttachment) ta.getDetails()).withAttachementId(attach.getPk())
                                              .withFileName(attach.getOrigfilename())
                                              .withSysFileName(attach.getFilename());
        ta.save();
      }

      flash(SessionConstants.SESSION_PARAM_MSG, APPConstants.MSG_FILE_UPLOAD_SUCCESS);
      return  redirect(routes.TripController.guides(tripModel.tripid,
                                   new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.ATTACHMENTS)));
    }
    catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

  }

  @With({Authenticated.class, Authorized.class})
  public Result reorderFileAttachments() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final DynamicForm form = formFactory.form().bindFromRequest();
    String tripId = form.get("tripId");
    String inAttachmentIds = form.get("inAttachmentIds");
    if (inAttachmentIds != null && tripId != null && inAttachmentIds.trim().length() > 0) {
      String[] attachmentIds = inAttachmentIds.split(",");
      Trip tripModel = Trip.find.byId(tripId);
      if (tripModel != null) {
        SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
        if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND)) {
          BaseView baseView = new BaseView();
          baseView.message = "System Error - You are not allowed to delete files";
          return ok(views.html.common.message.render(baseView));
        }
        //save all the attachments incrementing the modified timestamp by 1 tick for ordering purposes.
        long modifiedTimestamp = System.currentTimeMillis();
        List<TripAttachment>  tripAttachments = TripAttachment.findByTripId(tripId);
        Map <String, TripAttachment> tripAttachmentMap = new HashMap<>();

        if (tripAttachments != null) {
          for (TripAttachment ta:tripAttachments) {
            tripAttachmentMap.put(ta.pk, ta);
          }
        }

        try {
          Ebean.beginTransaction();
          for (String attachmentId : attachmentIds) {
            TripAttachment ta = tripAttachmentMap.get(attachmentId);
            if (ta != null) {
              ta.setModifiedby(sessionMgr.getUserId());
              ta.setLastupdatedtimestamp(modifiedTimestamp--);
              ta.save();
            }
          }
          Ebean.commitTransaction();
          return redirect(routes.TripController.guides(tripId, new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.ATTACHMENTS)));
        } catch (Exception e) {
          Ebean.rollbackTransaction();
          Log.log(LogLevel.ERROR, "reorder Attachments: " + tripId, e);
        }

      }
    }

      BaseView baseView = new BaseView();
    baseView.message = "System Error - You are not allowed to re-order attachment";
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteFileAttachment() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    Form<FileUploadForm> form = formFactory.form(FileUploadForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    FileUploadForm fileInfo = form.get();
    if (fileInfo.getInTripId() == null || fileInfo.getInFileId() == null) {
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    else {
      if (fileInfo.getInFileId() == null || fileInfo.getInFileId().length() == 0) {
        BaseView baseView = new BaseView();
        baseView.message = "System Error - please resubmit";
        return ok(views.html.common.message.render(baseView));
      }
      Trip tripModel = Trip.find.byId(fileInfo.getInTripId());
      SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
      if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND)) {
        BaseView baseView = new BaseView();
        baseView.message = "System Error - You are not allowed to delete files";
        return ok(views.html.common.message.render(baseView));
      }



      try {
        TripAttachment attach = TripAttachment.find.byId(fileInfo.getInFileId());
        if (attach == null || !attach.tripid.equals(fileInfo.getInTripId())) {
          BaseView baseView = new BaseView();
          baseView.message = "System Error - please resubmit";
          return ok(views.html.common.message.render(baseView));
        }
        //Can only delete files created by the user himself
        if (accessLevel == SecurityMgr.AccessLevel.APPEND && !attach.getCreatedby().equals(sessionMgr.getUserId())) {
          BaseView baseView = new BaseView();
          baseView.message = "System Error - You are not allowed to delete files you did not upload yourself";
          return ok(views.html.common.message.render(baseView));
        }

        Ebean.beginTransaction();

        attach.setStatus(APPConstants.STATUS_DELETED);
        attach.setLastupdatedtimestamp(System.currentTimeMillis());
        attach.setModifiedby(sessionMgr.getUserId());
        attach.save();
        Ebean.commitTransaction();

        //Create audit log
        audit:
        {
          TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_DOC_ATTACHMENT,
                                               AuditActionType.DELETE,
                                               AuditActorType.WEB_USER)
                                  .withTrip(tripModel)
                                  .withCmpyid(tripModel.cmpyid)
                                  .withUserid(sessionMgr.getUserId());

          ((AuditTripDocAttachment) ta.getDetails()).withAttachementId(attach.getPk())
                                                .withFileName(attach.getOrigfilename())
                                                .withSysFileName(attach.getFilename());
          ta.save();;
        }

        //New logic to handle all related files from bookingDetail.files - George
        TripDetailAttach.deleteAllFiles(fileInfo.getInFileId());


        flash(SessionConstants.SESSION_PARAM_MSG, "File deleted successfully");
        flash(SessionConstants.SESSION_PARAM_ACTIVE_TAB, DestinationSearchView.Tab.ATTACHMENTS.name());
        return  redirect(routes.TripController.guides(tripModel.tripid,
                                     new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.ATTACHMENTS)));
      }
      catch (Exception e) {
        e.printStackTrace();
        Ebean.rollbackTransaction();
        BaseView baseView = new BaseView();
        baseView.message = "System Error - please resubmit";
        return ok(views.html.common.message.render(baseView));
      }
    }
  }

  //attach trip cover photo
  @With({Authenticated.class, Authorized.class})
  public Result uploadTripCover() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Account a;
    if(sessionMgr.getAccount().isPresent()) {
      a = sessionMgr.getAccount().get();
    } else {
      a = Account.find.byId(sessionMgr.getAccountId());
    }

    //bind html form to form bean
    Form<LogoUploadForm> form = formFactory.form(LogoUploadForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return BaseJsonResponse.formError(form);
    }

    LogoUploadForm logoInfo = form.get();
    if (logoInfo.getInTripId() == null) {
      return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_MISSING_DATA, "Browser didn't submit all data");
    }

    if (logoInfo.getInCmpyPhotoName() == null || logoInfo.getInCmpyPhotoName().length() == 0) {
      return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_MISSING_DATA, "No image filename.");
    }

    Trip trip = Trip.find.byId(logoInfo.getInTripId());
    //TODO: Serguei: Confirm only those with APPEND_N_PUBLISH and above permission can upload trip covers
    if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND_N_PUBLISH)) {
      return BaseJsonResponse.codeResponse(ReturnCode.AUTH_TRIP_FAIL, "Unauthorized trip access");
    }

    ObjectNode result = Json.newObject();
    String nextUrl = routes.TripController.tripInfo(trip.tripid).url();
    FileImage fileImage = uploadHelper(trip, logoInfo.getInCmpyPhotoName(), a, result, nextUrl);

    if(fileImage != null) {
      trip.setFileImage(fileImage);
      trip.setCoverfilename(logoInfo.getInCmpyPhotoName());
      trip.setCoverurl(fileImage.getUrl());
      trip.markModified(sessionMgr.getUserId());
      trip.update();
      return ok(result);
    }
    return BaseJsonResponse.codeResponse(ReturnCode.STORAGE_S3_UPLOAD, "3rd party service provider error.");
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteTripCover() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    final DynamicForm form = formFactory.form().bindFromRequest();
    String tripId = form.get("tripId");
    if (tripId == null || tripId.trim().length() == 0) {
      ObjectNode result = Json.newObject();
      result.put("msg", "Error - please resubmit");
      return ok(result);
    }
    else {
      Trip trip = Trip.find.byId(tripId);
      if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).lt(SecurityMgr.AccessLevel.OWNER)) {
        ObjectNode result = Json.newObject();
        result.put("msg", "Error - Unauthorized access");
        return ok(result);
      }

      try {
        Ebean.beginTransaction();
        trip.setCoverfilename(null);
        trip.setCoverurl(null);
        trip.setFileImage(null);
        trip.markModified(sessionMgr.getUserId());
        trip.save();
        Ebean.commitTransaction();
        flash(SessionConstants.SESSION_PARAM_MSG, "Cover Photo deleted successfully. ");
        return redirect(routes.TripController.tripInfo(trip.getTripid()));
      }
      catch (Exception e) {
        Ebean.rollbackTransaction();
        BaseView baseView = new BaseView();
        baseView.message = "System Error - please resubmit";
        return ok(views.html.common.message.render(baseView));
      }
    }
  }
}
