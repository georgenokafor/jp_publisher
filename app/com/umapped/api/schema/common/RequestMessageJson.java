package com.umapped.api.schema.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.JsonNode;
import com.mapped.publisher.utils.Log;
import com.umapped.api.schema.local.*;
import play.libs.Json;

import java.sql.Timestamp;

/**
 * Request
 */
public class RequestMessageJson {

  /**
   * Request message header
   */
  public RequestHeaderJson header;

  /**
   * Request message body
   * All message body types have to be registered
   */
  @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY)
  @JsonSubTypes({@JsonSubTypes.Type(value = ConsortiumJson.class, name = "CONSORTIUM"),
                 @JsonSubTypes.Type(value = CompanyJson.class, name = "COMPANY"),
                 @JsonSubTypes.Type(value = PoiApiJson.class, name = "POI"),
                 @JsonSubTypes.Type(value = PoiFileJson.class, name = "POIFILE"),
                 @JsonSubTypes.Type(value = BookingFileJson.class, name = "BKFILE"),
                 @JsonSubTypes.Type(value = WebSearchJson.class, name = "WEBSEARCH"),
                 @JsonSubTypes.Type(value = IcePhotosJson.class, name = "ICEPHOTOS"),
                 @JsonSubTypes.Type(value = AuthJson.class, name = "AUTHENTICATE"),
                 @JsonSubTypes.Type(value = T42LoginJson.class, name = "T42LOGIN"),
                 @JsonSubTypes.Type(value = TravelboundPhotosJson.class, name = "TRAVELBOUNDPHOTOS"),
                })
  public IBodyJson body;
  /**
   * Timestamp when the message was received by the system.
   * Not part of the message
   */
  @JsonIgnore
  private Timestamp timestamp;

  public RequestMessageJson() {
    timestamp = new Timestamp(System.currentTimeMillis());
  }


  public Timestamp getTimestamp() {
    return timestamp;
  }

  public static RequestMessageJson fromJson(JsonNode json) {
    RequestMessageJson result = null;
    try {
      result = Json.fromJson(json, RequestMessageJson.class);
    }
    catch (Exception e) {
      Log.err("Failed to parse incoming JSON request:" + json.toString());
      e.printStackTrace();
    }
    return result;
  }
}
