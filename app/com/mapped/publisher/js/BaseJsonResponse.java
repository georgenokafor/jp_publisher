package com.mapped.publisher.js;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.api.schema.types.SeverityLevel;
import play.data.Form;
import play.libs.Json;
import play.libs.Jsonp;
import play.mvc.Result;

import java.io.Serializable;

import static play.mvc.Results.ok;

/**
 * Base class for all JSON responses based on user requests.
 * <p>
 * Created by surge on 2014-09-23.
 */
public class BaseJsonResponse
    implements Serializable {
  public String msg;
  public int    returnCode;
  public int    severityLevel;

  @JsonIgnore
  private boolean prettyPrint;
  @JsonIgnore
  private String  callback;

  /**
   * By default build success response
   */
  public BaseJsonResponse() {
    prettyPrint = false;
    callback = null;
    returnCode = ReturnCode.SUCCESS.ordinal();
    severityLevel = ReturnCode.SUCCESS.level().ordinal();
    msg = ReturnCode.SUCCESS.msg();
  }

  /**
   * Useful for building simple error responses
   */
  public BaseJsonResponse(ReturnCode rc) {
    withCode(rc);
  }

  public BaseJsonResponse withCode(ReturnCode rc) {
    this.returnCode = rc.ordinal();
    this.msg = rc.msg();
    this.severityLevel = rc.level().ordinal();
    return this;
  }

  public static Result formError(Form form) {
    return codeResponse(ReturnCode.HTTP_REQ_MISSING_DATA, "Please resubmit.");
  }

  public static Result codeResponse(ReturnCode rc, String... customMessages) {
    StringBuilder sb = new StringBuilder(rc.msg());

    if (customMessages.length > 0) {
      for (String cm : customMessages) {
        sb.append(" - ");
        sb.append(cm);
      }
    }

    BaseJsonResponse jr = new BaseJsonResponse(rc);
    jr.msg = sb.toString();

    return ok(jr.toJson());
  }

  public JsonNode toJson() {
    return Json.toJson(this);
  }

  /**
   * Not only sets callback but also will return JsonP result structure
   *
   * @param callback
   */
  public void setCallback(String callback) {
    this.callback = callback;
  }

  public void enablePrettyPrint() {
    prettyPrint = true;
    /*
    ObjectMapper om = Json.mapper();
    om.enable(SerializationFeature.INDENT_OUTPUT); //Pretty-print for debugging
    */
  }

  public void disablePrettyPrint() {
    prettyPrint = false;
    /*
    ObjectMapper om = Json.mapper();
    om.disable(SerializationFeature.INDENT_OUTPUT);
    */
  }

  public void enableTypeEmbed() {
    ObjectMapper om = Json.mapper();
    om.enableDefaultTypingAsProperty(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS, "@type");
    om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS, JsonTypeInfo.As.PROPERTY);

  }

  public void disableTypeEmbed() {
    ObjectMapper om = Json.mapper();
    om.disableDefaultTyping();
  }

  public BaseJsonResponse withSeverityLevel(SeverityLevel level) {
    this.severityLevel = level.ordinal();
    return this;
  }

  public BaseJsonResponse withMsg(String msg) {
    this.msg = msg;
    return this;
  }

  public Result toJsonResult() {
    if (callback != null) {
      return ok(Jsonp.jsonp(callback, Json.toJson(this)));
    }

    if (prettyPrint) {
      return ok(Json.prettyPrint(Json.toJson((this)))).as("application/json");
    }

    return ok(Json.toJson(this));
  }

}
