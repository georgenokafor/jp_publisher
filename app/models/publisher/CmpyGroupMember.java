package models.publisher;

import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-18
 * Time: 6:39 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="cmpy_group_members")
public class CmpyGroupMember extends Model {
    public String comment;

    @Id
    public String pk;

    @Constraints.Required
    public String usercmpylinkpk;

    public String groupid;
    public int status;

    public Long createdtimestamp;
    public Long lastupdatedtimestamp;

    public String createdby;
    public String modifiedby;

    @Version
    public int version;

    public static Model.Finder<String,CmpyGroupMember> find = new Finder(CmpyGroupMember.class);

    public static List<CmpyGroupMember> findByGroupIdUserLink(String groupId, String userCmpyLink) {
         return find.where().eq("status", 0).eq("groupId", groupId).eq("usercmpylinkpk",userCmpyLink).findList();
    }

    public static List<CmpyGroupMember> findByGroupId(String groupId) {
        return find.where().eq("status", 0).eq("groupId", groupId).findList();
    }

    public static List<CmpyGroupMember> findByLinkId(String linkId) {
        return find.where().eq("status", 0).eq("usercmpylinkpk", linkId).findList();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getUsercmpylinkpk() {
        return usercmpylinkpk;
    }

    public void setUsercmpylinkpk(String usercmpylinkpk) {
        this.usercmpylinkpk = usercmpylinkpk;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getCreatedtimestamp() {
        return createdtimestamp;
    }

    public void setCreatedtimestamp(Long createdtimestamp) {
        this.createdtimestamp = createdtimestamp;
    }

    public Long getLastupdatedtimestamp() {
        return lastupdatedtimestamp;
    }

    public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
        this.lastupdatedtimestamp = lastupdatedtimestamp;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
