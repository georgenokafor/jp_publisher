package com.umapped.persistence.accountprop;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Created by twong on 16-03-11.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountPrefs implements Serializable{
  public enum DIGEST_FREQUENCY {DAILY, NOW, NONE};

  public DIGEST_FREQUENCY digest; //allow for receiving email digest for missed messages
  public boolean eNtfy = false; //allow for receiving email notifications for messages
  public boolean sNtfy = false; //allow for receiving SMS notifications for messages

  public boolean disableTalkback = false;

  public static AccountPrefs build() {
    AccountPrefs prefs = new AccountPrefs();
    return prefs;
  }

  public DIGEST_FREQUENCY getDigest() {
    if (digest == null) {
      return DIGEST_FREQUENCY.NONE;
    }
    return digest;
  }

  public AccountPrefs setDigest(DIGEST_FREQUENCY digest) {
    this.digest = digest;
    return this;
  }

  public boolean isDisableTalkback() {
    return disableTalkback;
  }

  public void setDisableTalkback(boolean disableTalkback) {
    this.disableTalkback = disableTalkback;
  }

  public boolean hasPreferences() {
    if ((digest != null && !digest.equals(DIGEST_FREQUENCY.NONE)) || eNtfy || sNtfy || disableTalkback) {
      return true;
    }

    return false;
  }

  public void prePersist() {

  }

}
