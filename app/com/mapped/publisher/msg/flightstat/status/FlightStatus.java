
package com.mapped.publisher.msg.flightstat.status;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"flightStatusUpdates","codeshares", "irregularOperations", "delays", "flightEquipment","divertedAirportFsCode"})

public class FlightStatus{
   	private String arrivalAirportFsCode;
   	private String divertedAirportFsCode;
   	private ArrivalDate arrivalDate;
   	private String carrierFsCode;
   	private Codeshares codeshares;
   	private String departureAirportFsCode;
   	private DepartureDate departureDate;
   	private FlightDurations flightDurations;
   	private FlightEquipment flightEquipment;
    private AirportResources airportResources;
   	private String flightId;
   	private String flightNumber;
    private Delays delays;
   	private FlightStatusUpdates flightStatusUpdates;
   	private OperationalTimes operationalTimes;
   	private Schedule schedule;
   	private String status;
    private IrregularOperations irregularOperations;

 	public String getArrivalAirportFsCode(){
		return this.arrivalAirportFsCode;
	}
	public void setArrivalAirportFsCode(String arrivalAirportFsCode){
		this.arrivalAirportFsCode = arrivalAirportFsCode;
	}
 	public ArrivalDate getArrivalDate(){
		return this.arrivalDate;
	}
	public void setArrivalDate(ArrivalDate arrivalDate){
		this.arrivalDate = arrivalDate;
	}
 	public String getCarrierFsCode(){
		return this.carrierFsCode;
	}
	public void setCarrierFsCode(String carrierFsCode){
		this.carrierFsCode = carrierFsCode;
	}
 	public Codeshares getCodeshares(){
		return this.codeshares;
	}
	public void setCodeshares(Codeshares codeshares){
		this.codeshares = codeshares;
	}
 	public String getDepartureAirportFsCode(){
		return this.departureAirportFsCode;
	}
	public void setDepartureAirportFsCode(String departureAirportFsCode){
		this.departureAirportFsCode = departureAirportFsCode;
	}
 	public DepartureDate getDepartureDate(){
		return this.departureDate;
	}
	public void setDepartureDate(DepartureDate departureDate){
		this.departureDate = departureDate;
	}
 	public FlightDurations getFlightDurations(){
		return this.flightDurations;
	}
	public void setFlightDurations(FlightDurations flightDurations){
		this.flightDurations = flightDurations;
	}
 	public FlightEquipment getFlightEquipment(){
		return this.flightEquipment;
	}
	public void setFlightEquipment(FlightEquipment flightEquipment){
		this.flightEquipment = flightEquipment;
	}
 	public String getFlightId(){
		return this.flightId;
	}
	public void setFlightId(String flightId){
		this.flightId = flightId;
	}
 	public String getFlightNumber(){
		return this.flightNumber;
	}
	public void setFlightNumber(String flightNumber){
		this.flightNumber = flightNumber;
	}
 	public FlightStatusUpdates getFlightStatusUpdates(){
		return this.flightStatusUpdates;
	}
	public void setFlightStatusUpdates(FlightStatusUpdates flightStatusUpdates){
		this.flightStatusUpdates = flightStatusUpdates;
	}
 	public OperationalTimes getOperationalTimes(){
		return this.operationalTimes;
	}
	public void setOperationalTimes(OperationalTimes operationalTimes){
		this.operationalTimes = operationalTimes;
	}
 	public Schedule getSchedule(){
		return this.schedule;
	}
	public void setSchedule(Schedule schedule){
		this.schedule = schedule;
	}
 	public String getStatus(){
		return this.status;
	}
	public void setStatus(String status){
		this.status = status;
	}

  public AirportResources getAirportResources() {
    return airportResources;
  }

  public void setAirportResources(AirportResources airportResources) {
    this.airportResources = airportResources;
  }

  public IrregularOperations getIrregularOperations() {
    return irregularOperations;
  }

  public void setIrregularOperations(IrregularOperations irregularOperations) {
    this.irregularOperations = irregularOperations;
  }

	public String getDivertedAirportFsCode() {
		return divertedAirportFsCode;
	}

	public void setDivertedAirportFsCode(String divertedAirportFsCode) {
		this.divertedAirportFsCode = divertedAirportFsCode;
	}
}
