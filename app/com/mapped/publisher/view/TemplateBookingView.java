package com.mapped.publisher.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-07-25
 * Time: 1:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class TemplateBookingView
    extends BaseView
    implements Serializable {
  public String templateId;
  public String templateName;
  public String cmpyId;
  public String cmpyName;
  public boolean canEdit;
  public String tripId;
  public String comments;
  public String coverUrl;

  public TripBookingDetailView bookingDetail;
  public TabType activeTab = TabType.FLIGHTS;

  public List<TripBookingDetailView> hotels;
  public List<TripBookingDetailView> flights;
  public List<TripBookingDetailView> transfers;
  public List<TripBookingDetailView> activities;
  public List<TripBookingDetailView> cruises;
  public List<DestinationView> guides;

  public TemplateBookingView() {
    flights = new ArrayList<TripBookingDetailView>();
    hotels = new ArrayList<TripBookingDetailView>();
    cruises = new ArrayList<TripBookingDetailView>();
    activities = new ArrayList<TripBookingDetailView>();
    transfers = new ArrayList<TripBookingDetailView>();
    guides = new ArrayList<DestinationView>();
  }
}
