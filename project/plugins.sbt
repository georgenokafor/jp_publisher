resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"

// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.5.10")

// EBean Plugin
addSbtPlugin("com.typesafe.sbt" % "sbt-play-ebean" % "3.1.0")
//lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

// Play enhancer - https://www.playframework.com/documentation/2.5.x/PlayEnhancer
// This plugin provides byte code enhancement to Play 2.4+ projects, generating
// getters and setters for any Java sources, and rewriting accessors for any
// classes that depend on them.
addSbtPlugin("com.typesafe.sbt" % "sbt-play-enhancer" % "1.1.0")

// web plugins
//A CoffeeScript plugin for SBT
//addSbtPlugin("com.typesafe.sbt" % "sbt-coffeescript" % "1.0.0")

//Allows less to be used from within sbt.
addSbtPlugin("com.typesafe.sbt" % "sbt-less" % "1.1.1")

//Allows jslint to be used from within sbt. Builds on com.typesafe:webdriver
//in order to execute jslint.js along with the scripts to verify
//addSbtPlugin("com.typesafe.sbt" % "sbt-jshint" % "1.0.3")

//RequireJs optimizer plugin for sbt-web
//addSbtPlugin("com.typesafe.sbt" % "sbt-rjs" % "1.0.7")

//sbt-web plugin for adding checksum files for web assets.
//Checksums are useful for asset fingerprinting and etag values.
addSbtPlugin("com.typesafe.sbt" % "sbt-digest" % "1.1.1")

//SBT plugin for running mocha JavaScript unit tests on node
//addSbtPlugin("com.typesafe.sbt" % "sbt-mocha" % "1.1.0")

//addSbtPlugin("com.jamesward" % "play-auto-refresh" % "0.0.12")
