package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-15
 * Time: 8:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class PassengerForm {
    private  String inTripId;
    private  String inEmail;
    private  String inName;
    private  String inPassengerId;
    private String inGroupId;
    private  String inOriginPage;


    public String getInTripId() {
        return inTripId;
    }

    public void setInTripId(String inTripId) {
        this.inTripId = inTripId;
    }

    public String getInEmail() {
        return inEmail;
    }

    public void setInEmail(String inEmail) {
        this.inEmail = inEmail;
    }

    public String getInName() {
        return inName;
    }

    public void setInName(String inName) {
        this.inName = inName;
    }

    public String getInPassengerId() {
        return inPassengerId;
    }

    public void setInPassengerId(String inPassengerId) {
        this.inPassengerId = inPassengerId;
    }

    public String getInGroupId() {
        return inGroupId;
    }

    public void setInGroupId(String inGroupId) {
        this.inGroupId = inGroupId;
    }

    public String getInOriginPage() {
        return inOriginPage;
    }

    public void setInOriginPage(String inOriginPage) {
        this.inOriginPage = inOriginPage;
    }
}
