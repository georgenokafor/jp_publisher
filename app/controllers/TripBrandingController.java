package controllers;

import com.avaje.ebean.Ebean;
import com.google.inject.Inject;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.common.SessionConstants;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.form.trip.FormToggleBranding;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.BusinessCardView;
import com.umapped.persistence.enums.MobileLink;
import models.publisher.*;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.util.List;

/**
 * Created by twong on 2014-07-15.
 */
public class TripBrandingController
    extends Controller {

  @Inject
  FormFactory formFactory;

  @With({Authenticated.class, Authorized.class})
  public Result toggleBranding() {
    FormToggleBranding form = (formFactory.form(FormToggleBranding.class)).bindFromRequest().get();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());


    Trip tripModel = Trip.findByPK(sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID));
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if ((accessLevel.lt(SecurityMgr.AccessLevel.APPEND_N_PUBLISH) && !SecurityMgr.canAccessCmpy(tripModel.cmpyid,
            sessionMgr)) ||
            accessLevel
                    .le(SecurityMgr.AccessLevel.READ)) {
      //Clearly someone exploits the system here as all previous search forms, etc check OWNER access level
      flash(SessionConstants.SESSION_PARAM_MSG,
              "Permission Error: You are not permitted to revoke access rights for this user.");
      return redirect("/tours/newTour/publish");
    }

    try {
      TripBranding tb = TripBranding.findActiveByPK(tripModel.tripid, form.inCmpyId);
      if (tb == null) {
        flash(SessionConstants.SESSION_PARAM_MSG, "Permission Error: You cannot perform this action.");
        return redirect("/tours/newTour/publish");
      }

      if (!form.inEnabled) {
        //make sure we have at least 1 enabled
        List<TripBranding> brands = TripBranding.findEnabledByTripId(tripModel.tripid);
        if (brands == null || brands.size() < 2) {
          flash(SessionConstants.SESSION_PARAM_MSG, "Error - At least one company must be enabled.");
          return redirect("/tours/newTour/publish");
        }
      }

      tb.setIsenabled(form.inEnabled);
      tb.setModifiedby(sessionMgr.getUserId());
      tb.setLastupdatedtimestamp(System.currentTimeMillis());
      tb.save();
      Company cmpy = Company.find.byId(form.inCmpyId);
      BaseView baseView = new BaseView();
      if (form.inEnabled) {
        baseView.message = "Co-Branding: " + cmpy.name + " has been enabled";
      } else {
        baseView.message = "Co-Branding: " + cmpy.name + " has been disabled";
      }
      return ok(views.html.common.message.render(baseView));

    } catch (Exception e) {
      e.printStackTrace();
    }
    return redirect("/tours/newTour/publish");
  }

  @With({Authenticated.class, Authorized.class})
  public Result toggleMainBrand() {
    FormToggleBranding form = (formFactory.form(FormToggleBranding.class)).bindFromRequest().get();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());


    Trip tripModel = Trip.findByPK(sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID));
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if ((accessLevel.lt(SecurityMgr.AccessLevel.APPEND_N_PUBLISH) && !SecurityMgr.canAccessCmpy(tripModel.cmpyid,
            sessionMgr)) ||
            accessLevel
                    .le(SecurityMgr.AccessLevel.READ)) {
      //Clearly someone exploits the system here as all previous search forms, etc check OWNER access level
      BaseView baseView = new BaseView();
      baseView.message = "Permission Error: You are not permitted to revoke access rights for this user.";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      TripBranding tb = TripBranding.findActiveByPK(tripModel.tripid, form.inCmpyId);
      if (tb == null) {
        flash(SessionConstants.SESSION_PARAM_MSG, "Permission Error: You cannot perform this action.");
        return redirect("/tours/newTour/publish");
      }
      Company cmpy = Company.find.byId(form.inCmpyId);

      if (!tb.isenabled) {
        flash(SessionConstants.SESSION_PARAM_MSG,
                "Error: This company is currently no enabled and cannot be set as the primary contact.");
        return redirect("/tours/newTour/publish");
      }

      Ebean.beginTransaction();
      List<TripBranding> mainBrands = TripBranding.findMainByTripId(form.inTripId);
      for (TripBranding mainBrand : mainBrands) {
        mainBrand.setIsmainbrand(false);
        mainBrand.setModifiedby(sessionMgr.getUserId());
        mainBrand.setLastupdatedtimestamp(System.currentTimeMillis());
        mainBrand.save();
      }

      tb.setIsmainbrand(true);
      tb.setModifiedby(sessionMgr.getUserId());
      tb.setLastupdatedtimestamp(System.currentTimeMillis());
      tb.save();

      Ebean.commitTransaction();

      BaseView baseView = new BaseView();
      baseView.message = "Co-Branding: " + cmpy.name + " is now the primary contact";
      return ok(views.html.common.message.render(baseView));

    } catch (Exception e) {

      Ebean.rollbackTransaction();
      flash(SessionConstants.SESSION_PARAM_MSG, "An Error has occurred - please try again.");

    }
    return redirect("/tours/newTour/publish");
  }

  public Result appstoreLink(String platform, String tripId) {
    MobileLink mobileLink = MobileLink.UMAPPED;
    if (platform != null && (platform.toUpperCase().equals("IOS") || platform.toUpperCase().equals("ANDROID")) && tripId != null) {
      Trip trip = Trip.findByPK(tripId);

      final DynamicForm form = formFactory.form().bindFromRequest();
      String groupId = form.get("inGroupId");
      boolean found = false;
      if (trip != null && trip.getStatus() != -1) {
        try {
          BusinessCardView bcv = WebItineraryController.getTripBusinessCard(trip, groupId);
          if (bcv != null && bcv.companyId != null && !bcv.companyId.isEmpty()) {

            Company parentCompany = null;


            if (bcv.parentCompanyId != null && bcv.parentCompanyId.trim().length() > 0) {
              parentCompany = Company.find.byId(bcv.parentCompanyId);
            }

            String cmpyName = bcv.companyName.toLowerCase();
            String parentCmpyName = null;
            if (parentCompany != null) {
              parentCmpyName = parentCompany.getName().toLowerCase();
            }

            for (MobileLink m : MobileLink.getCompanyLinks()) {
              if ((m.isExactMatch() && (cmpyName.equals(m.getName()) || (parentCmpyName != null && parentCmpyName.equals(m.getName())))) ||
                      (!m.isExactMatch() && (cmpyName.contains(m.getName()) || (parentCmpyName != null && parentCmpyName.contains(m.getName()))))) {
                found = true;
                mobileLink = m;
                break;
              }
            }

            List<String> consortiums = bcv.consortiumNames;
            if (!found && consortiums != null && consortiums.size() > 0) {
              for (MobileLink m : MobileLink.getConsortiumLinks()) {
                for (String consortium : consortiums) {
                  if (consortium != null &&
                          ((m.isExactMatch() && consortium.toLowerCase().equals(m.getName()))
                                  || (!m.isExactMatch() && consortium.toLowerCase().contains(m.getName())))) {
                    mobileLink = m;
                    break;
                  }
                }
              }
            }

            if (platform.toUpperCase().contains("IOS")) {
              Log.debug("TripBradingController:app store link for trip: " + tripId + " " + mobileLink.getIosLink());
              return redirect(mobileLink.getIosLink());
            } else if (platform.toUpperCase().contains("ANDROID")) {
              Log.debug("TripBradingController:play store link for trip: " + tripId + " " + mobileLink.getAndroidLink());
              return redirect(mobileLink.getAndroidLink());
            }
          }
        } catch (Exception e) {

        }
      }
    }
    Log.debug("TripBradingController:default store link for trip: " + tripId + " " + mobileLink.getIosLink() );

    return redirect(mobileLink.getIosLink());
  }
}