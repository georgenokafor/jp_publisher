package com.umapped.persistence.reservation.activity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.umapped.persistence.reservation.UmOrganization;
import com.umapped.persistence.reservation.UmThing;

/**
 * Created by ryan on 15-08-17.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmActivity
  extends UmThing
  implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public String activityType;

  private UmOrganization organizedBy;

  public String guideId;

  // Point to the destination
  public String umDocumentId;

  public UmOrganization getOrganizedBy() {
    if (organizedBy == null) {
      organizedBy = new UmOrganization();
    }
    return organizedBy;
  }

  public void setOrganizedBy(UmOrganization organizedBy) {
    this.organizedBy = organizedBy;
  }
  
  
}
