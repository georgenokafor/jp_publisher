package controllers;

import actors.ActorsHelper;
import actors.IcePortalActor;
import actors.SupervisorActor;
import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mapped.publisher.actions.AdminAccess;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.*;
import com.mapped.publisher.common.Credentials;
import com.mapped.publisher.form.DataTablesForm;
import com.mapped.publisher.parse.iceportal.*;
import com.mapped.publisher.persistence.*;
import com.mapped.publisher.persistence.cache.PoiFeedStateCO;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.umapped.api.schema.common.RequestMessageJson;
import com.umapped.api.schema.common.ResponseMessageJson;
import com.umapped.api.schema.local.*;
import com.umapped.api.schema.types.Address;
import com.umapped.api.schema.types.AddressType;
import com.umapped.api.schema.types.Coordinates;
import com.umapped.api.schema.types.Operation;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.external.travelbound.TravelboundItemMapper;
import models.publisher.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.lang3.tuple.Pair;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import views.html.poi.feeds;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static controllers.ImageController.uploadHelper;

/**
 * Created by surge on 2014-10-16.
 */
public class PoiController
    extends Controller {

  private final static boolean DEBUG = false;

  @Inject
  FormFactory formFactory;

  @Inject static TravelboundItemMapper travelboundItemMapper;

  interface PoiPhotosBuilder {
    void buildPhotos(PoiSrcFeed psf, PoiPhotosJson poiPhotosJson, Long importId);
  }

  /**
   * Perform operation on the request (Operations Router)
   */
  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result poiExecute() {
    StopWatch sw = new StopWatch();
    sw.start();
    RequestMessageJson poiRequest = RequestMessageJson.fromJson(request().body().asJson());
    Log.debug("POI REQ:" + request().body().asJson().toString());
    ResponseMessageJson poiResponse = null;

    PoiApiJson poiApiJson = (PoiApiJson) poiRequest.body;
    switch(poiApiJson.operation) {
      case ADD:
      case COPY:
        poiResponse = poiAddResponse(poiRequest);
        break;
      case UPDATE:
        poiResponse = poiUpdateResponse(poiRequest);
        break;
      case DELETE:
        poiResponse = poiDeleteResponse(poiRequest);
        break;
      case LIST:
      case SEARCH:
        poiResponse = poiSearchResponse(poiRequest);
        break;
    }

    sw.stop();
    if (poiResponse.header.getRespCode() != 0) {
      Log.debug("POI: POIERROR: Resp in " + sw.getTime() + "ms :" + poiResponse.toJson());
    } else {
      Log.debug("POI: Resp in " + sw.getTime() + "ms");
    }
    return ok(poiResponse.toJson());
  }

  private static Pattern validSearchPattern = Pattern.compile("\\w+", Pattern.UNICODE_CHARACTER_CLASS);

  /**
   * Admin company search
   * @param req
   * @return
   */
  private static ResponseMessageJson poiSearchResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = new SessionMgr(session());
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);

    PoiApiJson poiResponseJson = new PoiApiJson();
    poiResponseJson.operation = Operation.LIST;

    poiResponseJson.pois = new ArrayList<>();
    responseMessageJson.body = poiResponseJson;

    PoiApiJson poiRequestJson = (PoiApiJson) req.body;
    poiResponseJson.searchTerm = poiRequestJson.searchTerm;
    poiResponseJson.setSearchId(poiRequestJson.getSearchId());
    poiResponseJson.cmpyId = poiRequestJson.cmpyId;

    if (poiRequestJson.cmpyId == null || (poiRequestJson.searchTerm == null && poiRequestJson.getSearchId() == null)) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    if (poiRequestJson.searchTerm != null) {
      Matcher validM = validSearchPattern.matcher(poiRequestJson.searchTerm);
      if (!validM.find()) {
        responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA).withMsg("No valid search term");
        return responseMessageJson;
      }
    }

    //If user is requesting search for a company he/she does not have access to kick them out
    if (!SecurityMgr.isUmappedAdmin(sessionMgr) &&
        !sessionMgr.getCredentials().isCmpyMember(poiRequestJson.cmpyId)) {
      responseMessageJson.header.withCode(ReturnCode.AUTH_VENDOR_FAIL);
      return responseMessageJson;
    }

    /**
     * Based design discussions previous functionality to perform search on all accessible pois
     * is deprecated. User is able to search only for one company ID at a time. If at later point
     * this feature returns it should easy to add.
     *
     * A specific company will be searched and if that record also has a public record, those
     * will be merged, public records without corresponding private will be dropped
     */
    Set<Integer> searchCmpyIds = new HashSet<>();
    if (poiRequestJson.cmpyId == null || poiRequestJson.cmpyId >= 0) {
      searchCmpyIds.add(poiRequestJson.cmpyId); //Searching for one company at a time
    }
    searchCmpyIds.add(Company.PUBLIC_COMPANY_ID);//PUBLIC

    //Preparing types of pois to search
    List<Integer> searchTypes;
    if (poiRequestJson.selectedTypes == null || poiRequestJson.selectedTypes.size() == 0) {
      //need to add all
      searchTypes = PoiTypeInfo.Instance().getTypes();
    } else {
      searchTypes = poiRequestJson.selectedTypes;
    }

    Map<Long, List<PoiRS>> poiSet;
    if (poiRequestJson.getSearchId() != null &&
        poiRequestJson.getSearchId().trim().length() > 0 &&
        Long.parseLong(poiRequestJson.getSearchId()) > 0) {
      poiSet = new HashMap<>();
      Long searchId = Long.parseLong(poiRequestJson.getSearchId());
      poiSet.put(searchId, PoiMgr.findById(searchId, searchCmpyIds));
    } else {
      poiSet = PoiMgr.smartSearch(poiRequestJson.searchTerm, searchTypes, searchCmpyIds, 100);
    }

    Log.debug("POI: DB Search took:" + sw.getTime() + "ms");
    for(Long poiId : poiSet.keySet()) {
      PoiRS prs = mergePoi(poiSet.get(poiId), searchCmpyIds);
      poiResponseJson.addPoi(prs);
    }

    sw.stop();
    Log.debug("POI: Response prepared in:" + sw.getTime() + "ms");
    responseMessageJson.header.withCode(ReturnCode.SUCCESS);
    return responseMessageJson;
  }

  static private boolean validatePoiJson(PoiJson inPoi) {
    int nameLengh = StringUtils.length(inPoi.getName());
    if (inPoi.getCmpyId() == null || nameLengh < 2 || nameLengh > 255 ) {
      return false;
    }
    return true;
  }

  /**
   * POI new poi save operation
   * @param req
   * @return
   */
  private static ResponseMessageJson poiAddResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();
    SessionMgr sessionMgr = new SessionMgr(session());
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);

    PoiApiJson poiResponseJson = new PoiApiJson();
    poiResponseJson.operation = Operation.ADD;
    poiResponseJson.pois = new ArrayList<>();
    responseMessageJson.body = poiResponseJson;

    PoiApiJson poiRequestJson = (PoiApiJson) req.body;

    if (poiRequestJson.pois.size() != 1) { //Must be exactly one
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    PoiJson inPoi = poiRequestJson.pois.get(0);

    if (!validatePoiJson(inPoi)) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    /**
     * Algorithm for update
     * 1. Get list of user companies where he is admin or power user and verify requested poi cmpy_id is in the list
     * 2. Build new record
     * 3. Save in the database
     * 4. Send back the record
     */
    Credentials cred;

    Optional<Credentials> opCred = sessionMgr.getOptionalCredentials();

    if(!opCred.isPresent()) {
      responseMessageJson.header.withCode(ReturnCode.AUTH_ACCOUNT_SESSION_EXPIRED);
      return responseMessageJson;
    }

    cred = opCred.get();

    if(!(cred.isCmpyAdmin(inPoi.getCmpyId()) ||
                         cred.isCmpyPowerMember(inPoi.getCmpyId()) ||
                         cred.isUmappedAdmin())) {
      responseMessageJson.header.withCode(ReturnCode.AUTH_VENDOR_FAIL);
      return responseMessageJson;
    }

    PoiRS newPoi = PoiRS.buildRecord(inPoi,
                                     Consortium.INDEPENDENT_CONSORTIUM_ID,
                                     inPoi.getCmpyId(), inPoi.getPoiTypeId(),
                                     FeedSourcesInfo.byName("Web"),
                                     cred.getUserId());
    boolean dbSucess = PoiMgr.save(newPoi);
    if (dbSucess) {
      responseMessageJson.header.withCode(ReturnCode.SUCCESS).withMsg("Successfully created custom company vendor");
    } else {
      responseMessageJson.header.withCode(ReturnCode.DB_TRANSACTION_FAIL).withMsg(
          "Failed to create new vendor. Please contact UMapped");
    }

    poiResponseJson.addPoi(newPoi);

    sw.stop();
    Log.debug("POI: Record saved in:" + sw.getTime() + "ms");
    return responseMessageJson;
  }

  /**
   * POI Delete operation
   * @param req
   * @return
   */
  private static ResponseMessageJson poiDeleteResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();
    SessionMgr sessionMgr = new SessionMgr(session());
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);
    PoiApiJson poiRequestJson = (PoiApiJson) req.body;
    PoiApiJson poiResponseJson = new PoiApiJson();
    poiResponseJson.operation = Operation.DELETE;
    poiResponseJson.pois = new ArrayList<>();
    poiResponseJson.cmpyId = poiRequestJson.cmpyId;
    responseMessageJson.body = poiResponseJson;

    if (poiRequestJson.pois.size() != 1) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    PoiJson inPoi = poiRequestJson.pois.get(0);

    /**
     * Algorithm for deletion
     * 1. Get list of user companies where he is admin or power user and verify requested poi cmpy_id is in the list
     * 2. Find unique record
     * 3. Check if bookings or templates link to poi
     * 4. Mark record as deleted
     * 5. Update record
     * TODO: Serguei: Implement consortium poi deletion
     * TODO: Serguei: Implement src based deletion (rules to be defined)
     */

    //1.
    Credentials cred = sessionMgr.getCredentials();
    if(!(cred.isCmpyAdmin(inPoi.getCmpyId()) ||
         cred.isCmpyPowerMember(inPoi.getCmpyId()) ||
         cred.isUmappedAdmin())) {
      responseMessageJson.header.withCode(ReturnCode.AUTH_VENDOR_FAIL);
      return responseMessageJson;
    }

    //2.
    PoiRS poi = PoiMgr.findForCmpy(inPoi.getPoiId(), inPoi.getCmpyId());
    if (poi == null){
      responseMessageJson.header.withCode(ReturnCode.DB_RECORD_NOT_FOUND);
      return responseMessageJson;
    }

    //3.
    Pair<Integer, Integer> useCount = PoiMgr.countPoiInBookingsAndTemplates(inPoi.getPoiId(), inPoi.getCmpyId());
    if (useCount.getLeft() > 0 || useCount.getRight() > 0) {
      responseMessageJson.header.withCode(ReturnCode.DB_DELETE_FAIL_IN_USE)
                                .withMsg("Vendor can not be deleted! It is used in " + useCount.getLeft() + " bookings" +
                                         " and " + useCount.getRight() + " templates");
      return responseMessageJson;
    }

    //4.
    poi.updateLastUpdatedTS(cred.getUserId());
    boolean result = PoiMgr.markDeleted(poi);
    //Need to send record back to be deleted from cache
    poiResponseJson.addPoi(poi);

    if (result) {
      responseMessageJson.header.withCode(ReturnCode.SUCCESS);
    } else {
      responseMessageJson.header.withCode(ReturnCode.DB_TRANSACTION_FAIL);
    }

    sw.stop();
    Log.debug("POI: Record deleted in:" + sw.getTime() + "ms");
    return responseMessageJson;
  }

  /**
   * POI Update operation
   * @param req
   * @return
   */
  private static ResponseMessageJson poiUpdateResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();
    SessionMgr sessionMgr = new SessionMgr(session());
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);

    PoiApiJson poiResponseJson = new PoiApiJson();
    poiResponseJson.operation = Operation.UPDATE;
    poiResponseJson.pois = new ArrayList<>();
    responseMessageJson.body = poiResponseJson;

    PoiApiJson poiRequestJson = (PoiApiJson) req.body;

    if (poiRequestJson.pois.size() != 1) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    PoiJson inPoi = poiRequestJson.pois.get(0);

    if (!validatePoiJson(inPoi)) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    /**
     * Algorithm for update
     * 1. Get list of user companies where he is admin or power user and verify requested poi cmpy_id is in the list
     * 2. Find current poiId records
     * 3. Merge all of them excluding the one for the current company
     * 4. Cleanse incoming record of duplicate data
     * 5. If record never existed in the database for this company - save as new
     * 6. If record existed overwrite existing one
     */
    //1.
    Credentials cred = sessionMgr.getCredentials();

    if (!cred.isCmpyAdmin(inPoi.getCmpyId()) &&
        !cred.isCmpyPowerMember(inPoi.getCmpyId()) &&
        !cred.isUmappedAdmin()) {
      responseMessageJson.header.withCode(ReturnCode.AUTH_VENDOR_FAIL);
      return responseMessageJson;
    }


    //2.
    Set<Integer> mergeCmpy = new HashSet<>();
    mergeCmpy.add(inPoi.getCmpyId());
    mergeCmpy.add(Company.PUBLIC_COMPANY_ID);

    List<PoiRS> poiList = PoiMgr.findById(inPoi.getPoiId(), mergeCmpy);
    if (poiList == null){
      responseMessageJson.header.withCode(ReturnCode.DB_RECORD_NOT_FOUND);
      return responseMessageJson;
    }

    PoiRS userCmpyRecord = null;
    List<PoiRS> listToMerge = new ArrayList<>();
    for(PoiRS rs : poiList) {
      if (rs.getCmpyId() == inPoi.getCmpyId()) {
        userCmpyRecord = rs;
      } else {
        listToMerge.add(rs);
      }
    }

    PoiRS masterRecord = null;
    if(listToMerge.size() > 0) {
      masterRecord = mergePoi(listToMerge,mergeCmpy);
    }

    //There are parent records, cleanse data common with submitted record
    if (masterRecord != null) {
      inPoi.scrapMasterData(masterRecord.data);
    }

    //If previously there was a custom record for this company
    boolean dbSucess;
    if (userCmpyRecord != null) {
      inPoi.prepareForDb();
      userCmpyRecord.setData(inPoi);
      userCmpyRecord.state = RecordState.ACTIVE; //Even if was deleted before
      userCmpyRecord.updateLastUpdatedTS(sessionMgr.getUserId());
      dbSucess = PoiMgr.update(userCmpyRecord);
      if (dbSucess) {
        responseMessageJson.header.withCode(ReturnCode.SUCCESS).withMsg("Successfully updated vendor");
      } else {
        responseMessageJson.header.withCode(ReturnCode.DB_TRANSACTION_FAIL).withMsg("Failed to updated vendor. Please contact UMapped");
      }
    }
    else { //Brand new record for the company
      userCmpyRecord = PoiRS.buildRecord(inPoi,
                                         Consortium.INDEPENDENT_CONSORTIUM_ID,
                                         inPoi.getCmpyId(), inPoi.getPoiTypeId(),
                                         FeedSourcesInfo.byName("Web"),
                                         cred.getUserId());
      dbSucess = PoiMgr.save(userCmpyRecord);
      if (dbSucess) {
        responseMessageJson.header.withCode(ReturnCode.SUCCESS).withMsg("Successfully created custom company vendor");
      } else {
        responseMessageJson.header.withCode(ReturnCode.DB_TRANSACTION_FAIL).withMsg("Failed to create new vendor. Please contact UMapped");
      }
    }

    if(dbSucess) {
      //Re-merge again to show what the user will see
      listToMerge.add(userCmpyRecord);
      masterRecord = mergePoi(listToMerge, mergeCmpy);
      poiResponseJson.addPoi(masterRecord);
    }

    sw.stop();
    Log.debug("POI: Record updated in:" + sw.getTime() + "ms");
    return responseMessageJson;
  }

  /**
   * Perform operation on the request (Operations Router)
   */
  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result poiFileExecute() {
    StopWatch sw = new StopWatch();
    sw.start();
    RequestMessageJson poiFileRequest = RequestMessageJson.fromJson(request().body().asJson());
    ResponseMessageJson poiFileResponse = null;

    PoiFileJson poiFileJson = (PoiFileJson) poiFileRequest.body;
    Log.debug("POIFILE REQ: " + request().body().asJson().toString());
    switch(poiFileJson.operation) {
      case UPLOAD:
        poiFileResponse = poiFilesUploadResponse(poiFileRequest);
        break;
      case ADD:
      case COPY:
        poiFileResponse = poiFilesAddResponse(poiFileRequest);
        break;
      case UPDATE:
        poiFileResponse = new ResponseMessageJson(poiFileRequest.header, ReturnCode.NOT_SUPPORTED);
        break;
      case DELETE:
        poiFileResponse = poiFilesDeleteResponse(poiFileRequest);
        break;
      case LIST:
      case SEARCH:
        poiFileResponse = poiFilesSearchResponse(poiFileRequest);
        break;
    }

    sw.stop();
    Log.debug("POIFILE: Resp in " + sw.getTime() + "ms :" + poiFileResponse.toJson());
    return ok(poiFileResponse.toJson());
  }

  /**
   *
   * @param req
   * @return
   */
  private static ResponseMessageJson poiFilesUploadResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();
    SessionMgr sessionMgr = new SessionMgr(session());
    Credentials cred = sessionMgr.getCredentials();
    Account account = cred.getAccount();

    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);

    PoiFileJson poiFileResponseJson = new PoiFileJson();
    poiFileResponseJson.operation = Operation.UPLOAD;
    poiFileResponseJson.files = new ArrayList<>();
    responseMessageJson.body = poiFileResponseJson;

    PoiFileJson poiFileRequestJson = (PoiFileJson) req.body;
    poiFileResponseJson.poiId = poiFileRequestJson.poiId;
    poiFileResponseJson.cmpyId = poiFileRequestJson.cmpyId;

    //If user is requesting search for a company he/she does not have access to kick them out
    if (!cred.isCmpyAdmin(poiFileRequestJson.cmpyId) &&
        !cred.isCmpyPowerMember(poiFileRequestJson.cmpyId) &&
        !cred.isUmappedAdmin()) {
      responseMessageJson.header.withCode(ReturnCode.AUTH_VENDOR_FAIL);
      return responseMessageJson;
    }

    /**
     * For file upload functionality POI ID and Cmpy ID must be present
     */
    if (poiFileRequestJson.poiId == null || poiFileRequestJson.poiId.trim().length() == 0 ||
        poiFileRequestJson.cmpyId == null ||
        (poiFileRequestJson.cmpyId == 0 && !sessionMgr.getCredentials().isUmappedAdmin())) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    /**
     * Currently supporting only 1 file upload at a time
     */
    if(poiFileRequestJson.files.size() != 1) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    try {
      Long poiId = Long.parseLong(poiFileRequestJson.poiId);
      Integer cmpyId = poiFileRequestJson.cmpyId;

      PoiRS prs = PoiController.getMergedPoi(poiId, cmpyId);

      //Editing public company need to create a new POI
      if (prs.getCmpyId() == Company.PUBLIC_COMPANY_ID && !sessionMgr.getCredentials().isUmappedAdmin()) {
        prs = PoiController.createNewPoi(poiId, prs.getName(), cmpyId, prs.getTypeId(), sessionMgr.getUserId());
        //TODO: Bug? 2016-11-09: prs not saved here.
      }

      PoiFileJson.PoiFileDetails pfd = poiFileRequestJson.files.get(0);

      PoiFile pf = PoiFile.buildPoiFile(poiId, cmpyId, pfd.name, pfd.desc, sessionMgr.getUserId());
      pf.setSrcId(prs.getSrcId());
      ObjectNode on = Json.newObject();
      FileImage fileImage = uploadHelper(pf, pfd.name, account, on, null);

      if(fileImage == null) {
        Log.err("Failed to respond to POI file upload request :" + req.header.getReqId());
        responseMessageJson.header.withCode(ReturnCode.STORAGE_S3_SIGN);
        return responseMessageJson;
      }

      FileInfo file = fileImage.getFile();
      pf.setNameSys(file.getFilepath());


      Ebean.beginTransaction();
      if (cmpyId == Company.PUBLIC_COMPANY_ID && sessionMgr.getCredentials().isUmappedAdmin()) {
        pf.setSrcId(FeedSourcesInfo.byName("System"));
      } else {
        pf.setSrcId(FeedSourcesInfo.byName("Web"));
      }
      pf.setState(RecordState.PENDING);
      LstFileType fileType = LstFileType.findFromFileName(pfd.name);
      pf.setExtension(fileType);
      pf.setUrl(fileImage.getUrl());
      pf.save();
      Ebean.commitTransaction();
      poiFileResponseJson.addFile(pf.getFileId(), pf.getCmpyId(), pf.getName(),
                                  pf.getUrl(), pf.getDescription(), pf.getPriority());

      poiFileResponseJson.signed_request = on.get("signed_request").asText();
      poiFileResponseJson.policy = on.get("policy").asText();
      poiFileResponseJson.accessKey = on.get("accessKey").asText();
      poiFileResponseJson.key = on.get("key").asText();
      poiFileResponseJson.s3Bucket = on.get("s3Bucket").asText();
      poiFileResponseJson.signature = on.get("signature").asText();
    }
    catch (Exception e) {
      Log.err("Failed to respond to POI file upload request :" + req.header.getReqId());
      e.printStackTrace();
      Ebean.rollbackTransaction();
      responseMessageJson.header.withCode(ReturnCode.STORAGE_S3_SIGN);
      return responseMessageJson;
    }

    sw.stop();
    Log.debug("POIFILE: Upload Response prepared in:" + sw.getTime() + "ms");
    responseMessageJson.header.withCode(ReturnCode.SUCCESS);
    return responseMessageJson;
  }

  /**
   *
   * @param req
   * @return
   */
  private static ResponseMessageJson poiFilesAddResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = new SessionMgr(session());
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);

    PoiFileJson poiFileResponseJson = new PoiFileJson();
    poiFileResponseJson.operation = Operation.ADD;
    poiFileResponseJson.files = new ArrayList<>();
    responseMessageJson.body = poiFileResponseJson;

    PoiFileJson poiFileRequestJson = (PoiFileJson) req.body;
    poiFileResponseJson.poiId = poiFileRequestJson.poiId;
    poiFileResponseJson.cmpyId = poiFileRequestJson.cmpyId;

    //If user is requesting search for a company he/she does not have access to kick them out


    /**
     * For correct operation both POI ID and CMPY ID must be present in the request
     */
    if (poiFileRequestJson.poiId == null || poiFileRequestJson.poiId.trim().length() == 0 ||
        poiFileRequestJson.cmpyId == null ||
        (poiFileRequestJson.cmpyId == 0 && !sessionMgr.getCredentials().isUmappedAdmin())) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    /**
     * Currently supporting only one file upload at a time
     */
    if(poiFileRequestJson.files.size() != 1) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    try {
      PoiFileJson.PoiFileDetails pfd = poiFileRequestJson.files.get(0);
      Long fileId = Long.parseLong(pfd.fileId);
      Long poiId = Long.parseLong(poiFileRequestJson.poiId);

      Integer cmpyId = poiFileRequestJson.cmpyId;
      PoiFile newPoiFile = PoiFile.find.byId(fileId);

      if (newPoiFile == null) {
        responseMessageJson.header.withCode(ReturnCode.DB_RECORD_NOT_FOUND);
        return responseMessageJson;
      }

      boolean confirmed = ImageController.uploadConfirmationHelper(newPoiFile.getFile().getPk());

      newPoiFile.refresh();

      if(newPoiFile.getFile() == null) {
        responseMessageJson.header.withCode(ReturnCode.STORAGE_S3_UPLOAD);
        newPoiFile.delete();
        return responseMessageJson;
      }

      Ebean.beginTransaction();
      newPoiFile.setState(RecordState.ACTIVE);
      newPoiFile.updateModInfo(sessionMgr.getUserId());
      newPoiFile.update();
      incrementFileCount(poiId, cmpyId, sessionMgr.getUserId());
      Ebean.commitTransaction();

      //We need only images for now
      List<PoiFile> files = gatherFilesForPoi(poiId, cmpyId, "images");

      for(PoiFile pf : files) {
        poiFileResponseJson.addFile(pf.getFileId(), pf.getCmpyId(),
                                    pf.getName(), pf.getUrl(), pf.getDescription(), pf.getPriority());
      }

    } catch (Exception e) {
      Log.err("Failed while accepting successful file upload on request :" + req.header.getReqId());
      e.printStackTrace();
      Ebean.rollbackTransaction();
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    sw.stop();
    Log.debug("POIFILE: File ADD Success Response prepared in:" + sw.getTime() + "ms");
    responseMessageJson.header.withCode(ReturnCode.SUCCESS);
    return responseMessageJson;
  }

  /**
   * List files for a specific POI
   * @param req
   * @return
   */
  private static ResponseMessageJson poiFilesDeleteResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = new SessionMgr(session());
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);

    PoiFileJson poiFileResponseJson = new PoiFileJson();
    poiFileResponseJson.operation = Operation.DELETE;
    poiFileResponseJson.files = new ArrayList<>();
    responseMessageJson.body = poiFileResponseJson;

    PoiFileJson poiFileRequestJson = (PoiFileJson) req.body;
    poiFileResponseJson.poiId = poiFileRequestJson.poiId;
    poiFileResponseJson.cmpyId = poiFileRequestJson.cmpyId;

    //If user is requesting search for a company he/she does not have access to kick them out
    Credentials cred = sessionMgr.getCredentials();
    if (!cred.isCmpyAdmin(poiFileRequestJson.cmpyId) &&
        !cred.isCmpyPowerMember(poiFileRequestJson.cmpyId) &&
        !cred.isUmappedAdmin()) {
      responseMessageJson.header.withCode(ReturnCode.AUTH_VENDOR_FAIL);
      return responseMessageJson;
    }


    /**
     * For search functionality poi id and company id must be present
     * TODO: Accomodate consortia
     */
    if (poiFileRequestJson.poiId == null || poiFileRequestJson.poiId.trim().length() == 0 ||
        poiFileRequestJson.cmpyId == null ||
        (poiFileRequestJson.cmpyId == 0 && !sessionMgr.getCredentials().isUmappedAdmin())) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    if(poiFileRequestJson.files.size() != 1) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    PoiFileJson.PoiFileDetails fToDelete = poiFileRequestJson.files.get(0);
    if (poiFileRequestJson.poiId == null || fToDelete.fileId == null) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    Long poiId = Long.parseLong(poiFileRequestJson.poiId);
    Long fileId = Long.parseLong(fToDelete.fileId);
    if (poiId <= 0 || fileId <= 0) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }


    PoiFile pfToDel = PoiFile.find.byId(fileId);

    if(pfToDel.getCmpyId() == 0 && !sessionMgr.getCredentials().isUmappedAdmin()) {
      //If trying to delete file belonging to public companies, a new record should be created that marks file
      //as deleted. Filename, file url should match
      PoiRS prs = PoiMgr.findForCmpy(poiId, poiFileRequestJson.cmpyId);
      //If there is a previously deleted record - undeleting it
      if (prs != null && prs.state == RecordState.DELETED) {
        prs.state = RecordState.ACTIVE;
        prs.setFileCount(prs.getFileCount() - 1);
        PoiMgr.update(prs);
      }

      if (prs == null) { //No company specific record
        prs = getMergedPoi(poiId, poiFileResponseJson.cmpyId);
        PoiRS newCmpyPoi = PoiController.createNewPoi(poiId, prs.getName(), poiFileRequestJson.cmpyId,
                                                      prs.getTypeId(),
                                                      sessionMgr.getUserId());
        newCmpyPoi.setCode(prs.getCode());
        newCmpyPoi.setFileCount(-1);
        PoiMgr.update(newCmpyPoi);
      }


      List<PoiFile> prevDeleted = PoiFile.getDeleted(poiId, poiFileRequestJson.cmpyId, pfToDel.getNameSys());
      for(PoiFile f2d: prevDeleted) {
        f2d.delete(); //Deleting previously deleted records as there may be a lot of duplicates
      }

      PoiFile pf = PoiFile.buildPoiFile(poiId, poiFileRequestJson.cmpyId, pfToDel.getName(), pfToDel.getDescription(),
                                        sessionMgr.getUserId());
      pf.setFileImage(pfToDel.getFileImage());
      pf.setNameSys(pfToDel.getNameSys());
      pf.setUrl(pfToDel.getUrl());
      pf.setPriority(pfToDel.getPriority());
      pf.setExtension(pfToDel.getExtension());
      pf.setSrcId(FeedSourcesInfo.byName("Web"));
      pf.setState(RecordState.DELETED);
      pf.save();
    } else {
      pfToDel.updateModInfo(sessionMgr.getUserId());
      pfToDel.setState(RecordState.DELETED);
      pfToDel.update();
      decrementFileCount(poiId, poiFileRequestJson.cmpyId, sessionMgr.getUserId());
    }

    //We need only images for now
    List<PoiFile> files = gatherFilesForPoi(poiId, poiFileRequestJson.cmpyId, "images");

    //For this poi let's update all file counts
    poiUpdateFileCounts(poiId);

    Log.debug("POIFILE: DB Search took:" + sw.getTime() + "ms");

    for(PoiFile pf : files) {
      poiFileResponseJson.addFile(pf.getFileId(), pf.getCmpyId(),
                                  pf.getName(), pf.getUrl(), pf.getDescription(), pf.getPriority());
    }

    sw.stop();
    Log.debug("POIFILE: Response prepared in:" + sw.getTime() + "ms");
    responseMessageJson.header.withCode(ReturnCode.SUCCESS);
    return responseMessageJson;
  }

  /**
   * For all records associated with this poi id writes correct number of files associated
   * @param poiId
   */
  private static void poiUpdateFileCounts(long poiId) {
    List<PoiRS> pois = PoiMgr.findAllById(poiId);
    for(PoiRS prs : pois) {
      if (prs.getCmpyId() == Company.PUBLIC_COMPANY_ID) {
        List<PoiFile> files = PoiFile.getAllForPoiSrs(prs.getId(), prs.consortiumId, prs.getCmpyId(), prs.getSrcId());
        int counter = 0;
        for (PoiFile f : files) {
          counter += (f.getState() == RecordState.ACTIVE) ? 1 : 0;
        }
        if (prs.getFileCount() != counter) {
          prs.setFileCount(counter);
          PoiMgr.update(prs);
        }
      } else {
        List<PoiFile> files = gatherFilesForPoi(prs.getId(),prs.getCmpyId(), "image");
        if (prs.getFileCount() != files.size()) {
          prs.setFileCount(files.size());
          PoiMgr.update(prs);
        }
      }
    }
  }

  /**
   * List files for a specific POI
   * @param req
   * @return
   */
  private static ResponseMessageJson poiFilesSearchResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = new SessionMgr(session());
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);

    PoiFileJson poiFileResponseJson = new PoiFileJson();
    poiFileResponseJson.operation = Operation.LIST;
    poiFileResponseJson.files = new ArrayList<>();
    responseMessageJson.body = poiFileResponseJson;

    PoiFileJson poiFileRequestJson = (PoiFileJson) req.body;
    poiFileResponseJson.poiId = poiFileRequestJson.poiId;
    poiFileResponseJson.cmpyId = poiFileRequestJson.cmpyId;

    //If user is requesting search for a company he/she does not have access to kick them out
    Credentials cred = sessionMgr.getCredentials();
    if (!cred.isCmpyMember(poiFileRequestJson.cmpyId) &&
        !cred.isUmappedAdmin()) {
      responseMessageJson.header.withCode(ReturnCode.AUTH_VENDOR_FAIL);
      return responseMessageJson;
    }

    /**
     * For search functionality poi id and company id must be present
     * TODO: Accommodate consortia
     */
    if (poiFileRequestJson.poiId == null || poiFileRequestJson.poiId.trim().length() == 0 ||
        poiFileRequestJson.cmpyId == null) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }
    Long poiId = Long.parseLong(poiFileRequestJson.poiId);
    if (poiId <= 0) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    //We need only images for now
    PoiRS prs = getMergedPoi(poiId, poiFileRequestJson.cmpyId);
    if (prs == null) { //This happened when record got merged
      responseMessageJson.header.withCode(ReturnCode.DB_RECORD_NOT_FOUND);
      return responseMessageJson;
    }
    List<PoiFile> files = gatherFilesForPoi(poiId, prs.getCmpyId(), "images");

    Log.debug("POIFILE: DB Search took:" + sw.getTime() + "ms");

    for(PoiFile pf : files) {
      poiFileResponseJson.addFile(pf.getFileId(), pf.getCmpyId(),
                                  pf.getName(), pf.getUrl(), pf.getDescription(), pf.getPriority());
    }

    sw.stop();
    Log.debug("POIFILE: Response prepared in:" + sw.getTime() + "ms");
    responseMessageJson.header.withCode(ReturnCode.SUCCESS);
    return responseMessageJson;
  }

  /**
   * Traditional Scala template output (that performs async queries)
   * @return
   */
  @With({Authenticated.class, Authorized.class})
  public Result pois() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Credentials cred = sessionMgr.getCredentials();
    PoiBaseView bv = new PoiBaseView();
    bv.userCompanies = new HashMap<>();

    if(cred.getCmpyIdInt() != Company.NO_COMPANY_ID) {
      bv.userCompanies.put(cred.getCmpyIdInt(), cred.getCmpyName());
    }
    bv.isUMappedAdmin = SecurityMgr.isUmappedAdmin(sessionMgr);

    if(bv.isUMappedAdmin) {
      bv.userCompanies.put(Company.PUBLIC_COMPANY_ID, "Public");
    }

    bv.message = flash(SessionConstants.SESSION_PARAM_MSG);
    return  ok(views.html.poi.poi.render(bv));
  }

  public Result poiPhotos(Long poiId, Integer cmpyId) {
    if (poiId != null && cmpyId != null) {
      PoiView view = new PoiView();
      view.id = poiId.toString();
      view.photos = getPhotosForPoi(poiId, cmpyId);
      if (view.photos.size() > 0) {
        return ok(views.html.poi.photoModal.render(view));
      }
    }
    BaseView baseView = new BaseView();
    baseView.message = "No photos available.";
    return ok(views.html.common.message.render(baseView));
  }


  public static ArrayNode autocompleteHelper(String term, List<Integer> types, Set<Integer> searchCmpyIds)  {
    ObjectNode result = Json.newObject();
    ArrayNode array = result.arrayNode();

    PoiTypeInfo.PoiType mainType = null;
    if(types.size() == 1) {
      mainType = PoiTypeInfo.Instance().byId(types.get(0));
    }

    term = term.trim();
    //If nothing is being searched for - return nothing
    if (term == null || term.length() == 0) {
      return array;
    }

    //Limiting search to 30 results to limit load on the system
    Map<Long, List<PoiRS>> poiSet = PoiMgr.smartSearch(term, types, searchCmpyIds, 30);
    for(Long poiId : poiSet.keySet()) {
      PoiRS prs = mergePoi(poiSet.get(poiId), searchCmpyIds);
      String label = prs.getName();

      if (mainType != null) {
        switch (mainType.getName()) {
          case "Airline":
            String code = prs.getCode();
            if (code != null && code.length() > 1 && code.length() <= 3) {
              label += " (" + code + ")";
            }
            break;
          case "Accommodations":
          case "Activity":
          case "Transportation":
            String address = prs.getMainAddressString();
            if (address.length() > 0) {
              label +=  ", " + address;
            }
            break;
          case "Airport":
            if (prs.getCode() != null && prs.getCode().length() > 0) {
              label =  prs.getCode() + " - " + label ;
            }
            break;
          case "Ship":
          case "Port":
            if (prs.countryCode != null && prs.countryCode.length() > 0) {
              label += ", " + CountriesInfo.Instance().byAlpha3(prs.countryCode).getName(CountriesInfo.LangCode.EN);
            }
            break;
        }
      }

      if (prs.getCmpyId() != Company.PUBLIC_COMPANY_ID) {
        label = label + " **";
      }

      ObjectNode classNode = Json.newObject();
      classNode.put("label", label);
      classNode.put("id", Long.toString(prs.getId()));
      array.add(classNode);
    }

    return array;
  }

    /**
     * Stand-in replacement for ProviderController ProviderList
     * @param term
     * @param type
     * @param cmpyId
     * @return
     */
  @With({Authenticated.class, Authorized.class})
  public Result poiList(String term, Integer type, String cmpyId) {
    StopWatch sw = new StopWatch();
    sw.start();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    /**
     * Based design discussions previous functionality to perform search on all accessible pois
     * is deprecated. User is able to search only for one company ID at a time. If at later point
     * this feature returns it should easy to add.
     *
     * A specific company will be searched and if that record also has a public record, those
     * will be merged, public records without corresponding private will be dropped
     */
    Set<Integer> searchCmpyIds = new HashSet<>();
    if (cmpyId != null || cmpyId.length() != 0) {

      if (SecurityMgr.canAccessCmpy(cmpyId, sessionMgr)) {
        Company cmpy = Company.find.byId(cmpyId);
        searchCmpyIds.add(cmpy.getCmpyId()); //Searching for one company at a time
      }
      else {
        //shared booking, cmpy id belongs to company who shared trip => add all from session
        searchCmpyIds.add(sessionMgr.getCredentials().getCmpyIdInt());
      }
    }
    searchCmpyIds.add(Company.PUBLIC_COMPANY_ID);

    List<Integer> searchTypes;
    if (type == null || type == 0) {
      //need to add all
      searchTypes = PoiTypeInfo.Instance().getTypes();
    } else {
      searchTypes = new ArrayList<>(1);
      searchTypes.add(type);
    }

    ArrayNode array = autocompleteHelper(term, searchTypes, searchCmpyIds);

    sw.stop();
    Log.debug("POI: Autocomplete lookup prepared in:" + sw.getTime() + "ms");
    return ok(array);
  }

  /**
   * Creates a new record for a POI, usually when a public POI already exists. If custom record was previously
   * deleted (marked deleted) - previous entry is resurrected.
   * @param poiId
   * @param poiName
   * @param cmpyId
   * @param typeId
   * @param userId
   * @return
   */
  public static PoiRS createNewPoi(Long poiId, String poiName, Integer cmpyId, Integer typeId, String userId) {
    //See if record was previously deleted, if so - resurrect zombie instead of creating another oxygen waster
    PoiRS oldRec = PoiMgr.findForCmpy(poiId, cmpyId);
    if (oldRec == null) { //No zombies here
      PoiJson pjs = PoiJson.buildPoiJson(poiId, cmpyId, typeId, poiName);
      PoiRS newPoi = PoiRS.buildRecord(pjs,
                                       Consortium.INDEPENDENT_CONSORTIUM_ID,
                                       cmpyId,
                                       typeId,
                                       FeedSourcesInfo.byName("Web"),
                                       userId);
      PoiMgr.save(newPoi);
      return newPoi;
    }
    oldRec.state = RecordState.ACTIVE;
    oldRec.setName(poiName);
    oldRec.updateLastUpdatedTS(userId);
    PoiMgr.update(oldRec);
    return oldRec;
  }

  public static PoiRS mergePoi(List<PoiRS> dataset, Set<Integer> cmpyIds) {
    if (dataset.size() == 0) {
      return null;
    }

    /* 1. Remove unused data */
    for (int idx = 0; idx < dataset.size(); ) {
      PoiRS prs = dataset.get(idx);
      if ((prs.getCmpyId() != 0 && !cmpyIds.contains(prs.getCmpyId())) ||
          (prs.state != RecordState.ACTIVE)){
        dataset.remove(prs);
      } else {
        idx++;
      }
    }

    /* 2. Order data in order of importance */
    Collections.sort(dataset, new Comparator<PoiRS>() {
      @Override public int compare(PoiRS o1, PoiRS o2) {
        if (o1.getCmpyId() == o2.getCmpyId() && o1.getId() == o2.getId() &&
            o1.consortiumId == o2.consortiumId &&
            o1.getSrcId() == o2.getSrcId()) {
          return 0; //Equal
        }
        /* Merge rules:
         * 1. Same company ID (private or public records)
         *      a) If private company but different consortia: one with public consortium (i.e. private to
         *         the company is higher priority)
         *      b) If consortia are the same:
         *         i) If source is web it overwrites all other
         *        ii) Otherwise highest srcid is more important (vague assumption
         */
        if (o1.getCmpyId() == o2.getCmpyId()) {

          //Public companies, but different consortia consortium with higher number is more important
          if (o1.getCmpyId() == Company.PUBLIC_COMPANY_ID && o1.consortiumId != o2.consortiumId) {
            if (o1.consortiumId > o2.consortiumId) {
              return 1;
            }
            return -1;
          }

          //Private company but different consortium
          //   - If record does not belong to the consortium it is higher priority
          //   - Otherwise, consortium with larger id wins
          if (o1.getCmpyId() != Company.PUBLIC_COMPANY_ID && o1.consortiumId != o2.consortiumId) {
            if (o1.consortiumId == ConsortiumCompany.NO_CONSORTIUM) {
              return 1; //o1 more important
            }
            if (o1.consortiumId > o2.consortiumId) {
              return 1;
            }
            return -1; //o2 more important
          }

          if (o1.getSrcId() == FeedSourcesInfo.byName("Web")) {
            return 1; //o1 more important
          }
          if (o2.getSrcId() == FeedSourcesInfo.byName("Web")) {
            return -1; //o2 more important
          }
          if (o1.getSrcId() > o2.getSrcId()) {
            return 1; //o1 more important
          }
          return -1;
        }

        /* 2. One record is public and one record is private
         *    Private record overtakes public
         */
        if ((o1.getCmpyId() == Company.PUBLIC_COMPANY_ID  || o2.getCmpyId() == Company.PUBLIC_COMPANY_ID) &&
            (o1.getCmpyId() != Company.PUBLIC_COMPANY_ID  || o2.getCmpyId() != Company.PUBLIC_COMPANY_ID)) {
          if (o1.getCmpyId() != Company.PUBLIC_COMPANY_ID) {
            return 1; //o1 more important
          }
          return -1; //o2 more important
        }
        return 0;
      }
    });

    for(int idx = 0; idx < dataset.size(); idx++) {
      PoiRS prs = dataset.get(idx);
      if (DEBUG) {
        Log.debug("Sorted >>> Record #" + idx + prs.toString());
      }
    }

    /* 3.  Starting with empty record for each poi in the list overwrite data previously in the record */
    PoiRS merged = null;
    if (dataset.size() == 1) {
      merged = dataset.get(0);
    } else {
      merged = new PoiRS();
      for (PoiRS prs : dataset) {
        merged.overwriteWith(prs);
      }
    }

    merged.data.setFileCount(merged.getFileCount());
    merged.data.setCmpyId(merged.getCmpyId());

    //fix for screw up with port import where co-ordinates were in field but not in json
    if (merged.locLat != 0.0 || merged.locLong != 0.0 || merged.countryCode != null) {
      Address addr = merged.getMainAddress();
      if (addr == null) {
        addr = new Address();
        addr.setCountryCode(merged.countryCode);
        addr.setAddressType(AddressType.MAIN);
        Coordinates coordinates = new Coordinates();
        coordinates.setLatitude(merged.locLat);
        coordinates.setLongitude(merged.locLong);
        addr.setCoordinates(coordinates);
        merged.setMainAddress(addr);
      }

    }


    if (DEBUG) {
      Log.debug("Merged >>> Record" + merged.toString());
    }

    /* 4. Done */
    return merged;
  }

  public static PoiRS getMergedPoi(Long poiId, Integer cmpyId) {
    if (poiId == null || cmpyId == null) {
      return null;
    }

    Set<Integer> cmpies = new TreeSet<>();
    cmpies.add(cmpyId);

    return getMergedPoi(poiId, cmpies);
  }

  public static PoiRS getMergedPoi(Long poiId, Set<Integer> cmpyIds) {
    if (poiId == null || cmpyIds == null) {
      return null;
    }

    Set<Integer> cmpies = new TreeSet<>();
    cmpies.addAll(cmpyIds);
    cmpies.add(Company.PUBLIC_COMPANY_ID);

    List<PoiRS> dataset = PoiMgr.findById(poiId, cmpies);
    return mergePoi(dataset, cmpies);
  }

  /**
   * Helper function searching for a single POI ID based on unique code and specific type.
   *
   * @param code
   * @param type
   * @param cmpies
   * @return
   */
  public static PoiRS findAndMergeByCode(String code, Integer type, Set<Integer> cmpies) {
    if (code == null || type == null || cmpies == null) {
      return null;
    }
    List<Integer> types = new ArrayList<>(1);
    types.add(type);
    Set<Integer> allCmpies = new HashSet<>();
    allCmpies.addAll(cmpies);
    allCmpies.add(Company.PUBLIC_COMPANY_ID);
    Map<Long, List<PoiRS>> points = PoiMgr.findByCode(code, types, allCmpies, 1);

    for(Long pid : points.keySet()) {
      return mergePoi(points.get(pid), cmpies);
    }

    return null;
  }

  public static PoiRS findAndMergeByCode(String code, Integer type, Integer cmpy) {
    Set<Integer> cmpies = new TreeSet<>();
    cmpies.add(cmpy);
    return findAndMergeByCode(code, type, cmpies);
  }

  public static PoiRS findAirportByCityAndName (String city, String name, Integer cmpyId) {
    if (city == null || name == null || cmpyId == null) {
      return null;
    }

    name = name.toUpperCase();
    if (name.contains("INTL") ) {
      name = name.replace("INTL", "").trim();
    }
    Set<Integer> cmpies = new HashSet<>();
    cmpies.add(cmpyId);
    cmpies.add(Company.PUBLIC_COMPANY_ID);

    int pt = PoiTypeInfo.Instance().byName("Airport").getId();
    List<Integer> poiTypes = new ArrayList<>();
    poiTypes.add(pt);

    Map<Long, List<PoiRS>> pois = PoiMgr.findByTermAndKeywords(name, city, poiTypes, cmpies, 1);
    for(Long pid : pois.keySet()) {
      return mergePoi(pois.get(pid), cmpies);
    }
    return null;
  }

  /**
   *
   * @param term
   * @param types
   * @param cmpies
   * @param limit maximum number of records to return (smaller number => faster runtime)
   * @return never null
   */
  public static List<PoiRS> findMergedByTerm(String term, List<Integer> types, Set<Integer> cmpies, int limit) {
    List<PoiRS> result = new ArrayList<>();
    if (term == null || types == null || cmpies == null || limit <= 0) {
      return result;
    }
    Set<Integer> allCmpies = new HashSet<>();
    allCmpies.addAll(cmpies);
    if (!allCmpies.contains(Company.PUBLIC_COMPANY_ID)) {
      allCmpies.add(Company.PUBLIC_COMPANY_ID);
    }

    Map<Long, List<PoiRS>> poiSet = PoiMgr.smartSearch(term, types, allCmpies, limit);

    for(Long poiId : poiSet.keySet()) {
      PoiRS prs = mergePoi(poiSet.get(poiId), cmpies);
      result.add(prs);
    }

    return result;
  }

  /**
   *
   * @param term
   * @param types
   * @param cmpies
   * @param limit maximum number of records to return (smaller number => faster runtime)
   * @return never null
   */
  public static List<PoiRS> findMergedByName(String term, List<Integer> types, Set<Integer> cmpies, int limit) {
    List<PoiRS> result = new ArrayList<>();
    if (term == null || types == null || cmpies == null || limit <= 0) {
      return result;
    }


    Map<Long, List<PoiRS>> poiSet = PoiMgr.findByName(term, types, cmpies, limit);

    for(Long poiId : poiSet.keySet()) {
      PoiRS prs = mergePoi(poiSet.get(poiId), cmpies);
      result.add(prs);
    }

    return result;
  }

  /**
   * @param term
   * @param type
   * @param cmpy
   * @param limit maximum number of records to return (smaller number => faster runtime)
   * @return never null
   */
  public static List<PoiRS> findMergedByTag(String term, Integer type, Integer cmpy, int limit, boolean includePublic) {
    List<Integer> types = new ArrayList<>();
    types.add(type);
    Set<Integer> cmpies = new HashSet<>();
    cmpies.add(cmpy);


    List<PoiRS> result = new ArrayList<>();
    if (term == null || types == null || cmpies == null || limit <= 0) {
      return result;
    }
    Set<Integer> allCmpies = new HashSet<>();
    allCmpies.addAll(cmpies);
    if (includePublic && !allCmpies.contains(Company.PUBLIC_COMPANY_ID)) {
      allCmpies.add(Company.PUBLIC_COMPANY_ID);
    }

    //do a search by tag - if we find an exact match, then we exit
    Map<Long, List<PoiRS>> poiSet = PoiMgr.findByTag(term, types, allCmpies, limit);
    if (poiSet != null && poiSet.size() > 0) {

      for (Long poiId : poiSet.keySet()) {
        PoiRS prs = mergePoi(poiSet.get(poiId), cmpies);
        result.add(prs);
      }
    }

    return result;
  }

  /**
   * @param term
   * @param type
   * @param cmpy
   * @param limit maximum number of records to return (smaller number => faster runtime)
   * @return never null
   */
  public static List<PoiRS> findMatchByTagSrcReference(String term, Integer type, Integer cmpy, int limit) {
    List<Integer> types = new ArrayList<>();
    types.add(type);
    Set<Integer> cmpies = new HashSet<>();
    cmpies.add(cmpy);


    List<PoiRS> result = new ArrayList<>();
    if (term == null || types == null || cmpies == null || limit <= 0) {
      return result;
    }
    Set<Integer> allCmpies = new HashSet<>();
    allCmpies.addAll(cmpies);

    //do a search by tag - if we find an exact match, then we exit
    Map<Long, List<PoiRS>> poiSet = PoiMgr.findMatchByTagSrcReference(term, types, allCmpies, limit);
    if (poiSet != null && poiSet.size() > 0) {

      for (Long poiId : poiSet.keySet()) {
        PoiRS prs = mergePoi(poiSet.get(poiId), cmpies);
        result.add(prs);
      }
    }

    return result;
  }

  /**
   * @param term
   * @param type
   * @param cmpy
   * @param limit maximum number of records to return (smaller number => faster runtime)
   * @return never null
   */
  public static List<PoiRS> findMatchBySrcReference(String term, Integer type, Integer cmpy, int limit) {
    List<Integer> types = new ArrayList<>();
    types.add(type);
    Set<Integer> cmpies = new HashSet<>();
    cmpies.add(cmpy);


    List<PoiRS> result = new ArrayList<>();
    if (term == null || types == null || cmpies == null || limit <= 0) {
      return result;
    }
    Set<Integer> allCmpies = new HashSet<>();
    allCmpies.addAll(cmpies);

    //do a search by tag - if we find an exact match, then we exit
    Map<Long, List<PoiRS>> poiSet = PoiMgr.findMatchBySrcReference(term, types, allCmpies, limit);
    if (poiSet != null && poiSet.size() > 0) {

      for (Long poiId : poiSet.keySet()) {
        PoiRS prs = mergePoi(poiSet.get(poiId), cmpies);
        result.add(prs);
      }
    }

    return result;
  }

  /**
   * Helper function for single type and cmpy
   * @param term
   * @param type
   * @param cmpy
   * @param limit
   * @return
   */
  public static List<PoiRS> findMergedByTerm(String term, Integer type, Integer cmpy, int limit) {
    List<Integer> types = new ArrayList<>();
    types.add(type);
    Set<Integer> cmpies = new HashSet<>();
    cmpies.add(cmpy);
    return findMergedByTerm(term, types, cmpies, limit);
  }

  /**
   * Only searches cmpy record
   * @param term
   * @param limit maximum number of records to return (smaller number => faster runtime)
   * @return never null
   */
  public static List<PoiRS> findMergedByTermForCmpy(String term, Integer type, Integer cmpy, int limit) {
    List<Integer> types = new ArrayList<>();
    types.add(type);
    Set<Integer> cmpies = new HashSet<>();
    cmpies.add(cmpy);

    List<PoiRS> result = new ArrayList<>();
    if (term == null || types == null || cmpies == null || limit <= 0) {
      return result;
    }
    Set<Integer> allCmpies = new HashSet<>();
    allCmpies.addAll(cmpies);
    if (allCmpies.contains(Company.PUBLIC_COMPANY_ID)) {
      allCmpies.remove(Company.PUBLIC_COMPANY_ID);
    }
    Map<Long, List<PoiRS>> poiSet = PoiMgr.findByTerm(term, types, allCmpies, limit);

    for(Long poiId : poiSet.keySet()) {
      PoiRS prs = mergePoi(poiSet.get(poiId), cmpies);
      result.add(prs);
    }

    return result;
  }

  public static boolean incrementFileCount(Long poiId, Integer cmpyId, String userId) {
    PoiRS prs = PoiMgr.findForCmpy(poiId, cmpyId);

    if (prs == null) {
      return false;
    }

    return incrementFileCount(prs, userId);
  }

  public static boolean incrementFileCount(Long poiId, Integer cmpyId, Integer srcId, Integer consortiumId, String userId) {
    List<PoiRS> pois = PoiMgr.findByPk(poiId, cmpyId, consortiumId, srcId);
    if (pois == null || pois.size() == 0) {
      return false;
    }

    return incrementFileCount(pois.get(0), userId);
  }

  public static boolean incrementFileCount(PoiRS prs, String userId) {
    prs.setFileCount(prs.getFileCount() + 1);
    prs.updateLastUpdatedTS(userId);
    return PoiMgr.update(prs);
  }

  public static boolean decrementFileCount(Long poiId, Integer cmpyId, String userId) {
    PoiRS prs = PoiMgr.findForCmpy(poiId, cmpyId);

    if (prs == null) {
      return false;
    }

    if (prs.getFileCount() > 0) {
      prs.setFileCount(prs.getFileCount() - 1);
      prs.updateLastUpdatedTS(userId);
      return PoiMgr.update(prs);
    }
    return false;
  }

  /**
   * Queries database for files taking company and consortium information of the current user
   * More logic (like caching, etc to be added as needed)
   *
   * @param poiId - poi id to gather files
   * @return
   */
  public static List<PoiFile> gatherFilesForPoi(Long poiId, Integer cmpyId, String mimeType) {
    //Preparing companies whose pois to search
    List<Integer> memberCompanies = new ArrayList<>();
    memberCompanies.add(cmpyId);
    if (cmpyId != Company.PUBLIC_COMPANY_ID && cmpyId != 883) { //hack for ski.com
      memberCompanies.add(Company.PUBLIC_COMPANY_ID); //Always add public records
    }

    List<PoiFile> result;

    if (mimeType != null) {
      List<LstFileType> mimeTypes = null;
      switch(mimeType) {
        case "image":
          mimeTypes = LstFileType.getImageExtensions();
          break;
        default:
          break;
      }
      if (mimeTypes != null) {
        List<String> extensions = new ArrayList<>();
        for(LstFileType mt:mimeTypes) {
          extensions.add(mt.getExtension());
        }
        result = PoiFile.getCmpyTypedForPoi(poiId, memberCompanies, extensions);
      } else {
        result = PoiFile.getCmpyForPoi(poiId, memberCompanies);
      }
    } else {
      result =  PoiFile.getCmpyForPoi(poiId, memberCompanies);
    }

    if (result != null && ConfigMgr.isProduction() && cmpyId == 883) {
      //we sort by the numeric portion of the filename for ski.com
      Collections.sort(result, new Comparator<PoiFile>() {
        public int compare(PoiFile obj1, PoiFile obj2) {
          try {
            int i1 = 0;
            try {
              i1 = Integer.valueOf(obj1.getName().replaceAll("[^0-9]", ""));
            } catch (Exception e) {

            }
            int i2 = 0;
            try {
              i2 = Integer.valueOf(obj2.getName().replaceAll("[^0-9]", ""));
            } catch (Exception e) {

            }
            return i1-i2;
          } catch (Exception e){
            Log.err("Ski.com file compare error:" + obj1.getName()  + " -- " + obj2.getName(), e);
          }
          return 0;
        }
      });

    }

    //TODO: Merge files here, removing duplicates and removing duplicates files that are marked as deleted at least once
    Set<String> namesToDelete = new HashSet<>();
    for(Iterator<PoiFile> it = result.iterator(); it.hasNext();) {
      PoiFile pf = it.next();
      if (pf.getState() == RecordState.DELETED) {
        it.remove();
        namesToDelete.add(pf.getNameSys());
      }
    }

    //Remove any other items
    for(Iterator<PoiFile> it = result.iterator(); it.hasNext();) {
      PoiFile pf = it.next();
      if (namesToDelete.contains(pf.getNameSys())) {
        it.remove();
      }
    }

    return result;
  }

  /**
   * @param poiId
   * @return never returns null, empty list if no files
   */
  public static List<AttachmentView> getPhotosForPoi(Long poiId, Integer cmpyId) {
    List<AttachmentView> photos = new ArrayList<>();
    try {

      List<PoiFile> files = gatherFilesForPoi(poiId, cmpyId, "image");
      if (files != null) {
        for (PoiFile f : files) {
          AttachmentView v = new AttachmentView();
          v.id = Long.toString(f.getFileId());
          v.name = f.getName();
          v.attachName = f.getNameSys();
          v.attachUrl = f.getUrl();
          v.attachType = f.getExtension().getMimeType();
          v.comments = f.getDescription();
          photos.add(v);
        }
      }
    }
    catch (Exception e) {
      Log.err("getPhotosForPoi: " + poiId, e.getMessage());
      e.printStackTrace();
    }
    return photos;
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result poiFeed(Integer feedSrcId, Integer feedState) {
    feedSrcId = (feedSrcId!=null)?feedSrcId: FeedSourcesInfo.byName("Ensemble Hotel");
    feedState = (feedState!=null)?feedState:PoiImportRS.ImportState.UNPROCESSED.ordinal();
    PoiFeedView fv = buildPoiFeedView(feedSrcId, feedState);

    return  ok(feeds.render(fv));
  }

  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result poiFeedDT(Integer feedSrcId, Integer feedState) {

    feedSrcId = (feedSrcId!=null)?feedSrcId: FeedSourcesInfo.byName("Virtuoso Hotels");
    feedState = (feedState!=null)?feedState:PoiImportRS.ImportState.UNPROCESSED.ordinal();

    PoiImportRS.ImportState state = PoiImportRS.ImportState.fromIdx(feedState);

    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);

    class PoiFeedRow {
      public Long id;
      public String name;
      public String country;
      public String city;
      public String postalCode;
      public String importTime;
      public String fileName;
      public Map<String, String> actions;

      public PoiFeedRow() {
        actions = new TreeMap<>();
      }
    }

    //PoiImportMgr.getImportedPois(feedSrcId, state, 2000);

    DataTablesView<PoiFeedRow> dtv = new DataTablesView<>(dtRequest.draw);
    dtv.recordsTotal = PoiImport.getRowCount(feedSrcId, state);
    dtv.recordsFiltered = PoiImport.getRowCountFiltered(feedSrcId, state, dtRequest.search.value);

    List<PoiImport> recs = PoiImport.getFilteredPage(feedSrcId,
                                                     state,
                                                     dtRequest.start,
                                                     dtRequest.length,
                                                     dtRequest.search.value);
    for (PoiImport pi : recs) {
      PoiFeedRow row = new PoiFeedRow();
      row.id = pi.getImportId();
      row.name = pi.getName();
      row.country = pi.getCountryName();
      row.city = pi.getCity();
      row.postalCode = pi.getPostalCode();
      row.importTime = Utils.formatDateTimePrint(pi.getImportTs());
      row.fileName = pi.getSrcFilename();

      row.actions.put("Manual Match", routes.PoiController.feedShowSuggestions(pi.getImportId()).url());
      switch(state) {
        case UNSURE:
          row.actions.put("Auto Match",    routes.PoiController.feedMoveOneAsNewFeedRecord(feedSrcId, feedState, pi.getImportId(), false).url());
          row.actions.put("Set As New POI",routes.PoiController.feedMoveOneAsNewFeedRecord(feedSrcId, feedState,pi.getImportId(), true).url());
          break;
        case MISMATCH:
          row.actions.put("Set as New POI", routes.PoiController.feedMoveOneAsNewFeedRecord(feedSrcId, feedState, pi.getImportId(), true).url());
          break;
      }
      row.actions.put("Delete", routes.PoiController.feedDeleteOne(feedSrcId, feedState, pi.getImportId()).url());

      dtv.data.add(row);
    }

    return  ok(Json.toJson(dtv));
  }

  private static PoiFeedView buildPoiFeedView(int currSrc, int feedState) {
    PoiFeedView fv = new PoiFeedView();
    FeedSrc src = FeedSrc.find.byId(currSrc);
    fv.currSrc = src;
    fv.srcList = FeedSrc.findByType(FeedSrc.FeedType.POI);
    fv.currFeedState = PoiImportRS.ImportState.fromIdx(feedState);

    if (src.getLoaderClass() != null) {
      try {
        FeedHelperPOI helper = (FeedHelperPOI) Class.forName(src.getLoaderClass()).newInstance();
        fv.resources = helper.getResources();
      }
      catch (Exception e) {
        Log.err("Failure to instantiate POI helper class: " + src.getLoaderClass(), e);
        e.printStackTrace();
      }
    }
    return fv;
  }

  /**
   * As variety of file feeds increases - modify this function.
   *
   * @param srcId
   * @param fileName
   * @return
   */
  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedImportFile(final int srcId, final String fileName) {
    Map<PoiFeedUtil.FeedResult, Integer> results = null;
    FeedSrc src = FeedSrc.find.byId(srcId);
    if (src.getLoaderClass() != null) {
      try {
        FeedHelperPOI helper = (FeedHelperPOI) Class.forName(src.getLoaderClass()).newInstance();
        results = helper.load(fileName);
      }
      catch (Exception e) {
        Log.err("Failure to instantiate POI helper class:" + src.getLoaderClass(), e);
        e.printStackTrace();
      }
    }

    if (results == null) {
      results = new HashMap<>();
      results.put(PoiFeedUtil.FeedResult.SUCCESS, 0);
      results.put(PoiFeedUtil.FeedResult.ERROR, 0);
    }
    return ok(Json.toJson(results));
  }

  /**
   * Returns current feed activity state
   * @param srcId feed id
   * @return
   */
  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedState(final int srcId) {
    PoiFeedStateCO state = (PoiFeedStateCO) CacheMgr.get(APPConstants.CACHE_POI_FEED_STATE + srcId);

    if (state == null) {
      state = new PoiFeedStateCO();
    }

    return ok(Json.toJson(state));
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedImport(final int srcId) {
    Map<PoiFeedUtil.FeedResult, Integer> results = new HashMap<>();
    if (srcId == FeedSourcesInfo.byName("Ice Portal")) { //TODO: Not sure how to abstract this at this point
      final IcePortalActor.IceCommand cmd = IcePortalActor.IceCommand.GET_ALL;
      ActorsHelper.tell(SupervisorActor.UmappedActor.ICE_PORTAL, cmd);
      results.put(PoiFeedUtil.FeedResult.SUCCESS, 1);
      results.put(PoiFeedUtil.FeedResult.ERROR, 0);
    }
    else {
      results.put(PoiFeedUtil.FeedResult.SUCCESS, 0);
      results.put(PoiFeedUtil.FeedResult.ERROR, 1);
    }

    return ok(Json.toJson(results));
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedUpdate(final int srcId) {
    Map<PoiFeedUtil.FeedResult, Integer> results = new HashMap<>();
    if (srcId == FeedSourcesInfo.byName("Ice Portal")) { //TODO: Not sure how to abstract this at this point
      final IcePortalActor.IceCommand cmd = IcePortalActor.IceCommand.GET_UPDATED;
      ActorsHelper.tell(SupervisorActor.UmappedActor.ICE_PORTAL, cmd);

      results.put(PoiFeedUtil.FeedResult.SUCCESS, 1);
      results.put(PoiFeedUtil.FeedResult.ERROR, 0);
    }
    else {
      results.put(PoiFeedUtil.FeedResult.SUCCESS, 0);
      results.put(PoiFeedUtil.FeedResult.ERROR, 1);
    }

    return ok(Json.toJson(results));
  }



  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedAssociateData(final int srcId, final int poiType) {

    try {
      PoiTypeInfo.PoiType  pt      = PoiTypeInfo.Instance().byId(poiType);
      FeedSrc              feedSrc = FeedSrc.find.byId(srcId);
      FeedHelperPOI        helper  = (FeedHelperPOI) Class.forName(feedSrc.getLoaderClass()).newInstance();

      int matchedReference = PoiImportMgr.associateMatchedSrcReference(srcId);

      Log.debug("Matched " + matchedReference + " records with POI with same src_reference");

      Map<String, Integer> results = PoiImportMgr.associateImportFeed(srcId, pt.getId(), helper);
      return ok(Json.toJson(results));
    }
    catch (InstantiationException e) {
      Log.err("PoiController::feedAssociateData: Instance Error: ", e);
     }
    catch (IllegalAccessException e) {
      Log.err("PoiController::feedAssociateData: Access Error: ", e);

    }
    catch (ClassNotFoundException e) {
      Log.err("PoiController::feedAssociateData: Class Error: ", e);
    }
    return internalServerError();
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedClearAmenities(final int srcId) {
    Map<String, Integer> results = new HashMap<>();
    int                  result  = PoiImportMgr.clearAmenities(srcId);
    results.put("CLEARED", result);
    return ok(Json.toJson(results));

  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedPurgeData(final int srcId) {
    Map<String, Integer> results = new HashMap<>();
    int                  result  = PoiImportMgr.purge(srcId);
    results.put("SUCCESS", result);
    results.put("ERROR", 0);
    return ok(Json.toJson(results));

  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedMoveMatched(final int srcId) {
    Map<String, Integer> results = new HashMap<>();
    int                  result  = PoiImportMgr.insertMatched(srcId);
    results.put("INSERTS", result);
    result = PoiImportMgr.updateModified(srcId);
    results.put("UPDATED", result);
    result = PoiImportMgr.purge(srcId); //This completes "move" without it it is just "copy"
    results.put("PURGED", result);
    return ok(Json.toJson(results));
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedSyncPoi(final int srcId) {
    try {
      Map<PoiSrcFeed.FeedSyncResult, Integer> results;
      FeedSrc                                 feedSrc = FeedSrc.find.byId(srcId);
      FeedHelperPOI                           helper  = (FeedHelperPOI) Class.forName(feedSrc.getLoaderClass())
                                                                             .newInstance();
      //results = PoiSrcFeed.syncEachWithPoi(srcId, helper);
      results = PoiImportMgr.syncMatchedWithPoi(srcId, helper);
      return ok(Json.toJson(results));
    }
    catch (Exception e) {
      Log.err("PoiController::feedSyncPoi: Instance Error: ", e);
    }
    return internalServerError();
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedDeleteOrphans(final int srcId, final String deleteMode) {
    final SessionMgr                        sessionMgr = SessionMgr.fromContext(ctx());
    PoiMgr.DeleteMode                       dm         = PoiMgr.DeleteMode.valueOf(deleteMode);
    Map<PoiSrcFeed.FeedSyncResult, Integer> results    = new HashMap<>();
    int                                     deleted    = PoiMgr.deleteOrphanPoi(srcId, dm, sessionMgr.getUserId());
    results.put(PoiSrcFeed.FeedSyncResult.DELETED, deleted);
    return ok(Json.toJson(results));
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedDeleteDuplicates(Integer feedSrcId, Integer feedState) {
    Map<String, Integer> results = new HashMap<>();
    int result = PoiImportMgr.deleteDuplicates(feedSrcId);
    results.put("SUCCESS", result);
    results.put("ERROR", 0);
    return ok(Json.toJson(results));
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedWipeAll(Integer feedSrcId) {
    Map<String, Integer> results = new HashMap<>();
    int result = PoiImportMgr.wipeAll(feedSrcId);
    results.put("DELETED", result);
    return ok(Json.toJson(results));
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedDeleteOne(Integer feedSrcId, Integer feedState, final long importFeedId) {
    PoiImportMgr.deleteOne(importFeedId);
    return redirect(routes.PoiController.poiFeed(feedSrcId, feedState));
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedDeleteAll(Integer feedSrcId, Integer feedState) {
    PoiImportRS.ImportState state = PoiImportRS.ImportState.fromIdx(feedState);
    PoiImportMgr.deleteAll(feedSrcId, state);
    return redirect(routes.PoiController.poiFeed(feedSrcId, feedState));
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedMoveOneAsNewFeedRecord(int feedSrcId, int stateId, Long importId, boolean useNewPoiId) {

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    PoiImportMgr.moveOneToFeed(importId, useNewPoiId,  sessionMgr.getUserId());
    return redirect(routes.PoiController.poiFeed(feedSrcId, stateId));
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedMoveAllAsNewFeedRecords(Integer feedSrcId, int stateId, boolean newPoiId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    PoiImportRS.ImportState state = PoiImportRS.ImportState.fromIdx(stateId);
    PoiImportMgr.moveAllToFeed(feedSrcId, state, newPoiId,  sessionMgr.getUserId());
    return redirect(routes.PoiController.poiFeed(feedSrcId, stateId));
  }

  private static class FeedSuggestions {
    public PoiImportRS feed;
    public List<PoiJson> pois;
    public String      newUrl;
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedShowSuggestions(final long importId) {
    try {
      PoiImportRS                importRS = PoiImportMgr.getOne(importId);
      FeedSrc                    feedSrc  = FeedSrc.find.byId(importRS.srcId);
      FeedHelperPOI              helper   = (FeedHelperPOI) Class.forName(feedSrc.getLoaderClass()).newInstance();
      List<Pair<PoiRS, Integer>> pois     = helper.getRankedPois(importRS, true);
      List<PoiJson>              json     = new ArrayList<>();

      for (Pair<PoiRS, Integer> p : pois) {
        p.getLeft().data.prepareForBrowser();
        p.getLeft().data.setCountryCode(p.getLeft().countryCode);
        p.getLeft().data.setAdditionalProperty("score", p.getRight().toString());
        json.add(p.getLeft().data);
      }

      FeedSuggestions fs = new FeedSuggestions();
      fs.feed = importRS;
      fs.pois = json;
      fs.newUrl = routes.PoiController.feedMoveOneAsNewFeedRecord(feedSrc.getSrcId(),
                                                                  importRS.state.ordinal(),
                                                                  importId,
                                                                  true).url();

      return ok(Json.toJson(fs));
    } catch (Exception e) {
      Log.err("PoiController::feedShowSuggestions: Instance Error: ", e);
    }
    return internalServerError();
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result feedLinkToPoi(final long importId, final long poiId) {
    PoiImportRS             importRS  = PoiImportMgr.getOne(importId);
    PoiImportRS.ImportState prevState = importRS.state;
    importRS.poiId = poiId;
    importRS.state = PoiImportRS.ImportState.MATCH;
    PoiImportMgr.update(importRS);
    return redirect(routes.PoiController.poiFeed(importRS.srcId, prevState.ordinal()));
  }

  /**
   * Perform operation on the request (Operations Router)
   */
  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result iceExecute() {
    StopWatch sw = new StopWatch();
    sw.start();
    RequestMessageJson  iceRequest  = RequestMessageJson.fromJson(request().body().asJson());
    ResponseMessageJson iceResponse = null;
    if (iceRequest == null) {
      iceResponse = new ResponseMessageJson("ITINERARY", ReturnCode.HTTP_REQ_WRONG_DATA);
      return ok(iceResponse.toJson());
    }

    IcePhotosJson icePhotosJson = (IcePhotosJson) iceRequest.body;
    Log.debug("ICEPHOTO REQ: " + request().body().asJson().toString());
    switch (icePhotosJson.operation) {
      case UPLOAD:
        iceResponse = new ResponseMessageJson(iceRequest.header, ReturnCode.NOT_SUPPORTED);
        break;
      case ADD:
        iceResponse = iceAddResponse(iceRequest);
        break;
      case COPY:
        iceResponse = new ResponseMessageJson(iceRequest.header, ReturnCode.NOT_SUPPORTED);
        break;
      case UPDATE:
        iceResponse = new ResponseMessageJson(iceRequest.header, ReturnCode.NOT_SUPPORTED);
        break;
      case DELETE:
        iceResponse = new ResponseMessageJson(iceRequest.header, ReturnCode.NOT_SUPPORTED);
        break;
      case LIST:
        iceResponse = iceListResponse(iceRequest);
        break;
      case SEARCH:
        iceResponse = iceSearchResponse(iceRequest);
        break;
    }

    sw.stop();
    if (iceResponse.header.getRespCode() != 0) {
      Log.debug("ICEPHOTO: ERROR: Resp in " + sw.getTime() + "ms :" + iceResponse.toJson());
    }
    else {
      Log.debug("ICEPHOTO: Resp in " + sw.getTime() + "ms");
    }
    return ok(iceResponse.toJson());
  }

  /**
   * Perform operation on the request (Operations Router)
   */
  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result travelboundExecute() {
    StopWatch sw = new StopWatch();
    sw.start();
    RequestMessageJson  travelboundRequest  = RequestMessageJson.fromJson(request().body().asJson());
    ResponseMessageJson travelboundResponse = null;
    if (travelboundRequest == null) {
      travelboundResponse = new ResponseMessageJson("ITINERARY", ReturnCode.HTTP_REQ_WRONG_DATA);
      return ok(travelboundResponse.toJson());
    }

    TravelboundPhotosJson travelboundPhotosJson = (TravelboundPhotosJson) travelboundRequest.body;
    Log.debug("TRAVELBOUND PHOTO REQ: " + request().body().asJson().toString());
    switch (travelboundPhotosJson.operation) {
      case UPLOAD:
        travelboundResponse = new ResponseMessageJson(travelboundRequest.header, ReturnCode.NOT_SUPPORTED);
        break;
      case ADD:
        travelboundResponse = travelboundAddResponse(travelboundRequest);
        break;
      case COPY:
        travelboundResponse = new ResponseMessageJson(travelboundRequest.header, ReturnCode.NOT_SUPPORTED);
        break;
      case UPDATE:
        travelboundResponse = new ResponseMessageJson(travelboundRequest.header, ReturnCode.NOT_SUPPORTED);
        break;
      case DELETE:
        travelboundResponse = new ResponseMessageJson(travelboundRequest.header, ReturnCode.NOT_SUPPORTED);
        break;
      case LIST:
        travelboundResponse = new ResponseMessageJson(travelboundRequest.header, ReturnCode.NOT_SUPPORTED);
        break;
      case SEARCH:
        travelboundResponse = travelboundSearchResponse(travelboundRequest);
        break;
    }

    sw.stop();
    if (travelboundResponse.header.getRespCode() != 0) {
      Log.debug("TRAVELBOUNDPHOTO: ERROR: Resp in " + sw.getTime() + "ms :" + travelboundResponse.toJson());
    }
    else {
      Log.debug("TRAVELBOUNDPHOTO: Resp in " + sw.getTime() + "ms");
    }
    return ok(travelboundResponse.toJson());
  }

  private ResponseMessageJson travelboundSearchResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = new SessionMgr(session());
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);

    TravelboundPhotosJson travelboundPhotosJson = new TravelboundPhotosJson();
    travelboundPhotosJson.operation = Operation.SEARCH;
    responseMessageJson.body = travelboundPhotosJson;

    TravelboundPhotosJson travelboundRequestJson = (TravelboundPhotosJson) req.body;
    travelboundPhotosJson.searchId = travelboundRequestJson.searchId;
    travelboundPhotosJson.cmpyId = travelboundRequestJson.cmpyId;

    if (travelboundRequestJson.searchId == null || travelboundRequestJson.searchId.trim().length() == 0) {
      Log.err("TRAVELBOUNDPHOTO: Request with corrupted Long POI ID - Null or Empty");
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    if (travelboundRequestJson.cmpyId == null || travelboundRequestJson.cmpyId < 0) {
      Log.err("TRAVELBOUNDPHOTO: Request with corrupted Cmpy ID - Null");
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    long poiId = 0;

    try {
      poiId = Long.parseLong(travelboundRequestJson.searchId);
    }
    catch (NumberFormatException e) {
      Log.err("TRAVELBOUNDPHOTO: Request with corrupted Long POI ID:" + travelboundRequestJson.searchId);
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    //1. Check if POI has Ice Portal record and build images response
    PoiSrcFeed psf = PoiSrcFeed.findUniqueById(poiId, FeedSourcesInfo.byName("Travelbound Hotel"));
    if (psf != null) {
      travelboundBuildPhotos(psf, travelboundPhotosJson, null);
    }

    sw.stop();
    Log.debug("TRAVELBOUNDPHOTO: Response prepared in:" + sw.getTime() + "ms");
    responseMessageJson.header.withCode(ReturnCode.SUCCESS);
    return responseMessageJson;
  }

  static void travelboundBuildPhotos(PoiSrcFeed psf, PoiPhotosJson travelboundPhotosJson, Long importId) {
    List<PoiPhoto> photos = travelboundItemMapper.getHotelPhotos(psf.getRaw());
    for (PoiPhoto p : photos) {
      travelboundPhotosJson.addPhoto(p.thumb, p.url, p.caption, p.fileName, importId);
    }
  }

  private  ResponseMessageJson travelboundAddResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();

    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);

    TravelboundPhotosJson travelboundPhotosJson = new TravelboundPhotosJson();
    travelboundPhotosJson.operation = Operation.ADD;
    responseMessageJson.body = travelboundPhotosJson;

    TravelboundPhotosJson travelboundRequestJson = (TravelboundPhotosJson) req.body;
    travelboundPhotosJson.searchId = travelboundRequestJson.searchId;
    travelboundPhotosJson.cmpyId = travelboundRequestJson.cmpyId;
    responseMessageJson = poiPhotoAddResponse(req, responseMessageJson, travelboundRequestJson,
                                              PoiController :: travelboundBuildPhotos,
                                              "Travelbound Hotel", FileSrc.FileSrcType.IMG_VENDOR_TRAVELBOUND);
    sw.stop();
    Log.debug("TRAVELBOUND: Response prepared in:" + sw.getTime() + "ms");
    return responseMessageJson;
  }

  private  ResponseMessageJson poiPhotoAddResponse(RequestMessageJson req, ResponseMessageJson responseMessageJson,
                                                   PoiPhotosJson requestPoiPhotosJson, PoiPhotosBuilder photosBuilder,
                                                   String feedSourceName, FileSrc.FileSrcType imgFileSourceType) {

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Account account;
    if(sessionMgr.getAccount().isPresent()){
      account = sessionMgr.getAccount().get();
    } else {
      account = Account.find.byId(sessionMgr.getAccountId());
    }

    if (requestPoiPhotosJson.searchId == null || requestPoiPhotosJson.searchId.trim().length() == 0) {
      Log.err("TRAVELBOUNDPHOTO: Request with corrupted Long POI ID - Null or Empty");
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    if (requestPoiPhotosJson.cmpyId == null || requestPoiPhotosJson.cmpyId < 0) {
      Log.err("TRAVELBOUNDPHOTO: Request with corrupted Cmpy ID - Null");
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    if (requestPoiPhotosJson.photos == null || requestPoiPhotosJson.photos.size() == 0) {
      Log.err("TRAVELBOUNDPHOTO: Request with photos to add missing");
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    int srcId = FeedSourcesInfo.byName(feedSourceName);
    long poiId = 0;
    try {
      poiId = Long.parseLong(requestPoiPhotosJson.searchId);
    }
    catch (NumberFormatException e) {
      Log.err("TRAVELBOUNDPHOTO: Request with corrupted Long POI ID:" + requestPoiPhotosJson.searchId);
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    PoiPhoto photo = requestPoiPhotosJson.photos.get(0);

    //Implicitly linking Feed Record to POI record
    PoiSrcFeed psf;
    if (photo.importId != null) {
      FeedHelperIcePortal helper = new FeedHelperIcePortal();
      psf = PoiImportMgr.moveOneToFeed(photo.importId, poiId, sessionMgr.getUserId());
      PoiSrcFeed.FeedSyncResult currRes = helper.synchronize(psf);

      if (psf == null || currRes == PoiSrcFeed.FeedSyncResult.FAIL) {
        Log.err("TRAVELBOUNDPHOTO: Error while trying to link imported record to poi. ID" + photo.importId);
        responseMessageJson.header.withCode(ReturnCode.LOGICAL_ERROR);
        return responseMessageJson;
      }
    } else {
      psf = PoiSrcFeed.findUniqueById(poiId, srcId);
    }

    PoiPhotosJson responsePoiPhotosJson = (PoiPhotosJson) responseMessageJson.body;
    photosBuilder.buildPhotos(psf, responsePoiPhotosJson, null);

    responsePoiPhotosJson.removePhoto(photo.url);

    PoiRS prs = null;
    List<PoiRS> parts = PoiMgr.findAllById(poiId);
    for (PoiRS p: parts) {
      if (p.getSrcId() == srcId) {
        prs = p;
      }
    }

    if (prs == null) {
      Log.err("POIPHOTO: Can't find POI record for POI ID:" + poiId);
      responseMessageJson.header.withCode(ReturnCode.DB_RECORD_NOT_FOUND);
      return responseMessageJson;
    }

    FileImage image = ImageController.downloadAndSaveImage(photo.url,
                                                           photo.fileName,
                                                           imgFileSourceType,
                                                           null,
                                                           account,
                                                           false);

    if(image == null) {
      Log.err("POIPHOTO: Failed to load photo to S3: " + photo.url);
      responseMessageJson.header.withCode(ReturnCode.STORAGE_S3_UPLOAD);
      return responseMessageJson;
    }

    Ebean.beginTransaction();
    PoiFile pf = null;
    // Find if file was previously deleted or person is trying to add the same file twice
    List<PoiFile> currFiles = PoiFile.getAllForPoiSrcOrCmpy(poiId, srcId, requestPoiPhotosJson.cmpyId);
    for (PoiFile cpf : currFiles) {
      if (cpf.getFileImage() != null && cpf.getFileImage().getPk().equals(image.getPk())) {
        if (cpf.getCmpyId() != Company.PUBLIC_COMPANY_ID &&
            cpf.getState() == RecordState.DELETED) {
          cpf.delete(); //Perm deleting otherwise there will be duplicates
        } else {
          pf = cpf;
        }
      }
    }

    if (pf != null) { //Previously file existed
      pf.setState(RecordState.ACTIVE);
      pf.setDescription(photo.caption);
      pf.updateModInfo(sessionMgr.getUserId());
      pf.update();
    }
    else { //New record for this poi with this filename - Public from Travelbound
      pf = PoiFile.buildPoiFile(poiId, Company.PUBLIC_COMPANY_ID, photo.fileName, photo.caption, sessionMgr.getUserId());
      pf.setFileImage(image);
      pf.setSrcId(srcId);
      pf.setNameSys(S3Util.normalizeFilename(image.getFile().getFilename()));
      pf.setUrl(image.getUrl());

      LstFileType ft = LstFileType.findFromFileName(photo.fileName);
      if (ft.getExtension().equals("uki")) {
        Log.err("Failed to determine file type from filename :" + photo.fileName);
      }
      pf.setExtension(ft);
      pf.save();
    }

    incrementFileCount(prs, sessionMgr.getUserId()); //Updating Common Ice Portal record

    //Updating all file counts
    poiUpdateFileCounts(prs.getId());

    Ebean.commitTransaction();

    responseMessageJson.header.withCode(ReturnCode.SUCCESS);

    return responseMessageJson;
  }


  /**
   * Linking feed records and poi
   * @param req
   * @return
   */
  private static ResponseMessageJson iceListResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = new SessionMgr(session());
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);

    IcePhotosJson icePhotosJson = new IcePhotosJson();
    icePhotosJson.operation = Operation.LIST;
    responseMessageJson.body = icePhotosJson;

    IcePhotosJson iceRequestJson = (IcePhotosJson) req.body;
    icePhotosJson.searchId = iceRequestJson.searchId;
    icePhotosJson.cmpyId = iceRequestJson.cmpyId;

    if (iceRequestJson.searchId == null || iceRequestJson.searchId.trim().length() == 0) {
      Log.err("ICEPHOTO: Request with corrupted Long POI ID - Null or Empty");
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    if (iceRequestJson.suggestions == null || iceRequestJson.suggestions.size() != 1) {
      Log.err("ICEPHOTO: Request with record linking information missing");
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    IcePhotosJson.Suggestion suggestion = iceRequestJson.suggestions.get(0);

    long poiId = 0;
    long importId = 0;
    try {
      poiId = Long.parseLong(suggestion.poiId);
      importId = Long.parseLong(suggestion.id);
    }
    catch (NumberFormatException e) {
      Log.err("ICEPHOTO: Request with corrupted Suggestion");
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    PoiSrcFeed psf = PoiImportMgr.buildPoiSourceFeed(importId, poiId, sessionMgr.getUserId());

    if (psf != null) {
      iceBuildPhotos(psf, icePhotosJson, importId);
    } else {
      Log.err("ICEPHOTO: Failed to find record after linking");
      responseMessageJson.header.withCode(ReturnCode.LOGICAL_ERROR);
      return responseMessageJson;
    }

    sw.stop();
    Log.debug("ICEPHOTO: Response prepared in:" + sw.getTime() + "ms");
    responseMessageJson.header.withCode(ReturnCode.SUCCESS);
    return responseMessageJson;
  }

  /**
   * List files for a specific POI
   * @param req
   * @return
   */
  private ResponseMessageJson iceAddResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();

    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);

    IcePhotosJson icePhotosJson = new IcePhotosJson();
    icePhotosJson.operation = Operation.ADD;
    responseMessageJson.body = icePhotosJson;

    IcePhotosJson iceRequestJson = (IcePhotosJson) req.body;
    icePhotosJson.searchId = iceRequestJson.searchId;
    icePhotosJson.cmpyId = iceRequestJson.cmpyId;

    responseMessageJson = poiPhotoAddResponse(req, responseMessageJson, iceRequestJson, PoiController::iceBuildPhotos,
                                              "Ice Portal", FileSrc.FileSrcType.IMG_VENDOR_ICE_PORTAL);

    sw.stop();
    Log.debug("ICEPHOTO: Response prepared in:" + sw.getTime() + "ms");
    return responseMessageJson;
  }

  /**
   * List files for a specific POI
   * @param req
   * @return
   */
  private static ResponseMessageJson iceSearchResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = new SessionMgr(session());
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);

    IcePhotosJson icePhotosJson = new IcePhotosJson();
    icePhotosJson.operation = Operation.SEARCH;
    responseMessageJson.body = icePhotosJson;

    IcePhotosJson iceRequestJson = (IcePhotosJson) req.body;
    icePhotosJson.searchId = iceRequestJson.searchId;
    icePhotosJson.cmpyId = iceRequestJson.cmpyId;

    if (iceRequestJson.searchId == null || iceRequestJson.searchId.trim().length() == 0) {
      Log.err("ICEPHOTO: Request with corrupted Long POI ID - Null or Empty");
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    if (iceRequestJson.cmpyId == null || iceRequestJson.cmpyId < 0) {
      Log.err("ICEPHOTO: Request with corrupted Cmpy ID - Null");
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    long poiId = 0;

    try {
      poiId = Long.parseLong(iceRequestJson.searchId);
    }
    catch (NumberFormatException e) {
      Log.err("ICEPHOTO: Request with corrupted Long POI ID:" + iceRequestJson.searchId);
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    //1. Check if POI has Ice Portal record and build images response
    PoiSrcFeed psf = PoiSrcFeed.findUniqueById(poiId, FeedSourcesInfo.byName("Ice Portal"));
    if (psf != null) {
      iceBuildPhotos(psf, icePhotosJson, null);
    }
    //2. If no association, find list of suggestions for the POI
    else {
      FeedHelperIcePortal helper = new FeedHelperIcePortal();
      List<Pair<PoiImportRS, Integer>> suggestions = helper.getFeedSuggestions(poiId, iceRequestJson.cmpyId);

      for(Pair<PoiImportRS, Integer> item : suggestions) {
        PoiImportRS importRS = item.getLeft();
        //If no photos we will not show suggestion not to confuse
        if (!iceBrochureHasPhotos(importRS)) {
          continue;
        }
        IcePhotosJson.Suggestion suggestion = new IcePhotosJson.Suggestion();
        suggestion.address = importRS.address;
        suggestion.name = importRS.name;
        suggestion.id = Long.toString(importRS.importId);
        suggestion.countryCode = importRS.countryCode;
        suggestion.region = importRS.region;
        suggestion.locality = importRS.city;
        suggestion.postalCode = importRS.postalCode;
        suggestion.phone = importRS.phone;
        suggestion.score = item.getRight();

        icePhotosJson.suggestions.add(suggestion);
      }
    }

    sw.stop();
    Log.debug("ICEPHOTO: Response prepared in:" + sw.getTime() + "ms");
    responseMessageJson.header.withCode(ReturnCode.SUCCESS);
    return responseMessageJson;
  }

  private static boolean iceBrochureHasPhotos(PoiImportRS importRS) {
    Brochure brochure = FeedHelperIcePortal.getBrochureResponse(importRS.raw);

    if (brochure.getContent() == null || brochure.getContent().getPictures() == null ||
        brochure.getContent().getPictures().getImages() == null) {
      Log.debug("ICEPHOTO: INCOMPLETE. Brochure ICE " + importRS.srcReference + " needs to be updated");
      return false;
    }
    try {
      List<BrochureImage> photos = brochure.getContent().getPictures().getImages().getBrochureImage();
      if (photos.size() > 0) {
        return true;
      }
    } catch (Exception e) {
      //Ignoring it
    }
    return false;
  }

  /**
   *
   * @param psf
   * @param icePhotosJson
   * @param importId (optional) can be null for already associated feed records
   */
  static void iceBuildPhotos(PoiSrcFeed psf, PoiPhotosJson icePhotosJson, Long importId) {
    Brochure brochure = FeedHelperIcePortal.getBrochureResponse(psf.getRaw());

    if (brochure.getContent() == null || brochure.getContent().getPictures() == null ||
        brochure.getContent().getPictures().getImages() == null) {
      Log.debug("ICEPHOTO: INCOMPLETE. Brochure ICE " + psf.getSrcReference() + " needs to be updated");
      return; //Incomplete brochure unfortunately
    }
    try {
      List<BrochureImage> photos = brochure.getContent().getPictures().getImages().getBrochureImage();
      List<Caption> captions = brochure.getContent().getPictures().getCaptions().getCaption();
      List<Category> categories = brochure.getContent().getPictures().getCategories().getCategory();

      for (int idx = 0; idx < photos.size(); idx++) {
        BrochureImage image = photos.get(idx);
        List<Item> longCaps = captions.get(idx).getLongCaption().getItem();
        Category cat = categories.get(idx);
        String caption = "";
        for (Item item : longCaps) {
          if (item.getLcid() == 1) {
            caption = item.getText();
          }
        }
        StringBuilder fileName = new StringBuilder();
        fileName.append("ice");
        fileName.append(brochure.getBrochureInfo().getIceID());
        fileName.append("_cat");
        fileName.append(cat.getMainCategory());
        fileName.append("_sub");
        fileName.append(cat.getSubCategory());
        fileName.append(".jpg");
        icePhotosJson.addPhoto(image.getThumbUrl(), image.getMediaLink(), caption, fileName.toString(), importId);
      }
    } catch (Exception e) {
      Log.err("Some part of the brochure ICE " + psf.getSrcReference() + " is not initialized. Update manually");
      e.printStackTrace();
    }
  }
}
