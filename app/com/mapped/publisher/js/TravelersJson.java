package com.mapped.publisher.js;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.parse.schemaorg.Person;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.mapped.publisher.view.TripBookingDetailView;
import com.umapped.api.schema.types.ReturnCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ryan on 29/12/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TravelersJson
    extends BaseJsonResponse
    implements Serializable{

    public List<Person> travelers;

    public TravelersJson(ReturnCode rc) {
        super(rc);
        travelers = new ArrayList<>();
    }
}
