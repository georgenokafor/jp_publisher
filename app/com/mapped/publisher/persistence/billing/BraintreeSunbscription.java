package com.mapped.publisher.persistence.billing;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.parse.schemaorg.DateTime;

import java.io.Serializable;

/**
 * Created by twong on 2016-10-18.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BraintreeSunbscription
    implements Serializable {
  public String custToken;
  public String subToken;
  public String planId;
  public DateTime createdTS;
  public boolean addon;
  public boolean active;
  public String braintreeStatus;
  public String lastUpdatedTS;

  public boolean isActive() {
    if (subToken != null && !subToken.isEmpty() && custToken != null && !custToken.isEmpty() && active) {
      return true;
    }
    return false;
  }
}
