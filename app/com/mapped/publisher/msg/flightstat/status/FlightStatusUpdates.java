
package com.mapped.publisher.msg.flightstat.status;

import java.util.List;

public class FlightStatusUpdates{
   	private List<FlightStatusUpdate> flightStatusUpdate;

 	public List<FlightStatusUpdate> getFlightStatusUpdate(){
		return this.flightStatusUpdate;
	}
	public void setFlightStatusUpdate(List<FlightStatusUpdate> flightStatusUpdate){
		this.flightStatusUpdate = flightStatusUpdate;
	}
}
