
package com.mapped.publisher.parse.BigFive.TourList;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}tour_name"/>
 *         &lt;element ref="{}tour_highlights"/>
 *         &lt;element ref="{}tour_itinerary"/>
 *         &lt;element ref="{}tour_length"/>
 *         &lt;element ref="{}tour_price"/>
 *         &lt;element ref="{}tour_rates"/>
 *         &lt;element ref="{}tour_short_description"/>
 *         &lt;element ref="{}tour_images"/>
 *         &lt;element ref="{}tour_pdf"/>
 *         &lt;element ref="{}tour_series"/>
 *         &lt;element ref="{}tour_countries"/>
 *         &lt;element ref="{}tour_interests"/>
 *         &lt;element ref="{}link"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tourName",
    "tourHighlights",
    "tourItinerary",
    "tourLength",
    "tourPrice",
    "tourRates",
    "tourShortDescription",
    "tourImages",
    "tourPdf",
    "tourSeries",
    "tourCountries",
    "tourInterests",
    "link"
})
@XmlRootElement(name = "tour")
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
public class Tour {

    @XmlElement(name = "tour_name", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    protected String tourName;
    @XmlElement(name = "tour_highlights", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    protected String tourHighlights;
    @XmlElement(name = "tour_itinerary", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    protected String tourItinerary;
    @XmlElement(name = "tour_length", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    protected String tourLength;
    @XmlElement(name = "tour_price", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    protected String tourPrice;
    @XmlElement(name = "tour_rates", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    protected String tourRates;
    @XmlElement(name = "tour_short_description", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    protected String tourShortDescription;
    @XmlElement(name = "tour_images", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    protected TourImages tourImages;
    @XmlElement(name = "tour_pdf", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    protected String tourPdf;
    @XmlElement(name = "tour_series", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    protected TourSeries tourSeries;
    @XmlElement(name = "tour_countries", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    protected TourCountries tourCountries;
    @XmlElement(name = "tour_interests", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    protected TourInterests tourInterests;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    protected String link;

    /**
     * Gets the value of the tourName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public String getTourName() {
        return tourName;
    }

    /**
     * Sets the value of the tourName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public void setTourName(String value) {
        this.tourName = value;
    }

    /**
     * Gets the value of the tourHighlights property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public String getTourHighlights() {
        return tourHighlights;
    }

    /**
     * Sets the value of the tourHighlights property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public void setTourHighlights(String value) {
        this.tourHighlights = value;
    }

    /**
     * Gets the value of the tourItinerary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public String getTourItinerary() {
        return tourItinerary;
    }

    /**
     * Sets the value of the tourItinerary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public void setTourItinerary(String value) {
        this.tourItinerary = value;
    }

    /**
     * Gets the value of the tourLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public String getTourLength() {
        return tourLength;
    }

    /**
     * Sets the value of the tourLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public void setTourLength(String value) {
        this.tourLength = value;
    }

    /**
     * Gets the value of the tourPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public String getTourPrice() {
        return tourPrice;
    }

    /**
     * Sets the value of the tourPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public void setTourPrice(String value) {
        this.tourPrice = value;
    }

    /**
     * Gets the value of the tourRates property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public String getTourRates() {
        return tourRates;
    }

    /**
     * Sets the value of the tourRates property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public void setTourRates(String value) {
        this.tourRates = value;
    }

    /**
     * Gets the value of the tourShortDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public String getTourShortDescription() {
        return tourShortDescription;
    }

    /**
     * Sets the value of the tourShortDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public void setTourShortDescription(String value) {
        this.tourShortDescription = value;
    }

    /**
     * Gets the value of the tourImages property.
     * 
     * @return
     *     possible object is
     *     {@link TourImages }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public TourImages getTourImages() {
        return tourImages;
    }

    /**
     * Sets the value of the tourImages property.
     * 
     * @param value
     *     allowed object is
     *     {@link TourImages }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public void setTourImages(TourImages value) {
        this.tourImages = value;
    }

    /**
     * Gets the value of the tourPdf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public String getTourPdf() {
        return tourPdf;
    }

    /**
     * Sets the value of the tourPdf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public void setTourPdf(String value) {
        this.tourPdf = value;
    }

    /**
     * Gets the value of the tourSeries property.
     * 
     * @return
     *     possible object is
     *     {@link TourSeries }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public TourSeries getTourSeries() {
        return tourSeries;
    }

    /**
     * Sets the value of the tourSeries property.
     * 
     * @param value
     *     allowed object is
     *     {@link TourSeries }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public void setTourSeries(TourSeries value) {
        this.tourSeries = value;
    }

    /**
     * Gets the value of the tourCountries property.
     * 
     * @return
     *     possible object is
     *     {@link TourCountries }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public TourCountries getTourCountries() {
        return tourCountries;
    }

    /**
     * Sets the value of the tourCountries property.
     * 
     * @param value
     *     allowed object is
     *     {@link TourCountries }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public void setTourCountries(TourCountries value) {
        this.tourCountries = value;
    }

    /**
     * Gets the value of the tourInterests property.
     * 
     * @return
     *     possible object is
     *     {@link TourInterests }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public TourInterests getTourInterests() {
        return tourInterests;
    }

    /**
     * Sets the value of the tourInterests property.
     * 
     * @param value
     *     allowed object is
     *     {@link TourInterests }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public void setTourInterests(TourInterests value) {
        this.tourInterests = value;
    }

    /**
     * Gets the value of the link property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public String getLink() {
        return link;
    }

    /**
     * Sets the value of the link property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-28T03:57:15-04:00", comments = "JAXB RI v2.2.4-2")
    public void setLink(String value) {
        this.link = value;
    }

}
