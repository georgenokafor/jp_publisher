
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetTravelStyleKeywordsResult" type="{http://webapi.globusandcosmos.com/}ArrayOfString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTravelStyleKeywordsResult"
})
@XmlRootElement(name = "GetTravelStyleKeywordsResponse")
public class GetTravelStyleKeywordsResponse {

    @XmlElement(name = "GetTravelStyleKeywordsResult")
    protected ArrayOfString getTravelStyleKeywordsResult;

    /**
     * Gets the value of the getTravelStyleKeywordsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getGetTravelStyleKeywordsResult() {
        return getTravelStyleKeywordsResult;
    }

    /**
     * Sets the value of the getTravelStyleKeywordsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setGetTravelStyleKeywordsResult(ArrayOfString value) {
        this.getTravelStyleKeywordsResult = value;
    }

}
