package com.mapped.common;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2012-10-11
 * Time: 11:13 AM
 * To change this template use File | Settings | File Templates.
 */
public class CoreConstants {

  public final static String DB_URL       = "DB_URL";
  public final static String DB_DRIVER    = "DB_DRIVER";
  public final static String DB_USERID    = "DB_USERID";
  public final static String DB_PWD       = "DB_PWD";
  public final static String LOG_SETTINGS = "logSettings";
  public final static String LOG_LEVEL    = "logLevel";
  public final static String APP_NAME     = "APP_NAME";
  public final static String ENVIRONMENT  = "ENV";
  public final static String ENV_SUFFIX   = "ENV_SUFFIX";
  public final static String DEV          = "DEV";
  public final static String PROD         = "PROD";

  public final static String APPLE_CERT             = "APPLE_CERT";
  public final static String APPLE_CERT_PWD         = "APPLE_CERT_PWD";
  public final static String APPLE_PUSH_PROCESSOR   = "APPLE_NOTIFICATION";
  public final static String APPLE_NOTIFICATION_MSG = "APPLE_NOTIFICATION_MSG";

  public final static String EMAIL_SMTP_HOST      = "SMTP_HOST";
  public final static String EMAIL_SMTP_PORT      = "SMTP_PORT";
  public final static String EMAIL_SMTP_USER_ID   = "SMTP_USER_ID";
  public final static String EMAIL_SMTP_PASSWORD  = "SMTP_PASSWORD";
  public final static String SMS_CLICKATELL_APIID = "SMS_CLICKATELL_APIID";
  public final static String SMS_CLICKATELL_TOKEN = "SMS_CLICKATELL_TOKEN";
  public final static String SMS_CLICKATELL_MSISDN = "SMS_CLICKATELL_MSISDN";
  public final static String SMS_CALLBACK_USER    = "SMS_CALLBACK_USER";
  public final static String SMS_CALLBACK_PASS    = "SMS_CALLBACK_PASS";


  public final static String BATCH_PROCESSOR = "BATCH_PROCESSOR";

  public final static int CODE_OK                            = 0;
  public final static int CODE_SYSTEM_EXCEPTION              = -1;
  public final static int CODE_APPLE_PROCESSOR_QUEUECREATION = -1001;
  public final static int CODE_APPLE_PROCESSOR_PUSHERROR     = -1002;

  public final static String EMAIL_INVITE_PROCESSOR   = "EMAIL_INVITE";
  public final static String EMAIL_DIGEST_PROCESSOR   = "EMAIL_DIGEST";
  public final static String EMAIL_PASSWORD_PROCESSOR = "EMAIL_PASSWORD_RESET";
  public final static String TRIP_PUBLISHER           = "TRIP_PUBLISHER";


  public final static String EMAIL_DIGEST_FROM    = "EMAIL_DIGEST_FROM";
  public final static String EMAIL_DIGEST_SUBJECT = "EMAIL_DIGEST_SUBJECT";

  public final static String EMAIL_PWD_RESET_FROM              = "EMAIL_PWD_RESET_FROM";
  public final static String EMAIL_PWD_RESET_SUBJECT           = "EMAIL_PWD_RESET_SUBJECT";
  public final static String EMAIL_NEW_ACCOUNT_SUBJECT         = "EMAIL_NEW_ACCOUNT_SUBJECT";
  public final static String EMAIL_NEW_UMAPPED_ACCOUNT_SUBJECT = "EMAIL_NEW_UMAPPED_ACCOUNT_SUBJECT";
  public final static String EMAIL_MOBILE_PWD_RESET_FROM       = "EMAIL_MOBILE_PWD_RESET_FROM";
  public final static String EMAIL_MOBILE_PWD_RESET_SUBJECT    = "EMAIL_MOBILE_PWD_RESET_SUBJECT";


  public final static String EMAIL_COMMUNICATOR_HOST = "EMAIL_COMMUNICATOR_HOST";

  public final static String EMAIL_TRIP_NOTIFICATION_FROM    = "EMAIL_TRIP_NOTIFICATION_FROM";
  public final static String EMAIL_TRIP_NOTIFICATION_SUBJECT = "EMAIL_TRIP_NOTIFICATION_SUBJECT";
  public final static String EMAIL_NEW_CMPY_LINK_SUBJECT     = "EMAIL_NEW_CMPY_LINK_SUBJECT";
  public final static String EMAIL_DELETE_CMPY_LINK_SUBJECT  = "EMAIL_DELETE_CMPY_LINK_SUBJECT";
  public final static String HOST_URL                        = "HOST_URL";
  public final static String EMAIL_ENABLED                   = "EMAIL_ENABLED";


  public final static String EMAIL_INVITE_FROM    = "EMAIL_INVITE_FROM";
  public final static String EMAIL_INVITE_SUBJECT = "EMAIL_INVITE_SUBJECT";

  public final static String EMAIL_PASSWORD_FROM    = "EMAIL_PASSWORD_FROM";
  public final static String EMAIL_PASSWORD_SUBJECT = "EMAIL_PASSWORD_SUBJECT";

  public final static String TEXT = "TEXT";
  public final static String HTML = "HTML";

  public final static String VIDEO = "Video";
  public final static String AUDIO = "Audio";
  public final static String IMAGE = "Image";


  public final static String HOTEL_TYPE      = "Hotel";
  public final static String RESTAURANT_TYPE = "Restaurant";
  public final static String NIGHTLIFE_TYPE  = "Nightlife";
  public final static String SHOPPING_TYPE   = "Shopping";
  public final static String SEE_TYPE        = "Things To See";
  public final static String DO_TYPE         = "Things To Do";
  public final static String CULTURE_TYPE    = "Happening";
  public final static String OTHER_TYPE      = "Other";

  public final static String HOTEL_TYPE_ICON      = "r_hotel.jpg";
  public final static String RESTAURANT_TYPE_ICON = "r_restaurant.jpg";
  public final static String NIGHTLIFE_TYPE_ICON  = "r_nightlife.jpg";
  public final static String SHOPPING_TYPE_ICON   = "r_shopping.jpg";
  public final static String SEE_TYPE_ICON        = "r_thingstosee.jpg";
  public final static String DO_TYPE_ICON         = "r_thingstodo.jpg";
  public final static String CULTURE_TYPE_ICON    = "r_happening.jpg";
  public final static String OTHER_TYPE_ICON      = "r_other.jpg";


  public final static String JS_URL  = "JS_URL";
  public final static String CSS_URL = "CSS_URL";
  public final static String IMG_URL = "IMG_URL";
  public final static String LIB_URL = "LIB_URL";

  public final static String S3_BUCKET     = "S3_BUCKET";
  public final static String S3_ACCESS_KEY = "S3_ACCESS_KEY";
  public final static String S3_SECRET     = "S3_SECRET";
  public final static String S3_PDF_BUCKET = "S3_PDF_BUCKET";

  public final static String S3_IMG_CROP_BUCKET     = "S3_IMG_CROP_BUCKET";
  public final static String S3_IMG_CROP_BUCKET_URL     = "um-img";


  /**
   * Individual Freshbooks API URL
   */
  public final static String FRESHBOOKS_URL   = "FRESHBOOKS_URL";
  /**
   * Freshbooks Authentication token
   */
  public final static String FRESHBOOKS_TOKEN = "FRESHBOOKS_TOKEN";

  /**
   * An email address used to forward emails that are not recognized locally
   */
  public final static String WORLDMATE_ADDR = "WORLDMATE_ADDR";

  public final static String FIREBASE_URI      = "FIREBASE_URI";
  public final static String FIREBASE_SECRET   = "FIREBASE_SECRET";
  public final static String FIREBASE_PING_SEC = "FIREBASE_PING_SEC";

  public final static String SQS_Q_URL          = "SQS_Q_URL";
  public final static String MEMCACHED_SERVERS  = "MEMCACHIER_SERVERS";
  public final static String MEMCACHED_USER     = "MEMCACHIER_USERNAME";
  public final static String MEMCACHED_PASSWORD = "MEMCACHIER_PASSWORD";

  public final static String FIVE_FILTERS = "FIVE_FILTERS";

  public final static String REDIS_URL = "REDIS_URL";


  public final static String BRAINTREE_MERCHANT = "BRAINTREE_MERCHANT";
  public final static String BRAINTREE_PUBLIC = "BRAINTREE_PUBLIC";
  public final static String BRAINTREE_PRIVATE = "BRAINTREE_PRIVATE";
  public final static String BRAINTREE_USD = "BRAINTREE_USD";
  public final static String BRAINTREE_CAD = "BRAINTREE_CAD";
  public final static String BRAINTREE_AUD = "BRAINTREE_AUD";
  public final static String BRAINTREE_EUR = "BRAINTREE_EUR";

  public final static String TRAVELBOUND_URL_USA = "TRAVELBOUND_URL_USA";
  public final static String TRAVELBOUND_URL_CAN = "TRAVELBOUND_URL_CAN";
  public final static String TRAVELBOUND_UMAPPED_CREDENTIAL = "TRAVELBOUND_UMAPPED_CREDENTIAL";

  public final static String GOGO_URL = "GOGO_URL";
}
