package com.umapped.itinerary.analyze.geo;

import com.google.maps.GeoApiContext;

/**
 * Created by wei on 2017-02-23.
 */
public class GoogleMapApiSupport {
    private static String GEOCODING_API_KEY = "AIzaSyAY7RX8nj6vPo9YBtSAsKXSc9wBgUk8QXI";

    public static GeoApiContext getGeoCodingApiContext() {
        GeoApiContext context = new GeoApiContext();
        context.setApiKey(GEOCODING_API_KEY);
        return context;
    }
}
