package com.umapped.itinerary.analyze.segment.decisiontree;

import com.umapped.itinerary.analyze.segment.AnalyzerContext;

/**
 * Created by wei on 2017-03-09.
 */
public interface DecisionNode {
  Object getEvaluateResult(AnalyzerContext context);
  boolean isLeaf();
  DecisionNode next(AnalyzerContext context);
}
