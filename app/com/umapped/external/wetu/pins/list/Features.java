
package com.umapped.external.wetu.pins.list;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "star_authority",
    "stars",
    "rooms",
    "check_in_time",
    "check_out_time",
    "spoken_languages",
    "special_interests",
    "suggested_visitor_types",
    "property_facilities",
    "room_facilities",
    "available_services",
    "activities_on_site",
    "activities_off_site"
})
public class Features {

    @JsonProperty("star_authority")
    private String starAuthority;
    @JsonProperty("stars")
    private Integer stars;
    @JsonProperty("rooms")
    private Integer rooms;
    @JsonProperty("check_in_time")
    private String checkInTime;
    @JsonProperty("check_out_time")
    private String checkOutTime;
    @JsonProperty("spoken_languages")
    private List<String> spokenLanguages = new ArrayList<String>();
    @JsonProperty("special_interests")
    private List<String> specialInterests = new ArrayList<String>();
    @JsonProperty("suggested_visitor_types")
    private List<String> suggestedVisitorTypes = new ArrayList<String>();
    @JsonProperty("property_facilities")
    private List<String> propertyFacilities = new ArrayList<String>();
    @JsonProperty("room_facilities")
    private List<String> roomFacilities = new ArrayList<String>();
    @JsonProperty("available_services")
    private List<String> availableServices = new ArrayList<String>();
    @JsonProperty("activities_on_site")
    private List<String> activitiesOnSite = new ArrayList<String>();
    @JsonProperty("activities_off_site")
    private List<String> activitiesOffSite = new ArrayList<String>();

    /**
     * 
     * @return
     *     The starAuthority
     */
    @JsonProperty("star_authority")
    public String getStarAuthority() {
        return starAuthority;
    }

    /**
     * 
     * @param starAuthority
     *     The star_authority
     */
    @JsonProperty("star_authority")
    public void setStarAuthority(String starAuthority) {
        this.starAuthority = starAuthority;
    }

    /**
     * 
     * @return
     *     The stars
     */
    @JsonProperty("stars")
    public Integer getStars() {
        return stars;
    }

    /**
     * 
     * @param stars
     *     The stars
     */
    @JsonProperty("stars")
    public void setStars(Integer stars) {
        this.stars = stars;
    }

    /**
     * 
     * @return
     *     The rooms
     */
    @JsonProperty("rooms")
    public Integer getRooms() {
        return rooms;
    }

    /**
     * 
     * @param rooms
     *     The rooms
     */
    @JsonProperty("rooms")
    public void setRooms(Integer rooms) {
        this.rooms = rooms;
    }

    /**
     * 
     * @return
     *     The checkInTime
     */
    @JsonProperty("check_in_time")
    public String getCheckInTime() {
        return checkInTime;
    }

    /**
     * 
     * @param checkInTime
     *     The check_in_time
     */
    @JsonProperty("check_in_time")
    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    /**
     * 
     * @return
     *     The checkOutTime
     */
    @JsonProperty("check_out_time")
    public String getCheckOutTime() {
        return checkOutTime;
    }

    /**
     * 
     * @param checkOutTime
     *     The check_out_time
     */
    @JsonProperty("check_out_time")
    public void setCheckOutTime(String checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    /**
     * 
     * @return
     *     The spokenLanguages
     */
    @JsonProperty("spoken_languages")
    public List<String> getSpokenLanguages() {
        return spokenLanguages;
    }

    /**
     * 
     * @param spokenLanguages
     *     The spoken_languages
     */
    @JsonProperty("spoken_languages")
    public void setSpokenLanguages(List<String> spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
    }

    /**
     * 
     * @return
     *     The specialInterests
     */
    @JsonProperty("special_interests")
    public List<String> getSpecialInterests() {
        return specialInterests;
    }

    /**
     * 
     * @param specialInterests
     *     The special_interests
     */
    @JsonProperty("special_interests")
    public void setSpecialInterests(List<String> specialInterests) {
        this.specialInterests = specialInterests;
    }

    /**
     * 
     * @return
     *     The suggestedVisitorTypes
     */
    @JsonProperty("suggested_visitor_types")
    public List<String> getSuggestedVisitorTypes() {
        return suggestedVisitorTypes;
    }

    /**
     * 
     * @param suggestedVisitorTypes
     *     The suggested_visitor_types
     */
    @JsonProperty("suggested_visitor_types")
    public void setSuggestedVisitorTypes(List<String> suggestedVisitorTypes) {
        this.suggestedVisitorTypes = suggestedVisitorTypes;
    }

    /**
     * 
     * @return
     *     The propertyFacilities
     */
    @JsonProperty("property_facilities")
    public List<String> getPropertyFacilities() {
        return propertyFacilities;
    }

    /**
     * 
     * @param propertyFacilities
     *     The property_facilities
     */
    @JsonProperty("property_facilities")
    public void setPropertyFacilities(List<String> propertyFacilities) {
        this.propertyFacilities = propertyFacilities;
    }

    /**
     * 
     * @return
     *     The roomFacilities
     */
    @JsonProperty("room_facilities")
    public List<String> getRoomFacilities() {
        return roomFacilities;
    }

    /**
     * 
     * @param roomFacilities
     *     The room_facilities
     */
    @JsonProperty("room_facilities")
    public void setRoomFacilities(List<String> roomFacilities) {
        this.roomFacilities = roomFacilities;
    }

    /**
     * 
     * @return
     *     The availableServices
     */
    @JsonProperty("available_services")
    public List<String> getAvailableServices() {
        return availableServices;
    }

    /**
     * 
     * @param availableServices
     *     The available_services
     */
    @JsonProperty("available_services")
    public void setAvailableServices(List<String> availableServices) {
        this.availableServices = availableServices;
    }

    /**
     * 
     * @return
     *     The activitiesOnSite
     */
    @JsonProperty("activities_on_site")
    public List<String> getActivitiesOnSite() {
        return activitiesOnSite;
    }

    /**
     * 
     * @param activitiesOnSite
     *     The activities_on_site
     */
    @JsonProperty("activities_on_site")
    public void setActivitiesOnSite(List<String> activitiesOnSite) {
        this.activitiesOnSite = activitiesOnSite;
    }

    /**
     * 
     * @return
     *     The activitiesOffSite
     */
    @JsonProperty("activities_off_site")
    public List<String> getActivitiesOffSite() {
        return activitiesOffSite;
    }

    /**
     * 
     * @param activitiesOffSite
     *     The activities_off_site
     */
    @JsonProperty("activities_off_site")
    public void setActivitiesOffSite(List<String> activitiesOffSite) {
        this.activitiesOffSite = activitiesOffSite;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(starAuthority).append(stars).append(rooms).append(checkInTime).append(checkOutTime).append(spokenLanguages).append(specialInterests).append(suggestedVisitorTypes).append(propertyFacilities).append(roomFacilities).append(availableServices).append(activitiesOnSite).append(activitiesOffSite).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Features) == false) {
            return false;
        }
        Features rhs = ((Features) other);
        return new EqualsBuilder().append(starAuthority, rhs.starAuthority).append(stars, rhs.stars).append(rooms, rhs.rooms).append(checkInTime, rhs.checkInTime).append(checkOutTime, rhs.checkOutTime).append(spokenLanguages, rhs.spokenLanguages).append(specialInterests, rhs.specialInterests).append(suggestedVisitorTypes, rhs.suggestedVisitorTypes).append(propertyFacilities, rhs.propertyFacilities).append(roomFacilities, rhs.roomFacilities).append(availableServices, rhs.availableServices).append(activitiesOnSite, rhs.activitiesOnSite).append(activitiesOffSite, rhs.activitiesOffSite).isEquals();
    }

}
