package com.umapped.external.travelbound;

import com.mapped.publisher.utils.Utils;
import org.apache.commons.lang.StringUtils;

import java.sql.Timestamp;

/**
 * Created by wei on 2017-05-15.
 */
public class TravelboundBookingSearchResult {
  private Integer clientId;
  private String name;
  private Timestamp departureDate;
  private String bookingRefNumber;
  private String status;

  public TravelboundBookingSearchResult(Integer clientId, String name, Timestamp departureDate, String bookingRefNumber, String status) {
    this.clientId = clientId;
    this.name = name;
    this.departureDate = departureDate;
    this.bookingRefNumber = bookingRefNumber;
    this.status = status;
  }

  public Integer getClientId() {
    return clientId;
  }

  public String getName() {
    return name;
  }

  public Timestamp getDepartureDate() {
    return departureDate;
  }

  public String getDepartureDateStr() {
    if (departureDate != null) {
      return Utils.getDateStringPrint(departureDate.getTime());
    } else {
      return "";
    }
  }

  public String getBookingRefNumber() {
    return bookingRefNumber;
  }

  public String getStatus() {
    return status;
  }

  public boolean isValid() {
    return !StringUtils.equalsIgnoreCase("Cancelled", status);
  }
}
