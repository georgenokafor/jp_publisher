package models.publisher;

import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.Constraint;
import java.util.List;

/**
 * Created by twong on 15-10-15.
 */
@Entity @Table(name = "api_cmpy_token")

public class ApiCmpyToken extends Model {
  public static Model.Finder<Long, ApiCmpyToken> find = new Finder(ApiCmpyToken.class);

  private long expiryTs;
  private String tag;

  @Id
  private long pk;

  @Constraints.Required
  private String uCmpyId;
  private String token;
  private String tokenType;
  private int bkApiSrcId;
  private String srcCmpyId;
  private long createdTs;
  private long updatedTs;
  private String createdBy;
  private String modifiedBy;


  @Version
  private int version;

  public static List<ApiCmpyToken> findByCmpyId (String cmpyId) {
    return find.where().eq("u_cmpy_id", cmpyId).findList();
  }

  public static List<ApiCmpyToken> findByBkApiSrcId (int bkApiSrcId) {
    return find.where().eq("bk_api_src_id", bkApiSrcId).findList();
  }

  public static ApiCmpyToken findByToken (String token) {
    return find.where().eq("token", token).findUnique();
  }

  public static ApiCmpyToken findByTokenSrcId (int bkApiSrcId, String token) {
    return find.where().eq("token", token).eq("bk_api_src_id", bkApiSrcId).findUnique();
  }

  public static ApiCmpyToken findByCmpySrcId(String srcCmpyId, int bkApiSrcId) {
    return find.where().eq("src_cmpy_id", srcCmpyId).eq("bk_api_src_id", bkApiSrcId).findUnique();
  }

  public static boolean doesCmpyTokenExist (String cmpyId, int bkApiSrcId, String srcCmpyId) {
    //check to make sure there is no existing combination of srcCmpyId & bkApiSrcId in the system
    int srcCmpyIdCount = find.where().eq("src_cmpy_id", srcCmpyId).eq("bk_api_src_id",bkApiSrcId).findCount();
    if (srcCmpyIdCount > 0) {
      return true;
    }


/*
    int i =  find.where().eq("u_cmpy_id", cmpyId).eq("bk_api_src_id",bkApiSrcId).findCount();
    if (i >0) {
      return true;
    }
    */
    return false;
  }

  public static ApiCmpyToken findCmpyTokenByType (String cmpyId, int bkApiSrcId) {
    //check to make sure there is no existing combination of srcCmpyId & bkApiSrcId in the system
    return find.where().eq("u_cmpy_id", cmpyId).eq("bk_api_src_id", bkApiSrcId).findUnique();
  }

  public static List<ApiCmpyToken> findCmpyTokensByType (String cmpyId, int bkApiSrcId) {
    //check to make sure there is no existing combination of srcCmpyId & bkApiSrcId in the system
    return find.where().eq("u_cmpy_id", cmpyId).eq("bk_api_src_id", bkApiSrcId).orderBy("srcCmpyId asc").findList();
  }

  public static ApiCmpyToken findCmpyTokenByTypSrcCmpy (String cmpyId, int bkApiSrcId) {
    //check to make sure there is no existing combination of srcCmpyId & bkApiSrcId in the system
    return find.where().eq("src_cmpy_id", cmpyId).eq("bk_api_src_id", bkApiSrcId).findUnique();
  }

  public long getExpiryTs() {
    return expiryTs;
  }

  public void setExpiryTs(long expiryTs) {
    this.expiryTs = expiryTs;
  }

  public long getPk() {
    return pk;
  }

  public void setPk(long pk) {
    this.pk = pk;
  }

  public String getuCmpyId() {
    return uCmpyId;
  }

  public void setuCmpyId(String uCmpyId) {
    this.uCmpyId = uCmpyId;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getTokenType() {
    return tokenType;
  }

  public void setTokenType(String tokenType) {
    this.tokenType = tokenType;
  }

  public int getBkApiSrcId() {
    return bkApiSrcId;
  }

  public void setBkApiSrcId(int bkApiSrcId) {
    this.bkApiSrcId = bkApiSrcId;
  }

  public String getSrcCmpyId() {
    return srcCmpyId;
  }

  public void setSrcCmpyId(String srcCmpyId) {
    this.srcCmpyId = srcCmpyId;
  }

  public long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(long createdTs) {
    this.createdTs = createdTs;
  }

  public long getUpdatedTs() {
    return updatedTs;
  }

  public void setUpdatedTs(long updatedTs) {
    this.updatedTs = updatedTs;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
