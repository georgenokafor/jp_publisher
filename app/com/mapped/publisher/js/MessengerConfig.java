package com.mapped.publisher.js;

import com.fasterxml.jackson.annotation.JsonInclude;
import models.publisher.Account;

/**
 * Information required to sign-in into the Umapped Messenger
 * Created by surge on 2016-05-03.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessengerConfig {
  public String  token;
  public String  dbUrl;
  public String  uid;
  public String  postUrl;
  public String  name;
  public String  initials;
  public Boolean talkback;
  public String  aid;

  public MessengerConfig setAid(Long aid) {
    this.aid = Account.encodeId(aid);
    return this;
  }

  public MessengerConfig setInitials(String initials) {
    this.initials = initials;
    return this;
  }

  public MessengerConfig setTalkback(Boolean talkback) {
    this.talkback = talkback;
    return this;
  }

  public MessengerConfig setName(String name) {
    this.name = name;
    return this;
  }

  public MessengerConfig setPostUrl(String postUrl) {
    this.postUrl = postUrl;
    return this;
  }

  public MessengerConfig setUid(String uid) {
    this.uid = uid;
    return this;
  }

  public MessengerConfig setToken(String token) {
    this.token = token;
    return this;
  }

  public MessengerConfig setDbUrl(String dbUrl) {
    this.dbUrl = dbUrl;
    return this;
  }
}
