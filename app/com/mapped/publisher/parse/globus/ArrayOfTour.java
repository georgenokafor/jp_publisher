
package com.mapped.publisher.parse.globus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfTour complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ArrayOfTour">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Tour" type="{http://webapi.globusandcosmos.com/AvailableTours.xsd}Tour" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfTour", namespace = "http://webapi.globusandcosmos.com/AvailableTours.xsd", propOrder = {
    "tours"
})
public class ArrayOfTour {

    @XmlElement(name = "Tour", nillable = true)
    protected ArrayList<Tour> tours;

    /**
     * Gets the value of the tours property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tours property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTours().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tour }
     *
     *
     */
    public ArrayList<Tour> getTours() {
        if (tours == null) {
            tours = new ArrayList<Tour>();
        }
        return this.tours;
    }

}
