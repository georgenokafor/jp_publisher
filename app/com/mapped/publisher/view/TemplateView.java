package com.mapped.publisher.view;

import models.publisher.Template;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-07-25
 * Time: 10:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class TemplateView
    extends BaseView {
  public String templateId;
  public String templateName;
  public String tripAgency;
  public String tripAgencyId;

  public HashMap<String, String> tripAgencyList;
  public String templateNote;
  public Integer templateStatus;
  public String userId;

  public Template.Visibility templateSharing;
  public Boolean canEdit;
  public int duration;
  public String coverUrl;
}
