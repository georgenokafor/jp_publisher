package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2014-10-28.
 */
@Entity @Table(name = "consortium")
public class Consortium
    extends Model {

  public static final int INDEPENDENT_CONSORTIUM_ID = 0;
  public static Model.Finder<Integer, Consortium> find = new Finder(Consortium.class);

  @Id
  @Column(name = "cons_id")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_consortium_id")
  private Integer consId;

  private String name;
  private Long createdtimestamp;
  private Long lastupdatedtimestamp;
  private String createdby;
  private String modifiedby;
  @Version
  private int version;

  public static Consortium buildConsortium(String name, String userId) {
    Consortium consortium = new Consortium();

    consortium.setCreatedby(userId);
    consortium.setCreatedtimestamp(System.currentTimeMillis());
    consortium.setModifiedby(userId);
    consortium.setLastupdatedtimestamp(consortium.createdtimestamp);
    consortium.setName(name);

    return consortium;
  }

  public static List<Consortium> findByName(String name) {
    return find.where().icontains("name", name).findList();
  }

  public static List<Consortium> findAllSorted(String orderBy) {
    return find.where().orderBy(orderBy).findList();
  }

  public static boolean nameExists(String name) {
    List<Consortium> consortiumList =  find.where().eq("lower(name)", name.toLowerCase()).findList();
    if (consortiumList.size() > 0) {
      return true;
    }
    return false;
  }

  public Integer getConsId() {
    return consId;
  }

  public void setConsId(Integer consId) {
    this.consId = consId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
