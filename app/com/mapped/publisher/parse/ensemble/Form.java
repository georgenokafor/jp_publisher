
package com.mapped.publisher.parse.ensemble;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="text" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="seq" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "value"
})
@XmlRootElement(name = "form")
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
public class Form {

    @XmlValue
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String value;
    @XmlAttribute(name = "type")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String type;
    @XmlAttribute(name = "name")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String name;
    @XmlAttribute(name = "text")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String text;
    @XmlAttribute(name = "seq")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String seq;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setText(String value) {
        this.text = value;
    }

    /**
     * Gets the value of the seq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getSeq() {
        return seq;
    }

    /**
     * Sets the value of the seq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setSeq(String value) {
        this.seq = value;
    }

}
