package com.umapped.itinerary.analyze.supplier.wcities;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.name.Named;
import com.umapped.itinerary.analyze.webservice.JsonWebServiceRequestHandler;

import javax.inject.Inject;

/**
 * Created by wei on 2017-03-30.
 */
public class WCitiesWebServiceRequestHandler extends JsonWebServiceRequestHandler {
  private static final String SERVER_URL = "http://dev.wcities.com/V3";


  @Inject
  public WCitiesWebServiceRequestHandler(@Named("ItineraryAnalayzeObjectMapper")ObjectMapper objectMapper) {
    super(objectMapper);
  }

  @Override protected String getServerUrl() {
    return SERVER_URL;
  }
}
