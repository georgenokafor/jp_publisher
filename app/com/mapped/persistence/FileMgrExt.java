package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.*;
import java.util.ArrayList;



public class FileMgrExt extends FileMgr{


    //files that belong to any maps owned by a user (incl. files for milestones or other files that are changed by other users as part of map sharing)
    private static final String SELECT_STR = "Select a.Pk, a.FilePath, a.FileTag, a.FileNote, a.Status, a.FileType, a.LastUpdateTimestamp, a.CreateTimestamp, a.ModifiedBy, a.CreatedBy, a.parentId, a.SyncTimestamp, a.rank, a.uploaded, a.imgwidth, a.imgheight, a.latitude, a.longitude, a.bearing from file a ";
    private static final String SELECT_ALL_MAP_OWNER_SINCE_LAST_UPDATE = SELECT_STR + "where (a.parentId = ? and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?)) or (a.parentid in (select m.Pk from maps m where m.createdBy = ?) and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?)) or (a.parentid in (select mi.Pk from milestones mi, maps m where mi.mapsid = m.Pk and m.createdBy = ?)  and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?)) or (a.parentid in (select f.Pk from file f where (f.parentid in (select m.Pk from maps m where m.createdBy = ?)) or (f.parentid in (select mi.Pk from milestones mi, maps m where mi.mapsid = m.Pk and m.createdBy = ?) )) and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?)) or (a.parentid in (select mi.Pk from milestonestypes mi, maps m where mi.mapsid = m.Pk and m.createdBy = ?)  and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?));";
    private static final String SELECT_SHARED_BY_USER_SINCE_LAST_UPDATE =  SELECT_STR + "where (a.parentid in (select m.mapsid from sharedmaps m where m.shareuserid = ? and m.status = 0) and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?)) or (a.parentid in (select mi.Pk from milestones mi where mi.mapsid in(select mapsid from sharedmaps where shareuserid =? and status = 0))  and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?)) or (a.parentid in (select f.Pk from file f where (f.parentid in (select m.mapsid from sharedmaps m where m.shareuserid = ? and status = 0)) or (f.parentid in (select mi.Pk from milestones mi where mi.mapsid in(select mapsid from sharedmaps where shareuserid =? and status = 0)))) and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?)) or (a.parentid in (select mi.Pk from milestonestypes mi where mi.mapsid in(select mapsid from sharedmaps where shareuserid =? and status = 0))  and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?));";
    private static final String SELECT_ALL_PARENT_ID = SELECT_STR + "where a.ParentId = ? ORDER BY a.rank, a.CreateTimestamp;";
    private static final String SELECT_ALL_ACTIVE_PARENT_ID = SELECT_STR + "where a.ParentId = ? and status = 0 ORDER BY a.rank, a.CreateTimestamp;";

    private static final String SELECT_NEW_SHARED_BY_USER_SINCE_LAST_UPDATE = SELECT_STR + "where (a.parentid in (select m.mapsid from sharedmaps m where m.shareuserid = ? and m.status = 0 and (m.SyncTimestamp = 0 or m.SyncTimestamp > ?) and m.createtimestamp = m.lastupdatetimestamp)) or (a.parentid in (select mi.Pk from milestones mi where mi.mapsid in(select mapsid from sharedmaps where shareuserid =? and status = 0 and (SyncTimestamp = 0 or SyncTimestamp > ?) and createtimestamp = lastupdatetimestamp))) or (a.parentid in (select f.Pk from file f where (f.parentid in (select m.mapsid from sharedmaps m where m.shareuserid = ? and m.status = 0 and (m.SyncTimestamp = 0 or m.SyncTimestamp > ?) and m.createtimestamp = m.lastupdatetimestamp)))) or (a.parentid in (select f.Pk from file f where (f.parentid in (select mi.Pk from milestones mi where mi.mapsid in(select mapsid from sharedmaps where shareuserid =? and status =  0 and (SyncTimestamp = 0 or SyncTimestamp > ?) and createtimestamp = lastupdatetimestamp))))) or (a.parentid in (select mi.Pk from milestonestypes mi where mi.mapsid in(select mapsid from sharedmaps where shareuserid =? and status = 0 and (SyncTimestamp = 0 or SyncTimestamp > ?) and createtimestamp = lastupdatetimestamp)));";
    private static final String SELECT_PROFILE_PHOTO_BY_USER_SINCE_LAST_UPDATE =  SELECT_STR + "where (a.parentid in (select owner from sharedMaps where shareUserId = ? and status = 0) or a.parentid in (select shareuserid from sharedmaps where owner = ? and status = 0) or a.parentid in (select shareuserid from sharedmaps where owner in (select owner from sharedmaps where shareuserid = ? and status = 0))) and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?);";
    private static final String SELECT_PROFILE_PHOTO_FOR_NEW_SHARED_USER_SINCE_LAST_UPDATE =   SELECT_STR + " where (a.parentid in (select owner from sharedMaps where shareUserId = ? and status = 0 and (SyncTimestamp = 0 or SyncTimestamp > ?)) or a.parentid in (select shareuserid from sharedmaps where owner = ? and status = 0 and (SyncTimestamp = 0 or SyncTimestamp > ?)) or a.parentid in (select shareuserid from sharedmaps where owner in (select owner from sharedmaps where shareuserid = ? and status = 0 and (SyncTimestamp = 0 or SyncTimestamp > ?))) ) ;";
    private static final String SELECT_ALL_STATIC_MAPS_SINCE_LAST_UPDATE = "Select FileType, FilePath, FileTag, FileNote, Status, Pk, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, ParentId, SyncTimestamp, Rank, uploaded, imgwidth, imgheight, latitude, longitude, bearing from File where (pk in  (select mi.Pk from milestones mi, maps m where mi.mapsid = m.Pk and m.createdBy = ?) or pk in (select mi.Pk from milestones mi where mi.mapsid in(select mapsid from sharedmaps where shareuserid =? and status = 0 and (SyncTimestamp = 0 or SyncTimestamp > ?) and createtimestamp = lastupdatetimestamp))) and status = 0 and synctimestamp > ?;";

    private static final String SELECT_ALL_MAP_OWNER_PAGE_SINCE_LAST_UPDATE = SELECT_STR + "where ((a.parentid in (select pk from page where mapsid in (select pk from maps where createdby = ?))) and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?)) or ((a.parentid in (select f.Pk from file f where (f.parentid in (select pk from page where mapsid in (select pk from maps where createdby = ?))))) and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?));";
    private static final String SELECT_SHARED_BY_USER_PAGE_SINCE_LAST_UPDATE = SELECT_STR + "where ((a.parentid in (select pk from page where mapsid in (select mapsid from sharedmaps where shareuserid =? and status = 0))) and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?)) or ((a.parentid in (select f.Pk from file f where (f.parentid in (select pk from page where mapsid in (select mapsid from sharedmaps where shareuserid =? and status = 0))))) and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?));";
    private static final String SELECT_NEWSHARED_BY_USER_PAGE_SINCE_LAST_UPDATE = SELECT_STR + "where (a.parentid in (select pk from page where mapsid in (select m.mapsid from sharedmaps m where m.shareuserid = ? and m.status = 0 and (m.SyncTimestamp = 0 or m.SyncTimestamp > ?) and m.createtimestamp = m.lastupdatetimestamp))) or (a.parentid in (select f.Pk from file f where (f.parentid in (select pk from page where mapsid in (select m.mapsid from sharedmaps m where m.shareuserid = ? and m.status = 0 and (m.SyncTimestamp = 0 or m.SyncTimestamp > ?) and m.createtimestamp = m.lastupdatetimestamp)))));";

    private static final String SELECT_PLACES_PHOTOS_FOR_PAGES =  SELECT_STR + "where a.status = 0 and a.FileType ='Image' and a.parentId in (select contentid from pagecontent where status = 0 and pageid in (select pk from page where mapsid = ? and status = 0)) ORDER BY a.rank, a.CreateTimestamp";
    private static final String SELECT_PHOTOS_FOR_PAGES =  SELECT_STR + "where a.status = 0 and a.FileType ='Image' and a.parentId in (select milestonesid from page where mapsid = ? and status = 0) ORDER BY a.rank, a.CreateTimestamp";
    private static final String SELECT_FILES_FOR_PLACES =  SELECT_STR + "where a.status = 0 and a.parentId in (select contentid from pagecontent where status = 0 and pageid in (select pk from page where mapsid = ? and status = 0)) ORDER BY a.rank, a.CreateTimestamp";
    private static final String SELECT_FILES_FOR_PAGES =  SELECT_STR + "where a.status = 0 and a.parentId in (select pk from page where mapsid = ? and status = 0) ORDER BY a.rank, a.CreateTimestamp";
    private static final String SELECT_FILES_FOR_PAGE=  SELECT_STR + "where a.status = 0 and ((a.parentid = ? or a.parentId in (select milestonesid from page where pk = ? and status = 0)) or (a.parentid in (select pk from file where status = 0 and (parentid = ? or parentId in (select milestonesid from page where pk = ? and status = 0)))))  ORDER BY a.rank, a.CreateTimestamp";
    private static final String INSERT = "insert into File (Pk, FilePath, FileTag, FileNote, Status, FileType, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, ParentId, SyncTimestamp, Rank, Uploaded, ImgWidth, ImgHeight, Latitude, Longitude, Bearing) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String UPDATE = "update File SET FilePath=?, FileTag=?, FileNote=?, Status=?, FileType=?, LastUpdateTimestamp=?, ModifiedBy=?, ParentId=?, SyncTimestamp=?, Rank=?, Uploaded=?, ImgWidth=?, ImgHeight=?, Latitude=?, Longitude=?, Bearing=? WHERE Pk = ?; ";


    public static ArrayList <File> getAllByMapOwnerSinceLastUpdate (String userId, long lastUpdate) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_ALL_MAP_OWNER_SINCE_LAST_UPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);

            prep.setString(3, userId);
            prep.setLong(4, lastUpdate);
            prep.setString(5, userId);
            prep.setLong(6, lastUpdate);
            prep.setString(7, userId);
            prep.setString(8, userId);
            prep.setLong(9, lastUpdate);
            prep.setString(10, userId);
            prep.setLong(11, lastUpdate);

            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <File> getAllPagesByMapOwnerSinceLastUpdate (String userId, long lastUpdate) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_ALL_MAP_OWNER_PAGE_SINCE_LAST_UPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            prep.setString(3, userId);
            prep.setLong(4, lastUpdate);


            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <File> getSharedByUserPageSinceLastUpdate (String userId, long lastUpdate) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_SHARED_BY_USER_PAGE_SINCE_LAST_UPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            prep.setString(3, userId);
            prep.setLong(4, lastUpdate);


            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <File> getNewSharedByUserPageSinceLastUpdate (String userId, long lastUpdate) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_NEWSHARED_BY_USER_PAGE_SINCE_LAST_UPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            prep.setString(3, userId);
            prep.setLong(4, lastUpdate);


            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }


    public static ArrayList <File> getAllByParentId (String userId) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_ALL_PARENT_ID);
            prep.setString(1, userId);


            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <File> getAllActiveByParentId (String parentId) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_ALL_ACTIVE_PARENT_ID);
            prep.setString(1, parentId);


            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <File> getSharedByUserSinceLastUpdate (String userId, long lastUpdate) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_SHARED_BY_USER_SINCE_LAST_UPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            prep.setString(3, userId);
            prep.setLong(4, lastUpdate);
            prep.setString(5, userId);
            prep.setString(6, userId);
            prep.setLong(7, lastUpdate);
            prep.setString(8, userId);
            prep.setLong(9, lastUpdate);

            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }

    }

    public static ArrayList <File> getNewSharedByUserSinceLastUpdate (String userId, long lastUpdate) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_NEW_SHARED_BY_USER_SINCE_LAST_UPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            prep.setString(3, userId);
            prep.setLong(4, lastUpdate);
            prep.setString(5, userId);
            prep.setLong(6, lastUpdate);
            prep.setString(7, userId);
            prep.setLong(8, lastUpdate);
            prep.setString(9, userId);
            prep.setLong(10, lastUpdate);

            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }

    }

    public static ArrayList <File> getUpdatedProfilePhotosForSharedUsers (String userId, long lastUpdate) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_PROFILE_PHOTO_BY_USER_SINCE_LAST_UPDATE);
            prep.setString(1, userId);
            prep.setString(2, userId);
            prep.setString(3, userId);
            prep.setLong(4, lastUpdate);

            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }

    }

    public static ArrayList <File> getProfilePhotosForNewShares (String userId, long lastUpdate) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_PROFILE_PHOTO_FOR_NEW_SHARED_USER_SINCE_LAST_UPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            prep.setString(3, userId);
            prep.setLong(4, lastUpdate);
            prep.setString(5, userId);
            prep.setLong(6, lastUpdate);

            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }

    }


    public static ArrayList <File> getStaticMaps (String userId, long lastUpdate) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_ALL_STATIC_MAPS_SINCE_LAST_UPDATE);
            prep.setString(1, userId);
            prep.setString(2, userId);
            prep.setLong(3, lastUpdate);
            prep.setLong(4, lastUpdate);


            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }

    }

    public static ArrayList <File> getAllPhotosForPlacesByMapsId (String mapsId) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_PLACES_PHOTOS_FOR_PAGES);
            prep.setString(1, mapsId);


            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <File> getAllPhotosForPagesByMapsId (String mapsId) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_PHOTOS_FOR_PAGES);
            prep.setString(1, mapsId);


            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <File> getAllFilesForPlacesByMapsId (String mapsId) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_FILES_FOR_PLACES);
            prep.setString(1, mapsId);


            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <File> getAllFilesForPagesByMapsId (String mapsId) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_FILES_FOR_PAGES);
            prep.setString(1, mapsId);


            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <File> getAllFilesForPage (String pageId) throws SQLException {
        ArrayList <File> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_FILES_FOR_PAGE);
            prep.setString(1, pageId);
            prep.setString(2, pageId);
            prep.setString(3, pageId);
            prep.setString(4, pageId);
            rs = prep.executeQuery();
            return (FileMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static void insert (File c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(INSERT);
            prep.setString(1, c.getPk());
            prep.setString(2, c.getFilePath());
            prep.setString(3, c.getFileTag());
            prep.setString(4, c.getFileNote());
            prep.setLong(5, c.getStatus());
            prep.setString(6, c.getFileType());
            prep.setLong(7, c.getLastUpdateTimestamp());
            prep.setLong(8, c.getCreateTimestamp());
            prep.setString(9, c.getModifiedBy());
            prep.setString(10, c.getCreatedBy());
            prep.setString(11, c.getParentId());
            prep.setLong(12, c.getSyncTimestamp());
            prep.setString(13, c.getRank());
            prep.setString(14, c.getUploaded());
            prep.setString(15, c.getImgWidth());
            prep.setString(16, c.getImgHeight());
            prep.setString(17, c.getLatitude());
            prep.setString(18, c.getLongitude());
            prep.setString(19, c.getBearing());
            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

    public static int update (File c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(UPDATE);
            prep.setString(1, c.getFilePath());
            prep.setString(2, c.getFileTag());
            prep.setString(3, c.getFileNote());
            prep.setLong(4, c.getStatus());
            prep.setString(5, c.getFileType());
            prep.setLong(6, c.getLastUpdateTimestamp());
            prep.setString(7, c.getModifiedBy());
            prep.setString(8, c.getParentId());
            prep.setLong(9, c.getSyncTimestamp());
            prep.setString(10, c.getRank());
            prep.setString(11, c.getUploaded());
            prep.setString(12, c.getImgWidth());
            prep.setString(13, c.getImgHeight());
            prep.setString(14, c.getLatitude());
            prep.setString(15, c.getLongitude());
            prep.setString(16, c.getBearing());
            prep.setString(17, c.getPk());
            return prep.executeUpdate();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

}
