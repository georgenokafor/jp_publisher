package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class MapAnnotationMgr {

	 private static final String SELECT_ALL = "Select Pk, MapsId, UsersId, Longitude, Latitude, Bearing, Status, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, MilestonesId, Tag, Note, ModifiedBy, CreatedBy from MapAnnotation;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, MapsId, UsersId, Longitude, Latitude, Bearing, Status, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, MilestonesId, Tag, Note, ModifiedBy, CreatedBy from MapAnnotation where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from MapAnnotation where Pk = ?;";
	 private static final String INSERT = "insert into MapAnnotation (Pk, MapsId, UsersId, Longitude, Latitude, Bearing, Status, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, MilestonesId, Tag, Note, ModifiedBy, CreatedBy) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update MapAnnotation SET MapsId=?, UsersId=?, Longitude=?, Latitude=?, Bearing=?, Status=?, LastUpdateTimestamp=?, SyncTimestamp=?, MilestonesId=?, Tag=?, Note=?, ModifiedBy=? WHERE Pk = ?; ";
	 public static ArrayList <MapAnnotation> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return MapAnnotationMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static MapAnnotation getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<MapAnnotation> results = MapAnnotationMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (MapAnnotation c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getMapsId());
				 prep.setString(3, c.getUsersId());
				 prep.setString(4, c.getLongitude());
				 prep.setString(5, c.getLatitude());
				 prep.setString(6, c.getBearing());
				 prep.setLong(7, c.getStatus());
				 prep.setLong(8, c.getLastUpdateTimestamp());
				 prep.setLong(9, c.getCreateTimestamp());
				 prep.setLong(10, c.getSyncTimestamp());
				 prep.setString(11, c.getMilestonesId());
				 prep.setString(12, c.getTag());
				 prep.setString(13, c.getNote());
				 prep.setString(14, c.getModifiedBy());
				 prep.setString(15, c.getCreatedBy());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (MapAnnotation c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getMapsId());
				 prep.setString(2, c.getUsersId());
				 prep.setString(3, c.getLongitude());
				 prep.setString(4, c.getLatitude());
				 prep.setString(5, c.getBearing());
				 prep.setLong(6, c.getStatus());
				 prep.setLong(7, c.getLastUpdateTimestamp());
				 prep.setLong(8, c.getSyncTimestamp());
				 prep.setString(9, c.getMilestonesId());
				 prep.setString(10, c.getTag());
				 prep.setString(11, c.getNote());
				 prep.setString(12, c.getModifiedBy());
				 prep.setString(13, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <MapAnnotation> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <MapAnnotation> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 MapAnnotation c = new MapAnnotation();
			 c.setPk(rs.getString("Pk"));
			 c.setMapsId(rs.getString("MapsId"));
			 c.setUsersId(rs.getString("UsersId"));
			 c.setLongitude(rs.getString("Longitude"));
			 c.setLatitude(rs.getString("Latitude"));
			 c.setBearing(rs.getString("Bearing"));
			 c.setStatus(rs.getLong("Status"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setMilestonesId(rs.getString("MilestonesId"));
			 c.setTag(rs.getString("Tag"));
			 c.setNote(rs.getString("Note"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 results.add(c);
		 }
		 return results;
	 }
}
