
package com.mapped.publisher.parse.classicvacations.datesearch;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "AgentName",
    "LeadTravellerName",
    "BookingID",
    "BookingDate",
    "DepartureDate",
    "Destination",
    "AmountPosted",
    "AmountDue",
    "DueDate",
    "TotalSell",
    "Commission"
})
public class Booking {

    @JsonProperty("AgentName")
    private String AgentName;
    @JsonProperty("LeadTravellerName")
    private String LeadTravellerName;
    @JsonProperty("BookingID")
    private String BookingID;
    @JsonProperty("BookingDate")
    private String BookingDate;
    @JsonProperty("DepartureDate")
    private String DepartureDate;
    @JsonProperty("Destination")
    private String Destination;
    @JsonProperty("AmountPosted")
    private Double AmountPosted;
    @JsonProperty("AmountDue")
    private Double AmountDue;
    @JsonProperty("DueDate")
    private String DueDate;
    @JsonProperty("TotalSell")
    private Double TotalSell;
    @JsonProperty("Commission")
    private Double Commission;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The AgentName
     */
    @JsonProperty("AgentName")
    public String getAgentName() {
        return AgentName;
    }

    /**
     * 
     * @param AgentName
     *     The AgentName
     */
    @JsonProperty("AgentName")
    public void setAgentName(String AgentName) {
        this.AgentName = AgentName;
    }

    /**
     * 
     * @return
     *     The LeadTravellerName
     */
    @JsonProperty("LeadTravellerName")
    public String getLeadTravellerName() {
        return LeadTravellerName;
    }

    /**
     * 
     * @param LeadTravellerName
     *     The LeadTravellerName
     */
    @JsonProperty("LeadTravellerName")
    public void setLeadTravellerName(String LeadTravellerName) {
        this.LeadTravellerName = LeadTravellerName;
    }

    /**
     * 
     * @return
     *     The BookingID
     */
    @JsonProperty("BookingID")
    public String getBookingID() {
        return BookingID;
    }

    /**
     * 
     * @param BookingID
     *     The BookingID
     */
    @JsonProperty("BookingID")
    public void setBookingID(String BookingID) {
        this.BookingID = BookingID;
    }

    /**
     * 
     * @return
     *     The BookingDate
     */
    @JsonProperty("BookingDate")
    public String getBookingDate() {
        return BookingDate;
    }

    /**
     * 
     * @param BookingDate
     *     The BookingDate
     */
    @JsonProperty("BookingDate")
    public void setBookingDate(String BookingDate) {
        this.BookingDate = BookingDate;
    }

    /**
     * 
     * @return
     *     The DepartureDate
     */
    @JsonProperty("DepartureDate")
    public String getDepartureDate() {
        return DepartureDate;
    }

    /**
     * 
     * @param DepartureDate
     *     The DepartureDate
     */
    @JsonProperty("DepartureDate")
    public void setDepartureDate(String DepartureDate) {
        this.DepartureDate = DepartureDate;
    }

    /**
     * 
     * @return
     *     The Destination
     */
    @JsonProperty("Destination")
    public String getDestination() {
        return Destination;
    }

    /**
     * 
     * @param Destination
     *     The Destination
     */
    @JsonProperty("Destination")
    public void setDestination(String Destination) {
        this.Destination = Destination;
    }

    /**
     * 
     * @return
     *     The AmountPosted
     */
    @JsonProperty("AmountPosted")
    public Double getAmountPosted() {
        return AmountPosted;
    }

    /**
     * 
     * @param AmountPosted
     *     The AmountPosted
     */
    @JsonProperty("AmountPosted")
    public void setAmountPosted(Double AmountPosted) {
        this.AmountPosted = AmountPosted;
    }

    /**
     * 
     * @return
     *     The AmountDue
     */
    @JsonProperty("AmountDue")
    public Double getAmountDue() {
        return AmountDue;
    }

    /**
     * 
     * @param AmountDue
     *     The AmountDue
     */
    @JsonProperty("AmountDue")
    public void setAmountDue(Double AmountDue) {
        this.AmountDue = AmountDue;
    }

    /**
     * 
     * @return
     *     The DueDate
     */
    @JsonProperty("DueDate")
    public String getDueDate() {
        return DueDate;
    }

    /**
     * 
     * @param DueDate
     *     The DueDate
     */
    @JsonProperty("DueDate")
    public void setDueDate(String DueDate) {
        this.DueDate = DueDate;
    }

    /**
     * 
     * @return
     *     The TotalSell
     */
    @JsonProperty("TotalSell")
    public Double getTotalSell() {
        return TotalSell;
    }

    /**
     * 
     * @param TotalSell
     *     The TotalSell
     */
    @JsonProperty("TotalSell")
    public void setTotalSell(Double TotalSell) {
        this.TotalSell = TotalSell;
    }

    /**
     * 
     * @return
     *     The Commission
     */
    @JsonProperty("Commission")
    public Double getCommission() {
        return Commission;
    }

    /**
     * 
     * @param Commission
     *     The Commission
     */
    @JsonProperty("Commission")
    public void setCommission(Double Commission) {
        this.Commission = Commission;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
