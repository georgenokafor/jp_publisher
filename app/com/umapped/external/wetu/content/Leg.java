
package com.umapped.external.wetu.content;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "days",
    "itinerary_leg_id",
    "sequence",
    "content_entity_id",
    "destination_content_entity_id",
    "nights",
    "booking_reference",
    "type",
    "check_in_time",
    "check_out_time",
    "check_in_sequence",
    "check_out_sequence"
})
public class Leg {

    @JsonProperty("days")
    private List<Day> days = null;
    @JsonProperty("itinerary_leg_id")
    private int itineraryLegId;
    @JsonProperty("sequence")
    private int sequence;
    @JsonProperty("content_entity_id")
    private int contentEntityId;
    @JsonProperty("destination_content_entity_id")
    private int destinationContentEntityId;
    @JsonProperty("nights")
    private int nights;
    @JsonProperty("booking_reference")
    private String bookingReference;
    @JsonProperty("type")
    private String type;
    @JsonProperty("check_in_time")
    private String checkInTime;
    @JsonProperty("check_out_time")
    private String checkOutTime;
    @JsonProperty("check_in_sequence")
    private int checkInSequence;
    @JsonProperty("check_out_sequence")
    private int checkOutSequence;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("days")
    public List<Day> getDays() {
        return days;
    }

    @JsonProperty("days")
    public void setDays(List<Day> days) {
        this.days = days;
    }

    @JsonProperty("itinerary_leg_id")
    public int getItineraryLegId() {
        return itineraryLegId;
    }

    @JsonProperty("itinerary_leg_id")
    public void setItineraryLegId(int itineraryLegId) {
        this.itineraryLegId = itineraryLegId;
    }

    @JsonProperty("sequence")
    public int getSequence() {
        return sequence;
    }

    @JsonProperty("sequence")
    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    @JsonProperty("content_entity_id")
    public int getContentEntityId() {
        return contentEntityId;
    }

    @JsonProperty("content_entity_id")
    public void setContentEntityId(int contentEntityId) {
        this.contentEntityId = contentEntityId;
    }

    @JsonProperty("destination_content_entity_id")
    public int getDestinationContentEntityId() {
        return destinationContentEntityId;
    }

    @JsonProperty("destination_content_entity_id")
    public void setDestinationContentEntityId(int destinationContentEntityId) {
        this.destinationContentEntityId = destinationContentEntityId;
    }

    @JsonProperty("nights")
    public int getNights() {
        return nights;
    }

    @JsonProperty("nights")
    public void setNights(int nights) {
        this.nights = nights;
    }

    @JsonProperty("booking_reference")
    public String getBookingReference() {
        return bookingReference;
    }

    @JsonProperty("booking_reference")
    public void setBookingReference(String bookingReference) {
        this.bookingReference = bookingReference;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("check_in_time")
    public String getCheckInTime() {
        return checkInTime;
    }

    @JsonProperty("check_in_time")
    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    @JsonProperty("check_out_time")
    public String getCheckOutTime() {
        return checkOutTime;
    }

    @JsonProperty("check_out_time")
    public void setCheckOutTime(String checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    @JsonProperty("check_in_sequence")
    public int getCheckInSequence() {
        return checkInSequence;
    }

    @JsonProperty("check_in_sequence")
    public void setCheckInSequence(int checkInSequence) {
        this.checkInSequence = checkInSequence;
    }

    @JsonProperty("check_out_sequence")
    public int getCheckOutSequence() {
        return checkOutSequence;
    }

    @JsonProperty("check_out_sequence")
    public void setCheckOutSequence(int checkOutSequence) {
        this.checkOutSequence = checkOutSequence;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("days", days).append("itineraryLegId", itineraryLegId).append("sequence", sequence).append("contentEntityId", contentEntityId).append("destinationContentEntityId", destinationContentEntityId).append("nights", nights).append("bookingReference", bookingReference).append("type", type).append("checkInTime", checkInTime).append("checkOutTime", checkOutTime).append("checkInSequence", checkInSequence).append("checkOutSequence", checkOutSequence).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(checkOutTime).append(type).append(checkInTime).append(checkInSequence).append(contentEntityId).append(checkOutSequence).append(days).append(additionalProperties).append(sequence).append(destinationContentEntityId).append(nights).append(bookingReference).append(itineraryLegId).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Leg) == false) {
            return false;
        }
        Leg rhs = ((Leg) other);
        return new EqualsBuilder().append(checkOutTime, rhs.checkOutTime).append(type, rhs.type).append(checkInTime, rhs.checkInTime).append(checkInSequence, rhs.checkInSequence).append(contentEntityId, rhs.contentEntityId).append(checkOutSequence, rhs.checkOutSequence).append(days, rhs.days).append(additionalProperties, rhs.additionalProperties).append(sequence, rhs.sequence).append(destinationContentEntityId, rhs.destinationContentEntityId).append(nights, rhs.nights).append(bookingReference, rhs.bookingReference).append(itineraryLegId, rhs.itineraryLegId).isEquals();
    }

}
