package com.mapped.publisher.audit.event;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.sql.Timestamp;

/**
 * Created by Serguei Moutovkin on 2014-05-21.
 */
@JsonTypeName("TRIP_BOOKING_FLIGHT")
public class AuditTripBookingFlight
    extends AuditEvent {

  public String bookingId;
  public String number;
  public Timestamp departure;


  public AuditTripBookingFlight withBookingId(String id) {
    bookingId = id;
    return this;
  }

  public AuditTripBookingFlight withFlightNumber(String number) {
    this.number = number;
    return this;
  }

  public AuditTripBookingFlight withDeparture(Timestamp dt) {
    this.departure = dt;
    return this;
  }

  public AuditTripBookingFlight withDeparture(long dt) {
    this.departure = new Timestamp(dt);
    return this;
  }
}
