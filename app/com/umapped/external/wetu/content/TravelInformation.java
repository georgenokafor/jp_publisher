package com.umapped.external.wetu.content;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by george on 2016-06-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class TravelInformation {
  public String health;
  public String safety;
  public String electricity;
  public String banking;
  public String visa;
  public String cuisine;
  public String climate;
  public String transport;
  public String dress;
  public String additionalInfo;

  public String getHealth() {
    return health;
  }

  public void setHealth(String health) {
    this.health = health;
  }

  public String getSafety() {
    return safety;
  }

  public void setSafety(String safety) {
    this.safety = safety;
  }

  public String getElectricity() {
    return electricity;
  }

  public void setElectricity(String electricity) {
    this.electricity = electricity;
  }

  public String getBanking() {
    return banking;
  }

  public void setBanking(String banking) {
    this.banking = banking;
  }

  public String getVisa() {
    return visa;
  }

  public void setVisa(String visa) {
    this.visa = visa;
  }

  public String getCuisine() {
    return cuisine;
  }

  public void setCuisine(String cuisine) {
    this.cuisine = cuisine;
  }

  public String getClimate() {
    return climate;
  }

  public void setClimate(String climate) {
    this.climate = climate;
  }

  public String getTransport() {
    return transport;
  }

  public void setTransport(String transport) {
    this.transport = transport;
  }

  public String getDress() {
    return dress;
  }

  public void setDress(String dress) {
    this.dress = dress;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  @Override
  public String toString() {
    return "TravelInformation{" +
        "health='" + health + '\'' +
        ", safety='" + safety + '\'' +
        ", electricity='" + electricity + '\'' +
        ", banking='" + banking + '\'' +
        ", visa='" + visa + '\'' +
        ", cuisine='" + cuisine + '\'' +
        ", climate='" + climate + '\'' +
        ", transport='" + transport + '\'' +
        ", dress='" + dress + '\'' +
        ", additionalInfo='" + additionalInfo + '\'' +
        '}';
  }
}
