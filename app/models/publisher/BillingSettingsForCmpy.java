package models.publisher;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freshbooks.model.Client;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-03-26.
 */
@Entity @Table(name = "bil_settings_cmpy")
public class BillingSettingsForCmpy
    extends Model {
  public static Model.Finder<String, BillingSettingsForCmpy> find = new Finder(BillingSettingsForCmpy.class);

  @Id @Column(name = "cmpyid")
  private String cmpyId;
  @ManyToOne @JoinColumn(name = "schedule")
  private BillingSchedule schedule;
  @ManyToOne @JoinColumn(name = "plan", nullable = true)
  private BillingPlan plan;

  @ManyToOne(optional = true) @JoinColumn(name = "pmnt_type", nullable = true, referencedColumnName = "name")
  private BillingPmntMethod pmntType;

  private Long startTs;
  private Long cutoffTs;
  private Boolean recurrent;
  private String meta;
  private Long syncTs;
  private Long createdTs;
  private String createdBy;
  private Long modifiedTs;
  private String modifiedBy;
  @Version
  private int version;

  public static BillingSettingsForCmpy buildCmpySettings(String cmpyId, String createdBy) {
    BillingSettingsForCmpy bsc = new BillingSettingsForCmpy();
    bsc.setCmpyId(cmpyId);
    long ts = System.currentTimeMillis();
    bsc.setCreatedBy(createdBy);
    bsc.setModifiedBy(createdBy);
    bsc.setCreatedTs(ts);
    bsc.setModifiedTs(ts);
    bsc.setSyncTs(0l);
    return bsc;
  }


  /**
   * If in the future we will have various types of integrations each will be
   * assigned to a dedicated variable in this wrapper class. For now freshbooks
   * is the only type.
   */
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class MetaDataWrapper {
    public Client freshbooks;
  }


  public static int getPlanUseCount(Long planId) {
    return find.where().eq("plan.planId", planId).findCount();
  }

  public Client getFreshbookClientData() {
    if (this.meta == null) {
      return null;
    }

    ObjectMapper m = new ObjectMapper();
    m.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    MetaDataWrapper md = null;
    try {
      md = m.readValue(this.meta, MetaDataWrapper.class);
    }
    catch (Exception je) {
      Log.err("Failed to unmarshal Freshbooks Client Data from JSON:", je);
    }
    return md.freshbooks;
  }

  public void setFreshbookClientData(Client client) {
    ObjectMapper m = new ObjectMapper();
    m.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    //Needs this black magic to avoid picking up methods that looks like real fields
    m.setVisibilityChecker(m.getSerializationConfig()
                            .getDefaultVisibilityChecker()
                            .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                            .withIsGetterVisibility(JsonAutoDetect.Visibility.NONE)
                            .withGetterVisibility(JsonAutoDetect.Visibility.NONE));
    String result = null;

    MetaDataWrapper idw = new MetaDataWrapper();
    idw.freshbooks = client;

    try {
      //result = m.writerWithDefaultPrettyPrinter().writeValueAsString(idw);
      result = m.writeValueAsString(idw);
    }
    catch (JsonProcessingException jpe) {
      Log.err("Failed to generate Freshbooks Client JSON Data:", jpe);
    }

    this.setMeta(result);
  }

  public String getMeta() {
     return meta;
  }

  public void setMeta(String meta) {
    this.meta = meta;
  }

  public Long getSyncTs() {
    return syncTs;
  }

  public void setSyncTs(Long syncTs) {
    this.syncTs = syncTs;
  }

  public static List<BillingSettingsForCmpy> findBillableOutOfSyncCmpies() {
    return find.where()
               .raw("sync_ts < modified_ts OR sync_ts IS NULL")
               .findList();
  }

  public void markModified(String userId) {
    setModifiedBy(userId);
    setModifiedTs(System.currentTimeMillis());
  }

  public void setSyncTsToModTs() {
    setSyncTs(modifiedTs);
  }

  public String getStartDate() {
    return Utils.getISO8601Date(startTs);
  }

  public String getCutoffDate() {
    return Utils.getISO8601Date(cutoffTs);
  }

  public Boolean getRecurrent() {
    return recurrent;
  }

  public void setRecurrent(Boolean recurrent) {
    if (recurrent == null) {
      this.recurrent = false;
    }
    else {
      this.recurrent = recurrent;
    }
  }

  public String getCmpyId() {
    return cmpyId;
  }

  public void setCmpyId(String cmpyId) {
    this.cmpyId = cmpyId;
  }

  public BillingSchedule getSchedule() {
    return schedule;
  }

  public void setSchedule(BillingSchedule schedule) {
    this.schedule = schedule;
  }

  public BillingPlan getPlan() {
    return plan;
  }

  public void setPlan(BillingPlan plan) {
    this.plan = plan;
  }

  public BillingPmntMethod getPmntType() {
    return pmntType;
  }

  public void setPmntType(BillingPmntMethod pmntType) {
    this.pmntType = pmntType;
  }

  public Long getStartTs() {
    return startTs;
  }

  public void setStartTs(Long startTs) {
    this.startTs = startTs;
  }

  public Long getCutoffTs() {
    return cutoffTs;
  }

  public void setCutoffTs(Long cutoffTs) {
    this.cutoffTs = cutoffTs;
  }

  public Long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Long createdTs) {
    this.createdTs = createdTs;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Long modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }


}
