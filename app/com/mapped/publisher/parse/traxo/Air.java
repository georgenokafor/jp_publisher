package com.mapped.publisher.parse.traxo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.List;

/**
 * Created by twong on 10/06/17.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Air extends Segment {
    public String airline;
    public String arrival_datetime;
    public String arrival_time_zone_id;
    public String class_of_service;
    public String departure_datetime;
    public String departure_time_zone_id;
    public String destination;
    public String destination_name;
    public String destination_admin_code;
    public String destination_city_name;
    public String destination_country;
    public String destination_lat;
    public String destination_lon;
    public String fare_basis_code;
    public String flight_number;
    public String iata_code;
    public String normalized_airline;
    public String number_of_pax;
    public String origin;
    public String origin_name;
    public String origin_admin_code;
    public String origin_city_name;
    public String origin_country;
    public String origin_lat;
    public String origin_lon;
    public String seat_assignment;
    public String ticket_number;
    public List<String> tickets;
    public List<String> seats;
}
