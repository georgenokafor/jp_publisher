package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class PageEntryMgr {

	 private static final String SELECT_ALL = "Select Pk, Name, Note, Longitude, Latitude, Tag, Address, StartDate, EndDate, MapsId, PageId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from PageEntry;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, Name, Note, Longitude, Latitude, Tag, Address, StartDate, EndDate, MapsId, PageId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from PageEntry where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from PageEntry where Pk = ?;";
	 private static final String INSERT = "insert into PageEntry (Pk, Name, Note, Longitude, Latitude, Tag, Address, StartDate, EndDate, MapsId, PageId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update PageEntry SET Name=?, Note=?, Longitude=?, Latitude=?, Tag=?, Address=?, StartDate=?, EndDate=?, MapsId=?, PageId=?, ModifiedBy=?, Status=?, SyncTimestamp=?, LastUpdateTimestamp=? WHERE Pk = ?; ";
	 private static final String SELECT_ALL_BY_PAGE_ID = "Select Pk, Name, Note, Longitude, Latitude, Tag, Address, StartDate, EndDate, MapsId, PageId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from PageEntry where PageId=? ";
	 private static final String DELETE_BY_PAGE_ID = "delete from PageEntry where PageId=? ";
	 public static ArrayList <PageEntry> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return PageEntryMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static PageEntry getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<PageEntry> results = PageEntryMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (PageEntry c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getName());
				 prep.setString(3, c.getNote());
				 prep.setString(4, c.getLongitude());
				 prep.setString(5, c.getLatitude());
				 prep.setString(6, c.getTag());
				 prep.setString(7, c.getAddress());
				 prep.setString(8, c.getStartDate());
				 prep.setString(9, c.getEndDate());
				 prep.setString(10, c.getMapsId());
				 prep.setString(11, c.getPageId());
				 prep.setString(12, c.getModifiedBy());
				 prep.setString(13, c.getCreatedBy());
				 prep.setLong(14, c.getStatus());
				 prep.setLong(15, c.getSyncTimestamp());
				 prep.setLong(16, c.getLastUpdateTimestamp());
				 prep.setLong(17, c.getCreateTimestamp());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (PageEntry c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getName());
				 prep.setString(2, c.getNote());
				 prep.setString(3, c.getLongitude());
				 prep.setString(4, c.getLatitude());
				 prep.setString(5, c.getTag());
				 prep.setString(6, c.getAddress());
				 prep.setString(7, c.getStartDate());
				 prep.setString(8, c.getEndDate());
				 prep.setString(9, c.getMapsId());
				 prep.setString(10, c.getPageId());
				 prep.setString(11, c.getModifiedBy());
				 prep.setLong(12, c.getStatus());
				 prep.setLong(13, c.getSyncTimestamp());
				 prep.setLong(14, c.getLastUpdateTimestamp());
				 prep.setString(15, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <PageEntry> getAllByPageId(String id) throws SQLException {
		 ResultSet rs = null;
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PAGE_ID);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 return PageEntryMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void deleteByPageId (String id) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PAGE_ID);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <PageEntry> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <PageEntry> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 PageEntry c = new PageEntry();
			 c.setPk(rs.getString("Pk"));
			 c.setName(rs.getString("Name"));
			 c.setNote(rs.getString("Note"));
			 c.setLongitude(rs.getString("Longitude"));
			 c.setLatitude(rs.getString("Latitude"));
			 c.setTag(rs.getString("Tag"));
			 c.setAddress(rs.getString("Address"));
			 c.setStartDate(rs.getString("StartDate"));
			 c.setEndDate(rs.getString("EndDate"));
			 c.setMapsId(rs.getString("MapsId"));
			 c.setPageId(rs.getString("PageId"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setStatus(rs.getLong("Status"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 results.add(c);
		 }
		 return results;
	 }
}
