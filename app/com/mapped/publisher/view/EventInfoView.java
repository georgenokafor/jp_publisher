package com.mapped.publisher.view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-28
 * Time: 12:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class EventInfoView extends BaseView implements Serializable {
    public String id;
    public List<UserView> owners;
    public String startDatePrint;
    public String endDatePrint;
    public String note;
    public String name;
    public boolean displayMap;
    public boolean allPosts = false;


    public boolean isTrip;


    public Map<String, List<EventPageView>> eventPages;
    public List<DestinationView> desttinationGuides;
    public List<String> dateGroup;
    public List<String> tagGroup;
    public Map<String, String> eventPageCovers;
    public  String selectedPage;
    public  String previousPage;
    public  String selectedPagePk;


    public List<EventPageView> eventPageList;
    public Map<String, EventPageView> eventPageMap;

    public EventPageView selectedPageView;


    public String agent;
    public String agentName;
    public String agentPhone;
    public String agentEmail;
    public String agentWebSite;
    public String agentProfilePhoto;
    public String agencyName;
    public String agencyAddress;
    public String agencyPhone;
    public String agencyEmail;
    public String agencyWebsite;
    public String agencyLogo;
    public String agencyFacebook;
    public String agencyTwitter;


    public AttachmentView cover;
    public List <AttachmentView> audio;
    public List <AttachmentView> photos;
    public List <AttachmentView> videos;

    public HashMap<String, Integer> photoCountByTag;


    public long cacheTimestamp;



}
