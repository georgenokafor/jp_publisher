package com.umapped.itinerary.analyze.adapters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import modules.ItineraryAnalyzeModule;
import com.umapped.itinerary.analyze.model.Itinerary;
import com.umapped.itinerary.analyze.model.ItineraryItem;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by wei on 2017-03-07.
 */
public class RPItinearyExport {
  private ObjectMapper om;

  ReservationPackageItineraryAdapter adapter;

  private static String DATA_ROOT = "/Users/wei/DevResources/data/trips/";
  private static String RESERVATION_ROOT = DATA_ROOT + "reservations/";
  private static String ITINERARY_ROOT = DATA_ROOT + "itinerary/";

  private static String DATE = "2017-03-09/";

  @Inject public RPItinearyExport(ObjectMapper om) {
    this.om = om;
    this.adapter = new ReservationPackageItineraryAdapter();
  }

  public Itinerary extract(File reservationFile)
      throws Exception {
    ReservationPackage rp = om.readValue(reservationFile, ReservationPackage.class);
    return adapter.adapt(rp);
  }

  public void outputJson(File reservationFile, Itinerary itinerary)
      throws Exception {
    if (itinerary.getItems().size() == 0)
      return;
    om.writeValue(new File(ITINERARY_ROOT + DATE + reservationFile.getName()), itinerary);

  }

  public void outputCsv(File reservationFile, Itinerary itinerary)
      throws Exception {
    if (itinerary.getItems().size() == 0)
      return;

    String header = "Id, Type, Start Date, End Date, Start Location, End Location\n";
    String format = "%s, %s, %s, %s, %s, %s\n";
    FileWriter w = new FileWriter(new File(ITINERARY_ROOT + DATE  + reservationFile.getName()
                                                                                        .replace("json", "csv")));
    w.write(header);
    itinerary.getItems().stream().
        sorted(ItineraryItem::compareByStartTime).forEach(item -> {
      try {
        w.write(String.format(format,
                              item.getUmId(),
                              item.getReservationType(),
                              item.getStartDateTime(),
                              item.getEndDateTime(),
                              item.getStartLocation() == null ? "" : item.getStartLocation().getAddress().getLocality(),
                              item.getEndLocation() == null ? "" : item.getEndLocation().getAddress().getLocality()));
      }
      catch (IOException e) {
      }
    });
    w.close();
  }

  public static void main(String[] args)
      throws Exception {

    Injector container = Guice.createInjector(new ItineraryAnalyzeModule());

    RPItinearyExport exporter = container.getInstance(RPItinearyExport.class);


    File dir = new File(RESERVATION_ROOT + DATE);

    File[] files = dir.listFiles((direction, name) -> name.endsWith(".json"));

    for (File f : files) {
      Itinerary itinerary = exporter.extract(f);
      exporter.outputJson(f, itinerary);
      exporter.outputCsv(f, itinerary);
    }
  }
}
