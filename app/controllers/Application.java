package controllers;

import actors.FlightPollAlertsActor;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.mapped.publisher.actions.AdminAccess;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.actions.Secured;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.DataTablesForm;
import com.mapped.publisher.persistence.ActiveTrips;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.DashboardView;
import com.mapped.publisher.view.DataTablesView;
import com.mapped.publisher.view.HomeView;
import models.publisher.AccountCmpyLink;
import models.publisher.Company;
import models.publisher.EmailParseLog;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;
import views.html.index;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class Application
    extends Controller {

  @Inject
  FormFactory formFactory;

  @With({Authenticated.class, Authorized.class})
  public Result index() {
    return redirect("/home");
  }

  @With({Secured.class})
  public Result login() {
    BaseView view = new BaseView();
    SessionMgr sessionMgr = new SessionMgr(session());
    String msg = sessionMgr.getParam(SessionConstants.SESSION_PARAM_MSG);
    sessionMgr.setParam(SessionConstants.SESSION_PARAM_MSG, null);
    if (msg == null) {
      msg = flash(SessionConstants.SESSION_PARAM_MSG);
    }
    if (msg != null) {
      view.message = msg;
    }

    return ok(views.html.login.render(view));
  }

  @With({Secured.class})
  public Result resetPwd() {
    BaseView view = new BaseView();

    return ok(views.html.resetPwd.render(view));
  }

  @With({Secured.class, Authenticated.class, Authorized.class})
  public Result home() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    if (sessionMgr.getCredentials().isUmappedAdmin()) {
      return adminHome();
    }
    else {
      return userHome();
    }
  }

  @With({Secured.class, AdminAccess.class, Authorized.class})
  public Result adminHome() {
    SessionMgr sessionMgr = new SessionMgr(session());
    BaseView view = new BaseView();
    Credentials cred = sessionMgr.getCredentials();

    view.firstName = cred.getFirstName();
    view.lastName = cred.getLastName();
    view.loggedInUserId = sessionMgr.getUserId();
    view.isUMappedAdmin = cred.isUmappedAdmin();
    view.isCmpyAdmin = false;

    Html a = views.html.adminIndex.render(view);
    return ok(a);
  }

  @With({Secured.class, Authenticated.class, Authorized.class})
  public Result userHome() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    HomeView view = new HomeView().withFirebaseURI(ConfigMgr.getAppParameter(com.mapped.common
                                                                                           .CoreConstants.FIREBASE_URI))
                                            .withFirebaseToken(sessionMgr.getCredentials().getFirebaseToken());

    Credentials cred = sessionMgr.getCredentials();
    view.billingStatus = cred.getBillingStatus();

    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      view.message = msg;
    }



    Company c = Company.find.byId(cred.getCmpyId());
    if(c != null) {
      view.cmpyName = c.getName();
    }

    view.firstName = cred.getFirstName();
    view.lastName = cred.getLastName();
    view.loggedInUserId = cred.getUserId();
    view.isUMappedAdmin = cred.isUmappedAdmin();
    view.isCmpyAdmin = cred.getCmpyLinkType() == AccountCmpyLink.LinkType.ADMIN;
    view.isPowerUser = cred.getCmpyLinkType() == AccountCmpyLink.LinkType.POWER;
    view.addCapabilities(sessionMgr.getCredentials().getCapabilities());
    return ok(index.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result dashboard() {

    SessionMgr    sessionMgr = SessionMgr.fromContext(ctx());
    Credentials   cred       = sessionMgr.getCredentials();
    DashboardView view       = new DashboardView();

    view.groupPublishedTours = new ArrayList<>();
    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      view.message = msg;
    } else {
      msg = sessionMgr.getParam(SessionConstants.SESSION_PARAM_MSG);
      if (msg != null && !msg.isEmpty()) {
        view.message = msg;
        if (cred.getBillingStatus() == Credentials.BillingStatus.CREDIT_CARD_EXPIRING) {
          view.linkId = "<a onclick=\"javascript:getPage('/user/billing')\" class='btn btn-info'> Edit Billing Information </a>";
        }
        sessionMgr.setParam(SessionConstants.SESSION_PARAM_MSG, "");
      }
    }

    //get  tours
    view.defaultTrips = TourController.buildTripsView(sessionMgr,
                                                      "%",
                                                      APPConstants.SEARCH_MY_TRIPS,
                                                      null,
                                                      0,
                                                      "myTours",
                                                      "");

    view.recentActiveTrips = AuditController.buildActiveTripsView(ActiveTrips.TripSearchType.USER_PUBLISHED,
                                                                  sessionMgr.getUserId(),
                                                                  0,
                                                                  //Today, i.e. within 0 day ago
                                                                  sessionMgr.getTimezoneOffsetMins() * 60 * 1000,
                                                                  "myTours");


    //hide the company tabs if the user has no extra access
    view.displayCmpyTabs = ((cred.getCmpyLinkType() == AccountCmpyLink.LinkType.ADMIN) || cred.hasGroupies());
    return ok(views.html.dashboard.render(view));
  }


  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result emailParseLogDT() {
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);
    SessionMgr    sessionMgr = SessionMgr.fromContext(ctx());

    class EmailRow {
      public String createdDate;
      public String subject;
      public String state;
      public String parser;
      public int bookingCount;
      public int attachCount;

    }

    DataTablesView<EmailRow> dtv = new DataTablesView<>(dtRequest.draw);
    dtv.recordsTotal    = EmailParseLog.getRowCount(sessionMgr.getUserId());
    dtv.recordsFiltered = EmailParseLog.getRowCountFiltered(sessionMgr.getUserId(), dtRequest.search.value);
    List<EmailParseLog> recs = EmailParseLog.getPage(sessionMgr.getUserId(), sessionMgr.getAccountId(), dtRequest.start, dtRequest.length, dtRequest.search.value);

    Instant now = Instant.now();
    for (EmailParseLog epl : recs) {
      Instant eplInstant = epl.getCreatedTs().toInstant();

      EmailRow row = new EmailRow();
      row.createdDate  = Utils.formatOffsetDateTimePrint(epl.getCreatedTs(), sessionMgr.getTimezoneOffsetMins() * 60 * 1000);
      row.subject    = epl.getSubject();
      if (epl.getState() == EmailParseLog.EmailState.WM_SUCCESS ||
          epl.getState() == EmailParseLog.EmailState.PARSED_LOCAL ||
          epl.getState() == EmailParseLog.EmailState.PARSED_WM ||
          epl.getState() == EmailParseLog.EmailState.WM_PARTIAL_SUCCESS ) {
        row.state = "Successfully Parsed";
      } else if (epl.getState() == EmailParseLog.EmailState.WM_NO_DATA ||
              epl.getState() == EmailParseLog.EmailState.WM_NO_ITEMS ||
              epl.getState() == EmailParseLog.EmailState.WM_UNRECOGNIZED_FORMAT ||
              epl.getState() == EmailParseLog.EmailState.WM_NO_ITEMS) {
        row.state = "Unsupported Format";
      } else if (epl.getState() == EmailParseLog.EmailState.SCHEDULED ||
              epl.getState() == EmailParseLog.EmailState.PREPROCESSING ||
              epl.getState() == EmailParseLog.EmailState.PARSING ||
              epl.getState() == EmailParseLog.EmailState.WM_SENT) {
        if (Duration.between(eplInstant, now).toMinutes() < 15 ) {
          row.state = "Processing";
        } else {
          row.state = "Unrecognized Format";
        }
      } else {
        row.state = "Unrecognized Format";
      }

      if (epl.getParser() != null) {
        String parser = epl.getParser().toLowerCase();
        if (parser.contains("sabre") || parser.contains("tripcase")) {
          row.parser = "Sabre";
        } else if (parser.contains("clientbase")) {
          row.parser = "Clientbase";
        } else if (parser.contains("tramada")) {
          row.parser = "Tramada";
        } else if (parser.contains("travel2")) {
          row.parser = "";
        }  else if (parser.contains("alpine")) {
          row.parser = "Alpine Adventures";
        }  else if (parser.contains("apollo")) {
          row.parser = "Apollo";
        }
      } else {
        row.parser = epl.getParser();
      }
      if (epl.getBkCount() != null) {
        row.bookingCount = epl.getBkCount();
      }else{
        row.bookingCount = 0;
      }

      if (epl.getAttachCount() != null) {
        row.attachCount = epl.getAttachCount();
      } else {
        row.attachCount = 0;
      }


      dtv.data.add(row);
    }

    return  ok(Json.toJson(dtv));
  }


  /**
   * Check status on batch processes from the memcache instance
   */
  public Result healthcheck() {

    final DynamicForm form = formFactory.form().bindFromRequest();
    String            auth = form.get("auth");
    if (auth == null || !auth.equals("umapped_sys_monitor")) {
      BaseView view = new BaseView();
      view.message = "";
      return ok(views.html.error.error404.render(view));
    }
    List<String>        activeProcesses    = (List<String>) CacheMgr.getMemcached(APPConstants.CACHE_SYS_ACTIVE_PROCESSES);
    List<String>        noHeartBeats       = new ArrayList<String>();
    Map<String, String> exceptions         = new HashMap<String, String>();
    List<String>        hearbeats          = new ArrayList<String>();
    Map<String, String> status             = new HashMap<String, String>();
    List<String>        process45Minexpiry = new ArrayList<>();
    process45Minexpiry.add(FlightPollAlertsActor.APPNAME);


    if (activeProcesses != null && activeProcesses.size() > 0) {
      //remove com.umapped.mobile.EmailInvite from the active process list
      activeProcesses.remove("com.umapped.mobile.EmailInvite"); //turning off email invite
      for (String process : activeProcesses) {
        try {
          String lastHeartbeat = (String) CacheMgr.getMemcached(APPConstants.CACHE_SYS_HEARTBEAT_PREFX + process);
          if (lastHeartbeat == null) {
            noHeartBeats.add(process);
          }
          else {
            long hb = Long.parseLong(lastHeartbeat);
            if (!process45Minexpiry.contains(process) && (System.currentTimeMillis() - hb) > (60 * 5 * 1000)) { //5
              // mins timeout
              Date d = new Date(hb);
              noHeartBeats.add(process + " last hb: " + d);
            }
            else if (process45Minexpiry.contains(process) && (System.currentTimeMillis() - hb) > (60 * 45 * 1000)) {
              //45 mins timeout
              Date d = new Date(hb);
              noHeartBeats.add(process + " last hb: " + d);
            }
            else {
              Date d = new Date(hb);
              hearbeats.add(process + " last hb: " + d);
            }
          }

          List<String> ex = (List<String>) CacheMgr.getMemcached(APPConstants.CACHE_SYS_EXCEPTION_PREFX + process);
          if (ex != null && ex.size() > 0) {
            for (String s : ex) {
              exceptions.put(process, s);
            }
          }

          String stat = (String) CacheMgr.getMemcached(APPConstants.CACHE_SYS_STATUS_PREFX + process);
          if (stat != null) {
            status.put(process, stat);
          }

        }
        catch (Exception e) {
          noHeartBeats.add(process);
        }
      }
      if (noHeartBeats.size() > 0 || exceptions.size() > 0) {
        StringBuffer sb = new StringBuffer();
        sb.append("ALERT \n");
        for (String s : noHeartBeats) {
          sb.append("NO HB: " + s + "\n");
        }

        for (String s : exceptions.keySet()) {
          sb.append("ERROR: " + s + " ::: " + exceptions.get(s) + "\n");
        }
        return status(500, sb.toString());


      }
      else {
        StringBuffer sb = new StringBuffer("STATUS: \n\n");
        sb.append("HEARTBEATS: \n");
        for (String s : hearbeats) {
          sb.append(s + " \n");
        }

        sb.append("\nSTATUS: \n");
        for (String s : status.keySet()) {
          sb.append(s + " : " + status.get(s) + " \n");
        }

        return ok("All processes up. \n" + sb.toString());
      }

    }
    else {
      return status(405, "No active processes");
    }
  }
}
