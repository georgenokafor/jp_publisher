package com.mapped.publisher.view;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.view.billing.BillingInfoView;
import models.publisher.Company;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-03
 * Time: 3:09 PM
 * To change this template use File | Settings | File Templates.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CmpyInfoView
    extends BaseView {
  public String cmpyId;
  public String cmpyName;
  public String logoUrl;
  public String logoName;
  public String comments;
  public int status;
  public String tag;
  public String cmpyType;
  public String contact;

  public int numAccounts = 1;
  public int numTrips;
  public String expiryDate;

  public PoiAddressView address;

  public String acceptedAgreementVersion;
  public String selectedAgreementVersion;
  public String acceptedAgreementDate;

  public String authorizedUserId;
  public List<UserView> adminUsers;
  public Map<String, String> agreements;

  public String currentAgreementId;

  public boolean canAccessApi;
  public String apiAccessCode;
  public List<CmpyApiParserView> cmpyParsers;

  public boolean itineraryCheck = false;
  public boolean enableCollaboration = true;

  public Company.BillingType billingType;
  public BillingInfoView billingInfo;
  public boolean flightTracking =  true;

  public String parentCmpyName;
  public int parentCmpyId;
  public boolean isParentCmpy;

  public Map<Integer, String> cmpyConsortiaMap;
  public EmailConfigView emailConfig;

  public String getAddressShort() {
    StringBuilder sb = new StringBuilder();

    if (address != null) {
      if (address.formatAddr1 != null && address.formatAddr1.length() > 0) {
        sb.append(address.formatAddr1);
        sb.append("<br/>");
      }
      if (address.formatAddr2 != null && address.formatAddr2.length() > 0) {
        sb.append(address.formatAddr2);
        sb.append("<br/>");
      }
    }

    return sb.toString();
  }

  public String getContactShort() {

    StringBuilder sb = new StringBuilder();

    if (contact != null && contact.length() > 0) {
      sb.append(contact);
      sb.append("<br/>");
    }

    if (address != null) {
      if (address.phone != null && address.phone.length() > 0) {
        sb.append("<abbr title=\"Phone\">Phone:</abbr>" + address.phone + "<br>");
      }

      if (address.email != null && address.email.length() > 0) {
        sb.append("<abbr title=\"Email\">Email:</abbr>" + address.email + "<br>");
      }

      if (address.web != null && address.web.length() > 0) {
        sb.append("<a href=\"" + address.web +"\" target=\"_blank\"><h5>Website</h5></a>");
      }
    }
    return sb.toString();
  }

  public String getCompanyType() {
    switch (cmpyType) {
      case "1":
        return "Travel Company";
      case "2":
        return "Destination Company";
      case "3":
        return "Trial Company";
    }
    return "UNKNOWN";
  }

  public List<ApiCmpyTokenView> apicmpyTokens;
  public ApiCmpyTokenView selectedApiCmpyToken;
  public Map <Integer, String> apiSrcTypeList;

  public boolean isDisplay12hrClock() {
    if (tag != null && tag.contains("24hrclock")) {
      return false;
    } else if (cmpyName != null && cmpyName.contains("Top Atlant")) {
      return false;
    }

    return true;
  }
}
