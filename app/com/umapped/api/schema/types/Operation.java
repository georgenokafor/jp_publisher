package com.umapped.api.schema.types;

/**
 * Json Request operation types
 *
 * Created by surge on 2014-09-25.
 */
public enum Operation {
  /** Create/add */
  ADD,
  /** Copy from existing or from a similar object (i.e. template) */
  COPY,
  /** Update existing */
  UPDATE,
  /** Delete existing */
  DELETE,
  /** List all */
  LIST,
  /** Find specific one or many */
  SEARCH,
  /** S3 Upload request, different from standard ADD/COPY */
  UPLOAD,
  /** Get state about an operation */
  STATE,
  /** Authentication, like providing username/password to get data from 3rd party systems, etc */
  AUTHENTICATE
}
