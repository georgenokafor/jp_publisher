package com.umapped.external.afar;

import com.umapped.itinerary.analyze.recommendation.RecommendedItem;

import java.util.List;

/**
 * Created by george on 2017-12-20.
 */
public class AfarPoiResults {
  List<Highlight> pois;
  int nextPos = -1;
  int prevPos = -1;
  int fromIndex = 0;
  int toIndex = 0;
  int totalPois = 0;
  String keyword="";

  public AfarPoiResults(List<Highlight> pois) {
    this.pois = pois;
  }

  public List<Highlight> getPois() {
    return pois;
  }

  public int getNextPos() {
    return nextPos;
  }

  public void setNextPos(int nextPos) {
    this.nextPos = nextPos;
  }

  public int getPrevPos() {
    return prevPos;
  }

  public void setPrevPos(int prevPos) {
    this.prevPos = prevPos;
  }

  public int getFromIndex() {
    return fromIndex;
  }

  public void setFromIndex(int fromIndex) {
    this.fromIndex = fromIndex;
  }

  public int getToIndex() {
    return toIndex;
  }

  public void setToIndex(int toIndex) {
    this.toIndex = toIndex;
  }

  public int getTotalPois() {
    return totalPois;
  }

  public void setTotalPois(int totalPois) {
    this.totalPois = totalPois;
  }

  public String getKeyword() {
    return keyword;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }


}
