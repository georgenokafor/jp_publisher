package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by ryan on 15-08-18.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TrainReservation
    extends Reservation {

  public TrainTrip reservationFor;

  @Override
  public DateTime getStartDateTime() {
    return (reservationFor != null) ? reservationFor.departTime : null;
  }

  @Override
  public DateTime getFinishDateTime() {
    return (reservationFor != null) ? reservationFor.arrivalTime : null;
  }

}
