package com.mapped.publisher.view;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.utils.Utils;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-01
 * Time: 11:31 AM
 * To change this template use File | Settings | File Templates.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DestinationGuideView
    extends BaseView
    implements Serializable {
  public String id;
  public Long noteId;

  public String destinationId;
  public String name;
  public String intro;
  public String description;
  public int status;
  public String createdById;
  public String streetAddr1;
  public String state;
  public String city;
  public String zipCode;
  public String country;
  public String locLat;
  public String locLong;
  public String tag;
  public int rank;
  public String landmark;

  public String coverUrl;
  public String coverName;
  public String destinationName;

  public String date;
  public String time;
  public String datePrint;
  public String time24hr;
  public String time12hr;


  public Long timestamp;

  public String timestampPrint;

  public List<AttachmentView> photos;
  public List<AttachmentView> videos;
  public List<AttachmentView> links;
  public List<AttachmentView> files;

  public boolean canEdit;
  public String cmpyId;
  public String tripId;
  public String tripName;
  public String tripStartDate;
  public boolean isMobilized;

  public int recommendationType = -1;


  public String tripBookingDetailId;

  // this object can have a nested hierarchy - so far we only have 1 level
  // this representation will be something like a chapter and pages - the parent is the chapter and the groupedViews are the pages for this chapter
  public List<DestinationGuideView> groupedViews;


  //configure the various datetime fields
  public void setupTimestamp(Long inTimestamp) {
    this.timestampPrint = Utils.formatDateTimePrint(inTimestamp);
    this.date = Utils.getDateString(inTimestamp);
    this.datePrint = Utils.formatDatePrint(inTimestamp);
    this.time = Utils.getTimeString(inTimestamp);
    this.timestamp = inTimestamp;
    this.time24hr = Utils.formatTime24(inTimestamp);
    this.time12hr = Utils.formatTime12(inTimestamp);
  }


  public static Comparator<DestinationGuideView> DestinationGuideViewComparator = new Comparator<DestinationGuideView>() {

    public int compare(DestinationGuideView t1, DestinationGuideView t2) {
      if (t1.timestamp != null && t2.timestamp != null) {
        int i =  t1.timestamp.compareTo(t2.timestamp);
        if (i == 0) {
          if (t1.rank > t2.rank)
            return 1;
          else
            return -1;
        } else
          return i;
      }
      return 0;
    }
  };
}


