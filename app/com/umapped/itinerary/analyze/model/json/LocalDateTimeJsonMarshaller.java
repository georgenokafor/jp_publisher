package com.umapped.itinerary.analyze.model.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by wei on 2017-03-01.
 */
public class LocalDateTimeJsonMarshaller {
  private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

  /**
   * Created by wei on 2017-03-01.
   */
  public static class LocalDateTimeDeserializer extends StdDeserializer<LocalDateTime> {
    public LocalDateTimeDeserializer() {
      super(LocalDateTime.class);
    }

    public LocalDateTimeDeserializer(Class<?> vc) {
      super(vc);
    }

    public LocalDateTimeDeserializer(JavaType valueType) {
      super(valueType);
    }

    public LocalDateTimeDeserializer(StdDeserializer<?> src) {
      super(src);
    }

    @Override public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
        throws IOException, JsonProcessingException {
      String text = jsonParser.getText();
      if (StringUtils.isEmpty(text)) {
        return null;
      } else {
        return LocalDateTime.parse(text, formatter);
      }
    }
  }

  /**
   * Created by wei on 2017-02-28.
   */
  public static class LocalDateTimeSerializer
      extends StdSerializer<LocalDateTime> {

    public LocalDateTimeSerializer() {
      super(LocalDateTime.class);
    }

    public LocalDateTimeSerializer(Class<LocalDateTime> t) {
      super(t);
    }

    public LocalDateTimeSerializer(JavaType type) {
      super(type);
    }

    public LocalDateTimeSerializer(Class<?> t, boolean dummy) {
      super(t, dummy);
    }

    public LocalDateTimeSerializer(StdSerializer<?> src) {
      super(src);
    }

    @Override
    public void serialize(LocalDateTime localDateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
        throws IOException {
      if (localDateTime != null) {
        jsonGenerator.writeString(formatter.format(localDateTime));
      }
    }
  }
}
