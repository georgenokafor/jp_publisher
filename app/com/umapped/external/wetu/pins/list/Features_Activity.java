package com.umapped.external.wetu.pins.list;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by george on 2016-06-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class Features_Activity {
  public int maxGroupSIze;
  public ArrayList<String> spokenLanguages;
  public ArrayList<String> facilities;
  public ArrayList<String> suggestedVisitorTypes;
  public ArrayList<String> weatherRequirement;
  public ArrayList<String> specialInterests;
  public OperatingHours operatingHours;

  public int getMaxGroupSIze() {
    return maxGroupSIze;
  }

  public void setMaxGroupSIze(int maxGroupSIze) {
    this.maxGroupSIze = maxGroupSIze;
  }

  public ArrayList<String> getSpokenLanguages() {
    return spokenLanguages;
  }

  public void setSpokenLanguages(ArrayList<String> spokenLanguages) {
    this.spokenLanguages = spokenLanguages;
  }

  public ArrayList<String> getFacilities() {
    return facilities;
  }

  public void setFacilities(ArrayList<String> facilities) {
    this.facilities = facilities;
  }

  public ArrayList<String> getSuggestedVisitorTypes() {
    return suggestedVisitorTypes;
  }

  public void setSuggestedVisitorTypes(ArrayList<String> suggestedVisitorTypes) {
    this.suggestedVisitorTypes = suggestedVisitorTypes;
  }

  public ArrayList<String> getWeatherRequirement() {
    return weatherRequirement;
  }

  public void setWeatherRequirement(ArrayList<String> weatherRequirement) {
    this.weatherRequirement = weatherRequirement;
  }

  public ArrayList<String> getSpecialInterests() {
    return specialInterests;
  }

  public void setSpecialInterests(ArrayList<String> specialInterests) {
    this.specialInterests = specialInterests;
  }

  public OperatingHours getOperatingHours() {
    return operatingHours;
  }

  public void setOperatingHours(OperatingHours operatingHours) {
    this.operatingHours = operatingHours;
  }

  @Override
  public String toString() {
    return "Features_Activity{" +
        "maxGroupSIze=" + maxGroupSIze +
        ", spokenLanguages=" + spokenLanguages +
        ", facilities=" + facilities +
        ", suggestedVisitorTypes=" + suggestedVisitorTypes +
        ", weatherRequirement=" + weatherRequirement +
        ", specialInterests=" + specialInterests +
        ", operatingHours=" + operatingHours +
        '}';
  }
}
