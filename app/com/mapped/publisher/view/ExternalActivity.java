package com.mapped.publisher.view;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 15-06-09.
 */


public class ExternalActivity extends BaseView{
  public enum ProviderType {
    SHORETRIPS;
  }

  public String tripCode;
  public ProviderType providerType;
  public String url;
  public String name;
  public String description;
  public String instructions;
  public List<String> imageUrls;

  public String restrictions;
  public List<ExternalActivityPrice> prices;
  public List<ExternalActivityTimes> times;
  public ArrayList<Integer> dayOfWeek; //jodattime constants

  public float locLat;
  public float locLong;

  public void addImg(String url) {
    if (imageUrls == null)
      imageUrls = new ArrayList<>();

    if (url != null) {
      imageUrls.add(url);
    }
  }

  public void addPrice(ExternalActivityPrice price) {
    if (prices == null)
      prices = new ArrayList<>();

    if (price != null) {
      prices.add(price);
    }
  }

  public void addTimes(ExternalActivityTimes time) {
    if (times == null)
      times = new ArrayList<>();

    if (time != null) {
      times.add(time);
    }
  }

  public void addDay (int day) {
    if (dayOfWeek == null) {
      dayOfWeek =  new ArrayList<>();
    }
    dayOfWeek.add(day);
  }

}
