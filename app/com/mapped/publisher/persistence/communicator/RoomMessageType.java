package com.mapped.publisher.persistence.communicator;

/**
 * Created by surge on 2015-07-20.
 */
public enum RoomMessageType {
  /**
   * Message should be decorated to attract the most attention
   */
  CRITICAL,
  /**
   * Message should be decorated to indicate that action might be needed
   */
  WARNING,
  /**
   * High priority message that does not require alert level decoration
   */
  HIGH,
  /**
   * Informational message
   */
  INFO,
  /**
   * Regular message no additional decorations
   */
  NORMAL,
  DEFAULT, //Backwards compatibility
  /**
   * Decorate message as a declaration of successful operation
   */
  SUCCESS
}
