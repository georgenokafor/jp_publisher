package com.mapped.publisher.audit;

/**
 * Activity performed on the component.
 *
 * Created by Serguei Moutovkin on 2014-05-21.
 */
public enum AuditActionType {
  ADD,
  MODIFY,
  DELETE,
  COPY,
  MERGE
}
