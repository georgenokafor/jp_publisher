package com.umapped.mobile.api;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;
import com.google.inject.Singleton;
import models.publisher.Account;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bouncycastle.crypto.modes.EAXBlockCipher;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by wei on 2017-07-17.
 */
@Singleton
public class TripQueries {
  private static String TRIP_LAST_CHANGE = "WITH trip_last_change as (SELECT distinct tripid, max(eventtime) lastUpdatedTs from trip_audit group by tripid) ";

  // get the deleted and published trips, deleted is needed to remove it on the client side
//  private static String GET_TRAVELER_CHANGED_ACTIVE_TRIPS = TRIP_LAST_CHANGE + "SELECT trip.tripid, trip.status from trip, account_trip_link atl, trip_last_change tlc "
//                                                            + " where trip.tripid = atl.tripid and trip.tripid = tlc.tripid"
//                                                            + " and trip.endtimestamp > :currentTs and atl.uid = :accountId"
//                                                            + " and tlc.lastUpdatedTs > :lastSyncTs and status <=0";
//
//  private static String GET_PUBLISHER_CHANGED_ACTIVE_TRIPS = TRIP_LAST_CHANGE + "SELECT trip.tripid, trip.status from trip, trip_last_change tlc "
//                                                            + " where trip.createdby = :userId and trip.tripid = tlc.tripid"
//                                                            + " and trip.endtimestamp > :currentTs"
//                                                            + " and tlc.lastUpdatedTs > :lastSyncTs and status <=0";

  private static String GET_TRAVELER_CHANGED_ACTIVE_TRIPS = "SELECT distinct trip.tripid, trip.status from trip, account_trip_link atl, trip_audit ta "
                                                            + " where trip.tripid = atl.tripid and trip.tripid = ta.tripid"
                                                            + " and trip.endtimestamp > :currentTs"
                                                            + " and atl.uid = :accountId"
                                                            + " and ta.eventtime > :lastSyncTs and (trip.status <=0 or (trip.status = 1 and trip.visibility=1))";

  private static String GET_PUBLISHER_CHANGED_ACTIVE_TRIPS = "SELECT distinct trip.tripid, trip.status from trip, trip_audit ta "
                                                             + " where trip.createdby = :userId and trip.tripid = ta.tripid"
                                                             + " and trip.endtimestamp > :currentTs"
                                                             + " and ta.eventtime > :lastSyncTs and (trip.status <=0 or (trip.status = 1 and trip.visibility=1))";

  private static String GET_DELETED_TRAVELER_TRIP = "SELECT distinct tripid from trip_audit where action='DELETE' and module='TRIP_TRAVELLER' and " +
                                                    " eventtime > :lastSyncTs and " +
                                                    " record like :email";

  /**
   * The trip endtTS is endDate's UTC 00:00:00, adjust it to the end of date
   * and consider the local server time and UTC, relax it for 48 hours
   * @return
   */

  private static long ADJUSTED_TS = 48 * 60 * 60 * 1000L;

  private long getCurrentAdjustedTs() {
    return System.currentTimeMillis() - ADJUSTED_TS;
  }

  public Set<Pair<String, Integer>> getTravelerChangedActiveTrips(Account traveler, long lastSyncTs) {
    SqlQuery query = Ebean.createSqlQuery(GET_TRAVELER_CHANGED_ACTIVE_TRIPS);
    if (traveler.getAccountType() == Account.AccountType.PUBLISHER) {
      query.setParameter("currentTs", getCurrentAdjustedTs());
    } else {
      query.setParameter("currentTs", 0);
    }
    
    query.setParameter("accountId", traveler.getUid());
    query.setParameter("lastSyncTs", lastSyncTs);
    List<SqlRow> rows = query.findList();
    return buildResult(rows);
  }

  public Set<Pair<String, Integer>> getPublisherChangedActiveTrips(Account publisher, long lastSyncTs) {
    SqlQuery query = Ebean.createSqlQuery(GET_PUBLISHER_CHANGED_ACTIVE_TRIPS);
    query.setParameter("userId", publisher.getLegacyId());
    query.setParameter("currentTs", getCurrentAdjustedTs());
    query.setParameter("lastSyncTs", lastSyncTs);
    List<SqlRow> rows = query.findList();
    return buildResult(rows);
  }

  // Do not call with lastSyncTs == 0
  public Set<Pair<String, Integer>> getDeleteTravelerTrip(Account traveler, long lastSyncTs) {
    SqlQuery query = Ebean.createSqlQuery(GET_DELETED_TRAVELER_TRIP);
    query.setParameter("lastSyncTs", lastSyncTs);
    query.setParameter("email", String.format("%%\"tripParticipantEmail\":\"%s\"%%", traveler.getEmail()));
    List<SqlRow> rows = query.findList();
    Set<Pair<String, Integer>> result = new HashSet();
    for (SqlRow row : rows) {
      result.add(new ImmutablePair<>(row.getString("tripid"), -1));
    }
    return result;
  }

  private Set<Pair<String, Integer>> buildResult(List<SqlRow> rows) {
    Set<Pair<String, Integer>> trips = new HashSet<>();
    for (SqlRow row : rows) {
      trips.add(new ImmutablePair<>(row.getString("tripid"), row.getInteger("status")));
    }
    return trips;
  }
}