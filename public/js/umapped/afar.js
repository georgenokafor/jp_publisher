(function () {

  console.log("Afar File Loaded");

  // Establish a reference to the `window` object, and save the previous value
  // of the `Afar` variable.
  var root = this;

  function Afar(el, options) {
    var self = this, options = options || {};

    if (!el) {
      throw new Error('Travel42: Missing required argument `el`');
    }

    self._options = options;
  }

  Afar.prototype = {};

})();

var initAfar = function(destId, tripId, date, onAddUrl) {
  console.warn("Afar is Initializing.");

  $(document).off('keypress', '#afarGuide');
  $(document).on('keypress', '#afarGuide', function(event){
    if(event.keyCode == 13) {
      $('#afarSearchBtn').trigger('click');
    }
  });

  $(document).undelegate('afarSearchBtn', 'click');
  $(document).delegate('#afarSearchBtn', 'click', function(){
    console.warn("Delegate afarSearchBtn called...");
    var term = $('#afarGuide').val();
    var url = '/guide/afar/search?term=' + term;
    getHtmlAndReplace(url, 'afarSearchResults', function(){
      //Initializing grid table here
      var table = $('#afarSearchResultsTable');
      if(isDataTable(table[0])){
        table.DataTable().destroy();
      }
      table.DataTable({
        paging: true,
        lengthChange: false,
        pageLength: 10,
        searching: false
      });
    });
  });

  $(document).off('click', 'a[data-target=afarChoice]');
  $(document).on('click', 'a[data-target=afarChoice]', 'click', function(){
    var city = $(this).data('key');
    var url = '/guide/afar/destination?city=' + city;
    $('#afarDestinationGuides').children().remove();
    getHtmlAndReplace(url, 'afarDestinationGuides', function(){
      $.jStorage.deleteKey(getJStorageKey('t42_guides'));
      $.jStorage.deleteKey(getJStorageKey('t42_afar'));

      $.fn.modal.defaults.maxHeight = function(){
        // subtract the height of the modal header and footer
        return $(window).height() - 165;
      };

      $('#afarGuidesModal').modal('show');
    });
  });

  $(document).off('click', 'a[data-target=t42ContentAdd]');
  $(document).on('click', 'a[data-target=t42ContentAdd]',  function(event) {
    var destId = $(this).data('dest');
    var seqNum = $(this).data('number');
    var tab = $(this).data('tab');
    var afar = $(this).data('afar');

    var guides = $.jStorage.get(getJStorageKey('t42_guides'));
    if (guides == null) {
      guides = {};
    }

    if(guides[destId] == null) {
      guides[destId] = {};
    }

    var currGuides = guides[destId][tab];

    if(currGuides == null) {
      currGuides = [];
    }
    var isNew = true;
    for (var idx = 0; idx < currGuides.length; idx++) { //Checking for duplicates
      if (currGuides[idx] == seqNum) {
        isNew = false;
      }
    }

    if (isNew) {
      console.log(currGuides);
      currGuides.push(seqNum);
      guides[destId][tab] = currGuides;
      var aKeys = $.jStorage.get(getJStorageKey('t42_afar'));
      console.log(aKeys);
      console.log(guides);
      if(aKeys == null) {
        aKeys = [];
      }
      console.log(aKeys);
      aKeys.push(afar);
      guides[destId][tab][afar] = aKeys;

      $.jStorage.set(getJStorageKey('t42_guides'), guides);
      $.jStorage.set(getJStorageKey('t42_afar'), aKeys);
    }
    $('#' + tab + '_count').text(currGuides.length);
    $(this).attr('data-target', 't42ContentRemove');
    $(this).children().remove();
    $(this).append($('<i class="fa fa-minus-circle fa-lg"></i>'));
  });

  $(document).off('click', 'a[data-target=t42ContentRemove]');
  $(document).on('click', 'a[data-target=t42ContentRemove]', function(event) {
    var destId = $(this).data('dest');
    var seqNum = $(this).data('number');
    var tab = $(this).data('tab');
    var afar = $(this).data('afar');

    var counter = $('#' + tab + '_count');

    var guides = $.jStorage.get(getJStorageKey('t42_guides'));
    if (guides == null) {
      guides = {};
    }
    if(guides[destId] == null) {
      guides[destId] = {};
    }
    var currGuides = guides[destId][tab];
    for (var idx = 0; idx < currGuides.length; idx++) { //Checking for duplicates
      if (currGuides[idx] == seqNum) {
        currGuides.splice(idx, 1);
        break;
      }
    }
    guides[destId][tab] = currGuides;
    $.jStorage.set(getJStorageKey('t42_guides'), guides);

    var aKey = $.jStorage.get(getJStorageKey('t42_afar'));
    var newAKey = aKey;
    for (var idx = 0; idx < newAKey.length; idx++) { //Checking for duplicates
      if (newAKey[idx] == afar) {
        newAKey.splice(idx, 1);
        break;
      }
    }
    aKey = newAKey;
    $.jStorage.set(getJStorageKey('t42_afar'), aKey);
    counter.text(currGuides.length);
    $(this).attr('data-target', 't42ContentAdd');
    $(this).children().remove();
    $(this).append($('<i class="fa fa-plus-circle fa-lg"></i>'));
  });


  $(document).off('click', 'a[data-target=t42HotelAdd]');
  $(document).on('click', 'a[data-target=t42HotelAdd]',  function(event) {
    var hotelId = $(this).data('hotel');
    var tab = $(this).data('tab');

    var hotels = $.jStorage.get(getJStorageKey('t42_hotels'));
    if (hotels == null) {
      hotels = [];
    }

    if(hotels[hotelId] == null) {
      hotels.push(hotelId);
    }

    $.jStorage.set(getJStorageKey('t42_hotels'), hotels);

    $('#' + tab + '_count').text(hotels.length);
    $(this).attr('data-target', 't42HotelRemove');
    $(this).children().remove();
    $(this).append($('<i class="fa fa-minus-circle fa-lg"></i>'));
  });


  $(document).off('click', 'a[data-target=t42HotelRemove]');
  $(document).on('click', 'a[data-target=t42HotelRemove]', function(event) {
    var hotelId = $(this).data('hotel');
    var tab = $(this).data('tab');

    var counter = $('#' + tab + '_count');

    var hotels = $.jStorage.get(getJStorageKey('t42_hotels'));
    if (hotels == null) {
      hotels = [];
    }

    var idx = hotels.indexOf(hotelId);
    if(idx >= 0) {
      hotels.splice(idx, 1); //Removing
    }

    $.jStorage.set(getJStorageKey('t42_hotels'), hotels);
    counter.text(hotels.length);
    $(this).attr('data-target', 't42ContentAdd');
    $(this).children().remove();
    $(this).append($('<i class="fa fa-plus-circle fa-lg"></i>'));
  });

  $(document).off('click', 'button[data-target=afarAddSelected]');
  $(document).on('click', 'button[data-target=afarAddSelected]', function(event){
    var url = '';

    if(destId) {
      url = '/guide/afar/todest/' + destId;
    }

    if(tripId) {
      url = '/trips/' + tripId + '/afarnotes?date=' + date;
    }

    var guides =  $.jStorage.get(getJStorageKey('t42_guides'));
    var hotels =  $.jStorage.get(getJStorageKey('t42_hotels'));
    var afar = $.jStorage.get(getJStorageKey('t42_afar'));
    console.log(afar);

    if(guides == null && hotels == null) {
      return;
    }

    var packet = {
      destinations: guides,
      hotels: hotels,
      afar: afar
    };

    $.ajax(
        {
          type: "POST",
          url: url,
          data: JSON.stringify(packet),
          contentType: "application/json; charset=utf-8",
          dataType: "json"
        }).always(
        function (data, textStatus, jqXHR) {
          if (textStatus == 'success') {
            $.jStorage.deleteKey(getJStorageKey('t42_guides'));
            $.jStorage.deleteKey(getJStorageKey('t42_hotels'));
            $.jStorage.deleteKey(getJStorageKey('t42_afar'));

            $('#afarGuidesModal').modal('hide');
            $('#afarSearchModal').modal('hide');

            if(onAddUrl) {
              getPage(onAddUrl);
            } else {
              reloadPage();
            }
          }
          else if (textStatus == 'parsererror') {
            //Need to go to login page
            window.location.href = "/";
          }
          else if (textStatus != 'abort') {
            switch (jqXHR.status) {
              case 503:
              case 504:
                headLineAlertDisplay("Request timeout. Please try again in a couple of minutes.");
                break;
              default:
                headLineAlertDisplay("Unknown System Error has occurred - " + textStatus);
            }
          }
        });
  });
};