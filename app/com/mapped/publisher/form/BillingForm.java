package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-03-26
 * Time: 11:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class BillingForm {
    private String inStartDate;
    private String inEndDate;
    private String inCmpyId;
    private String inReportType;

    public String getInStartDate() {
        return inStartDate;
    }

    public void setInStartDate(String inStartDate) {
        this.inStartDate = inStartDate;
    }

    public String getInEndDate() {
        return inEndDate;
    }

    public void setInEndDate(String inEndDate) {
        this.inEndDate = inEndDate;
    }

    public String getInCmpyId() {
        return inCmpyId;
    }

    public void setInCmpyId(String inCmpyId) {
        this.inCmpyId = inCmpyId;
    }

    public String getInReportType() {
        return inReportType;
    }

    public void setInReportType(String inReportType) {
        this.inReportType = inReportType;
    }
}
