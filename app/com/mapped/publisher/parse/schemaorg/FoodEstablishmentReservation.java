package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by surge on 2015-05-12.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FoodEstablishmentReservation
    extends Reservation {
  public DateTime          startTime;
  public DateTime          endTime;
  public int               partySize;
  public FoodEstablishment reservationFor;
  public Place             location;

  @Override
  public DateTime getStartDateTime() {
    return startTime;
  }

  @Override
  public DateTime getFinishDateTime() {
    return endTime;
  }
}
