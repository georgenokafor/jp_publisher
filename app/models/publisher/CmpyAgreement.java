package models.publisher;

import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-16
 * Time: 1:05 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="cmpy_agreement")
public class CmpyAgreement extends Model{

    @Id
    public String pk;


    @Constraints.Required
    public String cmpyid;
    public String agreementversion;
    public Boolean acceptauthorized;
    public String createdipaddr;

    public int status;
    public Long createdtimestamp;
    public Long lastupdatedtimestamp;
    public String createdby;
    public String modifiedby;

    @Version
    public int version;

    public static Model.Finder<String,CmpyAgreement> find = new Finder(CmpyAgreement.class);

    public static List<CmpyAgreement> findByCmpyId (String cmpyid) {
        return find.where().eq("cmpyid", cmpyid).orderBy("lastupdatedtimestamp desc").findList();

    }

    public static boolean hasAccepted (String cmpyid, String agreementversion) {
        List <CmpyAgreement> agreements =  find.where().eq("cmpyid", cmpyid).eq("agreementVersion", agreementversion).eq("status", 0).findList();
        if (agreements != null && agreements.size() > 0) {
            return true;
        }

        return false;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getCmpyid() {
        return cmpyid;
    }

    public void setCmpyid(String cmpyid) {
        this.cmpyid = cmpyid;
    }

    public String getAgreementversion() {
        return agreementversion;
    }

    public void setAgreementversion(String agreementversion) {
        this.agreementversion = agreementversion;
    }

    public Boolean getAcceptauthorized() {
        return acceptauthorized;
    }

    public void setAcceptauthorized(Boolean acceptauthorized) {
        this.acceptauthorized = acceptauthorized;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getCreatedtimestamp() {
        return createdtimestamp;
    }

    public void setCreatedtimestamp(Long createdtimestamp) {
        this.createdtimestamp = createdtimestamp;
    }

    public Long getLastupdatedtimestamp() {
        return lastupdatedtimestamp;
    }

    public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
        this.lastupdatedtimestamp = lastupdatedtimestamp;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getCreatedipaddr() {
        return createdipaddr;
    }

    public void setCreatedipaddr(String createdipaddr) {
        this.createdipaddr = createdipaddr;
    }

    public static Finder<String, CmpyAgreement> getFind() {
        return find;
    }

    public static void setFind(Finder<String, CmpyAgreement> find) {
        CmpyAgreement.find = find;
    }
}
