package models.publisher.utils.reservation.migration;

import java.util.EnumMap;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.umapped.persistence.enums.ReservationType;

import models.publisher.TripDetail;
import models.utils.migration.AbstractMigrator;
import models.utils.migration.Migrator;

/**
 * The manager of all TripDetail migration, who delegates to specific
 * type of trip detail migrator
 * 
 * @author wei
 *
 */
@Singleton
public class TripDetailMigrator extends AbstractMigrator<TripDetail> {

  private EnumMap<ReservationType, Migrator<TripDetail>> migratorMap;
  private Migrator<TripDetail> NoOpMigrator = (entity) -> { return false; };
  
  @Inject
  public TripDetailMigrator() {
    migratorMap = new EnumMap<>(ReservationType.class);
    migratorMap.put(ReservationType.FLIGHT, new FlightTripDetailMigrator());
    migratorMap.put(ReservationType.HOTEL, new HotelTripDeteailMigrator());
    migratorMap.put(ReservationType.CRUISE, new CruiseTripDetailMigrator());
    migratorMap.put(ReservationType.TRANSPORT, new TransportTripDetailMigrator());
    migratorMap.put(ReservationType.ACTIVITY, new ActivityTripDetailMigrator());
   
    // set migrator for sub types of activity and transport
    for (ReservationType rt : ReservationType.values()) {
      ReservationType prt = rt.getParent();
      if (ReservationType.ACTIVITY == prt)
        migratorMap.put(rt, migratorMap.get(ReservationType.ACTIVITY));
      else if (ReservationType.TRANSPORT == prt)
        migratorMap.put(rt, migratorMap.get(ReservationType.TRANSPORT));
    }
  }

  /**
   * If the reservationData is not null, it has been migrated
   */
  @Override
  public boolean isMigrated(TripDetail entity) {
    return entity.getReservationData() != null;
  }

  @Override
  protected boolean doMigrate(TripDetail entity) {
    Migrator<TripDetail> m = migratorMap.getOrDefault(entity.detailtypeid, NoOpMigrator);
    return m.migrate(entity);
  }
}
