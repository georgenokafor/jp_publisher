package com.mapped.publisher.view;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-20
 * Time: 9:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class MobileStatsView {
    public int numActiveUsers;
    public int numNewUsers;

    public int numActiveEvents;
    public int numActiveTrips;

    public int numNewEvents;
    public int numNewTrips;

    public int numActiveShares;
    public int numActiveFullShares;
    public int numActiveRecommendShares;
    public int numActiveViewShares;

    public int numNewShares;
    public int numNewFullShares;
    public int numNewRecommendShares;
    public int numNewViewShares;
    public int numDeletedShares;



    public int numDeletedEvents;
    public int numModifiedEvents;
    public List<MobileSessionsView> sessions;
}
