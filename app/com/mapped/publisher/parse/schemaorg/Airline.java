package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * An organization that provides flights for passengers.
 * https://schema.org/Airline
 *
 * Created by surge on 2015-05-11.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Airline
    extends Organization {

    public String boardingPolicy;

  /**
   * [Text] IATA identifier for an airline or airport.
   */
  public String iataCode;
}
