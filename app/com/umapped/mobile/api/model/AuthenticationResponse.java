package com.umapped.mobile.api.model;

import java.io.Serializable;

/**
 * Created by wei on 2017-06-28.
 */
public class AuthenticationResponse extends AuthenticatedUser implements Serializable {


  private AuthenticationStatus status;

  public AuthenticationResponse() {
    this.status = AuthenticationStatus.USER_NOT_FOUND;
  }


  public AuthenticationResponse(AuthenticationStatus status) {
    this.status = status;
  }


  public AuthenticationStatus getStatus() {
    return status;
  }

  public void setStatus(AuthenticationStatus status) {
    this.status = status;
  }


}
