package controllers;

import actors.ActorsHelper;
import actors.EmailAttachmentParserActor;
import actors.EmailBodyParserActor;
import actors.SupervisorActor;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.mapped.common.CoreConstants;
import com.mapped.common.EmailMgr;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.api.ApiConstants;
import com.mapped.publisher.api.InternalMsg;
import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.audit.AuditModuleType;
import com.mapped.publisher.audit.event.AuditTrip;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.PreviewEmailForm;
import com.mapped.publisher.form.SendGridForm;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.WorldMateResult;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.mapped.publisher.parse.traxo.TraxoHelper;
import com.mapped.publisher.parse.worldmate.Header;
import com.mapped.publisher.parse.worldmate.WorldmateParsingResult;
import com.mapped.publisher.persistence.RecordState;
import com.mapped.publisher.persistence.RecordStatus;
import com.mapped.publisher.persistence.VOModeller;
import com.mapped.publisher.persistence.bookingapi.BookingAPIMgr;
import com.mapped.publisher.persistence.bookingapi.BookingRS;
import com.mapped.publisher.utils.*;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.EmailLogView;
import com.mapped.publisher.view.EmailParseWarningView;
import com.umapped.BodyParsers.TolerantText1Mb;
import com.umapped.external.sendgrid.webhooks.EventWebhook;
import models.publisher.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.util.MimeMessageParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.libs.Json;
import play.mvc.With;


import javax.activation.DataSource;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.persistence.OptimisticLockException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.ObjectMapper;
import play.twirl.api.Html;

import static com.mapped.common.CoreConstants.HOST_URL;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-09
 * Time: 3:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class EmailController
    extends Controller {

  @Inject
  FormFactory formFactory;

  final static String SQS_CALLER_ID = "EmailContoller::processEmail";
  final static int MAX_EMAIL_SIZE = 25 * 1024 * 1024; //25 megabYtes, max possible with SendGrid
  public static final String EMAIL_HEADER_EPLID     = "X-Umapped-EPL-ID";
  public static final String EMAIL_HEADER_WM_SUB    = "Subject";
  public static final String EMAIL_FROM             = "From";

  public static final int RAW_FILE_EXPIRE_DELAY = 2 * 365; //Delay by 2 YEARS
  final private static Pattern accessCode = Pattern.compile("^.*<([0-9a-zA-Z+]+)@.*>$");
  final private static Pattern emailAddr = Pattern.compile("^.*<(.+@.+)>$");
  final private static Pattern emailName = Pattern.compile("^(.*)\\s*<.*>$");
  final private static Pattern subjectLine = Pattern.compile("^\\s*[>]*\\s*Subject\\s*:\\s*(.*)$",
                                                             Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
  private static final int DB_UPDATE_RETRIES = 3;

  private static final boolean DEBUG = true;
  private static void s3UploadRaw(EmailParseLog epa, SendGridForm message, String accessCode) {
    String isoTimestamp = Utils.getISO8601Timestamp(epa.getCreatedTs());
    String filename = epa.getSubject().replaceAll("(\\s+|/)", "_").replace("\u00A0","_"); //Replacing non breaking space
    StringBuilder s3Path = new StringBuilder();
    s3Path.append(accessCode);
    s3Path.append('/');
    s3Path.append(Utils.getISO8601Date(epa.getCreatedTs()));
    s3Path.append('/');
    s3Path.append(epa.getPk());
    s3Path.append('_');
    s3Path.append(isoTimestamp);
    s3Path.append('_');
    s3Path.append(filename).append(".eml");

    FileSrc fSrc = FileSrc.find.byId(FileSrc.FileSrcType.EMAIL_RAW);
    String bucket = fSrc.getRealBucketName();
    LstFileType ft = LstFileType.find.byId("eml");

    S3Util.S3FileInfo s3Info = S3Util.S3FileInfo.buildInfo(bucket,
                                                           s3Path.toString(),
                                                           message.getEmail(),
                                                           ft.getProperMimeType());
    FileInfo file = FileInfo.byHash(s3Info.hash);
    if(file == null) { //New upload needed
      if(S3Util.uploadS3Data(s3Info)) {
        file = FileInfo.buildFileInfo(epa.getAccount().getUid(), fSrc, RAW_FILE_EXPIRE_DELAY);
        file.setBucket(bucket);
        file.setFilepath(s3Info.key);
        file.setFilename(filename);
        file.setFilesize(s3Info.size);
        file.setHash(s3Info.hash); //MD5 Hash for now
        file.setFiletype(ft);
        file.setState(RecordState.ACTIVE);
        file.setCreatedBy(epa.getAccount());
        file.save();
      } else {
        Log.err("EMAIL_Stage_A: Failed to upload RAW Email to S3: " + s3Path);
        epa.setState(EmailParseLog.EmailState.ERROR);
      }
    }

    if (file != null) {
      epa.setFile(file);
      epa.setState(EmailParseLog.EmailState.PERSISTED);
      epa.save();
    }
  }

  private static String s3GetRaw(FileInfo f) {
    try {
      S3Object so = S3Util.getS3File(f.getBucket(), f.getFilepath());
      S3ObjectInputStream is = so.getObjectContent();
      StringWriter writer = new StringWriter();
      IOUtils.copy(is, writer, "US-ASCII");
      return writer.toString();
    } catch (Exception e) {
      Log.err("EMAIL_Stage_B: Failed to download RAW from S3", e);
      e.printStackTrace();
      return null;
    }
  }

  /**
   * Takes string in format commonly used in mime headers:
   * UMapped Email API <h7mzY3aFV1@api.umapped.com>
   * and extracts email address h7mzY3aFV1@api.umapped.com
   * @return
   */
  public static String extractEmailAddress(String input) {
    String result = null;
    if (input == null) {
      return result;
    }
    if(input.indexOf('<') == -1) {
      return input;
    }
    Matcher em = emailAddr.matcher(input);
    if(em.matches()) {
      result = em.group(1);
    }
    return result;
  }

  /**
   * Takes string in format commonly used in mime headers:
   * UMapped Email API <h7mzY3aFV1@api.umapped.com>
   * and extracts email address h7mzY3aFV1@api.umapped.com
   * @return
   */
  public static String extractEmailName(String input) {
    String result;
    if(input.indexOf('<') == -1) {
      return "";
    }

    Matcher em = emailName.matcher(input);
    if(em.matches()) {
      result = em.group(1);
      return result;
    }
    return "";
  }

  /**
   * @param input
   * @return null if no code can be extracted
   */
  public static String extractAccessCode(String input) {
    if(input.indexOf('<') == -1) {
      int atSign = input.indexOf('@');
      if(atSign == -1) {
        return null;
      }
      return input.substring(0, atSign);
    }

    Matcher m  = accessCode.matcher(input);
    if(m.matches()) {
      return  m.group(1);
    }

    return null;
  }

  @BodyParser.Of(BodyParser.MultipartFormData.class)
  public Result processSendGridRaw() {
    Log.err("email_processor: Step 1" );

    StopWatch sw = new StopWatch();
    sw.start();

    Form<SendGridForm> emailForm = formFactory.form(SendGridForm.class);
    SendGridForm message = emailForm.bindFromRequest().get();

    //0. Keeping up to date state for this e-mail
    EmailParseLog epl = EmailParseLog.buildRecord();

    epl.setSpamScore(message.getSpam_score());
    epl.setSpamReport(message.getSpam_report());
    Log.err("email_processor: Step 2" + message.getFrom() + " | " + message.getSubject() + " | " + message.getTo() );

    String fromAddr = extractEmailAddress(message.getFrom());
    //Company authorization mode
    if(fromAddr == null) {
      //EPLID is not needed here
      Log.err("EMAIL_Stage_A: Can't parse From field (address extraction):" + message.getFrom());
      return ok();
    }

    String fromName = extractEmailName(message.getFrom()).trim();
    epl.setFromAddr(fromAddr);
    epl.setFromName(fromName);
    epl.setSubject(message.getSubject());

    //1. Checking to see if we got full email
    if (message.getEmail() == null || message.getEmail().length() == 0) {
      //epa.setState(EmailParseLog.EmailState.ERROR);
      //epa.setParseMs(sw.getTime());
      //epa.save();
      Log.err("EMAIL_Stage_A: From" + epl.getFromAddr() + " to: " + message.getTo() +
              " has no body (EPLID:" + epl.getPk() + " )");
      return ok();
    }

    //2. Authenticate and Authorize
    String token = extractAccessCode(message.getTo());
    if (token == null) {
      Log.err("EMAIL_Stage_A: Failed to extract access code: " + message.getTo() +
              "(EPLID: " + epl.getPk() + ")");
      return ok();
    }

    //2.a Check if "pass-through" to WorldMate suffix is there
    int suffixStartPos = token.indexOf("+parse");
    boolean isPassthrough = false;
    if (suffixStartPos > 0) {
      isPassthrough = true;
      token = token.substring(0, suffixStartPos);
    }

    int parseTagPos = message.getSubject().indexOf("#parse");
    if (!isPassthrough && parseTagPos > 0) {
      epl.setSubject(message.getSubject().replaceAll("\\s*#parse\\s*",""));
      isPassthrough = true;
    }

    EmailToken tkn = EmailToken.find.byId(token.toLowerCase());
    if(tkn == null) {
      Log.err("EMAIL_Stage_A: Failed to find access token: " + token +  "(EPLID: " + epl.getPk() + ")");
      return ok();
    }

    if(tkn.getState() == APPConstants.STATUS_DELETED) {
      Log.err("EMAIL_Stage_A: Found Expired email token: " + token + "(EPLID: " + epl.getPk() + ")");
      return ok();
    }
    epl.setToken(tkn);

    if(tkn.getType() == EmailType.EmailTypeName.PARSE_CMPY) {

      Account account = Account.findByEmail(fromAddr);
      if(account == null || account.getState() != RecordStatus.ACTIVE) {
        Log.err("EMAIL_Stage_A: Error: Can't find user for email: " + fromAddr + " (EPLID:" +epl.getPk() + ")");
        return ok();
      }

      epl.setAccount(account);

      AccountCmpyLink acl = AccountCmpyLink.findActiveAccountAndCmpy(account.getUid(), tkn.getBelongId());
      if(acl == null || acl.getState() != RecordStatus.ACTIVE) {
        epl.setState(EmailParseLog.EmailState.AUTH).save();
        Log.err("EMAIL_Stage_A: Error: Account found but invalid token for email: " + fromAddr +
                "(EPLID:" +epl.getPk() + ") Account ID: " + account.getUid());
        return ok();
      }

      Log.debug("EMAIL_Stage_A: userId: " + account.getLegacyId() + "(EPLID: " + epl.getPk() + ")");
    }

    //3. Saving email to S3
    s3UploadRaw(epl, message, token);

    //4. Saving email to cache and sending SQS Message
    try {
      if (epl.getState() != EmailParseLog.EmailState.ERROR) {
        try { //Catch only memcached error
          CacheMgr.set(APPConstants.CACHE_EMAIL_RAW_PREFIX + Long.toString(epl.getPk()),
                       message.getEmail(),
                       APPConstants.CACHE_EMAIL_RAW_EXPIRY_SEC);
        }catch (Exception e) {
          Log.info("Failed to save RAW in Cache. Will read from S3 in the next phase. (EPLID: " + epl.getPk() + ")");
        }

        if(isPassthrough) {
          Log.debug("EMAIL_Stage_A: WorldMate Passthrough (EPLID:" + epl.getPk() + ") in " + sw.getTime() + "ms");
          worldMateForward(epl);
        } else {
          //build msg to queue
          InternalMsg msg = new InternalMsg();
          msg.setCallerId(SQS_CALLER_ID); //
          msg.setMsgTimestamp(System.currentTimeMillis());
          msg.setReqId(epl.getPk()); //match the key to the audit record
          msg.setServiceId(ApiConstants.MSG_EMAIL);
          msg.setStatus(ApiConstants.STATUS_OK);
          String sqsUrl = ConfigMgr.getAppParameter(CoreConstants.SQS_Q_URL);
          epl.setState(EmailParseLog.EmailState.SCHEDULED);
          epl.setParseMs(sw.getTime());
          epl.update();
          SQSUtils.sendMessage(sqsUrl, msg);
          sw.stop();
          Log.debug("EMAIL_Stage_A: SQS Queued (EPLID:" + epl.getPk() + ") in " + sw.getTime() + "ms");
        }
      }
    } catch (Exception e) {
      Log.err("EMAIL_Stage_A: Failed to queue SQS message. EPLID:" + epl.getPk() + ") in " + sw.getTime() + "ms", e);
      e.printStackTrace();
      epl.setState(EmailParseLog.EmailState.ERROR);
      epl.setParseMs(sw.getTime());
      epl.update();
    }

    return ok(); //Thank you SendGrid your job is done
  }



  public Result processWebhooks() {
    StopWatch sw = new StopWatch();
    sw.start();

    try {
      //1. Get email information from the ballback
      JsonNode jsonMsg = request().body().asJson();
      EventWebhook[] details = Json.fromJson(jsonMsg, EventWebhook[].class);

      Log.info("EmailController:processWebhook - SENDGRID: Receiving Webhook Response from SendGrid");

      for (EventWebhook ew : details) {
        EmailLog emailLog = EmailLog.find.byId(ew.getMsgId());
        if (emailLog != null) {
          switch (ew.getEvent()) {
            case "open":
              emailLog.setStatus(EmailLog.EmailStatus.OPENED);
              emailLog.setOpened_ts(new Timestamp(ew.getTimestamp() * 1000L));
              emailLog.setOpen_counter(emailLog.getOpen_counter() + 1);
              emailLog.setModified_ts(new Timestamp(System.currentTimeMillis()));
              break;
            case "click":
              emailLog.setStatus(EmailLog.EmailStatus.CLICKED);
              emailLog.setClicked_ts(new Timestamp(ew.getTimestamp() * 1000L));
              emailLog.setClick_counter(emailLog.getClick_counter() + 1);
              emailLog.setModified_ts(new Timestamp(System.currentTimeMillis()));
              break;
            case "processed":
              emailLog.setStatus(EmailLog.EmailStatus.PROCESSED);
              emailLog.setProcessed_ts(new Timestamp(ew.getTimestamp() * 1000L));
              emailLog.setModified_ts(new Timestamp(System.currentTimeMillis()));
              break;
            case "delivered":
              emailLog.setStatus(EmailLog.EmailStatus.DELIVERED);
              emailLog.setDelivered_ts(new Timestamp(ew.getTimestamp() * 1000L));
              emailLog.setModified_ts(new Timestamp(System.currentTimeMillis()));
              break;
            case "dropped":
              emailLog.setStatus(EmailLog.EmailStatus.DROPPED);
              emailLog.setFailed_ts(new Timestamp(ew.getTimestamp() * 1000L));
              emailLog.setModified_ts(new Timestamp(System.currentTimeMillis()));
              break;
            case "deferred":
              emailLog.setStatus(EmailLog.EmailStatus.DEFERRED);
              emailLog.setFailed_ts(new Timestamp(ew.getTimestamp() * 1000L));
              emailLog.setModified_ts(new Timestamp(System.currentTimeMillis()));
              break;
            case "bounce":
              emailLog.setStatus(EmailLog.EmailStatus.BOUNCED);
              emailLog.setFailed_ts(new Timestamp(ew.getTimestamp() * 1000L));
              emailLog.setModified_ts(new Timestamp(System.currentTimeMillis()));
              break;
          }
          try {
            emailLog.update();
          } catch (OptimisticLockException ole) {
            Log.err("EmailController:processWebhook - Optimistic lock exception: PK: " + emailLog.getPk());
          }
          try {
            if ((ew.getEvent().equals("bounce") || ew.getEvent().equals("dropped")) && emailLog.getType() == EmailLog.EmailTypes.TRAVELLER_ITINERARY) {
              //notify the agent that the email was not delivered
              Trip t = Trip.findByPK(emailLog.getMeta());
              TripPublishHistory tph = TripPublishHistory.getLastPublished(t.getTripid(), null);
              if (t != null && tph != null) {
                Account agent = Account.findActiveByLegacyId(tph.getCreatedby());
                String url = ConfigMgr.getAppParameter(HOST_URL) + "/home#/tours/newTour/publish?tripId=" + t.getTripid();
                List<String> emails = new ArrayList<>();
                emails.add(agent.getEmail());
                UmappedEmail email = UmappedEmail.buildDefault();
                email.addReplyTo("support@umapped.com");
                email.addTo(agent.getEmail());
                email.setFrom("no-reply@umapped.com", "Umapped");
                email.withHtml(views.html.email.bounceEmailNotification.render(t.getName(), url, emailLog.getTo_email()).toString())
                        .withSubject("Itinerary Email Delivery Failed for " + t.getName())
                        .withType(UmappedEmail.SubjectType.DIRECT)
                        .withEmailType(EmailLog.EmailTypes.WARNING)
                        .withTripId(t.tripid)
                        .withAccountUid(agent.getUid())
                        .withToList(emails)
                        .buildAndSend();

              }

            }
          } catch (Exception e) {
            Log.err("EmailController:processWebhook - Cannot sent bounce notification: PK: " + emailLog.getPk());

          }
        }
        Log.info("EmailController:processWebhook - Finished processing Event: '" + ew.getEvent().toUpperCase() + "' for email log: " + ew.getMsgId());
      }
    } catch (Exception e) {
      Log.err("EmailController:processWebhook - Error processing webhook", e);
    }

    BaseView view = new BaseView();
    return ok(views.html.common.message.render(view));
  }


  public Result processSQSCallback() {
    StopWatch sw = new StopWatch();
    sw.start();

    //1. Get email information from the ballback
    JsonNode jsonMsg = request().body().asJson();
    InternalMsg msg = InternalMsg.fromJson(jsonMsg);
    EmailParseLog epl = EmailParseLog.find.byId(msg.getReqId());
    if(epl == null) {
      Log.err("EMAIL_Stage_B: Corrupted SQS message. Can't find log entry. (EPLID:" + msg.getReqId() +")");
      return ok();
    }
    epl.setState(EmailParseLog.EmailState.PREPROCESSING);

    //2. Get RAW email string either from Cache or S3
    String raw = (String) CacheMgr.get(APPConstants.CACHE_EMAIL_RAW_PREFIX + epl.getPkString());
    if(raw == null) {
      //Need to retrieve from S3 as cache was gone
      raw = s3GetRaw(epl.getFile());
    }

    //3. Parsing MIME RAW email message
    try {
      Session session = EmailMgr.getDefault().getSession();
      MimeMessage mm = new MimeMessage(session, new ByteArrayInputStream(raw.getBytes()));
      MimeMessageParser mimeParser = new MimeMessageParser(mm).parse();

      if(DEBUG) { //Debug only here
        StringBuilder debugStr = new StringBuilder("EMAIL_Stage_B: MIME Message Received");
        debugStr.append("\nis multipart: ")
                .append(mimeParser.isMultipart())
                .append("\nhas attachments: ")
                .append(mimeParser.hasAttachments())
                .append("\nhas html: ")
                .append(mimeParser.hasHtmlContent())
                .append("\nhas text: ")
                .append(mimeParser.hasPlainContent());

        for (DataSource ds : mimeParser.getAttachmentList()) {
          debugStr.append("\nattachment type: " + ds.getContentType());
          debugStr.append("\nattachment name: " + ds.getName());
          debugStr.append("\nattachment size: " + ds.getInputStream().available());
        }
        debugStr.append("\nparse time: ").append(sw.getTime()).append("ms");
        Log.debug(debugStr.toString());
      }

      //3.a check if we have message forwarded as attachment (i.e. message/rfc822 type) - then process only that one
      boolean isFWDAsAttachment = false;
      for (DataSource ds : mimeParser.getAttachmentList()) {
        if(ds.getContentType().equals("message/rfc822")) {
          isFWDAsAttachment = true;
          Session fwdSession = EmailMgr.getDefault().getSession();
          MimeMessage  mmFwd = new MimeMessage(fwdSession, ds.getInputStream());
          MimeMessageParser mmFwdParser = new MimeMessageParser(mmFwd).parse();
          epl.setSrc(EmailParseLog.EmailSource.FORWARD_ATTACH);
          handleMimeMessage(epl, mmFwdParser);
          break; //TODO: For now handling only single forwarded as attachment message
        }
      }
      if(!isFWDAsAttachment) {
        handleMimeMessage(epl, mimeParser);
      }

      for (int retry = 0; retry < DB_UPDATE_RETRIES; retry++) {
        try {
          epl.refresh();
          epl.addParseMs(sw.getTime());
          epl.update();
          break;
        }
        catch (OptimisticLockException ole) {
          //Skipping on purpose
        }
      }

    }catch (Exception e) {
      Log.err("Failed to process email attachments (EPLID:" + epl.getPk() + ")", e);
      e.printStackTrace();

      for (int retry = 0; retry < DB_UPDATE_RETRIES; retry++) {
        try {
          epl.refresh();
          epl.addParseMs(sw.getTime());
          epl.setState(EmailParseLog.EmailState.ERROR);
          epl.update();
          break;
        }
        catch (OptimisticLockException ole) {
          //Skipping on purpose
        }
      }
    }
    sw.stop();
    Log.debug("Finished processing email (EPLID:" +epl.getPk() + ") within " + sw.getTime() + "ms" );
    return ok();
  }

  private static void handleMimeMessage(EmailParseLog epl, MimeMessageParser mimeParser) {
    switch(epl.getToken().getType()) {
      case PARSE_CMPY:
        handleTokenCompany(epl, mimeParser);
        break;
      case PARSE_USER:
        handleTokenUser(epl, mimeParser);
        break;
      case PARSE_TRVL:
        handleTokenTrvlr(epl, mimeParser);
        break;
      case FEED_POI:
        handleTokenPoi(epl, mimeParser);
        break;
      case FEED_TMPLT:
        handleTokenTmplt(epl, mimeParser);
        break;
    }
  }

  private static boolean handleTokenCompany(EmailParseLog epl, MimeMessageParser mimeParser) {
    Trip trip;

    boolean isNewTrip = false;

    String subject = cleanupSubject(epl.getSubject());

    List<Trip> trips = Trip.findPendingByCmpyNameType(subject, epl.getToken().getBelongId(), APPConstants.TOUR_TYPE);
    if (trips != null && trips.size() == 1) {
      trip = trips.get(0);

      //this is an existing tour, make sure the user has access as per access rules
      if (epl.getAccount().getState() != RecordStatus.ACTIVE) {
        Log.info("EMAIL_Stage_B:TRIP: Invalid User: Not ACTIVE:" + epl.getAccount().getState());
        return false;
      }

      Credentials cred = SecurityMgr.getCredentials(epl.getAccount());
      SecurityMgr.AccessLevel aLevel = SecurityMgr.getTripAccessLevel(trip, cred);
      if (aLevel.lt(SecurityMgr.AccessLevel.OWNER)) {
        Log.info("EMAIL_Stage_B:TRIP: Cannot access trip. Tripid: " + trip.tripid +
                 " Account: " + epl.getAccount().getUid() +
                 " Access Level: " + aLevel +
                 " (EPLID: " + epl.getPk() + ")");
        return false;
      }
    }
    else {
      isNewTrip = true;
      trip = Trip.buildTrip(epl.getAccount().getLegacyId(), epl.getToken().getBelongId(),subject);
      trip.save();
    }

    audit:
    {
      //TODO: 2016-10-26 - Trip Audit has not been migrated to Accounts yet but needs to be
      if(epl.getAccount().getAccountType() == Account.AccountType.PUBLISHER) {
        TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP,
                                             isNewTrip ? AuditActionType.ADD : AuditActionType.MODIFY,
                                             AuditActorType.EMAIL_USER)
                                .withTrip(trip)
                                .withCmpyid(trip.cmpyid)
                                .withUserid(epl.getAccount().getLegacyId());

        ((AuditTrip) ta.getDetails()).withName(trip.getName()).withId(trip.getTripid()).withStatus(AuditTrip.TripStatus.PENDING);
        ta.save();
      }
    }


    final EmailAttachmentParserActor.EmailAttachMsg attachMsg =
        new EmailAttachmentParserActor.EmailAttachMsg(EmailAttachmentParserActor.EmailAttachCmd.TO_TRIP);
    attachMsg.setTripId(trip.getTripid());
    attachMsg.setEplId(epl.getPk());
    if(attachMsg.getFiles().size() > 0) {
      Log.err("#&#&#&#: BUG BUG BUG: New Command Attachment count :" + attachMsg.getFiles().size() +
              " (EPLID: " + epl.getPk() + ")");
    }
    int attachCount = processAttachments(attachMsg, epl, mimeParser);
    epl.setAttachCount(attachCount);


    final EmailBodyParserActor.EmailBodyMsg bodyCmd =
        new EmailBodyParserActor.EmailBodyMsg(EmailBodyParserActor.EmailBodyCmd.TO_TRIP);
    bodyCmd.setTripId(trip.getTripid());
    bodyCmd.setEplId(epl.getPk());
    if(bodyCmd.getHtmls().size() > 0 || bodyCmd.getTexts().size() > 0) {
      Log.err("#&#&#&#: BUG BUG BUG: New Command HTML count :" + bodyCmd.getHtmls().size() +
              " Text count :" + bodyCmd.getTexts().size() + " (EPLID: " + epl.getPk() + ")");
    }
    int bodyCount = processBodies(bodyCmd, epl, mimeParser);
    epl.setState(EmailParseLog.EmailState.PARSING);
    epl.update();

    if (bodyCount > 0) {
      ActorsHelper.tell(SupervisorActor.UmappedActor.EMAIL_PARSER_BODY, bodyCmd);
    }

    if(attachCount > 0) {
      ActorsHelper.tell(SupervisorActor.UmappedActor.EMAIL_PARSER_ATTACH, attachMsg);
    }
    return true;
  }

  private static void handleTokenUser(EmailParseLog epl, MimeMessageParser mimeParser) {
    throw new NotImplementedException("Unable to handle user tokens for now");
  }

  private static void handleTokenTrvlr(EmailParseLog epl, MimeMessageParser mimeParser) {
    throw new NotImplementedException("Unable to handle Traveler tokens for now");
  }

  private static void handleTokenPoi(EmailParseLog epl, MimeMessageParser mimeParser) {
    throw new NotImplementedException("Unable to handle POI tokens for now");
  }

  private static void handleTokenTmplt(EmailParseLog epl, MimeMessageParser mimeParser) {
    throw new NotImplementedException("Unable to handle Tmplt tokens for now");
  }

  /**
   *
   */
  private static int processBodies(final EmailBodyParserActor.EmailBodyMsg msg,
                                    EmailParseLog epl,
                                    MimeMessageParser mimeParser) {
    int bodyCount = 0;
    for(DataSource ds: mimeParser.getAttachmentList()) {

      try {
        switch (ds.getContentType()) {
          case "text/plain":
            StringWriter textWriter = new StringWriter();
            IOUtils.copy(ds.getInputStream(), textWriter, "UTF-8");
            msg.addText(textWriter.toString());
            Log.debug("Plain Text: " + textWriter.toString());
            bodyCount++;
            break;
          case "text/html":
            StringWriter htmlWriter = new StringWriter();
            IOUtils.copy(ds.getInputStream(), htmlWriter, "UTF-8");
            msg.addHtml(htmlWriter.toString());
            Log.debug("HTML Text: " + htmlWriter.toString());
            bodyCount++;
            break;
          default:
            break;
        }
      }catch(IOException e) {
        Log.err("EMAIL_Stage_B: Failed to read textual input stream", e);
        e.printStackTrace();
      }
    }

    if(mimeParser.hasPlainContent() && mimeParser.getPlainContent().length() > 0) {
      msg.addText(mimeParser.getPlainContent());
      bodyCount++;
    }
    if(mimeParser.hasHtmlContent() && mimeParser.getHtmlContent().length() > 0) {
      bodyCount++;
      msg.addHtml(mimeParser.getHtmlContent());
    }
    return bodyCount;
  }

  /**
   * Process attachments
   * @param mimeParser
   * @return number of attachments found and saved
   */
  private static int processAttachments(EmailAttachmentParserActor.EmailAttachMsg msg,
                                        EmailParseLog epl,
                                        MimeMessageParser mimeParser) {
    int attachCount = 0;
    for (DataSource ds : mimeParser.getAttachmentList()) {
      try {
        if (ds.getName() != null) {
          String ext = FilenameUtils.getExtension(ds.getName()).toLowerCase();
          if (!(ext.equals("pdf") ||
                ext.equals("doc") ||
                ext.equals("docx")||
                ext.equals("xls") ||
                ext.equals("xlsx"))) {
            Log.info("EMAIL_Stage_B: Ignoring unsupported attached file: " + ds.getName());
            continue; //Not a useful document
          }

          FileSrc fSrc = FileSrc.find.byId(FileSrc.FileSrcType.EMAIL_ATTACH);
          LstFileType ft = LstFileType.find.byId(ext);
          S3Util.S3FileInfo s3Info = S3Util.S3FileInfo.buildInfo(fSrc.getRealBucketName(),
                                                                 null, //For now I don't have path, but I need hash
                                                                 ds.getInputStream(),
                                                                 ft.getProperMimeType());
          FileInfo file = FileInfo.byHash(s3Info.hash);
          if (file == null) { //New attachment
            file = FileInfo.buildFileInfo(epl.getAccount().getUid(),
                                          FileSrc.FileSrcType.EMAIL_ATTACH,
                                          RAW_FILE_EXPIRE_DELAY);
            String filename = S3Util.normalizeFilename(ds.getName().replaceAll("\\s+", "_"));
            StringBuffer s3Key = new StringBuffer();
            s3Key.append(epl.getToken().getToken());
            s3Key.append("_");
            s3Key.append(epl.getToken().getBelongId());
            s3Key.append("_");
            s3Key.append(file.getPk());
            s3Key.append("_");
            s3Key.append(filename);
            s3Info.key = s3Key.toString();
            if (S3Util.uploadS3Data(s3Info)) {
              file.setFiletype(ft);
              file.setFilepath(s3Info.key);
              file.setBucket(s3Info.bucket);
              file.setFilename(filename);
              file.setFilesize(s3Info.size);
              file.setHash(s3Info.hash); //MD5 Hash for now
              file.setFiletype(ft);
              file.setState(RecordState.ACTIVE);
              file.setCreatedBy(epl.getAccount().getUid());
              file.save();
            }
            else {
              Log.err("Failed to upload Email attachment for email (EPLID:" + epl.getPk() + ") file:" + ds.getName());
              continue;
            }
          }
          if (file != null) {
            try {
              FileEmailAttach fea = FileEmailAttach.newAttachment(epl, file);
              fea.save();
              attachCount++;
              msg.addAttachment(file.getPk());
            } catch (Exception e) {
              //Ignoring this for a good reason - when controller is being called 2nd time
            }
          }
        }
      }
      catch (IOException e) {
        Log.err("Error while reading Email attachment stream", e);
        e.printStackTrace();
      }
    }


    return attachCount;
  }

  public static String cleanupSubject(String subject) {
    if (subject != null) {
      subject = subject.replace("\u00A0"," "); //Replacing non-breaking space character
      return subject.replaceAll("Re:\\s*|Fwd:\\s*|Fw:\\s*|RE:\\s*|FWD:\\s*|FW:\\s*","").trim();
    }
    return "";
  }

  /**
   * Looks for the last appearance of the "Subject:" in the email and extracts the information trimming information.
   *
   * @param text Text version of the email content
   * @return
   */
  public static String getOriginalSubject(String text) {
    Matcher sm = subjectLine.matcher(text);
    String subject = null;
    while (sm.find()) {
      subject = sm.group(1);
    }

    if (subject != null) {
      subject = cleanupSubject(subject);
    }

    return subject;
  }

  public static void worldMateForward(EmailParseLog epl) {
    StopWatch sw = new StopWatch();
    sw.start();
    EmailParseLog.EmailState emailState = null;
    try {
      //Need to parse message for the 3rd time as Input Streams once depleted no longer have any content
      String raw = (String) CacheMgr.get(APPConstants.CACHE_EMAIL_RAW_PREFIX + epl.getPkString());
      if(raw == null) {
        //Need to retrieve from S3 as cache was gone
        raw = s3GetRaw(epl.getFile());
      } else {
        //Bye bye cache
        CacheMgr.set(APPConstants.CACHE_EMAIL_RAW_PREFIX + epl.getPkString(), null);
      }
      Session session = EmailMgr.getDefault().getSession();
      MimeMessage mm = new MimeMessage(session, new ByteArrayInputStream(raw.getBytes()));
      MimeMessageParser mmParser = new MimeMessageParser(mm).parse();

      boolean isFWDAsAttachment = false;
      for (DataSource ds : mmParser.getAttachmentList()) {
        if (ds.getContentType().equals("message/rfc822")) {
          isFWDAsAttachment = true;
          Session     fwdSession = EmailMgr.getDefault().getSession();
          MimeMessage mmFwd      = new MimeMessage(fwdSession, ds.getInputStream());
          epl.setSrc(EmailParseLog.EmailSource.FORWARD_ATTACH);
          emailState = deliverMimeMessageToWorldMate(epl, mmFwd);
          break; //TODO: For now handling only single forwarded as attachment message
        }
      }

      if (!isFWDAsAttachment) {
        emailState = deliverMimeMessageToWorldMate(epl, mm);
      }
    }
    catch (Exception e) {
      Log.err("EMAIL_Stage_C:WORLDMATE: Failed to forward e-mail to WorldMate", e);
      emailState = EmailParseLog.EmailState.ERROR;
    }

    sw.stop();
    epl.refresh();
    epl.setState(emailState);
    epl.addParseMs(sw.getTime());
    epl.update();
  }


  private static EmailParseLog.EmailState deliverMimeMessageToWorldMate(EmailParseLog epl, MimeMessage mm) {
    EmailParseLog.EmailState emailState;

    try {
      String fromEmail = ConfigMgr.getAppParameter(CoreConstants.EMAIL_TRIP_NOTIFICATION_FROM);
      String emailEnabled = ConfigMgr.getAppParameter(CoreConstants.EMAIL_ENABLED);

      if (emailEnabled == null || !emailEnabled.equalsIgnoreCase("Y")) {
        Log.info("EMAIL_Stage_C:TRIP:BODY: Email is disabled (EPLID: " + epl.getPk() + ")");
        return EmailParseLog.EmailState.ERROR;
      }

      String sender = "Umapped Inc.";

      List<String> emails = new ArrayList<>();

      emails.add(ConfigMgr.getAppParameter(CoreConstants.WORLDMATE_ADDR));

      List<String> bccEmail = new ArrayList<>();

      Map<String, String> headers = new HashMap<>();
      headers.put(EMAIL_HEADER_EPLID, epl.getPkString());
      String subject = "Fwd:" + mm.getSubject(); //Preserving original subject name from provider

      EmailMgr emailMgr = EmailMgr.getDefault();
      emailMgr.send(mm, fromEmail, sender, emails, bccEmail, subject, headers);

      Log.info("EMAIL_Stage_C:TRIP:BODY: Forwarded MimeMessage to WORLDMATE  (EPLID: " + epl.getPk() + ")");
      emailState = EmailParseLog.EmailState.WM_SENT;

    } catch (Exception e) {
      Log.err("EMAIL_Stage_C:WORLDMATE: Failed to forward MimeMessage to WorldMate (EPLID: " + epl.getPk() + ")",e);
      emailState = EmailParseLog.EmailState.ERROR;
    }

    return emailState;
  }

  public Result worldMateCallback() {
    StopWatch sw = new StopWatch();
    sw.start();
    String resultXMLText = request().body().asText();
    StringReader strReader = new StringReader(resultXMLText);

    WorldMateResult wmr = null;
    try {
      JAXBContext jc = JAXBContext.newInstance("com.mapped.publisher.parse.worldmate");
      Unmarshaller u = jc.createUnmarshaller();
      wmr = new WorldMateResult((WorldmateParsingResult) u.unmarshal(strReader));
    }
    catch (Exception e) {
      Log.err("Error parsing xml: " + e.getMessage());
      sendUmappedSupportEmail("Error parsing xml", resultXMLText);
      return ok();
    }

    String eplIdStr = null;
    String subject = null;
    String fromEmail = null;

    for (Header header : wmr.getHeaders().getHeader()) {
      switch (header.getName()) {
        case EMAIL_HEADER_EPLID:
          eplIdStr = header.getValue();
          break;
        case EMAIL_HEADER_WM_SUB:
          subject = header.getValue();
          break;
        case EMAIL_FROM:
          fromEmail = extractEmailAddress(header.getValue());
          break;
      }
    }

    Log.debug("MAIL_Stage_D:WM_CB: Received " + wmr.getStatusCode().name() + " EPLID: " + eplIdStr + " Subject: " + subject);

    if(eplIdStr == null) {
      Log.debug("EmailController - Inbox: " + strReader);
      String tripName = "Email Confirmation";
      if (subject != null && !subject.isEmpty()) {
        tripName = cleanupSubject(subject);
      }
      if (wmr.getStatusCode() == WorldMateResult.ReturnCode.SUCCESS ||
              wmr.getStatusCode() == WorldMateResult.ReturnCode.PARTIAL_SUCCESS) {

        //this is something that was sent directly to the worldmate API
        //so we send it to the booking inbox
        String email = "";
        if (wmr.getEndUserEmails() != null && wmr.getEndUserEmails().getUser() != null && wmr.getEndUserEmails().getUser().size() > 0) {
          email = wmr.getEndUserEmails().getUser().get(0).getEmail().trim();
        }
        if (email == null || email.toLowerCase().contains("import@umapped.com")) {
          //the email is wrong - let's try to find the guy who actually sent it from the header
          email = fromEmail;

        }
        Account a = Account.findByEmail(email);
        if (a == null && fromEmail != null && !fromEmail.equals(email)) {
          //let's try one more time
          a = Account.findByEmail(fromEmail);
        }
        if (a != null && !a.isTraveler() && a.getState() == RecordStatus.ACTIVE) {
          AccountCmpyLink cmpyLink = AccountCmpyLink.findActiveAccount(a.getUid());
          if (cmpyLink != null) {
            Company c = Company.find.byId(cmpyLink.getCmpyid());
            if (c != null && c.getStatus() == APPConstants.STATUS_ACTIVE) {
              List<BookingRS> newRecs = wmr.getBookingRS(a.getLegacyId(), c.getCmpyid(), tripName);
              int insert = 0;
              int failed = 0;
              for (BookingRS rs : newRecs) {
                if (!rs.getState().equals("DELETED")) {
                  try {
                    if (rs.createdBy == null) {
                      rs.createdBy = "system";
                    }
                    if (rs.modifiedBy == null) {
                      rs.modifiedBy = "system";
                    }
                    BookingAPIMgr.insert(rs);
                    insert++;
                  }
                  catch (Exception e) {
                    failed++;
                    Log.err("Worldmate Email Parser: Cannot insert reservation " + email + " - " + tripName + " - " + rs.toString());
                  }
                }
              }
              //create an EPL log
              List<EmailToken> tokens = EmailToken.getCompanyActiveTokens(c.getCmpyid());
              EmailToken emailToken = EmailToken.buildCmpyToken(c.getCmpyid());
              if (tokens != null && tokens.size() > 0) {
                emailToken = tokens.get(0);
              }

              EmailParseLog epl = EmailParseLog.buildRecord();
              epl.setFromAddr(a.getEmail());
              epl.setFromName(a.getFullName());
              epl.setSubject(tripName);
              epl.setAccount(a);
              epl.setToken(emailToken);
              epl.setState(EmailParseLog.EmailState.PARSED_WM);
              epl.setBkCount(insert);
              epl.setCreatedTs(new Timestamp(Instant.now().toEpochMilli()));
              epl.setSrc(EmailParseLog.EmailSource.DIRECT);
              epl.save();
            } else {
              Log.err("Worldmate Email Parser: unkown cmpy " + email + " - " + tripName);
            }
          } else {
            Log.err("Worldmate Email Parser: unkown cmpy linking " + email + " - " + tripName);
          }

        } else {
          Log.err("Worldmate Email Parser: unkown email " + email + " - " + tripName);
        }

      } else {
        String email = "";
        if (wmr.getEndUserEmails() != null && wmr.getEndUserEmails().getUser() != null && wmr.getEndUserEmails().getUser().size() > 0) {
          email = wmr.getEndUserEmails().getUser().get(0).getEmail().trim();
        }
        Account a = Account.findByEmail(email);
        if (a != null && !a.isTraveler() && a.getState() == RecordStatus.ACTIVE) {
          //email not parsed when sent directly
          AccountCmpyLink cmpyLink = AccountCmpyLink.findActiveAccount(a.getUid());
          if (cmpyLink != null) {
            Company c = Company.find.byId(cmpyLink.getCmpyid());
            List<EmailToken> tokens = EmailToken.getCompanyActiveTokens(c.getCmpyid());
            EmailToken emailToken = EmailToken.buildCmpyToken(c.getCmpyid());
            if (tokens != null && tokens.size() > 0) {
              emailToken = tokens.get(0);
            }
            EmailParseLog epl = EmailParseLog.buildRecord();
            epl.setFromAddr(a.getEmail());
            epl.setFromName(a.getFullName());
            epl.setSubject(tripName);
            epl.setAccount(a);
            epl.setToken(emailToken);
            epl.setState(EmailParseLog.EmailState.WM_UNSUPPORTED);
            epl.setCreatedTs(new Timestamp(Instant.now().toEpochMilli()));
            epl.setSrc(EmailParseLog.EmailSource.DIRECT);
            epl.save();
          }
        } else {

          S3Util.debugToS3(resultXMLText, "wm_result_xml_no_headers", "application/xml");

          Log.err("EMAIL_Stage_D:WM_CB: Worldmate didn't not return all headers");
          sendUmappedSupportEmail("Worldmate didn't not return all headers", resultXMLText);
        }
      }
      return ok();

    }

    Long eplId = Long.parseLong(eplIdStr);
    EmailParseLog epl = EmailParseLog.find.byId(eplId);

    if(epl == null) {
      Log.err("EMAIL_Stage_D:WM_CB: Can't find Email Parse Log record. EPLID: " + eplIdStr);
      sendUmappedSupportEmail(" Can't find Email Parse Log record. EPLID: " + eplIdStr, resultXMLText);

      return ok();
    }

    String tripName = cleanupSubject(epl.getSubject());
    List<Trip> trips = Trip.findPendingByCmpyNameType(tripName, epl.getToken().getBelongId(), APPConstants.TOUR_TYPE);

    if (wmr.getStatusCode() != WorldMateResult.ReturnCode.SUCCESS &&
        wmr.getStatusCode() != WorldMateResult.ReturnCode.PARTIAL_SUCCESS) {
      //add the original email as a trip note
      if (trips != null && trips.size() == 1) {
        attachTripNote(trips.get(0).getTripid(), eplId);
      }
      Log.err("EMAIL_Stage_D:WM_CB: Failed Parsing: " + wmr.getStatus() +
              " RequestID: " + wmr.getRequestId() + " (EPLID: " + epl.getPk() + ")");
      epl.setState(wmStatusToEmailState(wmr.getStatusCode()));
      epl.update();
      sendUmappedSupportEmail("Failed Parsing: " + wmr.getStatus() +
                              " RequestID: " + wmr.getRequestId() + " (EPLID: " + epl.getPk() + ")", resultXMLText);
      return ok();
    }

    if(epl.getToken().getType() == EmailType.EmailTypeName.PARSE_USER) {
      throw new NotImplementedException("EMAIL_Stage_D:WM_CB: WorldMate to Inbox support not implemented");
    }

    if(epl.getToken().getType() == EmailType.EmailTypeName.PARSE_CMPY) {
      Trip trip = null;
      boolean isNewTrip = false;

      if (trips != null && trips.size() == 1) {
        trip = trips.get(0);

        //this is an existing tour, make sure the user has access as per access rules
        if (epl.getAccount().getState() != RecordStatus.ACTIVE) {
          Log.err("EMAIL_Stage_D:WM_CB: Invalid Account: Not ACTIVE:" + epl.getAccount().getUid());
          S3Util.debugToS3(resultXMLText, "wm_result_xml_reject", "application/xml");
          epl.setState(EmailParseLog.EmailState.USER_DISABLED);
          epl.update();
          sendUmappedSupportEmail("Invalid User: " + epl.getAccount().getLegacyId(), resultXMLText);
          return ok();
        }
        Credentials cred = SecurityMgr.getCredentials(epl.getAccount());
        SecurityMgr.AccessLevel aLevel = SecurityMgr.getTripAccessLevel(trip, cred);
        if (aLevel.lt(SecurityMgr.AccessLevel.OWNER)) {
          Log.err("EMAIL_Stage_D:WM_CB: Cannot access trip. Tripid: " + trip.tripid +
                  " Account: " + epl.getAccount().getUid() +
                  " Access Level: " + aLevel + " (EPLID: " + epl.getPk() + ")");
          S3Util.debugToS3(resultXMLText, "wm_result_xml_reject", "application/xml");
          epl.setState(EmailParseLog.EmailState.USER_NO_TRIP_PERMISSIONS);
          epl.update();
          sendUmappedSupportEmail("Cannot access trip. Tripid: " + trip.tripid +
                                  " Account: " + epl.getAccount().getUid() +
                                  " Access Level: " + aLevel + " (EPLID: " + epl.getPk() + ")", resultXMLText);
          return ok();
        }
      }
      else {
        isNewTrip = true;
        //TODO: 2016-10-26: Trip does not support accounts yet
        trip = Trip.buildTrip(epl.getAccount().getLegacyId(), epl.getToken().getBelongId(), tripName);
      }

      //For now putting debugging all WorldMate Responses to S3
      S3Util.debugToS3(resultXMLText, "wm_result_xml_success", "application/xml");

      try {
        TripVO tvo = wmr.toTripVO();

        if (tvo != null) {
          //Ensuring that we know which email bookings came from
          tvo.setImportSrc(BookingSrc.ImportSrc.WORLDMATE);
          tvo.setImportSrcId(epl.getPk().toString());

          VOModeller voModeller = new VOModeller(trip, epl.getAccount(), isNewTrip);
          voModeller.buildTripVOModels(tvo);
          voModeller.saveAllModels();
          audit:
          {
            TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP,
                                                 isNewTrip ? AuditActionType.ADD : AuditActionType.MODIFY,
                                                 AuditActorType.EMAIL_USER)
                                    .withTrip(trip)
                                    .withCmpyid(trip.cmpyid)
                                    .withUserid(trip.getCreatedby());

            ((AuditTrip) ta.getDetails()).withName(trip.getName())
                                         .withId(trip.getTripid())
                                         .withStatus(AuditTrip.TripStatus.PENDING);
            ta.save();;
          }
          epl.addBkCount(voModeller.getBookingsCount());
          epl.setState(wmStatusToEmailState(wmr.getStatusCode()));
        }
        else {
          Log.err("WorldMate Failed Parsing: " + wmr.getStatus() + " (EPLID: " + epl.getPk() + ")");
          sendUmappedSupportEmail("Failed Parsing: " + wmr.getStatus() + " (EPLID: " + epl.getPk() + ")", resultXMLText);
          epl.setState(EmailParseLog.EmailState.ERROR);
        }
      }
      catch (Exception e) {
        Log.err("Failed on WorldMate Result: " + e.getMessage() + " (EPLID: " + epl.getPk() + ")");
        epl.setState(EmailParseLog.EmailState.ERROR);
        sendUmappedSupportEmail("Failed on WorldMate Result: ", resultXMLText);
        e.printStackTrace();
      }
    }
    sw.stop();
    epl.addParseMs(sw.getTime());
    epl.update();
    return ok();
  }

  @BodyParser.Of(TolerantText1Mb.class)
  public Result traxoCallback() {
    String msg = "Traxo: ";
    StopWatch sw = new StopWatch();
    sw.start();
    String resultJsonText = request().body().asText();
    if (resultJsonText != null) {
      try {
        TraxoHelper th = new TraxoHelper();
        th.parseJson(resultJsonText);
        if (th.hasReservations() && th.dataObject != null && th.dataObject.from_address != null && !th.dataObject.from_address.isEmpty()) {
          String fromEmail = th.dataObject.from_address;
          String toEmail = th.dataObject.to_address;

          String tripName = th.dataObject.subject;
          Log.info(msg + " Processing" + fromEmail + " - " + tripName + " - " + resultJsonText);

          if (th.response.type != null && th.response.type.equals("email.created") && (th.dataObject.segments == null || th.dataObject.segments.isEmpty())) {
            Log.err(msg + " First email callback " + fromEmail + " - " + tripName + " - " + resultJsonText);
            return ok();
          }
          Account a = Account.findByEmail(fromEmail);
          if (a == null && th.dataObject.user_address != null && !th.dataObject.user_address.equalsIgnoreCase(fromEmail)) {
            fromEmail = th.dataObject.user_address;
            a = Account.findByEmail(fromEmail);
          }
          if (a == null && toEmail != null && toEmail.contains("umapped.com") && toEmail.indexOf("+") > 0 && toEmail.indexOf("+") < toEmail.indexOf("@")) {
            String userid = toEmail.substring(toEmail.indexOf("+") + 1, toEmail.indexOf("@"));
            a = Account.findActiveByLegacyId(userid);
            fromEmail = a.getEmail();
          }

          if (a != null && !a.isTraveler() && a.getState() == RecordStatus.ACTIVE) {
            AccountCmpyLink cmpyLink = AccountCmpyLink.findActiveAccount(a.getUid());
            if (cmpyLink != null) {
              Company c = Company.find.byId(cmpyLink.getCmpyid());
              if (c != null && c.getStatus() == APPConstants.STATUS_ACTIVE) {
                List<BookingRS> newRecs = th.getBookingRS(a.getLegacyId(), c.getCmpyid(), tripName);
                int insert = 0;
                int failed = 0;
                for (BookingRS rs : newRecs) {
                  if (!rs.getState().equals("DELETED")) {
                    try {
                      if (rs.createdBy == null) {
                        rs.createdBy = "system";
                      }
                      if (rs.modifiedBy == null) {
                        rs.modifiedBy = "system";
                      }
                      BookingAPIMgr.insert(rs);
                      insert++;
                    }
                    catch (Exception e) {
                      failed++;
                      Log.err(msg + " Cannot insert reservation " + fromEmail + " - " + tripName + " - " + rs.toString());
                    }
                  }
                }
                //create an EPL log
                List<EmailToken> tokens = EmailToken.getCompanyActiveTokens(c.getCmpyid());
                EmailToken emailToken = EmailToken.buildCmpyToken(c.getCmpyid());
                if (tokens != null && tokens.size() > 0) {
                  emailToken = tokens.get(0);
                }

                EmailParseLog epl = EmailParseLog.buildRecord();
                epl.setFromAddr(a.getEmail());
                epl.setFromName(a.getFullName());
                epl.setSubject(tripName);
                epl.setAccount(a);
                epl.setToken(emailToken);
                epl.setState(EmailParseLog.EmailState.PARSED_WM);
                epl.setBkCount(insert);
                epl.setCreatedTs(new Timestamp(Instant.now().toEpochMilli()));
                epl.setSrc(EmailParseLog.EmailSource.DIRECT);
                epl.save();
              } else {
                Log.err(msg + "  unkown cmpy " + fromEmail + " - " + tripName);
              }
            } else {
              Log.err(msg + "  unkown cmpy linking " + fromEmail + " - " + tripName);
            }

          } else {
            Log.err(msg + "  unkown email " + fromEmail + " - " + tripName);
          }
        } else {
          Log.info(msg + "No Reservations. " + resultJsonText);
        }
      } catch (Exception e) {
        Log.err(msg + "Exception parsing Reservations. " + resultJsonText, e);

      }
    }

    return ok();
  }

  private void sendUmappedSupportEmail (String subject, String emailBody) {
    try {
      String smtp = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_HOST);
      String user = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_USER_ID);
      String pwd = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_PASSWORD);
      String fromEmail = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_PWD_RESET_FROM);
      List<Map<String, String>> body = new ArrayList<Map<String, String>>();

      if (ConfigMgr.getInstance().isProd()) {
        EmailMgr emailMgr = new EmailMgr(smtp, user, pwd);
        emailMgr.init();
        Map<String, String> token = new HashMap<String, String>();
        token.put(CoreConstants.TEXT, emailBody);
        body.add(token);

        emailMgr.send(fromEmail, "Umapped", "support@umapped.com", "Worldmate Error - " + subject, body);
      }
    } catch (Exception e) {

    }
  }

  public EmailParseLog.EmailState wmStatusToEmailState(WorldMateResult.ReturnCode rc) {
    switch (rc) {
      case PARTIAL_SUCCESS:
        return EmailParseLog.EmailState.WM_PARTIAL_SUCCESS;
      case UNSUPPORTED:
        return EmailParseLog.EmailState.WM_UNSUPPORTED;
      case NO_DATA:
        return EmailParseLog.EmailState.WM_NO_DATA;
      case INSUFFICIENT_DATA:
        return EmailParseLog.EmailState.WM_INSUFFICIENT_DATA;
      case NO_ITEMS:
        return EmailParseLog.EmailState.WM_NO_ITEMS;
      case EXTERNAL_ERROR:
        return EmailParseLog.EmailState.WM_EXTERNAL_ERROR;
      case INTERNAL_ERROR:
        return EmailParseLog.EmailState.WM_INTERNAL_ERROR;
      case UNRECOGNIZED_FORMAT:
        return EmailParseLog.EmailState.WM_UNRECOGNIZED_FORMAT;
      case REPEATED_FAILURE:
        return EmailParseLog.EmailState.WM_REPEATED_FAILURE;
      case ACCOUNT_SUSPENDED:
        return EmailParseLog.EmailState.WM_ACCOUNT_SUSPENDED;
      case QUOTA_EXCEDEED:
        return EmailParseLog.EmailState.WM_QUOTA_EXCEDEED;
      case SUCCESS:
      default:
        return EmailParseLog.EmailState.WM_SUCCESS;
    }
  }

  /**
   * Email parsing controller can call this method to send a list of warnings back to the sender.
   *
   * @return true if message has been sent, false otherwise
   */
  public static boolean sendUMappedAgentParsingWarningEmail(String recepient,
                                                            String inSubject,
                                                            Trip trip,
                                                            Map<String, ArrayList<ParseError>> parseErrors)

  {
    List<String> emails = new ArrayList<>();
    //Filter out all the usual suspects
    if (trip == null || parseErrors == null || parseErrors.size() == 0) {
      return false;
    }

    String fromEmail = ConfigMgr.getAppParameter(CoreConstants.EMAIL_TRIP_NOTIFICATION_FROM);
    UmappedEmail email = UmappedEmail.buildDefault();
    try {

      Company cmpy = Company.find.byId(trip.getCmpyid());
      if (cmpy != null && cmpy.name != null && cmpy.name.trim().length() > 0) {
        email.withFromName(cmpy.name + " via Umapped");
      }

      if (email.isEnabled()) {
        Log.err("Email disabled");
        return false;
      }

      EmailParseWarningView view = new EmailParseWarningView();
      view.tripUrl = email.getHostUrl() + controllers.routes.WebItineraryController.index(trip.tripid, null, 0L).url();
      view.parseErrors = parseErrors;

      emails.add(recepient);

      email.addTo(recepient);

      email.withHtml(views.html.email.parse.render(view).toString())
           .withSubject(inSubject)
           .withType(UmappedEmail.SubjectType.REPLY)
           .withEmailType(EmailLog.EmailTypes.WARNING)
           .withFromEmail(fromEmail).withTripId(trip.tripid)
           .withToList(emails)
           .buildAndSend();

    }catch (EmailException ee) {
      Log.err("Cannot send parse warning email. Tripid: " + trip.tripid, ee);
      return false;
    }

    return true;
  }

  public static void attachTripNote(String tripId, Long eplId) {
    TripNote tn = new TripNote();
    EmailParseLog epl = EmailParseLog.find.byId(eplId);
    FileInfo f = epl.getFile();
    Trip trip = Trip.find.byId(tripId);
    Long time = System.currentTimeMillis();

    try {
      String emailRaw = s3GetRaw(f);

      Session session = Session.getInstance(new Properties());
      MimeMessage mm = new MimeMessage(session, new ByteArrayInputStream(emailRaw.getBytes()));
      MimeMessageParser mimeParser = new MimeMessageParser(mm).parse();

      String sanitized = Utils.sanitizeHtmlFromCKEditor(mimeParser.getHtmlContent());
      sanitized = sanitized.replace("<p>","");
      sanitized = sanitized.replace("<div>","");
      sanitized = sanitized.replace("</p>","<br>");
      sanitized = sanitized.replace("</div>","<br>");
      sanitized = sanitized.replace("</div>","<br>");
      sanitized = sanitized.replace("<html>","");
      sanitized = sanitized.replace("</html>","");
      sanitized = sanitized.replace("<head>","");
      sanitized = sanitized.replace("</head>","");
      sanitized = sanitized.replaceAll("<body.*.>","");
      sanitized = sanitized.replace("</body>","");

      Document doc =  Jsoup.parse(sanitized);
      doc.select("img").remove();

      String info = doc.toString();
      info = info.replace("<html>","");
      info = info.replace("</html>","");
      info = info.replace("<head>","");
      info = info.replace("</head>","");
      info = info.replaceAll("<body.*.>","");
      info = info.replace("</body>","");
      info = info.replace("<div>","");
      info = info.replace("</p>","<br>");
      info = info.replace("</div>","<br>");
      info = info.replace("</div>","<br>");


      tn.setTrip(trip);
      tn.setNoteId(DBConnectionMgr.getUniqueLongId());
      tn.setCreatedBy(epl.getAccount().getLegacyId());
      tn.setModifiedBy(epl.getAccount().getLegacyId());
      tn.setCreatedTimestamp(time);
      tn.setLastUpdatedTimestamp(time);
      tn.setIntro(info);
      tn.setName(mimeParser.getSubject());

      tn.insert();
    }
    catch(Exception e) {
      System.out.println(e.getMessage());
    }
  }

  @With({Authenticated.class, Authorized.class})
  public static Map<String, List<EmailLogView>> buildEmailLogs (String tripId, EmailLog.EmailTypes emailType) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    final long localTimeOffsetMillis = sessionMgr.getTimezoneOffsetMins() * 60 * 1000;

    List<EmailLog> emailLogs = EmailLog.findByTrip(tripId, emailType);
    Map<String, List<EmailLogView>> emailLogsMap = new HashMap<>();
    for (EmailLog emailLog : emailLogs) {
      EmailLogView emailLogView = new EmailLogView();
      emailLogView.tripId = emailLog.getMeta();
      emailLogView.toEmail = emailLog.getTo_email();
      emailLogView.subject = emailLog.getSubject();
      emailLogView.type = emailLog.getType().toString();
      emailLogView.pk = emailLog.getPk();
      emailLogView.clickCounter = emailLog.getClick_counter();
      emailLogView.openCounter = emailLog.getOpen_counter();
      emailLogView.archived = emailLog.isArchive();
      emailLogView.sentTs = Utils.formatDateTimeSecondsPrint(emailLog.getSent_ts().getTime() + localTimeOffsetMillis);
      if (emailLog.getClicked_ts() != null) {
        emailLogView.status = EmailLog.EmailStatus.OPENED.name();
        emailLogView.timestamp = Utils.formatDateTimeSecondsPrint(emailLog.getClicked_ts().getTime() + localTimeOffsetMillis);
      } else if (emailLog.getOpened_ts() != null) {
        emailLogView.status = EmailLog.EmailStatus.OPENED.name();
        emailLogView.timestamp = Utils.formatDateTimeSecondsPrint(emailLog.getOpened_ts().getTime() + localTimeOffsetMillis);
      }
      else if (emailLog.getDelivered_ts() != null && emailLog.getClick_counter() == 0 && emailLog.getOpen_counter() == 0) {
        emailLogView.status = EmailLog.EmailStatus.DELIVERED.name();
        emailLogView.timestamp = Utils.formatDateTimeSecondsPrint(emailLog.getDelivered_ts().getTime() + localTimeOffsetMillis);
      }
      else if (emailLog.getProcessed_ts() != null && emailLog.getClick_counter() == 0 && emailLog.getOpen_counter() == 0) {
        emailLogView.status = EmailLog.EmailStatus.PROCESSED.name();
        emailLogView.timestamp = Utils.formatDateTimeSecondsPrint(emailLog.getProcessed_ts().getTime() + localTimeOffsetMillis);
      }
      else if (emailLog.getFailed_ts() != null && emailLog.getClick_counter() == 0 && emailLog.getOpen_counter() == 0) {
        emailLogView.status = emailLog.getStatus().name();
        emailLogView.timestamp = Utils.formatDateTimeSecondsPrint(emailLog.getFailed_ts().getTime() + localTimeOffsetMillis);
      }
      else {
        emailLogView.status = EmailLog.EmailStatus.SENT.name();
        emailLogView.timestamp = Utils.formatDateTimeSecondsPrint(emailLog.getSent_ts().getTime() + localTimeOffsetMillis);
      }
      List<EmailLogView> tempEmailView = new ArrayList<>();
      if (emailLogView.toEmail != null && emailLogsMap != null) {
        if (emailLogsMap.get(emailLogView.toEmail) != null && emailLogsMap.get(emailLogView.toEmail).size() > 0) {
          tempEmailView = emailLogsMap.get(emailLogView.toEmail);
        }
      }
      tempEmailView.add(emailLogView);
      emailLogsMap.put(emailLogView.toEmail, tempEmailView);

    }
    return emailLogsMap;
  }

  @With({Authenticated.class, Authorized.class})
  public Result viewArchiveTripEmail(String pk) {
    SessionMgr sessionMgr = new SessionMgr(session());
    try {
      EmailLog log = EmailLog.find.byId(Long.parseLong(pk));
      if (log != null && log.isArchive() && log.getMeta() != null) {
        Trip trip = Trip.findByPK(log.getMeta());
        if (trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.READ)) {
          S3Object obj = S3Util.getS3File(S3Util.getPDFBucketName(), getTripEmailFilename(log));
          String result = IOUtils.toString(obj.getObjectContent(), StandardCharsets.UTF_8);
          if (result != null && !result.isEmpty()) {
            return ok(new Html(result));
          }
        }
      }
    } catch (Exception e) {

    }
    return ok("Cannot preview email - please contact support@umapped.com");


  }

  public static String getTripEmailFilename (EmailLog log) {
    if (log != null && log.getMeta() != null) {
      return "trip/" + log.getMeta()  + "/archive/email/" + log.getPk() + ".html";
    }
    return "";
  }


}
