package com.mapped.publisher.view;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-03-26
 * Time: 11:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class BillingDetailView
    extends BaseView {

  public String tripId;
  public String tripName;
  public String createdBy;
  public String groupId;
  public String groupName;
  public String firstName;
  public String lastName;
  public int publishCount;
  public String publishedDate;
  public int numOfTransactions;

  public ArrayList<TripPassengerView> travellers;
  public ArrayList<TripPassengerView> agents;
}
