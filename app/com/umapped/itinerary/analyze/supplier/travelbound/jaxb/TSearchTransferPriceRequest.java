//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.04.17 at 04:55:33 PM EDT 
//


package com.umapped.itinerary.analyze.supplier.travelbound.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for t_SearchTransferPriceRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_SearchTransferPriceRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ImmediateConfirmationOnly" type="{}t_Empty" minOccurs="0"/>
 *         &lt;element name="ItemName" type="{}t_SearchName" minOccurs="0"/>
 *         &lt;element name="ItemCode" type="{}t_ItemCode" minOccurs="0"/>
 *         &lt;element name="TransferPickUp" type="{}t_TransferPickUp"/>
 *         &lt;element name="TransferDropOff" type="{}t_TransferDropOff"/>
 *         &lt;element name="TransferDate" type="{}t_Date"/>
 *         &lt;element name="NumberOfPassengers" type="{}t_NumberOfPassengers"/>
 *         &lt;element name="PreferredLanguage" type="{}t_LanguageCode"/>
 *         &lt;element name="AlternateLanguage" type="{}t_LanguageCode" minOccurs="0"/>
 *         &lt;element name="IncludeChargeConditions" type="{}t_IncludeChargeConditions" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_SearchTransferPriceRequest", propOrder = {
    "immediateConfirmationOnly",
    "itemName",
    "itemCode",
    "transferPickUp",
    "transferDropOff",
    "transferDate",
    "numberOfPassengers",
    "preferredLanguage",
    "alternateLanguage",
    "includeChargeConditions"
})
public class TSearchTransferPriceRequest {

    @XmlElement(name = "ImmediateConfirmationOnly")
    protected TEmpty immediateConfirmationOnly;
    @XmlElement(name = "ItemName")
    protected String itemName;
    @XmlElement(name = "ItemCode")
    protected String itemCode;
    @XmlElement(name = "TransferPickUp", required = true)
    protected TTransferPickUp transferPickUp;
    @XmlElement(name = "TransferDropOff", required = true)
    protected TTransferDropOff transferDropOff;
    @XmlElement(name = "TransferDate", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar transferDate;
    @XmlElement(name = "NumberOfPassengers")
    @XmlSchemaType(name = "integer")
    protected int numberOfPassengers;
    @XmlElement(name = "PreferredLanguage", required = true)
    protected String preferredLanguage;
    @XmlElement(name = "AlternateLanguage")
    protected String alternateLanguage;
    @XmlElement(name = "IncludeChargeConditions")
    protected TIncludeChargeConditions includeChargeConditions;

    /**
     * Gets the value of the immediateConfirmationOnly property.
     * 
     * @return
     *     possible object is
     *     {@link TEmpty }
     *     
     */
    public TEmpty getImmediateConfirmationOnly() {
        return immediateConfirmationOnly;
    }

    /**
     * Sets the value of the immediateConfirmationOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link TEmpty }
     *     
     */
    public void setImmediateConfirmationOnly(TEmpty value) {
        this.immediateConfirmationOnly = value;
    }

    /**
     * Gets the value of the itemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * Sets the value of the itemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemName(String value) {
        this.itemName = value;
    }

    /**
     * Gets the value of the itemCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * Sets the value of the itemCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemCode(String value) {
        this.itemCode = value;
    }

    /**
     * Gets the value of the transferPickUp property.
     * 
     * @return
     *     possible object is
     *     {@link TTransferPickUp }
     *     
     */
    public TTransferPickUp getTransferPickUp() {
        return transferPickUp;
    }

    /**
     * Sets the value of the transferPickUp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TTransferPickUp }
     *     
     */
    public void setTransferPickUp(TTransferPickUp value) {
        this.transferPickUp = value;
    }

    /**
     * Gets the value of the transferDropOff property.
     * 
     * @return
     *     possible object is
     *     {@link TTransferDropOff }
     *     
     */
    public TTransferDropOff getTransferDropOff() {
        return transferDropOff;
    }

    /**
     * Sets the value of the transferDropOff property.
     * 
     * @param value
     *     allowed object is
     *     {@link TTransferDropOff }
     *     
     */
    public void setTransferDropOff(TTransferDropOff value) {
        this.transferDropOff = value;
    }

    /**
     * Gets the value of the transferDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransferDate() {
        return transferDate;
    }

    /**
     * Sets the value of the transferDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransferDate(XMLGregorianCalendar value) {
        this.transferDate = value;
    }

    /**
     * Gets the value of the numberOfPassengers property.
     * 
     */
    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    /**
     * Sets the value of the numberOfPassengers property.
     * 
     */
    public void setNumberOfPassengers(int value) {
        this.numberOfPassengers = value;
    }

    /**
     * Gets the value of the preferredLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredLanguage() {
        return preferredLanguage;
    }

    /**
     * Sets the value of the preferredLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredLanguage(String value) {
        this.preferredLanguage = value;
    }

    /**
     * Gets the value of the alternateLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateLanguage() {
        return alternateLanguage;
    }

    /**
     * Sets the value of the alternateLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateLanguage(String value) {
        this.alternateLanguage = value;
    }

    /**
     * Gets the value of the includeChargeConditions property.
     * 
     * @return
     *     possible object is
     *     {@link TIncludeChargeConditions }
     *     
     */
    public TIncludeChargeConditions getIncludeChargeConditions() {
        return includeChargeConditions;
    }

    /**
     * Sets the value of the includeChargeConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link TIncludeChargeConditions }
     *     
     */
    public void setIncludeChargeConditions(TIncludeChargeConditions value) {
        this.includeChargeConditions = value;
    }

}
