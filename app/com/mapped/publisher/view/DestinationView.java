package com.mapped.publisher.view;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-01
 * Time: 11:31 AM
 * To change this template use File | Settings | File Templates.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DestinationView
    extends BaseView
    implements Serializable {
  public String id;
  public String cmpyId;
  /**
   * Actually createdById is an id of the user who linked Destination to the trip
   */
  public String createdById;
  public String name;
  public String intro;
  public String description;
  public int status;
  public String formattedAddr;
  public String streetAddr1;
  public String state;
  public String city;
  public String zipCode;
  public String country;
  public String locLat;
  public String locLong;
  public String tag;

  public String coverUrl;
  public String coverName;
  public ImageView cover;

  public String destinationType;
  public String destinationTypeDesc;

  public HashMap<String, String> tripAgencyList;
  public List<GenericTypeView> destinationTypeList;
  public String cmpyName;
  public boolean displayMap;
  public boolean isTourGuide;
  public String tourName;

  public List<DestinationGuideView> guides;
  public boolean canEdit;
  public UserView agent;

  public CmpyInfoView cmpyInfo;

  public TripBrandingView tripBrand;
  public List<TripBrandingView> tripCoBranding;

  public String tripId;
  public long tripStartDate;
  public String tripStartDatePrint;
  public long tripEndDate;
  public String tripEndDatePrint;

  public boolean mergeBookings = false;

  public Map<String, String> mobilizedDomains;

  public boolean wasPublished;

  public boolean isPrivate;
  public boolean createdByThisUser;


  public void addMobilizedDomain(String domain, String siteName) {
    if (mobilizedDomains == null) {
      mobilizedDomains = new TreeMap<>();
    }
    mobilizedDomains.put(siteName, domain);
  }

  //extra fields to support bookings in the custom document pdf
  public TripBookingView tripBookingView;
  public HashMap <String, List<BookingAndPageView>> chronologicalBookingsPages;
  public ArrayList <String> chronologicalPagesBookings;

  //list of travel42 reports <url, name>
  public Map<String, String> travel42Reports;

  public static boolean introFits(String intro) {
    boolean result = false;
    if (countLines(intro) > 20) {
      result = false;
    } else {
     if (stringArea(intro) > 10) {
        result = false;
      } else {
        result = true;
      }
    }
    return result;
  }

  private static int countLines(String s) {
    return (s + " ").split("(\r\n)|(\r)|(\n)").length;
  }

  private static int stringArea (String s) {
    return s.length() / 110;
  }



}
