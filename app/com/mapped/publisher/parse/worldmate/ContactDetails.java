
package com.mapped.publisher.parse.worldmate;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for contact-details complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="contact-details">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contact-details", namespace = "http://www.worldmate.com/schemas/worldmate-api-v1.xsd", propOrder = {
    "name",
    "url",
    "phone",
    "fax"
})
@XmlSeeAlso({
    BookingDetails.class,
    ProviderDetails.class
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
public class ContactDetails {

    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected String name;
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected String url;
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected String phone;
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected String fax;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Gets the value of the fax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public String getFax() {
        return fax;
    }

    /**
     * Sets the value of the fax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setFax(String value) {
        this.fax = value;
    }

}
