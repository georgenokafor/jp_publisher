package com.mapped.publisher.persistence;

import java.util.Date;

/**
 * Created by surge on 2015-10-06.
 */
public class FeedResource {
  Origin origin;
  Date date;
  String name;
  Long size;

  /**
   * Possible feed origins
   */
  public enum Origin {
    AWS_S3,
    FTP,
    HTTP_GET
  }

  public Long getSize() {
    return size;
  }

  public void setSize(Long size) {
    this.size = size;
  }

  public Origin getOrigin() {
    return origin;
  }

  public void setOrigin(Origin origin) {
    this.origin = origin;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
