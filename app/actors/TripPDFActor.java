package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.google.inject.Inject;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.persistence.TripBrandingMgr;
import com.mapped.publisher.utils.*;
import com.mapped.publisher.view.*;
import com.umapped.persistence.accountprop.PreferenceMgr;
import com.umapped.persistence.accountprop.TripPrefs;
import controllers.*;
import it.innove.play.pdf.PdfGenerator;
import models.publisher.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import play.i18n.Lang;
import play.i18n.MessagesApi;
import play.libs.concurrent.HttpExecutionContext;
import play.twirl.api.Html;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by twong on 2014-12-24.
 */
public class TripPDFActor
    extends UntypedActor {

  @Inject
  MessagesApi          messagesApi;
  @Inject
  HttpExecutionContext ec;

  /**
   * Akka Actor Properties
   */
  public final static Props                                     props                           = Props.create(
      TripPDFActor.class);
  private static      HashMap<String, java.lang.reflect.Method> cachedCustomTripPDFTemplates    = new HashMap<>();
  private static      ArrayList<String>                         cmpIdsForDefaultTripPDFTemplate = new ArrayList<>();
  @Inject
  static PdfGenerator pdfGenerator;
  @Inject
  static PreferenceMgr preferenceMgr;
  boolean fontsLoaded = false;

  public static class CommandRec
      implements Serializable {
    private final String  fileName;
    private final Long    startTimestamp;
    private final Boolean isPrintFriendly;
    private final Boolean archive;


    public CommandRec(String fileName, Long startTs, Boolean isPrintFriendly, Boolean archive) {
      this.fileName = fileName;
      this.startTimestamp = startTs;
      this.isPrintFriendly = isPrintFriendly;
      this.archive = archive;
    }

    public Long getStartTimestamp() {
      return startTimestamp;
    }

    public String getFileName() {
      return fileName;
    }

    public Boolean isPrintFriendly() {
      return isPrintFriendly;
    }
  }

  public static class Command
      implements Serializable {
    private final Trip    trip;
    private final String  fileName;
    private final Lang    lang;
    private final Boolean isPrintFriendly;
    private final Boolean archive;
    private final Long archivePk;

    public Command(Trip trip, String fileName, Lang l, Boolean isPrintFriendly, Boolean archive, Long archivePk) {
      this.trip = trip;
      this.fileName = fileName;
      this.lang = l;
      this.isPrintFriendly = isPrintFriendly;
      this.archive = archive;
      if (archivePk == null) {
        this.archivePk = archivePk;
      } else {
        this.archivePk = Instant.now().toEpochMilli();
      }
    }

    public String getFileName() {
      return fileName;
    }

    public Trip getTrip() {
      return trip;
    }

    public Lang getLang() {
      return lang;
    }

    public Boolean isPrintFriendly() {
      return isPrintFriendly;
    }
  }

  public static boolean displayCoverPhoto(String tag) {
    if (tag != null) {
      String t = tag.toLowerCase();
      if ((tag.toLowerCase().contains("no") && tag.toLowerCase().contains("cover")) ||
          (tag.toLowerCase().contains("no") && tag.toLowerCase().contains("photo"))) {
        return false;
      }
    }
    return true;
  }

  @Override
  public void onReceive(Object message)
      throws Exception {

    if (message instanceof Command) {
      if (!fontsLoaded) { //TODO: Hack should make it injectable
        PDFUtils.loadLocalFonts(pdfGenerator);
        fontsLoaded = true;
      }
      generatePdf((Command) message);

    }
    else {
      unhandled(message);
    }
  }

  public static void generatePdf(Command inMsg) {
    StopWatch    sw   = new StopWatch();
    sw.start();

    TripPrefs prefs = null;
    Trip      trip  = inMsg.trip;
    try {
      TripPreviewView previewView = new TripPreviewView();
      Company         cmpy        = null;

      previewView.tripCoverUrl = trip.getImageUrl();
      previewView.wasPublished = (TripPublishHistory.countPublished(trip.tripid) > 0); //George: toggle for draft
      // B/G in PDF
      previewView.isPrinterFriendly = inMsg.isPrintFriendly; //Flag to ignore or display booking photos

      //get agent info
      UserProfile        up = null;
      TripPublishHistory tp = TripPublishHistory.getLastPublished(trip.tripid, null);
      if (tp != null) {
        up = UserProfile.findByPK(tp.createdby);

      }
      else {
        up = UserProfile.find.byId(trip.createdby);
      }

      if (up != null) {
        AgentView agentView = new AgentView();
        previewView.agent = agentView;
        StringBuffer sb = new StringBuffer();
        if (up.firstname != null) {
          sb.append(up.firstname);
        }
        if (sb.length() > 0 && up.lastname != null) {
          sb.append(" ");
          sb.append(up.lastname);
        }
        agentView.agentName = sb.toString();
        agentView.agentEmail = TripBrandingMgr.getWhieLabelEmail(trip, up.getEmail()); //allow overwrite of email domain
        // for ski.com
        agentView.agentPhone = up.phone;
        agentView.agentMobile = up.mobile;
        agentView.cmpyWeb = up.web;
        agentView.agentFax = up.fax;
        agentView.agentFacebook = up.facebook;
        agentView.agentTwitter = up.twitter;

        TripBrandingView       overwritten   = null;
        List<TripBrandingView> brandingViews = TripBrandingMgr.findActiveBrandedCmpies(trip);
        if (brandingViews != null && brandingViews.size() > 0) {
          cmpy = TripBrandingMgr.getMainContact(brandingViews);
          previewView.tripBranding = brandingViews;
          //if this is overwritten by tag for the same company e.g. ski.com
          overwritten = TripBrandingMgr.getOverwrittenByTag(brandingViews, trip);
        }

        if (cmpy == null) {
          cmpy = Company.find.byId(trip.cmpyid);
        }

        if (overwritten == null) {
          if (cmpy != null) {
            agentView.cmpyName = cmpy.name;
            if (cmpy.getLogoname() != null &&
                cmpy.getLogoname().length() > 0 &&
                cmpy.getLogourl() != null &&
                cmpy.getLogourl().length() > 0) {
              agentView.cmpyLogo = Utils.escapeS3Umapped_Prd(cmpy.getLogourl());
              agentView.cmpyTag = cmpy.getTag();
            }
          }
          List<CmpyAddress> cmpyAddrs = CmpyAddress.findMainActiveByCmpyId(cmpy.cmpyid);
          if (cmpyAddrs != null && cmpyAddrs.size() > 0) {
            CmpyAddress cmpyAddress = cmpyAddrs.get(0);
            if (agentView.agentPhone == null || agentView.agentPhone.isEmpty()) {
              agentView.agentPhone =  TripBrandingMgr.getCompanyExtension(Account.findActiveByLegacyId(up.getUserid()), cmpyAddress.phone);
            }
            if (agentView.cmpyWeb == null || agentView.cmpyWeb.isEmpty()) {
              agentView.cmpyWeb = cmpyAddress.web;
            }

          }
        }
        else {
          agentView.cmpyName = overwritten.cmpyName;
          agentView.cmpyLogo = Utils.escapeS3Umapped_Prd(overwritten.cmpyLogoUrl);
          agentView.cmpyEmail = overwritten.cmpyEmail;
          agentView.cmpyPhone = TripBrandingMgr.getCompanyExtension(Account.findActiveByLegacyId(up.getUserid()), overwritten.cmpyPhone);
          agentView.cmpyTag = overwritten.cmpyTag;
          agentView.cmpyFax = overwritten.cmpyFax;
          agentView.cmpyFacebook = overwritten.cmpyFacebook;
          agentView.cmpyTwitter = overwritten.cmpyTwitter;
          agentView.agentMobile = "";
          if (agentView.agentPhone == null || agentView.agentPhone.length() < 1) {
            agentView.agentPhone = agentView.cmpyPhone;
          }
          if (agentView.cmpyWeb == null || agentView.cmpyWeb.length() < 1) {
            agentView.cmpyWeb = overwritten.cmpyWebsite;
          }
        }

      }

      previewView.businessCard = WebItineraryController.getTripBusinessCard(trip, null);

      ArrayList<TripBookingDetailView> bookingLocations = new ArrayList<TripBookingDetailView>();

      previewView.tripName = trip.name;
      previewView.tripStatus = String.valueOf(trip.status);
      previewView.tripStartDate = Utils.getDateString(trip.starttimestamp);
      previewView.tripEndDate = Utils.getDateString(trip.endtimestamp);
      previewView.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
      previewView.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
      previewView.tripStartDatePrintFull = Utils.getDateStringFullPrint(trip.starttimestamp);
      previewView.tripEndDatePrintFull = Utils.getDateStringFullPrint(trip.endtimestamp);
      previewView.tripStartDateMs = String.valueOf(trip.starttimestamp);
      previewView.tripEndDateMs = String.valueOf(trip.endtimestamp);
      previewView.tripNote = trip.comments;
      if (cmpy != null) {
        previewView.display12Hr = cmpy.display12hrClock();
      }
      //check user preferences for overrides
      if (tp != null) {
        Account account = Account.findActiveByLegacyId(tp.getCreatedby());
        prefs = preferenceMgr.getTripPref(account);
      }
      else {
        Account account = Account.findActiveByLegacyId(trip.getCreatedby());
        prefs = preferenceMgr.getTripPref(account);
      }
      if (prefs != null) {
        previewView.display12Hr = prefs.isIs12Hr();
      }

      previewView.tripTag = trip.tag;


      TripBookingView view = new TripBookingView();
      view.lang = inMsg.getLang();

      previewView.bookings = view;

      previewView.tripId = trip.tripid;
      previewView.tripStatus = String.valueOf(trip.status);
      view.comments = Utils.escapeHtml(trip.comments);

      view.tripId = trip.tripid;
      view.tripCmpyId = trip.cmpyid;

      if (trip.starttimestamp > 0) {
        view.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
        view.tripStartDate = String.valueOf(trip.starttimestamp);


        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(trip.starttimestamp);
        previewView.startYear = String.valueOf(cal.get(Calendar.YEAR));
        previewView.startMonth = String.valueOf(cal.get(Calendar.MONTH));
        previewView.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
      }


      if (trip.endtimestamp > 0) {
        view.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
        view.tripEndDate = Utils.getDateString(trip.endtimestamp);
      }
      view.tripName = trip.name;
      view.tripStatus = trip.status;

      List<TripDetail> tripDetails = TripDetail.findActiveByTripId(trip.tripid);
      if (tripDetails != null) {
        for (TripDetail tripDetail : tripDetails) {
          TripBookingDetailView bookingDetails = BookingController.buildBookingView(trip.tripid, tripDetail, true);
          if (bookingDetails != null) {
            if (bookingDetails.locStartLat != null &&
                bookingDetails.locStartLat != 0.0 &&
                bookingDetails.locStartLong != null &&
                bookingDetails.locStartLong != 0.0) {
              bookingLocations.add(bookingDetails);
            }

            if (tripDetail.starttimestamp > 0) {
              Calendar cal = Calendar.getInstance();
              cal.setTimeInMillis(tripDetail.starttimestamp);
              bookingDetails.startYear = String.valueOf(cal.get(Calendar.YEAR));
              bookingDetails.startMonth = String.valueOf(cal.get(Calendar.MONTH));
              bookingDetails.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
              bookingDetails.startHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
              bookingDetails.startMin = String.valueOf(cal.get(Calendar.MINUTE));

            }
            if (bookingDetails.startDatePrint != null && bookingDetails.startDateMs > 0) {
              bookingDetails.startDatePrint = Utils.formatDatePrint(bookingDetails.startDateMs,
                                                                    inMsg.getLang().toLocale());
            }

            if (tripDetail.endtimestamp > 0) {
              Calendar cal = Calendar.getInstance();
              cal.setTimeInMillis(tripDetail.endtimestamp);
              bookingDetails.endYear = String.valueOf(cal.get(Calendar.YEAR));
              bookingDetails.endMonth = String.valueOf(cal.get(Calendar.MONTH));
              bookingDetails.endDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
              bookingDetails.endHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
              bookingDetails.endMin = String.valueOf(cal.get(Calendar.MINUTE));
            }
            if (bookingDetails.endDatePrint != null && bookingDetails.endDateMs > 0) {
              bookingDetails.endDatePrint = Utils.formatDatePrint(bookingDetails.endDateMs,
                                                                  inMsg.getLang().toLocale());
            }

            view.addBookingDetailView(tripDetail.getDetailtypeid(), bookingDetails);
          }
        }

        if (!bookingLocations.isEmpty()) {
          previewView.bookingsLocations = bookingLocations;
        }
      }
      HashMap<Integer, String> destinationTypeDesc = new HashMap<>();
      List<DestinationType>    destinationTypes    = DestinationType.findActive();
      if (destinationTypes != null) {
        ArrayList<GenericTypeView> destinationTypeList = new ArrayList<GenericTypeView>();
        for (DestinationType u : destinationTypes) {
          GenericTypeView typeView = new GenericTypeView();
          typeView.id = String.valueOf(u.getDestinationtypeid());
          typeView.name = u.getName();
          destinationTypeList.add(typeView);
          destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());

        }
        previewView.destinationTypeList = destinationTypeList;
      }

      //get trip notes
      previewView.bookings.notes = BookingNoteController.getAllTripNotesView(trip, null, true);

      boolean embeddedDeepTables = checkEmbeddedTable(previewView.bookings.notes);
      previewView.hasEmbeddedTables = embeddedDeepTables;
      ArrayList<DestinationView> guides = new ArrayList<>();
      DestinationSearchView      dests  = TripController.getGuides(view);
      previewView.destinations = dests;
      if (previewView.destinations != null && previewView.destinations.destinationList != null) {
        for (DestinationView v : previewView.destinations.destinationList) {
          DestinationView destView = DestinationController.getDestView(v.id, destinationTypeDesc);
          guides.add(destView);

        }
        previewView.destinations.destinationList = guides;
      }

      //handle cross timezone booking
      if (previewView.bookings != null && previewView.bookings.flights != null && previewView.bookings.flights.size
          () > 0) {
        previewView.bookings.handleCrosTimezoneFlights();
      }

      byte[] pdf = null;

      Log.info("TripPDFActor: start to generate PDF for trip - " + view.tripId);

      if (previewView.isPrinterFriendly || (prefs != null && prefs.getTripPdfTmplt() == TripPrefs.TRIP_PDF_TMPLT
          .PRINT)) {
        previewView.isPrinterFriendly = true;
        pdf = pdfGenerator.toBytes(views.html.trip.tripPdf.render(previewView, inMsg.getLang().code(), embeddedDeepTables),
                                   "http://umapped.com");
      }
      else if (prefs != null && (prefs.getTripPdfTmplt() == TripPrefs.TRIP_PDF_TMPLT.PDF1 ||
                                 prefs.getTripPdfTmplt() == TripPrefs.TRIP_PDF_TMPLT.PDF2)) {
        if (prefs.getTripPdfTmplt() == TripPrefs.TRIP_PDF_TMPLT.PDF1) {
          previewView.showThumbnails = false;
        }
        else {
          previewView.showThumbnails = true;
        }

        pdf = pdfGenerator.toBytes(views.html.tripPdfV1.tripPdf.render(previewView, inMsg.getLang().code(), embeddedDeepTables),
                                   "http://umapped.com");
      }
      else {

        try {
          if (!cmpIdsForDefaultTripPDFTemplate.contains(trip.cmpyid)) {
            java.lang.reflect.Method render = cachedCustomTripPDFTemplates.get(trip.cmpyid);
            if (render == null) {
              CmpyCustomTemplate customTemplate = CmpyCustomTemplate.findActiveByCmpyType(trip.cmpyid,
                                                                                          CmpyCustomTemplate
                                                                                              .TEMPLATE_TYPE.TRIP_PDF
                                                                                              .ordinal());
              if (customTemplate != null &&
                  customTemplate.isfiletemplate &&
                  customTemplate.getPath() != null &&
                  customTemplate.getPath().startsWith("views")) {

                final Class<?> clazz = Class.forName(customTemplate.getPath());
                render = clazz.getDeclaredMethod("render", TripPreviewView.class);
                cachedCustomTripPDFTemplates.put(trip.cmpyid, render);
              }
            }
            if (render != null) {
              Html html = (Html) render.invoke(null, previewView);
              pdf = pdfGenerator.toBytes(html, "http://umapped.com");
            }

          }

        }
        catch (Exception e) {
          Log.err("TripPDFActor: Cannot generate PDF for trip:" + trip.getTripid(), e);
          e.printStackTrace();
        }
        if (!cmpIdsForDefaultTripPDFTemplate.contains(trip.cmpyid) && !(cachedCustomTripPDFTemplates.containsKey
            (trip.cmpyid))) {
          cmpIdsForDefaultTripPDFTemplate.add(trip.cmpyid);
        }
        if (pdf == null) {
          pdf = pdfGenerator.toBytes(views.html.trip.tripPdf.render(previewView, inMsg.getLang().code(), embeddedDeepTables),
                                     "http://umapped.com");
        }
      }


      sw.split();
      long splitTime = sw.getSplitTime();
      sw.unsplit();
      if (pdf != null) {
        try {
          S3Util.savePDF(pdf, inMsg.fileName);
          //if archive - we also save a copy to the archive directory
          if (inMsg.archive) {
            S3Util.savePDF(pdf, "trip/" + trip.getTripid() + "/archive/pdf/itinerary_" + inMsg.archivePk + ".pdf");
          }
          sw.stop();
          Log.info("TripPDFActor: Time for: " + trip.tripid + " Generate: " + splitTime + " S3: " +
                   (sw.getTime() -  splitTime) + " Total: " + sw.getTime());
        }
        catch (Exception e) {
          Log.log(LogLevel.ERROR, "TripPDFActor: cannot generate pdf for trip: " + trip.tripid, e);
        }
      }
    }
    finally {
      CacheMgr.set(APPConstants.CACHE_TRIP_PDF + trip.tripid, null);
    }
  }

  private static boolean checkEmbeddedTable(List<TripBookingDetailView> tripNoteViews) {
    if (tripNoteViews == null) {
      return false;
    }
    for (TripBookingDetailView noteView : tripNoteViews) {
      if (checkEmbeddedTable(noteView.intro)) {
        return true;
      }
    }
    return false;
  }

  public static boolean checkEmbeddedTable(String html) {
    try {
      if (StringUtils.isEmpty(html)) {
        return false;
      }
      Document document = Jsoup.parse(html);
      Elements elements = document.select("table table");
      if (elements != null && elements.size() > 0) {
        return true;
      }
    } catch (Exception e) {
      Log.err("Parsing html failed", e);
    }
    return false;
  }

  public interface Factory {
    public Actor create();
  }

  public  static  String getFileName (Trip trip) {
    return "trip/" + trip.tripid + "/itinerary_" + trip.createdtimestamp +".pdf";
  }
}
