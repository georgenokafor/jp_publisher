package com.mapped.publisher.parse.extractor.booking.email;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.EmailBodyExtractor;
import com.mapped.publisher.parse.extractor.booking.valueObject.AirportVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.FlightVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.PassengerVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.utils.Log;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by twong on 2015-02-05.
 */
public class CentravExtractor
    extends EmailBodyExtractor {

  enum CentravFormat {
    CENTRAV_OLD,
    CENTRAV_2,
    CENTRAV_2016_Q1
  }

  /**
   * Where in email are wwe
   */
  enum CentravEmailZone {
    UNKNOWN,
    SAME,
    RECORD_INFORMATION,
    TICKET_NUMBERS,
    PASSENGERS,
    ITINERARY
  }

  private static String[] dateTimeFormat   = {"yyyy EEE, MMM dd - h:mma", "yyyy ddMMM h:mma"};
  private static Pattern  airportPattern   = Pattern.compile("\\(([A-Z]{3})\\)");
  private static Pattern  airlineCode      = Pattern.compile("[A-Z]{2}");
  private static Pattern  flightNumber     = Pattern.compile("[0-9]{2,4}\\s*$");
  //public         Pattern  dateTimePattern  = Pattern.compile("\\s[A-Za-z]{3}[,]\\s[A-Za-z]{3}\\s[0-9]{2}\\s[-]\\s[0-9]+[:][0-9]{2}(am|pm)");
  //public         Pattern  dateTimePattern1 = Pattern.compile("\\s[0-9]{2}[A-Za-z]{3}\\s[0-9]+[:][0-9]{2}(am|pm)");


  Pattern patternRecordLocator = Pattern.compile("^\\*?Record Locator\\*?:\\s([A-Z0-9]+)$");
  Pattern patternEticket = Pattern.compile("^(.*)\\s-\\s(\\w+)/([\\.\\w]+)$");
  Pattern patternAirlineName = Pattern.compile("([a-zA-Z\\s]{4,})");


  Pattern patternAirlineRL = Pattern.compile("\\*?Airline RL\\*?:\\s([A-Z0-9]+)(Depart)?");
  /**
    Possible lines:
    "Airline RL: 28LCP7Depart30Jul12:20pm-Madrid(MAD) \n"
    "Depart\t16Jun\t3:19pm\t-\tBloomington-Normal, IL\t(BMI)\n"
    "Depart\t21Jan\t9:45am\t-\tBirmingham, AL\t(BHM)\n"
    "*Depart* 18Apr 9:30am - Gothenburg (GOT) *Arrive* 18Apr 11:05am - Amsterdam\n" +
  */
  Pattern patternDepart  = Pattern.compile("\\*?Depart\\*?\\s?([\\w:\\s]+)\\s?-\\s?([\\w,\\-\\s]+)(\\(([A-Z]+)\\))?");

  /**
   Possible lines:
   1. NOT SUPPORTED NOW
     *Arrive* 02Apr 1:06pm - Atlanta
     " +
     "(ATL)
   2.
    "Arrive\t16Jun\t7:25pm\t-\tEl Paso\t(ELP)\n"
     "Arrive\t21Jan\t11:43am\t-\tAtlanta\t(ATL)\n"
     "Arrive01Aug1:30pm-Lisbon(LIS)\n"

   */
  Pattern patternArrive  =  Pattern.compile("\\*?Arrive\\*?\\s?([\\w:\\s]+)\\s?-\\s?([\\w,\\-\\s]+)(\\(([A-Z]+)\\))?");


  /**
   * Slighly modified from dateTimePattern1
   * Possible lines:
   * 30Jul12:20pm
   * 16Jun\t3:19pm
   * 21Jan\t9:45am
   * 18Apr 9:30am
   */
  Pattern patternDT = Pattern.compile("\\s?([0-9]{2}[A-Za-z]{3})\\s?([0-9]+:[0-9]{2})(am|pm)");

  CentravFormat                format;
  CentravEmailZone             zone;
  TripVO                       tripVO;
  String                       recordLocator;
  HashMap<String, PassengerVO> passengers;


  public CentravExtractor() {
    tripVO = new TripVO();
    passengers = new HashMap<>();
    recordLocator = null;
    zone = CentravEmailZone.UNKNOWN;
    format = CentravFormat.CENTRAV_OLD;
  }

  protected void initImpl(Map<String, String> params) {
  }

  protected TripVO extractDataImpl(String input)
      throws Exception {

    format = CentravFormat.CENTRAV_2016_Q1;

    try (BufferedReader reader = new BufferedReader(new StringReader(input))) {
      String l = reader.readLine();
      while (l != null) {

        //Scrolling through nowhere land
        if(zone == CentravEmailZone.UNKNOWN ) {
          l = reader.readLine();
          CentravEmailZone newZone = getZone(l);
          if (newZone == CentravEmailZone.SAME) {
            continue; //Continue reading
          }
          zone = newZone;
        }

        switch (zone) {
          case RECORD_INFORMATION:
            recordInformationParser(reader);
            break;
          case TICKET_NUMBERS:
            ticketNumberParser(reader);
            break;
          case PASSENGERS:
            passengersParser(reader);
            break;
          case ITINERARY:
            itineraryParser(reader);
            l = null;
            break;
        }
      }
    }
    catch (IOException exc) {
      Log.err("Exception in CentravExtractor", exc);
    }

    tripVO.setImportSrc(BookingSrc.ImportSrc.CENTRAV);
    tripVO.setImportSrcId(recordLocator);
    tripVO.setImportTs(System.currentTimeMillis());
    return tripVO;
  }

  @Override
  public ArrayList<ParseError> getParseErrors() {
    return null;
  }

  private CentravEmailZone getZone(String line) {
    if (line.contains("Record Information")) {
      return CentravEmailZone.RECORD_INFORMATION;
    }
    if (line.contains("Ticket Numbers")) {
      return CentravEmailZone.TICKET_NUMBERS;
    }
    /*
    if (line.contains("Status")) {
      return CentravEmailZone.SKIP;
    }
    */
    if (line.contains("Passengers")) {
      return CentravEmailZone.PASSENGERS;
    }
    if (line.contains("Itinerary")) {
      return CentravEmailZone.ITINERARY;
    }
    return CentravEmailZone.SAME;
  }

  private void recordInformationParser(BufferedReader reader)
      throws IOException {
    while (true) {
      String           l  = reader.readLine();
      CentravEmailZone nz = getZone(l);

      //If new zone - return
      if (nz != CentravEmailZone.SAME) {
        zone = nz;
        return;
      }

      Matcher m = patternRecordLocator.matcher(l);
      if (m.matches()) {
        recordLocator = m.group(1);
      }
    }
  }

  private void ticketNumberParser(BufferedReader reader)
      throws IOException {
    while (true) {
      String           l  = reader.readLine();
      CentravEmailZone nz = getZone(l);

      //If new zone - return
      if (nz != CentravEmailZone.SAME) {
        zone = nz;
        return;
      }

      Matcher m = patternEticket.matcher(l);
      if (m.matches()) {
        String ticket = m.group(1);
        String lName  = m.group(2);
        String fName  = m.group(3);

        String fullName = lName.toUpperCase() + "/" + fName.toUpperCase();

        PassengerVO vo = passengers.get(fullName);
        if (vo == null) {
          vo = new PassengerVO();
          vo.setFirstName(fName);
          vo.setLastName(lName);
        }

        if (vo.geteTicketNumber() != null) {
          vo.seteTicketNumber(vo.geteTicketNumber() + " " + ticket);
        }
        else {
          vo.seteTicketNumber(ticket);
        }

        passengers.put(fullName, vo);
      }
    }
  }

  private void passengersParser(BufferedReader reader)
      throws IOException {
    while (true) {
      String           l  = reader.readLine();
      CentravEmailZone nz = getZone(l);

      //If new zone - return
      if (nz != CentravEmailZone.SAME) {
        zone = nz;
        return;
      }
    }
  }

  private void itineraryParser(BufferedReader reader)
      throws IOException {
    FlightVO  flightVO = null;
    String firstLine;

    while (true) {
      String l = reader.readLine();
      firstLine = l;

      //Reached the end of the parser
      if (l == null) {
        return;
      }

      CentravEmailZone nz = getZone(l);
      //If new zone - return
      if (nz != CentravEmailZone.SAME) {
        zone = nz;
        return;
      }

      if(format == CentravFormat.CENTRAV_2016_Q1 || true) {
        while(firstLine != null) {
          flightVO = new FlightVO();
          firstLine = parseFlight_2016Q1(flightVO, reader, firstLine);
          if (flightVO != null && flightVO.isValid()) {
            for (PassengerVO p : passengers.values()) {
              flightVO.addPassenger(p);
            }
            tripVO.addFlight(flightVO);
          } else {
            //Log.err("CentravExtractor: Incomplete flight:" + flightVO.toString());
          }
        }
      }
    }
  }

  /**
   * Parses flight end to end
   * @param fvo
   * @param reader
   * @param firstLine - (optional) line that might have been read but is a beginning of a new line
   * @return first line for the next flight
   */
  public String parseFlight_2016Q1(FlightVO fvo, BufferedReader reader, String firstLine)
      throws IOException {

    boolean nameFound = false;
    StringBuilder notes = new StringBuilder();
    if(recordLocator != null) {
      notes.append("Record Locator: ").append(recordLocator).append(System.lineSeparator());
    }

    String l;
    if(firstLine == null) {
      l = reader.readLine();
    } else  {
      l = firstLine;
    }
    while(l != null) {

      //Skipping empty lines
      if(l.length() <= 3) {
        l = reader.readLine();
        continue;
      }

      if ((!l.contains(":") || l.contains("[image: ")) && !(l.contains("Depart") || l.contains("Arrive")) ) {
        if(!nameFound) {
          if (!parseFlightNameLine(fvo, l)) {
            Log.err("CentravExtractor: Unparsable nameline: " + l);
          } else {
            nameFound = true;
          }
          l = reader.readLine();
          continue;
        } else {

          //Handling exception for longer lines cut off
          Matcher airportMatcher = airportPattern.matcher(l);
          if(airportMatcher.find()) {
            if (fvo.getDepartureAirport() != null && fvo.getArrivalAirport() == null &&
                fvo.getDepartureAirport().getCode() == null) {
              AirportVO avo = fvo.getDepartureAirport();
              avo.setCode(airportMatcher.group(1));
            }
            else if (fvo.getDepartureAirport() != null && fvo.getArrivalAirport() != null &&
                     fvo.getArrivalAirport().getCode() == null) {
              AirportVO avo = fvo.getArrivalAirport();
              avo.setCode(airportMatcher.group(1));
            }
            l = reader.readLine();
            continue;
          } else {
            fvo.setNotes(notes.toString());
            return l; //This will be the base for the future flight
          }
        }
      }

      if (l.contains("Class:") || l.contains("Seats:") || l.contains("Miles:") ||
          l.contains("Aircraft:") || l.contains("operated by:")) {
        notes.append(l).append(System.lineSeparator());
        l = reader.readLine();
        continue;
      }

      Matcher m = patternAirlineRL.matcher(l);
      if(m.find()) {
        fvo.setReservationNumber(m.group(1));
        if(!l.contains("Depart")) {
          l = reader.readLine();
          continue;
        }
      }

      if(l.contains("Depart")) {

        while (true) {
          Log.debug("CentravExtractor: Trying depart line: " + l);
          m = patternDepart.matcher(l);
          if (m.find()) {
            String time = m.group(1);
            String city = m.group(2);
            String code = m.group(4);

            AirportVO avo = new AirportVO();
            avo.setCode(code);
            avo.setCity(city);
            fvo.setDepartureAirport(avo);

            Matcher dt = patternDT.matcher(time);
            if (dt.find()) {
              String    date = dt.group(1) + " " + dt.group(2) + dt.group(3);
              Timestamp ts   = processDateTime(date);
              fvo.setDepatureTime(ts);
            }
            break;
          }
          //In case departure line is split into multiple lines - then combining them into a single line
          String nextL = reader.readLine();
          if(nextL == null) {
            Log.err("CentravExtractor: Failed to parse Depart line: " + l);
            break;
          }

          if(nextL.length() > 0) {
            l = l + " " + nextL;
          }
        }
      }

      if(l.contains("Arrive")) {
        while(true) {
          Log.debug("CentravExtractor: Trying Arrive line: " + l);
          m = patternArrive.matcher(l);
          if (m.find()) {
            String time = m.group(1);
            String city = m.group(2);
            String code = m.group(4);

            AirportVO avo = new AirportVO();
            avo.setCode(code);
            avo.setCity(city);
            fvo.setArrivalAirport(avo);

            Matcher dt = patternDT.matcher(time);
            if(dt.find()) {
              String date = dt.group(1) + " " + dt.group(2) + dt.group(3);
              Timestamp ts = processDateTime(date);
              fvo.setArrivalTime(ts);
            }
            break;
          }

          //In case departure line is split into multiple lines - then combining them into a single line
          String nextL = reader.readLine();
          if(nextL == null) {
            Log.err("CentravExtractor: Failed to parse Arrive line: " + l);
            break;
          }

          if(nextL.length() > 0) {
            l = l + " " + nextL;
          }
        }
      }
      l = reader.readLine();
    }

    fvo.setNotes(notes.toString());
    return null;
  }

  private Timestamp processDateTime(String dts) {
    DateUtils du              = new DateUtils();
    DateTime  currentDateTime = new DateTime(System.currentTimeMillis());
    int       year            = currentDateTime.getYear();
    Timestamp ts              = null;
    try {
      dts = Integer.toString(year) + " " + dts;
      ts = new Timestamp(du.parseDate(dts, dateTimeFormat).getTime());
      if (ts.getTime() < currentDateTime.getMillis()) {
        //we need to move the year
        DateTime d = new DateTime(ts);
        ts = new Timestamp(d.plusYears(1).getMillis());
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return ts;
  }

  public boolean parseFlightNameLine(FlightVO fvo, String line) {
    boolean result = false;
    Matcher m = flightNumber.matcher(line);
    if(m.find()) {
      fvo.setNumber(m.group(0));
      result = true;
    }

    m = airlineCode.matcher(line);
    if(m.find()) {
      fvo.setCode(m.group(0));
      return result;
    }

    m = patternAirlineName.matcher(line);
    if(m.find()) {
      fvo.setCode(m.group(0));
    }
    return result;
  }
}
