package com.umapped.persistence.notes.tableNote;

import com.mapped.publisher.form.TripNoteForm;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.InsuranceNoteView;
import com.mapped.publisher.view.StructuredNoteView;
import com.mapped.publisher.view.TableNoteView;
import com.umapped.persistence.notes.StructuredNote;
import com.umapped.persistence.reservation.UmDateTime;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;
import com.umapped.persistence.reservation.UmTraveler;
import models.publisher.TripNote;


import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.umapped.persistence.notes.utils.StructuredNoteUtils.cast;

/**
 * Created by george on 2017-05-09.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TableNote extends StructuredNote {
  private String noteName;
  private String intro;
  private UmDateTime startDateTime;
  private UmDateTime endDateTime;

  public String getNoteName() {
    return noteName;
  }

  public void setNoteName(String noteName) {
    this.noteName = noteName;
  }

  public String getIntro() {
    return intro;
  }

  public void setIntro(String intro) {
    this.intro = intro;
  }

  public UmDateTime getStartDateTime() {
    return startDateTime;
  }

  @JsonProperty("startDateTime")
  public TableNote setStartDateTime(UmDateTime startDateTime) {
    this.startDateTime = startDateTime;
    return this;
  }

  @JsonIgnore
  public TableNote setStartDateTime(Long ts) {
    return setStartDateTime(timestampToDateTime(ts));
  }


  public UmDateTime getEndDateTime() {
    return endDateTime;
  }

  @JsonProperty("endDateTime")
  public TableNote setEndDateTime(UmDateTime endDateTime) {
    this.endDateTime = endDateTime;
    return this;
  }

  @JsonIgnore
  public TableNote setEndDateTime(Long ts) {
    return setEndDateTime(timestampToDateTime(ts));
  }

  private UmDateTime timestampToDateTime(Long ts) {
    if (ts == null) {
      return null;
    }
    Instant i   = Instant.ofEpochMilli(ts);
    LocalDateTime ldt = LocalDateTime.ofInstant(i, ZoneId.of("UTC"));
    UmDateTime      dt  = new UmDateTime(ldt);
    return dt;
  }

  //Overriden method from SuperClass StructuredNote. Called by renderByNoteType()
  //Used to create intro to be displayed on itinerary view page etc
  @Override
  protected String renderIntro(StructuredNote note) {
    TableNote tableNote = cast(note, TableNote.class);

    return tableNote.getIntro();
  }

  //Overriden method from SuperClass StructuredNote. Called by populateByNoteType()
  //Used to create Note object to be saved, for respective note type
  protected TableNote populateNote(TripNote tripNote, TripNoteForm tripNoteInfo) {
    TableNote note = cast(tripNote.getNote(), TableNote.class);
    if (note == null) {
      note = new TableNote();
    }

    note.name = tripNoteInfo.getInTripNoteName();
    note.setIntro(tripNoteInfo.getInTripNoteIntro());

    return note;
  }
}
