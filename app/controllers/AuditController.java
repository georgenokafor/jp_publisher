package controllers;

import com.google.inject.Inject;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.audit.AuditRecord;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.common.SessionConstants;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.form.RecentActiveTripsForm;
import com.mapped.publisher.form.TripRecentActivityForm;
import com.mapped.publisher.persistence.ActiveTrips;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.RecentActiveTripsView;
import com.mapped.publisher.view.RecentActivityView;
import models.publisher.Trip;
import models.publisher.TripAudit;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by surge on 2014-05-26.
 */
public class AuditController
    extends Controller {

  @Inject
  FormFactory formFactory;

  @With({Authenticated.class, Authorized.class})
  public CompletionStage<Result> recentTripActivity() {

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    String tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
    final Trip tripModel = Trip.findByPK(tripId);
    final long localTimeOffsetMillis = sessionMgr.getTimezoneOffsetMins() * 60 * 1000;

    //Anyone with READ access permissions can get full trip history
    final SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.READ)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to see activity stream for this booking";
      return CompletableFuture.completedFuture(ok(views.html.common.message.render(baseView)));
    }

    //Getting incoming GET parameters
    Form<TripRecentActivityForm> tripAuditForm = formFactory.form(TripRecentActivityForm.class);
    TripRecentActivityForm tripRecentActivityForm = tripAuditForm.bindFromRequest().get();
    final int startPos = tripRecentActivityForm.getInStartPos();
    final int maxResults = tripRecentActivityForm.getInMaxResults();

    CompletableFuture<RecentActivityView> promiseOfView = CompletableFuture.completedFuture(buildActivityView(tripModel,
                                                                                                     startPos,
                                                                               maxResults,
                                                                               localTimeOffsetMillis));
    return  promiseOfView.thenApplyAsync(recentActivityView -> {
      recentActivityView.accessLevel = accessLevel;
      if (startPos > 0) {
        return ok(views.html.trip.recentActivityRecords.render(recentActivityView));
      }
      else {
        return ok(views.html.trip.recentActivity.render(recentActivityView));
      }
    });
  }

  public static RecentActivityView buildActivityView(Trip tripModel, int startPos, int maxResults, long localTimeOffsetMillis) {
    RecentActivityView rav = new RecentActivityView();
    rav.tripModel = tripModel;
    rav.localTimeOffset = localTimeOffsetMillis;

    try {
      rav.auditEvents = TripAudit.findAuditsForTrip(tripModel, startPos, maxResults);
      /* Loading objects from JSON. TODO: Figure out how to do during DB loading
         TODO: MIG24: Verify that this now correctly works */
      for (TripAudit ta: rav.auditEvents){
        ta.auditRecord = new AuditRecord();
        ta.auditRecord = AuditRecord.fromJson(ta.record);
      }

      Log.debug("Search returned results #" + rav.auditEvents.size() + " StartPos=" + startPos);
      if (rav.auditEvents.size() == maxResults) {
        rav.hasMore = true;
        rav.nextMaxResults = maxResults;
        rav.nextStartPos = startPos + maxResults;
      }
      else {
        rav.hasMore = false;
      }

      if (startPos > 0) {
        rav.prevStartPos = startPos;
      }
      else {
        rav.prevStartPos = -1;
      }
    }
    catch (Exception e) {
      Log.err("Failed to get audit events:" + e.getMessage());
    }

    return rav;
  }


  @With({Authenticated.class, Authorized.class})
  public CompletionStage<Result> recentActiveTrips() {

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    final long localTimeOffsetMillis = sessionMgr.getTimezoneOffsetMins() * 60 * 1000;

    //Getting incoming GET parameters
    Form<RecentActiveTripsForm>      activeTripsForm         = formFactory.form(RecentActiveTripsForm.class);
    final RecentActiveTripsForm      recentlyActiveTripsForm = activeTripsForm.bindFromRequest().get();
    final String                     userId                  = sessionMgr.getUserId();
    final ActiveTrips.TripSearchType searchType              = recentlyActiveTripsForm.getSearchType();
    final String                     tableName               = recentlyActiveTripsForm.getInTableName();
    final int                        daysAgo                 = recentlyActiveTripsForm.getDaysAgo();

    CompletionStage<RecentActiveTripsView> promiseOfView = CompletableFuture.completedFuture(buildActiveTripsView(searchType, userId, daysAgo,
                                                                                                                  localTimeOffsetMillis,
                                                                                                                  tableName));

    return promiseOfView.thenApplyAsync((RecentActiveTripsView recentActiveTripsView) ->
         ok(views.html.trip.recentActiveTrips.render(recentActiveTripsView))
      );
  }

  public static RecentActiveTripsView buildActiveTripsView(ActiveTrips.TripSearchType searchType,
                                                               String userId,
                                                               int daysAgo,
                                                               long timeOffsetMillis,
                                                               String tableName) {
    RecentActiveTripsView ratv = new RecentActiveTripsView();
    ratv.searchType = searchType;
    ratv.tableName = tableName;
    ratv.maxResultCount = APPConstants.MAX_AUDIT_RESULT_COUNT;
    ratv.localTimeOffsetMillis = timeOffsetMillis;
    long currTime = System.currentTimeMillis();
    int autoSearch = 0;
    while ((ratv.activeTrips == null || ratv.activeTrips.size() == 0) && autoSearch < 30) {
      long fromTime = currTime - (long)(24 * 3600 * 1000) * (daysAgo + 1);
      long toTime   = currTime - (long)(24 * 3600 * 1000) * daysAgo;
      ratv.activeTrips = ActiveTrips.findActiveTrips(searchType, userId, fromTime, toTime, 0, APPConstants.MAX_AUDIT_RESULT_COUNT * 10);
      ratv.daysAgo = daysAgo + 1;
      daysAgo++;
      autoSearch++;
    }

    return ratv;
  }
}
