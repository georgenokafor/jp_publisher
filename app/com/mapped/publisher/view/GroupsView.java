package com.mapped.publisher.view;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-22
 * Time: 9:39 AM
 * To change this template use File | Settings | File Templates.
 */
public class GroupsView extends BaseView {
    public List<GroupView> groups;
    public String cmpyId;
    public boolean isCmpyAdmin;
    public String cmpyName;
    public List<UserView> users;

}
