package com.mapped.publisher.js;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.view.BusinessCardView;
import com.umapped.api.schema.types.ReturnCode;

/**
 * Created by surge on 2016-01-21.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TripInfo
    extends BaseJsonResponse {
  public boolean authenticated = false;
  public boolean authorized    = false;
  public String           id;
  public String           name;
  public String           coverUrl;
  public Long             startTs;
  public Long             finishTs;
  public String           note;
  public BusinessCardView bc;
  public SecurityMgr.AccessLevel access;
  public boolean          collab;
  public boolean          hasAfarGuides;
  public boolean          guestAccess;
  public boolean          is24Hr;

  /**
   * TODO: This is a hack - will use this flag to overwrite messenger configuration parameters
   */
  public boolean          talkback;

  public TripInfo(ReturnCode rc) {
    super(rc);
  }
}
