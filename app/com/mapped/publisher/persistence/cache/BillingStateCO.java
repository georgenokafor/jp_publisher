package com.mapped.publisher.persistence.cache;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;

import java.io.Serializable;

/**
 * Create generic version as logic seems to be identical across all items needing to maintain progress
 * Created by surge on 2015-04-14.
 */
public class BillingStateCO
    implements Serializable {

  public static enum State {
    STOPPED,
    INVOICES_GENERATING,
    INVOICES_PUSHING,
    INVOICES_SENDING,
    INVOICES_SYNCING,
    PLANS_SYNCING,
    USERS_SYNCING,
    CMPYS_SYNCING
  }

  public State currState;

  public long lastUpdateTs;
  public int currItem;
  /** -1 indicates unknown number of items. 0 for stop all positive number are legal. */
  public int totalItems;

  public BillingStateCO() {
    currState = State.STOPPED;
    lastUpdateTs = System.currentTimeMillis();
    currItem = 0;
    totalItems = 0;
  }

  public static BillingStateCO getFromCache() {
    BillingStateCO billingState = (BillingStateCO) CacheMgr.get(APPConstants.CACHE_BILLING_STATE);
    if (billingState == null) {
      billingState = new BillingStateCO();
    }
    return billingState;
  }

  public boolean start(State state, int totalItems) {
    currState = state;
    lastUpdateTs = System.currentTimeMillis();
    currItem = 0;
    this.totalItems = totalItems;
    updateCache();
    return true;
  }

  /**
   * Increments the running state and updates it every 50th item
   */
  public void next() {
    currItem++;
    if (currItem % 50 == 0) {
      update(currItem);
    }
  }

  public void update(int currItemId) {
    lastUpdateTs = System.currentTimeMillis();
    currItem = currItemId;
    updateCache();
  }

  public void stop() {
    currState = State.STOPPED;
    lastUpdateTs = System.currentTimeMillis();
    currItem = 0;
    totalItems = 0;
    updateCache();
  }

  private void updateCache() {
    CacheMgr.set(APPConstants.CACHE_BILLING_STATE, this);
  }
}
