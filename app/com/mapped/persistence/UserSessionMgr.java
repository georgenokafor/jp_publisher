package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class UserSessionMgr {

	 private static final String SELECT_ALL = "Select Pk, UsersId, DevId, OSVer, DevVer, Dev, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp, Notify from UserSession;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, UsersId, DevId, OSVer, DevVer, Dev, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp, Notify from UserSession where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from UserSession where Pk = ?;";
	 private static final String INSERT = "insert into UserSession (Pk, UsersId, DevId, OSVer, DevVer, Dev, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp, Notify) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update UserSession SET UsersId=?, DevId=?, OSVer=?, DevVer=?, Dev=?, SyncTimestamp=?, LastUpdateTimestamp=?, Notify=? WHERE Pk = ?; ";
	 private static final String SELECT_ALL_BY_USERS_ID = "Select Pk, UsersId, DevId, OSVer, DevVer, Dev, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp, Notify from UserSession where UsersId=? ";
	 private static final String DELETE_BY_USERS_ID = "delete from UserSession where UsersId=? ";
	 public static ArrayList <UserSession> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return UserSessionMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static UserSession getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<UserSession> results = UserSessionMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (UserSession c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getUsersId());
				 prep.setString(3, c.getDevId());
				 prep.setString(4, c.getOSVer());
				 prep.setString(5, c.getDevVer());
				 prep.setString(6, c.getDev());
				 prep.setLong(7, c.getSyncTimestamp());
				 prep.setLong(8, c.getLastUpdateTimestamp());
				 prep.setLong(9, c.getCreateTimestamp());
				 prep.setString(10, c.getNotify());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (UserSession c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getUsersId());
				 prep.setString(2, c.getDevId());
				 prep.setString(3, c.getOSVer());
				 prep.setString(4, c.getDevVer());
				 prep.setString(5, c.getDev());
				 prep.setLong(6, c.getSyncTimestamp());
				 prep.setLong(7, c.getLastUpdateTimestamp());
				 prep.setString(8, c.getNotify());
				 prep.setString(9, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <UserSession> getAllByUsersId(String id) throws SQLException {
		 ResultSet rs = null;
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_USERS_ID);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 return UserSessionMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void deleteByUsersId (String id) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_USERS_ID);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <UserSession> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <UserSession> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 UserSession c = new UserSession();
			 c.setPk(rs.getString("Pk"));
			 c.setUsersId(rs.getString("UsersId"));
			 c.setDevId(rs.getString("DevId"));
			 c.setOSVer(rs.getString("OSVer"));
			 c.setDevVer(rs.getString("DevVer"));
			 c.setDev(rs.getString("Dev"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setNotify(rs.getString("Notify"));
			 results.add(c);
		 }
		 return results;
	 }
}
