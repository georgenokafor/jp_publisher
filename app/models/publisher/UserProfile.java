package models.publisher;

import com.avaje.ebean.*;
import com.mapped.publisher.common.APPConstants;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-14
 * Time: 11:19 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "user_profile")
public class UserProfile
    extends Model {
  public static Model.Finder<String, UserProfile> find = new Finder(UserProfile.class);
  public Long lastlogintimestamp;
  public String facebook;
  public String twitter;
  public String web;
  public String phone;
  public String mobile;
  public String fax;
  public String profilephoto;
  public String profilephotourl;
  public int failedlogincount;
  public String pwdresetid;
  public Long pwdresetexpiry;
  @Id
  public String userid;
  @Constraints.Required
  public String firstname;
  public String lastname;
  public String email;
  public int status;
  public int usertype;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;

  @Override
  public void save() {
    super.save();
  }

  /**
   * Direct or indirect users of the plan
   * @param planId
   * @return
   */
  public static List<UserProfile> getActiveBilledPlanUsers(Long planId){
    com.avaje.ebean.Query<BillingSettingsForCmpy> cmpyOnPlan = Ebean
        .createQuery(BillingSettingsForCmpy.class)
        .select("cmpyId")
        .where()
        .eq("plan.planId", planId)
        .or(Expr.lt("cutoff_ts", System.currentTimeMillis()), Expr.isNull("cutoff_ts"))
        .eq("recurrent", true)
        .query();
    com.avaje.ebean.Query<BillingSettingsForUser> usersOnPlan = Ebean
        .createQuery(BillingSettingsForUser.class)
        .select("userId")
        .where()
        .eq("plan.planId", planId)
        .or(Expr.lt("cutoff_ts", System.currentTimeMillis()), Expr.isNull("cutoff_ts"))
        .eq("recurrent", true)
        .query();


    com.avaje.ebean.Query<BillingSettingsForUser> billedUsers = Ebean
        .createQuery(BillingSettingsForUser.class)
        .select("userId")
        .where()
        .or(Expr.in("userId", usersOnPlan),
            Expr.or(Expr.and(Expr.eq("schedule.name", BillingSchedule.Type.COMPANY), Expr.in("cmpyid", cmpyOnPlan)),
                    Expr.and(Expr.eq("schedule.name", BillingSchedule.Type.AGENT),   Expr.in("agent",  usersOnPlan))))
        .or(Expr.lt("cutoff_ts", System.currentTimeMillis()), Expr.isNull("cutoff_ts"))
        .eq("recurrent", true)
        .query();

    return find.where()
               .in("userid", billedUsers)
               .findList();
  }

  public String getInitials() {
    if (firstname != null && lastname != null) {
      return (firstname.substring(0, 1) + lastname.substring(0, 1)).toUpperCase();
    }
    return "";
  }


  public static List<UserProfile> userProfileOutOfSync(){
    com.avaje.ebean.Query<Account> accounts = Ebean
        .createQuery(Account.class)
        .select("legacyId")
        .where()
        .query();


    String sql = "SELECT userid\n" +
                 "FROM user_profile up, account a\n" +
                 "WHERE a.legacy_id = up.userid AND " +
                 "      to_timestamp(up.lastupdatedtimestamp::double precision/1000) > a.modified_ts";

    RawSql rawSql = RawSqlBuilder
        // let ebean parse the SQL so that it can
        // add expressions to the WHERE and HAVING
        // clauses
        .parse(sql)
        // map resultSet columns to bean properties
        .columnMapping("userid", "userid")
        .create();

    Query<UserProfile> query = Ebean.find(UserProfile.class);
    query.setRawSql(rawSql);

    return find.where()
               .or(Expr.not(Expr.in("userid", accounts)),
                   Expr.in("userid", query))
               .findList();
  }


  public static List<UserProfile> getActiveBilledConsortiumUsers(Integer consId) {
    com.avaje.ebean.Query<ConsortiumCompany> consIntCompanies = Ebean
        .createQuery(ConsortiumCompany.class)
        .select("consCmpyId.cmpyId")
        .where()
        .eq("cons_id", consId)
        .query();


    com.avaje.ebean.Query<Company> consCompanies = Ebean
        .createQuery(Company.class)
        .select("cmpyid")
        .where()
        .in("cmpy_id",consIntCompanies)
        .query();

    com.avaje.ebean.Query<UserCmpyLink> userIds = Ebean
        .createQuery(UserCmpyLink.class)
        .select("user.userid")
        .where()
        .in("cmpyid", consCompanies)
        .eq("status", APPConstants.STATUS_ACTIVE)
        .query();

    //Looking for users who are actually still actively billed
    com.avaje.ebean.Query<BillingSettingsForUser> billedUsers = Ebean
        .createQuery(BillingSettingsForUser.class)
        .select("userId")
        .where()
        .in("userid", userIds)
        .lt("cutoff_ts", System.currentTimeMillis())
        .eq("recurrent", true)
        .query();

    return find.where()
               .in("userid", billedUsers)
               .findList();
  }

  public String getFullName() {
    StringBuilder sb = new StringBuilder();
    if (firstname != null) {
      sb.append(firstname)
        .append(" ");
    }
    if (lastname != null) {
      sb.append(lastname);
    }
    return sb.toString();
  }

  public static UserProfile findByPK(String pk) {
    return find.byId(pk);
  }

  public static UserProfile findUserCaseInsentive(String pk) {
    List<UserProfile> users = doesExist(pk);
    if (users != null && users.size() == 1) {
      return users.get(0);
    }

    return null;
  }

  /**
   * Total number of users;
   * @return
   */
  public static int getRowCount() {
    return find.findCount();
  }

  /**
   * Total number of filtered user as used by DataTables
   */
  public static int getRowCountFiltered(String filter) {
    String[] filterList = filter.trim().split(",");
    List<String> keywords = new ArrayList<String>();
    List<String> keywords2 = new ArrayList<String>();

    String sql = "(userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
    String sql2  = "plan in (select plan_id  from bil_plan where name ilike ?";
    String str = "%" + filterList[0].trim().replaceAll(" ", "%") + "%";
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords2.add(str);


    for (int i = 1; i < filterList.length;  i++) {
      str = "%" + filterList[i].trim().replaceAll(" ", "%") + "%";

      sql += " or userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
      sql2 += " or name ilike ?";

      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords2.add(str);
    }
    sql += ")";
    sql2 +=")";

    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean.createQuery(BillingSettingsForUser.class)
        .select("userId")
        .where()
        .raw(sql2, keywords2.toArray())
        .query();

    return find.where()
               .ne("status", -1)
               .or(Expr.raw(sql, keywords.toArray()), Expr.in("userid", subQuery))
               .findCount();
  }

  public static List<UserProfile> getPage(int start, int count, String filter, String sortOrder) {
    return find.where()
        .ne("status", -1)
        .or(Expr.or(Expr.icontains("userid", filter), Expr.icontains("email", filter)),
            Expr.or(Expr.icontains("firstname", filter), Expr.icontains("lastname", filter)))
        .orderBy(sortOrder)
        .setFirstRow(start).setMaxRows(count)
        .findList();
  }

  public static List<UserProfile> getPage_Billing(int start, int count, String filter, String sortOrder) {
    String[] filterList = filter.trim().split(",");
    List<String> keywords = new ArrayList<String>();
    List<String> keywords2 = new ArrayList<String>();

    String sql = "(userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
    String sql2  = "plan in (select plan_id  from bil_plan where name ilike ?";
    String str = "%" + filterList[0].trim().replaceAll(" ", "%") + "%";
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords2.add(str);


    for (int i = 1; i < filterList.length;  i++) {
      str = "%" + filterList[i].trim().replaceAll(" ", "%") + "%";

      sql += " or userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
      sql2 += " or name ilike ?";

      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords2.add(str);
    }
    sql += ")";
    sql2 +=")";

    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean.createQuery(BillingSettingsForUser.class)
                                                                  .select("userId")
                                                                  .where()
                                                                  .raw(sql2, keywords2.toArray())
                                                                  .query();

    return find.where()
               .ne("status", -1)
               .or(Expr.raw(sql, keywords.toArray()), Expr.in("userid", subQuery))
               .orderBy(sortOrder)
               .setFirstRow(start).setMaxRows(count)
               .findList();
  }

  public static int getRowCount_BillingNoConf() {
    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean.createQuery(BillingSettingsForUser.class)
                                                                  .select("userId")
                                                                  .where()
                                                                  .query();

    return find.where().not(Expr.in("userid", subQuery)).findCount();
  }

  public static int getRowCountFiltered_BillingNoConf(String filter) {
    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean.createQuery(BillingSettingsForUser.class)
                                                                  .select("userId")
                                                                  .where()
                                                                  .query();
    String[] filterList = filter.trim().split(",");
    List<String> keywords = new ArrayList<String>();

    String sql = "(userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
    String str = "%" + filterList[0].trim().replaceAll(" ", "%") + "%";
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);

    for (int i = 1; i < filterList.length;  i++) {
      str = "%" + filterList[i].trim().replaceAll(" ", "%") + "%";

      sql += " or userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";

      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
    }
    sql += ")";

    return find.where()
               .not(Expr.in("userid", subQuery))
               .raw(sql, keywords.toArray())
               .findCount();
  }

  public static List<UserProfile> getPage_BillingNoConf(int start, int count, String filter, String sortOrder) {
    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean.createQuery(BillingSettingsForUser.class)
                                                                  .select("userId")
                                                                  .where()
                                                                  .query();

    String[] filterList = filter.trim().split(",");
    List<String> keywords = new ArrayList<String>();

    String sql = "(userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
    String str = "%" + filterList[0].trim().replaceAll(" ", "%") + "%";
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);

    for (int i = 1; i < filterList.length;  i++) {
      str = "%" + filterList[i].trim().replaceAll(" ", "%") + "%";

      sql += " or userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";

      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
    }
    sql += ")";

    return find.where()
               .not(Expr.in("userid", subQuery))
               .raw(sql, keywords.toArray())
               .orderBy(sortOrder)
               .setFirstRow(start)
               .setMaxRows(count)
               .findList();
  }

  public static int getRowCount_BillingNonRecurrent() {
    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean.createQuery(BillingSettingsForUser.class)
                                                                  .select("userId")
                                                                  .where()
                                                                  .eq("recurrent", false)
                                                                  .query();

    return find.where().in("userid", subQuery).findCount();
  }

  public static int getRowCountFiltered_BillingNonRecurrent(String filter) {
    String[] filterList = filter.trim().split(",");
    List<String> keywords = new ArrayList<String>();
    List<String> keywords2 = new ArrayList<String>();

    String sql = "(userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
    String sql2  = "userid in (select bsu.userid from bil_settings_user bsu where bsu.plan in (select bp.plan_id from bil_plan bp where bp.name ilike ?";
    String str = "%" + filterList[0].trim().replaceAll(" ", "%") + "%";
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords2.add(str);


    for (int i = 1; i < filterList.length;  i++) {
      str = "%" + filterList[i].trim().replaceAll(" ", "%") + "%";

      sql += " or userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
      sql2 += " or bp.name ilike ?";

      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords2.add(str);
    }
    sql += ")";
    sql2 +="))";

    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean.createQuery(BillingSettingsForUser.class)
                                                                  .select("userId")
                                                                  .where()
                                                                  .eq("recurrent", false)
                                                                  .query();

    return find.where()
               .in("userid", subQuery)
               .or(Expr.raw(sql, keywords.toArray()), Expr.raw(sql2, keywords2.toArray()))
               .findCount();
  }

  public static List<UserProfile> getPage_BillingNonRecurrent(int start, int count, String filter, String sortOrder) {
    String[] filterList = filter.trim().split(",");
    List<String> keywords = new ArrayList<String>();
    List<String> keywords2 = new ArrayList<String>();

    String sql = "(userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
    String sql2  = "userid in (select bsu.userid from bil_settings_user bsu where bsu.plan in (select bp.plan_id from bil_plan bp where bp.name ilike ?";
    String str = "%" + filterList[0].trim().replaceAll(" ", "%") + "%";
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords2.add(str);


    for (int i = 1; i < filterList.length;  i++) {
      str = "%" + filterList[i].trim().replaceAll(" ", "%") + "%";

      sql += " or userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
      sql2 += " or bp.name ilike ?";

      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords2.add(str);
    }
    sql += ")";
    sql2 +="))";

    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean.createQuery(BillingSettingsForUser.class)
                                                                  .select("userId")
                                                                  .where()
                                                                  .eq("recurrent", false)
                                                                  .query();

    return find.where()
               .in("userid", subQuery)
               .or(Expr.raw(sql, keywords.toArray()), Expr.raw(sql2, keywords2.toArray()))
               .orderBy(sortOrder)
               .setFirstRow(start)
               .setMaxRows(count)
               .findList();
  }

  public static int getRowCount_BillingWithAss() {
    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean.createQuery(BillingSettingsForUser.class)
                                                                  .select("agent")
                                                                  .where()
                                                                  .isNotNull("agent")
                                                                  .query();

    return find.where().in("userid", subQuery).orderBy("lastname").findCount();
  }

  public static int getRowCountFiltered_BillingWithAss(String filter) {
    String[] filterList = filter.trim().split(",");
    List<String> keywords = new ArrayList<String>();
    List<String> keywords2 = new ArrayList<String>();

    String sql = "(userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
    String sql2  = "userid in (select bsu.userid from bil_settings_user bsu where bsu.plan in (select bp.plan_id from bil_plan bp where bp.name ilike ?";
    String str = "%" + filterList[0].trim().replaceAll(" ", "%") + "%";
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords2.add(str);


    for (int i = 1; i < filterList.length;  i++) {
      str = "%" + filterList[i].trim().replaceAll(" ", "%") + "%";

      sql += " or userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
      sql2 += " or bp.name ilike ?";

      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords2.add(str);
    }
    sql += ")";
    sql2 +="))";

    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean.createQuery(BillingSettingsForUser.class)
                                                                  .select("agent")
                                                                  .where()
                                                                  .isNotNull("agent")
                                                                  .query();

    return find.where()
               .in("userid", subQuery)
               .or(Expr.raw(sql, keywords.toArray()), Expr.raw(sql2, keywords2.toArray()))
               .findCount();
  }

  public static List<UserProfile> getPage_BillingWithAss(int start, int count, String filter, String sortOrder) {
    String[] filterList = filter.trim().split(",");
    List<String> keywords = new ArrayList<String>();
    List<String> keywords2 = new ArrayList<String>();

    String sql = "(userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
    String sql2  = "userid in (select bsu.userid from bil_settings_user bsu where bsu.plan in (select bp.plan_id from bil_plan bp where bp.name ilike ?";
    String str = "%" + filterList[0].trim().replaceAll(" ", "%") + "%";
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords2.add(str);


    for (int i = 1; i < filterList.length;  i++) {
      str = "%" + filterList[i].trim().replaceAll(" ", "%") + "%";

      sql += " or userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
      sql2 += " or bp.name ilike ?";

      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords2.add(str);
    }
    sql += ")";
    sql2 +="))";

    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean.createQuery(BillingSettingsForUser.class)
                                                                  .select("agent")
                                                                  .where()
                                                                  .isNotNull("agent")
                                                                  .query();

    return find.where()
               .in("userid", subQuery)
               .or(Expr.raw(sql, keywords.toArray()), Expr.raw(sql2, keywords2.toArray()))
               .orderBy(sortOrder)
               .setFirstRow(start)
               .setMaxRows(count)
               .findList();
  }

  public static int getRowCount_BillingPerSched(BillingSchedule.Type type) {
    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean.createQuery(BillingSettingsForUser.class)
                                                                  .select("userId")
                                                                  .where()
                                                                  .eq("bill_to", BillingEntity.Type.USER)
                                                                  .eq("schedule.name", type)
                                                                  .query();

    return find.where().in("userid", subQuery).findCount();
  }

  public static int getRowCountFiltered_BillingPerSched(BillingSchedule.Type type, String filter) {
    String[] filterList = filter.trim().split(",");
    List<String> keywords = new ArrayList<String>();
    List<String> keywords2 = new ArrayList<String>();

    String sql = "(userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
    String sql2  = "userid in (select bsu.userid from bil_settings_user bsu where bsu.plan in (select bp.plan_id from bil_plan bp where bp.name ilike ?";
    String str = "%" + filterList[0].trim().replaceAll(" ", "%") + "%";
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords2.add(str);


    for (int i = 1; i < filterList.length;  i++) {
      str = "%" + filterList[i].trim().replaceAll(" ", "%") + "%";

      sql += " or userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
      sql2 += " or bp.name ilike ?";

      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords2.add(str);
    }
    sql += ")";
    sql2 +="))";

    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean.createQuery(BillingSettingsForUser.class)
                                                                  .select("userId")
                                                                  .where()
                                                                  .eq("bill_to", BillingEntity.Type.USER)
                                                                  .eq("schedule.name", type)
                                                                  .query();

    return find.where()
               .and(Expr.in("userid", subQuery),
                  Expr.or(Expr.raw(sql, keywords.toArray()), Expr.raw(sql2, keywords2.toArray())))
               .findCount();
  }

  public static List<UserProfile> getPage_BillingPerSched(BillingSchedule.Type type,
                                                          int start,
                                                          int count,
                                                          String filter, String sortOrder) {
    String[] filterList = filter.trim().split(",");
    List<String> keywords = new ArrayList<String>();
    List<String> keywords2 = new ArrayList<String>();

    String sql = "(userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
    String sql2  = "userid in (select bsu.userid from bil_settings_user bsu where bsu.plan in (select bp.plan_id from bil_plan bp where bp.name ilike ?";
    String str = "%" + filterList[0].trim().replaceAll(" ", "%") + "%";
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords.add(str);
    keywords2.add(str);


    for (int i = 1; i < filterList.length;  i++) {
      str = "%" + filterList[i].trim().replaceAll(" ", "%") + "%";

      sql += " or userid ilike ? or email ilike ? or firstname ilike ? or lastname ilike ?";
      sql2 += " or bp.name ilike ?";

      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords.add(str);
      keywords2.add(str);
    }
    sql += ")";
    sql2 +="))";

    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean.createQuery(BillingSettingsForUser.class)
                                                                  .select("userId")
                                                                  .where()
                                                                  .eq("bill_to", BillingEntity.Type.USER)
                                                                  .eq("schedule.name", type)
                                                                  .query();

    return find.where()
               .in("userid", subQuery)
               .or(Expr.raw(sql, keywords.toArray()), Expr.raw(sql2, keywords2.toArray()))
               .orderBy(sortOrder)
               .setFirstRow(start)
               .setMaxRows(count)
               .findList();
  }

  private final static String SQL_COWORKERS_AUTOCOMPLETE  =
      "(SELECT userid " +
      "FROM bil_settings_user " +
      "WHERE bill_to = 'USER') " +
      "INTERSECT " +
      "(SELECT userid " +
      "FROM user_cmpy_link " +
      "WHERE cmpyid IN ( " +
      "    SELECT cmpyid   " +
      "    FROM user_cmpy_link   " +
      "    WHERE userid = ?))";


  public static List<UserProfile> getCoworkersByIdandName(String userid, String cwName) {

    SqlQuery rawSql = Ebean.createSqlQuery(SQL_COWORKERS_AUTOCOMPLETE);
    rawSql.setParameter(1, userid);
    List<SqlRow> list = rawSql.findList();
    List<String> workers = new ArrayList<>();
    for(SqlRow r : list) {
      workers.add(r.getString("userid"));
    }

    return find.where().in("userid", workers)
               .or(Expr.icontains("firstname", cwName),
                   Expr.icontains("lastname", cwName))
               .ne("userid", userid)
               .findList();
  }

  private final static String SQL_CMPY_COWORKERS_AUTOCOMPLETE  =
      "(SELECT userid " +
      "FROM bil_settings_user " +
      "WHERE bill_to = 'USER') " +
      "INTERSECT " +
      "(SELECT userid " +
      "FROM user_cmpy_link " +
      "WHERE cmpyid  = ?)";


  public static List<UserProfile> getCoworkersByCmpyAndName(String cmpyId, String cwName) {

    SqlQuery rawSql = Ebean.createSqlQuery(SQL_CMPY_COWORKERS_AUTOCOMPLETE);
    rawSql.setParameter(1, cmpyId);
    List<SqlRow> list = rawSql.findList();
    List<String> workers = new ArrayList<>();
    for(SqlRow r : list) {
      workers.add(r.getString("userid"));
    }

    return find.where().in("userid", workers)
               .or(Expr.icontains("firstname", cwName),
                   Expr.icontains("lastname", cwName))
               .findList();
  }


  public static List<UserProfile> doesExist(String userId) {
    return find.where().ieq("userId", userId).findList();
  }

  public static List<UserProfile> findActiveByEmail(String email) {
    return find.where().ieq("email", email).ne("status", APPConstants.STATUS_DELETED).findList();
  }

  public static List<UserProfile> findActiveUsers (long startTime, long endTime) {
    return find.where().gt("createdtimestamp",startTime).lt("createdtimestamp",endTime).ne("status", APPConstants.STATUS_DELETED).findList();
  }

  public static List<UserProfile> findActiveByEmailAndNotCreatorOrAdmin(String email, Trip trip) {
    final String sql = "userid NOT IN (SELECT userid\n" +
                       "FROM user_cmpy_link\n" +
                       "WHERE cmpyid = ? and linktype = " + UserCmpyLink.LinkType.ADMIN.getIdx() + ")";

    return find.where().ieq("email", email).
           eq("status", APPConstants.STATUS_ACTIVE).
           ne("userid", trip.createdby).
           raw(sql, trip.cmpyid).findList();
  }

  public static UserProfile findByUserIdAndNotCreatorOrAdmin(String userid, Trip trip) {
    final String sql = "userid NOT IN (SELECT userid\n" +
                       "FROM user_cmpy_link\n" +
                       "WHERE cmpyid = ? and linktype = " + UserCmpyLink.LinkType.ADMIN.getIdx() + ")";

    return find.where().eq("userid", userid).
                ne("userid", trip.createdby).
                raw(sql, trip.cmpyid).findUnique();
  }

  public static List<UserProfile> getUserProfiles(List<String> userIds) {
    return find.where().in("userId", userIds).findList();
  }

  public static List<UserProfile> getActiveUserProfiles(String userId) {
    return find.where().ieq("userId", userId).ne("status", -1).findList();
  }

  public static int getContUserProfiles(String userId) {
    userId += "%";
    return find.where().ilike("userId", userId).findCount();
  }

  public static List<UserProfile> findAll() {
    return find.all();
  }

  public Long getLastlogintimestamp() {
    return lastlogintimestamp;
  }

  public void setLastlogintimestamp(Long lastlogintimestamp) {
    this.lastlogintimestamp = lastlogintimestamp;
  }

  public String getUserid() {
    return userid;
  }

  public void setUserid(String userid) {
    this.userid = userid;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getFacebook() {
    return facebook;
  }

  public void setFacebook(String facebook) {
    this.facebook = facebook;
  }

  public String getTwitter() {
    return twitter;
  }

  public void setTwitter(String twitter) {
    this.twitter = twitter;
  }

  public String getWeb() {
    return web;
  }

  public void setWeb(String web) {
    this.web = web;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getProfilephoto() {
    return profilephoto;
  }

  public void setProfilephoto(String profilephoto) {
    this.profilephoto = profilephoto;
  }

  public String getProfilephotourl() {
    return profilephotourl;
  }

  public void setProfilephotourl(String profilephotourl) {
    this.profilephotourl = profilephotourl;
  }

  public int getUsertype() {
    return usertype;
  }

  public void setUsertype(int usertype) {
    this.usertype = usertype;
  }

  public int getFailedlogincount() {
    return failedlogincount;
  }

  public void setFailedlogincount(int failedlogincount) {
    this.failedlogincount = failedlogincount;
  }

  public String getPwdresetid() {
    return pwdresetid;
  }

  public void setPwdresetid(String pwdresetid) {
    this.pwdresetid = pwdresetid;
  }

  public Long getPwdresetexpiry() {
    return pwdresetexpiry;
  }

  public void setPwdresetexpiry(Long pwdresetexpiry) {
    this.pwdresetexpiry = pwdresetexpiry;
  }
}
