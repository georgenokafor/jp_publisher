//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.05.08 at 02:53:54 PM EDT 
//


package com.umapped.external.travelbound.api;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_LinkType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="t_LinkType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="flash"/>
 *     &lt;enumeration value="image"/>
 *     &lt;enumeration value="map"/>
 *     &lt;enumeration value="richmedia"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "t_LinkType")
@XmlEnum
public enum TLinkType {

    @XmlEnumValue("flash")
    FLASH("flash"),
    @XmlEnumValue("image")
    IMAGE("image"),
    @XmlEnumValue("map")
    MAP("map"),
    @XmlEnumValue("richmedia")
    RICHMEDIA("richmedia");
    private final String value;

    TLinkType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TLinkType fromValue(String v) {
        for (TLinkType c: TLinkType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
