package com.umapped.external.wetu.pins.list;

/**
 * Created by george on 2016-06-07.
 */

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "destination_image"
})
public class Destination extends Pin {
  @JsonProperty("destination_image")
  private Image destinationImage;

  @JsonProperty("destination_image")
  public Image getDestinationImage() {
    return destinationImage;
  }

  @JsonProperty("destination_image")
  public void setDestinationImage(Image destinationImage) {
    this.destinationImage = destinationImage;
  }
}
