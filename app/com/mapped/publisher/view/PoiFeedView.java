package com.mapped.publisher.view;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.mapped.publisher.persistence.FeedResource;
import com.mapped.publisher.persistence.PoiImportRS;
import models.publisher.FeedSrc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2015-02-04.
 */
public class PoiFeedView
    extends BaseView {

  public List<FeedSrc> srcList;
  public FeedSrc currSrc;
  public List<S3ObjectSummary> files;
  public PoiImportRS.ImportState currFeedState;
  public List<FeedResource> resources;

  //Initialize all defaults here
  public PoiFeedView() {
    files = new ArrayList<>(5);
    currFeedState = PoiImportRS.ImportState.UNPROCESSED;
  }
}
