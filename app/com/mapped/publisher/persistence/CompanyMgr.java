package com.mapped.publisher.persistence;

import java.sql.*;
import java.util.HashMap;

/**
 * Created by twong on 2014-05-28.
 */
public class CompanyMgr {

  public final static String SHARED_CMPIES = "select cmpyid, name from company where status = 0 and cmpyid in " +
                                             "(select cmpyid from trip where status <> -1 and tripid in (select tripid " +
                                             "from trip_share where status = 0 and userid = ?)) order by name;";

  public final static String TRIP_CMPIES = "select cmpyid, name from company where status = 0 and cmpyid in " +
      "(select cmpyid from trip where status <> -1 and (createdby = ? or tripid in (select tripid " +
      "from trip_share where status = 0 and userid = ?))) order by name;";


  public static HashMap<String, String> findSharingCmpies(String userId, Connection conn)
      throws SQLException {
    HashMap<String, String> results = new HashMap<String, String> ();
    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {

      prep = conn.prepareStatement(SHARED_CMPIES);
      prep.setString(1, userId);
      rs = prep.executeQuery();

      if (rs == null) {
        return results;
      }

      while (rs.next()) {
        String cmpyId = rs.getString("cmpyId");
        String cmpyName = rs.getString("name");
        results.put(cmpyId, cmpyName);
      }
      return results;
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static HashMap<String, String> findCmpies(String userId, Connection conn)
      throws SQLException {
    HashMap<String, String> results = new HashMap<String, String> ();
    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {

      prep = conn.prepareStatement(TRIP_CMPIES);
      prep.setString(1, userId);
      prep.setString(2, userId);
      rs = prep.executeQuery();

      if (rs == null) {
        return results;
      }

      while (rs.next()) {
        String cmpyId = rs.getString("cmpyId");
        String cmpyName = rs.getString("name");
        results.put(cmpyId, cmpyName);
      }
      return results;
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }
}
