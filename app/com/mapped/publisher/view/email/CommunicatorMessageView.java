package com.mapped.publisher.view.email;

import com.mapped.publisher.view.BaseView;

/**
 * Created by surge on 2016-02-04.
 */
public class CommunicatorMessageView
    extends BaseView {
  public String title;
  public String toEmail;
  public String fromName;
  public String fromEmail;
  public String message;
  public String time;
  public boolean replyEnabled;
  public String id;
  public String webItineraryUrl;
  public String tripName;

  public CommunicatorMessageView() {
    id = "";
    replyEnabled = true;
    time = "";
    toEmail = "";
  }
}
