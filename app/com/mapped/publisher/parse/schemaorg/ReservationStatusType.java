package com.mapped.publisher.parse.schemaorg;

/**
 * Created by surge on 2015-05-11.
 */
public enum ReservationStatusType {
  CONFIRMED("http://schema.org/Confirmed"),
  CANCELLED("http://schema.org/Cancelled");

  private String namespace;

  private ReservationStatusType(String ns) {
    this.namespace = ns;
  }

  public static ReservationStatusType fromString(String namespace) {
    switch (namespace) {
      case "http://schema.org/Confirmed":
        return CONFIRMED;
      case "http://schema.org/Cancelled":
        return CANCELLED;
    }
    return null;
  }

  public String getNamespace() {
    return namespace;
  }

}
