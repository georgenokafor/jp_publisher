package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-24
 * Time: 1:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class TripSearchForm {
    private String inCmpyId;
    private String inSearchTerm;
    private int tripType;
    private int searchType;
    private int inStartPos;
    private int inStartAdminPos;
    private int searchStatus;
    private boolean inAllTrips;
    private boolean inArchived;

    private String inTableName;

    public String getInCmpyId() {
        return inCmpyId;
    }

    public void setInCmpyId(String inCmpyId) {
        this.inCmpyId = inCmpyId;
    }

    public String getInSearchTerm() {
        return inSearchTerm;
    }

    public void setInSearchTerm(String inSearchTerm) {
        this.inSearchTerm = inSearchTerm;
    }

    public int getTripType() {
        return tripType;
    }

    public void setTripType(int tripType) {
        this.tripType = tripType;
    }

    public int getSearchType() {
        return searchType;
    }

    public void setSearchType(int searchType) {
        this.searchType = searchType;
    }

    public int getInStartPos() {
        return inStartPos;
    }

    public void setInStartPos(int inStartPos) {
        this.inStartPos = inStartPos;
    }

    public String getInTableName() {
        return inTableName;
    }

    public void setInTableName(String inTableName) {
        this.inTableName = inTableName;
    }

    public int getInStartAdminPos() {
        return inStartAdminPos;
    }

    public void setInStartAdminPos(int inStartAdminPos) {
        this.inStartAdminPos = inStartAdminPos;
    }

    public int getSearchStatus() {
        return searchStatus;
    }

    public void setSearchStatus(int searchStatus) {
        this.searchStatus = searchStatus;
    }

    public boolean isInAllTrips() {
        return inAllTrips;
    }

    public void setInAllTrips(boolean inAllTrips) {
        this.inAllTrips = inAllTrips;
    }

    public boolean isInArchived() {
        return inArchived;
    }

    public void setInArchived(boolean inArchived) {
        this.inArchived = inArchived;
    }
}
