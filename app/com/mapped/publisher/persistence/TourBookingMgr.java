package com.mapped.publisher.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-02
 * Time: 3:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class TourBookingMgr {

    public static int countActiveBookings (String tourId) throws SQLException {
        ArrayList<TourCmpyRS> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Statement stat= null;
        Connection conn = null;
        try {
            conn = DBConnectionMgr.getConnection4Publisher();
            prep = conn.prepareStatement("select count(tb.detailsid) as numtours from tour_booking tb,  trip t where tb.tripid = t.tripid and t.status <> -1 and tb.tourid = ? and tb.status = 0;");
            prep.setString(1, tourId);


            rs = prep.executeQuery();
            if (rs == null)
                return 0;

            while (rs.next()) {
                return rs.getInt("numtours");
            }
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public static Map<String, Integer> countActiveBookingsByCmpy (String tourId) throws SQLException {
        ArrayList<TourCmpyRS> results = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement prep= null;
        Statement stat= null;
        Connection conn = null;
        try {
            conn = DBConnectionMgr.getConnection4Publisher();
            prep = conn.prepareStatement("select c.cmpyid as cmpyid, count(tb.detailsid) as numtours from tour_booking tb,  trip t, company c where tb.tripid = t.tripid and t.status <> -1 and tb.tourid = ? and tb.status = 0 and t.cmpyid = c.cmpyid group by c.cmpyid;");
            prep.setString(1, tourId);

            rs = prep.executeQuery();
            if (rs == null)
                return null;

            HashMap<String, Integer> counts = new HashMap<String, Integer>();

            while (rs.next()) {
                int c = rs.getInt("numtours");
                String cmpyId = rs.getString("cmpyid");
                counts.put(cmpyId, c);
            }
            return counts;
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
}
