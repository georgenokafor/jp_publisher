package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freshbooks.model.Item;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.utils.Log;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-03-26.
 */
@Entity
@Table(name = "bil_plan")
public class BillingPlan
    extends Model {
  public enum PlanType {
    PLAN,
    ADDON
  }

  public enum Currency {
    USD,
    CAD,
    EUR,
    AUD
  }

  public enum BillType {
    INV, //invoice
    BOTH, // invoice + credit card
    CC //credit card
  }

  public static Model.Finder<Long, BillingPlan> find = new Finder(BillingPlan.class);
  @Id
  private Long                 planId;
  private String               name;
  private String               description;
  private BillingSchedule.Type schedule;
  private Float                cost;
  @Enumerated(value = EnumType.STRING)
  private Currency             currency;
  private Long                 expireTs;
  private Boolean              enabled;
  private String               meta;
  private Boolean              visible;
  private Boolean              newUserState;
  @ManyToOne
  @JoinColumn(name = "parent_plan", nullable = true, referencedColumnName = "plan_id")
  private BillingPlan parentPlan;
  @ManyToOne
  @JoinColumn(name = "cons_id")
  private Consortium  consortium;
  @Enumerated(value = EnumType.STRING)
  private PlanType    type;
  private Integer     trialLength;
  private Long        createdTs;
  private String      createdBy;
  private Long        modifiedTs;
  private String      modifiedBy;
  @Enumerated(value = EnumType.STRING)
  private BillType    billingType;
  @Version
  private int         version;

  /**
   * If in the future we will have various types of plans each will be
   * assigned to a dedicated variable in this wrapper class. For now freshbooks
   * is the only type.
   */
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class ItemDataWrapper {
    public Item freshbooks;
  }

  public static BillingPlan buildPlan(String userId) {
    BillingPlan bp = new BillingPlan();
    bp.setPlanId(DBConnectionMgr.getUniqueLongId());
    bp.setCreatedBy(userId);
    bp.setCreatedTs(System.currentTimeMillis());
    bp.setModifiedBy(userId);
    bp.setModifiedTs(bp.createdTs);
    return bp;
  }

  public static List<BillingPlan> findPlans(){
    return find.where().eq("type", PlanType.PLAN).orderBy("modified_ts DESC").findList();
  }


  public static List<BillingPlan> findPlanAddons(Long planId){
    return find.where().eq("type", PlanType.ADDON).eq("parent_plan",planId).findList();
  }

  public static List<BillingPlan> findPlansPerScheduleFeatures(BillingSchedule.Type schedule, boolean isPro, BillType billType, List<Integer> consIds) {
    long currTime = System.currentTimeMillis();

    String featureName = "%Pro Features%";

    com.avaje.ebean.Query<SysFeature> sysFeatures = Ebean
          .createQuery(SysFeature.class)
          .select("pk")
          .where()
          .ilike("name", featureName)
          .query();


    com.avaje.ebean.Query<BillingPlanFeature> planFeatures = Ebean
        .createQuery(BillingPlanFeature.class)
        .select("pk.planId")
        .where()
        .in("feat_pk", sysFeatures)
        .query();

    if (isPro) {
      if (consIds.contains(0)) {
        return find
            .where()
            .or(Expr.ge("expire_ts", currTime), Expr.isNull("expire_ts"))
            .eq("type", PlanType.PLAN)
            .eq("enabled", true)
            .eq("schedule", schedule.name())
            .eq("billing_type", billType.name())
            .or(Expr.in("cons_id", consIds), Expr.isNull("cons_id"))
            .in("plan_id", planFeatures)
            .orderBy("name ASC")
            .findList();
      } else {
        return find
            .where()
            .or(Expr.ge("expire_ts", currTime), Expr.isNull("expire_ts"))
            .eq("type", PlanType.PLAN)
            .eq("enabled", true)
            .eq("schedule", schedule.name())
            .eq("billing_type", billType.name())
            .in("cons_id", consIds)
            .in("plan_id", planFeatures)
            .orderBy("name ASC")
            .findList();
      }
    } else {
      if (consIds.contains(0)) {
        return find
            .where()
            .or(Expr.ge("expire_ts", currTime), Expr.isNull("expire_ts"))
            .eq("type", PlanType.PLAN)
            .eq("enabled", true)
            .eq("schedule", schedule.name())
            .eq("billing_type", billType.name())
            .or(Expr.in("cons_id", consIds), Expr.isNull("cons_id"))
            .notIn("plan_id", planFeatures)
            .orderBy("name ASC")
            .findList();
      } else {
        return find
            .where()
            .or(Expr.ge("expire_ts", currTime), Expr.isNull("expire_ts"))
            .eq("type", PlanType.PLAN)
            .eq("enabled", true)
            .eq("schedule", schedule.name())
            .eq("billing_type", billType.name())
            .in("cons_id", consIds)
            .notIn("plan_id", planFeatures)
            .orderBy("name ASC")
            .findList();
      }
    }
  }


  /**
   * Find all active (enabled and not expired) plans that can be assigned to a
   * company.
   *
   * @return List of available plans
   */
  public static List<BillingPlan> plansForCmpies() {
    long currTime = System.currentTimeMillis();

    com.avaje.ebean.Query<BillingSchedule> subQuery = Ebean
        .createQuery(BillingSchedule.class)
        .select("name")
        .where()
        .eq("assignable_cmpy", true)
        .query();


    return find
        .where()
        .or(Expr.ge("expire_ts", currTime), Expr.isNull("expire_ts"))
        .eq("enabled", true)
        .in("schedule", subQuery)
        .findList();
  }

  public static BillingPlan cmpyPlan(String cmpyId) {
    com.avaje.ebean.Query<BillingSettingsForCmpy> subQuery = Ebean
        .createQuery(BillingSettingsForCmpy.class)
        .select("plan")
        .where()
        .eq("cmpyid", cmpyId)
        .query();

    return find.where().in("plan_id", subQuery).findUnique();
  }

  public static BillingPlan userPlan(String userId) {
    com.avaje.ebean.Query<BillingSettingsForUser> subQuery = Ebean
        .createQuery(BillingSettingsForUser.class)
        .select("plan")
        .where()
        .eq("userid", userId)
        .query();

    return find.where().in("plan_id", subQuery).findUnique();
  }

  public static List<BillingPlan> getAllAddons() {
    return find.where().eq("type", "ADDON").findList();
  }

  public static List<BillingPlan> plansForUsers() {
    long currTime = System.currentTimeMillis();

    com.avaje.ebean.Query<BillingSchedule> subQuery = Ebean
        .createQuery(BillingSchedule.class)
        .select("name")
        .where()
        .eq("assignable_user", true)
        .query();


    return find
        .where()
        .or(Expr.ge("expire_ts", currTime), Expr.isNull("expire_ts"))
        .eq("enabled", true)
        .in("schedule", subQuery)
        .findList();
  }

  public static List<BillingPlan> newAddonsForUser(String userId, Long currPlanId, boolean hideInvisible) {
    long currTime = System.currentTimeMillis();


    com.avaje.ebean.Query<BillingPlan> allAddons = Ebean
        .createQuery(BillingPlan.class)
        .select("planId")
        .where()
        .eq("type", PlanType.ADDON)
        .eq("parent_plan", currPlanId)
        .query();

    com.avaje.ebean.Query<BillingAddonsForUser> userAddons = Ebean
        .createQuery(BillingAddonsForUser.class)
        .select("pk.planId")
        .where()
        .eq("userid", userId)
        .query();

    if (hideInvisible) {
      return find
          .where()
          .or(Expr.ge("expire_ts", currTime), Expr.isNull("expire_ts"))
          .eq("enabled", true)
          .eq("visible", true)
          .and(Expr.in("plan_id", allAddons), Expr.not(Expr.in("plan_id", userAddons)))
          .findList();
    } else {
      return find
          .where()
          .or(Expr.ge("expire_ts", currTime), Expr.isNull("expire_ts"))
          .eq("enabled", true)
          .and(Expr.in("plan_id", allAddons), Expr.not(Expr.in("plan_id", userAddons)))
          .findList();
    }
  }

  public static List<BillingPlan> plansPerSchedule(BillingSchedule.Type schedule) {
    long currTime = System.currentTimeMillis();
    return find
        .where()
        .or(Expr.ge("expire_ts", currTime), Expr.isNull("expire_ts"))
        .eq("type", PlanType.PLAN)
        .eq("enabled", true)
        .eq("schedule", schedule.name())
        .orderBy("name ASC")
        .findList();
  }

  public BillingPlan getParentPlan() {
    return parentPlan;
  }

  public void setParentPlan(BillingPlan parentPlan) {
    this.parentPlan = parentPlan;
  }

  public Boolean getNewUserState() {
    return newUserState;
  }

  public void setNewUserState(Boolean newUserState) {
    this.newUserState = newUserState;
  }

  public Integer getTrialLength() {
    return trialLength;
  }

  public void setTrialLength(Integer trialLength) {
    this.trialLength = trialLength;
  }

  public PlanType getType() {
    return type;
  }

  public void setType(PlanType type) {
    this.type = type;
  }

  public Consortium getConsortium() {
    return consortium;
  }

  public void setConsortium(Consortium consortium) {
    this.consortium = consortium;
  }

  public Boolean getVisible() {
    return visible;
  }

  public void setVisible(Boolean visible) {
    this.visible = visible;
  }

  public Item getFreshbookItemData() {
    if (this.meta == null) {
      return null;
    }

    ObjectMapper    m   = new ObjectMapper();
    ItemDataWrapper idw = null;
    try {
      idw = m.readValue(this.meta, ItemDataWrapper.class);
    }
    catch (Exception je) {
      Log.err("Failed to unmarshal Freshbooks Item Data from JSON:", je);
    }

    return idw.freshbooks;
  }

  public void setFreshbookItemData(Item item) {
    ObjectMapper m = new ObjectMapper();
    m.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    //Needs this black magic to avoid picking up methods that looks like real fields
    m.setVisibility(m.getSerializationConfig()
                     .getDefaultVisibilityChecker()
                     .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                     .withIsGetterVisibility(JsonAutoDetect.Visibility.NONE));
    String result = null;

    ItemDataWrapper idw = new ItemDataWrapper();
    idw.freshbooks = item;

    try {
      //result = m.writerWithDefaultPrettyPrinter().writeValueAsString(idw);
      result = m.writeValueAsString(idw);
    }
    catch (JsonProcessingException jpe) {
      Log.err("Failed to generate Item JSON Data:", jpe);
    }

    this.setMeta(result);
  }

  public void markModified(String userId) {
    setModifiedBy(userId);
    setModifiedTs(System.currentTimeMillis());
  }

  public String getDescriptive() {
    StringBuilder sb = new StringBuilder();
    sb.append(name);
    sb.append(" (");
    sb.append(currency.name());
    sb.append(" ");
    sb.append(String.format("%.2f", cost));
    sb.append(") Charged ");
    sb.append(schedule.getScheduleName());
    return sb.toString();
  }

  public String getMeta() {
    return meta;
  }

  public void setMeta(String meta) {
    this.meta = meta;
  }

  public Long getPlanId() {
    return planId;
  }

  public void setPlanId(Long planId) {
    this.planId = planId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Boolean getEnabled() {
    return enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BillingSchedule.Type getSchedule() {
    return schedule;
  }

  public void setSchedule(BillingSchedule.Type schedule) {
    this.schedule = schedule;
  }

  public Float getCost() {
    return cost;
  }

  public void setCost(Float cost) {
    this.cost = cost;
  }

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public Long getExpireTs() {
    return expireTs;
  }

  public void setExpireTs(Long expireTs) {
    this.expireTs = expireTs;
  }

  public Long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Long createdTs) {
    this.createdTs = createdTs;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Long modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public BillType getBillingType() {
    return billingType;
  }

  public void setBillingType(BillType billingType) {
    this.billingType = billingType;
  }
}
