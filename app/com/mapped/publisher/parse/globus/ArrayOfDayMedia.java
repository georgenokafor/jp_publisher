
package com.mapped.publisher.parse.globus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfDayMedia complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ArrayOfDayMedia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DayMedia" type="{http://webapi.globusandcosmos.com/TourInformation.xsd}DayMedia" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDayMedia", namespace = "http://webapi.globusandcosmos.com/TourInformation.xsd", propOrder = {
    "dayMedias"
})
public class ArrayOfDayMedia {

    @XmlElement(name = "DayMedia", nillable = true)
    protected ArrayList<DayMedia> dayMedias;

    /**
     * Gets the value of the dayMedias property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dayMedias property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDayMedias().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DayMedia }
     *
     *
     */
    public ArrayList<DayMedia> getDayMedias() {
        if (dayMedias == null) {
            dayMedias = new ArrayList<DayMedia>();
        }
        return this.dayMedias;
    }

}
