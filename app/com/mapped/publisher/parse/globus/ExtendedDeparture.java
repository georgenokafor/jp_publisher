
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ExtendedDeparture complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExtendedDeparture">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Brand" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AirStartDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="LandStartDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="LandEndDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="LandOnlyPrice" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="ShipName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GuaranteedDeparture" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IntraTourAirRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IntraTourAir" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="IntraTourAirTax" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Single" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Triple" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="TourStartAirportCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TourEndAirportCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtendedDeparture", namespace = "http://webapi.globusandcosmos.com/ExtendedDepartureList.xsd", propOrder = {
    "brand",
    "name",
    "airStartDate",
    "landStartDate",
    "landEndDate",
    "landOnlyPrice",
    "shipName",
    "status",
    "departureCode",
    "guaranteedDeparture",
    "intraTourAirRequired",
    "intraTourAir",
    "intraTourAirTax",
    "single",
    "triple",
    "tourStartAirportCode",
    "tourEndAirportCode"
})
public class ExtendedDeparture {

    @XmlElement(name = "Brand")
    protected String brand;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "AirStartDate", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar airStartDate;
    @XmlElement(name = "LandStartDate", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar landStartDate;
    @XmlElement(name = "LandEndDate", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar landEndDate;
    @XmlElement(name = "LandOnlyPrice")
    protected double landOnlyPrice;
    @XmlElement(name = "ShipName")
    protected String shipName;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "DepartureCode")
    protected String departureCode;
    @XmlElement(name = "GuaranteedDeparture")
    protected boolean guaranteedDeparture;
    @XmlElement(name = "IntraTourAirRequired")
    protected boolean intraTourAirRequired;
    @XmlElement(name = "IntraTourAir")
    protected double intraTourAir;
    @XmlElement(name = "IntraTourAirTax")
    protected double intraTourAirTax;
    @XmlElement(name = "Single")
    protected double single;
    @XmlElement(name = "Triple")
    protected double triple;
    @XmlElement(name = "TourStartAirportCode")
    protected String tourStartAirportCode;
    @XmlElement(name = "TourEndAirportCode")
    protected String tourEndAirportCode;

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrand(String value) {
        this.brand = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the airStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAirStartDate() {
        return airStartDate;
    }

    /**
     * Sets the value of the airStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAirStartDate(XMLGregorianCalendar value) {
        this.airStartDate = value;
    }

    /**
     * Gets the value of the landStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLandStartDate() {
        return landStartDate;
    }

    /**
     * Sets the value of the landStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLandStartDate(XMLGregorianCalendar value) {
        this.landStartDate = value;
    }

    /**
     * Gets the value of the landEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLandEndDate() {
        return landEndDate;
    }

    /**
     * Sets the value of the landEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLandEndDate(XMLGregorianCalendar value) {
        this.landEndDate = value;
    }

    /**
     * Gets the value of the landOnlyPrice property.
     * 
     */
    public double getLandOnlyPrice() {
        return landOnlyPrice;
    }

    /**
     * Sets the value of the landOnlyPrice property.
     * 
     */
    public void setLandOnlyPrice(double value) {
        this.landOnlyPrice = value;
    }

    /**
     * Gets the value of the shipName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipName() {
        return shipName;
    }

    /**
     * Sets the value of the shipName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipName(String value) {
        this.shipName = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the departureCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureCode() {
        return departureCode;
    }

    /**
     * Sets the value of the departureCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureCode(String value) {
        this.departureCode = value;
    }

    /**
     * Gets the value of the guaranteedDeparture property.
     * 
     */
    public boolean isGuaranteedDeparture() {
        return guaranteedDeparture;
    }

    /**
     * Sets the value of the guaranteedDeparture property.
     * 
     */
    public void setGuaranteedDeparture(boolean value) {
        this.guaranteedDeparture = value;
    }

    /**
     * Gets the value of the intraTourAirRequired property.
     * 
     */
    public boolean isIntraTourAirRequired() {
        return intraTourAirRequired;
    }

    /**
     * Sets the value of the intraTourAirRequired property.
     * 
     */
    public void setIntraTourAirRequired(boolean value) {
        this.intraTourAirRequired = value;
    }

    /**
     * Gets the value of the intraTourAir property.
     * 
     */
    public double getIntraTourAir() {
        return intraTourAir;
    }

    /**
     * Sets the value of the intraTourAir property.
     * 
     */
    public void setIntraTourAir(double value) {
        this.intraTourAir = value;
    }

    /**
     * Gets the value of the intraTourAirTax property.
     * 
     */
    public double getIntraTourAirTax() {
        return intraTourAirTax;
    }

    /**
     * Sets the value of the intraTourAirTax property.
     * 
     */
    public void setIntraTourAirTax(double value) {
        this.intraTourAirTax = value;
    }

    /**
     * Gets the value of the single property.
     * 
     */
    public double getSingle() {
        return single;
    }

    /**
     * Sets the value of the single property.
     * 
     */
    public void setSingle(double value) {
        this.single = value;
    }

    /**
     * Gets the value of the triple property.
     * 
     */
    public double getTriple() {
        return triple;
    }

    /**
     * Sets the value of the triple property.
     * 
     */
    public void setTriple(double value) {
        this.triple = value;
    }

    /**
     * Gets the value of the tourStartAirportCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTourStartAirportCode() {
        return tourStartAirportCode;
    }

    /**
     * Sets the value of the tourStartAirportCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTourStartAirportCode(String value) {
        this.tourStartAirportCode = value;
    }

    /**
     * Gets the value of the tourEndAirportCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTourEndAirportCode() {
        return tourEndAirportCode;
    }

    /**
     * Sets the value of the tourEndAirportCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTourEndAirportCode(String value) {
        this.tourEndAirportCode = value;
    }

}
