package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.publisher.persistence.billing.BraintreeSunbscription;
import com.mapped.publisher.utils.Log;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-12-11.
 */
@Entity
@Table(name = "bil_user_addon")
public class BillingAddonsForUser
    extends Model {

  public static Finder<BillingAddonsForUserID, BillingAddonsForUser> find = new Finder(BillingAddonsForUser.class);
  @Id
  BillingAddonsForUserID pk;
  @Column(name = "trial_cnt")
  Integer                trialCount;
  @Column(nullable = true)
  Long                   startTs;
  Long    cutoffTs;
  Boolean enabled;
  Long    createdTs;
  String  createdBy;
  Long    modifiedTs;
  String  modifiedBy;
  String  meta;
  @Version
  int version;

  @Embeddable
  public static class BillingAddonsForUserID {

    /**
     * User id who has this addon
     */
    @Column(name = "userid")
    public String userid;
    /**
     * Which add add-on plan is being used
     */
    @Column(name = "plan_id")
    public Long   planId;

    public String getUserid() {
      return userid;
    }

    public void setUserid(String userid) {
      this.userid = userid;
    }

    public Long getPlanId() {
      return planId;
    }

    public void setPlanId(Long planId) {
      this.planId = planId;
    }

    @Override
    public int hashCode() {
      return userid.hashCode() * planId.hashCode();
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof BillingAddonsForUserID)) {
        return false;
      }
      BillingAddonsForUserID ok = (BillingAddonsForUserID) o;
      if (!ok.userid.equals(userid)) {
        return false;
      }
      if (!ok.planId.equals(planId)) {
        return false;
      }
      return true;
    }
  }

  /**
   * If in the future we will have various types of integrations each will be
   * assigned to a dedicated variable in this wrapper class. For now Braintree
   * is the only type.
   */
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class MetaDataWrapper {
    public BraintreeSunbscription braintree;
  }

  public MetaDataWrapper getMetaDataWrapper() {
    if (this.meta == null) {
      return null;
    }
    ObjectMapper m = new ObjectMapper();
    m.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    MetaDataWrapper md = null;
    try {
      md = m.readValue(this.meta, MetaDataWrapper.class);
    }
    catch (Exception je) {
      Log.err("Failed to unmarshal Freshbooks Client Data from JSON:", je);
    }
    return md;
  }

  public void setBraintreeData(BraintreeSunbscription braintreeData) {
    ObjectMapper m = new ObjectMapper();
    m.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    //Needs this black magic to avoid picking up methods that looks like real fields
    m.setVisibilityChecker(m.getSerializationConfig()
                            .getDefaultVisibilityChecker()
                            .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                            .withIsGetterVisibility(JsonAutoDetect.Visibility.NONE)
                            .withGetterVisibility(JsonAutoDetect.Visibility.NONE));
    String result = null;

    MetaDataWrapper idw = getMetaDataWrapper();

    if (idw == null) {
      idw = new MetaDataWrapper();
    }

    idw.braintree = braintreeData;

    try {
      //result = m.writerWithDefaultPrettyPrinter().writeValueAsString(idw);
      result = m.writeValueAsString(idw);
    }
    catch (JsonProcessingException jpe) {
      Log.err("Failed to generate Braintree Client JSON Data:", jpe);
    }

    this.setMeta(result);
  }

  public BraintreeSunbscription getBraintree() {
    MetaDataWrapper md = getMetaDataWrapper();
    if (md != null)
      return md.braintree;
    else
      return null;
  }

  public static int getEnabledCount(Long planId) {
    return find.where().eq("pk.planId", planId).eq("enabled", true).findCount();
  }


  public static int getDisabledCount(Long planId) {
    return find.where().eq("pk.planId", planId).eq("enabled", false).findCount();
  }


  /**
   * @param userId      - User id of the user being assigned an addon
   * @param planId      - Plan id of the addon, must be an addon plan (not checked here)
   * @param adminUserId - Admin user performing association
   * @return
   */
  public static BillingAddonsForUser buildAddonForUser(String userId, Long planId, String adminUserId) {
    BillingAddonsForUser userAddon = new BillingAddonsForUser();

    BillingAddonsForUserID id = new BillingAddonsForUserID();
    id.userid = userId;
    id.planId = planId;
    userAddon.setPk(id);
    userAddon.setCreatedBy(adminUserId);
    userAddon.setCreatedTs(System.currentTimeMillis());
    userAddon.setModifiedBy(adminUserId);
    userAddon.setModifiedTs(userAddon.createdTs);
    userAddon.setTrialCount(0);
    userAddon.setEnabled(false);
    return userAddon;
  }

  public static BillingAddonsForUser getByPk(Long planId, String userId) {
    BillingAddonsForUserID pk = new BillingAddonsForUserID();
    pk.planId = planId;
    pk.userid = userId;

    return find.byId(pk);
  }

  public static List<BillingAddonsForUser> getAddonsForUser(String userid) {
    return find.where().eq("userid", userid).findList();
  }

  public static List<BillingAddonsForUser> getPaidActiveAddonsForUser(String userid) {
    com.avaje.ebean.Query<BillingPlan> subQuery = Ebean
        .createQuery(BillingPlan.class)
        .select("planId")
        .where()
        .eq("enabled",true)
        .gt("cost", 0)
        .isNotNull("parent_plan")
        .query();

    return find.where()
               .eq("userid", userid)
               .eq("enabled", true)
               .in("plan_id", subQuery)
               .isNotNull("start_ts")
               .findList();
  }

  public static BillingAddonsForUser getByBraintreeSubscriptionId (String subId) {
    if (subId != null) {
      String meta = "%braintree%\"subToken\":\"" + subId +"\"%";
      return find.where().like("meta", meta).findUnique();
    }
    return null;
  }

  public void markModified(String userId) {
    setModifiedBy(userId);
    setModifiedTs(System.currentTimeMillis());
  }

  public Boolean getEnabled() {
    return enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public BillingAddonsForUserID getPk() {
    return pk;
  }

  public void setPk(BillingAddonsForUserID pk) {
    this.pk = pk;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public Integer getTrialCount() {
    return trialCount;
  }

  public void setTrialCount(Integer trialCount) {
    this.trialCount = trialCount;
  }

  public Long getStartTs() {
    return startTs;
  }

  public void setStartTs(Long startTs) {
    this.startTs = startTs;
  }

  public Long getCutoffTs() {
    return cutoffTs;
  }

  public void setCutoffTs(Long cutoffTs) {
    this.cutoffTs = cutoffTs;
  }

  public Long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Long createdTs) {
    this.createdTs = createdTs;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Long modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public String getMeta() {
    return meta;
  }

  public void setMeta(String meta) {
    this.meta = meta;
  }
}
