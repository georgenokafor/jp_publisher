package models.publisher;

import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.persistence.IUmappedModel;
import com.mapped.publisher.persistence.IUmappedModelWithImage;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Link between Travel42 destinations and imported Travel42 images
 * <p>
 * Created by surge on 2016-11-04.
 */
@Entity
@Table(name = "t42_destination_image")
public class T42DestinationImage
    extends UmappedModelWithImage
    implements IUmappedModel<Long, T42DestinationImage> {

  public final static Model.Finder<Long, T42DestinationImage> find = new Finder(T42DestinationImage.class);

  /**
   * Umapped standard generated PK
   */
  @Id
  Long      pk;
  @ManyToOne
  @JoinColumn(name = "image_pk", nullable = true, referencedColumnName = "pk")
  FileImage image;
  Integer   geoPlaceKey;
  @Column(name = "t42_image_id", nullable = false)
  Integer   t42ImageId;
  Integer   rank;
  Timestamp lastSeenTs;
  Timestamp createdTs;
  Timestamp modifiedTs;

  public static T42DestinationImage build(Integer geoPlaceKey, Integer t42ImageId, FileImage fi) {
    T42DestinationImage t42di = new T42DestinationImage();
    t42di.setPk(DBConnectionMgr.getUniqueLongId());
    t42di.setGeoPlaceKey(geoPlaceKey);
    t42di.setT42ImageId(t42ImageId);
    t42di.setCreatedTs(Timestamp.from(Instant.now()));
    t42di.setFileImage(fi);
    t42di.setModifiedTs(t42di.createdTs);
    t42di.setLastSeenTs(t42di.createdTs);
    return t42di;
  }

  public T42DestinationImage markSeen(){
    setLastSeenTs(Timestamp.from(Instant.now()));
    return  this;
  }

  public static  T42DestinationImage findHighestRanked(Integer geoPlaceKey) {
    return find.where().eq("geo_place_key", geoPlaceKey).orderBy("rank").setMaxRows(1).findUnique();
  }

  public static List<T42DestinationImage> byGeoPlaceKey(Integer geoPlaceKey) {
    return find.where().eq("geo_place_key", geoPlaceKey).orderBy("rank").findList();
  }

  public static T42DestinationImage findByDestinationAndId(Integer geoPlaceKey, Integer t42imageId) {
    return find.where().eq("t42_image_id", t42imageId).eq("geo_place_key", geoPlaceKey).findUnique();
  }

  public Integer getT42ImageId() {
    return t42ImageId;
  }

  public T42DestinationImage setT42ImageId(Integer t42ImageId) {
    this.t42ImageId = t42ImageId;
    return this;
  }

  public Integer getGeoPlaceKey() {
    return geoPlaceKey;
  }

  public T42DestinationImage setGeoPlaceKey(Integer geoPlaceKey) {
    this.geoPlaceKey = geoPlaceKey;
    return this;
  }

  public Integer getRank() {
    return rank;
  }

  public T42DestinationImage setRank(Integer rank) {
    this.rank = rank;
    return this;
  }

  public Timestamp getLastSeenTs() {
    return lastSeenTs;
  }

  public T42DestinationImage setLastSeenTs(Timestamp lastSeenTs) {
    this.lastSeenTs = lastSeenTs;
    return this;
  }

  @Override
  public T42DestinationImage getByPk(Long pk) {
    return find.byId(pk);
  }

  @Override
  public Long getPk() {
    return pk;
  }

  public T42DestinationImage setPk(Long pk) {
    this.pk = pk;
    return this;
  }

  @Override
  public FileImage getFileImage() {
    return getImage();
  }

  public FileImage getImage() {
    return image;
  }

  public T42DestinationImage setImage(FileImage image) {
    this.image = image;
    return this;
  }

  @Override
  public IUmappedModelWithImage setFileImage(FileImage image) {
    return setImage(image);
  }

  @Override
  public String getPkAsString() {
    return pk.toString();
  }

  @Override
  public Long getCreatedByPk() {
    return Account.AID_PUBLISHER;
  }

  @Override
  public Long getModifiedByPk() {
    return Account.AID_PUBLISHER;
  }

  @Override
  public String getCreatedByLegacy() {
    return Account.LEGACY_PUBLISHER_USERID;
  }

  @Override
  public String getModifiedByLegacy() {
    return Account.LEGACY_PUBLISHER_USERID;
  }

  @Override
  public Long getCreatedEpoch() {
    return createdTs.getTime();
  }

  @Override
  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public T42DestinationImage setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
    return this;
  }

  @Override
  public Long getModifiedEpoch() {
    return modifiedTs.getTime();
  }

  @Override
  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public T42DestinationImage setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
    return this;
  }
}
