package com.mapped.publisher.form.admin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2016-01-04.
 */
public class FormFeature {
  public Integer featId;
  public String  featureName;
  public String  featureDescription;
  public Boolean featureEnabled;
  public Boolean featureUserControlled;
  public List<Integer> featureCapabilities = new ArrayList<>();
}
