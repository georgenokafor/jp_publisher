package com.mapped.publisher.form;

import models.publisher.ImageLibrary;

import java.util.List;

/**
 * Created by george on 2016-04-09.
 */
public class FileForm {
  private String inDetailId;
  private String inFileId;

  private String              inCmpyId;
  private String              inTripId;
  private String              inTemplateId;
  private Long                inNoteId;
  private String              inDocId;
  private String              inPageId;
  private String              inVendorId;
  private String              inKeyword;
  private int                 inFileSrc;
  private String              inFileUrl;
  private ImageLibrary.ImgSrc inFileSearchType;
  private String              inWebUrl;
  private String              inFileName;
  private int                 inMultipleFiles;
  private String              inFileCaption;
  private String              inSearchOrigin;

  private List<String> inFiles;


  public String getInDetailId() {
    return inDetailId;
  }

  public void setInDetailId(String inDetailId) {
    this.inDetailId = inDetailId;
  }

  public String getInFileId() {
    return inFileId;
  }

  public void setInFileId(String inFileId) {
    this.inFileId = inFileId;
  }

  public List<String> getInFiles() {
    return inFiles;
  }

  public void setInFiles(List<String> inFiles) {
    this.inFiles = inFiles;
  }

  public String getInCmpyId() {
    return inCmpyId;
  }

  public void setInCmpyId(String inCmpyId) {
    this.inCmpyId = inCmpyId;
  }

  public String getInTripId() {
    return inTripId;
  }

  public void setInTripId(String inTripId) {
    this.inTripId = inTripId;
  }

  public String getInTemplateId() {
    return inTemplateId;
  }

  public void setInTemplateId(String inTemplateId) {
    this.inTemplateId = inTemplateId;
  }

  public Long getInNoteId() {
    return inNoteId;
  }

  public void setInNoteId(Long inNoteId) {
    this.inNoteId = inNoteId;
  }

  public String getInDocId() {
    return inDocId;
  }

  public void setInDocId(String inDocId) {
    this.inDocId = inDocId;
  }

  public String getInPageId() {
    return inPageId;
  }

  public void setInPageId(String inPageId) {
    this.inPageId = inPageId;
  }

  public String getInVendorId() {
    return inVendorId;
  }

  public void setInVendorId(String inVendorId) {
    this.inVendorId = inVendorId;
  }

  public String getInKeyword() {
    return inKeyword;
  }

  public void setInKeyword(String inKeyword) {
    this.inKeyword = inKeyword;
  }

  public int getInFileSrc() {
    return inFileSrc;
  }

  public void setInFileSrc(int inFileSrc) {
    this.inFileSrc = inFileSrc;
  }

  public String getInFileUrl() {
    return inFileUrl;
  }

  public void setInFileUrl(String inFileUrl) {
    this.inFileUrl = inFileUrl;
  }

  public String getInWebUrl() {
    return inWebUrl;
  }

  public void setInWebUrl(String inWebUrl) {
    this.inWebUrl = inWebUrl;
  }

  public String getInFileName() {
    return inFileName;
  }

  public void setInFileName(String inFileName) {
    this.inFileName = inFileName;
  }

  public int getInMultipleFiles() {
    return inMultipleFiles;
  }

  public void setInMultipleFiles(int inMultipleFiles) {
    this.inMultipleFiles = inMultipleFiles;
  }

  public String getInFileCaption() {
    return inFileCaption;
  }

  public void setInFileCaption(String inFileCaption) {
    this.inFileCaption = inFileCaption;
  }

  public ImageLibrary.ImgSrc getInFileSearchType() {
    return inFileSearchType;
  }

  public void setInFileSearchType(ImageLibrary.ImgSrc inFileSearchType) {
    this.inFileSearchType = inFileSearchType;
  }

  public String getInSearchOrigin() {
    return inSearchOrigin;
  }

  public void setInSearchOrigin(String inSearchOrigin) {
    this.inSearchOrigin = inSearchOrigin;
  }
}
