package com.mapped.publisher.audit;

import com.avaje.ebean.annotation.EnumValue;
import com.umapped.persistence.enums.ReservationType;

/**
 * Logical software component which has been modified
 *
 * Created by Serguei Moutovkin on 2014-05-18.
 */
public enum AuditModuleType {
  @EnumValue("TRIP")
  TRIP,
  @EnumValue("TRIP_TRAVELLER")
  TRIP_TRAVELLER,
  @EnumValue("TRIP_AGENT")
  TRIP_AGENT,
  @EnumValue("TRIP_GROUP")
  TRIP_GROUP,
  @EnumValue("TRIP_BOOKING_FLIGHT")
  TRIP_BOOKING_FLIGHT,
  @EnumValue("TRIP_BOOKING_HOTEL")
  TRIP_BOOKING_HOTEL,
  @EnumValue("TRIP_BOOKING_CRUISE")
  TRIP_BOOKING_CRUISE,
  @EnumValue("TRIP_BOOKING_TRANSPORT")
  TRIP_BOOKING_TRANSPORT,
  @EnumValue("TRIP_BOOKING_ACTIVITY")
  TRIP_BOOKING_ACTIVITY,
  @EnumValue("TRIP_BOOKING_NOTE")
  TRIP_BOOKING_NOTE,
  @EnumValue("TRIP_DOC_ATTACHMENT")
  TRIP_DOC_ATTACHMENT,
  @EnumValue("TRIP_DOC_GUIDE")
  TRIP_DOC_GUIDE,
  @EnumValue("TRIP_DOC_CUSTOM")
  TRIP_DOC_CUSTOM,
  @EnumValue("TRIP_COLLABORATION")
  TRIP_COLLABORATION,
  @EnumValue("UNSUPPORTED")
  UNSUPPORTED;


  public static AuditModuleType fromBookingType(ReservationType type) {
    switch (type.getRootLevelType()) {
      case FLIGHT:
        return AuditModuleType.TRIP_BOOKING_FLIGHT;
      case HOTEL:
        return AuditModuleType.TRIP_BOOKING_HOTEL;
      case CRUISE:
        return AuditModuleType.TRIP_BOOKING_CRUISE;
      case TRANSPORT:
        return AuditModuleType.TRIP_BOOKING_TRANSPORT;
      case ACTIVITY:
        return AuditModuleType.TRIP_BOOKING_ACTIVITY;
      default:
        return UNSUPPORTED;
    }
  }
}
