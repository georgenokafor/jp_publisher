package com.mapped.publisher.form.template;

import play.data.validation.Constraints;

import javax.validation.constraints.NotNull;

/**
 * Created by surge on 2016-01-04.
 */
public class HotelBookingForm {
  @Constraints.Required
  @NotNull
  public String inTemplateId;
  public String inTemplateDetailId;
  @Constraints.Required
  @NotNull
  public String inHotelName;
  public Long   inHotelProviderId;
  public int    inDayOffset;
  public int    inDuration;
  public String inStartTime;
  public String inEndTime;
  public String inHotelNote;
}
