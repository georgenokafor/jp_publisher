package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Created by surge on 2015-05-12.
 */
@JsonTypeName("RentalCarReservation")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RentalCarReservation
    extends Reservation {

  /**
   * The car that is reserved.
   */
  public RentalCar reservationFor;

  /**
   * [Place] 	Where a rental car can be dropped off.
   */
  public Place dropoffLocation;
  /**
   * [DateTime] 	When a rental car can be dropped off.
   */
  public DateTime dropoffTime;

  /**
   * [Place] 	Where a taxi will pick up a passenger or a rental car can be picked up.
   */
  public Place pickupLocation;

  /**
   * 	[DateTime] 	When a taxi will pickup a passenger or a rental car can be picked up.
   */
  public DateTime pickupTime;

  /**
   * [URL] Webpage where the passenger can check in.
   */
  public String checkinUrl;

  public String startTime;

  @Override
  public DateTime getStartDateTime() {
    return pickupTime;
  }

  @Override
  public DateTime getFinishDateTime() {
    return dropoffTime;
  }

}
