package com.mapped.publisher.view;

import java.util.List;

/**
 * Created by twong on 15-08-19.
 */
public class BookingReservationView extends BaseView {
  public String src;
  public String pk;
  public String travelerName;
  public String travelDate;
  public String details;
  public List<BookingDetailView> bookings;
  public String receivedTS;
  public String recLocator;
  public String reservationPackagePK;
  public String reservationName;
}
