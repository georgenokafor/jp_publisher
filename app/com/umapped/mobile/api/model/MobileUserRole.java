package com.umapped.mobile.api.model;

/**
 * Created by wei on 2017-07-19.
 */
public enum MobileUserRole {
  Traveler,
  Publisher
}
