
package com.mapped.publisher.msg.flightstat.status;

import com.mapped.publisher.msg.flightstat.status.Airport;

import java.util.List;

public class Airports{
   	private List<Airport> airport;

 	public List<Airport> getAirport(){
		return this.airport;
	}
	public void setAirport(List<Airport> airport){
		this.airport = airport;
	}
}
