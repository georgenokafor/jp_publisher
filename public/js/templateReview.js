function loadTemplateBookings(bookingsUrl) {
  var bookingsRequest = $.getJSON(bookingsUrl, function(result) {
    var bookinEvents = [];

    $.each(result.bookings, function(key, value) {
      for (var bookingIdx = 0; bookingIdx < value.length; bookingIdx++) {
        booking = value[bookingIdx];
        var event = {
          booking: booking,
          tmpltDetailId: booking.tmpltDetailId,
          title: booking.name,
          className : booking.cssClassName + "Cal",
          editable: true,
          typeLabel: booking.typeLabel,
          origCssClass: booking.cssClassName,
          type: booking.type
        };

        event[booking.type] = true;
        if (booking.timeStart && booking.dayOffset != -1) {
          event.start = moment.utc('1979-01-01 ' + booking.timeStart, "YYYY-MM-DD hh:mm A");
          if (booking.dayOffset != 0) {
            event.start.add(booking.dayOffset, "days");
          }
        } else {
          event.start = moment.utc('1979-01-01', "YYYY-MM-DD");
          event.start.add(booking.dayOffset,"days");
          event.allDay = true;
        }

        if (booking.timeEnd) {
          event.end = moment.utc('1979-01-01 '+booking.timeEnd,"YYYY-MM-DD hh:mm A");
          event.end.add(booking.dayOffset + booking.duration, "days");
        } else {
          event.end = moment.utc('1979-01-01',"YYYY-MM-DD");
          event.end.add(booking.dayOffset + booking.duration, "days");
        }
        bookinEvents.push(event);
      }
    });

    initTemplateCalendar(bookinEvents);
  });
}

function saveTemplateBookings(updateUrl) {
  $.ajax({
    type: "POST",
    url: updateUrl,
    data: JSON.stringify(getTemplateCalendarEvents()),
    contentType: "application/json; charset=utf-8",
    dataType: "json"}).
    always(function(data, textStatus, jqXHR) {
      $('#alertMsg').text(data.msg + " - " + textStatus);
      $('#alertBox').show();
    });
}

function getDaysDifference(momentBefore, momentAfter) {
  var yyBefore = momentBefore.get('year');
  var yyAfter  = momentAfter.get('year');
  var ddBefore = momentBefore.dayOfYear();
  var ddAfter  = momentAfter.dayOfYear();

  return (yyAfter*365 + ddAfter) - (yyBefore*365 + ddBefore);
}

function getTemplateCalendarEvents() {
  var calEvents = $('#calendar').fullCalendar('clientEvents');
  var postEvents = [];

  for (var eIdx = 0; eIdx < calEvents.length; eIdx++){
    var calEvent = calEvents[eIdx];
    var event = {
      tmpltDetailId: calEvent.tmpltDetailId,
      timeStart: calEvent.start.format('HH:mm'),
      timeEnd:   calEvent.end.format('HH:mm'),
      dayOffset: getDaysDifference(moment.utc('1979-01-01','YYYY-MM-DD'), calEvent.start),
      duration:  getDaysDifference(calEvent.start, calEvent.end)
    };
    postEvents.push(event);
  }
  return postEvents;
}

function initTemplateCalendar(bookingEvents) {
  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next',
      center: '',
      right: 'month,agendaWeek,agendaDay'
    },
    defaultDate: '1979-01-01',
    editable: true,
    eventLimit: false, // allow "more" link when too many events
    firstDay: 1,
    weekNumbers: true,
    events: bookingEvents,

    eventRender: function(event, element) {
      $(element).prepend('<span class="calendarLabel"><div class="'+ event.booking.cssClassName + ' calendarLabelIcon"></div><span class="calendarLabel">' + event.booking.typeLabel + '</span></span>');
    },
    eventClick: function(calEvent, jsEvent, view) {
      getPage(calEvent.booking.detailsEditUrl);return false;
    }
  });
}
