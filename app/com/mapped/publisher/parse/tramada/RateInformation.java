//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.08.08 at 12:11:19 PM EDT 
//


package com.mapped.publisher.parse.tramada;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AUDRate" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                 &lt;attribute name="Label" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" default="AUD Rate incl GST" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="LocalRate" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                 &lt;attribute name="Label" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" default="Local Rate incl GST" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AUDLateExtra" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>float">
 *                 &lt;attribute name="Label" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" default="AUD Late or Extrac incl GST" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "audRate",
    "localRate",
    "audLateExtra"
})
@XmlRootElement(name = "RateInformation")
public class RateInformation {

    @XmlElement(name = "AUDRate")
    protected RateInformation.AUDRate audRate;
    @XmlElement(name = "LocalRate")
    protected RateInformation.LocalRate localRate;
    @XmlElement(name = "AUDLateExtra")
    protected RateInformation.AUDLateExtra audLateExtra;

    /**
     * Gets the value of the audRate property.
     * 
     * @return
     *     possible object is
     *     {@link RateInformation.AUDRate }
     *     
     */
    public RateInformation.AUDRate getAUDRate() {
        return audRate;
    }

    /**
     * Sets the value of the audRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateInformation.AUDRate }
     *     
     */
    public void setAUDRate(RateInformation.AUDRate value) {
        this.audRate = value;
    }

    /**
     * Gets the value of the localRate property.
     * 
     * @return
     *     possible object is
     *     {@link RateInformation.LocalRate }
     *     
     */
    public RateInformation.LocalRate getLocalRate() {
        return localRate;
    }

    /**
     * Sets the value of the localRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateInformation.LocalRate }
     *     
     */
    public void setLocalRate(RateInformation.LocalRate value) {
        this.localRate = value;
    }

    /**
     * Gets the value of the audLateExtra property.
     * 
     * @return
     *     possible object is
     *     {@link RateInformation.AUDLateExtra }
     *     
     */
    public RateInformation.AUDLateExtra getAUDLateExtra() {
        return audLateExtra;
    }

    /**
     * Sets the value of the audLateExtra property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateInformation.AUDLateExtra }
     *     
     */
    public void setAUDLateExtra(RateInformation.AUDLateExtra value) {
        this.audLateExtra = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>float">
     *       &lt;attribute name="Label" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" default="AUD Late or Extrac incl GST" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class AUDLateExtra {

        @XmlValue
        protected float value;
        @XmlAttribute(name = "Label")
        @XmlSchemaType(name = "anySimpleType")
        protected String label;

        /**
         * Gets the value of the value property.
         * 
         */
        public float getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(float value) {
            this.value = value;
        }

        /**
         * Gets the value of the label property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLabel() {
            if (label == null) {
                return "AUD Late or Extrac incl GST";
            } else {
                return label;
            }
        }

        /**
         * Sets the value of the label property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLabel(String value) {
            this.label = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *       &lt;attribute name="Label" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" default="AUD Rate incl GST" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class AUDRate {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "Label")
        @XmlSchemaType(name = "anySimpleType")
        protected String label;

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Gets the value of the label property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLabel() {
            if (label == null) {
                return "AUD Rate incl GST";
            } else {
                return label;
            }
        }

        /**
         * Sets the value of the label property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLabel(String value) {
            this.label = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *       &lt;attribute name="Label" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" default="Local Rate incl GST" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class LocalRate {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "Label")
        @XmlSchemaType(name = "anySimpleType")
        protected String label;

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Gets the value of the label property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLabel() {
            if (label == null) {
                return "Local Rate incl GST";
            } else {
                return label;
            }
        }

        /**
         * Sets the value of the label property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLabel(String value) {
            this.label = value;
        }

    }

}
