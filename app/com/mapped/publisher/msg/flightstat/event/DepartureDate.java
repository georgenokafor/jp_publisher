
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class DepartureDate{
   	private String dateLocal;
   	private String dateUtc;

 	public String getDateLocal(){
		return this.dateLocal;
	}
	public void setDateLocal(String dateLocal){
		this.dateLocal = dateLocal;
	}
 	public String getDateUtc(){
		return this.dateUtc;
	}
	public void setDateUtc(String dateUtc){
		this.dateUtc = dateUtc;
	}
}
