package com.mapped.publisher.parse.classicvacations;

import java.io.Serializable;

/**
 * Created by twong on 15-06-29.
 */
public class CVCredentials implements Serializable {
  public String userId;
  public String token;
  public long tokenExpiry;
  public String refreshToken;
  public long refreshTokenExpiry;
  public long origLoginTimestamp;
}
