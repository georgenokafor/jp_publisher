package com.mapped.publisher.view;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-03-26
 * Time: 11:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class BillingView
    extends BaseView {

  public String cmpyId;
  public String cmpyName;
  public String startDate;
  public String endDate;
  public String startDatePrint;
  public String endDatePrint;
  public int numOfTrips;
  public int numOfTravellers;
  public int numOfAgents;
  public int numOfTransactions;

  public int numOfTravellersExistingTrips;
  public int numOfAgentsExistingTrips;

  public Map<String, BillingDetailView> trips;
  public Map<String, ArrayList<BillingAgentsView>> addedTravellers;
  public Map<String, ArrayList<BillingAgentsView>> addedAgents;
  public Map<String, Integer> addedTravellersTransactions;


  public Map<String, String> cmpies;

}
