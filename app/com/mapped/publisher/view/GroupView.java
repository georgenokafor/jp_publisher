package com.mapped.publisher.view;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-22
 * Time: 9:39 AM
 * To change this template use File | Settings | File Templates.
 */
public class GroupView extends BaseView {
    public String groupId;
    public String name;
    public String comment;
    public List<UserView> users;
    public String groupIndex;
}
