package com.umapped.itinerary.analyze.segment.decisiontree.impl;

import com.umapped.itinerary.analyze.segment.SegmentNature;
import com.umapped.itinerary.analyze.segment.decisiontree.AbstractDecisionTree;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionNode;
import com.umapped.itinerary.analyze.segment.decisiontree.EndDecisionNode;
import com.umapped.itinerary.analyze.segment.decisiontree.SplitDecisionNode;
import com.umapped.itinerary.analyze.segment.decisiontree.evaluators.ContentDecisionEvaluator;
import com.umapped.itinerary.analyze.segment.decisiontree.evaluators.CruiseContentDecisionEvaluator;

import java.util.ArrayList;

import static com.umapped.itinerary.analyze.segment.feature.SegmentFeature.*;

/**
 * Created by wei on 2017-06-09.
 */
public class ContentDecisionTree extends AbstractDecisionTree {
  @Override protected DecisionNode initTree() {
    EndDecisionNode noRecommendationResult = new EndDecisionNode((context) -> new ArrayList<>(0));
    EndDecisionNode recommendationResult = new EndDecisionNode(new ContentDecisionEvaluator());
    EndDecisionNode cruiseStopRecommendationResult = new EndDecisionNode(new CruiseContentDecisionEvaluator());

    SplitDecisionNode isTransitSegment = new SplitDecisionNode((context) -> context.getCurrent()
                                                                                   .hasNature(SegmentNature.Transit));
    SplitDecisionNode isCruseSegment = new SplitDecisionNode((context) -> context.getCurrent().hasNature(SegmentNature.Cruise));

    SplitDecisionNode hasCruiseStops = new SplitDecisionNode((context) -> context.getFeature(HasCruiseStops));
    SplitDecisionNode longDuration = new SplitDecisionNode((context) -> ((Integer) context.getFeature(DurationHours)) >= 12);

    SplitDecisionNode hasAccommodation = new SplitDecisionNode((context) ->
                                                                        ( context.getFeature(HasAccommondations)));

    isTransitSegment.addNode(Boolean.TRUE, noRecommendationResult)
                    .addNode(Boolean.FALSE, longDuration);

    isCruseSegment.addNode(Boolean.TRUE, hasCruiseStops)
                  .addNode(Boolean.FALSE, isTransitSegment);

    hasCruiseStops.addNode(Boolean.TRUE, cruiseStopRecommendationResult)
                  .addNode(Boolean.FALSE, noRecommendationResult);

    longDuration.addNode(Boolean.FALSE, hasAccommodation)
                .addNode(Boolean.TRUE, recommendationResult);

    hasAccommodation.addNode(Boolean.FALSE, noRecommendationResult)
                         .addNode(Boolean.TRUE, recommendationResult);

    return isCruseSegment;
  }
}
