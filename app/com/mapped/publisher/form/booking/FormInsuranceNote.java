package com.mapped.publisher.form.booking;

import models.publisher.TripNote;

/**
 * Created by george on 2017-03-22.
 */
public class FormInsuranceNote extends FormBookingBase {
  public Long inNoteId;
  public String inProvider;
  public String   inPolicyNum;
  public String inPolicyType;
  public String inEmergencyNum;
  public String inStartDate;
  public String inStartTime;
  public String inEndDate;
  public String inEndTime;
  public String inImportantInfo;
  public String inPolicyDesc;
  public TripNote.NoteType inNoteType;

}
