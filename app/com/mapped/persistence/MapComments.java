package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class MapComments { 
 
	 public final static String MapCommentsTypeId = "300"; 
	 protected static String pkTypeId =  "301"; 
	 private String pk; 
	 protected static String mapsIdTypeId =  "302"; 
	 private String mapsId; 
	 protected static String createdByTypeId =  "303"; 
	 private String createdBy; 
	 protected static String msgTypeId =  "304"; 
	 private String msg; 
	 protected static String lastUpdateTimestampTypeId =  "305"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "306"; 
	 private long createTimestamp; 
	 protected static String syncTimestampTypeId =  "307"; 
	 private long syncTimestamp; 
	 protected static String typeTypeId =  "308"; 
	 private String type; 

 	 // Contructor 
	 public MapComments () {} 
	 public MapComments ( String pk,  String mapsId,  String createdBy,  String msg,  long lastUpdateTimestamp,  long createTimestamp,  long syncTimestamp,  String type ) { 
		 this.pk=pk; 
		 this.mapsId=mapsId; 
		 this.createdBy=createdBy; 
		 this.msg=msg; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.syncTimestamp=syncTimestamp; 
		 this.type=type; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getMapsId () { 
	 	 return mapsId; 
 	 } 
 
 	 public void setMapsId(String mapsId) { 
	 	 this.mapsId = mapsId; 
 	 } 
 
 	 public  String getCreatedBy () { 
	 	 return createdBy; 
 	 } 
 
 	 public void setCreatedBy(String createdBy) { 
	 	 this.createdBy = createdBy; 
 	 } 
 
 	 public  String getMsg () { 
	 	 return msg; 
 	 } 
 
 	 public void setMsg(String msg) { 
	 	 this.msg = msg; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  String getType () { 
	 	 return type; 
 	 } 
 
 	 public void setType(String type) { 
	 	 this.type = type; 
 	 } 
 
 	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: MapComments"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 MapsId: ").append(this.mapsId );  
		 sb.append("	 CreatedBy: ").append(this.createdBy );  
		 sb.append("	 Msg: ").append(this.msg );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 Type: ").append(this.type );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", MapCommentsTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (mapsId != null) {
			 memberNode.put(mapsIdTypeId, mapsId);
		 }
		 if (createdBy != null) {
			 memberNode.put(createdByTypeId, createdBy);
		 }
		 if (msg != null) {
			 memberNode.put(msgTypeId, msg);
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (type != null) {
			 memberNode.put(typeTypeId, type);
		 }
		 return classNode;
	 } 
	 public static MapComments parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 MapComments  c= new MapComments(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.mapsId = fields.findPath(mapsIdTypeId).textValue();
			 c.createdBy = fields.findPath(createdByTypeId).textValue();
			 c.msg = fields.findPath(msgTypeId).textValue();
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 c.type = fields.findPath(typeTypeId).textValue();
			 return c;
		} else
			 return null;
	 } 
} 
 
