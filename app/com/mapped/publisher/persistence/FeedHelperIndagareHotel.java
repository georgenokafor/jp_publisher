package com.mapped.publisher.persistence;

import au.com.bytecode.opencsv.CSVReader;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.publisher.parse.traxo.Hotel;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import com.mapped.publisher.utils.Utils;
import com.umapped.api.schema.types.Address;
import com.umapped.api.schema.types.Feature;
import com.umapped.api.schema.types.PhoneNumber;
import com.umapped.api.schema.types.Privacy;
import models.publisher.FeedSrc;
import models.publisher.PoiSrcFeed;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.language.Metaphone;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import javax.xml.bind.DatatypeConverter;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by twong on 2017-03-15.
 */
public class FeedHelperIndagareHotel extends FeedHelperPOI {
  public final static String INDAGARE_HOTEL_FEED_NAME = "Indagare Hotel";
  public final static String INDAGARE_S3_PREFIX = "feeds/Indagare/";
  private final static int METAPHONE_LEN = 4;

  private static Pattern feedFile = Pattern.compile("Indagare_(2[0-9][0-9][0-9])([0-1][0-9])([0-3][0-9])_hotel.*.csv",
                                                    Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

  @Override public List<FeedResource> getResources() {
    List<FeedResource> result = new ArrayList<>();
    List<S3ObjectSummary> objs = S3Util.getObjectList(S3Util.getPDFBucketName(), INDAGARE_S3_PREFIX);
    Map<Date, S3ObjectSummary> amenities = new HashMap<>();
    Map<Date, S3ObjectSummary> hotels = new HashMap<>();

    for(S3ObjectSummary o : objs) {
      Matcher m = feedFile.matcher(o.getKey());
      if(m.find()) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)) - 1, Integer.parseInt(m.group(3)), 0, 0, 0);
        Date d = cal.getTime();
        hotels.put(d, o);
      }
    }

    for(Date d: hotels.keySet()) {
      S3ObjectSummary hotl = hotels.get(d);
      if(hotl != null) {
        FeedResource fr = new FeedResource();
        fr.setDate(d);
        fr.setName(hotl.getKey());
        fr.setOrigin(FeedResource.Origin.AWS_S3);
        fr.setSize(hotl.getSize());
        result.add(fr);
      }
    }
    return result;
  }

  public static class IndagareHotelData {
    public String propertyName;
    public String city;
    public String state;
    public String country;
    public String chain;
    public String sabreCode;
    public String amenity;
    public String street;
    public String zip;
    public float lng = 0;
    public float lat = 0;
  }


  private enum HotelsColumns {
    sabre_code,
    lodg_name,
    lodg_city,
    lodg_country,
    lodg_hotel_code,
    program_amenity,
    COLUMN_COUNT;
  }

  public String getFeedRef(FeedHelperIndagareHotel.IndagareHotelData hd) {
    return DigestUtils.md5Hex(simplifyName(hd.propertyName));
  }

  @Override public Map<PoiFeedUtil.FeedResult, Integer> load(String filePath) {
    Map<PoiFeedUtil.FeedResult, Integer> results = new HashMap<>();
    int errors  = 0;
    int success = 0;
    try {
      String hotelsFilepath = "";
      Matcher m = feedFile.matcher(filePath);
      if(m.find()) {
        hotelsFilepath = INDAGARE_S3_PREFIX +  "Indagare_"+m.group(1) + m.group(2) + m.group(3) + "_hotel.csv";
      }
      else {
        return null;
      }

      S3Object amenities = S3Util.getS3File(S3Util.getPDFBucketName(), filePath);
      S3Object hotels    = S3Util.getS3File(S3Util.getPDFBucketName(), hotelsFilepath);
      if(hotels == null || amenities == null) {
        return null;
      }

      Map<String, FeedHelperIndagareHotel.IndagareHotelData> data = new HashMap<>();
      Map<String, List<FeedHelperIndagareHotel.IndagareHotelData>> byCity = new HashMap<>();
      Reader hotelsReader = new InputStreamReader(hotels.getObjectContent());
      CSVReader hotelsCSV = new CSVReader(hotelsReader);
      Map<String, FeedHelperIndagareHotel.IndagareHotelData> hotelsById = new HashMap<>();

      String[] line;
      line = hotelsCSV.readNext(); //Skipping labels line
      Metaphone metaphone = new Metaphone();
      metaphone.setMaxCodeLen(METAPHONE_LEN);

      while ((line = hotelsCSV.readNext()) != null) {
        if(line.length < FeedHelperIndagareHotel.HotelsColumns.COLUMN_COUNT.ordinal()) {
          Log.err("TravelLeaders hotels CSV line has fewer columns than expected");
          continue;
        }
        FeedHelperIndagareHotel.IndagareHotelData hd = hotelsById.get(line[HotelsColumns.sabre_code.ordinal()]);
        if (hd == null) {
          hd = new FeedHelperIndagareHotel.IndagareHotelData();
          hd.propertyName =  Utils.trim(line[HotelsColumns.lodg_name.ordinal()]);

          hd.city =  Utils.trim(line[HotelsColumns.lodg_city.ordinal()]);
          hd.country =  Utils.trim(line[HotelsColumns.lodg_country.ordinal()]);
          hd.chain = Utils.trim(line[HotelsColumns.lodg_hotel_code.ordinal()]);
          hd.sabreCode = Utils.trim(line[HotelsColumns.sabre_code.ordinal()]);
        }

       if (hd.amenity != null) {
          hd.amenity += "\n";
         hd.amenity +=  Utils.trim(line[HotelsColumns.program_amenity.ordinal()]);
       } else {
         hd.amenity = Utils.trim(line[HotelsColumns.program_amenity.ordinal()]);
       }

        hd.propertyName = hd.propertyName.replaceAll(" +", " ");

        if(data.get(hd.propertyName) != null) {
          Log.err("{.}|{.} WTF these Indagare are doing???? === " + hd.propertyName);
        }
        data.put(simplifyName(hd.propertyName), hd);
        hotelsById.put(hd.sabreCode, hd);
      }


      ObjectMapper jsonMapper = new ObjectMapper();
      for(String name: data.keySet()) {
        FeedHelperIndagareHotel.IndagareHotelData hd = data.get(name);
        if(hd.amenity == null) {
          continue;
        }

        String json = jsonMapper.writeValueAsString(hd);
        PoiImportRS pirs = new PoiImportRS();
        pirs.state = PoiImportRS.ImportState.UNPROCESSED;
        pirs.srcId = FeedSourcesInfo.byName(INDAGARE_HOTEL_FEED_NAME);
        pirs.poiId = null;
        pirs.typeId = PoiTypeInfo.Instance().byName("Accommodations").getId();
        pirs.timestamp = System.currentTimeMillis();
        pirs.srcReference = getFeedRef(hd);
        pirs.srcFilename = hotelsFilepath;
        pirs.name = hd.propertyName;
        pirs.countryName = hd.country;

        CountriesInfo.Country c = CountriesInfo.Instance().searchByName(hd.country);
        if (c == null) {
          Log.err("Failed to find matching country: >>" + hd.country + "<< !!!");
          pirs.countryCode = "UNK";
          errors++;
        }
        else {
          pirs.countryCode = c.getAlpha3();
        }

        pirs.region = hd.state;
        pirs.city = hd.city;
        pirs.raw = json;
        pirs.hash = DigestUtils.md5Hex(pirs.raw);
        PoiImportMgr.save(pirs);
        success++;
      }
    }
    catch (Exception e) {
      Log.err("Failure during TravelLeaders hotels feed processing");
      e.printStackTrace();
    }
    results.put(PoiFeedUtil.FeedResult.SUCCESS, success);
    results.put(PoiFeedUtil.FeedResult.ERROR, errors);
    return results;
  }

  @Override public PoiImportRS.ImportState associate(PoiImportRS importRS) {
    List<Pair<PoiRS, Integer>> scores = getRankedPois(importRS, false);

    if (scores.size() == 0 || scores.get(0).getRight() <= 0) {
      Log.debug("Found no matches for:" + importRS.name + " from: " + importRS.countryCode);
      importRS.state = PoiImportRS.ImportState.MISMATCH;
      importRS.poiId = null;
    }
    else {
      importRS.state = (scores.get(0).getRight() >= 40) ?
                       PoiImportRS.ImportState.MATCH :
                       PoiImportRS.ImportState.UNSURE;
      importRS.poiId = scores.get(0).getLeft().getId();
    }

    PoiImportMgr.update(importRS);
    return importRS.state;
  }

  @Override public PoiSrcFeed.FeedSyncResult synchronize(PoiSrcFeed psf) {
    PoiSrcFeed.FeedSyncResult result = PoiSrcFeed.FeedSyncResult.FAIL;
    try {
      ObjectMapper jsonMapper = new ObjectMapper();
      com.fasterxml.jackson.databind.ObjectReader r = jsonMapper.reader(FeedHelperIndagareHotel.IndagareHotelData.class);
      FeedHelperIndagareHotel.IndagareHotelData data = r.readValue(psf.getRaw());
      FeedSrc feedSrc = FeedSrc.find.byId(psf.getFeedId().srcId);
      List<PoiRS> poiRecs = PoiMgr.findAllById(psf.getFeedId().poiId);

      PoiRS rsToUpdate = null;
      for (PoiRS p : poiRecs) {
        if (p.getCmpyId() == feedSrc.getCmpyId() &&
            p.consortiumId == feedSrc.getConsortiumId() &&
            p.getSrcId() == feedSrc.getSrcId()) {
          rsToUpdate = p;
        }
      }

      if (rsToUpdate == null) { //Creating a new POI record
        rsToUpdate = PoiRS.buildRecord(feedSrc.getConsortiumId(),
                                       feedSrc.getCmpyId(),
                                       feedSrc.getTypeId(),
                                       feedSrc.getSrcId(),
                                       "system");
        updatePoi(rsToUpdate, psf, data, feedSrc);
        rsToUpdate.setId(psf.getFeedId().poiId);
        PoiMgr.save(rsToUpdate);
        result = PoiSrcFeed.FeedSyncResult.NEW;
      }
      else { //updating existing one
        updatePoi(rsToUpdate, psf, data, feedSrc);
        PoiMgr.update(rsToUpdate);
        result = PoiSrcFeed.FeedSyncResult.UPDATED;
      }
      psf.setSyncTs(System.currentTimeMillis());
      psf.setSynced(true);
      psf.update();

    }
    catch (Exception e) {
      Log.err("Unexpected Error - not handling gracefully:", e.getMessage());
      e.printStackTrace();
    }
    return result;
  }

  private PoiRS updatePoi(PoiRS poi, PoiSrcFeed feedRec, FeedHelperIndagareHotel.IndagareHotelData hotelInfo, FeedSrc src) {
    poi.setSrcReference(feedRec.getSrcReference());
    poi.setName(hotelInfo.propertyName);
    Address address = new Address();
    address.setLocality(hotelInfo.city);
    address.setRegion(hotelInfo.state);
    address.setCountryCode(CountriesInfo.Instance().searchByName(hotelInfo.country).getAlpha3());
    poi.setMainAddress(address);



    poi.clearFeatures(); //Clearing all previously listed features for this feed
    if(hotelInfo.amenity != null) {
      Feature f = new Feature();
      f.setName("Special Amenities");
      f.setDesc(hotelInfo.amenity);
      f.addTag(src.getName());
      poi.addFeature(f);
    }
    return poi;
  }

  @Override public List<Pair<PoiRS, Integer>> getRankedPois(PoiImportRS importRS, boolean ignoreCountry) {
    String cleanName = importRS.name;
    if (cleanName != null && cleanName.trim().contains(" ")) {
      cleanName = '%' + cleanName + '%';
    }
    List<PoiRS> pois;
    if (ignoreCountry) {
      pois = PoiMgr.findForMerge(cleanName, null, importRS.typeId);
      if (importRS.name.contains("Hotel")) {
        String cleanName1 = importRS.name.trim().replace("Hotel","").trim();
        if (cleanName1.contains(" ")) {
          cleanName1 = "%" + cleanName1 + "%";
        }
        List<PoiRS> pois1 = PoiMgr.findForMerge(cleanName1, null, importRS.typeId);
        if (pois1 != null && pois1.size() > 0) {
          if (pois == null ||  pois.size() == 0) {
            pois = pois1;
          } else {
            pois.addAll(pois1);
          }

        }
      }
    }
    else {
      pois = PoiMgr.findForMerge(cleanName, importRS.countryCode, importRS.typeId);
      if (importRS.name.contains("Hotel")) {
        String cleanName1 = importRS.name.trim().replace("Hotel","").trim();
        if (cleanName1.contains(" ")) {
          cleanName1 = "%" + cleanName1 + "%";
        }
        List<PoiRS> pois1 = PoiMgr.findForMerge(cleanName1, importRS.countryCode, importRS.typeId);
        if (pois1 != null && pois1.size() > 0) {
          if (pois == null ||  pois.size() == 0) {
            pois = pois1;
          } else {
            pois.addAll(pois1);
          }

        }
      }
    }

    List<Pair<PoiRS, Integer>> scores = new ArrayList<>();
    for (PoiRS prs : pois) {
      if (prs.getCmpyId() == 0) {
        int currScore = rankPoi(cleanName, importRS, prs);
        scores.add(new ImmutablePair<>(prs, currScore));
      }
    }

    Collections.sort(scores, (o1, o2) -> o2.getRight() - o1.getRight());

    return scores;
  }

  @Override public PoiSrcFeed.FeedSyncResult clearAmenities(PoiSrcFeed psf) {
    PoiSrcFeed.FeedSyncResult result = PoiSrcFeed.FeedSyncResult.FAIL;

    ObjectMapper jsonMapper = new ObjectMapper();
    com.fasterxml.jackson.databind.ObjectReader r = jsonMapper.reader(FeedHelperIndagareHotel.IndagareHotelData.class);
    try {
      FeedHelperIndagareHotel.IndagareHotelData data = r.readValue(psf.getRaw());
      if (data.amenity != null) {
        data.amenity = null;
        psf.setSynced(false);
        psf.setRaw(jsonMapper.writeValueAsString(data));
        psf.setHash(DatatypeConverter.parseHexBinary(DigestUtils.md5Hex(psf.getRaw())));
        psf.update();
        result = PoiSrcFeed.FeedSyncResult.UPDATED;
      } else {
        result = PoiSrcFeed.FeedSyncResult.UNCHANGED;
      }
    } catch(Exception e) {
      Log.err("Failure to remove amenities for TravelLeaders feed", e);
      e.printStackTrace();
    }
    return result;
  }
}