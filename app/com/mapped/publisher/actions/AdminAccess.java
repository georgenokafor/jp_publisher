package com.mapped.publisher.actions;

import com.mapped.publisher.common.Credentials;
import com.mapped.publisher.common.SessionMgr;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-03
 * Time: 3:07 PM
 * To change this template use File | Settings | File Templates.
 */

public class AdminAccess
    extends Action.Simple {

  public CompletionStage<Result> call(Http.Context ctx) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx);
    if(sessionMgr == null) {
      sessionMgr = new SessionMgr(ctx.session());
    }

    if (sessionMgr.isSessionValid()) {
      ctx.args.put(SessionMgr.CONTEXT_KEY, sessionMgr);
    }

    Credentials cred = sessionMgr.getCredentials();
    if (cred != null && cred.isUmappedAdmin()) {
      return delegate.call(ctx);
    }
    else {
      return CompletableFuture.completedFuture(ok("No Access"));
    }
  }
}
