package com.mapped.publisher.parse.tramada;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Created by george on 2017-11-22.
 */
public class StringTrimAdapter extends XmlAdapter<String, String> {
  @Override
  public String unmarshal(String v) throws Exception {
    if (v == null || v.isEmpty())
      return null;
    return v.trim();
  }
  @Override
  public String marshal(String v) throws Exception {
    if (v == null || v.isEmpty())
      return null;
    return v.trim();
  }
}
