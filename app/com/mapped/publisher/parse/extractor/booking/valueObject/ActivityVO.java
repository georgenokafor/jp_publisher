package com.mapped.publisher.parse.extractor.booking.valueObject;

import com.umapped.persistence.enums.ReservationType;

import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-10-25
 * Time: 9:37 AM
 * To change this template use File | Settings | File Templates.
 */
public class ActivityVO
    extends ReservationVO {
  private Timestamp startDate;
  private Timestamp endDate;
  private String confirmation;
  private String contact;
  private ReservationType bookingType = ReservationType.ACTIVITY;
  private String tag;
  private String startLocation;
  private String endLocation;

  public boolean isValid() {
    if (name != null && name.trim().length() > 0 && startDate != null) {
      return true;
    }
    return false;
  }

  public String getRecordLocator() {
    return recordLocator;
  }

  public void setRecordLocator(String recordLocator) {
    this.recordLocator = recordLocator;
  }

  public ReservationType getBookingType() {
    return bookingType;
  }

  public void setBookingType(ReservationType bookingType) {
    this.bookingType = bookingType;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public Timestamp getStartDate() {
    return startDate;
  }

  public void setStartDate(Timestamp startDate) {
    this.startDate = startDate;
  }

  public Timestamp getEndDate() {
    return endDate;
  }

  public void setEndDate(Timestamp endDate) {
    this.endDate = endDate;
  }

  public String getConfirmation() {
    return confirmation;
  }

  public void setConfirmation(String confirmation) {
    this.confirmation = confirmation;
  }

  public String toString() {
    String nl = System.getProperty("line.separator");
    StringBuilder strBuilder = new StringBuilder(">>> Activity (Value Object)" + nl);
    strBuilder.append("Type          :" + bookingType.name() + nl);
    strBuilder.append("Name          :" + name + nl);
    strBuilder.append("Record Locator:" + recordLocator + nl);
    strBuilder.append("Start Date    :" + startDate + nl);
    strBuilder.append("End   Date    :" + endDate + nl);
    strBuilder.append("Contact       :" + contact + nl);
    strBuilder.append("Confirmation  :" + confirmation + nl);
    strBuilder.append("Note          :" + nl + getFullNotes() + nl);
    return strBuilder.toString();
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public void setStartLocation(String startLocation) {
    this.startLocation = startLocation;
  }

  public void setEndLocation(String endLocation) {
    this.endLocation = endLocation;
  }

  public String getEndLocation() {
    return endLocation;
  }

  public String getStartLocation() {
    return startLocation;
  }
}
