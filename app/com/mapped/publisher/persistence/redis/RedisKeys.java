package com.mapped.publisher.persistence.redis;

import com.sun.tools.jxc.apt.Const;

import java.util.concurrent.TimeUnit;

/**
 * Redis keys
 * <p>
 * Keys naming convention:
 * - H_* are for Hash keys/prefixes
 * - HF_* are for Hash secondary/keys (No Expiry Value for subkeys)
 * - S_* are for Set keys/prefixes
 * - L_* are for List key/prefixes
 * - K_* are for Simple Keys/Prefixes (i.e. for single key/value data store)
 * <p>
 * Created by surge on 2016-02-01.
 */
public enum RedisKeys {
  /**
   * Main key for all caches related to the trip
   */
  H_TRIP(TimeUnit.HOURS.toSeconds(4), Constants.BINARY, Constants.KEY),

  /**
   * Storage for TripBookingDetailView build from trip notes
   */
  HF_TRIP_NOTES_DETAIL_VIEW(0, Constants.BINARY, Constants.FIELD),

  /**
   * Sub key for TripBookingDetailView objects
   */
  HF_TRIP_BOOKING_DETAIL_VIEW(0, Constants.BINARY, Constants.FIELD),

  /**
   * Sub key for Trip Web Itinerary API ReservationPackage
   */
  HF_TRIP_RESERVATION_PACKAGE(0, Constants.BINARY, Constants.FIELD),


  /**
   * Sub key for Trip Web Itinerary API Map Points cache
   */
  HF_TRIP_MAP(0, Constants.BINARY, Constants.FIELD),


  /**
   * Sub key for Trip Web Itinerary API Map Points cache
   */
  HF_TRIP_MAP_MOBILE(0, Constants.BINARY, Constants.FIELD),

  /**
   * Sub key for Trip Web Itinerary API Calendar Events List
   */
  HF_TRIP_CALENDAR_EVENTS(0, Constants.BINARY, Constants.FIELD),

  /**
   * Background job objects expire after 1 hour of no activity
   */
  K_BACKGROUND_JOB_CACHE(TimeUnit.HOURS.toSeconds(1),  Constants.BINARY, Constants.KEY),

  /**
   * Key used to test List implementation
   */
  L_LIST_TEST(TimeUnit.SECONDS.toSeconds(5), Constants.BINARY, Constants.KEY),

  /**
   * Key is used to store urls to redirect to after image upload is confirmed
   */
  K_IMG_UPLOAD_REDIRECT(TimeUnit.MINUTES.toSeconds(10), Constants.TEXT, Constants.KEY),
  K_IMG_UPLOAD_MODEL(TimeUnit.MINUTES.toSeconds(10), Constants.BINARY, Constants.KEY),

  /**
   * Key is used to store urls to redirect to after file upload is confirmed
   */
  K_FILE_UPLOAD_REDIRECT(TimeUnit.MINUTES.toSeconds(10), Constants.TEXT, Constants.KEY),
  K_FILE_UPLOAD_MODEL(TimeUnit.MINUTES.toSeconds(10), Constants.BINARY, Constants.KEY),

  /**
   * Hash key to all trips (trip ids are the fields) that need to be published
   */
  H_TRIPS_TO_PUBLISH(0, Constants.BINARY, Constants.KEY),

  /**
   * Hash key to Trip's Poi Location
   */
  K_POI_LOCATION(TimeUnit.HOURS.toSeconds(4), Constants.BINARY, Constants.KEY),

  /**
   * Sub key to city Weather data
   */
  K_LOCATION_WEATHER(TimeUnit.HOURS.toSeconds(4), Constants.BINARY, Constants.KEY);


  long    expire;
  boolean field;
  boolean binary;

  private static class Constants {
    public static final boolean FIELD  = true;
    public static final boolean KEY    = false;
    public static final boolean BINARY = true;
    public static final boolean TEXT   = false;
  }

  RedisKeys(long expire, boolean isBinary, boolean isField) {
    this.expire = expire;
    this.field = isField;
    this.binary = isBinary;
  }

  public boolean isBinary() {
    return binary;
  }

  public boolean isField() {
    return field;
  }

  public int getExpire() {
    return (int) expire; //Should never overflow since we are talking seconds not milliseconds
  }

  public String getKey() {
    return this.name();
  }

  public String getKey(final String specifier) {
    final StringBuilder keyBuilder = new StringBuilder();
    keyBuilder.append(this.name());
    if (specifier != null) {
      keyBuilder.append(':').append(specifier);
    }
    return keyBuilder.toString();
  }
}
