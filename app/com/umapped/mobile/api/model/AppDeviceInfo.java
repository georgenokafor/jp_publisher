package com.umapped.mobile.api.model;

import java.io.Serializable;

/**
 * Created by wei on 2017-08-16.
 */
public class AppDeviceInfo implements Serializable {
  private String deviceUid;
  private String appBundleId;
  private String appVersion;
  private String deviceOSVersion;
  private String deviceOSName;
  private String deviceBrand;
  private String deviceModel;
  private String deviceVersion;

  public String getDeviceUid() {
    return deviceUid;
  }

  public void setDeviceUid(String deviceUid) {
    this.deviceUid = deviceUid;
  }

  public String getAppBundleId() {
    return appBundleId;
  }

  public void setAppBundleId(String appBundleId) {
    this.appBundleId = appBundleId;
  }

  public String getAppVersion() {
    return appVersion;
  }

  public void setAppVersion(String appVersion) {
    this.appVersion = appVersion;
  }

  public String getDeviceOSVersion() {
    return deviceOSVersion;
  }

  public void setDeviceOSVersion(String deviceOSVersion) {
    this.deviceOSVersion = deviceOSVersion;
  }

  public String getDeviceBrand() {
    return deviceBrand;
  }

  public void setDeviceBrand(String deviceBrand) {
    this.deviceBrand = deviceBrand;
  }

  public String getDeviceModel() {
    return deviceModel;
  }

  public void setDeviceModel(String deviceModel) {
    this.deviceModel = deviceModel;
  }

  public String getDeviceVersion() {
    return deviceVersion;
  }

  public void setDeviceVersion(String deviceVersion) {
    this.deviceVersion = deviceVersion;
  }

  public String getDeviceOSName() {
    return deviceOSName;
  }

  public void setDeviceOSName(String deviceOSName) {
    this.deviceOSName = deviceOSName;
  }
}
