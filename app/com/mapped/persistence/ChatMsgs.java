package com.mapped.persistence;
 
import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class ChatMsgs { 
 
	 public final static String ChatMsgsTypeId = "200"; 
	 protected static String pkTypeId =  "201"; 
	 private String pk; 
	 protected static String chatIdTypeId =  "202"; 
	 private String chatId; 
	 protected static String usersIdTypeId =  "203"; 
	 private String usersId; 
	 protected static String msgTypeId =  "204"; 
	 private String msg; 
	 protected static String lastUpdateTimestampTypeId =  "205"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "206"; 
	 private long createTimestamp; 
	 protected static String syncTimestampTypeId =  "207"; 
	 private long syncTimestamp; 

 	 // Contructor 
	 public ChatMsgs () {} 
	 public ChatMsgs ( String pk,  String chatId,  String usersId,  String msg,  long lastUpdateTimestamp,  long createTimestamp,  long syncTimestamp ) { 
		 this.pk=pk; 
		 this.chatId=chatId; 
		 this.usersId=usersId; 
		 this.msg=msg; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.syncTimestamp=syncTimestamp; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getChatId () { 
	 	 return chatId; 
 	 } 
 
 	 public void setChatId(String chatId) { 
	 	 this.chatId = chatId; 
 	 } 
 
 	 public  String getUsersId () { 
	 	 return usersId; 
 	 } 
 
 	 public void setUsersId(String usersId) { 
	 	 this.usersId = usersId; 
 	 } 
 
 	 public  String getMsg () { 
	 	 return msg; 
 	 } 
 
 	 public void setMsg(String msg) { 
	 	 this.msg = msg; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: ChatMsgs"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 ChatId: ").append(this.chatId );  
		 sb.append("	 UsersId: ").append(this.usersId );  
		 sb.append("	 Msg: ").append(this.msg );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", ChatMsgsTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (chatId != null) {
			 memberNode.put(chatIdTypeId, chatId);
		 }
		 if (usersId != null) {
			 memberNode.put(usersIdTypeId, usersId);
		 }
		 if (msg != null) {
			 memberNode.put(msgTypeId, msg);
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 return classNode;
	 } 
	 public static ChatMsgs parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 ChatMsgs  c= new ChatMsgs(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.chatId = fields.findPath(chatIdTypeId).textValue();
			 c.usersId = fields.findPath(usersIdTypeId).textValue();
			 c.msg = fields.findPath(msgTypeId).textValue();
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 return c;
		} else
			 return null;
	 } 
} 
 
