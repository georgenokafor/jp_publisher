package com.mapped.publisher.view;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-20
 * Time: 11:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class CmpyStatsView {
  public String cmpyid;
  public String name;
  public String targetAgreement;
  public String agreedAgreement;
  public String declinedAgreement;
  public String userid;
  public String timestamp;

  public int numTripsPublished;
  public int numTripsPublishedActivity;

  public int numTripsCreated;
  public int numTripsPending;
  public int numTripsDeleted;

  public int numToursCreated;
  public int numToursPending;
  public int numToursDeleted;

  public int numPoiCreated;
  public int numPoiModified;
  public int numPoiDeleted;

  public int numGuidesCreated;
  public int numGuidesDeleted;
  public int numGuidesPagesCreated;
  public int numGuidesPagesModified;
  public int numGuidesPagesDeleted;
  public String cmpyType;
}
