package com.mapped.publisher.persistence.communicator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ServerValue;
import com.mapped.publisher.utils.Log;

/**
 * Created by surge on 2015-07-20.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FirebaseObject {

  /**
   * Save record with an external identifier
   *
   * @param refParent - Parent where a new structure will be created
   * @param recId - Unique identifier as used by external umapped platform
   * @return
   */
  public Firebase overwrite(Firebase refParent, final String recId) {
    final Firebase conf = refParent.child(recId);

    try {
      conf.setValue(this, new Firebase.CompletionListener() {
        @Override public void onComplete(FirebaseError firebaseError, Firebase firebase) {
          if (firebaseError == null) {
            Log.debug("Firebase record created: " + conf.getPath().toString() + " id: " + recId);
          }
          else {
            Log.err("Firebase failed creating record: " + firebaseError.getMessage());
            Log.err("Failed Path: " + conf.getPath().toString());
          }
        }
      });
    } catch (Exception e) {
      Log.err("Firebase: Failure to overwrite object at: " + conf.toString(), e);
    }

    return conf;

  }

  /**
   * Save record at the specified location, assuming reference contains full path
   *
   * @param refObjectPath - Parent where a new structure will be created
   * @return
   */
  public Firebase overwrite(Firebase refObjectPath) {
    final Firebase conf = refObjectPath;

    try {
      conf.setValue(this, new Firebase.CompletionListener() {
        @Override public void onComplete(FirebaseError firebaseError, Firebase firebase) {
          if (firebaseError == null) {
            Log.debug("Firebase record created: " + conf.getPath().toString());
          }
          else {
            Log.err("Firebase failed creating record: " + firebaseError.getMessage());
            Log.err("Failed Path: " + conf.getPath().toString());
          }
        }
      });
    } catch(Exception e) {
      Log.err("Firebase: Failure to overwrite object at: " + conf.toString(), e);
    }
    return conf;
  }



  /**
   * Push record with a new auto-generated ID
   *
   * @param refParent - Parent where a new structure will be created
   * @return
   */
  public Firebase push(Firebase refParent) {
    final Firebase conf = refParent.push();

    try {
      conf.setValue(this, ServerValue.TIMESTAMP, new Firebase.CompletionListener() {
        @Override public void onComplete(FirebaseError firebaseError, Firebase firebase) {
          if (firebaseError == null) {
            Log.debug("Firebase record pushed: " + conf.getPath().toString() + " id: " + conf.getKey());
          }
          else {
            Log.err("Failed pushing record: " + firebaseError.getMessage());
            Log.err("Failed Path: " + conf.getPath().toString());
          }
        }
      });
    } catch (Exception e) {
      Log.err("Firebase: Failure to push object", e);
    }
    return conf;
  }
}
