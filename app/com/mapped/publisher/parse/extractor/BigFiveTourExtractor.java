package com.mapped.publisher.parse.extractor;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.parse.valueObject.DateDetails;
import com.mapped.publisher.parse.valueObject.TravelDetails;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;

import java.io.InputStream;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BigFiveTourExtractor extends DataExtractor {
    /**
     * Start and end text are used to strip the day by day itinerary
     */

    private String startKey;
    private String endKey1;
    private String endKey2;

    /**
     * Date format
     */
    static final String[] DATE_FORMAT_MMM_DD_YYYY = {"MMM dd yyyy"};

    /**
     * Regex to remove the unknown characters
     */
    static final String INVALID_CHARS_REGEX = "[]";
    static final String WORD_MERGE_FIELD = "FILLIN.*.MERGEFORMAT";

    /**
     * minimum length for the content
     */
    static final Integer CONTENT_MIN_LENGTH = 4;

    /**
     * Regex to strip by day
     */
    static final String REGEX_MONTH = "((?:JAN(?:UARY)?|FEB(?:RUARY)?|MAR(?:CH)?|APR(?:IL)?|MAY|JUN(?:E)?|JUL(?:Y)?|AUG(?:UST)?|SEP(?:TEMBER)?|SEPT|OCT(?:OBER)?|NOV(?:EMBER)?|DEC(?:EMBER)?)|(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|may|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))"; // Month
    static final String REGEX_2 = ".*?"; // Non-greedy match on filler
    static final String REGEX_DAY = "((?:(?:[0-2]?\\d{1})|(?:[3][01]{1})))(?![\\d])"; // Day
    static final String REGEX_ANY_SINGLE_CHAR = "(.)"; // Any Single Character 1
    static final String REGEX_WHITE_SPACE = "(\\s+)";
    static final String REGEX_AMPRESEND = "(&)";
    static final String REGEX_COMMA ="(,)";	// comma character
    static final String REGEX_DAY_OF_WEEK ="((?:SUN|MON|TUE|WED|THU|FRI|SAT))"; // Day Of Week 1
    static final String REGEX_COLON ="(:)";	// colon character
    static final String REGEX_YEAR = "\\d{4}";

    static final Pattern PATTERN = Pattern.compile(REGEX_MONTH + REGEX_2 + REGEX_DAY + REGEX_ANY_SINGLE_CHAR);
    static final Pattern PATTERN2 = Pattern.compile(REGEX_AMPRESEND + REGEX_WHITE_SPACE + REGEX_DAY + REGEX_ANY_SINGLE_CHAR);
    static final Pattern DAY_OF_WEEK_PATTERN = Pattern.compile(REGEX_COMMA + REGEX_WHITE_SPACE + REGEX_DAY_OF_WEEK + REGEX_COLON);
    static final Pattern YEAR_PATTERN = Pattern.compile(REGEX_YEAR);


    @Override
    protected void initImpl(Map<String, String> params) {
        if (params != null) {
            startKey = params.get(APPConstants.WORD_START_TOKEN);
            endKey1 = params.get(APPConstants.WORD_END_TOKEN_1);
            endKey2 = params.get(APPConstants.WORD_END_TOKEN_2);
        }




    }

    @Override
    protected TravelDetails extractDataImpl(InputStream input) throws Exception {
        return parse(input);
    }

    /**
     * Parser to extract the day by day data from Word
     *
     * @param fis
     * @return extracted Data Map (LinkedHashMap)
     */
    protected TravelDetails parse(InputStream fis) {


        try {

            HWPFDocument document = new HWPFDocument(fis);
            WordExtractor extractor = new WordExtractor(document);

            // extract all paragraphs
            String allParagraphs = StringUtils.join(extractor.getParagraphText());

            //get the intro
            if (allParagraphs != null) {
                return parseFile(allParagraphs);
             }
        } catch (Exception exep) {
            exep.printStackTrace();
        }


        return null;
    }

    protected TravelDetails parseFile(String allParagraphs) {
        TravelDetails travelDetails = new TravelDetails();
        travelDetails.setDateWiseDetails(new TreeMap<Date, DateDetails>());
        try {
            //get the intro
            if (allParagraphs != null) {

                allParagraphs = removeInvalidChars(allParagraphs);
                int startPos = 0;
                int endPos = allParagraphs.length() - 1;
                int startLength = 0;
                if (startKey != null && !startKey.isEmpty() && allParagraphs.indexOf(startKey) >= 0) {
                    startPos = allParagraphs.indexOf(startKey);
                    startLength = startKey.length();
                }
                if (startPos > 0) {
                    travelDetails.setIntroInfo(allParagraphs.substring(0, startPos));
                }

               if (endKey2 != null && !endKey2.isEmpty() && allParagraphs.indexOf(endKey2) >= 0) {
                   endPos = allParagraphs.indexOf(endKey2);
               } else if (endKey1 != null && !endKey1.isEmpty() && allParagraphs.indexOf(endKey1) >= 0) {
                  endPos =allParagraphs.indexOf(endKey1);
               }

                if (endPos > 0 && endPos < allParagraphs.length()) {
                    travelDetails.setAdditionalInfo(allParagraphs.substring(endPos));
                }


                // strip  day by day itinerary data and remove the invalid chars
                String dayByDayItinerary = removeInvalidChars(StringUtils.substring(allParagraphs, startPos + startLength, endPos));

                ArrayList<String> dateTokens = new ArrayList<String>();
                Matcher m = PATTERN.matcher(dayByDayItinerary);
                while (m.find()) {
                    // group1 will give the month (mmm) and group2 will give day (dd)
                    String s = m.group(1) + " " + m.group(2);
                    if (!dateTokens.contains(s))
                        dateTokens.add(m.group(1) + " " + m.group(2));
                }

                Map<List<Date>, String> dataMap2 = null;
                List<Date> dateList = null;
                List<Date> tempDateList = new ArrayList<Date>();

                int startIndex = 0;
                int endIndex = 0;
                int dayIndex = 1;
                String data = null;
                boolean flag = false;

                for (int i = 0; i < dateTokens.size(); i++) {
                    String dateToken = dateTokens.get(i);

                    startIndex = dayByDayItinerary.indexOf(dateToken) + dateToken.length();



                    //try to extract the year from the first 10 characters after month and day
                    String tempDayData = trimAndCleanData(StringUtils.substring(dayByDayItinerary, startIndex));
                    if (tempDayData.length() > 10) {
                        Matcher yearPattern = YEAR_PATTERN.matcher(tempDayData.substring(0,9));
                        while (yearPattern.find()) {
                            String y = yearPattern.group(0);
                            if (y != null && y.length() == 4) {
                                try {
                                    int year = Integer.parseInt(y);
                                    if (getCurrentYear() == year ||  (getCurrentYear() - 1) == year ||  (getCurrentYear() + 1) == year) {
                                        dateToken = dateTokens.get(i) + " " + y;
                                        int yearIndex = tempDayData.indexOf(y);
                                        startIndex = startIndex + yearIndex + 4;

                                    }
                                }   catch (Exception e) {
                                  e.printStackTrace();
                                }

                            }
                        }

                    }


                    if (!dateToken.contains(String.valueOf(getCurrentYear())) && !dateToken.contains(String.valueOf(getCurrentYear() + 1)) && !dateToken.contains(String.valueOf(getCurrentYear() - 1)) ) {
                        dateToken = dateTokens.get(i) + " " + getCurrentYear();
                    }


                    if ((i + 1) == dateTokens.size()) {
                        data = trimAndCleanData(StringUtils.substring(dayByDayItinerary, startIndex));
                    } else {
                        endIndex = dayByDayItinerary.indexOf(dateTokens.get(i + 1));

                        data = StringUtils.trim(StringUtils.substring(dayByDayItinerary, startIndex, endIndex));
                    }

                    if (data.length() < CONTENT_MIN_LENGTH) {
                        flag = true;
                        tempDateList.add(formatDate(dateToken, DATE_FORMAT_MMM_DD_YYYY));
                    }

                    if (data != null && data.length() > 0) {
                        if (data.contains(":") && data.indexOf(":") < (data.length() - 10)) {
                            data = data.substring(data.indexOf(":") + 1);
                        }


                        data = data.substring(0, 1).toUpperCase() + data.substring(1);

                    }

	     		/*if (data.startsWith("&")) {
                     Matcher m2 = PATTERN2.matcher(data);
	    	     	m2.region(0, 10);

	    	     	if (m2.find())
	     			System.out.println(m2.group());
	     		}*/

                    if (!flag) {
                        dateList = new ArrayList<Date>();
                        if (tempDateList != null && tempDateList.size() > 0) {
                            tempDateList.add(formatDate(dateToken, DATE_FORMAT_MMM_DD_YYYY));
                            dateList.addAll(tempDateList);
                            tempDateList = new ArrayList<Date>();
                        } else {
                            dateList.add(formatDate(dateToken, DATE_FORMAT_MMM_DD_YYYY));
                        }

                        if (dateList.size() > 0 && data != null && !data.isEmpty()) {
                            DateDetails details = new DateDetails();
                            details.setDate(dateList.get(0));
                            if (data.contains("\n")) {
                                String s = data.substring(0, data.indexOf("\n"));
                                if (s.equals(s.toUpperCase())) {
                                    details.setTitle(s);
                                    data = data.replace(details.getTitle(), "");
                                }
                            }

                            details.setFulText(data);
                            details.setDates(dateList);
                            if (dateList.size() > 1) {
                                details.setEndDate(dateList.get(dateList.size() - 1));
                            }
                            //get a breakdown of the daily activities
                            details.setActivities(this.parseDailyActivities(details));
                            travelDetails.getDateWiseDetails().put(details.getDate(), details);


                        }
                    } else {
                        flag = false;
                    }

                }

	     	/*
	     	// split each day data
	    	String[] dayByDaySplitUp = removeEmptyParagraphs(PATTERN.split(dayByDayItinerary));

	    	// count of dates and dayByDaySplitup array length should match
	     	if (dateList.size() == dayByDaySplitUp.length) {
	     		for (int i = 0; i < dayByDaySplitUp.length; i++){
	     			extractedDayByDayMap.put(dateList.get(i), StringUtils.trim(dayByDaySplitUp[i]));
	     		}
	     	} else {
	     		throw new Exception("Extracted dates and data counts are not matching. "
	     				+ "Dates: " + dateList.size() + ", Data: " + dayByDaySplitUp.length);
	     	}*/
            }

        } catch (Exception exep) {
            exep.printStackTrace();
        }
        return travelDetails;
    }


    /**
     * Returns the end key to parse the day by day itinerary
     *
     * @param str
     * @return string
     */
    protected String getEndKey(String str) {
        return str.indexOf(endKey1) < 0 ? endKey2 : endKey1;
    }

    /**
     * format the dateString to Date using specified pattern
     *
     * @param dateString
     * @return formatted date
     * @throws ParseException
     */
    protected Date formatDate(String dateString, String[] pattern) throws ParseException {
        Date d = DateUtils.parseDate(dateString, pattern);
        return d;
    }

    /**
     * returns the current year
     *
     * @return year
     */
    protected int getCurrentYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    /**
     * Removes the invalid chars from the String using specified regex
     *
     * @param str
     * @return string
     */
    protected String removeInvalidChars(String str) {
        String s = str.replaceAll(INVALID_CHARS_REGEX, StringUtils.EMPTY);
        return s.replaceAll(WORD_MERGE_FIELD, StringUtils.EMPTY);
    }

    /**
     * Removes the empty paragraphs from the string array
     *
     * @param paraStrings
     * @return String array empty removed
     */
    protected String[] removeEmptyParagraphs(String[] paraStrings) {

        List<String> paraStringsEmptyRemoved = new ArrayList<String>();
        for (String paraString : paraStrings) {
            if (!isParagraphEmpty(paraString)) {
                paraStringsEmptyRemoved.add(paraString);
            }
        }

        return paraStringsEmptyRemoved.toArray(new String[paraStringsEmptyRemoved.size()]);
    }

    /**
     * Returns true if the paragraph does not have any text
     *
     * @param paraString
     * @return boolean
     */
    protected boolean isParagraphEmpty(String paraString) {
        // this is a symbol not hyphen. need to remove this to remove the empty paragraphs
        String str = paraString.replaceAll("[–]", StringUtils.EMPTY);
        return StringUtils.isBlank(str.replaceAll("(\\r|\\n|\\t)", StringUtils.EMPTY));
    }

    private static String trimAndCleanData(String str) {

        String data = StringUtils.trim(str);

        // some of the text starts with ": " which needs to be cleaned
        if (StringUtils.startsWith(data, ": ")) {
            data = data.replaceFirst(": ", StringUtils.EMPTY);
        }

        // some of the documents starts with ", MON: " text which needs to be cleaned
        if (data.length() > CONTENT_MIN_LENGTH) {
            Matcher dayOfWeekMatcher = DAY_OF_WEEK_PATTERN.matcher(data);
            dayOfWeekMatcher.region(0, 10);
            if (dayOfWeekMatcher.find()) {
                data = data.replace(dayOfWeekMatcher.group(), StringUtils.EMPTY);
            }
        }

        //sometimes after above patterns are removed few content has whitespace at beginning
        return StringUtils.trim(data);
    }

}
