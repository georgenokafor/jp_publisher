package com.mapped.publisher.view;

import java.util.List;

/**
 * Created by twong on 2014-05-25.
 */
public class InviteInfoView extends BaseView{
  public String inviteId;
  public String firstName;
  public String lastName;
  public String email;
  public List<TripInfoView> trips;
  public String expiryDate;
  public boolean isExpired;
  public String createdTimestamp;
  public String createdBy;
}
