package com.mapped.publisher.persistence;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-23
 * Time: 5:05 PM
 */
public class SecurityRS {
  private String userId;
  private String groupId;
  private String cmpyId;
  private int linkType;
  private String cmpyType;
  private Long cmpyExpiryTimestamp;

  private String agreementVersion;
  private String targetAgreementVersion;
  private String authorizedUser;


  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getGroupId() {
    return groupId;
  }

  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }

  public String getCmpyId() {
    return cmpyId;
  }

  public void setCmpyId(String cmpyId) {
    this.cmpyId = cmpyId;
  }

  public int getLinkType() {
    return linkType;
  }

  public void setLinkType(int linkType) {
    this.linkType = linkType;
  }

  public String getCmpyType() {
    return cmpyType;
  }

  public void setCmpyType(String cmpyType) {
    this.cmpyType = cmpyType;
  }

  public Long getCmpyExpiryTimestamp() {
    return cmpyExpiryTimestamp;
  }

  public void setCmpyExpiryTimestamp(Long cmpyExpiryTimestamp) {
    this.cmpyExpiryTimestamp = cmpyExpiryTimestamp;
  }

  public String getAgreementVersion() {
    return agreementVersion;
  }

  public void setAgreementVersion(String agreementVersion) {
    this.agreementVersion = agreementVersion;
  }

  public String getTargetAgreementVersion() {
    return targetAgreementVersion;
  }

  public void setTargetAgreementVersion(String targetAgreementVersion) {
    this.targetAgreementVersion = targetAgreementVersion;
  }

  public String getAuthorizedUser() {
    return authorizedUser;
  }

  public void setAuthorizedUser(String authorizedUser) {
    this.authorizedUser = authorizedUser;
  }
}
