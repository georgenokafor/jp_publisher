package com.mapped.publisher.persistence;

import com.mapped.publisher.utils.Log;
import org.apache.commons.lang3.StringUtils;

import java.net.URLDecoder;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-04-11
 * Time: 3:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class ImgMgr {

  private static String PAGE_SEARCH = "select distinct attachname, attachUrl from destination_guide_attach where status = 0 " +
                                      "and attachtype = '3' and destinationguideid in (select g.destinationguideid " +
                                      "from destination_guide g, destination d where d" +
                                      ".cmpyid = ANY(?) and d.status = 0 and d.destinationid = g.destinationid and g" +
                                      ".status = 0 and (upper(g.name) like ? or upper(g.intro) like ? or upper(g.tag) like ? or " +
                                      "upper(g.description) like ? ))" +
                                      " limit 20;";

  private static String PAGE_SEARCH_FILE_NAME = "select distinct attachname, attachUrl from destination_guide_attach where status = 0 " +
                                      "and attachtype = '3' and destinationguideid in (select g.destinationguideid " +
                                      "from destination_guide g, destination d where d" +
                                      ".cmpyid = ANY(?) and d.status = 0 and d.destinationid = g.destinationid and g" +
                                      ".status = 0) and upper(attachname) like ?  limit 20;";
  
  
  private static String POI_SEARCH =
      "(SELECT file_id, name, name_sys, url " +
      "FROM poi_file pf INNER JOIN " +
      "    (SELECT DISTINCT poi_id  " +
      "     FROM poi p, company c " +
      "     WHERE p.state = 'ACTIVE' AND p.cmpy_id = c.cmpy_id AND c.cmpyid = ANY(?) AND p.file_count > 0 AND " +
      "     (to_tsvector('english', p.name) @@ to_tsquery(?) OR " +
      "      to_tsvector('english', p.tags) @@ to_tsquery(?)) " +
      "     ) p ON pf.poi_id = p.poi_id " +
      "LIMIT 20) " +
      "UNION " +
      "(SELECT file_id, name, name_sys, url " +
      "FROM poi_file pf INNER JOIN " +
      "    (SELECT DISTINCT poi_id  " +
      "     FROM poi p, company c " +
      "     WHERE p.state = 'ACTIVE' AND p.cmpy_id = c.cmpy_id AND c.cmpyid = ANY(?) AND p.file_count > 0) p" +
      "     ON pf.poi_id = p.poi_id " +
      "WHERE   (upper(pf.name) like ? OR " +
      "         to_tsvector('english', pf.description) @@ to_tsquery(?)) " +
      "LIMIT 20)";

  private static String TRIP_SEARCH = "select distinct coverfilename, coverurl from trip where status != -1 and cmpyid = ANY(?) " +
                                      "and coverfilename is not null and  (upper(name) like ? or upper(tag) like ? or" +
                                      " upper(comments) like ? or upper(coverfilename) like ?) limit 20;";
  private static String DOC_SEARCH = "select distinct covername, coverurl from destination where status = 0 and cmpyid = ANY" +
                                     "(?) and covername is not null and  (upper(name) like ? or upper(tag) like ? or " +
                                     "upper(intro) like ? or upper(description) like ? or upper (covername) like ?) " +
                                     "limit 20;";

  private static String TRIP_NOTE_SEARCH = "select tna.attach_name as attach_name, tna.attach_url as attach_url from trip_note_attach tna, trip_note tn " +
                                           "where tn.note_id = tna.note_id and tn.status = 0 and tna.status = 0 and " +
                                           "tn.trip_id in (select tripid from trip where cmpyid = ANY(?) and status != -1) and " +
                                           "( " +
                                           "upper(tn.name) like ? or " +
                                           "upper(tn.tag) like ? or " +
                                           "upper(tna.name) like ? or " +
                                           "upper(tna.tag) like ? or " +
                                           "upper(tna.comments) like ? " +
                                           ") " +
                                           "and tna.attach_type = '3' limit 20;";

  public static HashMap<String, String> searchPhotos(String keyword, List<String> cmpies, Connection conn)
      throws SQLException {

    HashMap<String, String> results = new HashMap<>();
    if (keyword == null) {
      return results;
    }

    ArrayList<String> origFileNames = new ArrayList<>();

    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;

    String[] searchParts = StringUtils.split(keyword, "+ ");
    String searchVector = StringUtils.join(searchParts,'&');

    keyword = keyword.replace("+", "%");
    keyword = keyword.replace(" ", "%");

    keyword = keyword.toUpperCase();
    keyword = "%" + keyword + "%";

    try {
      prep = conn.prepareStatement(PAGE_SEARCH_FILE_NAME);
      prep.setArray(1, conn.createArrayOf("text", cmpies.toArray()));
      prep.setString(2, keyword);



      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          String attachName = rs.getString("attachname");
          String attachUrl = rs.getString("attachUrl");
          if (attachName != null && attachUrl != null) {
            if (attachName.contains("_")) {
              int i = attachName.indexOf("_");
              if (i != -1) {
                String origFileName = attachName.substring(i + 1).trim();
                if (!origFileNames.contains(origFileName)) {
                  origFileNames.add(origFileName);
                  results.put(attachName, attachUrl);
                }
              }
            }
          }
        }
        rs.close();
        prep.close();
      }

      int paramCouter = 0;
      prep = conn.prepareStatement(POI_SEARCH);
      prep.setArray(++paramCouter, conn.createArrayOf("text", cmpies.toArray()));
      prep.setString(++paramCouter, searchVector);
      prep.setString(++paramCouter, searchVector);
      prep.setArray(++paramCouter, conn.createArrayOf("text", cmpies.toArray()));
      prep.setString(++paramCouter, keyword);
      prep.setString(++paramCouter, searchVector);

      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          paramCouter = 0;
          // file_id, name, name_sys, url,
          String fileid     = "pf_" + rs.getString(++paramCouter); //Need to figure out how to determine file later
          String name       = rs.getString(++paramCouter);
          String attachName = rs.getString(++paramCouter);
          String attachUrl  = rs.getString(++paramCouter);

          if (attachName == null || name == null || attachUrl == null) {
            continue;
          }


          if (attachName.equals(name)) { //System name is wrong - needs to be determined from URL
            if (!origFileNames.contains(name)) {
              attachName = getAttachNameFromUrl(attachUrl);
              origFileNames.add(name);
              results.put(attachName, attachUrl);
            }
          }
          else if (!origFileNames.contains(name)) {
            origFileNames.add(name);
            results.put(attachName, attachUrl);
          }
        }
        rs.close();
        prep.close();
      }

      prep = conn.prepareStatement(PAGE_SEARCH);
      prep.setArray(1, conn.createArrayOf("text", cmpies.toArray()));
      prep.setString(2, keyword);
      prep.setString(3, keyword);
      prep.setString(4, keyword);
      prep.setString(5, keyword);



      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          String attachName = rs.getString("attachname");
          String attachUrl = rs.getString("attachUrl");
          if (attachName != null && attachUrl != null) {
            if (attachName.contains("_")) {
              int i = attachName.indexOf("_");
              if (i != -1) {
                String origFileName = attachName.substring(i + 1).trim();
                if (!origFileNames.contains(origFileName)) {
                  origFileNames.add(origFileName);
                  results.put(attachName, attachUrl);
                }
              }
            }
          }
        }
        rs.close();
        prep.close();
      }



      prep = conn.prepareStatement(TRIP_SEARCH);
      prep.setArray(1, conn.createArrayOf("text", cmpies.toArray()));
      prep.setString(2, keyword);
      prep.setString(3, keyword);
      prep.setString(4, keyword);
      prep.setString(5, keyword);


      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          String attachName = rs.getString("coverfilename");
          String attachUrl = rs.getString("coverurl");
          if (attachName != null && attachUrl != null) {
            if (attachName.contains("_")) {
              int i = attachName.indexOf("_");
              if (i != -1) {
                String origFileName = attachName.substring(i + 1).trim();
                if (!origFileNames.contains(origFileName)) {
                  origFileNames.add(origFileName);
                  results.put(attachName, attachUrl);
                }
              }
            }
          }
        }
        rs.close();
        prep.close();
      }

      prep = conn.prepareStatement(DOC_SEARCH);
      prep.setArray(1, conn.createArrayOf("text", cmpies.toArray()));
      prep.setString(2, keyword);
      prep.setString(3, keyword);
      prep.setString(4, keyword);
      prep.setString(5, keyword);
      prep.setString(6, keyword);


      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          String attachName = rs.getString("covername");
          String attachUrl = rs.getString("coverurl");
          if (attachName != null && attachUrl != null) {
            if (attachName.contains("_")) {
              int i = attachName.indexOf("_");
              if (i != -1) {
                String origFileName = attachName.substring(i + 1).trim();
                if (!origFileNames.contains(origFileName)) {
                  origFileNames.add(origFileName);
                  results.put(attachName, attachUrl);
                }
              }
            }
          }
        }
        rs.close();
        prep.close();
      }

      prep = conn.prepareStatement(TRIP_NOTE_SEARCH);
      prep.setArray(1, conn.createArrayOf("text", cmpies.toArray()));
      prep.setString(2, keyword);
      prep.setString(3, keyword);
      prep.setString(4, keyword);
      prep.setString(5, keyword);
      prep.setString(6, keyword);


      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          String attachName = rs.getString("attach_name");
          String attachUrl = rs.getString("attach_url");
          if (attachName != null && attachUrl != null) {
            if (attachName.contains("_")) {
              int i = attachName.indexOf("_");
              if (i != -1) {
                String origFileName = attachName.substring(i + 1).trim();
                if (!origFileNames.contains(origFileName)) {
                  origFileNames.add(origFileName);
                  results.put(attachName, attachUrl);
                }
              }
            }
          }
        }
        rs.close();
        prep.close();
      }
    }
    catch(Exception e) {
      Log.err("Error while searching for images:" + e.getMessage());
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
    return results;
  }


  public static String getOrigFileName(String sysFilename) {
    int i = sysFilename.indexOf("_");
    return sysFilename.substring(i + 1).trim();
  }

  public static String getAttachNameFromUrl(String url) {
    String result = URLDecoder.decode(url);
    int pIdx = result.indexOf('?');
    result = result.substring(0, pIdx);
    pIdx = result.lastIndexOf('/');
    result = result.substring(pIdx);
    return result;
  }


}
