package com.umapped.mobile.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.mapped.publisher.persistence.redis.RedisKeys;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.Log;
import com.umapped.mobile.api.model.WeatherForecast;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by wei on 2017-09-08.
 */
@Singleton public class MobileWeatherService {

  @Inject WSClient ws;

  @Inject RedisMgr redisMgr;

  private String getQueryUrl(String location)
      throws UnsupportedEncodingException {
    return "http://api.wunderground.com/api/a20f51c30c485475/forecast/q/" + URLEncoder.encode(location,
                                                                                              "ISO-8859-1") + ".json";
  }

  public CompletionStage<WeatherForecast> getForcast(String location) {
    Optional<WeatherForecast> cachedForecast = redisMgr.getBin(RedisKeys.K_LOCATION_WEATHER,
                                                     location,
                                                     WeatherForecast.class);

    final LocalDate today = LocalDate.now();
    if (cachedForecast.isPresent()) {
      WeatherForecast data = cachedForecast.get();
      if (data.getReportingDate().equals(today)) {
        Log.debug("Get the weather from cache: " + location);
        return CompletableFuture.completedFuture(data);
      }
    }

    try {
      return ws.url(getQueryUrl(location)).setRequestTimeout(50000L).get().thenApply((WSResponse response) -> {
        if (response != null && response.getStatus() < 400) {

          JsonNode forecast = response.asJson().findPath("simpleforecast");

          if (!forecast.isMissingNode()) {
            JsonNode days = forecast.findPath("forecastday");

            if (!days.isMissingNode() && days.isArray()) {
              Iterator<JsonNode> it = days.iterator();
              WeatherForecast weatherForecast = new WeatherForecast();

              while (it.hasNext()) {
                JsonNode day = it.next();

                WeatherForecast.DayForecast f = parse(day);
                weatherForecast.addDayForcast(f);
              }
              weatherForecast.setReportingDate(today);
              redisMgr.set(RedisKeys.K_LOCATION_WEATHER, location, weatherForecast, RedisMgr.WriteMode.FAST);
              return weatherForecast;
            }
          }
        }
        Log.err("No forcast information for this location: " + location);
        return null;
      });
    }
    catch (Exception e) {
      Log.err("Failed to get forecast", e);
    }
    return CompletableFuture.completedFuture(null);
  }


  private WeatherForecast.DayForecast parse(JsonNode day) {
    JsonNode date = day.findPath("date");
    String year = date.get("year").asText();
    String month = date.get("month").asText();
    String dayStr = date.get("day").asText();
    WeatherForecast.DayForecast dayForecast = new WeatherForecast.DayForecast();

    dayForecast.date = (year + "-" + month + "-" + dayStr);
    JsonNode high = day.findPath("high");
    dayForecast.high = parseTemperature(high);
    JsonNode low = day.findPath("low");
    dayForecast.low = parseTemperature(low);
    dayForecast.condition = day.get("conditions").asText();
    dayForecast.conditionIcon = day.get("icon").asText();
    dayForecast.pop = day.get("pop").asText();
    return dayForecast;
  }

  private  WeatherForecast.Temperature parseTemperature(JsonNode temp) {
    WeatherForecast.Temperature t = new WeatherForecast.Temperature();
    t.c = temp.get("celsius").asText();
    t.f = temp.get("fahrenheit").asText();
    return t;
  }
}
