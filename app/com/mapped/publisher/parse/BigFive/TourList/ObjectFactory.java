
package com.mapped.publisher.parse.BigFive.TourList;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mapped.publisher.parse.BigFive.TourList package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Series_QNAME = new QName("", "series");
    private final static QName _TourShortDescription_QNAME = new QName("", "tour_short_description");
    private final static QName _TourName_QNAME = new QName("", "tour_name");
    private final static QName _TourPrice_QNAME = new QName("", "tour_price");
    private final static QName _TourRates_QNAME = new QName("", "tour_rates");
    private final static QName _Interest_QNAME = new QName("", "interest");
    private final static QName _TourItinerary_QNAME = new QName("", "tour_itinerary");
    private final static QName _TourLength_QNAME = new QName("", "tour_length");
    private final static QName _Link_QNAME = new QName("", "link");
    private final static QName _Image_QNAME = new QName("", "image");
    private final static QName _TourPdf_QNAME = new QName("", "tour_pdf");
    private final static QName _TourHighlights_QNAME = new QName("", "tour_highlights");
    private final static QName _Country_QNAME = new QName("", "country");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mapped.publisher.parse.BigFive.TourList
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TourInterests }
     * 
     */
    public TourInterests createTourInterests() {
        return new TourInterests();
    }

    /**
     * Create an instance of {@link TourSeries }
     * 
     */
    public TourSeries createTourSeries() {
        return new TourSeries();
    }

    /**
     * Create an instance of {@link TourImages }
     * 
     */
    public TourImages createTourImages() {
        return new TourImages();
    }

    /**
     * Create an instance of {@link TourCountries }
     * 
     */
    public TourCountries createTourCountries() {
        return new TourCountries();
    }

    /**
     * Create an instance of {@link Tour }
     * 
     */
    public Tour createTour() {
        return new Tour();
    }

    /**
     * Create an instance of {@link TourList }
     * 
     */
    public TourList createTourList() {
        return new TourList();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "series")
    public JAXBElement<String> createSeries(String value) {
        return new JAXBElement<String>(_Series_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tour_short_description")
    public JAXBElement<String> createTourShortDescription(String value) {
        return new JAXBElement<String>(_TourShortDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tour_name")
    public JAXBElement<String> createTourName(String value) {
        return new JAXBElement<String>(_TourName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tour_price")
    public JAXBElement<String> createTourPrice(String value) {
        return new JAXBElement<String>(_TourPrice_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tour_rates")
    public JAXBElement<String> createTourRates(String value) {
        return new JAXBElement<String>(_TourRates_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "interest")
    public JAXBElement<String> createInterest(String value) {
        return new JAXBElement<String>(_Interest_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tour_itinerary")
    public JAXBElement<String> createTourItinerary(String value) {
        return new JAXBElement<String>(_TourItinerary_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tour_length")
    public JAXBElement<String> createTourLength(String value) {
        return new JAXBElement<String>(_TourLength_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "link")
    public JAXBElement<String> createLink(String value) {
        return new JAXBElement<String>(_Link_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "image")
    public JAXBElement<String> createImage(String value) {
        return new JAXBElement<String>(_Image_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tour_pdf")
    public JAXBElement<String> createTourPdf(String value) {
        return new JAXBElement<String>(_TourPdf_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tour_highlights")
    public JAXBElement<String> createTourHighlights(String value) {
        return new JAXBElement<String>(_TourHighlights_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "country")
    public JAXBElement<String> createCountry(String value) {
        return new JAXBElement<String>(_Country_QNAME, String.class, null, value);
    }

}
