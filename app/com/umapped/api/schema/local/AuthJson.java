package com.umapped.api.schema.local;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.umapped.api.schema.common.IBodyJson;
import com.umapped.api.schema.types.Operation;

/**
 * Class to perform various types of login-in
 * Created by surge on 2015-04-30.
 */
@JsonTypeName("AUTHENTICATE")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthJson
    implements IBodyJson{

  @JsonProperty(required = false, value = "operation")
  public Operation operation;

  /** Username if used for authentication */
  @JsonProperty(required = false, value = "username")
  public String username;

  /** Password if used for authentication */
  @JsonProperty(required = false, value = "password")
  public String password;
}
