package com.mapped.publisher.parse.extractor;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.parse.pdf.AreaBasedPDFParser;
import com.mapped.publisher.parse.pdf.PDFParser;
import com.mapped.publisher.parse.valueObject.DateDetails;
import com.mapped.publisher.parse.valueObject.DayDetails;
import com.mapped.publisher.parse.valueObject.TravelDetails;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.pdfbox.pdmodel.PDPage;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.text.ParseException;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VIPToursDataExtractor extends DataExtractor {

	private static String END_KEY_WORD = "Buon Viaggio!";
	
	private static String[] dateFormat = { "EEEE dd'st' MMM yyyy", "EEEE dd'nd' MMM yyyy",
			"EEEE dd'th' MMM yyyy", "EEEE dd'rd' MMM yyyy", "EEEE dd MMM yyyy", "EEEE, dd MMM yyyy" };



    private String introData;
    private static Pattern datePattern;
    private static Pattern dateDetailsPattern;

    private final static String re2="((?:Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday|Tues|Thur|Thurs|Sun|Mon|Tue|Wed|Thu|Fri|Sat))";	// Day Of Week 1
    private final static String re3=".*.";//"(\\s+)";
    private final static String re4="((?:(?:[0-2]?\\d{1})|(?:[3][01]{1})))(?![\\d])";	// Day 1
    private final static String re5=".*.";//"((?:[a-z][a-z]+))";	// Word 1
    private final static String re6="";//"(\\s+)";
    private final static String re7="((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))";	// Month 1
    private final static String re8="(\\s+)";
    private final static String re9="((?:(?:[1]{1}\\d{1}\\d{1}\\d{1})|(?:[2]{1}\\d{3})))(?![\\d])";	// Year 1


    private final static String dre1 = "((?:•))"; // Any Single Character 1
    private final static String dre2 = "(\\s+)"; // Any Single Character 2
    private final static String dre3 = "(\\d+)"; // Integer Number 1
    private final static String dre4 = "(\\.)"; // Any Single Character 3
    private final static String dre5 = "(\\d+)"; // Integer Number 2
    private final static String dre6 = "(\\s+)"; // Any Single Character 4
    private final static String dre7 = "((?:am|AM|pm|PM))"; // Word 1
    private final static String dre8 = "(\\s+)"; // Any Single Character 5
    private final static String dre9 = "((?:–|-))"; // Any Single Character 6


    public String endToken;
    public String lineToken;
    private boolean createCover = false;

    public VIPToursDataExtractor () {

        if (datePattern == null)
            datePattern = Pattern.compile(re2 + re3 + re4 + re5  + re7 + re8 + re9); //re6


        if (dateDetailsPattern == null)
            dateDetailsPattern = Pattern.compile(dre1 + dre2 + dre3 + dre4 + dre5 + dre6 + dre7 + dre8 + dre9, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);



    }

    protected void initImpl (Map <String, String> params) {
        if (params != null) {
            endToken = params.get(APPConstants.PDF_END_TOKEN);
            String coverFlag = params.get(APPConstants.PDF_SAVE_COVER);
            if (coverFlag != null && (coverFlag.trim().toLowerCase().startsWith("y") || coverFlag.trim().toLowerCase().startsWith("t"))) {
                createCover = true;
            }
        }

        if (endToken == null)
            endToken = END_KEY_WORD;


    }

    @Override
	protected TravelDetails extractDataImpl(InputStream input)
			throws Exception {
        PDFParser parser = null;
        try {
		TravelDetails details = new TravelDetails();
		Rectangle rect = new Rectangle(0, 100, 612, 600);
		parser = new AreaBasedPDFParser(rect);
        long startTime = System.currentTimeMillis();
		// Parser and get the raw text data
		String txt = parser.parse(input);
		System.out.println("Parse time: " + (System.currentTimeMillis() - startTime));
		int startIndex = StringUtils.indexOf(txt, endToken);
		int endIndex = startIndex + endToken.length();
		
		String coreDetails = StringUtils.substring(txt, 0, startIndex);
		String additionalInfo = StringUtils.substring(txt, endIndex);
		
		// Extract and return the required data by applying regular expression
		Map<Date, DateDetails>  dateData = extract(cleanText(coreDetails));

        if (introData != null)
            details.setIntroInfo(cleanText(introData));

        if (dateData != null)
            details.setDateWiseDetails(dateData);

            details.setAdditionalInfo(cleanText(additionalInfo));

            //if we need to get the cover img
            if (createCover && parser.getAllPages() != null && parser.getAllPages().size() > 0) {
                details.setCoverImg(((PDPage)parser.getAllPages().get(0)).convertToImage(BufferedImage.TYPE_INT_RGB, 96)); //TYPE_INT_ARGB
            }


            return details;
        } finally {
            if (parser != null) {
               parser.getPDDocument().close();
            }
            if (input != null)
                input.close();
        }
    }

	/**
	 * Reads the text and load the data to a map
	 * 
	 * @param txt
	 * @return
	 * @throws ParseException 
	 */
	private Map<Date, DateDetails> extract(String txt)  {

        Map<Date, DateDetails> dateData =  new TreeMap<Date, DateDetails> ();

		Matcher m = datePattern.matcher(txt);
        int i = 1;
        List<String> dateTokens = new ArrayList<String>();
        while (m.find()) {
            dateTokens.add(m.group());
        }

        for (String s: dateTokens) {
            try {
                Date date = formatDate(s, dateFormat);
                String currentDate = s;


                String nextDate = null;
                if (i < dateTokens.size())
                    nextDate = dateTokens.get(i);

                if (currentDate != null) {
                    int startIndex = txt.indexOf(currentDate) + currentDate.length();
                    int endIndex = -1;
                    if (nextDate != null && !nextDate.isEmpty()) {
                        endIndex = txt.indexOf(nextDate);
                    }
                    String dateText = null;
                    if (endIndex > 0) {
                        dateText = txt.substring(startIndex, endIndex);
                    } else
                        dateText = txt.substring(startIndex);

                    if (dateText != null) {
                        for (int i1 = 0; i1 < dateText.length() - 1; i1++) {
                            if (StringUtils.isAlphanumeric(dateText.substring(i1, i1 + 1))) {
                                dateText = dateText.substring(i1);
                                break;
                            }
                        }
                        DateDetails dateDetails = extractDetailsByTime(dateText, date);
                        if (dateDetails != null) {
                            dateData.put(date, dateDetails);
                        }
                    }

                    //look for intro
                    if (i == 1 && startIndex > 0) {
                        String s1 = txt.substring(0, startIndex);

                        introData = s1;
                    }
                    i++;
                }
            } catch ( Exception e) {

            }


        }
        return dateData;
	}

	private DateDetails extractDetailsByTime(String txt, Date date) {
        DateDetails day = new DateDetails();
        day.setDate(date);
        day.setActivities(new TreeMap<Date, DayDetails>());
        day.setFulText(txt);
        day.setActivities(this.parseDailyActivities(day));

        return day;
	}


	
	/**
	 * Format the date for the given pattern
	 * 
	 * @param str
	 * @param format
	 * @return
	 * @throws ParseException
	 */
	private Date formatDate(String str, String[] format) throws ParseException {
		DateUtils du = new DateUtils();
		return du.parseDate(str, format);
	}




}
