package com.mapped.publisher.persistence;

import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import com.avaje.ebean.Transaction;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import models.publisher.*;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by surge on 2015-11-25.
 */
public class FeedHelperT42
    extends FeedHelperPOI {

  public final static String ENSEMBLE_T42_FEED_NAME = "Travel42 Reviews Feed";
  public final static String ENSEMBLE_T42_S3_PREFIX = "feeds/T42/";
  private static Pattern feedFolder = Pattern.compile(ENSEMBLE_T42_S3_PREFIX +
                                                      "(2[0-9][0-9][0-9])-([0-1][0-9])-([0-3][0-9])/$",
                                                      Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
  private final static boolean DEBUG_PARSER = false;

  class ParseResults {
    public int inserts;
    public int deletes;
    public int updates;
    public int errors;
    public int records;

    public ParseResults() {
      inserts = 0;
      deletes = 0;
      updates = 0;
      records = 0;
      errors = 0;
    }

    public void add(ParseResults pr) {
      inserts += pr.inserts;
      deletes += pr.deletes;
      updates += pr.updates;
      records += pr.records;
      errors  += pr.errors;
    }

    public String toString() {
      StringBuilder sb = new StringBuilder("T42 ");
      sb.append(" Inserts :").append(inserts);
      sb.append(" Deletes :").append(deletes);
      sb.append(" Updates :").append(updates);
      sb.append(" Errors :").append(errors);
      sb.append(" TOTAL :").append(records);
      return sb.toString();
    }
  }

  //AirportPlaceKey	AirportCode	Name	GeoPlaceKey	GeoDisplay	Latitude	Longitude
  enum AirportColumn{
    AirportPlaceKey,
    AirportCode,
    Name,
    GeoPlaceKey,
    GeoDisplay,
    Latitude,
    Longitude,
    COLUMN_COUNT
  }

  //GeoPlaceKey	ParentGeoPlaceKey	Type	RelatedGeoPlaceKey	CityCode	ISOCode	Name	Display	Latitude	Longitude
  enum DestColumn {
    GeoPlaceKey,
    ParentGeoPlaceKey,
    Type,
    RelatedGeoPlaceKey,
    CityCode,
    ISOCode,
    Name,
    Display,
    Latitude,
    Longitude,
    CountryDialCode,
    COLUMN_COUNT
  }

  //HotelId	HotelName	GeoPlaceKey	GeoDisplay	Latitude	Longitude	StarRating	StarReview
  enum HotelColumn {
    HotelId,
    HotelName,
    GeoPlaceKey,
    GeoDisplay,
    Latitude,
    Longitude,
    StarRating,
    StarReview,
    COLUMN_COUNT
  }

  //ClassId	RefClassId	TopClassId	ClassName	RefClassName	TopClassName	SequenceNbr	LevelNbr
  enum GuideIdxColumn {
    ClassId,
    RefClassId,
    TopClassId,
    ClassName,
    RefClassName,
    TopClassName,
    SequenceNbr,
    LevelNbr,
    COLUMN_COUNT
  }

  //GeoPlaceKey	SequenceNbr	PoiPlaceKey	RowType	ClassId	RefClassId	TopClassId	LevelNbr	Content
  enum GuideColumn {
    GeoPlaceKey,
    SequenceNbr,
    PoiPlaceKey,
    RowType,
    ClassId,
    RefClassId,
    TopClassId,
    LevelNbr,
    Content,
    COLUMN_COUNT
  }

  //PoiPlaceKey	ClassId	PoiName	Address1	Address2	ZipCode	Latitude	Longitude	GeoPlaceKey	GeoDisplay	Phone	Tollfree
  // Fax	Website	ShortDesc	HrsOper	CostRate1	CostRate2	FeesDetail	PmtAccept	PmtAcceptDetail	ResvPolicy	ResvDetail	DressCode	Comments
  enum PoiColumn {
    PoiPlaceKey,
    ClassId,
    PoiName,
    Address1,
    Address2,
    ZipCode,
    Latitude,
    Longitude,
    GeoPlaceKey,
    GeoDisplay,
    Phone,
    Tollfree,
    Fax,
    Website,
    ShortDesc,
    HrsOper,
    CostRate1,
    CostRate2,
    FeesDetail,
    PmtAccept,
    PmtAcceptDetail,
    ResvPolicy,
    ResvDetail,
    DressCode,
    Comments,
    COLUMN_COUNT
  }

  @Override public List<FeedResource> getResources() {
    List<FeedResource> result = new ArrayList<>();
    List<S3ObjectSummary> objs = S3Util.getObjectList(S3Util.getPDFBucketName(), ENSEMBLE_T42_S3_PREFIX);

    for(S3ObjectSummary o : objs) {
      Matcher m = feedFolder.matcher(o.getKey());
      if(m.matches()) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)) - 1, Integer.parseInt(m.group(3)), 0, 0, 0);
        Date d = cal.getTime();

        FeedResource fr = new FeedResource();
        fr.setDate(d);
        fr.setName(o.getKey());
        fr.setOrigin(FeedResource.Origin.AWS_S3);
        fr.setSize(o.getSize());
        result.add(fr);
      }
    }
    return result;
  }

  @Override public Map<PoiFeedUtil.FeedResult, Integer> load(String basePath) {
    ParseResults pr = new ParseResults();

    Map<PoiFeedUtil.FeedResult, Integer> results = new HashMap<>();
    try {
      Matcher m = feedFolder.matcher(basePath);
      if(!m.find()) {
        return null;
      }


      //
      // ORDER OF LOAD IS VERY IMPORTANT FOR FOREIGN KEY DEPENDENCIES
      //

      //1. GuideIndeces
      S3Object contents = S3Util.getS3File(S3Util.getPDFBucketName(), basePath + "GuideIndex.txt");
      if(contents == null) {
        Log.err("Can't read Ensemble T42 Feed Guide Indices from S3 from " + basePath);
        return null;
      }

      BufferedReader reader = new BufferedReader(new InputStreamReader(contents.getObjectContent(), "CP1252"));
      pr.add(parseGuideIndex(reader));
      reader.close();


      //2. Destinations
      contents = S3Util.getS3File(S3Util.getPDFBucketName(), basePath + "Destination.txt");
      if(contents == null) {
        Log.err("Can't read Ensemble T42 Feed Destinations from S3 from " + basePath);
        return null;
      }

      reader = new BufferedReader(new InputStreamReader(contents.getObjectContent(),  "CP1252"));
      pr.add(parseDestination(reader));
      reader.close();

      //3. POI (T42 pois)
      contents = S3Util.getS3File(S3Util.getPDFBucketName(), basePath + "Poi.txt");
      if(contents == null) {
        Log.err("Can't read Ensemble T42 Feed POIs from S3 from " + basePath);
        return null;
      }

      reader = new BufferedReader(new InputStreamReader(contents.getObjectContent(),  "CP1252"));
      pr.add(parsePoi(reader));
      reader.close();

      //4. Airports
      contents = S3Util.getS3File(S3Util.getPDFBucketName(), basePath + "Airport.txt");
      if(contents == null) {
        Log.err("Can't read Ensemble T42 Feed Airports from S3 from " + basePath);
        return null;
      }

      reader = new BufferedReader(new InputStreamReader(contents.getObjectContent(),  "CP1252"));
      pr.add(parseAirport(reader));
      reader.close();

      //5. Hotel
      contents = S3Util.getS3File(S3Util.getPDFBucketName(), basePath + "Hotel.txt");
      if(contents == null) {
        Log.err("Can't read Ensemble T42 Feed Hotels from S3 from " + basePath);
        return null;
      }

      reader = new BufferedReader(new InputStreamReader(contents.getObjectContent(),  "CP1252"));
      pr.add(parseHotel(reader));
      reader.close();


      //6. Guides
      contents = S3Util.getS3File(S3Util.getPDFBucketName(), basePath + "Guide.txt");
      if(contents == null) {
        Log.err("Can't read Ensemble T42 Feed Guides from S3 from " + basePath);
        return null;
      }

      reader = new BufferedReader(new InputStreamReader(contents.getObjectContent(),  "CP1252"));
      pr.add(parseGuide(reader));
      reader.close();

      pr.deletes = cleanInactive();

      Log.info("T42 Import results\n" + pr.toString());
    }
    catch (Exception e) {
      Log.err("Failure during Ensemble hotels feed processing");
      e.printStackTrace();
    }
    results.put(PoiFeedUtil.FeedResult.SUCCESS, pr.inserts + pr.updates);
    results.put(PoiFeedUtil.FeedResult.ERROR, pr.errors);
    return results;
  }

  public ParseResults parseAirport(BufferedReader reader) {
    ParseResults r = new ParseResults();
    try {
      Transaction t = Ebean.beginTransaction();
      t.setPersistCascade(false);
      t.setBatchMode(true);
      t.setBatchSize(100);

      SqlUpdate su = Ebean.createSqlUpdate("UPDATE t42_airport SET active = FALSE WHERE active = TRUE");
      Ebean.execute(su);

      while (true) {
        String line = reader.readLine();
        if (line == null) {
          break;
        }

        if (++r.records == 1) {
          continue;
        }

        //HotelId	HotelName	GeoPlaceKey	GeoDisplay	Latitude	Longitude	StarRating	StarReview
        String[] columns = line.split("\\t", -1);
        if (columns.length == AirportColumn.COLUMN_COUNT.ordinal()) {

          T42Airport airport = T42Airport.findByKey(columns[AirportColumn.AirportPlaceKey.ordinal()]);
          if(airport == null) {
            airport = new T42Airport();
            ++r.inserts;
          } else {
            ++r.updates;
          }

          airport.setAirportPlaceKey(columns[AirportColumn.AirportPlaceKey.ordinal()]);
          airport.setAirportCode(columns[AirportColumn.AirportCode.ordinal()]);
          airport.setName(columns[AirportColumn.Name.ordinal()]);
          airport.setGeoPlaceKey(columns[AirportColumn.GeoPlaceKey.ordinal()]);
          airport.setGeoDisplay(columns[AirportColumn.GeoDisplay.ordinal()]);
          airport.setLatitude(columns[AirportColumn.Latitude.ordinal()]);
          airport.setLongitude(columns[AirportColumn.Longitude.ordinal()]);
          airport.setActive(true);
          airport.save();

          if(DEBUG_PARSER) {
            StringBuilder sb = new StringBuilder("Line #");
            sb.append(r.records);
            sb.append(":");
            sb.append(" Key:");
            sb.append(columns[AirportColumn.AirportPlaceKey.ordinal()]);

            sb.append(" AirportCode:");
            sb.append(columns[AirportColumn.AirportCode.ordinal()]);

            sb.append(" Name:");
            sb.append(columns[AirportColumn.Name.ordinal()]);

            sb.append(" GeoPlaceKey:");
            sb.append(columns[AirportColumn.GeoPlaceKey.ordinal()]);

            sb.append(" GeoDisplay:");
            sb.append(columns[AirportColumn.GeoDisplay.ordinal()]);

            sb.append(" Latitude:");
            sb.append(columns[AirportColumn.Latitude.ordinal()]);

            sb.append(" Longitude:");
            sb.append(columns[AirportColumn.Longitude.ordinal()]);

            Log.debug(sb.toString());
          }
        } else {
          Log.err("Failed to parse T42 Airport Line #" + r.records + " : " + line);
          r.errors++;
        }
      }

      Ebean.commitTransaction();
    }
    catch (IOException e) {
      Log.err("Failed to read file", e);
      e.printStackTrace();
      Ebean.rollbackTransaction();
      r.errors++;
    }

    Log.info("T42 Airport Parse Results\n" + r.toString());
    r.records--; //Fix for header line
    return r;
  }

  public ParseResults parseDestination(BufferedReader reader) {
    ParseResults r = new ParseResults();
    try {
      Transaction t = Ebean.beginTransaction();

      //We are disabling FK constraint enforcement until the end of transaction
      SqlUpdate su = Ebean.createSqlUpdate("SET CONSTRAINTS ALL DEFERRED");
      Ebean.execute(su);

      t.setPersistCascade(false);
      t.setBatchMode(true);
      t.setBatchSize(200);

      su = Ebean.createSqlUpdate("UPDATE t42_destination SET active = FALSE WHERE active = TRUE");
      Ebean.execute(su);

      while (true) {
        String line = reader.readLine();
        if (line == null) {
          break;
        }
        if (++r.records == 1) {
          continue;
        }

        //GeoPlaceKey	ParentGeoPlaceKey	Type	RelatedGeoPlaceKey	CityCode	ISOCode	Name	Display	Latitude	Longitude
        String[] columns = line.split("\\t", -1);
        if (columns.length == DestColumn.COLUMN_COUNT.ordinal()) {

          T42Destination dest = T42Destination.findByKey(columns[DestColumn.GeoPlaceKey.ordinal()]);
          if(dest == null) {
            dest = new T42Destination();
            ++r.inserts;
          } else {
            ++r.updates;
          }

          dest.setGeoPlaceKey(columns[DestColumn.GeoPlaceKey.ordinal()]);
          dest.setParentGeoPlaceKey(columns[DestColumn.ParentGeoPlaceKey.ordinal()]);
          dest.setType(columns[DestColumn.Type.ordinal()]);
          dest.setRelatedGeoPlaceKey(columns[DestColumn.RelatedGeoPlaceKey.ordinal()]);
          dest.setCityCode(columns[DestColumn.CityCode.ordinal()]);
          dest.setISOCode(columns[DestColumn.ISOCode.ordinal()]);
          dest.setName(columns[DestColumn.Name.ordinal()]);
          dest.setDisplay(columns[DestColumn.Display.ordinal()]);
          dest.setLatitude(columns[DestColumn.Latitude.ordinal()]);
          dest.setLongitude(columns[DestColumn.Longitude.ordinal()]);
          dest.setActive(true);
          dest.save();

          if(DEBUG_PARSER) {
            StringBuilder sb = new StringBuilder("Line #");
            sb.append(r.records);
            sb.append(":");
            sb.append(" GeoPlaceKey:");
            sb.append(columns[DestColumn.GeoPlaceKey.ordinal()]);

            sb.append(" ParentGeoPlaceKey:");
            sb.append(columns[DestColumn.ParentGeoPlaceKey.ordinal()]);

            sb.append(" Type:");
            sb.append(columns[DestColumn.Type.ordinal()]);

            sb.append(" RelatedGeoPlaceKey:");
            sb.append(columns[DestColumn.RelatedGeoPlaceKey.ordinal()]);

            sb.append(" CityCode:");
            sb.append(columns[DestColumn.CityCode.ordinal()]);

            sb.append(" ISOCode:");
            sb.append(columns[DestColumn.ISOCode.ordinal()]);

            sb.append(" Name:");
            sb.append(columns[DestColumn.Name.ordinal()]);

            sb.append(" Display");
            sb.append(columns[DestColumn.Display.ordinal()]);

            sb.append(" Latitude");
            sb.append(columns[DestColumn.Latitude.ordinal()]);

            sb.append(" Longitude");
            sb.append(columns[DestColumn.Longitude.ordinal()]);

            Log.debug(sb.toString());
          }
        } else {
          Log.err("Failed to parse T42 Destination Line #" + r.records + " : " + line);
          ++r.errors;
        }
      }
      Ebean.commitTransaction();
    }
    catch (IOException e) {
      Log.err("Failed to read file", e);
      e.printStackTrace();
      Ebean.rollbackTransaction();
      r.errors++;
    }

    Log.info("T42 Destination Parse Results\n" + r.toString());
    r.records--; //Fix for header line
    return r;
  }

  public ParseResults parseHotel(BufferedReader reader) {
    ParseResults r = new ParseResults();
    try {
      Transaction t = Ebean.beginTransaction();
      t.setPersistCascade(false);
      t.setBatchMode(true);
      t.setBatchSize(100);

      SqlUpdate su = Ebean.createSqlUpdate("UPDATE t42_hotel SET active = FALSE WHERE active = TRUE");
      Ebean.execute(su);

      while (true) {
        String line = reader.readLine();
        if (line == null) {
          break;
        }
        if (++r.records == 1) {
          continue;
        }

        //HotelId	HotelName	GeoPlaceKey	GeoDisplay	Latitude	Longitude	StarRating	StarReview
        String[] columns = line.split("\\t", -1);
        if (columns.length == HotelColumn.COLUMN_COUNT.ordinal()) {

          T42Hotel hotel = T42Hotel.findByKey(columns[HotelColumn.HotelId.ordinal()]);
          if(hotel == null) {
            hotel = new T42Hotel();
            ++r.inserts;
          } else {
            ++r.updates;
          }

          hotel.setHotelId(columns[HotelColumn.HotelId.ordinal()]);
          hotel.setHotelName(columns[HotelColumn.HotelName.ordinal()]);
          hotel.setGeoPlaceKey(columns[HotelColumn.GeoPlaceKey.ordinal()]);
          hotel.setGeoDisplay(columns[HotelColumn.GeoDisplay.ordinal()]);
          hotel.setLatitude(columns[HotelColumn.Latitude.ordinal()]);
          hotel.setLongitude(columns[HotelColumn.Longitude.ordinal()]);
          hotel.setStarRating(columns[HotelColumn.StarRating.ordinal()]);
          hotel.setStarReview(columns[HotelColumn.StarReview.ordinal()]);
          hotel.setActive(true);
          hotel.save();

          if(DEBUG_PARSER) {
            StringBuilder sb = new StringBuilder("Line #");
            sb.append(r.records);
            sb.append(":");
            sb.append(" HotelID:");
            sb.append(columns[HotelColumn.HotelId.ordinal()]);

            sb.append(" HotelName:");
            sb.append(columns[HotelColumn.HotelName.ordinal()]);

            sb.append(" GeoPlaceKey:");
            sb.append(columns[HotelColumn.GeoPlaceKey.ordinal()]);

            sb.append(" GeoDisplay:");
            sb.append(columns[HotelColumn.GeoDisplay.ordinal()]);

            sb.append(" Latitude:");
            sb.append(columns[HotelColumn.Latitude.ordinal()]);

            sb.append(" Longitude:");
            sb.append(columns[HotelColumn.Longitude.ordinal()]);

            sb.append(" StarRating:");
            sb.append(columns[HotelColumn.StarRating.ordinal()]);

            sb.append(" StarReview:");
            sb.append(columns[HotelColumn.StarReview.ordinal()]);

            Log.debug(sb.toString());
          }
        } else {
          Log.err("Failed to parse T42 Hotel Line #" + r.records + " : " + line);
          r.errors++;
        }
      }
      Ebean.commitTransaction();
    }
    catch (IOException e) {
      Log.err("Failed to read file", e);
      e.printStackTrace();
      Ebean.rollbackTransaction();
      r.errors++;
    }

    Log.info("T42 Hotel Parse Results\n" + r.toString());
    r.records--; //Fix for header line
    return r;
  }

  public ParseResults parsePoi(BufferedReader reader) {
    ParseResults r = new ParseResults();

    try {
      Transaction t = Ebean.beginTransaction();
      t.setPersistCascade(false);
      t.setBatchMode(true);
      t.setBatchSize(100);

      SqlUpdate su = Ebean.createSqlUpdate("UPDATE t42_poi SET active = FALSE WHERE active = TRUE");
      Ebean.execute(su);

      StringBuilder lineBuilder = new StringBuilder();

      while (true) {
        String line = reader.readLine();

        if (line == null) {
          break;
        }

        if (++r.records == 1) {
          continue;
        }

        //PoiPlaceKey	ClassId	PoiName	Address1	Address2	ZipCode	Latitude	Longitude	GeoPlaceKey	GeoDisplay	Phone	Tollfree
        // Fax	Website	ShortDesc	HrsOper	CostRate1	CostRate2	FeesDetail	PmtAccept	PmtAcceptDetail	ResvPolicy	ResvDetail	DressCode	Comments
        String[] columns = line.split("\\t", -1);

        //Problematic lines workaround (if there a newline)
        if(columns.length < PoiColumn.COLUMN_COUNT.ordinal()) {
          Log.err("Will try to recover parse failure for T42 POI Line #" + r.records + " : " + line);

          //Attempt to recover
          if(lineBuilder.length() != 0) {
            lineBuilder.append(line);
            columns = lineBuilder.toString().split("\\t", -1);

            if(columns.length < PoiColumn.COLUMN_COUNT.ordinal()) {
              continue;
            }
          } else {
            //Just adding so that can be later tried again
            lineBuilder.append(line);
            continue;
          }
        }

        if(lineBuilder.length() > 0) {
          lineBuilder.setLength(0); //Clearing builder
        }

        if (columns.length == PoiColumn.COLUMN_COUNT.ordinal()) {

          T42Poi poi = T42Poi.findByKey(columns[PoiColumn.PoiPlaceKey.ordinal()]);
          if(poi == null)  {
            poi = new T42Poi();
            ++r.inserts;
          }
          else {
            ++r.updates;
          }

          poi.setPoiPlaceKey(columns[PoiColumn.PoiPlaceKey.ordinal()]);
          poi.setClassId(columns[PoiColumn.ClassId.ordinal()]);
          poi.setPoiName(columns[PoiColumn.PoiName.ordinal()]);
          poi.setAddress1(columns[PoiColumn.Address1.ordinal()]);
          poi.setAddress2(columns[PoiColumn.Address2.ordinal()]);
          poi.setZipCode(columns[PoiColumn.ZipCode.ordinal()]);
          poi.setLatitude(columns[PoiColumn.Latitude.ordinal()]);
          poi.setLongitude(columns[PoiColumn.Longitude.ordinal()]);
          poi.setGeoPlaceKey(columns[PoiColumn.GeoPlaceKey.ordinal()]);
          poi.setGeoDisplay(columns[PoiColumn.GeoDisplay.ordinal()]);
          poi.setPhone(columns[PoiColumn.Phone.ordinal()]);
          poi.setTollfree(columns[PoiColumn.Tollfree.ordinal()]);
          poi.setFax(columns[PoiColumn.Fax.ordinal()]);
          poi.setWebsite(columns[PoiColumn.Website.ordinal()]);
          poi.setShortDesc(columns[PoiColumn.ShortDesc.ordinal()]);
          poi.setHrsOper(columns[PoiColumn.HrsOper.ordinal()]);
          poi.setCostRate1(columns[PoiColumn.CostRate1.ordinal()]);
          poi.setCostRate2(columns[PoiColumn.CostRate2.ordinal()]);
          poi.setFeesDetail(columns[PoiColumn.FeesDetail.ordinal()]);
          poi.setPmtAccept(columns[PoiColumn.PmtAccept.ordinal()]);
          poi.setPmtAcceptDetail(columns[PoiColumn.PmtAcceptDetail.ordinal()]);
          poi.setResvPolicy(columns[PoiColumn.ResvPolicy.ordinal()]);
          poi.setResvDetail(columns[PoiColumn.ResvDetail.ordinal()]);
          poi.setDressCode(columns[PoiColumn.DressCode.ordinal()]);
          poi.setComments(columns[PoiColumn.Comments.ordinal()]);
          poi.setActive(true);
          poi.save();

          if (DEBUG_PARSER) {
            StringBuilder sb = new StringBuilder("Line #");
            sb.append(r.records);
            sb.append(":");
            sb.append(" PoiPlaceKey:");
            sb.append(columns[PoiColumn.PoiPlaceKey.ordinal()]);

            sb.append(" ClassId:");
            sb.append(columns[PoiColumn.ClassId.ordinal()]);

            sb.append(" PoiName:");
            sb.append(columns[PoiColumn.PoiName.ordinal()]);

            sb.append(" Address1:");
            sb.append(columns[PoiColumn.Address1.ordinal()]);

            sb.append(" Address2:");
            sb.append(columns[PoiColumn.Address2.ordinal()]);

            sb.append(" ZipCode:");
            sb.append(columns[PoiColumn.ZipCode.ordinal()]);

            sb.append(" Latitude:");
            sb.append(columns[PoiColumn.Latitude.ordinal()]);

            sb.append(" Longitude:");
            sb.append(columns[PoiColumn.Longitude.ordinal()]);

            sb.append(" GeoPlaceKey:");
            sb.append(columns[PoiColumn.GeoPlaceKey.ordinal()]);

            sb.append(" GeoDisplay:");
            sb.append(columns[PoiColumn.GeoDisplay.ordinal()]);

            sb.append(" Phone:");
            sb.append(columns[PoiColumn.Phone.ordinal()]);

            sb.append(" Tollfree:");
            sb.append(columns[PoiColumn.Tollfree.ordinal()]);

            sb.append(" Fax:");
            sb.append(columns[PoiColumn.Fax.ordinal()]);

            sb.append(" Website:");
            sb.append(columns[PoiColumn.Website.ordinal()]);

            sb.append(" ShortDesc:");
            sb.append(columns[PoiColumn.ShortDesc.ordinal()]);

            sb.append(" HrsOper:");
            sb.append(columns[PoiColumn.HrsOper.ordinal()]);

            sb.append(" CostRate1:");
            sb.append(columns[PoiColumn.CostRate1.ordinal()]);

            sb.append(" CostRate2:");
            sb.append(columns[PoiColumn.CostRate2.ordinal()]);

            sb.append(" FeesDetail:");
            sb.append(columns[PoiColumn.FeesDetail.ordinal()]);

            sb.append(" PmtAccept:");
            sb.append(columns[PoiColumn.PmtAccept.ordinal()]);

            sb.append(" PmtAcceptDetail:");
            sb.append(columns[PoiColumn.PmtAcceptDetail.ordinal()]);

            sb.append(" ResvPolicy:");
            sb.append(columns[PoiColumn.ResvPolicy.ordinal()]);

            sb.append(" ResvDetail:");
            sb.append(columns[PoiColumn.ResvDetail.ordinal()]);

            sb.append(" DressCode:");
            sb.append(columns[PoiColumn.DressCode.ordinal()]);

            sb.append(" Comments:");
            sb.append(columns[PoiColumn.Comments.ordinal()]);

            Log.debug(sb.toString());
          }
        } else {
          Log.err("Failed to parse T42 POI Line #" + r.records + " : " + line);
          r.errors++;
        }
      }
      Ebean.commitTransaction();
    }
    catch (IOException e) {
      Log.err("Failed to read file", e);
      e.printStackTrace();
      Ebean.rollbackTransaction();
      r.errors++;
    }

    Log.info("T42 POI Parse Results\n" + r.toString());
    r.records--; //Fix for header line
    return r;
  }

  public ParseResults parseGuideIndex(BufferedReader reader) {
    ParseResults r = new ParseResults();
    try {
      Transaction t = Ebean.beginTransaction();

      //We are disabling FK constraint enforcement until the end of transaction
      SqlUpdate su = Ebean.createSqlUpdate("SET CONSTRAINTS ALL DEFERRED");
      Ebean.execute(su);

      t.setPersistCascade(false);
      t.setBatchMode(true);
      t.setBatchSize(100);

      su = Ebean.createSqlUpdate("UPDATE t42_guide_index SET active = FALSE WHERE active = TRUE");
      Ebean.execute(su);

      while (true) {
        String line = reader.readLine();
        if (line == null) {
          break;
        }
        if (++r.records == 1) {
          continue;
        }

        String[] columns = line.split("\\t", -1);
        if (columns.length == GuideIdxColumn.COLUMN_COUNT.ordinal()) {

          T42GuideIndex gidx = T42GuideIndex.findByKey(columns[GuideIdxColumn.ClassId.ordinal()]);
          if(gidx == null){
            gidx = new T42GuideIndex();
            ++r.inserts;
          }
          else {
            ++r.updates;
          }

          gidx.setClassId(columns[GuideIdxColumn.ClassId.ordinal()]);
          gidx.setRefClassId(columns[GuideIdxColumn.RefClassId.ordinal()]);
          gidx.setTopClassId(columns[GuideIdxColumn.TopClassId.ordinal()]);
          gidx.setClassName(columns[GuideIdxColumn.ClassName.ordinal()]);
          gidx.setRefClassName(columns[GuideIdxColumn.RefClassName.ordinal()]);
          gidx.setTopClassName(columns[GuideIdxColumn.TopClassName.ordinal()]);
          gidx.setSequenceNbr(columns[GuideIdxColumn.SequenceNbr.ordinal()]);
          gidx.setLevelNbr(columns[GuideIdxColumn.LevelNbr.ordinal()]);
          gidx.setActive(true);
          gidx.save();

          if (DEBUG_PARSER) {
            StringBuilder sb = new StringBuilder("Line #");
            sb.append(r.records);
            sb.append(":");

            sb.append(" ClassId:");
            sb.append(columns[GuideIdxColumn.ClassId.ordinal()]);

            sb.append(" RefClassId:");
            sb.append(columns[GuideIdxColumn.RefClassId.ordinal()]);

            sb.append(" TopClassId:");
            sb.append(columns[GuideIdxColumn.TopClassId.ordinal()]);

            sb.append(" ClassName:");
            sb.append(columns[GuideIdxColumn.ClassName.ordinal()]);

            sb.append(" RefClassName:");
            sb.append(columns[GuideIdxColumn.RefClassName.ordinal()]);

            sb.append(" TopClassName:");
            sb.append(columns[GuideIdxColumn.TopClassName.ordinal()]);

            sb.append(" SequenceNbr:");
            sb.append(columns[GuideIdxColumn.SequenceNbr.ordinal()]);

            sb.append(" LevelNbr:");
            sb.append(columns[GuideIdxColumn.LevelNbr.ordinal()]);

            Log.debug(sb.toString());
          }
        } else {
          Log.err("Failed to parse T42 GuideIndex Line #" + r.records + " : " + line);
          r.errors++;
        }
      }
      Ebean.commitTransaction();
    }
    catch (IOException e) {
      Log.err("Failed to read file", e);
      e.printStackTrace();
      Ebean.rollbackTransaction();
      r.errors++;
    }

    Log.info("T42 Guide Index Results\n" + r.toString());
    r.records--; //Fix for header line

    return r;
  }

  public ParseResults parseGuide(BufferedReader reader) {
    ParseResults r = new ParseResults();
    try {
      Transaction t = Ebean.beginTransaction();

      //We are disabling FK constraint enforcement until the end of transaction
      SqlUpdate su = Ebean.createSqlUpdate("SET CONSTRAINTS ALL DEFERRED");
      Ebean.execute(su);

      t.setPersistCascade(false);
      t.setBatchMode(true);
      t.setBatchSize(100);

      su = Ebean.createSqlUpdate("UPDATE t42_guide SET active = FALSE WHERE active = TRUE");
      Ebean.execute(su);

      while (true) {
        String line = reader.readLine();
        if (line == null) {
          break;
        }
        if (r.records == 0) {
          r.records++;
          continue;
        }

        //GeoPlaceKey	SequenceNbr	PoiPlaceKey	RowType	ClassId	RefClassId	TopClassId	LevelNbr	Content
        String[] columns = line.split("\\t", -1);
        if (columns.length == GuideColumn.COLUMN_COUNT.ordinal()) {

          T42Guide guide = T42Guide.findByKey(columns[GuideColumn.GeoPlaceKey.ordinal()],
                                              columns[GuideColumn.SequenceNbr.ordinal()]);
          if(guide == null) {
            guide = new T42Guide();
            r.inserts++;
          } else {
            r.updates++;
          }

          guide.setId(columns[GuideColumn.GeoPlaceKey.ordinal()], columns[GuideColumn.SequenceNbr.ordinal()]);
          guide.setPoiPlaceKey(columns[GuideColumn.PoiPlaceKey.ordinal()]);
          guide.setRowType(columns[GuideColumn.RowType.ordinal()]);
          guide.setClassId(T42GuideIndex.findByKey(columns[GuideColumn.ClassId.ordinal()]));
          guide.setRefClassId(T42GuideIndex.findByKey(columns[GuideColumn.RefClassId.ordinal()]));
          guide.setTopClassId(T42GuideIndex.findByKey(columns[GuideColumn.TopClassId.ordinal()]));
          guide.setLevelNbr(columns[GuideColumn.LevelNbr.ordinal()]);
          guide.setContent(columns[GuideColumn.Content.ordinal()]);
          guide.setActive(true);
          guide.save();

          if(DEBUG_PARSER) {
            StringBuilder sb = new StringBuilder("Line #");
            sb.append(r.records);
            sb.append(":");
            sb.append(" GeoPlaceKey:");
            sb.append(columns[GuideColumn.GeoPlaceKey.ordinal()]);
            sb.append(" SequenceNbr:");
            sb.append(columns[GuideColumn.SequenceNbr.ordinal()]);
            sb.append(" PoiPlaceKey:");
            sb.append(columns[GuideColumn.PoiPlaceKey.ordinal()]);
            sb.append(" RowType:");
            sb.append(columns[GuideColumn.RowType.ordinal()]);
            sb.append(" ClassId:");
            sb.append(columns[GuideColumn.ClassId.ordinal()]);
            sb.append(" RefClassId:");
            sb.append(columns[GuideColumn.RefClassId.ordinal()]);
            sb.append(" TopClassId:");
            sb.append(columns[GuideColumn.TopClassId.ordinal()]);
            sb.append(" LevelNbr:");
            sb.append(columns[GuideColumn.LevelNbr.ordinal()]);
            sb.append(" Content:");
            sb.append(columns[GuideColumn.Content.ordinal()]);
            Log.debug(sb.toString());
          }
        } else {
          Log.err("Failed to parse T42 GuideIndex Line #" + r.records + " : " + line);
          r.errors++;
        }
      }
      Ebean.commitTransaction();
    }
    catch (IOException e) {
      Log.err("Failed to read file", e);
      e.printStackTrace();
      Ebean.rollbackTransaction();
      r.errors++;
    }

    Log.info("T42 Guide Parse Results\n" + r.toString());
    r.records--; //Fix for header line
    return r;
  }

  private int cleanInactive() {
    int result = 0;
    try {
      Ebean.beginTransaction();

      //We are disabling FK constraint enforcement until the end of transaction
      SqlUpdate su = Ebean.createSqlUpdate("SET CONSTRAINTS ALL DEFERRED");
      Ebean.execute(su);

      su = Ebean.createSqlUpdate("DELETE FROM t42_airport WHERE active = FALSE");
      result += Ebean.execute(su);

      su = Ebean.createSqlUpdate("DELETE FROM t42_destination WHERE active = FALSE");
      result += Ebean.execute(su);

      su = Ebean.createSqlUpdate("DELETE FROM t42_hotel WHERE active = FALSE");
      result += Ebean.execute(su);

      su = Ebean.createSqlUpdate("DELETE FROM t42_poi WHERE active = FALSE");
      result += Ebean.execute(su);

      su = Ebean.createSqlUpdate("DELETE FROM t42_guide_index WHERE active = FALSE");
      result += Ebean.execute(su);

      su = Ebean.createSqlUpdate("DELETE FROM t42_guide WHERE active = FALSE");
      result += Ebean.execute(su);

      Ebean.commitTransaction();
    } catch(Exception e) {
      Ebean.rollbackTransaction();
      e.printStackTrace();
    }
    return result;
  }

  @Override public PoiImportRS.ImportState associate(PoiImportRS rs) {
    return null;
  }

  @Override public PoiSrcFeed.FeedSyncResult synchronize(PoiSrcFeed psf) {
    return null;
  }

  @Override public List<Pair<PoiRS, Integer>> getRankedPois(PoiImportRS importRS, boolean ignoreCountry) {
    return null;
  }

  @Override public PoiSrcFeed.FeedSyncResult clearAmenities(PoiSrcFeed psf) {
    return null;
  }

}
