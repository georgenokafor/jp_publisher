import com.mapped.publisher.persistence.PoiRS;
import com.mapped.publisher.persistence.PoiSearchHelper;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by surge on 2016-09-23.
 */
public class PoiSearchHelperTest
    extends Assert {

  @Test
  public void smartNameSetTest()  {
    PoiSearchHelper psh = PoiSearchHelper.build().setTermSmart("Royal York, Toronto");
    assertEquals("Toronto", psh.getCity());

    psh = PoiSearchHelper.build().setTermSmart("Royal York Toronto");
    assertNull(psh.getCity());
  }

  @Test
  public void normalizerTest() {
    String in = "Hi, This is narrow and wide space~    ~\t-,.;:??'\"`!";
    String out = "hi this is narrow and wide space  ";

    assertEquals(out, PoiSearchHelper.normalizeTerm(in));
  }

  @Test
  public void rankName() {
    assertEquals(100, PoiSearchHelper.rankName("Aspen Mountain Lodge", "Aspen Mountain Lodge"));
    assertEquals(30, PoiSearchHelper.rankName("Aspen Mountain Lodge", "Aspen Mountain"));
    assertEquals(25, PoiSearchHelper.rankName("Aspen Mountain", "Aspen Mountain Lodge"));
    assertEquals(0, PoiSearchHelper.rankName("Mountain Lodge Aspen", "Aspen Mountain Lodge"));
  }


  @Test
  public void rankPoi_A() {
    PoiRS prs = new PoiRS();
    prs.setName("Aspen Mountain Lodge");
    PoiSearchHelper psh = PoiSearchHelper.build().setTermSmart("Mountain Lodge, Aspen");
    int val = psh.rankPoi(prs);
    assertEquals(100, val);
  }

  @Test
  public void rankPoi_B() {
    PoiRS prs = new PoiRS();
    prs.setName("Mountain Lodge Aspen");
    PoiSearchHelper psh = PoiSearchHelper.build().setTermSmart("Mountain Lodge, Aspen");
    int val = psh.rankPoi(prs);
    assertEquals(100, val);
  }
}
