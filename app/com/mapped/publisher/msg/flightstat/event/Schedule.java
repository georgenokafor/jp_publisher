
package com.mapped.publisher.msg.flightstat.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
@JsonIgnoreProperties({"uplines", "downlines"})

public class Schedule{
   	private String flightType;
   	private String restrictions;
   	private String serviceClasses;
   	private Uplines uplines;
    private Downlines downlines;

 	public String getFlightType(){
		return this.flightType;
	}
	public void setFlightType(String flightType){
		this.flightType = flightType;
	}
 	public String getRestrictions(){
		return this.restrictions;
	}
	public void setRestrictions(String restrictions){
		this.restrictions = restrictions;
	}
 	public String getServiceClasses(){
		return this.serviceClasses;
	}
	public void setServiceClasses(String serviceClasses){
		this.serviceClasses = serviceClasses;
	}
 	public Uplines getUplines(){
		return this.uplines;
	}
	public void setUplines(Uplines uplines){
		this.uplines = uplines;
	}

  public Downlines getDownlines() {
    return downlines;
  }

  public void setDownlines(Downlines downlines) {
    this.downlines = downlines;
  }
}
