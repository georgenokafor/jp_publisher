package com.mapped.publisher.msg;

import com.mapped.publisher.common.ConfigMgr;
import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Created by twong on 2015-01-15.
 */
public class FlightTrackMsg
    implements Serializable {
  public String tripId;
  public String tripDetailsId;
  public long flightId;
  public String airlineCode;
  public String departAirport;
  public String arriveAirport;
  public DateTime departTimestamp;
  public DateTime arriveTimestamp;
  public DateTime timestamp;

  public String userId;

  public boolean isValid() {
    if (!ConfigMgr.getInstance().isProd()) {
      return true;
    }
    if (tripId != null && tripDetailsId != null &&
        flightId > 0 && airlineCode != null && departAirport != null &&
        departTimestamp != null && userId != null) {
      return true;
    }
    return false;
  }


}
