package com.mapped.publisher.parse.traxo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by twong on 10/06/17.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Hotel extends Segment{
    public String address1;
    public String address2;
    public String admin_code;
    public String cancellation_policy;
    public String checkin_date;
    public String checkout_date;
    public String city_name;
    public String country;
    public String hotel_name;
    public String lat;
    public String lon;
    public String number_of_rooms;
    public String postal_code;
    public String rate_description;
    public String room_description;
    public String room_type;
    public String time_zone_id;
}