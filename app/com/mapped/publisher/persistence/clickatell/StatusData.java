package com.mapped.publisher.persistence.clickatell;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Sent in Json Callback
 * https://www.clickatell.com/developers/api-docs/callback-(push-status-and-cost-notification)-advanced-message-send/
 * Created by surge on 2016-02-09.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatusData {
  public Integer apiId;
  public String  apiMessageId;
  public String clientMessageId;
  public Long timestamp;
  public String to;
  public String from;
  public Float charge;
  public String messageStatus;
}
