package com.umapped.itinerary.analyze.webservice;

import com.umapped.itinerary.analyze.location.geonames.GeoNamesWebServiceRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wei on 2017-03-30.
 */
public class WebAPIServiceRequest {
  private static final Logger LOG = LoggerFactory.getLogger(WebAPIServiceRequest.class);

  private Map<String, String> params;

  private Map<String, String> headers;

  public WebAPIServiceRequest() {
    params = new HashMap<>();
    headers = new HashMap<>();
  }

  public void addParam(String name, String value) {
    params.put(name, value);
  }

  public void addHeader(String name, String value) {
    headers.put(name, value);
  }

  public Map<String, String> getHeaders() {
    return headers;
  }

  public String buildQueryString() {
    try {
      StringBuilder builder = new StringBuilder();
      for (String paramName : params.keySet()) {
        append(builder, paramName, params.get(paramName));
        builder.append("&");
      }
      return builder.substring(0, builder.length()-1);
    } catch (UnsupportedEncodingException e) {
      LOG.error("Fail to build query string", e);
      throw new RuntimeException(e);
    }
  }

  private void append(StringBuilder builder, String name, String value)
      throws UnsupportedEncodingException {
    builder.append(name);
    builder.append("=");
    builder.append(URLEncoder.encode(value, "UTF-8"));
  }
}
