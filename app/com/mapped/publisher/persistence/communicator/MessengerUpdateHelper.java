package com.mapped.publisher.persistence.communicator;

import com.avaje.ebean.BeanState;
import com.avaje.ebean.Ebean;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.persistence.SecurityDBMgr;
import com.mapped.publisher.persistence.SecurityRS;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.reservation.flight.UmFlightReservation;

import models.publisher.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;

import javax.persistence.OptimisticLockException;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Semaphore;

import static com.mapped.publisher.common.SecurityMgr.AccessLevel.OWNER;
import static com.mapped.publisher.persistence.communicator.Communicator.*;
import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

/**
 * Helper class to simplify process of creating and updating conferences
 * Created by surge on 2016-08-23.
 */
public class MessengerUpdateHelper {

  private static final boolean DEBUG = true;

  enum RoomAccessType {
    OWNER,
    TRAVELER,
    COLLABORATOR,
    TEAMMATE,
    ADMIN
  }

  static class RoomUser {
    public Account        account;
    public boolean        notify;
    public boolean        access;
    public RoomAccessType type;

    public static RoomUser build(Account account) {
      RoomUser ru = new RoomUser();
      ru.account = account;
      return ru;
    }

    public RoomUser setAccount(Account account) {
      this.account = account;
      return this;
    }

    public RoomUser setNotify(boolean notify) {
      this.notify = notify;
      return this;
    }

    public RoomUser setAccess(boolean access) {
      this.access = access;
      return this;
    }

    public RoomUser setType(RoomAccessType type) {
      this.type = type;
      return this;
    }
    public String getMessengerId(){
      return makeUserIdSafe(account.getLegacyId());
    }
  }

  Trip                            trip;
  MessengerConference             mc;
  Communicator                    comm;
  List<TripDetail>                roomsToUpdate;
  Map<TripDetail, List<RoomUser>> roomsToUsers;
  boolean                         addDefaultRooms;
  boolean                         isLastOperationSuccess;
  Long                            modifiedBy;
  boolean                         isForcedOverwrite;
  boolean                         isForcedCleanup;
  /**
   * If no bookings were added automatically load all bookings for the trip during update
   */
  boolean                         isAutoLoad;


  /**
   * Populates list of users who can access a given trip.
   * Might require some optimization/caching
   *
   * @return
   */
  void computeAuthorizedUsers() {
    StopWatch sw = new StopWatch();
    sw.start();

    //Who can access conference and bookings
    // 0. Owner
    Account owner = Account.findActiveByLegacyId(trip.getCreatedby());
    List<Account> owners = new ArrayList<>();
    owners.add(owner);

    // 1. Collaborators
    List<Account> collaborators = new ArrayList<>();
    List<TripShare> collabs = TripShare.getTripCollaborators(trip);
    if(collabs != null) {
      for (TripShare ts : collabs) {
        Account a = Account.findActiveByLegacyId(ts.getUser().getUserid());
        if(ts.getAccesslevel().ge(OWNER)) {
          owners.add(a);
        } else {
          collaborators.add(a);
        }
      }
    }

    // 2. Company group members (authorized but not notified)
    List<Account> teammates = new ArrayList<>();
    try {
      List<SecurityRS> team = SecurityDBMgr.findByUserId(trip.getCreatedby(), null);
      for (SecurityRS srs : team) {
        if (srs.getCmpyId().equals(trip.getCmpyid()) && !srs.getUserId().equals(trip.getCreatedby())) {
          Account a = Account.findActiveByLegacyId(srs.getUserId());
          teammates.add(a);
        }
      }
    }
    catch (SQLException e) {
      Log.err("Failed to get list of trip teammates for user:" + trip.getCreatedby());
    }

    // 3. Company admins (authorized but not notified)
    List<Account> admins = new ArrayList<>();
    List<AccountCmpyLink> cmpyAdmins = AccountCmpyLink.getActiveCmpyAccountsByType(trip.getCmpyid(),
                                                                                   AccountCmpyLink.LinkType.ADMIN);
    for(AccountCmpyLink acl: cmpyAdmins) {
      Account a = Account.find.byId(acl.getUid());
      if(!a.getLegacyId().equals(trip.getCreatedby())) {
        admins.add(a);
      }
    }

    // 4. Travelers
    List<Account> travelers = new ArrayList<>();
    List<AccountTripLink> tripLinks = AccountTripLink.findByTrip(trip.getTripid());
    for(AccountTripLink atl: tripLinks) {
      Account a = Account.find.byId(atl.getPk().getUid());
      a.setLegacyId(atl.getLegacyId()); //temporarily saving legacy id into account record (in-memory)
      travelers.add(a);
    }


    // Calculating access per trip detail users
    for(TripDetail td: roomsToUpdate) {
      List<RoomUser> roomUsers = new ArrayList<>();
      roomsToUsers.put(td, roomUsers);

      boolean includesTravellers = false;
      boolean includesCollaborators = false;
      boolean includesCollaboratorOwner = false;

      //If general trip chat
      if(td.getDetailsid().contains("-general")) {
        includesTravellers = true;
        includesCollaborators = true;
      } else if(td.getDetailsid().contains("-internal")) {
        includesCollaborators = true;
      } else {
        if(td.getCreatedby() != null && !trip.getCreatedby().equals(td.getCreatedby())) {
          includesCollaboratorOwner = true;
        }
        includesTravellers = true;
      }

      if(includesCollaborators) {
        for(Account a: collaborators) {
          RoomUser ru = RoomUser.build(a)
                                .setAccess(true)
                                .setNotify(false)
                                .setType(RoomAccessType.COLLABORATOR);

          String state = mc.getUsers().get(a.getLegacyId());
          if(state != null && state.equals("true")) {
            ru.setNotify(true);
          }

          roomUsers.add(ru);
        }
      }

      if(includesTravellers) {
        for(Account a: travelers) {
          RoomUser ru = RoomUser.build(a).setAccess(true).setNotify(true).setType(RoomAccessType.TRAVELER);
          roomUsers.add(ru);
        }
      }

      if(includesCollaboratorOwner) {
        for(Account a: collaborators) {
          if(a.getLegacyId().equals(td.getCreatedby())) {
            RoomUser ru = RoomUser.build(a).setAccess(true).setNotify(true).setType(RoomAccessType.COLLABORATOR);
            roomUsers.add(ru);
          }
        }
      }

      //Always include owners (trip creator & OWNER collaborators)
      for(Account oa: owners) {
        RoomUser oru = RoomUser.build(oa).setAccess(true).setNotify(true).setType(RoomAccessType.OWNER);
        roomUsers.add(oru);
      }

      //Add all team mates
      for(Account a: teammates) {
        RoomUser ru = RoomUser.build(a).setAccess(true).setNotify(false).setType(RoomAccessType.TEAMMATE);

        String state = mc.getUsers().get(a.getLegacyId());
        if(state != null && state.equals("true")) {
          ru.setNotify(true);
        }
        roomUsers.add(ru);
      }

      //All company admins
      for(Account a: admins) {
        RoomUser ru = RoomUser.build(a).setAccess(true).setNotify(false).setType(RoomAccessType.ADMIN);

        String state = mc.getUsers().get(a.getLegacyId());
        if(state != null && state.equals("true")) {
          ru.setNotify(true);
        }
        roomUsers.add(ru);
      }

    }

    sw.stop();
    Log.debug("Generated list of authorized users in: " + sw.getTime() + "ms");
  }


  MessengerUpdateHelper(final Trip trip, final Long modifiedBy) {
    this.trip = trip;
    this.modifiedBy = modifiedBy;
    comm = Communicator.Instance();
    roomsToUpdate = new ArrayList<>();
    isForcedOverwrite = false;
    isAutoLoad = true;
    addDefaultRooms = true;

    roomsToUsers = new HashMap<>();
    roomsToUpdate = new ArrayList<>();

    Optional<MessengerConference> opMC = MessengerConference.findByPk(trip.getTripid());
    if (opMC.isPresent()) {
      mc = opMC.get();
      isForcedCleanup = false;
      Log.debug("MessengerUpdateHelper: disabling forced cleanup for trip ID: " + trip.getTripid());
    }
    else {
      mc = MessengerConference.build(trip.getTripid(), modifiedBy);
      isForcedCleanup = true;
      Log.debug("MessengerUpdateHelper: enabling forced cleanup for trip ID: " + trip.getTripid());
    }
  }

  /**
   * Just a builder wrap for consistency
   *
   * @param trip
   * @param accountId
   * @return
   */
  public static MessengerUpdateHelper build(final Trip trip, final Long accountId) {
    return new MessengerUpdateHelper(trip, accountId);
  }

  public static MessengerUpdateHelper build(final Trip trip, final Account a) {
    return new MessengerUpdateHelper(trip, a.getUid());
  }

  public static MessengerUpdateHelper build(final String tripId, final Long modifiedBy) {
    Trip t = Trip.findByPK(tripId);
    return new MessengerUpdateHelper(t, modifiedBy);
  }

  public static boolean isValidMessengerTrip(final Trip trip) {
    return trip.getCreatedtimestamp() > RELEASE_TS_1_15 &&
           trip.getEndtimestamp() > (System.currentTimeMillis() - TRIP_EXPIRATION_OFFSET);
  }

  public MessengerUpdateHelper setAutoLoad(boolean autoLoad) {
    isAutoLoad = autoLoad;
    return this;
  }

  public MessengerUpdateHelper setForcedOverwrite(boolean forcedOverwrite) {
    isForcedOverwrite = forcedOverwrite;
    return this;
  }

  public boolean isLastOperationSuccess() {
    return isLastOperationSuccess;
  }

  public MessengerUpdateHelper setAddDefaultRooms(boolean addDefaultRooms) {
    this.addDefaultRooms = addDefaultRooms;
    return this;
  }

  public MessengerUpdateHelper addRoomToUpdate(TripDetail td) {
    roomsToUpdate.add(td);
    return this;
  }

  public MessengerUpdateHelper addRoomToUpdate(String tripDetailId) {
    TripDetail td = TripDetail.findByPK(tripDetailId);
    if (td != null && td.getStatus() != APPConstants.STATUS_DELETED) {
      roomsToUpdate.add(td);
    }
    else {
      Log.err("MessengerUpdateHelper: Problem finding trip detail ID:" + tripDetailId);
    }
    return this;
  }

  public MessengerUpdateHelper addRoomsToUpdate(Collection<TripDetail> tds) {
    roomsToUpdate.addAll(tds);
    return this;
  }

  private Map<String, RoomUser> getRoomUsers(String roomId) {
    for(TripDetail td: roomsToUsers.keySet()) {
      if(td.getDetailsid().equals(roomId)) {
        List<RoomUser> lru = roomsToUsers.get(td);
        Map<String, RoomUser> rmap = new HashMap<>();
        lru.forEach((ru)->rmap.put(ru.getMessengerId(), ru));
        return rmap;
      }
    }
    return new HashMap<>();
  }


  public MessengerUpdateHelper cleanupUserAccess(final String userId) {
    Log.debug("MessengerUpdateHelper: cleanupUserAccess(): Trip ID: " + trip.getTripid());

    final String safeUid = makeUserIdSafe(userId);
    final int MAX_TRIP_USERS = 1000;
    final Semaphore asyncProtector = new Semaphore(MAX_TRIP_USERS);
    Firebase        confRef        = comm.getUserNotificationConfRef(userId, trip.getTripid());
    if(DEBUG) {
      Log.debug("MessengerUpdateHelper: cleanupUserAccess(): Path: " + confRef.getPath());
    }

    asyncProtector.acquireUninterruptibly();
    confRef.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        if (dataSnapshot.exists()) {
          Log.debug("Firebase will process user: "+safeUid+" notifications for conference ID:", dataSnapshot.getKey());


          //Figure out all the existing rooms that have notification structure created for this user
          DataSnapshot rooms         = dataSnapshot.child("rooms");
          for (DataSnapshot ds : rooms.getChildren()) {
            String roomId = ds.getKey();
            boolean roomToBeDeleted = true;
            for(TripDetail td: roomsToUsers.keySet()) {
              if(td.getDetailsid().equals(roomId)) {
                List<RoomUser> acl = roomsToUsers.get(td);
                for(RoomUser ru: acl) { //Going over all users allowed in this room
                  if(ru.getMessengerId().equals(safeUid) && ru.notify) {
                    roomToBeDeleted = false;
                  }
                }
              }
            }

            if(roomToBeDeleted) {
              asyncProtector.acquireUninterruptibly();
              Firebase roomRef = comm.getUserNotificationRoomRef(safeUid, trip.getTripid(), roomId);
              roomRef.removeValue((firebaseError, firebase) -> {
                if(firebaseError == null) {
                  Log.info("MessengerUpdateHelper: cleanupUserAccess(): Firebase record removed:" + roomRef.getPath());
                } else {
                  Log.err("MessengerUpdateHelper: cleanupUserAccess(): Failed to clean user notifications for user:"
                          + roomRef.getPath());
                }
                asyncProtector.release();
              });
            }
          }
        }
        asyncProtector.release();
      }

      @Override
      public void onCancelled(FirebaseError firebaseError) {
        Log.err("MessengerUpdateHelper: cleanupUserAccess(): Failed to clean user notifications for user: "
                + confRef.getPath());
        asyncProtector.release();
      }
    });

    //Waiting for all jobs to finish
    asyncProtector.acquireUninterruptibly(MAX_TRIP_USERS);
    return this;
  }



  /**
   * Perform conference users cleanup (these users were created in the old logic where all authorized users
   * had their structures created in the messenger without subscribing for those notifications
   */
  public void cleanUsers() {
    Log.debug("MessengerUpdateHelper: cleanUsers(): Trip ID: " + trip.getTripid());

    final int MAX_TRIP_USERS = 1000;

    final Semaphore asyncProtector = new Semaphore(MAX_TRIP_USERS);
    Firebase        confRef        = comm.getConferenceReference(trip.getTripid());
    if(DEBUG) {
      Log.debug("MessengerUpdateHelper: cleanUsers(): ");
    }

    asyncProtector.acquireUninterruptibly();
    confRef.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        if (dataSnapshot.exists()) {
          asyncProtector.acquireUninterruptibly();
          Log.debug("Firebase will process conference ID:", dataSnapshot.getKey());

          //Figure out all the existing users that have notification structure created for this trip
          DataSnapshot rooms         = dataSnapshot.child("rooms");
          Set<String>  usersToRemove = new HashSet<>();
          Set<String>  usersToKeep = new HashSet<>();
          for (DataSnapshot ds : rooms.getChildren()) {
            RoomMetadata   rm    = ds.getValue(RoomMetadata.class);
            Map<String, RoomUser> users = getRoomUsers(rm.id);
            if(DEBUG) {
              Log.debug("MessengerUpdateHelper: cleanUsers(): total new room users:" + users.size() + " prev auth:"
                        + rm.authorizedUsers.size() + " prev notify: " + rm.notifyUsers.size());
            }

            for(String oau: rm.authorizedUsers.keySet()) {
              Boolean state   = rm.authorizedUsers.get(oau);
              RoomUser ru = users.get(oau);

              if(state && (ru == null || !ru.notify)) {
                usersToRemove.add(oau);
              }
              if(ru != null && ru.notify) {
                usersToKeep.add(oau);
              }
            }
          }

          if(DEBUG) {
            Log.debug("MessengerUpdateHelper: cleanUsers(): users to remove:" + usersToRemove.size() +
                      " users to keep: " + usersToKeep.size());
          }
          usersToRemove.removeAll(usersToKeep);

          //Remove conference notification structures for these users, preserving those that are currently authorized
          for (String removable : usersToRemove) {
            Firebase userConf = comm.getUserNotificationConfRef(removable, trip.getTripid());
            asyncProtector.acquireUninterruptibly();
            userConf.removeValue((FirebaseError firebaseError, Firebase firebase) -> {
              if (firebaseError != null) {
                Log.err("MessengerUpdateHelper: cleanUsers(): failed to clean user notifications for user:" + removable);
                mc.setUserState(removable, false);
              } else {
                Log.info("MessengerUpdateHelper: cleanUsers(): Firebase record removed:" + firebase.getPath());
              }
              asyncProtector.release();
            });

            //Removing joined conference structure
            Firebase userJoinedRef = comm.getRefUsers().child(removable).child("joined").child(trip.getTripid());
            userJoinedRef.removeValue();
            Log.info("Firebase record removed:" + userJoinedRef.getPath());
          }
          asyncProtector.release();
        }
        asyncProtector.release();
      }

      @Override
      public void onCancelled(FirebaseError firebaseError) {
        Log.err("Firebase error while processing trip ID: ", trip.getTripid());
        asyncProtector.release();
      }
    });

    //Waiting for all jobs to finish
    asyncProtector.acquireUninterruptibly(MAX_TRIP_USERS);

    mc.markModified(modifiedBy);
    mc.save();
  }

  public MessengerUpdateHelper modifyUserTripAccessState(String userId, boolean state) {
    String  userIdFixed     = makeUserIdSafe(userId);

    boolean stateWasChanged = true;
    if(!isForcedCleanup) {
      String sub = mc.getUsers().get(userIdFixed);
      if(sub != null) {
        boolean isSet = Boolean.valueOf(sub);
        if(state == isSet) {
          stateWasChanged = false;
        }
      }
    }

    if(stateWasChanged) {
      mc.setUserState(userIdFixed, state);
      mc.update(); //Without it record will be cleared by refresh() call
      isForcedCleanup |= !state; //OR Important here since we want to preserve cleanup if set before
      update();
      Log.info("MessengerUpdateHelper: user: " + userId + " has changed trip access to: " +
               (state?"SUBSCRIBED":"UNSUBSCRIBED"));
    } else {
      update();
      cleanupUserAccess(userId);
    }
    return this;
  }

  public MessengerUpdateHelper update() {
    if (!isValidMessengerTrip(trip)) {
      Log.err("MessengerUpdateHelper: update(): invalid trip ID:", trip.getTripid());
      return this;
    }

    StopWatch sw = new StopWatch();
    sw.start();

    //Let's be super helpful, someone forgot to get the trip details in.
    if (roomsToUpdate.size() == 0 && isAutoLoad) {
      List<TripDetail> tds = TripDetail.findActiveByTripId(trip.getTripid());
      roomsToUpdate.addAll(tds);
    }

    if(addDefaultRooms) {
      addInternalRoom();
    }

    if (trip.getStatus() == APPConstants.STATUS_PUBLISHED && addDefaultRooms) {
      addGeneralRoom();
    }

    //If database record is not new then try to refresh it to avoid stale data
    BeanState bs = Ebean.getBeanState(mc);
    if(!bs.isNewOrDirty()) {
      mc.refresh();
    }

    if(roomsToUsers.size() == 0) {
      computeAuthorizedUsers();
    }

    //If this is the first time trip is accessed after we have limited set of travellers
    if(isForcedCleanup) {
      cleanUsers();
    }

    //Updating trip/conference name and parameters
    updateConference();

    for (TripDetail td : roomsToUpdate) {
      singleRoomUpdate(td);
    }

    //Update MessengerConference with all users who have access, so that not to miss anyone who got notification
    //records created
    generateMCUserList();

    try {
      mc.save(); //Updating MessengerConference record with all changes
    } catch (OptimisticLockException ole) {
      Log.err("MessengerUpdateHelper: update(): OptimisticLockException: MessengerConference updated in parallel");
    }

    sw.stop();
    Log.debug("MessengerUpdateHelper: Published conference for trip: " + trip.getTripid() +
              " named: " + trip.getName() + " in " + sw.getTime() + "ms");
    return this;
  }

  MessengerUpdateHelper generateMCUserList() {
    roomsToUsers.forEach((td, rul) -> {
      rul.forEach((ru) -> {
        if (ru.notify) {
          mc.setUserState(ru.getMessengerId(), true);
        }
      });
    });
    return this;
  }

  /**
   * Given a trip create/update conference and default internal chat room
   *
   * @return reference to a created/updated conference
   */
  MessengerUpdateHelper updateConference() {
    Conference conf = new Conference();
    conf.id = trip.getTripid();
    conf.name = trip.getName();
    if (isForcedOverwrite) {
      conf.overwrite(comm.getRefConferences().child(trip.getTripid()));//Overwrites
    }
    else {
      conf.update(comm.getRefConferences().child(trip.getTripid()));   //Update
    }
    return this;
  }

  MessengerUpdateHelper addGeneralRoom() {
    //Creating default room
    TripDetail td = new TripDetail();
    td.setName("Trip Chat");
    td.detailsid = trip.getTripid() + "-general";
    td.createdtimestamp = trip.getCreatedtimestamp();
    td.createdby = makeUserIdSafe(trip.getCreatedby());
    roomsToUpdate.add(td);
    return this;
  }

  MessengerUpdateHelper addInternalRoom() {
    //Creating default room
    TripDetail td = new TripDetail();
    td.setName("Collaborator Trip Chat");
    td.detailsid = trip.getTripid() + "-internal";
    td.createdtimestamp = trip.getCreatedtimestamp();
    td.createdby = makeUserIdSafe(trip.getCreatedby());
    roomsToUpdate.add(td);
    return this;
  }

  MessengerUpdateHelper singleRoomUpdate(final TripDetail booking) {
    final Firebase roomRef = comm.getRoomReference(trip.getTripid(), booking.getDetailsid());

    final RoomMetadata room = new RoomMetadata();
    room.at = booking.getCreatedtimestamp();
    if (booking.getDetailtypeid() == null) {
      room.name = booking.getName();
    }
    else {
      room.name = getRoomName(booking);
    }
    room.by = makeUserIdSafe(booking.getCreatedby());
    room.id = booking.getDetailsid();
    room.type = RoomType.PRIVATE;

    //Map of safe userids to their full names
    Map<String, String> notifiedUsers = new HashMap<>();

    String ownerName = null;
    for(RoomUser ru: roomsToUsers.get(booking)) {
      String safeId = ru.getMessengerId();
      if(ru.notify) {
        notifiedUsers.put(safeId, ru.account.getFullName());
        room.addNotifiedUser(safeId);
        room.addAuthorizedUser(safeId);
      }
      if(ru.access) {
        //We will no-longer keep full list of users per room - saves space
      }
      if(ru.account.getLegacyId().equals(trip.getCreatedby())) {
        ownerName = ru.account.getFullName();
      }
    }

    if (isForcedOverwrite) {
      room.overwrite(roomRef);
    }
    else {
      room.update(roomRef);
    }

    //For all authorized users adding notification rooms
    final NotificationConf nConf = new NotificationConf();
    nConf.name = trip.getName();
    final NotificationRoom nRoom = new NotificationRoom();
    nRoom.name = room.name;

    final Notification notice = new Notification();
    notice.fromUserId = makeUserIdSafe(trip.getCreatedby());
    notice.fromUserName = ownerName;
    notice.notificationType = NotificationType.NEW;
    notice.priority = RoomMessageType.NORMAL;
    notice.status = RoomMessageStatus.UNREAD;
    notice.timestamp = System.currentTimeMillis();

    for (final String userId : notifiedUsers.keySet()) {
      Firebase userRef = comm.getUserRef(userId);
      User     user    = new User();
      user.id = userId;
      user.name = notifiedUsers.get(userId);
      user.update(userRef);
      final Firebase nConfRef = comm.getUserNotificationConfRef(userId, trip.getTripid());
      final Firebase nRoomRef = comm.getUserNotificationRoomRef(userId, trip.getTripid(), room.id);
      nConf.update(nConfRef);
      nRoom.update(nRoomRef);

      nRoomRef.child("events").addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
          if (dataSnapshot.getValue() == null) {
            notice.push(nRoomRef.child("events"));
          }
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
          Log.err("Can't access notification for trip:" + trip.getTripid());
        }
      });
    }
    return this;
  }

  private String getRoomName(TripDetail booking) {
    StringBuilder sb = new StringBuilder();
    sb.append(booking.getDetailtypeid().getLabel());
    sb.append(" on ");
    String date = Utils.getDayMonthString(booking.starttimestamp);
    sb.append(date);
    switch (booking.getDetailtypeid()) {
      case FLIGHT:
        sb.append(" ");
        sb.append(booking.getName());
        UmFlightReservation reservation = cast(booking.getReservation(), UmFlightReservation.class);

        if (reservation != null && !StringUtils.isEmpty(reservation.getFlight().flightNumber)) {
          sb.append(" ");
          sb.append(reservation.getFlight().flightNumber);
        }
        break;
      case HOTEL:
      case TRANSPORT:
      case ACTIVITY:
      case TOUR:
      case CRUISE:
      case RAIL:
      case CAR_RENTAL:
      case PRIVATE_TRANSFER:
      case TAXI:
      case BUS:
      case PUBLIC_TRANSPORT:
      case EVENT:
      case RESTAURANT:
      case SHORE_EXCURSION:
      case CRUISE_STOP:
      case CAR_DROPOFF:
      case SKI_LESSONS:
      case SKI_LIFT:
      case SKI_RENTALS:
      case FERRY:
        sb.append(" ");
        sb.append(booking.getName());
        break;
      default:
        Log.err("Encountered unknown booking type when generating chat name:" + booking.getDetailtypeid().name());
    }

    //Just in case we don't know
    return sb.toString();
  }


  /**
   * Removes booking from the conference, removes all instances of the notifications previously created
   * @param roomId
   */
  MessengerUpdateHelper roomDelete(String roomId) {

    final int       MAX_TRIP_USERS = 1000;
    final Semaphore asyncProtector = new Semaphore(MAX_TRIP_USERS);

    asyncProtector.acquireUninterruptibly();
    final Firebase refRoom = comm.getRoomReference(trip.getTripid(), roomId);
    refRoom.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        if (!dataSnapshot.exists()) {
          asyncProtector.release();
          return;
        }

        RoomMetadata currRoom = dataSnapshot.getValue(RoomMetadata.class);
        if (currRoom != null && currRoom.authorizedUsers != null) {
          for (String userId : currRoom.authorizedUsers.keySet()) {
            Firebase refUser = comm.getUserNotificationConfRef(userId, trip.getTripid()).child("rooms").child(roomId);
            refUser.removeValue();
            Log.info("MessengerUpdateHelper: roomDelete(): Firebase record removed:" + refUser.getPath());

            //Removing joined conference room structure
            Firebase userJoinedRef = comm.getRefUsers().child(userId).child("joined").child(trip.getTripid())
                                         .child(roomId);
            userJoinedRef.removeValue();
            Log.info("MessengerUpdateHelper: roomDelete(): Firebase record removed:" + userJoinedRef.getPath());
          }
        }
        refRoom.removeValue();
        Log.info("MessengerUpdateHelper: roomDelete(): Firebase record removed:" + refRoom.getPath());
        asyncProtector.release();
      }

      @Override
      public void onCancelled(FirebaseError firebaseError) {
        Log.err("Communicator: Failed to remove conference room confId:" + trip.getTripid() + " roomId:" + roomId);
        asyncProtector.release();
      }
    });

    asyncProtector.acquireUninterruptibly(MAX_TRIP_USERS);
    return this;
  }

  /**
   * Delete requested rooms
   * @return
   */
  public MessengerUpdateHelper delete() {
    for(TripDetail td: roomsToUpdate) {
      try { //Try catch here just to make sure we run like a steam roller
        this.roomDelete(td.getDetailsid());
        Log.info("MessengerUpdateHelper: delete(): Conf ID:" + trip.getTripid() + " Room ID: " + td.getDetailsid());
      } catch (Exception e) {
        Log.err("MessengerUpdateHelper: delete(): Failed to delete. Conf ID:" + trip.getTripid() +
                " Room ID: " + td.getDetailsid(), e);
      }
    }
    return  this;
  }



  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///// MESSENGER TRIP WIPE                                                                                          ///
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //
  //
  // 1. Request list of all conferences where expire is before 1 month before today
  // 2. Remove notifications structure from all users that have access
  // 3. If there are no messages remove structures under conferences and messages
  //
  //
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /**
   * Perform blocking trip cleanup.
   */
  public MessengerUpdateHelper wipeTrip() {
    Log.debug("Messenger: cleaning up trip ID: " + trip.getTripid());

    final int MAX_TRIP_USERS = 1000;

    if(mc.isRemoved()) {
      return this; //Already cleaned - nothing to be done
    }

    final Semaphore asyncProtector = new Semaphore(MAX_TRIP_USERS);
    Firebase        confRef        = comm.getRefConferences().child(trip.getTripid());

    asyncProtector.acquireUninterruptibly();
    confRef.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        if (dataSnapshot.exists()) {
          asyncProtector.acquireUninterruptibly();
          Log.debug("Firebase will process conference ID:", dataSnapshot.getKey());

          mc.setRooms(dataSnapshot.getValue());

          DataSnapshot rooms     = dataSnapshot.child("rooms");
          int          roomCount = 0;
          for (DataSnapshot ds : rooms.getChildren()) {
            Log.debug(" > > > Processing Conference Room ID: ", ds.getKey());
            RoomMetadata rm = ds.getValue(RoomMetadata.class);
            for (String user : rm.authorizedUsers.keySet()) {
              //Log.debug(" > > > > User:" + user + " is in room ID: " + ds.getKey());
              mc.addUser(user);
            }
            ++roomCount;
          }
          mc.setRoomCount(roomCount);

          //Backup of the messages and then sequential removal
          Firebase confMessages = comm.getRefMessages().child(trip.getTripid());
          asyncProtector.acquireUninterruptibly();
          confMessages.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
              if (!dataSnapshot.exists()) {
                asyncProtector.release();
                return;
              }

              int msgCount = 0;
              for (DataSnapshot dsRoom : dataSnapshot.getChildren()) {
                msgCount += dsRoom.getChildrenCount();
              }

              mc.setMsgCount(msgCount);

              if (msgCount > 0) {
                mc.setMessages(dataSnapshot.getValue());
              }

              //Now removing the path
              asyncProtector.acquireUninterruptibly();
              confMessages.removeValue((FirebaseError firebaseError, Firebase firebase) -> {
                if (firebaseError != null) {
                  Log.err("cleanupTrip(): Failed to remove messages for conference:" + trip.getTripid() + " Error: "
                          + firebaseError
                      .toString());
                }
                asyncProtector.release();
              });

              asyncProtector.release();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
              Log.err("cleanupTrip(): Error getting messages for conference ID:" + trip.getTripid() + " Error:" +
                      firebaseError
                  .toString());
              asyncProtector.release();
            }
          });


          //Remove conference notification structures for these users
          if (mc.getUsers() != null) {
            for (String userid : mc.getUsers().keySet()) {
              Firebase userConf = comm.getUserNotificationConfRef(userid, trip.getTripid());
              asyncProtector.acquireUninterruptibly();
              userConf.removeValue((FirebaseError firebaseError, Firebase firebase) -> {
                if (firebaseError != null) {
                  Log.err("cleanupTrip(): failed to clean user notifications for user:" + userid);
                  mc.getUsers().put(userid, "false");
                }
                else {
                  Log.debug("cleanupTrip(): removed path:" + firebase.toString());
                }
                asyncProtector.release();
              });

              //Removing joined conference structure
              Firebase userJoinedRef = comm.getRefUsers().child(userid).child("joined").child(trip.getTripid());
              userJoinedRef.removeValue();

              //Removing sessions - they seem not to be cleaned for some reason.
              Firebase userSessionsRef = comm.getRefUsers().child(userid).child("sessions");
              userSessionsRef.removeValue();
            }
          }

          //Removing room-users that have stale sessions
          comm.getRefRoomUsers().child(trip.getTripid()).removeValue();

          //Removing conferences meta information
          asyncProtector.acquireUninterruptibly();
          confRef.removeValue((firebaseError, firebase) -> {
            if (firebaseError != null) {
              Log.err("cleanupTrip(): failed to remove conference metadata for ID" + trip.getTripid() + " Error:" +
                      firebaseError
                  .toString());
            }
            else {
              Log.info("cleanupTrip(): Completed cleaning for conference ID:" + trip.getTripid() + " path:" +
                       firebase.toString());
            }
            mc.markRemoved();
            asyncProtector.release();
          });
          asyncProtector.release();
        }
        else {
          Log.info("Firebase didn't find conference ID: ", dataSnapshot.getKey());
          mc.markRemoved();
        }

        asyncProtector.release();
      }

      @Override
      public void onCancelled(FirebaseError firebaseError) {
        Log.err("Firebase error while processing trip ID: ", trip.getTripid());
        asyncProtector.release();
      }
    });

    //Waiting for all jobs to finish
    asyncProtector.acquireUninterruptibly(MAX_TRIP_USERS);

    mc.markModified(modifiedBy);
    mc.save();
    return this;
  }


  /**
   * Deletes user's notificatons for current trip
   * @param userId
   * @return
   */
  public MessengerUpdateHelper deleteUserNotications(String userId) {
    Log.debug("Messenger: removing user trip ID: " + trip.getTripid());
    String safeUid = makeUserIdSafe(userId);
    final int MAX_TRIP_USERS = 10;

    final Semaphore asyncProtector = new Semaphore(MAX_TRIP_USERS);
    Firebase        confRef        = comm.getUserNotificationConfRef(safeUid, trip.getTripid());

    asyncProtector.acquireUninterruptibly();
    confRef.removeValue((firebaseError, firebase) -> {
      if(firebaseError != null) {
        Log.err("deleteUser(): failed to clean user notifications for user:" + safeUid +
                " Path: " + firebase.getPath());
        mc.addUser(safeUid);
        return;
      } else {
        mc.setUserState(safeUid, false);
        Log.info("deleteUser(): Firebase removed path for user:" + safeUid + " Path: " + firebase.getPath());
      }

      //Removing joined conference structure
      Firebase userJoinedRef = comm.getRefUsers().child(safeUid).child("joined").child(trip.getTripid());
      userJoinedRef.removeValue();

      //Removing sessions - they seem not to be cleaned for some reason.
      Firebase userSessionsRef = comm.getRefUsers().child(safeUid).child("sessions");
      userSessionsRef.removeValue();

      asyncProtector.release();
    });

    //Waiting for all jobs to finish
    asyncProtector.acquireUninterruptibly(MAX_TRIP_USERS);
    return this;
  }
}
