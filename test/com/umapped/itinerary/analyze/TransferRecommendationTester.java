package com.umapped.itinerary.analyze;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.umapped.itinerary.analyze.segment.TripSegment;
import com.umapped.itinerary.analyze.segment.decisiontree.impl.TransferDecisionTree;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeatureDetectorRegistry;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import modules.ItineraryAnalyzeModule;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by wei on 2017-03-10.
 */
public class TransferRecommendationTester extends AbstractRecommendationTester {
  private static String DATE = "2017-03-08/";


  @Inject public TransferRecommendationTester(ObjectMapper om, SegmentFeatureDetectorRegistry registry) {
    super(om, registry, new TransferDecisionTree());
  }

  @Override protected String getOutputDir() {
    return DATE;
  }



  public void outputCsv(String fileName, String outputDir, List<TripSegment> segments, List<List<DecisionResult>> results)
      throws IOException {
    for (int i = 0; i < segments.size(); i++) {
      TripSegment segment = segments.get(i);
      List<DecisionResult> r = results.get(i);
      if (!r.isEmpty()) {
        System.out.println("Positive: " + fileName);
        System.out.println(r);
      }
    }
  }


  public static void main(String[] args)
      throws Exception {
    Injector container = Guice.createInjector(new ItineraryAnalyzeModule());

    TransferRecommendationTester tester = container.getInstance(TransferRecommendationTester.class);

    File dir = new File(SEGMENT_ROOT + DATE);

    File[] files = dir.listFiles((direction, name) -> name.endsWith(".json"));


    for (File f : files) {
      // if (f.getName().contains("my_test")) {
      tester.analyze(f);
      // }
    }
  }
}
