package com.umapped.itinerary.analyze.segment.decisiontree.impl;

import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.segment.decisiontree.EndDecisionNode;
import com.umapped.itinerary.analyze.segment.SegmentNature;
import com.umapped.itinerary.analyze.segment.decisiontree.AbstractDecisionTree;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionNode;
import com.umapped.itinerary.analyze.segment.decisiontree.SplitDecisionNode;
import com.umapped.itinerary.analyze.segment.decisiontree.evaluators.ActivityDecisionEvaluator;
import com.umapped.itinerary.analyze.segment.decisiontree.evaluators.CruiseActivityDecisionEvaluator;

import java.util.ArrayList;
import java.util.List;

import static com.umapped.itinerary.analyze.segment.feature.SegmentFeature.DatesWihoutActivities;
import static com.umapped.itinerary.analyze.segment.feature.SegmentFeature.DurationHours;
import static com.umapped.itinerary.analyze.segment.feature.SegmentFeature.HasCruiseStops;

/**
 * Created by wei on 2017-03-11.
 */
public class ActivityDecisionTree extends AbstractDecisionTree {
  @Override protected DecisionNode initTree() {
    EndDecisionNode noRecommendationResult = new EndDecisionNode((context) -> new ArrayList<>(0));
    EndDecisionNode recommendationResult = new EndDecisionNode(new ActivityDecisionEvaluator());
    EndDecisionNode cruiseStopRecommendationResult = new EndDecisionNode(new CruiseActivityDecisionEvaluator());

    SplitDecisionNode isTransitSegment = new SplitDecisionNode((context) -> context.getCurrent()
                                                                                   .hasNature(SegmentNature.Transit));
    SplitDecisionNode isCruseSegment = new SplitDecisionNode((context) -> context.getCurrent().hasNature(SegmentNature.Cruise));

    SplitDecisionNode hasCruiseStops = new SplitDecisionNode((context) -> context.getFeature(HasCruiseStops));
    SplitDecisionNode longDuration = new SplitDecisionNode((context) -> ((Integer) context.getFeature(DurationHours)) >= 24);

    SplitDecisionNode daysWithoutActivities = new SplitDecisionNode((context) ->
                                                                        ((List<StayPeriod>) context.getFeature(DatesWihoutActivities)).size() > 0);

    isTransitSegment.addNode(Boolean.TRUE, noRecommendationResult)
                    .addNode(Boolean.FALSE, longDuration);

    isCruseSegment.addNode(Boolean.TRUE, hasCruiseStops)
                  .addNode(Boolean.FALSE, isTransitSegment);

    hasCruiseStops.addNode(Boolean.TRUE, cruiseStopRecommendationResult)
                  .addNode(Boolean.FALSE, noRecommendationResult);

    longDuration.addNode(Boolean.FALSE, noRecommendationResult)
                .addNode(Boolean.TRUE, daysWithoutActivities);

    daysWithoutActivities.addNode(Boolean.FALSE, noRecommendationResult)
                         .addNode(Boolean.TRUE, recommendationResult);

    return isCruseSegment;
  }
}
