package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class PageContent { 
 
	 public final static String PageContentTypeId = "3100"; 
	 protected static String pkTypeId =  "3101"; 
	 private String pk; 
	 protected static String pageIdTypeId =  "3102"; 
	 private String pageId; 
	 protected static String contentIdTypeId =  "3103"; 
	 private String contentId; 
	 protected static String contentTypeTypeId =  "3104"; 
	 private String contentType; 
	 protected static String rankTypeId =  "3105"; 
	 private String rank; 
	 protected static String modifiedByTypeId =  "3190"; 
	 private String modifiedBy; 
	 protected static String createdByTypeId =  "3191"; 
	 private String createdBy; 
	 protected static String statusTypeId =  "3192"; 
	 private long status; 
	 protected static String syncTimestampTypeId =  "3193"; 
	 private long syncTimestamp; 
	 protected static String lastUpdateTimestampTypeId =  "3194"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "3195"; 
	 private long createTimestamp; 

 	 // Contructor 
	 public PageContent () {} 
	 public PageContent ( String pk,  String pageId,  String contentId,  String contentType,  String rank,  String modifiedBy,  String createdBy,  long status,  long syncTimestamp,  long lastUpdateTimestamp,  long createTimestamp ) { 
		 this.pk=pk; 
		 this.pageId=pageId; 
		 this.contentId=contentId; 
		 this.contentType=contentType; 
		 this.rank=rank; 
		 this.modifiedBy=modifiedBy; 
		 this.createdBy=createdBy; 
		 this.status=status; 
		 this.syncTimestamp=syncTimestamp; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getPageId () { 
	 	 return pageId; 
 	 } 
 
 	 public void setPageId(String pageId) { 
	 	 this.pageId = pageId; 
 	 } 
 
 	 public  String getContentId () { 
	 	 return contentId; 
 	 } 
 
 	 public void setContentId(String contentId) { 
	 	 this.contentId = contentId; 
 	 } 
 
 	 public  String getContentType () { 
	 	 return contentType; 
 	 } 
 
 	 public void setContentType(String contentType) { 
	 	 this.contentType = contentType; 
 	 } 
 
 	 public  String getRank () { 
	 	 return rank; 
 	 } 
 
 	 public void setRank(String rank) { 
	 	 this.rank = rank; 
 	 } 
 
 	 public  String getModifiedBy () { 
	 	 return modifiedBy; 
 	 } 
 
 	 public void setModifiedBy(String modifiedBy) { 
	 	 this.modifiedBy = modifiedBy; 
 	 } 
 
 	 public  String getCreatedBy () { 
	 	 return createdBy; 
 	 } 
 
 	 public void setCreatedBy(String createdBy) { 
	 	 this.createdBy = createdBy; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public Page getPage () throws Exception{ 
		 return PageMgr.getByPk(pageId); 
	}
	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: PageContent"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 PageId: ").append(this.pageId );  
		 sb.append("	 ContentId: ").append(this.contentId );  
		 sb.append("	 ContentType: ").append(this.contentType );  
		 sb.append("	 Rank: ").append(this.rank );  
		 sb.append("	 ModifiedBy: ").append(this.modifiedBy );  
		 sb.append("	 CreatedBy: ").append(this.createdBy );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", PageContentTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (pageId != null) {
			 memberNode.put(pageIdTypeId, pageId);
		 }
		 if (contentId != null) {
			 memberNode.put(contentIdTypeId, contentId);
		 }
		 if (contentType != null) {
			 memberNode.put(contentTypeTypeId, contentType);
		 }
		 if (rank != null) {
			 memberNode.put(rankTypeId, rank);
		 }
		 if (modifiedBy != null) {
			 memberNode.put(modifiedByTypeId, modifiedBy);
		 }
		 if (createdBy != null) {
			 memberNode.put(createdByTypeId, createdBy);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 return classNode;
	 } 
	 public static PageContent parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 PageContent  c= new PageContent(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.pageId = fields.findPath(pageIdTypeId).textValue();
			 c.contentId = fields.findPath(contentIdTypeId).textValue();
			 c.contentType = fields.findPath(contentTypeTypeId).textValue();
			 c.rank = fields.findPath(rankTypeId).textValue();
			 c.modifiedBy = fields.findPath(modifiedByTypeId).textValue();
			 c.createdBy = fields.findPath(createdByTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 return c;
		} else
			 return null;
	 } 
} 
 
