package com.umapped.mobile.api;

import com.google.inject.Singleton;
import com.mapped.publisher.common.Capability;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.parse.classicvacations.datesearch.Booking;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.utils.Log;
import models.publisher.Account;
import models.publisher.AccountTripLink;
import models.publisher.Trip;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 * Created by wei on 2017-07-14.
 */
@Singleton
public class MobileCommunicatorService {
  public Pair<String, Boolean> getCommunicatorSetting(Long accountId, String tripId) {
    Account account = Account.find.byId(accountId);
    Pair<String, Boolean> result = null;
    Boolean talkBack = false;

    Trip trip = Trip.findByPK(tripId);
    Pair<String, Account.AccountType> userTrip = getUserTrip(account, trip);
    if (userTrip != null) {
      switch (userTrip.getRight()) {
        case TRAVELER:
          talkBack = SecurityMgr.hasCapability(trip, Capability.COMMUNICATOR_TRAVELER_TALKBACK);
          break;
        case PUBLISHER:
          talkBack = true;
          break;
        default:
          Log.err("Should not happen");
          break;
      }
      String token = Communicator.getAccountTripFirebaseToken(trip, userTrip.getLeft(), account);
      result = new ImmutablePair<>(token, talkBack);
    }
    return result;
  }


  public Pair<String, Account.AccountType> getUserTrip(Account account, Trip trip) {
    Pair<String, Account.AccountType> result = null;
    if (account != null && trip != null) {
      switch (account.getAccountType()) {
        case TRAVELER:
          AccountTripLink.ATLPk pk = new AccountTripLink.ATLPk();
          pk.setUid(account.getUid());
          pk.setTripid(trip.getTripid());
          AccountTripLink tl = AccountTripLink.find.byId(pk);
          if (tl != null) {
            result = new ImmutablePair<>(tl.getLegacyId(), Account.AccountType.TRAVELER);
          }
          break;
        case PUBLISHER:
          result = new ImmutablePair<>(account.getLegacyId(), Account.AccountType.PUBLISHER);
          break;
        default:
          Log.err("Not support for role other than publisher or traveler");
          break;
      }
    }
    return result;
  }
}
