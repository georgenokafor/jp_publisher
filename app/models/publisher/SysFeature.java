package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-12-11.
 */
@Entity
@Table(name = "sys_feature")
public class SysFeature
    extends Model {
  public static Finder<Integer, SysFeature> find = new Finder(SysFeature.class);

  @Id
  @Column(name = "pk")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_sys_feature_pk")
  Integer pk;
  /**
   * Feature name
   */
  String  name;
  /**
   * Description for the feature
   */
  String  description;
  /**
   * Indicates whether or not to show capability to the user
   */
  Boolean userControlled;
  Boolean enabled;
  Long   createdTs;
  String createdBy;
  Long   modifiedTs;
  String modifiedBy;
  @javax.persistence.Version
  int version;

  public static SysFeature build(String userId) {
    SysFeature f = new SysFeature();
    f.setCreatedBy(userId);
    f.setModifiedBy(userId);
    f.setCreatedTs(System.currentTimeMillis());
    f.setModifiedTs(f.getCreatedTs());
    f.setEnabled(true);
    f.setUserControlled(false);
    return f;
  }

  public void markUpdated(String userId) {
    setModifiedBy(userId);
    setModifiedTs(System.currentTimeMillis());
  }

  public Boolean getUserControlled() {
    return userControlled;
  }

  public void setUserControlled(Boolean userControlled) {
    this.userControlled = userControlled;
  }

  public Boolean getEnabled() {
    return enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }


  public static List<SysFeature> allEnabled() {
    return find.where().eq("enabled", true).findList();
  }

  public static List<SysFeature> publicEnabled() {
    return find.where().eq("enabled", true).eq("user_controlled", true).findList();
  }

  public static List<SysFeature> getBillingPlanFeatures(Long planId) {
    com.avaje.ebean.Query<BillingPlanFeature> subQuery = Ebean
        .createQuery(BillingPlanFeature.class)
        .select("pk.featurePk")
        .where()
        .eq("plan_id", planId)
        .query();

    return find.where().in("pk", subQuery).findList();
  }

  public Integer getPk() {
    return pk;
  }

  public void setPk(Integer pk) {
    this.pk = pk;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Long createdTs) {
    this.createdTs = createdTs;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Long modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
