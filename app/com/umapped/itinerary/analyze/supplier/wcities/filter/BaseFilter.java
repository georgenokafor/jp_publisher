package com.umapped.itinerary.analyze.supplier.wcities.filter;

import com.umapped.itinerary.analyze.supplier.wcities.WCitiesGuideEnum;

import java.util.List;

public interface BaseFilter {

  public List<WCitiesGuideEnum> getExcluded();
  public List<WCitiesGuideEnum> getIncluded();
}
