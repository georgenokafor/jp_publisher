
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AvailableTours complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AvailableTours">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Tours" type="{http://webapi.globusandcosmos.com/AvailableTours.xsd}ArrayOfTour" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AvailableTours", namespace = "http://webapi.globusandcosmos.com/AvailableTours.xsd", propOrder = {
    "tours"
})
public class AvailableTours {

    @XmlElement(name = "Tours")
    protected ArrayOfTour tours;

    /**
     * Gets the value of the tours property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTour }
     *     
     */
    public ArrayOfTour getTours() {
        return tours;
    }

    /**
     * Sets the value of the tours property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTour }
     *     
     */
    public void setTours(ArrayOfTour value) {
        this.tours = value;
    }

}
