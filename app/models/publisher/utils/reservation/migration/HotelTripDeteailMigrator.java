package models.publisher.utils.reservation.migration;

import com.mapped.publisher.utils.Log;
import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;

import models.publisher.HotelBooking;
import models.publisher.TripDetail;
import models.utils.migration.Migrator;

public class HotelTripDeteailMigrator implements Migrator<TripDetail> {

  private UmAccommodationReservationBuilder builder = new UmAccommodationReservationBuilder();
  
  @Override
  public boolean migrate(TripDetail entity) {
    HotelBooking hotelBooking = HotelBooking.find.byId(entity.getDetailsid());
    Log.info("Migrate a hotel trip_detail");
    UmAccommodationReservation reservation = builder.build(entity, hotelBooking);
    entity.setReservation(reservation);
    return true;
  }
}