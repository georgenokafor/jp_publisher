package models.publisher.utils.reservation.migration;

import com.mapped.publisher.utils.Log;
import com.umapped.persistence.reservation.transfer.UmTransferReservation;

import models.publisher.TmpltDetails;
import models.publisher.TmpltTransport;
import models.utils.migration.Migrator;

public class TransportTmpltDetailMigrator implements Migrator<TmpltDetails> {

  private UmTransferReservationBuilder builder = new UmTransferReservationBuilder();
  
  @Override
  public boolean migrate(TmpltDetails entity) {
    
    TmpltTransport transport = TmpltTransport.find.byId(entity.detailsid);
    Log.info("Migrate a transport trip_detail");
    UmTransferReservation reservation = builder.build(entity, transport);
    entity.setReservation(reservation);
    return true;
  }

}
