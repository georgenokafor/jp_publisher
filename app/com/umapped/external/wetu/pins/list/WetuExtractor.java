package com.umapped.external.wetu.pins.list;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Map;


/**
 * Created by george on 2016-06-09.
 */
public class WetuExtractor {

  public WetuList[] extractDataFromList(String token) {
    HttpClient client = HttpClientBuilder.create().build();
    HttpGet get = new HttpGet("https://wetu.com/API/Pins/"+ token + "/List");
    get.setHeader("Content-Type", "application/json; charset=UTF-8");
    get.addHeader("accept", "application/json");

    HttpResponse r = null;
    try {
      r = client.execute(get);
    } catch (IOException e) {
      e.printStackTrace();
    }

    HttpEntity entity = r.getEntity();
    WetuList[] details = null;

    if (r != null && r.getStatusLine() != null && r.getStatusLine().getStatusCode() == 200) {

      try {
        String resp = EntityUtils.toString(entity);
        ObjectMapper mapper = new ObjectMapper();

        com.fasterxml.jackson.databind.ObjectReader reader = mapper.reader(WetuList[].class);
        details = reader.readValue(resp);
      }
      catch (Exception e) {
        Log.log(LogLevel.ERROR, "WetuList - Cannot read JSON from response: " , e);
        e.printStackTrace();
      }

    }
    return details;

  }


  public Accommodation[] getAccommodations(String token, String ids) {
    HttpEntity entity = getPin(token, ids);
    Accommodation[] details = null;

    if (entity != null) {

      try {
        String resp = EntityUtils.toString(entity);
        ObjectMapper mapper = new ObjectMapper();

        com.fasterxml.jackson.databind.ObjectReader reader = mapper.readerFor(Accommodation[].class);
        details = reader.readValue(resp);
      }
      catch (Exception e) {
        Log.log(LogLevel.ERROR, "Accommodations - Cannot read JSON from response: " , e);
        e.printStackTrace();
      }
    }
    return details;
  }

  public Activity[] getActivities(String token, String ids) {
    HttpEntity entity = getPin(token, ids);
    Activity[] details = null;

    if (entity != null) {

      try {
        String resp = EntityUtils.toString(entity);
        ObjectMapper mapper = new ObjectMapper();

        com.fasterxml.jackson.databind.ObjectReader reader = mapper.readerFor(Activity[].class);
        details = reader.readValue(resp);
      }
      catch (Exception e) {
        Log.log(LogLevel.ERROR, "Activities - Cannot read JSON from response: " , e);
        e.printStackTrace();
      }
    }
    return details;
  }

  public Destination getDestination(String token, String id) {
    HttpEntity entity = getPin(token, id);
    Destination[] destinations = null;

    if (entity != null) {
      try {
        String resp = EntityUtils.toString(entity);
        ObjectMapper mapper = new ObjectMapper();

        com.fasterxml.jackson.databind.ObjectReader reader = mapper.reader(Destination[].class);
        destinations = reader.readValue(resp);
        if (destinations != null && destinations.length > 0) {
          return destinations[0];
        }
      }
      catch (Exception e) {
        Log.log(LogLevel.ERROR, "Destinations - Cannot read JSON from response: " , e);
        e.printStackTrace();
      }
    }

    return null;
  }

  public Country getCountry(String token, String id) {
    HttpEntity entity = getPin(token, id);
    Country[] countries = null;

    if (entity != null) {

      try {
        String resp = EntityUtils.toString(entity);
        ObjectMapper mapper = new ObjectMapper();

        com.fasterxml.jackson.databind.ObjectReader reader = mapper.readerFor(Country[].class);
        countries = reader.readValue(resp);
        if (countries != null && countries.length > 0) {
          return countries[0];
        }
      }
      catch (Exception e) {
        Log.log(LogLevel.ERROR, "Accommodations - Cannot read JSON from response: " , e);
        e.printStackTrace();
      }
    }
    return null;
  }

  public HttpEntity getPin (String token, String ids) {
    HttpClient client = HttpClientBuilder.create().build();
    String url = "";
    if (ids != null) {
      url = "https://wetu.com/API/Pins/"+ token +"/Get?" + ids;
    }
    else {
      url = "https://wetu.com/API/Pins/" + token + "/Get";
    }

    HttpGet get = new HttpGet(url);
    get.setHeader("Content-Type", "application/json; charset=UTF-8");
    get.addHeader("accept", "application/json");

    HttpResponse r = null;
    try {
      r = client.execute(get);
      if (r != null && r.getStatusLine() != null && r.getStatusLine().getStatusCode() == 200) {
        return r.getEntity();
      }

    } catch (IOException e) {
      Log.err("Cannot get Wetu Content: id: " + ids, e);
    }
    return null;

  }
}
