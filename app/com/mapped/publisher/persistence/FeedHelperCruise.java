package com.mapped.publisher.persistence;

import java.util.Map;

/**
 * Created by surge on 2015-10-01.
 */
public abstract class FeedHelperCruise {
  abstract public Map<PoiFeedUtil.FeedResult, Integer> load(String parameters);
}
