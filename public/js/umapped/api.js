var ApiMessage = function() {
  this.header = null;
  this.body = null;
  this['@type'] = null;

  var toJson = function() {
    return JSON.stringify(this);
  };
};

var ApiRequestHeader = function() {
  this.api_key = "0";
  this.sub_version = "0";
  this.req_id = guid();
  this.callback_url = null;
};

var ApiResponseHeader = function() {
};


var CompanyBody = function(operation) {
  this.operation = operation;
  this.companies = [];
  this.search_term = null;
  this.search_id = null;
  this.cursor = null;
  this.count = null;

  var CompanyDetails = function(id, name, legacyId) {
    this.id = id;
    this.legacy_id = legacyId;
    this.name = name;
  };

  this.addCompany = function(id, name, legacyId) {
    this.companies.push(new CompanyDetails(id, name, legacyId));
  };
};

var EnumOperations = {
  ADD:    "ADD",
  AUTHENTICATE:   "AUTHENTICATE",
  COPY:   "COPY",
  UPDATE: "UPDATE",
  DELETE: "DELETE",
  LIST:   "LIST",
  SEARCH: "SEARCH",
  UPLOAD: "UPLOAD",
  STATE: "STATE",
};

var EnumPrivacy = {
  PUBLIC : "public",
  PRIVATE: "private"
};

var EnumAddressTypes = {
  ADDR_MAIN: "main",
  ADDR_BUSINESS:"business",
  ADDR_BILLING: "billing",
  ADDR_MAILING: "mailing",
  ADDR_HOME: "home",
  ADDR_ENTRANCE: "entrance",
  ADDR_PICKUP: "pickup",
  ADDR_DROPOFF: "dropoff"
};

var EnumPhoneTypes = {
  PHONE_MAIN: "main",
  PHONE_TOLLFREE:"tollfree",
  PHONE_DAY:"day",
  PHONE_EVENING:"evening",
  PHONE_NIGHT:"night",
  PHONE_EMERGENCY:"emergency",
  PHONE_BUSINESS:"business",
  PHONE_PERSONAL:"personal",
  PHONE_MOBILE:"mobile",
  PHONE_FAX:"fax"
};

var EnumEmailTypes = {
  MAIN:"main",
  BUSINESS:"business",
  BILLING:"billing",
  HOME:"home"
};


var IcePhotos = function(operation) {
  this.operation = operation;
  this.searchId = null;
  this.cmpyId = null;
  this.suggestions = [];
  this.photos = [];

  this.Photo = function(url, desc) {
    this.thumb = null;
    this.url = url;
    this.caption = desc;
    this.fileName = null;
    this.importId = null;
  };

  this.Suggestion = function(feedId, poiId) {
    this.poiId = poiId;
    this.id = feedId;
    this.name = null;
    this.countryCode = null;
    this.region = null;
    this.locality = null;
    this.address = null;
    this.postalCode = null;
    this.phone = null;
    this.score = null;
    
  };

  this.addPhoto = function(url, desc) {
    var photo = new this.Photo(url, desc);
    this.photos.push(photo);
    return photo;
  };

  this.addSuggestion = function(feedId, poiId) {
    var suggestion = new this.Suggestion(feedId, poiId);
    this.suggestions.push(suggestion);
    return suggestion;
  }
};

var buildAddress = function(privacy, addressType) {
  this.address_type = addressType;
  this.privacy = privacy;
  this.street_address = null;
  this.locality = null;
  this.region = null;
  this.postal_code = null;
  this.country_code = null;
  this.coordinates = {
    latitude : 0,
    longitude : 0
  };
};

var buildEmail = function(privacy, emailType, address) {
  this.privacy = privacy;
  this.email_type = emailType;
  this.address = address;
};

var buildPhoneNumber = function(privacy, phoneType, number) {
  this.privacy = privacy;
  this['phone-type'] = phoneType;
  this.number = number;
};

var buildSocialLink = function(serviceName, username, url) {
  this.service_name = serviceName;
  this.service_username = username;
  this.service_direct_url = url;
};

var Poi = function() {
  this['poi-id-js'] = null;
  this['poi-type-id'] = null;
  this['poi-type-name'] = null;
  this['cmpy-id'] = null;
  this.name = null;
  this.desc = null;
  this.code = null;

  this.tags = [];
  this.emails = [];
  this.addresses = [];
  this['phone-numbers'] = [];
  this['social-links'] = [];
  this.urls = [];

  this.addTag = function(tag) {
    this.tags.push(tag);
  };

  this.addEmail = function(email) {
    this.emails.push(email);
  };

  this.addAddress = function(address) {
    this.addresses.push(address);
  };

  this.addPhoneNumber = function(number) {
    this['phone-numbers'].push(number);
  };

  this.addSocialLink = function(socialLink) {
    this['social-links'].push(socialLink);
  };

  this.addUrl = function(url) {
    this.urls.push(url);
  };

  this.addProperty = function(name, value) {
    this[name] = value;
  };
};

var PoiBody = function(operation) {
  this.operation = operation;
  this.search_term = null;
  this.search_id = null;
  this.cursor = null;
  this.count = null;

  this.cmpy_id = null;
  this.selected_types = []; //List of poi type ids
  this.pois = [];

  this.addPoi = function(poi) {
    this.pois.push(poi);
  };
};

var PoiFile = function(fileId, cmpyId) {
  this.file_id = fileId;
  this.cmpy_id = cmpyId;
  this.name = null;
  this.url = null;
  this.desc = null;
  this.priority = null;
};

var PoiFileBody = function(operation) {
  this.operation = operation;
  this.poi_id = null;
  this.cmpy_id = null;
  this.files = [];

  this.addFile = function(name, desc, fileId) {
    var f = new PoiFile();
    f.name = name;
    f.desc = desc;
    f.file_id = fileId;
    this.files.push(f);
  };
};

var BookingFileBody = function(operation) {
  PoiFileBody.apply(this);
  this.operation = operation;
  this.detailsId = null;
  this.showPhotos = null;
};

var PageDetails = function() {
  this.resultId = null;
  this.title = null;
  this.url = null;
  this.displayUrl = null;
  this.description = null;
};

var WebSearch = function(operation){
  this.operation = operation;
  this.search_term = null;
  this.id = null; //Id of the parent document
  this.cursor = null;
  this.count = null;
  this.cmpyid = null; //Text
  this.jobId = null;
  this.jobCmd = null;
  this.state = null;
  this.pages = [];
  this.domains = [];

  this.addPage = function(id, title, url, displayUrl, desc) {
    var page = new PageDetails();
    page.resultId = id;
    page.title = title;
    page.url = url;
    page.displayUrl = displayUrl;
    page.description = desc;
    this.pages.push(page);
  };

  this.addDomain = function(domainName) {
    this.domains.push(domainName);
  }
};

/**
 * Consortium req/resp body object constructor
 * @constructor
 */
var ConsortiumBody = function(operation) {
  this.operation = operation;
  this.consortia = [];
  this.companies = [];
  this.search_term = null;
  this.search_id = null;
  this.cursor = null;
  this.count = null;

  var ConsortiumDetails = function(id, name) {
    this.id = id;
    this.name = name;
  };

  var ConsortiumCompanyDetails = function(consortiumId, companyId, companyName) {
    this.consortium_id = consortiumId;
    this.company_id = companyId;
    this.company_name = companyName;
  }

  this.addConsortium = function(id, name) {
    this.consortia.push(new ConsortiumDetails(id, name));
  }

  this.addConsortiumCompany = function(consortiumId, companyId, companyName) {
    this.companies.push(new ConsortiumCompanyDetails(consortiumId, companyId, companyName));
  }
};

//GUID Implementation from: http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript
var guid = (function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
  }
  return function() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  };
})();

/**
 * Sends via POST fully prepared API messages (api_message) to provided URL (api_url).
 * If receives back valid response header if the code is a success executes provided callback (success_cb),
 * otherwise if header informs of a problem executes provided (fail_cb) function.
 * If fail_cb parameter is empty reads message from the header and displays it to the user.
 *
 * @param api_message fully prepared ApiMessage object
 * @param api_url - url to send requests to
 * @param success_cb - callback function to execute when header indicates success
 * @param fail_cb - callback function execute when header indicates a failure
 * @constructor
 */
var ExecuteCommand = function(api_message, api_url, success_cb, fail_cb) {
  var jqxhr = $.ajax({
    type: "POST",
    url: api_url,
    data: JSON.stringify(api_message),
    contentType: "application/json; charset=utf-8",
    dataType: "json"})
      .always(function(data, textStatus, jqXHR) {
        if (textStatus == 'success' && data) {
          var isSuccess = processResponseHeader(data.header);
          if (isSuccess) {
            success_cb(data);
          } else if (fail_cb) {
            fail_cb(data);
          } else {
            headLineAlertDisplay(data.header.msg + ' (' + data.header.resp_code + ')');
          }
        } else if (textStatus == 'parsererror'){
          //Need to go to login page
          window.location.href = "/";
        }
        else if (textStatus != 'abort') {
          switch(data.status) {
            case 503:
            case 504:
              headLineAlertDisplay("Request timeout. Please try again in a couple of minutes.");
              break;
            default:
              headLineAlertDisplay("Unknown System Error has occurred - " + textStatus);
          }

          if (fail_cb) {
            fail_cb(data);
          }
        }
      });
  return jqxhr;
};

/**
 * Checks to see if returned API response is a success, if not prints message
 * @param header
 * @param operation
 * @returns {boolean}
 */
var processResponseHeader = function(header) {
  switch(header.resp_code) {
    case 0:
      return true;
    default:
      return false;
  }
};


/**
 * Login Request
 */
var AuthBody = function(operation){
  this.operation = operation;
  this.username = null;
  this.password = null;
  this.jobId = null;
};
