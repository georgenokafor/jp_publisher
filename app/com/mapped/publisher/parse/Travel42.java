package com.mapped.publisher.parse;

/**
 * For the lack of better place it is in com.mapped.publisher.parse
 * @note Probably should be deleted.
 * Created by surge on 2015-01-14.
 */
public class Travel42 {
  private final static String T42_DOMAIN_URL = "travel-42.com";
  private final static String T42_DETAIL_URL = "travel-42.com/Client/View/detail.aspx";

  /**
   * Helper function to determine if the link is travel42.
   *
   * @param url - - user input - treat carefully
   * @return
   */
  public static boolean isTravel42(String url) {
    return url.contains(T42_DOMAIN_URL);
  }

  /**
   * Helper function to determine if the link is travel42 details page
   *
   * @param url - user input - treat carefully
   * @return
   */
  public static boolean isTravel42Cover(String url) {
    return !url.contains(T42_DETAIL_URL);
  }

}
