
package com.umapped.external.bing.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "id",
    "readLink",
    "webSearchUrl",
    "isFamilyFriendly",
    "value",
    "displayShoppingSourcesBadges",
    "displayRecipeSourcesBadges"
})
public class Images {

    @JsonProperty("id")
    private String id;
    @JsonProperty("readLink")
    private String readLink;
    @JsonProperty("webSearchUrl")
    private String webSearchUrl;
    @JsonProperty("isFamilyFriendly")
    private boolean isFamilyFriendly;
    @JsonProperty("value")
    private List<Value> value = new ArrayList<Value>();
    @JsonProperty("displayShoppingSourcesBadges")
    private boolean displayShoppingSourcesBadges;
    @JsonProperty("displayRecipeSourcesBadges")
    private boolean displayRecipeSourcesBadges;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The readLink
     */
    @JsonProperty("readLink")
    public String getReadLink() {
        return readLink;
    }

    /**
     * 
     * @param readLink
     *     The readLink
     */
    @JsonProperty("readLink")
    public void setReadLink(String readLink) {
        this.readLink = readLink;
    }

    /**
     * 
     * @return
     *     The webSearchUrl
     */
    @JsonProperty("webSearchUrl")
    public String getWebSearchUrl() {
        return webSearchUrl;
    }

    /**
     * 
     * @param webSearchUrl
     *     The webSearchUrl
     */
    @JsonProperty("webSearchUrl")
    public void setWebSearchUrl(String webSearchUrl) {
        this.webSearchUrl = webSearchUrl;
    }

    /**
     * 
     * @return
     *     The isFamilyFriendly
     */
    @JsonProperty("isFamilyFriendly")
    public boolean isIsFamilyFriendly() {
        return isFamilyFriendly;
    }

    /**
     * 
     * @param isFamilyFriendly
     *     The isFamilyFriendly
     */
    @JsonProperty("isFamilyFriendly")
    public void setIsFamilyFriendly(boolean isFamilyFriendly) {
        this.isFamilyFriendly = isFamilyFriendly;
    }

    /**
     * 
     * @return
     *     The value
     */
    @JsonProperty("value")
    public List<Value> getValue() {
        return value;
    }

    /**
     * 
     * @param value
     *     The value
     */
    @JsonProperty("value")
    public void setValue(List<Value> value) {
        this.value = value;
    }

    /**
     * 
     * @return
     *     The displayShoppingSourcesBadges
     */
    @JsonProperty("displayShoppingSourcesBadges")
    public boolean isDisplayShoppingSourcesBadges() {
        return displayShoppingSourcesBadges;
    }

    /**
     * 
     * @param displayShoppingSourcesBadges
     *     The displayShoppingSourcesBadges
     */
    @JsonProperty("displayShoppingSourcesBadges")
    public void setDisplayShoppingSourcesBadges(boolean displayShoppingSourcesBadges) {
        this.displayShoppingSourcesBadges = displayShoppingSourcesBadges;
    }

    /**
     * 
     * @return
     *     The displayRecipeSourcesBadges
     */
    @JsonProperty("displayRecipeSourcesBadges")
    public boolean isDisplayRecipeSourcesBadges() {
        return displayRecipeSourcesBadges;
    }

    /**
     * 
     * @param displayRecipeSourcesBadges
     *     The displayRecipeSourcesBadges
     */
    @JsonProperty("displayRecipeSourcesBadges")
    public void setDisplayRecipeSourcesBadges(boolean displayRecipeSourcesBadges) {
        this.displayRecipeSourcesBadges = displayRecipeSourcesBadges;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(readLink).append(webSearchUrl).append(isFamilyFriendly).append(value).append(displayShoppingSourcesBadges).append(displayRecipeSourcesBadges).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Images) == false) {
            return false;
        }
        Images rhs = ((Images) other);
        return new EqualsBuilder().append(id, rhs.id).append(readLink, rhs.readLink).append(webSearchUrl, rhs.webSearchUrl).append(isFamilyFriendly, rhs.isFamilyFriendly).append(value, rhs.value).append(displayShoppingSourcesBadges, rhs.displayShoppingSourcesBadges).append(displayRecipeSourcesBadges, rhs.displayRecipeSourcesBadges).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
