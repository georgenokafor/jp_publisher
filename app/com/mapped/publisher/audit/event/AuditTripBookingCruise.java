package com.mapped.publisher.audit.event;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.sql.Timestamp;

/**
 * Created by Serguei Moutovkin on 2014-05-21.
 */
@JsonTypeName("TRIP_BOOKING_CRUISE")
public class AuditTripBookingCruise
    extends AuditEvent {

  public String bookingId;
  public String name;
  public Timestamp depart;
  public Timestamp arrive;

  public AuditTripBookingCruise withBookingId(String id) {
    bookingId = id;
    return this;
  }

  public AuditTripBookingCruise withName(String name) {
    this.name = name;
    return this;
  }

  public AuditTripBookingCruise withDepartDT(long inDT) {
    this.depart = new Timestamp(inDT);
    return this;
  }

  public AuditTripBookingCruise withArriveDT(long outDT) {
    this.arrive = new Timestamp(outDT);
    return this;
  }
}
