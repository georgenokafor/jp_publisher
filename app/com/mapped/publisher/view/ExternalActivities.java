package com.mapped.publisher.view;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 15-06-09.
 */
public class ExternalActivities extends BaseView {
  public String bookingDetailsId;
  public String tripId;
  public String name;

  public List<ExternalActivity> activities;

  public void addActivity (ExternalActivity activity) {
    if (activities == null) {
      activities =  new ArrayList<>();

    }
    activities.add(activity);
  }
}
