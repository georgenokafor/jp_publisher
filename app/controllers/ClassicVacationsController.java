package controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.auth.oauth2.*;
import com.google.api.client.http.BasicAuthentication;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.*;
import com.mapped.publisher.parse.classicvacations.CVCredentials;
import com.mapped.publisher.parse.classicvacations.bookingDetails.BookingDetail;
import com.mapped.publisher.parse.classicvacations.bookingDetails.Detail;
import com.mapped.publisher.parse.classicvacations.bookingDetails.Name;
import com.mapped.publisher.parse.classicvacations.datesearch.Booking;
import com.mapped.publisher.parse.classicvacations.datesearch.SearchResults;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.persistence.VOModeller;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.ClassicVacationsDetails;
import com.mapped.publisher.view.TabType;
import com.umapped.persistence.enums.ReservationType;
import models.publisher.Account;
import models.publisher.Trip;
import models.publisher.TripNote;
import models.publisher.UserProfile;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by twong on 15-06-17.
 */
public class ClassicVacationsController
    extends Controller {

  @Inject
  FormFactory formFactory;

  private final static String AUTH_TEST_CLIENT_ID = "Y1BT5WNLLA";
  private final static String AUTH_DEV_CLIENT_ID  = "7NCJH1CUSX";
  private final static String AUTH_PRD_CLIENT_ID  = "AT9JK4WB8H";

  private final static String AUTH_TEST_SECRET = "54S8RWRL3O04E5M36FH3L0";
  private final static String AUTH_DEV_SECRET  = "NKZZMHAMAJFCHBNXSYRAE5";
  private final static String AUTH_PRD_SECRET  = "LLW31BJDNP1CH6Z931LCTC";


  private final static String AUTH_TEST_REDIRECT_URL = "http://test.umapped.com/classicvacations/auth";
  private final static String AUTH_PRD_REDIRECT_URL  = "http://trip.umapped.com/classicvacations/auth";


  private final static String AUTH_PRD_CODE_URL  = "https://connect.classicvacations" +
                                                   ".com/AuthorizationServer/OAuth/Authorize";
  private final static String AUTH_PRD_TOKEN_URL = "https://connect.classicvacations" +
                                                   ".com/AuthorizationServer/OAuth/Token";

  private final static String AUTH_TEST_CODE_URL  = "https://connect-test.classicvacations" +
                                                    ".com/AuthorizationServer/OAuth/Authorize";
  private final static String AUTH_TEST_TOKEN_URL = "https://connect-test.classicvacations" +
                                                    ".com/AuthorizationServer/OAuth/Token";

  private final static String AUTH_TEST_SEARCH_DATE     = "https://connect-test.classicvacations" +
                                                          ".com/ResourceServer/api/GetBookings/ByBookedDateRange/";
  private final static String AUTH_TEST_BOOKING_DETAILS = "https://connect-test.classicvacations" +
                                                          ".com/ResourceServer/api/GetBookingDetails/";
  private final static String AUTH_PRD_SEARCH_DATE      = "https://connect.classicvacations" +
                                                          ".com/ResourceServer/api/GetBookings/ByBookedDateRange/";
  private final static String AUTH_PRD_BOOKING_DETAILS  = "https://connect.classicvacations" +
                                                          ".com/ResourceServer/api/GetBookingDetails/";


  private final static String AUTH_TOKEN_URL    = AUTH_PRD_TOKEN_URL;
  private final static String AUTH_CODE_URL     = AUTH_PRD_CODE_URL;
  private final static String AUTH_CLIENT_ID    = AUTH_PRD_CLIENT_ID;
  private final static String AUTH_SECRET       = AUTH_PRD_SECRET;
  private final static String AUTH_REDIRECT_URL = AUTH_PRD_REDIRECT_URL;

  private final static String AUTH_SEARCH_DATE     = AUTH_PRD_SEARCH_DATE;
  private final static String AUTH_BOOKING_DETAILS = AUTH_PRD_BOOKING_DETAILS;


  private final static String[]  DATE_TIME_FORMATS = {"MM/dd/yyyy", "MM/dd/yyyy hh:mm:ss a", "MM/dd/yyyy h:mm:ss a",
                                                      "MM/dd/yyyy h:mma"};
  private final static DateUtils dateUtils         = new DateUtils();

  public Result test() {
    //String s = "{\"Name\":[{\"Title\":\"Mr.\",\"FirstName\":\"MICHAEL\",\"LastName\":\"NATHAN\",
    // \"MiddleName\":\"\"},{\"Title\":\"Mrs.\",\"FirstName\":\"ALLISON\",\"LastName\":\"NATHAN\",
    // \"MiddleName\":\"\"},{\"Title\":\"Miss.\",\"FirstName\":\"JESSICA\",\"LastName\":\"NATHAN\",
    // \"MiddleName\":\"\"},{\"Title\":\"Mstr.\",\"FirstName\":\"JAKE\",\"LastName\":\"NATHAN\",
    // \"MiddleName\":\"\"}],\"Detail\":[{\"BeginDate\":\"2/13/2016 11:50:00 AM\",\"BeginTime\":\"11:50:00:000\",
    // \"VendorID\":\"US\",\"ProductID\":\"1671\",\"PartyName\":\"NATHAN, Mr. M. & Party\",\"ArrivalLoc\":\"CUN\",
    // \"UseLoc\":\"JFK\",\"EndDate\":\"2/13/2016 3:43:00 PM\",\"EndTime\":\"15:43:00:000\",
    // \"VendorConf\":\"CRVXYV\",\"VendorName\":\"US Airways\",\"ProductName\":\"\",\"ItemKey\":\"FI9603587396\",
    // \"InternalComments\":\"\",\"FlightPassengers\":\"  NATHAN MICHAEL |FFNo:,  NATHAN ALLISON |FFNo:,  NATHAN
    // JESSICA |FFNo:,  NATHAN JAKE |FFNo:\",\"FlightSeatMapping\":\"NATHAN MICHAEL:22A|NATHAN ALLISON:22D|NATHAN
    // JESSICA:22C|NATHAN JAKE:22F\",\"FlightStartAirportCity\":\"John F Kennedy Intl, New York, USA\",
    // \"FlightEndAirportCity\":\"Cancun International Airport, Cancun, Mexico\",
    // \"FlightOperatingAirlines\":\"American Airlines\",\"FlightDuration\":\"03:53:00\",\"FlightClass\":\"Economy\",
    // \"HotelPassengers\":\"\",\"HotelConditions\":\"\",\"HotelAmenities\":\"\",\"HotelNoOfGuests\":null,
    // \"HotelBedding\":null,\"DurationHours\":null,\"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",
    // \"DetailType\":\"FLIGHT\",\"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this for
    // the details - http://www.classicvacations
    // .com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www.classicvacations
    // .com/about-us/2015-travel-smart-plan\",\"VendorInfo\":null},{\"BeginDate\":\"2/20/2016 2:20:00 PM\",
    // \"BeginTime\":\"14:20:00:000\",\"VendorID\":\"US\",\"ProductID\":\"1327\",\"PartyName\":\"NATHAN, Mr. M. &
    // Party\",\"ArrivalLoc\":\"JFK\",\"UseLoc\":\"CUN\",\"EndDate\":\"2/20/2016 6:29:00 PM\",
    // \"EndTime\":\"18:29:00:000\",\"VendorConf\":\"CRVXYV\",\"VendorName\":\"US Airways\",\"ProductName\":\"\",
    // \"ItemKey\":\"FI9603587396\",\"InternalComments\":\"\",\"FlightPassengers\":\"  NATHAN MICHAEL |FFNo:,  NATHAN
    // ALLISON |FFNo:,  NATHAN JESSICA |FFNo:,  NATHAN JAKE |FFNo:\",\"FlightSeatMapping\":\"NATHAN
    // MICHAEL:29A|NATHAN ALLISON:29B|NATHAN JESSICA:29C|NATHAN JAKE:29D\",\"FlightStartAirportCity\":\"Cancun
    // International Airport, Cancun, Mexico\",\"FlightEndAirportCity\":\"John F Kennedy Intl, New York, USA\",
    // \"FlightOperatingAirlines\":\"American Airlines\",\"FlightDuration\":\"04:09:00\",\"FlightClass\":\"Economy\",
    // \"HotelPassengers\":\"\",\"HotelConditions\":\"\",\"HotelAmenities\":\"\",\"HotelNoOfGuests\":null,
    // \"HotelBedding\":null,\"DurationHours\":null,\"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",
    // \"DetailType\":\"FLIGHT\",\"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this for
    // the details - http://www.classicvacations
    // .com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www.classicvacations
    // .com/about-us/2015-travel-smart-plan\",\"VendorInfo\":null},{\"BeginDate\":\"2/13/2016 12:00:00 AM\",
    // \"BeginTime\":\"00:00:00:000\",\"VendorID\":\"VH01431\",\"ProductID\":\"R125ABSS2\",\"PartyName\":\"NATHAN, Mr
    // . M. & Party\",\"ArrivalLoc\":\"\",\"UseLoc\":\"CUN\",\"EndDate\":\"2/20/2016 12:00:00 AM\",
    // \"EndTime\":\"00:00:00:000\",\"VendorConf\":\"I-   97550-     1\",\"VendorName\":\"Grand Velas Riviera Maya\",
    // \"ProductName\":\"Ambassador Suite\",\"ItemKey\":\"HI9467169477\",\"InternalComments\":\"\",
    // \"FlightPassengers\":\"\",\"FlightSeatMapping\":\"\",\"FlightStartAirportCity\":\"\",
    // \"FlightEndAirportCity\":\"\",\"FlightOperatingAirlines\":null,\"FlightDuration\":null,\"FlightClass\":null,
    // \"HotelPassengers\":\"  Mr. MICHAEL  NATHAN,  Mrs. ALLISON  NATHAN,  Miss. JESSICA  NATHAN,  Mstr. JAKE
    // NATHAN\",\"HotelConditions\":\" Check-in time is 3:00pm and check-out time is 12:00pm noon.  No shows and
    // cancellations received within 11 days of arrival will be charged a 3 night penalty.  Early departures will be
    // charged a 1 night penalty.  Late check-outs can be requested and will be 50% of the day room rates plus taxes
    // for up to 4 hours. If more than 4 hours is needed, the full rate will apply.  Note: Hotel Policy does NOT
    // allow split bookings of a higher category in the beginning of the stay and changing to a lower category for
    // the end of the stay.\",\"HotelAmenities\":\"  Air Conditioning,  Child Program,  Internet Access,  Pool\",
    // \"HotelNoOfGuests\":\"4\",\"HotelBedding\":\"1 King Bed or 2 Queen Beds \",\"DurationHours\":null,
    // \"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",\"DetailType\":\"HOTEL\",
    // \"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this for the details - http://www
    // .classicvacations.com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www
    // .classicvacations.com/about-us/2015-travel-smart-plan\",\"VendorInfo\":{\"Address\":\"Carretera Cancun Tulum
    // Km. 62  Riviera Maya 77710   Mexico\",\"ContactInfo\":\"011 52 98 4877 4400\",
    // \"WebSiteAddress\":\"http://rivieramaya.grandvelas.com/\"}},{\"BeginDate\":\"2/13/2016 12:00:00 AM\",
    // \"BeginTime\":\"00:00:00.000\",\"VendorID\":\"VITRIPM\",\"ProductID\":\"WVRN\",\"PartyName\":\"NATHAN, Mr. M.
    // & Party\",\"ArrivalLoc\":\"\",\"UseLoc\":\"\",\"EndDate\":\"2/13/2016 12:00:00 AM\",
    // \"EndTime\":\"00:00:00.000\",\"VendorConf\":\"\",\"VendorName\":\"Trip Mate\",\"ProductName\":\"Travel Smart
    // Plan II ( TSP )\",\"ItemKey\":\"S9603599053\",\"InternalComments\":\"\",\"FlightPassengers\":\"\",
    // \"FlightSeatMapping\":\"\",\"FlightStartAirportCity\":\"\",\"FlightEndAirportCity\":\"\",
    // \"FlightOperatingAirlines\":null,\"FlightDuration\":null,\"FlightClass\":null,\"HotelPassengers\":\"\",
    // \"HotelConditions\":\"\",\"HotelAmenities\":\"\",\"HotelNoOfGuests\":null,\"HotelBedding\":null,
    // \"DurationHours\":null,\"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",
    // \"DetailType\":\"TSP_WAIVER\",\"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this
    // for the details - http://www.classicvacations
    // .com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www.classicvacations
    // .com/about-us/2015-travel-smart-plan\",\"VendorInfo\":{\"Address\":null,\"ContactInfo\":null,
    // \"WebSiteAddress\":\"http://www.classicvacations.com/about-us/2015-travel-smart-plan\"}}],
    // \"Payment\":{\"SellAmount\":15977.24,\"CommAmount\":1695.30,\"TaxAmount\":459.52,\"AmountBeforeTax\":15517.72,
    // \"DepAmountDue\":5577.52,\"AmountPosted\":5577.52,\"CurrAmountDue\":10399.72,\"DueDate\":\"7/13/2015 11:59:00
    // PM\"},\"Exception\":null,\"Destination\":\"Mexico\",\"PartyName\":\"NATHAN, Mr. M. & Party\",\"Adults\":2,
    // \"Children\":2,\"TimeStamp\":\"7/14/2015 6:33:47 AM\"}";

    //String s = "{\"Name\":[{\"Title\":\"Mr.\",\"FirstName\":\"THOMAS\",\"LastName\":\"BURNARD\",
    // \"MiddleName\":\"CARL\"},{\"Title\":\"Mrs.\",\"FirstName\":\"JANET\",\"LastName\":\"BURNARD\",
    // \"MiddleName\":\"ELAINE\"},{\"Title\":\"Miss.\",\"FirstName\":\"JACQUELINE\",\"LastName\":\"BURNARD\",
    // \"MiddleName\":\"ELAINE\"},{\"Title\":\"Miss.\",\"FirstName\":\"VICTORIA\",\"LastName\":\"BURNARD\",
    // \"MiddleName\":\"GRACE\"}],\"Detail\":[{\"BeginDate\":\"6/29/2016 10:15:00 PM\",
    // \"BeginTime\":\"22:15:00:000\",\"VendorID\":\"AA\",\"ProductID\":\"1930\",\"PartyName\":\"BURNARD, Mr. T. &
    // Party\",\"ArrivalLoc\":\"CLT\",\"UseLoc\":\"PDX\",\"EndDate\":\"6/30/2016 6:01:00 AM\",
    // \"EndTime\":\"06:01:00:000\",\"VendorConf\":\"VXXAPB\",\"VendorName\":\"American Airlines\",
    // \"ProductName\":\"\",\"ItemKey\":\"FI9837498897\",\"InternalComments\":\"\",\"FlightPassengers\":\" BURNARD
    // THOMAS CARL|FFNo:, BURNARD JANET ELAINE|FFNo:, BURNARD JACQUELINE ELAINE|FFNo:, BURNARD VICTORIA
    // GRACE|FFNo:\",\"FlightSeatMapping\":\"BURNARD THOMAS CARL:02A|BURNARD JANET ELAINE:02C|BURNARD JACQUELINE
    // ELAINE:02D|BURNARD VICTORIA GRACE:02F\",\"FlightStartAirportCity\":\"Portland International Airport, Portland,
    // USA\",\"FlightEndAirportCity\":\"Charlotte, Charlotte, USA\",\"FlightOperatingAirlines\":\"American
    // Airlines\",\"FlightDuration\":\"04:46:00\",\"FlightClass\":\"First\",\"HotelPassengers\":\"\",
    // \"HotelConditions\":\"\",\"HotelAmenities\":\"\",\"HotelNoOfGuests\":null,\"HotelBedding\":\"\",
    // \"DurationHours\":null,\"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",
    // \"DetailType\":\"FLIGHT\",\"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this for
    // the details - http://www.classicvacations
    // .com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www.classicvacations
    // .com/about-us/2015-travel-smart-plan\",\"VendorInfo\":null},{\"BeginDate\":\"6/30/2016 9:45:00 AM\",
    // \"BeginTime\":\"09:45:00:000\",\"VendorID\":\"AA\",\"ProductID\":\"893\",\"PartyName\":\"BURNARD, Mr. T. &
    // Party\",\"ArrivalLoc\":\"NAS\",\"UseLoc\":\"CLT\",\"EndDate\":\"6/30/2016 12:03:00 PM\",
    // \"EndTime\":\"12:03:00:000\",\"VendorConf\":\"VXXAPB\",\"VendorName\":\"American Airlines\",
    // \"ProductName\":\"\",\"ItemKey\":\"FI9837498897\",\"InternalComments\":\"\",\"FlightPassengers\":\" BURNARD
    // THOMAS CARL|FFNo:, BURNARD JANET ELAINE|FFNo:, BURNARD JACQUELINE ELAINE|FFNo:, BURNARD VICTORIA
    // GRACE|FFNo:\",\"FlightSeatMapping\":\"BURNARD THOMAS CARL:02A|BURNARD JANET ELAINE:02C|BURNARD JACQUELINE
    // ELAINE:02D|BURNARD VICTORIA GRACE:02F\",\"FlightStartAirportCity\":\"Charlotte, Charlotte, USA\",
    // \"FlightEndAirportCity\":\"Nassau International Airport, Nassau, Bahamas\",
    // \"FlightOperatingAirlines\":\"American Airlines\",\"FlightDuration\":\"02:18:00\",
    // \"FlightClass\":\"Business\",\"HotelPassengers\":\"\",\"HotelConditions\":\"\",\"HotelAmenities\":\"\",
    // \"HotelNoOfGuests\":null,\"HotelBedding\":\"\",\"DurationHours\":null,\"TourTime\":\"\",\"Conditions\":\"\",
    // \"DriverPickUpLoc\":\"\",\"DetailType\":\"FLIGHT\",\"Payment\":\"DepositRecieved\",
    // \"CancellationPolicy\":\"Please refer to this for the details - http://www.classicvacations
    // .com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www.classicvacations
    // .com/about-us/2015-travel-smart-plan\",\"VendorInfo\":null},{\"BeginDate\":\"7/9/2016 4:30:00 PM\",
    // \"BeginTime\":\"16:30:00:000\",\"VendorID\":\"AA\",\"ProductID\":\"1762\",\"PartyName\":\"BURNARD, Mr. T. &
    // Party\",\"ArrivalLoc\":\"CLT\",\"UseLoc\":\"NAS\",\"EndDate\":\"7/9/2016 6:45:00 PM\",
    // \"EndTime\":\"18:45:00:000\",\"VendorConf\":\"VXXAPB\",\"VendorName\":\"American Airlines\",
    // \"ProductName\":\"\",\"ItemKey\":\"FI9837498897\",\"InternalComments\":\"\",\"FlightPassengers\":\" BURNARD
    // THOMAS CARL|FFNo:, BURNARD JANET ELAINE|FFNo:, BURNARD JACQUELINE ELAINE|FFNo:, BURNARD VICTORIA
    // GRACE|FFNo:\",\"FlightSeatMapping\":\"BURNARD THOMAS CARL:01A|BURNARD JANET ELAINE:01C|BURNARD JACQUELINE
    // ELAINE:01D|BURNARD VICTORIA GRACE:01F\",\"FlightStartAirportCity\":\"Nassau International Airport, Nassau,
    // Bahamas\",\"FlightEndAirportCity\":\"Charlotte, Charlotte, USA\",\"FlightOperatingAirlines\":\"American
    // Airlines\",\"FlightDuration\":\"02:15:00\",\"FlightClass\":\"Business\",\"HotelPassengers\":\"\",
    // \"HotelConditions\":\"\",\"HotelAmenities\":\"\",\"HotelNoOfGuests\":null,\"HotelBedding\":\"\",
    // \"DurationHours\":null,\"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",
    // \"DetailType\":\"FLIGHT\",\"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this for
    // the details - http://www.classicvacations
    // .com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www.classicvacations
    // .com/about-us/2015-travel-smart-plan\",\"VendorInfo\":null},{\"BeginDate\":\"7/9/2016 8:05:00 PM\",
    // \"BeginTime\":\"20:05:00:000\",\"VendorID\":\"AA\",\"ProductID\":\"1788\",\"PartyName\":\"BURNARD, Mr. T. &
    // Party\",\"ArrivalLoc\":\"PDX\",\"UseLoc\":\"CLT\",\"EndDate\":\"7/9/2016 10:27:00 PM\",
    // \"EndTime\":\"22:27:00:000\",\"VendorConf\":\"VXXAPB\",\"VendorName\":\"American Airlines\",
    // \"ProductName\":\"\",\"ItemKey\":\"FI9837498897\",\"InternalComments\":\"\",\"FlightPassengers\":\" BURNARD
    // THOMAS CARL|FFNo:, BURNARD JANET ELAINE|FFNo:, BURNARD JACQUELINE ELAINE|FFNo:, BURNARD VICTORIA
    // GRACE|FFNo:\",\"FlightSeatMapping\":\"BURNARD THOMAS CARL:01A|BURNARD JANET ELAINE:01C|BURNARD JACQUELINE
    // ELAINE:01D|BURNARD VICTORIA GRACE:01F\",\"FlightStartAirportCity\":\"Charlotte, Charlotte, USA\",
    // \"FlightEndAirportCity\":\"Portland International Airport, Portland, USA\",
    // \"FlightOperatingAirlines\":\"American Airlines\",\"FlightDuration\":\"05:22:00\",\"FlightClass\":\"First\",
    // \"HotelPassengers\":\"\",\"HotelConditions\":\"\",\"HotelAmenities\":\"\",\"HotelNoOfGuests\":null,
    // \"HotelBedding\":\"\",\"DurationHours\":null,\"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",
    // \"DetailType\":\"FLIGHT\",\"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this for
    // the details - http://www.classicvacations
    // .com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www.classicvacations
    // .com/about-us/2015-travel-smart-plan\",\"VendorInfo\":null},{\"BeginDate\":\"6/30/2016 12:00:00 AM\",
    // \"BeginTime\":\"00:00:00:000\",\"VendorID\":\"VH00506\",\"ProductID\":\"BAR2CLBSOCNDLX\",
    // \"PartyName\":\"BURNARD, Mr. T. & Party\",\"ArrivalLoc\":\"\",\"UseLoc\":\"NAS\",\"EndDate\":\"7/9/2016
    // 12:00:00 AM\",\"EndTime\":\"00:00:00:000\",\"VendorConf\":\"KP5dM7DQvZ|6263206HI9840559055\",
    // \"VendorName\":\"The Cove Atlantis\",\"ProductName\":\"The Club Deluxe Ocean Suite\",
    // \"ItemKey\":\"HI9840559055\",\"InternalComments\":\"King with Sofa Sleeper\",\"FlightPassengers\":\"\",
    // \"FlightSeatMapping\":\"\",\"FlightStartAirportCity\":\"\",\"FlightEndAirportCity\":\"\",
    // \"FlightOperatingAirlines\":null,\"FlightDuration\":null,\"FlightClass\":null,\"HotelPassengers\":\" Mr.
    // THOMAS BURNARD, Mrs. JANET BURNARD, Miss. JACQUELINE BURNARD, Miss. VICTORIA BURNARD\",\"HotelConditions\":\"
    // Check in time is 3:00 pm and Check out time is 11:00 am. Cancellations 15 days or less, prior to arrival will
    // result in a 2 night's charge. A late-check out fee of $150 per hour, per room for check out after 11:00 am.
    // No-shows will be charged for the entire stay. $250 per room per day will be held for incidentals upon check-in
    // via credit card or cash deposit. All rooms booked must contain 1 adult at least 18 years of age. Early
    // departures will incur a penalty equal to the full-length of the stay. All rates include mandatory resort fees
    // and gratuities.\",\"HotelAmenities\":\" Air conditioning, Child Program, Internet Access, Pool\",
    // \"HotelNoOfGuests\":\"4\",\"HotelBedding\":\"1 King Bed or 2 Queen Beds \",\"DurationHours\":null,
    // \"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",\"DetailType\":\"HOTEL\",
    // \"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this for the details - http://www
    // .classicvacations.com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www
    // .classicvacations.com/about-us/2015-travel-smart-plan\",\"VendorInfo\":{\"Address\":\"One Casino Drive
    // Paradise Island Paradise Island Bahamas\",\"ContactInfo\":\"1 (242) 363 6900\",\"WebSiteAddress\":\"http://www
    // .thecoveatlantis.com\"}},{\"BeginDate\":\"6/30/2016 12:00:00 AM\",\"BeginTime\":\"00:00:00:000\",
    // \"VendorID\":\"VE00823\",\"ProductID\":\"NEW13\",\"PartyName\":\"BURNARD, Mr. T. & Party\",
    // \"ArrivalLoc\":\"\",\"UseLoc\":\"NAS\",\"EndDate\":\"6/30/2016 12:00:00 AM\",\"EndTime\":\"00:00:00.000\",
    // \"VendorConf\":\"\",\"VendorName\":\"Majestic\",\"ProductName\":\"Arrival Transfers - One Way Private SUV
    // transfer from NAS to Paradise Island Hotels (1-6 pax)\",\"ItemKey\":\"DI9841570749\",
    // \"InternalComments\":\"\",\"FlightPassengers\":\"\",\"FlightSeatMapping\":\"\",
    // \"FlightStartAirportCity\":\"\",\"FlightEndAirportCity\":\"\",\"FlightOperatingAirlines\":null,
    // \"FlightDuration\":null,\"FlightClass\":null,\"HotelPassengers\":\"\",\"HotelConditions\":\"\",
    // \"HotelAmenities\":\"\",\"HotelNoOfGuests\":null,\"HotelBedding\":\"\",\"DurationHours\":null,
    // \"TourTime\":\"\",\"Conditions\":\"| This transfer is provided by Majestic Tours.|| For this One Way Private
    // SUV transfer, guest will be picked up from Nassau International Airport and dropped off at hotels in Paradise
    // Island. Notes: If the flight is delayed more than 30 minutes beyond the scheduled time of arrival our hourly
    // rate would come into effect and will be in addition to the transfer rate.|| Please call ( 1 ) 242 322 2606 or
    // see Majestic Tours Desk at hotel at least 24 hours prior to departure or tour start to reconfirm; non
    // refundable if cancelled within 48 hours.\",\"DriverPickUpLoc\":\"\",\"DetailType\":\"TRANSFERS\",
    // \"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this for the details - http://www
    // .classicvacations.com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www
    // .classicvacations.com/about-us/2015-travel-smart-plan\",\"VendorInfo\":{\"Address\":\"Hillside Manor
    // Cumberland St. PO Box N-1401 Nassau Bahamas Bahamas\",\"ContactInfo\":null,\"WebSiteAddress\":null}},
    // {\"BeginDate\":\"7/9/2016 12:00:00 AM\",\"BeginTime\":\"00:00:00:000\",\"VendorID\":\"VE00823\",
    // \"ProductID\":\"NEW14\",\"PartyName\":\"BURNARD, Mr. T. & Party\",\"ArrivalLoc\":\"\",\"UseLoc\":\"NAS\",
    // \"EndDate\":\"7/9/2016 12:00:00 AM\",\"EndTime\":\"00:00:00.000\",\"VendorConf\":\"\",
    // \"VendorName\":\"Majestic\",\"ProductName\":\"Departure Transfers - One Way Private SUV transfer from Paradise
    // Island Hotels to NAS (1-6 pax)\",\"ItemKey\":\"DI9837515050\",\"InternalComments\":\"\",
    // \"FlightPassengers\":\"\",\"FlightSeatMapping\":\"\",\"FlightStartAirportCit- - y\":\"\",
    // \"FlightEndAirportCity\":\"\",\"FlightOperatingAirlines\":null,\"FlightDuration\":null,\"FlightClass\":null,
    // \"HotelPassengers\":\"\",\"HotelConditions\":\"\",\"HotelAmenities\":\"\",\"HotelNoOfGuests\":null,
    // \"HotelBedding\":\"\",\"DurationHours\":null,\"TourTime\":\"\",\"Conditions\":\"| This transfer is provided by
    // Majestic Tours.|| For this One Way Private SUV transfer, guest will be picked up from hotels in Paradise
    // Island and dropped off at Nassau International Airport.|| Please call ( 1 ) 242 322 2606 or see Majestic Tours
    // Desk at hotel at least 24 hours prior to departure or tour start to reconfirm; non refundable if cancelled
    // within 48 hours.\",\"DriverPickUpLoc\":\"\",\"DetailType\":\"TRANSFERS\",\"Payment\":\"DepositRecieved\",
    // \"CancellationPolicy\":\"Please refer to this for the details - http://www.classicvacations
    // .com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www.classicvacations
    // .com/about-us/2015-travel-smart-plan\",\"VendorInfo\":{\"Address\":\"Hillside Manor Cumberland St. PO Box
    // N-1401 Nassau Bahamas Bahamas\",\"ContactInfo\":null,\"WebSiteAddress\":null}},{\"BeginDate\":\"6/30/2016
    // 12:00:00 AM\",\"BeginTime\":\"00:00:00.000\",\"VendorID\":\"VITRIPM\",\"ProductID\":\"WVRN\",
    // \"PartyName\":\"BURNARD, Mr. T. & Party\",\"ArrivalLoc\":\"\",\"UseLoc\":\"\",\"EndDate\":\"6/30/2016 12:00:00
    // AM\",\"EndTime\":\"00:00:00.000\",\"VendorConf\":\"\",\"VendorName\":\"Trip Mate\",\"ProductName\":\"Travel
    // Smart Plan II ( TSP )\",\"ItemKey\":\"S9837012080\",\"InternalComments\":\"\",\"FlightPassengers\":\"\",
    // \"FlightSeatMapping\":\"\",\"FlightStartAirportCity\":\"\",\"FlightEndAirportCity\":\"\",
    // \"FlightOperatingAirlines\":null,\"FlightDuration\":null,\"FlightClass\":null,\"HotelPassengers\":\"\",
    // \"HotelConditions\":\"\",\"HotelAmenities\":\"\",\"HotelNoOfGuests\":null,\"HotelBedding\":\"\",
    // \"DurationHours\":null,\"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",
    // \"DetailType\":\"TSP_WAIVER\",\"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this
    // for the details - http://www.classicvacations
    // .com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www.classicvacations
    // .com/about-us/2015-travel-smart-plan\",\"VendorInfo\":{\"Address\":null,\"ContactInfo\":null,
    // \"WebSiteAddress\":\"http://www.classicvacations.com/about-us/2015-travel-smart-plan\"}}],
    // \"Payment\":{\"SellAmount\":15580.00,\"CommAmount\":1724.93,\"TaxAmount\":3316.33,
    // \"AmountBeforeTax\":12263.67,\"DepAmountDue\":4322.32,\"AmountPosted\":4324.00,\"CurrAmountDue\":11256.00,
    // \"DueDate\":\"9/25/2015 12:00:00 AM\"},\"Exception\":null,\"Destination\":\"Caribbean\",
    // \"PartyName\":\"BURNARD, Mr. T. & Party\",\"Adults\":2,\"Children\":2,\"TimeStamp\":\"11/6/2015 1:06:47
    // PM\"}\n" ;

    // String s = "{\"Name\":[{\"Title\":\"Mr.\",\"FirstName\":\"MICHAEL\",\"LastName\":\"DEDONKER\",
    // \"MiddleName\":\"\"},{\"Title\":\"Mrs.\",\"FirstName\":\"CANDACE\",\"LastName\":\"DEDONKER\",
    // \"MiddleName\":\"\"},{\"Title\":\"Mr.\",\"FirstName\":\"ANDREW\",\"LastName\":\"DEDONKER\",
    // \"MiddleName\":\"\"},{\"Title\":\"Mrs.\",\"FirstName\":\"MONIQUE\",\"LastName\":\"DEDONKER\",
    // \"MiddleName\":\"\"},{\"Title\":\"Mr.\",\"FirstName\":\"ALEX\",\"LastName\":\"DEDONKER\",
    // \"MiddleName\":\"\"}],\"Detail\":[{\"BeginDate\":\"12/17/2015 12:00:00 AM\",\"BeginTime\":\"00:00:00:000\",
    // \"VendorID\":\"VH01162\",\"ProductID\":\"HNY1\",\"PartyName\":\"DEDONKER, Mr. M. & Party\",
    // \"ArrivalLoc\":\"\",\"UseLoc\":\"CUN\",\"EndDate\":\"12/26/2015 12:00:00 AM\",\"EndTime\":\"00:00:00:000\",
    // \"VendorConf\":\"ZHLB7491958\",\"VendorName\":\"Le Blanc Spa Resort\",\"ProductName\":\"Royale Honeymoon Ocean
    // Front Room\",\"ItemKey\":\"HI9911072752\",\"InternalComments\":\"\",\"FlightPassengers\":\"\",
    // \"FlightSeatMapping\":\"\",\"FlightStartAirportCity\":\"\",\"FlightEndAirportCity\":\"\",
    // \"FlightOperatingAirlines\":null,\"FlightDuration\":null,\"FlightClass\":null,\"HotelPassengers\":\" Mr. ALEX
    // DEDONKER\",\"HotelConditions\":\" Check-in time is 3:00 pm and Check-out time is 12:00 Noon Cancellations
    // received within 15 days of arrival are subject to a 3 night penalty No show is non-refundable Resort is adults
    // only A 5 nights minumum stay is required Minimum night stay is subject to change without prior notice and will
    // not be covered by the Waiver or Travel Smart Plan for early departures\",\"HotelAmenities\":\" Air
    // Conditioning, Internet Access, Pool\",\"HotelNoOfGuests\":\"1\",\"HotelBedding\":\"1 King Bed \",
    // \"DurationHours\":null,\"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",
    // \"DetailType\":\"HOTEL\",\"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this for the
    // details - http://www.classicvacations.com/about-us/classic-vacations-price-protection-and-change-cancellation
    // -waiver http://www.classicvacations.com/about-us/2015-travel-smart-plan\",\"VendorInfo\":{\"Address\":\"Blvd
    // Kukulcan KM-10 Zona Hotelera Cancun 77500 Mexico\",\"ContactInfo\":\"011 52 998 881 4740\",
    // \"WebSiteAddress\":\"http://www.leblancsparesort.com/\"}},{\"BeginDate\":\"12/17/2015 12:00:00 AM\",
    // \"BeginTime\":\"00:00:00:000\",\"VendorID\":\"VH01162\",\"ProductID\":\"HNY2\",\"PartyName\":\"DEDONKER, Mr. M
    // . & Party\",\"ArrivalLoc\":\"\",\"UseLoc\":\"CUN\",\"EndDate\":\"12/26/2015 12:00:00 AM\",
    // \"EndTime\":\"00:00:00:000\",\"VendorConf\":\"ZHLB7491959\",\"VendorName\":\"Le Blanc Spa Resort\",
    // \"ProductName\":\"Royale Honeymoon Ocean Front Room\",\"ItemKey\":\"HI9911072755\",\"InternalComments\":\"\",
    // \"FlightPassengers\":\"\",\"FlightSeatMapping\":\"\",\"FlightStartAirportCity\":\"\",
    // \"FlightEndAirportCity\":\"\",\"FlightOperatingAirlines\":null,\"FlightDuration\":null,\"FlightClass\":null,
    // \"HotelPassengers\":\" Mr. MICHAEL DEDONKER, Mrs. CANDACE DEDONKER\",\"HotelConditions\":\" Check-in time is
    // 3:00 pm and Check-out time is 12:00 Noon Cancellations received within 15 days of arrival are subject to a 3
    // night penalty No show is non-refundable Resort is adults only A 5 nights minumum stay is required Minimum
    // night stay is subject to change without prior notice and will not be covered by the Waiver or Travel Smart
    // Plan for early departures\",\"HotelAmenities\":\" Air Conditioning, Internet Access, Pool\",
    // \"HotelNoOfGuests\":\"2\",\"HotelBedding\":\"1 King Bed \",\"DurationHours\":null,\"TourTime\":\"\",
    // \"Conditions\":\"\",\"DriverPickUpLoc\":\"\",\"DetailType\":\"HOTEL\",\"Payment\":\"DepositRecieved\",
    // \"CancellationPolicy\":\"Please refer to this for the details - http://www.classicvacations
    // .com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www.classicvacations
    // .com/about-us/2015-travel-smart-plan\",\"VendorInfo\":{\"Address\":\"Blvd Kukulcan KM-10 Zona Hotelera Cancun
    // 77500 Mexico\",\"ContactInfo\":\"011 52 998 881 4740\",\"WebSiteAddress\":\"http://www.leblancsparesort
    // .com/\"}},{\"BeginDate\":\"12/17/2015 12:00:00 AM\",\"BeginTime\":\"00:00:00:000\",\"VendorID\":\"VH01162\",
    // \"ProductID\":\"HNY2\",\"PartyName\":\"DEDONKER, Mr. M. & Party\",\"ArrivalLoc\":\"\",\"UseLoc\":\"CUN\",
    // \"EndDate\":\"12/26/2015 12:00:00 AM\",\"EndTime\":\"00:00:00:000\",\"VendorConf\":\"ZHLB7491962\",
    // \"VendorName\":\"Le Blanc Spa Resort\",\"ProductName\":\"Royale Honeymoon Ocean Front Room\",
    // \"ItemKey\":\"HI9911072758\",\"InternalComments\":\"\",\"FlightPassengers\":\"\",\"FlightSeatMapping\":\"\",
    // \"FlightStartAirportCity\":\"\",\"FlightEndAirportCity\":\"\",\"FlightOperatingAirlines\":null,
    // \"FlightDuration\":null,\"FlightClass\":null,\"HotelPassengers\":\" Mr. ANDREW DEDONKER, Mrs. MONIQUE
    // DEDONKER\",\"HotelConditions\":\" Check-in time is 3:00 pm and Check-out time is 12:00 Noon Cancellations
    // received within 15 days of arrival are subject to a 3 night penalty No show is non-refundable Resort is adults
    // only A 5 nights minumum stay is required Minimum night stay is subject to change without prior notice and will
    // not be covered by the Waiver or Travel Smart Plan for early departures\",\"HotelAmenities\":\" Air
    // Conditioning, Internet Access, Pool\",\"HotelNoOfGuests\":\"2\",\"HotelBedding\":\"1 King Bed \",
    // \"DurationHours\":null,\"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",
    // \"DetailType\":\"HOTEL\",\"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this for the
    // details - http://www.classicvacations.com/about-us/classic-vacations-price-protection-and-change-cancellation
    // -waiver http://www.classicvacations.com/about-us/2015-travel-smart-plan\",\"VendorInfo\":{\"Address\":\"Blvd
    // Kukulcan KM-10 Zona Hotelera Cancun 77500 Mexico\",\"ContactInfo\":\"011 52 998 881 4740\",
    // \"WebSiteAddress\":\"http://www.leblancsparesort.com/\"}},{\"BeginDate\":\"12/17/2015 12:00:00 AM\",
    // \"BeginTime\":\"00:00:00:000\",\"VendorID\":\"VE00747\",\"ProductID\":\"NT103\",\"PartyName\":\"DEDONKER, Mr.
    // M. & Party\",\"ArrivalLoc\":\"\",\"UseLoc\":\"CUN\",\"EndDate\":\"12/17/2015 12:00:00 AM\",
    // \"EndTime\":\"00:00:00.000\",\"VendorConf\":\"AAAC1041\",\"VendorName\":\"Gray Line Blue Diamond - Mayaland\",
    // \"ProductName\":\"Arrival Transfers - Roundtrip Standard Private Transfer from CUN to Cancun Hotels\",
    // \"ItemKey\":\"DI9911077532\",\"InternalComments\":\"Clients will arrive on AS 208 at 4:54pm and will depart on
    // December 26 on AS 209 at 5:54pm.\",\"FlightPassengers\":\"\",\"FlightSeatMapping\":\"\",
    // \"FlightStartAirportCity\":\"\",\"FlightEndAirportCity\":\"\",\"FlightOperatingAirlines\":null,
    // \"FlightDuration\":null,\"FlightClass\":null,\"HotelPassengers\":\"\",\"HotelConditions\":\"\",
    // \"HotelAmenities\":\"\",\"HotelNoOfGuests\":null,\"HotelBedding\":\"\",\"DurationHours\":null,
    // \"TourTime\":\"\",\"Conditions\":\"| This transfer is provided by Airport Concierge By Mayaland Cancun.||
    // Roundtrip Standard Private Transfer from Cancun International Airport to Cancun Hotels. Arrival instructions:
    // Pass Mexican immigration. Pick up your luggage and clear customs. After clearing customs you will exit to a
    // hall, where you will find different booths, please do not stop here. Walk all the way to the main gate, and
    // exit outside, where you will find a Gray Line/ Airport Concierge By Mayaland representatives who will welcome
    // you with a Classic Vacations sign, and take you to your transportation. For your return transfer, the shuttle
    // service will meet you at the lobby of your hotel for your transfer back to the Cancun International Airport.||
    // Please note: There is a $20 surcharge for a child car seat (each direction), subject to availability - to be
    // paid direct.|| Confirmation: It is recommended to call Airport Concierge By Mayaland Cancun to reconfirm your
    // service at least 24-48 hours prior. If calling from USA, dial 011 52 998 887 2450. If calling from Mexico,
    // dial 998 887 2450. Non-refundable if cancelled within 24 hours. NOTE: Maximum 2 pieces of luggage per
    // person\",\"DriverPickUpLoc\":\"\",\"DetailType\":\"TRANSFERS\",\"Payment\":\"DepositRecieved\",
    // \"CancellationPolicy\":\"Please refer to this for the details - http://www.classicvacations
    // .com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www.classicvacations
    // .com/about-us/2015-travel-smart-plan\",\"VendorInfo\":{\"Address\":\"Robalo S.M. 3 Cancun, Quintana Roo Mexico
    // Mexico\",\"ContactInfo\":null,\"WebSiteAddress\":null}},{\"BeginDate\":\"12/17/2015 12:00:00 AM\",
    // \"BeginTime\":\"00:00:00.000\",\"VendorID\":\"VITRIPM\",\"ProductID\":\"WVRN\",\"PartyName\":\"DEDONKER, Mr. M
    // . & Party\",\"ArrivalLoc\":\"\",\"UseLoc\":\"\",\"EndDate\":\"12/17/2015 12:00:00 AM\",
    // \"EndTime\":\"00:00:00.000\",\"VendorConf\":\"\",\"VendorName\":\"Trip Mate\",\"ProductName\":\"Travel Smart
    // Plan II ( TSP )\",\"ItemKey\":\"S9911077684\",\"InternalComments\":\"\",\"FlightPassengers\":\"\",
    // \"FlightSeatMapping\":\"\",\"FlightStartAirportCity\":\"\",\"FlightEndAirportCity\":\"\",
    // \"FlightOperatingAirlines\":null,\"FlightDuration\":null,\"FlightClass\":null,\"HotelPassengers\":\"\",
    // \"HotelConditions\":\"\",\"HotelAmenities\":\"\",\"HotelNoOfGuests\":null,\"HotelBedding\":\"\",
    // \"DurationHours\":null,\"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",
    // \"DetailType\":\"TSP_WAIVER\",\"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this
    // for the details - http://www.classicvacations
    // .com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www.classicvacations
    // .com/about-us/2015-travel-smart-plan\",\"VendorInfo\":{\"Address\":null,\"ContactInfo\":null,
    // \"WebSiteAddress\":\"http://www.classicvacations.com/about-us/2015-travel-smart-plan\"}}],
    // \"Payment\":{\"SellAmount\":22719.75,\"CommAmount\":3390.28,\"TaxAmount\":0.00,\"AmountBeforeTax\":22719.75,
    // \"DepAmountDue\":1045.00,\"AmountPosted\":1045.00,\"CurrAmountDue\":21674.75,\"DueDate\":\"10/20/2015 12:00:00
    // AM\"},\"Exception\":null,\"Destination\":\"Mexico\",\"PartyName\":\"DEDONKER, Mr. M. & Party\",\"Adults\":5,
    // \"Children\":0,\"TimeStamp\":\"11/6/2015 1:04:25 PM\"}\n";
    //String s = "{\"Name\":[{\"Title\":\"Mr.\",\"FirstName\":\"THOMAS\",\"LastName\":\"LEUNER\",
    // \"MiddleName\":\"\"},{\"Title\":\"Mrs.\",\"FirstName\":\"ELLEN\",\"LastName\":\"LEUNER\",
    // \"MiddleName\":\"\"}],\"Detail\":[{\"BeginDate\":\"5/28/2016 12:00:00 AM\",\"BeginTime\":\"00:00:00:000\",
    // \"VendorID\":\"VH02181\",\"ProductID\":\"SUP2\",\"PartyName\":\"LEUNER, Mr. T. & Party\",\"ArrivalLoc\":\"\",
    // \"UseLoc\":\"CDG\",\"EndDate\":\"6/1/2016 12:00:00 AM\",\"EndTime\":\"00:00:00:000\",
    // \"VendorConf\":\"334453\",\"VendorName\":\"Hotel Pont Royal\",\"ProductName\":\"Superior Room\",
    // \"ItemKey\":\"HI10256864641\",\"InternalComments\":\"*** VIP Clients ***\\r\\nTransportation in Paris on own
    // .\",\"FlightPassengers\":\"\",\"FlightSeatMapping\":\"\",\"FlightStartAirportCity\":\"\",
    // \"FlightEndAirportCity\":\"\",\"FlightOperatingAirlines\":null,\"FlightDuration\":null,\"FlightClass\":null,
    // \"HotelPassengers\":\" Mr. THOMAS LEUNER, Mrs. ELLEN LEUNER\",\"HotelConditions\":\" Check-in time is 3:00pm
    // and check-out time is 12:00pm noon. No shows, early departures or cancellations received within 3 days of
    // arrival will be charged a 1 night penalty. A regulation of the Municipality of Paris requires a mandatory
    // accommodation fee/city tax of 3-6 Euros per person per night in all hotel establishments. This fee is not
    // included in your rate plan and must be paid direct to the hotel upon check-out. City tax is a government
    // regulated fee and may change without prior notice.\",\"HotelAmenities\":\" Air Conditioning\",
    // \"HotelNoOfGuests\":\"2\",\"HotelBedding\":\"1 King Bed or 2 Twin Beds \",\"DurationHours\":null,
    // \"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",\"DetailType\":\"HOTEL\",
    // \"Payment\":\"FullyPaid\",\"CancellationPolicy\":\"Please refer to this for the details - http://www
    // .classicvacations.com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www
    // .classicvacations.com/about-us/2015-travel-smart-plan\",\"VendorInfo\":{\"Address\":\"7 Rue de Montalembart
    // Paris 75007 France\",\"ContactInfo\":\"011 33 42 84 70 00\",\"WebSiteAddress\":\"http://www.leshotelsduroy
    // .com/en/hotel-pont-royal\"}}],\"Payment\":{\"SellAmount\":1761.96,\"CommAmount\":233.66,\"TaxAmount\":204.21,
    // \"AmountBeforeTax\":1557.75,\"DepAmountDue\":40.00,\"AmountPosted\":1761.96,\"CurrAmountDue\":0.00,
    // \"DueDate\":\"1/25/2016 12:00:00 AM\"},\"Exception\":null,\"Destination\":\"Europe\",\"PartyName\":\"LEUNER,
    // Mr. T. & Party\",\"Adults\":2,\"Children\":0,\"TimeStamp\":\"5/4/2016 3:51:21 AM\"}" +
    //           "";

    String s = "{\"Name\":[{\"Title\":\"Mr.\",\"FirstName\":\"ALBERT IV\",\"LastName\":\"HEEKIN\"," +
               "\"MiddleName\":\"\"},{\"Title\":\"Ms.\",\"FirstName\":\"JILLIAN\",\"LastName\":\"STELTER\"," +
               "\"MiddleName\":\"\"},{\"Title\":\"Mr.\",\"FirstName\":\"JAMES\",\"LastName\":\"HEEKIN\"," +
               "\"MiddleName\":\"\"},{\"Title\":\"Ms.\",\"FirstName\":\"JAYNE\",\"LastName\":\"HEEKIN\"," +
               "\"MiddleName\":\"\"},{\"Title\":\"Mr.\",\"FirstName\":\"TYLER\",\"LastName\":\"HEEKIN\"," +
               "\"MiddleName\":\"\"},{\"Title\":\"Mr.\",\"FirstName\":\"WILLIAM\",\"LastName\":\"HEEKIN\"," +
               "\"MiddleName\":\"\"},{\"Title\":\"Mstr.\",\"FirstName\":\"BRADY\",\"LastName\":\"HEEKIN\"," +
               "\"MiddleName\":\"\"},{\"Title\":\"Miss.\",\"FirstName\":\"BARBARA\",\"LastName\":\"HEEKIN\"," +
               "\"MiddleName\":\"\"},{\"Title\":\"Mstr.\",\"FirstName\":\"JAMES JR\",\"LastName\":\"HEEKIN\"," +
               "\"MiddleName\":\"\"},{\"Title\":\"Mstr.\",\"FirstName\":\"HUNTER\",\"LastName\":\"HEEKIN\"," +
               "\"MiddleName\":\"\"}],\"Detail\":[{\"BeginDate\":\"12/17/2016 12:00:00 AM\"," +
               "\"BeginTime\":\"00:00:00:000\",\"VendorID\":\"VH01547\",\"ProductID\":\"AIRPBAPOVF2\"," +
               "\"PartyName\":\"HEEKIN, Mr. A. & Party\",\"ArrivalLoc\":\"\",\"UseLoc\":\"CUN\"," +
               "\"EndDate\":\"12/23/2016 12:00:00 AM\",\"EndTime\":\"00:00:00:000\"," +
               "\"VendorConf\":\"DREkOdNwdC|6832691HI11298930056\",\"VendorName\":\"Dreams Riviera Cancun Resort and " +
               "Spa\",\"ProductName\":\"Preferred Club Ocean View and Pool Front\",\"ItemKey\":\"HI11298930056\"," +
               "\"InternalComments\":\"Mr. TYLER HEEKIN age 12 and Mr. WILLIAM HEEKIN age 12 and are considered " +
               "children\",\"FlightPassengers\":\"\",\"FlightSeatMapping\":\"\",\"FlightStartAirportCity\":\"\"," +
               "\"FlightEndAirportCity\":\"\",\"FlightOperatingAirlines\":null,\"FlightDuration\":null," +
               "\"FlightClass\":null,\"HotelPassengers\":\" Mr. ALBERT IV HEEKIN, Mr. TYLER HEEKIN, Mr. WILLIAM " +
               "HEEKIN\",\"HotelConditions\":\" Check-in time is 3:00pm and check-out time is 12:00pm. Cancellations " +
               "received within 4 days of arrival will be charged a 2 night penalty.\",\"HotelAmenities\":\" Air " +
               "Conditioning, Child Program, Pool, Internet Access\",\"HotelNoOfGuests\":\"3\",\"HotelBedding\":\"2 " +
               "Double Beds \",\"DurationHours\":null,\"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\"," +
               "\"DetailType\":\"HOTEL\",\"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to " +
               "this for the details - http://www.classicvacations" +
               ".com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www" +
               ".classicvacations.com/about-us/2015-travel-smart-plan\",\"VendorInfo\":{\"Address\":\"Carretera " +
               "Federal 307, Chetumal - Puerto Juarez Benito Juarez, Puerto Morelos Riviera Cancún 77580 Mexico\"," +
               "\"ContactInfo\":\"011 52 998 872 9200\",\"WebSiteAddress\":\"http://www.dreamsresorts.com/drerc/index" +
               ".html\"}},{\"BeginDate\":\"12/17/2016 12:00:00 AM\",\"BeginTime\":\"00:00:00:000\"," +
               "\"VendorID\":\"VH01547\",\"ProductID\":\"AIRPBAPOVF2\",\"PartyName\":\"HEEKIN, Mr. A. & Party\"," +
               "\"ArrivalLoc\":\"\",\"UseLoc\":\"CUN\",\"EndDate\":\"12/23/2016 12:00:00 AM\"," +
               "\"EndTime\":\"00:00:00:000\",\"VendorConf\":\"DREEjEUnUm|6832691HI11298938597\"," +
               "\"VendorName\":\"Dreams Riviera Cancun Resort and Spa\",\"ProductName\":\"Preferred Club Ocean View " +
               "and Pool Front\",\"ItemKey\":\"HI11298938597\",\"InternalComments\":\"\",\"FlightPassengers\":\"\"," +
               "\"FlightSeatMapping\":\"\",\"FlightStartAirportCity\":\"\",\"FlightEndAirportCity\":\"\"," +
               "\"FlightOperatingAirlines\":null,\"FlightDuration\":null,\"FlightClass\":null,\"HotelPassengers\":\" " +
               "Ms. JAYNE HEEKIN, Mstr. HUNTER HEEKIN\",\"HotelConditions\":\" Check-in time is 3:00pm and check-out " +
               "time is 12:00pm. Cancellations received within 4 days of arrival will be charged a 2 night penalty" +
               ".\",\"HotelAmenities\":\" Air Conditioning, Child Program, Pool, Internet Access\"," +
               "\"HotelNoOfGuests\":\"2\",\"HotelBedding\":\"1 King Bed \",\"DurationHours\":null,\"TourTime\":\"\"," +
               "\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",\"DetailType\":\"HOTEL\"," +
               "\"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this for the details - " +
               "http://www.classicvacations.com/about-us/classic-vacations-price-protection-and-change-cancellation" +
               "-waiver http://www.classicvacations.com/about-us/2015-travel-smart-plan\"," +
               "\"VendorInfo\":{\"Address\":\"Carretera Federal 307, Chetumal - Puerto Juarez Benito Juarez, Puerto " +
               "Morelos Riviera Cancún 77580 Mexico\",\"ContactInfo\":\"011 52 998 872 9200\"," +
               "\"WebSiteAddress\":\"http://www.dreamsresorts.com/drerc/index.html\"}},{\"BeginDate\":\"12/17/2016 " +
               "12:00:00 AM\",\"BeginTime\":\"00:00:00:000\",\"VendorID\":\"VH01547\",\"ProductID\":\"PBAPOVF2\"," +
               "\"PartyName\":\"HEEKIN, Mr. A. & Party\",\"ArrivalLoc\":\"\",\"UseLoc\":\"CUN\"," +
               "\"EndDate\":\"12/23/2016 12:00:00 AM\",\"EndTime\":\"00:00:00:000\"," +
               "\"VendorConf\":\"DREDiKnYcc|6832691HI11298942083\",\"VendorName\":\"Dreams Riviera Cancun Resort and " +
               "Spa\",\"ProductName\":\"Preferred Club Ocean View and Pool Front\",\"ItemKey\":\"HI11298942083\"," +
               "\"InternalComments\":\"\",\"FlightPassengers\":\"\",\"FlightSeatMapping\":\"\"," +
               "\"FlightStartAirportCity\":\"\",\"FlightEndAirportCity\":\"\",\"FlightOperatingAirlines\":null," +
               "\"FlightDuration\":null,\"FlightClass\":null,\"HotelPassengers\":\" Mr. JAMES HEEKIN, Miss. BARBARA " +
               "HEEKIN, Mstr. JAMES JR HEEKIN\",\"HotelConditions\":\" Check-in time is 3:00pm and check-out time is " +
               "12:00pm. Cancellations received within 4 days of arrival will be charged a 2 night penalty.\"," +
               "\"HotelAmenities\":\" Air Conditioning, Child Program, Pool, Internet Access\"," +
               "\"HotelNoOfGuests\":\"3\",\"HotelBedding\":\"2 Double Beds \",\"DurationHours\":null," +
               "\"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\",\"DetailType\":\"HOTEL\"," +
               "\"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to this for the details - " +
               "http://www.classicvacations.com/about-us/classic-vacations-price-protection-and-change-cancellation" +
               "-waiver http://www.classicvacations.com/about-us/2015-travel-smart-plan\"," +
               "\"VendorInfo\":{\"Address\":\"Carretera Federal 307, Chetumal - Puerto Juarez Benito Juarez, Puerto " +
               "Morelos Riviera Cancún 77580 Mexico\",\"ContactInfo\":\"011 52 998 872 9200\"," +
               "\"WebSiteAddress\":\"http://www.dreamsresorts.com/drerc/index.html\"}},{\"BeginDate\":\"12/17/2016 " +
               "12:00:00 AM\",\"BeginTime\":\"00:00:00:000\",\"VendorID\":\"VH01547\",\"ProductID\":\"PBAPOVF2\"," +
               "\"PartyName\":\"HEEKIN, Mr. A. & Party\",\"ArrivalLoc\":\"\",\"UseLoc\":\"CUN\"," +
               "\"EndDate\":\"12/23/2016 12:00:00 AM\",\"EndTime\":\"00:00:00:000\"," +
               "\"VendorConf\":\"DREq9apVLV|6832691HI11298946114\",\"VendorName\":\"Dreams Riviera Cancun Resort and " +
               "Spa\",\"ProductName\":\"Preferred Club Ocean View and Pool Front\",\"ItemKey\":\"HI11298946114\"," +
               "\"InternalComments\":\"\",\"FlightPassengers\":\"\",\"FlightSeatMapping\":\"\"," +
               "\"FlightStartAirportCity\":\"\",\"FlightEndAirportCity\":\"\",\"FlightOperatingAirlines\":null," +
               "\"FlightDuration\":null,\"FlightClass\":null,\"HotelPassengers\":\" Ms. JILLIAN STELTER, Mstr. BRADY " +
               "HEEKIN\",\"HotelConditions\":\" Check-in time is 3:00pm and check-out time is 12:00pm. Cancellations " +
               "received within 4 days of arrival will be charged a 2 night penalty.\",\"HotelAmenities\":\" Air " +
               "Conditioning, Child Program, Pool, Internet Access\",\"HotelNoOfGuests\":\"2\",\"HotelBedding\":\"2 " +
               "Double Beds \",\"DurationHours\":null,\"TourTime\":\"\",\"Conditions\":\"\",\"DriverPickUpLoc\":\"\"," +
               "\"DetailType\":\"HOTEL\",\"Payment\":\"DepositRecieved\",\"CancellationPolicy\":\"Please refer to " +
               "this for the details - http://www.classicvacations" +
               ".com/about-us/classic-vacations-price-protection-and-change-cancellation-waiver http://www" +
               ".classicvacations.com/about-us/2015-travel-smart-plan\",\"VendorInfo\":{\"Address\":\"Carretera " +
               "Federal 307, Chetumal - Puerto Juarez Benito Juarez, Puerto Morelos Riviera Cancún 77580 Mexico\"," +
               "\"ContactInfo\":\"011 52 998 872 9200\",\"WebSiteAddress\":\"http://www.dreamsresorts.com/drerc/index" +
               ".html\"}}],\"Payment\":{\"SellAmount\":7145.16,\"CommAmount\":1395.72,\"TaxAmount\":0.00," +
               "\"AmountBeforeTax\":7145.16,\"DepAmountDue\":600.00,\"AmountPosted\":200.00," +
               "\"CurrAmountDue\":6945.16,\"DueDate\":\"11/2/2016 12:00:00 AM\"},\"Exception\":null," +
               "\"Destination\":\"Mexico\",\"PartyName\":\"HEEKIN, Mr. A. & Party\",\"Adults\":4,\"Children\":6," +
               "\"TimeStamp\":\"9/2/2016 5:41:35 AM\"}\n" + "";
    try {
      ObjectMapper                                mapper  = new ObjectMapper();
      com.fasterxml.jackson.databind.ObjectReader reader  = mapper.reader(BookingDetail.class);
      BookingDetail                               details = reader.readValue(s);
      if (details != null && details.getException() == null) {
        ClassicVacationsDetails view = ClassicVacationsController.buidlDetailsView(details, "1111");
        for (FlightVO fvo : view.tripVO.getFlights()) {
          System.out.println(fvo);
          for (PassengerVO pvo : fvo.getPassengers()) {
            System.out.println(pvo);
          }


        }

        for (HotelVO hotelVO : view.tripVO.getHotels()) {
          System.out.println(hotelVO);
        }

        for (TransportVO transportVO : view.tripVO.getTransport()) {
          System.out.println(transportVO);
        }

        for (TripNote noteVO : view.notes) {
          System.out.println(noteVO.getName());
          System.out.println(noteVO.getIntro());


        }
        System.out.println(view.bookingId);
      }
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Classic Vacations - Cannot search: ", e);
      e.printStackTrace();
    }

    return ok("ok");
  }

  private static ClassicVacationsDetails buidlDetailsView(BookingDetail details, String bookingId) {
    //first pass to get the cancellation policy if trip insurance is purchased
    //then we can eliminate duplicates if the same cancellation policy is in any bookings
    String defaultCancellationPolicy = "";
    if (details != null && details.getDetail() != null) {
      for (Detail d : details.getDetail()) {
        if (d.getDetailType() != null && d.getDetailType().equals("TSP_WAIVER") && d.getCancellationPolicy() != null) {
          defaultCancellationPolicy = d.getCancellationPolicy();
          break;
        }
      }
    }

    ClassicVacationsDetails view = new ClassicVacationsDetails();
    view.tripVO = new TripVO();
    view.notes = new ArrayList<>();
    view.destination = details.getDestination();
    if (details.getName() != null) {
      for (Name name : details.getName()) {
        PassengerVO passengerVO = new PassengerVO();
        passengerVO.setFirstName(name.getFirstName());
        passengerVO.setLastName(name.getLastName());
        passengerVO.setTitle(name.getTitle());
        view.tripVO.addPassenger(passengerVO);
      }
    }
    view.bookingId = bookingId;
    view.tripVO.setImportSrc(BookingSrc.ImportSrc.CLASSIC_VACATIONS);
    view.tripVO.setImportSrcId(view.bookingId);
    view.tripVO.setImportTs(System.currentTimeMillis());

    if (details.getDetail() != null) {
      for (Detail detail : details.getDetail()) {
        try {
          Timestamp startTimestamp = new Timestamp(dateUtils.parseDate(detail.getBeginDate(), DATE_TIME_FORMATS)
                                                            .getTime());

          Timestamp endTimestamp = null;

          try {
            endTimestamp = new Timestamp(dateUtils.parseDate(detail.getEndDate(), DATE_TIME_FORMATS).getTime());
          }
          catch (Exception e) {

          }
          if (startTimestamp != null) {
            detail.setBeginDate(Utils.formatDatePrint(startTimestamp.getTime()));
          }

          if (endTimestamp == null && startTimestamp != null) {
            endTimestamp = startTimestamp;
          }
          if (endTimestamp != null) {
            detail.setEndDate(Utils.formatDatePrint(endTimestamp.getTime()));
          }

          StringBuilder notes = new StringBuilder();
          if (detail.getVendorInfo() != null) {
            if (detail.getVendorInfo().getAddress() != null && detail.getVendorInfo()
                                                                     .getAddress()
                                                                     .trim()
                                                                     .length() > 0) {
              notes.append("Address:\n");
              notes.append(detail.getVendorInfo().getAddress());
              notes.append("\n\n");
            }
            if (detail.getVendorInfo().getContactInfo() != null && detail.getVendorInfo()
                                                                         .getContactInfo()
                                                                         .trim()
                                                                         .length() > 0) {
              notes.append("Contact Info:\n");
              notes.append(detail.getVendorInfo().getContactInfo());
              notes.append("\n\n");
            }
            if (detail.getVendorInfo().getWebSiteAddress() != null && detail.getVendorInfo()
                                                                            .getWebSiteAddress()
                                                                            .trim()
                                                                            .length() > 0) {
              notes.append("Web:\n");
              notes.append(detail.getVendorInfo().getWebSiteAddress());
              notes.append("\n\n");
            }
          }


          switch (detail.getDetailType()) {
            case "FLIGHT": //flight
              FlightVO fVO = new FlightVO();
              fVO.setCode(detail.getVendorID());
              fVO.setNumber(detail.getProductID());
              fVO.setDepatureTime(startTimestamp);
              fVO.setArrivalTime(endTimestamp);
              fVO.setReservationNumber(detail.getVendorConf());
              if (detail.getFlightDuration() != null && detail.getFlightDuration().length() > 0) {
                notes.append("Duration: ");
              }
              notes.append(detail.getFlightDuration());
              notes.append("\n");


              AirportVO arriveAirport = new AirportVO();
              arriveAirport.setName(detail.getFlightEndAirportCity());
              arriveAirport.setCode(detail.getArrivalLoc());
              fVO.setArrivalAirport(arriveAirport);

              AirportVO departAirport = new AirportVO();
              departAirport.setName(detail.getFlightStartAirportCity());
              departAirport.setCode(detail.getUseLoc());
              fVO.setDepartureAirport(departAirport);

              //try to match passengers for this flight
              if (detail.getFlightPassengers() != null && detail.getFlightPassengers().trim().length() > 0) {
                String[] tokens = detail.getFlightPassengers().split(",");
                if (tokens != null) {
                  for (String s : tokens) {
                    String[] tokens1 = s.split("\\|");
                    if (tokens1.length == 2) {
                      String name   = tokens1[0];
                      String fflyer = tokens1[1];
                      if (fflyer != null && fflyer.indexOf(":") > 0 && fflyer.indexOf(":") != (fflyer.length() - 1)) {
                        fflyer = fflyer.substring(fflyer.indexOf(":"));
                      }
                      else {
                        fflyer = "";
                      }
                      if (name != null) {
                        for (PassengerVO passengerVO : view.tripVO.getPassengers()) {
                          if (name.toUpperCase().contains(passengerVO.getFirstName().toUpperCase())) {
                            PassengerVO p = new PassengerVO();
                            p.setFirstName(passengerVO.getFirstName());
                            p.setLastName(passengerVO.getLastName());
                            p.setTitle(passengerVO.getTitle());
                            if (fflyer != null && fflyer.trim().length() > 0) {
                              p.setFrequentFlyer(fflyer);
                            }
                            p.setSeatClass(detail.getFlightClass());
                            fVO.addPassenger(p);
                          }
                        }
                      }
                    }
                  }
                }
              }

              //try to get the seat
              if (detail.getFlightSeatMapping() != null && detail.getFlightSeatMapping().trim().length() > 0) {
                String[] tokens = detail.getFlightSeatMapping().split("\\|");
                if (tokens != null) {
                  for (String s : tokens) {
                    String[] tokens1 = s.split(":");
                    if (tokens1.length == 2) {
                      String name = tokens1[0];
                      String seat = tokens1[1];
                      if (name != null && seat != null && seat.trim().length() > 0) {
                        boolean passengerFound = false;
                        for (PassengerVO passengerVO : fVO.getPassengers()) {
                          if (passengerVO.getFirstName() != null && name.toUpperCase()
                                                                        .contains(passengerVO.getFirstName()
                                                                                             .toUpperCase()) &&
                              passengerVO
                                                                                                                    .getLastName() != null && name
                                  .toUpperCase()
                                  .contains(passengerVO.getLastName().toUpperCase())) {
                            passengerFound = true;
                            String pNote = passengerVO.getNote();
                            if (pNote == null) {
                              pNote = "";
                            }
                            passengerVO.setSeat(seat);
                            //passengerVO.setNote(pNote + "\n" + "Seat: " + seat);
                            break;
                          }
                        }
                        if (!passengerFound) {
                          PassengerVO p = new PassengerVO();
                          p.setFirstName(name);
                          p.setSeat(seat);
                          //p.setNote("Seat: " + seat);
                          fVO.addPassenger(p);
                        }
                      }
                    }
                  }
                }
              }

              if (detail.getCancellationPolicy() != null && !detail.getCancellationPolicy()
                                                                   .isEmpty() && !defaultCancellationPolicy.contains(
                  detail.getCancellationPolicy())) {
                notes.append("\n\n");
                notes.append(detail.getCancellationPolicy());
                notes.append("\n");
              }
              if (fVO.isValid()) {
                fVO.setNotes(notes.toString());
                view.tripVO.addFlight(fVO);
              }
              detail.setBeginDate(Utils.formatDateTimePrint(startTimestamp.getTime()));
              detail.setEndDate(Utils.formatDateTimePrint(endTimestamp.getTime()));
              break;
            case "HOTEL": //hotel
              HotelVO hVO = new HotelVO();
              hVO.setName(detail.getVendorName());
              hVO.setCheckin(startTimestamp);
              hVO.setCheckout(endTimestamp);
              hVO.setConfirmation(detail.getVendorConf());

              if (detail.getProductName() != null && detail.getProductName().trim().length() > 0) {
                notes.append(detail.getProductName());
                notes.append("\n");
              }

              if (detail.getHotelPassengers() != null && detail.getHotelPassengers().trim().length() > 0) {
                notes.append("\n");
                notes.append("Guests: ");
                notes.append(detail.getHotelPassengers().trim());
                notes.append("\n");
              }

              if (detail.getInternalComments() != null && detail.getInternalComments().trim().length() > 0) {
                notes.append("\n");
                notes.append(detail.getInternalComments().trim());
                notes.append("\n");
              }

              if (detail.getHotelAmenities() != null && detail.getHotelAmenities().trim().length() > 0) {
                notes.append("\n");
                notes.append(detail.getHotelAmenities().trim());
                notes.append("\n");
              }
              if (detail.getConditions() != null && detail.getConditions().trim().length() > 0) {
                notes.append("\n");
                notes.append(detail.getConditions().trim());
                notes.append("\n");
              }
              if (detail.getHotelConditions() != null && detail.getHotelConditions().trim().length() > 0) {
                notes.append("\n");
                notes.append(detail.getHotelConditions().trim());
                notes.append("\n");
              }
              if (detail.getCancellationPolicy() != null && !detail.getCancellationPolicy()
                                                                   .isEmpty() && !defaultCancellationPolicy.contains(
                  detail.getCancellationPolicy())) {
                notes.append("\n\n");
                notes.append(detail.getCancellationPolicy().trim());
                notes.append("\n");
              }

              //double check the start/end time
              String lNotes = notes.toString().toLowerCase();
              Pattern timePattern = Pattern.compile("[0-9]{1,2}:[0-9]{2}[am,pm]{2}");                // 99 999 99999999

              if (startTimestamp != null && Utils.formatTime24(startTimestamp.getTime())
                                                 .isEmpty() && endTimestamp != null && Utils.formatTime24
                  (endTimestamp.getTime())
                                                                                            .isEmpty() && lNotes
                      .contains(
                  "check-in") && lNotes.contains("check-out")) {
                try {
                  int     i           = 0;
                  Matcher timeMatcher = timePattern.matcher(lNotes);
                  while (timeMatcher.find()) {
                    String time = timeMatcher.group();

                    if (i == 0) {
                      hVO.setCheckin(new Timestamp(dateUtils.parseDate(Utils.formatTimestamp(startTimestamp.getTime(),
                                                                                             "MM/dd/yyyy") + " " + time,
                                                                       DATE_TIME_FORMATS).getTime()));
                    }
                    else if (i == 1 && endTimestamp != null) {
                      hVO.setCheckout(new Timestamp(dateUtils.parseDate(Utils.formatTimestamp(endTimestamp.getTime(),
                                                                                              "MM/dd/yyyy") + " " +
                                                                        time,
                                                                        DATE_TIME_FORMATS).getTime()));
                    }
                    i++;
                  }
                }
                catch (Exception e) {
                  e.printStackTrace();
                }
              }

              if (hVO.isValid()) {
                hVO.setNote(notes.toString());
                view.tripVO.addHotel(hVO);
              }
              break;
            case "CRUISE": //cruise
              CruiseVO cVO = new CruiseVO();
              cVO.setName(detail.getProductName());
              cVO.setCompanyName(detail.getVendorName());
              cVO.setTimestampDepart(startTimestamp);
              cVO.setTimestampArrive(endTimestamp);
              cVO.setConfirmationNo(detail.getVendorConf());

              if (detail.getConditions() != null && detail.getConditions().trim().length() > 0) {
                notes.append("\n");
                notes.append(detail.getConditions());
                notes.append("\n");
              }
              if (detail.getCancellationPolicy() != null && !detail.getCancellationPolicy()
                                                                   .isEmpty() && !defaultCancellationPolicy.contains(
                  detail.getCancellationPolicy())) {
                notes.append("\n\n");
                notes.append(detail.getCancellationPolicy());
                notes.append("\n");
              }

              if (cVO.isValid()) {
                cVO.setNote(notes.toString());
                view.tripVO.addCruise(cVO);
              }
              break;
            case "EXCURSION": //activity
              ActivityVO aVO = new ActivityVO();
              aVO.setName(detail.getVendorName());
              aVO.setStartDate(startTimestamp);
              aVO.setEndDate(endTimestamp);
              aVO.setConfirmation(detail.getVendorConf());
              aVO.setBookingType(ReservationType.TOUR);
              aVO.setTag(detail.getItemKey());

              if (detail.getProductName() != null && detail.getProductName().trim().length() > 0) {
                notes.append(detail.getProductName());
                notes.append("\n");
              }
              if (detail.getConditions() != null && detail.getConditions().trim().length() > 0) {
                notes.append("\n");
                notes.append(detail.getConditions());
                notes.append("\n");
              }
              if (detail.getCancellationPolicy() != null && !detail.getCancellationPolicy()
                                                                   .isEmpty() && !defaultCancellationPolicy.contains(
                  detail.getCancellationPolicy())) {
                notes.append("\n\n");
                notes.append(detail.getCancellationPolicy());
                notes.append("\n");
              }

              if (aVO.isValid()) {
                aVO.setNote(notes.toString());
                view.tripVO.addActivity(aVO);
              }
              break;
            case "CONCIERGE": //Notes
            case "HOTEL_SERVICES":
            case "MERCHANDISE":
            case "TSP_WAIVER":
            case "SERVICE_FEE":
            case "CANCEL":
              TripNote tripNote = new TripNote();
              tripNote.setNoteId(DBConnectionMgr.getUniqueLongId());
              tripNote.setName(detail.getVendorName());
              tripNote.setStatus(0);
              tripNote.setImportSrc(BookingSrc.ImportSrc.CLASSIC_VACATIONS);
              tripNote.setImportSrcId(view.bookingId);
              tripNote.setImportTs(System.currentTimeMillis());
              tripNote.setNoteTimestamp(startTimestamp.getTime());
              tripNote.setTag(detail.getItemKey());

              if (detail.getVendorConf() != null && detail.getVendorConf().trim().length() > 0) {
                notes.append("Confirmation: ");
                notes.append(detail.getVendorConf());
                notes.append("\n");
              }
              if (detail.getProductName() != null && detail.getProductName().trim().length() > 0) {
                notes.append(detail.getProductName());
                notes.append("\n");
              }
              if (detail.getConditions() != null && detail.getConditions().trim().length() > 0) {
                notes.append("\n");
                notes.append(detail.getConditions());
                notes.append("\n");
              }
              if (detail.getCancellationPolicy() != null && !detail.getCancellationPolicy().isEmpty()) {
                notes.append("\n\n");
                notes.append(detail.getCancellationPolicy());
                notes.append("\n");
              }


              tripNote.setIntro(notes.toString());
              view.notes.add(tripNote);
              break;
            case "RAIL": //transfers
            case "CAR":
            case "TRANSFERS":
              TransportVO tVO = new TransportVO();
              tVO.setName(detail.getVendorName());
              tVO.setPickupDate(startTimestamp);
              tVO.setDropoffDate(endTimestamp);
              tVO.setConfirmationNumber(detail.getVendorConf());
              if (detail.getDetailType().equals("RAIL")) {
                tVO.setBookingType(ReservationType.RAIL);
              }
              else if (detail.getDetailType().equals("TRANSFERS")) {
                tVO.setBookingType(ReservationType.PRIVATE_TRANSFER);
              }
              else if (detail.getDetailType().equals("CAR")) {
                tVO.setBookingType(ReservationType.CAR_RENTAL);
                if (detail.getDriverPickUpLoc() != null || detail.getDriverPickUpLoc().trim().length() > 0) {
                  if ((detail.getDriverPickUpLoc().indexOf(")") + 1) < detail.getDriverPickUpLoc().length()) {
                    tVO.setPickupLocation(detail.getDriverPickUpLoc()
                                                .substring(detail.getDriverPickUpLoc().indexOf(")") + 1));
                  }
                  else {
                    tVO.setPickupLocation(detail.getDriverPickUpLoc());
                  }
                  detail.setDriverPickUpLoc(tVO.getPickupLocation());
                }
              }

              if (detail.getProductName() != null && detail.getProductName().trim().length() > 0) {
                notes.append(detail.getProductName());
                notes.append("\n");
              }
              if (detail.getConditions() != null && detail.getConditions().trim().length() > 0) {
                notes.append("\n");
                notes.append(detail.getConditions());
                notes.append("\n");
              }
              if (detail.getCancellationPolicy() != null && !detail.getCancellationPolicy()
                                                                   .isEmpty() && !defaultCancellationPolicy.contains(
                  detail.getCancellationPolicy())) {
                notes.append("\n\n");
                notes.append(detail.getCancellationPolicy());
                notes.append("\n");
              }

              if (tVO.isValid()) {
                tVO.setNote(notes.toString());
                view.tripVO.addTransport(tVO);
              }
              break;
          }
        }
        catch (ParseException pe) {

        }
      }
    }
    return view;
  }

  /*
  Retrieve the parse booking details from memcached and then save to the trip
   */
  @With({Authenticated.class, Authorized.class})
  public Result importBooking(String tripId, String bookingID) {

    try {
      SessionMgr  sessionMgr = SessionMgr.fromContext(ctx());
      Trip        trip       = Trip.findByPK(tripId);
      UserProfile up         = UserProfile.findByPK(sessionMgr.getUserId());
      if (SecurityMgr.getTripAccessLevel(trip, sessionMgr.getCredentials()).ge(SecurityMgr.AccessLevel.APPEND)) {
        ClassicVacationsDetails view = (ClassicVacationsDetails) CacheMgr.get(APPConstants
                                                                                  .CACHE_CLASSIC_VACATIONS_BOOKING_PREFIX + bookingID);

        if (view != null) {
          if (view.tripVO != null && view.tripVO.hasValidBookings()) {
            VOModeller voModeller = new VOModeller(trip, up, false);
            voModeller.buildTripVOModels(view.tripVO);
            voModeller.saveAllModels();
          }
          if (view.notes != null && view.notes.size() > 0) {
            for (TripNote tripNote : view.notes) {
              TripNote existingNote = TripNote.getNotesByTripIdNameTag(trip.tripid,
                                                                       tripNote.getName(),
                                                                       tripNote.getTag());
              if (existingNote != null) {
                existingNote.setIntro(tripNote.getIntro());
                existingNote.setNoteTimestamp(tripNote.getNoteTimestamp());

                existingNote.setImportSrc(tripNote.getImportSrc());
                existingNote.setImportSrcId(tripNote.getImportSrcId());
                existingNote.setImportTs(tripNote.getImportTs());

                existingNote.setModifiedBy(up.getUserid());
                existingNote.setLastUpdatedTimestamp(System.currentTimeMillis());

              }
              else {
                tripNote.setTrip(trip);
                tripNote.setCreatedBy(up.getUserid());
                tripNote.setModifiedBy(up.getUserid());
                tripNote.setLastUpdatedTimestamp(System.currentTimeMillis());
                tripNote.setCreatedTimestamp(System.currentTimeMillis());
                tripNote.save();
              }
            }
            BookingNoteController.invalidateCache(tripId);
          }

          flash(SessionConstants.SESSION_PARAM_MSG, view.destination + " bookings imported");
          return redirect(routes.BookingController.bookings(trip.tripid, new TabType.Bound(TabType.ITINERARY), null));
        }
      }
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Classic Vacations - Cannot search: " + tripId, e);
    }

    BaseView view = new BaseView();
    view.message = "Error Connecting with Classic Vacations -  please contact support";
    return ok(views.html.common.message.render(view));
  }

  /*
    Initiate the Oauth Workflow
   */
  @With({Authenticated.class, Authorized.class})
  public Result login(String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Trip       trip       = Trip.findByPK(tripId);
    if (SecurityMgr.getTripAccessLevel(trip, sessionMgr.getCredentials()).ge(SecurityMgr.AccessLevel.APPEND)) {
      //save a session flag so that the Classic Vacations Modal is displayed when the login flow is complete
      String modal = routes.ClassicVacationsController.searchByDate(tripId).url();
      session(SessionConstants.SESSION_MODAL_URL, modal);
      String authUrl = getOautStartUrl(sessionMgr.getUserId(), trip.tripid);
      return redirect(authUrl);
    }
    BaseView view = new BaseView();
    view.message = "You are not authorized to access this trip";
    return ok(views.html.common.message.render(view));

  }

  private static String getOautStartUrl(String userid, String tripId) {
    String state = userid + "____" + tripId;

    String url = new AuthorizationRequestUrl(AUTH_CODE_URL, AUTH_CLIENT_ID, Arrays.asList("code")).setState(state)
                                                                                                  .setRedirectUri(
                                                                                                      AUTH_REDIRECT_URL)
                                                                                                  .build();

    return url;
  }

  /*
    Retrieve the details of a booking by BOOKING ID
   */

  /*
  callback method from Classic Vacation with the Authentication Code
   */
  @With({Authenticated.class, Authorized.class})
  public Result auth() {
    //we got a call back from the ClassicVacations....
    String fullUrlBuf = "http://" + request().host() + request().uri();


    AuthorizationCodeResponseUrl authResponse = new AuthorizationCodeResponseUrl(fullUrlBuf);
    // check for user-denied error
    if (authResponse.getError() != null) {
      // authorization denied...
      Log.log(LogLevel.ERROR, "Classic Vacation - OAuth - Cannot get Auth Code " + authResponse.getCode());
    }
    else {
      // request access token using authResponse.getCode()...
      String scope = "";
      try {
        Log.log(LogLevel.DEBUG,
                "Classic Vacation - OAuth - Got Auth Code " + authResponse.getCode() + " " + authResponse.getState());
        scope = authResponse.getState();

        TokenResponse response = new AuthorizationCodeTokenRequest(new NetHttpTransport(),
                                                                   new JacksonFactory(),
                                                                   new GenericUrl(AUTH_TOKEN_URL),
                                                                   authResponse.getCode()).setRedirectUri(
            AUTH_REDIRECT_URL).setClientAuthentication(new BasicAuthentication(AUTH_CLIENT_ID, AUTH_SECRET)).execute();

        if (response.getAccessToken() != null && response.getAccessToken().length() > 0 && scope != null) {
          long currentTimestamp = (long) (System.currentTimeMillis() / 1000);
          //set the timestamp back 30secs to account for any lag
          currentTimestamp -= 30;
          //scope is userid____tripid so we know where to go next
          Log.log(LogLevel.DEBUG, "Classic Vacation - OAuth - Got New Token for " + scope);
          String[] tokens = scope.split("____");
          if (tokens != null && tokens.length == 2) {
            String userid = tokens[0];
            String tripId = tokens[1];
            if (userid != null && tripId != null) {
              Account a    = Account.findActiveByLegacyId(userid);
              Trip    trip = Trip.find.byId(tripId);
              if (a != null && trip != null) {
                Credentials credential = SecurityMgr.getCredentials(a);
                if (SecurityMgr.getTripAccessLevel(trip, credential).ge(SecurityMgr.AccessLevel.APPEND)) {
                  CVCredentials cvCredentials = new CVCredentials();
                  cvCredentials.userId = a.getLegacyId();
                  cvCredentials.token = response.getAccessToken();
                  cvCredentials.tokenExpiry = response.getExpiresInSeconds() + currentTimestamp;
                  if (response.getRefreshToken() != null && response.getRefreshToken().length() > 0) {
                    cvCredentials.refreshToken = response.getRefreshToken();
                    cvCredentials.refreshTokenExpiry = currentTimestamp + (5 * 24 * 60 * 60);
                  }
                  CacheMgr.set(APPConstants.CACHE_CLASSIC_VACATIONS_AUTH_PREFIX + a.getLegacyId(),
                               cvCredentials,
                               APPConstants.CACHE_CLASSIC_VACATIONS_SECS);

                  //now - let's continue with the import
                  return redirect("/home#/trips/bookings/" + trip.tripid + "/ITINERARY");
                }
              }
            }
          }
        }
      }
      catch (TokenResponseException e) {
        StringBuilder error = new StringBuilder();
        if (e.getDetails() != null) {
          error.append("Error: " + e.getDetails().getError());
          if (e.getDetails().getErrorDescription() != null) {
            error.append(" ");
            error.append(e.getDetails().getErrorDescription());
          }
          if (e.getDetails().getErrorUri() != null) {
            error.append(" ");
            error.append(e.getDetails().getErrorUri());
          }
        }
        else {
          error.append(e.getMessage());
        }
        Log.log(LogLevel.ERROR, "Classic Vacations - Cannot get token for user: " + scope + " Error: " + error, e);
      }
      catch (Exception e) {
        Log.log(LogLevel.ERROR,
                "Classic Vacations - Cannot get token for user: " + scope + " Error: " + e.getMessage(),
                e);
      }
    }
    flash(SessionConstants.SESSION_PARAM_MSG, "Error connecting to Classic Vacations, please contact Umapped Support");
    return redirect("/home#/tours/myTours?inSearchTerm=&inCmpyId=All&searchType=0&inTableName=myTours");
  }

  /*
    Search Classic Vacations to retrieve all bookings that are found within the date range of the trip.
    Since Classic does not have a service to get bookings by departure date, we get all bookings made in the previous
     3 months
    Then we filter out bookings that only occur during the trip dates
   */
  @With({Authenticated.class, Authorized.class})
  public Result searchByDate(String tripId) {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      Trip       trip       = Trip.findByPK(tripId);
      if (SecurityMgr.getTripAccessLevel(trip, sessionMgr.getCredentials()).ge(SecurityMgr.AccessLevel.APPEND)) {
        DateTime      currentDate = new DateTime();
        List<Booking> bookingList = new ArrayList<>();

        //search for all bookings made in the last 3 months
        String startDate = Utils.formatTimestamp(currentDate.minusMonths(3).getMillis(), "yyyy-MM-dd");
        String endDate   = Utils.formatTimestamp(currentDate.getMillis(), "yyyy-MM-dd");


        String token = getToken(sessionMgr.getUserId());
        if (token == null) {
          return ok(views.html.classicVacations.loginModal.render(trip.tripid));
        }

        String url = AUTH_SEARCH_DATE + "start=" + startDate + "&end=" + endDate;
        Log.log(LogLevel.DEBUG, "Classic Vacation - Searching: " + url + " for " + sessionMgr.getUserId());

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet    get    = new HttpGet(url);
        get.setHeader("Content-Type", "application/json; charset=UTF-8");
        get.addHeader("accept", "application/json");
        get.setHeader("Authorization", "Bearer " + token);
        HttpResponse r = client.execute(get);

        if (r != null && r.getStatusLine() != null && r.getStatusLine().getStatusCode() == 200) {
          String resp = EntityUtils.toString(r.getEntity());
          try {
            ObjectMapper                                mapper  = new ObjectMapper();
            com.fasterxml.jackson.databind.ObjectReader reader  = mapper.reader(SearchResults.class);
            SearchResults                               details = reader.readValue(resp);
            if (details != null) {
              if (details.getBooking() != null) {
                for (Booking name : details.getBooking()) {
                  java.util.Date departureDate = dateUtils.parseDate(name.getDepartureDate(), DATE_TIME_FORMATS);
                  if (departureDate.getTime() >= trip.starttimestamp && departureDate.getTime() <= trip.endtimestamp) {
                    java.util.Date bookedDate = dateUtils.parseDate(name.getBookingDate(), DATE_TIME_FORMATS);
                    name.setBookingDate(Utils.formatDatePrint(bookedDate.getTime()));
                    name.setDepartureDate(Utils.formatDatePrint(departureDate.getTime()));
                    bookingList.add(name);
                  }
                }
              }
            }
          }
          catch (Exception e) {
            Log.log(LogLevel.ERROR, "Classic Vacations - Cannot search: " + tripId, e);
            e.printStackTrace();
          }
        }
        return ok(views.html.classicVacations.searchModal.render(bookingList, trip.tripid));
      }
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Classic Vacations - Cannot search: " + tripId, e);
    }

    BaseView view = new BaseView();
    view.message = "Error Connecting with Classic Vacations -  please contact support";
    return ok(views.html.common.message.render(view));
  }

  public static String getToken(String userId) {
    /* Based on the userid passed in, check to see if we have a valid token
       3 posssibilities can happen here:
       1. user has a valid token - a token is found in memcached with the expiry timestamp > current
       2. user has an expired token but valid refresh token - a refresh token is found in memcached - request a new
       token
       3. user has no valid token or refresh token - so we initiate a full auth process.
    */
    long currentTimestamp = (long) (System.currentTimeMillis() / 1000);
    //set the timestamp back 30secs to account for any lag
    currentTimestamp -= 30;

    CVCredentials credentials = (CVCredentials) CacheMgr.get(APPConstants.CACHE_CLASSIC_VACATIONS_AUTH_PREFIX + userId);
    if (credentials != null && credentials.userId.equals(userId)) {
      //do we have tokens
      if (credentials.tokenExpiry > currentTimestamp) {
        // found a valid token - let's go
        Log.log(LogLevel.DEBUG, "Classic Vacation - OAuth - Found Token for " + userId);

        return credentials.token;
      }
      else if (credentials.refreshTokenExpiry > currentTimestamp) {
        // found a refresh token - let's request a refresh
        Log.log(LogLevel.DEBUG, "Classic Vacation - OAuth - Found Refresh Token for " + userId);
        try {
          TokenResponse response = new RefreshTokenRequest(new NetHttpTransport(),
                                                           new JacksonFactory(),
                                                           new GenericUrl(AUTH_TOKEN_URL),
                                                           credentials.refreshToken).setClientAuthentication(new BasicAuthentication(
              AUTH_CLIENT_ID,
              AUTH_SECRET)).execute();
          if (response.getAccessToken() != null && response.getAccessToken().length() > 0) {
            Log.log(LogLevel.DEBUG, "Classic Vacation - OAuth - Got New Refresh Token for " + userId);

            credentials.token = response.getAccessToken();
            credentials.tokenExpiry = response.getExpiresInSeconds() + currentTimestamp;
            if (response.getRefreshToken() != null && response.getRefreshToken().length() > 0) {
              credentials.refreshToken = response.getRefreshToken();
              credentials.refreshTokenExpiry = currentTimestamp + (5 * 24 * 60 * 60);
            }
            CacheMgr.set(APPConstants.CACHE_CLASSIC_VACATIONS_AUTH_PREFIX + userId,
                         credentials,
                         APPConstants.CACHE_CLASSIC_VACATIONS_SECS);
            return credentials.token;
          }

        }
        catch (TokenResponseException e) {
          StringBuilder error = new StringBuilder();
          if (e.getDetails() != null) {
            error.append("Error: " + e.getDetails().getError());
            if (e.getDetails().getErrorDescription() != null) {
              error.append(" ");
              error.append(e.getDetails().getErrorDescription());
            }
            if (e.getDetails().getErrorUri() != null) {
              error.append(" ");
              error.append(e.getDetails().getErrorUri());
            }
          }
          else {
            error.append(e.getMessage());
          }
          Log.log(LogLevel.ERROR,
                  "Classic Vacations - Cannot get refresh token for user: " + userId + " Error: " + error,
                  e);
        }
        catch (Exception e) {
          Log.log(LogLevel.ERROR,
                  "Classic Vacations - Cannot get refresh token for user: " + userId + " Error: " + e.getMessage(),
                  e);

        }
      }
    }

    //no tokens - let's trigger the oauth flow
    return null;
  }

  @With({Authenticated.class, Authorized.class})
  public Result viewDetails(String tripId, String bookingID) {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      Trip       trip       = Trip.findByPK(tripId);
      if (SecurityMgr.getTripAccessLevel(trip, sessionMgr.getCredentials()).ge(SecurityMgr.AccessLevel.APPEND)) {
        String token = getToken(sessionMgr.getUserId());
        if (token == null) {
          return ok(views.html.classicVacations.loginModal.render(trip.tripid));
        }

        String url = AUTH_BOOKING_DETAILS + bookingID;
        Log.log(LogLevel.DEBUG, "Classic Vacation - Importing: " + url + " for " + sessionMgr.getUserId());

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet    get    = new HttpGet(url);
        get.setHeader("Content-Type", "application/json; charset=UTF-8");
        get.addHeader("accept", "application/json");
        get.setHeader("Authorization", "Bearer " + token);
        HttpResponse r = client.execute(get);

        if (r != null && r.getStatusLine() != null && r.getStatusLine().getStatusCode() == 200) {
          String resp = EntityUtils.toString(r.getEntity());
          Log.log(LogLevel.DEBUG, "Classic Vacation - Importing BookingID: " + bookingID + " Response: " + resp);

          try {
            ObjectMapper                                mapper  = new ObjectMapper();
            com.fasterxml.jackson.databind.ObjectReader reader  = mapper.reader(BookingDetail.class);
            BookingDetail                               details = reader.readValue(resp);
            if (details != null && details.getException() == null) {
              ClassicVacationsDetails view = ClassicVacationsController.buidlDetailsView(details, bookingID);
              if (view.bookingId != null) {
                CacheMgr.set(APPConstants.CACHE_CLASSIC_VACATIONS_BOOKING_PREFIX + view.bookingId, view);
              }

              return ok(views.html.classicVacations.viewDetails.render(details, trip.tripid, bookingID));

            }
            else {
              BaseView view = new BaseView();
              view.message = "Classic Vacations booking not found. Email support@umapped.com with the Booking ID";
              return ok(views.html.common.message.render(view));
            }
          }
          catch (Exception e) {
            Log.log(LogLevel.ERROR,
                    "Classic Vacations - Cannot import Booking ID: " + bookingID + " for trip: " + tripId,
                    e);
            e.printStackTrace();
          }
        }
      }
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Classic Vacations - Cannot import: " + tripId, e);
    }

    BaseView view = new BaseView();
    view.message = "Error Connecting with Classic Vacations -  please contact support";
    return ok(views.html.common.message.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result clearSession() {
    BaseView view = new BaseView();

    SessionMgr    sessionMgr  = SessionMgr.fromContext(ctx());
    CVCredentials credentials = (CVCredentials) CacheMgr.get(APPConstants.CACHE_CLASSIC_VACATIONS_AUTH_PREFIX +
                                                             sessionMgr
        .getUserId());
    if (credentials == null) {
      view.message = "You are not logged in to Classic Vacations";
    }
    else {
      CacheMgr.set(APPConstants.CACHE_CLASSIC_VACATIONS_AUTH_PREFIX + sessionMgr.getUserId(), null);
      view.message = "Logged out of Classic Vacations";
    }
    return ok(views.html.common.message.render(view));

  }
}
