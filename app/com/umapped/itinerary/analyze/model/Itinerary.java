package com.umapped.itinerary.analyze.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wei on 2017-02-28.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Itinerary {
  private List<ItineraryItem> items;

  private String umItineraryId;

  public Itinerary() {
    this.umItineraryId = null;
    items = new ArrayList<>();
  }

  public Itinerary(String umId) {
    this.umItineraryId = umId;
    items = new ArrayList<>();
  }


  public String getUmItineraryId() {
    return umItineraryId;
  }

  public Itinerary setUmItineraryId(String umItineraryId) {
    this.umItineraryId = umItineraryId;
    return this;
  }

  public List<ItineraryItem> getItems() {
    return items;
  }

  public Itinerary setItems(List<ItineraryItem> items) {
    this.items = items;
    return this;
  }

  public Itinerary addItem(ItineraryItem item) {
    if (items == null) {
      items = new ArrayList<>();
    }
    items.add(item);
    return this;
  }
}
