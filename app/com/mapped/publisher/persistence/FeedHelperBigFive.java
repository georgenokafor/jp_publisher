package com.mapped.publisher.persistence;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.parse.BigFive.TourList.Tour;
import com.mapped.publisher.parse.BigFive.TourList.TourList;
import com.mapped.publisher.persistence.template.*;
import com.mapped.publisher.persistence.template.Currency;
import com.mapped.publisher.utils.Log;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.activity.UmActivityReservation;

import controllers.ImageController;
import models.publisher.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.xml.bind.*;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by surge on 2015-09-28.
 */
public class FeedHelperBigFive
    extends FeedHelperTmplt {

  public final static String  BIG_FIVE_TOURS_URL     = "http://www.bigfive.com/tour_list.xml";
  public final static String  BIG_FIVE_FEED_SRC_NAME = "Big Five";
  private static      Pattern dayStarter             = Pattern.compile("(Day.*)\\s*([0-9]+)",
                                                                       Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

  public static void main(String[] args) {
    String itinerary = "&lt;strong&gt;Day 1:  Arrive Cairo, " + "Egypt&lt;/strong&gt;\n" + "Sunny Egypt is a vast " +
                       "desert watered from end to end by the River Nile.  Visitors have come " + "for millennia to "
                       + "marvel at the eternal pyramids, the enigmatic sphinx, " + "Cairo's historic mosques, " +
                       "towering" + " obelisks, gigantic colossi, monumental temples, " + "and richly pained tombs "
                       + "depicting " + "everyday life some 4,000 years ago.  The Greek historian " + "Herodotus " +
                       "called" + " Egypt \"The " + "Gift of the Nile,\" and it is from the Nile that travelers can "
                       + "best " + "appreciate the " + "beauty of the country and explore its ancient monuments.  You" +
                       " are greeted" + " " + "upon arrival " + "in Cairo by a Big Five representative and escorted " +
                       "to your hotel.  " + "The remainder " + "of the " + "day is at leisure.  &lt;strong&gt;Four " +
                       "Seasons Hotel Cairo at " + "Nile Plaza &lt;" + "/strong&gt;" + "\n" + "\n" + "&lt;strong&gt;" +
                       "Day 2:  Cairo – Dahshur, " + "Saqqara, &amp;amp; Giza&lt;/strong&gt;" + "\n" + "This morning " +
                       "you begin exploring the desert" + " antiquities of a civilization begun more " + "than 5," +
                       "000 years ago.  In the company of " + "your Egyptologist specialist guide, " + "you " +
                       "visit the Bent Pyramid at Dahshur, Zoser’s " + "Step Pyramid at Saqqara, " + "and then the " +
                       "Great " + "Pyramids of Giza.  Nothing evokes the " + "long and intriguing history of Egypt "
                       + "as powerfully" + " as the Pyramids.  Rising from " + "the desert, Khufu, " + "Khafra and " +
                       "Menkaure seem to symbolize " + "the enigmatic tug of Egypt" + " in our imaginations.  The " +
                       "Great Pyramid of Khufu immortalizes" + " the son of Sneferu " + "and Hetepheres.  Though " +
                       "little is known" + " of this pharaoh, his " + "monument, the largest " + "of the three, is " +
                       "comprised of 2.3 million stone " + "blocks, each " + "weighing an average of" + " 2.5 tons.  " +
                       "You will also visit the colossal statue of the " + "Great " + "Sphinx, which has " + "stood " +
                       "guard over the Pyramids for more than 4," + "500 years.  Carved from " + "an outcrop of" + " " +
                       "rock, the Sphinx remains the ultimate symbol of Ancient " + "Egypt with its " + "lion’s body"
                       + " and human head.  The history and the lifestyle of ancient Egyptian " + "pharaohs " + "come" +
                       " " + "alive before our eyes through the skilled narrations of your specialist guide. " +
                       " Lunch " + "will be served at the Mena House Oberoi Hotel.  You will then return to the hotel" +
                       " " + "where" + " the remainder of the day is at your leisure.  &lt;strong&gt;Four Seasons " +
                       "Hotel " + "Cairo " + "at " + "Nile Plaza (B,L)&lt;/strong&gt;\n" + "\n" + "&lt;strong&gt;Day " +
                       "3:  Cairo&lt;" + "/strong&gt;\n" + "Today’s focus is Cairo, which encompasses Old Cairo’s " +
                       "Church of St. " + "Sergius" + " and Ben Ezra " + "Synagogue.  See the Citadel of Salah ad-Din" +
                       " with its ethereal " + "Mosque of " + "Mohammed Ali and then " + "the famed Museum of " +
                       "Antiquities.  This museum " + "houses the greatest " + "collection of pharaonic " +
                       "treasures in the world.  Your guide " + "will show you the highlights" + " of these " +
                       "artifacts, " + "including the Tutankhamen " + "Collection and the Mummy Room.  After " +
                       "lunch you will visit sprawling" + " Khan El " + "Khalili bazaar before you return to your " +
                       "hotel" + ".  &lt;strong&gt;Four Seasons Hotel " + "Cairo at Nile Plaza (B,L)&lt;/strong&gt;" +
                       "\n" + "\n" + "&lt;strong&gt;Day 4:  Cairo / Luxor&lt;" + "/strong&gt;\n" + "After breakfast, " +
                       "fly to Luxor.  You " + "are met upon arrival and " + "transferred to your hotel for " +
                       "check in before beginning your " + "tour of the West Bank" + ".  At the Valley of the Kings, " +
                       "" + "Big Five has arranged for special " + "access in several " + "tombs have been " +
                       "exclusively opened for you" + " to ensure VIP private " + "access.  Enter the" + " Tomb of " +
                       "Seti I, one of the most ornately decorated " + "tombs in the " + "valley.  It has " + "been " +
                       "closed to the general public in order to minimize human " + "impact. " + " You will also" + "" +
                       " explore tombs open to the general public including the Tomb of Ramses " + "VI," + " Ramses "
                       + "IX, and Horemheb.  Continue to the Valley of the Queens where Big Five has arranged" + " " +
                       "for " + "you to experience Egypt’s most exclusive Pharaonic tomb – the spectacular Tomb of "
                       + "Nefertari.  The tomb, also known as Egypt’s “Sistine Chapel,” was closed to the public in "
                       + "1950" + " because of various problems that threatened the spectacular paintings, " + "which" +
                       " " + "are considered to be the best preserved and most eloquent decorations of any Egyptian "
                       + "burial site.  Priceless and beautiful art is found on almost every available surface in the" +
                       " " + "tomb, including stars by the thousands painted on the ceiling of the burial chamber on " +
                       "a " + "blue " + "background to represent the sky.  From here, you proceed to the funerary " +
                       "Temple " + "of " + "Queen " + "Hatshepsut at Deir el-Bahri before stopping by the imposing " +
                       "Colossi of " + "Memnon on " + "your way back" + " to your hotel.  &lt;strong&gt;Hotel Sofitel" +
                       " Old Winter " + "Palace Luxor (B)" + "&lt;/strong&gt;\n" + "\n" + "&lt;strong&gt;Day 5:  " +
                       "Luxor / Qena / Nile " + "Cruise – West Bank\n" + "&lt;/strong&gt;This morning’s drive travels" +
                       " north to Qena to visit " + "the Temple of Dendara " + "complex.  The main building that " +
                       "dominates the complex is the " + "Temple of Hathor – the twin " + "temple to the Temple of " +
                       "Horus in Luxor.  The temple, " + "dedicated to Hathor, " + "is one of " + "the best preserved" +
                       " temples in all Egypt.  Subsequent" + " additions were added in Roman " + "times" + ".  " +
                       "Return to Luxor to board your special " + "dahabeya cruise ship.  &lt;strong&gt;Sonesta " +
                       "Amirat (B,L,D)&lt;/strong&gt;\n" + "\n" + "&lt;strong&gt;Day 6:  Nile Cruise – East Bank&lt;"
                       + "/strong&gt;\n" + "On your way to visit " + "the East Bank of Luxor, you stop first at the " +
                       "Temple " + "of Luxor, " + "built by the two " + "pharaohs Amenhotep III und Ramses II.  The " +
                       "temple was " + "dedicated to Amun-Ra, " + "whose " + "marriage to Mut was celebrated annually" +
                       " with a sacred " + "procession traveled the Nile from " + "" + "Karnak to Luxor Temple.  The " +
                       "entrance is a huge pylon " + "built by Ramses II and has " + "two seated " + "statues of the " +
                       "king.  Originally, two large " + "obelisks stood in front of " + "the pylon.  However, " +
                       "only one remains, while the other now " + "stands in Place de la " + "Concorde in Paris.  " +
                       "Beyond the " + "pylon, is the peristyle hall - " + "built by Ramses II - " + "and bordered on" +
                       " three sides by double rows" + " of columns.  In the " + "northeastern part of" + " the hall " +
                       "is the Mosque of Abu al-Haggag.  The rest " + "of the Temple " + "was built by " + "Amenhotep" +
                       " Ill, starting with the Colonnade that has fourteen huge " + "pillars " + "in two " + "rows. " +
                       " The large Court of Amenhotep III is surrounded on three sides by double " + "rows of " +
                       "pillars, and leads to the hypostyle hall, containing 32 pillars, " + "and on to the " +
                       "Sanctuary of the Sacred Boat.  Alexander the Great built a kiosk within that of " +
                       "Amenhotep II.  Finally, you reach the four-columned Holiest of Holies: the Sanctuary of the "
                       + "Sacred Statue.  You then proceed down the corniche to the Temple of Karnak.  This is the "
                       + "greatest place of worship in history.  It includes many singular temples, dedicated to " +
                       "Amun, " + "his wife (Mut), and their son Khonsu, the moon deity.  Since the Arab conquest, "
                       + "it became known as \"al-Karnak,\" meaning “the fort.”  The temple starts with the Avenue "
                       + "of" + " " + "the Rams, representing Amun, a symbol of fertility and growth.  Beneath the "
                       + "rams’ " + "heads, " + "small statues of Ramses II were carved.  You start your visit to " +
                       "Karnak by " + "passing through the" + " First Pylon, dating to King Nekhtebo of the 30th " +
                       "Dynasty, on to the " + "large forecourt where on " + "the right you find the chapels of the "
                       + "Theban Triad.  On the " + "left, " + "you will see the Temple of Ramses III.  Then you see " +
                       "" + "the remnants of the Second " + "Pylon which " + "leads to the large hypostyle hall which" +
                       " has " + "134 columns, with the double row" + " in the middle " + "higher than the rest.  You" +
                       " then come" + " to the remnants of the Third Pylon, " + "" + "before which stands the obelisk" +
                       " of Thutmosis " + "I, and on to the Fourth Plyon, " + "guarded" + " by the obelisk of " +
                       "Hatshepsut.  Beyond are " + "the remains of the Fifth Pylon, " + "leading to " + "the Holiest" +
                       " of Holies.  You finally come" + " to the court, from the Middle Kingdom, " + "which " +
                       "leads to a large Hall of Ceremonies, " + "dating back to Tuthmosis III.  The Sacred Lake, " +
                       "used for purification, lies outside the " + "main hall where a large statue of a scarab dates" +
                       " back" + " " + "to King Amenhotep.  &lt;" + "strong&gt;Sonesta  Amirat (B,L,D)&lt;/strong&gt;" +
                       "\n" + "\n" + "&lt;strong&gt;Day 7:  Nile " + "Cruise – Esna&lt;/strong&gt;\n" + "Today is at " +
                       "your leisure to " + "enjoy the beautiful " + "scenery as you cruise gently along the Nile, "
                       + "passing date palm " + "groves, fisherman " + "navigating their feluccas, " + "and smiling " +
                       "children.  Enjoy the fine " + "amenities of your " + "deluxe dahabeya.  You will also pass "
                       + "through the Esna locks en route " + "to Edfu.  &lt;" + "strong&gt;Sonesta Amirat (B,L," +
                       "D)&lt;/strong&gt;\n" + "\n" + "&lt;" + "strong&gt;Day 8:  " + "Nile Cruise – Edfu&lt;" +
                       "/strong&gt;\n" + "Disembark your dahabeya and drive " + "to the Temple " + "of Horus in the " +
                       "heart of Edfu.  Completely " + "buried in the sand until it was" + " " + "excavated in the " +
                       "1860's, this is the best-preserved ancient " + "temple in Egypt.  The " + "temple, dedicated " +
                       "to the falcon god Horus, " + "was built in the Ptolemaic period between " + "237" + " and 57 " +
                       "BC.  The inscriptions on its walls " + "provide important information on " + "language, " +
                       "myth and religion during the Greco-Roman period in " + "ancient Egypt.  In " + "particular, " +
                       "the " + "temple's inscribed building texts provide details both of " + "its " +
                       "construction, and also " + "about the mythical interpretation of this and all other temples "
                       + "as " + "the Island of " + "Creation.  There are also important scenes and inscriptions of " +
                       "the" + " Sacred Drama," + " which " + "related the age-old conflict between Horus and Seth.  " +
                       "After " + "your visit you will return " + "to your dahabeya for the afternoon.  &lt;" +
                       "strong&gt;Sonesta " + "Amirat (B,L,D)&lt;/strong&gt;\n" + "\n" + "&lt;strong&gt;Day 9:  Nile " +
                       "Cruise – Kom Ombo&lt;" + "/strong&gt;\n" + "After a leisurely " + "breakfast, begin your " +
                       "visit of the Temple of Kom " + "Ombo.  It is an unusual " + "double temple " + "built during " +
                       "the Ptolemaic dynasty in the " + "Egyptian town of Kom Ombo.  Some " + "additions to " + "it " +
                       "were later made during the Roman " + "period.  The building is unique because its " +
                       "'double'" + " design meant that there were " + "courts, halls, sanctuaries and rooms " +
                       "duplicated for two " + "sets of gods.  The southern half" + " of the temple was dedicated to " +
                       "the crocodile god Sobek, " + "god of fertility and creator of" + " the world with Hathor and " +
                       "Khonsu.  Meanwhile, " + "the " + "northern part of the temple was " + "dedicated to the " +
                       "falcon god Haroeris, " + "also known as " + "Horus the Elder.  The temple is" + " atypical " +
                       "because everything is perfectly " + "symmetrical " + "along the main axis.  &lt;" +
                       "strong&gt;Sonesta Amirat (B,L,D)&lt;/strong&gt;\n" + "\n" + "&lt;" + "strong&gt;Day 10:  " +
                       "Nile" + " Cruise / Aswan&lt;/strong&gt;\n" + "Aswan, 550 miles from Cairo and " + "133 miles " +
                       "from " + "Luxor, is Egypt’s southernmost town, " + "and it is totally different from the" + "" +
                       " rest of " + "the country.  The Nile is magically beautiful " + "here as it flows down from " +
                       "the " + "great " + "dams and around the giant granite boulders and " + "palm-studded islands " +
                       "that protrude " + "from the cascading rapids of the First Cataract.  Aswan " + "feels more " +
                       "African and the " + "majority of the inhabitants are Nubian, " + "darker and taller than " +
                       "Upper Egyptians, with a " + "different language and different customs.  From" + " time " +
                       "immemorial, this is where Egypt " + "ended and Nubia began.  Aswan’s position made it an " +
                       "important market for caravans " + "passing with gold, slaves, incense and ivory, " + "and " +
                       "today it remains the best bazaar " + "outside Cairo, bustling with Nubian and Egyptian " +
                       "traders" + ".  See the Unfinished Obelisk, " + "set in the quarries where pink granite was " +
                       "taken for use in " + "monuments throughout the " + "land.  The massive High Dam, an " +
                       "engineering marvel built to control " + "the Nile's annual " + "" + "floodwaters.  Then, you " +
                       "will proceed to the jewel-like Temple of Philae, " + "a " + "harmonious " + "blend of " +
                       "Egyptian and Greco-Roman architecture.  After your tour you will " + "check " + "into " +
                       "your hotel.  &lt;strong&gt;Movenpick Resort Aswan (B,L,D)&lt;/strong&gt;" + "\n" + "\n" +
                       "&lt;" + "strong&gt;Day 11:  Aswan / Abu Simbel / Cairo / Alexandria&lt;" + "/strong&gt;\n" +
                       "Transfer to " + "the airport for your flight to Abu Simbel where you will " + "have a tour of" +
                       " the two " + "temples" + ".  The Greater Temple of Ramses II is one of the many" + " relics " +
                       "erected by the Pharaoh " + "Ramses II, this is the grandest and most beautiful of " +
                       "temples.  The facade is 33 meters high," + " " + "and 38 meters broad, and guarded by for " +
                       "statues of Ramses II, " + "each of which is 20 " + "meters high.  High on the facade, there "
                       + "is a carved row of baboons, " + "smiling at the " + "sunrise.  The Great Hall of Pillars " +
                       "has " + "eight pillars bearing the deified " + "Ramses II in " + "the shape of Osiris.  The " +
                       "walls of " + "this hall bear inscriptions recording the " + "Battle of " + "Qadesh waged by " +
                       "Ramses II " + "against the Hittites.  The smaller ‘hall of the nobles," + "’ " + "contains " +
                       "four square " + "pillars.  The Holiest of Holies is where you find Amun-Ra four statues " +
                       "- Ra-Harakhte, " + "Ptah, Amun-Ra and King Ramses II.  This temple is unique, " + "since the " +
                       "sun" + " shines " + "directly on the Holiest of Holies two days a year:  February 21, " +
                       "the king's " + "birthday, and October 22, the date of his coronation.  Located north of the " +
                       "Greater" + " " + "Temple, the Temple of Nefertari was carved in the rock by Ramses II and " +
                       "dedicated to the " + "goddess of Love and Beauty, Hathor, and also to his favorite wife, " +
                       "Nefertari.  The " + "facade is adorned by six statues, four to Ramses II and two to his wife " +
                       "" + "Nefertari.  The " + "" + "entrance leads to a hall containing six pillars bearing the " +
                       "head of the " + "goddess, " + "Hathor" + ".  The eastern wall bears inscriptions depicting " +
                       "Ramses II striking the enemy " + "before " + "Ra-Harakhte and Amun-Ra.  Other wall scenes " +
                       "show Ramses II and Nefertari offering" + " " + "sacrifices to the gods.  Here is one of the " +
                       "greatest artificial domes and it bears the " + "" + "man-made mountain behind the Temples of " +
                       "Abu Simbel.  Fly to Cairo via Aswan.  Upon " + "arrival in" + " " + "Cairo you will drive " +
                       "north through the Nile Delta on your way to " + "Alexandria.  After " + "checking " + "into " +
                       "your hotel the remainder of the evening is at your" + " leisure.  &lt;" + "strong&gt;Four " +
                       "Seasons " + "Hotel Alexandria at San Stefano (B)&lt;" + "/strong&gt;\n" + "\n" + "&lt;" +
                       "strong&gt;Day 12:  Alexandria&lt;/strong&gt;\n" + "Few cities in" + " the world lean so " +
                       "heavily upon legend as Alexandria, " + "Egypt’s second largest city. " + " It was founded by " +
                       "" + "Alexander the Great in 331 BC, " + "and was where his embalmed body " + "was laid to " +
                       "rest eight " + "years later after his conquest of Asia" + ".  It became the " + "capital of " +
                       "Egypt under the " + "Ptolemaic dynasty until the last of the line " + "Cleopatra, " + "who " +
                       "ended her life.  Then, " + "Romans took over the city.  Spend the day exploring " + "remnants" +
                       " of the city's glorious " + "past, including the National Museum and Pompey's Pillar," + " "
                       + "towering 93 feet over the site " + "of the ancient Serapeum, " + "and the second " +
                       "century Roman Amphitheater with its elegant " + "marble terraces.  Also step into " + "the "
                       + "eerie tomb chambers of the 2000-year-old Kom " + "el-Shugafa Catacombs, " + "the last major" +
                       " " + "construction in the Ptolemaic tradition.  &lt;" + "strong&gt;Four Seasons Hotel " +
                       "Alexandria at San Stefano (B,L)&lt;/strong&gt;\n" + "\n" + "&lt;strong&gt;Day 13:  " +
                       "Alexandria / Bahariya Oasis&lt;/strong&gt;\n" + "Early this morning " + "you will begin the "
                       + "drive to the Bahariya Oasis.  Upon arrival, " + "you check into your hotel" + " before some" +
                       " " + "afternoon sightseeing.  First, " + "you will visit the olive and palm tree " + "gardens" +
                       " " + "before going to the salt lake.  You then cross " + "the sand dunes towards the " +
                       "Pyramid " + "Mountain and then to the English Mountain.  Return to your " + "hotel for dinner" +
                       " and " + "a " + "well deserved rest.  &lt;strong&gt;Western Desert Hotel (B,L," + "D)&lt;" +
                       "/strong&gt;\n" + "\n" + "&lt;strong&gt;Day 14:  Bahariya Oasis / El Qasr / Black Desert / " +
                       "White Desert\n" + "&lt;" + "/strong&gt;After breakfast, you will visit El Qasr, one of " +
                       "Egypt’s most fascinating " + "desert" + " villages.  This medieval town features many unique " +
                       "desert homes that have been " + "completely or " + "partially restored dating from the " +
                       "sixteenth through nineteenth " + "centuries" + ".  Then you leave the" + " oasis crossing " +
                       "madi and sand dunes to visit the " + "Black Desert where" + " you will see the Divided " +
                       "Mountain and El Zogag Mountain.  " + "Proceed to the hot and cold " + "springs and Crystal " +
                       "Mountain. " + " Drive to the White " + "Desert where you will have the option " + "to camp in" +
                       " the desert or return back" + " to your " + "hotel.  &lt;strong&gt;White Desert Camp or " +
                       "Western Desert Hotel (B,L," + "D)&lt;" + "/strong&gt;\n" + "\n" + "&lt;strong&gt;Day 15:  " +
                       "White " + "Desert / Bahariya Oasis / Cairo\n" + "&lt;/strong&gt;This morning, you will either" +
                       " have " + "breakfast in the White Desert or in " + "your " + "hotel.  You will have a tour of" +
                       " the White " + "Desert before driving to Ain Khadra " + "Spring.  Return " + "to the Bahariya" +
                       " Oasis for lunch and " + "continue onto Cairo.  Upon " + "arrival in Cairo you will check" +
                       " into your hotel, where the " + "remainder of the day is " + "at your leisure.  &lt;" +
                       "strong&gt;Four " + "Seasons Hotel Cairo at The " + "First Residence (B," + "L)&lt;/strong&gt;" +
                       "\n" + "\n" + "&lt;strong&gt;Day 16:  Cairo / Depart&lt;" + "/strong&gt;\n" + "Say goodbye to " +
                       "your Egyptian adventure as you transfer to Cairo " + "International Airport " + "for your" +
                       " departing flight home.&lt;strong&gt;  (B)&lt;/strong&gt;";


    Document doc        = Jsoup.parse(itinerary);
    String   unescaped  = doc.text();
    String   lUnescaped = unescaped.toLowerCase();
    Elements strongs    = doc.getElementsByTag("strong");

    int dayIdx = 0;
    for (org.jsoup.nodes.Element e : strongs) {
      if (e.text().toLowerCase().startsWith("day")) {
        String  name = e.text();
        Matcher m    = dayStarter.matcher(name);

        if (m.find()) {
          dayIdx = Integer.parseInt(m.group(2));
        }
        else {
          dayIdx++;
        }

        int startText = unescaped.indexOf(name);
        int endText   = lUnescaped.indexOf("day " + (dayIdx + 1), startText);
        if (endText == -1) {
          endText = lUnescaped.indexOf("days " + (dayIdx + 1), startText);
        }
        if (endText == -1) {
          endText = lUnescaped.indexOf("day " + (dayIdx + 2), startText);
        }
        if (endText == -1) {
          endText = lUnescaped.indexOf("days " + (dayIdx + 2), startText);
        }
        String dayText;
        if (startText >= 0 && endText > 0) {
          dayText = unescaped.substring(startText, endText);
        }
        else if (startText >= 0) {
          dayText = unescaped.substring(startText);
        }
        else {
          dayText = "";
        }
      }
    }
  }

  @Override
  public Map<PoiFeedUtil.FeedResult, Integer> all() {
    return null;
  }

  @Override
  public Map<PoiFeedUtil.FeedResult, Integer> wipe() {
    return null;
  }

  @Override
  public Map<PoiFeedUtil.FeedResult, Integer> load() {
    Map<PoiFeedUtil.FeedResult, Integer> results    = new HashMap<>();
    int                                  resSuccess = 0;
    int                                  resError   = 0;

    try {
      HttpClient   httpClient   = HttpClientBuilder.create().build();
      HttpGet      httpGet      = new HttpGet(BIG_FIVE_TOURS_URL);
      HttpResponse httpResponse = httpClient.execute(httpGet);
      if (httpResponse.getStatusLine() != null && httpResponse.getStatusLine().getStatusCode() == 200) {
        String          body   = StringEscapeUtils.unescapeJava(EntityUtils.toString(httpResponse.getEntity()));
        InputStream     source = new ByteArrayInputStream(body.getBytes());
        XMLInputFactory xif    = XMLInputFactory.newFactory();
        XMLStreamReader xsr    = xif.createXMLStreamReader(source);

        while (xsr.hasNext()) {
          xsr.next(); // Advance to Envelope tag
          if (xsr.getLocalName().equals("tour_list")) {
            break;
          }
        }

        JAXBContext           jaxbContext      = JAXBContext.newInstance(TourList.class);
        Unmarshaller          jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        JAXBElement<TourList> je               = jaxbUnmarshaller.unmarshal(xsr, TourList.class);
        TourList              tours            = je.getValue();

        if (tours.getTour() != null && tours.getTour().size() > 0) {
          //1. Deleting old imported records of Big Five type
          TmpltImport.deleteFromSrc(FeedSourcesInfo.byName(BIG_FIVE_FEED_SRC_NAME));

          //2. Load new batch
          for (Tour t : tours.getTour()) {
            TmpltImport ti = TmpltImport.buildTmpltImport(BIG_FIVE_FEED_SRC_NAME,
                                                          TmpltType.Type.TOUR,
                                                          t.getTourName().trim());

            JAXBContext jaxbHotelInfoContext = JAXBContext.newInstance(Tour.class);
            Marshaller  jaxbMarshaller       = jaxbHotelInfoContext.createMarshaller();
            // output pretty printed
            //jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            StringWriter raw = new StringWriter();
            jaxbMarshaller.marshal(t, raw);
            ti.setRaw(raw.toString());
            ti.setHash(DatatypeConverter.parseHexBinary(DigestUtils.md5Hex(raw.toString())));
            ti.setSrcReferenceId(getFeedRef(t));
            ti.save();
            Log.info("Imported Big Five Tour named: " + ti.getName());
            ++resSuccess;
          }
        }
      }
      //consume the request to close the HTTP connection
      EntityUtils.consume(httpResponse.getEntity());
    }
    catch (Exception e) {
      Log.err("Failed to download and extract Big Five feed", e);
      e.printStackTrace();
      ++resError;
    }
    results.put(PoiFeedUtil.FeedResult.ERROR, resError);
    results.put(PoiFeedUtil.FeedResult.SUCCESS, resSuccess);

    return results;
  }

  public String getFeedRef(Tour t) {
    StringBuilder sb = new StringBuilder();
    sb.append(t.getTourName().replaceAll("\\s+", "").toLowerCase());
    sb.append(t.getLink());
    return DigestUtils.md5Hex(sb.toString());
  }

  /**
   * Go over all imported records and generate templates
   * @return
   */
  @Override
  public Map<PoiFeedUtil.FeedResult, Integer> process() {
    FeedSrc                              feedSrc    = FeedSrc.findByName(BIG_FIVE_FEED_SRC_NAME);
    List<TmpltImport>                    imports    = TmpltImport.findBySrc(feedSrc.getSrcId());
    Map<PoiFeedUtil.FeedResult, Integer> results    = new HashMap<>();
    int                                  resSuccess = 0;
    int                                  resError   = 0;

    try {
      JAXBContext  jaxbContext      = JAXBContext.newInstance(Tour.class);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      for (TmpltImport ti : imports) {
        StringReader sr = new StringReader(ti.getRaw());
        Tour         t  = (Tour) jaxbUnmarshaller.unmarshal(sr);
        boolean      rc = tourToTemplate(modifiedBy.getLegacyId(), t, feedSrc, ti.getSrcReferenceId());
        if (rc) {
          ++resSuccess;
        }
        else {
          ++resError;
        }

      }
    }
    catch (Exception e) {
      Log.err("Failed to process Big Five imported records", e);
      e.printStackTrace();
    }

    results.put(PoiFeedUtil.FeedResult.ERROR, resError);
    results.put(PoiFeedUtil.FeedResult.SUCCESS, resSuccess);
    return results;
  }

  private boolean tourToTemplate(String userId, Tour t, FeedSrc feedSrc, String reference) {
    boolean result = false;
    boolean isNew  = false;
    //1. Looking if there is a template already with this
    Template template = Template.findFromFeedSrc(feedSrc.getSrcId(), reference);
    if (template == null) {
      template = Template.buildTemplate(userId);
      isNew = true;
    }
    else {
      template.setLastupdatedtimestamp(System.currentTimeMillis());
      template.setModifiedby(userId);
    }

    template.setDuration(getDuration(t));
    template.setVisibility(Template.Visibility.PUBLIC);
    template.setTmpltType(TmpltType.Type.TOUR);
    template.setFeedSrc(feedSrc);
    template.setFeedSrcReference(reference);
    template.setCmpyid("0");
    template.setName(t.getTourName());
    Account account = Account.findActiveByLegacyId(userId);

    //Saving cover image
    if (t.getTourImages() != null && t.getTourImages().getImage() != null && t.getTourImages().getImage().size() > 0) {
      String primaryImg = t.getTourImages().getImage().get(0);
      String fileName   = FilenameUtils.getName(primaryImg);
      try {
        FileImage image = ImageController.downloadAndSaveImage(primaryImg,
                                                               fileName,
                                                               FileSrc.FileSrcType.IMG_VENDOR_BIGFIVE,
                                                               t.getTourName(),
                                                               account,
                                                               false);
        template.setFileImage(image);
      }
      catch (Exception e) {
        Log.err("Failed to set Big Five cover for tour: " + t.getTourName() + " filename:" + fileName, e);
      }
    }

    template.setDescription(t.getTourShortDescription());
    template.save();

    TmpltMetaRs tmrs = tourToMeta(t, template.getTemplateid(), userId);
    if (isNew) {
      //Saving new metadata
      TmpltMgr.save(tmrs);
    }
    else {
      //Updating metadata
      TmpltMgr.update(tmrs);
      //Marking previously generated template details as deleted
      TmpltDetails.markDeleted(template.templateid);
    }

    highlightsToDetails(template.getTemplateid(), userId, t.getTourItinerary(), template.getDuration());


    Log.info("Processing Big Five tour: " + t.getTourName());
    return result;
  }

  private int getDuration(Tour t) {
    int result = 1;
    try {
      String l = t.getTourLength().replaceAll("[^\\d.]", "");
      if (l.trim().length() > 0) {
        result = Integer.parseInt(l.trim());
      }
    }
    catch (Exception e) {
      Log.err("Big Five feed tour" + t.getTourName() + " has non-parsable tour length: " + t.getTourLength(), e);
    }
    return result;
  }

  private TmpltMetaRs tourToMeta(Tour t, String templateId, String userId) {
    TmpltMetaRs meta = TmpltMetaRs.buildMeta(userId, templateId, t.getTourName().trim());

    TmpltMetaJson json = meta.getData();

    if (t.getTourSeries() != null && t.getTourSeries().getSeries() != null) {
      for (String ts : t.getTourSeries().getSeries()) {
        json.getCountries().add(ts);
      }
    }

    json.setDescription(t.getTourShortDescription());
    json.setHighlights(t.getTourHighlights());
    json.setLength(getDuration(t));
    if (NumberUtils.isNumber(t.getTourPrice())) {
      Integer i = Integer.parseInt(t.getTourPrice());
      if (i != null) {
        json.setPrice(i.floatValue());
      }
    }
    json.setCurrency(Currency.USD);
    json.setPriceInfo(t.getTourRates());
    json.setItineraryDesc(t.getTourItinerary());

    if (t.getTourImages() != null && t.getTourImages().getImage() != null) {
      for (String iUrl : t.getTourImages().getImage()) {
        json.getImages().add(iUrl);
      }
    }

    if (t.getTourPdf() != null) {
      Attachment pdf = new Attachment();
      pdf.setMimeType("application/pdf");
      pdf.setDescription("");
      pdf.setUrl(t.getTourPdf());
      pdf.setFilename(FilenameUtils.getName(t.getTourPdf()));
      json.getAttachments().add(pdf);
    }

    //TODO: Serguei: Let's see the results, this might be very wrong approach
    if (t.getTourCountries() != null && t.getTourCountries().getCountry() != null) {
      for (String c : t.getTourCountries().getCountry()) {
        List<String> countries = guesstractCountries(c);
        json.getCountries().addAll(countries);
      }
    }
    json.getLinks().add(t.getLink());
    return meta;
  }

  private void highlightsToDetails(String templateId, String userId, String itinerary, int dayCount) {
    Document doc        = Jsoup.parse(itinerary);
    String   unescaped  = doc.text();
    String   lUnescaped = unescaped.toLowerCase();
    Elements strongs    = doc.getElementsByTag("strong");

    int dayIdx = 0;
    for (org.jsoup.nodes.Element e : strongs) {
      String lower = e.text().toLowerCase();
      if (e.text().toLowerCase().startsWith("day") || e.text().toLowerCase().contains(" day ")) {
        String  name = e.text();
        Matcher m    = dayStarter.matcher(name);

        if (m.find()) {
          String        s  = m.group();
          boolean       f  = false;
          StringBuilder sb = new StringBuilder();
          for (char c : s.toCharArray()) {
            if (Character.isDigit(c)) {
              if (!f) {
                f = true;
              }
              sb.append(c);
            }
            else if (f) {
              break;
            }
          }
          if (f) {
            dayIdx = Integer.parseInt(sb.toString());
          }
          else {
            dayIdx = Integer.parseInt(m.group(2));
          }
        }
        else {
          dayIdx++;
        }

        int startText = unescaped.indexOf(name);
        int endText   = lUnescaped.indexOf("day " + (dayIdx + 1), startText);
        if (endText == -1) {
          endText = lUnescaped.indexOf("days " + (dayIdx + 1), startText);
        }
        if (endText == -1) {
          endText = lUnescaped.indexOf("day " + (dayIdx + 2), startText);
        }
        if (endText == -1) {
          endText = lUnescaped.indexOf("days " + (dayIdx + 2), startText);
        }
        String dayText;
        if (startText >= 0 && endText > 0) {
          dayText = unescaped.substring(startText, endText);
        }
        else if (startText >= 0) {
          dayText = unescaped.substring(startText);
        }
        else {
          dayText = "";
          Log.err("Big Five Formatting issue when converting highlights to details day " + dayIdx + " out of " +
                  dayCount);
          Log.err(unescaped);
        }

        boolean            isNew = false;
        List<TmpltDetails> tds   = TmpltDetails.findForSpecificTripDay(templateId, dayIdx);
        TmpltDetails       td    = null;

        if (tds == null || tds.size() == 0) {
          td = TmpltDetails.buildTmpltDetails(userId);
          isNew = true;
        }
        else {
          if (tds.size() == 1) {
            td = tds.get(0);
          }
          else {
            td = TmpltDetails.buildTmpltDetails(userId);
            isNew = true;
          }

          td.setLastupdatedtimestamp(System.currentTimeMillis());
          td.setModifiedby(userId);
          td.setStatus(APPConstants.STATUS_ACTIVE);
        }

        td.setName(name);
        td.setDayOffset(dayIdx);
        td.setTemplateid(templateId);
        td.setComments(dayText);
        td.setDuration(1);
        td.setDetailtypeid(ReservationType.ACTIVITY);
        UmActivityReservation activity = new UmActivityReservation();
        td.setReservation(activity);
        activity.setNotesPlainText(dayText);
        activity.getActivity().name = name;
        td.save();
      }
    }
  }

  private List<String> guesstractCountries(String name) {
    String[] parts = StringUtils.split(name, ',');
    if (parts.length > 1) {
      return Arrays.asList(parts);
    }
    parts = name.split("\\s+");
    if (parts.length > 1) {
      return Arrays.asList(parts);
    }
    ArrayList<String> result = new ArrayList<>(1);
    result.add(name);
    return result;
  }
}
