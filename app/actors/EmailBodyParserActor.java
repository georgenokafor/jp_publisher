package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.mapped.common.CoreConstants;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.parse.extractor.booking.EmailBodyExtractor;
import com.mapped.publisher.parse.extractor.booking.util.EmailUtils;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.persistence.VOModeller;
import com.mapped.publisher.utils.Log;
import controllers.EmailController;
import controllers.TripController;
import models.publisher.ApiAudit;
import models.publisher.EmailParseLog;
import models.publisher.Trip;
import models.publisher.UserProfile;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.time.StopWatch;

import javax.persistence.OptimisticLockException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by twong on 2015-02-04.
 */
public class EmailBodyParserActor
    extends UntypedActor {

  public interface Factory {
    public Actor create();
  }

  private static final boolean DEBUG = false;
  private static final int DB_UPDATE_RETRIES = 3;
  /**
   * Akka Actor Properties
   */
  public final static Props props = Props.create(EmailBodyParserActor.class);

  public static enum EmailBodyCmd {
    TO_TRIP,
    TO_INBOX
  }

  public static class EmailBodyMsg {
    EmailBodyCmd cmd;
    Long eplId;
    String tripId;
    List<String> htmls;
    List<String> texts;

    public EmailBodyMsg(EmailBodyCmd cmd) {
      htmls = new ArrayList<>(2);
      texts = new ArrayList<>(2);
      eplId = null;
      tripId = null;
      this.cmd = cmd;
    }

    public String toString() {
      StringBuilder sb = new StringBuilder("CMD BODY: ");
      sb.append(cmd.name());
      sb.append(" EPLID: ");
      sb.append(eplId);
      sb.append(" Trip ID: ");
      sb.append(tripId);
      sb.append(" Html Count: ");
      sb.append(htmls.size());
      sb.append(" Text Count: ");
      sb.append(texts.size());

      for (String h : htmls) {
        sb.append(">>> HTML:\n");
        if (h.length() >= 200) {
          sb.append(h.substring(0, 200));
        } else {
          sb.append(h);
        }
      }

      for(String t : texts) {
        sb.append(">>> PLAIN:\n");
        if(t.length() >= 200) {
          sb.append(t.substring(0, 200));
        } else {
          sb.append(t);
        }
      }

      return sb.toString();
    }

    public String getTripId() {
      return tripId;
    }

    public void setTripId(String tripId) {
      this.tripId = tripId;
    }

    public Long getEplId() {
      return eplId;
    }

    public void setEplId(Long eplId) {
      this.eplId = eplId;
    }

    public List<String> getHtmls() {
      return htmls;
    }

    public void setHtmls(List<String> htmls) {
      this.htmls = htmls;
    }

    public List<String> getTexts() {
      return texts;
    }

    public void setTexts(List<String> texts) {
      this.texts = texts;
    }

    public void addHtml(String html) {
      htmls.add(html);
    }

    public void addText(String text) {
      texts.add(text);
    }
  }

  private boolean updateTrip(EmailBodyMsg bodyMsg)
      throws  Exception {
    StopWatch sw = new StopWatch();
    sw.start();

    if (bodyMsg.tripId == null || bodyMsg.getEplId() == null) {
      return false;
    }

    EmailParseLog epl = EmailParseLog.find.byId(bodyMsg.eplId);
    if(epl == null) {
      Log.err("EMAIL_Stage_C:TRIP:BODY: Wrong EPLID: " + epl.getPk());
      return false;
    }

    Map<String, EmailBodyExtractor> extractors = EmailUtils.getExtractors(bodyMsg.getTexts(), bodyMsg.getHtmls());
    Trip trip = Trip.find.byId(bodyMsg.getTripId());
    UserProfile up = UserProfile.findByPK(epl.getUserid());
    VOModeller voModeller = new VOModeller(trip, up, false);
    voModeller.setActorType(AuditActorType.EMAIL_USER);

    for (String s : extractors.keySet()) {
      EmailBodyExtractor e = extractors.get(s);
      e.init(null);

      TripVO tripBookings = e.extractData(s);
      if (tripBookings != null && tripBookings.hasValidBookings()) {
        if (tripBookings.getImportSrc() == null) {
          tripBookings.setImportSrc(BookingSrc.ImportSrc.EMAIL_TEXT);
          tripBookings.setImportTs(System.currentTimeMillis());
        }
        if (tripBookings.getImportSrcId() == null) {
          tripBookings.setImportSrcId(epl.getFromAddr());
        }
        voModeller.buildTripVOModels(tripBookings);

        if (e.getParseErrors() != null && e.getParseErrors().size() > 0) {
          Log.info("EMAIL_Stage_C:TRIP:BODY: " + e.getParseErrors().size() +
                   " errors occurred while parsing (EPLID: " + epl.getPk() + ")");
          //handle parse errors
        }

        Log.debug("EMAIL_Stage_C:TRIP:BODY: " + e.getParserName() +
                  " Bookings: " + voModeller.getBookingsCount() + " trip name " +
                  trip.getName() + " sent by " +
                  epl.getFromAddr() + " for token " +
                  epl.getToken().getToken() + " in " + sw.getTime()  + " (EPLID: " + epl.getPk() + ")");
      }
      else {
        Log.err("EMAIL_Stage_C:TRIP:BODY: " + e.getParserName() +
                " Found No usable bookings for: " +
                trip.getName() + " sent by " +
                epl.getFromAddr() + " for token " +
                epl.getToken().getToken() + " in " + sw.getTime()  + " (EPLID: " + epl.getPk() + ")");
      }
    }

    if (voModeller.getBookingsCount() > 0) {
      voModeller.saveAllModels();
      //audit the bookings
      ApiAudit apiAudit = new ApiAudit();
      apiAudit.setPk(DBConnectionMgr.getUniqueId());
      apiAudit.setCmpyid(epl.getToken().getBelongId());
      apiAudit.setUserid(epl.getUserid());
      apiAudit.setCreatedtimestamp(System.currentTimeMillis());
      apiAudit.setNumattachments(epl.getAttachCount());
      apiAudit.setTripid(trip.tripid);
      apiAudit.setApitype(0);
      apiAudit.save();

      //Setting all extractors involved inprocessing of this email
      StringBuilder parserNames = new StringBuilder();
      for (Iterator<EmailBodyExtractor> it = extractors.values().iterator(); it.hasNext(); ) {
        EmailBodyExtractor currItem = it.next();
        parserNames.append(currItem.getParserEnum());
        if(it.hasNext()) {
          parserNames.append(", ");
        }
      }

      for (int retry = 0; retry < DB_UPDATE_RETRIES; retry++) {
        try {
          epl.refresh();
          epl.addParser(parserNames.toString());
          epl.addBkCount(voModeller.getBookingsCount());
          epl.addParseMs(sw.getTime());
          epl.setState(EmailParseLog.EmailState.PARSED_LOCAL);
          epl.update();
          break;
        }
        catch (OptimisticLockException ole) {
          //Ignoring on purpose
        }
      }

      //Cached RAW text is no longer needed - deleting it
      CacheMgr.set(APPConstants.CACHE_EMAIL_RAW_PREFIX + epl.getPkString(), null);
      return true;
    }
    else if (ConfigMgr.getAppParameter(CoreConstants.WORLDMATE_ADDR) != null &&
             ConfigMgr.getAppParameter(CoreConstants.WORLDMATE_ADDR).length() > 0 &&
             epl.getAttachCount() == 0) {
      //let's try to send to worldmate if we are in prod
      Log.debug("EMAIL_Stage_C:TRIP:BODY: Sending to Worldmate: Subj " + epl.getSubject() +
                " sent by " + epl.getFromAddr() +
                " for cmpy " + epl.getToken().getBelongId() +
                " in " + sw.getTime() +
                " (EPLID:"+epl.getPk()+")" );
      epl.addParseMs(sw.getTime());
      EmailController.worldMateForward(epl);
      return false;
    } else {

      for (int retry = 0; retry < DB_UPDATE_RETRIES; retry++) {
        try {
          epl.refresh();
          epl.addParseMs(sw.getTime());
          epl.setState(EmailParseLog.EmailState.PARSED_LOCAL);
          epl.update();
          break;
        }
        catch (OptimisticLockException ole) {
          //Ignoring on purpose
        }
      }

      //let's save this as a trip note
      EmailController.attachTripNote(trip.getTripid(), epl.getPk());

      Log.info("EMAIL_Stage_C:TRIP:BODY: Finished parsing email body. " +
               "Nothing found. No other action (EPL ID"+ epl.getPk() + ")");
    }
    return false;
  }

  private boolean updateInbox(EmailBodyMsg bodyMsg) {
    throw new NotImplementedException("Inbox creation is coming");
    //return false;
  }

  @Override
  public void onReceive(Object message)
      throws Exception {

    if(message instanceof EmailBodyMsg) {

      StopWatch sw = new StopWatch();
      sw.start();
      EmailBodyMsg bodyMsg = (EmailBodyMsg) message;

      Log.debug(bodyMsg.toString());

      boolean result = false;
      switch (bodyMsg.cmd) {
        case TO_INBOX:
          result = updateInbox(bodyMsg);
          break;
        case TO_TRIP:
          result = updateTrip(bodyMsg);
          break;
      }
      Log.debug("EMAIL_Stage_C:BODY: Parsing Result:" + result);
    }
  }
}
