package models.publisher;

import com.mapped.publisher.persistence.RecordState;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2014-10-16.
 */
@Entity @Table(name = "poi_type")
public class PoiType
    extends Model {
  public static Model.Finder<String, PoiType> find = new Finder(PoiType.class);
  @Id
  private Integer typeId;
  private Integer parentTypeId;
  private String name;
  private int priority;

  @Column(name = "state")
  @Enumerated(EnumType.STRING)
  private RecordState state;
  @Version
  private int version;

  public static List<PoiType> findAll() {
    return find.all();
  }

  public RecordState getState() {
    return state;
  }

  public void setState(RecordState state) {
    this.state = state;
  }

  public void setState(String state) {
    this.state = RecordState.valueOf(state);
  }

  public Integer getTypeId() {
    return typeId;
  }

  public void setTypeId(Integer typeId) {
    this.typeId = typeId;
  }

  public Integer getParentTypeId() {
    return parentTypeId;
  }

  public void setParentTypeId(Integer parentTypeId) {
    this.parentTypeId = parentTypeId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getPriority() {
    return priority;
  }

  public void setPriority(int priority) {
    this.priority = priority;
  }


  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
