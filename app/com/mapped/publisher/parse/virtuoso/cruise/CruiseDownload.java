package com.mapped.publisher.parse.virtuoso.cruise;

import com.mapped.publisher.utils.Log;
import org.apache.http.*;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.GzipDecompressingEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by twong on 15-05-19.
 */
public class CruiseDownload {

  private static Pattern resultsPattern =   Pattern.compile("\"data-total-results.*.[0-9]+\"");
  private static Pattern cruiseUrlPattern = Pattern.compile("data-url=\"[\\-A-Za-z0-9/]+\"");
  private static Pattern numResultsPattern = Pattern.compile("[0-9]+");

  /*
  Returns a map of cruiseid / cruise url
   */
  public static Map<String, String> parseCruises( String url, String startDate, String endDate, String term, int maxRows) {
    HashMap<String, String> cruiseFullUrl = new HashMap<>();

    try {
          /*
          curl 'http://www.virtuoso.com/search/ajax/getsearchview'
              -H 'Pragma: no-cache'
              -H 'Origin: http://www.virtuoso.Accept-Encoding: gzip, deflate'
              -H 'Accept-Language: en-US,en;q=0.8,pt;q=0.6,pt-BR;q=0.4,fr;q=0.2'
              -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36'
              -H 'Content-Type: application/json; charset=UTF-8'
              -H 'Accept: text/html, /; q=0.01'
              -H 'Cache-Control: no-cache'
              -H 'X-Requested-With: XMLHttpRequest'
              -H 'Cookie: slc=swovey1AL1hgEmONstVCimy9y4x5ROQ9gklCqlyL6TFxG4NnvzpeiVTS1cqLLP3e; CookiesEnabled=true; ASP.NET_SessionId=zin4m2bvizp3mjv1tjed2wuy; ATC=LastLoggedInAs=Anonymous&HasLoggedInBefore=0&SessionId=zin4m2bvizp3mjv1tjed2wuy'
              --data-binary '{"options":{"CurrentPage":1,"FacetCategoryIndex":0,"FacetCategoryTitle":"","FacetLimit":5,"LeftToShow":0,"ProductIds":[],"RowsPerPage":10,"SearchMode":"","SearchTerms":"","SearchType":"Cruise","SearchView":"1col","SelectedFacets":[{"Category":"cruise_departure_months","Name":"August 2015","Selected":true}],"SortType":"CruiseTravelDateAsc","StartRow":0}}'
              --compressed
          */


      int        currentPage = 1;
      int        maxPage     = 1;

      //TODO: !!!! Logic is converted to HttpClient 4.5 API but was not tested (2016-10-25)
      BasicCookieStore cookieStore = new BasicCookieStore();

      HttpResponseInterceptor interceptor = (response, context) -> {
        HttpEntity entity = response.getEntity();
        if (entity != null) {
          Header ceheader = entity.getContentEncoding();
          if (ceheader != null) {
            HeaderElement[] codecs = ceheader.getElements();
            for (int i = 0; i < codecs.length; i++) {
              if (codecs[i].getName().equalsIgnoreCase("gzip")) {
                response.setEntity(
                    new GzipDecompressingEntity(response.getEntity()));
                return;
              }
            }
          }
        }
      };

      CloseableHttpClient c = HttpClientBuilder.create()
                                               .setDefaultCookieStore(cookieStore)
                                               .addInterceptorFirst(interceptor)
                                               .build();

      if (term == null) {
        term ="";
      }

      HttpPost p = getPost(c, cookieStore, url, currentPage, maxRows, startDate, endDate, term);

      HttpResponse r = c.execute(p);

      ResponseHandler<String> handler = new BasicResponseHandler();

      String body = handler.handleResponse(r);
      int code = r.getStatusLine().getStatusCode();

      //System.out.println(code);
      //System.out.println(body);

      if (code == 200) {
        Matcher resultsMatcher = resultsPattern.matcher(body);
        int results = 0;

        if (resultsMatcher.find()) {
          Matcher numResults = numResultsPattern.matcher(resultsMatcher.group());
          if (numResults.find())
            results = Integer.parseInt(numResults.group());
        }

        maxPage = (int) Math.floor(results / maxRows);
        if (results > 0 && maxPage == 0) {
          maxPage = 1;
        }


        System.out.println("Num Results: " + results + " num of pages: " + maxPage);

        while (currentPage <= maxPage) {
          Matcher cruiseUrl = cruiseUrlPattern.matcher(body);
          int rowsExtracted = 0;
          while (cruiseUrl.find()) {
            String s = cruiseUrl.group();
            String cruiseName = s.replaceAll("\"", "");
            cruiseName = cruiseName.substring(cruiseName.indexOf("=") + 1);
            String[] tokens = cruiseName.split("/");
            String cruiseId = tokens[2];
            String fullUrl = "https://www.virtuoso.com:443" + cruiseName;

            if (!cruiseFullUrl.containsKey(cruiseId)) {
              cruiseFullUrl.put(cruiseId, fullUrl);
              rowsExtracted++;

            }
          }

          //alert if we get less than expected rows
          if(currentPage!=maxPage && rowsExtracted < maxRows) {
            Log.err("Virtuoso Cruise Parser: Cannot get max rows for " + p.getURI().getRawPath() + ". Expected " + maxRows + " rows. Got " + rowsExtracted + " instead.");
          }

          //get the next page
          if (currentPage < maxPage && rowsExtracted > 0) {
            currentPage++;
            p = getPost(c, cookieStore, url, currentPage, maxRows, startDate, endDate, term);
            r = c.execute(p);
            handler = new BasicResponseHandler();
            code = r.getStatusLine().getStatusCode();
            if (code != 200) {
              Log.err("Virtuoso Cruise Parser: Cannot get " + p.getURI().getRawPath());
              break;
            }
            body = handler.handleResponse(r);
          } else {
            break;
          }
        }
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return cruiseFullUrl;
  }


  private static HttpPost getPost (HttpClient c, CookieStore cookieStore,
                                   String url, int currentPage, int maxRow,
                                   String startDate,
                                   String endDate, String term) throws Exception {
    HttpPost p = new HttpPost(url);     //use service id in the future

    p.setHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36");
    p.setHeader("Pragma","no-cache");
    p.setHeader("Origin", "http://www.virtuoso.com");
    p.setHeader("Accept-Encoding", "gzip, deflate");
    p.setHeader("Accept-Language", "en-US,en;q=0.8,pt;q=0.6,pt-BR;q=0.4,fr;q=0.2");
    p.setHeader("Accept", "text/html, */*; q=0.01");
    p.setHeader("Cache-Control", "no-cache");
    p.setHeader("X-Requested-With", "XMLHttpRequest");
    p.setHeader("Content-Type","application/json; charset=UTF-8");
    p.setHeader("Referer", "http://www.virtuoso.com/cruises");

    int startRow = 0;
    if (currentPage > 1) {
      startRow = (maxRow * currentPage) + 1 ;
    }


/*

    StringEntity se = new StringEntity("{\"options\":{\"CurrentPage\":" + currentPage + ",\"FacetCategoryIndex\":0,\"FacetCategoryTitle\":\"\"," +
                                       "\"FacetLimit\":5 ,\"LeftToShow\":0,\"ProductIds\":[],\"RowsPerPage\":" + maxRow +  ",\"SearchMode\":\"\"," +
                                       "\"SearchTerms\":\""+ term +"\",\"SearchType\":\"Cruise\",\"SearchView\":\"1col\"," +
                                       "\"SelectedFacets\":[{\"Category\":\"cruise_departure_months\",\"Name\":\""+ month + "\"," +
                                       "\"Selected\":true}],\"SortType\":\"CruiseTravelDateAsc\",\"StartRow\":" + startRow + "}}");
                                       */

    StringEntity se = new StringEntity("{\"options\":{\"ClientSideOptions\":{\"CurrentPage\":" + currentPage + "," +
                                       "\"FacetCategoryIndex\":0,\"FacetCategoryTitle\":\"\",\"FacetLimit\":5," +
                                       "\"LeftToShow\":0,\"ProductIds\":[],\"RowsPerPage\":" + maxRow +  ",\"SearchMode\":\"\"," +
                                       "\"SearchTerms\":\""+ term + "\",\"SearchType\":\"Cruise\"," +
                                       "\"SearchView\":\"1col\",\"SelectedFacets\":[]," +
                                       "\"SortType\":\"CruiseTravelDateAsc\",\"StartRow\":" + startRow + "," +
                                       "\"TravelProductStartDate\":\"" +  startDate +"\"," +
                                       "\"TravelProductEndDate\":\"" + endDate + "\"},\"SearchTerms\":\"\"}}");

    p.setEntity(se);


    //TODO: !!! CookieStore logic needs to be verified after migration to HttpClient 4.5 API
    cookieStore.clear();

    BasicClientCookie cookie1 = new BasicClientCookie("slc", "PnOD/JKmoQNlG4NOlt8qZG84LaMk227BuZeFMplkPW3FYxchZL4cWsUv/o+mjYPJ");
    BasicClientCookie cookie2 = new BasicClientCookie("CookiesEnabled","true");
    BasicClientCookie cookie3 = new BasicClientCookie("ASP.NET_SessionId","lv55fp1laoi5ynl3oyrxvzcd");
    BasicClientCookie cookie4 = new BasicClientCookie("ATC", "LastLoggedInAs=Anonymous&HasLoggedInBefore=0&SessionId=lv55fp1laoi5ynl3oyrxvzcd");
    BasicClientCookie cookie5 = new BasicClientCookie("CMSPreferredCulture","en-US");
    BasicClientCookie cookie6 = new BasicClientCookie("cruisesCatalogContentLastModified","Wed%20Mar%2016%202016%2020%3A57%3A32%20GMT-0400%20(EDT)");
    BasicClientCookie cookie7 = new BasicClientCookie("cruisesCatalogContent","open");
    BasicClientCookie cookie8 = new BasicClientCookie("HotelsCatalogContent","open");
    BasicClientCookie cookie9 = new BasicClientCookie("HotelsCatalogContentLastModified","Tue%20Oct%2013%202015%2014%3A10%3A52%20GMT-0400%20(EDT)");
    BasicClientCookie cookie10 = new BasicClientCookie("fuu","1");


    cookie1.setPath("/");
    cookie1.setDomain(".virtuoso.com");
    cookie2.setPath("/");
    cookie2.setDomain(".virtuoso.com");
    cookie3.setPath("/");
    cookie3.setDomain(".virtuoso.com");
    cookie4.setPath("/");
    cookie4.setDomain(".virtuoso.com");

    cookieStore.addCookie(cookie1);
    cookieStore.addCookie(cookie2);
    cookieStore.addCookie(cookie3);
    cookieStore.addCookie(cookie4);
    cookieStore.addCookie(cookie5);
    cookieStore.addCookie(cookie6);
    cookieStore.addCookie(cookie7);
    cookieStore.addCookie(cookie8);
    cookieStore.addCookie(cookie9);
    cookieStore.addCookie(cookie10);

    return p;
  }

  public static void main (String[] args) {
    parseCruises("http://www.virtuoso.com/search/ajax/getsearchview", "2016-10-01T00:00:00.000Z", "2016-10-31T00:00:00.000Z","Crystal Cruises", 5);
  }
}
