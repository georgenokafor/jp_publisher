//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.04.17 at 04:55:33 PM EDT 
//


package com.umapped.itinerary.analyze.supplier.travelbound.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_HotelInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_HotelInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddressLines" type="{}t_AddressLines" minOccurs="0"/>
 *         &lt;element name="StarRating" type="{}t_StarRating" minOccurs="0"/>
 *         &lt;element name="Category" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AreaDetails" type="{}t_AreaDetails" minOccurs="0"/>
 *         &lt;element name="Reports" type="{}t_Reports" minOccurs="0"/>
 *         &lt;element name="RoomCategories" type="{}t_RoomCategories" minOccurs="0"/>
 *         &lt;element name="RoomTypes" type="{}t_RoomTypes" minOccurs="0"/>
 *         &lt;element name="RoomFacilities" type="{}t_Facilities" minOccurs="0"/>
 *         &lt;element name="Facilities" type="{}t_Facilities" minOccurs="0"/>
 *         &lt;element name="Links" type="{}t_Links" minOccurs="0"/>
 *         &lt;element name="GeoCodes" type="{}t_GeoCodes" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_HotelInformation", propOrder = {
    "addressLines",
    "starRating",
    "category",
    "areaDetails",
    "reports",
    "roomCategories",
    "roomTypes",
    "roomFacilities",
    "facilities",
    "links",
    "geoCodes"
})
public class THotelInformation {

    @XmlElement(name = "AddressLines")
    protected TAddressLines addressLines;
    @XmlElement(name = "StarRating")
    protected TStarRating starRating;
    @XmlElement(name = "Category")
    protected String category;
    @XmlElement(name = "AreaDetails")
    protected TAreaDetails areaDetails;
    @XmlElement(name = "Reports")
    protected TReports reports;
    @XmlElement(name = "RoomCategories")
    protected TRoomCategories roomCategories;
    @XmlElement(name = "RoomTypes")
    protected TRoomTypes roomTypes;
    @XmlElement(name = "RoomFacilities")
    protected TFacilities roomFacilities;
    @XmlElement(name = "Facilities")
    protected TFacilities facilities;
    @XmlElement(name = "Links")
    protected TLinks links;
    @XmlElement(name = "GeoCodes")
    protected TGeoCodes geoCodes;

    /**
     * Gets the value of the addressLines property.
     * 
     * @return
     *     possible object is
     *     {@link TAddressLines }
     *     
     */
    public TAddressLines getAddressLines() {
        return addressLines;
    }

    /**
     * Sets the value of the addressLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link TAddressLines }
     *     
     */
    public void setAddressLines(TAddressLines value) {
        this.addressLines = value;
    }

    /**
     * Gets the value of the starRating property.
     * 
     * @return
     *     possible object is
     *     {@link TStarRating }
     *     
     */
    public TStarRating getStarRating() {
        return starRating;
    }

    /**
     * Sets the value of the starRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link TStarRating }
     *     
     */
    public void setStarRating(TStarRating value) {
        this.starRating = value;
    }

    /**
     * Gets the value of the category property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategory(String value) {
        this.category = value;
    }

    /**
     * Gets the value of the areaDetails property.
     * 
     * @return
     *     possible object is
     *     {@link TAreaDetails }
     *     
     */
    public TAreaDetails getAreaDetails() {
        return areaDetails;
    }

    /**
     * Sets the value of the areaDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link TAreaDetails }
     *     
     */
    public void setAreaDetails(TAreaDetails value) {
        this.areaDetails = value;
    }

    /**
     * Gets the value of the reports property.
     * 
     * @return
     *     possible object is
     *     {@link TReports }
     *     
     */
    public TReports getReports() {
        return reports;
    }

    /**
     * Sets the value of the reports property.
     * 
     * @param value
     *     allowed object is
     *     {@link TReports }
     *     
     */
    public void setReports(TReports value) {
        this.reports = value;
    }

    /**
     * Gets the value of the roomCategories property.
     * 
     * @return
     *     possible object is
     *     {@link TRoomCategories }
     *     
     */
    public TRoomCategories getRoomCategories() {
        return roomCategories;
    }

    /**
     * Sets the value of the roomCategories property.
     * 
     * @param value
     *     allowed object is
     *     {@link TRoomCategories }
     *     
     */
    public void setRoomCategories(TRoomCategories value) {
        this.roomCategories = value;
    }

    /**
     * Gets the value of the roomTypes property.
     * 
     * @return
     *     possible object is
     *     {@link TRoomTypes }
     *     
     */
    public TRoomTypes getRoomTypes() {
        return roomTypes;
    }

    /**
     * Sets the value of the roomTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link TRoomTypes }
     *     
     */
    public void setRoomTypes(TRoomTypes value) {
        this.roomTypes = value;
    }

    /**
     * Gets the value of the roomFacilities property.
     * 
     * @return
     *     possible object is
     *     {@link TFacilities }
     *     
     */
    public TFacilities getRoomFacilities() {
        return roomFacilities;
    }

    /**
     * Sets the value of the roomFacilities property.
     * 
     * @param value
     *     allowed object is
     *     {@link TFacilities }
     *     
     */
    public void setRoomFacilities(TFacilities value) {
        this.roomFacilities = value;
    }

    /**
     * Gets the value of the facilities property.
     * 
     * @return
     *     possible object is
     *     {@link TFacilities }
     *     
     */
    public TFacilities getFacilities() {
        return facilities;
    }

    /**
     * Sets the value of the facilities property.
     * 
     * @param value
     *     allowed object is
     *     {@link TFacilities }
     *     
     */
    public void setFacilities(TFacilities value) {
        this.facilities = value;
    }

    /**
     * Gets the value of the links property.
     * 
     * @return
     *     possible object is
     *     {@link TLinks }
     *     
     */
    public TLinks getLinks() {
        return links;
    }

    /**
     * Sets the value of the links property.
     * 
     * @param value
     *     allowed object is
     *     {@link TLinks }
     *     
     */
    public void setLinks(TLinks value) {
        this.links = value;
    }

    /**
     * Gets the value of the geoCodes property.
     * 
     * @return
     *     possible object is
     *     {@link TGeoCodes }
     *     
     */
    public TGeoCodes getGeoCodes() {
        return geoCodes;
    }

    /**
     * Sets the value of the geoCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link TGeoCodes }
     *     
     */
    public void setGeoCodes(TGeoCodes value) {
        this.geoCodes = value;
    }

}
