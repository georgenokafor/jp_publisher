package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class UserPreferencesMgr {

	 private static final String SELECT_ALL = "Select Pk, UsersId, SummaryEmail, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from UserPreferences;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, UsersId, SummaryEmail, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from UserPreferences where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from UserPreferences where Pk = ?;";
	 private static final String INSERT = "insert into UserPreferences (Pk, UsersId, SummaryEmail, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp) values (?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update UserPreferences SET UsersId=?, SummaryEmail=?, SyncTimestamp=?, LastUpdateTimestamp=? WHERE Pk = ?; ";
	 private static final String SELECT_ALL_BY_USERS_ID = "Select Pk, UsersId, SummaryEmail, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from UserPreferences where UsersId=? ";
	 private static final String DELETE_BY_USERS_ID = "delete from UserPreferences where UsersId=? ";
	 public static ArrayList <UserPreferences> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return UserPreferencesMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static UserPreferences getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<UserPreferences> results = UserPreferencesMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (UserPreferences c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getUsersId());
				 prep.setString(3, c.getSummaryEmail());
				 prep.setLong(4, c.getSyncTimestamp());
				 prep.setLong(5, c.getLastUpdateTimestamp());
				 prep.setLong(6, c.getCreateTimestamp());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (UserPreferences c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getUsersId());
				 prep.setString(2, c.getSummaryEmail());
				 prep.setLong(3, c.getSyncTimestamp());
				 prep.setLong(4, c.getLastUpdateTimestamp());
				 prep.setString(5, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <UserPreferences> getAllByUsersId(String id) throws SQLException {
		 ResultSet rs = null;
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_USERS_ID);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 return UserPreferencesMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void deleteByUsersId (String id) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_USERS_ID);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <UserPreferences> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <UserPreferences> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 UserPreferences c = new UserPreferences();
			 c.setPk(rs.getString("Pk"));
			 c.setUsersId(rs.getString("UsersId"));
			 c.setSummaryEmail(rs.getString("SummaryEmail"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 results.add(c);
		 }
		 return results;
	 }
}
