package com.umapped.mobile.api.model;

import java.io.Serializable;

/**
 * Created by wei on 2017-07-17.
 */
public class GetItinerariesRequest
    implements Serializable {
  private long accountId;
  private Long lastSyncTimestamp;

  public long getAccountId() {
    return accountId;
  }

  public void setAccountId(long accountId) {
    this.accountId = accountId;
  }

  public Long getLastSyncTimestamp() {
    return lastSyncTimestamp;
  }

  public void setLastSyncTimestamp(Long lastSyncTimestamp) {
    this.lastSyncTimestamp = lastSyncTimestamp;
  }
}
