
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetExcursionMediaByExcursionIDResult" type="{http://webapi.globusandcosmos.com/}ExcursionMedia" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getExcursionMediaByExcursionIDResult"
})
@XmlRootElement(name = "GetExcursionMediaByExcursionIDResponse")
public class GetExcursionMediaByExcursionIDResponse {

    @XmlElement(name = "GetExcursionMediaByExcursionIDResult")
    protected ExcursionMedia getExcursionMediaByExcursionIDResult;

    /**
     * Gets the value of the getExcursionMediaByExcursionIDResult property.
     * 
     * @return
     *     possible object is
     *     {@link ExcursionMedia }
     *     
     */
    public ExcursionMedia getGetExcursionMediaByExcursionIDResult() {
        return getExcursionMediaByExcursionIDResult;
    }

    /**
     * Sets the value of the getExcursionMediaByExcursionIDResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExcursionMedia }
     *     
     */
    public void setGetExcursionMediaByExcursionIDResult(ExcursionMedia value) {
        this.getExcursionMediaByExcursionIDResult = value;
    }

}
