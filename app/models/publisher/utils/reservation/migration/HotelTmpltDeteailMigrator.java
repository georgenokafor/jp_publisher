package models.publisher.utils.reservation.migration;

import com.mapped.publisher.utils.Log;
import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;

import models.publisher.TmpltDetails;
import models.publisher.TmpltHotel;
import models.utils.migration.Migrator;

public class HotelTmpltDeteailMigrator implements Migrator<TmpltDetails> {

  private UmAccommodationReservationBuilder builder = new UmAccommodationReservationBuilder();

  @Override
  public boolean migrate(TmpltDetails entity) {
    TmpltHotel hotelBooking = TmpltHotel.find.byId(entity.getDetailsid());
    Log.info("Migrate a hotel trip_detail");
    UmAccommodationReservation reservation = builder.build(entity, hotelBooking);
    entity.setReservation(reservation);
    return true;
  }
}
