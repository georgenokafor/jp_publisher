package com.umapped.itinerary.analyze.segment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.model.Location;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by wei on 2017-03-01.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TripSegment {
  private ZonedDateTime startDateTime;
  private ZonedDateTime endDateTime;

  private Location location;

  private ItineraryItem mainItem;

  private Location startLocation;
  private Location endLocation;

  private ItineraryItem arrive;
  private ItineraryItem depart;

  private List<ItineraryItem> accommodations;

  private List<ItineraryItem> activities;
  private List<ItineraryItem> cruiseStops;
  private List<ItineraryItem> transfers;
  private Set<SegmentNature> natures;

  private TransitCategory category;

  public TripSegment() {
    this(null);
  }

  public TripSegment(TransitCategory category) {
    this.category = category;
    natures = new HashSet<>();
  }


  public void addNature(SegmentNature nature) {
    natures.add(nature);
  }

  public Set<SegmentNature> getNatures() {
    return natures;
  }

  public ZonedDateTime getStartDateTime() {
    return startDateTime;
  }

  public TripSegment setStartDateTime(ZonedDateTime startDateTime) {
    this.startDateTime = startDateTime;
    return this;
  }

  public ZonedDateTime getEndDateTime() {
    return endDateTime;
  }

  public TripSegment setEndDateTime(ZonedDateTime endDateTime) {
    this.endDateTime = endDateTime;
    return this;
  }

  public Location getLocation() {
    return location;
  }

  public TripSegment setLocation(Location location) {
    this.location = location;
    return this;
  }

  public ItineraryItem getArrive() {
    return arrive;
  }

  public TripSegment setArrive(ItineraryItem arrive) {
    this.arrive = arrive;
    return this;
  }

  public ItineraryItem getDepart() {
    return depart;
  }

  public TripSegment setDepart(ItineraryItem depart) {
    this.depart = depart;
    return this;
  }

  public List<ItineraryItem> getAccommodations() {
    return accommodations;
  }

  public TripSegment setAccommodations(List<ItineraryItem> accommodations) {
    this.accommodations = accommodations;
    return this;
  }

  public List<ItineraryItem> getActivities() {
    return activities;
  }

  public TripSegment setActivities(List<ItineraryItem> activities) {
    this.activities = activities;
    return this;
  }

  public List<ItineraryItem> getTransfers() {
    return transfers;
  }

  public TripSegment setTransfers(List<ItineraryItem> transfers) {
    this.transfers = transfers;
    return this;
  }

  public TripSegment addTransfer(ItineraryItem transfer) {
    if (this.transfers == null) {
      transfers = new ArrayList<>();
    }
    transfers.add(transfer);
    return this;
  }

  public TripSegment addCruiseStop(ItineraryItem stop) {
    if (this.cruiseStops == null) {
      cruiseStops = new ArrayList<>();
    }
    cruiseStops.add(stop);
    return this;
  }

  public List<ItineraryItem> getCruiseStops() {
    return cruiseStops;
  }

  public boolean contains(ZonedDateTime dt) {
    if (!dt.isBefore(startDateTime)  && !dt.isAfter(endDateTime)) {
      return true;
    }
    return false;
  }

  public Location getStartLocation() {
    return startLocation;
  }

  public TripSegment setStartLocation(Location startLocation) {
    this.startLocation = startLocation;
    return this;
  }

  public Location getEndLocation() {
    return endLocation;
  }

  public TripSegment setEndLocation(Location endLocation) {
    this.endLocation = endLocation;
    return this;
  }

  public boolean hasNature(SegmentNature nature) {
    return natures != null && natures.contains(nature);
  }

  public void addAccommodation(ItineraryItem item) {
    if (accommodations == null) {
      accommodations = new ArrayList<>();
    }
    accommodations.add(item);
  }

  public void addActivity(ItineraryItem item) {
    if (activities == null) {
      activities = new ArrayList<>();
    }
    activities.add(item);

  }

  public TransitCategory getCategory() {
    return category;
  }

  public TripSegment setCategory(TransitCategory category) {
    this.category = category;
    return this;
  }

  public ItineraryItem getMainItem() {
    return mainItem;
  }

  public TripSegment setMainItem(ItineraryItem mainItem) {
    this.mainItem = mainItem;
    return this;
  }

  public boolean isEmpty() {
    return ((startDateTime == null || endDateTime == null) && mainItem == null
      && accommodations == null && activities == null && transfers == null);
  }

  public int getAccommodationCount() {
    return accommodations != null ? accommodations.size() : 0;
  }

  public int getActivityCount() {
    return activities != null ? activities.size() : 0;
  }

  public long getDurationHours() {
    if (startDateTime != null && endDateTime != null) {
      return Duration.between(startDateTime, endDateTime).toHours();
    } else {
      return 0;
    }
  }
}
