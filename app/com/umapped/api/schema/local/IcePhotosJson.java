package com.umapped.api.schema.local;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.umapped.api.schema.common.IBodyJson;
import com.umapped.api.schema.types.Operation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by surge on 2015-02-25.
 */
@JsonTypeName("ICEPHOTOS")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IcePhotosJson
    extends PoiPhotosJson {

  /**
   * Ice Portal Suggestion if no direct link present
   */
  public static class Suggestion {
    public String poiId; //Not required for outbound
    public String id;   //Ice Portal Feed Import ID
    public String name;
    public String countryCode;
    public String region;
    public String locality;
    public String address;
    public String postalCode;
    public String phone;
    public int    score;
  }

  /**
   * Suggestions are always sent back not received from
   */
  @JsonProperty(required = false, value = "suggestions")
  public List<Suggestion> suggestions;



  public IcePhotosJson() {
    super();
    suggestions = new ArrayList<>(5);
  }

  public Suggestion addSuggestion(Long id, String name, String countryCode){
    Suggestion suggest = new Suggestion();
    suggest.id = Long.toString(id);
    suggest.name = name;
    suggest.countryCode = countryCode;

    suggestions.add(suggest);
    return suggest;
  }
}
