//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.04.17 at 04:55:33 PM EDT 
//


package com.umapped.itinerary.analyze.supplier.travelbound.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_PickUpStation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_PickUpStation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Station" type="{}t_Station" minOccurs="0"/>
 *         &lt;element name="ArrivingFrom" type="{}t_Where" minOccurs="0"/>
 *         &lt;element name="TrainName" type="{}t_Input50" minOccurs="0"/>
 *         &lt;element name="EstimatedArrival" type="{}t_Time" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_PickUpStation", propOrder = {
    "station",
    "arrivingFrom",
    "trainName",
    "estimatedArrival"
})
public class TPickUpStation {

    @XmlElement(name = "Station")
    protected TStation station;
    @XmlElement(name = "ArrivingFrom")
    protected TWhere arrivingFrom;
    @XmlElement(name = "TrainName")
    protected String trainName;
    @XmlElement(name = "EstimatedArrival")
    protected String estimatedArrival;

    /**
     * Gets the value of the station property.
     * 
     * @return
     *     possible object is
     *     {@link TStation }
     *     
     */
    public TStation getStation() {
        return station;
    }

    /**
     * Sets the value of the station property.
     * 
     * @param value
     *     allowed object is
     *     {@link TStation }
     *     
     */
    public void setStation(TStation value) {
        this.station = value;
    }

    /**
     * Gets the value of the arrivingFrom property.
     * 
     * @return
     *     possible object is
     *     {@link TWhere }
     *     
     */
    public TWhere getArrivingFrom() {
        return arrivingFrom;
    }

    /**
     * Sets the value of the arrivingFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link TWhere }
     *     
     */
    public void setArrivingFrom(TWhere value) {
        this.arrivingFrom = value;
    }

    /**
     * Gets the value of the trainName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrainName() {
        return trainName;
    }

    /**
     * Sets the value of the trainName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrainName(String value) {
        this.trainName = value;
    }

    /**
     * Gets the value of the estimatedArrival property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstimatedArrival() {
        return estimatedArrival;
    }

    /**
     * Sets the value of the estimatedArrival property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstimatedArrival(String value) {
        this.estimatedArrival = value;
    }

}
