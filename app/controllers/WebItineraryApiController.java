package controllers;

import com.avaje.ebean.Ebean;
import com.google.inject.Inject;
import com.mapped.persistence.*;
import com.mapped.publisher.actions.AccountJwtAuth;
import com.mapped.publisher.actions.GuestJwtAuth;
import com.mapped.publisher.common.*;
import com.mapped.publisher.js.*;
import com.mapped.publisher.js.FullCalendar.Event;
import com.mapped.publisher.parse.extractor.booking.email.ReservationsHolder;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.parse.globus.NumFacet;
import com.mapped.publisher.parse.schemaorg.*;
import com.mapped.publisher.persistence.PoiTypeInfo;
import com.mapped.publisher.persistence.RecordStatus;
import com.mapped.publisher.persistence.VOModeller;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.persistence.communicator.MessengerUpdateHelper;
import com.mapped.publisher.persistence.redis.RedisKeys;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.NoNamesWriter;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.umapped.BodyParsers.TolerantText1Mb;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.helper.TripPublisherHelper;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.helpers.TripReservationsHelper;
import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;
import com.umapped.persistence.reservation.activity.UmActivityReservation;
import com.umapped.persistence.reservation.cruise.UmCruiseReservation;
import com.umapped.persistence.reservation.transfer.UmTransferReservation;
import com.umapped.service.offer.ItineraryOffer;
import com.umapped.service.offer.LocationOffer;
import com.umapped.service.offer.OfferService;
import com.umapped.service.offer.UMappedCity;
import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import models.publisher.*;
import org.apache.commons.lang3.time.StopWatch;
import play.data.Form;
import play.data.FormFactory;
import play.data.validation.Constraints;
import play.db.DB;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLStreamException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

/**
 * Controller for all things Web Itinerary NG (using simple JSON APIs)
 * Created by surge on 2016-01-21.
 */
public class WebItineraryApiController
    extends Controller {

  /**
   * Disables caching
   */
  private static boolean DEBUG_NO_CACHE = false;

  @Inject
  RedisMgr redis;

  @Inject
  TripPublisherHelper tripPublisherHelper;

  @Inject
  FormFactory formFactory;

  @Inject
  HttpExecutionContext ec;

  @Inject private OfferService offerService;

  public static class FormApiLogin {
    @Constraints.Required
    public String email;
    @Constraints.Required
    public String tripid;
    /*
    @Constraints.Required
    public String password;
    */
  }

  public static AccountTripLink isTraveller(Account a, Trip trip) {
    AccountTripLink atl = AccountTripLink.findByUid(a.getUid(), trip.getTripid());
    if(atl == null || atl.getLinkType() != AccountDetailLink.DetailLinkType.TRAVELER) {
      return null;
    }
    return atl;
  }

  public Result auth() {
    StopWatch sw = new StopWatch();
    sw.start();
    Form<FormApiLogin> form = formFactory.form(FormApiLogin.class).bindFromRequest();

    if(form.hasErrors()) {
      return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_MISSING_DATA);
    }

    FormApiLogin loginForm    = form.get();

    JwtToken jwtToken = null;
    Trip trip = Trip.findByPK(loginForm.tripid);
    if(trip == null || trip.getStatus() == APPConstants.STATUS_DELETED) {
      return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "Please, use provided web itinerary access" +
                                                                           " link. Trip not found.");
    }

    if (loginForm.email != null && loginForm.email.length() > 0
            && (loginForm.email.trim().isEmpty() || loginForm.email.equals("internal.guest.access@umapped.com"))) {
      //check guest access to the trip

      Company cmpy = Company.find.byId(trip.getCmpyid());

      if (hasGuestAccess(cmpy)) {
        jwtToken = authorizeGuest(trip, sw);
      }
    } else {
      Account a = Account.findByEmail(loginForm.email);
      if (a == null) {
        return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "Account not found");
      }

      if (a.getState() == RecordStatus.DELETED) {
        return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "Account disabled");
      }

      SecurityMgr.AccessLevel al = SecurityMgr.getTripAccessLevel(trip, a);
      if (al == SecurityMgr.AccessLevel.NONE) {
        return BaseJsonResponse.codeResponse(ReturnCode.AUTH_TRIP_FAIL);
      }
      jwtToken = authorizeUser(loginForm, a, sw, trip, al);
    }

    if (jwtToken != null)
      return jwtToken.toJsonResult();
    else
      return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "Please, use provided web itinerary access" +
                                                                           " link. Trip not found.");
  }

  private JwtToken authorizeUser (FormApiLogin loginForm, Account a, StopWatch sw, Trip trip, SecurityMgr.AccessLevel al) {


    /* COMMENTED OUT FOR NOW SINCE WE ARE NOT CHECKING PASSWORDS INITIALLY :(
    AccountAuth aAuth = AccountAuth.getPassword(a.getUid());
    if(aAuth == null) {
      return BaseJsonResponse.codeResponse(ReturnCode.AUTH_ACCOUNT_PWD_NOT_SET);
    }

    if(aAuth.getExpire() != null && aAuth.getExpire() > System.currentTimeMillis()) {
      return BaseJsonResponse.codeResponse(ReturnCode.AUTH_ACCOUNT_PWD_EXPIRED);
    }

    String pwdHash = Utils.hashPwd(loginForm.password, aAuth.getSalt());
    if(!pwdHash.equals(aAuth.getValue())) {
      return BaseJsonResponse.codeResponse(ReturnCode.AUTH_ACCOUNT_PWD_FAIL);
    }
    //Updating login
    aAuth.updateOnSuccess();
    */


    //Second factor Authentication is trip id


    String msngrId = null;
    String msngrToken = null;

    switch (al) {
      case TRAVELER:
        AccountTripLink atl = AccountTripLink.findByUid(a.getUid(), trip.getTripid());
        msngrId = Communicator.getTravelerId(atl);
        msngrToken = Communicator.getTravellerFirebaseToken(trip, atl);
        break;
      default:
        Credentials cred = Credentials.getFromCache(a.getUid());
        msngrToken = cred.getFirebaseToken(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(365));
        msngrId = Communicator.getAccountId(a);
        break;
    }

    //Now we are fully authenticated, let's go and generate JWS token
    AccountAuth token = null;
    List<AccountAuth> tokens = AccountAuth.getAllByType(a.getUid(), AccountAuth.AAuthType.JWT);
    if(tokens.size() == 1) {
      token = tokens.get(0);
    } else if (tokens.size() > 1) {
      //Starting from scratch - error somewhere.
      for(AccountAuth tkn: tokens) {
        tkn.delete();
      }
    }

    // Thank you humans at SO:
    // http://stackoverflow.com/questions/5355466/converting-secret-key-into-a-string-and-vice-versa
    // Also Java 8 base64 is very fast!: http://java-performance.info/base64-encoding-and-decoding-performance/
    SecretKey key;
    if(token == null) {
      //Generate new key
      key = MacProvider.generateKey(SignatureAlgorithm.HS256);
      String encodedKey = Base64.getEncoder().encodeToString(key.getEncoded());

      token = AccountAuth.build(a.getUid())
                         .setUid(a.getUid())
                         .setAuthType(AccountAuth.AAuthType.JWT)
                         .setValue(encodedKey);
      token.save();
    } else {
      byte[] decodedKey = Base64.getDecoder().decode(token.getValue());
      // rebuild key using SecretKeySpec
      key = new SecretKeySpec(decodedKey, 0, decodedKey.length, SignatureAlgorithm.HS256.getValue());
    }

    Map<String, Object> claims = new HashMap<>();
    //claims.put("sub", "uid");
    claims.put("uid", a.getUid());

    JwtToken jwtToken = new JwtToken(ReturnCode.SUCCESS);
    jwtToken.token = Jwts.builder()
                         .setHeaderParam("typ", "JWT") //Library does not set this by default
                         .setClaims(claims)
                         .setSubject("uid")            //Has to go after other claims or will be overwritten
                         .signWith(SignatureAlgorithm.HS256, key)
                         .compact();

    jwtToken.authenticated = true;
    jwtToken.authorized = true;

    jwtToken.messenger = new MessengerConfig();

    Set<Capability> tripCaps = SecurityMgr.getTripCapabilities(trip);
    jwtToken.messenger
        .setToken(msngrToken)
        .setDbUrl(ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.FIREBASE_URI))
        .setAid(a.getUid())
        .setUid(msngrId)
        .setName(a.getFullName())
        .setInitials(a.getInitials())
        .setTalkback(tripCaps.contains(Capability.COMMUNICATOR_TRAVELER_TALKBACK))
        .setPostUrl(controllers.routes.CommunicatorController.messageSent(null).url());
    sw.stop();
    Log.debug("API Authorized Account: " + a.getUid() + " in " + sw.getTime() + "ms");

    return jwtToken;
  }

  private boolean hasGuestAccess (Company cmpy) {
    if (cmpy != null && cmpy.getTag() != null && cmpy.getTag().contains(APPConstants.CMPY_TAG_ITINERARY_GUEST_LOGIN)) {
      return true;
    }
    return false;
  }

  private JwtToken authorizeGuest(Trip trip, StopWatch sw) {

    // Thank you humans at SO:
    // http://stackoverflow.com/questions/5355466/converting-secret-key-into-a-string-and-vice-versa
    // Also Java 8 base64 is very fast!: http://java-performance.info/base64-encoding-and-decoding-performance/
    SecretKey key = MacProvider.generateKey(SignatureAlgorithm.HS256);
    String encodedKey = Base64.getEncoder().encodeToString(key.getEncoded());

    Map<String, Object> claims = new HashMap<>();
    //claims.put("sub", "uid");
    claims.put("guest", encodedKey);

    JwtToken jwtToken = new JwtToken(ReturnCode.SUCCESS);
    jwtToken.token = Jwts.builder()
                         .setHeaderParam("typ", "JWT") //Library does not set this by default
                         .setClaims(claims)
                         .setSubject("uid")            //Has to go after other claims or will be overwritten
                         .signWith(SignatureAlgorithm.HS256, key)
                         .compact();

    jwtToken.authenticated = false;
    jwtToken.authorized = true;

    sw.stop();
    Log.debug("API Guest Authorized Account for trip: " + trip.getTripid() + " in " + sw.getTime() + "ms");
    return jwtToken;
  }

  public Result login() {
    return ok(views.html.webItinerary.login.render());
  }

  public Result info(String tripId, String jsonpCbFnName) {
    StopWatch sw = new StopWatch();
    sw.start();
    Trip trip = Trip.find.byId(tripId);
    if(trip == null || trip.getStatus() == APPConstants.STATUS_DELETED) {
      return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "Trip not found");
    }
    TripInfo ti = new TripInfo(ReturnCode.SUCCESS);
    ti.name = trip.getName();
    ti.coverUrl = trip.getImageUrl();
    ti.id = trip.getTripid();
    ti.startTs = trip.getStarttimestamp();
    ti.finishTs = trip.getEndtimestamp();
    ti.note = trip.getComments();
    ti.collab = false;
    ti.talkback = false;

    Company cmpy = Company.find.byId(trip.getCmpyid());
    ti.guestAccess = hasGuestAccess(cmpy);

    ti.enablePrettyPrint();

    Set<Capability> caps = SecurityMgr.getTripCapabilities(trip);

    if (!caps.contains(Capability.DISABLE_CONTENT_AFAR)) {
      Integer noOfAfar = AfarController.afarGuidesAvailableForTrip(tripId, offerService);
      if (noOfAfar != null && noOfAfar > 0) {
        ti.hasAfarGuides = true;
      }
    }

    Long uid = null;
    try {
      Claims claims = AccountJwtAuth.parseFromContext(ctx());
      uid = claims.get("uid", Long.class);
      if(uid != null) {
        ti.authenticated = true;
      }
    }
    catch (Exception e) {
      try {
        Claims claimsGuest = GuestJwtAuth.parseFromContext(ctx());
        String guest = claimsGuest.get("guest", String.class);
        if(guest != null && hasGuestAccess(cmpy)) {
          ti.authenticated = false;
          ti.authorized = true;
        }
      }
      catch (Exception e2) {
        ti.authenticated = false;
        ti.authorized = false;
      }
    }

    //Checking if account is authorized to access this trip
    if (ti.authenticated) {
      Account a = Account.find.byId(uid);
      ti.access = SecurityMgr.getTripAccessLevel(trip, a);
      if(ti.access.ge(SecurityMgr.AccessLevel.READ)) {
        ti.authorized = true;
      }
    }

    TripPublishHistory tph = TripPublishHistory.getLastPublished(trip.getPk(), null);
    ti.is24Hr = false;
    if (tph != null) {
      Account a = Account.findByLegacyId(tph.getModifiedby());
      if(a != null) {
        AccountProp tripPrefs = AccountProp.findByPk(a.getUid(), AccountProp.PropertyType.TRIP_PREFERENCE);
        if (tripPrefs != null && tripPrefs.tripPreferences != null) {
          ti.is24Hr = !tripPrefs.tripPreferences.isIs12Hr();
        }
      }
    }


    if(ti.authorized && ti.access == SecurityMgr.AccessLevel.TRAVELER) {
      ti.talkback = caps.contains(Capability.COMMUNICATOR_TRAVELER_TALKBACK);
      ti.collab = caps.contains(Capability.TRAVELER_COLLABORATION);
    }

    ti.bc = WebItineraryController.getTripBusinessCard(trip, null);
    if (ti.bc != null) {
      if (ti.bc.companyPhone != null && ti.bc.phone != null && ti.bc.companyPhone.equals(ti.bc.phone)) {
        ti.bc.phone = null;
      }
      if (ti.bc.socialFacebookUrl != null && !ti.bc.socialFacebookUrl.isEmpty()) {
        ti.bc.companyFacebookUrl = ti.bc.socialFacebookUrl;
      }
      if (ti.bc.socialTwitterUrl != null && !ti.bc.socialTwitterUrl.isEmpty()) {
        ti.bc.companyTwitterUrl = ti.bc.socialTwitterUrl;
      }

    }
    ti.setCallback(jsonpCbFnName);
    Log.debug("WIAPI: Info: Built in " + sw.getTime() + "ms");
    return ti.toJsonResult();
  }

  /**
   * Matches protocol required for FullCalendar.io events
   * @param tripId
   * @param start
   * @param end
   * @param jsonpCbFnName
   * @return
   */
  @With({GuestJwtAuth.class})
  public CompletionStage<Result> cal(String tripId, String start, String end, String jsonpCbFnName) {
    return  CompletableFuture.supplyAsync(() -> {
      StopWatch sw = new StopWatch();
      sw.start();
      Trip trip = Trip.find.byId(tripId);
      if(trip == null || trip.getStatus() == APPConstants.STATUS_DELETED) {
        return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "Trip not found");
      }

      ArrayList<Event> events = null;

      try {

        Optional<ArrayList> oevents = redis.hashGetBin(RedisKeys.H_TRIP,
                                                       tripId,
                                                       RedisKeys.HF_TRIP_CALENDAR_EVENTS.name(),
                                                       ArrayList.class);

        if (oevents.isPresent()) {
          events = oevents.get();
        }
        else {
          events = new ArrayList<>();

          List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
          for (TripDetail td : tripDetails) {
            if (td.getStarttimestamp() == null || td.getStarttimestamp() < 0) {
              continue;
            }

            String name = td.getName();
            Long startTS = td.getStarttimestamp();
            Long endTS = td.getEndtimestamp();
            if (name == null || name.length() == 0) { //Old record type
              switch (td.getDetailtypeid()) {
                case HOTEL:
                  UmAccommodationReservation hotelReseration = cast(td.getReservation(), UmAccommodationReservation
                      .class);
                  if (hotelReseration != null) {
                    name = hotelReseration.getAccommodation().name;
                  }
                  break;
                case FLIGHT:
                  // remove after migration
                  //FlightBooking fb = FlightBooking.findByPK(td.getDetailsid());

                  break;
                case CRUISE:
                  UmCruiseReservation cruiseReservation = cast(td.getReservation(), UmCruiseReservation.class);
                  if (cruiseReservation != null) {
                    name = cruiseReservation.getCruise().name;
                  }
                  break;
                case ACTIVITY:
                  UmActivityReservation activityReservation = cast(td.getReservation(), UmActivityReservation.class);
                  if (activityReservation != null) {
                    name = activityReservation.getActivity().name;
                  }
                  break;
                case TRANSPORT:
                  UmTransferReservation transferReseration = cast(td.getReservation(), UmTransferReservation.class);
                  if (transferReseration != null) {
                    name = transferReseration.getTransfer().getProvider().name;
                  }
                  break;
              }
            }

            Event e = new Event(td.getDetailsid(), name, startTS, endTS);
            e.umBookingType = td.getDetailtypeid();
            e.umBookingTypeParent = td.getDetailtypeid().getParent();

            events.add(e);
          }

          redis.hashSet(RedisKeys.H_TRIP, tripId, RedisKeys.HF_TRIP_CALENDAR_EVENTS.name(), events, RedisMgr.WriteMode.UNSAFE);
        }
      } catch (Exception e) {
        Log.err("Unexpected exception", e);
      }

      Log.debug("WIAPI: Calendar Events: Built in " + sw.getTime() + "ms");
      if (jsonpCbFnName != null) {
        return ok(play.libs.Jsonp.jsonp(jsonpCbFnName, Json.toJson(events)));
      }
      else {
        return ok(Json.toJson(events));
      }
    }, ec.current());
  }


  @With({GuestJwtAuth.class})
  public CompletionStage<Result> map(String tripId, String jsonpCbFnName) {
    return  CompletableFuture.supplyAsync(() -> {
      StopWatch sw = new StopWatch();
      sw.start();
      Trip trip = Trip.find.byId(tripId);
      if(trip == null || trip.getStatus() == APPConstants.STATUS_DELETED) {
        return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "Trip not found");
      }


      TripMap tm = null;
      try {
        Optional<TripMap> otp = redis.hashGetBin(RedisKeys.H_TRIP, tripId, RedisKeys.HF_TRIP_MAP.name(), TripMap.class);

        if (otp.isPresent()) {
          tm = otp.get();
        }
        else {
          List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
          if (tripDetails == null) {
            return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "No bookings found");
          }

          tm = new TripMap(ReturnCode.SUCCESS);
          TripBookingView tbv = WebItineraryController.buildTripBookingView(trip, tripDetails, false);
          //TODO: Needs to be optimized
          //From already created Trip Bookings View creating list of locations
          for (ReservationType type : tbv.bookings.keySet()) {
            List<TripBookingDetailView> bdList = tbv.bookings.get(type);
            for (TripBookingDetailView tbdv : bdList) {
              if (tbdv.locStartLat != null && tbdv.locStartLat != 0.0 && tbdv.locStartLong != null && tbdv.locStartLong != 0.0) {

                tm.addLocation(tbdv.name, tbdv.locStartLat, tbdv.locStartLong);
              }

              if (tbdv.detailsTypeId == ReservationType.FLIGHT && tbdv.locFinishLat != null && tbdv.locFinishLat != 0.0 && tbdv.locFinishLong != null && tbdv.locFinishLong != 0.0) {
                tm.addLocation(tbdv.name, tbdv.locFinishLat, tbdv.locFinishLong);
              }
            }
          }


          for (TripBookingDetailView tbdv : tbv.notes) {
            if (tbdv.locStartLat != null && tbdv.locStartLat != 0.0 && tbdv.locStartLong != null && tbdv.locStartLong
                                                                                                    != 0.0) {
              tm.addLocation(tbdv.name, tbdv.locStartLat, tbdv.locStartLong);
            }
          }

          redis.hashSet(RedisKeys.H_TRIP, tripId, RedisKeys.HF_TRIP_MAP.name(), tm, RedisMgr.WriteMode.UNSAFE);
        }

        tm.enablePrettyPrint();
        tm.setCallback(jsonpCbFnName);
        Log.debug("WIAPI: Map: Built in " + sw.getTime() + "ms");
      } catch (Exception ex) {
        Log.err("Unexpected exception", ex);
      }
      return tm.toJsonResult();
    }, ec.current());
  }

  @With({GuestJwtAuth.class})
  public CompletionStage<Result> kml(String tripId) {
    return CompletableFuture.supplyAsync(() -> {
      StopWatch sw = new StopWatch();
      sw.start();
      Trip trip = Trip.find.byId(tripId);
      if(trip == null || trip.getStatus() == APPConstants.STATUS_DELETED) {
        return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "Trip not found");
      }

      List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
      if (tripDetails == null) {
        return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "No bookings found");
      }

      try {
        Kml kml = new Kml();
        Document document = kml.createAndSetDocument().withName(trip.getName());

        TripBookingView tbv = WebItineraryController.buildTripBookingView(trip, tripDetails, false);
        //TODO: Needs to be optimized
        //From already created Trip Bookings View creating list of locations
        for (ReservationType type : tbv.bookings.keySet()) {
          List<TripBookingDetailView> bdList = tbv.bookings.get(type);
          for (TripBookingDetailView tbdv : bdList) {
            if (tbdv.locStartLat != null && tbdv.locStartLat != 0.0 && tbdv.locStartLong != null && tbdv.locStartLong != 0.0) {

              document.createAndAddPlacemark()
                      .withName(tbdv.name)
                      .withOpen(Boolean.TRUE)
                      .createAndSetPoint()
                      .addToCoordinates(tbdv.locStartLong, tbdv.locStartLat);
            }
          }
        }

        kml.setFeature(document);

        StringWriter stringWriter = new StringWriter();
        try {
          Marshaller m = JAXBContext.newInstance(new Class[]{Kml.class}).createMarshaller();
          m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
          m.setProperty(Marshaller.JAXB_FRAGMENT, false);
          m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
          m.marshal(kml, NoNamesWriter.filter(stringWriter));
        }
        catch (JAXBException e) {
          e.printStackTrace();
        }
        catch (XMLStreamException e) {
          e.printStackTrace();
        }

        //TODO: Research needed to remove both ns2 and kml prefixes inside XML elements that Google Maps rejects
        Log.debug("WIAPI: KML: Built in " + sw.getTime() + "ms");
        return ok(stringWriter.toString()).as("application/vnd.google-earth.kml+xml");
      } catch (Exception ex) {
        Log.err("Unexpected exception", ex);
        return ok("").as("application/vnd.google-earth.kml+xml");
      }
    }, ec.current());
  }

  @With({AccountJwtAuth.class})
  public CompletionStage<Result> travelers(String tripId) {
    return CompletableFuture.supplyAsync(() -> {
      TravelersJson result = new TravelersJson(ReturnCode.SUCCESS);

      try {
        Account currentUser = Account.find.byId((Long) ctx().args.get("uid"));

        if (tripId != null && currentUser != null) {
          Trip trip = Trip.findByPK(tripId);
          List<AccountTripLink> tripLinks = null;
          AccountTripLink userATL = AccountTripLink.findByUid(currentUser.getUid(), tripId);
          String groupId = null;
          if (userATL != null) {
            groupId =userATL.getLegacyGroupId();
          }

          TripGroup group = null;
          if (groupId != null && groupId.trim().length() > 0) {
            group = TripGroup.findByPK(groupId);
            if (group == null || !group.tripid.equals(trip.tripid) || (group.publishstatus != APPConstants.STATUS_PUBLISHED && group.publishstatus != APPConstants.STATUS_PUBLISHED_REVIEW) || group.allowselfregistration != APPConstants.ALLOW_SELF_REGISTRATION) {

              //return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "System Error - cannot get emails.");
            }

            tripLinks = AccountTripLink.findByTripGroup(tripId, groupId);
          }
          else {
            tripLinks = AccountTripLink.findByTripNoGroup(tripId);
          }
        /*else if ((trip.getStatus() != APPConstants.STATUS_PUBLISHED && trip.getStatus() != APPConstants
                .STATUS_PUBLISHED_REVIEW) || trip.visibility != APPConstants.ALLOW_SELF_REGISTRATION) {
          //return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "System Error - cannot get emails.");
        }*/

          if (tripLinks != null) {
            Person p;
            Account account;
            for (AccountTripLink a : tripLinks) {
              p = new Person();
              account = Account.find.byId(a.getPk().getUid());
              p.givenName = account.getFirstName();
              p.familyName = account.getLastName();
              p.email = account.getEmail();
              p.telephone = account.getMsisdn();
              p.additionalType = account.getState().getShort();
              p.umId = String.valueOf(account.getUid());
              result.travelers.add(p);
            }
          }
        }
      } catch (Exception e) {
        Log.err("Unexpected exception: ", e);
      }
      return result.toJsonResult();
    }, ec.current());
  }

  @With({AccountJwtAuth.class})
  public Result addAllAfarGuides(String tripId) {
    Account currentUser = Account.find.byId((Long) ctx().args.get("uid"));
    String userId = currentUser.getLegacyId();
    if (currentUser != null && userId == null ) {
      userId = currentUser.getUid().toString();
    }
    ItineraryOffer offer = offerService.getOffer(tripId);
    if (offer != null && offer.getLocationOffers().size() > 0 && userId != null) {
      try {
        Trip trip = Trip.findByPK(tripId);

        int inserted = 0;
        int updated = 0;
        int failed = 0;

        for (LocationOffer locationOffer : offer.getLocationOffers()) {
          UMappedCity umCity = locationOffer.getLocation();
          if (umCity != null) {
            AfarCity afarCity = AfarCity.findByUmCity(umCity);
            if (afarCity != null) {
              List<AfarCityList> cityLists = AfarCityList.findByCityId(afarCity.getAfarId());
              if (cityLists != null && cityLists.size() > 0) {
                int response = AfarController.insertAfarGuideAsTripNote(trip, afarCity.getAfarId(), null, userId, false);

                if (response == APPConstants.INSERT_AFAR_NOTE_SUCCESS) {
                  Log.debug("Successfully added AFAR guide to Trip: " + trip.tripid + " for UmCity: " + umCity.getId() + " with AfarId: " + afarCity.getAfarId() + " - " + afarCity.getName());
                  inserted++;

                } else if (response == APPConstants.UPDATE_AFAR_NOTE_SUCCESS) {
                  Log.debug("Duplicate Afar Guide already added to trip: " + trip.tripid + " for AfarCityId: " + afarCity.getAfarId() + " - " + afarCity.getName());
                  updated++;
                } else {
                  Log.err("Error adding AFAR guide to Trip: " + trip.tripid + " for UmCity: " + umCity.getId() + " with AfarId: " + afarCity.getAfarId() + " - " + afarCity.getName());
                  failed++;
                }
              } else {
                Log.err("Data unavailable - AfarCityList does not exist for AfarCity: " + afarCity.getAfarId());
                failed++;
              }
            } else {
              Log.err("Data unavailable - AfarCity does not exist for UmCity: " + umCity.getId());
              failed++;
            }
          } else {
            Log.err("Data unavailable - UmCity does not exist for Location Offer: " + locationOffer.toString());
            failed++;
          }
        }

        flash(SessionConstants.SESSION_PARAM_MSG, "AFAR guides added to Itinerary - " + inserted + " Inserts and " + updated + " Updates.");
        flash(SessionConstants.SESSION_PARAM_TRIPID, trip.tripid);

        BookingController.invalidateCache(tripId, null);
        return BaseJsonResponse.codeResponse(ReturnCode.SUCCESS, "AFAR guides added to Itinerary - " + inserted + " Inserts and " + updated + " Updates.");

      } catch (Exception e) {
        Ebean.rollbackTransaction();
        Log.log(LogLevel.ERROR, "Error processing AFAR guides into Itinerary ", e);
        return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_FAIL, "Error processing AFAR guides into Itinerary");
      }
    } else {
      Log.debug("No available AFAR guides to update or add to this Itinerary");
      return BaseJsonResponse.codeResponse(ReturnCode.SUCCESS, "No available AFAR guides to update or add to this Itinerary");

    }

  }

  @With({AccountJwtAuth.class})
  public Result travelerAdd(String tripId) {

    String firstName = request().body().asJson().get("givenName").asText().trim();
    String lastName = request().body().asJson().get("familyName").asText().trim();
    String email = request().body().asJson().get("email").asText().trim();
    String phone = "";

    if(request().body().asJson().get("telephone") != null) {
      phone = request().body().asJson().get("telephone").asText().trim();
    }

    Account currentUser = Account.find.byId((Long) ctx().args.get("uid"));
    AccountTripLink userATL = AccountTripLink.findByUid(currentUser.getUid(), tripId);


    try {
      List<AccountTripLink> tripLinks = AccountTripLink.findByEmailTrip(email.trim(), tripId);
      if (tripLinks == null || tripLinks.size() == 0) {
        Trip trip = Trip.findByPK(tripId);
        String groupId = null;
        if(userATL != null) {
          groupId = userATL.getLegacyGroupId();
        }
        TripGroup group = TripGroup.findByPK(groupId);

        String name = null;
        AccountTripLink tripLink = null;
        Long uid = (Long) ctx().args.get("uid");

        TripPublisherHelper.TravelerResponse response = TripPublisherHelper.addPassenger(firstName,
                lastName,
                email,
                null,
                uid.toString(),
                trip,
                group,
                true, null);

        BaseView view = null;
        StringBuilder sb = new StringBuilder();
        switch (response) {
          case ERR_EMAIL:
            sb.append("Error - cannot send notification emails. ");
            //still ok - so we just add a message and return ok
          case OK:
            sb.append(email + " added to this trip.");
            flash(SessionConstants.SESSION_PARAM_MSG, sb.toString());
            flash(SessionConstants.SESSION_PARAM_TRIPID, trip.tripid);
            if (group != null) {
              flash(SessionConstants.SESSION_PARAM_GROUPID, group.groupid);
            }
            return BaseJsonResponse.codeResponse(ReturnCode.SUCCESS);
          case ERR_DUPLICATE:
            view = new BaseView();
            view.message = "This email is already registered.";
            return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_WRONG_DATA);
          case ERR_UNDEFINED:
            view = new BaseView();
            view.message = "Cannot add this traveler - please contact support";
            return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_FAIL);
        }
      }
    } catch(Exception e) {
      e.printStackTrace();
    }

    return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_FAIL);
  }

  @With({AccountJwtAuth.class})
  public Result travelerDelete(String tripId, String travelerId) {


    Account currentUser = Account.find.byId((Long) ctx().args.get("uid"));
    AccountTripLink userATL = AccountTripLink.findByUid(currentUser.getUid(), tripId);
    Trip trip = Trip.findByPK(tripId);

    if (trip != null && trip.getStatus() != APPConstants.STATUS_DELETED) {
      String groupId = null;
      if (userATL != null) {
        groupId = userATL.getLegacyGroupId();
      }
      TripGroup group = null;
      if (groupId != null && groupId.trim().length() > 0) {
        group = TripGroup.findByPK(groupId);
      }
      try {
        AccountTripLink tripLink = AccountTripLink.findByUid(Long.parseLong(travelerId), tripId);
        if (tripLink != null) {
          tripLink.delete();
          Account traveler = Account.find.byId(tripLink.getPk().getUid());

          //TODO: Here we don't know who the user is... so let's call it a system.
          MessengerUpdateHelper.build(trip, Account.AID_PUBLISHER).
              modifyUserTripAccessState(tripLink.getLegacyId(), false);

          //check to make sure tour is in umapped mobile
          //delete from the shared in umapped mobile
          String mapPK = trip.tripid;
          if (tripLink.getLegacyGroupId() != null) {
            mapPK = TourController.getCompositePK(trip.tripid, tripLink.getLegacyGroupId());
          }


          //flash(SessionConstants.SESSION_PARAM_MSG, formInfo.getInEmail() + " removed from this trip.");
          flash(SessionConstants.SESSION_PARAM_TRIPID, trip.tripid);

          if (group != null) {
            flash(SessionConstants.SESSION_PARAM_GROUPID, group.groupid);
          }
          //return this.getPassengers();
        }
      }
      catch(Exception e) {
        e.printStackTrace();
      }
    }

    return BaseJsonResponse.codeResponse(ReturnCode.SUCCESS);
  }

  @With({GuestJwtAuth.class})
  public CompletionStage<Result> reservations(String tripId, String jsonpCbFnName) {
    return CompletableFuture.supplyAsync(() -> {
      StopWatch sw = new StopWatch();
      sw.start();
      TripBookingsDailyJson result = new TripBookingsDailyJson(ReturnCode.SUCCESS);

      try {

        Trip trip = Trip.find.byId(tripId);
        if(trip == null || trip.getStatus() == APPConstants.STATUS_DELETED) {
          return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "No trip found");
        }
        Optional<ReservationPackage> orp;
        if (!DEBUG_NO_CACHE) {
          orp = redis.hashGetBin(RedisKeys.H_TRIP,
                                 tripId,
                                 RedisKeys.HF_TRIP_RESERVATION_PACKAGE.name(),
                                 ReservationPackage.class);
        }
        else {
          orp = Optional.empty();
        }

        //Thierry - emergency fix for now - do not cache
        orp = Optional.empty();

        ReservationPackage rp;
        if (orp.isPresent()) {
          rp = orp.get();
        }
        else {
          List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
          if (tripDetails == null) {
            return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "No bookings found");
          }

          List<TripNote> notes = TripNote.getNotesByTripId(trip.getTripid());

          TripReservationsHelper rh = TripReservationsHelper.build().setTrip(trip).setBookings(tripDetails).setNotes
              (notes, trip);
          rp = rh.prepareReservationPackage(true);
          redis.hashSet(RedisKeys.H_TRIP, tripId, RedisKeys.HF_TRIP_RESERVATION_PACKAGE.name(), rp, RedisMgr.WriteMode.UNSAFE);
        }

        result.reservations = rp;

        //If traveller update all reservations with editable information
        //TODO: This needs to be speed-up via caching
        //ctx().args.put("uid", 1059025452150000003l); //TODO: Remove after debugging
        boolean isTraveler = false;
        Account a = null;
        Long uid = (Long) ctx().args.get("uid");
        if (uid != null && uid > 0) {
          a = Account.find.byId(uid);
          SecurityMgr.AccessLevel al = SecurityMgr.getTripAccessLevel(trip, a);
          isTraveler = (al == SecurityMgr.AccessLevel.TRAVELER);

          if (isTraveler) {
            //increment access count
            AccountTripLink tripLink = AccountTripLink.findByUid(a.getUid(), trip.getTripid());
            if (tripLink != null) {
              //First, increment Click counts for webIntinerary and set Timestamps
              try {
                if (tripLink.getClickCounter() == 0) {
                  tripLink.setFirstClickTs(new Timestamp(System.currentTimeMillis()));
                }
                tripLink.setLastClickTs(new Timestamp(System.currentTimeMillis()));
                tripLink.setClickCounter(tripLink.getClickCounter() + 1);
                tripLink.update();
              }
              catch (Exception e) {
                e.printStackTrace();
              }
            }
          }
        }

        if (result.reservations.sorted != null) {
          Iterator<ListItem> liIt = result.reservations.sorted.itemListElement.iterator();
          while (liIt.hasNext()) {
            ListItem li = liIt.next();
            Reservation r = (Reservation) li.item;
            //All cards expanded by default.  Not final & will require actual logic later.
            if ((r.description != null && !r.description.trim().isEmpty()) || (r.additionalType.equals("https://schema.org/LodgingReservation") && ((LodgingReservation) li.item).reservationFor != null && ((LodgingReservation) li.item).reservationFor.description != null && !((LodgingReservation) li.item).reservationFor.description.trim().isEmpty())
                    || (r.additionalType.equals("https://schema.org/FlightReservation") && r.subReservation != null && r.subReservation.size() > 0)
                    || (r.cancellationPolicy != null && !r.cancellationPolicy.trim().isEmpty())
                    || (r.additionalType.equals("https://schema.org/LodgingReservation") && r.subReservation != null && r.subReservation.size() > 0)
                    || (r.additionalType.equals("https://umapped.com/CruiseReservation") && ((CruiseShipReservation) li.item).stops != null && ((CruiseShipReservation) li.item).stops.size() > 0)
                    || (r.rates != null && r.rates.size() > 0)
                    || ((r.subtotal != null && !r.subtotal.trim().isEmpty()) || (r.taxes != null && !r.taxes.trim().isEmpty()) || (r.fees != null && !r.fees.trim().isEmpty()) || (r.currency != null && !r.currency.trim().isEmpty()) || (r.total != null && !r.total.trim().isEmpty()))
                    || ((r.additionalType.equals("https://umapped.com/ActivityReservation") || r.additionalType.equals("https://umapped.com/TourReservation") || r.additionalType.equals("https://umapped.com/ShoreExcursionReservation") || r.additionalType.equals("https://umapped.com/CruiseStopReservation") || r.additionalType.equals("https://umapped.com/SkiLiftReservation") || r.additionalType.equals("https://umapped.com/SkiLessonsReservation") || r.additionalType.equals("https://umapped.com/SkiRentalReservation")) && ((ActivityReservation) li.item).startLocation != null &&  ((ActivityReservation) li.item).startLocation.description != null && !((ActivityReservation) li.item).startLocation.description.trim().isEmpty())
                    || ((r.additionalType.equals("https://umapped.com/ActivityReservation") || r.additionalType.equals("https://umapped.com/TourReservation") || r.additionalType.equals("https://umapped.com/ShoreExcursionReservation") || r.additionalType.equals("https://umapped.com/CruiseStopReservation") || r.additionalType.equals("https://umapped.com/SkiLiftReservation") || r.additionalType.equals("https://umapped.com/SkiLessonsReservation") || r.additionalType.equals("https://umapped.com/SkiRentalReservation")) && ((ActivityReservation) li.item).reservationFor != null && ((ActivityReservation) li.item).reservationFor.organizedBy != null &&  ((ActivityReservation) li.item).reservationFor.organizedBy.description != null && !((ActivityReservation) li.item).reservationFor.organizedBy.description.trim().isEmpty())
                    || (r.additionalType.equals("https://schema.org/BusReservation") && ((BusReservation) li.item).reservationFor != null && ((BusReservation) li.item).reservationFor.provider != null && ((BusReservation) li.item).reservationFor.provider.description != null && !((BusReservation) li.item).reservationFor.provider.description.trim().isEmpty())
                    || ((r.additionalType.equals("https://schema.org/RentalCarReservation") || r.additionalType.equals("https://umapped.com/RentalCarDropOffReservation")) && ((RentalCarReservation) li.item).reservationFor != null && ((RentalCarReservation) li.item).reservationFor.rentalCompany != null && ((RentalCarReservation) li.item).reservationFor.rentalCompany.description != null && !((RentalCarReservation) li.item).reservationFor.rentalCompany.description.trim().isEmpty())
                    || (r.additionalType.equals("https://umapped.com/CruiseReservation") && ((CruiseShipReservation) li.item).reservationFor != null && ((CruiseShipReservation) li.item).reservationFor.provider != null && ((CruiseShipReservation) li.item).reservationFor.provider.description != null && !((CruiseShipReservation) li.item).reservationFor.provider.description.trim().isEmpty())
                    || (r.additionalType.equals("https://schema.org/EventReservation") && ((EventReservation) li.item).reservationFor != null &&  ((EventReservation) li.item).reservationFor.description != null && !((EventReservation) li.item).reservationFor.description.trim().isEmpty())
                    || (r.additionalType.equals("https://schema.org/EventReservation") && ((EventReservation) li.item).reservationFor != null &&  ((EventReservation) li.item).reservationFor.organizer != null && ((EventReservation) li.item).reservationFor.organizer.description != null && !((EventReservation) li.item).reservationFor.organizer.description.trim().isEmpty())
                    || (r.additionalType.equals("https://schema.org/FlightReservation") && ((FlightReservation) li.item).reservationFor != null && ((FlightReservation) li.item).reservationFor.airline != null && ((FlightReservation) li.item).reservationFor.airline.description != null && !((FlightReservation) li.item).reservationFor.airline.description.trim().isEmpty())
                    || ((r.additionalType.equals("https://umapped.com/PrivateTransferReservation") || r.additionalType.equals("https://umapped.com/PublicTransportReservation") || r.additionalType.equals("https://umapped.com/TransferReservation") || r.additionalType.equals("https://umapped.com/FerryReservation")) && ((TransferReservation) li.item).reservationFor != null && ((TransferReservation) li.item).reservationFor.provider != null && ((TransferReservation) li.item).reservationFor.provider.description != null && !((TransferReservation) li.item).reservationFor.provider.description.trim().isEmpty())
                    || (r.additionalType.equals("https://schema.org/TaxiReservation") && ((TaxiReservation) li.item).reservationFor != null && ((TaxiReservation) li.item).reservationFor.provider != null && ((TaxiReservation) li.item).reservationFor.provider.description != null && !((TaxiReservation) li.item).reservationFor.provider.description.trim().isEmpty())
                    || (r.additionalType.equals("https://schema.org/TrainReservation") && ((TrainReservation) li.item).reservationFor != null && ((TrainReservation) li.item).reservationFor.provider != null && ((TrainReservation) li.item).reservationFor.provider.description != null && !((TrainReservation) li.item).reservationFor.provider.description.trim().isEmpty())
                    || (r.additionalType.equals("https://schema.org/FoodEstablishmentReservation") && ((FoodEstablishmentReservation) li.item).reservationFor != null && ((FoodEstablishmentReservation) li.item).reservationFor.description != null && !((FoodEstablishmentReservation) li.item).reservationFor.description.trim().isEmpty())) {
              r.autoExpand = true;
            }
            else {
              r.autoExpand = false;
            }
            boolean isCreator = false;
            if (a != null) {
              isCreator = r.creator.umId.equals(Long.toString(a.getUid()));
            }
            if (isCreator && isTraveler) {
              if (r.additionalType.equals("https://umapped.com/NoteReservation") && r.description != null && r.description.contains("afar-content")) {
                r.editable = Editable.LOCKED;
              } else {
                r.editable = Editable.EDITABLE;

              }
            }
            else if (!isTraveler && r.additionalType.equals("https://umapped.com/NoteReservation")) {
              if (Utils.isInteger(r.creator.umId)) {
                liIt.remove();
              }
            }
          }
        }

        sw.stop();
        Log.debug("WIAPI: Reservations: Built in " + sw.getTime() + "ms");
        result.setCallback(jsonpCbFnName);
        result.enablePrettyPrint();

      } catch (Exception e) {
        Log.err("WebItineraryApiController: reservations(): Exception: ", e);
      }

      return result.toJsonResult();
    }, ec.current());
  }


  @With({GuestJwtAuth.class})
  public CompletionStage<Result> bookings(String tripId, String jsonpCbFnName) {
    return  CompletableFuture.supplyAsync(() -> {
      StopWatch sw = new StopWatch();
      sw.start();

      //TODO: Add logic to check cache validity
      TripBookingsDailyJson result = null;
      try {
        //result = (TripBookingsDailyJson) CacheMgr.get(CACHE_WI_API_BOOKINGS + tripId);
        if (result != null) {
          Log.debug("WIAPI: Bookings: Cacheback in " + sw.getTime() + "ms");
          return result.toJsonResult();
        }
      } catch (Exception e) {
        Log.debug("WIAPI: Deserialization failed... just FYI");
      }

      Trip             trip        = Trip.find.byId(tripId);
      List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
      if (tripDetails == null) {
        return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "No bookings found");
      }

      result = new TripBookingsDailyJson(ReturnCode.SUCCESS);
      TripBookingView tbv = WebItineraryController.buildTripBookingView(trip, tripDetails, false);
      tbv.getChronologicalBookings();

      for(String day: tbv.chronologicalDates) {
        List<TripBookingDetailView> bookings = tbv.chronologicalBookings.get(day);
        //Set of front notes
        if(day.length() == 0) {
          result.notes = bookings;
          continue;
        }

        TripBookingsDailyJson.DayBookings db = new TripBookingsDailyJson.DayBookings();
        db.date =  Utils.getISO8601Date(bookings.get(0).startDateMs);
        db.bookings = bookings;

        //Find if there are cruises and build cruise stops
        for(TripBookingDetailView tbdv: bookings) {
          if(tbdv.detailsTypeId == null) {
            continue;
          }
          switch (tbdv.detailsTypeId) {
            case CRUISE:
              tbdv.enhancedView = EnhancedTripBookingDetailView.parseCruise(tbv, tbdv);
              break;
            case FLIGHT:
              tbdv.enhancedView = EnhancedTripBookingDetailView.parseFlight(tbdv);
              break;
          }
        }
        result.days.add(db);
      }

      sw.stop();
      Log.debug("WIAPI: Bookings: Built in " + sw.getTime() + "ms");
      result.setCallback(jsonpCbFnName);
      return result.toJsonResult();
    }, ec.current());
  }

  @With({AccountJwtAuth.class})
  public Result poiAutocomplete(String type, String term) {
    Set<Integer> searchCmpyIds = new HashSet<>();
    searchCmpyIds.add(Company.PUBLIC_COMPANY_ID);
    PoiTypeInfo.PoiType pt          = PoiTypeInfo.Instance().byName(type);
    List<Integer>       searchTypes = new ArrayList<>(1);
    if (pt != null) {
      searchTypes.add(pt.getId());
    }
    return ok(PoiController.autocompleteHelper(term, searchTypes, searchCmpyIds));
  }

  @With({GuestJwtAuth.class})
  public Result docs(String tripId, String jsonpCbFnName) {
    StopWatch sw = new StopWatch();
    sw.start();

    TripDocumentsJson docs = new TripDocumentsJson(ReturnCode.SUCCESS);

    DestinationSearchView dsv = TripController.getGuidesImproved(tripId);
    docs.docs = dsv.destinationList;
    docs.attachments = WebItineraryController.getTripAttachments(tripId, 0);

    sw.stop();
    Log.debug("WIAPI: Documents: Built in " + sw.getTime() + "ms");
    docs.setCallback(jsonpCbFnName);
    docs.enablePrettyPrint();
    return docs.toJsonResult();
  }

  @BodyParser.Of(TolerantText1Mb.class)
  @With({AccountJwtAuth.class})
  public Result reservationEdit(String tripId) {
    StopWatch sw = new StopWatch();
    sw.start();
    ReservationsHolder rh = new ReservationsHolder();
    boolean rc = rh.parseJson(request().body().asText());

    if(!rc) {
      return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_WRONG_DATA, "Input parse failure.");
    }

    Trip trip = Trip.findByPK(tripId);
    if(trip == null || trip.getStatus() == APPConstants.STATUS_DELETED) {
      return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "Trip can't be found.");
    }

    TripVO tvo = rh.toTripVO(null);
    Account a = Account.find.byId((Long)ctx().args.get("uid"));

    SecurityMgr.AccessLevel al = SecurityMgr.getTripAccessLevel(trip, a);
    if (al.lt(SecurityMgr.AccessLevel.TRAVELER)) {
      return BaseJsonResponse.codeResponse(ReturnCode.AUTH_TRIP_FAIL, "No trip modification access.");
    }

    VOModeller voModeller =  new VOModeller(trip, a, false);
    voModeller.setGenerateNotes(false); //Never generate comments in API mode
    voModeller.buildTripVOModels(tvo);

    //TODO: Figure out if the booking being submitted can be overwritten by the current account

    try {
      voModeller.saveAllModels();

      BookingController.invalidateCache(tripId, null); //Unfortunately for now removing all caches

      //let's schedule this trip to be republished
      AccountTripLink  tripLink = AccountTripLink.findByUid(a.getUid(), trip.getTripid());
      if (tripLink != null) {
        tripPublisherHelper.autoPublishTrip(trip, tripLink.getLegacyGroupId());
      }

    } catch (Exception e) {
      return BaseJsonResponse.codeResponse(ReturnCode.DB_TRANSACTION_FAIL, "Error saving reservations.");
    }

    sw.stop();
    Log.debug("WIAPI: Reservations: Created/Edited in " + sw.getTime() + "ms");
    return BaseJsonResponse.codeResponse(ReturnCode.SUCCESS);
  }


  @With({AccountJwtAuth.class})
  public Result reservationDelete(String tripId, String detailId) {
    StopWatch sw = new StopWatch();
    sw.start();

    Trip trip = Trip.findByPK(tripId);
    if(trip == null || trip.getStatus() == APPConstants.STATUS_DELETED) {
      return BaseJsonResponse.codeResponse(ReturnCode.DB_RECORD_NOT_FOUND, "Trip can't be found.");
    }

    Account a = Account.find.byId((Long)ctx().args.get("uid"));
    SecurityMgr.AccessLevel al = SecurityMgr.getTripAccessLevel(trip, a);
    if (al.lt(SecurityMgr.AccessLevel.TRAVELER)) {
      return BaseJsonResponse.codeResponse(ReturnCode.AUTH_TRIP_FAIL, "No trip modification access.");
    }

    //TODO: Verify security to access individual reservation

    TripDetail td = TripDetail.findByPK(detailId);
    if (td != null) {
      td.setStatus(APPConstants.STATUS_DELETED);
      td.update();
      BookingController.invalidateCache(td.getTripid(), td.getDetailsid());
    } else {
      try {
        TripNote tn = TripNote.find.byId(Long.parseLong(detailId));
        if (tn != null) {
          tn.setStatus(APPConstants.STATUS_DELETED);
          tn.update();
          BookingNoteController.invalidateCache(trip.tripid);
        }
      } catch (NumberFormatException nfe) {

      }
    }


    //let's schedule this trip to be republished
    AccountTripLink  tripLink = AccountTripLink.findByUid(a.getUid(), trip.getTripid());
    if (tripLink != null) {
      tripPublisherHelper.autoPublishTrip(trip, tripLink.getLegacyGroupId());
    }

    sw.stop();
    Log.debug("WIAPI: Reservations: Deleted in " + sw.getTime() + "ms");
    return BaseJsonResponse.codeResponse(ReturnCode.SUCCESS);
  }

  public static String getItineraryUrl(String tripId) {
    switch (ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.ENVIRONMENT)) {
      case "PROD":
        return "https://my.umapped.com/#!/login/" + tripId;
      case "STAGING":
        return "https://my-staging.umapped.com/#!/login/" + tripId;
      case "TEST":
        return "https://my-test.umapped.com/#!/login/" + tripId;
      case "DEV":
      default:
        return "https://my-dev.umapped.com/#!/login/" + tripId;
    }
  }

  public static String getItineraryUrl(String tripId, Company c) {
    if (ConfigMgr.isProduction() && c != null && c.name.toLowerCase().contains("indagare")) {
      return "https://itinerary.indagare.com/#!/login/" + tripId;
    }
    return getItineraryUrl(tripId);
  }

}