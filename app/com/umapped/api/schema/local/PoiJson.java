package com.umapped.api.schema.local;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.umapped.api.schema.types.SocialLink;
import com.mapped.publisher.persistence.PoiTypeInfo;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.umapped.api.schema.types.*;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Generated;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.*;


/**
 * Point of interest definition
 * <p/>
 * All-inclusive schema for UMapped POI representation
 */
@JsonInclude(JsonInclude.Include.NON_NULL) @Generated("org.jsonschema2pojo")
@JsonPropertyOrder({"poi-id", "poi-type-id", "poi-type-name", "name", "desc", "tags", "emails", "addresses",
                    "phone-numbers", "social-links", "url"})
public class PoiJson
    implements Cloneable, Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  @Transient
  private final static boolean DEBUG = false;
  @JsonProperty("poi-id")
  private Long poiId;
  @JsonProperty("poi-type-id")
  private Integer poiTypeId;
  @JsonProperty("poi-type-name")
  private String poiTypeName;
  @JsonProperty(required = false, value = "cmpy-id")
  private Integer cmpyId;
  /**
   * Additional representation of poi id as string for use with JavaScript
   */
  @JsonProperty(required = false, value = "poi-id-js")
  @JsonSerialize(using = ToStringSerializer.class, as = String.class)
  private String poiIdJs;
  @JsonProperty(required = false, value = "file-count")
  private Integer fileCount;
  @JsonProperty("name")
  private String name;
  @JsonProperty("desc")
  private String desc;
  @JsonProperty("code")
  private String code;
  @JsonProperty("country-code")
  private String countryCode = null;
  @JsonProperty("tags") @JsonDeserialize(as = LinkedHashSet.class)
  private Set<String> tags = new LinkedHashSet<String>();
  @JsonProperty("emails") @JsonDeserialize(as = LinkedHashSet.class)
  private Set<Email>          emails               = new LinkedHashSet<>();
  @JsonProperty("addresses") @JsonDeserialize(as = LinkedHashSet.class)
  private Set<Address>        addresses            = new LinkedHashSet<Address>();
  @JsonProperty("phone-numbers") @JsonDeserialize(as = LinkedHashSet.class)
  private Set<PhoneNumber>    phoneNumbers         = new LinkedHashSet<PhoneNumber>();
  @JsonProperty("social-links") @JsonDeserialize(as = LinkedHashSet.class)
  private Set<SocialLink>     socialLinks          = new LinkedHashSet<>();
  @JsonProperty("image-urls") @JsonDeserialize(as = LinkedHashSet.class)
  private Set<String>  imageUrls = new LinkedHashSet<>();

  /**
   * URL verification pattern should be used to verify content
   */
  @JsonProperty("urls") @JsonDeserialize(as = LinkedHashSet.class)
  private Set<String>         urls                 = new LinkedHashSet<String>();
  @JsonProperty("features") @JsonDeserialize(as = java.util.LinkedHashSet.class)
  private Set<Feature>        features             = new LinkedHashSet<Feature>();
  @JsonIgnore
  private Map<String, String> additionalProperties = new HashMap<String, String>();

  public static PoiJson buildPoiJson(Long id, Integer cmpyId, Integer type, String name) {
    PoiJson pj = new PoiJson();
    pj.poiId = id;
    pj.cmpyId = cmpyId;
    pj.poiTypeId = type;
    pj.name = name;
    return pj;
  }

  @JsonProperty("country-code")
  public String getCountryCode() {
    return countryCode;
  }
  @JsonProperty("country-code")
  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public PoiJson withCountryCode(String countryCode) {
    this.countryCode = countryCode;
    return this;
  }
  public void prepareForDb() {
    this.poiIdJs = null;
    this.poiTypeName = null;
  }

  public void prepareForBrowser() {
    if (poiId != null) {
      this.poiIdJs = poiId.toString();
    }
    else {
      Log.err("Corrupt POI is being sent to the browser: " + toString());
    }
    this.poiId = null;
  }

  @JsonProperty(required = false, value = "poi-id-js")
  @JsonSerialize(using = ToStringSerializer.class, as = String.class)
  public String getPoiIdJs() {
    return poiIdJs;
  }

  @JsonProperty(required = false, value = "poi-id-js")
  @JsonSerialize(using = ToStringSerializer.class, as = String.class)
  public void setPoiIdJs(String poiIdJs) {
    this.poiIdJs = poiIdJs;
    if (poiIdJs != null) {
      this.poiId = Long.parseLong(poiIdJs);
    }
  }

  /**
   * Provided master record finds all fields that are different and
   * keeps them while removing (nulling) anything that did not change
   *
   * @param master
   * @return true if there is useful data and objects are different, false otherwise
   */
  public boolean scrapMasterData(PoiJson master) {
    boolean isDifferent = false;
    if (DEBUG) {
      Log.debug(">>>>>>> Master Poi Record :" + master.toString());
      Log.debug(">>>>>>> Before scrapping:" + toString());
    }
    //Name is a required field, so always keeping it, just note if changed
    if (name != null && !name.equals(master.name)) {
      isDifferent = true;
    }

    if (desc != null && desc.equals(master.desc)) {
      desc = null;
    }
    else {
      isDifferent = true;
    }

    if (code != null && code.equals(master.code)) {
      code = null;
    }
    else {
      isDifferent = true;
    }

    Set<String> oldTags = tags;
    tags = new LinkedHashSet<>();
    for (String tag : oldTags) {
      //All tags shorter than 2 characters are ignored
      if (tag.length() > 2 && !master.tags.contains(tag)) {
        tags.add(tag);
        isDifferent = true;
      }
    }

    Set<Email> oldEmails = emails;
    emails = new LinkedHashSet<>();
    for (Email e : oldEmails) {
      if (!master.emails.contains(e)) {
        emails.add(e);
        isDifferent = true;
      }
    }

    Set<Address> oldAddresses = addresses;
    addresses = new LinkedHashSet<>();
    for (Address a : oldAddresses) {
      if (!master.addresses.contains(a)) {
        addresses.add(a);
        isDifferent = true;
      }
    }

    Set<PhoneNumber> oldPhoneNumbers = phoneNumbers;
    phoneNumbers = new LinkedHashSet<>();
    for (PhoneNumber n : oldPhoneNumbers) {
      if (!master.phoneNumbers.contains(n)) {
        phoneNumbers.add(n);
        isDifferent = true;
      }
    }


    Set<SocialLink> oldSocialLinks = socialLinks;
    socialLinks = new LinkedHashSet<>();
    for (SocialLink l : oldSocialLinks) {
      if (!master.socialLinks.contains(l)) {
        socialLinks.add(l);
        isDifferent = true;
      }
    }

    Set<String> oldUrls = urls;
    urls = new LinkedHashSet<>();
    for (String u : oldUrls) {
      if (!master.urls.contains(u)) {
        urls.add(u);
        isDifferent = true;
      }
    }

    Set<String> oldImageUrls = imageUrls;
    imageUrls = new LinkedHashSet<>();
    for (String url: oldImageUrls) {
      if (!master.imageUrls.contains(url)) {
        imageUrls.add(url);
        isDifferent = true;
      }
    }
    Map<String, String> oldAdditionalProperties = additionalProperties;
    additionalProperties = new HashMap<>();
    for (String s : oldAdditionalProperties.keySet()) {
      if (!master.additionalProperties.containsKey(s) || !master.additionalProperties.get(s)
                                                                                     .equals(oldAdditionalProperties
                                                                                                 .get(
                                                                                         s))) {
        additionalProperties.put(s, oldAdditionalProperties.get(s));
        isDifferent = true;
      }
    }

    if (DEBUG) {
      Log.debug("<<<<<<< After scrapping:" + toString());
    }
    return isDifferent;
  }

  @Override
  public Object clone()
      throws CloneNotSupportedException {
    PoiJson p = new PoiJson();
    p.poiId = poiId;
    p.poiIdJs = poiIdJs;
    p.poiTypeId = poiTypeId;
    p.poiTypeName = poiTypeName;
    p.name = name;
    p.desc = desc;
    p.code = code;
    p.cmpyId = cmpyId;
    p.fileCount = fileCount;

    p.tags.addAll(tags);
    p.emails.addAll(emails);
    p.addresses.addAll(addresses);
    p.phoneNumbers.addAll(phoneNumbers);
    p.socialLinks.addAll(socialLinks);
    p.urls.addAll(urls);
    p.additionalProperties.putAll(additionalProperties);
    return p;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    String nl = System.getProperty("line.separator");
    sb.append(nl + "============= POI =============" + nl);
    sb.append("ID         :" + poiId + nl);
    sb.append("Type ID    :" + poiTypeId + nl);
    sb.append("Cmpy ID    :" + cmpyId + nl);
    sb.append("File Count :" + fileCount + nl);
    sb.append("Type Name  :" + poiTypeName + nl);
    sb.append("Name       :" + name + nl);
    sb.append("Description:" + desc + nl);
    sb.append("Code       :" + code + nl);
    sb.append("Tags       :");
    for (String tag : tags) {
      sb.append(tag + ", ");
    }
    sb.append(nl);
    sb.append("Emails     :");
    for (Email email : emails) {
      sb.append(email.toString());
    }
    sb.append(nl);
    sb.append("Phones     :");
    for (PhoneNumber phone : phoneNumbers) {
      sb.append(phone.toString());
    }
    sb.append(nl);

    sb.append("Addresses  :");
    for (Address addr : addresses) {
      sb.append(addr.toString());
    }
    sb.append(nl);

    sb.append("Social     :");
    for (SocialLink sl : socialLinks) {
      sb.append(sl.toString());
    }
    sb.append(nl);

    sb.append("Urls       :" + nl);
    for (String url : urls) {
      sb.append(" ->" + url + nl);
    }
    sb.append("Properties:" + nl);
    for (String prop : additionalProperties.keySet()) {
      sb.append("{" + prop + "}:{" + additionalProperties.get(prop) + "}," + nl);
    }
    sb.append("^^^^^^^^^^^^^ POI ^^^^^^^^^^^^^" + nl);

    return sb.toString();
  }

  public void overwriteWith(PoiJson p) {
    poiId = p.poiId;
    poiTypeId = p.poiTypeId;
    poiTypeName = p.poiTypeName;
    cmpyId = p.cmpyId;
    if (p.fileCount != null) {
      if (fileCount == null) {
        fileCount = new Integer(0);
      }
      fileCount += p.fileCount;
    }

    if (p.name != null && p.name.length() > 0) {
      name = p.name;
    }

    if (p.desc != null && p.desc.length() > 0) {
      desc = p.desc;
    }

    if (p.code != null && p.code.length() > 0) {
      code = p.code;
    }

    tags.addAll(p.tags);

    for (Email e : p.emails) {
      for (Iterator<Email> it = emails.iterator(); it.hasNext(); ) {
        Email currItem = it.next();
        if (currItem.getEmailType() == e.getEmailType()) {
          it.remove();
          break;
        }
      }
      emails.add(e);
    }

    if (DEBUG) {
      for (Address a : addresses) {
        for (Address b : p.addresses) {
          Log.log(LogLevel.DEBUG, "Equals=" + a.equals(b) + " hashA:" + a.hashCode() + " hashB:" + b.hashCode());
        }
      }
    }

    for (Address a : p.addresses) {
      for (Iterator<Address> it = addresses.iterator(); it.hasNext(); ) {
        Address currItem = it.next();
        if (currItem.getAddressType() == a.getAddressType()) {
          /*
           * If lower priority poi has GPS coordinates, preserve them
           */
          if ((a.getCoordinates() == null || a.getCoordinates().getLatitude() == null ||
              a.getCoordinates().getLatitude() == 0.0 || a.getCoordinates().getLongitude() == null ||
              a.getCoordinates().getLongitude() == 0.0) &&
              currItem.getCoordinates() != null &&
              ((currItem.getCoordinates().getLatitude() != null && currItem.getCoordinates().getLatitude() != 0.0) ||
               (currItem.getCoordinates().getLongitude() != null && currItem.getCoordinates().getLongitude() != 0.0))) {
            a.setCoordinates(currItem.getCoordinates());
          }
          it.remove();
          break;
        }
      }
      addresses.add(a);
    }

    for (PhoneNumber ph : p.phoneNumbers) {
      for (Iterator<PhoneNumber> it = phoneNumbers.iterator(); it.hasNext(); ) {
        PhoneNumber currItem = it.next();
        if (currItem.getPhoneType() == ph.getPhoneType()) {
          it.remove();
          break;
        }
      }
      phoneNumbers.add(ph);
    }

    for (SocialLink sl : p.socialLinks) {
      for (Iterator<SocialLink> it = socialLinks.iterator(); it.hasNext(); ) {
        SocialLink currItem = it.next();
        if (currItem.getServiceName().equals(sl.getServiceName())) {
          it.remove();
          break;
        }
      }
      socialLinks.add(sl);
    }


    for (Feature f : p.features) {
      for (Iterator<Feature> fit = features.iterator(); fit.hasNext(); ) {
        Feature currItem = fit.next();
        if (currItem.getName().equals(f.getName())) {
          fit.remove();
          break;
        }
      }
      features.add(f);
    }


    if (p.urls.size() > 0) {
      urls.clear();
      urls.addAll(p.urls);
    }

    additionalProperties.putAll(p.additionalProperties);
  }

  @JsonProperty(required = false, value = "cmpy-id")
  public Integer getCmpyId() {
    return cmpyId;
  }

  @JsonProperty(required = false, value = "cmpy-id")
  public void setCmpyId(Integer cmpyId) {
    this.cmpyId = cmpyId;
  }

  public PoiJson withCmpyId(Integer cmpyId) {
    this.cmpyId = cmpyId;
    return this;
  }

  @JsonProperty(required = false, value = "file-count")
  public Integer getFileCount() {
    return fileCount;
  }

  @JsonProperty(required = false, value = "file-count")
  public void setFileCount(Integer fileCount) {
    this.fileCount = fileCount;
  }

  public PoiJson withFileCount(Integer fileCount) {
    this.fileCount = fileCount;
    return this;
  }

  /**
   * @return The poiId
   */
  @JsonProperty("poi-id")
  public Long getPoiId() {
    return poiId;
  }

  /**
   * @param poiId The poi-id
   */
  @JsonProperty("poi-id")
  public void setPoiId(Long poiId) {
    this.poiId = poiId;
    if (poiId != null) {
      this.poiIdJs = poiId.toString();
    }
  }

  public PoiJson withPoiId(Long poiId) {
    this.poiId = poiId;
    return this;
  }

  /**
   * @return The poiTypeId
   */
  @JsonProperty("poi-type-id")
  public Integer getPoiTypeId() {
    return poiTypeId;
  }

  /**
   * @param poiTypeId The poi-type-id
   */
  @JsonProperty("poi-type-id")
  public void setPoiTypeId(Integer poiTypeId) {
    this.poiTypeId = poiTypeId;
    if (poiTypeId != null) {
      poiTypeName = PoiTypeInfo.Instance().byId(poiTypeId).getName();
    }
  }

  public PoiJson withPoiTypeId(Integer poiTypeId) {
    this.poiTypeId = poiTypeId;
    return this;
  }

  /**
   * @return The poiTypeName
   */
  @JsonProperty("poi-type-name")
  public String getPoiTypeName() {
    return poiTypeName;
  }

  /**
   * @param poiTypeName The poi-type-name
   */
  @JsonProperty("poi-type-name")
  public void setPoiTypeName(String poiTypeName) {
    this.poiTypeName = poiTypeName;
  }

  public PoiJson withPoiTypeName(String poiTypeName) {
    this.poiTypeName = poiTypeName;
    return this;
  }

  /**
   * @return The name
   */
  @JsonProperty("name")
  public String getName() {
    return name;
  }

  /**
   * @param name The name
   */
  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

  public PoiJson withName(String name) {
    this.name = name;
    return this;
  }

  /**
   * @return The desc
   */
  @JsonProperty("desc")
  public String getDesc() {
    return desc;
  }

  /**
   * @param desc The desc
   */
  @JsonProperty("desc")
  public void setDesc(String desc) {
    this.desc = desc;
  }

  public PoiJson withDesc(String desc) {
    this.desc = desc;
    return this;
  }

  @JsonProperty("code")
  public String getCode() {
    return code;
  }

  @JsonProperty("code")
  public void setCode(String code) {
    this.code = code;
  }

  /**
   * @return The tags
   */
  @JsonProperty("tags")
  public Set<String> getTags() {
    return tags;
  }

  /**
   * @param tags The tags
   */
  @JsonProperty("tags")
  public void setTags(Set<String> tags) {
    this.tags = tags;
  }

  public PoiJson withTags(Set<String> tags) {
    this.tags = tags;
    return this;
  }

  /**
   * Imports tags from a comman separated string
   *
   * @param commaedTags
   */
  public void addTags(String commaedTags) {
    if (commaedTags != null) {
      String[] inTags = StringUtils.split(commaedTags, ',');

      for (String t : inTags) {
        tags.add(t.trim());
      }
    }
  }

  /**
   * Delete all tags
   */
  public void deleteTags() {
    tags.clear();
  }

  /**
   * Returns comma separated tags
   *
   * @return
   */
  @JsonIgnore
  public String getTagsString() {
    return StringUtils.join(tags, ", ");
  }

  /**
   * @return The emails
   */
  @JsonProperty("emails")
  public Set<Email> getEmails() {
    return emails;
  }

  /**
   * @param emails The emails
   */
  @JsonProperty("emails")
  public void setEmails(Set<Email> emails) {
    this.emails = emails;
  }

  public PoiJson withEmails(Set<Email> emails) {
    this.emails = emails;
    return this;
  }

  public void addEmail(Email email) {
    this.emails.add(email);
  }

  /**
   * @return The addresses
   */
  @JsonProperty("addresses")
  public Set<Address> getAddresses() {
    return addresses;
  }

  /**
   * @param addresses The addresses
   */
  @JsonProperty("addresses")
  public void setAddresses(Set<Address> addresses) {
    this.addresses = addresses;
  }

  public PoiJson withAddresses(Set<Address> addresses) {
    this.addresses = addresses;
    return this;
  }

  /**
   * Adding address to the set
   *
   * @param address
   */
  public void addAddress(Address address) {
    addresses.add(address);
  }

  /**
   * @return The phoneNumbers
   */
  @JsonProperty("phone-numbers")
  public Set<PhoneNumber> getPhoneNumbers() {
    return phoneNumbers;
  }

  /**
   * @param phoneNumbers The phone-numbers
   */
  @JsonProperty("phone-numbers")
  public void setPhoneNumbers(Set<PhoneNumber> phoneNumbers) {
    this.phoneNumbers = phoneNumbers;
  }

  public PoiJson withPhoneNumbers(Set<PhoneNumber> phoneNumbers) {
    this.phoneNumbers = phoneNumbers;
    return this;
  }

  public void addPhoneNumber(PhoneNumber number) {
    phoneNumbers.add(number);
  }

  /**
   * @return The socialLinks
   */
  @JsonProperty("social-links")
  public Set<SocialLink> getSocialLinks() {
    return socialLinks;
  }

  /**
   * @param socialLinks The social-links
   */
  @JsonProperty("social-links")
  public void setSocialLinks(Set<SocialLink> socialLinks) {
    this.socialLinks = socialLinks;
  }

  public PoiJson withSocialLinks(Set<SocialLink> socialLinks) {
    this.socialLinks = socialLinks;
    return this;
  }

  public void addSocialLink(SocialLink socialLink) {
    socialLinks.add(socialLink);
  }

  /**
   * URL verification pattern should be used to verify content
   *
   * @return The url
   */
  @JsonProperty("urls")
  public Set<String> getUrls() {
    return urls;
  }

  /**
   * URL verification pattern should be used to verify content
   *
   * @param url The url
   */
  @JsonProperty("urls")
  public void setUrls(Set<String> urls) {
    this.urls = urls;
  }

  public PoiJson withUrls(Set<String> urls) {
    this.urls = urls;
    return this;
  }

  public void addUrl(String url) {
    this.urls.add(url);
  }
  /**
   * URL verification pattern should be used to verify content
   *
   * @param imageUrl The url
   */
  @JsonProperty("image-urls")
  public void setImageUrls(Set<String> imageUrls) {
    this.imageUrls = imageUrls;
  }

  public PoiJson withImageUrls(Set<String> imageUrls) {
    this.imageUrls = imageUrls;
    return this;
  }

  public void addImageUrl(String imageUrl) {
    this.imageUrls.add(imageUrl);
  }

  /**
   * @return The features
   */
  @JsonProperty("features")
  public Set<Feature> getFeatures() {
    return features;
  }

  /**
   * @param features The features
   */
  @JsonProperty("features")
  public void setFeatures(Set<Feature> features) {
    this.features = features;
  }

  public PoiJson withFeatures(Set<Feature> features) {
    this.features = features;
    return this;
  }

  @JsonAnyGetter
  public Map<String, String> getAdditionalProperties() {
    return this.additionalProperties;
  }

  @JsonAnySetter
  public void setAdditionalProperty(String name, String value) {
    this.additionalProperties.put(name, value);
  }

  public PoiJson withAdditionalProperty(String name, String value) {
    this.additionalProperties.put(name, value);
    return this;
  }


  @JsonIgnore
  public PhoneNumber getFax() {
    for(PhoneNumber ph: phoneNumbers) {
      if(ph.getPhoneType() == PhoneNumber.PhoneType.FAX) {
        return ph;
      }
    }
    return null;
  }
}
