package com.umapped.external.travelbound;

import com.umapped.external.travelbound.api.TError;
import com.umapped.external.travelbound.api.TErrors;

import java.util.Collections;
import java.util.List;

/**
 * Created by wei on 2017-05-09.
 */
public class TravelboundAPIException extends Exception {
  private List<TError> errors;

  public TravelboundAPIException(String message) {
    super(message);
    this.errors = Collections.emptyList();
  }

  public TravelboundAPIException(List<TError> errors) {
    super();
    this.errors = errors;
  }

  public List<TError> getErrors() {
    return errors;
  }
}
