package com.mapped.publisher.parse.valueObject;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-22
 * Time: 9:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class DayDetails {
    private long timestamp;
    private long endTimestamp;

    private String intro;
    private String body;

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(long endTimestamp) {
        this.endTimestamp = endTimestamp;
    }
}
