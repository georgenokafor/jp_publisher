
package com.umapped.external.wetu.itinerary.details;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "rooms",
    "name",
    "simple_item_id",
    "room_type"
})
public class Room {

    @JsonProperty("rooms")
    private int rooms;
    @JsonProperty("name")
    private String name;
    @JsonProperty("simple_item_id")
    private String simpleItemId;
    @JsonProperty("room_type")
    private String roomType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The rooms
     */
    @JsonProperty("rooms")
    public int getRooms() {
        return rooms;
    }

    /**
     * 
     * @param rooms
     *     The rooms
     */
    @JsonProperty("rooms")
    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The simpleItemId
     */
    @JsonProperty("simple_item_id")
    public String getSimpleItemId() {
        return simpleItemId;
    }

    /**
     * 
     * @param simpleItemId
     *     The simple_item_id
     */
    @JsonProperty("simple_item_id")
    public void setSimpleItemId(String simpleItemId) {
        this.simpleItemId = simpleItemId;
    }

    /**
     * 
     * @return
     *     The roomType
     */
    @JsonProperty("room_type")
    public String getRoomType() {
        return roomType;
    }

    /**
     * 
     * @param roomType
     *     The room_type
     */
    @JsonProperty("room_type")
    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(rooms).append(name).append(simpleItemId).append(roomType).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Room) == false) {
            return false;
        }
        Room rhs = ((Room) other);
        return new EqualsBuilder().append(rooms, rhs.rooms).append(name, rhs.name).append(simpleItemId, rhs.simpleItemId).append(roomType, rhs.roomType).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
