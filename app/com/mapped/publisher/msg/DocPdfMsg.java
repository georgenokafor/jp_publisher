package com.mapped.publisher.msg;

import play.api.i18n.Lang;

import java.io.Serializable;

/**
 * Created by twong on 2014-12-24.
 */
public class DocPdfMsg
    implements Serializable {
  public Long    accountId;
  public String  destinationId;
  public String  tripId;
  public String  fileName;
  public boolean htmlDebug;
  public Lang    lang;
}
