package com.umapped.mobile.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.umapped.mobile.api.model.Itinerary;
import models.publisher.Account;
import models.publisher.Trip;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.ApplicationLoader;
import play.Environment;
import play.inject.guice.GuiceApplicationBuilder;
import play.inject.guice.GuiceApplicationLoader;
import play.test.Helpers;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by wei on 2017-06-30.
 */
public class MobileItineraryServiceTest {
  @Inject Application application;

  @Inject MobileItineraryService service;
  @Before
  public void setup() {
    Module testModule = new AbstractModule() {
      @Override
      public void configure() {
        // Install custom test binding here
      }
    };

    GuiceApplicationBuilder builder = new GuiceApplicationLoader()
        .builder(new ApplicationLoader.Context(Environment.simple()))
        .overrides(testModule);
    Guice.createInjector(builder.applicationModule()).injectMembers(this);

    Helpers.start(application);

  }

  @After
  public void teardown() {
    Helpers.stop(application);
  }

  //@Test
  public void testGetItinerary() throws IOException {
    Account account = new Account();
    account.setUid(1416765721790000041L);
    List<Trip> trips = service.findActiveTripsForTraveler(account);
    System.out.println(trips);
  }
}
