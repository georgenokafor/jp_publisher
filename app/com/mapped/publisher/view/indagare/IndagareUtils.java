package com.mapped.publisher.view.indagare;

import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.umapped.persistence.enums.ReservationType;
import models.publisher.Account;
import org.openrdf.query.algebra.In;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 03/05/17.
 */
public class IndagareUtils {

    public static String diffDays (long start,long end) {
        Instant s = Instant.ofEpochMilli(start);
        Instant e = Instant.ofEpochMilli(end);
        long l = ChronoUnit.DAYS.between(s.truncatedTo(ChronoUnit.DAYS), e.truncatedTo(ChronoUnit.DAYS));
        if (l == -1) {
            return "- 1 Day";
        }
        if (l == -2) {
            return "- 2 Days";
        }
        if ( l== 1 ) {
            return "+ 1 Day";
        }

        return "";
    }

    public static TripBookingDetailView getLetter(TripBookingView view) {
        if (view != null && view.notes != null) {
            for (TripBookingDetailView n : view.notes) {
                if (n.name != null && n.name.trim().toLowerCase().startsWith("dear ")) {
                    return n;
                }
            }
        }
        return null;
    }

    public static TripBookingDetailView getContact(TripBookingView view) {
        if (view != null && view.notes != null) {
            for (TripBookingDetailView n : view.notes) {
                if (n.name != null && n.name.trim().toLowerCase().startsWith("important contact")) {
                    return n;
                }
            }
        }
        return null;
    }

    public static TripBookingDetailView getInfo(TripBookingView view) {
        if (view != null && view.notes != null) {
            for (TripBookingDetailView n : view.notes) {
                if (n.name != null && n.name.trim().toLowerCase().startsWith("important information")) {
                    return n;
                }
            }
        }
        return null;
    }

    public static TripBookingDetailView getOverview(TripBookingView view) {
        if (view != null && view.notes != null) {
            for (TripBookingDetailView n : view.notes) {
                if (n.name != null && n.name.trim().toLowerCase().startsWith("itinerary overview")) {
                    return n;
                }
            }
        }
        return null;
    }

    public static String getHotelDate(TripBookingDetailView h) {
        StringBuilder sb = new StringBuilder();
        if (h != null && h.startDateMs > 1 && h.endDateMs > 1) {
            sb.append(Utils.formatTimestamp(h.startDateMs, "MMM dd"));
            sb.append(" - ");
            sb.append(Utils.formatTimestamp(h.endDateMs, "MMM dd"));
            if (h.startDateMs > 1 && h.endDateMs > h.startDateMs) {
                int days = Utils.getDaysBetween(h.startDateMs, h.endDateMs);//()int) Math.floor(d);
                sb.append(" (");
                if (days > 1)
                    sb.append(String.valueOf(days) + " nights)");
                else
                    sb.append(String.valueOf(days) + " night)");
            }
        }
        return sb.toString();
    }

    public static String getHotelNights(TripBookingDetailView h, TripPreviewView m) {
        StringBuilder sb = new StringBuilder();
        if (h != null && h.startDateMs > 1 && h.endDateMs > 1) {
            if (h.startDateMs > 1 && h.endDateMs > h.startDateMs) {
                int startDay = Utils.getDaysBetween(Long.parseLong(m.tripStartDateMs), h.startDateMs) + 1;
                int endDay = Utils.getDaysBetween(Long.parseLong(m.tripStartDateMs), h.endDateMs) + 1;

                sb.append("DAY ");
                sb.append(startDay);
                sb.append("-");
                sb.append(endDay);

            }
        }
        return sb.toString();
    }

    public static String getRoomType(TripBookingDetailView h) {
        UmAccommodationReservationView hotelView = (UmAccommodationReservationView) h;
        StringBuilder sb = new StringBuilder();

        if (hotelView != null && hotelView.getPassengers() != null && hotelView.getPassengers().size() > 0) {
            for (UmAccommodationReservationView.PassengerInfo p: hotelView.getPassengers()) {
                if (p.getRoomType() != null && !p.getRoomType().trim().isEmpty()) {
                    if (sb.toString().length() > 0) {
                        sb.append(", ");
                    }
                    sb.append(p.getRoomType());
                    if(p.getBedding() != null && !p.getBedding().isEmpty()) {
                        sb.append(" with ");
                        sb.append(p.getBedding());
                    }

                }
            }
        }
        if (h != null && h.origComments != null && sb.toString().length() == 0) {
            String[] tokens = h.origComments.split("\n");
            for (String s : tokens) {
                if (s.startsWith("Room Type:") && s.length() > 10) {
                    return s.substring(10);
                }
            }
        }
        return sb.toString();
    }

    public static String getAmenities(TripBookingDetailView h) {
        UmAccommodationReservationView hotelView = (UmAccommodationReservationView) h;
        StringBuilder sb = new StringBuilder();

        if (hotelView != null && hotelView.getPassengers() != null && hotelView.getPassengers().size() > 0) {
            for (UmAccommodationReservationView.PassengerInfo p: hotelView.getPassengers()) {
                if (p.getAmenities() != null && !p.getAmenities().trim().isEmpty() && !sb.toString().contains(p.getAmenities())) {
                    if (sb.toString().length() > 0) {
                        sb.append("<br/>");
                    }
                    sb.append(p.getAmenities());

                }
            }
        }
        if (h != null && h.comments != null ) {
            String[] tokens = h.comments.split("<br/>");

            boolean processing = false;
            for (String s : tokens) {
                if ((s.startsWith("Indagare Amenities") || s.startsWith("Amenities"))) {
                    processing = true;
                    int i = s.indexOf(":") + 1;
                    if (i > 0 && i < s.length()) {
                        String s1 = s.substring(i);
                        if (s1 != null && !s1.isEmpty()) {
                            sb.append(s1.trim());
                        }
                    }
                } else if (s.isEmpty()) {
                    processing = false;
                } else if (processing) {
                    if (sb.toString().length() > 0) {
                        sb.append("<br/>");
                    }
                    sb.append(s);
                }
            }
        }
        return sb.toString();
    }

    public static String removeAmenities (String h) {
        StringBuilder sb = new StringBuilder();

        if (h != null ) {
            String[] tokens = h.split("<br/>");

            boolean processing = false;
            for (String s : tokens) {
                if ((s.startsWith("Indagare Amenities") || s.startsWith("Amenities"))) {
                    processing = true;
                } else if (s.isEmpty()) {
                    processing = false;
                } else if (!processing) {
                    if (sb.toString().length() > 0) {
                        sb.append("<br/>");
                    }
                    sb.append(s);
                }
            }
        }

        return sb.toString();
    }
    public static String getMeals(TripBookingDetailView h) {
        if (h != null && h.origComments != null) {
            String[] tokens = h.origComments.split("\n");
            for (String s : tokens) {
                if (s.startsWith("Meals Included:") && s.length() > 15) {
                    return s.substring(15);
                }
            }
        }
         return "";
    }



    public static String getTip(TripBookingDetailView h) {
        if (h != null && h.origComments != null) {
            String[] tokens = h.origComments.split("\n");
            for (String s : tokens) {
                if (s.startsWith("Indagare Tip") && s.length() > 13) {
                    return s.substring(13).trim();
                }
            }
        }
        return "";
    }

    public static String removeTips(TripBookingDetailView h) {
        StringBuilder sb = new StringBuilder();
        if (h != null && h.comments != null) {
            String[] tokens = h.comments.split("<br/>");
            for (String s : tokens) {
                if (!s.startsWith("Indagare Tip") ) {
                    sb.append(s);
                    sb.append("<br/>");
                }
            }
        }
        return removeAmenities(sb.toString());

    }

    public static String getFooter(TripPreviewView v) {
        StringBuilder sb = new StringBuilder();
        if (v.tripName.contains("-") && v.tripName.length() > (v.tripName.indexOf(" - ") + 3)) {
            sb.append("<span style='font-family: Futura-Bold;font-size: 7pt'>");
            sb.append(v.tripName.substring(0, v.tripName.indexOf("-")));
            sb.append("</span>");
            sb.append("&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;");
            sb.append(v.tripName.substring(v.tripName.indexOf("-") + 1));
        } else if (v.tripName.contains(" for ") && v.tripName.length() > (v.tripName.indexOf(" for ") + 5)) {
            sb.append("<span style='font-family: Futura-Bold;font-size: 7pt'>");
            sb.append(v.tripName.substring(0, v.tripName.indexOf(" for ")));
            sb.append("</span>");
            sb.append("&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;");
            sb.append(v.tripName.substring(v.tripName.indexOf(" for ") + 5));
        } else {
            sb.append(v.tripName);
        }
        sb.append("&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;");
        if (Utils.formatTimestamp(Long.parseLong(v.tripStartDateMs), "MMM").equals(Utils.formatTimestamp(Long.parseLong(v.tripEndDateMs), "MMM"))) {
            sb.append(Utils.formatTimestamp(Long.parseLong(v.tripStartDateMs), "MMM"));
            sb.append(" ");
            sb.append(Utils.formatTimestamp(Long.parseLong(v.tripStartDateMs), "dd"));
            sb.append(" - ");
            sb.append(Utils.formatTimestamp(Long.parseLong(v.tripEndDateMs), "dd"));
            sb.append(", ");
            sb.append(Utils.formatTimestamp(Long.parseLong(v.tripStartDateMs), "YYYY"));
        } else {
            sb.append(v.tripStartDatePrint);
            sb.append(" - ");
            sb.append(v.tripEndDatePrint);
        }

        return sb.toString();
    }

    public static String getEmailTitle(EmailView v) {
        StringBuilder sb = new StringBuilder();

        if (v.tripInfo.tripName.contains("-") && v.tripInfo.tripName.length() > (v.tripInfo.tripName.indexOf(" - ") + 3)) {
            sb.append(v.tripInfo.tripName.substring(0, v.tripInfo.tripName.indexOf("-")));
            sb.append("<br/>");
            sb.append(v.tripInfo.tripName.substring(v.tripInfo.tripName.indexOf("-") + 1));
            sb.append("<br/>");
        } else if (v.tripInfo.tripName.contains(" for ") && v.tripInfo.tripName.length() > (v.tripInfo.tripName.indexOf(" for ") + 5)) {
            sb.append(v.tripInfo.tripName.substring(0, v.tripInfo.tripName.indexOf(" for ")));
            sb.append("<br/>");
            sb.append(v.tripInfo.tripName.substring(v.tripInfo.tripName.indexOf(" for ") + 5));
            sb.append("<br/>");
        } else {
            sb.append(v.tripInfo.tripName);
            sb.append("<br/>");
        }

        sb.append(v.tripInfo.tripStartDatePrint);
        sb.append(" - ");
        sb.append(v.tripInfo.tripEndDatePrint);

        return sb.toString();
    }

    public static Account findTraveler(Long travelerId) {
        return Account.find.byId(travelerId);
    }

    public static String getTittle(TripPreviewView v) {
        StringBuilder sb = new StringBuilder();
        if (v.tripName.contains("-") && v.tripName.length() > (v.tripName.indexOf(" - ") + 3)) {
            sb.append("<span class=' boxTitle'>");
            sb.append(v.tripName.substring(0, v.tripName.indexOf("-")));
            sb.append("</span><br/>");
            sb.append(v.tripName.substring(v.tripName.indexOf("-") + 1));
        } else if (v.tripName.contains(" for ") && v.tripName.length() > (v.tripName.indexOf(" for ") + 5)) {
            sb.append("<span class=' boxTitle'>");
            sb.append(v.tripName.substring(0, v.tripName.indexOf(" for ")));
            sb.append("</span><br/>");
            sb.append(v.tripName.substring(v.tripName.indexOf(" for ") + 5));
        } else {
            sb.append("<span class=' boxTitle'>");
            sb.append(v.tripName);
            sb.append("</span><br/>");
        }

        sb.append("<br/>");
        if (Utils.formatTimestamp(Long.parseLong(v.tripStartDateMs), "MMM").equals(Utils.formatTimestamp(Long.parseLong(v.tripEndDateMs), "MMM"))) {
            sb.append(Utils.formatTimestamp(Long.parseLong(v.tripStartDateMs), "MMM"));
            sb.append(" ");
            sb.append(Utils.formatTimestamp(Long.parseLong(v.tripStartDateMs), "dd"));
            sb.append(" - ");
            sb.append(Utils.formatTimestamp(Long.parseLong(v.tripEndDateMs), "dd"));
            sb.append(", ");
            sb.append(Utils.formatTimestamp(Long.parseLong(v.tripStartDateMs), "YYYY"));
        } else {
            sb.append(v.tripStartDatePrint);
            sb.append(" - ");
            sb.append(v.tripEndDatePrint);
        }

        return sb.toString();
    }

    public static TripBookingDetailView getNote(TripBookingDetailView hotel, TripBookingView v) {
        if (hotel != null) {
            for (TripBookingDetailView note : v.notes) {
                if (note.linkedTripDetailsId != null && note.linkedTripDetailsId.equals(hotel.detailsId)) {
                    return note;
                }
            }
        }
        return null;
    }

    public static String pageBreak(TripBookingDetailView hotel, TripBookingView v) {
        TripBookingDetailView note = getNote(hotel, v);
        if (note != null && note.tag != null && !note.tag.isEmpty()) {
            if (note.tag.toLowerCase().contains("page break before")) {
                return "style='page-break-before:always'";
            } else if (note.tag.toLowerCase().contains("page break after")) {
                return "style='page-break-after: always'";

            }
        }
        return "";
    }
    public static String nextPageBreak(int i, TripBookingView v) {
        if ((i + 1) < v.hotels.size()) {
            for (int j = i+ 1; j < v.hotels.size(); j++) {
                TripBookingDetailView note = getNote(v.hotels.get(j), v);
                if (note != null) {
                    return pageBreak(v.hotels.get(j), v);
                }

            }
        }
        return "";
    }

    public static boolean hasAccomodationInDepth(TripPreviewView m) {
        boolean hasInDepth = false;

        if (m.bookings != null && m.bookings.hotels != null && m.bookings.hotels.size() > 0 && m.bookings.notes != null && m.bookings.notes.size() > 0) {
            for (TripBookingDetailView n : m.bookings.notes) {
                if (n != null && n.linkedTripDetailsId != null && !n.linkedTripDetailsId.isEmpty()) {
                    for (TripBookingDetailView h : m.bookings.hotels) {
                        if (h.detailsId.equals(n.linkedTripDetailsId)) {
                            return true;
                        }
                    }
                }
            }
        }

        return hasInDepth;
    }


    public static String getDayByDayTitle(String date, TripPreviewView v) {
        StringBuilder sb = new StringBuilder();
        if (date != null && v != null && v.bookings != null && v.bookings.chronologicalBookings != null && v.bookings.chronologicalBookings.containsKey(date)) {
            List<TripBookingDetailView> bookings = v.bookings.chronologicalBookings.get(date);
            //are there flight
            List<String> cities = new ArrayList<>();
            boolean haveFlights = false;
            for (TripBookingDetailView booking : bookings) {
                if (booking.noteId == null && booking.detailsTypeId != null && booking.detailsTypeId.getRootLevelType() == ReservationType.FLIGHT) {
                    haveFlights = true;
                } else if (booking.city != null && !booking.city.isEmpty() && !cities.contains(booking.city)) {
                    cities.add(booking.city.replace(",",""));
                }
            }

            if (haveFlights) {
                sb.append("Flights");
                if (cities.size() > 0) {
                    sb.append(" / ");
                }
            }
            for (String city : cities) {
                sb.append(city);
                if (cities.indexOf(city) != (cities.size() - 1)) {
                    sb.append(" / ");
                }
            }
        }

        StringBuilder title = new StringBuilder();
        title.append("<span class=\"black_title fxlarge fheavy\">");
        title.append(date);
        if (!sb.toString().isEmpty()) {
            title.append(":");
        }
        title.append("</span>");
        if (!sb.toString().isEmpty()) {
            title.append("<span class=\"red_title fxlarge fheavy\" style=\"margin-left: 10px\">");
            title.append(sb.toString());
            title.append("</span>");
        }

        return title.toString();
    }

    public static String getDayByDayNum(String date, TripPreviewView v) {
        long dateMs = Utils.getMilliSecs(date);
        long startDateMs = Long.parseLong(v.tripStartDateMs);
        if (dateMs > 0) {
            int i = Utils.getDaysBetween(startDateMs, dateMs);
            String s = String.valueOf(i+1);
            return "DAY " + s;
        }
        return "";
    }

    public static String getTime (long startDateMs) {
         if(startDateMs > 0) {
             String time = Utils.formatTimestamp(startDateMs, "h:mm");
             String time1 = Utils.formatTimestamp(startDateMs, "h:mma");

             if (time != null && !time1.contains("12:00AM")) {
                 StringBuilder sb = new StringBuilder();
                 sb.append("<span class='timeTitle'>");
                 sb.append(time);
                 sb.append("</span>");
                 sb.append("<span class='timeTitleIndicator'>");
                 sb.append(Utils.formatTimestamp(startDateMs, " a"));
                 sb.append("</span>");
                 return sb.toString();
             }
        }
        return "";
    }
    public static String getTimeHr (long startDateMs) {
        if(startDateMs > 0) {
            String time = Utils.formatTimestamp(startDateMs, "h");
            String time1 = Utils.formatTimestamp(startDateMs, "ha");
            if (time != null && !time.contains("12AM")) {
                StringBuilder sb = new StringBuilder();
                sb.append("<span class='timeTitle'>");
                sb.append(time);
                sb.append("</span>");
                sb.append("<span class='timeTitleIndicator'>");
                sb.append(Utils.formatTimestamp(startDateMs, "a"));
                sb.append("</span>");
                return sb.toString();
            }
        }
        return "";
    }

    public static String getCheckinOut (TripBookingDetailView h) {
        StringBuilder sb = new StringBuilder();
        sb.append(IndagareUtils.getTimeHr(h.startDateMs));
        sb.append("/");
        sb.append(IndagareUtils.getTimeHr(h.endDateMs));


        return sb.toString();
    }



    public static String getFlightClass (com.mapped.publisher.view.EnhancedTripBookingDetailView e) {
        if(e != null) {
            for (EnhancedTripBookingDetailView.PassengerInfo p  : e.passengers) {
                if (p.fClass != null && !p.fClass.isEmpty()) {
                    return " in " + p.fClass;
                }
            }
        }
        return "";
    }

    public static String getIcon (TripBookingDetailView booking) {
        if (booking != null && booking.noteId == null) {
            StringBuffer sb = new StringBuffer();
            if (booking.detailsTypeId != null && (booking.detailsTypeId.getRootLevelType() == ReservationType.ACTIVITY ||
                    booking.detailsTypeId.getRootLevelType() == ReservationType.FLIGHT) ||
                    booking.detailsTypeId.getRootLevelType() == ReservationType.HOTEL ||
                    booking.detailsTypeId.getRootLevelType() == ReservationType.TRANSPORT) {
             //   sb.append("<br/><br/>");
                if (booking.detailsTypeId.getRootLevelType() == ReservationType.FLIGHT) {
                  //  sb.append("<img src='https://s3.amazonaws.com/static-umapped-com/public/external/indagare/flight.png' width='40px' style='text-align:center'>");
                    sb.append("<div class='fbold fxsmall black_upper'style='padding-top:6px'>");
                    sb.append("Flight");
                    sb.append("</div>");
                } else if (booking.detailsTypeId.getRootLevelType() == ReservationType.HOTEL) {
                  //  sb.append("<img src='https://s3.amazonaws.com/static-umapped-com/public/external/indagare/hotel.png' width='40px' style='text-align:center'>");
                    sb.append("<div class='fbold fxsmall black_upper' style='padding-top:6px'>");
                    sb.append("&nbsp;Hotel");
                    sb.append("</div>");
                } else if (booking.detailsTypeId.getRootLevelType() == ReservationType.TRANSPORT) {
                 //   sb.append("<img src='https://s3.amazonaws.com/static-umapped-com/public/external/indagare/transfer.png' width='35px' style='text-align:center'>");
                    sb.append("<div class='fbold fxsmall black_upper' style='padding-top:6px'>");
                    if(booking.getServiceType() != null && !booking.getServiceType().isEmpty()){
                       sb.append(booking.getServiceType());
                    }else{
                        sb.append("Transfer");
                    }
                    sb.append("</div>");
                } else if (booking.detailsTypeId.getRootLevelType() == ReservationType.ACTIVITY) {
                    if (booking.detailsTypeId == ReservationType.RESTAURANT) {
                   //     sb.append("<img src='https://s3.amazonaws.com/static-umapped-com/public/external/indagare/food.png' width='35px' style='text-align:center'>");
                        sb.append("<div class='fbold fxsmall black_upper' style='padding-top:6px'>");
                        if(booking.getServiceType() != null && !booking.getServiceType().isEmpty()){
                            sb.append(booking.getServiceType());
                        }else{
                            sb.append("Dinner");
                        }
                        sb.append("</div>");
                    } else if (booking.detailsTypeId == ReservationType.TOUR) {
                     //   sb.append("<img src='https://s3.amazonaws.com/static-umapped-com/public/external/indagare/tour.png' width='35px' style='text-align:center'>");
                        sb.append("<div class='fbold fxsmall black_upper' style='padding-top:6px'>");
                        if(booking.getServiceType() != null && !booking.getServiceType().isEmpty()){
                            sb.append(booking.getServiceType());
                        } else {
                            sb.append("Tour");
                        }                        sb.append("</div>");
                    } else {
                       // sb.append("<img src='https://s3.amazonaws.com/static-umapped-com/public/external/indagare/activity.png' width='35px' style='text-align:center'>");
                        sb.append("<div class='fbold fxsmall black_upper' style='padding-top:6px'>");
                        if(booking.getServiceType() != null && !booking.getServiceType().isEmpty()){
                            sb.append(booking.getServiceType());
                        } else {
                            sb.append("Activity");
                        }
                        sb.append("</div>");
                    }
                }

            }


            return sb.toString();
        }
        return "";
    }

    public static String getDuration (TripBookingDetailView booking) {
        if (booking.startDateMs > 0 && booking.endDateMs > 0 && booking.startDateMs != booking.endDateMs) {
            long duration = booking.endDateMs - booking.startDateMs;
            long hours = duration / (1000*60*60);
            long min = (duration - (hours * 60 * 60 * 1000)) / (1000*60);

            StringBuilder sb = new StringBuilder();
            if (hours > 0) {
                sb.append(hours);
            } else {
                sb.append("00");
            }
            sb.append("H ");
            if (min > 0) {
                sb.append(min);
            } else {
                sb.append("00");
            }
            sb.append("M ");
            return sb.toString();
        }
        return "";
    }

    public  static boolean isHotelNote (TripBookingDetailView note, TripPreviewView v) {
        if (note != null && v.bookings != null && v.bookings.hotels != null) {
            for (TripBookingDetailView hotel : v.bookings.hotels) {
                if (note.linkedTripDetailsId != null && note.linkedTripDetailsId.equals(hotel.detailsId)) {
                    return true;
                }
            }
        }
        return false;
    }

    public  static boolean isActivityorTransfer (TripBookingDetailView note, TripPreviewView v) {
        if (note != null && v.bookings != null && v.bookings.activities != null) {
            for (TripBookingDetailView activtity : v.bookings.activities) {
                if (note.linkedTripDetailsId != null && note.linkedTripDetailsId.equals(activtity.detailsId)) {
                    return true;
                }
            }
        }
        if (note != null && v.bookings != null && v.bookings.transfers != null) {
            for (TripBookingDetailView activtity : v.bookings.transfers) {
                if (note.linkedTripDetailsId != null && note.linkedTripDetailsId.equals(activtity.detailsId)) {
                    return true;
                }
            }
        }
        return false;
    }

    public  static String getActivityorTransferPadding (TripBookingDetailView note, TripPreviewView v) {
        String regular = "margin-top: -10px;";
        String more = "margin-top: -5px;";

        if (note != null && v.bookings != null && v.bookings.activities != null) {
            for (TripBookingDetailView activtity : v.bookings.activities) {
                if (note.linkedTripDetailsId != null && note.linkedTripDetailsId.equals(activtity.detailsId)) {
                    if (activtity.getCancellation() != null && !activtity.getCancellation().isEmpty() && ((activtity.comments != null && !activtity.comments.isEmpty()) || (activtity.providerComments != null && !activtity.providerComments.isEmpty()))) {
                        return regular;
                    } else {
                        return more;
                    }
                }
            }
        }
        if (note != null && v.bookings != null && v.bookings.transfers != null) {
            for (TripBookingDetailView activtity : v.bookings.transfers) {
                if (note.linkedTripDetailsId != null && note.linkedTripDetailsId.equals(activtity.detailsId)) {
                    if (activtity.getCancellation() != null && !activtity.getCancellation().isEmpty() && ((activtity.comments != null && !activtity.comments.isEmpty()) || (activtity.providerComments != null && !activtity.providerComments.isEmpty()))) {
                        return regular;
                    } else {
                        return more;
                    }
                }
            }
        }
        return "";
    }

    public  static String getBookingOrNoteName (TripBookingDetailView detail, TripPreviewView v) {
        if (detail != null && v.bookings != null && v.bookings.notes != null) {
            for (TripBookingDetailView note : v.bookings.notes) {
                if (note.linkedTripDetailsId != null && note.linkedTripDetailsId.equals(detail.detailsId)) {
                    return note.name;
                }
            }
        }
        return detail.name;
    }

    public static String getUrl(String url) {
        if (url != null && !url.isEmpty() && !url.startsWith("http")) {
            return "http://" + url;
        }
        return url;
    }


    public static String getFlightPassengers(EnhancedTripBookingDetailView f) {
        StringBuilder sb = new StringBuilder();
        if (f != null && f.passengers != null && f.passengers.size() > 0) {
            int i = 0;
            sb.append("For ");
            for (EnhancedTripBookingDetailView.PassengerInfo m: f.passengers) {
                if(i == (f.passengers.size() - 1) && i > 0) {
                    sb.append(" and ");
                } else if(i > 0 && i != (f.passengers.size() - 1)) {
                    sb.append(", ");
                }

                if(m.firstName != null && !m.firstName.isEmpty()){
                    sb.append(m.firstName);
                } else {
                    sb.append(m.name);
                }

                if(i == (f.passengers.size() - 1)) {
                    sb.append(":");
                }
                i++;
            }

        }
        return sb.toString();

    }

}