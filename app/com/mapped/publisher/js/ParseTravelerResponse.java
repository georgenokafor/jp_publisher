package com.mapped.publisher.js;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wei on 2017-03-02.
 */
public class ParseTravelerResponse
    extends BaseJsonResponse {
  public static class Traveler {
    public String firstName;
    public String lastName;
    public String eTicket;
    public String frequentFlyerNumber;
    public String seat;
    public String seatClass;
    public String meal;
  }

  public String departureTerminal;
  public String arrivalTerminal;
  public List<Traveler> travelerList;

  public String comments;

  public void addTraveler(Traveler t) {
    if (travelerList == null) {
      travelerList = new ArrayList<>();
    }
    travelerList.add(t);
  }
}
