package com.umapped.persistence.reservation.flight;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.umapped.persistence.reservation.UmReservation;
import com.umapped.persistence.reservation.UmTraveler;

public class UmFlightReservationTest {

  @Test
  public void testJson() throws Exception {
    UmFlightReservation fixture = new UmFlightReservation();

    UmFlight flight = new UmFlight();

    List<UmTraveler> travelers = Arrays.asList(
    new UmTraveler[] {
        new UmFlightTraveler().setGivenName("T1").setFamilyName("Test"),
        new UmFlightTraveler().setGivenName("P2").setFamilyName("Test")
    }
    );
    flight.flightNumber = "CA 029";
    fixture.setFlight(flight)
        .setCheckinUrl("http://aircanda.ca/checkin")
        .setTravelers(travelers);
    
    StringWriter sw = new StringWriter();
    ObjectMapper om = new ObjectMapper();
    om.enable(SerializationFeature.INDENT_OUTPUT);
    om.writer().writeValue(sw, fixture);
    
    String json = sw.toString();
    System.out.println(json);
    
    UmFlightReservation reservation =  om.readerFor(UmReservation.class).readValue(json);
    
    Assert.assertEquals(fixture.getFlight().flightNumber, reservation.getFlight().flightNumber);
    Assert.assertEquals(fixture.getTravelers().size(), reservation.getTravelers().size());
  }
}
