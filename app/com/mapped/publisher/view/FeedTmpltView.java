package com.mapped.publisher.view;

import models.publisher.FeedSrc;

import java.util.List;

/**
 * Created by surge on 2015-10-02.
 */
public class FeedTmpltView
    extends BaseView {

  public List<FeedSrc> srcList;
  public FeedSrc currSrc;

  public FeedTmpltView() {
    currSrc = null;
    srcList = null;
  }
}
