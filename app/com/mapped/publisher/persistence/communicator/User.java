package com.mapped.publisher.persistence.communicator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.firebase.client.Firebase;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by surge on 2015-07-29.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class User
    extends FirebaseObject {
  public String id;
  public String name;


  public Firebase update(Firebase ref) {
    Map<String, Object> uVals = new HashMap<>();
    uVals.put("name", name);
    uVals.put("id", id);
    ref.updateChildren(uVals);
    return ref;
  }
}
