package com.mapped.publisher.view;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by george on 2017-03-22.
 */
public class StructuredNoteView extends BaseView
    implements Serializable {

  public List<String> noteKeys;
  public Map<String, Object> noteMapping;
}
