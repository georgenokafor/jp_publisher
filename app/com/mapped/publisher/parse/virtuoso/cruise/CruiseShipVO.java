package com.mapped.publisher.parse.virtuoso.cruise;

import java.util.HashMap;

/**
 * Created by twong on 15-05-19.
 */
public class CruiseShipVO {

  private String company;
  private String name;
  private String deckplanUrl;
  HashMap<String, String> cruisePhotos = new HashMap<>();
  private String note;

  public String getCompany() {
    return company;
  }

  public void setCompany(String company) {
    this.company = company;
  }

  public String getName() {
    if (name != null)
      return name.trim();
    else
      return name;
  }

  public void setName(String name) {

    if (name != null)
      this.name = name.trim();
    else
      this.name = name;
  }

  public String getDeckplanUrl() {
    return deckplanUrl;
  }

  public void setDeckplanUrl(String deckplanUrl) {
    this.deckplanUrl = deckplanUrl;
  }

  public HashMap<String, String> getCruisePhotos() {
    return cruisePhotos;
  }

  public void setCruisePhotos(HashMap<String, String> cruisePhotos) {
    this.cruisePhotos = cruisePhotos;
  }

  public void addCruisePhoto(String name, String url) {
    this.cruisePhotos.put(name, url);
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }
}
