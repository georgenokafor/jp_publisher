package com.umapped.persistence.reservation;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmImageObject
    extends UmMediaObject {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  /**
   * The caption for this object.
   */
  public String caption;
  /**
   * Exif data for this object
   */
  List<UmPropertyValue> exifData;
  /**
   * Thumbnail image for an image or video.
   */
  UmImageObject         thumbnail;
  /**
   * Indicates whether this image is representative of the content of the page.
   * (IGNORED)
   */
  //public Boolean representativeOfPage;
}