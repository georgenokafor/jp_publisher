
package com.umapped.external.bing.image;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "text",
    "displayText",
    "webSearchUrl",
    "searchLink",
    "thumbnail"
})
public class Suggestion {

    @JsonProperty("text")
    private String text;
    @JsonProperty("displayText")
    private String displayText;
    @JsonProperty("webSearchUrl")
    private String webSearchUrl;
    @JsonProperty("searchLink")
    private String searchLink;
    @JsonProperty("thumbnail")
    private Thumbnail thumbnail;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The text
     */
    @JsonProperty("text")
    public String getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The text
     */
    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    /**
     * 
     * @return
     *     The displayText
     */
    @JsonProperty("displayText")
    public String getDisplayText() {
        return displayText;
    }

    /**
     * 
     * @param displayText
     *     The displayText
     */
    @JsonProperty("displayText")
    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    /**
     * 
     * @return
     *     The webSearchUrl
     */
    @JsonProperty("webSearchUrl")
    public String getWebSearchUrl() {
        return webSearchUrl;
    }

    /**
     * 
     * @param webSearchUrl
     *     The webSearchUrl
     */
    @JsonProperty("webSearchUrl")
    public void setWebSearchUrl(String webSearchUrl) {
        this.webSearchUrl = webSearchUrl;
    }

    /**
     * 
     * @return
     *     The searchLink
     */
    @JsonProperty("searchLink")
    public String getSearchLink() {
        return searchLink;
    }

    /**
     * 
     * @param searchLink
     *     The searchLink
     */
    @JsonProperty("searchLink")
    public void setSearchLink(String searchLink) {
        this.searchLink = searchLink;
    }

    /**
     * 
     * @return
     *     The thumbnail
     */
    @JsonProperty("thumbnail")
    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    /**
     * 
     * @param thumbnail
     *     The thumbnail
     */
    @JsonProperty("thumbnail")
    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Suggestion{" +
            "text='" + text + '\'' +
            ", displayText='" + displayText + '\'' +
            ", webSearchUrl='" + webSearchUrl + '\'' +
            ", searchLink='" + searchLink + '\'' +
            ", thumbnail=" + thumbnail +
            ", additionalProperties=" + additionalProperties +
            '}';
    }
}
