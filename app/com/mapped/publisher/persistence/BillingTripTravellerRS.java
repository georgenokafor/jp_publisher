package com.mapped.publisher.persistence;

import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-03-26
 * Time: 11:05 AM
 * To change this template use File | Settings | File Templates.
 */
public class BillingTripTravellerRS {
    private Long passengerPk;
    private String tripId;
    private String tripName;
    private String groupName;

    private String groupId;
    private String email;
    private String userId;

    private Timestamp createdTimestamp;

    public Long getPassengerPk() {
        return passengerPk;
    }

    public void setPassengerPk(Long passengerPk) {
        this.passengerPk = passengerPk;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }




    public Timestamp getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(Timestamp createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
