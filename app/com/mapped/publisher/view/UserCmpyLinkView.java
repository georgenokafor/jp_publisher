package com.mapped.publisher.view;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-18
 * Time: 6:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserCmpyLinkView {
    public String UserCmpyLinkId;
    public String userId;
    public String cmpyId;
    public String cmpyName;
    public int linkTypeId;
    public String linkType;
    public int status;
    public String createTimestamp;
    public String createTimestampPrint;

    public List<CmpyGroupView> cmpyGroups;

}
