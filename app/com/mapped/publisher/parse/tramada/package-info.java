/**
 * Created by george on 2017-11-22.
 */
@XmlJavaTypeAdapter(value=StringTrimAdapter.class,type=String.class)
package com.mapped.publisher.parse.tramada;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
