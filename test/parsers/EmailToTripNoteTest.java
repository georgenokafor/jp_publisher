package parsers;

import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.utils.S3Util;
import controllers.EmailController;
import models.publisher.EmailParseLog;
import models.publisher.FileInfo;
import models.publisher.Trip;
import models.publisher.TripNote;
import org.apache.commons.io.IOUtils;
import org.apache.commons.mail.util.MimeMessageParser;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.time.Instant;
import java.util.Properties;

import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.running;

import com.mapped.common.EmailMgr;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;

import javax.activation.DataSource;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import org.jsoup.Jsoup;

/**
 * Created by ryan on 08/07/16.
 */
public class EmailToTripNoteTest extends Assert{

    @Test
    public void email_sample () {
        running(fakeApplication(), new Runnable() {
            public void run() {
                EmailController.attachTripNote("425562280040000064", Long.parseLong("904587628050065467"));
                /*TripNote tn = new TripNote();
                Long fid = Long.parseLong("904587628050065468");
                Long eplid = Long.parseLong("904587628050065467");
                FileInfo f = FileInfo.find.byId(fid);
                System.out.println(f.getPk());
                EmailParseLog epl = EmailParseLog.find.byId(eplid);


                try {
                    S3Object so = S3Util.getS3File(f.getBucket(), f.getFilepath());
                    S3ObjectInputStream is = so.getObjectContent();
                    StringWriter writer = new StringWriter();
                    IOUtils.copy(is, writer, "US-ASCII");
                    String emailRaw = writer.toString();
                    //System.out.println("email: " + emailRaw);

                    Session session = Session.getInstance(new Properties());
                    MimeMessage mm = new MimeMessage(session, new ByteArrayInputStream(emailRaw.getBytes()));
                    MimeMessageParser mimeParser = new MimeMessageParser(mm).parse();
                    //System.out.println(mimeParser.getPlainContent());
                    //System.out.println(mimeParser.getHtmlContent());

                    PolicyFactory policy = new HtmlPolicyBuilder().allowCommonBlockElements().allowCommonInlineFormattingElements().allowStandardUrlProtocols().allowStyling().toFactory();
                    String sanitized = policy.sanitize(mimeParser.getHtmlContent());
                    //System.out.println(policy.sanitize(mimeParser.getHtmlContent()));
                    //System.out.println(policy.sanitize(mimeParser.getHtmlContent()).contains("image"));
                    sanitized = sanitized.replace("<p>","");
                    sanitized = sanitized.replace("<div>","");
                    sanitized = sanitized.replace("</p>","<br>");
                    sanitized = sanitized.replace("</div>","<br>");
                    Document doc =  Jsoup.parse(sanitized);
                    doc.select("img").remove();
                    System.out.println(doc.toString());

                    Long tid = Long.parseLong("425562280040000064");
                    Trip trip = Trip.find.byId("425562280040000064");
                    Long time = System.currentTimeMillis();

                    tn.setTrip(trip);
                    tn.setNoteId(DBConnectionMgr.getUniqueLongId());
                    tn.setCreatedBy(epl.getUserid());
                    tn.setModifiedBy(epl.getUserid());
                    tn.setCreatedTimestamp(time);
                    tn.setLastUpdatedTimestamp(time);
                    tn.setDescription(doc.toString());
                    tn.setName(mimeParser.getSubject());
                    System.out.println("DETAILS");
                    System.out.println(tn.getNoteId());
                    System.out.println(tn.getCreatedTimestamp());
                    System.out.println(epl.getUserid());
                    System.out.println(tn.getTrip().getTripid());
                    System.out.println(tn.getName());
                    tn.insert();
                    System.out.println("saved");
                }
                catch(Exception e) {
                    System.out.println(e.getMessage());
                }*/
            }

        });
    }

}
