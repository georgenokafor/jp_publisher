package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class MilestonesLikeMgr {

	 private static final String SELECT_ALL = "Select Pk, MilestonesId, UsersId, Notes, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from MilestonesLike;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, MilestonesId, UsersId, Notes, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from MilestonesLike where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from MilestonesLike where Pk = ?;";
	 private static final String INSERT = "insert into MilestonesLike (Pk, MilestonesId, UsersId, Notes, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update MilestonesLike SET MilestonesId=?, UsersId=?, Notes=?, ModifiedBy=?, Status=?, SyncTimestamp=?, LastUpdateTimestamp=? WHERE Pk = ?; ";
	 private static final String SELECT_ALL_BY_MILESTONES_ID = "Select Pk, MilestonesId, UsersId, Notes, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from MilestonesLike where MilestonesId=? ";
	 private static final String DELETE_BY_MILESTONES_ID = "delete from MilestonesLike where MilestonesId=? ";
	 public static ArrayList <MilestonesLike> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return MilestonesLikeMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static MilestonesLike getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<MilestonesLike> results = MilestonesLikeMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (MilestonesLike c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getMilestonesId());
				 prep.setString(3, c.getUsersId());
				 prep.setString(4, c.getNotes());
				 prep.setString(5, c.getModifiedBy());
				 prep.setString(6, c.getCreatedBy());
				 prep.setLong(7, c.getStatus());
				 prep.setLong(8, c.getSyncTimestamp());
				 prep.setLong(9, c.getLastUpdateTimestamp());
				 prep.setLong(10, c.getCreateTimestamp());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (MilestonesLike c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getMilestonesId());
				 prep.setString(2, c.getUsersId());
				 prep.setString(3, c.getNotes());
				 prep.setString(4, c.getModifiedBy());
				 prep.setLong(5, c.getStatus());
				 prep.setLong(6, c.getSyncTimestamp());
				 prep.setLong(7, c.getLastUpdateTimestamp());
				 prep.setString(8, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <MilestonesLike> getAllByMilestonesId(String id) throws SQLException {
		 ResultSet rs = null;
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_MILESTONES_ID);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 return MilestonesLikeMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void deleteByMilestonesId (String id) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_MILESTONES_ID);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <MilestonesLike> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <MilestonesLike> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 MilestonesLike c = new MilestonesLike();
			 c.setPk(rs.getString("Pk"));
			 c.setMilestonesId(rs.getString("MilestonesId"));
			 c.setUsersId(rs.getString("UsersId"));
			 c.setNotes(rs.getString("Notes"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setStatus(rs.getLong("Status"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 results.add(c);
		 }
		 return results;
	 }
}
