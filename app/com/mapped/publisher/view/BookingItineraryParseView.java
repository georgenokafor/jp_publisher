package com.mapped.publisher.view;

import com.mapped.publisher.persistence.VOModeller;
import com.mapped.publisher.utils.Utils;

/**
 * Created by surge on 2014-06-20.
 */
public class BookingItineraryParseView {
  public VOModeller parseData;
  public TabType activeTab = TabType.FLIGHTS;

  public String getHumanTime(long timestamp) {
    return Utils.formatTimestampPrint(timestamp);
  }
}
