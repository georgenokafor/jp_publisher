package com.mapped.persistence;
 
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;

/*
import com.mapped.persistence.Feature;
import com.mapped.persistence.FeatureMgr;
*/
 public class Org { 
 
	 public final static String OrgTypeId = "1000"; 
	 protected static String pkTypeId =  "1001"; 
	 private String pk; 
	 protected static String nameTypeId =  "1002"; 
	 private String name; 
	 protected static String typeTypeId =  "1003"; 
	 private String type; 
	 protected static String noteTypeId =  "1004"; 
	 private String note; 
	 protected static String uriTypeId =  "1005"; 
	 private String uri; 
	 protected static String modifiedByTypeId =  "1090"; 
	 private String modifiedBy; 
	 protected static String createdByTypeId =  "1091"; 
	 private String createdBy; 
	 protected static String statusTypeId =  "1092"; 
	 private long status; 
	 protected static String syncTimestampTypeId =  "1093"; 
	 private long syncTimestamp; 
	 protected static String lastUpdateTimestampTypeId =  "1094"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "1095"; 
	 private long createTimestamp; 

 	 // Contructor 
	 public Org () {} 
	 public Org ( String pk,  String name,  String type,  String note,  String uri,  String modifiedBy,  String createdBy,  long status,  long syncTimestamp,  long lastUpdateTimestamp,  long createTimestamp ) { 
		 this.pk=pk; 
		 this.name=name; 
		 this.type=type; 
		 this.note=note; 
		 this.uri=uri; 
		 this.modifiedBy=modifiedBy; 
		 this.createdBy=createdBy; 
		 this.status=status; 
		 this.syncTimestamp=syncTimestamp; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getName () { 
	 	 return name; 
 	 } 
 
 	 public void setName(String name) { 
	 	 this.name = name; 
 	 } 
 
 	 public  String getType () { 
	 	 return type; 
 	 } 
 
 	 public void setType(String type) { 
	 	 this.type = type; 
 	 } 
 
 	 public  String getNote () { 
	 	 return note; 
 	 } 
 
 	 public void setNote(String note) { 
	 	 this.note = note; 
 	 } 
 
 	 public  String getUri () { 
	 	 return uri; 
 	 } 
 
 	 public void setUri(String uri) { 
	 	 this.uri = uri; 
 	 } 
 
 	 public  String getModifiedBy () { 
	 	 return modifiedBy; 
 	 } 
 
 	 public void setModifiedBy(String modifiedBy) { 
	 	 this.modifiedBy = modifiedBy; 
 	 } 
 
 	 public  String getCreatedBy () { 
	 	 return createdBy; 
 	 } 
 
 	 public void setCreatedBy(String createdBy) { 
	 	 this.createdBy = createdBy; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
     /*
 	 public ArrayList <Feature>  getFeature () throws Exception{ 
		 return FeatureMgr.getAllByOrgId(pk); 
	}
	*/
	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: Org"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 Name: ").append(this.name );  
		 sb.append("	 Type: ").append(this.type );  
		 sb.append("	 Note: ").append(this.note );  
		 sb.append("	 Uri: ").append(this.uri );  
		 sb.append("	 ModifiedBy: ").append(this.modifiedBy );  
		 sb.append("	 CreatedBy: ").append(this.createdBy );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", OrgTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (name != null) {
			 memberNode.put(nameTypeId, name);
		 }
		 if (type != null) {
			 memberNode.put(typeTypeId, type);
		 }
		 if (note != null) {
			 memberNode.put(noteTypeId, note);
		 }
		 if (uri != null) {
			 memberNode.put(uriTypeId, uri);
		 }
		 if (modifiedBy != null) {
			 memberNode.put(modifiedByTypeId, modifiedBy);
		 }
		 if (createdBy != null) {
			 memberNode.put(createdByTypeId, createdBy);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 return classNode;
	 } 
	 public static Org parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 Org  c= new Org(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.name = fields.findPath(nameTypeId).textValue();
			 c.type = fields.findPath(typeTypeId).textValue();
			 c.note = fields.findPath(noteTypeId).textValue();
			 c.uri = fields.findPath(uriTypeId).textValue();
			 c.modifiedBy = fields.findPath(modifiedByTypeId).textValue();
			 c.createdBy = fields.findPath(createdByTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 return c;
		} else
			 return null;
	 } 
} 
 
