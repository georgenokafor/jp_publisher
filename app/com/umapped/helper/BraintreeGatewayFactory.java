package com.umapped.helper;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Environment;
import com.mapped.common.CoreConstants;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.ConfigMgr;
import models.publisher.BillingPlan;

import javax.inject.Singleton;
import java.util.HashMap;

/**
 * Created by twong on 2016-10-19.
 */
@Singleton
public class BraintreeGatewayFactory {
  private String merchantId;
  private String publicKey;
  private String privateKey;
  private Environment environment;
  private HashMap<BillingPlan.Currency, String> curMerchantIds = new HashMap<>();


  public BraintreeGatewayFactory() {
    merchantId = ConfigMgr.getAppParameter(CoreConstants.BRAINTREE_MERCHANT);
    publicKey = ConfigMgr.getAppParameter(CoreConstants.BRAINTREE_PUBLIC);
    privateKey = ConfigMgr.getAppParameter(CoreConstants.BRAINTREE_PRIVATE);

    if (ConfigMgr.getAppParameter(CoreConstants.BRAINTREE_USD) != null) {
      curMerchantIds.put(BillingPlan.Currency.USD, ConfigMgr.getAppParameter(CoreConstants.BRAINTREE_USD));
    }

    if (ConfigMgr.getAppParameter(CoreConstants.BRAINTREE_CAD) != null) {
      curMerchantIds.put(BillingPlan.Currency.CAD, ConfigMgr.getAppParameter(CoreConstants.BRAINTREE_CAD));
    }

    if (ConfigMgr.getAppParameter(CoreConstants.BRAINTREE_AUD) != null) {
      curMerchantIds.put(BillingPlan.Currency.AUD, ConfigMgr.getAppParameter(CoreConstants.BRAINTREE_AUD));
    }

    if (ConfigMgr.getAppParameter(CoreConstants.BRAINTREE_EUR) != null) {
      curMerchantIds.put(BillingPlan.Currency.EUR, ConfigMgr.getAppParameter(CoreConstants.BRAINTREE_EUR));
    }
    if (ConfigMgr.isProduction()) {
      environment = Environment.PRODUCTION;
    } else {
      environment = Environment.SANDBOX;
    }
  }

  public BraintreeGateway getInstance() {
    return new BraintreeGateway(environment, merchantId, publicKey, privateKey);
  }

  public String getMerchantIdForCurrency (BillingPlan.Currency currency) {
    if (curMerchantIds.containsKey(currency)) {
      return curMerchantIds.get(currency);
    } else
      return merchantId;
  }
}
