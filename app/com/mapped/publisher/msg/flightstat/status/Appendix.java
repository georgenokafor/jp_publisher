
package com.mapped.publisher.msg.flightstat.status;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties({"equipments"})

public class Appendix{
   	private List<Airline> airlines;
   	private List<Airport> airports;
    private List<Equipments> equpments;

  public List<Airline> getAirlines() {
    return airlines;
  }

  public void setAirlines(List<Airline> airlines) {
    this.airlines = airlines;
  }

  public List<Airport> getAirports() {
    return airports;
  }

  public void setAirports(List<Airport> airports) {
    this.airports = airports;
  }

  public List<Equipments> getEqupments() {
    return equpments;
  }

  public void setEqupments(List<Equipments> equpments) {
    this.equpments = equpments;
  }
}
