
package com.mapped.publisher.parse.globus;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mapped.publisher.parse.globus package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MinInclusive_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "minInclusive");
    private final static QName _MinLength_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "minLength");
    private final static QName _Enumeration_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "enumeration");
    private final static QName _GetTourMediaXMLResult_QNAME = new QName("http://webapi.globusandcosmos.com/TourInformation.xsd", "GetTourMediaXMLResult");
    private final static QName _GetDeparturesXMLResult_QNAME = new QName("http://webapi.globusandcosmos.com/DepartureList.xsd", "GetDeparturesXMLResult");
    private final static QName _Key_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "key");
    private final static QName _MaxInclusive_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "maxInclusive");
    private final static QName _GetTourMediaByBrandXMLResult_QNAME = new QName("http://webapi.globusandcosmos.com/TourInformation.xsd", "GetTourMediaByBrandXMLResult");
    private final static QName _MinExclusive_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "minExclusive");
    private final static QName _AnyAttribute_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "anyAttribute");
    private final static QName _MaxExclusive_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "maxExclusive");
    private final static QName _Length_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "length");
    private final static QName _GetGSADeparturesXMLResult_QNAME = new QName("http://webapi.globusandcosmos.com/DepartureList.xsd", "GetGSADeparturesXMLResult");
    private final static QName _FractionDigits_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "fractionDigits");
    private final static QName _All_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "all");
    private final static QName _GetAllAvailableMediaToursXMLResult_QNAME = new QName("http://webapi.globusandcosmos.com/AvailableTours.xsd", "GetAllAvailableMediaToursXMLResult");
    private final static QName _GetTourDayMediaByBrandXMLResult_QNAME = new QName("http://webapi.globusandcosmos.com/TourInformation.xsd", "GetTourDayMediaByBrandXMLResult");
    private final static QName _GetAllAvailableToursXMLResult_QNAME = new QName("http://webapi.globusandcosmos.com/AvailableTours.xsd", "GetAllAvailableToursXMLResult");
    private final static QName _GetAvailableToursXMLResult_QNAME = new QName("http://webapi.globusandcosmos.com/AvailableTours.xsd", "GetAvailableToursXMLResult");
    private final static QName _GetAvailableMediaToursXMLResult_QNAME = new QName("http://webapi.globusandcosmos.com/AvailableTours.xsd", "GetAvailableMediaToursXMLResult");
    private final static QName _GetDeparturesBySeasonXMLResult_QNAME = new QName("http://webapi.globusandcosmos.com/DepartureList.xsd", "GetDeparturesBySeasonXMLResult");
    private final static QName _GetAvailableGSAToursXMLResult_QNAME = new QName("http://webapi.globusandcosmos.com/AvailableTours.xsd", "GetAvailableGSAToursXMLResult");
    private final static QName _GetTourDayMediaXMLResult_QNAME = new QName("http://webapi.globusandcosmos.com/TourInformation.xsd", "GetTourDayMediaXMLResult");
    private final static QName _Choice_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "choice");
    private final static QName _MaxLength_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "maxLength");
    private final static QName _Sequence_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "sequence");
    private final static QName _GetDeparturesExtendedXMLResult_QNAME = new QName("http://webapi.globusandcosmos.com/ExtendedDepartureList.xsd", "GetDeparturesExtendedXMLResult");
    private final static QName _Unique_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "unique");
    private final static QName _GroupTypeElement_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "element");
    private final static QName _GroupTypeGroup_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "group");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mapped.publisher.parse.globus
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetCurrentPromotionsResponse }
     * 
     */
    public GetCurrentPromotionsResponse createGetCurrentPromotionsResponse() {
        return new GetCurrentPromotionsResponse();
    }

    /**
     * Create an instance of {@link GetDeparturePricingResponse }
     * 
     */
    public GetDeparturePricingResponse createGetDeparturePricingResponse() {
        return new GetDeparturePricingResponse();
    }

    /**
     * Create an instance of {@link GetDeparturesBySeasonResponse }
     * 
     */
    public GetDeparturesBySeasonResponse createGetDeparturesBySeasonResponse() {
        return new GetDeparturesBySeasonResponse();
    }

    /**
     * Create an instance of {@link GetTourDayMediaByBrandResponse }
     * 
     */
    public GetTourDayMediaByBrandResponse createGetTourDayMediaByBrandResponse() {
        return new GetTourDayMediaByBrandResponse();
    }

    /**
     * Create an instance of {@link GetTourMediaResponse }
     * 
     */
    public GetTourMediaResponse createGetTourMediaResponse() {
        return new GetTourMediaResponse();
    }

    /**
     * Create an instance of {@link GetAvailableToursResponse }
     * 
     */
    public GetAvailableToursResponse createGetAvailableToursResponse() {
        return new GetAvailableToursResponse();
    }

    /**
     * Create an instance of {@link GetDeparturesResponse }
     * 
     */
    public GetDeparturesResponse createGetDeparturesResponse() {
        return new GetDeparturesResponse();
    }

    /**
     * Create an instance of {@link GetTourDayMediaResponse }
     * 
     */
    public GetTourDayMediaResponse createGetTourDayMediaResponse() {
        return new GetTourDayMediaResponse();
    }

    /**
     * Create an instance of {@link GetVacationsByKeywordResponse }
     * 
     */
    public GetVacationsByKeywordResponse createGetVacationsByKeywordResponse() {
        return new GetVacationsByKeywordResponse();
    }

    /**
     * Create an instance of {@link GetAllAvailableToursResponse }
     * 
     */
    public GetAllAvailableToursResponse createGetAllAvailableToursResponse() {
        return new GetAllAvailableToursResponse();
    }

    /**
     * Create an instance of {@link GetTourMediaByBrandResponse }
     * 
     */
    public GetTourMediaByBrandResponse createGetTourMediaByBrandResponse() {
        return new GetTourMediaByBrandResponse();
    }

    /**
     * Create an instance of {@link GetAvailableMediaToursResponse }
     * 
     */
    public GetAvailableMediaToursResponse createGetAvailableMediaToursResponse() {
        return new GetAvailableMediaToursResponse();
    }

    /**
     * Create an instance of {@link GetGroupDeparturesResponse }
     * 
     */
    public GetGroupDeparturesResponse createGetGroupDeparturesResponse() {
        return new GetGroupDeparturesResponse();
    }

    /**
     * Create an instance of {@link GetAllAvailableMediaToursResponse }
     * 
     */
    public GetAllAvailableMediaToursResponse createGetAllAvailableMediaToursResponse() {
        return new GetAllAvailableMediaToursResponse();
    }

    /**
     * Create an instance of {@link GetAvailableGSAToursResponse }
     * 
     */
    public GetAvailableGSAToursResponse createGetAvailableGSAToursResponse() {
        return new GetAvailableGSAToursResponse();
    }

    /**
     * Create an instance of {@link Schema }
     * 
     */
    public Schema createSchema() {
        return new Schema();
    }

    /**
     * Create an instance of {@link OpenAttrs }
     * 
     */
    public OpenAttrs createOpenAttrs() {
        return new OpenAttrs();
    }

    /**
     * Create an instance of {@link Include }
     * 
     */
    public Include createInclude() {
        return new Include();
    }

    /**
     * Create an instance of {@link Annotated }
     * 
     */
    public Annotated createAnnotated() {
        return new Annotated();
    }

    /**
     * Create an instance of {@link Annotation }
     * 
     */
    public Annotation createAnnotation() {
        return new Annotation();
    }

    /**
     * Create an instance of {@link Appinfo }
     * 
     */
    public Appinfo createAppinfo() {
        return new Appinfo();
    }

    /**
     * Create an instance of {@link Documentation }
     * 
     */
    public Documentation createDocumentation() {
        return new Documentation();
    }

    /**
     * Create an instance of {@link Import }
     * 
     */
    public Import createImport() {
        return new Import();
    }

    /**
     * Create an instance of {@link Redefine }
     * 
     */
    public Redefine createRedefine() {
        return new Redefine();
    }

    /**
     * Create an instance of {@link SimpleType }
     * 
     */
    public SimpleType createSimpleType() {
        return new SimpleType();
    }

    /**
     * Create an instance of {@link Union }
     * 
     */
    public Union createUnion() {
        return new Union();
    }

    /**
     * Create an instance of {@link LocalSimpleType }
     * 
     */
    public LocalSimpleType createLocalSimpleType() {
        return new LocalSimpleType();
    }

    /**
     * Create an instance of {@link List }
     * 
     */
    public List createList() {
        return new List();
    }

    /**
     * Create an instance of {@link Restriction }
     * 
     */
    public Restriction createRestriction() {
        return new Restriction();
    }

    /**
     * Create an instance of {@link Facet }
     * 
     */
    public Facet createFacet() {
        return new Facet();
    }

    /**
     * Create an instance of {@link TotalDigits }
     * 
     */
    public TotalDigits createTotalDigits() {
        return new TotalDigits();
    }

    /**
     * Create an instance of {@link NumFacet }
     * 
     */
    public NumFacet createNumFacet() {
        return new NumFacet();
    }

    /**
     * Create an instance of {@link NoFixedFacet }
     * 
     */
    public NoFixedFacet createNoFixedFacet() {
        return new NoFixedFacet();
    }

    /**
     * Create an instance of {@link WhiteSpace }
     * 
     */
    public WhiteSpace createWhiteSpace() {
        return new WhiteSpace();
    }

    /**
     * Create an instance of {@link Pattern }
     * 
     */
    public Pattern createPattern() {
        return new Pattern();
    }

    /**
     * Create an instance of {@link ComplexType }
     * 
     */
    public ComplexType createComplexType() {
        return new ComplexType();
    }

    /**
     * Create an instance of {@link ExplicitGroup }
     * 
     */
    public ExplicitGroup createExplicitGroup() {
        return new ExplicitGroup();
    }

    /**
     * Create an instance of {@link All }
     * 
     */
    public All createAll() {
        return new All();
    }

    /**
     * Create an instance of {@link GroupRef }
     * 
     */
    public GroupRef createGroupRef() {
        return new GroupRef();
    }

    /**
     * Create an instance of {@link AttributeType }
     * 
     */
    public AttributeType createAttributeType() {
        return new AttributeType();
    }

    /**
     * Create an instance of {@link AttributeGroupRef }
     * 
     */
    public AttributeGroupRef createAttributeGroupRef() {
        return new AttributeGroupRef();
    }

    /**
     * Create an instance of {@link Wildcard }
     * 
     */
    public Wildcard createWildcard() {
        return new Wildcard();
    }

    /**
     * Create an instance of {@link ComplexContent }
     * 
     */
    public ComplexContent createComplexContent() {
        return new ComplexContent();
    }

    /**
     * Create an instance of {@link ExtensionType }
     * 
     */
    public ExtensionType createExtensionType() {
        return new ExtensionType();
    }

    /**
     * Create an instance of {@link ComplexRestrictionType }
     * 
     */
    public ComplexRestrictionType createComplexRestrictionType() {
        return new ComplexRestrictionType();
    }

    /**
     * Create an instance of {@link SimpleContent }
     * 
     */
    public SimpleContent createSimpleContent() {
        return new SimpleContent();
    }

    /**
     * Create an instance of {@link SimpleExtensionType }
     * 
     */
    public SimpleExtensionType createSimpleExtensionType() {
        return new SimpleExtensionType();
    }

    /**
     * Create an instance of {@link SimpleRestrictionType }
     * 
     */
    public SimpleRestrictionType createSimpleRestrictionType() {
        return new SimpleRestrictionType();
    }

    /**
     * Create an instance of {@link Group }
     * 
     */
    public Group createGroup() {
        return new Group();
    }

    /**
     * Create an instance of {@link RealGroup }
     * 
     */
    public RealGroup createRealGroup() {
        return new RealGroup();
    }

    /**
     * Create an instance of {@link LocalElement }
     * 
     */
    public LocalElement createLocalElement() {
        return new LocalElement();
    }

    /**
     * Create an instance of {@link Any }
     * 
     */
    public Any createAny() {
        return new Any();
    }

    /**
     * Create an instance of {@link AttributeGroup }
     * 
     */
    public AttributeGroup createAttributeGroup() {
        return new AttributeGroup();
    }

    /**
     * Create an instance of {@link Element }
     * 
     */
    public Element createElement() {
        return new Element();
    }

    /**
     * Create an instance of {@link LocalComplexType }
     * 
     */
    public LocalComplexType createLocalComplexType() {
        return new LocalComplexType();
    }

    /**
     * Create an instance of {@link Keybase }
     * 
     */
    public Keybase createKeybase() {
        return new Keybase();
    }

    /**
     * Create an instance of {@link Keyref }
     * 
     */
    public Keyref createKeyref() {
        return new Keyref();
    }

    /**
     * Create an instance of {@link Selector }
     * 
     */
    public Selector createSelector() {
        return new Selector();
    }

    /**
     * Create an instance of {@link Field }
     * 
     */
    public Field createField() {
        return new Field();
    }

    /**
     * Create an instance of {@link Attribute }
     * 
     */
    public Attribute createAttribute() {
        return new Attribute();
    }

    /**
     * Create an instance of {@link Notation }
     * 
     */
    public Notation createNotation() {
        return new Notation();
    }

    /**
     * Create an instance of {@link RestrictionType }
     * 
     */
    public RestrictionType createRestrictionType() {
        return new RestrictionType();
    }

    /**
     * Create an instance of {@link SimpleExplicitGroup }
     * 
     */
    public SimpleExplicitGroup createSimpleExplicitGroup() {
        return new SimpleExplicitGroup();
    }

    /**
     * Create an instance of {@link NarrowMaxMin }
     * 
     */
    public NarrowMaxMin createNarrowMaxMin() {
        return new NarrowMaxMin();
    }

    /**
     * Create an instance of {@link GetTourMediaXML }
     * 
     */
    public GetTourMediaXML createGetTourMediaXML() {
        return new GetTourMediaXML();
    }

    /**
     * Create an instance of {@link GetAllAvailableMediaTours }
     * 
     */
    public GetAllAvailableMediaTours createGetAllAvailableMediaTours() {
        return new GetAllAvailableMediaTours();
    }

    /**
     * Create an instance of {@link GetCurrentPromotionsResponse.GetCurrentPromotionsResult }
     * 
     */
    public GetCurrentPromotionsResponse.GetCurrentPromotionsResult createGetCurrentPromotionsResponseGetCurrentPromotionsResult() {
        return new GetCurrentPromotionsResponse.GetCurrentPromotionsResult();
    }

    /**
     * Create an instance of {@link GetDeparturePricingResponse.GetDeparturePricingResult }
     * 
     */
    public GetDeparturePricingResponse.GetDeparturePricingResult createGetDeparturePricingResponseGetDeparturePricingResult() {
        return new GetDeparturePricingResponse.GetDeparturePricingResult();
    }

    /**
     * Create an instance of {@link GetDeparturesBySeasonResponse.GetDeparturesBySeasonResult }
     * 
     */
    public GetDeparturesBySeasonResponse.GetDeparturesBySeasonResult createGetDeparturesBySeasonResponseGetDeparturesBySeasonResult() {
        return new GetDeparturesBySeasonResponse.GetDeparturesBySeasonResult();
    }

    /**
     * Create an instance of {@link GetTourDayMediaByBrandResponse.GetTourDayMediaByBrandResult }
     * 
     */
    public GetTourDayMediaByBrandResponse.GetTourDayMediaByBrandResult createGetTourDayMediaByBrandResponseGetTourDayMediaByBrandResult() {
        return new GetTourDayMediaByBrandResponse.GetTourDayMediaByBrandResult();
    }

    /**
     * Create an instance of {@link GetTourMediaResponse.GetTourMediaResult }
     * 
     */
    public GetTourMediaResponse.GetTourMediaResult createGetTourMediaResponseGetTourMediaResult() {
        return new GetTourMediaResponse.GetTourMediaResult();
    }

    /**
     * Create an instance of {@link GetGSADeparturesXML }
     * 
     */
    public GetGSADeparturesXML createGetGSADeparturesXML() {
        return new GetGSADeparturesXML();
    }

    /**
     * Create an instance of {@link GetBasicHotelMediaByTour }
     * 
     */
    public GetBasicHotelMediaByTour createGetBasicHotelMediaByTour() {
        return new GetBasicHotelMediaByTour();
    }

    /**
     * Create an instance of {@link GetBasicHotelMedia }
     * 
     */
    public GetBasicHotelMedia createGetBasicHotelMedia() {
        return new GetBasicHotelMedia();
    }

    /**
     * Create an instance of {@link GetBasicHotelMediaByLocationResponse }
     * 
     */
    public GetBasicHotelMediaByLocationResponse createGetBasicHotelMediaByLocationResponse() {
        return new GetBasicHotelMediaByLocationResponse();
    }

    /**
     * Create an instance of {@link ArrayOfBasicHotelMedia }
     * 
     */
    public ArrayOfBasicHotelMedia createArrayOfBasicHotelMedia() {
        return new ArrayOfBasicHotelMedia();
    }

    /**
     * Create an instance of {@link GetAvailableToursResponse.GetAvailableToursResult }
     * 
     */
    public GetAvailableToursResponse.GetAvailableToursResult createGetAvailableToursResponseGetAvailableToursResult() {
        return new GetAvailableToursResponse.GetAvailableToursResult();
    }

    /**
     * Create an instance of {@link GetTourDayMediaXML }
     * 
     */
    public GetTourDayMediaXML createGetTourDayMediaXML() {
        return new GetTourDayMediaXML();
    }

    /**
     * Create an instance of {@link GetTourDayMediaByBrandXMLResponse }
     * 
     */
    public GetTourDayMediaByBrandXMLResponse createGetTourDayMediaByBrandXMLResponse() {
        return new GetTourDayMediaByBrandXMLResponse();
    }

    /**
     * Create an instance of {@link WebTourMedia }
     * 
     */
    public WebTourMedia createWebTourMedia() {
        return new WebTourMedia();
    }

    /**
     * Create an instance of {@link GetAvailableMediaTours }
     * 
     */
    public GetAvailableMediaTours createGetAvailableMediaTours() {
        return new GetAvailableMediaTours();
    }

    /**
     * Create an instance of {@link GetDeparturesResponse.GetDeparturesResult }
     * 
     */
    public GetDeparturesResponse.GetDeparturesResult createGetDeparturesResponseGetDeparturesResult() {
        return new GetDeparturesResponse.GetDeparturesResult();
    }

    /**
     * Create an instance of {@link GetAvailableGSAToursXML }
     * 
     */
    public GetAvailableGSAToursXML createGetAvailableGSAToursXML() {
        return new GetAvailableGSAToursXML();
    }

    /**
     * Create an instance of {@link GetHotelMediaResponse }
     * 
     */
    public GetHotelMediaResponse createGetHotelMediaResponse() {
        return new GetHotelMediaResponse();
    }

    /**
     * Create an instance of {@link HotelMedia }
     * 
     */
    public HotelMedia createHotelMedia() {
        return new HotelMedia();
    }

    /**
     * Create an instance of {@link GetHotelMedia }
     * 
     */
    public GetHotelMedia createGetHotelMedia() {
        return new GetHotelMedia();
    }

    /**
     * Create an instance of {@link GetAllAvailableTours }
     * 
     */
    public GetAllAvailableTours createGetAllAvailableTours() {
        return new GetAllAvailableTours();
    }

    /**
     * Create an instance of {@link GetTourMediaXMLResponse }
     * 
     */
    public GetTourMediaXMLResponse createGetTourMediaXMLResponse() {
        return new GetTourMediaXMLResponse();
    }

    /**
     * Create an instance of {@link GetShipMedia }
     * 
     */
    public GetShipMedia createGetShipMedia() {
        return new GetShipMedia();
    }

    /**
     * Create an instance of {@link GetDeparturesBySeasonXMLResponse }
     * 
     */
    public GetDeparturesBySeasonXMLResponse createGetDeparturesBySeasonXMLResponse() {
        return new GetDeparturesBySeasonXMLResponse();
    }

    /**
     * Create an instance of {@link DepartureList }
     * 
     */
    public DepartureList createDepartureList() {
        return new DepartureList();
    }

    /**
     * Create an instance of {@link GetAvailableMediaToursXMLResponse }
     * 
     */
    public GetAvailableMediaToursXMLResponse createGetAvailableMediaToursXMLResponse() {
        return new GetAvailableMediaToursXMLResponse();
    }

    /**
     * Create an instance of {@link AvailableMediaTours }
     * 
     */
    public AvailableMediaTours createAvailableMediaTours() {
        return new AvailableMediaTours();
    }

    /**
     * Create an instance of {@link GetTourDayMediaResponse.GetTourDayMediaResult }
     * 
     */
    public GetTourDayMediaResponse.GetTourDayMediaResult createGetTourDayMediaResponseGetTourDayMediaResult() {
        return new GetTourDayMediaResponse.GetTourDayMediaResult();
    }

    /**
     * Create an instance of {@link GetTravelStyleKeywords }
     * 
     */
    public GetTravelStyleKeywords createGetTravelStyleKeywords() {
        return new GetTravelStyleKeywords();
    }

    /**
     * Create an instance of {@link GetDeparturesXMLResponse }
     * 
     */
    public GetDeparturesXMLResponse createGetDeparturesXMLResponse() {
        return new GetDeparturesXMLResponse();
    }

    /**
     * Create an instance of {@link GetDeparturesBySeasonXML }
     * 
     */
    public GetDeparturesBySeasonXML createGetDeparturesBySeasonXML() {
        return new GetDeparturesBySeasonXML();
    }

    /**
     * Create an instance of {@link GetTravelStyleKeywordsResponse }
     * 
     */
    public GetTravelStyleKeywordsResponse createGetTravelStyleKeywordsResponse() {
        return new GetTravelStyleKeywordsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link GetDepartures }
     * 
     */
    public GetDepartures createGetDepartures() {
        return new GetDepartures();
    }

    /**
     * Create an instance of {@link GetHotelMediaByTourCodeResponse }
     * 
     */
    public GetHotelMediaByTourCodeResponse createGetHotelMediaByTourCodeResponse() {
        return new GetHotelMediaByTourCodeResponse();
    }

    /**
     * Create an instance of {@link ArrayOfHotelMedia }
     * 
     */
    public ArrayOfHotelMedia createArrayOfHotelMedia() {
        return new ArrayOfHotelMedia();
    }

    /**
     * Create an instance of {@link GetDocumentTourItineraryXMLResponse }
     * 
     */
    public GetDocumentTourItineraryXMLResponse createGetDocumentTourItineraryXMLResponse() {
        return new GetDocumentTourItineraryXMLResponse();
    }

    /**
     * Create an instance of {@link GetAllHotelCodes }
     * 
     */
    public GetAllHotelCodes createGetAllHotelCodes() {
        return new GetAllHotelCodes();
    }

    /**
     * Create an instance of {@link GetTourMediaByBrandXML }
     * 
     */
    public GetTourMediaByBrandXML createGetTourMediaByBrandXML() {
        return new GetTourMediaByBrandXML();
    }

    /**
     * Create an instance of {@link GetLocationKeywordsResponse }
     * 
     */
    public GetLocationKeywordsResponse createGetLocationKeywordsResponse() {
        return new GetLocationKeywordsResponse();
    }

    /**
     * Create an instance of {@link GetVacationsByKeywordResponse.GetVacationsByKeywordResult }
     * 
     */
    public GetVacationsByKeywordResponse.GetVacationsByKeywordResult createGetVacationsByKeywordResponseGetVacationsByKeywordResult() {
        return new GetVacationsByKeywordResponse.GetVacationsByKeywordResult();
    }

    /**
     * Create an instance of {@link GetTourDayMediaByBrand }
     * 
     */
    public GetTourDayMediaByBrand createGetTourDayMediaByBrand() {
        return new GetTourDayMediaByBrand();
    }

    /**
     * Create an instance of {@link GetDeparturesExtendedXMLResponse }
     * 
     */
    public GetDeparturesExtendedXMLResponse createGetDeparturesExtendedXMLResponse() {
        return new GetDeparturesExtendedXMLResponse();
    }

    /**
     * Create an instance of {@link ExtendedepartureList }
     * 
     */
    public ExtendedepartureList createExtendedepartureList() {
        return new ExtendedepartureList();
    }

    /**
     * Create an instance of {@link GetDeparturePricingXML }
     * 
     */
    public GetDeparturePricingXML createGetDeparturePricingXML() {
        return new GetDeparturePricingXML();
    }

    /**
     * Create an instance of {@link GetAvailableMediaToursXML }
     * 
     */
    public GetAvailableMediaToursXML createGetAvailableMediaToursXML() {
        return new GetAvailableMediaToursXML();
    }

    /**
     * Create an instance of {@link GetExcursionMediaByOptionCode }
     * 
     */
    public GetExcursionMediaByOptionCode createGetExcursionMediaByOptionCode() {
        return new GetExcursionMediaByOptionCode();
    }

    /**
     * Create an instance of {@link GetTourMediaByBrandXMLResponse }
     * 
     */
    public GetTourMediaByBrandXMLResponse createGetTourMediaByBrandXMLResponse() {
        return new GetTourMediaByBrandXMLResponse();
    }

    /**
     * Create an instance of {@link GetDocumentTourItineraryXML }
     * 
     */
    public GetDocumentTourItineraryXML createGetDocumentTourItineraryXML() {
        return new GetDocumentTourItineraryXML();
    }

    /**
     * Create an instance of {@link GetHotelMediaByCityAndTourCodeResponse }
     * 
     */
    public GetHotelMediaByCityAndTourCodeResponse createGetHotelMediaByCityAndTourCodeResponse() {
        return new GetHotelMediaByCityAndTourCodeResponse();
    }

    /**
     * Create an instance of {@link GetHotelMediaByCityAndTourCode }
     * 
     */
    public GetHotelMediaByCityAndTourCode createGetHotelMediaByCityAndTourCode() {
        return new GetHotelMediaByCityAndTourCode();
    }

    /**
     * Create an instance of {@link GetAllAvailableToursXMLResponse }
     * 
     */
    public GetAllAvailableToursXMLResponse createGetAllAvailableToursXMLResponse() {
        return new GetAllAvailableToursXMLResponse();
    }

    /**
     * Create an instance of {@link AvailableTours }
     * 
     */
    public AvailableTours createAvailableTours() {
        return new AvailableTours();
    }

    /**
     * Create an instance of {@link GetDeparturesXML }
     * 
     */
    public GetDeparturesXML createGetDeparturesXML() {
        return new GetDeparturesXML();
    }

    /**
     * Create an instance of {@link GetAllAvailableToursResponse.GetAllAvailableToursResult }
     * 
     */
    public GetAllAvailableToursResponse.GetAllAvailableToursResult createGetAllAvailableToursResponseGetAllAvailableToursResult() {
        return new GetAllAvailableToursResponse.GetAllAvailableToursResult();
    }

    /**
     * Create an instance of {@link GetExcursionMediaByExcursionID }
     * 
     */
    public GetExcursionMediaByExcursionID createGetExcursionMediaByExcursionID() {
        return new GetExcursionMediaByExcursionID();
    }

    /**
     * Create an instance of {@link GetAllExcursionIDs }
     * 
     */
    public GetAllExcursionIDs createGetAllExcursionIDs() {
        return new GetAllExcursionIDs();
    }

    /**
     * Create an instance of {@link GetExcursionMediaByExcursionIDResponse }
     * 
     */
    public GetExcursionMediaByExcursionIDResponse createGetExcursionMediaByExcursionIDResponse() {
        return new GetExcursionMediaByExcursionIDResponse();
    }

    /**
     * Create an instance of {@link ExcursionMedia }
     * 
     */
    public ExcursionMedia createExcursionMedia() {
        return new ExcursionMedia();
    }

    /**
     * Create an instance of {@link GetShipMediaResponse }
     * 
     */
    public GetShipMediaResponse createGetShipMediaResponse() {
        return new GetShipMediaResponse();
    }

    /**
     * Create an instance of {@link ShipMedia }
     * 
     */
    public ShipMedia createShipMedia() {
        return new ShipMedia();
    }

    /**
     * Create an instance of {@link GetDeparturesBySeason }
     * 
     */
    public GetDeparturesBySeason createGetDeparturesBySeason() {
        return new GetDeparturesBySeason();
    }

    /**
     * Create an instance of {@link GetDeparturePricing }
     * 
     */
    public GetDeparturePricing createGetDeparturePricing() {
        return new GetDeparturePricing();
    }

    /**
     * Create an instance of {@link GetTourMedia }
     * 
     */
    public GetTourMedia createGetTourMedia() {
        return new GetTourMedia();
    }

    /**
     * Create an instance of {@link GetTourMediaByBrand }
     * 
     */
    public GetTourMediaByBrand createGetTourMediaByBrand() {
        return new GetTourMediaByBrand();
    }

    /**
     * Create an instance of {@link GetTourDayMedia }
     * 
     */
    public GetTourDayMedia createGetTourDayMedia() {
        return new GetTourDayMedia();
    }

    /**
     * Create an instance of {@link GetAllAvailableToursXML }
     * 
     */
    public GetAllAvailableToursXML createGetAllAvailableToursXML() {
        return new GetAllAvailableToursXML();
    }

    /**
     * Create an instance of {@link GetAvailableGSATours }
     * 
     */
    public GetAvailableGSATours createGetAvailableGSATours() {
        return new GetAvailableGSATours();
    }

    /**
     * Create an instance of {@link GetAllAvailableMediaToursXML }
     * 
     */
    public GetAllAvailableMediaToursXML createGetAllAvailableMediaToursXML() {
        return new GetAllAvailableMediaToursXML();
    }

    /**
     * Create an instance of {@link GetTourMediaByBrandResponse.GetTourMediaByBrandResult }
     * 
     */
    public GetTourMediaByBrandResponse.GetTourMediaByBrandResult createGetTourMediaByBrandResponseGetTourMediaByBrandResult() {
        return new GetTourMediaByBrandResponse.GetTourMediaByBrandResult();
    }

    /**
     * Create an instance of {@link GetAllAvailableMediaToursXMLResponse }
     * 
     */
    public GetAllAvailableMediaToursXMLResponse createGetAllAvailableMediaToursXMLResponse() {
        return new GetAllAvailableMediaToursXMLResponse();
    }

    /**
     * Create an instance of {@link GetGSADeparturesXMLResponse }
     * 
     */
    public GetGSADeparturesXMLResponse createGetGSADeparturesXMLResponse() {
        return new GetGSADeparturesXMLResponse();
    }

    /**
     * Create an instance of {@link GetAvailableMediaToursResponse.GetAvailableMediaToursResult }
     * 
     */
    public GetAvailableMediaToursResponse.GetAvailableMediaToursResult createGetAvailableMediaToursResponseGetAvailableMediaToursResult() {
        return new GetAvailableMediaToursResponse.GetAvailableMediaToursResult();
    }

    /**
     * Create an instance of {@link GetAllExcursionIDsResponse }
     * 
     */
    public GetAllExcursionIDsResponse createGetAllExcursionIDsResponse() {
        return new GetAllExcursionIDsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfInt }
     * 
     */
    public ArrayOfInt createArrayOfInt() {
        return new ArrayOfInt();
    }

    /**
     * Create an instance of {@link GetBasicHotelMediaByLocation }
     * 
     */
    public GetBasicHotelMediaByLocation createGetBasicHotelMediaByLocation() {
        return new GetBasicHotelMediaByLocation();
    }

    /**
     * Create an instance of {@link GetAvailableToursXML }
     * 
     */
    public GetAvailableToursXML createGetAvailableToursXML() {
        return new GetAvailableToursXML();
    }

    /**
     * Create an instance of {@link GetGroupDepartures }
     * 
     */
    public GetGroupDepartures createGetGroupDepartures() {
        return new GetGroupDepartures();
    }

    /**
     * Create an instance of {@link GetBasicHotelMediaByTourResponse }
     * 
     */
    public GetBasicHotelMediaByTourResponse createGetBasicHotelMediaByTourResponse() {
        return new GetBasicHotelMediaByTourResponse();
    }

    /**
     * Create an instance of {@link GetCurrentPromotionsXMLResponse }
     * 
     */
    public GetCurrentPromotionsXMLResponse createGetCurrentPromotionsXMLResponse() {
        return new GetCurrentPromotionsXMLResponse();
    }

    /**
     * Create an instance of {@link ArrayOfCurrentPromotion }
     * 
     */
    public ArrayOfCurrentPromotion createArrayOfCurrentPromotion() {
        return new ArrayOfCurrentPromotion();
    }

    /**
     * Create an instance of {@link GetTourDayMediaXMLResponse }
     * 
     */
    public GetTourDayMediaXMLResponse createGetTourDayMediaXMLResponse() {
        return new GetTourDayMediaXMLResponse();
    }

    /**
     * Create an instance of {@link GetLocationKeywords }
     * 
     */
    public GetLocationKeywords createGetLocationKeywords() {
        return new GetLocationKeywords();
    }

    /**
     * Create an instance of {@link GetCurrentPromotionsXML }
     * 
     */
    public GetCurrentPromotionsXML createGetCurrentPromotionsXML() {
        return new GetCurrentPromotionsXML();
    }

    /**
     * Create an instance of {@link GetDeparturePricingXMLResponse }
     * 
     */
    public GetDeparturePricingXMLResponse createGetDeparturePricingXMLResponse() {
        return new GetDeparturePricingXMLResponse();
    }

    /**
     * Create an instance of {@link ArrayOfDeparturePricing }
     * 
     */
    public ArrayOfDeparturePricing createArrayOfDeparturePricing() {
        return new ArrayOfDeparturePricing();
    }

    /**
     * Create an instance of {@link GetAllHotelCodesResponse }
     * 
     */
    public GetAllHotelCodesResponse createGetAllHotelCodesResponse() {
        return new GetAllHotelCodesResponse();
    }

    /**
     * Create an instance of {@link GetDeparturesExtendedXML }
     * 
     */
    public GetDeparturesExtendedXML createGetDeparturesExtendedXML() {
        return new GetDeparturesExtendedXML();
    }

    /**
     * Create an instance of {@link GetExcursionMediaByOptionCodeResponse }
     * 
     */
    public GetExcursionMediaByOptionCodeResponse createGetExcursionMediaByOptionCodeResponse() {
        return new GetExcursionMediaByOptionCodeResponse();
    }

    /**
     * Create an instance of {@link GetGroupDeparturesResponse.GetGroupDeparturesResult }
     * 
     */
    public GetGroupDeparturesResponse.GetGroupDeparturesResult createGetGroupDeparturesResponseGetGroupDeparturesResult() {
        return new GetGroupDeparturesResponse.GetGroupDeparturesResult();
    }

    /**
     * Create an instance of {@link GetAllAvailableMediaToursResponse.GetAllAvailableMediaToursResult }
     * 
     */
    public GetAllAvailableMediaToursResponse.GetAllAvailableMediaToursResult createGetAllAvailableMediaToursResponseGetAllAvailableMediaToursResult() {
        return new GetAllAvailableMediaToursResponse.GetAllAvailableMediaToursResult();
    }

    /**
     * Create an instance of {@link GetAvailableGSAToursXMLResponse }
     * 
     */
    public GetAvailableGSAToursXMLResponse createGetAvailableGSAToursXMLResponse() {
        return new GetAvailableGSAToursXMLResponse();
    }

    /**
     * Create an instance of {@link GetVacationsByKeyword }
     * 
     */
    public GetVacationsByKeyword createGetVacationsByKeyword() {
        return new GetVacationsByKeyword();
    }

    /**
     * Create an instance of {@link GetCurrentPromotions }
     * 
     */
    public GetCurrentPromotions createGetCurrentPromotions() {
        return new GetCurrentPromotions();
    }

    /**
     * Create an instance of {@link GetAvailableTours }
     * 
     */
    public GetAvailableTours createGetAvailableTours() {
        return new GetAvailableTours();
    }

    /**
     * Create an instance of {@link GetTourDayMediaByBrandXML }
     * 
     */
    public GetTourDayMediaByBrandXML createGetTourDayMediaByBrandXML() {
        return new GetTourDayMediaByBrandXML();
    }

    /**
     * Create an instance of {@link GetAvailableToursXMLResponse }
     * 
     */
    public GetAvailableToursXMLResponse createGetAvailableToursXMLResponse() {
        return new GetAvailableToursXMLResponse();
    }

    /**
     * Create an instance of {@link GetHotelMediaByTourCode }
     * 
     */
    public GetHotelMediaByTourCode createGetHotelMediaByTourCode() {
        return new GetHotelMediaByTourCode();
    }

    /**
     * Create an instance of {@link GetAvailableGSAToursResponse.GetAvailableGSAToursResult }
     * 
     */
    public GetAvailableGSAToursResponse.GetAvailableGSAToursResult createGetAvailableGSAToursResponseGetAvailableGSAToursResult() {
        return new GetAvailableGSAToursResponse.GetAvailableGSAToursResult();
    }

    /**
     * Create an instance of {@link GetBasicHotelMediaResponse }
     * 
     */
    public GetBasicHotelMediaResponse createGetBasicHotelMediaResponse() {
        return new GetBasicHotelMediaResponse();
    }

    /**
     * Create an instance of {@link ArrayOfDeparturePricingDetail }
     * 
     */
    public ArrayOfDeparturePricingDetail createArrayOfDeparturePricingDetail() {
        return new ArrayOfDeparturePricingDetail();
    }

    /**
     * Create an instance of {@link ExcursionFeature }
     * 
     */
    public ExcursionFeature createExcursionFeature() {
        return new ExcursionFeature();
    }

    /**
     * Create an instance of {@link CurrentPromotion }
     * 
     */
    public CurrentPromotion createCurrentPromotion() {
        return new CurrentPromotion();
    }

    /**
     * Create an instance of {@link BasicHotelMedia }
     * 
     */
    public BasicHotelMedia createBasicHotelMedia() {
        return new BasicHotelMedia();
    }

    /**
     * Create an instance of {@link DeparturePricingDetail }
     * 
     */
    public DeparturePricingDetail createDeparturePricingDetail() {
        return new DeparturePricingDetail();
    }

    /**
     * Create an instance of {@link HotelImage }
     * 
     */
    public HotelImage createHotelImage() {
        return new HotelImage();
    }

    /**
     * Create an instance of {@link HotelAmenity }
     * 
     */
    public HotelAmenity createHotelAmenity() {
        return new HotelAmenity();
    }

    /**
     * Create an instance of {@link DeparturePricing }
     * 
     */
    public DeparturePricing createDeparturePricing() {
        return new DeparturePricing();
    }

    /**
     * Create an instance of {@link ArrayOfExcursionFeature }
     * 
     */
    public ArrayOfExcursionFeature createArrayOfExcursionFeature() {
        return new ArrayOfExcursionFeature();
    }

    /**
     * Create an instance of {@link CurrentPromotionTour }
     * 
     */
    public CurrentPromotionTour createCurrentPromotionTour() {
        return new CurrentPromotionTour();
    }

    /**
     * Create an instance of {@link RoomMedia }
     * 
     */
    public RoomMedia createRoomMedia() {
        return new RoomMedia();
    }

    /**
     * Create an instance of {@link ArrayOfHotelAmenity }
     * 
     */
    public ArrayOfHotelAmenity createArrayOfHotelAmenity() {
        return new ArrayOfHotelAmenity();
    }

    /**
     * Create an instance of {@link ArrayOfExcursionPrice }
     * 
     */
    public ArrayOfExcursionPrice createArrayOfExcursionPrice() {
        return new ArrayOfExcursionPrice();
    }

    /**
     * Create an instance of {@link ArrayOfCurrentPromotionTour }
     * 
     */
    public ArrayOfCurrentPromotionTour createArrayOfCurrentPromotionTour() {
        return new ArrayOfCurrentPromotionTour();
    }

    /**
     * Create an instance of {@link ExcursionPrice }
     * 
     */
    public ExcursionPrice createExcursionPrice() {
        return new ExcursionPrice();
    }

    /**
     * Create an instance of {@link ArrayOfRoomMedia }
     * 
     */
    public ArrayOfRoomMedia createArrayOfRoomMedia() {
        return new ArrayOfRoomMedia();
    }

    /**
     * Create an instance of {@link ArrayOfHotelImage }
     * 
     */
    public ArrayOfHotelImage createArrayOfHotelImage() {
        return new ArrayOfHotelImage();
    }

    /**
     * Create an instance of {@link ArrayOfExtendedDeparture }
     * 
     */
    public ArrayOfExtendedDeparture createArrayOfExtendedDeparture() {
        return new ArrayOfExtendedDeparture();
    }

    /**
     * Create an instance of {@link ExtendedDeparture }
     * 
     */
    public ExtendedDeparture createExtendedDeparture() {
        return new ExtendedDeparture();
    }

    /**
     * Create an instance of {@link ArrayOfDeparture }
     * 
     */
    public ArrayOfDeparture createArrayOfDeparture() {
        return new ArrayOfDeparture();
    }

    /**
     * Create an instance of {@link Departure }
     * 
     */
    public Departure createDeparture() {
        return new Departure();
    }

    /**
     * Create an instance of {@link TourInfo }
     * 
     */
    public TourInfo createTourInfo() {
        return new TourInfo();
    }

    /**
     * Create an instance of {@link ArrayOfDayMedia }
     * 
     */
    public ArrayOfDayMedia createArrayOfDayMedia() {
        return new ArrayOfDayMedia();
    }

    /**
     * Create an instance of {@link ArrayOfTourMedia }
     * 
     */
    public ArrayOfTourMedia createArrayOfTourMedia() {
        return new ArrayOfTourMedia();
    }

    /**
     * Create an instance of {@link TourMedia }
     * 
     */
    public TourMedia createTourMedia() {
        return new TourMedia();
    }

    /**
     * Create an instance of {@link DayMedia }
     * 
     */
    public DayMedia createDayMedia() {
        return new DayMedia();
    }

    /**
     * Create an instance of {@link TourKeyword }
     * 
     */
    public TourKeyword createTourKeyword() {
        return new TourKeyword();
    }

    /**
     * Create an instance of {@link ArrayOfTourKeyword }
     * 
     */
    public ArrayOfTourKeyword createArrayOfTourKeyword() {
        return new ArrayOfTourKeyword();
    }

    /**
     * Create an instance of {@link ArrayOfAvailableMediaTour }
     * 
     */
    public ArrayOfAvailableMediaTour createArrayOfAvailableMediaTour() {
        return new ArrayOfAvailableMediaTour();
    }

    /**
     * Create an instance of {@link AvailableMediaTour }
     * 
     */
    public AvailableMediaTour createAvailableMediaTour() {
        return new AvailableMediaTour();
    }

    /**
     * Create an instance of {@link ArrayOfTour }
     * 
     */
    public ArrayOfTour createArrayOfTour() {
        return new ArrayOfTour();
    }

    /**
     * Create an instance of {@link Tour }
     * 
     */
    public Tour createTour() {
        return new Tour();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Facet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "minInclusive")
    public JAXBElement<Facet> createMinInclusive(Facet value) {
        return new JAXBElement<Facet>(_MinInclusive_QNAME, Facet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NumFacet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "minLength")
    public JAXBElement<NumFacet> createMinLength(NumFacet value) {
        return new JAXBElement<NumFacet>(_MinLength_QNAME, NumFacet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoFixedFacet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "enumeration")
    public JAXBElement<NoFixedFacet> createEnumeration(NoFixedFacet value) {
        return new JAXBElement<NoFixedFacet>(_Enumeration_QNAME, NoFixedFacet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebTourMedia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webapi.globusandcosmos.com/TourInformation.xsd", name = "GetTourMediaXMLResult")
    public JAXBElement<WebTourMedia> createGetTourMediaXMLResult(WebTourMedia value) {
        return new JAXBElement<WebTourMedia>(_GetTourMediaXMLResult_QNAME, WebTourMedia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DepartureList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webapi.globusandcosmos.com/DepartureList.xsd", name = "GetDeparturesXMLResult")
    public JAXBElement<DepartureList> createGetDeparturesXMLResult(DepartureList value) {
        return new JAXBElement<DepartureList>(_GetDeparturesXMLResult_QNAME, DepartureList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Keybase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "key")
    public JAXBElement<Keybase> createKey(Keybase value) {
        return new JAXBElement<Keybase>(_Key_QNAME, Keybase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Facet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "maxInclusive")
    public JAXBElement<Facet> createMaxInclusive(Facet value) {
        return new JAXBElement<Facet>(_MaxInclusive_QNAME, Facet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebTourMedia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webapi.globusandcosmos.com/TourInformation.xsd", name = "GetTourMediaByBrandXMLResult")
    public JAXBElement<WebTourMedia> createGetTourMediaByBrandXMLResult(WebTourMedia value) {
        return new JAXBElement<WebTourMedia>(_GetTourMediaByBrandXMLResult_QNAME, WebTourMedia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Facet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "minExclusive")
    public JAXBElement<Facet> createMinExclusive(Facet value) {
        return new JAXBElement<Facet>(_MinExclusive_QNAME, Facet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Wildcard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "anyAttribute")
    public JAXBElement<Wildcard> createAnyAttribute(Wildcard value) {
        return new JAXBElement<Wildcard>(_AnyAttribute_QNAME, Wildcard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Facet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "maxExclusive")
    public JAXBElement<Facet> createMaxExclusive(Facet value) {
        return new JAXBElement<Facet>(_MaxExclusive_QNAME, Facet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NumFacet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "length")
    public JAXBElement<NumFacet> createLength(NumFacet value) {
        return new JAXBElement<NumFacet>(_Length_QNAME, NumFacet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DepartureList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webapi.globusandcosmos.com/DepartureList.xsd", name = "GetGSADeparturesXMLResult")
    public JAXBElement<DepartureList> createGetGSADeparturesXMLResult(DepartureList value) {
        return new JAXBElement<DepartureList>(_GetGSADeparturesXMLResult_QNAME, DepartureList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NumFacet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "fractionDigits")
    public JAXBElement<NumFacet> createFractionDigits(NumFacet value) {
        return new JAXBElement<NumFacet>(_FractionDigits_QNAME, NumFacet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link All }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "all")
    public JAXBElement<All> createAll(All value) {
        return new JAXBElement<All>(_All_QNAME, All.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AvailableMediaTours }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webapi.globusandcosmos.com/AvailableTours.xsd", name = "GetAllAvailableMediaToursXMLResult")
    public JAXBElement<AvailableMediaTours> createGetAllAvailableMediaToursXMLResult(AvailableMediaTours value) {
        return new JAXBElement<AvailableMediaTours>(_GetAllAvailableMediaToursXMLResult_QNAME, AvailableMediaTours.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebTourMedia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webapi.globusandcosmos.com/TourInformation.xsd", name = "GetTourDayMediaByBrandXMLResult")
    public JAXBElement<WebTourMedia> createGetTourDayMediaByBrandXMLResult(WebTourMedia value) {
        return new JAXBElement<WebTourMedia>(_GetTourDayMediaByBrandXMLResult_QNAME, WebTourMedia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AvailableTours }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webapi.globusandcosmos.com/AvailableTours.xsd", name = "GetAllAvailableToursXMLResult")
    public JAXBElement<AvailableTours> createGetAllAvailableToursXMLResult(AvailableTours value) {
        return new JAXBElement<AvailableTours>(_GetAllAvailableToursXMLResult_QNAME, AvailableTours.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AvailableTours }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webapi.globusandcosmos.com/AvailableTours.xsd", name = "GetAvailableToursXMLResult")
    public JAXBElement<AvailableTours> createGetAvailableToursXMLResult(AvailableTours value) {
        return new JAXBElement<AvailableTours>(_GetAvailableToursXMLResult_QNAME, AvailableTours.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AvailableMediaTours }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webapi.globusandcosmos.com/AvailableTours.xsd", name = "GetAvailableMediaToursXMLResult")
    public JAXBElement<AvailableMediaTours> createGetAvailableMediaToursXMLResult(AvailableMediaTours value) {
        return new JAXBElement<AvailableMediaTours>(_GetAvailableMediaToursXMLResult_QNAME, AvailableMediaTours.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DepartureList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webapi.globusandcosmos.com/DepartureList.xsd", name = "GetDeparturesBySeasonXMLResult")
    public JAXBElement<DepartureList> createGetDeparturesBySeasonXMLResult(DepartureList value) {
        return new JAXBElement<DepartureList>(_GetDeparturesBySeasonXMLResult_QNAME, DepartureList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AvailableTours }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webapi.globusandcosmos.com/AvailableTours.xsd", name = "GetAvailableGSAToursXMLResult")
    public JAXBElement<AvailableTours> createGetAvailableGSAToursXMLResult(AvailableTours value) {
        return new JAXBElement<AvailableTours>(_GetAvailableGSAToursXMLResult_QNAME, AvailableTours.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebTourMedia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webapi.globusandcosmos.com/TourInformation.xsd", name = "GetTourDayMediaXMLResult")
    public JAXBElement<WebTourMedia> createGetTourDayMediaXMLResult(WebTourMedia value) {
        return new JAXBElement<WebTourMedia>(_GetTourDayMediaXMLResult_QNAME, WebTourMedia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExplicitGroup }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "choice")
    public JAXBElement<ExplicitGroup> createChoice(ExplicitGroup value) {
        return new JAXBElement<ExplicitGroup>(_Choice_QNAME, ExplicitGroup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NumFacet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "maxLength")
    public JAXBElement<NumFacet> createMaxLength(NumFacet value) {
        return new JAXBElement<NumFacet>(_MaxLength_QNAME, NumFacet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExplicitGroup }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "sequence")
    public JAXBElement<ExplicitGroup> createSequence(ExplicitGroup value) {
        return new JAXBElement<ExplicitGroup>(_Sequence_QNAME, ExplicitGroup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExtendedepartureList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webapi.globusandcosmos.com/ExtendedDepartureList.xsd", name = "GetDeparturesExtendedXMLResult")
    public JAXBElement<ExtendedepartureList> createGetDeparturesExtendedXMLResult(ExtendedepartureList value) {
        return new JAXBElement<ExtendedepartureList>(_GetDeparturesExtendedXMLResult_QNAME, ExtendedepartureList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Keybase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "unique")
    public JAXBElement<Keybase> createUnique(Keybase value) {
        return new JAXBElement<Keybase>(_Unique_QNAME, Keybase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocalElement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "element", scope = GroupType.class)
    public JAXBElement<LocalElement> createGroupTypeElement(LocalElement value) {
        return new JAXBElement<LocalElement>(_GroupTypeElement_QNAME, LocalElement.class, GroupType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupRef }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "group", scope = GroupType.class)
    public JAXBElement<GroupRef> createGroupTypeGroup(GroupRef value) {
        return new JAXBElement<GroupRef>(_GroupTypeGroup_QNAME, GroupRef.class, GroupType.class, value);
    }

}
