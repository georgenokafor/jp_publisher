package com.mapped.publisher.api;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-10
 * Time: 9:28 AM
 * To change this template use File | Settings | File Templates.
 */
public class ApiConstants {

    public final static String MSG_EMAIL = "MSG_EMAIL";
    public final static String MSG_PUBLISH = "MSG_PUBLISH";

    public final static String STATUS_OK = "OK";
    public final static String STATUS_ERROR = "GENERIC_ERROR";
    public final static String STATUS_MSG_ERROR = "MSG_ERROR";
    public final static String STATUS_SERVER_ERROR = "SERVER_ERROR";

    public final static String S3_EMAIL_FILE_PATH = "email_";



}
