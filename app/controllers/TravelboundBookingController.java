package controllers;

import com.google.inject.Inject;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.actions.TravelboundAuthorized;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.TabType;
import com.mapped.publisher.view.TravelboundBookingView;
import com.umapped.external.travelbound.*;
import com.umapped.external.travelbound.api.*;
import io.jsonwebtoken.lang.Collections;
import models.publisher.Account;
import models.publisher.Trip;
import models.publisher.TripDetail;
import org.h2.util.StringUtils;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by wei on 2017-05-11.
 */
public class TravelboundBookingController
    extends Controller {

  @Inject TravelboundWebService service;
  @Inject private FormFactory formFactory;

  @Inject private TravelboundBookingMapper mapper;

  @Inject private TravelboundImportService importService;

  @With({Authenticated.class, Authorized.class, TravelboundAuthorized.class})
  public Result searchBookingForm(String tripId) {
    List<TravelboundAPICredential> credentials = (List<TravelboundAPICredential>) ctx().args.get(TravelboundAuthorized
                                                                                        .TRAVELBOUND_CREDENTIAL);
    TravelboundBookingView view = buildView(tripId, credentials);
    return ok(views.html.travelbound.travelboundImport.render(view));
  }

  private TravelboundAPICredential getCredential(List<TravelboundAPICredential> credentials, Integer clientId) {
    for (TravelboundAPICredential c : credentials) {
      if (c.getClientId() == clientId) {
        return c;
      }
    }
    return null;
  }

  @With({Authenticated.class, Authorized.class, TravelboundAuthorized.class})
  public Result searchBooking(String tripId, Integer inAgentId, String inRefType, String inRefNumber) {

    List<TravelboundAPICredential> credentials = (List<TravelboundAPICredential>) ctx().args.get(TravelboundAuthorized
                                                                                        .TRAVELBOUND_CREDENTIAL);

    TravelboundAPICredential credential = getCredential(credentials, inAgentId);
    if (credential == null) {
      Log.err("No matching credentails found for client Id:" + inAgentId);
      BaseView view = new BaseView();
      view.message = "AgentId is invalid";
      return ok(views.html.common.message.render(view));
    }

    Log.debug("getBooking: " + inRefNumber);
    TBookingReference bookingReference = new TBookingReference();
    bookingReference.setValue(inRefNumber);
    if (StringUtils.equals(inRefType, "client")) {
      bookingReference.setReferenceSource(TReferenceSource.CLIENT);
    }
    else {
      bookingReference.setReferenceSource(TReferenceSource.API);
    }

    try {
      TSearchBookingResponse result = service.searchBooking(credential, bookingReference);
      // request specific error, i.e. function not authorized, need to investigate and likely contact travelbound
      if (result.getErrors() != null &&  !Collections.isEmpty(result.getErrors().getErrors())) {
        throw new TravelboundAPIException(result.getErrors().getErrors());
      }

      List<TravelboundBookingSearchResult> bookings = mapper.map(credential.getClientId(), result);
      if (Collections.isEmpty(bookings)) {
        BaseView view = new BaseView();
        view.message = "Booking not found or cancelled";
        return ok(views.html.common.message.render(view));
      }
      else {
        return ok(views.html.travelbound.travelboundBookings.render(tripId, bookings));
      }

    }
    catch (TravelboundAPIException ex) {
      if (ex.getErrors() != null) {
        for (TError error : ex.getErrors()) {
          Log.err("Travelbound API Detailed error: " + error.getErrorText());
        }
      }

      Log.err("Fail to search travelbound booking", ex);

      BaseView view = new BaseView();
      view.message = "Search error, please try again or contact support";
      return ok(views.html.common.message.render(view));
    }
  }

  @With({Authenticated.class, Authorized.class, TravelboundAuthorized.class})
  public Result importBooking(String tripId, Integer inAgentId, String bookingRefNumber) {

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Account account = sessionMgr.getCredentials().getAccount();

    List<TravelboundAPICredential> credentials = (List<TravelboundAPICredential>) ctx().args.get(TravelboundAuthorized
                                                                                        .TRAVELBOUND_CREDENTIAL);

    TravelboundAPICredential credential = getCredential(credentials, inAgentId);
    if (credential == null) {
      Log.err("No matching credentails found for client Id:" + inAgentId);
      BaseView view = new BaseView();
      view.message = "AgentId is invalid";
      return ok(views.html.common.message.render(view));
    }

    try {
      importService.importBooking(account, credential, tripId, bookingRefNumber);
    }
    catch (TravelboundImportException e) {
      Log.err("Fail to import booking", e);
      BaseView view = new BaseView();
      view.message = "Importing booking from TravelBound failed, please try again or contact support";
      return ok(views.html.common.message.render(view));
    }
    return redirect(routes.BookingController.bookings(tripId, new TabType.Bound(TabType.ITINERARY), ""));
  }

  @With({Authenticated.class, Authorized.class, TravelboundAuthorized.class})
  public Result updateBookings(String tripId, Integer inAgentId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Account account = sessionMgr.getCredentials().getAccount();
    Trip trip = Trip.find.byId(tripId);
    List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);

    Set<String> bookingCodes = new HashSet<>();
    if (tripDetails != null) {
      for (TripDetail detail : tripDetails) {
        if (BookingSrc.ImportSrc.TRAVELBOUND.equals(detail.getImportSrc())) {
          bookingCodes.add(detail.getImportSrcId());
        }
      }
    }

    List<TravelboundAPICredential> credentials = (List<TravelboundAPICredential>) ctx().args.get(TravelboundAuthorized
                                                                                        .TRAVELBOUND_CREDENTIAL);

    TravelboundAPICredential credential = getCredential(credentials, inAgentId);
    if (credential == null) {
      Log.err("No matching credentails found for client Id:" + inAgentId);
      BaseView view = new BaseView();
      view.message = "AgentId is invalid";
      return ok(views.html.common.message.render(view));
    }

    try {
      importService.updateBookings(account, credential, trip, bookingCodes);
    }
    catch (TravelboundImportException e) {
      Log.err("Fail to update booking", e);
      BaseView view = new BaseView();
      view.message = "Update booking from TravelBound failed, please try again or contact support";
      return ok(views.html.common.message.render(view));
    }
    return redirect(routes.BookingController.bookings(tripId, new TabType.Bound(TabType.ITINERARY), ""));
  }

  public TravelboundBookingView buildView(String tripId, List<TravelboundAPICredential> credentials) {

    Trip trip = Trip.find.byId(tripId);
    List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
    boolean hasTravelBoundBooking = false;

    if (tripDetails != null) {
      for (TripDetail detail : tripDetails) {
        if (BookingSrc.ImportSrc.TRAVELBOUND.equals(detail.getImportSrc())) {
          hasTravelBoundBooking = true;
          break;
        }
      }
    }

    TravelboundBookingView view = new TravelboundBookingView(hasTravelBoundBooking);
    view.tripId = tripId;

    view.tripName = trip.name;

    if (trip.starttimestamp > 0) {
      view.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
    }

    if (trip.endtimestamp > 0) {
      view.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
    }

    List<Integer> clientIds = new ArrayList<>();
    for (TravelboundAPICredential c : credentials) {
      clientIds.add(c.getClientId());
    }
    view.clientIds = clientIds;
    return view;
  }
}
