package com.mapped.publisher.persistence;

import models.publisher.FileImage;

import java.beans.Transient;

/**
 * Created by surge on 2016-11-03.
 */
public interface IUmappedModelWithImage {

  @Transient
  Long getImageFileId();

  @Transient
  String getImageUrl();

  @Transient
  FileImage getFileImage();

  @Transient
  IUmappedModelWithImage setFileImage(FileImage image);

  @Transient
  String getMobileImageFilename();
}
