package com.mapped.publisher.utils;

import it.innove.play.pdf.PdfGenerator;
import org.apache.commons.lang3.time.StopWatch;
import play.api.Play;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by surge on 2016-01-05.
 */
public class PDFUtils {

  public static void loadLocalFonts(PdfGenerator pdfGenerator) {
    if (pdfGenerator != null) {
      StopWatch sw = new StopWatch();
      sw.reset();
      sw.start();
      File              dir       = new File(Play.current().path() + "/conf/fonts");
      ArrayList<String> fontFiles = new ArrayList<>(50);
      for (File file : dir.listFiles()) {
        fontFiles.add(file.getAbsolutePath());
      }

      pdfGenerator.loadLocalFonts(fontFiles); //TODO: MIG24 How to Inject?????
      sw.stop();
      Log.info("Loaded " + fontFiles.size() + " font name in " + sw.getTime() + "ms");
    }
  }
}
