package parsers;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.extractor.booking.OpenSkiesExtractor;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import models.publisher.FileInfo;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.running;


/**
 * Created by george on 2018-05-29.
 */
public class OpenSkiesExtractorTest extends Assert {

  @Test
  public void testSample1()  {
    try {
      String dir = "/Users/george/OpenSkies/Sample2.pdf";
      File file = new File(dir);
      InputStream ins = new FileInputStream(file);

      OpenSkiesExtractor p = new OpenSkiesExtractor();
      p.initImpl(null);
      TripVO t = p.extractData(ins);
      //System.out.println(t);
      assertTrue("ok", t.getImportSrc() == BookingSrc.ImportSrc.OPEN_SKIES_PDF);

    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

}
