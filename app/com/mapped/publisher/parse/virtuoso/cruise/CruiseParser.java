package com.mapped.publisher.parse.virtuoso.cruise;

import com.mapped.publisher.parse.virtuoso.cruise.v5_0.*;
import com.mapped.publisher.parse.virtuoso.cruise.v5_0.Root.Cruise.CruiseInfo.Promotions.Promotion;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import com.mapped.publisher.utils.Utils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.apache.commons.lang3.StringEscapeUtils;

import javax.rmi.CORBA.Util;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by twong on 15-05-19.
 */
public class CruiseParser {


  private static String[] dateTimeFormat = {"dd MMM yyyy hh:mm a", "yyyy-MM-dd HH:mm"} ;
  private static DateUtils du = new DateUtils();
  private static Calendar now = Calendar.getInstance();

  private static Pattern dateRegExPattern = Pattern.compile("[0-9]+\\s[A-Za-z]{3}\\s[0-9]{4}");
  private static Pattern cruiseNamePattern = Pattern.compile("[A-Za-z0-9\\s.,]+[0-9]+\\sdays");
  private static Pattern cruiseDurationPattern = Pattern.compile("[0-9]+\\sdays");
  private static Pattern cruiseIdPattern = Pattern.compile("[0-9]{8}");


  public static CruiseVO getCruise(String url) {
    CruiseVO cruiseTrip =  null;
    CruiseShipVO cruise =  null;

    try {
      StopWatch sw = new StopWatch();
      sw.start();

      HttpClient client = new HttpClient();
      client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
      HttpMethod method = new GetMethod(url);
      method.setFollowRedirects(true);

      client.executeMethod(method);


      InputStream in = method.getResponseBodyAsStream();

      BufferedReader rd = new BufferedReader (new InputStreamReader(in));
      StringBuilder result = new StringBuilder();
      String line;
      while ((line = rd.readLine()) != null) {
        result.append(line);
      }


      long t0 = sw.getTime();

      //Document cruiseDoc = Jsoup.connect(url).userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
      //                         .referrer("http://www.google.com").timeout(30000).get();

      Document cruiseDoc = Jsoup.parse(result.toString());

      long t1 = sw.getTime();

      if (cruiseDoc != null && cruiseDoc.hasText() && cruiseDoc.getElementById("titleName") != null) {
        cruiseTrip = new CruiseVO();
        cruise = new CruiseShipVO();
        String cruiseName1 = "";
        if(cruiseDoc.getElementById("shipContainer") != null) {
          if(cruiseDoc.getElementById("shipContainer").getElementsByTag("a") != null && !cruiseDoc.getElementById("shipContainer").getElementsByTag("a").isEmpty()) {
            cruise.setName(cruiseDoc.getElementById("shipContainer").getElementsByTag("a").get(0).text());
          }
          if(cruiseDoc.getElementById("shipContainer").getElementsByTag("div") != null && cruiseDoc.getElementById("shipContainer").getElementsByTag("div").size() >= 2) {
            cruise.setCompany(cruiseDoc.getElementById("shipContainer").getElementsByTag("div").get(1).text().replace("Cruise Line:", "").trim());
          }
          if(cruiseDoc.getElementById("shipContainer").getElementsByTag("div") != null && cruiseDoc.getElementById("shipContainer").getElementsByTag("div").size() >= 3) {
            String t = cruiseDoc.getElementById("shipContainer").getElementsByTag("div").get(3).text().replace("Cruise Line:", "").trim();
            if (t != null && t.contains(",") && t.contains(" to ")) {
              cruiseName1 =  " - " + t;
            }
          }
        }

        if (cruiseDoc.getElementById("abouttheShipContainer") != null && cruiseDoc.getElementById("abouttheShipContainer").getElementsContainingText("Deck Plans") != null && !cruiseDoc.getElementById("abouttheShipContainer").getElementsContainingText("Deck Plans").isEmpty()) {
          cruise.setDeckplanUrl("https://www.virtuoso.com" + cruiseDoc.getElementById("abouttheShipContainer").getElementsByAttributeValueContaining("href", "deckPlanTab").get(0).attr("href"));
        }

        HashMap<String, String> cruisePhotos = new HashMap<>();
        for (Element e : cruiseDoc.getElementsByTag("img")) {
          //if (!e.attr("data-big").equals("") && !e.attr("alt").equals("")) {
          //  cruisePhotos.put(e.attr("data-big"), e.attr("alt"));
          //}
          if(e.attr("src").contains("Brochures")) {
            String imgName = e.attr("src").substring(e.attr("src").lastIndexOf("/") + 1, e.attr("src").length());
            String key = "https://media.virtuoso.com/m/Images/Brochures/" + imgName;
            String value = imgName;
            cruisePhotos.put(key, value);
          }
        }
        cruise.setCruisePhotos(cruisePhotos);

        cruiseTrip.setCruiseShip(cruise);
        cruiseTrip.setCruiseName(cruiseDoc.getElementById("titleName").text() + cruiseName1);
        if (cruiseDoc.getElementById("pageInfo") != null) {
          cruiseTrip.setVirtuosoUrl(cruiseDoc.getElementById("pageInfo").attr("data-shareurl"));
        }
        if(cruiseDoc.getElementById("WhatIsIncluded") != null && cruiseDoc.getElementById("WhatIsIncluded").getElementsByClass("whatIsIncludedSection") != null && cruiseDoc.getElementById("WhatIsIncluded").getElementsByClass("whatIsIncludedSection").size() > 1) {
          cruiseTrip.setIncludedAmenities(cruiseDoc.getElementById("WhatIsIncluded").getElementsByClass("whatIsIncludedSection").get(0)
                  .text()
                  .replace("What's Included", "")
                  .trim());
          cruiseTrip.setExcludedAmenities(cruiseDoc.getElementById("WhatIsIncluded").getElementsByClass("whatIsIncludedSection").get(1)
                  .text()
                  .replace("Not included", "")
                  .trim());
        }

        if(cruiseDoc.getElementById("Description") != null && cruiseDoc.getElementById("Description").getElementsByTag("div") != null && cruiseDoc.getElementById("Description").getElementsByTag("div").size() >= 2) {
          cruiseTrip.setDescription(cruiseDoc.getElementById("Description").getElementsByTag("div").get(1).text());
        }
        cruiseTrip.setCruiseDescription("");
        if(cruiseDoc.getElementById("termsAndConditions") != null && cruiseDoc.getElementById("termsAndConditions").getElementsByClass("read-only-text-content") != null && !cruiseDoc.getElementById("termsAndConditions").getElementsByClass("read-only-text-content").isEmpty()) {
          cruiseTrip.setTermsAndCondition(cruiseDoc.getElementById("termsAndConditions")
                  .getElementsByClass("read-only-text-content").last()
                  .text());
        }

        if(cruiseDoc.getElementById("hdValues") != null) {
          String[] dates = cruiseDoc.getElementById("hdValues").attr("data-token").split("-");
          cruiseTrip.setStartDate(getTimestamp(dates[0].trim(), "12:00 AM"));
          cruiseTrip.setEndDate(getTimestamp(dates[1].trim(), "12:00 AM"));
          cruiseTrip.setDuration(cruiseTrip.getCruiseName().substring(cruiseTrip.getCruiseName().lastIndexOf("("), cruiseTrip.getCruiseName().length()).replace("(", "").replace(")", ""));
        }

        if(cruiseDoc.getElementById("shipContainer") != null && cruiseDoc.getElementById("shipContainer").getElementsByTag("div").size() >= 4) {
          String[] locations = cruiseDoc.getElementById("shipContainer").getElementsByTag("div").get(3).text().split(" to ");
          cruiseTrip.setStartLocation(locations[0]);
          cruiseTrip.setEndLocation(locations[1]);
        }

        /*String title = cruiseDoc.getElementById("titleDetails").text();

        String[] titleDetails = title.split("\\|");
        if (titleDetails != null) {
          if (titleDetails.length > 2 ) {
            for (int i = 2; i < titleDetails.length; i++) {
              titleDetails[1] = titleDetails[1] + " " + titleDetails[i];
            }
          }
          cruise.setCompany(titleDetails[0].trim());

          Matcher dates = dateRegExPattern.matcher(titleDetails[1]);
          if (dates.find()) {
            cruiseTrip.setStartDate(getTimestamp(dates.group(), "12:00 AM"));
            titleDetails[1] = titleDetails[1].replace(dates.group(), "").replace("-", "");
          }
          if (dates.find()) {
            cruiseTrip.setEndDate(getTimestamp(dates.group(), "12:00 AM"));
            titleDetails[1] = titleDetails[1].replace(dates.group(), "");
          }

          Matcher cruiseName = cruiseNamePattern.matcher(titleDetails[1]);
          if (cruiseName.find()) {
            cruise.setName(cruiseName.group().substring(0, cruiseName.group().length() - 8).trim());
            System.out.println(cruise.getName());
            titleDetails[1] = titleDetails[1].replace(cruise.getName(), "");
          }

          cruiseTrip.setCruiseDescription(titleDetails[1].trim());

          Matcher duration = cruiseDurationPattern.matcher(titleDetails[1]);
          if (duration.find()) {
            cruiseTrip.setDuration(duration.group().split(" ")[0]);
            titleDetails[1] = titleDetails[1].replace(duration.group(), "");
          }

          cruiseTrip.setStartLocation(titleDetails[1].split(" to ")[0].trim());
          cruiseTrip.setEndLocation(titleDetails[1].split(" to ")[1].trim());
        }*/

        String cruiseId = "";
        if (cruiseDoc.getElementById("pageInfo") != null ) {
          Matcher cruiseIds = cruiseIdPattern.matcher(cruiseDoc.getElementById("pageInfo").attr("data-shareurl"));
          if (cruiseIds.find())
            cruiseId = cruiseIds.group();
        } else if (cruiseDoc.getElementById("termsAndConditionsHeader") != null){
          cruiseId = cruiseDoc.getElementById("termsAndConditionsHeader")
                   .attr("data-meid");
        }


        cruiseTrip.setCruiseId(cruiseId);
        long t2 = sw.getTime();

        long t3 = sw.getTime();

        ArrayList<CruiseStopVO> cruiseItineraries = new ArrayList<>();
        String lastStartDate = "";
        String lastEndDate = "";

        Elements rows = cruiseDoc.getElementById("cruise-itinerary-container").getElementsByTag("tr");
        for(Element e: rows) {
          if(e.getElementsByClass("js-itinerary-row") != null && !e.getElementsByClass("js-itinerary-row").isEmpty()) {
            CruiseStopVO cruiseItinerary = new CruiseStopVO();

            cruiseItinerary.setCruiseStop(true);
            cruiseItinerary.setCruiseId(cruiseId);
            cruiseItinerary.setDay(Integer.parseInt(e.attr("data-cruise-day")));

            if(e.getElementsByTag("td") != null && e.getElementsByTag("td").size() >= 4) {
              cruiseItinerary.setName(e.getElementsByTag("td").get(1).text().replace("Shore Excursion(s)", "").trim());

              if (e.getElementsByTag("td").size() == 4) {
                if (e.getElementsByTag("td").get(2).text().equals("—") || e.getElementsByTag("td").get(2).text().trim().isEmpty()) {
                  cruiseItinerary.setStartTimestamp(getTimestamp(lastStartDate, "12:00 AM"));
                  cruiseItinerary.setName(e.getElementsByTag("td")
                                           .get(0)
                                           .text()
                                           .replace("Shore Excursion(s)", "")
                                           .trim());
                } else {
                  cruiseItinerary.setStartTimestamp(getTimestamp(lastStartDate, e.getElementsByTag("td").get(1).text()));
                  cruiseItinerary.setName(e.getElementsByTag("td")
                                           .get(0)
                                           .text()
                                           .replace("Shore Excursion(s)", "")
                                           .trim());
                }
              } else if (e.getElementsByTag("td").get(2).text().equals("—") || e.getElementsByTag("td").get(2).text().trim().isEmpty()) {
                if (e.getElementsByTag("div").get(0).text().isEmpty()) {
                  cruiseItinerary.setStartTimestamp(getTimestamp(lastStartDate, "12:00 AM"));
                  cruiseItinerary.setName(e.getElementsByTag("td").get(0).text().replace("Shore Excursion(s)", "").trim());
                } else {
                  cruiseItinerary.setStartTimestamp(getTimestamp(e.getElementsByTag("div").get(0).text(), "12:00 AM"));
                  cruiseItinerary.setName(e.getElementsByTag("td").get(1).text().replace("Shore Excursion(s)", "").trim());
                  lastStartDate = e.getElementsByTag("div").get(0).text();
                }
              } else {
                if (e.getElementsByTag("div").get(0).text().isEmpty()) {
                  cruiseItinerary.setStartTimestamp(getTimestamp(lastStartDate, e.getElementsByTag("td").get(2).text()));
                  cruiseItinerary.setName(e.getElementsByTag("td").get(0).text().replace("Shore Excursion(s)", "").trim());
                } else {
                    cruiseItinerary.setStartTimestamp(getTimestamp(e.getElementsByTag("div").get(0).text(), e.getElementsByTag("td").get(2).text()));
                    cruiseItinerary.setName(e.getElementsByTag("td").get(1).text().replace("Shore Excursion(s)", "").trim());
                    lastStartDate = e.getElementsByTag("div").get(0).text();
                                  }
              }

              if (e.getElementsByTag("td").size() == 4) {
                if (e.getElementsByTag("td").get(2).text().equals("—") || e.getElementsByTag("td").get(2).text().trim().isEmpty()) {
                  cruiseItinerary.setEndTimestamp(getTimestamp(lastStartDate, "12:00 AM"));
                  cruiseItinerary.setName(e.getElementsByTag("td").get(0).text().replace("Shore Excursion(s)", "").trim());
                } else {
                  cruiseItinerary.setEndTimestamp(getTimestamp(lastStartDate, e.getElementsByTag("td").get(2).text()));
                  cruiseItinerary.setName(e.getElementsByTag("td").get(0).text().replace("Shore Excursion(s)", "").trim());
                }
              } else if (e.getElementsByTag("td").get(3).text().equals("—") || e.getElementsByTag("td").get(3).text().trim().isEmpty()) {
                if (e.getElementsByTag("div").get(0).text().isEmpty()) {
                  cruiseItinerary.setEndTimestamp(getTimestamp(lastEndDate, "12:00 AM"));
                  cruiseItinerary.setName(e.getElementsByTag("td").get(0).text().replace("Shore Excursion(s)", "").trim());
                } else {
                  cruiseItinerary.setEndTimestamp(getTimestamp(e.getElementsByTag("div").get(0).text(), "12:00 AM"));
                  cruiseItinerary.setName(e.getElementsByTag("td").get(1).text().replace("Shore Excursion(s)", "").trim());
                  lastEndDate = e.getElementsByTag("div").get(0).text();
                }
              } else {
                if (e.getElementsByTag("div").get(0).text().isEmpty()) {
                  cruiseItinerary.setEndTimestamp(getTimestamp(lastEndDate, e.getElementsByTag("td").get(3).text()));
                  cruiseItinerary.setName(e.getElementsByTag("td").get(0).text().replace("Shore Excursion(s)", "").trim());
                } else {
                  cruiseItinerary.setEndTimestamp(getTimestamp(e.getElementsByTag("div").get(0).text(), e.getElementsByTag("td").get(3).text()));
                  cruiseItinerary.setName(e.getElementsByTag("td").get(1).text().replace("Shore Excursion(s)", "").trim());
                  lastEndDate = e.getElementsByTag("div").get(0).text();
                }
              }

              String descriptionId = "js-day-description-" + e.attr("data-cruise-day");
              if (cruiseDoc.getElementById(descriptionId) != null && cruiseDoc.getElementById(descriptionId).getElementsByTag("td") != null && cruiseDoc.getElementById(descriptionId).getElementsByTag("td").size() >= 2) {
                cruiseItinerary.setNote(cruiseDoc.getElementById(descriptionId).getElementsByTag("td").get(1).text());
              }

              //cruiseItinerary.setLatitude();
              //cruiseItinerary.setLongitude();
            }
            if (cruiseItinerary.getStartTimestamp() != null) {
              if (cruiseItinerary.getName().contains("Pre-")) {
                cruiseItinerary.setName(cruiseItinerary.getName().substring(0, cruiseItinerary.getName().indexOf("Pre-")));
              }
              if (cruiseItinerary.getName().contains("Virtuoso")) {
                cruiseItinerary.setName(cruiseItinerary.getName().substring(0, cruiseItinerary.getName().indexOf("Virtuoso")));
              }
              if (cruiseItinerary.getName().contains("Post-")) {
                cruiseItinerary.setName(cruiseItinerary.getName().substring(0, cruiseItinerary.getName().indexOf("Post-")));
              }
              if (cruiseItinerary.getName().contains("Shore Exc")) {
                cruiseItinerary.setName(cruiseItinerary.getName().substring(0, cruiseItinerary.getName().indexOf("Shore Exc")));
              }
              cruiseItineraries.add(cruiseItinerary);
            }
          }
        }

        cruiseTrip.setCruiseStops(cruiseItineraries);

        /*Document partialDoc = Jsoup.connect("http://www.virtuoso.com/cruises/ItineraryPartial/" + cruiseId + "/View")
                                   .userAgent(
                                       "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
                                   .referrer("http://www.google.com")
                                   .timeout(40000)
                                   .get();

        long t3 = sw.getTime();

        ArrayList<CruiseStopVO> cruiseItineraries = new ArrayList<>();

        Matcher cruiseItineraryLatitude = Pattern.compile("\"Latitude\":(\"[\\-0-9.]+\"|null)")
                                                 .matcher(cruiseDoc.toString());
        Matcher cruiseItineraryLongitude = Pattern.compile("\"Longitude\":(\"[\\-0-9.]+\"|null)")
                                                  .matcher(cruiseDoc.toString());

        Elements rows = partialDoc.getElementById("cruiseItinerary").select("tr");

        for (Element e : rows) {
          CruiseStopVO cruiseItinerary = new CruiseStopVO();

          List<Element> cruiseItineraryDetails = e.getElementsByTag("td");
          if (cruiseItineraryDetails != null && cruiseItineraryDetails.size() > 4) {
            if (e.className() != null && e.className().contains("POI-green")){
              cruiseItinerary.setCruiseStop(false);
            }
            cruiseItinerary.setCruiseId(cruiseId);
            cruiseItinerary.setDay(Integer.parseInt(cruiseItineraryDetails.get(0).text()));
            cruiseItinerary.setName(StringEscapeUtils.escapeHtml4(cruiseItineraryDetails.get(2).text()));
            cruiseItinerary.setStartTimestamp(getTimestamp(cruiseItineraryDetails.get(1).text(),
                                                           cruiseItineraryDetails.get(3).text()));
            cruiseItinerary.setEndTimestamp(getTimestamp(cruiseItineraryDetails.get(1).text(),
                                                         cruiseItineraryDetails.get(4).text()));
            if (cruiseItineraryLatitude.find()) {
              if (cruiseItineraryLatitude.group().contains("null"))
                cruiseItinerary.setLatitude(0);
              else
                cruiseItinerary.setLatitude(Float.parseFloat(cruiseItineraryLatitude.group()
                                                                                    .substring(cruiseItineraryLatitude.group()
                                                                                                                      .indexOf(
                                                                                                                          ":") + 2,
                                                                                               cruiseItineraryLatitude.group()
                                                                                                                      .length() - 1)));

            }
            if (cruiseItineraryLongitude.find()) {
              if (cruiseItineraryLongitude.group().contains("null"))
                cruiseItinerary.setLongitude(0);
              else
                cruiseItinerary.setLongitude(Float.parseFloat(cruiseItineraryLongitude.group()
                                                                                      .substring(
                                                                                          cruiseItineraryLongitude
                                                                                              .group().indexOf(":") + 2,
                                                                                          cruiseItineraryLongitude
                                                                                              .group().length() - 1)));
            }

            cruiseItineraries.add(cruiseItinerary);
          }
        }*/

        //cruiseTrip.setCruiseStops(cruiseItineraries);
        long t4 = sw.getTime();

        //Log.debug("CruiseDownload - " + cruiseId + "T0: " + t0 +" T1: " + t1 + " T2: " + t2 + " T3: " + t3 + " T4: " + t4);
      }




    }
    catch(Exception e) {
      e.printStackTrace();
    }

    return cruiseTrip;
  }

  public static List<CruiseVO> getCruiseFromXML(String filename, String inFromDate, String inToDate) {
    List<CruiseVO> cruiseVOList = new ArrayList<>();

    try {

      String path = FilenameUtils.concat("virtuoso/Cruise/V5_0/", filename);
      S3Object s3Obj = S3Util.getS3File(S3Util.getPDFBucketName(), path);
      JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);

      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

      Root root = (Root) jaxbUnmarshaller.unmarshal(s3Obj.getObjectContent());

      //For Local Files
      /*File file = new File("/Users/george/Virtuoso Files/"+filename);
      JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      Root root = (Root) jaxbUnmarshaller.unmarshal(file);*/

      long fromDate = 0L;
      long toDate = 0L;

      if (inFromDate != null & inFromDate.trim().length() > 1) {
        fromDate = Utils.getMilliSecs(inFromDate);
      }
      if (inToDate != null & inToDate.trim().length() > 1) {
        toDate = Utils.getMilliSecs(inToDate);
      }


      if (root != null) {

        for (Root.Cruise.CruiseInfo cruiseInfo : root.getCruise().getCruiseInfo()) {
          long cruiseStartDate = Utils.getMilliSecs(cruiseInfo.getTravelStartDate());
          if (fromDate == 0L && toDate == 0L) {
            CruiseVO cruiseTrip = buildCruiseVO5(cruiseInfo);
            cruiseVOList.add(cruiseTrip);
          } else {
            if ((fromDate > 0L && toDate > 0L) && (fromDate <= cruiseStartDate && toDate >= cruiseStartDate)) {
              CruiseVO cruiseTrip = buildCruiseVO5(cruiseInfo);
              cruiseVOList.add(cruiseTrip);
            } else if ((fromDate > 0L && toDate == 0L) && (fromDate <= cruiseStartDate)) {
              CruiseVO cruiseTrip = buildCruiseVO5(cruiseInfo);
              cruiseVOList.add(cruiseTrip);
            } else if ((fromDate == 0L && toDate > 0L) && (toDate >= cruiseStartDate)) {
              CruiseVO cruiseTrip = buildCruiseVO5(cruiseInfo);
              cruiseVOList.add(cruiseTrip);
            }
          }

        }

      }

    }
    catch(Exception e) {
      e.printStackTrace();
    }

    return cruiseVOList;
  }

  /*private static CruiseVO buildCruiseVO4(Root.Cruise.CruiseInfo cruiseInfo) {
    CruiseVO cruiseVO = new CruiseVO();
    CruiseShipVO cruiseShipVO = new CruiseShipVO();

    CruiseVO cruiseTrip =  new CruiseVO();
    CruiseShipVO cruise =  new CruiseShipVO();

    cruise.setName(cruiseInfo.getShipName());
    cruise.setCompany(cruiseInfo.getCompanyName());

    HashMap<String, String> cruisePhotos = new HashMap<>();
    for (Root.Cruise.CruiseInfo.ProductImages e : cruiseInfo.getProductImages()) {
      for (Root.Cruise.CruiseInfo.ProductImages.Image image : e.getImage()) {
        String img = image.getProductImageURL();
        String imgName = img.substring(img.lastIndexOf("/") + 1,img.length());
        cruisePhotos.put(img, imgName);

      }
    }
    cruise.setCruisePhotos(cruisePhotos);

    cruiseTrip.setCruiseShip(cruise);
    cruiseTrip.setCruiseName(cruiseInfo.getTitle());


    cruiseTrip.setStartDate(getTimestamp(cruiseInfo.getTravelStartDate(), "00:00"));
    cruiseTrip.setEndDate(getTimestamp(cruiseInfo.getTravelEndDate(), "00:00"));
    cruiseTrip.setDuration(cruiseInfo.getLengthDays());


    cruiseTrip.setCruiseId(cruiseInfo.getMasterEntityID());

    Map<Timestamp, List<CruiseStopVO>> chronologicalCruiseStops = new TreeMap<>();
    ArrayList<CruiseStopVO> cruiseItineraries = new ArrayList<>();
    String lastStartDate = "";
    String lastEndDate = "";
    String dayTracker = "";
    int dayCounter = 1;



    for (int i = 0; i < cruiseInfo.getItinerary().getItineraryStop().size(); i++) {
      Root.Cruise.CruiseInfo.Itinerary.ItineraryStop stop = cruiseInfo.getItinerary().getItineraryStop().get(i);
      CruiseStopVO cruiseItinerary = new CruiseStopVO();

      cruiseItinerary.setCruiseStop(true);
      cruiseItinerary.setCruiseId(cruiseInfo.getMasterEntityID());

            if (!stop.getSegmentDate().equals(dayTracker)) {
              dayCounter++;
              dayTracker = stop.getSegmentDate();
            }
            cruiseItinerary.setDay(dayCounter);

      cruiseItinerary.setName(stop.getItineraryLocation());

      String arriveTime = "00:00";
      if (stop.getItineraryArriveTime() != null) {
        arriveTime= stop.getItineraryArriveTime();
      }
      cruiseItinerary.setStartTimestamp(getTimestamp(stop.getSegmentDate(), arriveTime));


      String departTime = "00:00";
      if (stop.getItineraryDepartTime() != null) {
        departTime= stop.getItineraryDepartTime();
      }

      cruiseItinerary.setEndTimestamp(getTimestamp(stop.getSegmentDate(), departTime));


      if (cruiseItinerary.getStartTimestamp() != null) {
        if (cruiseItinerary.getName().contains("Pre-")) {
          cruiseItinerary.setName(cruiseItinerary.getName().substring(0, cruiseItinerary.getName().indexOf("Pre-")));
        }
        if (cruiseItinerary.getName().contains("Virtuoso")) {
          cruiseItinerary.setName(cruiseItinerary.getName().substring(0, cruiseItinerary.getName().indexOf("Virtuoso")));
        }
        if (cruiseItinerary.getName().contains("Post-")) {
          cruiseItinerary.setName(cruiseItinerary.getName().substring(0, cruiseItinerary.getName().indexOf("Post-")));
        }
        if (cruiseItinerary.getName().contains("Shore Exc")) {
          cruiseItinerary.setName(cruiseItinerary.getName().substring(0, cruiseItinerary.getName().indexOf("Shore Exc")));
        }

        //cruiseItineraries.add(cruiseItinerary);
        List<CruiseStopVO> temp = chronologicalCruiseStops.get(cruiseItinerary.getStartTimestamp());
        if (temp == null || temp.size() < 1) {
          temp = new ArrayList<>();
          temp.add(cruiseItinerary);
        } else {
          temp.add(cruiseItinerary);
        }
        chronologicalCruiseStops.put(cruiseItinerary.getStartTimestamp(), temp);
      }
    }

    int index = 1;
    for (Timestamp key : chronologicalCruiseStops.keySet()) {
      for (CruiseStopVO cStop : chronologicalCruiseStops.get(key)) {
        cStop.setDay(index);
        cruiseItineraries.add(cStop);
      }
      index++;
    }

    cruiseTrip.setStartLocation(cruiseItineraries.get(0).getName());
    cruiseTrip.setEndLocation(cruiseItineraries.get(cruiseItineraries.size() - 1).getName());


    cruiseTrip.setCruiseStops(cruiseItineraries);

    return cruiseTrip;
  }*/

  private static CruiseVO buildCruiseVO5(Root.Cruise.CruiseInfo cruiseInfo) {
    CruiseVO cruiseVO = new CruiseVO();
    CruiseShipVO cruiseShipVO = new CruiseShipVO();

    CruiseVO cruiseTrip =  new CruiseVO();
    CruiseShipVO cruise =  new CruiseShipVO();

    cruise.setName(cruiseInfo.getShipName());
    cruise.setCompany(cruiseInfo.getCompanyName());

    HashMap<String, String> cruisePhotos = new HashMap<>();
    for (Root.Cruise.CruiseInfo.ProductImages e : cruiseInfo.getProductImages()) {
      for (Root.Cruise.CruiseInfo.ProductImages.Image image : e.getImage()) {
        for (Root.Cruise.CruiseInfo.ProductImages.Image.ProductImageURL imageURL : image.getProductImageURL()) {
          String img = imageURL.getValue();
          String imgName = img.substring(img.lastIndexOf("/") + 1, img.length());
          cruisePhotos.put(img, imgName);
        }

      }
    }
    cruise.setCruisePhotos(cruisePhotos);

    cruiseTrip.setCruiseShip(cruise);
    cruiseTrip.setCruiseName(cruiseInfo.getTitle());
    cruiseTrip.setVirtuosoUrl(cruiseInfo.getVirtuosoDetailsUrl());

    cruiseTrip.setStartDate(getTimestamp(cruiseInfo.getTravelStartDate(), "00:00"));
    cruiseTrip.setEndDate(getTimestamp(cruiseInfo.getTravelEndDate(), "00:00"));
    cruiseTrip.setDuration(cruiseInfo.getLengthDays());
    cruiseTrip.setTermsAndCondition(StringEscapeUtils.unescapeHtml4(cruiseInfo.getTermsAndConditions()));
    cruiseTrip.setBookingInstructions(StringEscapeUtils.unescapeHtml4(cruiseInfo.getBookingInstructions()));
    cruiseTrip.setNote(StringEscapeUtils.unescapeHtml4(cruiseInfo.getAllGuestsReceiveNote()));

    List<Benefit> benefitList = new ArrayList<>();
    List<Promotion> promotions = new ArrayList<>();

    for (Root.Cruise.CruiseInfo.Promotions promos : cruiseInfo.getPromotions()) {
      for (Root.Cruise.CruiseInfo.Promotions.Promotion promo : promos.getPromotion()) {
        Promotion promotion = new Promotion();
        promotion.setPromotionUrl(promo.getPromotionUrl());
        promotion.setName(StringEscapeUtils.unescapeHtml4(promo.getName()));
        promotion.setCountries(promo.getCountries());
        promotion.setDescription(StringEscapeUtils.unescapeHtml4(promo.getDescription()));
        promotions.add(promotion);
      }

    }
    cruiseTrip.setPromotions(promotions);


    for (Root.Cruise.CruiseInfo.VirtuosoVoyage voyage : cruiseInfo.getVirtuosoVoyage()) {
      for (Root.Cruise.CruiseInfo.VirtuosoVoyage.NonHosted nonHosted : voyage.getNonHosted()) {
        for (AllGuestsReceive allGuestsReceive : nonHosted.getAllGuestsReceive()) {
          for (com.mapped.publisher.parse.virtuoso.cruise.v5_0.Benefit bnft : allGuestsReceive.getBenefit()) {
            Benefit benefit = buildBenefit(bnft, false, true);
            benefitList.add(benefit);
          }
        }
      }

      for (Root.Cruise.CruiseInfo.VirtuosoVoyage.Hosted hosted : voyage.getHosted()) {
        for (AllGuestsReceive allGuestsReceive : hosted.getAllGuestsReceive()) {
          for (com.mapped.publisher.parse.virtuoso.cruise.v5_0.Benefit bnft : allGuestsReceive.getBenefit()) {
            Benefit benefit = buildBenefit(bnft, true, true);
            benefitList.add(benefit);
          }
        }

        for (Root.Cruise.CruiseInfo.VirtuosoVoyage.Hosted.BenefitGroups bGroups : hosted.getBenefitGroups()) {
          for (Root.Cruise.CruiseInfo.VirtuosoVoyage.Hosted.BenefitGroups.BenefitGroup bGroup : bGroups.getBenefitGroup()) {
            for (Root.Cruise.CruiseInfo.VirtuosoVoyage.Hosted.BenefitGroups.BenefitGroup.Benefits bnfts : bGroup.getBenefits()) {
              for (com.mapped.publisher.parse.virtuoso.cruise.v5_0.Benefit bnft : bnfts.getBenefit()) {
                Benefit benefit = buildBenefit(bnft, true, false);
                benefitList.add(benefit);
              }
            }
          }
        }
      }
    }

    cruiseTrip.setBenefits(benefitList);


    cruiseTrip.setCruiseId(cruiseInfo.getMasterEntityID());

    Map<Timestamp, List<CruiseStopVO>> chronologicalCruiseStops = new TreeMap<>();
    ArrayList<CruiseStopVO> cruiseItineraries = new ArrayList<>();
    String lastStartDate = "";
    String lastEndDate = "";
    String dayTracker = "";
    int dayCounter = 1;



    for (int i = 0; i < cruiseInfo.getItinerary().getItineraryStop().size(); i++) {
      Root.Cruise.CruiseInfo.Itinerary.ItineraryStop stop = cruiseInfo.getItinerary().getItineraryStop().get(i);
      CruiseStopVO cruiseItinerary = new CruiseStopVO();

      cruiseItinerary.setCruiseStop(true);
      cruiseItinerary.setCruiseId(cruiseInfo.getMasterEntityID());

            /*if (!stop.getSegmentDate().equals(dayTracker)) {
              dayCounter++;
              dayTracker = stop.getSegmentDate();
            }
            cruiseItinerary.setDay(dayCounter);*/

      cruiseItinerary.setName(stop.getItineraryLocation());

      String arriveTime = "00:00";

      cruiseItinerary.setStartTimestamp(getTimestamp(stop.getSegmentDate(), arriveTime));


      String departTime = "00:00";
      if (stop.getItineraryDepartTime() != null) {
        departTime= stop.getItineraryDepartTime();
      }

      cruiseItinerary.setEndTimestamp(getTimestamp(stop.getSegmentDate(), departTime));

      List<Benefit> stopsBenefits = new ArrayList<>();
      for (Root.Cruise.CruiseInfo.Itinerary.ItineraryStop.VirtuosoVoyageBenefits voyage : stop.getVirtuosoVoyageBenefits()) {
        for (com.mapped.publisher.parse.virtuoso.cruise.v5_0.Benefit bnft : voyage.getBenefit()) {
          Benefit benefit = buildBenefit(bnft, false, true);
          stopsBenefits.add(benefit);
        }
      }
      cruiseItinerary.setBenefits(stopsBenefits);


      if (cruiseItinerary.getStartTimestamp() != null) {
        if (cruiseItinerary.getName().contains("Pre-")) {
          cruiseItinerary.setName(cruiseItinerary.getName().substring(0, cruiseItinerary.getName().indexOf("Pre-")));
        }
        if (cruiseItinerary.getName().contains("Virtuoso")) {
          cruiseItinerary.setName(cruiseItinerary.getName().substring(0, cruiseItinerary.getName().indexOf("Virtuoso")));
        }
        if (cruiseItinerary.getName().contains("Post-")) {
          cruiseItinerary.setName(cruiseItinerary.getName().substring(0, cruiseItinerary.getName().indexOf("Post-")));
        }
        if (cruiseItinerary.getName().contains("Shore Exc")) {
          cruiseItinerary.setName(cruiseItinerary.getName().substring(0, cruiseItinerary.getName().indexOf("Shore Exc")));
        }

        //cruiseItineraries.add(cruiseItinerary);
        List<CruiseStopVO> temp = chronologicalCruiseStops.get(cruiseItinerary.getStartTimestamp());
        if (temp == null || temp.size() < 1) {
          temp = new ArrayList<>();
          temp.add(cruiseItinerary);
        } else {
          temp.add(cruiseItinerary);
        }
        chronologicalCruiseStops.put(cruiseItinerary.getStartTimestamp(), temp);
      }
    }

    int index = 1;
    for (Timestamp key : chronologicalCruiseStops.keySet()) {
      for (CruiseStopVO cStop : chronologicalCruiseStops.get(key)) {
        cStop.setDay(index);
        cruiseItineraries.add(cStop);
      }
      index++;
    }

    cruiseTrip.setStartLocation(cruiseItineraries.get(0).getName());
    cruiseTrip.setEndLocation(cruiseItineraries.get(cruiseItineraries.size() - 1).getName());


    cruiseTrip.setCruiseStops(cruiseItineraries);

    return cruiseTrip;
  }

  private static Benefit buildBenefit(com.mapped.publisher.parse.virtuoso.cruise.v5_0.Benefit bnft, boolean isHosted, boolean forAllGuests) {
    Benefit benefit = new Benefit();
    benefit.setHosted(isHosted);
    benefit.setForAllGuests(forAllGuests);
    benefit.setBenefitName(StringEscapeUtils.unescapeHtml4(bnft.getBenefitName()));
    benefit.setCountries(bnft.getCountries());
    benefit.setBenefitDate(bnft.getBenefitDate());
    benefit.setBenefitDescription(StringEscapeUtils.unescapeHtml4(bnft.getBenefitDescription()));
    benefit.setIsWheelchairAccessible(bnft.getIsWheelchairAccessible());
    benefit.setBenefitLocation(bnft.getBenefitLocation());
    benefit.setMealsBitMask(bnft.getMealsBitMask());
    benefit.setProductLengthMinutes(bnft.getProductLengthMinutes());

    return benefit;
  }

  private static  Timestamp getTimestamp (String date, String time) {
    try {
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime() );
      return t;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public static void main (String[] args) {
    try {
//      String s = FileUtils.readFileToString(new File("/Volumes/data2/temp/virtuoso.html"));
      CruiseVO cruise = getCruise("http://www.virtuoso.com/cruises/14654714/denali-explorer");
      System.out.println(cruise.toJson());
//      long pk = cruise.save();
//      System.out.println("PK: " + pk);
    } catch (Exception e) {

    }
  }

}
