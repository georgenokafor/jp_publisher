package com.umapped.api.schema.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;
import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL) @Generated("org.jsonschema2pojo")
@JsonPropertyOrder({"latitude", "longitude"})
public class Coordinates
    implements Serializable {

  @JsonProperty("latitude")
  private Float latitude;
  @JsonProperty("longitude")
  private Float longitude;

  public Coordinates() {
  }

  public Coordinates(float longitude, float latitude) {
    this.latitude = latitude;
    this.longitude = longitude;
  }


  @Override
  public int hashCode() {
    int hash = 1;
    if (latitude != null) {
      hash *= latitude;
    }
    if (longitude != null) {
      hash *= longitude;
    }

    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if ((obj == null) || (obj.getClass() != this.getClass())) {
      return false;
    }

    Coordinates o = (Coordinates) obj;

    return latitude == o.latitude && longitude == o.longitude;
  }

  /**
   * @return The latitude
   */
  @JsonProperty("latitude")
  public Float
  getLatitude() {
    return latitude;
  }

  /**
   * @param latitude The latitude
   */
  @JsonProperty("latitude")
  public void setLatitude(Float latitude) {
    this.latitude = latitude;
  }

  public Coordinates withLatitude(Float latitude) {
    this.latitude = latitude;
    return this;
  }

  /**
   * @return The longitude
   */
  @JsonProperty("longitude")
  public Float getLongitude() {
    return longitude;
  }

  /**
   * @param longitude The longitude
   */
  @JsonProperty("longitude")
  public void setLongitude(Float longitude) {
    this.longitude = longitude;
  }

  public Coordinates withLongitude(Float longitude) {
    this.longitude = longitude;
    return this;
  }
}
