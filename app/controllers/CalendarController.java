package controllers;


import com.google.inject.Inject;
import com.mapped.common.CoreConstants;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.utils.*;
import com.mapped.publisher.view.*;
import com.umapped.itinerary.analyze.model.Itinerary;
import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.segment.TripSegment;
import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;

import com.umapped.itinerary.analyze.segment.TripSegmentAnalyzeResult;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.activity.UmActivity;
import com.umapped.service.offer.OfferService;
import models.publisher.*;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.*;
import net.fortuna.ical4j.model.Calendar;

import play.mvc.Controller;
import play.mvc.Result;
import play.twirl.api.Html;

import java.net.URI;
import java.time.ZoneId;
import java.util.*;


/**
 * Created by george on 2016-07-14.
 */
public class CalendarController extends Controller {
  @Inject
  OfferService offerService;


  public Result createCalendarByTripId(String tripId){
    List<TripBookingView> tripBookingViewList = new ArrayList<>();
    TripBookingView tripBookingView = new TripBookingView();

    List<TripBookingDetailView> tripBookingDetailViews = new ArrayList<>();
    Trip trip = Trip.findByPK(tripId);

    List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);


    if (tripDetails == null || tripDetails.size() == 0) {
      BaseView view1 = new BaseView();
      view1.message = "No Bookings/Documents/Attachments added.";
      return ok(views.html.common.message.render(view1));
    }

    //get all the segments
    ItineraryAnalyzeResult analyzeResult = offerService.analyzeTrip(trip.tripid);
    //process all the bookings
    Calendar calendar = new Calendar();
    calendar.getProperties().add(new ProdId("-//Umapped//iCal4j 1.0//EN"));
    calendar.getProperties().add(Version.VERSION_2_0);
    calendar.getProperties().add(CalScale.GREGORIAN);

    if (tripDetails != null && tripDetails.size() > 0) {
      for (TripDetail tripDetail : tripDetails) {
        TripBookingDetailView bookingDetails = BookingController.buildBookingView(tripId, tripDetail, true);
        if (bookingDetails != null && bookingDetails.startDateMs > 0 && bookingDetails.name != null) {
          calendar.getComponents().add(getIcalEvent(bookingDetails, analyzeResult));


        }
      }
    }

    response().setContentType("text/calendar; charset=utf-8");
    response().setHeader("Content-disposition", "attachment; filename="+ trip.getName().replaceAll("[^a-zA-Z0-9.-]", "_") + ".ics");
    response().setHeader("Cache-Control", "no-cache, no-store,max-age = 0, must-revalidate"); // HTTP 1.1.
    response().setHeader("Pragma", "no-cache"); // HTTP 1.0.
    response().setHeader("Expires", "0"); // Proxies.

    return ok(calendar.toString());
  }

  public Result createCalendarByDetailId(String tripId, String detailId){
    List<TripBookingView> tripBookingViewList = new ArrayList<>();
    TripBookingView tripBookingView = new TripBookingView();

    List<TripBookingDetailView> tripBookingDetailViews = new ArrayList<>();
    Trip trip = Trip.findByPK(tripId);

    TripDetail td = TripDetail.find.byId(detailId);


    if (trip == null || td == null || !td.tripid.equals(trip.tripid)) {
      BaseView view1 = new BaseView();
      view1.message = "No Bookings/Documents/Attachments added.";
      return ok(views.html.common.message.render(view1));
    }
    ItineraryAnalyzeResult analyzeResult = offerService.analyzeTrip(trip.tripid);

    //process all the bookings
    Calendar calendar = new Calendar();
    calendar.getProperties().add(new ProdId("-//Umapped//iCal4j 1.0//EN"));
    calendar.getProperties().add(Version.VERSION_2_0);
    calendar.getProperties().add(CalScale.GREGORIAN);

    String title = "Calendar";
    TripBookingDetailView bookingDetails = BookingController.buildBookingView(tripId, td, true);
    if (bookingDetails != null && bookingDetails.startDateMs > 0 && bookingDetails.name != null) {
      title = bookingDetails.name;
      VEvent vEvent = getIcalEvent(bookingDetails, analyzeResult);
      if (vEvent != null) {
        title = vEvent.getSummary().getValue();
        calendar.getComponents().add(vEvent);
      }
    }



    response().setContentType("text/calendar; charset=utf-8");
    response().setHeader("Content-disposition", "attachment; filename=" + title.replaceAll("[^a-zA-Z0-9.-]", "_") +".ics");
    response().setHeader("Cache-Control", "no-cache, no-store,max-age = 0, must-revalidate"); // HTTP 1.1.
    response().setHeader("Pragma", "no-cache"); // HTTP 1.0.
    response().setHeader("Expires", "0"); // Proxies.

    return ok(calendar.toString());
  }


  public Result createCalendarByUserId(String userId){

    List<TripBookingView> tripBookingViewList = new ArrayList<>();

    List<AccountTripLink> accountTripLinkList = AccountTripLink.findAllByUid(Long.parseLong(userId));
    //process all the bookings
    Calendar calendar = new Calendar();
    calendar.getProperties().add(new ProdId("-//Umapped//iCal4j 1.0//EN"));
    calendar.getProperties().add(Version.VERSION_2_0);
    calendar.getProperties().add(CalScale.GREGORIAN);

    for (AccountTripLink accountTripLink : accountTripLinkList) {
      TripBookingView tripBookingView = new TripBookingView();

      Trip trip = Trip.findByPK(accountTripLink.getPk().getTripid());

      List<TripDetail> tripDetails = TripDetail.findActiveByTripId(trip.tripid);


      if (tripDetails == null || tripDetails.size() == 0) {
        BaseView view1 = new BaseView();
        view1.message = "No Bookings/Documents/Attachments added.";
        return ok(views.html.common.message.render(view1));
      }


      //process all the bookings
      if (tripDetails != null && tripDetails.size() > 0) {
        ItineraryAnalyzeResult analyzeResult = offerService.analyzeTrip(trip.tripid);
        for (TripDetail tripDetail : tripDetails) {
          TripBookingDetailView bookingDetails = BookingController.buildBookingView(tripDetail.getTripid(), tripDetail, true);
          if (bookingDetails != null && bookingDetails.startDateMs > 0 && bookingDetails.name != null) {
            calendar.getComponents().add(getIcalEvent(bookingDetails, analyzeResult));


          }
        }

      }
    }


    response().setContentType("text/calendar; charset=utf-8");
    response().setHeader("Content-disposition", "attachment; filename=calendar.ics");
    response().setHeader("Cache-Control", "no-cache, no-store,max-age = 0, must-revalidate"); // HTTP 1.1.
    response().setHeader("Pragma", "no-cache"); // HTTP 1.0.
    response().setHeader("Expires", "0"); // Proxies.

    return ok(calendar.toString());
  }


  public VEvent getIcalEvent (TripBookingDetailView detailView, ItineraryAnalyzeResult analyzeResult) {
    //TODO: convert times to UTC timezone from local timezone
    //net.fortuna.ical4j.model.TimeZoneRegistry registry = net.fortuna.ical4j.model.TimeZoneRegistryFactory.getInstance().createRegistry();
    //use an outlook compatible timezone registry
    net.fortuna.ical4j.model.TimeZoneRegistry registry = new net.fortuna.ical4j.model.TimeZoneRegistryImpl("zoneinfo-outlook/");


    ZoneId startTZ = getStartTZ(detailView, analyzeResult);
    ZoneId endTZ = getEndTZ(detailView, analyzeResult);
    if (endTZ == null) {
      endTZ = startTZ;
    }
    if (startTZ == null && endTZ != null) {
      startTZ = endTZ;
    }

    VEvent event = null;
    net.fortuna.ical4j.model.TimeZone timezone1 = null;
    if (startTZ != null) {
      timezone1 = registry.getTimeZone(startTZ.toString());
    }
    net.fortuna.ical4j.model.DateTime start = null;
    if (timezone1 != null) {
      try {
        String startTime = Utils.formatTimestamp(detailView.startDateMs, "yyyyMMdd'T'HHmmss");
        start = new net.fortuna.ical4j.model.DateTime(startTime, timezone1);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    if (start == null) {
      start = new net.fortuna.ical4j.model.DateTime(detailView.startDateMs);
    }



    StringBuilder sb = new StringBuilder();
    StringBuilder timeDesc = new StringBuilder();
    timeDesc.append("Please review times carefully as they may be different depending on your calendar’s time zone settings. Local times are shown below (where applicable):\n");

    switch (detailView.detailsTypeId) {
      case FLIGHT:
        sb.append("Flight: ");
        sb.append(detailView.name);
        if (detailView.poiStart != null) {
          sb.append(" from ");
          sb.append(detailView.poiStart.getName());
        } else if (detailView.departureAirport != null && !detailView.departureAirport.isEmpty()) {
          sb.append(" from ");
          sb.append(detailView.departureAirport);
        }
        if (detailView.poiFinish != null) {
          sb.append(" to ");
          sb.append(detailView.poiFinish.getName());
        } else if (detailView.arrivalAirport != null && !detailView.arrivalAirport.isEmpty()) {
          sb.append(" to ");
          sb.append(detailView.arrivalAirport);
        }
        sb.append("\nDepart: ");
        sb.append(getTS(detailView.startDateMs));
        if (detailView.endDateMs > 0 && detailView.endDateMs != detailView.startDateMs) {
          sb.append("\nArrive: ");
          sb.append(getTS(detailView.endDateMs));
        }
        timeDesc.append(sb.toString());

        break;
      case HOTEL:
        sb.append("Hotel: ");
        sb.append(detailView.name);
        sb.append("\nCheck-in: ");
        sb.append(getTS(detailView.startDateMs));
        if (detailView.endDateMs > 0 && detailView.endDateMs != detailView.startDateMs) {
          sb.append("\nCheck-out: ");
          sb.append(getTS(detailView.endDateMs));
        }
        timeDesc.append(sb.toString());

        break;
      case CRUISE:
        sb.append("Cruise: ");
        sb.append(detailView.name);
        if (detailView.poiStart != null) {
          sb.append(" from ");
          sb.append(detailView.poiStart.getName());
        } else if (detailView.locStartName != null && !detailView.locStartName.isEmpty()) {
          sb.append(" from ");
          sb.append(detailView.locStartName);
        }
        sb.append("\nDepart: ");
        sb.append(getTS(detailView.startDateMs));
        timeDesc.append(sb.toString());

        break;
      case CAR_RENTAL:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");

        } else {
          sb.append("Car Rental: ");
        }
        sb.append(detailView.name);
        sb.append("\nPick-up: ");
        sb.append(getTS(detailView.startDateMs));
        if (detailView.endDateMs > 0 && detailView.endDateMs != detailView.startDateMs) {
          sb.append("\nDrop Off: ");
          sb.append(getTS(detailView.endDateMs));
        }
        timeDesc.append(sb.toString());
        break;
      case CAR_DROPOFF:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");

        } else {
          sb.append("Car Rental Drop-off: ");
        }
        sb.append(detailView.name);
        sb.append("\nDrop-off:");
        sb.append(getTS(detailView.startDateMs));
        timeDesc.append(sb.toString());

        break;
      case FERRY:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");

        } else {
          sb.append("Ferry: ");
        }
        sb.append(detailView.name);
        sb.append("\nDepart: ");
        sb.append(getTS(detailView.startDateMs));
        timeDesc.append(sb.toString());

        break;
      case RAIL:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");

        } else {
          sb.append("Train: ");
        }
        sb.append(detailView.name);
        if (detailView.poiStart != null) {
          sb.append(" from ");
          sb.append(detailView.poiStart.getName());
        } else if (detailView.locStartName != null && !detailView.locStartName.isEmpty()) {
          sb.append(" from ");
          sb.append(detailView.locStartName);
        }
        sb.append("\nDepart: ");
        sb.append(getTS(detailView.startDateMs));
        if (detailView.endDateMs > 0 && detailView.endDateMs != detailView.startDateMs) {
          sb.append("\nArrive: ");
          sb.append(getTS(detailView.endDateMs));
        }
        timeDesc.append(sb.toString());

        break;
      case PRIVATE_TRANSFER:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");

        } else {
          sb.append("Private Transfer: ");
        }
        sb.append(detailView.name);
        sb.append("\nPick-up: ");
        sb.append(getTS(detailView.startDateMs));
        timeDesc.append(sb.toString());

        break;
      case TAXI:
      case BUS:
      case PUBLIC_TRANSPORT:
      case TRANSPORT:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");
        } else {
          sb.append("Transfer: ");
        }
        sb.append(detailView.name);
        sb.append("\nPick-up: ");
        sb.append(getTS(detailView.startDateMs));
        timeDesc.append(sb.toString());

        break;
      case CRUISE_STOP:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");

        } else {
          sb.append("Cruise Stop: ");
        }
        sb.append(detailView.name);
        sb.append("\nArrive: ");
        sb.append(getTS(detailView.startDateMs));
        timeDesc.append(sb.toString());

        break;
      case EVENT:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");

        } else {
          sb.append("Event: ");
        }
        sb.append(detailView.name);
        sb.append("\nDate: ");
        sb.append(getTS(detailView.startDateMs));
        timeDesc.append(sb.toString());

        break;
      case RESTAURANT:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");

        } else {
          sb.append("Restaurant Reservation: ");
        }
        sb.append(detailView.name);
        sb.append("\nDate: ");
        sb.append(getTS(detailView.startDateMs));
        timeDesc.append(sb.toString());

        break;
      case SKI_LESSONS:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");

        } else {
          sb.append("Ski Lesson: ");
        }
        sb.append(detailView.name);
        sb.append("\nDate: ");
        sb.append(getTS(detailView.startDateMs));
        timeDesc.append(sb.toString());

        break;
      case SKI_LIFT:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");

        } else {
          sb.append("Lift Ticket: ");
        }
        sb.append(detailView.name);
        sb.append("\nDate: ");
        sb.append(getTS(detailView.startDateMs));
        timeDesc.append(sb.toString());

        break;
      case SKI_RENTALS:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");

        } else {
          sb.append("Ski Rentals: ");
        }
        sb.append(detailView.name);
        sb.append("\nDate: ");
        sb.append(getTS(detailView.startDateMs));
        timeDesc.append(sb.toString());

        break;
      case SHORE_EXCURSION:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");

        } else {
          sb.append("Shore Excursion: ");
        }
        sb.append(detailView.name);
        sb.append("\nDate: ");
        sb.append(getTS(detailView.startDateMs));
        timeDesc.append(sb.toString());

        break;
      case TOUR:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");

        } else {
          sb.append("Tour: ");
        }
        sb.append(detailView.name);
        sb.append("\nDate: ");
        sb.append(getTS(detailView.startDateMs));
        timeDesc.append(sb.toString());
        break;
      case ACTIVITY:
        if (detailView.getServiceType() != null && !detailView.getServiceType().isEmpty()) {
          sb.append(detailView.getServiceType());
          sb.append(": ");

        } else {
          sb.append("Activity: ");
        }
        sb.append(detailView.name);
        sb.append("\nDate: ");
        sb.append(getTS(detailView.startDateMs));
        timeDesc.append(sb.toString());

        break;
    }


    if (detailView.endDateMs > 0) {
      net.fortuna.ical4j.model.DateTime end = null;
      timezone1 = null;
      if (endTZ != null) {
        timezone1 = registry.getTimeZone(endTZ.toString());
      }
      if (timezone1 != null) {
        try {
          String endTime = Utils.formatTimestamp(detailView.endDateMs, "yyyyMMdd'T'HHmmss");
          end = new net.fortuna.ical4j.model.DateTime(endTime, timezone1);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      if (end == null) {
        end = new net.fortuna.ical4j.model.DateTime(detailView.endDateMs);
      }



      event = new VEvent(start, end, sb.toString());
    } else {
      event = new VEvent(start, sb.toString());
    }

    Uid uid = new Uid(detailView.detailsId);
    event.getProperties().add(uid);

    try {
      URI uri = new URI(ConfigMgr.getAppParameter(CoreConstants.HOST_URL) + routes.WebItineraryController.index(
          detailView.tripId,
          null,
          0));
      Url url = new Url();
      url.setUri(uri);
      event.getProperties().add(url);
    } catch (Exception e) {
    }

    StringBuilder description = new StringBuilder();
    description.append(timeDesc);
    description.append("\n");

    if (detailView.detailsTypeId != ReservationType.FLIGHT) {
      if (detailView.poiMain != null ) {
        if (detailView.poiMain.getMainAddress() != null){
          description.append("Address: ");
          description.append(detailView.poiMain.getMainAddressString());
          description.append("\n");

          Location location = new Location();
          location.setValue(detailView.poiMain.getMainAddressString());
          event.getProperties().add(location);
        }
        if (detailView.poiMain.getMainPhone() != null) {
          description.append("Phone: ");
          description.append(detailView.poiMain.getMainPhone().getNumber());
          description.append("\n");
        }
        if (detailView.poiMain.getMainEmail() != null) {
          description.append("Email: ");
          description.append(detailView.poiMain.getMainEmail().getAddress());
          description.append("\n");
        }
        description.append("\n");
      }
      if (detailView.locStartName != null && !detailView.locStartName.isEmpty()) {
        if (detailView.detailsTypeId == ReservationType.CRUISE || detailView.detailsTypeId == ReservationType.RAIL) {
          description.append("Depart: ");
        } else if (detailView.detailsTypeId.getRootLevelType() == ReservationType.ROOT.TRANSPORT || detailView.detailsTypeId == ReservationType.TOUR) {
          description.append("Pickup: ");
        } else {
          description.append("Location: ");
        }
        if (!description.toString().contains(detailView.locStartName)) {
          description.append(detailView.locStartName);
        }
        description.append("\n");

        //add a location to the event
        Location location = new Location();
        location.setValue(detailView.locStartName);
        event.getProperties().add(location);
      }
      if (detailView.locFinishName != null && !detailView.locFinishName.isEmpty()) {
        if (detailView.detailsTypeId == ReservationType.CRUISE || detailView.detailsTypeId == ReservationType.RAIL) {
          description.append("Arrive: ");
        } else {
          description.append("Drop-Off: ");
        }
        description.append(detailView.locFinishName);
        description.append("\n");
      }
    }
    /*
    if (detailView.comments != null && !detailView.comments.isEmpty()) {
      description.append(detailView.origComments);
    }
    */
    if (!description.toString().isEmpty()) {
      Description desc = new Description(description.toString());
      event.getProperties().add(desc);
    }


    return event;

  }

  private ZoneId getStartTZ(TripBookingDetailView detailView, ItineraryAnalyzeResult analyzeResult) {
    if (detailView != null && analyzeResult != null) {
      int i = 0;
      for (TripSegmentAnalyzeResult result: analyzeResult.getSegmentResults()) {
        ItineraryItem item = getItineraryItem(detailView, result.getSegment());

        if (item != null && item.getEnhancedStartLocation() != null) {
          ZoneId tz = item.getEnhancedStartLocation().getTimezone();
          if (tz == null && result.getSegment() != null && result.getSegment().getStartLocation() != null) {
            tz = result.getSegment().getStartLocation().getTimezone();
          }
          if(item.getType() == ItineraryItemType.LongTransfer && tz == null) {
            if (i > 0) {
              tz = getEndTZ(analyzeResult.getSegmentResults().get(i - 1));
            }
          }
          return tz;
        }
        i++;
      }
    }
    return null;
  }

  private ZoneId getStartTZ(TripSegmentAnalyzeResult result) {
    if (result != null && result.getSegment() != null && result.getSegment().getStartLocation() != null && result.getSegment().getStartLocation().getTimezone() != null) {
      return result.getSegment().getStartLocation().getTimezone();
    } else if (result != null && result.getSegment() != null && result.getSegment().getEndLocation() != null && result.getSegment().getEndLocation().getTimezone() != null) {
      return result.getSegment().getEndLocation().getTimezone();
    }
    return null;
  }

  private ZoneId getEndTZ(TripSegmentAnalyzeResult result) {
    if (result != null && result.getSegment() != null && result.getSegment().getEndLocation() != null && result.getSegment().getEndLocation().getTimezone() != null) {
      return result.getSegment().getEndLocation().getTimezone();
    } else if (result != null && result.getSegment() != null && result.getSegment().getStartLocation() != null && result.getSegment().getStartLocation().getTimezone() != null) {
      return result.getSegment().getStartLocation().getTimezone();
    }
    return null;
  }

    private ZoneId getEndTZ(TripBookingDetailView detailView, ItineraryAnalyzeResult analyzeResult) {
    if (detailView != null && analyzeResult != null) {
      int i = 0;
      for (TripSegmentAnalyzeResult result: analyzeResult.getSegmentResults()) {
        ItineraryItem item = getItineraryItem(detailView, result.getSegment());
        if (item != null && item.getEnhancedEndLocation() != null) {
          ZoneId tz = item.getEnhancedEndLocation().getTimezone();
          if (tz == null && result.getSegment() != null && result.getSegment().getEndLocation() != null) {
            tz = result.getSegment().getEndLocation().getTimezone();
          }
          if (tz == null && result.getSegment() != null && result.getSegment().getStartLocation() != null) {
            tz = result.getSegment().getStartLocation().getTimezone();
          }
          if(item.getType() == ItineraryItemType.LongTransfer && tz == null) {
            if (i < (analyzeResult.getSegmentResults().size() - 1)) {
              tz = getStartTZ(analyzeResult.getSegmentResults().get(i + 1));
            }
          }

          return tz;
        }
        i++;
      }
    }
    return null;
  }

  private ItineraryItem getItineraryItem (TripBookingDetailView detailView, TripSegment segment) {
    if (segment.getArrive() != null && detailView.getDetailsId().equals(segment.getArrive().getUmId())) {
      return segment.getArrive();
    }
    if (segment.getDepart() != null && detailView.getDetailsId().equals(segment.getDepart().getUmId())) {
      return segment.getDepart();
    }
    if (segment.getMainItem() != null && detailView.getDetailsId().equals(segment.getMainItem().getUmId())) {
      return segment.getMainItem();
    }
    List<ItineraryItem> items = new ArrayList<>();
    switch (detailView.detailsTypeId) {
      case HOTEL:
        if (segment.getAccommodations() != null) {
          items.addAll(segment.getAccommodations());
        }
        break;
      case CAR_RENTAL:
      case CAR_DROPOFF:
      case FERRY:
      case RAIL:
      case PRIVATE_TRANSFER:
      case TAXI:
      case BUS:
      case PUBLIC_TRANSPORT:
      case TRANSPORT:
        if (segment.getTransfers() != null) {
          items.addAll(segment.getTransfers());
        }
        break;
      case CRUISE_STOP:
        if (segment.getCruiseStops() != null) {
          items.addAll(segment.getCruiseStops());
        }
        break;
      case EVENT:
      case RESTAURANT:
      case SKI_LESSONS:
      case SKI_LIFT:
      case SKI_RENTALS:
      case SHORE_EXCURSION:
      case TOUR:
      case ACTIVITY:
        if (segment.getActivities() != null) {
          items.addAll(segment.getActivities());
        }
        break;

    }
    for (ItineraryItem itineraryItem: items) {
      if (itineraryItem.getUmId().equals(detailView.getDetailsId())) {
        return itineraryItem;
      }
    }
    return null;
  }

  private String getTS (long timems) {
    String s = Utils.formatTimestamp(timems, "MMM dd YYY, HH:mm");
    if (s.contains(", 00:00")) {
      return s.replace(", 00:00", "");
    }
    return s;
  }
}
