package models.utils;

public class JsonObjectMappingException extends RuntimeException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public JsonObjectMappingException(String message, Throwable cause) {
    super(message, cause);
  }

  public JsonObjectMappingException(Throwable cause) {
    super(cause);
  }
}
