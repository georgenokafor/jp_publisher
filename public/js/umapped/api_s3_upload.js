(function () {

  window.APIS3Upload = (function () {
    /** UMapped API URL */
    APIS3Upload.prototype.api_request_endpoint = '';
    /** Identifier for body object type */
    APIS3Upload.prototype.api_request_type = '';
    APIS3Upload.prototype.upload_form = '';

    APIS3Upload.prototype.api_request_body_builder = function() {
      //console.error("UMapped API Request Builder was not provided");
      return null;
    };
    APIS3Upload.prototype.file_upload_success_callback = function (data) {
      //console.error('Callback function for successful upload was not provided', data);
      return null;
    };
    APIS3Upload.prototype.file_caption = '';
    APIS3Upload.prototype.file_dom_selector = '#file_upload';
    APIS3Upload.prototype.onProgress = function (percent, status) {
      //return console.log('base.onProgress()', percent, status);
    };

    APIS3Upload.prototype.onError = function (status) {
      //return console.log('base.onError()', status);
    };

    function APIS3Upload(options) {
      if (options == null) {
        options = {};
      }
      _.extend(this, options);
      var a = document.getElementById(this.file_dom_selector.replace('#',''));
      this.handleFileSelect(a);

    }

    APIS3Upload.prototype.handleFileSelect = function (file_element) {
      var f, files, _i, _len;
      this.onProgress(0, 'Upload started.');
      files = file_element.files;
      if (files != null && files.length > 0) {
        for (_i = 0, _len = files.length; _i < _len; _i++) {
          f = files[_i];
          this.uploadFile(f);
        }
      }
      else {
        f = {name: file_element.value.substring(file_element.value.lastIndexOf("\\") + 1),
          size: 0,  // it's not possible to get file size w/o flash or so
          type: file_element.value.substring(file_element.value.lastIndexOf(".") + 1)
        };
        this.uploadFile(f);
      }
    };

    APIS3Upload.prototype.uploadFile = function (file) {
      var request = new ApiMessage();
      request['@type']  = this.api_request_type;
      request.header = new ApiRequestHeader();
      request.body = this.api_request_body_builder('UPLOAD');

      //All file upload builders must have files[] array created
      request.body.addFile(file.name, this.file_caption);

      var s3UploadObject = this;
      //Authorizing file for upload
      ExecuteCommand(request, this.api_request_endpoint, function (data) {
        if ($.browser.msie) {
          s3UploadObject.IEuploadToS3(file, data.body.signed_request, data);
        } else {
          s3UploadObject.uploadToS3(file, data.body.signed_request, data);
        }
      });
    };

    APIS3Upload.prototype.IEuploadToS3 = function (file, url, data) {
      $('#' + this.upload_form + ' #key').val(data.body.key);
      $('#' + this.upload_form + ' #policy').val(data.body.policy);
      $('#' + this.upload_form + ' #signature').val(data.body.signature);
      $('#' + this.upload_form + ' #AWSAccessKeyId').val(data.body.accessKey);
      var this_s3upload = this;
      var options = { url: data.body.s3Bucket,
        iframe: true,
        success: function() {
          this_s3upload.successUpload(data);
        },
        error: function (response, textStatus, xhr) {
          this_s3upload.onError('Upload error: ' + xhr.status);
        }
      };

      $('#' + this.upload_form).ajaxSubmit(options);
    };

    APIS3Upload.prototype.uploadToS3 = function (file, url, data) {
      //console.log("S3-1: " + url);
      var this_s3upload, xhr;
      this_s3upload = this;

      xhr = this.createCORSRequest('PUT', url);
      if (!xhr) {
        alert("Your browser does not support cross origin requests, please try another browser");
        this.onError('CORS not supported');
      }
      else {
        xhr.onload = function () {
          if (xhr.status === 200) {
            this_s3upload.onProgress(100, 'Upload completed.');
            return this_s3upload.successUpload(data);
          }
          else {
            return this_s3upload.onError('Upload error: ' + xhr.status);
          }
        };

        xhr.onerror = function () {
          return this_s3upload.onError('XHR error.');
        };

        xhr.upload.onprogress = function (e) {
          var percentLoaded;
          if (e.lengthComputable) {
            percentLoaded = Math.round((e.loaded / e.total) * 100);
            return this_s3upload.onProgress(percentLoaded, percentLoaded === 100 ? 'Finalizing.' : 'Uploading.');
          }
        };
      }
      xhr.setRequestHeader('Content-Type', file.type);
      return xhr.send(file);
    };

    APIS3Upload.prototype.createCORSRequest = function (method, url) {
      var xhr;
      xhr = new XMLHttpRequest();
      if (xhr.withCredentials != null) {
        xhr.open(method, url, true);
      }
      else if (typeof XDomainRequest !== "undefined") {
        //console.log("XDomain Created");
        xhr = new XDomainRequest();
        xhr.open('POST', url);
      }
      else {
        xhr = null;
      }
      return xhr;
    };

    /**
     * Called when file is successfully uploaded to S3
     * @param data
     */
    APIS3Upload.prototype.successUpload = function (data) {
      var request = new ApiMessage();
      request['@type']  = this.api_request_type;
      request.header = new ApiRequestHeader();
      request.body = this.api_request_body_builder('ADD');

      //All file upload builders must have files[] array created
      $.each(data.body.files, function(k,v){
        request.body.addFile(v.name, v.file_caption, v.file_id);
      });

      var s3UploadObject = this;
      //Confirming successful file for upload
      ExecuteCommand(request, this.api_request_endpoint, function (data) {
        s3UploadObject.file_upload_success_callback(data);
      });
    };


    return APIS3Upload;
  })();

}).call(this);



