
package com.umapped.external.wetu.itinerary.details;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "start_time",
    "duration",
    "end_time",
    "time_slot",
    "content_entity_id",
    "simple_item_id",
    "sequence",
    "name",
    "is_highlight",
    "type",
    "reference",
    "prevent_voucher"
})
public class Activity {

    @JsonProperty("start_time")
    private String startTime;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("end_time")
    private String endTime;
    @JsonProperty("time_slot")
    private String timeSlot;
    @JsonProperty("content_entity_id")
    private int contentEntityId;
    @JsonProperty("simple_item_id")
    private String simpleItemId;
    @JsonProperty("sequence")
    private int sequence;
    @JsonProperty("name")
    private String name;
    @JsonProperty("is_highlight")
    private boolean isHighlight;
    @JsonProperty("type")
    private String type;
    @JsonProperty("reference")
    private String reference;
    @JsonProperty("prevent_voucher")
    private boolean preventVoucher;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The startTime
     */
    @JsonProperty("start_time")
    public String getStartTime() {
        return startTime;
    }

    /**
     * 
     * @param startTime
     *     The start_time
     */
    @JsonProperty("start_time")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * 
     * @return
     *     The duration
     */
    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    /**
     * 
     * @param duration
     *     The duration
     */
    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * 
     * @return
     *     The endTime
     */
    @JsonProperty("end_time")
    public String getEndTime() {
        return endTime;
    }

    /**
     * 
     * @param endTime
     *     The end_time
     */
    @JsonProperty("end_time")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * 
     * @return
     *     The timeSlot
     */
    @JsonProperty("time_slot")
    public String getTimeSlot() {
        return timeSlot;
    }

    /**
     * 
     * @param timeSlot
     *     The time_slot
     */
    @JsonProperty("time_slot")
    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    /**
     * 
     * @return
     *     The contentEntityId
     */
    @JsonProperty("content_entity_id")
    public int getContentEntityId() {
        return contentEntityId;
    }

    /**
     * 
     * @param contentEntityId
     *     The content_entity_id
     */
    @JsonProperty("content_entity_id")
    public void setContentEntityId(int contentEntityId) {
        this.contentEntityId = contentEntityId;
    }

    /**
     * 
     * @return
     *     The simpleItemId
     */
    @JsonProperty("simple_item_id")
    public String getSimpleItemId() {
        return simpleItemId;
    }

    /**
     * 
     * @param simpleItemId
     *     The simple_item_id
     */
    @JsonProperty("simple_item_id")
    public void setSimpleItemId(String simpleItemId) {
        this.simpleItemId = simpleItemId;
    }

    /**
     * 
     * @return
     *     The sequence
     */
    @JsonProperty("sequence")
    public int getSequence() {
        return sequence;
    }

    /**
     * 
     * @param sequence
     *     The sequence
     */
    @JsonProperty("sequence")
    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The isHighlight
     */
    @JsonProperty("is_highlight")
    public boolean isIsHighlight() {
        return isHighlight;
    }

    /**
     * 
     * @param isHighlight
     *     The is_highlight
     */
    @JsonProperty("is_highlight")
    public void setIsHighlight(boolean isHighlight) {
        this.isHighlight = isHighlight;
    }

    /**
     * 
     * @return
     *     The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The reference
     */
    @JsonProperty("reference")
    public String getReference() {
        return reference;
    }

    /**
     * 
     * @param reference
     *     The reference
     */
    @JsonProperty("reference")
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * 
     * @return
     *     The preventVoucher
     */
    @JsonProperty("prevent_voucher")
    public boolean isPreventVoucher() {
        return preventVoucher;
    }

    /**
     * 
     * @param preventVoucher
     *     The prevent_voucher
     */
    @JsonProperty("prevent_voucher")
    public void setPreventVoucher(boolean preventVoucher) {
        this.preventVoucher = preventVoucher;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(startTime).append(duration).append(endTime).append(timeSlot).append(contentEntityId).append(simpleItemId).append(sequence).append(name).append(isHighlight).append(type).append(reference).append(preventVoucher).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Activity) == false) {
            return false;
        }
        Activity rhs = ((Activity) other);
        return new EqualsBuilder().append(startTime, rhs.startTime).append(duration, rhs.duration).append(endTime, rhs.endTime).append(timeSlot, rhs.timeSlot).append(contentEntityId, rhs.contentEntityId).append(simpleItemId, rhs.simpleItemId).append(sequence, rhs.sequence).append(name, rhs.name).append(isHighlight, rhs.isHighlight).append(type, rhs.type).append(reference, rhs.reference).append(preventVoucher, rhs.preventVoucher).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
