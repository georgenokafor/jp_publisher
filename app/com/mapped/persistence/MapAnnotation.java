package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class MapAnnotation { 
 
	 public final static String MapAnnotationTypeId = "240"; 
	 protected static String pkTypeId =  "241"; 
	 private String pk; 
	 protected static String mapsIdTypeId =  "242"; 
	 private String mapsId; 
	 protected static String usersIdTypeId =  "243"; 
	 private String usersId; 
	 protected static String longitudeTypeId =  "244"; 
	 private String longitude; 
	 protected static String latitudeTypeId =  "245"; 
	 private String latitude; 
	 protected static String bearingTypeId =  "246"; 
	 private String bearing; 
	 protected static String statusTypeId =  "247"; 
	 private long status; 
	 protected static String lastUpdateTimestampTypeId =  "248"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "249"; 
	 private long createTimestamp; 
	 protected static String syncTimestampTypeId =  "250"; 
	 private long syncTimestamp; 
	 protected static String milestonesIdTypeId =  "251"; 
	 private String milestonesId; 
	 protected static String tagTypeId =  "252"; 
	 private String tag; 
	 protected static String noteTypeId =  "253"; 
	 private String note; 
	 protected static String modifiedByTypeId =  "254"; 
	 private String modifiedBy; 
	 protected static String createdByTypeId =  "255"; 
	 private String createdBy; 

 	 // Contructor 
	 public MapAnnotation () {} 
	 public MapAnnotation ( String pk,  String mapsId,  String usersId,  String longitude,  String latitude,  String bearing,  long status,  long lastUpdateTimestamp,  long createTimestamp,  long syncTimestamp,  String milestonesId,  String tag,  String note,  String modifiedBy,  String createdBy ) { 
		 this.pk=pk; 
		 this.mapsId=mapsId; 
		 this.usersId=usersId; 
		 this.longitude=longitude; 
		 this.latitude=latitude; 
		 this.bearing=bearing; 
		 this.status=status; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.syncTimestamp=syncTimestamp; 
		 this.milestonesId=milestonesId; 
		 this.tag=tag; 
		 this.note=note; 
		 this.modifiedBy=modifiedBy; 
		 this.createdBy=createdBy; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getMapsId () { 
	 	 return mapsId; 
 	 } 
 
 	 public void setMapsId(String mapsId) { 
	 	 this.mapsId = mapsId; 
 	 } 
 
 	 public  String getUsersId () { 
	 	 return usersId; 
 	 } 
 
 	 public void setUsersId(String usersId) { 
	 	 this.usersId = usersId; 
 	 } 
 
 	 public  String getLongitude () { 
	 	 return longitude; 
 	 } 
 
 	 public void setLongitude(String longitude) { 
	 	 this.longitude = longitude; 
 	 } 
 
 	 public  String getLatitude () { 
	 	 return latitude; 
 	 } 
 
 	 public void setLatitude(String latitude) { 
	 	 this.latitude = latitude; 
 	 } 
 
 	 public  String getBearing () { 
	 	 return bearing; 
 	 } 
 
 	 public void setBearing(String bearing) { 
	 	 this.bearing = bearing; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  String getMilestonesId () { 
	 	 return milestonesId; 
 	 } 
 
 	 public void setMilestonesId(String milestonesId) { 
	 	 this.milestonesId = milestonesId; 
 	 } 
 
 	 public  String getTag () { 
	 	 return tag; 
 	 } 
 
 	 public void setTag(String tag) { 
	 	 this.tag = tag; 
 	 } 
 
 	 public  String getNote () { 
	 	 return note; 
 	 } 
 
 	 public void setNote(String note) { 
	 	 this.note = note; 
 	 } 
 
 	 public  String getModifiedBy () { 
	 	 return modifiedBy; 
 	 } 
 
 	 public void setModifiedBy(String modifiedBy) { 
	 	 this.modifiedBy = modifiedBy; 
 	 } 
 
 	 public  String getCreatedBy () { 
	 	 return createdBy; 
 	 } 
 
 	 public void setCreatedBy(String createdBy) { 
	 	 this.createdBy = createdBy; 
 	 } 
 
 	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: MapAnnotation"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 MapsId: ").append(this.mapsId );  
		 sb.append("	 UsersId: ").append(this.usersId );  
		 sb.append("	 Longitude: ").append(this.longitude );  
		 sb.append("	 Latitude: ").append(this.latitude );  
		 sb.append("	 Bearing: ").append(this.bearing );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 MilestonesId: ").append(this.milestonesId );  
		 sb.append("	 Tag: ").append(this.tag );  
		 sb.append("	 Note: ").append(this.note );  
		 sb.append("	 ModifiedBy: ").append(this.modifiedBy );  
		 sb.append("	 CreatedBy: ").append(this.createdBy );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", MapAnnotationTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (mapsId != null) {
			 memberNode.put(mapsIdTypeId, mapsId);
		 }
		 if (usersId != null) {
			 memberNode.put(usersIdTypeId, usersId);
		 }
		 if (longitude != null) {
			 memberNode.put(longitudeTypeId, longitude);
		 }
		 if (latitude != null) {
			 memberNode.put(latitudeTypeId, latitude);
		 }
		 if (bearing != null) {
			 memberNode.put(bearingTypeId, bearing);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (milestonesId != null) {
			 memberNode.put(milestonesIdTypeId, milestonesId);
		 }
		 if (tag != null) {
			 memberNode.put(tagTypeId, tag);
		 }
		 if (note != null) {
			 memberNode.put(noteTypeId, note);
		 }
		 if (modifiedBy != null) {
			 memberNode.put(modifiedByTypeId, modifiedBy);
		 }
		 if (createdBy != null) {
			 memberNode.put(createdByTypeId, createdBy);
		 }
		 return classNode;
	 } 
	 public static MapAnnotation parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 MapAnnotation  c= new MapAnnotation(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.mapsId = fields.findPath(mapsIdTypeId).textValue();
			 c.usersId = fields.findPath(usersIdTypeId).textValue();
			 c.longitude = fields.findPath(longitudeTypeId).textValue();
			 c.latitude = fields.findPath(latitudeTypeId).textValue();
			 c.bearing = fields.findPath(bearingTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 c.milestonesId = fields.findPath(milestonesIdTypeId).textValue();
			 c.tag = fields.findPath(tagTypeId).textValue();
			 c.note = fields.findPath(noteTypeId).textValue();
			 c.modifiedBy = fields.findPath(modifiedByTypeId).textValue();
			 c.createdBy = fields.findPath(createdByTypeId).textValue();
			 return c;
		} else
			 return null;
	 } 
} 
 
