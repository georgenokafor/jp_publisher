package com.mapped.publisher.view;

import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.umapped.persistence.enums.ReservationType;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import java.util.Map;
import java.util.Optional;

/** Types of tabs used in a variety of views */
public enum TabType {
  FLIGHTS,
  HOTELS,
  CRUISES,
  TRANSPORTS,
  ACTIVITIES,
  TEMPLATES,
  NOTES,
  UPLOADS,
  RECLOCATORS,
  ITINERARY,
  OFFERS,
  WCITIES,
  AFAR;

  /**
   * Class to allow specifying parameter from enum in Play Framework.
   */
  public static class Bound
      implements QueryStringBindable<Bound>, PathBindable<Bound> {
    private TabType value;

    /**
     * This empty constructor must be there, implicit one is not recognized
     * when other constructors are present.
     */
    public Bound() {
      this.value = null;
    }

    public Bound(TabType tab) {
      this.value = tab;
    }

    @Override
    public Bound bind(String key, String txt) {
      this.value = TabType.valueOf(txt);
      return this;
    }

    @Override
    public Optional<Bound> bind(String key, Map<String, String[]> params) {
      String[] arr = params.get(key);
      if (arr != null && arr.length > 0) {
        this.value = TabType.valueOf(arr[0]);
        return Optional.of(this);
      }
      else {
        return Optional.empty();
      }
    }

    @Override
    public String unbind(String key) {
      return this.value.name();
    }

    @Override
    public String javascriptUnbind() {
      return this.value.name();
    }

    public TabType value() {
      return this.value;
    }
  }


  public static TabType fromString(String name) {
    try {
      return TabType.valueOf(name);
    }
    catch (Exception e) {
      //Nothing to do here.
    }
    return ITINERARY;
  }

  public static TabType fromBookingType(ReservationType type) {
    switch (type.getRootLevelType()) {
      case FLIGHT:
        return FLIGHTS;
      case HOTEL:
        return HOTELS;
      case CRUISE:
        return CRUISES;
      case TRANSPORT:
        return TRANSPORTS;
      case ACTIVITY:
        return ACTIVITIES;
      default:
        Log.log(LogLevel.ERROR, "Unsupported trip detail tabbable type:" + type);
        return FLIGHTS;
    }
  }
}
