package com.mapped.publisher.view.billing;

import com.mapped.publisher.view.BaseView;
import models.publisher.BillingInvoice;
import models.publisher.Company;
import models.publisher.UserProfile;
import org.joda.time.YearMonth;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2015-03-27.
 */
public class InvoiceView
    extends BaseView {
  public YearMonth period;
  public boolean   isPushed;

  public static class InvoiceDetails {
    public BillingInvoice invoice;
    public Company cmpy;
    public UserProfile user;
    public String reportUrl;
    public String deleteUrl;
  }

  public List<InvoiceDetails> invoices;

  public InvoiceView() {
    invoices = new ArrayList<>();
    isPushed = false;
  }
}
