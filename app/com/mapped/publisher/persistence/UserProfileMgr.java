package com.mapped.publisher.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-19
 * Time: 8:41 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserProfileMgr {

  private final static  String SELECT_ALL_BY_KEYWORD =
      "SELECT usertype, profilephotourl, profilephoto, fax, mobile, userid, firstname, lastname, lastlogintimestamp, " +
      "       status, createdtimestamp, lastupdatedtimestamp, createdby, version, modifiedby, email, facebook, twitter, " +
      "       web, '' as cmpyid, '0' as iscmpyowner, phone, '-1' as cmpylinktype, '' as linkId " +
      "FROM user_profile " +
      "WHERE status <> -1 AND " +
      "       (LOWER(userid) like ? or LOWER(firstName) like ? OR LOWER(lastName) LIKE ? OR LOWER(email) like ?)" +
      "ORDER BY lastName asc";
  private final static  String SELECT_ALL_BY_KEYWORD_CMPY = "SELECT u.usertype, u.profilephotourl, u.profilephoto, u.fax, u.mobile, u.userid, u.firstname, u.lastname, u.lastlogintimestamp, u.status, u.createdtimestamp, u.lastupdatedtimestamp, u.createdby, u.version, u.modifiedby, u.email, u.facebook, u.twitter, u.web, u.phone, c.cmpyid as cmpyid, c.isowner as iscmpyowner, c.linktype as cmpylinktype, c.pk as linkId FROM user_profile u, user_cmpy_link c where u.status <> -1 and ( LOWER(u.userid) like ? or  LOWER(u.firstName) like ? or  LOWER(u.lastName) like ? or  LOWER(u.email) like ?) and u.userid = c.userid and c.cmpyid = ? and c.status = 0 order by u.lastName asc; ";
  private final static  String COUNT_ALL_BY_KEYWORD_CMPY = "SELECT count(*) FROM user_profile u, user_cmpy_link c where u.status <> -1 and ( LOWER(u.userid) like ? or  LOWER(u.firstName) like ? or  LOWER(u.lastName) like ? or  LOWER(u.email) like ?) and u.userid = c.userid and c.cmpyid = ? and c.status = 0; ";
  private final static  String SELECT_ALL_BY_KEYWORD_CMPY_DT = "SELECT u.usertype, u.profilephotourl, u.profilephoto, u.fax, u.mobile, u.userid, u.firstname, u.lastname, u.lastlogintimestamp, u.status, u.createdtimestamp, u.lastupdatedtimestamp, u.createdby, u.version, u.modifiedby, u.email, u.facebook, u.twitter, u.web, u.phone, c.cmpyid as cmpyid, c.isowner as iscmpyowner, c.linktype as cmpylinktype, c.pk as linkId FROM user_profile u, user_cmpy_link c where u.status <> -1 and ( LOWER(u.userid) like ? or  LOWER(u.firstName) like ? or  LOWER(u.lastName) like ? or  LOWER(u.email) like ?) and u.userid = c.userid and c.cmpyid = ? and c.status = 0 order by u.lastName asc OFFSET ?; ";
  private final static  String SELECT_ALL_BY_CMPYID = "SELECT u.usertype,u.profilephotourl, u.profilephoto, u.fax, u.mobile, u.userid, u.firstname, u.lastname, u.lastlogintimestamp, u.status, u.createdtimestamp, u.lastupdatedtimestamp, u.createdby, u.version, u.modifiedby, u.email, u.facebook, u.twitter, u.web, u.phone, c.cmpyid as cmpyid, c.isowner as iscmpyowner, c.linktype as cmpylinktype, c.pk as linkId FROM user_profile u, user_cmpy_link c where u.status <> -1 and u.userid = c.userid and c.cmpyid = ? and c.status = 0 order by u.lastName asc; ";
  private final static  String SELECT_ALL_BY_GROUPID = "SELECT u.usertype,u.profilephotourl, u.profilephoto, u.fax, u.mobile, u.userid, u.firstname, u.lastname, u.lastlogintimestamp, u.status, u.createdtimestamp, u.lastupdatedtimestamp, u.createdby, u.version, u.modifiedby, u.email, u.facebook, u.twitter, u.web, u.phone, c.cmpyid as cmpyid, c.isowner as iscmpyowner, c.linktype as cmpylinktype, c.pk as linkId FROM user_profile u, user_cmpy_link c, cmpy_group_members m where u.status <> -1 and u.userid = c.userid and c.status = 0  and c.pk = m.usercmpylinkpk and m.groupid =? and m.status =0 order by u.lastName asc; ";
  private final static  String SELECT_ADMIN_BY_CMPYID = "SELECT u.usertype,u.profilephotourl, u.profilephoto, u.fax, u.mobile, u.userid, u.firstname, u.lastname, u.lastlogintimestamp, u.status, u.createdtimestamp, u.lastupdatedtimestamp, u.createdby, u.version, u.modifiedby, u.email, u.facebook, u.twitter, u.web, u.phone, c.cmpyid as cmpyid, c.isowner as iscmpyowner, c.linktype as cmpylinktype, c.pk as linkId FROM user_profile u, user_cmpy_link c where u.status <> -1 and u.userid = c.userid and c.cmpyid = ? and c.status = 0 and c.linktype = 1 order by u.lastName asc; ";


  public static List<UserProfileRS> findByTerm(String term, Connection conn)
      throws SQLException {
    ResultSet rs = null;
    PreparedStatement prep = null;
    try {
      if (term != null) {
        term = term.toLowerCase();
      }
      prep = conn.prepareStatement(SELECT_ALL_BY_KEYWORD);
      prep.setString(1, term);
      prep.setString(2, term);
      prep.setString(3, term);
      prep.setString(4, term);

      rs = prep.executeQuery();
      return (handleResults(rs));
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static ArrayList<UserProfileRS> handleResults(ResultSet rs)
      throws SQLException {
    ArrayList<UserProfileRS> results = new ArrayList<>();
    if (rs == null) {
      return results;
    }
    while (rs.next()) {
      UserProfileRS c = new UserProfileRS();
      c.setUserid(rs.getString("UserId"));
      c.setFirstname(rs.getString("FirstName"));
      c.setLastname(rs.getString("LastName"));
      c.setEmail(rs.getString("Email"));
      c.setStatus(rs.getInt("Status"));
      c.setPhone(rs.getString("Phone"));
      c.setMobile(rs.getString("Mobile"));
      c.setFacebook(rs.getString("Facebook"));
      c.setTwitter(rs.getString("Twitter"));
      c.setWeb(rs.getString("Web"));
      c.setFax(rs.getString("Fax"));
      c.setProfilephoto(rs.getString("ProfilePhoto"));
      c.setProfilephotourl(rs.getString("ProfilePhotoUrl"));
      c.setCmpyId(rs.getString("CmpyId"));
      c.setCmpyIdOwner(rs.getInt("isCmpyOwner"));
      c.setCmpyLinkType(rs.getInt("cmpylinktype"));
      c.setLinkId(rs.getString("linkId"));
      c.setLastlogintimestamp(rs.getLong("LastLoginTimestamp"));
      c.setLastupdatedtimestamp(rs.getLong("LastUpdatedTimestamp"));
      c.setCreatedtimestamp(rs.getLong("CreatedTimestamp"));
      c.setModifiedby(rs.getString("ModifiedBy"));
      c.setCreatedby(rs.getString("CreatedBy"));
      c.setUsertype(rs.getInt("UserType"));
      results.add(c);
    }
    return results;
  }

  public static List<UserProfileRS> findByTermAndCmpy(String term, String cmpyId, Connection conn)
      throws SQLException {
    ResultSet rs = null;
    PreparedStatement prep = null;
    try {
      if (term != null) {
        term = term.toLowerCase();
      }
      prep = conn.prepareStatement(SELECT_ALL_BY_KEYWORD_CMPY);
      prep.setString(1, term);
      prep.setString(2, term);
      prep.setString(3, term);
      prep.setString(4, term);
      prep.setString(5, cmpyId);


      rs = prep.executeQuery();
      return (handleResults(rs));
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static List<UserProfileRS> findByTermAndCmpyForDT(String term, String cmpyId, int start, int count, Connection conn)
      throws SQLException {
    ResultSet rs = null;
    PreparedStatement prep = null;
    try {
      if (term != null) {
        term = term.toLowerCase();
      }
      prep = conn.prepareStatement(SELECT_ALL_BY_KEYWORD_CMPY_DT);
      prep.setString(1, term);
      prep.setString(2, term);
      prep.setString(3, term);
      prep.setString(4, term);
      prep.setString(5, cmpyId);
      prep.setInt(6, start);

      prep.setMaxRows(count);


      rs = prep.executeQuery();
      return (handleResults(rs));
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static int countByTermAndCmpy(String term, String cmpyId, Connection conn)
      throws SQLException {
    ResultSet rs = null;
    PreparedStatement prep = null;
    try {
      if (term != null || !term.isEmpty()) {
        term = term.toLowerCase();
      }
      prep = conn.prepareStatement(COUNT_ALL_BY_KEYWORD_CMPY);
      prep.setString(1, term);
      prep.setString(2, term);
      prep.setString(3, term);
      prep.setString(4, term);
      prep.setString(5, cmpyId);


      rs = prep.executeQuery();
      if (rs == null) {
        return 0;
      }

      rs.next();
      return rs.getInt(1);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static List<UserProfileRS> findByCmpy(String cmpyId, Connection conn)
      throws SQLException {
    ResultSet rs = null;
    PreparedStatement prep = null;
    try {
      prep = conn.prepareStatement(SELECT_ALL_BY_CMPYID);
      prep.setString(1, cmpyId);

      rs = prep.executeQuery();
      return (handleResults(rs));
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static List<UserProfileRS> findAdminByCmpy(String cmpyId, Connection conn)
      throws SQLException {
    ResultSet rs = null;
    PreparedStatement prep = null;
    try {

      prep = conn.prepareStatement(SELECT_ADMIN_BY_CMPYID);
      prep.setString(1, cmpyId);


      rs = prep.executeQuery();
      return (handleResults(rs));
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static List<UserProfileRS> findByGroup(String groupId, Connection conn)
      throws SQLException {
    ResultSet rs = null;
    PreparedStatement prep = null;
    try {

      prep = conn.prepareStatement(SELECT_ALL_BY_GROUPID);
      prep.setString(1, groupId);


      rs = prep.executeQuery();
      return (handleResults(rs));
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }
}
