package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by ryan on 15-08-17.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Activity
    extends Thing {

  public String activityType;

  public Organization organizedBy;

  public String guideId;

}
