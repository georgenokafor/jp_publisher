
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://webapi.globusandcosmos.com/ExtendedDepartureList.xsd}GetDeparturesExtendedXMLResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDeparturesExtendedXMLResult"
})
@XmlRootElement(name = "GetDeparturesExtendedXMLResponse")
public class GetDeparturesExtendedXMLResponse {

    @XmlElement(name = "GetDeparturesExtendedXMLResult", namespace = "http://webapi.globusandcosmos.com/ExtendedDepartureList.xsd", required = true, nillable = true)
    protected ExtendedepartureList getDeparturesExtendedXMLResult;

    /**
     * Gets the value of the getDeparturesExtendedXMLResult property.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedepartureList }
     *     
     */
    public ExtendedepartureList getGetDeparturesExtendedXMLResult() {
        return getDeparturesExtendedXMLResult;
    }

    /**
     * Sets the value of the getDeparturesExtendedXMLResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedepartureList }
     *     
     */
    public void setGetDeparturesExtendedXMLResult(ExtendedepartureList value) {
        this.getDeparturesExtendedXMLResult = value;
    }

}
