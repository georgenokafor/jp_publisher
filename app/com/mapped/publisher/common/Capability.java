package com.mapped.publisher.common;

/**
 * Capabilities are managed via this enumeration, group capabilities so that they correspond to features
 */
public enum Capability {
  /**
   * Indicates whether or not to allow access to Travel42 inside Documents and Custom docs
   */
  TRAVEL42_GUIDES,
  /**
   * Communicator
   */
  COMMUNICATOR,
  /**
   * Communicator ON by default
   */
  COMMUNICATOR_TRAVELER_TALKBACK,
  /**
   * Communicator MSG Digest ON by default
   */
  COMMUNICATOR_MSG_DIGEST,
  /**
   * Whether or not to enable 2-way communication via email
   */
  COMMUNICATOR_NOTIFICATION_EMAIL,
  /**
   * Whether or not to enable chat message notificaiton via email
   */
  COMMUNICATOR_NOTIFICATION_SMS,
  /**
   * Enabling access to flight notifications
   */
  FLIGHT_NOTIFICATIONS,
  /**
   * Enabling access to new web itinerary
   */
  BETA_WEB_ITINERARY,
  /**
   * Traveller editable itinerary
   */
  TRAVELER_COLLABORATION,
  /**
   * SABRE Live Bookings in Inbox
   */
  INBOX_SABRE,
  /**
   * Clientbase Rescards Bookings in Inbox
   */
  INBOX_CLIENTBASE,
  /**
   * Allow for itinerary analysis
   */
  OFFER_ANALYSIS,
  /**
   * Allow access to Amadeus GDS import
   */
  AMADEUS_GDS_IMPORT,
  /**
   * Diable access to Afar
   */
  DISABLE_CONTENT_AFAR,
  /**
   * Diable access to Wcities
   */
  DISABLE_CONTENT_WCITIES

}
