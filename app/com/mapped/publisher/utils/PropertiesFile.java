/****************
@author  $Author:   mminhas  $, Toronto Stock Exchange. 
@version $Id: PropertiesFile.java,v 1.3 2001/11/14 20:43:40 twong Exp $
<PRE>
MAINTENANCE LOG
===============
$Log:   Y:/New Archives/TSE Applications/Web Systems/Web Framework/source/com/tse/common/util/PropertiesFile.v_va  $
   
      Rev 1.0   Mar 09 2002 10:18:10   mminhas
   Initial revision.
   
      Rev 1.2   Mar 04 2002 16:35:30   twong
   Return null if parameter is not found
   
      Rev 1.1   Jan 17 2002 17:43:24   mminhas
   Second revision
Revision 1.3  2001/11/14 20:43:40  twong
Trim all values read from properties files

Revision 1.2  2001/10/30 15:02:53  fhirsh
Adding CVS Headers to all files from Perl script

</PRE>
**/

/*
 * ConfigMgr.java
 *
 * Created on July 25, 2001, 1:37 PM
 */

package com.mapped.publisher.utils;

import java.util.*;
import java.io.*;


/**
 *
 * @author  twong
 * @version 
 */
public class PropertiesFile {

    private Properties dataProperties;
    
   
    public PropertiesFile() {
    }
   
    public void loadConfigFile (String fileName) throws Exception{

        File dataFile = new File(fileName);
        if (dataFile.exists()) {
            FileInputStream dataStream = new FileInputStream(dataFile);
            dataProperties = new Properties();
            dataProperties.load(dataStream);
            if (dataProperties.isEmpty()) {
                throw new Exception("PropertiesFile:loadConfigFile - Data file empty!!! " + fileName);
            }
        } else {
            throw new Exception ("PropertiesFile:loadConfigFile - Data file not found!!! " + fileName);
        }
    }
        


    public String getParameter (String param) {
        if (dataProperties != null && dataProperties.containsKey(param))
            return (dataProperties.getProperty(param).trim());
        else
            return null;
    }
    
    public Collection getParameters (String param) {
        if (dataProperties != null && dataProperties.containsKey(param)) {
            String listValues = dataProperties.getProperty(param).trim();
            return parseValues(listValues);
        } else
            return null;
       
    }
    
    public Collection parseValues (String listValues) {
        Collection list = new ArrayList<>();
        int startIndex = 0;
        boolean end = false;
        while (!end && listValues != null) {
            int endIndex = listValues.indexOf(',',startIndex);
            if (endIndex != -1) {
                list.add(listValues.substring(startIndex, endIndex));
                startIndex = endIndex + 1;
            } else {
                int lastIndex = listValues.length()  ;
                if (lastIndex > startIndex) {
                    list.add(listValues.substring(startIndex, lastIndex));
                }
                end = true;
            }
            
        }
        return list;
    }    
}
