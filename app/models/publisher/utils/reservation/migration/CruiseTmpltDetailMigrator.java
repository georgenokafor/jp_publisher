package models.publisher.utils.reservation.migration;

import com.mapped.publisher.utils.Log;
import com.umapped.persistence.reservation.cruise.UmCruiseReservation;

import models.publisher.TmpltCruise;
import models.publisher.TmpltDetails;
import models.utils.migration.Migrator;

public class CruiseTmpltDetailMigrator implements Migrator<TmpltDetails> {
  private UmCruiseReservationBuilder builder = new UmCruiseReservationBuilder();
  
  @Override
  public boolean migrate(TmpltDetails entity) {
    
    TmpltCruise cruise = TmpltCruise.find.byId(entity.detailsid);
   // Log.info("Migrate a cruise trip_detail");
    UmCruiseReservation reservation = builder.build(entity, cruise);
    entity.setReservation(reservation);
    return true;
  }
}
