package com.mapped.publisher.utils;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.mapped.publisher.api.InternalMsg;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.common.CoreConstants;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-10
 * Time: 12:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class SQSUtils {
  private final static String ACCESS_KEY = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.S3_ACCESS_KEY);
  private final static String SECRET_KEY = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.S3_SECRET);
  private static BasicAWSCredentials cre = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
  private static AmazonSQSClient sqsClient = null;

  public static SendMessageResult sendMessage(String queueUrl, InternalMsg msg)
      throws Exception {
    try {
      if (sqsClient == null) {
        sqsClient = new AmazonSQSClient(cre);
      }

      SendMessageRequest request = new SendMessageRequest();
      request.setMessageBody(msg.toJson().toString());
      request.setQueueUrl(queueUrl);
      request.setRequestCredentials(cre);
      return sqsClient.sendMessage(request);

    }
    catch (Exception e) {
      sqsClient = null;
      throw e;
    }
  }
}
