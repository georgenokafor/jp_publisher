package com.mapped.publisher.persistence;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-03-26
 * Time: 11:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class BillingMgr {

  public static ArrayList<BillingTripRS> findNewTripsPublished(long startTimestamp,
                                                               long endTimestamp,
                                                               String cmpyId,
                                                               Connection conn)
      throws SQLException {
    String trips_sql = "select t.tripid as tripid, t.name as tripname, tp.createdtimestamp as publishedtimestamp, " +
                       "tp.createdby as userid, u.firstname as firstname, " +
                       "u.lastname as lastname  from trip_publish_history tp, trip t, " +
                       "user_profile u where tp.createdtimestamp >= ? and tp.createdtimestamp <= ? and tp.tripid not " +
                       "in (select distinct(tripid) from trip_publish_history where createdtimestamp  < ? and groupid" +
                       " is null) and tp.groupid is null and tp.tripid = t.tripid and t.cmpyid = ? and tp.createdby =" +
                       " u.userid order by t.createdtimestamp asc;";
    String groups_sql = "select t.tripid as tripid, t.name as tripname, tp.createdtimestamp as publishedtimestamp, " +
                        "tg.groupid as groupid, tg.name as groupname, tp.createdby as userid, " +
                        "u.firstname as firstname, u.lastname as lastname from trip_publish_history tp, trip t, " +
                        "trip_group tg, user_profile u  where tp.createdtimestamp >= ? and tp.createdtimestamp <= ? " +
                        "and tp.groupid not in (select distinct(groupid) from trip_publish_history where " +
                        "createdtimestamp  < ? and groupid is not  null) and tp.groupid is not null and tp.tripid = t" +
                        ".tripid and t.cmpyid = ? and tg.tripid  = tp.tripid and tg.groupid  = tp.groupid and tp" +
                        ".createdby = u.userid  order by t.createdtimestamp asc;";

    ArrayList<BillingTripRS> results = new ArrayList<BillingTripRS>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {

      prep = conn.prepareStatement(trips_sql);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      prep.setLong(3, startTimestamp);
      prep.setString(4, cmpyId);

      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          BillingTripRS rec = new BillingTripRS();
          rec.setCreatedTimestamp(rs.getLong("publishedtimestamp"));
          rec.setGroupId(null);
          rec.setCmpyName(null);
          rec.setGroupName(null);
          rec.setNumPublished(0);
          rec.setTripId(rs.getString("tripid"));
          rec.setTripName(rs.getString("tripname"));
          rec.setCreatedBy(rs.getString("userid"));
          rec.setFirstName(rs.getString("firstname"));
          rec.setLastName(rs.getString("lastname"));
          results.add(rec);

        }
        rs.close();
        prep.close();
      }


      prep = conn.prepareStatement(groups_sql);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      prep.setLong(3, startTimestamp);
      prep.setString(4, cmpyId);

      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          BillingTripRS rec = new BillingTripRS();
          rec.setCreatedTimestamp(rs.getLong("publishedtimestamp"));
          rec.setGroupId(rs.getString("groupid"));
          rec.setCmpyName(null);
          rec.setGroupName(rs.getString("groupname"));
          rec.setNumPublished(0);
          rec.setTripId(rs.getString("tripid"));
          rec.setTripName(rs.getString("tripname"));
          rec.setCreatedBy(rs.getString("userid"));
          rec.setFirstName(rs.getString("firstname"));
          rec.setLastName(rs.getString("lastname"));
          results.add(rec);
        }
        rs.close();
        prep.close();
      }


    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
    return results;
  }


  public static ArrayList<BillingTripTravellerRS> findNewTripTravellerPublished(long startTimestamp,
                                                                                long endTimestamp,
                                                                                String cmpyId,
                                                                                Connection conn)
      throws SQLException {
    String trips_sql = "select a.uid, tl.tripid, a.email, tl.created_ts, tl.created_by from account a, account_trip_link tl where tl.legacy_group_id is" +
                       " null and tl.tripid in (select tp.tripid from trip_publish_history tp, " +
                       "trip t where tp.createdtimestamp >= ? and tp.createdtimestamp <= ? and tp.tripid not in " +
                       "(select distinct(tripid) from trip_publish_history where createdtimestamp  < ? and groupid is" +
                       " null) and tp.groupid is null and tp.tripid = t.tripid and t.cmpyid = ?) and a.uid = tl.uid and tl.created_ts" +
                       " <= ?;";


    String groups_sql = "select a.uid, tl.tripid, a.email, tl.created_ts, tl.created_by, tl.legacy_group_id from account a, account_trip_link tl where " +
                        "legacy_group_id is not null and legacy_group_id in (select tp.groupid from " +
                        "trip_publish_history tp, trip t where tp.createdtimestamp >= ? and tp.createdtimestamp <= ? " +
                        "and tp.groupid not in (select distinct(groupid) from trip_publish_history where " +
                        "createdtimestamp  < ? and groupid is not  null) and tp.groupid is not null and tp.tripid = t" +
                        ".tripid and t.cmpyid = ? ) and a.uid = tl.uid and tl.created_ts <= ?;";

    ArrayList<BillingTripTravellerRS> results = new ArrayList<BillingTripTravellerRS>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {

      prep = conn.prepareStatement(trips_sql);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      prep.setLong(3, startTimestamp);
      prep.setString(4, cmpyId);
      prep.setTimestamp(5, new Timestamp(endTimestamp));


      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          BillingTripTravellerRS rec = new BillingTripTravellerRS();
          rec.setCreatedTimestamp(rs.getTimestamp("created_ts"));
          rec.setGroupId(null);
          rec.setTripId(rs.getString("tripid"));
          rec.setEmail(rs.getString("email"));
          rec.setPassengerPk(rs.getLong("uid"));
          rec.setUserId(rs.getString("created_by"));
          results.add(rec);

        }
        rs.close();
        prep.close();
      }


      prep = conn.prepareStatement(groups_sql);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      prep.setLong(3, startTimestamp);
      prep.setString(4, cmpyId);
      prep.setTimestamp(5, new Timestamp(endTimestamp));


      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          BillingTripTravellerRS rec = new BillingTripTravellerRS();
          rec.setCreatedTimestamp(rs.getTimestamp("createdtimestamp"));
          rec.setGroupId(rs.getString("legacy_group_id"));
          rec.setTripId(rs.getString("tripid"));
          rec.setEmail(rs.getString("email"));
          rec.setPassengerPk(rs.getLong("uid"));
          rec.setUserId(rs.getString("created_by"));

          results.add(rec);
        }
        rs.close();
        prep.close();
      }


    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
    return results;
  }

  public static ArrayList<BillingTripTravellerRS> findNewTripTravelAgentPublished(long startTimestamp,
                                                                                  long endTimestamp,
                                                                                  String cmpyId,
                                                                                  Connection conn)
      throws SQLException {
    String trips_sql = "select pk, tripid, email, createdtimestamp, createdby from trip_agent where groupid is null " +
                       "and status = 0 and tripid in (select tp.tripid from trip_publish_history tp, " +
                       "trip t where tp.createdtimestamp >= ? and tp.createdtimestamp <= ? and tp.tripid not in " +
                       "(select distinct(tripid) from trip_publish_history where createdtimestamp  < ? and groupid is" +
                       " null) and tp.groupid is null and tp.tripid = t.tripid and t.cmpyid = ?) and createdtimestamp" +
                       " <= ?;";
    String groups_sql = "select pk, tripid, groupid, email, createdtimestamp, createdby from trip_agent where groupid" +
                        " is not null and status = 0 and groupid in (select tp.groupid from trip_publish_history tp, " +
                        "trip t where tp.createdtimestamp >= ? and tp.createdtimestamp <= ? and tp.groupid not in " +
                        "(select distinct(groupid) from trip_publish_history where createdtimestamp  < ? and groupid " +
                        "is not  null) and tp.groupid is not null and tp.tripid = t.tripid and t.cmpyid = ? ) and " +
                        "createdtimestamp <= ?;";

    ArrayList<BillingTripTravellerRS> results = new ArrayList<BillingTripTravellerRS>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {

      prep = conn.prepareStatement(trips_sql);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      prep.setLong(3, startTimestamp);
      prep.setString(4, cmpyId);
      prep.setLong(5, endTimestamp);

      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          BillingTripTravellerRS rec = new BillingTripTravellerRS();
          rec.setCreatedTimestamp(new Timestamp(rs.getLong("createdtimestamp")));
          rec.setGroupId(null);
          rec.setTripId(rs.getString("tripid"));
          rec.setEmail(rs.getString("email"));
          rec.setPassengerPk(rs.getLong("pk"));
          rec.setUserId(rs.getString("createdby"));

          results.add(rec);

        }
        rs.close();
        prep.close();
      }


      prep = conn.prepareStatement(groups_sql);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      prep.setLong(3, startTimestamp);
      prep.setString(4, cmpyId);
      prep.setLong(5, endTimestamp);

      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          BillingTripTravellerRS rec = new BillingTripTravellerRS();
          rec.setCreatedTimestamp(new Timestamp(rs.getLong("createdtimestamp")));
          rec.setGroupId(rs.getString("groupid"));
          rec.setTripId(rs.getString("tripid"));
          rec.setEmail(rs.getString("email"));
          rec.setPassengerPk(rs.getLong("pk"));
          rec.setUserId(rs.getString("createdby"));

          results.add(rec);
        }
        rs.close();
        prep.close();
      }


    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
    return results;
  }


  public static ArrayList<BillingTripTravellerRS> findNewTravellerExistingTrip(long startTimestamp,
                                                                               long endTimestamp,
                                                                               String cmpyId,
                                                                               Connection conn)
      throws SQLException {

    String trips_sql = "select t.tripid as tripid, t.name as tripname, a.uid as travellerid, a.email as email, " +
                       "tl.created_by as userid, tl.created_ts as createdtimestamp from Account a, Account_trip_link tl, " +
                       "trip t where tl.legacy_group_id is null  and  tl.created_ts >= ? and tl" +
                       ".created_ts <=? and tl.tripid in (select tripid from trip_publish_history where " +
                       "createdtimestamp < ? and groupid is null) and tl.uid = a.uid and t.tripid = tl.tripid and t.cmpyid = ?;";

    String groups_sql = "select t.tripid as tripid, t.name as tripname, a.uid as travellerid, a.email as email, " +
                        "tl.created_by as userid, g.name as groupname, g.groupid as groupid,  " +
                        "tl.created_ts as createdtimestamp from Account a, Account_trip_link tl, trip t, " +
                        "trip_group g where tl.legacy_group_id is not null and tl.legacy_group_id = g.groupid and  " +
                        "tl.created_ts >= ? and tl.created_ts <=? and tl.legacy_group_id in (select groupid from " +
                        "trip_publish_history where createdtimestamp < ? and groupid is not null) and t.tripid = tl" +
                        ".tripid and a.uid = tl.uid and t.cmpyid = ?;";

    ArrayList<BillingTripTravellerRS> results = new ArrayList<BillingTripTravellerRS>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {

      prep = conn.prepareStatement(trips_sql);
      prep.setTimestamp(1, new Timestamp(startTimestamp));
      prep.setTimestamp(2, new Timestamp(endTimestamp));
      prep.setLong(3, startTimestamp);
      prep.setString(4, cmpyId);

      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          BillingTripTravellerRS rec = new BillingTripTravellerRS();
          rec.setCreatedTimestamp(rs.getTimestamp("createdtimestamp"));
          rec.setGroupId(null);
          rec.setTripId(rs.getString("tripid"));
          rec.setTripName(rs.getString("tripName"));
          rec.setEmail(rs.getString("email"));
          rec.setPassengerPk(rs.getLong("travellerid"));
          rec.setUserId(rs.getString("userid"));
          results.add(rec);

        }
        rs.close();
        prep.close();
      }


      prep = conn.prepareStatement(groups_sql);
      prep.setTimestamp(1, new Timestamp(startTimestamp));
      prep.setTimestamp(2, new Timestamp(endTimestamp));
      prep.setLong(3, startTimestamp);
      prep.setString(4, cmpyId);

      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          BillingTripTravellerRS rec = new BillingTripTravellerRS();
          rec.setCreatedTimestamp(rs.getTimestamp("createdtimestamp"));
          rec.setGroupId(rs.getString("groupid"));
          rec.setTripId(rs.getString("tripid"));
          rec.setTripName(rs.getString("tripName"));
          rec.setEmail(rs.getString("email"));
          rec.setPassengerPk(rs.getLong("travellerid"));
          rec.setGroupName(rs.getString("groupname"));
          rec.setUserId(rs.getString("userid"));

          results.add(rec);
        }
        rs.close();
        prep.close();
      }


    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
    return results;
  }

  public static ArrayList<BillingTripTravellerRS> findNewTravelAgentExistingTrip(long startTimestamp,
                                                                                 long endTimestamp,
                                                                                 String cmpyId,
                                                                                 Connection conn)
      throws SQLException {

    String trips_sql = "select t.tripid as tripid, t.name as tripname, tp.pk as travellerid, tp.email as email, " +
                       "tp.createdby as userid, tp.createdtimestamp as createdtimestamp from trip_agent tp, " +
                       "trip t where tp.groupid is null and tp.status = 0 and tp.createdtimestamp >= ? and tp" +
                       ".createdtimestamp <=? and tp.tripid in (select tripid from trip_publish_history where " +
                       "createdtimestamp < ? and groupid is null) and t.tripid = tp.tripid and t.cmpyid = ?;";
    String groups_sql = "select t.tripid as tripid, t.name as tripname, tp.pk as travellerid, tp.email as email, " +
                        "tp.createdby as userid, g.name as groupname, g.groupid as groupid, " +
                        "tp.createdtimestamp as createdtimestamp from trip_agent tp, trip t, " +
                        "trip_group g where tp.groupid is not null and tp.groupid = g.groupid and tp.status = 0  and " +
                        "tp.createdtimestamp >= ? and tp.createdtimestamp <=? and tp.groupid in (select groupid from " +
                        "trip_publish_history where createdtimestamp < ? and groupid is not null) and t.tripid = tp" +
                        ".tripid and t.cmpyid = ?;";

    ArrayList<BillingTripTravellerRS> results = new ArrayList<BillingTripTravellerRS>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {

      prep = conn.prepareStatement(trips_sql);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      prep.setLong(3, startTimestamp);
      prep.setString(4, cmpyId);

      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          BillingTripTravellerRS rec = new BillingTripTravellerRS();
          rec.setCreatedTimestamp(new Timestamp(rs.getLong("createdtimestamp")));
          rec.setGroupId(null);
          rec.setTripId(rs.getString("tripid"));
          rec.setTripName(rs.getString("tripName"));
          rec.setEmail(rs.getString("email"));
          rec.setUserId(rs.getString("userid"));
          rec.setPassengerPk(rs.getLong("travellerid"));
          results.add(rec);

        }
        rs.close();
        prep.close();
      }


      prep = conn.prepareStatement(groups_sql);
      prep.setLong(1, startTimestamp);
      prep.setLong(2, endTimestamp);
      prep.setLong(3, startTimestamp);
      prep.setString(4, cmpyId);

      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          BillingTripTravellerRS rec = new BillingTripTravellerRS();
          rec.setCreatedTimestamp(new Timestamp(rs.getLong("createdtimestamp")));
          rec.setGroupId(rs.getString("groupid"));
          rec.setTripId(rs.getString("tripid"));
          rec.setTripName(rs.getString("tripName"));
          rec.setEmail(rs.getString("email"));
          rec.setUserId(rs.getString("userid"));
          rec.setPassengerPk(rs.getLong("travellerid"));
          rec.setGroupName(rs.getString("groupname"));

          results.add(rec);
        }
        rs.close();
        prep.close();
      }


    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
    return results;
  }


  public static int numOfExistingTravellers(long startTimestamp, String tripId, String groupId, Connection conn)
      throws SQLException {

    String trips_sql = "select count(*) as transactions from account_trip_link where tripid like ? and legacy_group_id is " +
                       "null and  created_ts < ?;";
    String groups_sql = "select count(*) as transactions from account_trip_link where tripid like ? and legacy_group_id like " +
                        "?  and created_ts < ?;";

    ArrayList<BillingTripTravellerRS> results = new ArrayList<BillingTripTravellerRS>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {


      if (groupId == null) {
        prep = conn.prepareStatement(trips_sql);
        prep.setString(1, tripId);
        prep.setTimestamp(2, new Timestamp(startTimestamp));
      }
      else {
        prep = conn.prepareStatement(groups_sql);
        prep.setString(1, tripId);
        prep.setString(2, groupId);
        prep.setTimestamp(3, new Timestamp(startTimestamp));
      }

      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          return rs.getInt("transactions");

        }
      }
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
    return 0;
  }
}
