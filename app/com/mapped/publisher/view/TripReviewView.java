package com.mapped.publisher.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-25
 * Time: 11:51 AM
 * To change this template use File | Settings | File Templates.
 */
public class TripReviewView
    extends BaseView {

  /**
   * Current user identifier
   */
  public String userId;
  public String tripId;
  public String tripName;
  public String tripStartDatePrint;
  public String tripEndDatePrint;
  public String tripStartDate;
  public String tripEndDate;
  public String tripCmpyId;
  public int tripStatus;
  public List<TripPassengerInfoView> passengers;
  public List<TripPublishingView> publishList;
  public List<CmpyInfoView> cmpies;
  public Map<String, List<EmailLogView>> tripEmailLogs;

  public List<TripAgentView> agents;

  public String groupId;
  public String groupName;
  public List<TripGroupView> groups;

  public String compositeId;


  public String tripUrl;

  public boolean allowSelfRegistration = true;


  public List <TripBrandingView> tripBranding = new ArrayList<TripBrandingView>();

  /**
   * All relevant collaborators information.
   *
   * @note Remove this field if trip info page no longer features collaboration
   */
  public CollaboratorsView  collabView;
  public boolean collaborationEnabled;

}
