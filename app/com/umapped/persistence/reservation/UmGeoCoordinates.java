package com.umapped.persistence.reservation;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmGeoCoordinates 
  implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public String elevation;

  public String latitude;

  public String longitude;
}
