package com.mapped.publisher.view.billing;

/**
 * Created by twong on 2016-10-27.
 */
public class CreditCardSubView {
  public String subId;
  public String custId;
  public String createdOn;
  public String planId;
  public String name;
  public String amount;
  public String firstBillDate = "N/A";
  public String nextBillDate = "N/A";
  public String paidThru = "N/A";

  public int failureCount = 0;
  public int daysPastDue = 0;
  public boolean isAddOn = false;
  public boolean isActive = false;

  public String braintreeStatus;
  public String braintreeStatusAsOf;


}
