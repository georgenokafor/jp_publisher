package com.mapped.publisher.view.util;

import com.mapped.publisher.view.BookingAndPageView;
import com.mapped.publisher.view.DestinationGuideView;
import com.mapped.publisher.view.DestinationView;
import com.mapped.publisher.view.TripBookingDetailView;
import com.umapped.persistence.enums.ReservationType;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 2015-03-09.
 */
public class Travcoa {

  public static List<DestinationGuideView> getAllItinerary(List<DestinationGuideView> allPages){
    List<DestinationGuideView> pages = new ArrayList<DestinationGuideView>();

    for (DestinationGuideView d: allPages) {
      if ((d.groupedViews != null && d.groupedViews.size() > 0) || (d.name != null && d.name.toLowerCase().startsWith("day "))) {
        pages.add(d);
      }
    }

    if (pages.size() > 1) {
      DestinationGuideView last = pages.get(pages.size() - 1);
      last.rank = 999;
    }

    return pages;
  }

  public static List<DestinationGuideView> getAllBeforeItinerary(List<DestinationGuideView> allPages){
    List<DestinationGuideView> pages = new ArrayList<DestinationGuideView>();

    for (DestinationGuideView d: allPages) {

      if ((d.groupedViews != null && d.groupedViews.size() > 0) || (d.name != null && d.name.toLowerCase().startsWith("day "))) {
        break;
      } else {
        pages.add(d);
      }
    }

    return pages;
  }

  public static List<DestinationGuideView> getAllItineraryAfter(List<DestinationGuideView> allPages){
    List<DestinationGuideView> pages = new ArrayList<DestinationGuideView>();

    boolean foundItinerary = false;

    for (DestinationGuideView d: allPages) {
      if ((d.groupedViews != null && d.groupedViews.size() > 0) || (d.name != null && d.name.toLowerCase().startsWith("day "))) {
        foundItinerary = true;
      } else {
        if (foundItinerary) {
          pages.add(d);
        }
      }
    }

    return pages;
  }

  public static String getHotel (DestinationView mainView, DestinationGuideView day) {
    if (mainView.chronologicalBookingsPages != null && mainView.chronologicalBookingsPages.size() > 0) {
      DateTime targetDate = new DateTime(day.timestamp).withTimeAtStartOfDay();

      for (List<BookingAndPageView> views:  mainView.chronologicalBookingsPages.values()) {
        for (BookingAndPageView b : views) {
          if (b.tripBookingDetailView != null) {
            TripBookingDetailView v = b.tripBookingDetailView;
            if (v.detailsTypeId.getRootLevelType() == ReservationType.HOTEL) {
              DateTime checkin = new DateTime(v.startDateMs).withTimeAtStartOfDay();
              DateTime checkout = new DateTime(v.endDateMs).withTimeAtStartOfDay();
              if (targetDate.getMillis() >= checkin.getMillis() && targetDate.getMillis() < checkout.getMillis() ) {
                return v.name;
              }
            }

          }
        }
      }
    }

    return null;
  }

  public static List<TripBookingDetailView> getHotels (DestinationView mainView) {
    List<TripBookingDetailView> hotels = new ArrayList<>();
    if (mainView.chronologicalBookingsPages != null && mainView.chronologicalBookingsPages.size() > 0) {
      for (List<BookingAndPageView> views:  mainView.chronologicalBookingsPages.values()) {
        for (BookingAndPageView b : views) {
          if (b.tripBookingDetailView != null) {
            TripBookingDetailView v = b.tripBookingDetailView;
            if (v.detailsTypeId.getRootLevelType() == ReservationType.HOTEL) {
              hotels.add(v);
            }

          }
        }
      }
    }

    return hotels;
  }
}
