package modules;

import actors.*;
import com.google.inject.AbstractModule;
import com.mapped.publisher.sys.ActorInit;
import com.mapped.publisher.sys.ActorLoader;
import play.libs.akka.AkkaGuiceSupport;

/**
 * Created by surge on 2016-01-05.
 */
public class ActorsModule
    extends AbstractModule
    implements AkkaGuiceSupport {

  @Override
  protected void configure() {
    bindActor(SupervisorActor.class, SupervisorActor.UmappedActor.SUPERVISOR.getRelativePath());
    bindActorFactory(BillingActor.class, BillingActor.Factory.class);
    bindActorFactory(FirebaseActor.class, FirebaseActor.Factory.class);
    bindActorFactory(CommunicatorReplyActor.class, CommunicatorReplyActor.Factory.class);
    bindActorFactory(CommunicatorToAllActor.class, CommunicatorToAllActor.Factory.class);
    bindActorFactory(CommunicatorSMSActor.class, CommunicatorSMSActor.Factory.class);
    bindActorFactory(CommunicatorDigestActor.class, CommunicatorDigestActor.Factory.class);

    bindActorFactory(DocPDFActor.class, DocPDFActor.Factory.class);
    bindActorFactory(EmailAttachmentParserActor.class, EmailAttachmentParserActor.Factory.class);
    bindActorFactory(EmailBodyParserActor.class, EmailBodyParserActor.Factory.class);
    bindActorFactory(FlightNotifyActor.class, FlightNotifyActor.Factory.class);
    bindActorFactory(FlightPollAlertsActor.class, FlightPollAlertsActor.Factory.class);
    bindActorFactory(FlightTrackActor.class, FlightTrackActor.Factory.class);
    bindActorFactory(IcePortalActor.class, IcePortalActor.Factory.class);
    bindActorFactory(MobilizerActor.class, MobilizerActor.Factory.class);
    bindActorFactory(RecLocatorActor.class, RecLocatorActor.Factory.class);
    bindActorFactory(TripPDFActor.class, TripPDFActor.Factory.class);
    bindActorFactory(VirtuosoCruiseActor.class, VirtuosoCruiseActor.Factory.class);
    bindActorFactory(VirtuosoCruisesActor.class, VirtuosoCruisesActor.Factory.class);

    bindActorFactory(PublishFirebaseActor.class, PublishFirebaseActor.Factory.class);
    bindActorFactory(MobilePushNotificationActor.class, MobilePushNotificationActor.Factory.class);
    bindActorFactory(MaintenanceActor.class, MaintenanceActor.Factory.class);
    bindActorFactory(TripPublisherActor.class, TripPublisherActor.Factory.class);
    bindActorFactory(WetuItineraryActor.class, WetuItineraryActor.Factory.class);

    bindActorFactory(TripAutoCreatePublishActor.class, TripAutoCreatePublishActor.Factory.class);
    bindActorFactory(TripAnalysisActor.class, TripAnalysisActor.Factory.class);


    bind(ActorLoader.class).to(ActorInit.class).asEagerSingleton();
  }
}
