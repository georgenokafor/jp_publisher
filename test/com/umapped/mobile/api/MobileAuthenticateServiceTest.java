package com.umapped.mobile.api;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.umapped.mobile.api.model.AuthenticationResponse;
import com.umapped.mobile.api.model.AuthenticationStatus;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.ApplicationLoader;
import play.Environment;
import play.inject.guice.GuiceApplicationBuilder;
import play.inject.guice.GuiceApplicationLoader;
import play.test.Helpers;

import java.io.IOException;

/**
 * Created by wei on 2017-07-05.
 */
public class MobileAuthenticateServiceTest {
  @Inject Application application;

  @Inject MobileAuthenticateService service;
  @Before
  public void setup() {
    Module testModule = new AbstractModule() {
      @Override
      public void configure() {
        // Install custom test binding here
      }
    };

    GuiceApplicationBuilder builder = new GuiceApplicationLoader()
        .builder(new ApplicationLoader.Context(Environment.simple()))
        .overrides(testModule);
    Guice.createInjector(builder.applicationModule()).injectMembers(this);

    Helpers.start(application);

  }

  @After
  public void teardown() {
    Helpers.stop(application);
  }

  //@Test
  public void testValidPassword() throws IOException {
    AuthenticationResponse response = service.authenticate("thierry+1@umapped.com", "password");
    Assert.assertEquals(AuthenticationStatus.SUCCESSFUL, response.getStatus());
    Assert.assertNotNull(response.getJwtToken());
  }
}
