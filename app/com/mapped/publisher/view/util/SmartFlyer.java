package com.mapped.publisher.view.util;

import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.EnhancedTripBookingDetailView;
import com.mapped.publisher.view.TripBookingDetailView;
import com.mapped.publisher.view.TripBookingView;
import com.mapped.publisher.view.TripPreviewView;
import com.mapped.publisher.view.smartflyer.SmartFlyerDetailView;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.flight.UmFlightTraveler;
import org.apache.commons.lang3.time.DateUtils;

import java.util.*;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

/**
 * Created by twong on 2014-09-18.
 */
public class SmartFlyer {

  public static String getCountry (String tripName) {
    if (tripName != null && tripName.indexOf(" - ") > 0) {
      return tripName.substring(0, tripName.indexOf(" - ")).trim();
    }
    return "";
  }

  public static String getTitle (String tripName) {
    int startIndex = 0;
    int endIndex = tripName.length();

    if (tripName.indexOf(" - ") > 0)
      startIndex = tripName.indexOf(" - ") + 3;

    if (tripName.toLowerCase().indexOf(" for ") > 0)
        endIndex = tripName.toLowerCase().indexOf(" for ");

    if (startIndex > endIndex) {
      startIndex = 0;
    }

    return tripName.substring(startIndex, endIndex).trim();

  }

  public static String getPassenger (String tripName) {
    if (tripName != null && tripName.toLowerCase().indexOf(" for ") > 0) {
      return tripName.substring(tripName.toLowerCase().indexOf(" for ") + 5).trim();
    }
    return "";
  }

  public static SmartFlyerDetailView parseFlight (TripBookingDetailView view) {
    SmartFlyerDetailView flightView = new SmartFlyerDetailView();

    if (view != null && view.comments != null && view.comments.trim().length() > 0) {
      flightView.passengers = getPassengerInfo(view);

      String s = view.comments.replaceAll("\n", "");
      s = s.replaceAll("<br/>", "<br>");
      String[] tokens = s.split("<br>");
      StringBuilder sb = new StringBuilder();
      for (String token: tokens) {
        if (token.startsWith("Departure Terminal:")) {
          flightView.departTerminal = token.substring(token.indexOf("Departure Terminal:") + 19).trim();

        }
        if (token.startsWith("Departure Terminal")) {
          flightView.departTerminal = token.substring(token.indexOf("Departure Terminal") + 18).trim();

        }
        if (token.startsWith("Arrival Terminal:") ) {
          flightView.arriveTerminal = token.substring(token.indexOf("Arrival Terminal:") + 17).trim();
        }
        if (token.startsWith("Arrival Terminal") ) {
          flightView.arriveTerminal = token.substring(token.indexOf("Arrival Terminal") + 16).trim();
        }
        if (token.startsWith("Duration:") ) {
          flightView.duration = token.substring(token.indexOf("Duration:") + 9).trim();
        }

        if (token.startsWith("Distance:") ) {
          flightView.distance = token.substring(token.indexOf("Distance:") + 9).trim();
        }
        if (token.startsWith("Stops:") ) {
          flightView.stops = token.substring(token.indexOf("Stops:") + 6).trim();
        }


        if (token.startsWith("Aircraft:") ) {
          flightView.aircraft = token.substring(token.indexOf("Aircraft:") + 9).trim();
        }

        boolean addToken = true;
        if (token.trim().length() == 0) {
          addToken = false;
        } else if (view.comments.contains("Passengers:") && (token.toLowerCase().startsWith("passengers") || token.toLowerCase().startsWith("seat") || token.toLowerCase().startsWith("name")
                                                             || token.toLowerCase().startsWith("class") || token.toLowerCase().startsWith("eticket") || token.toLowerCase().startsWith("frequent") || token.toLowerCase().startsWith("meal"))) {
          addToken = false;
        } else if (token.toLowerCase().startsWith("departure terminal") || token.toLowerCase().startsWith("arrival terminal")
                   || token.toLowerCase().startsWith("duration:") || token.toLowerCase().startsWith("aircraft:") || token.toLowerCase().startsWith("distance:") || token.toLowerCase().startsWith("stops:")) {
          addToken = false;
        }

        if (addToken) {
          sb.append(token);sb.append("<br>");
        }


      }
      flightView.comments = sb.toString();
    }
      return flightView;
  }


  public static SmartFlyerDetailView parseHotel (TripBookingDetailView view) {
    SmartFlyerDetailView hotelView = new SmartFlyerDetailView();
    hotelView.city = view.city;
    hotelView.country = view.country;
    hotelView.providerPhone = view.providerPhone;
    hotelView.state = view.state;
    hotelView.providerAddr1 = view.providerAddr1;
    hotelView.providerAddr2 = view.providerAddr2;
    hotelView.zip = view.zip;
    hotelView.providerWeb = view.providerWeb;


    if (view != null && view.comments != null && view.comments.trim().length() > 0) {
      if (view != null && view.comments  != null && view.comments.startsWith("Address:")) {
        String s  = view.comments.substring(view.comments.indexOf("Address:") + 8);
        s = s.replaceAll("\n", "");
        s = s.replaceAll("<br/>", "<br>");

        String[] tokens = s.split("<br>");
        int i =0 ;
        int j = 0;
        for (String token:tokens) {
          if (token.trim().length() == 0 && i > 0) {
            break;
          } else if (j==0 && token.trim().length() > 0 ) {
            hotelView.providerAddr1 = token;
            j++;
          } else if (j  == 1 && token.trim().length() > 0) {
            hotelView.providerAddr2 = token;
          }

          i++;

        }
      }
    }

    if (view.comments != null && view.comments.contains("Phone:")) {
      String s = view.comments.substring(view.comments.indexOf("Phone:") + 6).trim();
      s = s.replaceAll("\n", "");
      s = s.replaceAll("<br/>", "<br>");
      if (s.contains("<br>")) {
        hotelView.providerPhone = s.substring(0, s.indexOf("<br>"));

      }
    }
    if (view.comments != null && view.comments.contains("Web:")) {
      String s = view.comments.substring(view.comments.indexOf("Web:") + 6).trim();
      s = s.replaceAll("\n", "");
      s = s.replaceAll("<br/>", "<br>");
      if (s.contains("<br>")) {
        hotelView.providerWeb = s.substring(0, s.indexOf("<br>"));

      }
    }
    if (view.comments != null && view.comments.contains("Fax:")) {
      String s = view.comments.substring(view.comments.indexOf("Fax:") + 6).trim();
      s = s.replaceAll("\n", "");
      s = s.replaceAll("<br/>", "<br>");
      if (s.contains("<br>")) {
        hotelView.providerFax = s.substring(0, s.indexOf("<br>"));

      }
    }


    if (view.comments != null && view.comments.contains("Room Details")) {
      String s = view.comments.substring(view.comments.indexOf("Room Details")).trim();
      s = s.replaceAll("\n", "");
      s = s.replaceAll("<br/>", "<br>");
      int i = s.indexOf("<br>");
      if (i > 0) {
        hotelView.comments = s.substring(i + 4).trim();
      } else {
        hotelView.comments = view.comments.substring(view.comments.indexOf("Room Details")).trim();
      }
    } else if (view.comments != null){
      //sanitize comments by removing any duplicate info
      String s  = view.comments;
      s = s.replaceAll("\n", "");
      s = s.replaceAll("<br/>", "<br>");

      String[] tokens = s.split("<br>");
      StringBuilder sb = new StringBuilder();

      for (String s1: tokens) {
        boolean add = true;
        if (s1.startsWith("Fax:") || s1.startsWith("Address:") || s1.startsWith("Web:") || s1.startsWith("Phone:") || s1.startsWith("Fax:")) {
          add = false;
        } else if ( (hotelView.providerAddr1 != null && hotelView.providerAddr1.trim().length()  == s1.trim().length() && s1.contains(hotelView.providerAddr1) ) || (hotelView.providerAddr2 != null && hotelView.providerAddr2.trim().length()  == s1.trim().length() && s1.contains(hotelView.providerAddr2))
                 || (hotelView.providerFax != null && hotelView.providerFax.trim().length()  == s1.trim().length()  && s1.contains(hotelView.providerFax))  || (hotelView.providerPhone != null && hotelView.providerPhone.trim().length() == s1.trim().length()  && s1.contains(hotelView.providerPhone))
                 || (hotelView.providerWeb != null && hotelView.providerWeb.trim().length()  == s1.trim().length() && s1.contains(hotelView.providerWeb)) || (hotelView.city != null && hotelView.city.trim().length() == s1.trim().length() && s1.contains(hotelView.city))
                 || (hotelView.zip != null && hotelView.zip.trim().length()  == s1.trim().length() &&  s1.contains(hotelView.zip))) {
          add = false;

        }

        if (add) {
          if (s1.trim().length() == 0) {
            if (sb.toString().length() > 0) {
              sb.append(s1);
              sb.append("<br/>");
            }
          } else {
            sb.append(s1);
            sb.append("<br/>");
          }
        }
      }

      hotelView.comments = sb.toString();
    }

    if(view.startDateMs > 1 && view.endDateMs > view.startDateMs) {
      double d = view.endDateMs - view.startDateMs;
      d = d / (24 * 60 * 60 * 1000);
      int days = (int)Math.ceil(d);
      if (days > 1 )
        hotelView.numNights = String.valueOf(days) + " NIGHTS";
      else
        hotelView.numNights = "1 NIGHT";
    }

    return hotelView;
  }


  public static SmartFlyerDetailView parseCruise (TripBookingView bookingView,  TripBookingDetailView view) {
    SmartFlyerDetailView cruiseView = new SmartFlyerDetailView();
    cruiseView.cruiseStops = new ArrayList<SmartFlyerDetailView.CruiseStop>();

    if (bookingView != null && bookingView.activities != null && view != null) {
      //extract all cruise stops that belong to this cruise
      for (TripBookingDetailView activity : bookingView.activities) {
        if (activity.detailsTypeId == ReservationType.CRUISE_STOP && activity.startDateMs > view.startDateMs && activity.startDateMs < view.endDateMs) {
          SmartFlyerDetailView.CruiseStop stop = new SmartFlyerDetailView.CruiseStop();
          stop.arriveTime = activity.startDatePrint;
          if (activity.startTime != null && activity.startTime.trim().length() > 0) {
            stop.arriveTime += " - " + activity.startTime;

          }
          stop.date = activity.startDate;
          stop.day = " -- ";
          stop.itinerary = activity.providerName;
          stop.departTime = activity.endDatePrint;
          if (activity.endTime != null && activity.endTime.trim().length() > 0) {
            stop.departTime += " - " + activity.endTime;

          }
          cruiseView.cruiseStops.add(stop);

        }
      }
    }

    if (view != null) {
      if(view.startDateMs > 1 && view.endDateMs > view.startDateMs) {
        double d = view.endDateMs - view.startDateMs;
        d = d / (24 * 60 * 60 * 1000);
        int days = (int)Math.ceil(d);
        if (days > 1 )
          cruiseView.numNights = String.valueOf(days) + " Days";
        else
          cruiseView.numNights = "1 Day";
      }
    }

    return cruiseView;


  }


  public static boolean displayDate (TripPreviewView v, String date) {
    int count = 0;

    if (v.bookings != null) {
      for (TripBookingDetailView detail :  v.bookings.chronologicalBookings.get(date)) {
        if (detail.detailsTypeId != ReservationType.CRUISE_STOP || (detail.detailsTypeId == ReservationType.CRUISE_STOP && detail.comments != null && detail.comments.trim().length() > 0)) {
          count++;
        }
      }
    }
    if (count > 0)
      return true;
    else
      return false;
  }


  public static List<SmartFlyerDetailView.PassengerInfo> getPassengerInfo(TripBookingDetailView view) {
    List<SmartFlyerDetailView.PassengerInfo> passengers = new ArrayList<SmartFlyerDetailView.PassengerInfo>();

    UmFlightReservation flight = cast(view.reservation, UmFlightReservation.class);

    if (view != null && flight != null && flight.getTravelers() != null) {

      for (UmTraveler t : flight.getTravelers()) {
        UmFlightTraveler ft = cast(t, UmFlightTraveler.class);
        if (ft != null) {
          SmartFlyerDetailView.PassengerInfo passenger = new SmartFlyerDetailView.PassengerInfo();
          passengers.add(passenger);

          passenger.name = ft.getName();
          passenger.seat = ft.getSeat();
          passenger.fClass = ft.getSeatClass().seatCategory;

          passenger.eticket = ft.getTicketNumber();
          passenger.frequentflyer = ft.getProgram().membershipNumber;
          passenger.meal = ft.getMeal();
        }
      }
    }
    return passengers;
  }




  public static String getValue (String s) {
    if (s != null && s.contains(":")) {
      return s.substring(s.indexOf(":") + 1).trim();
    }
    return "";
  }

  public static void updateChronologicalBookings (TripBookingView m) {
    if (m.chronologicalBookings != null && m.chronologicalDates != null && m.transfers != null) {
      ArrayList<TripBookingDetailView> rentals = new ArrayList<TripBookingDetailView>();

      for (TripBookingDetailView v : m.transfers) {
        if (v.detailsTypeId == ReservationType.CAR_RENTAL && v.startDate != null && v.endDate != null && !v.startDate.equals(v.endDate)) {
          rentals.add(v);
        }
      }

      boolean sortNeeded = false;

      for (TripBookingDetailView v : rentals) {
        String date = Utils.formatDatePrint(v.endDateMs);
        if (m.chronologicalDates.contains(date)) {
          m.chronologicalBookings.get(date).add(v);
          List<TripBookingDetailView> bookings = new ArrayList<>();
          List<TripBookingDetailView> newVersion = new ArrayList<>();

          List<TripBookingDetailView> notesForLinkedBookings = new ArrayList<>();


          bookings.addAll(m.chronologicalBookings.get(date));
          for (TripBookingDetailView v1 : m.chronologicalBookings.get(date)) {
            if (v1.noteId != null && v1.noteId > 0 && v1.detailsId != null && v1.detailsId.trim().length() > 0) {
              bookings.remove(v1);
              notesForLinkedBookings.add(v1);
            }
          }


          //resort the chronological bookings
          Collections.sort(bookings, new Comparator<TripBookingDetailView>() {
            @Override public int compare(TripBookingDetailView t1, TripBookingDetailView t2) {
              long origT1 = t1.startDateMs;
              long origT2 = t2.startDateMs;
              //figure out if we should use start or end date for comparison
              long diff = origT1 - origT2;
              if (diff < 0) {
                diff *= -1;
                if (diff > (24 * 60 * 60 * 1000)) {
                  //greater than a day - try again with end date
                  origT1 = t1.endDateMs;
                }
                else if (diff > 0) {
                  if (diff > (24 * 60 * 60 * 1000)) {
                    //greater than a day - try again with end date
                    origT2 = t2.endDateMs;
                  }
                }
              }
              if ((t1.rank > 0 || t2.rank > 0) && (t1.noteId == null || t1.noteId == 0) && (t2.noteId == null || t2.noteId == 0)) {
                if (t1.rank > t2.rank) {
                  return 1;
                }
                else if (t1.rank < t2.rank) {
                  return -1;
                }
                return 0;
              }
              else {
                if (origT1 > origT2) {
                  return 1;
                }
                else if (origT1 < origT2) {
                  return -1;
                }
                else {
                  if (t1.rank > t2.rank) {
                    return 1;
                  }
                  else if (t1.rank < t2.rank) {
                    return -1;
                  }
                  return 0;
                }
              }
            }
          });
          //reattach notes
          ///after everything is sorted, we go back to add the notes bookings to the right spot
          for (TripBookingDetailView booking: bookings) {
            newVersion.add(booking);
            for (TripBookingDetailView note: notesForLinkedBookings) {
              if (note.detailsId != null && note.linkedTripDetailsId.equals(booking.detailsId)) {
                newVersion.add(note);
              }
            }
          }

          m.chronologicalBookings.put(date, newVersion);

        } else {
          //insert new date
          m.chronologicalDates.add(date);
          ArrayList<TripBookingDetailView> newRentals = new ArrayList<TripBookingDetailView>();
          newRentals.add(v);
          m.chronologicalBookings.put(date, newRentals);
          sortNeeded = true;
        }
      }

      if (sortNeeded) {
        Collections.sort(m.chronologicalDates, new Comparator<String>() {
                           @Override public int compare(String o1, String o2) {
                             long sDate = getMillis(o1);
                             long eDate = getMillis(o2);
                             if (sDate > eDate)
                               return 1;
                             else if (sDate < eDate)
                               return -1;
                             else
                             return 0;
                           }
                         }
                         );
      }
    }
  }



  public static long getMillis (String s) {
    try {
      String[] formatters = new String[2];
      formatters[0] = "EEE MMM dd, yyyy";

      Date date = DateUtils.parseDate(s, formatters);
      return date.getTime();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return 0;

  }

}
