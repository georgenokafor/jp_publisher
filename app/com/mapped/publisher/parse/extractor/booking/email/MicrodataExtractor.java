package com.mapped.publisher.parse.extractor.booking.email;

import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.EmailBodyExtractor;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.utils.Log;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by surge on 2015-05-20.
 */
public class MicrodataExtractor
    extends EmailBodyExtractor {
  @Override protected void initImpl(Map<String, String> params) {

  }

  @Override protected TripVO extractDataImpl( String htmlInput)
      throws Exception {

    ReservationsHolder rh = SemanticParser.parseMicrodata(htmlInput);

    if (!rh.hasValidReservations()) {
      Log.err("MICRODATA: No Usable Information");
    }

    if(rh != null) {
      return rh.toTripVO(null);
    }

    return null;
  }

  /**
   * Access to the list of parse errors.
   *
   * @return list of parse errors, null if class does not support error reporting
   * @note must be called only after calling @see extractData() method
   */
  @Override public ArrayList<ParseError> getParseErrors() {
    return null;
  }
}
