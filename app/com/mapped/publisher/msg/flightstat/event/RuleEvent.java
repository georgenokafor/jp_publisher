
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class RuleEvent{
   	private String type;
    private String value;

 	public String getType(){
		return this.type;
	}
	public void setType(String type){
		this.type = type;
	}

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
