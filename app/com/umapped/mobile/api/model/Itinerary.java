package com.umapped.mobile.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.parse.globus.List;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;

import java.io.Serializable;

/**
 * Created by wei on 2017-06-30.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Itinerary implements Serializable {

  private ItineraryAuthorization authorization;
  private ItineraryInfo itineraryInfo;
  private ReservationPackage reservations;
  private ItineraryDocuments documents;
  private ItineraryMap map;
  private ItinearyAnalysisResult analysisResult;

  public ItineraryAuthorization getAuthorization() {
    return authorization;
  }

  public void setAuthorization(ItineraryAuthorization authorization) {
    this.authorization = authorization;
  }

  public ItineraryInfo getItineraryInfo() {
    return itineraryInfo;
  }

  public void setItineraryInfo(ItineraryInfo itineraryInfo) {
    this.itineraryInfo = itineraryInfo;
  }

  public ReservationPackage getReservations() {
    return reservations;
  }

  public void setReservations(ReservationPackage reservations) {
    this.reservations = reservations;
  }

  public ItineraryDocuments getDocuments() {
    return documents;
  }

  public void setDocuments(ItineraryDocuments documents) {
    this.documents = documents;
  }

  public ItineraryMap getMap() {
    return map;
  }

  public void setMap(ItineraryMap map) {
    this.map = map;
  }

  public ItinearyAnalysisResult getAnalysisResult() {
    return analysisResult;
  }

  public void setAnalysisResult(ItinearyAnalysisResult analysisResult) {
    this.analysisResult = analysisResult;
  }
}
