
package com.umapped.external.sendgrid.webhooks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "response",
    "reason",
    "sg_event_id",
    "sg_message_id",
    "sg_user_id",
    "ip",
    "useragent",
    "event",
    "email",
    "timestamp",
    "url",
    "url_offset",
    "unique_arg_key",
    "category",
    "attempt",
    "newsletter",
    "asm_group_id",
    "tls",
    "cert_err",
    "send_at",
    "marketing_campaign_id",
    "marketing_campaign_name",
    "marketing_campaign_version",
    "marketing_campaign_split_id",
    "post_type",
    "msg_id"
})
public class EventWebhook {

    @JsonProperty("response")
    private String response;
    @JsonProperty("reason")
    private String reason;
    @JsonProperty("sg_event_id")
    private String sgEventId;
    @JsonProperty("sg_message_id")
    private String sgMessageId;
    @JsonProperty("sg_user_id")
    private int sgUserId;
    @JsonProperty("ip")
    private String ip;
    @JsonProperty("useragent")
    private String useragent;
    @JsonProperty("event")
    private String event;
    @JsonProperty("email")
    private String email;
    @JsonProperty("timestamp")
    private long timestamp;
    @JsonProperty("url")
    private String url;
    @JsonProperty("url_offset")
    private UrlOffset urlOffset;
    @JsonProperty("unique_arg_key")
    private String uniqueArgKey;
    @JsonProperty("category")
    private String category;
    @JsonProperty("attempt")
    private String attempt;
    @JsonProperty("newsletter")
    private Newsletter newsletter;
    @JsonProperty("asm_group_id")
    private int asmGroupId;
    @JsonProperty("tls")
    private String tls;
    @JsonProperty("cert_err")
    private String certErr;
    @JsonProperty("send_at")
    private int sendAt;
    @JsonProperty("marketing_campaign_id")
    private int marketingCampaignId;
    @JsonProperty("marketing_campaign_name")
    private String marketingCampaignName;
    @JsonProperty("marketing_campaign_version")
    private String marketingCampaignVersion;
    @JsonProperty("marketing_campaign_split_id")
    private int marketingCampaignSplitId;
    @JsonProperty("post_type")
    private String postType;
    @JsonProperty("msg_id")
    private long msgId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The response
     */
    @JsonProperty("response")
    public String getResponse() {
        return response;
    }

    /**
     * 
     * @param response
     *     The response
     */
    @JsonProperty("response")
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * 
     * @return
     *     The reason
     */
    @JsonProperty("reason")
    public String getReason() {
        return reason;
    }

    /**
     * 
     * @param reason
     *     The reason
     */
    @JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * 
     * @return
     *     The sgEventId
     */
    @JsonProperty("sg_event_id")
    public String getSgEventId() {
        return sgEventId;
    }

    /**
     * 
     * @param sgEventId
     *     The sg_event_id
     */
    @JsonProperty("sg_event_id")
    public void setSgEventId(String sgEventId) {
        this.sgEventId = sgEventId;
    }

    /**
     * 
     * @return
     *     The sgMessageId
     */
    @JsonProperty("sg_message_id")
    public String getSgMessageId() {
        return sgMessageId;
    }

    /**
     * 
     * @param sgMessageId
     *     The sg_message_id
     */
    @JsonProperty("sg_message_id")
    public void setSgMessageId(String sgMessageId) {
        this.sgMessageId = sgMessageId;
    }

    /**
     * 
     * @return
     *     The sgUserId
     */
    @JsonProperty("sg_user_id")
    public int getSgUserId() {
        return sgUserId;
    }

    /**
     * 
     * @param sgUserId
     *     The sg_user_id
     */
    @JsonProperty("sg_user_id")
    public void setSgUserId(int sgUserId) {
        this.sgUserId = sgUserId;
    }

    /**
     * 
     * @return
     *     The ip
     */
    @JsonProperty("ip")
    public String getIp() {
        return ip;
    }

    /**
     * 
     * @param ip
     *     The ip
     */
    @JsonProperty("ip")
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * 
     * @return
     *     The useragent
     */
    @JsonProperty("useragent")
    public String getUseragent() {
        return useragent;
    }

    /**
     * 
     * @param useragent
     *     The useragent
     */
    @JsonProperty("useragent")
    public void setUseragent(String useragent) {
        this.useragent = useragent;
    }

    /**
     * 
     * @return
     *     The event
     */
    @JsonProperty("event")
    public String getEvent() {
        return event;
    }

    /**
     * 
     * @param event
     *     The event
     */
    @JsonProperty("event")
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * 
     * @return
     *     The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The timestamp
     */
    @JsonProperty("timestamp")
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * 
     * @param timestamp
     *     The timestamp
     */
    @JsonProperty("timestamp")
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 
     * @return
     *     The urlOffset
     */
    @JsonProperty("url_offset")
    public UrlOffset getUrlOffset() {
        return urlOffset;
    }

    /**
     * 
     * @param urlOffset
     *     The url_offset
     */
    @JsonProperty("url_offset")
    public void setUrlOffset(UrlOffset urlOffset) {
        this.urlOffset = urlOffset;
    }

    /**
     * 
     * @return
     *     The uniqueArgKey
     */
    @JsonProperty("unique_arg_key")
    public String getUniqueArgKey() {
        return uniqueArgKey;
    }

    /**
     * 
     * @param uniqueArgKey
     *     The unique_arg_key
     */
    @JsonProperty("unique_arg_key")
    public void setUniqueArgKey(String uniqueArgKey) {
        this.uniqueArgKey = uniqueArgKey;
    }

    /**
     * 
     * @return
     *     The category
     */
    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    /**
     * 
     * @param category
     *     The category
     */
    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * 
     * @return
     *     The attempt
     */
    @JsonProperty("attempt")
    public String getAttempt() {
        return attempt;
    }

    /**
     * 
     * @param attempt
     *     The attempt
     */
    @JsonProperty("attempt")
    public void setAttempt(String attempt) {
        this.attempt = attempt;
    }

    /**
     * 
     * @return
     *     The newsletter
     */
    @JsonProperty("newsletter")
    public Newsletter getNewsletter() {
        return newsletter;
    }

    /**
     * 
     * @param newsletter
     *     The newsletter
     */
    @JsonProperty("newsletter")
    public void setNewsletter(Newsletter newsletter) {
        this.newsletter = newsletter;
    }

    /**
     * 
     * @return
     *     The asmGroupId
     */
    @JsonProperty("asm_group_id")
    public int getAsmGroupId() {
        return asmGroupId;
    }

    /**
     * 
     * @param asmGroupId
     *     The asm_group_id
     */
    @JsonProperty("asm_group_id")
    public void setAsmGroupId(int asmGroupId) {
        this.asmGroupId = asmGroupId;
    }

    /**
     * 
     * @return
     *     The tls
     */
    @JsonProperty("tls")
    public String getTls() {
        return tls;
    }

    /**
     * 
     * @param tls
     *     The tls
     */
    @JsonProperty("tls")
    public void setTls(String tls) {
        this.tls = tls;
    }

    /**
     * 
     * @return
     *     The certErr
     */
    @JsonProperty("cert_err")
    public String getCertErr() {
        return certErr;
    }

    /**
     * 
     * @param certErr
     *     The cert_err
     */
    @JsonProperty("cert_err")
    public void setCertErr(String certErr) {
        this.certErr = certErr;
    }

    /**
     * 
     * @return
     *     The sendAt
     */
    @JsonProperty("send_at")
    public int getSendAt() {
        return sendAt;
    }

    /**
     * 
     * @param sendAt
     *     The send_at
     */
    @JsonProperty("send_at")
    public void setSendAt(int sendAt) {
        this.sendAt = sendAt;
    }

    /**
     * 
     * @return
     *     The marketingCampaignId
     */
    @JsonProperty("marketing_campaign_id")
    public int getMarketingCampaignId() {
        return marketingCampaignId;
    }

    /**
     * 
     * @param marketingCampaignId
     *     The marketing_campaign_id
     */
    @JsonProperty("marketing_campaign_id")
    public void setMarketingCampaignId(int marketingCampaignId) {
        this.marketingCampaignId = marketingCampaignId;
    }

    /**
     * 
     * @return
     *     The marketingCampaignName
     */
    @JsonProperty("marketing_campaign_name")
    public String getMarketingCampaignName() {
        return marketingCampaignName;
    }

    /**
     * 
     * @param marketingCampaignName
     *     The marketing_campaign_name
     */
    @JsonProperty("marketing_campaign_name")
    public void setMarketingCampaignName(String marketingCampaignName) {
        this.marketingCampaignName = marketingCampaignName;
    }

    /**
     * 
     * @return
     *     The marketingCampaignVersion
     */
    @JsonProperty("marketing_campaign_version")
    public String getMarketingCampaignVersion() {
        return marketingCampaignVersion;
    }

    /**
     * 
     * @param marketingCampaignVersion
     *     The marketing_campaign_version
     */
    @JsonProperty("marketing_campaign_version")
    public void setMarketingCampaignVersion(String marketingCampaignVersion) {
        this.marketingCampaignVersion = marketingCampaignVersion;
    }

    /**
     * 
     * @return
     *     The marketingCampaignSplitId
     */
    @JsonProperty("marketing_campaign_split_id")
    public int getMarketingCampaignSplitId() {
        return marketingCampaignSplitId;
    }

    /**
     * 
     * @param marketingCampaignSplitId
     *     The marketing_campaign_split_id
     */
    @JsonProperty("marketing_campaign_split_id")
    public void setMarketingCampaignSplitId(int marketingCampaignSplitId) {
        this.marketingCampaignSplitId = marketingCampaignSplitId;
    }

    /**
     * 
     * @return
     *     The postType
     */
    @JsonProperty("post_type")
    public String getPostType() {
        return postType;
    }

    /**
     * 
     * @param postType
     *     The post_type
     */
    @JsonProperty("post_type")
    public void setPostType(String postType) {
        this.postType = postType;
    }

    @JsonProperty("msg_id")
    public long getMsgId() {
        return msgId;
    }

    @JsonProperty("msg_id")
    public void setMsgId(long msgId) {
        this.msgId = msgId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(response).append(reason).append(sgEventId).append(sgMessageId).append(sgUserId).append(ip).append(useragent).append(event).append(email).append(timestamp).append(url).append(urlOffset).append(uniqueArgKey).append(category).append(attempt).append(newsletter).append(asmGroupId).append(tls).append(certErr).append(sendAt).append(marketingCampaignId).append(marketingCampaignName).append(marketingCampaignVersion).append(marketingCampaignSplitId).append(postType).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof EventWebhook) == false) {
            return false;
        }
        EventWebhook rhs = ((EventWebhook) other);
        return new EqualsBuilder().append(response, rhs.response).append(reason, rhs.reason).append(sgEventId, rhs.sgEventId).append(sgMessageId, rhs.sgMessageId).append(sgUserId, rhs.sgUserId).append(ip, rhs.ip).append(useragent, rhs.useragent).append(event, rhs.event).append(email, rhs.email).append(timestamp, rhs.timestamp).append(url, rhs.url).append(urlOffset, rhs.urlOffset).append(uniqueArgKey, rhs.uniqueArgKey).append(category, rhs.category).append(attempt, rhs.attempt).append(newsletter, rhs.newsletter).append(asmGroupId, rhs.asmGroupId).append(tls, rhs.tls).append(certErr, rhs.certErr).append(sendAt, rhs.sendAt).append(marketingCampaignId, rhs.marketingCampaignId).append(marketingCampaignName, rhs.marketingCampaignName).append(marketingCampaignVersion, rhs.marketingCampaignVersion).append(marketingCampaignSplitId, rhs.marketingCampaignSplitId).append(postType, rhs.postType).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
