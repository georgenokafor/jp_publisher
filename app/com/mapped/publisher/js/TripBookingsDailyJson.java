package com.mapped.publisher.js;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.mapped.publisher.view.TripBookingDetailView;
import com.umapped.api.schema.types.ReturnCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Trip bookings information presented as a list of ordered days
 * Created by surge on 2016-04-14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TripBookingsDailyJson
    extends BaseJsonResponse
    implements Serializable {

  public List<TripBookingDetailView> notes;
  public List<DayBookings>           days;

  public ReservationPackage          reservations;

  public static class DayBookings
      implements Serializable {
    /**
     * Trip Date in ISO8601 Format
     */
    public String                      date;
    public List<TripBookingDetailView> bookings;
  }

  public TripBookingsDailyJson(ReturnCode rc) {
    super(rc);
    notes = null;
    days = new ArrayList<>();
  }
}
