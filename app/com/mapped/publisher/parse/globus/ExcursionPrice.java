
package com.mapped.publisher.parse.globus;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ExcursionPrice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExcursionPrice">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webapi.globusandcosmos.com/}DataObject">
 *       &lt;sequence>
 *         &lt;element name="AdultPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ChildPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="PrintStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="PrintEndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ExcursionId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ExcursionPriceId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MaxChildDiscountAge" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PrivateAdultPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PrivateChildPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="GroupPrice" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExcursionPrice", propOrder = {
    "adultPrice",
    "childPrice",
    "currencyCode",
    "primaryCurrency",
    "effectiveDate",
    "printStartDate",
    "printEndDate",
    "excursionId",
    "excursionPriceId",
    "maxChildDiscountAge",
    "privateAdultPrice",
    "privateChildPrice",
    "groupPrice"
})
public class ExcursionPrice
    extends DataObject
{

    @XmlElement(name = "AdultPrice", required = true)
    protected BigDecimal adultPrice;
    @XmlElement(name = "ChildPrice", required = true)
    protected BigDecimal childPrice;
    @XmlElement(name = "CurrencyCode")
    protected String currencyCode;
    @XmlElement(name = "PrimaryCurrency")
    protected String primaryCurrency;
    @XmlElement(name = "EffectiveDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElement(name = "PrintStartDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar printStartDate;
    @XmlElement(name = "PrintEndDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar printEndDate;
    @XmlElement(name = "ExcursionId")
    protected int excursionId;
    @XmlElement(name = "ExcursionPriceId")
    protected int excursionPriceId;
    @XmlElement(name = "MaxChildDiscountAge")
    protected int maxChildDiscountAge;
    @XmlElement(name = "PrivateAdultPrice", required = true)
    protected BigDecimal privateAdultPrice;
    @XmlElement(name = "PrivateChildPrice", required = true)
    protected BigDecimal privateChildPrice;
    @XmlElement(name = "GroupPrice", required = true)
    protected BigDecimal groupPrice;

    /**
     * Gets the value of the adultPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAdultPrice() {
        return adultPrice;
    }

    /**
     * Sets the value of the adultPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAdultPrice(BigDecimal value) {
        this.adultPrice = value;
    }

    /**
     * Gets the value of the childPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChildPrice() {
        return childPrice;
    }

    /**
     * Sets the value of the childPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChildPrice(BigDecimal value) {
        this.childPrice = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the primaryCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryCurrency() {
        return primaryCurrency;
    }

    /**
     * Sets the value of the primaryCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryCurrency(String value) {
        this.primaryCurrency = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the printStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPrintStartDate() {
        return printStartDate;
    }

    /**
     * Sets the value of the printStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPrintStartDate(XMLGregorianCalendar value) {
        this.printStartDate = value;
    }

    /**
     * Gets the value of the printEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPrintEndDate() {
        return printEndDate;
    }

    /**
     * Sets the value of the printEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPrintEndDate(XMLGregorianCalendar value) {
        this.printEndDate = value;
    }

    /**
     * Gets the value of the excursionId property.
     * 
     */
    public int getExcursionId() {
        return excursionId;
    }

    /**
     * Sets the value of the excursionId property.
     * 
     */
    public void setExcursionId(int value) {
        this.excursionId = value;
    }

    /**
     * Gets the value of the excursionPriceId property.
     * 
     */
    public int getExcursionPriceId() {
        return excursionPriceId;
    }

    /**
     * Sets the value of the excursionPriceId property.
     * 
     */
    public void setExcursionPriceId(int value) {
        this.excursionPriceId = value;
    }

    /**
     * Gets the value of the maxChildDiscountAge property.
     * 
     */
    public int getMaxChildDiscountAge() {
        return maxChildDiscountAge;
    }

    /**
     * Sets the value of the maxChildDiscountAge property.
     * 
     */
    public void setMaxChildDiscountAge(int value) {
        this.maxChildDiscountAge = value;
    }

    /**
     * Gets the value of the privateAdultPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrivateAdultPrice() {
        return privateAdultPrice;
    }

    /**
     * Sets the value of the privateAdultPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrivateAdultPrice(BigDecimal value) {
        this.privateAdultPrice = value;
    }

    /**
     * Gets the value of the privateChildPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrivateChildPrice() {
        return privateChildPrice;
    }

    /**
     * Sets the value of the privateChildPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrivateChildPrice(BigDecimal value) {
        this.privateChildPrice = value;
    }

    /**
     * Gets the value of the groupPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGroupPrice() {
        return groupPrice;
    }

    /**
     * Sets the value of the groupPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGroupPrice(BigDecimal value) {
        this.groupPrice = value;
    }

}
