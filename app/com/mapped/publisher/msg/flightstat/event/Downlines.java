package com.mapped.publisher.msg.flightstat.event;

/**
 * Created by twong on 2015-01-27.
 */
public class Downlines {
  private Downline upline;

  public Downline getUpline() {
    return upline;
  }

  public void setUpline(Downline upline) {
    this.upline = upline;
  }
}
