package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 12-07-26
 * Time: 10:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class SharedMapsMgrExt extends SharedMapsMgr{
    private static final String SELECT_ACTIVE_SHARED_MAPS_BY_EMAIL = "Select Pk, MapsId, Owner, ShareUserId, shareType, Status, LastUpdateTimestamp, CreateTimestamp, ShareEmail, SyncTimestamp, Msg from SharedMaps where shareemail=? and (SyncTimestamp = 0 or SyncTimestamp > ?);";
    private static final String SELECT_ACTIVE_SHARED_MAPS_BY_OWNER = "Select Pk, MapsId, Owner, ShareUserId, shareType, Status, LastUpdateTimestamp, CreateTimestamp, ShareEmail, SyncTimestamp, Msg from SharedMaps where Owner=? and (SyncTimestamp = 0 or SyncTimestamp > ?);";
    private static final String SELECT_ACTIVE_SHARED_MAPS_BY_SHAREUSER = "Select Pk, MapsId, Owner, ShareUserId, shareType, Status, LastUpdateTimestamp, CreateTimestamp, ShareEmail, SyncTimestamp, Msg from SharedMaps where status = 0 and ShareUserId=?;";
    private static final String SELECT_SHARED_MAPS_BY_SHAREUSER_LASTUPDATE  = "Select Pk, MapsId, Owner, ShareUserId, shareType, Status, LastUpdateTimestamp, CreateTimestamp, ShareEmail, SyncTimestamp, Msg from SharedMaps where ShareUserId=? and (SyncTimestamp = 0 or SyncTimestamp > ?);";
    private static final String SELECT_ALL_SHARED_MAPS_BY_SHAREUSER_LASTUPDATE  = "Select Pk, MapsId, Owner, ShareUserId, shareType, Status, LastUpdateTimestamp, CreateTimestamp, ShareEmail, SyncTimestamp, Msg from SharedMaps where mapsid in (select mapsid from sharedmaps where ShareUserId=? and status = 0 and shareType = '1') and (SyncTimestamp = 0 or SyncTimestamp > ?);";

    private static final String SELECT_SHARED_MAPS_BY_OWNER_LASTUPDATE = "Select Pk, MapsId, Owner, ShareUserId, shareType, Status, LastUpdateTimestamp, CreateTimestamp, ShareEmail, SyncTimestamp, Msg from SharedMaps where owner = ? and (SyncTimestamp = 0 or SyncTimestamp > ?);";
    private static final String UPDATE_USERID_BY_EMAIL = "update SharedMaps SET  ShareUserId=?, LastUpdateTimestamp=?, SyncTimestamp =? WHERE ShareEmail=?; ";
    private static final String UPDATE_STATUS_BY_MAPID = "update SharedMaps SET  status=?, LastUpdateTimestamp=?, SyncTimestamp =? WHERE mapsid=?; ";
    private static final String INSERT = "insert into SharedMaps (Pk, MapsId, Owner, ShareUserId, ShareType, Status, LastUpdateTimestamp, CreateTimestamp, ShareEmail, SyncTimestamp, Msg) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String UPDATE = "update SharedMaps SET MapsId=?, Owner=?, ShareUserId=?, ShareType=?, Status=?, LastUpdateTimestamp=?, ShareEmail=?, SyncTimestamp=?, Msg=?, CreateTimestamp=? WHERE Pk = ?; ";
    private static final String SELECT_OWNER_SHARED_MAPS_BY_MAPSID = "Select Pk, MapsId, Owner, ShareUserId, shareType, Status, LastUpdateTimestamp, CreateTimestamp, ShareEmail, SyncTimestamp, Msg from SharedMaps where status = 0 and MapsId=? and shareType='3'';";
    private static final String UPDATE_STATUS_BY_EMAIL_TRIP = "update SharedMaps SET  status=?, LastUpdateTimestamp=?, SyncTimestamp =? WHERE ShareEmail=? and MapsId =? and status = 0; ";

    public static ArrayList<SharedMaps> getOwnersForSharedTrip (String mapsId) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_OWNER_SHARED_MAPS_BY_MAPSID);
            prep.setString(1, mapsId);
            rs = prep.executeQuery();
            return SharedMapsMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }


    public static ArrayList<SharedMaps> getActiveSharesByEmail(String email) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
             stat = conn.createStatement();
             prep = conn.prepareStatement(SELECT_ACTIVE_SHARED_MAPS_BY_EMAIL);
            prep.setString(1, email);
            rs = prep.executeQuery();
            return SharedMapsMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList<SharedMaps> getAllActiveSharesByOwner(String userId) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
             stat = conn.createStatement();
             prep = conn.prepareStatement(SELECT_ACTIVE_SHARED_MAPS_BY_OWNER);
            prep.setString(1, userId);
            rs = prep.executeQuery();
            return SharedMapsMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList<SharedMaps> getAllActiveSharesByShareUser(String userId) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
             stat = conn.createStatement();
             prep = conn.prepareStatement(SELECT_ACTIVE_SHARED_MAPS_BY_SHAREUSER);
            prep.setString(1, userId);
            rs = prep.executeQuery();
            return SharedMapsMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList<SharedMaps> getSharedMapsByOwnerSinceLastUpdate (String userId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
             stat = conn.createStatement();
             prep = conn.prepareStatement(SELECT_SHARED_MAPS_BY_OWNER_LASTUPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            rs = prep.executeQuery();
            return SharedMapsMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList<SharedMaps> getSharedMapsByShareUserSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
             stat = conn.createStatement();
             prep = conn.prepareStatement(SELECT_SHARED_MAPS_BY_SHAREUSER_LASTUPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            rs = prep.executeQuery();
            return SharedMapsMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList<SharedMaps> getOtherSharedMapsByShareUserSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_ALL_SHARED_MAPS_BY_SHAREUSER_LASTUPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            rs = prep.executeQuery();
            return SharedMapsMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static void updateSharedUserID (String email, String userID) throws SQLException {
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
             stat = conn.createStatement();
             prep = conn.prepareStatement(UPDATE_USERID_BY_EMAIL);
            prep.setString(1, userID);
            prep.setLong(2, System.currentTimeMillis());
            prep.setLong(3, System.currentTimeMillis());
            prep.setString(4, email);
            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static void deleteByEmailTrip (String email, String tripId) throws SQLException {
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(UPDATE_STATUS_BY_EMAIL_TRIP);
            prep.setInt(1, -1);
            prep.setLong(2, System.currentTimeMillis());
            prep.setLong(3, System.currentTimeMillis());
            prep.setString(4, email);
            prep.setString(5, tripId);

            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static void deleteByEmailTrip (String email, String tripId, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(UPDATE_STATUS_BY_EMAIL_TRIP);
            prep.setInt(1, -1);
            prep.setLong(2, System.currentTimeMillis());
            prep.setLong(3, System.currentTimeMillis());
            prep.setString(4, email);
            prep.setString(5, tripId);

            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

    public static void updateStatusByMapID (long status, String mapsId) throws SQLException {
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
             stat = conn.createStatement();
             prep = conn.prepareStatement(UPDATE_STATUS_BY_MAPID);

            prep.setLong(1, status);
            prep.setLong(2, System.currentTimeMillis());
            prep.setLong(3, System.currentTimeMillis());

            prep.setString(4, mapsId);
            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            DBConnectionMgr.closeConnection(conn);
        }
    }


    public static void insert (SharedMaps c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(INSERT);
            prep.setString(1, c.getPk());
            prep.setString(2, c.getMapsId());
            prep.setString(3, c.getOwner());
            prep.setString(4, c.getShareUserId());
            prep.setString(5, c.getShareType());
            prep.setLong(6, c.getStatus());
            prep.setLong(7, c.getLastUpdateTimestamp());
            prep.setLong(8, c.getCreateTimestamp());
            prep.setString(9, c.getShareEmail());
            prep.setLong(10, c.getSyncTimestamp());   //for syncing purposes the create and last update must be the same
            prep.setString(11, c.getMsg());
            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

    public static int update (SharedMaps c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(UPDATE);
            prep.setString(1, c.getMapsId());
            prep.setString(2, c.getOwner());
            prep.setString(3, c.getShareUserId());
            prep.setString(4, c.getShareType());
            prep.setLong(5, c.getStatus());
            prep.setLong(6, c.getLastUpdateTimestamp());
            prep.setString(7, c.getShareEmail());
            prep.setLong(8, c.getSyncTimestamp());
            prep.setString(9, c.getMsg());
            prep.setLong(10, c.getCreateTimestamp());
            prep.setString(11, c.getPk());
            return prep.executeUpdate();
        } finally {
            if (prep != null)
                prep.close();
        }
    }



}
