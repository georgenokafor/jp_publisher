package models.publisher;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jdk.nashorn.internal.ir.annotations.Ignore;
import models.AbstractEBeanModelTest;

public class DestinationTest extends AbstractEBeanModelTest {

  // ignore as the data is local to a database
  @Ignore
  @Test
  public void testGetAllActivelyLinkedForTrip() {
    List<Destination> ds = Destination.getAllActivelyLinkedForTrip("1297470068090000004");
    Assert.assertEquals(2, ds.size());
    
    ds = Destination.getAllActivelyLinkedForTrip("1295797517850000004");
    Assert.assertEquals(0, ds.size());
    
    ds = Destination.getGuidesForActivities("1297470068090000004");
    Assert.assertEquals(2, ds.size());  
  }
}
