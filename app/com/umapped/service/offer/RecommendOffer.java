package com.umapped.service.offer;

import com.umapped.itinerary.analyze.recommendation.RecommendedItem;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by wei on 2017-07-28.
 */
public class RecommendOffer {
  private LocalDate date;
  private RecommendedItem item;
  private String noteDate;
  private List<RecommendedItem> items;

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public RecommendedItem getItem() {
    return item;
  }

  public void setItem(RecommendedItem item) {
    this.item = item;
  }

  public String getNoteDate() {
    return noteDate;
  }

  public void setNoteDate(String noteDate) {
    this.noteDate = noteDate;
  }

  public List<RecommendedItem> getItems() {
    return items;
  }

  public void setItems(List<RecommendedItem> items) {
    this.items = items;
  }
}
