package com.mapped.common;

import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.utils.Log;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;


/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2012-10-12
 * Time: 11:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class EmailMgr {

  Session mailSession;
  Transport transport;
  private String host;
  private String user;
  private String pwd;
  private String fromEmail;
  private String noticeEmail;
  private int port = 587;

  private class SMTPAuthenticator
      extends javax.mail.Authenticator {
    public PasswordAuthentication getPasswordAuthentication() {
      String username = user;
      String password = pwd;
      return new PasswordAuthentication(username, password);
    }
  }

  public static EmailMgr buildDefaultTripManager() {
    String smtp = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_HOST);
    String user = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_USER_ID);
    String pwd = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_PASSWORD);
    String fromEmail = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_TRIP_NOTIFICATION_FROM);

    EmailMgr emailMgr = new EmailMgr(smtp, user, pwd, fromEmail);
    try {
      emailMgr.init();
    }
    catch (Exception e) {
      Log.err("EmailMgr: Failed to build default Email Manager", e);
      return null;
    }
    return emailMgr;
  }


  public EmailMgr(String smtpHost, String smtpUser, String smtpPwd) {
    host = smtpHost;
    user = smtpUser;
    pwd = smtpPwd;
  }

  public EmailMgr(String smtpHost, String smtpUser, String smtpPwd, String fromEmail) {
    host = smtpHost;
    user = smtpUser;
    pwd = smtpPwd;
    this.fromEmail = fromEmail;
  }


  public static EmailMgr buildEmailMgr (String smtpHost, String smtpUser, String smtpPwd, int port, String fromEmail, String noticeEmail) {
    EmailMgr emailMgr = new EmailMgr(smtpHost, smtpUser, smtpPwd, port, fromEmail, noticeEmail);
    try {
      emailMgr.init();
    }
    catch (Exception e) {
      Log.err("EmailMgr: Failed to build default Email Manager Host:" + smtpHost + " User:" + smtpUser + " Pwd:" + smtpPwd + " Port:" + port , e);
      return null;
    }
    return emailMgr;
  }

  public EmailMgr (String smtpHost, String smtpUser, String smtpPwd, int port, String fromEmail, String noticeEmail) {
    host = smtpHost;
    user = smtpUser;
    pwd = smtpPwd;
    this.fromEmail = fromEmail;
    if (port > 0) {
      this.port = port;
    }
    this.noticeEmail = noticeEmail;
  }

  public static EmailMgr getDefault()
      throws Exception {
    EmailMgr mgr = new EmailMgr(ConfigMgr.getAppParameter(CoreConstants.EMAIL_SMTP_HOST),
                                ConfigMgr.getAppParameter(CoreConstants.EMAIL_SMTP_USER_ID),
                                ConfigMgr.getAppParameter(CoreConstants.EMAIL_SMTP_PASSWORD));
    mgr.init();
    return mgr;
  }

  public void init()
      throws Exception {
    Properties props = new Properties();
    props.put("mail.transport.protocol", "smtp");
    props.put("mail.smtp.host", host);
    props.put("mail.smtp.port", port);
    props.put("mail.smtp.auth", "true");

    Authenticator auth = new SMTPAuthenticator();
    mailSession = Session.getInstance(props, auth);
    // uncomment for debugging infos to stdout
    // mailSession.setDebug(true);
    transport = mailSession.getTransport();
    transport.connect();
  }

  public Session getSession() {
    return mailSession;
  }


  //      emailMgr.send(mm, fromEmail, sender, emails, bccEmail, subject, headers);
  public void send(MimeMessage msg,
                   String fromEmail,
                   String fromName,
                   List<String> toEmails,
                   List<String> bccEmails,
                   String subject,
                   Map<String, String> headers) {

    try {
      msg.setFrom(new InternetAddress(fromEmail, fromName));
      msg.setRecipients(Message.RecipientType.TO, (String)null); //Clearing whatever is already set
      msg.setRecipients(Message.RecipientType.CC, (String)null); //Clearing whatever is already set
      msg.setRecipients(Message.RecipientType.BCC, (String)null); //Clearing whatever is already set

      for (String s : toEmails) {
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(s));
      }
      for (String s : bccEmails) {
        msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(s));
      }

      msg.setSubject(subject);
      if (headers != null) {
        for (String headerID : headers.keySet()) {
          msg.setHeader(headerID, headers.get(headerID));
        }
      }
      transport.sendMessage(msg, msg.getAllRecipients());
      transport.close();
    }
    catch (MessagingException e) {
      Log.err("Failed to send mime message ...");
      e.printStackTrace();
    }
    catch (UnsupportedEncodingException e) {
      Log.err("Failed to create mime message to send ...");
      e.printStackTrace();
    }
  }

  public void send(String fromEmail,
                   String fromName,
                   List<String> toEmails,
                   List<String> bccEmails,
                   String subject,
                   List<Map<String, String>> body,
                   Map<String, String> headers)
      throws Exception {

    if (fromEmail == null || fromEmail.isEmpty() ||
        ((toEmails == null || toEmails.size() == 0) &&
         (bccEmails == null || bccEmails.size() == 0)) ) {
      return;
    }

    MimeMessage message = new MimeMessage(mailSession);

    Multipart multipart = new MimeMultipart("alternative");

    for (Map<String, String> s : body) {
      BodyPart part = new MimeBodyPart();
      if (s.containsKey(CoreConstants.HTML)) {
        part.setContent(s.values().iterator().next(), "text/html; charset=UTF-8");
      }
      else {
        part.setText(s.values().iterator().next());
      }

      multipart.addBodyPart(part);
    }
    message.setContent(multipart);
    message.setFrom(new InternetAddress(fromEmail, fromName));
    message.setSubject(subject);

    if (headers != null) {
      for (String headerID : headers.keySet()) {
        message.setHeader(headerID, headers.get(headerID));
      }
    }

    for (String s : toEmails) {
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(s));
    }

    if (bccEmails != null) {
      for (String bcc : bccEmails) {
        message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc));
      }
    }

    transport.sendMessage(message, message.getAllRecipients());
    transport.close();
  }

  /**
   * Single destination email helper send function (without headers)
   * @param fromEmail
   * @param fromName
   * @param toEmail
   * @param subject
   * @param body
   * @throws Exception
   */
  public void send(String fromEmail, String fromName, String toEmail, String subject, List<Map<String, String>> body)
      throws Exception {
    List<String> toEmailList = new ArrayList<>();
    toEmailList.add(toEmail);
    send(fromEmail, fromName, toEmailList, null, subject, body, null);
  }

  /**
   * Multiple destination emails helper send function (without headers)
   * @param fromEmail
   * @param fromName
   * @param toEmail
   * @param subject
   * @param body
   * @throws Exception
   */
  public void send(String fromEmail,
                   String fromName,
                   List<String> toEmail,
                   String subject,
                   List<Map<String, String>> body)
      throws Exception {
    send(fromEmail, fromName, toEmail, null, subject, body, null);
  }

  /**
   * Single TO and BCC destination email helper send function (without headers)
   * @param fromEmail
   * @param fromName
   * @param toEmail
   * @param bccEmail
   * @param subject
   * @param body
   * @throws Exception
   */
  public void sendBCC(String fromEmail,
                      String fromName,
                      String toEmail,
                      String bccEmail,
                      String subject,
                      List<Map<String, String>> body)
      throws Exception {
    List<String> toEmailList = new ArrayList<String>();
    toEmailList.add(toEmail);
    List<String> bccEmailList = new ArrayList<String>();
    bccEmailList.add(bccEmail);

    send(fromEmail, fromName, toEmailList, bccEmailList, subject, body, null);
  }

  public void send(String fromName,
                   List<String> toEmail,
                   String subject,
                   List<Map<String, String>> body) throws Exception {
    send(fromEmail, fromName, toEmail, null, subject, body, null);
  }

  public void close()
      throws Exception {
    if (transport != null) {
      transport.close();
    }
  }

  public String getFromEmail() {
    return fromEmail;
  }

  public void setFromEmail(String fromEmail) {
    this.fromEmail = fromEmail;
  }
}
