package com.mapped.publisher.view.billing;

import com.mapped.publisher.view.BaseView;
import models.publisher.BillingPlan;
import models.publisher.Consortium;

import java.io.Serializable;

/**
 * Created by george on 2017-09-24.
 */
public class SubscriptionPlanView extends BaseView
    implements Serializable {

  public Long id;
  public String name;
  public String descriptive;
  public String price;
  public String description;
  public String schedule;
  public Consortium consortium;


  public static SubscriptionPlanView buildView(Long planId) {
    if (planId != null && planId > 0L) {
      BillingPlan plan = BillingPlan.find.byId(planId);
      SubscriptionPlanView view = null;
      if (plan != null) {
        view = new SubscriptionPlanView();
        view.id = plan.getPlanId();
        view.name = plan.getName();
        view.descriptive = plan.getDescriptive();
        view.consortium = plan.getConsortium();

        StringBuilder price = new StringBuilder();
        price.append(plan.getCurrency().name());
        price.append(" ");
        price.append(String.format("%.2f", plan.getCost()));
        view.price = price.toString();

        StringBuilder sched = new StringBuilder();
        sched.append("(billed ");
        sched.append(plan.getSchedule().getScheduleName().toLowerCase());
        sched.append(")");
        view.schedule = sched.toString();

        if (plan.getDescription() != null && !plan.getDescription().isEmpty()) {
          view.description = plan.getDescription();
        }

      }
      return view;
    }
    return null;
  }
  
}
