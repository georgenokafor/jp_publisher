package com.mapped.publisher.persistence.communicator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.firebase.client.Firebase;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by surge on 2015-07-15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoomMessage
    extends FirebaseObject {
  /*
    room-messages:
    type: object
    properties:
      ~$roomId:
        type: object
        properties:
          ~$msgId:
            type: object
            properties:
              userId:     {type: string}
              name:       {type: string}
              message:    {type: string}
              timestamp:  {type: number}
              #Per message list of users who read it
              readBy:
                type: object
                properties:
                  ~$userId: {type: boolean}
            required: [userId, name, message, timestamp]
   */

  /**
   * Id of the user who posted the message
   */
  public String userId;

  /**
   * Name of the user who posted the message
   */
  public String name;

  /**
   * Message body
   */
  public String message;

  /**
   * When the message was posted in *seconds* since Epoch
   */
  public Long timestamp;

  /**
   * Message types
   */
  public RoomMessageType type;


  public Map<String, Boolean> readBy;

  public RoomMessage() {
    readBy = new TreeMap<>();
  }

  public Firebase update(Firebase ref) {
    Map<String, Object> uVals = new HashMap<>();
    uVals.put("userId", userId);
    uVals.put("name", name);
    uVals.put("message", message);
    uVals.put("type", type);
    uVals.put("timestamp", timestamp);
    ref.updateChildren(uVals);
    return ref;
  }
}
