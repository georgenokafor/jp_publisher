//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.04.17 at 04:55:33 PM EDT 
//


package com.umapped.itinerary.analyze.supplier.travelbound.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_DropOffAirport complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_DropOffAirport">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Airport" type="{}t_Airport" minOccurs="0"/>
 *         &lt;element name="DepartingTo" type="{}t_Where" minOccurs="0"/>
 *         &lt;element name="FlightNumber" type="{}t_FlightNumber" minOccurs="0"/>
 *         &lt;element name="DepartureTime" type="{}t_DepartureTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_DropOffAirport", propOrder = {
    "airport",
    "departingTo",
    "flightNumber",
    "departureTime"
})
public class TDropOffAirport {

    @XmlElement(name = "Airport")
    protected TAirport airport;
    @XmlElement(name = "DepartingTo")
    protected TWhere departingTo;
    @XmlElement(name = "FlightNumber")
    protected String flightNumber;
    @XmlElement(name = "DepartureTime")
    protected TDepartureTime departureTime;

    /**
     * Gets the value of the airport property.
     * 
     * @return
     *     possible object is
     *     {@link TAirport }
     *     
     */
    public TAirport getAirport() {
        return airport;
    }

    /**
     * Sets the value of the airport property.
     * 
     * @param value
     *     allowed object is
     *     {@link TAirport }
     *     
     */
    public void setAirport(TAirport value) {
        this.airport = value;
    }

    /**
     * Gets the value of the departingTo property.
     * 
     * @return
     *     possible object is
     *     {@link TWhere }
     *     
     */
    public TWhere getDepartingTo() {
        return departingTo;
    }

    /**
     * Sets the value of the departingTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TWhere }
     *     
     */
    public void setDepartingTo(TWhere value) {
        this.departingTo = value;
    }

    /**
     * Gets the value of the flightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Sets the value of the flightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Gets the value of the departureTime property.
     * 
     * @return
     *     possible object is
     *     {@link TDepartureTime }
     *     
     */
    public TDepartureTime getDepartureTime() {
        return departureTime;
    }

    /**
     * Sets the value of the departureTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link TDepartureTime }
     *     
     */
    public void setDepartureTime(TDepartureTime value) {
        this.departureTime = value;
    }

}
