package com.mapped.publisher.form.booking;

import org.apache.commons.lang3.StringUtils;
import play.data.validation.Constraints;

import java.util.List;

/**
 * Created by surge on 2016-01-04.
 */
public class FormBookingBase {
  @Constraints.Required
  public String     inTripId;
  public List<Long> inDetailPhotos;
  public String     inTripDetailId;
  public Boolean    inPhotosToggle;
  public Boolean    inPhotosReset;
  public String     inTaxes;
  public String     inFees;
  public String     inCurrency;
  public String     inTotal;
  public String   inSubtotal;
  public List<Rate> rates;
  public Boolean    inCrossDateFlight = false;

  public static class Rate {
    public String rateName;
    public String ratePrice;
    public String rateDescription;

    public boolean isEmpty() {
      return StringUtils.isEmpty(ratePrice) && StringUtils.isEmpty(rateDescription);
    }
  }

}
