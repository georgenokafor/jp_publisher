package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.utils.Utils;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by twong on 2014-05-26.
 */
@Entity @Table(name = "trip_invite")
public class TripInvite
    extends Model {
  @Transient
  public static final long RESEND_TIME = 1000 * 60 * 60 * 24 * 1; //1 Days //TODO: Serguei: Modified to 0 for testing
  public static Finder<String, TripInvite> find = new Finder<>(TripInvite.class);
  @Id
  public String pk;
  @Constraints.Required @ManyToOne @JoinColumn(name = "inviteid")
  public UserInvite userInvite;
  @Constraints.Required
  public String tripid;
  @Enumerated(value = EnumType.STRING)
  public SecurityMgr.AccessLevel accesslevel;
  public int status;
  public String note;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;

  public static TripInvite buildTripInvite(String agentLegacyId,
                                           Trip trip,
                                           UserInvite userInvite,
                                           SecurityMgr.AccessLevel accesslevel,
                                           String note) {
    TripInvite tripInvite = new TripInvite();

    tripInvite.pk = Utils.getUniqueId();
    tripInvite.tripid = trip.tripid;

    tripInvite.createdby = agentLegacyId;
    tripInvite.createdtimestamp = System.currentTimeMillis();
    tripInvite.modifiedby = tripInvite.createdby;
    tripInvite.lastupdatedtimestamp = tripInvite.createdtimestamp;
    tripInvite.note = note;
    tripInvite.userInvite = userInvite;
    tripInvite.status = APPConstants.STATUS_ACTIVE;
    tripInvite.accesslevel = accesslevel;


    return tripInvite;
  }

  public static List<TripInvite> getAllActiveInvites(String inviteid) {
    return find.where().eq("status", 0).eq("inviteid", inviteid).findList();
  }

  /**
   * List of all active invites for the trip.
   *
   * @param trip trip under question
   * @return
   */
  public static List<TripInvite> getTripInvites(Trip trip) {
    return find.where().eq("tripid", trip.tripid).eq("status", APPConstants.STATUS_ACTIVE).findList();
  }

  public static int updateAllInviteId(String inviteid, String userId) {
    String s = "UPDATE trip_invite set status = 2, lastupdatedtimestamp = :timestamp, note=:note,  " +
               "" + "modifiedby = :userid  where inviteid = :inviteid and status = 0";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("timestamp", System.currentTimeMillis());
    update.setParameter("userid", userId);
    update.setParameter("inviteid", inviteid);
    update.setParameter("note", "thierry");


    return Ebean.execute(update);
  }

  public static TripInvite findByPk(String pk) {
    return find.where().eq("pk", pk).findUnique();
  }

  public static TripInvite findByTripAndInvitee(String tripId, String inviteId) {
    return find.where().eq("tripid", tripId).eq("inviteid", inviteId).findUnique();
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public UserInvite getUserInvite() {
    return userInvite;
  }

  public void setUserInvite(UserInvite userInvite) {
    this.userInvite = userInvite;
  }

  public SecurityMgr.AccessLevel getAccesslevel() {
    return accesslevel;
  }

  public void setAccesslevel(SecurityMgr.AccessLevel accessLevel) {
    this.accesslevel = accessLevel;
  }

  public String getPk() {
    return pk;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public String getTripid() {
    return tripid;
  }

  public void setTripid(String tripid) {
    this.tripid = tripid;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
