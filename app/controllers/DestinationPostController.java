package controllers;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.audit.AuditModuleType;
import com.mapped.publisher.audit.event.AuditTripDocCustom;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.DestinationForm;
import com.mapped.publisher.form.DestinationGuideForm;
import com.mapped.publisher.form.DestinationGuideUrlForm;
import com.mapped.publisher.form.FileUploadForm;
import com.mapped.publisher.js.BaseJsonResponse;
import com.mapped.publisher.persistence.PageAttachType;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.DestinationSearchView;
import com.mapped.publisher.view.UserMessage;
import com.umapped.api.schema.types.ReturnCode;
import models.publisher.*;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.util.List;

import static controllers.ImageController.uploadHelper;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-11
 * Time: 12:57 PM
 */
public class DestinationPostController
    extends Controller {

  @Inject
  FormFactory formFactory;

  //POST
  @With({Authenticated.class, Authorized.class})
  public Result createDestination() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    Form<DestinationForm> form = formFactory.form(DestinationForm.class).bindFromRequest();
    //use bean validator framework
    if (form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    DestinationForm destInfo = form.get();

    if (destInfo.getInCmpyId() != null) {
      List<UserCmpyLink> cl = UserCmpyLink.findActiveByUserIdCmpyId(sessionMgr.getUserId(), destInfo.getInCmpyId());
      if (cl == null || cl.isEmpty()) {
        BaseView baseView = new BaseView();
        baseView.message = "Invalid agency - please resubmit";
        return ok(views.html.common.message.render(baseView));
      }
    }

    if ((destInfo.getInDestCountry() == null ||
         destInfo.getInDestCountry().length() == 0) &&
         destInfo.getInDestinationTypeId() != null &&
         (destInfo.getInDestinationTypeId().equals(String.valueOf(APPConstants.CITY_DESTINATION_TYPE)) ||
          destInfo.getInDestinationTypeId().equals(String.valueOf(APPConstants.CITY_DESTINATION_TYPE)))) {
      BaseView baseView = new BaseView();
      baseView.message = "Invalid country  - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    if ((destInfo.getInDestCity() == null || destInfo.getInDestCity()
                                                     .length() == 0) && destInfo.getInDestinationTypeId() != null
        && destInfo
            .getInDestinationTypeId()
            .equals(String.valueOf(APPConstants.CITY_DESTINATION_TYPE))) {
      BaseView baseView = new BaseView();
      baseView.message = "Invalid city  - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    if (destInfo.getInCmpyId() != null) {
      Company cmpy = Company.find.byId(destInfo.getInCmpyId());
      if (cmpy == null || cmpy.status != APPConstants.STATUS_ACTIVE ||
          (!SecurityMgr.isCmpyAdmin(cmpy, sessionMgr) && !SecurityMgr.isUmappedAdmin(sessionMgr)
           && !SecurityMgr.isPowerMember(cmpy, sessionMgr))) {
        BaseView baseView = new BaseView();
        baseView.message = "Action not authorized";
        return ok(views.html.common.message.render(baseView));
      }
    }
    else
    {
      BaseView baseView = new BaseView();
      baseView.message = "Company Name was not specified";
      return ok(views.html.common.message.render(baseView));
    }

    boolean privateDoc = false;

    if (destInfo.isInPrivateDest()){
      privateDoc = true;
    }

    try {
      Ebean.beginTransaction();
      Destination dest;
      if (destInfo.getInDestId() == null || destInfo.getInDestId().length() == 0) {
        dest = new Destination();
        dest.setStatus(APPConstants.STATUS_ACTIVE);
        dest.setCreatedby(sessionMgr.getUserId());
        dest.setCreatedtimestamp(System.currentTimeMillis());
        dest.setDestinationid(Utils.getUniqueId());
      }
      else {
        dest = Destination.find.byId(destInfo.getInDestId());
        if (dest == null || dest.status != APPConstants.STATUS_ACTIVE ||
            !SecurityMgr.canEditDestination(dest, sessionMgr)) {
          BaseView baseView = new BaseView();
          baseView.message = "Unauthorized Action";
          return ok(views.html.common.message.render(baseView));
        }
      }
      if (destInfo.getInDestinationTypeId() != null) {
        try {
          dest.setDestinationtypeid(Integer.parseInt(destInfo.getInDestinationTypeId()));

        }
        catch (NumberFormatException ne) {
          dest.setDestinationtypeid(APPConstants.TOUR_DESTINATION_TYPE);
        }
      }
      else {
        dest.setDestinationtypeid(APPConstants.TOUR_DESTINATION_TYPE);
      }
      dest.setCity(destInfo.getInDestCity());
      dest.setCmpyid(destInfo.getInCmpyId());
      dest.setCountry(destInfo.getInDestCountry());
      dest.setIntro(destInfo.getInDestIntro());
      dest.setTag(destInfo.getInDestTag());
      if (destInfo.getInDestLocLat() != null) {
        try {
          float lat = Float.parseFloat(destInfo.getInDestLocLat());
          dest.setLoclat(lat);
        }
        catch (NumberFormatException ne) {
          dest.setLoclat(0);

        }
      }
      else {
        dest.setLoclat(0);
      }

      if (destInfo.getInDestLocLong() != null) {
        try {
          float locLong = Float.parseFloat(destInfo.getInDestLocLong());
          dest.setLoclong(locLong);
        }
        catch (NumberFormatException ne) {
          dest.setLoclong(0);

        }
      }
      else {
        dest.setLoclong(0);
      }

      dest.setName(destInfo.getInDestName());
      dest.setModifiedby(sessionMgr.getUserId());
      dest.setLastupdatedtimestamp(System.currentTimeMillis());

      if (privateDoc) {
        dest.setAccessLevel(Destination.AccessLevel.PRIVATE);
      } else {
        dest.setAccessLevel(Destination.AccessLevel.PUBLIC);
      }
      dest.save();

      Ebean.commitTransaction();

      invalidateCache(dest.destinationid);
      if (destInfo.getInDestId() == null || destInfo.getInDestId().length() == 0) {
        flash(SessionConstants.SESSION_PARAM_MSG, destInfo.getInDestName() + " added successfully");
      }
      else {
        flash(SessionConstants.SESSION_PARAM_MSG, destInfo.getInDestName() + " updated successfully");
      }
      return redirect(routes.DestinationController.newDestination(dest.getDestinationid()));
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      Log.log(LogLevel.DEBUG, "Error inserting destination", e);
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
  }

  public static void invalidateCache(String destId) {
    if (destId != null) {
      try {
        CacheMgr.set(APPConstants.CACHE_DEST_VIEW_PREFIX + destId, null, APPConstants.CACHE_EXPIRY_SECS);

      }
      catch (Exception e) {
        Log.log(LogLevel.ERROR, "invalidateCache (destintions): " + destId, e);
      }
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteDestination() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<DestinationForm> form = formFactory.form(DestinationForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    DestinationForm destInfo = form.get();
    if ( destInfo.getInDestId() == null || destInfo.getInDestId().length() == 0) {
      return UserMessage.message(ReturnCode.HTTP_REQ_WRONG_DATA);
    }

    Destination dest = Destination.find.byId(destInfo.getInDestId());
    if (dest == null || dest.status != APPConstants.STATUS_ACTIVE || !SecurityMgr.canEditDestination(dest, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    dest.setLastupdatedtimestamp(System.currentTimeMillis());
    dest.setModifiedby(sessionMgr.getUserId());
    dest.setStatus(APPConstants.STATUS_DELETED);

    int count = TripDestination.deleteByDestinationId(dest.destinationid, sessionMgr.getUserId());

    dest.save();
    flash(SessionConstants.SESSION_PARAM_MSG,
          dest.getName() + " deleted successfully - removed from " + count + " trips.");

    invalidateCache(dest.destinationid);

    if (dest.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE ||
        dest.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE) {
      return  redirect(routes.TripController.guides(destInfo.getInTripId(),
                                   new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.CUSTOM_DOC)));
    }
    else {
      return redirect(routes.DestinationController.destination());
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result createDestinationGuide() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<DestinationGuideForm> form = formFactory.form(DestinationGuideForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    DestinationGuideForm destGuideInfo = form.get();
    if (destGuideInfo.getInDestId() == null ||
        destGuideInfo.getInDestId().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    Destination destination = Destination.find.byId(destGuideInfo.getInDestId());
    //TODO: Serguei: canEditDestination is incorrect logic here, it must be can access destination and then either can
    //TODO: Serguei: edit the page or create new page in the exisiting guide.
    if (destination == null || !SecurityMgr.canAccessDestination(destination, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "Cannot access this guide  - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();
      DestinationGuide destGuide = null;
      //NEW PAGE (DESTINATION GUIDE)
      if (destGuideInfo.getInDestGuideId() == null || destGuideInfo.getInDestGuideId().length() == 0) {
        int count = DestinationGuide.activeCount(destination.destinationid);
        if (count > 150) {
          BaseView baseView = new BaseView();
          baseView.message = "Error - you have exceeded the maximum number of pages.";
          return ok(views.html.common.message.render(baseView));
        }
        destGuide = new DestinationGuide();
        destGuide.setDestinationid(destGuideInfo.getInDestId());
        destGuide.setDestinationguideid(Utils.getUniqueId());
        int maxRow = DestinationGuide.maxPageRankByDestId(destination.destinationid);
        destGuide.setRank(++maxRow);
        destGuide.setStatus(APPConstants.STATUS_ACTIVE);
        destGuide.setCreatedby(sessionMgr.getUserId());
        destGuide.setCreatedtimestamp(System.currentTimeMillis());
        destGuide.setDestinationguideid(Utils.getUniqueId());

        //set as last page

      }
      else { //MODIFY EXISTING PAGE (DESTINATION GUIDE)
        destGuide = DestinationGuide.find.byId(destGuideInfo.getInDestGuideId());
        if (destGuide == null ||
            destGuide.status != APPConstants.STATUS_ACTIVE ||
            !destGuide.destinationid.equals(destination.destinationid) ||
            !SecurityMgr.canEditGuide(destination, destGuide, sessionMgr)) {
          BaseView baseView = new BaseView();
          baseView.message = "System Error - No permission to modify this page";
          Ebean.rollbackTransaction();

          return ok(views.html.common.message.render(baseView));
        }
      }
      destGuide.setLandmark(destGuideInfo.getInDestGuideLandmark());
      destGuide.setStreetaddr1(destGuideInfo.getInDestGuideAddr());
      destGuide.setCity(destGuideInfo.getInDestGuideCity());
      destGuide.setCountry(destGuideInfo.getInDestGuideCountry());
      destGuide.setState(destGuideInfo.getInDestGuideState());
      destGuide.setIntro(destGuideInfo.getInDestGuideIntro());
      if (destGuide.getPageId() == null) { //Not mobilized so write description
        destGuide.setDescription(destGuideInfo.getInDestGuideDesc());
      } else {
        //if you save it,  let's reset the page id - this is now yours
        LibraryPage lp = LibraryPage.find.byId(destGuide.getPageId());
        if (lp != null && lp.getContent() != null) {
          destGuide.setDescription(lp.getContent());
          destGuide.setPageId(null);
        }
      }
      destGuide.setTag(destGuideInfo.getInDestGuideTag());
      destGuide.setZipcode(destGuideInfo.getInDestGuideZip());

      switch (destGuideInfo.getInDestGuideRecommendationType()) {
        case APPConstants.RECOMMENDATION_TYPE_HOTEL:
          destGuide.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_HOTEL);
          break;
        case APPConstants.RECOMMENDATION_TYPE_FOOD:
          destGuide.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_FOOD);
          break;
        case APPConstants.RECOMMENDATION_TYPE_GOINGS_ON:
          destGuide.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_GOINGS_ON);
          break;
        case APPConstants.RECOMMENDATION_TYPE_SHOPPING:
          destGuide.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_SHOPPING);
          break;
        case APPConstants.RECOMMENDATION_TYPE_SIGHTS:
          destGuide.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_SIGHTS);
          break;
        case APPConstants.RECOMMENDATION_TYPE_OTHER:
          destGuide.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_OTHER);
          break;
        default:
          destGuide.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_UNDEFINED);
          break;
      }


      if (destGuideInfo.getInDestGuideLocLat() != null) {
        try {
          float lat = Float.parseFloat(destGuideInfo.getInDestGuideLocLat());
          destGuide.setLoclat(lat);
        }
        catch (NumberFormatException ne) {
          destGuide.setLoclat(0);

        }
      }
      else {
        destGuide.setLoclat(0);
      }

      if (destGuideInfo.getInDestGuideLocLong() != null) {
        try {
          float locLong = Float.parseFloat(destGuideInfo.getInDestGuideLocLong());
          destGuide.setLoclong(locLong);
        }
        catch (NumberFormatException ne) {
          destGuide.setLoclong(0);

        }
      }
      else {
        destGuide.setLoclong(0);
      }

      destGuide.setName(destGuideInfo.getInDestGuideName());
      destGuide.setModifiedby(sessionMgr.getUserId());
      destGuide.setLastupdatedtimestamp(System.currentTimeMillis());

      //set date if applicable
      if (destGuideInfo.getInDestGuideDate() != null && destGuideInfo.getInDestGuideDate().trim().length() > 0) {
        StringBuilder sb = new StringBuilder();
        sb.append(destGuideInfo.getInDestGuideDate());
        if (destGuideInfo.getInDestGuideTime() != null && destGuideInfo.getInDestGuideTime().trim().length() > 0) {
          sb.append(" ");
          sb.append(destGuideInfo.getInDestGuideTime());
        }

        long timestamp = Utils.getMilliSecs(sb.toString());
        if (timestamp > 0) {
          destGuide.setDestguidetimestamp(timestamp);
        }
      } else
        destGuide.setDestguidetimestamp(null);

      destGuide.save();

      Ebean.commitTransaction();

      //Create audit log if this is a trip document
      if (destination.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE || destination.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE) {

        audit:
        {
          Trip tripModel = null;
          List<TripDestination> tdests = TripDestination.findActiveByDest(destination.getDestinationid());
          if (tdests != null && tdests.size() != 0) {
            tripModel = Trip.find.byId(tdests.get(0).getTripid());
          }

          if (tripModel != null) {
            TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_DOC_CUSTOM,
                                                 AuditActionType.MODIFY,
                                                 AuditActorType.WEB_USER)
                                    .withTrip(tripModel)
                                    .withCmpyid(tripModel.cmpyid)
                                    .withUserid(sessionMgr.getUserId());

            ((AuditTripDocCustom) ta.getDetails()).withGuideId(destination.getDestinationid())
                                                  .withName(destination.getName())
                                                  .withCoverImage(destination.getCovername())
                                                  .withText("Added New Page: " + destGuide.getName());
            ta.save();;
          }
          else {
            Log.log(LogLevel.ERROR, "Trip ID can not be determined while adding new destination page audit:" + destination.getDestinationid());
          }
        }
      }


      if (destGuideInfo.getInDestGuideId() == null || destGuideInfo.getInDestGuideId().length() == 0) {
        flash(SessionConstants.SESSION_PARAM_MSG, destGuideInfo.getInDestGuideName() + " added successfully");
      }
      else {
        flash(SessionConstants.SESSION_PARAM_MSG, destGuideInfo.getInDestGuideName() + " updated successfully");
      }

      invalidateCache(destGuide.destinationid);
      return redirect(routes.DestinationController.newDestinationGuide(destGuide.destinationid,
                                                                       destGuide.destinationguideid));
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      Log.log(LogLevel.DEBUG, "Error inserting destination", e);
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteDestinationGuide() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<DestinationGuideForm> form = formFactory.form(DestinationGuideForm.class).bindFromRequest();

    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    DestinationGuideForm destGuideInfo = form.get();

    //use bean validator framework
    if (destGuideInfo.getInDestId() == null ||
        destGuideInfo.getInDestId().length() == 0 ||
        destGuideInfo.getInDestGuideId() == null ||
        destGuideInfo.getInDestGuideId().length() == 0) {
      return UserMessage.message(ReturnCode.HTTP_REQ_WRONG_DATA);
    }

    Destination destination = Destination.find.byId(destGuideInfo.getInDestId());
    DestinationGuide destGuide = DestinationGuide.find.byId(destGuideInfo.getInDestGuideId());

    if (destination == null || destGuide == null ||
        destGuide.status != APPConstants.STATUS_ACTIVE ||
        !destGuide.destinationid.equals(destination.destinationid)) {
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    if (!SecurityMgr.canEditGuide(destination, destGuide, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "Permission Error - Cannot delete this page";
      return ok(views.html.common.message.render(baseView));
    }

    destGuide.setLastupdatedtimestamp(System.currentTimeMillis());
    destGuide.setModifiedby(sessionMgr.getUserId());
    destGuide.setStatus(APPConstants.STATUS_DELETED);
    destGuide.save();

    Trip tripModel = null;
    List<TripDestination> tdests = TripDestination.findActiveByDest(destination.getDestinationid());
    if (tdests != null && tdests.size() != 0) {
      tripModel = Trip.find.byId(tdests.get(0).getTripid());
    }

    //Create audit log for custom trip
    if (destination.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE ||
        destination.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE) {

      audit:
      {
        if (tripModel != null) {
          TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_DOC_CUSTOM, AuditActionType.MODIFY,
                                               AuditActorType.WEB_USER)
                                  .withTrip(tripModel)
                                  .withCmpyid(tripModel.cmpyid)
                                  .withUserid(sessionMgr.getUserId());

          ((AuditTripDocCustom) ta.getDetails()).withGuideId(destination.getDestinationid())
                                                .withName(destination.getName())
                                                .withCoverImage(destination.getCovername())
                                                .withText("Deleted Page: " + destGuide.getName());
          ta.save();;
        }
        else {
          Log.log(LogLevel.ERROR, "Trip ID can not be determined while deleting new destination page audit:" +
                                  destination.getDestinationid());
        }
      }
    }

    flash(SessionConstants.SESSION_PARAM_MSG, destGuide.getName() + " deleted successfully");
    invalidateCache(destGuide.destinationid);
    if (destination.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE ||
        destination.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE) {
      return  redirect(routes.TripController.guides(tripModel.tripid,
                                   new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.CUSTOM_DOC)));
    }
    else {
      return redirect(routes.DestinationController.newDestination(destGuide.getDestinationid()));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result newDestinationGuideUrl() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<DestinationGuideUrlForm> form = formFactory.form(DestinationGuideUrlForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    DestinationGuideUrlForm destGuideUrlInfo = form.get();
    if (destGuideUrlInfo.getInDestId() == null ||
        destGuideUrlInfo.getInDestGuideId() == null) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA, "Please resubmit");
    }

    if (destGuideUrlInfo.getInDestGuideUrlTitle() == null ||
        destGuideUrlInfo.getInDestGuideUrlTitle().length() == 0 ||
        destGuideUrlInfo.getInDestGuideUrl() == null ||
        destGuideUrlInfo.getInDestGuideUrl().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Invalid data - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    Destination destination = Destination.find.byId(destGuideUrlInfo.getInDestId());
    if (destination == null || !SecurityMgr.canEditDestination(destination, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "Cannot access this guide  - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    DestinationGuide destGuide = DestinationGuide.find.byId(destGuideUrlInfo.getInDestGuideId());
    if (destGuide == null || !destGuide.destinationid.equals(destination.destinationid)) {
      BaseView baseView = new BaseView();
      baseView.message = "Cannot access this guide  - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();
      DestinationGuideAttach attach = null;
      if (destGuideUrlInfo.getInDestGuideUrlId() != null && destGuideUrlInfo.getInDestGuideUrlId().length() > 0) {
        attach = DestinationGuideAttach.find.byId(destGuideUrlInfo.getInDestGuideUrlId());
        if (attach == null || attach.status != APPConstants.STATUS_ACTIVE || !attach.destinationguideid.equals(
            destGuide.destinationguideid)) {
          BaseView baseView = new BaseView();
          baseView.message = "System Error - please resubmit";
          Ebean.rollbackTransaction();

          return ok(views.html.common.message.render(baseView));
        }
      }
      else {
        attach = new DestinationGuideAttach();

        attach.setFileid(Utils.getUniqueId());
        attach.setDestinationguideid(destGuide.destinationguideid);
        attach.setStatus(APPConstants.STATUS_ACTIVE);
        attach.setCreatedby(sessionMgr.getUserId());
        attach.setCreatedtimestamp(System.currentTimeMillis());
      }
      //TODO validate youtube and vimeo link for video id

      attach.setAttachtype(PageAttachType.WEB_LINK);

      if (destGuideUrlInfo.getInDestGuideUrlType() != null) {
        try {
          if (destGuideUrlInfo.getInDestGuideUrlType().equals(PageAttachType.VIDEO_LINK.getStrVal())) {
            if (destGuideUrlInfo.getInDestGuideUrl() != null &&
                !destGuideUrlInfo.getInDestGuideUrl().toLowerCase().contains("youtube.com") &&
                !destGuideUrlInfo.getInDestGuideUrl().toLowerCase().contains("youtu.be") &&
                destGuideUrlInfo.getInDestGuideUrl().toLowerCase().contains("vimeo.com")) {
              BaseView baseView = new BaseView();
              baseView.message = "System Error - please resubmit";
              return ok(views.html.common.message.render(baseView));
            }
            else {
              attach.setAttachtype(PageAttachType.VIDEO_LINK);
            }
          }
        }
        catch (NumberFormatException ne) {
          ne.printStackTrace();
        }
      }
      //set rank
      int maxRow = DestinationGuideAttach.maxPageRankByDestGuideId(destGuide.destinationguideid, attach.getAttachtypeStr());
      attach.setRank(++maxRow);

      attach.setName(destGuideUrlInfo.getInDestGuideUrlTitle());
      if (destGuideUrlInfo.getInDestGuideUrl() != null && !destGuideUrlInfo.getInDestGuideUrl()
                                                                           .toLowerCase()
                                                                           .startsWith("http")) {
        destGuideUrlInfo.setInDestGuideUrl("http://" + destGuideUrlInfo.getInDestGuideUrl());
      }

      attach.setAttachurl(destGuideUrlInfo.getInDestGuideUrl());
      attach.setAttachname(destGuideUrlInfo.getInDestGuideUrlTitle());
      attach.setComments(destGuideUrlInfo.getInDestGuideUrlCaption());
      attach.setLastupdatedtimestamp(System.currentTimeMillis());
      attach.setModifiedby(sessionMgr.getUserId());
      attach.save();
      Ebean.commitTransaction();
      if (destGuideUrlInfo.getInDestGuideUrlId() == null || destGuideUrlInfo.getInDestGuideUrlId().length() == 0) {
        flash(SessionConstants.SESSION_PARAM_MSG, destGuideUrlInfo.getInDestGuideUrlTitle() + " added successfully");
      }
      else {
        flash(SessionConstants.SESSION_PARAM_MSG,
              destGuideUrlInfo.getInDestGuideUrlTitle() + " updated successfully");
      }
      invalidateCache(destGuide.destinationid);
      return redirect(routes.DestinationController.newDestinationGuide(destGuide.destinationid,
                                                                       destGuideUrlInfo.getInDestGuideId()));
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result createFileAttachment() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    Form<FileUploadForm> form = formFactory.form(FileUploadForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return BaseJsonResponse.formError(form);
    }

    FileUploadForm destUploadInfo = form.get();
    String uploadType = destUploadInfo.getInType();

    if (destUploadInfo.getInDestId() == null || destUploadInfo.getInDestGuideId() == null) {
      return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_MISSING_DATA);
    }
    
    if (uploadType.equals("IMG")) {
      if (destUploadInfo.getInDestPhotoName() == null || destUploadInfo.getInDestPhotoName().length() == 0) {
        return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_MISSING_DATA, "Browser did not provide filename");
      }
    } else if (uploadType.equals("FILE")) {
      if (destUploadInfo.getInDestFileName() == null || destUploadInfo.getInDestFileName().length() == 0) {
        return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_MISSING_DATA, "Browser did not provide filename");
      }
    }

    Destination destination = Destination.find.byId(destUploadInfo.getInDestId());
    if (destination == null || !SecurityMgr.canEditDestination(destination, sessionMgr)) {
      return BaseJsonResponse.codeResponse(ReturnCode.AUTH_DOCUMENT_FAIL, "Unauthorized document access");
    }

    DestinationGuide destGuide = DestinationGuide.find.byId(destUploadInfo.getInDestGuideId());
    if (destGuide == null || !destGuide.destinationid.equals(destination.destinationid)) {
      return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_WRONG_DATA, "Document page ID mismatch.");
    }

    if (uploadType.equals("IMG")) {
      int count = DestinationGuideAttach.activeCountByDestGuideId(destGuide.destinationguideid,
          PageAttachType.PHOTO_LINK.getStrVal());
      if (count > 10) {
        BaseJsonResponse.codeResponse(ReturnCode.RESOURCE_LIMIT_REACHED, "You have exceeded the maximum number of " +
            "photos.");
      }
    } else if (uploadType.equals("FILE")) {
      int count = DestinationGuideAttach.activeCountByDestGuideId(destGuide.destinationguideid,
          PageAttachType.FILE_LINK.getStrVal());
      if (count > 10) {
        BaseJsonResponse.codeResponse(ReturnCode.RESOURCE_LIMIT_REACHED, "You have exceeded the maximum number of " +
            "files.");
      }
    }

    Account a;
    if(sessionMgr.getAccount().isPresent()) {
      a = sessionMgr.getAccount().get();
    } else {
      a = Account.find.byId(sessionMgr.getAccountId());
    }

    DestinationGuideAttach attach = DestinationGuideAttach.buildGuideAttachment(destGuide, sessionMgr.getUserId());
    String nextUrl = routes.DestinationPostController.uploadSuccess(destGuide.destinationid,
                                                                    destGuide.destinationguideid,
                                                                    attach.getPk()).url();
    ObjectNode result = Json.newObject();

    if (uploadType.equals("IMG")) {
      FileImage fileImage = uploadHelper(attach, destUploadInfo.getInDestPhotoName(), a, result, nextUrl);

      if (fileImage != null) {
        attach.setName(destUploadInfo.getInDestPhotoName());
        attach.setFileImage(fileImage);
        attach.setAttachurl(fileImage.getUrl());
        attach.setAttachname(fileImage.getFilename());
        attach.setStatus(APPConstants.STATUS_PENDING);
        attach.setAttachtype(PageAttachType.PHOTO_LINK);
        int maxRow = DestinationGuideAttach.maxPageRankByDestGuideId(destGuide.destinationguideid, attach.getAttachtypeStr());
        attach.setRank(++maxRow);
        attach.setComments(destUploadInfo.getInDestPhotoCaption());
        attach.update();
        return ok(result);
      }
    } else if (uploadType.equals("FILE")) {
      FileInfo file = FileController.uploadHelper(attach, destUploadInfo.getInDestFileName(), a, result, nextUrl);

      if (file != null) {
        attach.setName(destUploadInfo.getInDestFileName());
        attach.setFileInfo(file);
        attach.setAttachurl(file.getUrl());
        attach.setAttachname(file.getFilename());
        attach.setStatus(APPConstants.STATUS_PENDING);
        attach.setAttachtype(PageAttachType.FILE_LINK);
        int maxRow = DestinationGuideAttach.maxPageRankByDestGuideId(destGuide.destinationguideid, attach.getAttachtypeStr());
        attach.setRank(++maxRow);
        attach.setComments(destUploadInfo.getInDestFileCaption());
        attach.update();
        return ok(result);
      }
    }

    return BaseJsonResponse.codeResponse(ReturnCode.STORAGE_S3_UPLOAD, "3rd party service provider error.");
  }

  @With({Authenticated.class, Authorized.class})
  public Result uploadSuccess(String destId, String guideId, String attachId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Destination destination = Destination.find.byId(destId);
    if (destination == null || !SecurityMgr.canEditDestination(destination, sessionMgr)) {
      return UserMessage.message(ReturnCode.AUTH_DOCUMENT_FAIL, "Unauthorized document access");
    }

    DestinationGuide destGuide = DestinationGuide.find.byId(guideId);
    if (destGuide == null || !destGuide.destinationid.equals(destination.destinationid)) {
      return UserMessage.message(ReturnCode.HTTP_REQ_WRONG_DATA, "Document page ID mismatch.");
    }

    try {
      DestinationGuideAttach attach = DestinationGuideAttach.find.byId(attachId);
      if (attach == null || !attach.destinationguideid.equals(guideId)) {
        return UserMessage.message(ReturnCode.HTTP_REQ_WRONG_DATA, "Page image ID mismatch.");
      }

      //Error happened during previous step of confirmation
      if(attach.getFileImage() == null && attach.getFileInfo() == null) {
        attach.delete();
        return UserMessage.message(ReturnCode.LOGICAL_ERROR, "File upload failure. Please retry or contact support.");
      }

      String successMsg = "Photo uploaded successfully";

      if (attach.getAttachtype() == PageAttachType.FILE_LINK) {
        successMsg = "File uploaded successfully";
      }

      Ebean.beginTransaction();
      attach.setStatus(APPConstants.STATUS_ACTIVE);
      attach.setLastupdatedtimestamp(System.currentTimeMillis());
      attach.setModifiedby(sessionMgr.getUserId());
      attach.save();
      Ebean.commitTransaction();
      invalidateCache(destination.destinationid);
      flash(SessionConstants.SESSION_PARAM_MSG, successMsg);
      return redirect(routes.DestinationController.newDestinationGuide(destId, guideId));
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result createCover() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    Form<FileUploadForm> form = formFactory.form(FileUploadForm.class).bindFromRequest();
    if (form.hasErrors()) {
      return BaseJsonResponse.formError(form);
    }

    FileUploadForm destPhotoInfo = form.get();
    if(destPhotoInfo.getInDestId() == null ||
       destPhotoInfo.getInDestPhotoName() == null ||
       destPhotoInfo.getInDestPhotoName().length() == 0) {
      return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_MISSING_DATA);
    }

    Destination destination = Destination.find.byId(destPhotoInfo.getInDestId());
    if (destination == null ||
        !SecurityMgr.canEditDestination(destination, sessionMgr) ||
        destination.status != APPConstants.STATUS_ACTIVE) {
      return BaseJsonResponse.codeResponse(ReturnCode.AUTH_DOCUMENT_FAIL, "Unauthorized document access");
    }

    Account a;
    if(sessionMgr.getAccount().isPresent()) {
      a = sessionMgr.getAccount().get();
    } else {
      a = Account.find.byId(sessionMgr.getAccountId());
    }

    String nextUrl = routes.DestinationController.newDestination(destination.destinationid).url();
    if (destination.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE ||
        destination.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE) {
      nextUrl = routes.TripController.guides(destPhotoInfo.getInTripId(),
                                             new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.CUSTOM_DOC)).url();
    }


    ObjectNode result = Json.newObject();
    FileImage fileImage = uploadHelper(destination, destPhotoInfo.getInDestPhotoName(), a, result, nextUrl);

    if(fileImage != null) {
      destination.setCovername(destPhotoInfo.getInDestPhotoName());
      destination.setCoverurl(fileImage.getUrl());
      destination.setFileImage(fileImage);
      destination.markModified(sessionMgr.getUserId());
      destination.update();
      invalidateCache(destination.getDestinationid());
      return ok(result);
    }

    return BaseJsonResponse.codeResponse(ReturnCode.STORAGE_S3_UPLOAD, "3rd party service provider error.");
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteCover() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    final DynamicForm form = formFactory.form().bindFromRequest();
    String docId = form.get("inDocId");
    String source = form.get("inSource");
    String tripId = form.get("inTripId"); //for custom docs only
    if (docId == null || docId.trim().length() == 0) {
      ObjectNode result = Json.newObject();
      result.put("msg", "Error - please resubmit");
      return ok(result);
    }
    else {
      Destination destination = Destination.find.byId(docId);
      if (!SecurityMgr.canEditDestination(destination, sessionMgr)) {
        ObjectNode result = Json.newObject();
        result.put("msg", "Error - Unauthorized access");
        return ok(result);
      }

      try {
        Ebean.beginTransaction();
        destination.setCovername(null);
        destination.setCoverurl(null);
        destination.setFileImage(null);
        destination.markModified(sessionMgr.getUserId());
        destination.save();
        Ebean.commitTransaction();
        flash(SessionConstants.SESSION_PARAM_MSG, "Cover Photo deleted successfully. ");
        invalidateCache(destination.getDestinationid());

        if (source.equals("CUSTOM_DOC")) {
          return redirect(routes.TripController.guides(tripId, new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.CUSTOM_DOC)));
        }
        return redirect(routes.DestinationController.newDestination(destination.destinationid));
      }
      catch (Exception e) {
        Ebean.rollbackTransaction();
        BaseView baseView = new BaseView();
        baseView.message = "System Error - please resubmit";
        return ok(views.html.common.message.render(baseView));
      }
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteDestinationGuideUrl() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<DestinationGuideUrlForm> form = formFactory.form(DestinationGuideUrlForm.class).bindFromRequest();
    DestinationGuideUrlForm destGuideUrlInfo = form.get();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    if (destGuideUrlInfo.getInDestId() == null ||
        destGuideUrlInfo.getInDestGuideId() == null) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA);
    }

    Destination destination = Destination.find.byId(destGuideUrlInfo.getInDestId());
    if (destination == null || !SecurityMgr.canEditDestination(destination, sessionMgr)) {
      BaseView baseView = new BaseView();
      baseView.message = "Cannot access this guide  - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    DestinationGuide destGuide = DestinationGuide.find.byId(destGuideUrlInfo.getInDestGuideId());
    if (destGuide == null || !destGuide.destinationid.equals(destination.destinationid)) {
      BaseView baseView = new BaseView();
      baseView.message = "Cannot access this guide  - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      DestinationGuideAttach attach = null;
      if (destGuideUrlInfo.getInDestGuideUrlId() != null && destGuideUrlInfo.getInDestGuideUrlId().length() > 0) {
        attach = DestinationGuideAttach.find.byId(destGuideUrlInfo.getInDestGuideUrlId());
        if (attach == null || attach.status != APPConstants.STATUS_ACTIVE ||
            !attach.destinationguideid.equals(destGuide.destinationguideid)) {
          BaseView baseView = new BaseView();
          baseView.message = "System Error - please resubmit";
          return ok(views.html.common.message.render(baseView));
        }
        Ebean.beginTransaction();

        attach.setStatus(APPConstants.STATUS_DELETED);
        attach.setModifiedby(sessionMgr.getUserId());
        attach.setLastupdatedtimestamp(System.currentTimeMillis());

        attach.save();
        Ebean.commitTransaction();

        if (attach.getAttachname() != null) {
          flash(SessionConstants.SESSION_PARAM_MSG, attach.getAttachname() + " deleted successfully");
        }
        else {
          flash(SessionConstants.SESSION_PARAM_MSG, "Attachment deleted successfully");
        }

        invalidateCache(destination.destinationid);
        return redirect(routes.DestinationController.newDestinationGuide(destination.destinationid,
                                                                         destGuideUrlInfo.getInDestGuideId()));
      }
      else {
        BaseView baseView = new BaseView();
        baseView.message = "System Error - please resubmit";
        return ok(views.html.common.message.render(baseView));
      }
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
  }

  //POST
  @With({Authenticated.class, Authorized.class})
  public Result copyDestination() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<DestinationForm> destForm = formFactory.form(DestinationForm.class);
    DestinationForm destInfo = destForm.bindFromRequest().get();
    if (destInfo != null && destInfo.getInDestId() != null && destInfo.getInDestId().length() > 0) {
      Destination sourceDest = Destination.find.byId(destInfo.getInDestId());

      if (sourceDest != null && SecurityMgr.canEditDestination(sourceDest, sessionMgr)) {
        String name = sourceDest.getName();
        if (destInfo.getInDestName() != null && destInfo.getInDestName().trim().length() > 0) {
          name = destInfo.getInDestName();
        }

        Destination targetDest = DestinationController.copyDestination(sourceDest, name, sessionMgr.getUserId());
        if (targetDest != null) {
          BaseView baseView = new BaseView();
          baseView.message = "Document copied successfully.";
          return ok(views.html.common.message.render(baseView));
        }
      }
    }

    BaseView baseView = new BaseView();
    baseView.message = "System Error - please resubmit";
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result copyDestinationGuide() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<DestinationGuideForm> destGuideForm = formFactory.form(DestinationGuideForm.class);
    DestinationGuideForm destGuideInfo = destGuideForm.bindFromRequest().get();
    if (destGuideInfo != null &&
        destGuideInfo.getInDestId() != null &&
        destGuideInfo.getInDestId().length() > 0 &&
        destGuideInfo.getInDestGuideId() != null) {
      DestinationGuide sourceGuide = DestinationGuide.find.byId(destGuideInfo.getInDestGuideId());

      Destination targetDestination = Destination.find.byId(destGuideInfo.getInDestId());

      if (sourceGuide != null &&
          targetDestination != null &&
          SecurityMgr.canEditDestination(targetDestination, sessionMgr)) {

        Destination sourceDest = Destination.find.byId(sourceGuide.destinationid);
        //make sure this is going to the same cmpy or going to a trip document
        if (sourceDest != null &&
            (sourceDest.getCmpyid().equals(targetDestination.getCmpyid()) ||
             targetDestination.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE ||
             targetDestination.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE)) {
          DestinationGuide targetGuide = DestinationController.copyDestinationGuide(targetDestination.getDestinationid(),
                                                                                    sourceGuide,
                                                                                    sessionMgr.getUserId());

          Trip tripModel = null;
          List<TripDestination> tdests = TripDestination.findActiveByDest(targetDestination.getDestinationid());
          if (tdests != null && tdests.size() != 0) {
            tripModel = Trip.find.byId(tdests.get(0).getTripid());
          }

          //Create audit log for a trip operation
          if (targetDestination.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE ||
              targetDestination.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE) {

            audit:
            {


              if (tripModel != null) {
                TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_DOC_CUSTOM,
                                                     AuditActionType.MODIFY,
                                                     AuditActorType.WEB_USER)
                                        .withTrip(tripModel)
                                        .withCmpyid(tripModel.cmpyid)
                                        .withUserid(sessionMgr.getUserId());

                ((AuditTripDocCustom) ta.getDetails()).withGuideId(targetDestination.getDestinationid())
                                                      .withName(targetDestination.getName())
                                                      .withCoverImage(targetDestination.getCovername())
                                                      .withText("Copied Existing Page: " + targetGuide.getName());
                ta.save();;
              }
              else {
                Log.log(LogLevel.ERROR, "Trip ID can not be determined while adding new destination page audit:" + targetDestination.getDestinationid());
              }
            }
          }

          if (targetGuide != null) {
            invalidateCache(targetDestination.getDestinationid());

            if (targetDestination.destinationtypeid == APPConstants.TRIP_DOC_DESTINATION_TYPE ||
                targetDestination.destinationtypeid == APPConstants.TOUR_DOC_DESTINATION_TYPE) {
              flash(SessionConstants.SESSION_PARAM_MSG, "Page copied successfully to " + targetDestination.name + ".");

              return redirect(routes.TripController.guides(tripModel.tripid,
                                           new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.CUSTOM_DOC)));
            }
            else {
              flash(SessionConstants.SESSION_PARAM_MSG, "Page copied successfully to " + targetDestination.name + ".");
              return redirect(routes.DestinationController.newDestination(targetDestination.destinationid));
            }
          }
        }
      }
    }


    BaseView baseView = new BaseView();
    baseView.message = "System Error - please resubmit";
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result createTripDestination() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<DestinationForm> form = formFactory.form(DestinationForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    DestinationForm destInfo = form.get();

    if (destInfo.getInTripId() == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Action not authorized";
      return ok(views.html.common.message.render(baseView));
    }

    Trip tripModel = Trip.findByPK(destInfo.getInTripId());
    if (SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "Action not authorized";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();
      Destination dest;
      if (destInfo.getInDestId() == null || destInfo.getInDestId().length() == 0) {
        dest = new Destination();
        dest.setStatus(APPConstants.STATUS_ACTIVE);
        dest.setCreatedby(sessionMgr.getUserId());
        dest.setCreatedtimestamp(System.currentTimeMillis());
        dest.setDestinationid(Utils.getUniqueId());
      }
      else {
        dest = Destination.find.byId(destInfo.getInDestId());
        if (dest == null || dest.status != APPConstants.STATUS_ACTIVE ||
            !SecurityMgr.canEditDestination(dest, sessionMgr)) {
          BaseView baseView = new BaseView();
          baseView.message = "Unauthorized Action";
          return ok(views.html.common.message.render(baseView));
        }
      }

      if (tripModel.triptype == APPConstants.TOUR_TYPE) {
        dest.setDestinationtypeid(APPConstants.TOUR_DOC_DESTINATION_TYPE);
      }
      else {
        dest.setDestinationtypeid(APPConstants.TRIP_DOC_DESTINATION_TYPE);
      }

      dest.setCity(destInfo.getInDestCity());
      dest.setCmpyid(tripModel.getCmpyid());
      dest.setCountry(destInfo.getInDestCountry());
      dest.setIntro(destInfo.getInDestIntro());
      dest.setTag(destInfo.getInDestTag());
      if (destInfo.getInDestLocLat() != null) {
        try {
          float lat = Float.parseFloat(destInfo.getInDestLocLat());
          dest.setLoclat(lat);
        }
        catch (NumberFormatException ne) {
          dest.setLoclat(0);
        }
      }
      else {
        dest.setLoclat(0);
      }

      if (destInfo.getInDestLocLong() != null) {
        try {
          float locLong = Float.parseFloat(destInfo.getInDestLocLong());
          dest.setLoclong(locLong);
        }
        catch (NumberFormatException ne) {
          dest.setLoclong(0);

        }
      }
      else {
        dest.setLoclong(0);
      }

      dest.setName(destInfo.getInDestName());
      dest.setModifiedby(sessionMgr.getUserId());
      dest.setLastupdatedtimestamp(System.currentTimeMillis());
      dest.setAccessLevel(Destination.AccessLevel.PUBLIC);
      dest.save();


      //add the guide to the trip
      //note: a reference to the trip document can be found in the trip_destination table
      // tipdestid == tripid indicates the reference to the trip document
      TripDestination tripDestination = TripDestination.find.byId(tripModel.tripid);
      if (tripDestination == null) {
        tripDestination = new TripDestination();
        tripDestination.setCreatedby(sessionMgr.getUserId());
        tripDestination.setCreatedtimestamp(System.currentTimeMillis());
      }
      tripDestination.setName(dest.name);
      tripDestination.setTripdestid(tripModel.tripid);
      tripDestination.setDestinationid(dest.destinationid);
      tripDestination.setTripid(tripModel.tripid);
      tripDestination.setStatus(APPConstants.STATUS_ACTIVE);
      tripDestination.setLastupdatedtimestamp(System.currentTimeMillis());
      tripDestination.setModifiedby(sessionMgr.getUserId());
      tripDestination.setMergebookings(destInfo.isInMergeBookings());
      tripDestination.save();

      Ebean.commitTransaction();

      //Create audit log
      audit:
      {
        TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_DOC_CUSTOM,
                                             (destInfo.getInDestId() == null || destInfo.getInDestId().length() == 0)?
                                             AuditActionType.ADD:AuditActionType.MODIFY,
                                             AuditActorType.WEB_USER)
                                .withTrip(tripModel)
                                .withCmpyid(tripModel.cmpyid)
                                .withUserid(sessionMgr.getUserId());

        ((AuditTripDocCustom) ta.getDetails()).withGuideId(dest.getDestinationid())
                                              .withName(dest.getName());
        ta.save();;
      }

      invalidateCache(dest.destinationid);
      if (destInfo.getInDestId() == null || destInfo.getInDestId().length() == 0) {
        flash(SessionConstants.SESSION_PARAM_MSG, "Trip Document added successfully");
        //save new id for retieval
        flash(SessionConstants.SESSION_DEST_ID, dest.getDestinationid());

      }
      else {
        flash(SessionConstants.SESSION_PARAM_MSG, "Trip Document updated successfully");

      }
      return  redirect(routes.TripController.guides(tripModel.tripid,
                                   new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.CUSTOM_DOC)));
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      Log.log(LogLevel.DEBUG, "Error inserting destination", e);
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }


  }

  @With({Authenticated.class, Authorized.class})
  public Result reorderPages() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    final DynamicForm form = formFactory.form().bindFromRequest();
    String destId = form.get("inDestId");
    String tripId = form.get("tripId");
    String inDestGuideIds = form.get("inDestGuideIds");
    if (inDestGuideIds != null && destId != null && inDestGuideIds.trim().length() > 0) {
      String[] destGuideIds = inDestGuideIds.split(",");

      Destination dest = Destination.find.byId(destId);
      if (dest != null && SecurityMgr.canEditDestination(dest, sessionMgr)) {
        try {
          Ebean.beginTransaction();
          int count = 0;
          for (String id : destGuideIds) {
            DestinationGuide guide = DestinationGuide.find.byId(id);
            if (guide != null &&
                guide.status == APPConstants.STATUS_ACTIVE &&
                guide.destinationid.equals(dest.destinationid)) {
              guide.setRank(count);
              guide.setLastupdatedtimestamp(System.currentTimeMillis());
              guide.setModifiedby(sessionMgr.getUserId());
              guide.save();
            }
            count++;
          }
          Ebean.commitTransaction();
          BaseView baseView = new BaseView();
          invalidateCache(dest.destinationid);
          if (tripId != null && tripId.trim().length() > 0) {
            return  redirect(routes.TripController.guides(tripId.trim(),
                                         new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.CUSTOM_DOC)));
          } else
          return ok(views.html.common.message.render(baseView));

        }
        catch (Exception e) {
          Ebean.rollbackTransaction();
          Log.log(LogLevel.ERROR, "reorderPages: " + destId, e);
        }
      }
    }
    BaseView baseView = new BaseView();
    baseView.message = "Cannot reorder pages";
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result reorderPhotos() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    final DynamicForm form = formFactory.form().bindFromRequest();
    String destId = form.get("inDestId");
    String destGuideId = form.get("inDestGuideId");
    String inDestFileIds = form.get("inDestFileIds");
    if (inDestFileIds != null && destId != null && inDestFileIds.trim().length() > 0) {
      String[] destFileIds = inDestFileIds.split(",");

      Destination dest = Destination.find.byId(destId);
      DestinationGuide destGuide = DestinationGuide.find.byId(destGuideId);
      if (dest != null && SecurityMgr.canEditDestination(dest, sessionMgr) &&
          destGuide != null && destGuide.destinationid.equals(dest.destinationid)) {
        try {
          Ebean.beginTransaction();
          int count = 0;
          for (String id : destFileIds) {
            DestinationGuideAttach file = DestinationGuideAttach.find.byId(id);
            if (file != null && file.status == APPConstants.STATUS_ACTIVE &&
                file.getDestinationguideid().equals(destGuide.destinationguideid)) {
              file.setRank(count);
              file.setLastupdatedtimestamp(System.currentTimeMillis());
              file.setModifiedby(sessionMgr.getUserId());
              file.save();
            }
            count++;
          }
          Ebean.commitTransaction();
          BaseView baseView = new BaseView();
          invalidateCache(dest.destinationid);
          return ok(views.html.common.message.render(baseView));

        }
        catch (Exception e) {
          Ebean.rollbackTransaction();
          Log.log(LogLevel.ERROR, "reorderPhotos: " + destGuideId, e);

        }
      }
    }
    BaseView baseView = new BaseView();
    baseView.message = "Cannot reorder Photos";
    return ok(views.html.common.message.render(baseView));
  }

}
