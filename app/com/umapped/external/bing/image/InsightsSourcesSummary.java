
package com.umapped.external.bing.image;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "shoppingSourcesCount",
    "recipeSourcesCount"
})
public class InsightsSourcesSummary {

    @JsonProperty("shoppingSourcesCount")
    private int shoppingSourcesCount;
    @JsonProperty("recipeSourcesCount")
    private int recipeSourcesCount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The shoppingSourcesCount
     */
    @JsonProperty("shoppingSourcesCount")
    public int getShoppingSourcesCount() {
        return shoppingSourcesCount;
    }

    /**
     * 
     * @param shoppingSourcesCount
     *     The shoppingSourcesCount
     */
    @JsonProperty("shoppingSourcesCount")
    public void setShoppingSourcesCount(int shoppingSourcesCount) {
        this.shoppingSourcesCount = shoppingSourcesCount;
    }

    /**
     * 
     * @return
     *     The recipeSourcesCount
     */
    @JsonProperty("recipeSourcesCount")
    public int getRecipeSourcesCount() {
        return recipeSourcesCount;
    }

    /**
     * 
     * @param recipeSourcesCount
     *     The recipeSourcesCount
     */
    @JsonProperty("recipeSourcesCount")
    public void setRecipeSourcesCount(int recipeSourcesCount) {
        this.recipeSourcesCount = recipeSourcesCount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "InsightsSourcesSummary{" +
            "shoppingSourcesCount=" + shoppingSourcesCount +
            ", recipeSourcesCount=" + recipeSourcesCount +
            ", additionalProperties=" + additionalProperties +
            '}';
    }
}
