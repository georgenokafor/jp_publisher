package com.mapped.publisher.persistence.cache;

import java.io.Serializable;

/**
 * POI Import Cache Object.
 *
 * Created by surge on 2015-02-20.
 */
public class PoiFeedStateCO
    implements Serializable {
  static public enum State {
    INIT,
    IMPORT,
    /** Clearing amenities  */
    AMENITIES,
    ASSOCIATE,
    POI_SYNC,
    STOPPED
  }

  /**
   * State of the object
   */
  public State currState;

  /**
   * Last Updated Timestamp
   */
  public long lastUpdatedTs;

  /**
   * Current item number;
   */
  public int currItem;

  /**
   * Total number of items to process
   */
  public int totalItems;


  public PoiFeedStateCO() {
    currState = State.STOPPED;
    lastUpdatedTs = 0;
    currItem = 0;
    totalItems = 0;
  }

  public boolean start(State state, int totalItems) {
    /* TODO: Serguei: Decide if this is what I want
    if (currState != State.STOPPED) {
      return false;
    }
    */
    currState = state;
    lastUpdatedTs = System.currentTimeMillis();
    currItem = 0;
    this.totalItems = totalItems;
    return true;
  }

  /**
   * Update when currItem counter is maintained externally
   */
  public void update(){
    lastUpdatedTs = System.currentTimeMillis();
  }

  public void update(int currItemId) {
    lastUpdatedTs = System.currentTimeMillis();
    currItem = currItemId;
  }

  public void stop() {
    currState = State.STOPPED;
    lastUpdatedTs = System.currentTimeMillis();
    currItem = 0;
    totalItems = 0;
  }
}
