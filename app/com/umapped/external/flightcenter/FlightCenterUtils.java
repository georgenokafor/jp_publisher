package com.umapped.external.flightcenter;

import com.mapped.publisher.utils.Log;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.client.HttpClient;

public class FlightCenterUtils {
    public static boolean destImageExists(String brand, String dest, int pageNum) {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet s3Get = new HttpGet("https://s3.amazonaws.com/static-umapped-com/public/external/flightcenter/" + brand + "/cover/" + dest + "-cover" + pageNum + ".jpg");

        try {
            HttpResponse resp = client.execute(s3Get);
            return resp.getStatusLine().getStatusCode() == 200;
        } catch(Exception e) {
            Log.err("Exception in FlightCenterUtils.java: " + e.getMessage());
            return false;
        }
    }
}
