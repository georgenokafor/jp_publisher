package com.umapped.itinerary.analyze.adapters;

import com.mapped.publisher.parse.schemaorg.ActivityReservation;
import com.mapped.publisher.parse.schemaorg.Reservation;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.Location;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by wei on 2017-03-07.
 */
public class ActivityReservationAdapter
    extends AbstractReservationItinearyItemAdapter {
  @Override protected ItineraryItemType getItemType(Reservation reservation) {
    if (StringUtils.endsWith(reservation.additionalType, "CruiseStopReservation")) {
      return ItineraryItemType.CruiseStop;
    } else {
      return ItineraryItemType.Activity;
    }
  }

  @Override protected Location getStartLocation(Reservation reservation) {
    ActivityReservation r = (ActivityReservation) reservation;
    Location location = getLocationFromPlace(r.startLocation);
    if (StringUtils.isEmpty(location.getDescription())) {
      location.setDescription(r.name);
    }
    return location;
  }

  // Activity only has start location
  @Override protected Location getEndLocation(Reservation reservation) {
   return null;
  }
}
