package com.mapped.publisher.persistence;

import com.mapped.publisher.common.SecurityMgr;
import models.publisher.Trip;

/**
 * Created by twong on 2014-06-05.
 */
public class SharedTripRS {
  public  Trip trip;

  public SecurityMgr.AccessLevel accessLevel;

  public SecurityMgr.AccessLevel getAccessLevel() {
    return accessLevel;
  }

  public void setAccessLevel(SecurityMgr.AccessLevel accessLevel) {
    this.accessLevel = accessLevel;
  }

  public Trip getTrip() {
    return trip;
  }

  public void setTrip(Trip trip) {
    this.trip = trip;
  }
}
