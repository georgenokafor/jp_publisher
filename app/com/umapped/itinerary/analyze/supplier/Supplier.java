package com.umapped.itinerary.analyze.supplier;

/**
 * Created by wei on 2017-04-06.
 */
public enum Supplier {
  WCities,
  Travelbound,
  Amusement,
  ShoreTrip,
  Musement
}
