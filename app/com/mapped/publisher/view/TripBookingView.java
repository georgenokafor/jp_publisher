package com.mapped.publisher.view;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.Credentials;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.parse.extractor.booking.BookingExtractor;
import com.mapped.publisher.parse.schemaorg.Reservation;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import controllers.routes;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import play.api.i18n.Lang;

import java.io.Serializable;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-19
 * Time: 4:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class TripBookingView
    extends BaseView
    implements Serializable {

  public String tripId;
  public String tripName;
  public String tripStartDatePrint;
  public String tripEndDatePrint;
  public String tripStartDate;
  public String tripEndDate;
  public String comments;
  public String logoUrl;
  public AgentView agentView;

  public int tripStatus;
  public String tripCmpyId;
  public Integer tripCmpyIdInt;
  public TripBookingDetailView bookingDetail;
  public UmReservationView reservationDetail;
  public TabType activeTab = TabType.FLIGHTS;

  public EnumMap<ReservationType, List<TripBookingDetailView>> bookings;

  public boolean printFriendlyPDF = false;

  //Serguei: temporary measure until all templates are converted individual types kept in place
  public List<TripBookingDetailView> hotels;
  public List<TripBookingDetailView> cruises;
  public List<TripBookingDetailView> flights;
  public List<TripBookingDetailView> transfers;
  public List<TripBookingDetailView> activities;
  public List<TripBookingDetailView> notes;
  public Lang lang;


  public List<TripBookingDetailView> orderedBookings;

  public List<AttachmentView> tripAttachments;

  public String cities;
  public String countries;

  public List<TripBrandingView> tripBranding;

  public HashMap<String, List<TripBookingDetailView>> chronologicalBookings;
  public List<String> chronologicalDates;
  public List<String> chronologicalDatesTimeline;

  public PoiBaseView pbv;

  public String defaultParser = null;

  public String tripCmpyEmail = null;
  public boolean isCollablorator = false;

  public Map<String, String> mobilizedDomains = null;
  public boolean addWCitiesGuideBtn = false;

  public void setupPoiView(SessionMgr sessionMgr) {
    pbv = new PoiBaseView();
    pbv.userCompanies = new HashMap<>();
    Credentials cred = sessionMgr.getCredentials();
    pbv.userCompanies.put(cred.getCmpyIdInt(), cred.getCmpyName());
    pbv.isUMappedAdmin = SecurityMgr.isUmappedAdmin(sessionMgr);
  }

  public TripBookingView() {
    bookings = new EnumMap<>(ReservationType.class);

    /* Serguei: Temporary measure until all template code is generalized */
    hotels = new ArrayList<>();
    cruises = new ArrayList<>();
    flights = new ArrayList<>();
    transfers = new ArrayList<>();
    activities = new ArrayList<>();
  }

  public List<TripBookingDetailView> getSubtreeBookings(ReservationType type) {
    List<TripBookingDetailView> result = new ArrayList<TripBookingDetailView>();

    for (ReservationType b : ReservationType.values()) {
      if (b == type || b.getParent() == type){
        List<TripBookingDetailView> currList = bookings.get(b);
        if (currList!=null) {
          result.addAll(currList);
        }
      }
    }

    //sort the result chronologically
    Collections.sort(result, TripBookingDetailView.TripBookingDetailViewComparator);
    return result;
  }

  public void addBookingDetailView(ReservationType type, TripBookingDetailView view) {
    if (type == null || view == null) {
      Log.log(LogLevel.ERROR, "Failed to add trip booking detail view either type or view is null");
      return;
    }

    List<TripBookingDetailView> tbdvs = bookings.get(type);
    if (tbdvs == null) {
      tbdvs = new ArrayList<>();
      bookings.put(type, tbdvs);
    }
    tbdvs.add(view);

    //Serguei: Temporary Measure Until All Code is converted
    switch (type.getRootLevelType()) {
      case FLIGHT:
        flights.add(view);
        break;
      case CRUISE:
        cruises.add(view);
        break;
      case HOTEL:
        hotels.add(view);
        break;
      case TRANSPORT:
        transfers.add(view);
        break;
      case ACTIVITY:
        activities.add(view);
        break;
    }
  }

  public void  removeOfferNotes() {
    if (notes != null && notes.size() > 0) {
      Iterator it = notes.iterator();
      while (it.hasNext()) {
        TripBookingDetailView n = (TripBookingDetailView) it.next();
        if (n != null && n.tag != null && n.tag.contains("musement")) {
          it.remove();
        }
      }
    }
  }
  public void getChronologicalBookings() {
    HashMap<String, List<TripBookingDetailView>> notesForLinkedBookings = new HashMap<>();
    List<TripBookingDetailView> notesForLastItem = new ArrayList<>();

    chronologicalBookings = new HashMap<>();
    chronologicalDates = new ArrayList<>();
    chronologicalDatesTimeline = new ArrayList<>();
    orderedBookings = new ArrayList<>();

    for (ReservationType type : bookings.keySet()) {
      orderedBookings.addAll(bookings.get(type));
    }

    if (notes != null) {
      //add all notes that are not linked to a booking... notes linked to bookings will be added to the sorted list
      for (TripBookingDetailView note: notes) {
        if (note.tag != null && note.tag.contains(APPConstants.NOTE_TAG_LAST_ITEM)) {
          notesForLastItem.add(note);
        } else if (note.linkedTripDetailsId == null || note.linkedTripDetailsId.trim().length() == 0) {
          orderedBookings.add(note);
        } else {
          List<TripBookingDetailView> linkedNotes = notesForLinkedBookings.get(note.linkedTripDetailsId);
          if (linkedNotes == null) {
            linkedNotes = new ArrayList<>();
          }
          linkedNotes.add(note);
          notesForLinkedBookings.put(note.linkedTripDetailsId, linkedNotes);
        }
      }
    }

    try {
      Collections.sort(orderedBookings, TripBookingDetailView.TripBookingDetailViewComparator);
    } catch (Exception e) {
      e.printStackTrace();
      Log.err("TripBookingView:getChronologicalBookings Cannot sort trip:" + tripId, e);
    }
    List <String> datesToReorder = new ArrayList <>();

    for (TripBookingDetailView b : orderedBookings) {
      String date = null;
      String dateTimeline = null;
      if (lang == null) {
        date = Utils.formatTimestamp(b.startDateMs, "EEE MMM dd, yyyy", "en-US");
        dateTimeline = Utils.formatTimestamp(b.startDateMs, "dd/MM/yyyy", "en-US");
      } else {
        date = Utils.formatTimestamp(b.startDateMs, "EEE MMM dd, yyyy", lang.language());
        dateTimeline = Utils.formatTimestamp(b.startDateMs, "dd/MM/yyyy", "en-US");
      }
      if (date.contains(", 1970")) { //if there are dates set to 0, don't show the date
        date = "";
        dateTimeline = "";
      }
      if (!chronologicalDates.contains(date)) {
        chronologicalDates.add(date);
      }
      if(!chronologicalDatesTimeline.contains(dateTimeline)) {
        chronologicalDatesTimeline.add(dateTimeline);
      }

      List<TripBookingDetailView> datebookings = chronologicalBookings.get(date);
      if (datebookings == null) {
        datebookings = new ArrayList<>();
      }

      datebookings.add(b);
      chronologicalBookings.put(date, datebookings);

      if (b.detailsTypeId == ReservationType.FLIGHT && b.endDateMs > 0 && b.startDateMs > b.endDateMs) {
        datesToReorder.add(date);
      }
    }

    //for each day - try to see if there are cross timezone flights that need to be regrouped
    for (String date: datesToReorder) {
      List<TripBookingDetailView> bookings = chronologicalBookings.get(date);
      TripBookingDetailView crossTimezoneFlight = null;
      List<TripBookingDetailView> dayFlights = new ArrayList<TripBookingDetailView>();
      boolean sortOverride = false;
      String departDate = null;
      String arriveDate = null;
      for (TripBookingDetailView d : bookings) {
        if (d.detailsTypeId == ReservationType.FLIGHT) {
          dayFlights.add(d);
          if (d.startDateMs > d.endDateMs && d.endDateMs > 0 && crossTimezoneFlight==null) {
            crossTimezoneFlight = d;
            departDate = Utils.formatDateControl(crossTimezoneFlight.startDateMs);
            arriveDate = Utils.formatDateControl(crossTimezoneFlight.endDateMs);
          }
        }
        if (d.rank > 0) {
          sortOverride =true;
        }
      }
      if (crossTimezoneFlight != null && (!sortOverride || !arriveDate.equals(departDate))) { //if it is back in time, we ignore any ranking and try to sort things again
        //rearrange all bookings...
        //any bookings before the arrival time will be before the cross zone flight
        dayFlights = reorderFlights(dayFlights);

        //we need 2 arrays - one to contain all the bookings prior to the cross zone flight sorted chronologically
        //one to contain all the bookings after the cross zone flight
        List<TripBookingDetailView> beforeCrossZone = new ArrayList<>();
        List<TripBookingDetailView> afterCrosssZone = new ArrayList<>();
        boolean processingBefore = true;

        for (TripBookingDetailView v : dayFlights) {
          if (v.detailsId.equals(crossTimezoneFlight.detailsId)) {
            processingBefore = false;
          } else {
            if (processingBefore) {
              beforeCrossZone.add(v);
            } else {
              afterCrosssZone.add(v);
            }
          }
        }

        for (TripBookingDetailView v : bookings) {
          if (v.detailsTypeId != ReservationType.FLIGHT) {
           if (v.startDateMs != 0 && v.startDateMs < crossTimezoneFlight.startDateMs) {
              if (crossTimezoneFlight.endDateMs > 0 && v.startDate.equals(arriveDate) && v.startDateMs > crossTimezoneFlight.endDateMs) { //try to figure out if these bookings happen after the cross flight lands
                if (v.locFinishName != null && crossTimezoneFlight.departureAirportCode != null && v.locFinishName.toUpperCase().contains(crossTimezoneFlight.departureAirportCode)){
                  beforeCrossZone.add(v); //handle any transfers to the airport for the cross time zone flight - pickup time will be before departure time
                } else {
                  afterCrosssZone.add(v); //definitely after
                }
              } else {
                beforeCrossZone.add(v);
              }
            } else {
              afterCrosssZone.add(v);
            }
          }
        }
        //if the flight lands 1 day before - are there any bookings from the day before that should be moved?
        if (!departDate.equals(arriveDate)) {
          List<TripBookingDetailView> prevBookings = chronologicalBookings.get(Utils.formatDatePrint(crossTimezoneFlight.endDateMs));
          if (prevBookings != null && prevBookings.size() > 0) {
            processCrossFlight(prevBookings, afterCrosssZone, null, arriveDate);

          }
          //if the flight lands 1 day before - are there any bookings from the day same day but needs to move after the flight lands that should be moved?
          List<TripBookingDetailView> sameBookings = chronologicalBookings.get(Utils.formatDatePrint(crossTimezoneFlight.startDateMs));
          if (sameBookings != null && sameBookings.size() > 0) {
            processCrossFlight(sameBookings, afterCrosssZone, beforeCrossZone, departDate);
          }
        }

        Collections.sort(beforeCrossZone, TripBookingDetailView.TripBookingDetailViewComparator );
        Collections.sort(afterCrosssZone, TripBookingDetailView.TripBookingDetailViewComparator );
        beforeCrossZone.add(crossTimezoneFlight);
        beforeCrossZone.addAll(afterCrosssZone);

        //now we sort each chronologically
        chronologicalBookings.put(date, beforeCrossZone);
      }
    }

    ///after everything is sorted, we go back to add the notes bookings to the right spot
    if (notesForLinkedBookings.size() > 0) {
      for (String key :chronologicalBookings.keySet()) {
        List<TripBookingDetailView> newVersion = new ArrayList<>();

        for (TripBookingDetailView booking: chronologicalBookings.get(key)) {
          if (notesForLinkedBookings.containsKey(booking.detailsId)) {
            //the booking has notes... so add the notes right after the booking
            newVersion.add(booking);
            for (TripBookingDetailView note: notesForLinkedBookings.get(booking.detailsId)) {
              newVersion.add(note);
            }
          } else {
            newVersion.add(booking);
          }

        }
        chronologicalBookings.put(key, newVersion);
      }
    }

    //if there are notes set to be display at the end, we soft link it to the end
    if (notesForLastItem != null && notesForLastItem.size() > 0 && chronologicalDates.size() > 0) {
      String lastKey = chronologicalDates.get(chronologicalDates.size() -1);
      //add the notes to the last list
      if (chronologicalBookings.containsKey(lastKey)) {
        chronologicalBookings.get(lastKey).addAll(notesForLastItem);
      }
    }
  }

  public void processCrossFlight(List<TripBookingDetailView> bookings, List<TripBookingDetailView> afterCrosssZone, List<TripBookingDetailView> beforeCrossZone, String arriveDate ) {
    if (bookings != null && bookings.size() > 0) {
      Iterator<TripBookingDetailView> it = bookings.iterator();
      while (it.hasNext()) {
        TripBookingDetailView v = it.next();
        String vStartDate = Utils.formatDateControl(v.startDateMs);
        if (vStartDate.equals(arriveDate) && v.afterCrossDateFlight) {
          afterCrosssZone.add(v); //explicitly marked as after
          it.remove(); //remove from its current day
          if (beforeCrossZone != null && beforeCrossZone.contains(v)) {
            beforeCrossZone.remove(v);
          }
        }
      }
    }
  }

  /* -resort bookings to introduce rental car dropoff on a separate day */
  public void updateChronologicalBookingsWithRentalDropOff () {
    if (chronologicalBookings != null && chronologicalDates != null && transfers != null) {
      ArrayList<TripBookingDetailView> rentals = new ArrayList<TripBookingDetailView>();

      for (TripBookingDetailView v : transfers) {
        if (v.detailsTypeId == ReservationType.CAR_RENTAL && v.startDate != null && v.endDate != null && !v.startDate.equals(v.endDate)) {
          rentals.add(v);
        }
      }

      boolean sortNeeded = false;

      for (TripBookingDetailView v : rentals) {
        String date = Utils.formatTimestamp(v.endDateMs,"EEE MMM dd, yyyy", lang.code());
        String dateTimeline = Utils.formatTimestamp(v.endDateMs,"dd/MM/yyyy", lang.code());
        if (chronologicalDates.contains(date)) {
          chronologicalBookings.get(date).add(v);
          //resort the chronological bookings
          Collections.sort(chronologicalBookings.get(date), new Comparator<TripBookingDetailView>() {
            @Override public int compare(TripBookingDetailView o1, TripBookingDetailView o2) {
              long date1 = o1.startDateMs;
              long date2 = o2.startDateMs;
              //figure out if we should use start or end date for comparison
              long diff = date1 - date2;
              if (diff < 0) {
                diff *= -1;
                if (diff > (24 * 60 * 60 * 1000)) {
                  //greater than a day - try again with end date
                  date1 = o1.endDateMs;
                  if (date1 > date2)
                    return 1;
                  else if (date1 < date2)
                    return -1;
                  else
                    return 0;
                }

                return  -1;
              } else if (diff > 0){
                if (diff > (24 * 60 * 60 * 1000)) {
                  //greater than a day - try again with end date
                  date2 = o2.endDateMs;
                  if (date1 > date2)
                    return 1;
                  else if (date1 < date2)
                    return -1;
                  else
                    return 0;
                }
                return 1;
              } else
                return 0;

            }
          });

        } else {
          //insert new date
          chronologicalDates.add(date);
          chronologicalDatesTimeline.add(dateTimeline);
          ArrayList<TripBookingDetailView> newRentals = new ArrayList<TripBookingDetailView>();
          newRentals.add(v);
          chronologicalBookings.put(date, newRentals);
          sortNeeded = true;
        }
      }

      if (sortNeeded) {
        Collections.sort(chronologicalDates, new Comparator<String>() {
                           @Override public int compare(String o1, String o2) {
                             long sDate = getMillis(o1);
                             long eDate = getMillis(o2);
                             if (sDate > eDate)
                               return 1;
                             else if (sDate < eDate)
                               return -1;
                             else
                               return 0;
                           }
                         }
        );
      }
    }
  }


  /* runtime function to sort flights that goes back in time by crossing date line from east to west */

  public void handleCrosTimezoneFlights() {
    flights = reorderFlights(flights);
  }

  public static List<TripBookingDetailView> reorderFlights (List<TripBookingDetailView> flights) {
    if (flights != null && flights.size() > 0) {

      List<DateTime> dates = new ArrayList<DateTime>();
      //first pass - determine if there are any flights that crossed the date line backwards
      for (TripBookingDetailView f : flights) {
        if (f.endDateMs > 0 && f.endDateMs != f.startDateMs && f.endDateMs < f.startDateMs && dates.size()==0) {
          DateTime dt = new DateTime(f.startDateMs);
          dates.add(dt);
        }
      }

      if (dates.size() > 0) {
        //there are flights that crossed the date lines
        List<TripBookingDetailView> newFlights = new ArrayList<TripBookingDetailView> ();

        // reorder the flights for that day so they are chronological

        for (DateTime dt : dates) {
          //find flights on that day to process...
          ArrayList<TripBookingDetailView> sameDay = new ArrayList<TripBookingDetailView>();
          ArrayList<TripBookingDetailView> sameDayOrdered = new ArrayList<TripBookingDetailView>();

          TripBookingDetailView crossZoneFlight = null;
          for (TripBookingDetailView f : flights) {
            if (f.startDateMs < dt.toDateMidnight().getMillis() && !newFlights.contains(f)) {
              //add all flights that happen before the cross over date
              newFlights.add(f);
            }
            else if (f.startDateMs >= dt.toDateMidnight().getMillis() && f.startDateMs < dt.plusDays(1)
                                                                                           .toDateMidnight()
                                                                                           .getMillis()) {
              //get all the flights that happen on the same day so they can be rearranged
              if (f.endDateMs < f.startDateMs && crossZoneFlight == null) {
                crossZoneFlight = f;
                sameDayOrdered.add(f);
              }
              else {
                sameDay.add(f);
              }
            }
          }
          if (crossZoneFlight != null) {
            for (TripBookingDetailView v : sameDay) {
              if (v.startDateMs > crossZoneFlight.endDateMs
                  &&((v.arrivalAirport != null && crossZoneFlight.departureAirport != null && v.arrivalAirport.equals(crossZoneFlight.departureAirport)) ||
                     (v.arrivalAirportCode != null && crossZoneFlight.departureAirportCode != null && v.arrivalAirportCode.equals(crossZoneFlight.departureAirportCode)))
                  ) {
                //flight is after the cross zone flight but comes in the same airport
                sameDayOrdered.add(sameDayOrdered.indexOf(crossZoneFlight), v);
              } else
              if (v.startDateMs > crossZoneFlight.endDateMs
                  &&((v.arrivalAirport != null && crossZoneFlight.departureAirport != null && !v.arrivalAirport.equals(crossZoneFlight.departureAirport)) &&
                     (v.arrivalAirportCode != null && crossZoneFlight.departureAirportCode != null && !v.arrivalAirportCode.equals(crossZoneFlight.departureAirportCode)))
                  ) {
                //flight is after the cross zone flight
                sameDayOrdered.add(v);
              }
              else {
                sameDayOrdered.add(sameDayOrdered.indexOf(crossZoneFlight), v);
              }
            }
            newFlights.addAll(sameDayOrdered);
          }
        }

        //add all flights after the last cross over date
        for (TripBookingDetailView f : flights) {
          if (!newFlights.contains(f)) {
            newFlights.add(f);
          }
        }


        return newFlights;
      }
    }

    return flights;
  }


  public String getTabSaveUrl(TabType tab) {
    switch (tab) {
      case FLIGHTS:
        return routes.BookingController.createFlight().url();
      case HOTELS:
        return routes.BookingController.createHotel().url();
      case CRUISES:
        return routes.BookingController.createCruise().url();
      case TRANSPORTS:
        return routes.BookingController.createTransport().url();
      case ACTIVITIES:
        return routes.BookingController.createActivity().url();
      default:
        return "";
    }
  }


  public static long getMillis (String s) {
    try {
      String[] formatters = new String[2];
      formatters[0] = "EEE MMM dd, yyyy";

      Date date = DateUtils.parseDate(s, formatters);
      return date.getTime();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return 0;

  }

  public int getNoteCount() {
    int count =0;
    if (notes != null) {
      for (TripBookingDetailView note: notes) {
        if (note.linkedTripDetailsId == null || note.linkedTripDetailsId.trim().length() == 0) {
          count++;
        }
      }
    }

    return count;


  }

  public boolean hasTripSummary() {
    if (notes != null) {
      for (TripBookingDetailView note: notes) {
        if (note.noteId == Long.parseLong(tripId) && note.status == APPConstants.STATUS_ACTIVE) {
          return true;
        }
      }
    }
    return false;
  }

  public void addMobilizedDomain(String domain, String siteName) {
    if (mobilizedDomains == null) {
      mobilizedDomains = new TreeMap<>();
    }
    mobilizedDomains.put(siteName, domain);
  }


}
