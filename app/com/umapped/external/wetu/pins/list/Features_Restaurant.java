package com.umapped.external.wetu.pins.list;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by george on 2016-06-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class Features_Restaurant {
  public int seats;
  public ArrayList<String> spokenLanguages;
  public ArrayList<String> cuisine;
  public ArrayList<String> ambiance;
  public ArrayList<String> facilities;
  public OperatingHours operatingHourse;


  public int getSeats() {
    return seats;
  }

  public void setSeats(int seats) {
    this.seats = seats;
  }

  public ArrayList<String> getSpokenLanguages() {
    return spokenLanguages;
  }

  public void setSpokenLanguages(ArrayList<String> spokenLanguages) {
    this.spokenLanguages = spokenLanguages;
  }

  public ArrayList<String> getCuisine() {
    return cuisine;
  }

  public void setCuisine(ArrayList<String> cuisine) {
    this.cuisine = cuisine;
  }

  public ArrayList<String> getAmbiance() {
    return ambiance;
  }

  public void setAmbiance(ArrayList<String> ambiance) {
    this.ambiance = ambiance;
  }

  public ArrayList<String> getFacilities() {
    return facilities;
  }

  public void setFacilities(ArrayList<String> facilities) {
    this.facilities = facilities;
  }

  public OperatingHours getOperatingHourse() {
    return operatingHourse;
  }

  public void setOperatingHourse(OperatingHours operatingHourse) {
    this.operatingHourse = operatingHourse;
  }
}
