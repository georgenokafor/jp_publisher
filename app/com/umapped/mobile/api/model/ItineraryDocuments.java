package com.umapped.mobile.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.view.AttachmentView;
import com.mapped.publisher.view.DestinationView;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wei on 2017-06-30.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ItineraryDocuments implements Serializable {
  public List<DestinationView> docs;
  public List<AttachmentView> attachments;
}
