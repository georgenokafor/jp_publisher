
package com.mapped.publisher.msg.flightstat.status;

public class Airline {
   	private String active;
   	private String fs;
   	private String iata;
   	private String icao;
   	private String name;
    private String phoneNumber;

 	public String getActive(){
		return this.active;
	}
	public void setActive(String active){
		this.active = active;
	}
 	public String getFs(){
		return this.fs;
	}
	public void setFs(String fs){
		this.fs = fs;
	}
 	public String getIata(){
		return this.iata;
	}
	public void setIata(String iata){
		this.iata = iata;
	}
 	public String getIcao(){
		return this.icao;
	}
	public void setIcao(String icao){
		this.icao = icao;
	}
 	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name = name;
	}

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }
}
