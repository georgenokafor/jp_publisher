package com.mapped.publisher.view;

import com.umapped.persistence.enums.ReservationType;

/**
 * Collection of
 * Created by surge on 2014-11-04.
 */
public class ViewUtils {

  public static String getFontAwesomeIconForPoiType(int poiType) {
    switch(poiType) {
      case 1: //Airline
        return "fa-plane";
      case 2: //Airport
        return "fa-plane";
      case 3: //Hotel
        return "fa-home";
      case 4: //Transportation
        return "fa-road";
      case 5: //Activity
        return "fa-globe";
      case 6: //Cruise
        return "fa-life-ring";
      case 7: //Port
        return "fa-university";
      default:
        return "fa-globe";
    }
  }

  public static String getClassTypeForBookingType(ReservationType detailType) {
    switch (detailType) {
      case FLIGHT:
        return "flight";
      case HOTEL:
        return "hotel";
      case FERRY:
      case CRUISE:
        return "cruise";
      case PUBLIC_TRANSPORT:
      case BUS:
        return "bus";
      case RAIL:
        return "train";
      case TRANSPORT:
      case CAR_RENTAL:
      case PRIVATE_TRANSFER:
      case TAXI:
        return "transport";
      case EVENT:
        return "goingson";
      case RESTAURANT:
        return "food";
      case ACTIVITY:
      case TOUR:
        return "activity";
      case CRUISE_STOP:
      case SHORE_EXCURSION:
        return "cruise";
    }
    return "activity";
  }

  public static String getimgNameForBookingType(ReservationType detailType) {
    switch (detailType) {
      case PUBLIC_TRANSPORT:
      case BUS:
        return "um-bt-icon-bus-64-dgrey.png";
      case RAIL:
        return "um-bt-icon-bus-64-dgrey.png";
      case PRIVATE_TRANSFER:
        return "um-bt-icon-ground-transfer-64-grey.png";
      case TRANSPORT:
      case CAR_RENTAL:
      case TAXI:
      case CAR_DROPOFF:
        return "um-bt-icon-car-64-dgrey.png";
      case EVENT:
        return "um-bt-icon-calendar-64-dgrey.png";
      case RESTAURANT:
        return "um-bt-icon-food-64-dgrey.png";
      case ACTIVITY:
        return "um-bt-icon-activity-star-64-grey.png";
      case TOUR:
        return "um-bt-icon-runner-64-dgrey.png";
      case CRUISE_STOP:
      case SHORE_EXCURSION:
      case FERRY:
        return "um-bt-icon-cruise-64-dgrey.png";
      case SKI_LESSONS:
        return "um-bt-icon-ski-lessons-64-grey.png";
      case SKI_LIFT:
        return "um-bt-icon-lift-ticket-64-grey.png";
      case SKI_RENTALS:
        return "um-bt-icon-ski-rental-64-grey.png";
    }
    return "um-bt-icon-runner-64-dgrey.png";
  }

  public static String getimgNameForBookingTypeMobile(ReservationType detailType) {
    switch (detailType) {
      case PUBLIC_TRANSPORT:
      case BUS:
        return "https://s3.amazonaws.com/static-umapped-com/public/img/u_icon_bus_mobile.png";
      case RAIL:
        return "https://s3.amazonaws.com/static-umapped-com/public/img/u_icon_bus_mobile.png";
      case PRIVATE_TRANSFER:
        return "http://static-umapped-com.s3.amazonaws.com/public/img/ground-transfer-icon-mobile.png";
      case TRANSPORT:
      case CAR_RENTAL:
      case CAR_DROPOFF:
      case TAXI:
        return "web/img/car-gray.png";
      case EVENT:
        return "https://s3.amazonaws.com/static-umapped-com/public/img/u_icon_goingson_mobile.png";
      case RESTAURANT:
        return "https://s3.amazonaws.com/static-umapped-com/public/img/u_icon_food_mobile.png";
      case ACTIVITY:
        return "https://s3.amazonaws.com/static-umapped-com/public/img/icons/um-bt-icon-activity-star-64-blue.png";
      case TOUR:
        return "web/img/tour-gray.png";
      case CRUISE_STOP:
      case SHORE_EXCURSION:
      case FERRY:
        return "https://s3.amazonaws.com/static-umapped-com/public/img/666-cruise-mobile.png";
      case SKI_LESSONS:
        return "http://static-umapped-com.s3.amazonaws.com/public/img/lessons-icon-mobile.png";
      case SKI_LIFT:
        return "http://static-umapped-com.s3.amazonaws.com/public/img/lift-tickets-icon-mobile.png";
      case SKI_RENTALS:
        return "http://static-umapped-com.s3.amazonaws.com/public/img/ski-rental-icon-mobile.png";
    }
    return "web/img/tour-gray.png";
  }

}
