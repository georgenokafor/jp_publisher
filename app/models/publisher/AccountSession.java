package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.Query;
import com.avaje.ebean.annotation.DbJsonB;
import com.fasterxml.jackson.databind.JsonNode;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created by wei on 2017-08-16.
 */
@Entity
@Table(name = "account_session")
public class AccountSession
    extends Model {
  @Id
  Long      sessionUid;

  Long      accountUid;
  String    deviceUid;
  String    appId;

  Long      createdTs;
  Long      modifiedTs;
  Long      syncTs;

  String    notificationToken;

  @DbJsonB
  JsonNode detail;

  @Version
  int version;

  public static Model.Finder<Long, AccountSession> find                 = new Finder(AccountSession.class);

  public static AccountSession findById(Long sessionUid) {
    return find.byId(sessionUid);
  }

  public static AccountSession findByKey(Long accountUid, String deviceUid, String appId) {
    return find.where().eq("accountUid", accountUid)
        .eq("deviceUid", deviceUid)
        .eq("appId", appId)
        .findUnique();
  }

  public static boolean hasSession (Long accountUid) {
    int c = find.where().eq("accountUid", accountUid)
            .findCount();
    return (c > 0);
  }

  public static List<AccountSession> findByEmail(String email) {
    Query<Account> subQuery = Ebean.createQuery(Account.class);
    subQuery.select("uid").where().eq("email", email);
    return find.where().in("accountUid", subQuery).findList();
  }

  public Long getSessionUid() {
    return sessionUid;
  }

  public void setSessionUid(Long sessionUid) {
    this.sessionUid = sessionUid;
  }

  public Long getAccountUid() {
    return accountUid;
  }

  public void setAccountUid(Long accountUid) {
    this.accountUid = accountUid;
  }

  public String getDeviceUid() {
    return deviceUid;
  }

  public void setDeviceUid(String deviceUid) {
    this.deviceUid = deviceUid;
  }

  public String getAppId() {
    return appId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }

  public Long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Long createdTs) {
    this.createdTs = createdTs;
  }

  public Long getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Long modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public Long getSyncTs() {
    return syncTs;
  }

  public void setSyncTs(Long syncTs) {
    this.syncTs = syncTs;
  }

  public String getNotificationToken() {
    return notificationToken;
  }

  public void setNotificationToken(String notificationToken) {
    this.notificationToken = notificationToken;
  }

  public JsonNode getDetail() {
    return detail;
  }

  public void setDetail(JsonNode detail) {
    this.detail = detail;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
