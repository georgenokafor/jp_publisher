package com.umapped.itinerary.analyze.adapters;

import com.mapped.publisher.parse.schemaorg.FoodEstablishmentReservation;
import com.mapped.publisher.parse.schemaorg.Reservation;
import com.umapped.itinerary.analyze.model.Location;

/**
 * Created by wei on 2017-03-22.
 */
public class FoodEstablishmentReservationReservationAdapter
    extends ActivityReservationAdapter {
  @Override protected Location getStartLocation(Reservation reservation) {
    FoodEstablishmentReservation r = (FoodEstablishmentReservation) reservation;
    return getLocationFromPlace(r.location);
  }

  @Override protected Location getEndLocation(Reservation reservation) {
    return getStartLocation(reservation);
  }
}
