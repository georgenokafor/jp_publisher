package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class PageEntry { 
 
	 public final static String PageEntryTypeId = "3200"; 
	 protected static String pkTypeId =  "3201"; 
	 private String pk; 
	 protected static String nameTypeId =  "3202"; 
	 private String name; 
	 protected static String noteTypeId =  "3203"; 
	 private String note; 
	 protected static String longitudeTypeId =  "3204"; 
	 private String longitude; 
	 protected static String latitudeTypeId =  "3205"; 
	 private String latitude; 
	 protected static String tagTypeId =  "3206"; 
	 private String tag; 
	 protected static String addressTypeId =  "3207"; 
	 private String address; 
	 protected static String startDateTypeId =  "3208"; 
	 private String startDate; 
	 protected static String endDateTypeId =  "3209"; 
	 private String endDate; 
	 protected static String mapsIdTypeId =  "3210"; 
	 private String mapsId; 
	 protected static String pageIdTypeId =  "3211"; 
	 private String pageId; 
	 protected static String modifiedByTypeId =  "3290"; 
	 private String modifiedBy; 
	 protected static String createdByTypeId =  "3291"; 
	 private String createdBy; 
	 protected static String statusTypeId =  "3292"; 
	 private long status; 
	 protected static String syncTimestampTypeId =  "3293"; 
	 private long syncTimestamp; 
	 protected static String lastUpdateTimestampTypeId =  "3294"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "3295"; 
	 private long createTimestamp; 

 	 // Contructor 
	 public PageEntry () {} 
	 public PageEntry ( String pk,  String name,  String note,  String longitude,  String latitude,  String tag,  String address,  String startDate,  String endDate,  String mapsId,  String pageId,  String modifiedBy,  String createdBy,  long status,  long syncTimestamp,  long lastUpdateTimestamp,  long createTimestamp ) { 
		 this.pk=pk; 
		 this.name=name; 
		 this.note=note; 
		 this.longitude=longitude; 
		 this.latitude=latitude; 
		 this.tag=tag; 
		 this.address=address; 
		 this.startDate=startDate; 
		 this.endDate=endDate; 
		 this.mapsId=mapsId; 
		 this.pageId=pageId; 
		 this.modifiedBy=modifiedBy; 
		 this.createdBy=createdBy; 
		 this.status=status; 
		 this.syncTimestamp=syncTimestamp; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getName () { 
	 	 return name; 
 	 } 
 
 	 public void setName(String name) { 
	 	 this.name = name; 
 	 } 
 
 	 public  String getNote () { 
	 	 return note; 
 	 } 
 
 	 public void setNote(String note) { 
	 	 this.note = note; 
 	 } 
 
 	 public  String getLongitude () { 
	 	 return longitude; 
 	 } 
 
 	 public void setLongitude(String longitude) { 
	 	 this.longitude = longitude; 
 	 } 
 
 	 public  String getLatitude () { 
	 	 return latitude; 
 	 } 
 
 	 public void setLatitude(String latitude) { 
	 	 this.latitude = latitude; 
 	 } 
 
 	 public  String getTag () { 
	 	 return tag; 
 	 } 
 
 	 public void setTag(String tag) { 
	 	 this.tag = tag; 
 	 } 
 
 	 public  String getAddress () { 
	 	 return address; 
 	 } 
 
 	 public void setAddress(String address) { 
	 	 this.address = address; 
 	 } 
 
 	 public  String getStartDate () { 
	 	 return startDate; 
 	 } 
 
 	 public void setStartDate(String startDate) { 
	 	 this.startDate = startDate; 
 	 } 
 
 	 public  String getEndDate () { 
	 	 return endDate; 
 	 } 
 
 	 public void setEndDate(String endDate) { 
	 	 this.endDate = endDate; 
 	 } 
 
 	 public  String getMapsId () { 
	 	 return mapsId; 
 	 } 
 
 	 public void setMapsId(String mapsId) { 
	 	 this.mapsId = mapsId; 
 	 } 
 
 	 public  String getPageId () { 
	 	 return pageId; 
 	 } 
 
 	 public void setPageId(String pageId) { 
	 	 this.pageId = pageId; 
 	 } 
 
 	 public  String getModifiedBy () { 
	 	 return modifiedBy; 
 	 } 
 
 	 public void setModifiedBy(String modifiedBy) { 
	 	 this.modifiedBy = modifiedBy; 
 	 } 
 
 	 public  String getCreatedBy () { 
	 	 return createdBy; 
 	 } 
 
 	 public void setCreatedBy(String createdBy) { 
	 	 this.createdBy = createdBy; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public Page getPage () throws Exception{ 
		 return PageMgr.getByPk(pageId); 
	}
	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: PageEntry"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 Name: ").append(this.name );  
		 sb.append("	 Note: ").append(this.note );  
		 sb.append("	 Longitude: ").append(this.longitude );  
		 sb.append("	 Latitude: ").append(this.latitude );  
		 sb.append("	 Tag: ").append(this.tag );  
		 sb.append("	 Address: ").append(this.address );  
		 sb.append("	 StartDate: ").append(this.startDate );  
		 sb.append("	 EndDate: ").append(this.endDate );  
		 sb.append("	 MapsId: ").append(this.mapsId );  
		 sb.append("	 PageId: ").append(this.pageId );  
		 sb.append("	 ModifiedBy: ").append(this.modifiedBy );  
		 sb.append("	 CreatedBy: ").append(this.createdBy );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", PageEntryTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (name != null) {
			 memberNode.put(nameTypeId, name);
		 }
		 if (note != null) {
			 memberNode.put(noteTypeId, note);
		 }
		 if (longitude != null) {
			 memberNode.put(longitudeTypeId, longitude);
		 }
		 if (latitude != null) {
			 memberNode.put(latitudeTypeId, latitude);
		 }
		 if (tag != null) {
			 memberNode.put(tagTypeId, tag);
		 }
		 if (address != null) {
			 memberNode.put(addressTypeId, address);
		 }
		 if (startDate != null) {
			 memberNode.put(startDateTypeId, startDate);
		 }
		 if (endDate != null) {
			 memberNode.put(endDateTypeId, endDate);
		 }
		 if (mapsId != null) {
			 memberNode.put(mapsIdTypeId, mapsId);
		 }
		 if (pageId != null) {
			 memberNode.put(pageIdTypeId, pageId);
		 }
		 if (modifiedBy != null) {
			 memberNode.put(modifiedByTypeId, modifiedBy);
		 }
		 if (createdBy != null) {
			 memberNode.put(createdByTypeId, createdBy);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 return classNode;
	 } 
	 public static PageEntry parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 PageEntry  c= new PageEntry(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.name = fields.findPath(nameTypeId).textValue();
			 c.note = fields.findPath(noteTypeId).textValue();
			 c.longitude = fields.findPath(longitudeTypeId).textValue();
			 c.latitude = fields.findPath(latitudeTypeId).textValue();
			 c.tag = fields.findPath(tagTypeId).textValue();
			 c.address = fields.findPath(addressTypeId).textValue();
			 c.startDate = fields.findPath(startDateTypeId).textValue();
			 c.endDate = fields.findPath(endDateTypeId).textValue();
			 c.mapsId = fields.findPath(mapsIdTypeId).textValue();
			 c.pageId = fields.findPath(pageIdTypeId).textValue();
			 c.modifiedBy = fields.findPath(modifiedByTypeId).textValue();
			 c.createdBy = fields.findPath(createdByTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 return c;
		} else
			 return null;
	 } 
} 
 
