package com.mapped.publisher.msg.flightstat.status;


import java.util.List;

/**
 * Created by twong on 2015-02-10.
 */
public class FlightStatusMsg {
  private Request request;
  private Error error;
  private Appendix appendix;
  private List <FlightStatus> flightStatuses;


  public Request getRequest() {
    return request;
  }

  public void setRequest(Request request) {
    this.request = request;
  }

  public Error getError() {
    return error;
  }

  public void setError(Error error) {
    this.error = error;
  }

  public Appendix getAppendix() {
    return appendix;
  }

  public void setAppendix(Appendix appendix) {
    this.appendix = appendix;
  }

  public List<FlightStatus> getFlightStatuses() {
    return flightStatuses;
  }

  public void setFlightStatuses(List<FlightStatus> flightStatuses) {
    this.flightStatuses = flightStatuses;
  }
}
