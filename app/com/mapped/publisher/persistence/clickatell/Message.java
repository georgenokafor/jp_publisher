package com.mapped.publisher.persistence.clickatell;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

/**
 * Message object to send SMS message
 *
 * https://www.clickatell.com/developers/api-docs/send-message-rest/
 *
 * A full advanced list of parameters:
 * https://www.clickatell.com/developers/api-docs/rest-overview-of-api-features/
 *
 * Note: not all fields from above list are in this class
 *
 * Created by surge on 2016-02-09.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Message {

  /**
   * Feature: Client Message ID
   *
   * Client message ID defined by user for message tracking.
   * We can use Umapped Message IDs
   */
  public String clientMessageId;

  /**
   * Feature: Message Text
   * The content of your SMS message.
   */
  public String text;

  /**
   * Feature: Destination Address
   *
   * The mobile number of the handset to which the message must be delivered (MSISDN).
   * The number must be in international format with no leading zeros or + symbol.
   */
  public List<String> to;

  /**
   * Feature: Sender ID
   * The source/sender address that the message will appear to come from (also known as the “Sender ID”). These must be
   * registered within your online account and approved by us before they may be used. Two-way numbers rented from us do
   * not require approval.
   * https://www.clickatell.com/developers/api-docs/sender-id-advanced-message-send/
   */
  public String from;

  /**
   * Feature: Enable Callback
   * Enables you to receive message delivery status updates via an HTTP request made to your server.
   */
  public Integer callback;

  /**
   * Feature: Mobile Originated
   *
   * This is only applicable to clients that have subscribed to a two-way messaging service.
   * We route via pre-defined carriers to enable the ability for a reply to be received back.
  */
  public Integer mo;

  /**
   * Feature: Unicode Message
   *
   * Two digit language code. Convert your text to Unicode [UCS-2 encoding]. See http://www.Unicode.org/.
   * Default: 0
   */
  public Integer unicode;

  /**
   * Feature: Validity Period
   * The validity period in minutes relative to the time at which the SMS was received by our gateway.
   * The message will not be delivered if it is still queued on our gateway after this time period.
   * Default: 24h
   */
  public Integer validityPeriod;

  public Message() {
    to = new ArrayList<>(1);
  }

  public static Message build(){
    Message m = new Message();
    return m;
  }

  public void addRecepient(String msisdn) {
    to.add(msisdn);
  }

  public String toString() {
    StringBuilder sb = new StringBuilder("Clickatell Message");
    sb.append(System.lineSeparator());
    if(clientMessageId != null) {
      sb.append("clientMessageId: ").append(clientMessageId).append(System.lineSeparator());
    }
    if(text != null) {
      sb.append("text: ").append(text).append(System.lineSeparator());
    }
    if(to != null) {
      sb.append("to: ").append(to).append(System.lineSeparator());
    }
    if(from != null) {
      sb.append("from: ").append(from).append(System.lineSeparator());
    }
    if(callback != null) {
      sb.append("callback: ").append(callback).append(System.lineSeparator());
    }
    if(mo != null) {
      sb.append("mo: ").append(mo).append(System.lineSeparator());
    }
    if(unicode != null) {
      sb.append("unicode: ").append(unicode).append(System.lineSeparator());
    }
    if(validityPeriod != null) {
      sb.append("validityPeriod: ").append(validityPeriod).append(System.lineSeparator());
    }
    return sb.toString();
  }
}
