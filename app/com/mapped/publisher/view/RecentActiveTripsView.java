package com.mapped.publisher.view;

import com.mapped.publisher.persistence.ActiveTrips;
import com.mapped.publisher.utils.Utils;

import java.util.List;

/**
 * Created by surge on 2014-05-27.
 */
public class RecentActiveTripsView
    extends BaseView {
  public List<ActiveTrips.RecentActiveTrip> activeTrips;
  public int maxResultCount;
  public long localTimeOffsetMillis;
  public int daysAgo = 0;
  public ActiveTrips.TripSearchType searchType;
  public String tableName;


  public String getDate(ActiveTrips.RecentActiveTrip activeTrip) {
    //Need to offset time to show user when changes happened
    return Utils.getDateStringPrint(activeTrip.avgtime.getTime() + localTimeOffsetMillis);
  }
}
