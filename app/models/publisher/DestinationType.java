package models.publisher;

import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-04
 * Time: 2:40 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "destination_type")
public class DestinationType
    extends Model {
  public static Model.Finder<Integer, DestinationType> find = new Finder(DestinationType.class);
  public String comments;
  @Id
  public int destinationtypeid;
  @Constraints.Required
  public String name;
  public int    rank;
  public int    status;
  public Long   createdtimestamp;
  public Long   lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;

  public static List<DestinationType> findActive() {
    return find.where().eq("status", 0).orderBy("rank asc").findList();
  }


  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public int getDestinationtypeid() {
    return destinationtypeid;
  }

  public void setDestinationtypeid(int destinationtypeid) {
    this.destinationtypeid = destinationtypeid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }
}
