package com.umapped.service.offer;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import com.avaje.ebean.SqlUpdate;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.google.inject.Singleton;
import com.mapped.publisher.parse.schemaorg.Reservation;
import com.umapped.itinerary.analyze.model.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by wei on 2017-06-19.
 */
@Singleton
public class UMappedCityLookup {
  private static final String City_Lookup = "SELECT id as city_id, name as name, region as region, country as" +
                                            " country, extra_detail as detail, geo_location as geo_location, ST_Distance(geo_location, ST_MakePoint(:lng, :lat)) distance " + "" +
                                            " FROM um_city WHERE ST_Distance(geo_location, ST_MakePoint(:lng, :lat))" +
                                            " <= :radius * 1000 order by distance asc";

  private static final String lookup_city_country = "SELECT id as city_id, name as name, region as region, country as" +
          " country, extra_detail " + "" +
          " FROM um_city WHERE name = :city and country = :country" +
          " order by um_city asc";

  private static final String lookup_city_id = "SELECT id as city_id, name as name, region as region, country as" +
          " country, extra_detail " + "" +
          " FROM um_city WHERE id = :id";

  private static final String lookup_country = "SELECT id as city_id, name as name, region as region, country as" +
          " country, extra_detail " + "" +
          " FROM um_city WHERE country = :country" +
          " order by um_city asc";

  private static final String update_photo = "UPDATE um_city set extra_detail = cast(:detail as jsonb) where id = :id";
  private static ObjectMapper om = new ObjectMapper();

  public UMappedCityLookup() {
  }

  private List<SqlRow> getCities(Location.GeoLocation location) {
    if (location == null) {
      return null;
    }
    List<SqlRow> rows = Ebean.createSqlQuery(City_Lookup)
                             .setParameter("lng", location.getLongitude())
                             .setParameter("lat", location.getLatitude())
                             .setParameter("radius", 50)
                             .findList();

    return rows;
  }


  public UMappedCity getCity(Location.GeoLocation location) {
    List<SqlRow> rows = getCities(location);
    if (rows != null && rows.size() > 0) {
      SqlRow row = rows.get(0);
      return new UMappedCity(row.getString("city_id"),
                             row.getString("name"),
                             row.getString("region"),
                             row.getString("country"),
                             row.getString("detail"),
                             row.get("geo_location"));
    }
    else {
      return null;
    }
  }

  public UMappedCity getCityById(String id) {
    SqlRow row = Ebean.createSqlQuery(lookup_city_id)
            .setParameter("id", id).findUnique();
    if (row != null) {
      return new UMappedCity(row.getString("city_id"),
              row.getString("name"),
              row.getString("region"),
              row.getString("country"),
              row.getString("detail"),
              row.get("geo_location"));
    }
    else {
      return null;
    }
  }

  public UMappedCity getCityCountry(String city, String country) {
    List<SqlRow> rows = Ebean.createSqlQuery(lookup_city_country)
            .setParameter("city", city)
            .setParameter("country", country)
            .findList();
    if (rows != null && rows.size() > 0) {
      SqlRow row = rows.get(0);
      return new UMappedCity(row.getString("city_id"),
              row.getString("name"),
              row.getString("region"),
              row.getString("country"));
    }
    else {
      return null;
    }
  }


  public List<String> getPhotosByCityCountry(String city, String country) {
    if (city == null || country == null) {
      return null;
    }
    List<SqlRow> rows = Ebean.createSqlQuery(lookup_city_country)
            .setParameter("city", city)
            .setParameter("country", country)
            .findList();

    if (rows != null ) {
      for (SqlRow row : rows) {
        String jsonText = row.getString("extra_detail");
        if (jsonText != null && !jsonText.isEmpty()) {
          try {
            ObjectReader or = om.readerFor(ExtraDetail.class);
            JsonFactory f = new JsonFactory();
            JsonParser jp = f.createParser(jsonText);
            ExtraDetail json =  or.readValue(jp, ExtraDetail.class);
            if (json.photos != null && json.photos.size() > 0) {
              return json.photos;

            }
          } catch (Exception e) {
            e.printStackTrace();
          }

        }
      }
    }
    return null;
  }

  public String getPhotoByCityCountry(String city, String country) {
    List<String> photos = getPhotosByCityCountry(city, country);
    if (photos != null && photos.size() > 0) {
      int randomNum = ThreadLocalRandom.current().nextInt(0, photos.size());
      return photos.get(randomNum);
    }
    return null;
  }

  public int updatePhoto (ExtraDetail extraDetail, String id) throws Exception{
    if (extraDetail != null && extraDetail.photos != null && extraDetail.photos.size() > 0) {
      String json = om.writeValueAsString(extraDetail);
      SqlUpdate update = Ebean.createSqlUpdate(update_photo);
      update.setParameter("detail", json);
      update.setParameter("id", id);
      int i = Ebean.execute(update);
      return i;
    }
    return  -1;
  }

  public static class ExtraDetail {
    public List<String> photos;

    public ExtraDetail addPhoto (String s) {
      if (s != null && s.startsWith("http")) {
        if (this.photos == null) {
          this.photos = new ArrayList<>();
        }
        this.photos.add(s);
      }
      return this;
    }
  }


}
