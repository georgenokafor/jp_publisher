
package com.umapped.external.wetu.content;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "day",
    "room_basis",
    "drinks_basis",
    "notes",
    "consultant_notes",
    "included",
    "excluded",
    "routes",
    "activities"
})
public class Day {

    @JsonProperty("day")
    private int day;
    @JsonProperty("room_basis")
    private String roomBasis;
    @JsonProperty("drinks_basis")
    private String drinksBasis;
    @JsonProperty("notes")
    private String notes;
    @JsonProperty("consultant_notes")
    private String consultantNotes;
    @JsonProperty("included")
    private String included;
    @JsonProperty("excluded")
    private String excluded;
    @JsonProperty("routes")
    private List<Route> routes = null;
    @JsonProperty("activities")
    private List<Activity> activities = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("day")
    public int getDay() {
        return day;
    }

    @JsonProperty("day")
    public void setDay(int day) {
        this.day = day;
    }

    @JsonProperty("room_basis")
    public String getRoomBasis() {
        return roomBasis;
    }

    @JsonProperty("room_basis")
    public void setRoomBasis(String roomBasis) {
        this.roomBasis = roomBasis;
    }

    @JsonProperty("drinks_basis")
    public String getDrinksBasis() {
        return drinksBasis;
    }

    @JsonProperty("drinks_basis")
    public void setDrinksBasis(String drinksBasis) {
        this.drinksBasis = drinksBasis;
    }

    @JsonProperty("notes")
    public String getNotes() {
        return notes;
    }

    @JsonProperty("notes")
    public void setNotes(String notes) {
        this.notes = notes;
    }

    @JsonProperty("consultant_notes")
    public String getConsultantNotes() {
        return consultantNotes;
    }

    @JsonProperty("consultant_notes")
    public void setConsultantNotes(String consultantNotes) {
        this.consultantNotes = consultantNotes;
    }

    @JsonProperty("included")
    public String getIncluded() {
        return included;
    }

    @JsonProperty("included")
    public void setIncluded(String included) {
        this.included = included;
    }

    @JsonProperty("excluded")
    public String getExcluded() {
        return excluded;
    }

    @JsonProperty("excluded")
    public void setExcluded(String excluded) {
        this.excluded = excluded;
    }

    @JsonProperty("routes")
    public List<Route> getRoutes() {
        return routes;
    }

    @JsonProperty("routes")
    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    @JsonProperty("activities")
    public List<Activity> getActivities() {
        return activities;
    }

    @JsonProperty("activities")
    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("day", day).append("roomBasis", roomBasis).append("drinksBasis", drinksBasis).append("notes", notes).append("consultantNotes", consultantNotes).append("included", included).append("excluded", excluded).append("routes", routes).append("activities", activities).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(consultantNotes).append(excluded).append(routes).append(additionalProperties).append(drinksBasis).append(roomBasis).append(activities).append(day).append(included).append(notes).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Day) == false) {
            return false;
        }
        Day rhs = ((Day) other);
        return new EqualsBuilder().append(consultantNotes, rhs.consultantNotes).append(excluded, rhs.excluded).append(routes, rhs.routes).append(additionalProperties, rhs.additionalProperties).append(drinksBasis, rhs.drinksBasis).append(roomBasis, rhs.roomBasis).append(activities, rhs.activities).append(day, rhs.day).append(included, rhs.included).append(notes, rhs.notes).isEquals();
    }

}
