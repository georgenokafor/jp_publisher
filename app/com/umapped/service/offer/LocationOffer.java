package com.umapped.service.offer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.recommendation.RecommendedItemType;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;

import java.io.Serializable;
import java.util.*;

/**
 * Created by wei on 2017-06-09.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LocationOffer implements Serializable {
  private UMappedCity location;
  private Map<RecommendedItemType, List<DecisionResult>> offerMap = new HashMap<>();
  private List<StayPeriod> stayPeriods = new ArrayList<>();

  public LocationOffer(UMappedCity location) {
    this.location = location;
  }

  public void add(DecisionResult result) {
    RecommendedItemType type = result.getType();
    List<DecisionResult> results = offerMap.get(type);
    if (results == null) {
      results = new ArrayList<>();
      offerMap.put(type, results);

    }
    if (result.getStartDate() != null) {
      stayPeriods.add(new StayPeriod(result.getStartDate(), result.getEndDate()));
      stayPeriods.sort(Comparator.comparing(StayPeriod::getStart));
    }
    results.add(result);
  }

  public UMappedCity getLocation() {
    return location;
  }

  public List<StayPeriod> getStayPeriods() {
    return stayPeriods;
  }

  public Map<RecommendedItemType, List<DecisionResult>> getOfferMap() {
    return offerMap;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append("[");
    builder.append(location.getName());
    builder.append("] : ");
    for (RecommendedItemType type : offerMap.keySet()) {
      builder.append(type);
      builder.append(",");
    }
    return builder.toString();
  }

  public void addStayPeriod(StayPeriod stayPeriod) {
    stayPeriods.add(stayPeriod);
  }
}
