/****************
@author  $Author:   mminhas  $, Toronto Stock Exchange. 
@version $Id: Log4J.java,v 1.3 2001/10/30 15:02:52 fhirsh Exp $
<PRE>
MAINTENANCE LOG
===============
$Log:   Y:/New Archives/TSE Applications/Web Systems/Web Framework/source/com/tse/common/logging/Log4J.v_va  $
   
      Rev 1.1   Jul 02 2002 16:53:22   mminhas
   changed categories to allow second log file for errors only
   
      Rev 1.0   Mar 09 2002 10:18:10   mminhas
   Initial revision.
   
      Rev 1.1   Jan 17 2002 17:43:24   mminhas
   Second revision
Revision 1.3  2001/10/30 15:02:52  fhirsh
Adding CVS Headers to all files from Perl script

</PRE>
**/

package com.mapped.publisher.utils;

import java.util.*;

import org.apache.log4j.Category;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.BasicConfigurator;


/**
 *
 * @author  mminhas
 * @version
 */
public class Log4J implements LogInterface {

  /** Log4J Category */    
  //private Category cat;
  private Category debugCategory;
  private Category errorCategory;
  
  private String className;

  /** log level */
  private int logLevel;

  /**
   * Create a Log4J logger.
   *
   * @param logSettings Log settings as a name/value pair list delimited by ';' characters
   * @param logLevel    Log level
   * @param className   The caller's class name
   */  
  public Log4J(String logSettings, int logLevel, String className) {
    //cat = Category.getInstance(className);
    this.className = className;
    this.logLevel = logLevel;
    
    Properties p = new Properties();
    StringTokenizer properties = new StringTokenizer(logSettings, ";");
    while (properties.hasMoreTokens()) {
      StringTokenizer property = new StringTokenizer(properties.nextToken(), "=");
      p.setProperty(property.nextToken(), property.nextToken());
    }
    PropertyConfigurator.configure(p);    
    
    debugCategory = Category.getInstance("debugCategory");
    errorCategory = Category.getInstance("errorCategory");
  } 

  /**
   * Log a message with severity 'debug'.
   *
   * @param className   String Class name
   * @param msg         String Log message
   */
  public void debug(String methodName, String msg) {
    if ( (logLevel & DEBUG) != 0 )
      debugCategory.debug(className + " - " + methodName + ": " + msg);
  }

  /**
   * Log a message with severity 'debug' and print the stack trace.
   *
   * @param className   String Class name
   * @param msg         String Log message
   * @param t           Throwable Exception
   */
  public void debug(String methodName, String msg, Throwable t) {
    if ( (logLevel & DEBUG) != 0 )
      debugCategory.debug(className + " - " + methodName + ": " + msg, t);
  }

  /**
   * Log a message with severity 'info'.
   *
   * @param className   String Class name
   * @param msg         String Log message
   */
  public void info(String methodName, String msg) {
    if ( (logLevel & INFO) != 0 )
      debugCategory.info(className + " - " + methodName + ": " + msg);
  }

  /**
   * Log a message with severity 'info' and print the stack trace.
   *
   * @param className   String Class name
   * @param msg         String Log message
   * @param t           Throwable Exception
   */
  public void info(String methodName, String msg, Throwable t) {
    if ( (logLevel & INFO) != 0 )
      debugCategory.info(className + " - " + methodName + ": " + msg);
  }

  /**
   * Log a message with severity 'warn'.
   *
   * @param className   String Class name
   * @param msg         String Log message
   */
  public void warn(String methodName, String msg) {
    if ( (logLevel & WARN) != 0 )
      debugCategory.warn(className + " - " + methodName + ": " + msg);
  }

  /**
   * Log a message with severity 'warn' and print the stack trace.
   *
   * @param className   String Class name
   * @param msg         String Log message
   * @param t           Throwable Exception
   */
  public void warn(String methodName, String msg, Throwable t) {
    if ( (logLevel & WARN) != 0 )
      debugCategory.warn(className + " - " + methodName + ": " + msg, t);
  }

  /**
   * Log a message with severity 'error'.
   *
   * @param className   String Class name
   * @param msg         String Log message
   */
  public void error(String methodName, String msg) {
    if ( (logLevel & ERROR) != 0 ) {
      debugCategory.error(className + " - " + methodName + ": " + msg);  
      errorCategory.error(className + " - " + methodName + ": " + msg);
    }
  }

  /**
   * Log a message with severity 'error' and print the stack trace.
   *
   * @param className   String Class name
   * @param msg         String Log message
   * @param t           Throwable Exception
   */
  public void error(String methodName, String msg, Throwable t) {
    if ( (logLevel & ERROR) != 0 ) {
      debugCategory.error(className + " - " + methodName + ": " + msg, t);    
      errorCategory.error(className + " - " + methodName + ": " + msg, t);
    }
  }

  /**
   * Log a message with severity 'fatal'.
   *
   * @param className   String Class name
   * @param msg         String Log message
   */
  public void fatal(String methodName, String msg) {
    if ( (logLevel & FATAL) != 0 ) {
      debugCategory.fatal(className + " - " + methodName + ": " + msg);  
      errorCategory.fatal(className + " - " + methodName + ": " + msg);
    }
  }

  /**
   * Log a message with severity 'fatal' and print the stack trace.
   *
   * @param className   String Class name
   * @param msg         String Log message
   * @param t           Throwable Exception
   */
  public void fatal(String methodName, String msg, Throwable t) {
    if ( (logLevel & FATAL) != 0 ) {
      debugCategory.fatal(className + " - " + methodName + ": " + msg, t);
      errorCategory.fatal(className + " - " + methodName + ": " + msg, t);
    }
  }

}
