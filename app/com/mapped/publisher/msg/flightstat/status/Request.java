package com.mapped.publisher.msg.flightstat.status;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by twong on 2015-02-10.
 */
@JsonIgnoreProperties ({"extendedOptions", "codeType"})
public class Request {

  private RequestField airline;
  private RequestField flight;
  private RequestField date;
  private RequestField airport;
  private RequestField utc;
  private RequestField codeType;
  private RequestField extendedOptions;
  private String url;

  public RequestField getAirline() {
    return airline;
  }

  public void setAirline(RequestField airline) {
    this.airline = airline;
  }

  public RequestField getFlight() {
    return flight;
  }

  public void setFlight(RequestField flight) {
    this.flight = flight;
  }

  public RequestField getDate() {
    return date;
  }

  public void setDate(RequestField date) {
    this.date = date;
  }

  public RequestField getAirport() {
    return airport;
  }

  public void setAirport(RequestField airport) {
    this.airport = airport;
  }

  public RequestField getUtc() {
    return utc;
  }

  public void setUtc(RequestField utc) {
    this.utc = utc;
  }

  public RequestField getCodeType() {
    return codeType;
  }

  public void setCodeType(RequestField codeType) {
    this.codeType = codeType;
  }

  public RequestField getExtendedOptions() {
    return extendedOptions;
  }

  public void setExtendedOptions(RequestField extendedOptions) {
    this.extendedOptions = extendedOptions;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
