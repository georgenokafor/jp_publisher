package models.publisher;

import com.avaje.ebean.*;
import com.avaje.ebean.Query;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.audit.AuditModuleType;
import com.mapped.publisher.audit.AuditRecord;
import com.mapped.publisher.audit.event.*;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;

import javax.persistence.*;
import javax.persistence.Version;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Serguei Moutovkin on 2014-05-18.
 */
@Entity
@Table(name = "trip_audit")
public class TripAudit
    extends Model {
  public static Finder<String, TripAudit> find = new Finder<>(TripAudit.class);
  @Id
  public String pk;
  public long   eventtime;

  @ManyToOne
  @JoinColumn(name = "tripid", nullable = false, referencedColumnName = "tripid")
  public Trip            trip;
  public String          cmpyid;
  public String          userid;
  @Enumerated(value = EnumType.STRING)
  public AuditModuleType module;
  @Enumerated(value = EnumType.STRING)
  public AuditActionType action;
  @Enumerated(value = EnumType.STRING)
  public AuditActorType  actor;
  @Column(name = "record")
  public String          record;
  @Transient
  public AuditRecord     auditRecord; //JSON Text
  @Version
  public int             version;

  /**
   * Find all Audit events for given trip
   *
   * @param tripModel model of the trip to find events for
   * @param numRows   number of rows of results to return
   * @return List of events
   */
  public static List<TripAudit> findAuditsForTrip(Trip tripModel, int startPos, int numRows) {
    return find
        .where()
        .eq("tripid", tripModel.tripid)
        .setFirstRow(startPos)
        .setMaxRows(numRows)
        .orderBy("eventtime desc")
        .findList();
  }

  /**
   * Factory to return a new audit record based on the type specified
   *
   * @param module modified system logical unit
   * @param action action performed on the logical unit
   * @return new AuditRecord with event field set to a new object of correct type
   */
  public static TripAudit buildRecord(AuditModuleType module, AuditActionType action, AuditActorType actor) {
    TripAudit ta = new TripAudit();
    ta.pk = DBConnectionMgr.getUniqueId();
    ta.eventtime = System.currentTimeMillis();

    ta.auditRecord = new AuditRecord();
    ta.action = action;
    ta.module = module;
    ta.actor = actor;

    switch (module) {
      case TRIP:
        ta.auditRecord.details = new AuditTrip();
        break;
      case TRIP_TRAVELLER:
        ta.auditRecord.details = new AuditTripTraveller();
        break;
      case TRIP_AGENT:
        ta.auditRecord.details = new AuditTripAgent();
        break;
      case TRIP_GROUP:
        ta.auditRecord.details = new AuditTripGroup();
        break;
      case TRIP_BOOKING_FLIGHT:
        ta.auditRecord.details = new AuditTripBookingFlight();
        break;
      case TRIP_BOOKING_HOTEL:
        ta.auditRecord.details = new AuditTripBookingHotel();
        break;
      case TRIP_BOOKING_CRUISE:
        ta.auditRecord.details = new AuditTripBookingCruise();
        break;
      case TRIP_BOOKING_TRANSPORT:
        ta.auditRecord.details = new AuditTripBookingTransport();
        break;
      case TRIP_BOOKING_ACTIVITY:
        ta.auditRecord.details = new AuditTripBookingActivity();
        break;
      case TRIP_DOC_ATTACHMENT:
        ta.auditRecord.details = new AuditTripDocAttachment();
        break;
      case TRIP_DOC_GUIDE:
        ta.auditRecord.details = new AuditTripDocGuide();
        break;
      case TRIP_DOC_CUSTOM:
        ta.auditRecord.details = new AuditTripDocCustom();
        break;
      case TRIP_COLLABORATION:
        ta.auditRecord.details = new AuditCollaboration();
        break;
      case TRIP_BOOKING_NOTE:
        ta.auditRecord.details = new AuditTripDocCustom();
        break;
      default:
        Log.log(LogLevel.ERROR, "Unhandled Audit module:" + module);
        break;
    }

    return ta;
  }

  public static List<TripAudit> getTripWithBookingsModificationEvents (String tripId, long timestamp) {
    List<AuditModuleType> modules = new ArrayList<>();
    modules.add(AuditModuleType.TRIP_BOOKING_ACTIVITY);
    modules.add(AuditModuleType.TRIP_BOOKING_CRUISE);
    modules.add(AuditModuleType.TRIP_BOOKING_FLIGHT);
    modules.add(AuditModuleType.TRIP_BOOKING_CRUISE);
    modules.add(AuditModuleType.TRIP_BOOKING_TRANSPORT);
    modules.add(AuditModuleType.TRIP_BOOKING_HOTEL);

    List<TripAudit> tripAudits = find.where().eq("tripid", tripId).gt("eventtime", timestamp).in("module", modules).findList();
    return tripAudits;

  }

  static final List<AuditModuleType> modules = new ArrayList<>();

  static {
    modules.add(AuditModuleType.TRIP_BOOKING_ACTIVITY);
    modules.add(AuditModuleType.TRIP_BOOKING_CRUISE);
    modules.add(AuditModuleType.TRIP_BOOKING_FLIGHT);
    modules.add(AuditModuleType.TRIP_BOOKING_CRUISE);
    modules.add(AuditModuleType.TRIP_BOOKING_TRANSPORT);
    modules.add(AuditModuleType.TRIP_BOOKING_HOTEL);
  }

  public static List<Trip> getTripWithBookingsModifiedSince (long timestamp) {
    Query<Trip> tripQuery = Ebean.createQuery(Trip.class);
    Query<TripAudit> tripAuditQuery =Ebean.createQuery(TripAudit.class);
    tripAuditQuery.select("trip.tripid").setDistinct(true).where().gt("eventtime", timestamp).in("module", modules);
    List<Trip> trips = tripQuery.where().in("tripid", tripAuditQuery).findList();

    return trips;

  }

  public String getRecord() {
    return record;
  }

  public void setRecord(String record) {
    this.record = record;
  }

  public AuditRecord getAuditRecord() {
    return auditRecord;
  }

  public void setAuditRecord(AuditRecord auditRecord) {
    this.auditRecord = auditRecord;
  }

  public String getPk() {
    return pk;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public long getEventtime() {
    return eventtime;
  }

  public void setEventtime(long eventtime) {
    this.eventtime = eventtime;
  }

  public Trip getTrip() {
    return trip;
  }

  public void setTrip(Trip trip) {
    this.trip = trip;
  }

  public String getCmpyid() {
    return cmpyid;
  }

  public void setCmpyid(String cmpyid) {
    this.cmpyid = cmpyid;
  }

  public String getUserid() {
    return userid;
  }

  public void setUserid(String userid) {
    this.userid = userid;
  }

  public AuditModuleType getModule() {
    return module;
  }

  public void setModule(AuditModuleType module) {
    this.module = module;
  }

  public AuditActionType getAction() {
    return action;
  }

  public void setAction(AuditActionType action) {
    this.action = action;
  }

  public AuditActorType getActor() {
    return actor;
  }

  public void setActor(AuditActorType actor) {
    this.actor = actor;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public void updateAuditRecord() {
    auditRecord.setTime(new Timestamp(eventtime));
    auditRecord.setCmpyId(getCmpyid());
    auditRecord.setTripId(getTrip().getTripid());
    auditRecord.setUserId(getUserid());
    auditRecord.setModule(getModule());
    auditRecord.setAction(getAction());
    auditRecord.setActor(getActor());
    setRecord(auditRecord.toString());
  }

  public void save() {
    updateAuditRecord();
    super.save();
  }

  public TripAudit withTrip(Trip trip) {
    this.trip = trip;
    return this;
  }

  public TripAudit withCmpyid(String cmpyid) {
    this.cmpyid = cmpyid;
    return this;
  }

  public TripAudit withUserid(String userid) {
    this.userid = userid;
    return this;
  }

  public AuditEvent getDetails() {
    if (auditRecord != null) {
      return auditRecord.details;
    }
    return null;
  }
}
