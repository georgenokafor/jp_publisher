package controllers;

/**
 * Created by surge on 2015-12-30.
 */

import com.mapped.publisher.utils.Log;
import com.mapped.publisher.view.BaseView;
import play.Configuration;
import play.Environment;
import play.api.OptionalSourceMapper;
import play.api.UsefulException;
import play.api.routing.Router;
import play.data.FormFactory;
import play.http.DefaultHttpErrorHandler;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import play.mvc.Results;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * https://www.playframework.com/documentation/2.4.x/JavaErrorHandling
 * https://www.playframework.com/documentation/2.4.x/api/java/play/http/DefaultHttpErrorHandler.html
 */
public class ErrorHandler
    extends DefaultHttpErrorHandler {

  @Inject
  FormFactory formFactory;


  @Inject
  public ErrorHandler(Configuration configuration,
                      Environment environment,
                      OptionalSourceMapper sourceMapper,
                      Provider<Router> routes) {
    super(configuration, environment, sourceMapper, routes);
  }

  @Override
  protected CompletionStage<Result> onBadRequest(RequestHeader requestHeader, String s) {
    BaseView view = new BaseView();
    return CompletableFuture.completedFuture(Results.badRequest(views.html.error.error404.render(view)));
  }

  protected CompletionStage<Result> onForbidden(RequestHeader request, String message) {
    return CompletableFuture.completedFuture(Results.forbidden("You're not allowed to access this resource."));
  }

  @Override
  protected CompletionStage<Result> onNotFound(RequestHeader requestHeader, String s) {
    BaseView view = new BaseView();
    return CompletableFuture.completedFuture(Results.notFound(views.html.error.error404.render(view)));
  }

  @Override
  public CompletionStage<Result> onServerError(RequestHeader requestHeader, Throwable throwable) {
    Log.err("HTTP500: Controller Action Unhandled Exception", throwable);
    BaseView view = new BaseView();
    return CompletableFuture.completedFuture(Results.internalServerError(views.html.error.error500.render(view)));
  }

  protected CompletionStage<Result> onProdServerError(RequestHeader request, UsefulException exception) {
    Log.err("HTTP500: Controller Action Unhandled Exception", exception);
    BaseView view = new BaseView();
    return CompletableFuture.completedFuture(Results.internalServerError(views.html.error.error500.render(view)));
  }
}
