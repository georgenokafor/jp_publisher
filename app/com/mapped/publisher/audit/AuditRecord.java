package com.mapped.publisher.audit;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.publisher.audit.event.*;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;

import java.sql.Timestamp;

/**
 * Created by Serguei Moutovkin on 2014-05-13.
 */

public class AuditRecord {
  public Timestamp time;
  public String    tripId;
  public String    cmpyId;
  public String    userId;
  /**
   * Logical software component which has been modified
   */
  public AuditModuleType module;
  /**
   * Activity performed on the component
   */
  public AuditActionType action;
  /**
   * Who is generating audit-able action
   */
  public AuditActorType  actor;


  @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY)
  @JsonSubTypes({@JsonSubTypes.Type(value = AuditTrip.class, name = "TRIP"),
                 @JsonSubTypes.Type(value = AuditTripTraveller.class, name = "TRIP_TRAVELLER"),
                 @JsonSubTypes.Type(value = AuditTripAgent.class, name = "TRIP_AGENT"),
                 @JsonSubTypes.Type(value = AuditTripGroup.class, name = "TRIP_GROUP"),
                 @JsonSubTypes.Type(value = AuditTripBookingFlight.class, name = "TRIP_BOOKING_FLIGHT"),
                 @JsonSubTypes.Type(value = AuditTripBookingHotel.class, name = "TRIP_BOOKING_HOTEL"),
                 @JsonSubTypes.Type(value = AuditTripBookingCruise.class, name = "TRIP_BOOKING_CRUISE"),
                 @JsonSubTypes.Type(value = AuditTripBookingTransport.class, name = "TRIP_BOOKING_TRANSPORT"),
                 @JsonSubTypes.Type(value = AuditTripBookingActivity.class, name = "TRIP_BOOKING_ACTIVITY"),
                 @JsonSubTypes.Type(value = AuditTripDocAttachment.class, name = "TRIP_DOC_ATTACHMENT"),
                 @JsonSubTypes.Type(value = AuditTripDocGuide.class, name = "TRIP_DOC_GUIDE"),
                 @JsonSubTypes.Type(value = AuditTripDocCustom.class, name = "TRIP_DOC_CUSTOM"),
                 @JsonSubTypes.Type(value = AuditCollaboration.class, name = "TRIP_COLLABORATION")})
  public AuditEvent details;

  public static AuditRecord fromJson(String json) {
    ObjectMapper om = new ObjectMapper();
    AuditRecord  ar = null;
    try {
      ar = om.readValue(json, AuditRecord.class);
    }
    catch (Exception je) {
      Log.log(LogLevel.ERROR, "Failed to create Audit object from JSON:" + je.getMessage());
    }

    return ar;
  }

  public Timestamp getTime() {
    return time;
  }

  public void setTime(Timestamp time) {
    this.time = time;
  }

  public String getTripId() {
    return tripId;
  }

  public void setTripId(String tripId) {
    this.tripId = tripId;
  }

  public String getCmpyId() {
    return cmpyId;
  }

  public void setCmpyId(String cmpyId) {
    this.cmpyId = cmpyId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public AuditModuleType getModule() {
    return module;
  }

  public void setModule(AuditModuleType module) {
    this.module = module;
  }

  public AuditActionType getAction() {
    return action;
  }

  public void setAction(AuditActionType action) {
    this.action = action;
  }

  public AuditActorType getActor() {
    return actor;
  }

  public void setActor(AuditActorType actor) {
    this.actor = actor;
  }

  public AuditEvent getDetails() {
    return details;
  }

  public void setDetails(AuditEvent details) {
    this.details = details;
  }

  public String toString() {
    ObjectMapper m      = new ObjectMapper();
    String       result = null;
    try {
      //result = m.writerWithDefaultPrettyPrinter().writeValueAsString(this);
      result = m.writeValueAsString(this);
    }
    catch (JsonProcessingException jpe) {
      Log.log(LogLevel.ERROR, "Failed to generate Audit JSON:" + jpe.getMessage());
    }

    return result;
  }
}
