package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created by twong on 15-10-15.
 */
@Entity @Table(name = "api_user_link")

public class ApiUserLink extends Model {
  public static Model.Finder<Long, ApiUserLink> find = new Finder(ApiUserLink.class);

  @Id
  private long pk;

  @Constraints.Required
  private String uUserId;
  private long apiCmpyTokenPk;
  private String srcUserId;
  private long createdTs;
  private long updatedTs;
  private String createdBy;
  private String modifiedBy;

  @Version
  private int version;

  public static List<ApiUserLink> findByUserId (String userId) {
    return find.where().eq("u_user_id", userId).findList();
  }
  public static List<ApiUserLink> findByUserId (String userId, String uCmpyId) {
    return find.where().eq("u_user_id", userId).eq("u_cmpy_id", uCmpyId).findList();
  }

  public static ApiUserLink findByTokenPkSrcId (long tokenPk, String srcUserId) {
    return find.where().eq("src_user_id", srcUserId).eq("api_cmpy_token_pk", tokenPk).findUnique();
  }

  public static boolean doesUserIdExist (String userId, long apiCmpyTokenPk,  String srcUserId) {
    //make sure there is not already a link for this cmpy and this src user id
    int userLinkCount = find.where().eq("src_user_id", srcUserId).eq("api_cmpy_token_pk",apiCmpyTokenPk).findCount();
    if (userLinkCount > 0) {
      return true;
    }

    /*
    int i =  find.where().eq("u_user_id", userId).eq("api_cmpy_token_pk",apiCmpyTokenPk).findCount();
    if (i >0)
      return true;
*/
    return false;

  }

  public static int deleteByTokenPk (long tokenPk) {
    String s = "delete from api_user_link where api_cmpy_token_pk = :tokenPk ";
    SqlUpdate deleteStmt = Ebean.createSqlUpdate(s);
    deleteStmt.setParameter("tokenPk", tokenPk);


    return Ebean.execute(deleteStmt);

  }


  public long getPk() {
    return pk;
  }

  public void setPk(long pk) {
    this.pk = pk;
  }

  public String getuUserId() {
    return uUserId;
  }

  public void setuUserId(String uUserId) {
    this.uUserId = uUserId;
  }

  public long getApiCmpyTokenPk() {
    return apiCmpyTokenPk;
  }

  public void setApiCmpyTokenPk(long apiCmpyTokenPk) {
    this.apiCmpyTokenPk = apiCmpyTokenPk;
  }

  public String getSrcUserId() {
    return srcUserId;
  }

  public void setSrcUserId(String srcUserId) {
    this.srcUserId = srcUserId;
  }

  public long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(long createdTs) {
    this.createdTs = createdTs;
  }

  public long getUpdatedTs() {
    return updatedTs;
  }

  public void setUpdatedTs(long updatedTs) {
    this.updatedTs = updatedTs;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
