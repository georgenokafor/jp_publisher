package controllers;

import actors.ActorsHelper;
import actors.SupervisorActor;
import actors.WetuItineraryActor;
import akka.NotUsed;
import akka.actor.Status;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import com.google.inject.Inject;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.*;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.WetuDetailsView;
import com.umapped.external.wetu.itinerary.ItineraryExtractor;
import com.umapped.external.wetu.itinerary.list.Itinerary;
import com.umapped.external.wetu.itinerary.list.ItineraryList;
import models.publisher.ApiCmpyToken;
import models.publisher.BkApiSrc;
import models.publisher.Company;
import models.publisher.Trip;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.StopWatch;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by george on 2016-06-16.
 */
public class WetuController
    extends Controller {

  private final static String[]  DATE_TIME_FORMATS = {"MM/dd/yyyy", "MM/dd/yyyy hh:mm:ss a", "MM/dd/yyyy h:mm:ss a",
                                                      "MM/dd/yyyy h:mma", "yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd " +
                                                                                                      "h:mma"};
  private final static DateUtils dateUtils         = new DateUtils();

  @Inject
  private FormFactory formFactory;

  //https://wetu.com/API/Itinerary/DIAKOANOUFR6UB6I/V7/List?results=10&start=0&type=All&own=false&search=%22East
  // %20African%22

  @With({Authenticated.class, Authorized.class})
  public Result searchWetu(String tripId) {
    SessionMgr   sessionMgr = SessionMgr.fromContext(ctx());
    ApiCmpyToken token      = ApiCmpyToken.findCmpyTokenByType(sessionMgr.getCredentials().getCmpyId(),
                                                               BkApiSrc.getId("WETU"));
    if (token == null || token.getToken() == null) {
      BaseView view = new BaseView();
      view.message = "WETU is not configured for your company - please contact Umapped Support";
      return ok(views.html.common.message.render(view));
    }

    Trip trip = Trip.find.byId(tripId);
    if (trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {
      return ok(views.html.wetu.wetuSearchModal.render(trip.tripid, Utils.formatDateControl(trip.starttimestamp)));

    }

    BaseView view = new BaseView();
    view.message = "You are not authorized to import a booking for this trip";
    return ok(views.html.common.message.render(view));
  }


  @With({Authenticated.class, Authorized.class})
  public Result searchWetuByKeywords(String keywords, String type, String sort, String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    sessionMgr.getCredentials().getCmpyId();

    ApiCmpyToken token = ApiCmpyToken.findCmpyTokenByType(sessionMgr.getCredentials().getCmpyId(),
                                                          BkApiSrc.getId("WETU"));
    if (token == null || token.getToken() == null) {
      BaseView view = new BaseView();
      view.message = "WETU is not configured for your company - please contact Umapped Support";
      return ok(views.html.common.message.render(view));
    }


    ItineraryExtractor extractor = new ItineraryExtractor();

    ItineraryList         itineraryList       = extractor.extractItineraryList(token.getToken(),
                                                                               "search=" + keywords.replaceAll(" ",
                                                                                                               "%20")
                                                                               + "&type=" + type + "&sort=" + sort);
    List<WetuDetailsView> wetuDetailsViewList = new ArrayList<>();

    if (itineraryList != null) {
      for (Itinerary i : itineraryList.getItineraries()) {
        WetuDetailsView detailsView = new WetuDetailsView();
        detailsView.type = i.getType();
        detailsView.identifier = i.getIdentifier();
        detailsView.identifierKey = i.getIdentifierKey();
        detailsView.days = i.getDays();
        detailsView.name = i.getName();
        detailsView.referenceNumber = i.getReferenceNumber();
        detailsView.clientName = i.getClientName();
        detailsView.clientEmail = i.getClientEmail();
        detailsView.startDate = i.getStartDate();
        detailsView.lastModified = i.getLastModified();
        detailsView.accessCount = i.getAccessCount();
        detailsView.isDisabled = i.isIsDisabled();
        detailsView.categories = i.getCategories();
        detailsView.bookingStatus = i.getBookingStatus();
        wetuDetailsViewList.add(detailsView);

      }
      return ok(views.html.wetu.wetuSearchResultModal.render(wetuDetailsViewList, tripId, keywords, null, false, null));

    }

    BaseView baseView = new BaseView();
    baseView.message = "Search Error - No matching Itineraries found!";
    return ok(views.html.common.message.render(baseView));


  }

  @With({Authenticated.class, Authorized.class})
  public Result getItinerary(String id, String tripId, String cmpyId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    if (cmpyId == null || cmpyId.isEmpty()) {
      cmpyId = sessionMgr.getCredentials().getCmpyId();
    }

    ApiCmpyToken token = ApiCmpyToken.findCmpyTokenByType(cmpyId, BkApiSrc.getId("WETU"));
    if (token == null || token.getToken() == null) {
      BaseView view = new BaseView();
      view.message = "WETU is not configured for this company - please contact Umapped Support";
      return ok(views.html.common.message.render(view));
    }
    Trip trip = Trip.find.byId(tripId);
    if (trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseView view = new BaseView();
      view.message = "You are not authorized to import a booking for this trip";
      return ok(views.html.common.message.render(view));
    }

    final DynamicForm form = formFactory.form().bindFromRequest();
    String date = form.get("inPublicTourStartDate");

    long startTs = trip.getStarttimestamp();
    if (date != null) {
      try {
        startTs = Utils.getMilliSecs(date);
      } catch (Exception e) {

      }
    }
    if (startTs == -1) {
      startTs = trip.getStarttimestamp();
    }

    //chunk encoding to keep the socket alive past 30 sec
    try {
      CacheMgr.set(APPConstants.CACHE_WETU_ITINERARY + id + sessionMgr.getUserId(), null);
      //call the actor
      final WetuItineraryActor.Command cmd = new WetuItineraryActor.Command(trip,
                                                                            token.getToken(),
                                                                            id,
                                                                            sessionMgr.getUserId(),
                                                                            new Timestamp(startTs));
      ActorsHelper.tell(SupervisorActor.UmappedActor.WETU_ITINERARY, cmd);

      Source<ByteString, ?> source = Source.<ByteString>actorRef(65535, OverflowStrategy.dropNew())
              .mapMaterializedValue(sourceActor -> {
                StopWatch sw = new StopWatch();
                sw.start();
                boolean timeout = true;
                while (sw.getTime() < 600000) {
                  try {
                    Thread.sleep(1000);
                    Object o = CacheMgr.get(APPConstants.CACHE_WETU_ITINERARY + id + sessionMgr.getUserId());
                    if (o != null) {
                      if (o instanceof String) {
                        String msg = (String) o;
                        sessionMgr.setParam(SessionConstants.SESSION_PARAM_MSG, msg);
                        String url  = "/trips/bookings/" + trip.tripid + "/ITINERARY";
                        Html   html = views.html.wetu.wetuRedirect.render(url);
                        sourceActor.tell(ByteString.fromString(html.toString()), null);
                      }
                      else {
                        BaseView baseView = new BaseView();
                        baseView.message = "Error importing itinerary  - please contact support@umapped.com if the problem " +
                                "persists";
                        String s = views.html.common.message.render(baseView).toString();
                        sourceActor.tell(ByteString.fromString(s), null);
                      }
                      timeout = false;
                      break;
                    }
                    else {
                      Log.debug("WETU Import - chunking");
                      sourceActor.tell(ByteString.fromString(" "), null);
                    }
                  }
                  catch (Exception e) {
                  }
                }
                if (timeout) {
                  BaseView baseView = new BaseView();
                  baseView.message = "Error importing itinerary  - please contact support@umapped.com if the problem " +
                          "persists";
                  String s = views.html.common.message.render(baseView).toString();
                  sourceActor.tell(ByteString.fromString(s), null);
                }
                sourceActor.tell(new Status.Success(NotUsed.getInstance()), null);
                return null;
              });
      // Serves this stream with 200 OK
      return ok().chunked(source);

    }
    catch (Exception e) {
      e.printStackTrace();
      BaseView baseView = new BaseView();
      baseView.message = "Error importing itinerary  - please contact support@umapped.com if the problem persists";
      return ok(views.html.common.message.render(baseView));
    }

  }


  @With({Authenticated.class, Authorized.class})
  public Result searchWetuSupplier(String tripId, String cmpyName, boolean publicItinerary) {
    SessionMgr sessionMgr   = SessionMgr.fromContext(ctx());
    //it can be an exact company name or by API token
    Company    supplierCmpy = Company.findByCmpyName(cmpyName);
    if (supplierCmpy == null) {
      ApiCmpyToken cmpyToken = ApiCmpyToken.findByToken(cmpyName);
      if (cmpyToken == null || !BkApiSrc.getName(cmpyToken.getBkApiSrcId()).equals("API")) {
        BaseView view = new BaseView();
        view.message = "WETU is not configured for this company - please contact Umapped Support";
        return ok(views.html.common.message.render(view));
      } else {
        supplierCmpy = Company.find.byId(cmpyToken.getuCmpyId());
      }
    }


    ApiCmpyToken token = ApiCmpyToken.findCmpyTokenByType(supplierCmpy.getCmpyid(), BkApiSrc.getId("WETU"));
    if (token == null || token.getToken() == null) {
      BaseView view = new BaseView();
      view.message = "WETU is not configured for this company - please contact Umapped Support";
      return ok(views.html.common.message.render(view));
    }

    Trip trip = Trip.find.byId(tripId);
    if (trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {
      return ok(views.html.wetu.wetuSupplierSearchModal.render(trip.tripid,
                                                               Utils.formatDateControl(trip.starttimestamp),
                                                               supplierCmpy.getLogourl(),
                                                               supplierCmpy.getName(),
                                                               supplierCmpy.getCmpyid(),
                                                               publicItinerary));

    }

    BaseView view = new BaseView();
    view.message = "You are not authorized to import a booking for this trip";
    return ok(views.html.common.message.render(view));
  }


  @With({Authenticated.class, Authorized.class})
  public Result searchWetuSupplierByConfirmationNumber(String cmpyId,
                                                       String keywords,
                                                       String type,
                                                       String sort,
                                                       String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());


    ApiCmpyToken token = ApiCmpyToken.findCmpyTokenByType(cmpyId, BkApiSrc.getId("WETU"));
    if (token == null || token.getToken() == null) {
      BaseView view = new BaseView();
      view.message = "WETU is not configured for this supplier - please contact Umapped Support";
      return ok(views.html.common.message.render(view));
    }


    ItineraryExtractor extractor = new ItineraryExtractor();

    ItineraryList         itineraryList       = extractor.extractItineraryList(token.getToken(),
                                                                               "search=" + keywords.replaceAll(" ",
                                                                                                               "%20")
                                                                               + "&type=" + type + "&sort=" + sort);
    List<WetuDetailsView> wetuDetailsViewList = new ArrayList<>();

    if (itineraryList != null) {
      for (Itinerary i : itineraryList.getItineraries()) {
        if (i.getReferenceNumber() != null && i.getReferenceNumber().equalsIgnoreCase(keywords)) {
          WetuDetailsView detailsView = new WetuDetailsView();
          detailsView.type = i.getType();
          detailsView.identifier = i.getIdentifier();
          detailsView.identifierKey = i.getIdentifierKey();
          detailsView.days = i.getDays();
          detailsView.name = i.getName();
          detailsView.referenceNumber = i.getReferenceNumber();
          detailsView.clientName = i.getClientName();
          detailsView.clientEmail = i.getClientEmail();
          detailsView.startDate = i.getStartDate();
          detailsView.lastModified = i.getLastModified();
          detailsView.accessCount = i.getAccessCount();
          detailsView.isDisabled = i.isIsDisabled();
          detailsView.categories = i.getCategories();
          detailsView.bookingStatus = i.getBookingStatus();
          wetuDetailsViewList.add(detailsView);
        }

      }
      return ok(views.html.wetu.wetuSearchResultModal.render(wetuDetailsViewList, tripId, keywords, cmpyId, false, null));
    }

    BaseView baseView = new BaseView();
    baseView.message = "Search Error - No matching Itineraries found!";
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result searchWetuSupplierPublicItineraries(String cmpyId,
                                                       String keywords,
                                                       String sort,
                                                       String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Trip trip = Trip.findByPK(tripId);
    String defaultDate = Utils.formatDateControlYYYY(trip.getStarttimestamp());

    ApiCmpyToken token = ApiCmpyToken.findCmpyTokenByType(cmpyId, BkApiSrc.getId("WETU"));
    if (token == null || token.getToken() == null) {
      BaseView view = new BaseView();
      view.message = "WETU is not configured for this supplier - please contact Umapped Support";
      return ok(views.html.common.message.render(view));
    }


    ItineraryExtractor extractor = new ItineraryExtractor();

    ItineraryList         itineraryList       = extractor.extractItineraryList(token.getToken(),
                                                                               "search=" + keywords.replaceAll(" ",
                                                                                                               "%20")
                                                                               + "&type=AllComponents&sort=" + sort);

    ItineraryList         itineraryList1       = extractor.extractItineraryList(token.getToken(),
            "search=" + keywords.replaceAll(" ",
                    "%20")
                    + "&type=Sample&sort=" + sort);

    List<WetuDetailsView> wetuDetailsViewList = new ArrayList<>();

    loadSearchResults(itineraryList, wetuDetailsViewList);
    loadSearchResults(itineraryList1, wetuDetailsViewList);


    if (!wetuDetailsViewList.isEmpty()) {
      return ok(views.html.wetu.wetuSearchResultModal.render(wetuDetailsViewList, tripId, keywords, cmpyId, true, defaultDate));
    }

    BaseView baseView = new BaseView();
    baseView.message = "Search Error - No matching Itineraries found!";
    return ok(views.html.common.message.render(baseView));
  }

  private void loadSearchResults(ItineraryList itineraryList, List<WetuDetailsView> wetuDetailsViewList) {
    if (itineraryList != null) {
      for (Itinerary i : itineraryList.getItineraries()) {
        WetuDetailsView detailsView = new WetuDetailsView();
        detailsView.type = i.getType();
        detailsView.identifier = i.getIdentifier();
        detailsView.identifierKey = i.getIdentifierKey();
        detailsView.days = i.getDays();
        detailsView.name = i.getName();
        detailsView.referenceNumber = i.getReferenceNumber();
        detailsView.clientName = i.getClientName();
        detailsView.clientEmail = i.getClientEmail();
        detailsView.startDate = i.getStartDate();
        detailsView.lastModified = i.getLastModified();
        detailsView.accessCount = i.getAccessCount();
        detailsView.isDisabled = i.isIsDisabled();
        detailsView.categories = i.getCategories();
        detailsView.bookingStatus = i.getBookingStatus();
        wetuDetailsViewList.add(detailsView);
      }
    }


  }
}
