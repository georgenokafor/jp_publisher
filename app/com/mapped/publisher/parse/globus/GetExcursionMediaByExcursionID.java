
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="excursionID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "excursionID"
})
@XmlRootElement(name = "GetExcursionMediaByExcursionID")
public class GetExcursionMediaByExcursionID {

    protected int excursionID;

    /**
     * Gets the value of the excursionID property.
     * 
     */
    public int getExcursionID() {
        return excursionID;
    }

    /**
     * Sets the value of the excursionID property.
     * 
     */
    public void setExcursionID(int value) {
        this.excursionID = value;
    }

}
