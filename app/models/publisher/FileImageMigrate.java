package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * Created by surge on 2016-10-28.
 */
@Entity
@Table(name = "file_image_migrate")
public class FileImageMigrate
    extends Model {

  /*
  --- TABLE DDL:
  ---
  --- This is a temporary table to put images until they can be safely deleted
  CREATE TABLE file_image_cleanup (
    pk                      INTEGER NOT NULL DEFAULT nextval('seq_file_image_cleanup_pk'),

    bucket                  CHARACTER VARYING NOT NULL,                 -- S3 bucket name
    key                     CHARACTER VARYING NOT NULL,                 -- S3 key
    public_url              CHARACTER VARYING DEFAULT NULL,             -- Public URL
    source_use              CHARACTER VARYING(15),                      -- Source where image was used
    source_pk               CHARACTER VARYING,                          -- Source PK/ID that can be referenced to
    verify correctness,
    deleted                 BOOLEAN DEFAULT FALSE,                      -- Flags when file is removed from the bucket
    created_ts              TIMESTAMP WITHOUT TIME ZONE NOT NULL,       -- When image was modified (in case image is
    re-generated)
    version                 INTEGER DEFAULT 0,
    CONSTRAINT file_image_cleanup_pk PRIMARY KEY (pk)
  );
   */

  public enum ImageUser {
    TRIP_COVER,
    TMPLT_COVER,
    DEST_COVER,
    POI_FILE,
    TRIP_NOTE_ATACH,
    DEST_GUID_ATACH,
    IMAGE_LIBRARY
  }

  public static Model.Finder<Integer, FileImageMigrate> find = new Model.Finder(FileImageMigrate.class);
  @Id
  @Column(name = "pk")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_file_image_migrate_pk")
  int pk;
  Long   filePk;
  String bucket;
  String key;
  String publicUrl;
  String filename;
  String filenameNormalized;
  @Enumerated(value = EnumType.STRING)
  ImageUser sourceUse;
  String  sourcePk;
  boolean cropped;
  boolean duplicate;
  boolean deleted;
  boolean migrated;
  Timestamp createdTs;
  @Version
  int version;

  public static FileImageMigrate build() {
    FileImageMigrate fim = new FileImageMigrate();
    fim.setCreatedTs(Timestamp.from(Instant.now()));
    fim.setDuplicate(false);
    fim.setMigrated(false);
    fim.setCropped(false);
    fim.setDeleted(false);
    return fim;
  }

  public boolean isCropped() {
    return cropped;
  }

  public FileImageMigrate setCropped(boolean cropped) {
    this.cropped = cropped;
    return this;
  }

  public String getFilenameNormalized() {
    return filenameNormalized;
  }

  public FileImageMigrate setFilenameNormalized(String filenameNormalized) {
    this.filenameNormalized = filenameNormalized;
    return this;
  }

  public boolean isMigrated() {
    return migrated;
  }

  public FileImageMigrate setMigrated(boolean migrated) {
    this.migrated = migrated;
    return this;
  }

  public String getFilename() {
    return filename;
  }

  public FileImageMigrate setFilename(String filename) {
    this.filename = filename;
    return this;
  }


  public boolean isDuplicate() {
    return duplicate;
  }

  public FileImageMigrate setDuplicate(boolean duplicate) {
    this.duplicate = duplicate;
    return this;
  }

  public Long getFilePk() {
    return filePk;
  }

  public FileImageMigrate setFilePk(Long filePk) {
    this.filePk = filePk;
    return this;
  }

  public int getPk() {
    return pk;
  }

  public FileImageMigrate setPk(int pk) {
    this.pk = pk;
    return this;
  }

  public String getBucket() {
    return bucket;
  }

  public FileImageMigrate setBucket(String bucket) {
    this.bucket = bucket;
    return this;
  }

  public String getKey() {
    return key;
  }

  public FileImageMigrate setKey(String key) {
    this.key = key;
    return this;
  }

  public String getPublicUrl() {
    return publicUrl;
  }

  public FileImageMigrate setPublicUrl(String publicUrl) {
    this.publicUrl = publicUrl;
    return this;
  }

  public ImageUser getSourceUse() {
    return sourceUse;
  }

  public FileImageMigrate setSourceUse(ImageUser sourceUse) {
    this.sourceUse = sourceUse;
    return this;
  }

  public String getSourcePk() {
    return sourcePk;
  }

  public FileImageMigrate setSourcePk(String sourcePk) {
    this.sourcePk = sourcePk;
    return this;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public FileImageMigrate setDeleted(boolean deleted) {
    this.deleted = deleted;
    return this;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public FileImageMigrate setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
    return this;
  }

  public int getVersion() {
    return version;
  }

  public FileImageMigrate setVersion(int version) {
    this.version = version;
    return this;
  }


}
