package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbJsonB;
import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.persistence.util.DBConnectionMgr;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * Created by surge on 2016-02-24.
 */
@Entity
@Table(name = "account_audit")
public class AccountAudit
    extends Model {

  @Transient
  public static final ObjectMapper om;

  /**
   * Zones of account subsystems
   */
  public enum AccountZone {
    @EnumValue("ACC")
    ACCOUNT,
    @EnumValue("AUT")
    AUTHENTICATION,
    @EnumValue("PRP")
    PROPERTIES,
    @EnumValue("LNK")
    LINK,
    @EnumValue("GRP")
    GROUP,
    @EnumValue("CLK")
    CMPY_LINK
  }

  public enum AccountEvent {
    @EnumValue("ADD")
    ADD,
    @EnumValue("MOD")
    MODIFY,
    @EnumValue("DEL")
    DELETE,
    @EnumValue("CPY")
    COPY,
    @EnumValue("MRG")
    MERGE
  }

  public static Model.Finder<Long, AccountAudit> find = new Finder(AccountAudit.class);

  static {
    om = new ObjectMapper();
    //Perform any kind of configuration for object mapper in this area
  }

  @Id
  Long pk;
  Long uid;
  @Enumerated
  AccountEvent eventType;
  @DbJsonB
  JsonNode     meta;
  Timestamp createdTs;
  Long      createdBy;
  @Version
  int version;


  public static AccountAudit build(AccountZone zone, AccountEvent event, Long createdBy ) {
    AccountAudit aa = new AccountAudit();

    aa.setPk(DBConnectionMgr.getUniqueLongId());
    aa.setCreatedTs(Timestamp.from(Instant.now()));
    aa.setCreatedBy(createdBy);
    return aa;
  }


  @PostLoad
  public void postLoad() {
    /*
    try {
      ObjectReader or = om.readerFor(PoiJson.class);
      json = or.treeToValue(data, PoiJson.class);
      //json = om.treeToValue(data, PoiJson.class);
    }
    catch (JsonProcessingException e) {
      Log.err("Poi: Failed to parse JSON tree");
    }
    */
  }

  @PrePersist
  public void prePersist() {
    /*
    json.prepareForDb();
    data = om.valueToTree(json);
    */
  }


  public Long getPk() {
    return pk;
  }

  public void setPk(Long pk) {
    this.pk = pk;
  }

  public Long getUid() {
    return uid;
  }

  public void setUid(Long uid) {
    this.uid = uid;
  }

  public AccountEvent getEventType() {
    return eventType;
  }

  public void setEventType(AccountEvent eventType) {
    this.eventType = eventType;
  }

  public JsonNode getMeta() {
    return meta;
  }

  public void setMeta(JsonNode meta) {
    this.meta = meta;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
