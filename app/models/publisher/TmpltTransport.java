package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-07-24
 * Time: 2:33 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "tmplt_transport")
public class TmpltTransport
    extends Model {
  public static Model.Finder<String, TmpltTransport> find = new Finder(TmpltTransport.class);
  public String comments;
  public String contact;
  public String pickuplocation;
  public String dropofflocation;
  @Id
  public String detailsid;
  public String name;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;

  public static TmpltTransport buildTransport(String detailsId, String userId) {
    TmpltTransport tmpltTransport = new TmpltTransport();
    tmpltTransport.setDetailsid(detailsId);
    tmpltTransport.setCreatedtimestamp(System.currentTimeMillis());
    tmpltTransport.setLastupdatedtimestamp(tmpltTransport.createdtimestamp);
    tmpltTransport.setCreatedby(userId);
    tmpltTransport.setModifiedby(userId);
    return tmpltTransport;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public String getPickuplocation() {
    return pickuplocation;
  }

  public void setPickuplocation(String pickuplocation) {
    this.pickuplocation = pickuplocation;
  }

  public String getDropofflocation() {
    return dropofflocation;
  }

  public void setDropofflocation(String dropofflocation) {
    this.dropofflocation = dropofflocation;
  }

  public String getDetailsid() {
    return detailsid;
  }

  public void setDetailsid(String detailsid) {
    this.detailsid = detailsid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
