package com.mapped.publisher.sys;

import actors.SupervisorActor;
import akka.actor.ActorRef;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by surge on 2016-01-05.
 */
@Singleton
public class ActorInit
    implements ActorLoader {


  @Inject
  public ActorInit(@Named("main") ActorRef actorSupervisor) {
    actorSupervisor.tell(SupervisorActor.Protocol.build(SupervisorActor.Protocol.Command.INIT), null);
  }
}
