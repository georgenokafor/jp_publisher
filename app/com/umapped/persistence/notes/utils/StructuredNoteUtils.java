package com.umapped.persistence.notes.utils;

/**
 * Helper Class
 *
 * @author george
 * Created on 2017-03-21.
 */
public class StructuredNoteUtils {

  /**
   * Safe cast, return null if the instance is not assignable from subtype
   *
   * @param instance
   * @param subtype
   * @return
   */
  @SuppressWarnings("unchecked")
  public static <T> T cast(Object instance, Class<T> subtype) {
    if (instance == null) {
      return null;
    }

    if (subtype.isInstance(instance)) {
      return (T) (instance);
    }
    else {
      return null;
    }
  }
}
