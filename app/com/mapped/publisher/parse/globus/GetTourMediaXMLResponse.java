
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://webapi.globusandcosmos.com/TourInformation.xsd}GetTourMediaXMLResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTourMediaXMLResult"
})
@XmlRootElement(name = "GetTourMediaXMLResponse")
public class GetTourMediaXMLResponse {

    @XmlElement(name = "GetTourMediaXMLResult", namespace = "http://webapi.globusandcosmos.com/TourInformation.xsd", required = true, nillable = true)
    protected WebTourMedia getTourMediaXMLResult;

    /**
     * Gets the value of the getTourMediaXMLResult property.
     * 
     * @return
     *     possible object is
     *     {@link WebTourMedia }
     *     
     */
    public WebTourMedia getGetTourMediaXMLResult() {
        return getTourMediaXMLResult;
    }

    /**
     * Sets the value of the getTourMediaXMLResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebTourMedia }
     *     
     */
    public void setGetTourMediaXMLResult(WebTourMedia value) {
        this.getTourMediaXMLResult = value;
    }

}
