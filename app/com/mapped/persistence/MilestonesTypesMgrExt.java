package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2012-12-10
 * Time: 9:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class MilestonesTypesMgrExt  extends MilestonesTypesMgr{

    private static final String SELECT_ALL_BY_MAP_OWNER_LASTUPDATE = "Select a.Pk, a.PlaceType, a.PlaceName, a.MapsId, a.Rank, a.ModifiedBy, a.CreatedBy, a.Status, a.SyncTimestamp, a.LastUpdateTimestamp, a.CreateTimestamp from MilestonesTypes a, Maps b where b.CreatedBy = ? and a.MapsId = b.Pk and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?);";
    private static final String SELECT_SHARED_BY_USER_LASTUPDATE = "Select a.Pk, a.PlaceType, a.PlaceName, a.MapsId, a.Rank, a.ModifiedBy, a.CreatedBy, a.Status, a.SyncTimestamp, a.LastUpdateTimestamp, a.CreateTimestamp from MilestonesTypes a, Maps b where b.Pk in (select mapsid from sharedmaps where shareuserid = ? and status = 0) and a.MapsId = b.Pk and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?);";
    private static final String SELECT_NEW_SHARED_BY_USER_LASTUPDATE = "Select a.Pk, a.PlaceType, a.PlaceName, a.MapsId, a.Rank, a.ModifiedBy, a.CreatedBy, a.Status, a.SyncTimestamp, a.LastUpdateTimestamp, a.CreateTimestamp from MilestonesTypes a, Maps b where b.Pk in (select mapsid from sharedmaps where shareuserid = ? and status = 0 and (SyncTimestamp = 0 or SyncTimestamp > ?) and createtimestamp = lastupdatetimestamp) and a.MapsId = b.Pk;";

    public static ArrayList<MilestonesTypes> getAllByMapOwnerSinceLastUpdate(String userId, long lastUpdateTimestamp) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_ALL_BY_MAP_OWNER_LASTUPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdateTimestamp);
            rs = prep.executeQuery();
            return (MilestonesTypesMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <MilestonesTypes> getSharedByUserSinceLastUpdate(String userId, long lastUpdateTimestamp) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_SHARED_BY_USER_LASTUPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdateTimestamp);
            rs = prep.executeQuery();
            return (MilestonesTypesMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <MilestonesTypes> getNewSharedByUserSinceLastUpdate(String userId, long lastUpdateTimestamp) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_NEW_SHARED_BY_USER_LASTUPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdateTimestamp);
            rs = prep.executeQuery();
            return (MilestonesTypesMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }


}
