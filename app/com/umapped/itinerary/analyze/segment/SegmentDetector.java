package com.umapped.itinerary.analyze.segment;

import com.umapped.itinerary.analyze.model.Itinerary;

import java.util.List;

/**
 * Created by wei on 2017-03-01.
 */
public interface SegmentDetector {
  public List<TripSegment> analyze(Itinerary itinerary);
}
