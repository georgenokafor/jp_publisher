
package com.mapped.publisher.parse.iceportal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VideoURLCaption complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VideoURLCaption">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ordinal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="UrlWin56" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UrlWin100" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UrlWin300" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UrlReal56" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UrlReal100" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UrlReal300" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VideoURLCaption", propOrder = {
    "ordinal",
    "language",
    "urlWin56",
    "urlWin100",
    "urlWin300",
    "urlReal56",
    "urlReal100",
    "urlReal300"
})
public class VideoURLCaption {

    protected int ordinal;
    protected int language;
    @XmlElement(name = "UrlWin56")
    protected String urlWin56;
    @XmlElement(name = "UrlWin100")
    protected String urlWin100;
    @XmlElement(name = "UrlWin300")
    protected String urlWin300;
    @XmlElement(name = "UrlReal56")
    protected String urlReal56;
    @XmlElement(name = "UrlReal100")
    protected String urlReal100;
    @XmlElement(name = "UrlReal300")
    protected String urlReal300;

    /**
     * Gets the value of the ordinal property.
     * 
     */
    public int getOrdinal() {
        return ordinal;
    }

    /**
     * Sets the value of the ordinal property.
     * 
     */
    public void setOrdinal(int value) {
        this.ordinal = value;
    }

    /**
     * Gets the value of the language property.
     * 
     */
    public int getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     */
    public void setLanguage(int value) {
        this.language = value;
    }

    /**
     * Gets the value of the urlWin56 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlWin56() {
        return urlWin56;
    }

    /**
     * Sets the value of the urlWin56 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlWin56(String value) {
        this.urlWin56 = value;
    }

    /**
     * Gets the value of the urlWin100 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlWin100() {
        return urlWin100;
    }

    /**
     * Sets the value of the urlWin100 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlWin100(String value) {
        this.urlWin100 = value;
    }

    /**
     * Gets the value of the urlWin300 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlWin300() {
        return urlWin300;
    }

    /**
     * Sets the value of the urlWin300 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlWin300(String value) {
        this.urlWin300 = value;
    }

    /**
     * Gets the value of the urlReal56 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlReal56() {
        return urlReal56;
    }

    /**
     * Sets the value of the urlReal56 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlReal56(String value) {
        this.urlReal56 = value;
    }

    /**
     * Gets the value of the urlReal100 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlReal100() {
        return urlReal100;
    }

    /**
     * Sets the value of the urlReal100 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlReal100(String value) {
        this.urlReal100 = value;
    }

    /**
     * Gets the value of the urlReal300 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlReal300() {
        return urlReal300;
    }

    /**
     * Sets the value of the urlReal300 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlReal300(String value) {
        this.urlReal300 = value;
    }

}
