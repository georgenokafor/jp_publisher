
package com.mapped.publisher.parse.iceportal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchBrochuresResult" type="{http://services.iceportal.com/service}SearchBrochureInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchBrochuresResult"
})
@XmlRootElement(name = "SearchBrochuresResponse")
public class SearchBrochuresResponse {

    @XmlElement(name = "SearchBrochuresResult")
    protected SearchBrochureInfo searchBrochuresResult;

    /**
     * Gets the value of the searchBrochuresResult property.
     * 
     * @return
     *     possible object is
     *     {@link SearchBrochureInfo }
     *     
     */
    public SearchBrochureInfo getSearchBrochuresResult() {
        return searchBrochuresResult;
    }

    /**
     * Sets the value of the searchBrochuresResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchBrochureInfo }
     *     
     */
    public void setSearchBrochuresResult(SearchBrochureInfo value) {
        this.searchBrochuresResult = value;
    }

}
