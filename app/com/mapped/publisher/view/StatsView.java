package com.mapped.publisher.view;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-20
 * Time: 11:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class StatsView
    extends BaseView {

  public Map<String, Collection<CmpyStatsView>> cmpyActivityByMonth;
  public Map<String, ActivityStatsView> summaryActivityByMonth;
  public Map<String, Collection<UserStatsView>> userActivityByMonth;
  public Map<String, MobileStatsView> summaryMobileActivityByMonth;
  public List<CmpyInfoView> expiredTrials;
  public List<CmpyInfoView> expiringTrials;
  public Map<String, UsersView> users;
  public Map<String, CmpyInfoView> cmpies;
  public List<String> dates;
}
