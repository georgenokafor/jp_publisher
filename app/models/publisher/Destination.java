package models.publisher;

import com.avaje.ebean.*;
import com.avaje.ebean.annotation.EnumValue;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.persistence.IUmappedModel;
import com.mapped.publisher.persistence.IUmappedModelWithImage;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.ImageView;
import com.umapped.persistence.reservation.activity.UmActivityReservation;

import play.data.validation.Constraints;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-01
 * Time: 11:18 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "destination")
public class Destination
    extends UmappedModelWithImage implements IUmappedModel<String, Destination> {
  public static Model.Finder<String, Destination> find                  = new Finder(Destination.class);
  public static Comparator<Destination>           DestinationComparator = (t1, t2) -> {
    if (t1.name == null) {
      return -1;
    }

    if (t2.name == null) {
      return 1;
    }
    return t1.getName().compareTo(t2.getName());
  };

  public enum AccessLevel {
    @EnumValue("PUB")
    PUBLIC,
    @EnumValue("PRV")
    PRIVATE
  }

  @Id
  public String destinationid;
  public float  loclat;
  public float  loclong;
  public String tag;
  public String intro;
  public String description;
  public String streetaddr1;
  public String city;
  public String zipcode;
  public String state;
  public String country;
  @Constraints.Required
  public String name;
  public String cmpyid;
  public int    status;
  public int    destinationtypeid;
  public Long   createdtimestamp;
  public Long   lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Enumerated(value = EnumType.STRING)
  @Column(name = "access_level")
  public AccessLevel accessLevel;

  String coverurl;
  String covername;
  @ManyToOne
  @JoinColumn(name = "cover_image_pk", nullable = true, referencedColumnName = "pk")
  FileImage coverImage;

  public Destination markModified(String userId) {
    setModifiedby(userId);
    setLastupdatedtimestamp(System.currentTimeMillis());
    return this;
  }

  /**
   * Takes this object makes it possible to re-insert as new record
   * @return
   */
  public Destination prepareForReuse(String createdBy, String modifiedBy) {
    BeanState state = Ebean.getBeanState(this);
    state.resetForInsert();
    setDestinationid(DBConnectionMgr.getUniqueId());
    setModifiedby(modifiedBy);
    setCreatedby(createdBy);
    setLastupdatedtimestamp(System.currentTimeMillis());
    setCreatedtimestamp(lastupdatedtimestamp);
    return this;
  }

  public static List<Destination> findMostRecentActiveByCmpies(List<Integer> types,
                                                               List<String> cmpyId,
                                                               int startPos,
                                                               int maxRows) {
    return find.where()
               .in("cmpyid", cmpyId)
               .in("destinationtypeid", types)
               .eq("status", 0)
               .orderBy("destinationTypeId,name asc")
               .setFirstRow(startPos)
               .setMaxRows(maxRows)
               .findList();
  }

  public static List<Destination> findMostRecentActiveForCmpy(List<Integer> types,
                                                               String cmpyId,
                                                               int startPos,
                                                               int maxRows, String orderBy) {
    return find.where()
               .eq("cmpyid", cmpyId)
               .in("destinationtypeid", types)
               .eq("status", 0)
               .orderBy(orderBy)
               .setFirstRow(startPos)
               .setMaxRows(maxRows)
               .findList();
  }

  public static List<Destination> findActiveDestByTypeCityCmpy(String cmpyId,
                                                               int destType,
                                                               List<String> cities,
                                                               List<String> countries) {
    if (cities != null && cities.size() > 0 && countries != null && countries.size() > 0) {
      return find.where()
                 .eq("cmpyid", cmpyId)
                 .eq("status", 0)
                 .eq("destinationtypeid", destType)
                 .in("city", cities)
                 .in("country", countries)
                 .orderBy("name asc")
                 .findList();
    }
    else if (cities != null && cities.size() > 0) {
      return find.where()
                 .eq("cmpyid", cmpyId)
                 .eq("status", 0)
                 .eq("destinationtypeid", destType)
                 .in("city", cities)
                 .orderBy("name asc")
                 .findList();
    }
    else if (countries != null && countries.size() > 0) {
      return find.where()
                 .eq("cmpyid", cmpyId)
                 .eq("status", 0)
                 .eq("destinationtypeid", destType)
                 .in("country", countries)
                 .orderBy("name asc")
                 .findList();
    }

    return find.where()
               .eq("cmpyid", cmpyId)
               .eq("status", 0)
               .eq("destinationtypeid", destType)
               .orderBy("name asc")
               .findList();

  }

  public static List<Destination> findActiveDestByTypeCityCmpy(String cmpyId,
                                                               int destType,
                                                               List<String> cities,
                                                               List<String> countries,
                                                               String keyword) {
    if (keyword != null) {
      keyword = keyword.replace(" ", "%");
      keyword = "%" + keyword + "%";
    }
    else {
      keyword = "%";
    }
    String       sql      = "(name ilike ? or intro ilike ? or tag ilike ?)";
    List<String> keywords = new ArrayList<String>();
    keywords.add(keyword);
    keywords.add(keyword);
    keywords.add(keyword);

    if (cities != null && cities.size() > 0 && countries != null && countries.size() > 0) {
      return find.where()
                 .eq("cmpyid", cmpyId)
                 .eq("status", 0)
                 .eq("destinationtypeid", destType)
                 .raw(sql, keywords.toArray())
                 .in("city", cities)
                 .in("country", countries)
                 .orderBy("name asc")
                 .findList();
    }
    else if (cities != null && cities.size() > 0) {
      return find.where()
                 .eq("cmpyid", cmpyId)
                 .eq("status", 0)
                 .eq("destinationtypeid", destType)
                 .raw(sql, keywords.toArray())
                 .in("city", cities)
                 .orderBy("name asc")
                 .findList();
    }
    else if (countries != null && countries.size() > 0) {
      return find.where()
                 .eq("cmpyid", cmpyId)
                 .eq("status", 0)
                 .eq("destinationtypeid", destType)
                 .raw(sql, keywords.toArray())
                 .in("country", countries)
                 .orderBy("name asc")
                 .findList();
    }

    return find.where()
               .eq("cmpyid", cmpyId)
               .eq("status", 0)
               .eq("destinationtypeid", destType)
               .raw(sql, keywords.toArray())
               .orderBy("name asc")
               .findList();

  }

  public static List<Destination> findActiveDestByTypeCmpies(List<String> cmpyIds, String keyword) {
    if (keyword != null) {
      keyword = keyword.replace(" ", "%");
      keyword = "%" + keyword + "%";
    }
    else {
      keyword = "%";
    }
    String       sql      = "(name ilike ? or tag ilike ?)";
    List<String> keywords = new ArrayList<String>();
    keywords.add(keyword);
    keywords.add(keyword);


    return find.where()
               .in("cmpyid", cmpyIds)
               .eq("status", 0)
               .lt("destinationtypeid", 6)
               .raw(sql, keywords.toArray())
               .orderBy("name asc")
               .findList();

  }

  public static List<Destination> findActiveDestByTypeCityCmpy(String cmpyId,
                                                               int destType,
                                                               String city,
                                                               String country) {
    if (city != null && country != null) {
      return find.where()
                 .eq("cmpyid", cmpyId)
                 .eq("status", 0)
                 .eq("destinationtypeid", destType)
                 .ilike("city", "%" + city + "%")
                 .ilike("country", "%" + country + "%")
                 .orderBy("name asc")
                 .findList();
    }
    else if (city != null) {
      return find.where()
                 .eq("cmpyid", cmpyId)
                 .eq("status", 0)
                 .eq("destinationtypeid", destType)
                 .ilike("city", "%" + city + "%")
                 .orderBy("name asc")
                 .findList();
    }
    else if (country != null) {
      return find.where()
                 .eq("cmpyid", cmpyId)
                 .eq("status", 0)
                 .eq("destinationtypeid", destType)
                 .ilike("country", "%" + country + "%")
                 .orderBy("name asc")
                 .findList();
    }

    return find.where()
               .eq("cmpyid", cmpyId)
               .eq("status", 0)
               .eq("destinationtypeid", destType)
               .orderBy("name asc")
               .findList();

  }


  public static List<Destination> findActiveDestByKeywordForCmpy(String cmpyId,
                                                                 List<Integer> types,
                                                                 String keyword,
                                                                 int startPos,
                                                                 int maxRows,
                                                                 boolean titleOnly, String orderBy) {
    if (keyword != null) {
      keyword = keyword.replace(" ", "%");
      keyword = "%" + keyword + "%";
    }
    else {
      keyword = "%";
    }
    String       sql      = null;
    List<String> keywords = new ArrayList<String>();

    if (titleOnly) {
      sql = "(name ilike ? or tag ilike ?)";
      keywords.add(keyword);
      keywords.add(keyword);
    }
    else {
      sql = "(name ilike ? or intro ilike ? or tag ilike ?)";
      keywords.add(keyword);
      keywords.add(keyword);
      keywords.add(keyword);
    }


    return find.where()
               .in("destinationtypeid", types)
               .eq("cmpyid", cmpyId)
               .eq("status", 0)
               .raw(sql, keywords.toArray())
               .setFirstRow(startPos)
               .setMaxRows(maxRows)
               .orderBy(orderBy)
               .findList();

  }

  public static int countActiveDestByKeywordForCmpy(String cmpyId,
                                                     List<Integer> types,
                                                     String keyword,
                                                     boolean titleOnly) {
    if (keyword != null) {
      keyword = keyword.replace(" ", "%");
      keyword = "%" + keyword + "%";
    }
    else {
      keyword = "%";
    }
    String       sql      = null;
    List<String> keywords = new ArrayList<String>();

    if (titleOnly) {
      sql = "(name ilike ? or tag ilike ?)";
      keywords.add(keyword);
      keywords.add(keyword);
    }
    else {
      sql = "(name ilike ? or intro ilike ? or tag ilike ?)";
      keywords.add(keyword);
      keywords.add(keyword);
      keywords.add(keyword);
    }


    return find.where()
        .in("destinationtypeid", types)
        .eq("cmpyid", cmpyId)
        .eq("status", 0)
        .raw(sql, keywords.toArray())
        .orderBy("name asc")
        .findCount();

  }

  public static List<Destination> findActiveDestByKeywordForUser(String cmpyId,
                                                                 List<Integer> types,
                                                                 String keyword,
                                                                 int startPos,
                                                                 int maxRows,
                                                                 boolean titleOnly, String orderBy, String userId) {
    if (keyword != null) {
      keyword = keyword.replace(" ", "%");
      keyword = "%" + keyword + "%";
    }
    else {
      keyword = "%";
    }
    String       sql      = null;
    List<String> keywords = new ArrayList<String>();

    if (titleOnly) {
      sql = "(name ilike ? or tag ilike ?)";
      keywords.add(keyword);
      keywords.add(keyword);
    }
    else {
      sql = "(name ilike ? or intro ilike ? or tag ilike ?)";
      keywords.add(keyword);
      keywords.add(keyword);
      keywords.add(keyword);
    }


    return find.where()
        .in("destinationtypeid", types)
        .eq("cmpyid", cmpyId)
        .eq("status", 0)
        .or(Expr.isNull("access_level"), Expr.or(Expr.eq("access_level", AccessLevel.PUBLIC), Expr.and(Expr.eq("access_level", AccessLevel.PRIVATE), Expr.eq("createdby", userId))))
        .raw(sql, keywords.toArray())
        .setFirstRow(startPos)
        .setMaxRows(maxRows)
        .orderBy(orderBy)
        .findList();

  }

  public static int countActiveDestByKeywordForUser(String cmpyId,
                                                    List<Integer> types,
                                                    String keyword,
                                                    boolean titleOnly, String userId) {
    if (keyword != null) {
      keyword = keyword.replace(" ", "%");
      keyword = "%" + keyword + "%";
    }
    else {
      keyword = "%";
    }
    String       sql      = null;
    List<String> keywords = new ArrayList<String>();

    if (titleOnly) {
      sql = "(name ilike ? or tag ilike ?)";
      keywords.add(keyword);
      keywords.add(keyword);
    }
    else {
      sql = "(name ilike ? or intro ilike ? or tag ilike ?)";
      keywords.add(keyword);
      keywords.add(keyword);
      keywords.add(keyword);
    }


    return find.where()
        .in("destinationtypeid", types)
        .eq("cmpyid", cmpyId)
        .eq("status", 0)
        .or(Expr.isNull("access_level"), Expr.or(Expr.eq("access_level", AccessLevel.PUBLIC), Expr.and(Expr.eq("access_level", AccessLevel.PRIVATE), Expr.eq("createdby", userId))))
        .raw(sql, keywords.toArray())
        .orderBy("name asc")
        .findCount();

  }

  public static List<Destination> findActiveDestByKeyword(List<String> cmpyIds,
                                                          List<Integer> types,
                                                          String keyword,
                                                          int startPos,
                                                          int maxRows,
                                                          boolean titleOnly,
                                                          String orderBy) {
    if (keyword != null) {
      keyword = keyword.replace(" ", "%");
      keyword = "%" + keyword + "%";
    }
    else {
      keyword = "%";
    }
    String       sql      = null;
    List<String> keywords = new ArrayList<String>();

    if (titleOnly) {
      sql = "(name ilike ? or tag ilike ?)";
      keywords.add(keyword);
      keywords.add(keyword);
    }
    else {
      sql = "(name ilike ? or intro ilike ? or tag ilike ?)";
      keywords.add(keyword);
      keywords.add(keyword);
      keywords.add(keyword);
    }


    return find.where()
               .in("destinationtypeid", types)
               .in("cmpyid", cmpyIds)
               .eq("status", 0)
               .raw(sql, keywords.toArray())
               .setFirstRow(startPos)
               .setMaxRows(maxRows)
               .orderBy(orderBy)
               .findList();

  }

  public static int countActiveDestByKeyword(List<String> cmpyIds,
                                             List<Integer> types,
                                             String keyword,
                                             boolean titleOnly) {
    if (keyword != null) {
      keyword = keyword.replace(" ", "%");
      keyword = "%" + keyword + "%";
    }
    else {
      keyword = "%";
    }
    String       sql      = null;
    List<String> keywords = new ArrayList<String>();

    if (titleOnly) {
      sql = "(name ilike ? or tag ilike ?)";
      keywords.add(keyword);
      keywords.add(keyword);
    }
    else {
      sql = "(name ilike ? or intro ilike ? or tag ilike ?)";
      keywords.add(keyword);
      keywords.add(keyword);
      keywords.add(keyword);
    }


    return find.where()
        .in("destinationtypeid", types)
        .in("cmpyid", cmpyIds)
        .eq("status", 0)
        .raw(sql, keywords.toArray())
        .orderBy("name asc")
        .findCount();

  }

  public static List<Destination> findActiveDestAllTypesByKeyword(List<String> cmpyIds, String keyword) {
    if (keyword != null) {
      keyword = keyword.replace(" ", "%");
      keyword = "%" + keyword + "%";
    }
    else {
      keyword = "%";
    }
    String       sql      = "(name ilike ? or intro ilike ? or tag ilike ?)";
    List<String> keywords = new ArrayList<String>();
    keywords.add(keyword);
    keywords.add(keyword);
    keywords.add(keyword);

    return find.where()
               .in("cmpyid", cmpyIds)
               .eq("status", 0)
               .raw(sql, keywords.toArray())
               .orderBy("name asc")
               .findList();

  }

  public static List<Destination> findActiveDestByKeyword(List<Integer> types, String keyword) {
    if (keyword != null) {
      keyword = keyword.replace(" ", "%");
      keyword = "%" + keyword + "%";
    }
    else {
      keyword = "%";
    }
    String       sql      = "(name ilike ? or intro ilike ? or tag ilike ?)";
    List<String> keywords = new ArrayList<String>();
    keywords.add(keyword);
    keywords.add(keyword);
    keywords.add(keyword);
    return find.where()
               .in("destinationtypeid", types)
               .eq("status", 0)
               .raw(sql, keywords.toArray())
               .orderBy("name asc")
               .findList();

  }

  public static List<Destination> findActiveDestByIds(List<String> ids) {
    return find.where().in("destinationid", ids).eq("status", 0).orderBy("name asc").findList();

  }

  public static int mergeCmpy(String srcCmpyId, String targetCmpyId, String userId) {
    String s = "UPDATE destination set cmpyid = :targetCmpyId, lastupdatedtimestamp = :timestamp, " + "modifiedby = "
               + ":userid  where cmpyid = :srcCmpyId and status = 0";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("timestamp", System.currentTimeMillis());
    update.setParameter("userid", userId);
    update.setParameter("srcCmpyId", srcCmpyId);
    update.setParameter("targetCmpyId", targetCmpyId);

    return Ebean.execute(update);
  }

  public static List<Destination> findActiveByNameCmpy(String name, String cmpyid) {
    return find.where().ieq("name", name).eq("cmpyid", cmpyid).eq("status", 0).findList();
  }

  /**
   * Combines all documents linked to the trip and also documents linked via activities
   *
   * @param tripId trip id
   * @return
   */
  public static List<Destination> getAllActivelyLinkedForTrip(String tripId) {
    com.avaje.ebean.Query<TripDetail> details = Ebean.createQuery(TripDetail.class)
                                                     .select("reservationData")
                                                     .where()
                                                     .eq("tripid", tripId)
                                                     .eq("status",
                                                         APPConstants.STATUS_ACTIVE) //We don't need dead souls here
                                                     .jsonExists("reservationData", "activity.umDocumentId")
                                                     .query();
    
    List<TripDetail> detailList = details.findList();
    List<String> destinationIds = detailList.stream().
        map( td -> { 
              UmActivityReservation r = cast(td.getReservation(), UmActivityReservation.class);
              // r must exist as it checked the jsonExists
              return r.getActivity().umDocumentId;
            }
         ).collect(Collectors.toList());


    com.avaje.ebean.Query<TripDestination> tds = Ebean.createQuery(TripDestination.class)
                                                      .select("destinationid")
                                                      .where()
                                                      .eq("tripid", tripId)
                                                      .eq("status",
                                                          APPConstants.STATUS_ACTIVE) //We don't need dead souls here
                                                      .query();


    return find.where().or(Expr.in("destinationid", destinationIds), Expr.in("destinationid", tds))
               //.eq("status", APPConstants.STATUS_ACTIVE) //This apparently is not needed so long as it is linked
               .findList();
  }

  /**
   * Helper method to get all documents linked to various activities
   *
   * @param tripId
   * @return
   */
  public static List<Destination> getGuidesForActivities(String tripId) {
    com.avaje.ebean.Query<TripDetail> details = Ebean.createQuery(TripDetail.class)
                                                    .select("reservationData")
                                                    .where()
                                                    .eq("tripid", tripId)
                                                    .eq("status",
                                                        APPConstants.STATUS_ACTIVE) //We don't need dead souls here
                                                    .jsonExists("reservationData", "activity.umDocumentId")
                                                    .query();

    List<TripDetail> detailList = details.findList();
    List<String> destinationIds = detailList.stream().map(td -> {
      UmActivityReservation r = cast(td.getReservation(), UmActivityReservation.class);
      return r.getActivity().umDocumentId;
    }).collect(Collectors.toList());


    return find.where().in("destinationid", destinationIds).eq("status", APPConstants.STATUS_ACTIVE).findList();
  }

  public static List<Destination> getTripActiveDestinations(String tripId) {

    com.avaje.ebean.Query<TripDestination> tds = Ebean.createQuery(TripDestination.class)
                                                      .select("destinationid")
                                                      .where()
                                                      .eq("tripid", tripId)
                                                      .eq("status",
                                                          APPConstants.STATUS_ACTIVE) //We don't need dead souls here
                                                      .query();

    return find.where().in("destinationid", tds).findList();
  }

  public String getStreetaddr1() {
    return streetaddr1;
  }

  public void setStreetaddr1(String streetAddr1) {
    this.streetaddr1 = streetAddr1;
  }

  public boolean hasCover() {
    return getCoverurl() != null && getCoverurl().length() > 0;
  }

  public String getCoverurl() {
    return coverurl;
  }

  public Destination setCoverurl(String coverurl) {
    this.coverurl = coverurl;
    return this;
  }

  public ImageView getCoverView() {
    if (getFileImage() != null) {
      return getFileImage().toImageView();
    }
    if (this.coverurl == null || this.coverurl.length() < 10) {
      return null;
    }
    ImageView iv = new ImageView();
    iv.url = this.coverurl;
    iv.fileName = this.covername;
    return iv;
  }

  public String getCovername() {
    return covername;
  }

  public Destination setCovername(String covername) {
    this.covername = covername;
    return this;
  }

  public FileImage getCoverImage() {
    return coverImage;
  }

  public Destination setCoverImage(FileImage coverImage) {
    this.coverImage = coverImage;
    return this;
  }

  public float getLoclat() {
    return loclat;
  }

  public void setLoclat(float loclat) {
    this.loclat = loclat;
  }

  public float getLoclong() {
    return loclong;
  }

  public void setLoclong(float loclong) {
    this.loclong = loclong;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public String getIntro() {
    return intro;
  }

  public void setIntro(String intro) {
    this.intro = Utils.sanitizeHtmlFromCKEditor(intro);
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = Utils.sanitizeHtmlFromCKEditor(description);
  }

  public String getStreetaddr1ddr1() {
    return streetaddr1;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getDestinationid() {
    return destinationid;
  }

  public void setDestinationid(String destinationid) {
    this.destinationid = destinationid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCmpyid() {
    return cmpyid;
  }

  public void setCmpyid(String cmpyid) {
    this.cmpyid = cmpyid;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getDestinationtypeid() {
    return destinationtypeid;
  }

  public void setDestinationtypeid(int destinationtypeid) {
    this.destinationtypeid = destinationtypeid;
  }

  public AccessLevel getAccessLevel() {
    return accessLevel;
  }

  public void setAccessLevel(AccessLevel accessLevel) {
    this.accessLevel = accessLevel;
  }

  @Override
  public String getImageUrl() {
    if(getFileImage() == null) {
      return getCoverurl();
    } else {
      return getFileImage().getUrl();
    }
  }

  @Override
  public FileImage getFileImage() {
    return getCoverImage();
  }

  @Override
  public IUmappedModelWithImage setFileImage(FileImage image) {
    setCoverImage(image);
    if(image != null) {
      setCoverurl(image.getUrl());
      setCovername(image.getFilename());
    }
    return this;
  }

  @Override
  public Destination getByPk(String pk) {
    return find.byId(pk);
  }

  @Override
  public String getPk() {
    return getDestinationid();
  }

  @Override
  public String getPkAsString() {
    return getDestinationid();
  }

  @Override
  public Long getCreatedByPk() {
    return Account.findByLegacyId(getCreatedby()).getUid();
  }

  @Override
  public Long getModifiedByPk() {
    return Account.findByLegacyId(getModifiedby()).getUid();
  }

  @Override
  public String getCreatedByLegacy() {
    return getCreatedby();
  }

  @Override
  public String getModifiedByLegacy() {
    return getModifiedby();
  }

  @Override
  public Long getCreatedEpoch() {
    return getCreatedtimestamp();
  }

  @Override
  public Timestamp getCreatedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getCreatedtimestamp()));
  }

  @Override
  public Long getModifiedEpoch() {
    return getLastupdatedtimestamp();
  }

  @Override
  public Timestamp getModifiedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getLastupdatedtimestamp()));
  }
}
