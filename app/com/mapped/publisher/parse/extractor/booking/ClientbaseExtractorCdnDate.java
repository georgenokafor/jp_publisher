package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by twong on 07/06/17.
 */
public class ClientbaseExtractorCdnDate extends ClientbaseExtractor {

    @Override
    public Timestamp getTimestamp(String year, String month, String day, int hours, int minutes, String ampm) {
        return super.getTimestamp(year, day, month, hours, minutes, ampm);
    }

    @Override
    public Timestamp getUndefinedYearTimestamp(int bookinsYearStart,
                                               int bookingsYearEnd,
                                               String month,
                                               String day,
                                               String hours,
                                               String minutes,
                                               String ampm) {
        return super.getUndefinedYearTimestamp(bookinsYearStart, bookingsYearEnd, day, month, hours, minutes, ampm);
    }


}