package com.umapped.external.travelbound;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mapped.publisher.utils.Log;
import com.umapped.external.travelbound.api.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import javax.xml.bind.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Created by wei on 2017-05-08.
 */
@Singleton public class TravelboundWebService {

  private TravelboundAPIInvoker invoker;
  private TravelboundRequestBuilder requestBuilder;

  @Inject public TravelboundWebService(TravelboundAPIInvoker invoker) {
    this.invoker = invoker;
    this.requestBuilder = new TravelboundRequestBuilder();
  }

  public TSearchBookingResponse searchBooking(TravelboundAPICredential credential, TBookingReference bookingReference)
      throws TravelboundAPIException {
    TRequestDetails requestDetails = requestBuilder.requestDetails(requestBuilder.createSearchBookingRequest(
        bookingReference));

    Response response = invoker.invoke(credential, requestDetails);

    List<JAXBElement<?>> results = getResults(response);

    if (results.size() == 1) {
      return (TSearchBookingResponse) results.get(0).getValue();
    }
    else {
      throw new TravelboundAPIException("Unexpected error: the response does not contain 1 result: " + results.size());
    }
  }

  public TSearchBookingItemResponse getBookingItems(TravelboundAPICredential credential, String apiBookingRefNumber)
      throws TravelboundAPIException {
    TBookingReference bookingReference = new TBookingReference();
    bookingReference.setValue(apiBookingRefNumber);
    bookingReference.setReferenceSource(TReferenceSource.API);
    return getBookingItems(credential, bookingReference);
  }

  public TSearchBookingItemResponse getBookingItems(TravelboundAPICredential credential,
                                                    TBookingReference bookingReference)
      throws TravelboundAPIException {
    TRequestDetails requestDetails = requestBuilder.requestDetails(requestBuilder.createSearchBookingItemRequest(
        bookingReference));

    Response response = invoker.invoke(credential, requestDetails);

    return getSingleResponse(response);
  }

  public List<TSearchBookingItemResponse> getBookingItems(TravelboundAPICredential credential,
                                                          Set<String> apiBookingIds)
      throws TravelboundAPIException {
    TRequestDetails requestDetails = requestBuilder.requestDetails(requestBuilder.createSearchBookingItemRequests(
        apiBookingIds));

    Response response = invoker.invoke(credential, requestDetails);

    List<JAXBElement<?>> results = getResults(response);
    List<TSearchBookingItemResponse> itemResponses = new ArrayList<>(results.size());
    for (JAXBElement ele : results) {
      itemResponses.add((TSearchBookingItemResponse) ele.getValue());
    }
    return itemResponses;
  }

  public TSearchItemInformationResponse searchItemInfo(TravelboundAPICredential credential,
                                                       TItemType type,
                                                       String cityCode,
                                                       String itemCode)
      throws TravelboundAPIException {
    TRequestDetails requestDetails = requestBuilder.requestDetails(requestBuilder.createSearchItemInformationRequest(
        type,
        cityCode,
        itemCode));
    Response response = invoker.invoke(credential, requestDetails);
    return getSingleResponse(response);
  }

  public TSearchChargeConditionsResponse searchChargeConditions(TravelboundAPICredential credential,
                                                                String apiBookingId,
                                                                int referenceNumber)
      throws TravelboundAPIException {
    TRequestDetails requestDetails = requestBuilder.requestDetails(requestBuilder.createSearchChargeConditionRequest(
        apiBookingId,
        referenceNumber));
    Response response = invoker.invoke(credential, requestDetails);
    return getSingleResponse(response);
  }

  public TSearchItemInformationResponse getItemInformation(TravelboundAPICredential credential, TItemType itemType,
                                                           TravelBoundItemInfoKey key)
      throws TravelboundAPIException {
    TRequestDetails requestDetails = requestBuilder.requestDetails(requestBuilder.createSearchItemInformationRequest(itemType, key.getCityCode(), key.getItemCode()));
    Response response = invoker.invoke(credential, requestDetails);
    return getSingleResponse(response);
  }

  protected <T> T getSingleResponse(Response response)
      throws TravelboundAPIException {
    List<JAXBElement<?>> results = getResults(response);

    if (results.size() == 1) {
      return (T) (results.get(0).getValue());
    }
    else {
      throw new TravelboundAPIException("Unexpected error: the response does not contain 1 result: " + results.size());
    }
  }

  protected List<JAXBElement<?>> getResults(Response response)
      throws TravelboundAPIException {
    if (response.getErrors() == null || CollectionUtils.isEmpty(response.getErrors().getErrors())) {
      // success response
      return response.getResponseDetails()
                     .getSearchHotelPricePaxResponsesAndApartmentPriceBreakdownResponsesAndAvailabilityCacheResponses();
    }
    else {
      throw new TravelboundAPIException(response.getErrors().getErrors());
    }
  }

  public List<String> getCountries(TravelboundAPICredential credential)
      throws TravelboundAPIException {
    TRequestDetails requestDetails = requestBuilder.requestDetails(requestBuilder.createSearchCountryRequest());

    Response response = invoker.invoke(credential, requestDetails);

    TSearchCountryResponse searchCountryResponse = getSingleResponse(response);

    List<String> countries = new ArrayList<>();
    for (TCountry country : searchCountryResponse.getCountryDetails().getCountries()) {
      if (!StringUtils.isEmpty(country.getCode())) {
        countries.add(country.getCode());
      }
    }
    return countries;
  }

  public List<String> getCities(TravelboundAPICredential credential, String countryCode)
      throws TravelboundAPIException {
    TRequestDetails requestDetails = requestBuilder.requestDetails(requestBuilder.createSearchCityRequest(countryCode));

    try {
      Response response = invoker.invoke(credential, requestDetails);

      TSearchCityResponse searchCityResponse = getSingleResponse(response);

      List<String> cities = new ArrayList<>();
      for (TCity city : searchCityResponse.getCityDetails().getCities()) {
        cities.add(city.getCode());
      }
      return cities;
    } catch (TravelboundAPIException e) {
      Log.err("Fail to get cities for country : " + countryCode);
      return Collections.emptyList();
    }
  }
}
