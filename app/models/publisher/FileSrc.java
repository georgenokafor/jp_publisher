package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.EnumValue;
import com.mapped.publisher.common.ConfigMgr;

import javax.persistence.*;

/**
 * Created by surge on 2015-11-04.
 */
@Entity
@Table(name = "file_src")
public class FileSrc
    extends Model {

  public enum FileSrcType {
    @EnumValue("EMAIL_RAW")
    EMAIL_RAW,
    @EnumValue("EMAIL_ATTACH")
    EMAIL_ATTACH,
    @EnumValue("ACCOUNT")
    ACCOUNT,
    @EnumValue("IMG_UUP")
    IMG_USER_UPLOAD,
    @EnumValue("FILE_UUP")
    FILE_USER_UPLOAD,
    @EnumValue("IMG_WEB")
    IMG_USER_DOWNLOAD,
    @EnumValue("IMG_T42")
    IMG_VENDOR_T42,
    @EnumValue("IMG_ICE")
    IMG_VENDOR_ICE_PORTAL,
    @EnumValue("IMG_WIKIPEDIA")
    IMG_VENDOR_WIKIPEDIA,
    @EnumValue("IMG_UMAPPED")
    IMG_VENDOR_UMAPPED,
    @EnumValue("IMG_WETU")
    IMG_VENDOR_WETU,
    @EnumValue("IMG_BIGFIVE")
    IMG_VENDOR_BIGFIVE,
    @EnumValue("IMG_GLOBUS")
    IMG_VENDOR_GLOBUS,
    @EnumValue("IMG_MOBILIZER")
    IMG_MOBILIZER,
    @EnumValue("IMG_VIRTUOSO")
    IMG_VENDOR_VIRTUOSO,
    @EnumValue("IMG_TRAVELBOUND")
    IMG_VENDOR_TRAVELBOUND,
    @EnumValue("IMG_UMCITY")
    IMG_UM_CITY,
    @EnumValue("IMG_WCITIES")
    IMG_WCITIES
  }

  public static Model.Finder<FileSrcType, FileSrc> find = new Finder(FileSrc.class);
  /**
   * ALL CAPS/NO SPACES name of the source (will be enum in Java)
   */
  @Id
  @Enumerated(value = EnumType.STRING)
  @Column(name = "name")
  FileSrcType name;
  /**
   * Description, it might be user visible
   */
  String description;
  String bucket;
  String prefix;

  public String getPrefix() {
    return prefix;
  }

  public FileSrc setPrefix(String prefix) {
    this.prefix = prefix;
    return this;
  }

  public String getBucket() {
    return bucket;
  }

  public void setBucket(String bucket) {
    this.bucket = bucket;
  }

  public FileSrcType getName() {
    return name;
  }

  public void setName(FileSrcType name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getRealBucketName() {
    if(bucket.equals("um-image")) {
      return bucket + ConfigMgr.awsSuffix();
    }

    if (ConfigMgr.getInstance().isProd()) {
      return bucket;
    }
    return bucket + "-test";
  }
}
