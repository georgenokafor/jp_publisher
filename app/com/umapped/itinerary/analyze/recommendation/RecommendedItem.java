package com.umapped.itinerary.analyze.recommendation;

import com.umapped.itinerary.analyze.supplier.Supplier;

import java.util.List;

/**
 * Created by wei on 2017-03-31.
 */
public class RecommendedItem {
  public Supplier supplier;
  public RecommendedItemType type;
  public String name;
  public String description;
  public String bookingUrl;
  public String id;
  public String imageUrl;
  public String imageAttribution;
  public Address address;
  public List<Price> prices;

  public static class Address {
    public String streetAddress;
    public String locality;
    public String region;
    public String country;
  }

  public static class Price {
    public String value;
    public String currency;
    public String description;

    public Price() {

    }
    public Price(String value, String currency, String description) {
      this.value = value;
      this.currency = currency;
      this.description = description;
    }

    public String toString() {
      return String.format("Price : [%s, %s, %s]", description, value, currency);
    }
  }

  public String toString() {
    return String.format("%s: %s, %s:  %s\n", supplier, id, name,  prices);
  }
}
