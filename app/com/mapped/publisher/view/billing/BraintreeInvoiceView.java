package com.mapped.publisher.view.billing;

import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.CmpyInfoView;
import com.umapped.api.billing.BillingAddress;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 2016-11-01.
 */
public class BraintreeInvoiceView extends BaseView {

  public static class TxnDetail {
    public String date;
    public String plan;
    public String user;
    public String amount;
    public String currency;
    public String status;
    public String txnType;
  }

  public List<TxnDetail> transactions;


  public String invoiceDate;
  public String total;
  public String taxes;
  public String netTotal;
  public String dueTotal;


  public String currency;

  public String name;
  public String cmpyName;
  public BillingAddress address;



  public void addTransactions (TxnDetail txnDetail) {
    if (transactions == null) {
      transactions = new ArrayList<>();
    }
    transactions.add(txnDetail);
  }



}
