#!/bin/sh
docker run --rm -ti \
  -p 80:9000 \
  -e SMTP_HOST=$SMTP_HOST \
  -e SMTP_USER_ID=$SMTP_USER_ID \
  -e SMTP_PASSWORD=$SMTP_PASSWORD \
  -e DATABASE_WEB_URL=$DATABASE_WEB_URL \
  -e DATABASE_URL=$DATABASE_URL \
  -e MEMCACHIER_USERNAME=$MEMCACHIER_USERNAME \
  -e MEMCACHIER_PASSWORD=$MEMCACHIER_PASSWORD \
  -e MEMCACHIER_SERVERS=$MEMCACHIER_SERVERS \
  -e S3_BUCKET=$S3_BUCKET \
  -e S3_ACCESS_KEY=$S3_ACCESS_KEY \
  -e S3_SECRET=$S3_SECRET \
  -e HOST_URL=$HOST_URL \
  -e SQS_Q_URL=$SQS_Q_URL \
  -e FIVE_FILTERS=$FIVE_FILTERS \
  -e FRESHBOOKS_URL=$FRESHBOOKS_URL \
  -e FRESHBOOKS_TOKEN=$FRESHBOOKS_TOKEN \
  umapped/publisher

