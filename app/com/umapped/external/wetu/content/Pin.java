
package com.umapped.external.wetu.content;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "features",
    "rooms",
    "activities",
    "restaurants",
    "travel_information",
    "map_object_id",
    "name",
    "status",
    "last_modified",
    "type",
    "category",
    "position",
    "content",
    "documentation",
    "subcategory"
})
public class Pin {

    @JsonProperty("features")
    private Features features;
    @JsonProperty("rooms")
    private List<Room> rooms = null;
    @JsonProperty("activities")
    private List<Activity> activities = null;
    @JsonProperty("restaurants")
    private List<Restaurant> restaurants = null;
    @JsonProperty("travel_information")
    private TravelInformation travelInformation;
    @JsonProperty("map_object_id")
    private int mapObjectId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("status")
    private String status;
    @JsonProperty("last_modified")
    private String lastModified;
    @JsonProperty("type")
    private String type;
    @JsonProperty("category")
    private String category;
    @JsonProperty("position")
    private Position position;
    @JsonProperty("content")
    private Content content;
    @JsonProperty("documentation")
    private List<Documentation> documentation = null;
    @JsonProperty("subcategory")
    private String subcategory;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("features")
    public Features getFeatures() {
        return features;
    }

    @JsonProperty("features")
    public void setFeatures(Features features) {
        this.features = features;
    }

    @JsonProperty("rooms")
    public List<Room> getRooms() {
        return rooms;
    }

    @JsonProperty("rooms")
    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    @JsonProperty("activities")
    public List<Activity> getActivities() {
        return activities;
    }

    @JsonProperty("activities")
    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    @JsonProperty("restaurants")
    public List<Restaurant> getRestaurants() {
        return restaurants;
    }

    @JsonProperty("restaurants")
    public void setRestaurants(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    @JsonProperty("travel_information")
    public TravelInformation getTravelInformation() {
        return travelInformation;
    }

    @JsonProperty("travel_information")
    public void setTravelInformation(TravelInformation travelInformation) {
        this.travelInformation = travelInformation;
    }

    @JsonProperty("map_object_id")
    public int getMapObjectId() {
        return mapObjectId;
    }

    @JsonProperty("map_object_id")
    public void setMapObjectId(int mapObjectId) {
        this.mapObjectId = mapObjectId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("last_modified")
    public String getLastModified() {
        return lastModified;
    }

    @JsonProperty("last_modified")
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    @JsonProperty("position")
    public Position getPosition() {
        return position;
    }

    @JsonProperty("position")
    public void setPosition(Position position) {
        this.position = position;
    }

    @JsonProperty("content")
    public Content getContent() {
        return content;
    }

    @JsonProperty("content")
    public void setContent(Content content) {
        this.content = content;
    }

    @JsonProperty("documentation")
    public List<Documentation> getDocumentation() {
        return documentation;
    }

    @JsonProperty("documentation")
    public void setDocumentation(List<Documentation> documentation) {
        this.documentation = documentation;
    }

    @JsonProperty("subcategory")
    public String getSubcategory() {
        return subcategory;
    }

    @JsonProperty("subcategory")
    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("features", features).append("rooms", rooms).append("activities", activities).append("restaurants", restaurants).append("mapObjectId", mapObjectId).append("name", name).append("status", status).append("lastModified", lastModified).append("type", type).append("category", category).append("position", position).append("content", content).append("documentation", documentation).append("subcategory", subcategory).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(position).append(lastModified).append(status).append(mapObjectId).append(type).append(content).append(category).append(additionalProperties).append(subcategory).append(name).append(documentation).append(features).append(activities).append(restaurants).append(rooms).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Pin) == false) {
            return false;
        }
        Pin rhs = ((Pin) other);
        return new EqualsBuilder().append(position, rhs.position).append(lastModified, rhs.lastModified).append(status, rhs.status).append(mapObjectId, rhs.mapObjectId).append(type, rhs.type).append(content, rhs.content).append(category, rhs.category).append(additionalProperties, rhs.additionalProperties).append(subcategory, rhs.subcategory).append(name, rhs.name).append(documentation, rhs.documentation).append(features, rhs.features).append(activities, rhs.activities).append(restaurants, rhs.restaurants).append(rooms, rhs.rooms).isEquals();
    }

}
