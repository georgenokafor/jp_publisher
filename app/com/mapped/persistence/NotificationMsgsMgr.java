package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class NotificationMsgsMgr {

	 private static final String SELECT_ALL = "Select Pk, DevId, UsersId, Msg, LastUpdateTimestamp, CreateTimestamp, Status, SyncTimestamp from NotificationMsgs;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, DevId, UsersId, Msg, LastUpdateTimestamp, CreateTimestamp, Status, SyncTimestamp from NotificationMsgs where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from NotificationMsgs where Pk = ?;";
	 private static final String INSERT = "insert into NotificationMsgs (Pk, DevId, UsersId, Msg, LastUpdateTimestamp, CreateTimestamp, Status, SyncTimestamp) values (?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update NotificationMsgs SET DevId=?, UsersId=?, Msg=?, LastUpdateTimestamp=?, Status=?, SyncTimestamp=? WHERE Pk = ?; ";
	 public static ArrayList <NotificationMsgs> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return NotificationMsgsMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static NotificationMsgs getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<NotificationMsgs> results = NotificationMsgsMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (NotificationMsgs c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getDevId());
				 prep.setString(3, c.getUsersId());
				 prep.setString(4, c.getMsg());
				 prep.setLong(5, c.getLastUpdateTimestamp());
				 prep.setLong(6, c.getCreateTimestamp());
				 prep.setLong(7, c.getStatus());
				 prep.setLong(8, c.getSyncTimestamp());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (NotificationMsgs c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getDevId());
				 prep.setString(2, c.getUsersId());
				 prep.setString(3, c.getMsg());
				 prep.setLong(4, c.getLastUpdateTimestamp());
				 prep.setLong(5, c.getStatus());
				 prep.setLong(6, c.getSyncTimestamp());
				 prep.setString(7, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <NotificationMsgs> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <NotificationMsgs> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 NotificationMsgs c = new NotificationMsgs();
			 c.setPk(rs.getString("Pk"));
			 c.setDevId(rs.getString("DevId"));
			 c.setUsersId(rs.getString("UsersId"));
			 c.setMsg(rs.getString("Msg"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setStatus(rs.getLong("Status"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 results.add(c);
		 }
		 return results;
	 }
}
