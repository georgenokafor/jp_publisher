package com.mapped.publisher.parse.extractor.booking;


import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripperByArea;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ryan on 15-09-03.
 */
public class GoldmanParser
    extends BookingExtractor {

  private String file;
  private TripVO trip;

  private static String[] dateTimeFormat = {"EEE dd MMM yy h:mm a", "EEE dd MMM yy HH:mm"};
  private static DateUtils du = new DateUtils();
  private static Calendar now = Calendar.getInstance();

  private enum BOOKING_TYPE {
    HOTEL,
    FLIGHT,
    TOUR,
    TRANSFER,
    CRUISE,
    TRAIN,
    INSURANCE,
    CAR,
    UNDEFINED
  }

  ;

  private static Pattern timePattern = Pattern.compile("[0-9]{2}:[0-9]{2}");
  private static Pattern timePattern12 = Pattern.compile("[0-9]{1,2}:[0-9]{2}\\s[APap][mM]");


  protected void initImpl(Map<String, String> params) {

  }

  protected TripVO extractDataImpl(InputStream input)
      throws Exception {
    trip = new TripVO();
    file = getPDFContentAsString(input);
    processFile();
    trip.setImportSrc(BookingSrc.ImportSrc.TRAMADA);
    trip.setImportTs(System.currentTimeMillis());

    return trip;
  }

  private void processFile() {
    List<String> frequentFlyer = new ArrayList();
    List<String> etickets = new ArrayList();

    String[] tokens = file.split("\n");
    for (int i = 0; i < tokens.length; i++) {
      String s = tokens[i];
      BOOKING_TYPE booking_type = getBookingType(s);
      if (booking_type != BOOKING_TYPE.UNDEFINED) {
        if (booking_type == BOOKING_TYPE.HOTEL) {
          i = parseHotel(i, tokens);
        }
        else if (booking_type == BOOKING_TYPE.TRANSFER) {
          i = parseTransfers(i, tokens);
        }
        else if (booking_type == BOOKING_TYPE.TRAIN) {
          i = parseTrains(i, tokens);
        }
        else if (booking_type == BOOKING_TYPE.TOUR) {
          i = parseTours(i, tokens);
        }
        else if (booking_type == BOOKING_TYPE.FLIGHT) {
          i = parseFlight(i, tokens);
        } else if (booking_type == BOOKING_TYPE.CRUISE) {
          i = parseCruise(i, tokens);
        } else if (booking_type == BOOKING_TYPE.CAR) {
          i = parseCar(i, tokens);
        }
      }
    }

    if (trip.getFlights() != null && trip.getFlights().size() > 0) {
      for (String s : tokens) {
        if (s.trim().startsWith("TKT ")) {
          etickets.add(s);
        } else if (s.contains("FF ")) {
          frequentFlyer.add(s);
        }
      }

      //process tkt
      for (String s: etickets) {
        String airlineCode = s.substring(4,6);
        String ticket = s.substring(7, s.indexOf(" - "));
        String s1 = s.substring(s.indexOf(" - ") + 2);
        String lastName = s1.substring(0, s1.indexOf("/")).trim();
        String firstName = s1.substring(s1.indexOf("/") + 1, s1.indexOf(" - ")).trim();
        firstName = cleanFirstName(firstName);
        boolean found = false;
        for (FlightVO f: trip.getFlights()) {
          if (f.getCode().equals(airlineCode)) {
            if (f.getPassengers() != null) {
              for (PassengerVO p : f.getPassengers()) {
                if (p.getFirstName().equals(firstName) && p.getLastName().equals(lastName)) {
                  found = true;
                  if (p.geteTicketNumber() == null) {
                    p.seteTicketNumber(ticket);
                  }
                }
              }
            }
            if (!found) {
              PassengerVO p =new PassengerVO();
              p.setFirstName(firstName);
              p.setLastName(lastName);
              p.seteTicketNumber(ticket);
              f.addPassenger(p);
            }
          }
        }
      }

      String lastName1 = "";
      String firstName1 ="";

      for (String s: frequentFlyer) {
        String airlineCode = null;
        boolean found = false;
        String flyer = null;
        if (s.startsWith("FF")) {
          airlineCode = s.substring(3,5).trim();
          flyer = s.substring(6).trim();

        } else {
          if (s.contains("/") && s.contains(" FF ")) {
            lastName1 = s.substring(0, s.indexOf("/")).trim();
            firstName1 = s.substring(s.indexOf("/") + 1, s.indexOf(" FF ")).trim();
            firstName1 = cleanFirstName(firstName1);
            airlineCode = s.substring(s.indexOf(" FF ") + 4, s.indexOf(" FF ") + 6).trim();
            found = false;
            flyer = s.substring(s.indexOf(" FF " + airlineCode) + 6).trim();
          }
        }

        for (FlightVO f: trip.getFlights()) {
          if (airlineCode != null && f.getCode().equals(airlineCode) && flyer != null) {
            if (f.getPassengers() != null) {
              for (PassengerVO p: f.getPassengers()) {
                if (p.getFirstName().equals(firstName1) && p.getLastName().equals(lastName1)) {
                  found = true;
                  if (p.getFrequentFlyer() == null) {
                    p.setFrequentFlyer(flyer);
                    break;
                  }
                }
              }
            }
            if (!found && firstName1.trim().length() > 0 && lastName1.trim().length() > 0) {
              PassengerVO p =new PassengerVO();
              p.setFirstName(firstName1);
              p.setLastName(lastName1);
              p.setFrequentFlyer(flyer);
              f.addPassenger(p);
            }
          }
        }
      }
    }
  }

  private BOOKING_TYPE getBookingType(String s) {
    if (s != null && containsDay(s)) {
      if (s.contains("Hotel Name:")) {
        return BOOKING_TYPE.HOTEL;
      }
      else if (s.contains("Airline:")) {
        return BOOKING_TYPE.FLIGHT;
      }
      else if (s.contains("Tour Company:")) {
        return BOOKING_TYPE.TOUR;
      }
      else if (s.contains("Transfer Company:")) {
        return BOOKING_TYPE.TRANSFER;
      }
      else if (s.contains("Train Name:")) {
        return BOOKING_TYPE.TRAIN;
      }
      else if (s.contains("Cruise Company:")) {
        return BOOKING_TYPE.CRUISE;
      }  else if (s.contains("Car Company:")) {
        return BOOKING_TYPE.CAR;
      }

    }
    return BOOKING_TYPE.UNDEFINED;
  }

  private boolean containsDay(String s) {
    if (s != null) {
      s = s.toLowerCase();
      if (s.contains("monday") || s.contains("tuesday") || s.contains("wednesday") || s.contains("thursday") || s
          .contains(

              "friday") || s.contains("saturday") || s.contains("sunday")) {
        return true;
      }
    }

    return false;
  }

  @Override
  public ArrayList<ParseError> getParseErrors() {
    return null;
  }

  @Override
  public void postProcessingImpl(String tripId, String userId) {
  }

  public int parseHotel(int i, String[] tokens) {
    HotelVO hotel = new HotelVO();
    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (s.contains("Hotel Name:")) {
        hotel.setName(s.substring(s.indexOf("Hotel Name:") + 11).trim());
      }
      else if (s.contains("Check-In Date:")) {
        hotel.setCheckin(parseTimestamp(s, "Check-In Date:"));
      }
      else if (s.contains("Check-Out Date:")) {
        hotel.setCheckout(parseTimestamp(s, "Check-Out Date:"));
      }
      else if (s.contains("Booking Reference:")) {
        hotel.setConfirmation(s.substring(s.indexOf("Booking Reference:") + 18).trim());
      }
      else {
        if (s.startsWith("Comment")) {
          sb.append("\n");
        }
        sb.append(s);
        sb.append("\n");
      }

      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }

    }
    hotel.setNote(sb.toString());

    if (hotel.isValid()) {
      trip.addHotel(hotel);
    }

    return i;


  }

  public int parseFlight(int i, String[] tokens) {
    FlightVO flight = new FlightVO();
    String fClass = null;
    StringBuilder sb = new StringBuilder();
    boolean processPassenger = false;
    boolean processSeat = false;

    List<String> passengers = new ArrayList<>();
    List<String> seats = new ArrayList<>();

    while (true) {
      String s = tokens[i];
      if (s.contains("Airline:") && s.contains("Flight")) {
        String fnum = s.substring(s.indexOf("Airline:") + 8);
        fnum = fnum.substring(fnum.indexOf(" Flight ") + 8);
        if (fnum != null) {
          flight.setCode(fnum.substring(0, 2));
          flight.setNumber(fnum.substring(2).trim());
        }
      }
      else if (s.contains("Departure Date:")) {
        flight.setDepatureTime(parseTimestamp(s, "Departure Date:"));
        String d = s.substring(s.indexOf("Departure Date:") + "Departure Date:".length());
        if (s.length() > 23) {
          int index = 23;
          if (d.contains(" AM")) {
            index = d.indexOf(" AM") + 3;
          } else if (d.contains(" PM")) {
            index = d.indexOf(" PM") + 3;
          }

          d = d.substring(index).trim();
          AirportVO airportVO = new AirportVO();

          if (d.contains(",")) {
            airportVO.setCountry(d.substring(d.indexOf(",") + 1).trim());
            airportVO.setCity(d.substring(0, d.indexOf(",")));
          }
          else {
            airportVO.setCity(d);
            airportVO.setName(d);
          }
          flight.setDepartureAirport(airportVO);
        }

      }
      else if (s.contains("Arrival Date:")) {
        flight.setArrivalTime(parseTimestamp(s, "Arrival Date:"));
        String d = s.substring(s.indexOf("Arrival Date:") + "Arrival Date:".length());
        if (d.length() > 23) {
          int index = 23;
          if (d.contains(" AM")) {
            index = d.indexOf(" AM") + 3;
          } else if (d.contains(" PM")) {
            index = d.indexOf(" PM") + 3;
          }
          d = d.substring(index).trim();
          AirportVO airportVO = new AirportVO();

          if (d.contains(",")) {
            airportVO.setCountry(d.substring(d.indexOf(",") + 1).trim());
            airportVO.setCity(d.substring(0, d.indexOf(",")));
          }
          else {
            airportVO.setCity(d);
            airportVO.setName(d);

          }
          flight.setArrivalAirport(airportVO);
        }
      }
      else if (s.contains("Airline Reference:")) {
        flight.setReservationNumber(s.substring(s.indexOf("Airline Reference:") + "Airline Reference:".length())
                                     .trim());
      }
      else if (s.contains("Class:")) {
        fClass = s.substring(s.indexOf("Class:") + "Class:".length()).trim();
      }
      else {
        if (s.contains("Passengers:")) {
          processPassenger = true;
          processSeat = false;

        }
        if (s.contains("Seats:")) {
          processSeat = true;
          processPassenger = false;

        }
        if (s.contains("Requests:") || s.contains("Comments:")) {
          processPassenger = false;
          processSeat = false;
        }

        if (processPassenger) {
          if (s.contains("Passengers:")) {
            passengers.add(s.substring(s.indexOf("Passengers:") + "Passengers:".length()).trim());
          }
          else {
            passengers.add(s.trim());
          }
        }
        else if (processSeat) {
          if (s.contains("Seats:")) {
            seats.add(s.substring(s.indexOf("Seats:") + "Seats:".length()).trim());
          }
          else {
            if (s.contains("-") && s.contains("/") && s.indexOf("-") < s.indexOf("/")) {
              seats.add(s.trim());
            } else {
              processSeat=false;
            }
          }
        }
        else if (!processPassenger && !processSeat) {
          if (s.startsWith("Comment")) {
            sb.append("\n");
          }
          sb.append(s);
          sb.append("\n");
        }
      }


      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }
      }
    }

    for (String s: passengers) {
      PassengerVO p = new PassengerVO();
      if (s.indexOf("/") > 0) {
        p.setLastName(s.substring(0, s.indexOf("/")).trim());
        String firstName = s.substring(s.indexOf("/") + 1) ;
        firstName = cleanFirstName(firstName);

        p.setFirstName(firstName.trim());

        flight.addPassenger(p);
      }
    }

    for (String s: seats) {
      if (s.contains("-") && s.contains("/") && s.indexOf("-") < s.indexOf("/")) {
        String seat = s.substring(0, s.indexOf("-")).trim();
        String lastName = s.substring(s.indexOf("-") + 1, s.indexOf("/")).trim();
        String firstName = s.substring(s.indexOf("/") + 1).trim();
        firstName = cleanFirstName(firstName);

        if (flight != null && flight.getPassengers() != null) {
          for (PassengerVO p : flight.getPassengers()) {
            if (p.getFirstName().equals(firstName) && p.getLastName().equals(lastName)) {
              p.setSeat(seat);
            }
          }
        } else {
          PassengerVO p = new PassengerVO();
          p.setFirstName(firstName);
          p.setLastName(lastName);
          p.setSeat(seat);
          flight.addPassenger(p);
        }
      }
    }

    if (flight.isValid()) {
      trip.addFlight(flight);
    }

    return i;
  }

  public String cleanFirstName  (String firstName) {
    firstName = firstName.replace(" MISS","");
    firstName = firstName.replace(" MS","");
    firstName = firstName.replace(" MR","");
    firstName = firstName.replace(" MRS", "");
    firstName = firstName.trim();
    return firstName;
  }

  public int parseCruise(int i, String[] tokens) {
    CruiseVO cruise = new CruiseVO();

    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (s.contains("Cruise Company:")) {
        cruise.setCompanyName(s.substring(s.indexOf("Cruise Company:") + "Cruise Company:".length()).trim());
      }
      else if (s.contains("Cruise Name:")) {
        cruise.setName(s.substring(s.indexOf("Cruise Name:") + "Cruise Name:".length()).trim());
      }
      else if (s.contains("Embark") && s.contains("Date:")) {
        cruise.setTimestampDepart(parseTimestamp(s, "Date:"));
      }
      else if (s.contains("Disembark") && s.contains("Date:")) {
        cruise.setTimestampArrive(parseTimestamp(s, "Date:"));
      } else if (s.contains("Disembark Date:")) {
        cruise.setTimestampArrive(parseTimestamp(s, "Disembark Date:"));
      }
      else if (s.contains("Booking Reference:")) {
        cruise.setConfirmationNo(s.substring(s.indexOf("Booking Reference:") + 18).trim());
      }
      else if (s.contains("Cabin No:")) {
        String t = s.substring(s.indexOf("Cabin No:") + "Cabin No:".length()).trim();
        cruise.setCabinNumber(t);
      }
      else if (s.contains("Embark Address:")) {
        cruise.setPortDepart(s.substring(s.indexOf("Embark Address:") + "Embark Address:".length()).trim());

      }
      else if (s.contains("Disembark Address:")) {
        cruise.setPortArrive(s.substring(s.indexOf("Disembark Address:") + "Disembark Address:".length()).trim());
      }
      else {
        if (s.contains("Passengers:") || s.contains("Cruise Itinerary:") || s.startsWith("Comment")) {
         sb.append("\n");
        }
        sb.append(s);
        sb.append("\n");
      }

      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }

    }


    cruise.setNote(sb.toString());

    if (cruise.isValid()) {
      trip.addCruise(cruise);
    }

    return i;


  }

  public int parseTrains(int i, String[] tokens) {
    TransportVO transport = new TransportVO();
    transport.setBookingType(ReservationType.RAIL);

    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (s.contains("Train Name:")) {
        transport.setCmpyName(s.substring(s.indexOf("Train Name:") + "Train Name:".length()).trim());
      }
      else if (s.contains("Type:")) {
        transport.setName(s.substring(s.indexOf("Type:") + "Type:".length()).trim());
      }
      else if (s.contains("Embark") && s.contains("Date:") && transport.getPickupDate() == null) {
        transport.setPickupDate(parseTimestamp(s, "Embark Date:"));
      }
      else if (s.contains("Disembark") && s.contains("Date:") && transport.getDropoffDate() == null) {
        transport.setDropoffDate(parseTimestamp(s, "Date:"));
      }
      else if (s.contains("Booking Reference:")) {
        transport.setConfirmationNumber(s.substring(s.indexOf("Booking Reference:") + 18).trim());
      }
      else if (s.contains("Train No:")) {
        String t = s.substring(s.indexOf("Train No:") + "Train No:".length()).trim();
        transport.setName(s + " - " + transport.getName());
      }
      else if (s.contains("Embark Address:")) {
        if (transport.getPickupLocation() == null) {
          transport.setPickupLocation(s.substring(s.indexOf("Embark Address:") + "Embark Address:".length()).trim());
        }
        sb.append("\n");
        sb.append(s);
      }
      else if (s.contains("Disembark Address:")) {
        transport.setDropoffLocation(s.substring(s.indexOf("Disembark Address:") + "Disembark Address:".length())
                                      .trim());
        sb.append("\n");
        sb.append(s);
      }
      else {
        if (s.startsWith("Comment")) {
          sb.append("\n");
        }
        sb.append(s);
        sb.append("\n");
      }

      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }

    }

    if (transport.getName() == null || transport.getName().length() == 0) {
      transport.setName(transport.getCmpyName());
      transport.setCmpyName("");
    }
    transport.setNote(sb.toString());

    if (transport.isValid()) {
      trip.addTransport(transport);
    }

    return i;


  }

  public int parseTransfers(int i, String[] tokens) {
    TransportVO transport = new TransportVO();
    transport.setBookingType(ReservationType.TRANSPORT);

    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (s.contains("Transfer Company:")) {
        transport.setCmpyName(s.substring(s.indexOf("Transfer Company:") + "Transfer Company:".length()).trim());
      }
      else if (s.contains("Type:")) {
        transport.setName(s.substring(s.indexOf("Type:") + "Type:".length()).trim());
      }
      else if (s.contains("Pick-Up Date:") && transport.getPickupDate() == null) {
        transport.setPickupDate(parseTimestamp(s, "Pick-Up Date:"));
      }
      else if (s.contains("Drop-off date:") && transport.getDropoffDate() == null) {
        transport.setDropoffDate(parseTimestamp(s, "Drop-off date:"));
      }
      else if (s.contains("Booking Reference:")) {
        transport.setConfirmationNumber(s.substring(s.indexOf("Booking Reference:") + 18).trim());
      }
      else if (s.contains("Hotel:")) {
        transport.setPickupLocation(s.substring(s.indexOf("Hotel:") + "Hotel:".length()).trim());
      }
      else if (s.contains("Pick-Up Address:")) {
        if (transport.getPickupLocation() == null) {
          transport.setPickupLocation(s.substring(s.indexOf("Pick-Up Address:") + "Pick-Up Address:".length()).trim());
        }
        sb.append("\n");
        sb.append(s);
        sb.append("\n");

      }
      else if (s.contains("Drop-off Address:")) {
        transport.setDropoffLocation(s.substring(s.indexOf("Drop-off Address:") + "Drop-off Address:".length()).trim());
        sb.append("\n");
        sb.append(s);
        sb.append("\n");

      }
      else {
        if (s.startsWith("Comment")) {
          sb.append("\n");
        }
        sb.append(s);
        sb.append("\n");
      }

      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }

    }

    if (transport.getName() == null || transport.getName().length() == 0) {
      transport.setName(transport.getCmpyName());
      transport.setCmpyName("");
    }
    transport.setNote(sb.toString());

    if (transport.isValid()) {
      trip.addTransport(transport);
    }

    return i;
  }

  public int parseCar(int i, String[] tokens) {
    TransportVO transport = new TransportVO();
    transport.setBookingType(ReservationType.CAR_RENTAL);

    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (s.contains("Car Company:")) {
        transport.setCmpyName(s.substring(s.indexOf("Car Company:") + "Car Company:".length()).trim());
      }
      else if (s.contains("Car Type:")) {
        StringBuilder name = new StringBuilder();
        if (transport.getCmpyName() != null) {
          name.append(transport.getCmpyName()); name.append(" ");
        }
        name.append(s.substring(s.indexOf("Car Type:") + "Car Type:".length()).trim());
        transport.setName(name.toString());
        sb.append(s);sb.append("\n");
      }
      else if (s.contains("Pick-Up Date:") && transport.getPickupDate() == null) {
        transport.setPickupDate(parseTimestamp(s, "Pick-Up Date:"));
        String location = null;
        if (s.contains(" AM ") && s.indexOf(" AM ") < (s.length() - 3)) {
          System.out.println(s);
          location = s.substring(s.indexOf(" AM ") + 4).trim();
        } else if (s.contains(" PM ") && s.indexOf(" PM ") < (s.length() - 3)) {
          location = s.substring(s.indexOf(" PM ") + 4).trim();
        }
        if (location != null) {
          transport.setPickupLocation(location);
        }
      }
      else if (s.contains("Drop-off date:") && transport.getDropoffDate() == null) {
        transport.setDropoffDate(parseTimestamp(s, "Drop-off date:"));
        String location = null;
        if (s.contains(" AM ") && s.indexOf(" AM ") < (s.length() - 3)) {
          location = s.substring(s.indexOf(" AM ") + 4).trim();
        } else if (s.contains(" PM ") && s.indexOf(" PM ") < (s.length() - 3)) {
          location = s.substring(s.indexOf(" PM ") + 4).trim();
        }
        if (location != null) {
          transport.setDropoffLocation(location);
        }
      }
      else if (s.contains("Booking Reference:")) {
        transport.setConfirmationNumber(s.substring(s.indexOf("Booking Reference:") + 18).trim());
      }
      else if (s.contains("Hotel:")) {
        transport.setPickupLocation(s.substring(s.indexOf("Hotel:") + "Hotel:".length()).trim());
      }
      else if (s.contains("Pick-Up Address:")) {
        if (transport.getPickupLocation() == null) {
          transport.setPickupLocation(s.substring(s.indexOf("Pick-Up Address:") + "Pick-Up Address:".length()).trim());
        }
        sb.append("\n");
        sb.append(s);
        sb.append("\n");

      }
      else if (s.contains("Drop-off Address:")) {
        if (transport.getDropoffLocation() == null) {
          transport.setDropoffLocation(s.substring(s.indexOf("Drop-off Address:") + "Drop-off Address:".length()).trim());
        }
        sb.append("\n");
        sb.append(s);
        sb.append("\n");

      }
      else {
        if (s.startsWith("Comment") || s.startsWith("Inclusion")) {
          sb.append("\n");
        }
        sb.append(s);
        sb.append("\n");
      }

      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }

    }

    if (transport.getName() == null || transport.getName().length() == 0) {
      transport.setName(transport.getCmpyName());
      transport.setCmpyName("");
    }
    transport.setNote(sb.toString());

    if (transport.isValid()) {
      trip.addTransport(transport);
    }

    return i;
  }

  public int parseTours(int i, String[] tokens) {
    ActivityVO activity = new ActivityVO();
    activity.setBookingType(ReservationType.ACTIVITY);

    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (s.contains("Tour Company:")) {
        activity.setName(s.substring(s.indexOf("Tour Company:") + "Tour Company:".length()).trim());
      }
      else if (s.contains("Type:")) {
        activity.setName(s.substring(s.indexOf("Type:") + "Type:".length()).trim());
      }
      else if (s.contains("Start Date:") && activity.getStartDate() == null) {
        activity.setStartDate(parseTimestamp(s, "Start Date:"));
      }
      else if (s.contains("Finish Date:") && activity.getEndDate() == null) {
        activity.setEndDate(parseTimestamp(s, "Finish Date:"));
      }
      else if (s.contains("Booking Reference:")) {
        activity.setConfirmation(s.substring(s.indexOf("Booking Reference:") + 18).trim());
      }
      else if (s.contains("Hotel:") && activity.getStartLocation() == null) {
        activity.setStartLocation(s.substring(s.indexOf("Hotel:") + "Hotel:".length()).trim());
      }
      else if (s.contains("Hotel:") && activity.getEndLocation() == null) {
        activity.setEndLocation(s.substring(s.indexOf("Hotel:") + "Hotel:".length()).trim());
      }
      else if (s.contains("Start Address:")) {
        if (activity.getStartLocation() == null) {
          activity.setStartLocation(s.substring(s.indexOf("Start Address:") + "Start Address:".length()).trim());
        }
        sb.append("\n");
        sb.append(s);
      }
      else if (s.contains("Finish Address:")) {
        if (activity.getEndLocation() == null) {
          activity.setEndLocation(s.substring(s.indexOf("Finish Address:") + "Finish Address:".length()).trim());
        }
        sb.append("\n");
        sb.append(s);
      }
      else {
        if (s.startsWith("Comment")) {
          sb.append("\n");
        }
        sb.append(s);
        sb.append("\n");
      }

      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }

    }

    activity.setNote(sb.toString());

    if (activity.isValid()) {
      trip.addActivity(activity);
    }

    return i;

  }

  private Timestamp parseTimestamp(String s, String keyword) {
    if (s != null && keyword != null) {
      if (s.contains(" AM") || s.contains(" PM"))
        return parseTimestamp12(s, keyword);
      else
        return parseTimestamp24(s, keyword);
    }
    return null;
  }

  private Timestamp parseTimestamp24(String s, String keyword) {
    String date = s.substring(s.indexOf(keyword) + keyword.length());
    String time = "";
    if (date.contains(" at ")) {
      time = date.substring(date.indexOf(" at ") + 4).trim();
      if (time.length() > 5) {
        Matcher m = timePattern.matcher(time);
        if (m.find()) {
          time = m.group();
        }
      }
      date = date.substring(0, date.indexOf(" at "));
    }
    else if (date.length() > 13) {
      date = date.substring(0, 14).trim();
    }
    if (time.length() == 5) {
      return getTimestamp(date.trim(), time);
    }
    else {
      return getTimestamp(date.trim(), "00:00");
    }
  }

  private Timestamp parseTimestamp12(String s, String keyword) {
    String date = s.substring(s.indexOf(keyword) + keyword.length());
    String time = "";
    if (date.contains(" at ")) {
      time = date.substring(date.indexOf(" at ") + 4).trim();
      if (time.length() > 5) {
        Matcher m = timePattern12.matcher(time);
        if (m.find()) {
          time = m.group();
        }
      }
      date = date.substring(0, date.indexOf(" at "));
    }
    else if (date.length() > 13) {
      date = date.substring(0, 14).trim();
    }

    if (time.contains(" AM") || time.contains(" PM")) {
      return getTimestamp(date.trim(), time.trim());
    }
    else {
      return getTimestamp(date.trim(), "00:00");
    }
  }

  private Timestamp getTimestamp(String date, String time) {
    try {
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime());
      return t;
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Get the PDF content as String
   *
   * @param fileInputStream
   * @return pdf content as String
   */
  private static final String getPDFContentAsString(InputStream fis) {
    PDDocument pdfDocument = null;
    StringBuilder contents = new StringBuilder();
    try {
      PDFParser parser = new PDFParser(fis);
      parser.parse();

      pdfDocument = parser.getPDDocument();

      PDFTextStripperByArea stripper = new PDFTextStripperByArea();

      stripper.setSortByPosition(true);
      Rectangle rect = new Rectangle(0, 70, 650, 670);
      stripper.addRegion("body", rect);

      java.util.List<?> allPages = pdfDocument.getDocumentCatalog().getAllPages();

      for (Object pdPage : allPages) {
        stripper.extractRegions((PDPage) pdPage);
        contents.append(stripper.getTextForRegion("body"));
      }

    }
    catch (IOException e) {
      e.printStackTrace();
    }
    return contents.toString();
  }

  public static void main(String[] args) {
    try {
      String dir = "/volumes/data2/downloads/";//"/Users/twong/Dropbox/UMapped_Share/Companies/Goldman Travel/Tramada Invoices/"; //"/volumes/data2/downloads/";
      String filename = "allure1.pdf";// "B66753-DEUEL-LIZA-MS-Itinerary---Full.pdf"; //"B53398-SUTTON_ANDREW-Itinerary---Full.pdf"
      File file = new File(dir + filename);



      InputStream ins = new FileInputStream(file);

      GoldmanParser p = new GoldmanParser();
      p.initImpl(null);
      TripVO t = p.extractData(ins);
      System.out.println(t);
    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }
}
