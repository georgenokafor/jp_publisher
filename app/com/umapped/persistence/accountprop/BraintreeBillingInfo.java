package com.umapped.persistence.accountprop;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.umapped.api.billing.BillingAddress;
import com.umapped.api.billing.BillingCCInfo;

import java.io.Serializable;

/**
 * Created by twong on 2016-10-18.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BraintreeBillingInfo
    implements Serializable {
  public BillingAddress address;
  public String pymtNonce;
  public String custId;
  public String addrId;
  public String pymtId;
  public BillingCCInfo ccInfo;
  public boolean ccUpdateNeeded;

  public boolean hasPayment() {
    if (custId != null && !custId.isEmpty() && addrId != null && !addrId.isEmpty() && pymtId != null && !pymtId.isEmpty()) {
      return true;
    }
    return false;
  }
}
