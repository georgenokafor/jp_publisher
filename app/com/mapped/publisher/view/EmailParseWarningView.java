package com.mapped.publisher.view;

import com.mapped.publisher.parse.ParseError;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Serguei Moutovkin on 2014-05-08.
 *
 * Parameters for warning e-mail.
 */
public class EmailParseWarningView extends BaseView
{
    //public TripInfoView tripInfo;
    public String tripUrl;
    public Map<String, ArrayList<ParseError>> parseErrors;
    public String note;
}
