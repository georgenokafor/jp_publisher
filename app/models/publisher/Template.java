package models.publisher;

import com.avaje.ebean.*;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.persistence.IUmappedModel;
import com.mapped.publisher.persistence.IUmappedModelWithImage;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static com.avaje.ebean.Expr.ilike;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-07-24
 * Time: 2:22 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "template")
public class Template
    extends UmappedModelWithImage implements IUmappedModel<String, Template> {

  public enum Visibility {
    PRIVATE,
    PUBLIC;

    public static Visibility fromInt(int idx) {
      if (idx == 0) {
        return PRIVATE;
      }
      return PUBLIC;
    }
  }

  public static Model.Finder<String, Template> find = new Finder(Template.class);
  public String     description;
  @Enumerated(EnumType.ORDINAL)
  public Visibility visibility;
  @Id
  public String     templateid;
  @Constraints.Required
  public String     cmpyid;
  public String     name;
  public int        status;
  public Long       createdtimestamp;
  public Long       lastupdatedtimestamp;
  public String     createdby;
  public String     modifiedby;
  @ManyToOne
  @JoinColumn(name = "cover_image_pk", nullable = true, referencedColumnName = "pk")
  FileImage coverImage;
  @Enumerated(EnumType.STRING)
  private TmpltType.Type tmpltType;
  private int            duration;
  private String         coverUrl;
  private String         coverFilename;
  @ManyToOne
  @JoinColumn(name = "feed_src_id")
  private FeedSrc        feedSrc;
  private String         feedSrcReference;

  public static Template buildTemplate(String userId) {
    Template template = new Template();
    template.setTemplateid(DBConnectionMgr.getUniqueId());
    template.setCreatedby(userId);
    template.setCreatedtimestamp(System.currentTimeMillis());
    template.setModifiedby(userId);
    template.setLastupdatedtimestamp(template.createdtimestamp);
    template.setStatus(APPConstants.STATUS_ACTIVE);
    return template;
  }

  public static int markOrphansAsDeleted(int srcId, String userId) {
    Update<Template> upd = Ebean.createUpdate(Template.class,
                                              "UPDATE template " +
                                              "SET  status = -1, " +
                                              "     lastupdatedtimestamp=:modifiedTs, " +
                                              "     modifiedBy=:modifiedByUserId " +
                                              "WHERE feed_src_id = :srcId AND" +
                                              "      templateid NOT IN (SELECT tmplt_id " +
                                              "                         FROM tmplt_import " +
                                              "                         WHERE src_id = :srcId)");
    upd.set("modifiedTs", System.currentTimeMillis());
    upd.set("modifiedByUserId", userId);
    upd.set("srcId", srcId);
    return upd.execute();
  }

  public static Template findFromFeedSrc(int feedSrcId, String feedSrcReference) {
    return find.where().eq("feed_src_id", feedSrcId).eq("feed_src_reference", feedSrcReference).findUnique();
  }

  public static void forEachActiveFromFeedSrc(int feedSrcId, QueryEachConsumer<Template> consumer) {
    find.where().eq("feed_src_id", feedSrcId).eq("status", APPConstants.STATUS_ACTIVE).findEach(consumer);
  }

  /* Commenting out until upgrade to something fresher than eBean 3.3 in Play 2.3
  @Column(columnDefinition = "hstore")
  private Map<String, String> tags;

  public void addTag(String value, String type) {
    if(tags == null) {
      setTags(new HashMap<String, String>());
    }
    tags.put(value, type);
  }

  public List<String> getTagsByType(String type) {
    if(tags == null || !tags.containsValue(type)) {
      return new ArrayList<>();
    }

    List<String> result = new ArrayList<>();
    for(String key: tags.keySet()) {
      if(tags.get(key).equals(type)) {
        result.add(key);
      }
    }
    return result;
  }

  public Map<String, String> getTags() {
    return tags;
  }

  public void setTags(Map<String, String> tags) {
    this.tags = tags;
  }
  */

  public static void forEachMissingFromFeedSrc(int feedSrcId, QueryEachConsumer<Template> consumer) {
    com.avaje.ebean.Query<TmpltImport> subQuery = Ebean.createQuery(TmpltImport.class)
                                                       .select("tmpltId")
                                                       .where()
                                                       .eq("src_id", feedSrcId)
                                                       .isNotNull("tmpltId")
                                                       .query();

    find.where().eq("feed_src_id", feedSrcId).notIn("templateid", subQuery).findEach(consumer);
  }

  public static List<Template> findFromFeedByNameAndSeason(int feedSrcId, String term, String season) {
    if (term == null || term.trim().length() == 0) {
      return new ArrayList<>();
    }

    if (term.indexOf('%') <= 0) {
      term = "%" + term + "%";
    }

    com.avaje.ebean.Query<TmpltMeta> subQuery = Ebean.createQuery(TmpltMeta.class)
                                                     .select("templateId")
                                                     .where()
                                                     .raw("data @> '{\"season\": \"" + season + "\"}'")
                                                     .query();

    return find.where()
               .eq("feed_src_id", feedSrcId)
               .eq("status", 0)
               .or(ilike("name", term), ilike("description", term))
               .in("templateid", subQuery)
               .findList();
  }

  public static List<Template> findToursByKeywords(String keyword, int srcId) {
    if (keyword != null) {
      if (!keyword.contains("%")) {
        keyword = "%" + keyword + "%";

      }
      return find.where()
                 .or(ilike("name", keyword), ilike("description", keyword))
                 .eq("status", 0)
                 .eq("cmpyid", "0") //TODO: Probably not required????
                 .eq("status", 0)
                 .ge("feed_src_id", srcId)
                 .findList();
    }
    return null;
  }

  public static List<Template> findMostRecentPublicTemplates(List<String> cmpyId, String userId) {
    return find.where()
               .in("cmpyid", cmpyId)
               .eq("status", 0)
               .ne("createdby", userId)
               .eq("visibility", 1)
               .orderBy("lastupdatedtimestamp desc")
               .setMaxRows(20)
               .findList();
  }

  public static List<Template> findMostMyRecentPTemplates(List<String> cmpyId, String userId) {
    return find.where()
               .in("cmpyid", cmpyId)
               .eq("status", 0)
               .eq("createdby", userId)
               .orderBy("lastupdatedtimestamp desc")
               .setMaxRows(20)
               .findList();
  }

  public static List<Template> findMostAllRecentTemplates(List<String> cmpyId) {
    return find.where()
               .in("cmpyid", cmpyId)
               .eq("status", 0)
               .orderBy("lastupdatedtimestamp desc")
               .setMaxRows(40)
               .findList();
  }

  public static List<Template> findPublicTemplates(List<String> cmpyId, String term, String userId) {
    return find.where()
               .in("cmpyid", cmpyId)
               .eq("status", 0)
               .ne("createdby", userId)
               .or(com.avaje.ebean.Expr.icontains("name", term), com.avaje.ebean.Expr.icontains("description", term))
               .eq("visibility", 1)
               .orderBy("lastupdatedtimestamp desc")
               .setMaxRows(20)
               .findList();
  }

  public static List<Template> findMyTemplates(List<String> cmpyIds, String userId, String term) {
    return find.where()
               .in("cmpyid", cmpyIds)
               .eq("status", 0)
               .or(com.avaje.ebean.Expr.icontains("name", term), com.avaje.ebean.Expr.icontains("description", term))
               .eq("createdby", userId)
               .orderBy("lastupdatedtimestamp desc")
               .setMaxRows(20)
               .findList();
  }

  public static List<Template> findAllTemplates(List<String> cmpyId, String term) {
    return find.where()
               .in("cmpyid", cmpyId)
               .eq("status", 0)
               .or(com.avaje.ebean.Expr.icontains("name", term), com.avaje.ebean.Expr.icontains("description", term))
               .orderBy("lastupdatedtimestamp desc")
               .setMaxRows(40)
               .findList();
  }

  public static int mergeCmpy(String srcCmpyId, String targetCmpyId, String userId) {
    String s = "UPDATE template set cmpyid = :targetCmpyId, lastupdatedtimestamp = :timestamp, " + "" + "modifiedby =" +
               " :userid  where cmpyid = :srcCmpyId and status = 0";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("timestamp", System.currentTimeMillis());
    update.setParameter("userid", userId);
    update.setParameter("srcCmpyId", srcCmpyId);
    update.setParameter("targetCmpyId", targetCmpyId);

    return Ebean.execute(update);
  }

  public FileImage getCoverImage() {
    return coverImage;
  }

  public Template setCoverImage(FileImage coverImage) {
    this.coverImage = coverImage;
    return this;
  }

  public String getFeedSrcReference() {
    return feedSrcReference;
  }

  public void setFeedSrcReference(String feedSrcReference) {
    this.feedSrcReference = feedSrcReference;
  }

  public FeedSrc getFeedSrc() {
    return feedSrc;
  }

  public void setFeedSrc(FeedSrc feedSrc) {
    this.feedSrc = feedSrc;
  }

  public TmpltType.Type getTmpltType() {
    return tmpltType;
  }

  public void setTmpltType(TmpltType.Type tmpltType) {
    this.tmpltType = tmpltType;
  }

  @Deprecated
  public String getCoverUrl() {
    return coverUrl;
  }

  @Deprecated
  public void setCoverUrl(String coverUrl) {
    this.coverUrl = coverUrl;
  }

  @Deprecated
  public String getCoverFilename() {
    return coverFilename;
  }

  @Deprecated
  public void setCoverFilename(String coverFilename) {
    this.coverFilename = coverFilename;
  }

  public int getDuration() {
    return duration;
  }

  public void setDuration(int duration) {
    this.duration = duration;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Visibility getVisibility() {
    return visibility;
  }

  public void setVisibility(Visibility visibility) {
    this.visibility = visibility;
  }

  public String getTemplateid() {
    return templateid;
  }

  public void setTemplateid(String templateid) {
    this.templateid = templateid;
  }

  public String getCmpyid() {
    return cmpyid;
  }

  public void setCmpyid(String cmpyid) {
    this.cmpyid = cmpyid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public void markModified(String legacyId) {
    setModifiedby(legacyId);
    setLastupdatedtimestamp(System.currentTimeMillis());
  }


  @Override
  public String getImageUrl() {
    if (getFileImage() == null) {
      return getCoverUrl();
    }
    else {
      return getFileImage().getUrl();
    }
  }

  @Override
  public FileImage getFileImage() {
    return getCoverImage();
  }

  @Override
  public IUmappedModelWithImage setFileImage(FileImage image) {
    setCoverImage(image);
    if(image != null) {
      this.setCoverUrl(image.getUrl());
      this.setCoverFilename(image.getFile().getFilename());
    } else {
      this.setCoverUrl(null);
      this.setCoverImage(null);
      this.setCoverFilename(null);
    }
    return this;
  }

  @Override
  public Template getByPk(String pk) {
    return find.byId(pk);
  }

  @Override
  public String getPk() {
    return getTemplateid();
  }

  @Override
  public String getPkAsString() {
    return getTemplateid();
  }

  @Override
  public Long getCreatedByPk() {
    return Account.findByLegacyId(getCreatedby()).getUid();
  }

  @Override
  public Long getModifiedByPk() {
    return Account.findByLegacyId(getModifiedby()).getUid();
  }

  @Override
  public String getCreatedByLegacy() {
    return getCreatedby();
  }

  @Override
  public String getModifiedByLegacy() {
    return getModifiedby();
  }

  @Override
  public Long getCreatedEpoch() {
    return getCreatedtimestamp();
  }

  @Override
  public Timestamp getCreatedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getCreatedtimestamp()));
  }

  @Override
  public Long getModifiedEpoch() {
    return getLastupdatedtimestamp();
  }

  @Override
  public Timestamp getModifiedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getLastupdatedtimestamp()));
  }
}
