
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExtendedepartureList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExtendedepartureList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExtendedDepartures" type="{http://webapi.globusandcosmos.com/ExtendedDepartureList.xsd}ArrayOfExtendedDeparture" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtendedepartureList", namespace = "http://webapi.globusandcosmos.com/ExtendedDepartureList.xsd", propOrder = {
    "extendedDepartures"
})
public class ExtendedepartureList {

    @XmlElement(name = "ExtendedDepartures")
    protected ArrayOfExtendedDeparture extendedDepartures;

    /**
     * Gets the value of the extendedDepartures property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfExtendedDeparture }
     *     
     */
    public ArrayOfExtendedDeparture getExtendedDepartures() {
        return extendedDepartures;
    }

    /**
     * Sets the value of the extendedDepartures property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfExtendedDeparture }
     *     
     */
    public void setExtendedDepartures(ArrayOfExtendedDeparture value) {
        this.extendedDepartures = value;
    }

}
