package com.mapped.publisher.persistence;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.parse.globus.*;
import com.mapped.publisher.persistence.template.Currency;
import com.mapped.publisher.persistence.template.TmpltMetaJson;
import com.mapped.publisher.persistence.template.TmpltMetaRs;
import com.mapped.publisher.persistence.template.TmpltMgr;
import com.mapped.publisher.utils.Log;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.activity.UmActivityReservation;

import controllers.ImageController;
import models.publisher.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.xml.bind.DatatypeConverter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

/**
 * Created by surge on 2015-10-20.
 */
public class FeedHelperGlobusTours
    extends FeedHelperTmplt {
  public final static  String  GLOBUS_TOURS_FEED_SRC_NAME = "Globus Tours";
  private final static boolean DEBUG                      = false;
  private final static boolean DEBUG_PERSIST              = true;
  private final static String  GLOBUS_USERNAME            = "UMappedApiTest";
  private final static String  GLOBUS_PASSWORD            = "C08x20Z";

  public static class GlobusTourData {
    public AvailableMediaTour tour;
    public WebTourMedia       media;
    public DepartureList      departures;

    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append(tour.getBrand());
      sb.append(" media tour: ");
      sb.append(tour.getTourNumber());
      sb.append(" year: ");
      sb.append(tour.getYear());
      sb.append(" name: ");
      sb.append(tour.getName());
      sb.append("\n");
      sb.append(media.toString());
      for (Departure d : departures.getDepartures().getDepartures()) {
        sb.append("\n");
        sb.append("     Start: ");
        sb.append(d.getLandStartDate());
        sb.append(" code: ");
        sb.append(d.getDepartureCode());
        sb.append(" status: ");
        sb.append(d.getStatus());
        sb.append(" tsac: ");
        sb.append(d.getTourStartAirportCity());
      }
      return sb.toString();
    }
  }

  @Override
  public Map<PoiFeedUtil.FeedResult, Integer> all() {
    return null;
  }

  @Override
  public Map<PoiFeedUtil.FeedResult, Integer> wipe() {
    return null;
  }

  @Override
  public Map<PoiFeedUtil.FeedResult, Integer> load() {
    //Anything imported before barrier time will be deleted
    long prevImportBarrier = System.currentTimeMillis();

    Map<PoiFeedUtil.FeedResult, Integer> results    = new HashMap<>();
    int                                  resSuccess = 0;
    int                                  resError   = 0;
    StopWatch                            sw         = new StopWatch();

    if (DEBUG && false) {
      //Enables underlying implementation to extensively log what is being received
      System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
      System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
      System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
      System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
    }

    GlobusX0020FamilyX0020OfX0020BrandsX0020WebAPISoap globusWebService   = getGlobusWebService();
    AvailableMediaTours                                mtours             = globusWebService.getAvailableMediaToursXML(
        Brand.GLOBUS);
    ObjectMapper                                       jsonMapper         = new ObjectMapper();
    int                                                counter            = 0;
    for (AvailableMediaTour t : mtours.getMediaTours().getAvailableMediaTours()) {

      try {

        sw.reset();
        sw.start();
        GlobusTourData gtd            = new GlobusTourData();
        String         srcReferenceId = getReferenceId(t);
        ++counter;
        gtd.tour = t;
        DepartureList dl = globusWebService.getDeparturesBySeasonXML(Brand.GLOBUS,
                                                                     t.getTourNumber(),
                                                                     t.getYear(),
                                                                     CountryPricing.USA);

        //This tour has no departures this season - deleting previous record if exists
        if (dl.getDepartures() != null && dl.getDepartures().getDepartures() != null && dl.getDepartures()
                                                                                          .getDepartures()
                                                                                          .size() == 0) {
          TmpltImport expiredTour = TmpltImport.findFromSrcByReferenceId(feedSrc.getSrcId(), srcReferenceId);
          if (expiredTour != null) {
            Log.debug("Deleting previous " + t.getBrand() + " Tour " + t.getTourNumber() + " Import for year: " + t.getYear());

            expiredTour.delete();
          }
          continue;
        }

        WebTourMedia wtm = globusWebService.getTourMediaByBrandXML(t.getTourNumber(),
                                                                   t.getYear(),
                                                                   Brand.GLOBUS,
                                                                   MediaLanguage.ENGLISH);
        gtd.media = wtm;
        gtd.departures = dl;
        sw.stop();
        if (DEBUG) {
          Log.debug("Tour #" + counter + " in " + sw.getTime() + "ms \n" + gtd.toString());
        }
        else {
          Log.debug("Tour #" + counter + " in " + sw.getTime() + "ms name:" + t.getName() + " year:" + t.getYear());
        }

        TmpltImport tmpltImport = TmpltImport.findFromSrcByReferenceId(feedSrc.getSrcId(), srcReferenceId);
        if (tmpltImport == null) {
          tmpltImport = TmpltImport.buildTmpltImport(GLOBUS_TOURS_FEED_SRC_NAME, TmpltType.Type.TOUR, t.getName());
          tmpltImport.setSrcReferenceId(srcReferenceId);
        }

        try {
          String json = jsonMapper.writeValueAsString(gtd);
          byte[] hash = DatatypeConverter.parseHexBinary(DigestUtils.md5Hex(json));
          if (!Arrays.equals(hash, tmpltImport.getHash())) {
            tmpltImport.setRaw(json);
            tmpltImport.setHash(hash);
            tmpltImport.setSyncTs(null);
            resSuccess++;
          }

          tmpltImport.setImportTs(System.currentTimeMillis());
          tmpltImport.save();
        }
        catch (JsonProcessingException e) {
          Log.err("Failed to convert " + t.getBrand() + " import to JSON", e);
          e.printStackTrace();
          resError++;
        }
      } catch (Exception e) {
        Log.err("FeedHelperGlobusTours: SERIOUS ERROR: " + t.getName() + " Year:" + t.getYear() + " #:" +
                t.getTourNumber(), e);
      }
    }

    //Cleanup of old imported records before barrier time
    int oldrecs = TmpltImport.deleteFromSrcBeforeBarrierTime(feedSrc.getSrcId(), prevImportBarrier);
    Log.info("FeedHelperGlobusTours: load(): deleted " + oldrecs + " import records that are not in the feed");

    results.put(PoiFeedUtil.FeedResult.ERROR, resError);
    results.put(PoiFeedUtil.FeedResult.SUCCESS, resSuccess);
    return results;
  }

  /**
   * Creating properly wrapped SOAP message
   * <p/>
   * With great thanks to post at SO:
   * http://examples.javacodegeeks.com/enterprise-java/jws/jax-ws-soap-handler-example/
   * and
   * http://stackoverflow.com/questions/10654608/add-soap-header-object-using-pure-jax-ws
   * and
   * http://stackoverflow.com/questions/830691/how-do-i-add-a-soap-header-using-java-jax-ws
   *
   * @return
   */
  public static GlobusX0020FamilyX0020OfX0020BrandsX0020WebAPISoap getGlobusWebService() {
    GlobusX0020FamilyX0020OfX0020BrandsX0020WebAPI globusWebService = new
        GlobusX0020FamilyX0020OfX0020BrandsX0020WebAPI();
    return globusWebService.getGlobusX0020FamilyX0020OfX0020BrandsX0020WebAPISoap();
    /* 2016-10-11: Test API Endpoint is not working
    boolean isProd = ConfigMgr.getInstance().isProd();
    if (DEBUG || isProd) {
      GlobusX0020FamilyX0020OfX0020BrandsX0020WebAPI globusWebService = new
      GlobusX0020FamilyX0020OfX0020BrandsX0020WebAPI();
      return globusWebService.getGlobusX0020FamilyX0020OfX0020BrandsX0020WebAPISoap();
    } else {
      Log.debug("Creating Globus TEST connection instance");
      GlobusX0020FamilyX0020OfX0020BrandsX0020TestWebAPI globusWebService = new
          GlobusX0020FamilyX0020OfX0020BrandsX0020TestWebAPI();
      return globusWebService.getGlobusX0020FamilyX0020OfX0020BrandsX0020TestWebAPISoap();
    }
    */
  }

  public String getReferenceId(AvailableMediaTour tour) {
    StringBuilder sb = new StringBuilder();
    sb.append(tour.getTourNumber());
    sb.append('_');
    sb.append(tour.getYear());
    return sb.toString();
  }

  @Override
  public Map<PoiFeedUtil.FeedResult, Integer> process() {
    List<TmpltImport>                    imports    = TmpltImport.findNewBySrc(feedSrc.getSrcId());
    Map<PoiFeedUtil.FeedResult, Integer> results    = new HashMap<>();
    int                                  resSuccess = 0;
    int                                  resError   = 0;

    try {
      ObjectMapper                                jsonMapper = new ObjectMapper();
      com.fasterxml.jackson.databind.ObjectReader r          = jsonMapper.reader(GlobusTourData.class);
      StopWatch                                   sw         = new StopWatch();
      for (TmpltImport ti : imports) {
        sw.reset();
        sw.start();
        GlobusTourData gtd     = r.readValue(ti.getRaw());
        String         tmpltId = tourToTemplate(modifiedBy.getLegacyId(), gtd, feedSrc, ti.getSrcReferenceId());
        sw.stop();
        Log.debug("Processing in " + sw.getTime() + "ms " + gtd.tour.getBrand() + " tour " + gtd.tour.getName() +
                  " for " + gtd.tour.getYear());
        if (tmpltId != null) {
          ti.setSyncTs(System.currentTimeMillis());
          ti.setTmpltId(tmpltId);
          ti.update();
          ++resSuccess;
        }
        else {
          ++resError;
        }
      }
    }
    catch (Exception e) {
      Log.err("Failed to process GLOBUS imported records", e);
      e.printStackTrace();
    }

    //Marking templates that are no longer present as deleted
    int markedDeleted = Template.markOrphansAsDeleted(feedSrc.getSrcId(), modifiedBy.getLegacyId());
    Log.info("Marked " + markedDeleted + " templates as DELETED because they are no longer in the feed");

    results.put(PoiFeedUtil.FeedResult.ERROR, resError);
    results.put(PoiFeedUtil.FeedResult.SUCCESS, resSuccess);
    return results;
  }

  private String tourToTemplate(String userId, GlobusTourData gtd, FeedSrc feedSrc, String reference) {
    String  resultTmpltId = null;
    boolean isNew         = false;

    Template template = Template.findFromFeedSrc(feedSrc.getSrcId(), reference);
    if (template == null) {
      template = Template.buildTemplate(userId);
      isNew = true;
    }
    else {
      template.setLastupdatedtimestamp(System.currentTimeMillis());
      template.setModifiedby(userId);
    }

    template.setDuration(gtd.media.getTourInfo().getLength());
    template.setVisibility(Template.Visibility.PUBLIC);
    template.setTmpltType(TmpltType.Type.TOUR);
    template.setFeedSrc(feedSrc);
    template.setFeedSrcReference(reference);
    template.setCmpyid("0");
    template.setName(gtd.tour.getName());
    Account account = Account.findActiveByLegacyId(userId);

    //Saving cover image - map as primary image
    StopWatch sw = new StopWatch();
    sw.start();
    //String primaryImg = "http://images.globusfamily.com/vacation/"+ gtd.tour.getTourNumber() +".jpg";
    String primaryImg = "http://images.globusfamily.com/maps/" + gtd.tour.getBrand() + "/" + gtd.tour.getYear() + "/"
                        + gtd.tour.getTourNumber() + ".jpg";

    String fileName = gtd.tour.getTourNumber() + ".jpg";
    try {
      FileImage image = ImageController.downloadAndSaveImage(primaryImg,
                                                             fileName,
                                                             FileSrc.FileSrcType.IMG_VENDOR_GLOBUS,
                                                             gtd.tour.getName(),
                                                             account,
                                                             false);
      template.setFileImage(image);
    }
    catch (Exception e) {
      Log.err("Failed to set " + gtd.tour.getBrand() + " cover for tour: " + gtd.tour.getName() + " filename:" +
              fileName, e);
    }
    sw.stop();
    Log.debug("Loading image in " + sw.getTime() + "ms");

    //Find tour overview
    String        overview    = null;
    StringBuilder description = new StringBuilder();
    for (TourMedia tm : gtd.media.getTourMediaCollection().getTourMedias()) {
      if (tm != null && tm.getContentType() != null && tm.getContentType().equals("Vacation Overview")) {
        description.append("Vacation Overview\n");
        overview = tm.getContent();
        description.append(overview);
      }
    }

    //Find highlights
    String highlights = null;
    for (TourMedia tm : gtd.media.getTourMediaCollection().getTourMedias()) {
      if (tm != null && tm.getContentType() != null && tm.getContentType().equals("City Highlights")) {
        description.append("City Highlights\n");
        Document doc = Jsoup.parseBodyFragment(tm.getContent());
        highlights = doc.text();
        description.append(highlights);
      }
    }

    template.setDescription(description.toString());
    if (DEBUG_PERSIST) {
      template.save();
      resultTmpltId = template.getTemplateid();
    }

    TmpltMetaRs tmrs = tourToMeta(gtd, template, userId, overview, highlights);

    if (DEBUG_PERSIST) {
      if (isNew) {
        //Saving new metadata
        TmpltMgr.save(tmrs);
      }
      else {
        //Updating metadata
        TmpltMgr.update(tmrs);
        //Marking previously generated template details as deleted
        TmpltDetails.markDeleted(template.templateid);
      }
    }

    updateTourDays(template, gtd, userId);
    return resultTmpltId;
  }

  private TmpltMetaRs tourToMeta(GlobusTourData gtd,
                                 Template template,
                                 String userId,
                                 String overview,
                                 String highlights) {
    TmpltMetaRs   meta = TmpltMetaRs.buildMeta(userId, template.getTemplateid(), gtd.tour.getName());
    TmpltMetaJson json = meta.getData();

    json.setDescription(overview);
    json.setHighlights(highlights);
    json.setLength(template.getDuration());
    json.setCurrency(Currency.USD);
    json.setSeason(gtd.tour.getYear());

    for (TourMedia tm : gtd.media.getTourMediaCollection().getTourMedias()) {
      if (tm != null && tm.getContentType() != null && tm.getContentType().equals("Price Details US")) {
        Document doc = Jsoup.parseBodyFragment(tm.getContent());
        json.setPriceInfo(doc.text());
      }
    }

    String tourImg = "http://images.globusfamily.com/vacation/" + gtd.tour.getTourNumber() + ".jpg";
    json.getImages().add(tourImg);

    //Trying to determine countries
    for (TourKeyword kw : gtd.media.getTourKeywordCollection().getTourKeywords()) {
      if (kw.getKeyWordType() == null) {
        continue;
      }
      if (kw.getKeyWordType().equals("LOCATION")) {
        CountriesInfo.Country c = CountriesInfo.Instance().searchByName(kw.getKeyWord());
        if (c != null) {
          json.getCountries().add(c.getName(CountriesInfo.LangCode.EN));
        }
      }
    }
    return meta;
  }

  private void updateTourDays(Template template, GlobusTourData gtd, String userId) {
    for (DayMedia dm : gtd.media.getDayMediaCollection().getDayMedias()) {
      if (dm == null || !dm.getContentType().equals("Vacation Itinerary")) {
        continue;
      }

      //There is a possibility to have the same day more than once
      List<TmpltDetails> tds   = TmpltDetails.findForSpecificTripDay(template.getTemplateid(), dm.getStartDayNum());
      boolean            isNew = false;

      String   name;
      Document doc = Jsoup.parseBodyFragment(dm.getContent());
      Elements els = doc.getElementsByAttributeValue("class", "Location");
      if (els.size() == 1) {
        name = els.text();
      }
      else {
        name = gtd.tour.getName() + " Tour Day " + dm.getStartDayNum();
      }

      //Find my day
      TmpltDetails td = null;
      if (tds != null || tds.size() > 0) {
        for (TmpltDetails tdc : tds) {
          if (tdc.getName().equals(name)) {
            td = tdc;
          }
        }
      }

      if (td == null) {
        isNew = true;
        td = TmpltDetails.buildTmpltDetails(userId);
        td.setName(name);
        td.setDayOffset(dm.getStartDayNum());
        td.setTemplateid(template.getTemplateid());
        td.setDuration(1);
      }
      else {
        td.setLastupdatedtimestamp(System.currentTimeMillis());
        td.setModifiedby(userId);
        td.setStatus(APPConstants.STATUS_ACTIVE);
      }

      td.setComments(doc.text());
      td.setDetailtypeid(ReservationType.ACTIVITY);

      UmActivityReservation activity = cast(td.getReservation(), UmActivityReservation.class);
      if (activity == null) {
        activity = new UmActivityReservation();
        td.setReservation(activity);
      }
      activity.getActivity().name = name;
      activity.setNotesPlainText(doc.text());
      if (DEBUG_PERSIST) {
        td.save();
      }
    }
  }
}
