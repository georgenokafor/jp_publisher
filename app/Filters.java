import play.filters.cors.CORSFilter;
import play.http.DefaultHttpFilters;

import javax.inject.Inject;

/**
 * CORS Filters
 * https://www.playframework.com/documentation/2.5.x/CorsFilter#Enabling-the-CORS-filter
 * <p>
 * Filters logic in Play:
 * https://github.com/playframework/playframework/blob/master/framework/src/play-filters-helpers/src/main/scala/play/filters/cors/AbstractCORSPolicy.scala#L322
 * <p>
 * Created by surge on 2016-02-03.
 */
public class Filters extends DefaultHttpFilters {
  @Inject public Filters(CORSFilter corsFilter) {
    super(corsFilter);
  }
}
