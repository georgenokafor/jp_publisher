
package com.mapped.publisher.parse.globus;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeparturePricing complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeparturePricing">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Discount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="CabinCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeparturePricingDetails" type="{http://webapi.globusandcosmos.com/}ArrayOfDeparturePricingDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeparturePricing", propOrder = {
    "price",
    "discount",
    "cabinCategory",
    "departurePricingDetails"
})
public class DeparturePricing {

    @XmlElement(name = "Price", required = true)
    protected BigDecimal price;
    @XmlElement(name = "Discount", required = true)
    protected BigDecimal discount;
    @XmlElement(name = "CabinCategory")
    protected String cabinCategory;
    @XmlElement(name = "DeparturePricingDetails")
    protected ArrayOfDeparturePricingDetail departurePricingDetails;

    /**
     * Gets the value of the price property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Sets the value of the price property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPrice(BigDecimal value) {
        this.price = value;
    }

    /**
     * Gets the value of the discount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscount(BigDecimal value) {
        this.discount = value;
    }

    /**
     * Gets the value of the cabinCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabinCategory() {
        return cabinCategory;
    }

    /**
     * Sets the value of the cabinCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabinCategory(String value) {
        this.cabinCategory = value;
    }

    /**
     * Gets the value of the departurePricingDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDeparturePricingDetail }
     *     
     */
    public ArrayOfDeparturePricingDetail getDeparturePricingDetails() {
        return departurePricingDetails;
    }

    /**
     * Sets the value of the departurePricingDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDeparturePricingDetail }
     *     
     */
    public void setDeparturePricingDetails(ArrayOfDeparturePricingDetail value) {
        this.departurePricingDetails = value;
    }

}
