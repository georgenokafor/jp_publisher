package com.umapped.itinerary.analyze.supplier.musement;

import com.google.inject.Guice;
import com.google.inject.Injector;
import modules.ItineraryAnalyzeModule;
import modules.OfferModule;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by wei on 2017-06-14.
 */
public class MusementWebServiceTest {
  Injector container;

  @Before
  public void setup() {
     container = Guice.createInjector(new OfferModule(), new ItineraryAnalyzeModule());
  }

  @Test
  public void testSearch() {
    MusementWebService service = container.getInstance(MusementWebService.class);
    service.getEvents("40", null, null);
  }
}
