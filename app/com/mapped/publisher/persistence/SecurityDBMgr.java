package com.mapped.publisher.persistence;

import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-23
 * Time: 5:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class SecurityDBMgr {

  private final static String GROUP_MEMBERS = "SELECT ul.userid as userid, ul.cmpyid as cmpyid, c.groupid as groupid," +
                                              "       ul.linktype as linktype, cmpy.type as cmpyType," +
                                              "       cmpy.expiryTimestamp as cmpyExpiryTimestamp " +
                                              "FROM cmpy_group c, cmpy_group_members cm, user_cmpy_link ul, " +
                                              "     company cmpy " +
                                              "WHERE cm.groupid IN " +
                                              "     (SELECT cm1.groupid" +
                                              "      FROM cmpy_group_members cm1, user_cmpy_link ul1 " +
                                              "      WHERE ul1.userid=? AND ul1.pk = cm1.usercmpylinkpk AND " +
                                              "            ul1.status = 0 AND cm1.status = 0 ) AND " +
                                              "      ul.pk = cm.usercmpylinkpk AND" +
                                              "      cm.groupid = c.groupid AND" +
                                              "      ul.status = 0 AND" +
                                              "      cm.status = 0 AND" +
                                              "      c.status=0 AND" +
                                              "      cmpy.cmpyid = ul.cmpyid AND" +
                                              "      cmpy.status = 0";

  public static List<SecurityRS> findByUserId(String userId, Connection conn)
      throws SQLException {

    boolean newConnection = false;
    if(conn == null) {
      conn = getConnection();
      newConnection = true;
      if(conn == null) {
        return null;
      }
    }

    ArrayList<SecurityRS> results = new ArrayList<>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    try {

      prep = conn.prepareStatement(GROUP_MEMBERS);
      prep.setString(1, userId);
      rs = prep.executeQuery();
      if (rs == null) {
        return results;
      }

      while (rs.next()) {
        SecurityRS c = new SecurityRS();
        c.setUserId(rs.getString("UserId"));
        c.setCmpyId(rs.getString("CmpyId"));
        c.setGroupId(rs.getString("GroupId"));
        c.setLinkType(rs.getInt("linkType"));
        c.setCmpyType(rs.getString("cmpyType"));
        c.setCmpyExpiryTimestamp(rs.getLong("cmpyExpiryTimestamp"));

        results.add(c);
      }
      return results;
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
      if(newConnection) {
        releaseConnection(conn);
      }
    }
  }

  /**
   * Using this specific routine to get connection.
   *
   * Future optimization can be to implement a naive connection pool for vendors.
   * @return Database connection
   */
  private static Connection getConnection() {

    Connection conn = null;

    try {
      conn = DBConnectionMgr.getConnection4Publisher();
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Failed to obtain database connection for POI operation:" + e.getMessage());
      e.printStackTrace();
    }

    return conn;
  }

  /**
   * Returning connection
   */
  private static void releaseConnection(Connection conn) {
    try {
      conn.close();
    } catch (SQLException e) {
      Log.log(LogLevel.ERROR, "Failed to release database connection:" + e.getMessage());
      e.printStackTrace();
    }
  }
}
