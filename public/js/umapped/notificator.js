var noti;

var initNotificator = function(options){

  console.log("Initializing communicator");
  noti = Object.create(Notificator);
  noti.init(options);
  return noti;
};

var Notificator = {
  init: function(options) {
    var self = this;
    self._options = options || {};
    self.bell      = $('#notificationBell');
    self.bellCount = '#notificationBellCount';
    self.commArea  = $('#communicatorArea');
    self.commFrame = $('#communicatorFrame');
    self.notificationCount = 0;
    self.communicatorURI = self._options.dbUri;
    self.communicatorToken = self._options.authToken;
    self.communicator = 0;
    self.msgPostUrl = self._options.msgPostUrl;
    self.talkbackEnabled = self._options.talkback;

    var width = (window.innerWidth < 500)? window.innerWidth - 10 : 500;
    var height= (window.innerHeight < 600)? window.innerHeight - 10 : 600;
    self.commArea
        .dialog({
          title: self._options.title,
          closeText: "Close",
          autoOpen: false,
          /* Animations cause multiple reloads
           show: {
           effect: "blind",
           duration: 250
           },
           hide: {
           effect: "blind",
           duration: 250
           },*/
          resizable: true, /* need to monitor jQuery UI behaviour sometimes UI gets corrupted during resizes */
          position: { my: "right top", at: "left bottom", of: "#notificationBell" },
          width: width,
          height: height,
          closeOnEscape: true,
          open: function() {
            $(this).closest(".ui-dialog")
                .find(".ui-dialog-titlebar-close")
              //.removeClass("ui-dialog-titlebar-close")
              //.addClass("ui-button")
              //.html("<span class='ui-button-icon-primary ui-icon ui-icon-closethick'></span>");
                .html("<i class='fa fa-times'></i>");

            if (self.ui) {
              self.ui.setVisible(true);
            }
          },
          close: function(){
            if(self.ui) {
              self.ui.setVisible(false);
            }
          },
          //This prevents re-ordering of elements that cause iframe reload
          _moveToTop: function( event, silent ) {
            return false;
          }
        });

    if(self._options.title) {
      $('div[aria-describedby="communicatorArea"] .ui-dialog-titlebar').css("background-image", "none");
      $('div[aria-describedby="communicatorArea"] .ui-dialog-titlebar .ui-dialog-title').css("background-image", "none");
    }

    $("#notificationBell").click(function () {
      if (self.commArea.dialog("isOpen")) {
        self.commArea.dialog("close");
      } else {

        if($.browser.msie && $.browser.version < 10) {
          alert("Umapped Communicator is not compatible with Internet Explorer 9 or older.")
          return false;
        }

        self.checkCurrentUrl();
        self.commArea.dialog("open");
      }
    });

    self.commFrame.attr("src", "/assets/js/firechat/com.html?v=20160926");
  },

  setConfRoom: function(confId, roomId) {
    var self = this;
    if(self.communicator) {
      self.communicator.setConfRoom(confId, roomId);
    } else {
      console.log("No Communicator Initialized")
    }
  },

  talkback: function(confId, roomId) {
    var self = this;
    self.setConfRoom(confId, roomId);
    self.open();
  },

  disableTalkback: function() {
    self.talkbackEnabled = false;
  },

  loadCommunicator: function() {
    var self = this;
    self.communicator = self.commFrame[0].contentWindow.initCommunicator(
        {
          dbUri:  self.communicatorURI,
          authToken: self.communicatorToken,
          msgSentUpdateUrl: self.msgPostUrl,
          notificator: self,
          talkback: self.talkbackEnabled
        }
    );
    self.setCommunicatorUI(self.communicator.chat);
  },

  open: function() {
    this.commArea.dialog( "open" );
  },

  setCommunicatorUI: function(ui) {
    this.ui = ui;
  },

  /**
   * @param count if no value is passed increments existing value by one
   */
  setCount: function(count) {
    var self = this;
    self.notificationCount = count;

    var badgeClass = 'badge-success';
    if(count > 0 && count < 10) {
      badgeClass = 'badge-warning';
    } else  if (count >= 10){
      badgeClass = 'badge-important';
    }

    var newBellCount = $('<span id="notificationBellCount" class="badge ' +badgeClass+ ' badge-notify"></span>');
    newBellCount.text(self.notificationCount);
    $(self.bellCount).remove();
    self.bell.append(newBellCount);
  },

  /**
   * Can be called whenever URL changed to obtain new chat parameters
   * @param urlStr
   */
  checkCurrentUrl: function() {
    var self = this,
        urlStr = window.location.hash,
        myReg = /\/trips\/bookings\/([0-9]+)\/.*inDetailsId=([0-9]+)/g,
        tripId = null,
        bookingId = null,
        matches = myReg.exec(urlStr);

    if (matches != null && matches.length == 3) {
      tripId = matches[1];
      bookingId = matches[2];
    }

    if (!tripId) {
      //Just in the trip itinerary page
      myReg = /\/trips\/bookings\/([0-9]+)\/ITINERARY/g;
      matches = myReg.exec(urlStr);
      var tripId = null
      if (matches != null && matches.length == 2) {
        tripId = matches[1];
      }
    }

    if(!tripId) {
      //Just in the trip itinerary page
      myReg = /\/trips\/.*\?tripId=([0-9]+)/g;
      matches = myReg.exec(urlStr);
      if (matches != null && matches.length == 2) {
        tripId = matches[1];
      }
    }

    if(!tripId) {
      myReg = /\/trips\/newTrip\/guides\/([0-9]+)\/ATTACHMENTS/g;
      matches = myReg.exec(urlStr);
      if (matches != null && matches.length == 2) {
        tripId = matches[1];
      }
    }

    if(!tripId){
      myReg = /\/tours\/newTour\/publish\?tripId=([0-9]+)/g;
      matches = myReg.exec(urlStr);
      if (matches != null && matches.length == 2) {
        tripId = matches[1];
      }
    }

    if(!tripId){
      myReg = /\/webItinerary\/([0-9]+)\/data.*/g;
      matches = myReg.exec(window.location.pathname);
      if (matches != null && matches.length == 2) {
        tripId = matches[1];
      }
    }

    if(tripId) {
      if(bookingId) {
        self.communicator.setConfRoom(tripId, bookingId);
        console.log("Communicator switching to TripID: " + tripId + " BookingID: " + bookingId);

      }
      else {
        self.communicator.setConfRoomList(tripId);
        console.log("Communicator switching to room listing for TripID: " + tripId);
      }
    } else {
      self.communicator.resetUI();

    }
  }
};
