package com.umapped.external.travelbound;

/**
 * Created by wei on 2017-05-15.
 */
public class TravelboundImportException extends Exception {
  public TravelboundImportException(String message) {
    super(message);
  }

  public TravelboundImportException(String message, Exception cause) {
    super(message, cause);
  }
}
