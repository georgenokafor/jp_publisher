package com.mapped.publisher.persistence;

import com.mapped.publisher.parse.ensemble.EnsembletravelOffer;
import com.mapped.publisher.parse.ensemble.Form;
import com.mapped.publisher.parse.ensemble.Offer;
import com.mapped.publisher.parse.ensemble.Ship;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.umapped.api.schema.types.Feature;
import models.publisher.CruiseAmenities;
import models.publisher.FeedSrc;
import models.publisher.TmpltImport;
import models.publisher.TmpltType;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import javax.xml.bind.*;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.*;

/**
 * Created by surge on 2015-10-06.
 */
public class FeedHelperEnsembleCruise
    extends FeedHelperTmplt {

  private static final String ENSEMBLE_CRUISES_URL = "http://agent.ensembletravel.com/ws_ensxmlfeeds.asmx/GetOffersGeneral?CountryChoiceRecID=0&Key=Phedep@l";
  public static final String ENSEMBLE_CRUISES_FEED_SRC_NAME = "Ensemble Cruise";
  Map<String, PoiRS> missingShips;

  public FeedHelperEnsembleCruise() {
    missingShips = new HashMap<>();
  }

  @Override
  public Map<PoiFeedUtil.FeedResult, Integer> all() {
    return null;
  }

  @Override
  public Map<PoiFeedUtil.FeedResult, Integer> wipe() {
    return null;
  }

  @Override public Map<PoiFeedUtil.FeedResult, Integer> load() {
    Map<PoiFeedUtil.FeedResult, Integer> results = new HashMap<>();
    int resSuccess = 0;
    int resError = 0;

    try {
      HttpClient httpClient = HttpClientBuilder.create().build();
      HttpGet httpGet = new HttpGet(ENSEMBLE_CRUISES_URL);
      HttpResponse httpResponse = httpClient.execute(httpGet);

      if (httpResponse.getStatusLine() != null && httpResponse.getStatusLine().getStatusCode() == 200) {
        String body = StringEscapeUtils.unescapeJava(EntityUtils.toString(httpResponse.getEntity()));
        InputStream source = new ByteArrayInputStream(body.getBytes());
        XMLInputFactory xif = XMLInputFactory.newFactory();
        XMLStreamReader xsr = xif.createXMLStreamReader(source);

        JAXBContext jaxbContext = JAXBContext.newInstance(EnsembletravelOffer.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        JAXBElement<EnsembletravelOffer> je = jaxbUnmarshaller.unmarshal(xsr, EnsembletravelOffer.class);
        EnsembletravelOffer offers = je.getValue();

        if(offers.getOffer() != null && offers.getOffer().size() > 0) {
          //1. Deleting old imported records of Ensemble type
          TmpltImport.deleteFromSrc(FeedSourcesInfo.byName(ENSEMBLE_CRUISES_FEED_SRC_NAME));

          //2. Load new batch
          for (Offer o : offers.getOffer()) {
            TmpltImport ti = TmpltImport.buildTmpltImport(ENSEMBLE_CRUISES_FEED_SRC_NAME,
                                                          TmpltType.Type.CRUISE_ENSEMBLE,
                                                          o.getTitle().trim());

            JAXBContext jaxbHotelInfoContext = JAXBContext.newInstance(Offer.class);
            Marshaller jaxbMarshaller = jaxbHotelInfoContext.createMarshaller();

            // output pretty printed
            //jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            StringWriter raw = new StringWriter();
            jaxbMarshaller.marshal(o, raw);
            ti.setRaw(raw.toString());
            ti.setHash(DatatypeConverter.parseHexBinary(DigestUtils.md5Hex(raw.toString())));
            ti.setSrcReferenceId(o.getId());
            ti.save();
            Log.info("Imported EnsembleCruise named: " + ti.getName());
            ++resSuccess;
          }
        }
      }
      //consume the request to close the HTTP connection
      EntityUtils.consume(httpResponse.getEntity());
    }
    catch (Exception e) {
      Log.err("Failed to download and extract Ensemble Cruises feed", e);
      e.printStackTrace();
      ++resError;
    }
    results.put(PoiFeedUtil.FeedResult.ERROR, resError);
    results.put(PoiFeedUtil.FeedResult.SUCCESS, resSuccess);

    return results;
  }

  @Override public Map<PoiFeedUtil.FeedResult, Integer> process() {
    FeedSrc feedSrc = FeedSrc.findByName(ENSEMBLE_CRUISES_FEED_SRC_NAME);
    List<TmpltImport> imports = TmpltImport.findNewBySrc(feedSrc.getSrcId());

    Map<PoiFeedUtil.FeedResult, Integer> results = new HashMap<>();

    int resSuccess = 0;
    int resError = 0;

    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(Offer.class);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      //1. Wiping old records
      CruiseAmenities.wipeConsortiumFeed(feedSrc.getConsortiumId());

      //2. Loading new Cruise Amenities
      for (TmpltImport ti : imports) {
        StringReader sr = new StringReader(ti.getRaw());
        Offer o = (Offer) jaxbUnmarshaller.unmarshal(sr);

        if(o.getEmbarkationdate() == null || o.getEmbarkationdate().size() == 0) {
          Log.err("Cruise missing start date id:" + o.getId());
        }

        for(String date: o.getEmbarkationdate()) {
          CruiseAmenities ca = offerToCruiseAmenities(o, date, modifiedBy.getLegacyId());
          if (ca != null) {
            ca.setRaw(ti.getRaw());
            ca.setHash(ti.getHash());
            ca.setConsortium_id(new Long(feedSrc.getConsortiumId()));
            ca.save();
            ti.setSyncTs(System.currentTimeMillis());
            ti.update();
            ++resSuccess;
          }
          else {
            ti.setSyncTs(null);
            ti.update();
            ++resError;
          }
        }
      }
    } catch (Exception e) {
      Log.err("Failed to process Ensemble imported records", e);
      e.printStackTrace();
    }

    for(String sh: missingShips.keySet()) {
      Log.err("NEW POI Cruises created named:" + sh);
    }

    results.put(PoiFeedUtil.FeedResult.ERROR, resError);
    results.put(PoiFeedUtil.FeedResult.SUCCESS, resSuccess);
    return results;
  }

  private CruiseAmenities offerToCruiseAmenities(Offer o, String date, String userId){
    CruiseAmenities ca = CruiseAmenities.buildCruiseAmenities(o.getTitle(), userId);
    ca.setSrc_reference_id(o.getId());

    //Adding all amenities (can be multiple)
    if(o.getForm() != null) {
      StringBuilder amens = new StringBuilder();
      for(Form f : o.getForm()) {
        if(f.getName().equals("EnsembleExclusives")) {
          amens.append(f.getText());
          amens.append('\n');
        }
      }
      ca.setAmenities(amens.toString());
    }

    ca.setShip_name(o.getShip().getName());

    Map<Long, List<PoiRS>> poiMap = findShipPOI(ca.getShip_name());
    if (!poiMap.keySet().isEmpty()) {
      ca.setPoi_id(poiMap.keySet().iterator().next());
    }
    else {
      PoiRS prs = missingShips.get(o.getShip().getName());
      if(prs == null) {
        prs = createMissnigShip(o, userId);
        missingShips.put(prs.getName(), prs);
      }

      ca.setPoi_id(prs.getId());
      Log.err("No POI found for Ship named: " + ca.getShip_name());
    }

    ca.setCmpy_name(o.getSupplier().getName());
    String startDate = o.getEmbarkationdate().get(0);
    long startTs = Utils.getMilliSecs(startDate);
    ca.setStart_ts(startTs);

    if(NumberUtils.isNumber(o.getNumdays())) {
      Integer numDays = Integer.parseInt(o.getNumdays());
      long endTs = Utils.addDaysToTs(startTs, numDays);
      ca.setEnd_ts(endTs);
    } else {
      Log.err("Cruise missing end date id: " + o.getId());
    }
    return ca;
  }

  public static  Map<Long, List<PoiRS>> findShipPOI(String name) {
    name = name.toLowerCase()
               .replaceAll("^(ss|ms|mv)\\s+","")
               .replace("teresa", "theresa");

    if(name.contains("seabourn")) {
      Log.debug("Seabourn Odyssey is here");
    }
    List<Integer> poiTypes = new ArrayList<>();
    poiTypes.add(PoiTypeInfo.Instance().byName("Cruise").getId());
    Set<Integer> cmpyIds = new HashSet<>();
    cmpyIds.add(0);

    Map<Long, List<PoiRS>> poiMap = PoiMgr.findByTerm(name, poiTypes, cmpyIds, 1);
    return poiMap;
  }


  private static PoiRS createMissnigShip(Offer o, String userId) {
    FeedSrc feedSrc = FeedSrc.findByName(ENSEMBLE_CRUISES_FEED_SRC_NAME);
    int consId = 0; //Making it public for all
    int cmpyId = 0; //Public for all
    int typeId = PoiTypeInfo.Instance().byName("Cruise").getId();

    PoiRS prs = PoiRS.buildRecord(consId, cmpyId, typeId, feedSrc.getSrcId(), userId);
    Ship s = o.getShip();
    prs.setName(s.getName());
    prs.setSrcReference(s.getId());
    if(s.getAmadeusShipCode() != null) {
      Feature f = new Feature();
      f.setName("Amadeus Code");
      f.setDesc(s.getAmadeusShipCode());
      prs.addFeature(f);
    }

    if(s.getSabreShipCode() != null) {
      Feature f = new Feature();
      f.setName("Sabre Code");
      f.setDesc(s.getSabreShipCode());
      prs.addFeature(f);
      prs.setCode(s.getSabreShipCode());
    }

    PoiMgr.save(prs);

    return prs;
  }

}
