package com.umapped.itinerary.analyze.supplier.travelbound;

import com.umapped.itinerary.analyze.webservice.WebServiceRequestHandler;

/**
 * Created by wei on 2017-04-19.
 */
public class TravelBoundWebServiceRequestHandler extends WebServiceRequestHandler {
  private static String SERVER_URL = "https://interface.demo.gta-travel.com/rbstbapi/RequestListenerServlet";

  @Override protected String getServerUrl() {
    return SERVER_URL;
  }
}
