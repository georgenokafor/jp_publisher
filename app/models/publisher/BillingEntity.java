package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.EnumValue;

import javax.persistence.*;

/**
 * Created by surge on 2015-03-26.
 */
@Entity @Table(name = "bil_entity_type")
public class BillingEntity
    extends Model {

  /**
   * Billable entity represent who needs to be billed to for the current account
   * Applicable for each user account but not for a company
   *
   * Created by surge on 2015-03-26.
   */
  public static enum Type {
    /** User's primary company to be billed */
    @EnumValue("COMPANY")
    COMPANY,
    /** User himself is responsible for bills */
    @EnumValue("USER")
    USER,
    /** This account is an assistant account and needs to be billed as to that user */
    @EnumValue("AGENT")
    AGENT
  }

  public static Model.Finder<Type, BillingEntity> find = new Finder(BillingEntity.class);

  @Id
  @Enumerated(value = EnumType.STRING)
  @Column(name = "name", length = 15)
  private Type name;

  public Type getName() {
    return name;
  }

  public void setName(Type name) {
    this.name = name;
  }
}
