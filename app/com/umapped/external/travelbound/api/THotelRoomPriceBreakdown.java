//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.05.08 at 02:53:54 PM EDT 
//


package com.umapped.external.travelbound.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_HotelRoomPriceBreakdown complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_HotelRoomPriceBreakdown">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RoomPrice" type="{}t_BookingPrice" minOccurs="0"/>
 *         &lt;element name="PriceRanges" type="{}t_PriceRanges" minOccurs="0"/>
 *         &lt;element name="Offer" type="{}t_Offer" minOccurs="0"/>
 *         &lt;element name="Confirmation" type="{}t_ItemConfirmation" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{}a_Room"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_HotelRoomPriceBreakdown", propOrder = {
    "roomPrice",
    "priceRanges",
    "offer",
    "confirmation"
})
public class THotelRoomPriceBreakdown {

    @XmlElement(name = "RoomPrice")
    protected TBookingPrice roomPrice;
    @XmlElement(name = "PriceRanges")
    protected TPriceRanges priceRanges;
    @XmlElement(name = "Offer")
    protected TOffer offer;
    @XmlElement(name = "Confirmation")
    protected TItemConfirmation confirmation;
    @XmlAttribute(name = "Code")
    protected String code;
    @XmlAttribute(name = "Id")
    protected String id;
    @XmlAttribute(name = "Description")
    protected String description;
    @XmlAttribute(name = "Quantity")
    protected Integer quantity;
    @XmlAttribute(name = "NumberOfRooms")
    protected Integer numberOfRooms;
    @XmlAttribute(name = "NumberOfCots")
    protected Integer numberOfCots;
    @XmlAttribute(name = "ChildAge")
    protected Integer childAge;
    @XmlAttribute(name = "NumberOfChildren")
    protected Integer numberOfChildren;
    @XmlAttribute(name = "SharingBedding")
    protected Boolean sharingBedding;
    @XmlAttribute(name = "FromAge")
    protected Integer fromAge;
    @XmlAttribute(name = "ToAge")
    protected Integer toAge;

    /**
     * Gets the value of the roomPrice property.
     * 
     * @return
     *     possible object is
     *     {@link TBookingPrice }
     *     
     */
    public TBookingPrice getRoomPrice() {
        return roomPrice;
    }

    /**
     * Sets the value of the roomPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link TBookingPrice }
     *     
     */
    public void setRoomPrice(TBookingPrice value) {
        this.roomPrice = value;
    }

    /**
     * Gets the value of the priceRanges property.
     * 
     * @return
     *     possible object is
     *     {@link TPriceRanges }
     *     
     */
    public TPriceRanges getPriceRanges() {
        return priceRanges;
    }

    /**
     * Sets the value of the priceRanges property.
     * 
     * @param value
     *     allowed object is
     *     {@link TPriceRanges }
     *     
     */
    public void setPriceRanges(TPriceRanges value) {
        this.priceRanges = value;
    }

    /**
     * Gets the value of the offer property.
     * 
     * @return
     *     possible object is
     *     {@link TOffer }
     *     
     */
    public TOffer getOffer() {
        return offer;
    }

    /**
     * Sets the value of the offer property.
     * 
     * @param value
     *     allowed object is
     *     {@link TOffer }
     *     
     */
    public void setOffer(TOffer value) {
        this.offer = value;
    }

    /**
     * Gets the value of the confirmation property.
     * 
     * @return
     *     possible object is
     *     {@link TItemConfirmation }
     *     
     */
    public TItemConfirmation getConfirmation() {
        return confirmation;
    }

    /**
     * Sets the value of the confirmation property.
     * 
     * @param value
     *     allowed object is
     *     {@link TItemConfirmation }
     *     
     */
    public void setConfirmation(TItemConfirmation value) {
        this.confirmation = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQuantity(Integer value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the numberOfRooms property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfRooms() {
        return numberOfRooms;
    }

    /**
     * Sets the value of the numberOfRooms property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfRooms(Integer value) {
        this.numberOfRooms = value;
    }

    /**
     * Gets the value of the numberOfCots property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfCots() {
        return numberOfCots;
    }

    /**
     * Sets the value of the numberOfCots property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfCots(Integer value) {
        this.numberOfCots = value;
    }

    /**
     * Gets the value of the childAge property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChildAge() {
        return childAge;
    }

    /**
     * Sets the value of the childAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChildAge(Integer value) {
        this.childAge = value;
    }

    /**
     * Gets the value of the numberOfChildren property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfChildren() {
        return numberOfChildren;
    }

    /**
     * Sets the value of the numberOfChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfChildren(Integer value) {
        this.numberOfChildren = value;
    }

    /**
     * Gets the value of the sharingBedding property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSharingBedding() {
        return sharingBedding;
    }

    /**
     * Sets the value of the sharingBedding property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSharingBedding(Boolean value) {
        this.sharingBedding = value;
    }

    /**
     * Gets the value of the fromAge property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFromAge() {
        return fromAge;
    }

    /**
     * Sets the value of the fromAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFromAge(Integer value) {
        this.fromAge = value;
    }

    /**
     * Gets the value of the toAge property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getToAge() {
        return toAge;
    }

    /**
     * Sets the value of the toAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setToAge(Integer value) {
        this.toAge = value;
    }

}
