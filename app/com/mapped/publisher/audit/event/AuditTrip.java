package com.mapped.publisher.audit.event;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;

/**
 * Created by Serguei Moutovkin on 2014-05-21.
 */
@JsonTypeName("TRIP")
public class AuditTrip
    extends AuditEvent {
  public String tripId;
  public String name;
  public TripStatus status;
  public TripAction currentAction = TripAction.MODIFIED;

  public AuditTripGroup group = null;

  public enum TripStatus {
    /**
     * Pending status is given to a trip that was generated via automated method like auto e-mail parsing.
     */
    PENDING,
    /**
     * Published Pending is given when the trip is just created, i.e. new trip
     *
     * @note As of 2014-05-21 synonym for PUBLISHED_REVIEW
     */
    PUBLISH_PENDING,
    /**
     * Published Review is assigned when trip submitted for traveller review
     *
     * @note As of 2014-05-21 synonym for PUBLISHED_PENDING
     */
    PUBLISHED_REVIEW,
    /**
     * Published for "middle users", i.e. Mobile Sync Users
     */
    PUBLISHED,
    /**
     * Deleted is assigned when trip is manually deleted by the actor
     */
    DELETED,
    /**
     * Active is a synonym for PUBLISHED (used in Tour Controller)
     *
     * @deprecated
     */
    ACTIVE;
  }

  /**
   * Specific trip action to guide user about what happened to the trip
   */
  public enum TripAction {
    /** When new trip is created */
    NEW,
    /** When trip dates or comments change */
    MODIFIED,
    /** Sent for review */
    SENT_FOR_REVIEW,
    /** When trip is published for the first time */
    PUBLISHED,
    /** When trip is published again */
    REPUBLISHED
  }

  public AuditTrip(){
    group = new AuditTripGroup();
    group.name = "Default";
    group.groupId = "";
  }

  public AuditTrip withId(String id) {
    this.tripId = id;
    return this;
  }

  public AuditTrip withName(String name) {
    this.name = name;
    return this;
  }

  public AuditTrip withStatus(TripStatus status) {
    this.status = status;
    return this;
  }

  public AuditTrip withAction(TripAction action) {
    this.currentAction = action;
    return this;
  }

  public AuditTrip withGroupName(String groupName){
    this.group.name = groupName;
    return this;
  }

  public AuditTrip withGroupId(String groupId) {
    this.group.groupId = groupId;
    return this;
  }

  /**
   * Helper mapping function to Audit Trip Types
   * @param appConstStatus legacy trip status code
   */
  public AuditTrip withStatus(final int appConstStatus) {
    switch (appConstStatus) {
      case APPConstants.STATUS_PUBLISHED:
        status = TripStatus.PUBLISHED;
        break;

      case APPConstants.STATUS_PUBLISH_PENDING:
        status = TripStatus.PUBLISH_PENDING;
        break;

      /* As of 2014-05-21 APPConstants.STATUS_PUBLISH_PENDING === APPConstants.STATUS_PUBLISHED_REVIEW
      case APPConstants.STATUS_PUBLISHED_REVIEW:
        status = TripStatus.PUBLISHED_REVIEW;
        break;
      */
      /* As of 2014-05-21 APPConstants.STATUS_ACTIVE === APPConstants.STATUS_PUBLISHED
      case APPConstants.STATUS_ACTIVE:
        status = TripStatus.ACTIVE;
        break;
      */

      case APPConstants.STATUS_DELETED:
        status = TripStatus.DELETED;
        break;

      /* As of 2014-05-21 APPConstants.STATUS_PENDING === APPConstants.STATUS_PUBLISHED_REVIEW
      case APPConstants.STATUS_PENDING:
        status = TripStatus.PENDING;
        break
      */

      default:
        status = TripStatus.PUBLISH_PENDING;
        Log.log(LogLevel.ERROR, "Unknown trip status Id:" + appConstStatus);
    }
    return this;
  }

}
