package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 12-08-24
 * Time: 9:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class MapCommentsExtMgr extends  MapCommentsMgr  {

private static final String SELECT_ALL_BY_OWNER_SINCE_LAST_UPDATE= "Select Pk, MapsId, CreatedBy, Msg, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, Type from MapComments where CreatedBy = ? and (SyncTimestamp = 0 or SyncTimestamp > ?);";
private static final String SELECT_SHARED_BY_USER_SINCE_LAST_UPDATE= "Select Pk, MapsId, CreatedBy, Msg, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, Type from MapComments where (MapsId in (select mapsid from sharedmaps where shareuserid = ? and status = 0) or MapsId in (select mapsid from sharedmaps where owner = ? and status = 0) ) and (SyncTimestamp = 0 or SyncTimestamp > ?);";
private static final String SELECT_NEW_SHARED_BY_USER_SINCE_LAST_UPDATE= "Select Pk, MapsId, CreatedBy, Msg, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, Type from MapComments where MapsId in (select mapsid from sharedmaps where shareuserid = ? and status = 0 and (SyncTimestamp = 0 or SyncTimestamp > ?) and createtimestamp = lastupdatetimestamp);";

private static final String SELECT_COMMENTS_SHARED_BY_USER_SINCE_LAST_UPDATE= "Select Pk, MapsId, CreatedBy, Msg, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, Type from MapComments where (MapsId in (select mapsid from sharedmaps where shareuserid = ? and status = 0) or MapsId in (select mapsid from sharedmaps where owner = ? and status = 0) ) and (SyncTimestamp = 0 or SyncTimestamp > ?) and type like 'USER';";
private static final String SELECT_COMMENTS_ALL_BY_OWNER_SINCE_LAST_UPDATE= "Select Pk, MapsId, CreatedBy, Msg, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, Type from MapComments where CreatedBy = ? and (SyncTimestamp = 0 or SyncTimestamp > ?) and type like 'USER';";
private static final String SELECT_SYSTEM_COMMENTS_ALL_BY_MAPID_SINCE_LAST_UPDATE= "Select Pk, MapsId, CreatedBy, Msg, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, Type from MapComments where mapsId = ? and (SyncTimestamp = 0 or SyncTimestamp > ?) and type like 'SYSTEM';";

    public static ArrayList<MapComments> getSytemUpdatesByMapsIdSinceLastUpdate(String mapsId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_SYSTEM_COMMENTS_ALL_BY_MAPID_SINCE_LAST_UPDATE);
            prep.setString(1, mapsId);
            prep.setLong(2,lastUpdate);
            rs = prep.executeQuery();
            return MapCommentsMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }


    public static ArrayList<MapComments> getAllByOwnerSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
            ResultSet rs = null;
            PreparedStatement prep= null;
            Connection conn= null;
            Statement stat= null;
            try {
                conn = DBConnectionMgr.getConnection4Mobile();
                stat = conn.createStatement();
                prep = conn.prepareStatement(SELECT_ALL_BY_OWNER_SINCE_LAST_UPDATE);
                prep.setString(1, userId);
                prep.setLong(2,lastUpdate);
                rs = prep.executeQuery();
                return MapCommentsMgr.handleResults(rs);
            } finally {
                if (prep != null)
                    prep.close();
                if (stat != null)
                    stat.close();
                if (rs != null) {
                    rs.close();
                }
                DBConnectionMgr.closeConnection(conn);
            }
        }

        public static ArrayList<MapComments> getSharedByUserSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
            ResultSet rs = null;
            PreparedStatement prep= null;
            Connection conn= null;
            Statement stat= null;
            try {
                conn = DBConnectionMgr.getConnection4Mobile();
                stat = conn.createStatement();
                prep = conn.prepareStatement(SELECT_SHARED_BY_USER_SINCE_LAST_UPDATE);
                prep.setString(1, userId);
                prep.setString(2, userId);
                prep.setLong(3,lastUpdate);
                rs = prep.executeQuery();
                return MapCommentsMgr.handleResults(rs);
            } finally {
                if (prep != null)
                    prep.close();
                if (stat != null)
                    stat.close();
                if (rs != null) {
                    rs.close();
                }
                DBConnectionMgr.closeConnection(conn);
            }
        }

        public static ArrayList<MapComments> getNewSharedByUserSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
            ResultSet rs = null;
            PreparedStatement prep= null;
            Connection conn= null;
            Statement stat= null;
            try {
                conn = DBConnectionMgr.getConnection4Mobile();
                stat = conn.createStatement();
                prep = conn.prepareStatement(SELECT_NEW_SHARED_BY_USER_SINCE_LAST_UPDATE);
                prep.setString(1, userId);
                prep.setLong(2,lastUpdate);
                rs = prep.executeQuery();
                return MapCommentsMgr.handleResults(rs);
            } finally {
                if (prep != null)
                    prep.close();
                if (stat != null)
                    stat.close();
                if (rs != null) {
                    rs.close();
                }
                DBConnectionMgr.closeConnection(conn);
            }
        }

    public static ArrayList<MapComments> getCommentsSharedByUserSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_COMMENTS_SHARED_BY_USER_SINCE_LAST_UPDATE);
            prep.setString(1, userId);
            prep.setString(2, userId);
            prep.setLong(3,lastUpdate);
            rs = prep.executeQuery();
            return MapCommentsMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList<MapComments> getCommentsByOwnerSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_COMMENTS_ALL_BY_OWNER_SINCE_LAST_UPDATE);
            prep.setString(1, userId);
            prep.setLong(2,lastUpdate);
            rs = prep.executeQuery();
            return MapCommentsMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

}
