package models.publisher;

import com.avaje.ebean.BeanState;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.persistence.IUmappedModel;
import com.mapped.publisher.persistence.IUmappedModelWithImage;
import com.mapped.publisher.persistence.PageAttachType;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-01
 * Time: 11:28 AM
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "destination_guide_attach")
public class DestinationGuideAttach
    extends UmappedModelWithImage implements IUmappedModel<String, DestinationGuideAttach> {

  public static Model.Finder<String, DestinationGuideAttach> find = new Finder(DestinationGuideAttach.class);
  public String tag;
  public String comments;
  public String parentid;
  public int    height;
  public int    width;
  @Id
  public String fileid;
  @Constraints.Required
  public String destinationguideid;
  public String name;
  public int    rank;
  public int    status;
  public Long   createdtimestamp;
  public Long   lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;

  String         attachname;
  String         attachurl;
  PageAttachType attachtype;
  @ManyToOne
  @JoinColumn(name = "image_pk", nullable = true, referencedColumnName = "pk")
  FileImage  image;
  @ManyToOne
  @JoinColumn(name = "file_pk", nullable = true, referencedColumnName = "pk")
  FileInfo  file;

  /**
   * Takes this object makes it possible to re-insert as new record
   * @return
   */
  public DestinationGuideAttach prepareForReuse(String createdBy, String modifiedBy) {
    BeanState state = Ebean.getBeanState(this);
    state.resetForInsert();
    setFileid(DBConnectionMgr.getUniqueId());
    setModifiedby(modifiedBy);
    setCreatedby(createdBy);
    setLastupdatedtimestamp(System.currentTimeMillis());
    setCreatedtimestamp(lastupdatedtimestamp);
    return this;
  }

  public static DestinationGuideAttach buildGuideAttachment(DestinationGuide dg, String userId) {
    return buildGuideAttachment(dg.getDestinationguideid(), userId);
  }

  public static DestinationGuideAttach buildGuideAttachment(String guideId, String userId) {
    DestinationGuideAttach dga = new DestinationGuideAttach();
    dga.setFileid(DBConnectionMgr.getUniqueId());
    dga.setDestinationguideid(guideId);
    dga.setCreatedtimestamp(System.currentTimeMillis());
    dga.setLastupdatedtimestamp(dga.createdtimestamp);
    dga.setCreatedby(userId);
    dga.setModifiedby(userId);
    dga.setStatus(APPConstants.STATUS_ACTIVE);
    return dga;
  }

  public static List<DestinationGuideAttach> findActiveByDestGuide(String destinationguideid) {
    return find.where()
               .eq("destinationguideid", destinationguideid)
               .eq("status", 0)
               .orderBy("attachType,rank asc")
               .findList();
  }

  public static List<DestinationGuideAttach> getCover(String destinationguideid) {
    return find.where()
               .eq("destinationguideid", destinationguideid)
               .eq("status", 0)
               .eq("attachtype", "3")
               .orderBy("rank asc")
               .setMaxRows(1)
               .findList();
  }

  public static List<DestinationGuideAttach> getAllPhotosByDestId(String destinationid) {
    String sql = "destinationguideid in (select t1.destinationguideid from destination_guide t1 where t1.status = 0 "
                 + "and t1.destinationid = '" + destinationid + "')";

    return find.where().raw(sql).eq("status", 0).eq("attachtype", "3").orderBy("rank asc").findList();
  }

  public static int maxPageRankByDestGuideId(String destinationGuideid, String attachtype) {
    return find.where().eq("destinationguideid", destinationGuideid).eq("attachtype", attachtype).findCount();
  }

  public static int activeCountByDestGuideId(String destinationGuideid, String attachtype) {
    return find.where()
               .eq("destinationguideid", destinationGuideid)
               .eq("attachtype", attachtype)
               .eq("status", 0)
               .findCount();
  }

  public static List<DestinationGuideAttach> findActivePhotos() {
    return find.where().eq("attachtype", "3").eq("status", 0).findList();
  }

  /**
   * Creates duplicate of this record attached to a different "Destination/Document"
   *
   * @return
   */
  public DestinationGuideAttach duplicate(String guideId, String modifiedBy) {
    DestinationGuideAttach newGuideFile = new DestinationGuideAttach();

    newGuideFile.setAttachname(this.getAttachname());
    newGuideFile.setAttachtype(this.getAttachtype());
    newGuideFile.setAttachurl(this.getAttachurl());
    newGuideFile.setComments(this.comments);
    newGuideFile.setHeight(this.height);
    newGuideFile.setName(this.name);
    newGuideFile.setParentid(this.parentid);
    newGuideFile.setRank(this.rank);
    newGuideFile.setTag(this.tag);
    newGuideFile.setWidth(this.width);

    newGuideFile.setFileid(DBConnectionMgr.getUniqueId());
    newGuideFile.setDestinationguideid(guideId);
    newGuideFile.setModifiedby(modifiedBy);
    newGuideFile.setCreatedby(modifiedBy);
    newGuideFile.setStatus(APPConstants.STATUS_ACTIVE);
    newGuideFile.setCreatedtimestamp(System.currentTimeMillis());
    newGuideFile.setLastupdatedtimestamp(newGuideFile.createdtimestamp);
    newGuideFile.save();
    return newGuideFile;
  }

  public String getAttachname() {
    return attachname;
  }

  public DestinationGuideAttach setAttachname(String attachname) {
    this.attachname = attachname;
    return this;
  }

  public DestinationGuideAttach setAttachtypeFromIntString(final String intStr) {
    setAttachtype(PageAttachType.fromIntString(intStr));
    return this;
  }

  public DestinationGuideAttach setAttachtype(PageAttachType attachtype) {
    this.attachtype = attachtype;
    return this;
  }

  public PageAttachType getAttachtype() {
    return attachtype;
  }

  public String getAttachurl() {
    return attachurl;
  }

  public DestinationGuideAttach setAttachurl(String attachurl) {
    this.attachurl = attachurl;
    return this;
  }

  public String getAttachtypeName() {
    return attachtype.name();
  }

  public String getAttachtypeStr() {
    return attachtype.getStrVal();
  }


  public FileImage getImage() {
    return image;
  }

  public FileInfo getFile() {
    return file;
  }

  public DestinationGuideAttach setImage(FileImage image) {
    this.image = image;
    return this;
  }

  public DestinationGuideAttach setFile(FileInfo file) {
    this.file = file;
    return this;
  }

  public int getVersion() {
    return version;
  }

  public DestinationGuideAttach setVersion(int version) {
    this.version = version;
    return this;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public String getComments() {
    return comments;
  }

  public DestinationGuideAttach setComments(String comments) {
    this.comments = comments;
    return this;
  }

  public String getParentid() {
    return parentid;
  }

  public DestinationGuideAttach setParentid(String parentid) {
    this.parentid = parentid;
    return this;
  }

  public int getHeight() {
    return height;
  }

  public DestinationGuideAttach setHeight(int height) {
    this.height = height;
    return this;
  }

  public int getWidth() {
    return width;
  }

  public DestinationGuideAttach setWidth(int width) {
    this.width = width;
    return this;
  }

  public String getFileid() {
    return fileid;
  }

  public DestinationGuideAttach setFileid(String fileid) {
    this.fileid = fileid;
    return this;
  }

  public String getDestinationguideid() {
    return destinationguideid;
  }

  public DestinationGuideAttach setDestinationguideid(String destinationguideid) {
    this.destinationguideid = destinationguideid;
    return this;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getRank() {
    return rank;
  }

  public DestinationGuideAttach setRank(int rank) {
    this.rank = rank;
    return this;
  }

  public int getStatus() {
    return status;
  }

  public DestinationGuideAttach setStatus(int status) {
    this.status = status;
    return this;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public DestinationGuideAttach setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
    return this;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public DestinationGuideAttach setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
    return this;
  }

  public String getCreatedby() {
    return createdby;
  }

  public DestinationGuideAttach setCreatedby(String createdby) {
    this.createdby = createdby;
    return this;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public DestinationGuideAttach setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
    return this;
  }

  @Override
  public String getImageUrl() {
    if(getFileImage() == null) {
      return getAttachurl();
    } else {
      return getFileImage().getUrl();
    }
  }

  @Override
  public FileImage getFileImage() {
    return getImage();
  }

  public FileInfo getFileInfo() {
    return getFile();
  }

  @Override
  public IUmappedModelWithImage setFileImage(FileImage image) {
    setImage(image);

    if(image != null) {
      setName(image.getFilename());
      setAttachname(image.getFilename());
      setAttachtype(PageAttachType.PHOTO_LINK);
      setAttachurl(image.getUrl());
    }
    return this;
  }

  public DestinationGuideAttach setFileInfo(FileInfo file) {
    setFile(file);

    if(file != null) {
      if (this.getName() == null || this.getName().isEmpty()) {
        setName(file.getFilename());
      }
      if (this.getAttachname() == null || this.getAttachname().isEmpty()) {
        setAttachname(file.getFilename());
      }
      setAttachtype(PageAttachType.FILE_LINK);
      setAttachurl(file.getUrl());
    }
    return this;
  }

  @Override
  public DestinationGuideAttach getByPk(String pk) {
    return find.byId(pk);
  }

  @Override
  public String getPk() {
    return getFileid();
  }

  @Override
  public String getPkAsString() {
    return getPk().toString();
  }

  @Override
  public Long getCreatedByPk() {
    return Account.findByLegacyId(getCreatedby()).getUid();
  }

  @Override
  public Long getModifiedByPk() {
    return Account.findByLegacyId(getModifiedby()).getUid();
  }

  @Override
  public String getCreatedByLegacy() {
    return getCreatedby();
  }

  @Override
  public String getModifiedByLegacy() {
    return getModifiedby();
  }

  @Override
  public Long getCreatedEpoch() {
    return getCreatedtimestamp();
  }

  @Override
  public Timestamp getCreatedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getCreatedtimestamp()));
  }

  @Override
  public Long getModifiedEpoch() {
    return getLastupdatedtimestamp();
  }

  @Override
  public Timestamp getModifiedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getLastupdatedtimestamp()));
  }
}
