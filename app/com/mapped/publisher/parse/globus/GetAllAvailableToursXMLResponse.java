
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://webapi.globusandcosmos.com/AvailableTours.xsd}GetAllAvailableToursXMLResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllAvailableToursXMLResult"
})
@XmlRootElement(name = "GetAllAvailableToursXMLResponse")
public class GetAllAvailableToursXMLResponse {

    @XmlElement(name = "GetAllAvailableToursXMLResult", namespace = "http://webapi.globusandcosmos.com/AvailableTours.xsd", required = true, nillable = true)
    protected AvailableTours getAllAvailableToursXMLResult;

    /**
     * Gets the value of the getAllAvailableToursXMLResult property.
     * 
     * @return
     *     possible object is
     *     {@link AvailableTours }
     *     
     */
    public AvailableTours getGetAllAvailableToursXMLResult() {
        return getAllAvailableToursXMLResult;
    }

    /**
     * Sets the value of the getAllAvailableToursXMLResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AvailableTours }
     *     
     */
    public void setGetAllAvailableToursXMLResult(AvailableTours value) {
        this.getAllAvailableToursXMLResult = value;
    }

}
