package com.umapped.external.wetu.pins.list;


import java.util.ArrayList;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by george on 2016-06-07.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "features",
    "rooms",
    "units",
    "activities",
    "spa_options",
    "restaurants",
    "map_object_id",
    "name",
    "last_modified",
    "type",
    "category",
    "position",
    "content",
    "documentation",
    "specials"
})
public class Accommodation extends Pin {
  @JsonProperty("features")
  private Features_Accommodation features;
  @JsonProperty("rooms")
  private ArrayList<Room> rooms;
  @JsonProperty("units")
  private ArrayList<Unit> units;
  @JsonProperty("activities")
  private ArrayList<SimpleItem> activities;
  @JsonProperty("spa_options")
  private ArrayList<SimpleItem> spaOptions;
  @JsonProperty("restaurants")
  private ArrayList<SimpleItem> restaurants;


  @JsonProperty("features")
  public Features_Accommodation getFeatures() {
    return features;
  }

  @JsonProperty("features")
  public void setFeatures(Features_Accommodation features) {
    this.features = features;
  }

  @JsonProperty("rooms")
  public ArrayList<Room> getRooms() {
    return rooms;
  }

  @JsonProperty("rooms")
  public void setRooms(ArrayList<Room> rooms) {
    this.rooms = rooms;
  }

  @JsonProperty("units")
  public ArrayList<Unit> getUnits() {
    return units;
  }

  @JsonProperty("units")
  public void setUnits(ArrayList<Unit> units) {
    this.units = units;
  }

  @JsonProperty("activities")
  public ArrayList<SimpleItem> getActivities() {
    return activities;
  }

  @JsonProperty("activities")
  public void setActivities(ArrayList<SimpleItem> activities) {
    this.activities = activities;
  }

  @JsonProperty("spa_options")
  public ArrayList<SimpleItem> getSpaOptions() {
    return spaOptions;
  }

  @JsonProperty("spa_options")
  public void setSpaOptions(ArrayList<SimpleItem> spaOptions) {
    this.spaOptions = spaOptions;
  }

  @JsonProperty("restaurants")
  public ArrayList<SimpleItem> getRestaurants() {
    return restaurants;
  }

  @JsonProperty("restaurants")
  public void setRestaurants(ArrayList<SimpleItem> restaurants) {
    this.restaurants = restaurants;
  }
}
