package com.umapped.persistence.notes.insurance;

import com.mapped.publisher.form.TripNoteForm;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.umapped.persistence.notes.StructuredNote;
import com.umapped.persistence.reservation.UmDateTime;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;
import com.umapped.persistence.reservation.UmTraveler;
import models.publisher.TripNote;


import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.umapped.persistence.notes.utils.StructuredNoteUtils.cast;

/**
 * Created by george on 2017-03-21.
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class InsuranceNote extends StructuredNote {
  private String policyNumber;
  private String emergencyNumber;
  private String importantInformation;
  private String policyType;
  private String insuranceProvider;
  private String policyDescription;
  private UmDateTime startDateTime;
  private UmDateTime endDateTime;

  public String getPolicyNumber() {
    return policyNumber;
  }

  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }

  public String getEmergencyNumber() {
    return emergencyNumber;
  }

  public void setEmergencyNumber(String emergencyNumber) {
    this.emergencyNumber = emergencyNumber;
  }

  public String getImportantInformation() {
    return importantInformation;
  }

  public void setImportantInformation(String importantInformation) {
    this.importantInformation = importantInformation;
  }

  public String getPolicyType() {
    return policyType;
  }

  public void setPolicyType(String policyType) {
    this.policyType = policyType;
  }

  public String getInsuranceProvider() {
    return insuranceProvider;
  }

  public void setInsuranceProvider(String insuranceProvider) {
    this.insuranceProvider = insuranceProvider;
  }

  public String getPolicyDescription() {
    return policyDescription;
  }

  public void setPolicyDescription(String policyDescription) {
    this.policyDescription = policyDescription;
  }

  public UmDateTime getStartDateTime() {
    return startDateTime;
  }

  @JsonProperty("startDateTime")
  public InsuranceNote setStartDateTime(UmDateTime startDateTime) {
    this.startDateTime = startDateTime;
    return this;
  }

  @JsonIgnore
  public InsuranceNote setStartDateTime(Long ts) {
    return setStartDateTime(timestampToDateTime(ts));
  }


  public UmDateTime getEndDateTime() {
    return endDateTime;
  }

  @JsonProperty("endDateTime")
  public InsuranceNote setEndDateTime(UmDateTime endDateTime) {
    this.endDateTime = endDateTime;
    return this;
  }

  @JsonIgnore
  public InsuranceNote setEndDateTime(Long ts) {
    return setEndDateTime(timestampToDateTime(ts));
  }

  private UmDateTime timestampToDateTime(Long ts) {
    if (ts == null) {
      return null;
    }
    Instant i   = Instant.ofEpochMilli(ts);
    LocalDateTime ldt = LocalDateTime.ofInstant(i, ZoneId.of("UTC"));
    UmDateTime      dt  = new UmDateTime(ldt);
    return dt;
  }

  //Overriden method from SuperClass StructuredNote. Called by renderByNoteType()
  //Used to create intro to be displayed on itinerary view page etc
  @Override
  protected String renderIntro(StructuredNote note) {
    InsuranceNote insuranceNote = cast(note, InsuranceNote.class);
    InsuranceNoteView view = new InsuranceNoteView();
    if (insuranceNote != null) {
      view.provider = insuranceNote.getInsuranceProvider();
      view.policyNum = insuranceNote.getPolicyNumber();
      view.policyType = insuranceNote.getPolicyType();
      view.policyDesc = insuranceNote.getPolicyDescription();
      view.emergencyNum = insuranceNote.getEmergencyNumber();
      view.importantInfo = insuranceNote.getImportantInformation();

      view.passengers = new ArrayList<>();

      if (insuranceNote.getTravelers() != null) {

        for (UmTraveler t : insuranceNote.getTravelers()) {
          InsuranceTraveler it = cast(t, InsuranceTraveler.class);
          if (it != null) {
            NoteBaseView.PassengerInfo passenger = new NoteBaseView.PassengerInfo();
            view.passengers.add(passenger);
            passenger.firstName = it.getGivenName();
            passenger.lastName = it.getFamilyName();
            passenger.name = it.getName();
            passenger.startDate = Utils.formatDateTimePagePrint(it.getStartDate());
            passenger.endDate = Utils.formatDateTimePagePrint(it.getEndDate());

          }
        }
      }
    }
    return views.html.trip.bookings.details.insuranceIntro.render(view).toString();
  }

  //Overriden method from SuperClass StructuredNote. Called by populateByNoteType()
  //Used to create Note object to be saved, for respective note type
  protected InsuranceNote populateNote(TripNote tripNote, TripNoteForm tripNoteInfo) {
    InsuranceNote note = cast(tripNote.getNote(), InsuranceNote.class);
    if (note == null) {
      note = new InsuranceNote();
    }

    StringBuilder noteTitle = new StringBuilder();

    noteTitle.append("Insurance: ");
    noteTitle.append(tripNoteInfo.inProvider);


    note.name = noteTitle.toString();
    note.setInsuranceProvider(tripNoteInfo.inProvider);
    note.setPolicyNumber(tripNoteInfo.inPolicyNum);
    note.setPolicyType(tripNoteInfo.inPolicyType);
    note.setPolicyDescription(tripNoteInfo.inPolicyDesc);
    note.setEmergencyNumber(tripNoteInfo.inEmergencyNum);
    note.setImportantInformation(tripNoteInfo.inImportantInfo);

    //check if we need to link to a booking

    List<UmTraveler> travelers = new ArrayList<>();
    if (tripNoteInfo.passengers != null) {
      tripNoteInfo.passengers.stream()
          .forEach((p) -> {
            if (!p.isEmpty()) {
              InsuranceTraveler t = new InsuranceTraveler();
              t.setFamilyName(StringUtils.trim(p.lastName));
              t.setGivenName(StringUtils.trim(p.firstName));
              t.setStartDate(StringUtils.trim(p.startDate));
              t.setEndDate(StringUtils.trim(p.endDate));
              travelers.add(t);
            }
          });
    }
    note.setTravelers(travelers);
    //return views.html.trip.bookings.details.insuranceIntro.render(view).toString();
    return note;
  }
}
