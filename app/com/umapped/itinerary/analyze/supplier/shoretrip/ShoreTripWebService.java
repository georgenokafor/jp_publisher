package com.umapped.itinerary.analyze.supplier.shoretrip;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.umapped.itinerary.analyze.recommendation.RecommendedItemType;
import com.umapped.itinerary.analyze.webservice.HttpResponseException;
import modules.ItineraryAnalyzeModule;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.recommendation.RecommendedItem;
import com.umapped.itinerary.analyze.supplier.Supplier;
import com.umapped.itinerary.analyze.supplier.shoretrip.jaxb.IntegrationResponse;
import com.umapped.itinerary.analyze.supplier.shoretrip.jaxb.ObjectFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by wei on 2017-04-05.
 */
@Singleton
public class ShoreTripWebService implements Constants {
  private static final Logger LOG = LoggerFactory.getLogger(ShoreTripWebService.class);

  private PortLookup portLookup;
  private ShoreTripWebServiceRequestHandler requestHandler;

  @Inject
  public ShoreTripWebService(PortLookup portLookup) {
    this.portLookup = portLookup;
    this.requestHandler = new ShoreTripWebServiceRequestHandler();
  }

  public List<RecommendedItem> getRecommnededActivity(StayPeriod period, Location location) {
    if (location.getGeoLocation() != null) {
      String port = portLookup.getPort(location.getGeoLocation());
      if (StringUtils.isNotEmpty(port)) {
        ShoreTripAPIRequest request =  new ShoreTripAPIRequest();
        try {
          return requestHandler.get(GET_EXCURSIONS + port, request, ShoreTripWebService::getTrips);
        } catch (HttpResponseException e) {
          throw new RuntimeException(e);
        }
      }
    }
    return Collections.emptyList();
  }

  public static List<RecommendedItem> getTrips(InputStream inputStream) {

    List<RecommendedItem> excursionList = new ArrayList<>();
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      IntegrationResponse integrationResponse = (IntegrationResponse) unmarshaller.unmarshal(inputStream);
      if (integrationResponse.getExcursions() != null) {
        if (integrationResponse.getExcursions().getRegions() != null) {
          if (integrationResponse.getExcursions().getRegions().getRegion() != null) {
            if (integrationResponse.getExcursions().getRegions().getRegion().getPorts() != null) {
              if (integrationResponse.getExcursions().getRegions().getRegion().getPorts().getLocation() != null) {
                List<IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup> tripGroupList = integrationResponse
                    .getExcursions()
                    .getRegions()
                    .getRegion()
                    .getPorts()
                    .getLocation()
                    .getTripGroups()
                    .getTripGroup();
                if (tripGroupList != null) {
                  for (IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup tripGroup : tripGroupList) {
                    if (tripGroup.getTrips() != null && tripGroup.getTrips().getTrip() != null && tripGroup.getTrips()
                                                                                                           .getTrip()
                                                                                                           .size() > 0) {
                      for (IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip trip : tripGroup
                          .getTrips()
                          .getTrip()) {
                        RecommendedItem excursion = new RecommendedItem();
                        excursion.supplier = Supplier.ShoreTrip;
                        excursion.type = RecommendedItemType.Activity;
                        excursion.name = trip.getName();
                        excursion.bookingUrl = trip.getUrl();
                        excursion.description = (trip.getNarrative());
                        excursion.imageUrl = trip.getImageUrl();
                        excursion.id = trip.getCode() == null ? "" : trip.getCode().toString();
                        excursion.prices = extractPrices(trip.getPrices());
                        excursionList.add(excursion);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return excursionList;
    } catch (JAXBException e) {
      LOG.error("Fail to parse the shore trip response", e);
      return Collections.emptyList();
    }
  }

  private static List<RecommendedItem.Price> extractPrices(IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip.Prices prices) {
    List<RecommendedItem.Price> mappedPrices = null;
    if (prices != null && prices.getTripPrice() != null) {
      mappedPrices = new ArrayList<>();
      for (IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup.Trips.Trip.Prices.TripPrice p : prices.getTripPrice()) {
        String priceDescription = p.getName();
        if (StringUtils.isNotEmpty(p.getDescription())) {
          priceDescription += "(" + p.getDescription() + ")";
        }
        mappedPrices.add(new RecommendedItem.Price(String.format("%.2f", p.getAmount()), "", priceDescription));
      }
    }
    return mappedPrices;
  }

  public static void main(String[] args) {
    Injector container = Guice.createInjector(new ItineraryAnalyzeModule());

    ShoreTripWebService service = container.getInstance(ShoreTripWebService.class);

    LocalDate start = LocalDate.now().plusDays(10);
    LocalDate end = start.plusDays(10);

    StayPeriod p = new StayPeriod(start, end);

    Location location = new Location();
    location.setGeoLocation(new Location.GeoLocation(51.4775, -0.461389));

    System.out.println(service.getRecommnededActivity(p, location));

  }
}
