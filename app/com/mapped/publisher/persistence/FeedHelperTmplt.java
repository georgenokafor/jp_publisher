package com.mapped.publisher.persistence;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.utils.Log;
import controllers.TemplateController;
import models.publisher.Account;
import models.publisher.FeedSrc;
import models.publisher.Template;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by surge on 2015-10-01.
 */
public abstract class FeedHelperTmplt {

  FeedSrc feedSrc;
  Account modifiedBy;

  public static FeedHelperTmplt build(Account a, int srcId) {
    FeedSrc src = FeedSrc.find.byId(srcId);
    return build(a, src);
  }

  public static FeedHelperTmplt build(Account a, FeedSrc src) {

    FeedHelperTmplt helper = null;
    try {
      helper = (FeedHelperTmplt) Class.forName(src.getLoaderClass()).newInstance();
      helper.feedSrc = src;
      helper.modifiedBy = a;
    }
    catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
      Log.err("FeedHelperTmplt: Can't instantiate class: " + src.getLoaderClass(), e);
    }

    return helper;
  }

  public FeedSrc getFeedSrc() {
    return feedSrc;
  }

  public FeedHelperTmplt setModifiedBy(Account modifiedBy) {
    this.modifiedBy = modifiedBy;
    return this;
  }

  /**
   * Function that runs all events in appropriate sequence for the feed to allow scheduled updates
   *
   * @return
   */
  abstract public Map<PoiFeedUtil.FeedResult, Integer> all();

  /**
   * Step 1: Delete all records for the feed (usually) before loading new records
   *
   * @return
   */
  abstract public Map<PoiFeedUtil.FeedResult, Integer> wipe();

  /**
   * Step 2: Load feed from remote location
   *
   * @return
   */
  abstract public Map<PoiFeedUtil.FeedResult, Integer> load();

  /**
   * Step 3: Convert feed into templates
   *
   * @return
   */
  abstract public Map<PoiFeedUtil.FeedResult, Integer> process();

  /**
   * Step 4: Mark templates that are not in the feed as deleted (soft delete)
   *
   * @param delMode SOFT or HARD delete obsolete template records
   * @return
   */
  public Map<PoiFeedUtil.FeedResult, Integer> cleanup(PoiMgr.DeleteMode delMode) {
    final AtomicInteger counter = new AtomicInteger(0);
    Template.forEachMissingFromFeedSrc(feedSrc.getSrcId(), t -> {
      Log.debug(String.format("%1$s: cleanup: %2$3d %3$s deleting: %4$s",
                              feedSrc.getName(), counter.incrementAndGet(), delMode.name(), t.getName()));

      switch (delMode) {
        case HARD:
          int c = TemplateController.deleteTemplateHard(t.getTemplateid());
          Log.debug("FeedHelperTmplt: Deleted " + c + " details for template: " + t.getName() +
                    " ["+t.getTemplateid()+"]");
          break;
        case SOFT:
        default:
          t.setStatus(APPConstants.STATUS_DELETED);
          t.markModified(modifiedBy.getLegacyId());
          t.update();
          break;
      }
    });

    HashMap<PoiFeedUtil.FeedResult, Integer> result = new HashMap<>();

    result.put(PoiFeedUtil.FeedResult.SUCCESS, counter.intValue());
    return result;
  }
}
