package com.umapped.itinerary.analyze;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.segment.SegmentNature;
import com.umapped.itinerary.analyze.segment.TripSegment;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionTree;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeatureDetectorRegistry;
import com.umapped.itinerary.analyze.segment.AnalyzerContext;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by wei on 2017-03-12.
 */
abstract public class AbstractRecommendationTester {
  protected static String DATA_ROOT = "/Users/wei/DevResources/data/trips/";
  protected static String SEGMENT_ROOT = DATA_ROOT + "segment/";
  protected static String RECOMMEND_ROOT = DATA_ROOT + "recommend/";


  protected ObjectMapper om;
  protected DecisionTree decisionTree;
  protected SegmentFeatureDetectorRegistry registry;

  protected AbstractRecommendationTester(ObjectMapper om, SegmentFeatureDetectorRegistry registry, DecisionTree decisionTree) {
    this.om = om;
    this.registry = registry;
    this.decisionTree = decisionTree;
  }

  abstract protected void outputCsv(String fileName, String outputDir, List<TripSegment> segments, List<List<DecisionResult>> results)
      throws IOException;

  abstract protected String getOutputDir();

  public void analyze(File segmentFile)
      throws Exception {
    List<TripSegment> segments = om.readValue(segmentFile, new TypeReference<List<TripSegment>>() {
    });
    List<List<DecisionResult>> results = recommend(segments);
    outputCsv(segmentFile.getName(), RECOMMEND_ROOT + getOutputDir(), segments, results);
  }

  public List<List<DecisionResult>> recommend(List<TripSegment> segments) {
    List<List<DecisionResult>> results = new ArrayList<>();
    TripSegment p = null, c = null, n = null;

    int total = segments.size();
    for (int i = 0; i < total; i++) {
      p = c;
      c = segments.get(i);
      n = (i == total - 1) ? null : segments.get(i + 1);
      AnalyzerContext context = new AnalyzerContext(registry);
      context.setup(c, p, n);
      results.add(decisionTree.decide(context));
    }
    return results;
  }

  protected String getNatures(Set<SegmentNature> natures) {
    StringBuilder builder = new StringBuilder();
    if (natures != null) {
      natures.stream().forEach(item -> {
        builder.append(item);
        builder.append(";");
      });
    }
    return builder.length() > 0 ? builder.substring(0, builder.length() - 1).toString() : "";
  }

  protected String getIds(List<ItineraryItem> segments) {
    StringBuilder builder = new StringBuilder();
    if (segments != null) {
      segments.stream().forEach(item -> {
        builder.append(item.getUmId());
        builder.append(String.format("(%s)", item.getDuration()));
        builder.append("/");
      });
    }

    return builder.length() > 0 ? builder.substring(0, builder.length() - 1).toString() : "";
  }

}
