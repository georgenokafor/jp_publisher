package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.amazonaws.services.s3.model.S3Object;
import com.avaje.ebean.Ebean;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.extractor.DataExtractor;
import com.mapped.publisher.parse.extractor.booking.BookingExtractor;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.parse.valueObject.DateDetails;
import com.mapped.publisher.parse.valueObject.DayDetails;
import com.mapped.publisher.parse.valueObject.TravelDetails;
import com.mapped.publisher.persistence.VOModeller;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import com.mapped.publisher.utils.Utils;
import controllers.DestinationPostController;
import controllers.EmailController;
import models.publisher.*;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.util.PDFTextStripper;
import org.joda.time.DateTime;

import javax.persistence.OptimisticLockException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by twong on 2015-02-04.
 */
public class EmailAttachmentParserActor
    extends UntypedActor {

  public interface Factory {
    public Actor create();
  }


  /**
   * Akka Actor Properties
   */
  public final static Props props = Props.create(EmailAttachmentParserActor.class);
  private static final int DB_UPDATE_RETRIES = 3;

  public static enum EmailAttachCmd {
    TO_TRIP,
    TO_INBOX
  }

  public static class EmailAttachMsg {
    EmailAttachCmd cmd;
    String tripId;
    List<Long> files;
    Long eplId;

    public String toString() {
      StringBuilder sb = new StringBuilder("CMD ATTACH: ");
      sb.append(cmd.name());
      sb.append(" EPLID: ");
      sb.append(eplId);
      sb.append(" Trip ID: ");
      sb.append(tripId);
      sb.append(" File Count: ");
      sb.append(files.size());

      for(Long fid : files) {
        sb.append(">>> File ID: ");
        sb.append(fid);
        FileInfo f = FileInfo.find.byId(fid);
        sb.append(" name: ");
        sb.append(f.getFilename());
      }

      return sb.toString();
    }

    public EmailAttachMsg(EmailAttachCmd cmd) {
      files = new ArrayList<>(2);
      eplId = null;
      tripId = null;
      this.cmd = cmd;
    }
    public Long getEplId() {
      return eplId;
    }

    public void setEplId(Long eplId) {
      this.eplId = eplId;
    }

    public List<Long> getFiles() {
      return files;
    }

    public void setFiles(List<Long> files) {
      this.files = files;
    }

    public String getTripId() {
      return tripId;
    }

    public void setTripId(String tripId) {
      this.tripId = tripId;
    }

    public void addAttachment(Long pk) {
      files.add(pk);
    }
  }

  private boolean attachmentsToTrip(EmailAttachMsg cmd)
      throws Exception{
    StopWatch sw = new StopWatch();
    sw.start();
    long startDate = DateTime.now().toDateMidnight().getMillis();
    long endDate = startDate;
    if (cmd.tripId == null || cmd.getEplId() == null) {
      return false;
    }

    EmailParseLog epl = EmailParseLog.find.byId(cmd.eplId);
    if(epl == null) {
      Log.err("EMAIL_Stage_C:TRIP:BODY: Wrong EmailParseLog ID: " + epl.getPk());
      return false;
    }

    Trip trip = Trip.find.byId(cmd.getTripId());

    if(trip == null || trip.status == APPConstants.STATUS_DELETED) {
      Log.err("EMAIL_Stage_C:TRIP:ATTACH: Problem with trip record");
      return false;
    }

    Log.debug("EMAIL_Stage_C:TRIP:ATTACH Start processing: " + trip.name);
    Account a = Account.findByLegacyId(epl.getUserid());
    VOModeller voModeller = new VOModeller(trip, a, false);
    voModeller.setActorType(AuditActorType.EMAIL_USER);

    StringBuilder parserNames = new StringBuilder();
    for(Long fileId: cmd.getFiles()) {
      FileInfo currFile = FileInfo.find.byId(fileId);

      Log.debug("EMAIL_Stage_C:TRIP:ATTACH: Processing file:" + currFile.getFilename() + " (EPLID: " + epl.getPk() + ")");
      try {
        String s3URL = S3Util.authorizeS3Get(currFile.getBucket(), currFile.getFilepath());
        currFile.setUrl(s3URL);

        voModeller.addAttachment(currFile.getFilename(),
                                 currFile.getFilepath(),
                                 currFile.getFiletype().getMimeType(),
                                 s3URL);

        CmpyApiParser parser = null;
        List<CmpyApiParser> parsers = CmpyApiParser.findByCmpy(epl.getToken().getBelongId());

        //Determining parser based on filename
        for (CmpyApiParser p : parsers) {
          if (currFile.getFilename().matches(p.filepattern)) {
            parser = p;
            break;
          }
        }

        //if there are no parsers try to determine if this document can be parsed with one of the custom parsers
        BookingExtractor.Parsers parserType = null;
        BookingExtractor bookingExtractor = null;

        if (parser == null && currFile.getFiletype().getExtension().equals("pdf")) {
          S3Object s3Obj = S3Util.getS3File(currFile.getBucket(), currFile.getFilepath());

          PDDocument document1 = PDDocument.load(s3Obj.getObjectContent());
          //More info about PDF Metadata: https://pdfbox.apache.org/cookbook/workingwithmetadata.html
          PDDocumentInformation docInfo = document1.getDocumentInformation();
          PDFTextStripper stripper2 = new PDFTextStripper();
          stripper2.setSortByPosition(false);
          String s2 = stripper2.getText(document1);
          if (s2 != null) {
            if (s2.contains("ITINERARY PASSENGER RECEIPT") && s2.contains("RESERVATION NUMBER(S)")) {
              parserType = BookingExtractor.Parsers.AMADEUS_OLD;
              parser = new CmpyApiParser();
              parser.setParser("com.mapped.publisher.parse.extractor.booking.AmadeusExtractor");
              parser.parsertype = CmpyApiParser.ParserType.BOOKINGS;
            }
            else if (s2.contains("TRIP TO") && s2.contains("PREPARED FOR") && (s2.contains("DEPARTURE:") || s2.contains(
                "PICK UP:") || s2.contains("CHECK IN:"))) {
              parserType = BookingExtractor.Parsers.SABRE;
              parser = new CmpyApiParser();
              parser.setParser("com.mapped.publisher.parse.extractor.booking.SabreExtractor");
              parser.parsertype = CmpyApiParser.ParserType.BOOKINGS;
            }
            else if (s2.contains("DESTINO") && s2.contains("PREPARADO PARA") && (s2.contains("PARTIDA:") || s2.contains(
                "PICK UP:") || s2.contains("CHECK IN:"))) {
              parserType = BookingExtractor.Parsers.SABRE;
              parser = new CmpyApiParser();
              parser.setParser("com.mapped.publisher.parse.extractor.booking.SabreExtractor");
              parser.parsertype = CmpyApiParser.ParserType.BOOKINGS;
            }
            else if (s2.contains("CheckMyTrip") && s2.contains("Traveller(s) Itinerary")) {
              parserType = BookingExtractor.Parsers.AMADEUS;
              parser = new CmpyApiParser();
              parser.setParser("com.mapped.publisher.parse.extractor.booking.AmadeusExtractor2");
              parser.parsertype = CmpyApiParser.ParserType.BOOKINGS;
            }
            else if (s2.contains("Queen of Clubs")) {
              parserType = BookingExtractor.Parsers.QOFC;
              parser = new CmpyApiParser();
              parser.setParser("com.mapped.publisher.parse.extractor.booking.QueenOfClubExtractor");
              parser.parsertype = CmpyApiParser.ParserType.BOOKINGS;
            }
            else if (docInfo.getProducer() != null && docInfo.getProducer().contains("Nevrona Designs")) {
              parserType = BookingExtractor.Parsers.CLIENTBASE;
              parser = new CmpyApiParser();
              parser.setParser("com.mapped.publisher.parse.extractor.booking.ClientbaseExtractor");
              parser.parsertype = CmpyApiParser.ParserType.BOOKINGS;
            }
            else if (s2.contains("Reservation") && s2.contains("Prior Invoiced") && s2.contains("This Invoice")) {
              parserType = BookingExtractor.Parsers.CLIENTBASE;
              parser = new CmpyApiParser();
              parser.setParser("com.mapped.publisher.parse.extractor.booking.ClientbaseExtractor");
              parser.parsertype = CmpyApiParser.ParserType.BOOKINGS;
            }
            else if (s2.contains("www.travel2-us.com") || s2.contains("www.qantasvacations.com") || s2.contains("www.islandsinthesun.com")) {
              parserType = BookingExtractor.Parsers.TRAVEL2;
              parser = new CmpyApiParser();
              parser.setParser("com.mapped.publisher.parse.extractor.booking.Travel2Extractor");
              parser.parsertype = CmpyApiParser.ParserType.BOOKINGS;
            } else if (s2.contains("Auto Europe voucher number") && s2.contains("39 COMMERCIAL STREET")) {
              parserType = BookingExtractor.Parsers.AUTOEUROPE;
              parser = new CmpyApiParser();
              parser.setParser("com.mapped.publisher.parse.extractor.booking.AutoEuropeExtractor");
              parser.parsertype = CmpyApiParser.ParserType.BOOKINGS;
            }
          }
        }

        if (parser == null) {
          Log.info("EMAIL_Stage_C:TRIP:ATTACH: File: " + currFile.getFilename() + " can't be parsed, was added as attachment");
        } else {
          S3Object s3Obj = S3Util.getS3File(currFile.getBucket(), currFile.getFilepath());

          if (parserType != null) {
            if ((epl.getParser() != null && epl.getParser().length() != 0 && parserNames.length() == 0) || parserNames.length() > 0) {
              parserNames.append(", ");
            }

            parserNames.append(parserType.name());
          }

          switch (parser.parsertype) {
            case BOOKINGS:
              //parse bookings and add it to the
              bookingExtractor = (BookingExtractor) Class.forName(parser.parser).newInstance();
              bookingExtractor.init(parser.getParameters());
              bookingExtractor.setCmpyId(epl.getToken().getBelongId());
              bookingExtractor.setUserId(epl.getUserid());

              TripVO tripBookings = bookingExtractor.extractData(s3Obj.getObjectContent());
              if (tripBookings != null && tripBookings.getImportSrc() == null) {
                tripBookings.setImportSrc(BookingSrc.ImportSrc.EMAIL_PDF);
                tripBookings.setImportTs(System.currentTimeMillis());
              }
              if (tripBookings != null && tripBookings.getImportSrcId() == null) {
                tripBookings.setImportSrcId(currFile.getFilepath());
              }
              voModeller.buildTripVOModels(tripBookings);
              break;
            case ITINERARY:
              TripDestination tripDestination = null;
              Destination tripDocument = null;
              List<DestinationGuide> tripDocumentPages = new ArrayList<>();
              DataExtractor pdfParser = (DataExtractor) Class.forName(parser.parser).newInstance();
              pdfParser.init(parser.getParameters());
              TravelDetails details = pdfParser.extractData(s3Obj.getObjectContent());

              if (details != null &&
                  details.getDateWiseDetails() != null &&
                  details.getDateWiseDetails().size() > 0) {
                //parse the details to build the trip document itinerary and the activity bookings
                //get the trip destination
                tripDestination = TripDestination.find.byId(trip.getTripid());
                //build the trip document link
                if (tripDestination == null) {
                  tripDestination = new TripDestination();
                  tripDestination.setCreatedby(epl.getUserid());
                  tripDestination.setCreatedtimestamp(System.currentTimeMillis());
                  tripDestination.setRank(1);
                  tripDestination.setStatus(APPConstants.STATUS_ACTIVE);
                  tripDestination.setTripdestid(trip.getTripid());
                  tripDestination.setTripid(trip.getTripid());
                  tripDestination.setName(EmailController.cleanupSubject(epl.getSubject()));
                  tripDestination.setDestinationid(null);
                }
                else if (tripDestination.getStatus() != APPConstants.STATUS_ACTIVE) {
                  tripDestination.setStatus(APPConstants.STATUS_ACTIVE);
                  tripDestination.setDestinationid(null);
                  tripDestination.setName(EmailController.cleanupSubject(epl.getSubject()));
                }
                tripDestination.setModifiedby(epl.getUserid());
                tripDestination.setLastupdatedtimestamp(System.currentTimeMillis());

                //get/create the trip document
                if (tripDestination.getDestinationid() != null && tripDocument == null) {
                  tripDocument = Destination.find.byId(tripDestination.getDestinationid());
                }

                if (tripDocument == null) {
                  tripDocument = new Destination();
                  tripDocument.setName(EmailController.cleanupSubject(epl.getSubject()));
                  tripDocument.setCreatedby(epl.getUserid());
                  tripDocument.setCreatedtimestamp(System.currentTimeMillis());
                  tripDocument.setCmpyid(epl.getToken().getBelongId());
                  tripDocument.setDestinationid(DBConnectionMgr.getUniqueId());
                  tripDocument.setDestinationtypeid(APPConstants.TRIP_DOC_DESTINATION_TYPE);
                  tripDocument.setStatus(APPConstants.STATUS_ACTIVE);
                  tripDestination.setDestinationid(tripDocument.destinationid);
                }

                tripDocument.setIntro("");
                tripDocument.setLastupdatedtimestamp(System.currentTimeMillis());
                tripDocument.setModifiedby(epl.getUserid());

                if (details.getIntroInfo() != null && !details.getIntroInfo().isEmpty()) {
                  //create the intro page
                  String title = "Introduction";
                  DestinationGuide guide = null;
                  List<DestinationGuide> guides = DestinationGuide.findDestGuidesByNameTemplateId(title,
                                                                                                  currFile
                                                                                                      .getFilename(),
                                                                                                  tripDocument.destinationid);
                  if (guides != null && guides.size() > 0) {
                    guide = guides.get(0);
                  }

                  if (guide == null) {
                    guide = new DestinationGuide();
                    guide.setCreatedby(epl.getUserid());
                    guide.setCreatedtimestamp(System.currentTimeMillis());
                    guide.setDestinationguideid(DBConnectionMgr.getUniqueId());
                    guide.setDestinationid(tripDocument.destinationid);
                    guide.setStatus(APPConstants.STATUS_ACTIVE);
                  }
                  guide.setTemplateid(currFile.getFilename());
                  guide.setRank(0);
                  guide.setName(title);
                  guide.setIntro(details.getIntroInfo());
                  guide.setLastupdatedtimestamp(System.currentTimeMillis());
                  guide.setModifiedby(epl.getUserid());
                  tripDocumentPages.add(guide);
                }

                int count = 1;
                for (Date date : details.getDateWiseDetails().keySet()) {
                  DateDetails dateDetails = details.getDateWiseDetails().get(date);

                  String title = Utils.formatDayMonthPagePrint(date.getTime());

                  DestinationGuide guide = null;
                  List<DestinationGuide> guides = DestinationGuide.findDestGuidesByNameTemplateId(title,
                                                                                                  currFile
                                                                                                      .getFilename(),
                                                                                                  tripDocument.destinationid);
                  if (guides != null && guides.size() > 0) {
                    guide = guides.get(0);
                  }

                  if (guide == null) {
                    guide = new DestinationGuide();
                    guide.setCreatedby(epl.getUserid());
                    guide.setCreatedtimestamp(System.currentTimeMillis());
                    guide.setDestinationguideid(DBConnectionMgr.getUniqueId());
                    guide.setDestinationid(tripDocument.destinationid);
                    guide.setStatus(APPConstants.STATUS_ACTIVE);
                  }
                  guide.setTemplateid(currFile.getFilename());
                  guide.setRank(count);
                  guide.setName(title);
                  guide.setIntro(dateDetails.getFulText());
                  guide.setLastupdatedtimestamp(System.currentTimeMillis());
                  guide.setModifiedby(epl.getUserid());
                  tripDocumentPages.add(guide);

                  //set the start date and end date of the trip
                  if (count == 1) {
                    //set start date
                    startDate = date.getTime();
                  }

                  if (count == (details.getDateWiseDetails().size())) {
                    endDate = date.getTime();
                  }

                  //get the detail time based activities if applicable
                  if (dateDetails.getActivities() != null && dateDetails.getActivities().size() > 0) {
                    for (Date timestamp : dateDetails.getActivities().keySet()) {
                      DayDetails dayDetails = dateDetails.getActivities().get(timestamp);

                      if (dayDetails != null && ((dayDetails.getIntro() != null && !dayDetails.getIntro()
                                                                                              .isEmpty()) || (dayDetails
                                                                                                                  .getBody() != null && !dayDetails
                          .getBody()
                          .isEmpty()))) {
                        voModeller.addActivity(dayDetails, timestamp);
                      }
                    }
                  }
                  count++;
                }
                if (details.getAdditionalInfo() != null && !details.getAdditionalInfo().isEmpty()) {
                  //create the intro page
                  String title = "Additional Information";
                  DestinationGuide guide = null;
                  List<DestinationGuide> guides = DestinationGuide.findDestGuidesByNameTemplateId(title,
                                                                                                  currFile.getFilename(),
                                                                                                  tripDocument.destinationid);
                  if (guides != null && guides.size() > 0) {
                    guide = guides.get(0);
                  }

                  if (guide == null) {
                    guide = new DestinationGuide();
                    guide.setCreatedby(epl.getUserid());
                    guide.setCreatedtimestamp(System.currentTimeMillis());
                    guide.setDestinationguideid(DBConnectionMgr.getUniqueId());
                    guide.setDestinationid(tripDocument.destinationid);
                    guide.setStatus(APPConstants.STATUS_ACTIVE);
                  }
                  guide.setTemplateid(currFile.getFilename());
                  guide.setRank(count);
                  guide.setName(title);
                  guide.setIntro(details.getAdditionalInfo());
                  guide.setLastupdatedtimestamp(System.currentTimeMillis());
                  guide.setModifiedby(epl.getUserid());
                  tripDocumentPages.add(guide);
                }

                //see if we need to save the cover
                if (details.getCoverImg() != null) {
                  try {
                    String fileName = tripDocument.getDestinationid() + ".png";
                    S3Util.saveImg(details.getCoverImg(), fileName);
                    tripDocument.setCovername(fileName);
                    tripDocument.setCoverurl(S3Util.authorizeS3Get(fileName));
                  }
                  catch (Exception e) {
                    e.printStackTrace();
                  }
                }
              }
              saveToDB(trip, epl, tripDocument, tripDocumentPages, tripDestination);
              break;
          }
        }

        if (trip != null) {
          if (trip.getStarttimestamp() == null || trip.getStarttimestamp() == 0 ||
              trip.getStarttimestamp().longValue() == trip.createdtimestamp.longValue()) {
            trip.setStarttimestamp(startDate);
          }

          if (trip.getEndtimestamp() == null ||
              trip.getEndtimestamp() == 0 ||
              trip.getEndtimestamp().equals(trip.createdtimestamp)) {
            if (endDate > startDate) {
              trip.setEndtimestamp(endDate);
            }
            else {
              trip.setEndtimestamp(startDate);
            }
          }

          if (trip.getEndtimestamp() < trip.getStarttimestamp()) {
            trip.setEndtimestamp(trip.getStarttimestamp());
          }
        }
        voModeller.saveAllModels();
        if (voModeller.getTrip() != null && voModeller.getTrip().getTripid() != null && bookingExtractor != null) {
          bookingExtractor.postProcessing(voModeller.getTrip().getTripid(), voModeller.getAccount().getLegacyId());
        }

        Log.debug("EMAIL_Stage_C:TRIP:ATTACH: - Processed: " + voModeller.getBookingsCount() +
                  " bookings from " + epl.getSubject() +
                  " sent by " + epl.getFromAddr() +
                  " for cmpy " + epl.getToken().getBelongId() +
                  " and file: " + currFile.getFilename() +
                  " in " + sw.getTime());
      } catch (Exception e) {
        Log.err("EMAIL_Stage_C:TRIP:ATTACH: - Error Processing: " + voModeller.getBookingsCount() +
                " bookings from " + epl.getSubject() +
                " sent by " + epl.getFromAddr() +
                " for cmpy " + epl.getToken().getBelongId() +
                " and file: " + currFile.getFilename() + " file ID: " + currFile.getPk());
        e.printStackTrace();
      }
    }

    sw.stop();

    for (int retry = 0; retry < DB_UPDATE_RETRIES; retry++) {
      try {
        epl.refresh();
        epl.addParseMs(sw.getTime());
        epl.addParser(parserNames.toString());

        if(voModeller.getBookingsCount() > 0) {
          epl.setState(EmailParseLog.EmailState.PARSED_LOCAL);
          epl.addBkCount(voModeller.getBookingsCount());
        }
        epl.update();
        break;
      }
      catch (OptimisticLockException ole) {
        //Skipping on purpose
      }
    }

    return false;
  }

  private static void saveToDB(Trip trip,
                              EmailParseLog epl,
                              Destination tripDocument,
                              List<DestinationGuide> tripDocumentPages,
                              TripDestination tripDocumentLink) {
    //single transaction for all save
    try {
      Ebean.beginTransaction();
      if (trip != null) {
        //we might run into concurrency issue with the trip... so we need to read just before we save

        if (tripDocument != null) {
          DestinationPostController.invalidateCache(tripDocument.destinationid);
          tripDocument.save();
        }
        if (tripDocumentPages != null) {
          for (DestinationGuide p : tripDocumentPages) {
            p.save();
          }
        }
        if (tripDocumentLink != null) {
          tripDocumentLink.save();
        }
      }
      ApiAudit apiAudit = new ApiAudit();
      apiAudit.setPk(DBConnectionMgr.getUniqueId());
      apiAudit.setCmpyid(epl.getToken().getBelongId());
      apiAudit.setUserid(epl.getUserid());
      apiAudit.setCreatedtimestamp(System.currentTimeMillis());
      apiAudit.setNumattachments(epl.getAttachCount());
      apiAudit.setTripid(trip.tripid);
      apiAudit.setApitype(0);
      apiAudit.save();
      Ebean.commitTransaction();
    }
    catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
      Log.err("EMAIL_Stage_C:TRIP:ATTACH: Cannot save: " + trip.getName(), e);
    }
  }

  @Override
  public void onReceive(Object message)
      throws Exception {
    if(message instanceof EmailAttachMsg) {
      EmailAttachMsg msg = (EmailAttachMsg) message;

      Log.debug(msg.toString());
      switch (msg.cmd) {
        case TO_INBOX:
          throw new NotImplementedException("Attachments to INBOX feature is currently not implemented");
        case TO_TRIP:
          attachmentsToTrip(msg);
          break;
      }

    }
  }
}
