package com.mapped.publisher.view;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-20
 * Time: 12:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserStatsView {
    public String userid;
    public String firstName;
    public String lastName;
    public String cmpyName;
    public String cmpyId;
    public String email;



    public int numTripsPublished;
    public int numTripsPublishedActivity;

    public int numTripsCreated;
    public int numTripsPending;
    public int numTripsDeleted;
    public int numToursCreated;
    public int numToursPending;
    public int numToursDeleted;
}
