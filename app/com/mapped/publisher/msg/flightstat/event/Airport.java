
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class Airport{
   	private String active;
   	private String city;
   	private String cityCode;
   	private String classification;
   	private String countryCode;
   	private String countryName;
   	private String elevationFeet;
   	private String faa;
   	private String fs;
   	private String iata;
   	private String icao;
   	private String latitude;
   	private String localTime;
   	private String longitude;
   	private String name;
   	private String postalCode;
   	private String regionName;
   	private String stateCode;
   	private String street1;
   	private String street2;
   	private String timeZoneRegionName;
   	private String utcOffsetHours;
   	private String weatherZone;

 	public String getActive(){
		return this.active;
	}
	public void setActive(String active){
		this.active = active;
	}
 	public String getCity(){
		return this.city;
	}
	public void setCity(String city){
		this.city = city;
	}
 	public String getCityCode(){
		return this.cityCode;
	}
	public void setCityCode(String cityCode){
		this.cityCode = cityCode;
	}
 	public String getClassification(){
		return this.classification;
	}
	public void setClassification(String classification){
		this.classification = classification;
	}
 	public String getCountryCode(){
		return this.countryCode;
	}
	public void setCountryCode(String countryCode){
		this.countryCode = countryCode;
	}
 	public String getCountryName(){
		return this.countryName;
	}
	public void setCountryName(String countryName){
		this.countryName = countryName;
	}
 	public String getElevationFeet(){
		return this.elevationFeet;
	}
	public void setElevationFeet(String elevationFeet){
		this.elevationFeet = elevationFeet;
	}
 	public String getFaa(){
		return this.faa;
	}
	public void setFaa(String faa){
		this.faa = faa;
	}
 	public String getFs(){
		return this.fs;
	}
	public void setFs(String fs){
		this.fs = fs;
	}
 	public String getIata(){
		return this.iata;
	}
	public void setIata(String iata){
		this.iata = iata;
	}
 	public String getIcao(){
		return this.icao;
	}
	public void setIcao(String icao){
		this.icao = icao;
	}
 	public String getLatitude(){
		return this.latitude;
	}
	public void setLatitude(String latitude){
		this.latitude = latitude;
	}
 	public String getLocalTime(){
		return this.localTime;
	}
	public void setLocalTime(String localTime){
		this.localTime = localTime;
	}
 	public String getLongitude(){
		return this.longitude;
	}
	public void setLongitude(String longitude){
		this.longitude = longitude;
	}
 	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name = name;
	}
 	public String getPostalCode(){
		return this.postalCode;
	}
	public void setPostalCode(String postalCode){
		this.postalCode = postalCode;
	}
 	public String getRegionName(){
		return this.regionName;
	}
	public void setRegionName(String regionName){
		this.regionName = regionName;
	}
 	public String getStateCode(){
		return this.stateCode;
	}
	public void setStateCode(String stateCode){
		this.stateCode = stateCode;
	}
 	public String getStreet1(){
		return this.street1;
	}
	public void setStreet1(String street1){
		this.street1 = street1;
	}
 	public String getStreet2(){
		return this.street2;
	}
	public void setStreet2(String street2){
		this.street2 = street2;
	}
 	public String getTimeZoneRegionName(){
		return this.timeZoneRegionName;
	}
	public void setTimeZoneRegionName(String timeZoneRegionName){
		this.timeZoneRegionName = timeZoneRegionName;
	}
 	public String getUtcOffsetHours(){
		return this.utcOffsetHours;
	}
	public void setUtcOffsetHours(String utcOffsetHours){
		this.utcOffsetHours = utcOffsetHours;
	}
 	public String getWeatherZone(){
		return this.weatherZone;
	}
	public void setWeatherZone(String weatherZone){
		this.weatherZone = weatherZone;
	}
}
