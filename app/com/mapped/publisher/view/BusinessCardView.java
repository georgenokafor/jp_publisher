package com.mapped.publisher.view;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2014-07-29.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BusinessCardView
    implements Serializable {
  public String email;
  public String phone;
  public String mobile;
  public String profilePhotoUrl;
  public String webUrl;
  public String fax;

  public String socialFacebookUrl;
  public String socialTwitterUrl;
  public String socialLinkedInUrl;
  public String socialGoogleUrl;
  public String socialYouTubeUrl;
  public String socialInstagramUrl;
  public String companyName;
  public String companyUrl;
  public String companyLogoUrl;
  public String companyEmail;
  public String companyPhone;
  public String companyFax;
  public String companyFacebookUrl;
  public String companyTwitterUrl;
  public boolean whitelabel;
  public String companyId;
  public Integer cmpyId;
  public String parentCompanyId;

  public List<String> consortiumNames;

  public AgentView agentView;

  /**
   * Companies that might be involved with this business card (i.e. during trip branding)
   */
  public  List<TripBrandingView> brands;
  private String                 firstName;
  private String                 lastName;

  public BusinessCardView() {
    brands = new ArrayList<>(1);
    socialFacebookUrl = null;
    socialTwitterUrl = null;
    socialLinkedInUrl = null;
    socialGoogleUrl = null;
    socialYouTubeUrl = null;
    socialInstagramUrl = null;
    mobile = null;
    phone = null;
    webUrl = null;

  }

  public String getPhone() {
    return getPhoneNumber(phone);
  }

  public String getPhoneNumber(String did) {
    if(did == null || did.length() <= 5) {
      return null;
    }

    if (did.indexOf('+') > 0) {
      return did;
    }
    return "+" + did;
  }

  public String getMobile() {
    return getPhoneNumber(mobile);
  }

  public String getCompanyUrl() {
    return getUrl(companyUrl);
  }

  public String getUrl(String url) {
    if(url == null) {
      return null;
    }

    if (url != null && url.indexOf("http") > 0) {
      return url;
    }
    return "http://" + url;
  }

  public String getCompanyPhone() {
    return getPhoneNumber(companyPhone);
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    if (firstName != null) {
      this.firstName = firstName;
    }
    else {
      this.firstName = "";
    }
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    if (lastName != null) {
      this.lastName = lastName;
    }
    else {
      this.lastName = "";
    }
  }

  public String getFullName() {
    StringBuilder sb = new StringBuilder();
    sb.append(firstName);
    if (lastName.length() > 0) {
      sb.append(" ");
      sb.append(lastName);
    }
    return sb.toString();
  }

  public List<String> getConsortiumNames() {
    return consortiumNames;
  }

  public void setConsortiumNames(List<String> consortiumNames) {
    this.consortiumNames = consortiumNames;
  }

  public void addConsortiumNames(String consortiumName) {
    if (consortiumNames == null) {
      consortiumNames = new ArrayList<>();
    }
    consortiumNames.add(consortiumName);
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }
}
