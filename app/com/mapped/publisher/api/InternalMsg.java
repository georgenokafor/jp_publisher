package com.mapped.publisher.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mapped.publisher.api.msg.BaseMsg;
import play.libs.Json;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-10
 * Time: 8:27 AM
 * To change this template use File | Settings | File Templates.
 */
public class InternalMsg
    implements BaseJson {
  private long reqId;
  private long msgTimestamp;
  private String serviceId;
  private String callerId;
  private String callbackUrl;
  private String status;
  private BaseMsg body;

  public static InternalMsg fromJson(JsonNode node) {
    JsonNode msgHeader = node.findValue("header");
    JsonNode msgBody = node.findValue("body");
    if (node == null || msgHeader == null) {
      return null;
    }
    InternalMsg msg = new InternalMsg();
    msg.serviceId = msgHeader.findPath("serviceId").textValue();
    msg.reqId = msgHeader.findPath("reqId").longValue();
    msg.callerId = msgHeader.findPath("callerId").textValue();
    msg.callbackUrl = msgHeader.findPath("callbackUrl").textValue();
    msg.msgTimestamp = msgHeader.findPath("msgTimestamp").longValue();
    msg.status = msgHeader.findPath("status").textValue();

    if (msgBody != null) {
      msg.parseJson(msgBody);
    }
    return msg;
  }

  public long getReqId() {
    return reqId;
  }

  public void setReqId(long reqId) {
    this.reqId = reqId;
  }

  public long getMsgTimestamp() {
    return msgTimestamp;
  }

  public void setMsgTimestamp(long msgTimestamp) {
    this.msgTimestamp = msgTimestamp;
  }

  public String getServiceId() {
    return serviceId;
  }

  public void setServiceId(String serviceId) {
    this.serviceId = serviceId;
  }

  public String getCallerId() {
    return callerId;
  }

  public void setCallerId(String callerId) {
    this.callerId = callerId;
  }

  public String getCallbackUrl() {
    return callbackUrl;
  }

  public void setCallbackUrl(String callbackUrl) {
    this.callbackUrl = callbackUrl;
  }

  String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public BaseMsg getBody() {
    return body;
  }

  public void setBody(BaseMsg body) {
    this.body = body;
  }

  public ObjectNode toJson() {
    ObjectNode result = Json.newObject();
    ObjectNode header = result.putObject("header");
    header.put("serviceId", serviceId);
    header.put("reqId", reqId);
    header.put("callerId", callerId);
    header.put("callbackUrl", callbackUrl);
    header.put("msgTimestamp", msgTimestamp);
    header.put("status", status);
    if (body != null) {
      result.set("body", body.toJson());
    }
    return result;
  }

  public boolean parseJson(JsonNode node) {
    JsonNode msgHeader = node.findValue("header");
    JsonNode msgBody = node.findValue("body");
    if (node == null || msgHeader == null) {
      return false;
    }
    serviceId = msgHeader.findPath("serviceId").textValue();
    reqId = msgHeader.findPath("reqId").longValue();
    callerId = msgHeader.findPath("callerId").textValue();
    callbackUrl = msgHeader.findPath("callbackUrl").textValue();
    msgTimestamp = msgHeader.findPath("msgTimestamp").longValue();
    status = msgHeader.findPath("status").textValue();

    if (msgBody != null) {
      body.parseJson(msgBody);
    }
    return true;
  }

}
