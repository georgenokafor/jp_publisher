package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-18
 * Time: 9:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class TripGuideForm {

    private String inTripId;
    private String inDestId;

    public String getInTripId() {
        return inTripId;
    }

    public void setInTripId(String inTripId) {
        this.inTripId = inTripId;
    }

    public String getInDestId() {
        return inDestId;
    }

    public void setInDestId(String inDestId) {
        this.inDestId = inDestId;
    }
}
