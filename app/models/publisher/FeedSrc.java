package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Sources of POI data
 * Created by surge on 2014-10-22.
 */
@Entity @Table(name = "feed_src")
public class FeedSrc
    extends Model {

  public static Model.Finder<Integer, FeedSrc> find = new Finder(FeedSrc.class);
  @Id
  private int srcId;
  private String name;
  private int consortiumId;
  private int cmpyId;
  private String loaderClass;
  private int typeId;
  @Enumerated(value = EnumType.STRING)
  private FeedType feedType;
  private boolean hasAmenities;

  public static FeedSrc findByName(String name) {
    return find.where().eq("name", name).findUnique();
  }

  public static List<FeedSrc> findByType(FeedType type) {
    return find.where().eq("feed_type", type.name()).findList();
  }

  public static enum FeedType {
    POI,
    CRUISE,
    TMPLT
  }

  public boolean isHasAmenities() {
    return hasAmenities;
  }

  public void setHasAmenities(boolean hasAmenities) {
    this.hasAmenities = hasAmenities;
  }

  public FeedType getFeedType() {
    return feedType;
  }

  public void setFeedType(FeedType feedType) {
    this.feedType = feedType;
  }

  public int getTypeId() {
    return typeId;
  }

  public void setTypeId(int typeId) {
    this.typeId = typeId;
  }

  public String getLoaderClass() {
    return loaderClass;
  }

  public void setLoaderClass(String loaderClass) {
    this.loaderClass = loaderClass;
  }

  public int getConsortiumId() {
    return consortiumId;
  }

  public void setConsortiumId(int consortiumId) {
    this.consortiumId = consortiumId;
  }

  public int getCmpyId() {
    return cmpyId;
  }

  public void setCmpyId(int cmpyId) {
    this.cmpyId = cmpyId;
  }

  public int getSrcId() {
    return srcId;
  }

  public void setSrcId(int srcId) {
    this.srcId = srcId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
