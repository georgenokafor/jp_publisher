package com.mapped.publisher.persistence.communicator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.firebase.client.Firebase;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by surge on 2015-09-01.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServerStatus
    extends FirebaseObject {

  public static enum ServerMode {
    FOLLOWER,
    CANDIDATE,
    LEADER
  }

  public Long       launch_ts;
  public Long       connect_ts;
  public Long       update_ts;
  public Long       mode_ts;
  public ServerMode mode;

  public static ServerStatus buildServerStatus() {
    ServerStatus ss = new ServerStatus();
    ss.launch_ts = System.currentTimeMillis();
    ss.update_ts = ss.launch_ts;
    ss.mode_ts = ss.launch_ts;
    ss.connect_ts = ss.launch_ts;
    ss.mode = ServerMode.FOLLOWER;
    return ss;
  }

  public Firebase updateTs(Firebase ref) {
    update_ts = System.currentTimeMillis();
    ref.child("update_ts").setValue(update_ts);
    return ref;
  }

  public Firebase setMode(Firebase ref, ServerMode newMode) {
    if(mode != newMode) {
      mode = newMode;
      mode_ts = System.currentTimeMillis();
      update_ts = mode_ts;
      Map<String, Object> uVals = new HashMap<>();
      uVals.put("update_ts", update_ts);
      uVals.put("mode_ts", mode_ts);
      uVals.put("mode", mode.name());
      ref.updateChildren(uVals);
    }
    return ref;
  }

  public Firebase update(Firebase ref) {
    Map<String, Object> uVals = new HashMap<>();
    uVals.put("launch_ts", launch_ts);
    uVals.put("connect_ts", connect_ts);
    uVals.put("update_ts", update_ts);
    uVals.put("mode_ts", mode_ts);
    uVals.put("mode", mode.name());
    ref.updateChildren(uVals);
    return ref;
  }
}
