package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class MilestonesTypesMgr {

	 private static final String SELECT_ALL = "Select Pk, PlaceType, PlaceName, MapsId, Rank, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from MilestonesTypes;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, PlaceType, PlaceName, MapsId, Rank, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from MilestonesTypes where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from MilestonesTypes where Pk = ?;";
	 private static final String INSERT = "insert into MilestonesTypes (Pk, PlaceType, PlaceName, MapsId, Rank, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update MilestonesTypes SET PlaceType=?, PlaceName=?, MapsId=?, Rank=?, ModifiedBy=?, Status=?, SyncTimestamp=?, LastUpdateTimestamp=? WHERE Pk = ?; ";
	 private static final String SELECT_ALL_BY_MAPS_ID = "Select Pk, PlaceType, PlaceName, MapsId, Rank, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from MilestonesTypes where MapsId=? ";
	 private static final String DELETE_BY_MAPS_ID = "delete from MilestonesTypes where MapsId=? ";
	 public static ArrayList <MilestonesTypes> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return MilestonesTypesMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static MilestonesTypes getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<MilestonesTypes> results = MilestonesTypesMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (MilestonesTypes c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getPlaceType());
				 prep.setString(3, c.getPlaceName());
				 prep.setString(4, c.getMapsId());
				 prep.setString(5, c.getRank());
				 prep.setString(6, c.getModifiedBy());
				 prep.setString(7, c.getCreatedBy());
				 prep.setLong(8, c.getStatus());
				 prep.setLong(9, c.getSyncTimestamp());
				 prep.setLong(10, c.getLastUpdateTimestamp());
				 prep.setLong(11, c.getCreateTimestamp());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (MilestonesTypes c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getPlaceType());
				 prep.setString(2, c.getPlaceName());
				 prep.setString(3, c.getMapsId());
				 prep.setString(4, c.getRank());
				 prep.setString(5, c.getModifiedBy());
				 prep.setLong(6, c.getStatus());
				 prep.setLong(7, c.getSyncTimestamp());
				 prep.setLong(8, c.getLastUpdateTimestamp());
				 prep.setString(9, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <MilestonesTypes> getAllByMapsId(String id) throws SQLException {
		 ResultSet rs = null;
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_MAPS_ID);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 return MilestonesTypesMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void deleteByMapsId (String id) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_MAPS_ID);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <MilestonesTypes> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <MilestonesTypes> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 MilestonesTypes c = new MilestonesTypes();
			 c.setPk(rs.getString("Pk"));
			 c.setPlaceType(rs.getString("PlaceType"));
			 c.setPlaceName(rs.getString("PlaceName"));
			 c.setMapsId(rs.getString("MapsId"));
			 c.setRank(rs.getString("Rank"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setStatus(rs.getLong("Status"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 results.add(c);
		 }
		 return results;
	 }
}
