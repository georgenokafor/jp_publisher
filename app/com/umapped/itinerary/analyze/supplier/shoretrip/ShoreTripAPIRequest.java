package com.umapped.itinerary.analyze.supplier.shoretrip;

import com.umapped.itinerary.analyze.webservice.WebAPIServiceRequest;

/**
 * Created by wei on 2017-04-05.
 */
public class ShoreTripAPIRequest extends WebAPIServiceRequest {
  private static String ACCESS_KEY = "IP-UM-20150612";

  public ShoreTripAPIRequest() {
    addParam(Constants.PARAM_PARTNER, "UMAPPED");
    addParam(Constants.PARAM_API_KEY, ACCESS_KEY);
  }
}
