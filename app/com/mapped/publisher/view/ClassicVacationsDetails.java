package com.mapped.publisher.view;

import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import models.publisher.TripNote;

import java.util.List;

/**
 * Created by twong on 15-06-30.
 */
public class ClassicVacationsDetails extends BaseView{
  public String bookingId;
  public TripVO tripVO;
  public List<TripNote> notes;
  public String destination;

}
