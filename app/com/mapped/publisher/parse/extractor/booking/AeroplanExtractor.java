package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.joda.time.DateTime;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by george on 2016-02-08.
 */
public class AeroplanExtractor extends BookingExtractor {
  private static String[] dateTimeFormat = {"dd MMM yyyy", "dd MMM yyyy ", "E dd MMM yyyy h:mma",  "E dd MMM yyyy", "E dd MMM yyyy ", "E dd MMMMM yyyy HH:mm" , "dd MMM yyyy HH:mm a", "E dd MMM yyyy HH:mm" , "dd MMMMM yyyy HH:mm", "E, MMMMM dd, yyyy HH:mm", "E, MMMMM dd, yyyy", "E, MMMMM dd, yyyy hh:mm a", "EEE dd MMM yyyy h:mm a", "EEE dd MMM yyyy 0h:mm a"} ;

  private static Pattern dayDatePattern = Pattern.compile("(Mon|Tue|Wed|Thu|Fri|Sat|Sun).[0-9]?[0-9].(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Sept|Oct|Nov|Dec).20[0-9]?[0-9]", Pattern.CASE_INSENSITIVE);
  private static Pattern timePattern = Pattern.compile("[0-9]{1,2}:[0-9]{2}");
  private static Pattern airportPattern = Pattern.compile("[(][A-Z]{3}[)]");
  private static Pattern dayDatePattern1 = Pattern.compile("(MONDAY|TUESDAY|WEDNESDAY|THURSDAY|FRIDAY|SATURDAY|SUNDAY).[0-9]?[0-9].(JANUARY|FEBRUARY|MARCH|APRIL|MAY|JUNE|JULY|AUGUST|SEPTEMBER|OCTOBER|NOVEMBER|DECEMBER).20[0-9]?[0-9]", Pattern.CASE_INSENSITIVE);


  private static Pattern flightNumberPattern = Pattern.compile("^[A-Z]{2,3}[0-9]{1,6}.*");
  private static Pattern flightBreakPattern = Pattern.compile("[A-Z- ]{1,100}.to.[A-Z- ]{1,100}"); //section break e.g.CHIANG MAI to DENPASAR-BALI
  private static Pattern timestampPattern = Pattern.compile("[A-Za-z]{3}\\s[0-9]{2}\\s[A-Za-z]{3}\\s[0-9]{4}.*.[0-9]{2}:[0-9]{2}",Pattern.CASE_INSENSITIVE); //section break e.g.CHIANG MAI to DENPASAR-BALI

  private DateUtils du = new DateUtils();
  protected void initImpl(Map<String, String> params) {

  }


  protected TripVO extractDataImpl(InputStream input) throws Exception {
    try {
      TripVO trip = new TripVO();
      List<PassengerVO> passengers = new ArrayList<>();
      List<FlightVO> flights = new ArrayList<>();

      PDDocument document = PDDocument.load(input);
      String aeroRewTkt = null;
      HashMap<String, List<String>> seatMap = new HashMap<>();
      int rank = 0;


      PDFTextStripper textStripper = new PDFTextStripper();
      textStripper.setSortByPosition(true);

      String s1 = textStripper.getText(document);
      String[] data = s1.split("\n");

      String dayDateLine = "^(Mon|Tue|Wed|Thu|Fri|Sat|Sun).[0-9]?[0-9].(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Sept|Oct|Nov|Dec).20[0-9]?[0-9].*";
      String timeLine = ".*[0-9]{1,2}:[0-9]{2}.*";
      String airportLine = ".*[(][A-Z]{3}[)].*";
      String passengerLine = "^[0-9]: .*";

      //Flags for flight info
      boolean flightComplete = false;
      boolean foundArrAirport = false;
      boolean foundDepAirport = false;
      boolean foundArrDate = false;
      boolean foundDepDate = false;
      boolean foundArrTime = false;
      boolean foundDepTime = false;
      boolean foundArrTerminal = false;
      boolean foundDepTerminal = false;
      
      boolean foundFName = false;
      boolean foundLName = false;
      boolean foundTix = false;
      boolean foundSeats = false;

      Map <String, String> flightCabins = new HashMap<>();

      String reservationCode = "";

      String itineraryMarker1 = "Booking reference:";
      String itineraryMarker2 = "Booking Reference:";
      String seatsMarker1 = "Seat number(s) requested:";
      String seatsMarker2 = "Seat Selection:";

      String seatKey ="";
      int numOfPass = 0;
      int numOfSeats = 0;

      StringBuilder notes = new StringBuilder();

      //FlightVO flightVO = null;
      String duration = "0hr00";
      String departAirport = null;
      String arriveAirport = null;
      String departTime = null;
      String arriveTime = null;
      String departTerminal = null;
      String arriveTerminal = null;
      String status = null;
      String flightNum = null;
      String cabin = null;
      String flightStops = null;
      String aircraftNo = null;
      String departDate = null;
      String arriveDate = null;
      String departCity = null;
      String departCountry = null;
      String arriveCity = null;
      String arriveCountry = null;

      //Passenger Fields
      String[] names = null;
      String passFullName = "";
      String title = "";
      String firstName = "";
      String midNames = "";
      String lastName = "";
      String ticketNo = "";
      String mealPref = "";
      String paymentCard = "";
      String specialNeeds = "";
      String flyerProg = "";
      String flyerProgNo = "";

      for (int i = 0; i < data.length; i++) {
        String s = data[i];


        if (s.contains("Economy")) {
          cabin = "Economy";
        } else if (s.contains("Business")) {
          cabin = "Business";
        } else if (s.contains("First")) {
          cabin = "First";
        }

        if (s.contains(itineraryMarker1) || s.contains(itineraryMarker2)) {
          String ref = null;
          if (s.contains(itineraryMarker1)) {
            ref = s.substring(s.indexOf(itineraryMarker1) + itineraryMarker1.length());
          }
          else {
            ref = s.substring(s.indexOf(itineraryMarker2) + itineraryMarker2.length(), s.indexOf("Customer"));
          }

          if (ref != null) {
            reservationCode = ref.trim();
          }
        }
        else if ((s.matches("^[A-Z]{2}[0-9]{2,}.*") || s.matches("^[0-9][A-Z][0-9]{2,}.*"))&& (i + 1) < data.length) {
          String[] flightDetails = s.split(" ");
          flightNum = flightDetails[0];
          if (flightNum.length() > 6) {
            flightNum = flightNum.substring(0, 6);
          }
          seatKey = flightNum;



          Matcher m = airportPattern.matcher(s);
          String firstAirport = null;
          String secondAirport = null;
          if (m.find()) {
            firstAirport = m.group().replace("(", "").replace(")", "");
            if (m.find()) {
              secondAirport = m.group().replace("(", "").replace(")", "");
            }
          }

          //we need to figure out which code is the depart airport and which one is the arrive airport
          //sometimes depart airport can end up in the second line because the name is too long
          if (firstAirport != null && secondAirport != null) {
            departAirport = firstAirport;
            arriveAirport = secondAirport;
            foundDepAirport = true;
            foundArrAirport = true;
          } else if (firstAirport != null) {
            //we check to see if there is another airport name following... another airport will be Uppercase char + lowercase char
            int airportIndex = s.indexOf(firstAirport) + firstAirport.length() + 1;
            String subString = s.substring(airportIndex).trim();
            if (subString.length() > 2) {
              if (Character.isAlphabetic(subString.charAt(0)) && Character.isUpperCase(subString.charAt(0)) &&  Character.isAlphabetic(subString.charAt(1)) && Character.isLowerCase(subString.charAt(1))) {
                departAirport = firstAirport;
                foundDepAirport = true;
              } else {
                arriveAirport = firstAirport;
                foundArrAirport = true;
              }
            } else {
              arriveAirport = firstAirport;
              foundArrAirport = true;
            }
          }


          if (!foundDepAirport || !foundArrAirport) {
            int j;
            for (j = 1; j < flightDetails.length; j++) {
              if (!foundDepAirport && flightDetails[j].matches("[(][A-Z]{3}[)]") && ((flightDetails.length - 1 - j >= 5))) {
                departAirport = flightDetails[j].replace("(", "").replace(")", "");
                foundDepAirport = true;
              } else if (!foundArrAirport && flightDetails[j].matches("[(][A-Z]{3}[)]") && (flightDetails.length - 1 - j <= 4)) {
                arriveAirport = flightDetails[j].replace("(", "").replace(")", "");
                foundArrAirport = true;
              } else if (flightDetails[j].matches("[A-Z0-9]{3}")) {
                aircraftNo = flightDetails[j];
                if (j + 1 < flightDetails.length)
                  cabin = flightDetails[j + 1];
                if (j + 2 < flightDetails.length)
                  status = flightDetails[j + 2];
                j = j + 2;

              }
            }
          }
          int j = i;
          while (!foundArrAirport || !foundDepAirport) {
            j++;
            if (j >=  data.length) {
              break;
            }
            m = airportPattern.matcher(data[j]);
              if (!foundArrAirport && !foundDepAirport) {
                if (m.find()) {
                  departAirport = m.group().replace("(", "").replace(")", "");
                  foundDepAirport = true;
                }
                if (m.find()) {
                  arriveAirport = m.group().replace("(", "").replace(")", "");
                  foundArrAirport = true;
                }
              }

              //look for arrival date
              else if (foundArrAirport && !foundDepAirport) {
                if (m.find()) {
                  departAirport = m.group().replace("(", "").replace(")", "");
                  foundDepAirport = true;
                }
              }

              else if (!foundArrAirport && foundDepAirport) {
                if (m.find()) {
                  arriveAirport = m.group().replace("(", "").replace(")", "");
                  foundArrAirport = true;
                }
              }
            }

        }
        else if (s.matches(dayDateLine)) {
          Matcher m = dayDatePattern.matcher(s);
          if (m.find()) {
            departDate = m.group().replace("-", " ");
            foundDepDate = true;
          }

          //look for arrival date
          if (m.find()) {
            arriveDate = m.group().replace("-", " ");
            foundArrDate = true;
          }

        }
        else if (s.matches(timeLine)) {
          int depInd, arrInd;

          Matcher m = timePattern.matcher(s);
          if (m.find()) {
            departTime = m.group();
            foundDepTime = true;
          }

          //look for arrival date
          if (m.find()) {
            arriveTime = m.group();
            foundArrTime = true;

          }

          String[] timeTerminal = s.replace("  ", " ").replace("- ", "").split(" ");
          depInd = Arrays.asList(timeTerminal).indexOf(departTime);
          arrInd = Arrays.asList(timeTerminal).indexOf(arriveTime);
          int k;
          int countTime = 0;
          for (k = 0; k < timeTerminal.length; k++) {
            if (timeTerminal[k].equalsIgnoreCase("Terminal") && k > depInd && k < arrInd) {
              departTerminal = timeTerminal[k].toUpperCase() + " " + timeTerminal[k+1];
              foundDepTerminal = true;
              foundArrTerminal = true;
            }
            else if (timeTerminal[k].equalsIgnoreCase("Terminal") && k > depInd && k > arrInd) {
              arriveTerminal = timeTerminal[k].toUpperCase() + " " + timeTerminal[k+1];
              foundArrTerminal = true;
              foundDepTerminal = true;
            }
            else if (!foundDepTerminal && !foundArrTerminal && k == timeTerminal.length - 1){
              foundDepTerminal = true;
              foundArrTerminal = true;
            }
          }

          //flight
          if (foundArrDate && foundDepDate && foundArrTime && foundDepTime && foundArrTerminal && foundDepTerminal ) {
            try {
              FlightVO flightVO = new FlightVO();
              flightVO.setArrivalAirport(new AirportVO());
              flightVO.setDepartureAirport(new AirportVO());
              flightVO.setReservationNumber(reservationCode);
              flightVO.setArrivalTime(getTimestamp(arriveDate, arriveTime));
              flightVO.setDepatureTime(getTimestamp(departDate, departTime));
              if (flightNum != null && flightNum.length() > 3) {
                flightVO.setCode(flightNum.substring(0, 2));
                flightVO.setNumber(flightNum.substring(2));
              } else
                flightVO.setNumber(flightNum);

              flightVO.setName(flightNum);
              if (arriveAirport != null && arriveAirport.trim().length() == 3) {
                flightVO.getArrivalAirport().setCode(arriveAirport);
              }
              flightVO.getArrivalAirport().setName(arriveAirport);
              flightVO.getArrivalAirport().setTerminal(arriveTerminal);
              flightVO.getArrivalAirport().setCity(arriveCity);
              flightVO.getArrivalAirport().setCountry(arriveCountry);
              if (departAirport != null && departAirport.trim().length() == 3) {
                flightVO.getDepartureAirport().setCode(departAirport);
              }
              flightVO.getDepartureAirport().setName(departAirport);
              flightVO.getDepartureAirport().setTerminal(departTerminal);
              flightVO.getDepartureAirport().setCity(departCity);
              flightVO.getDepartureAirport().setCountry(departCountry);
              if (aircraftNo != null && !aircraftNo.isEmpty()) {
                notes.append("Aircraft: ");
                notes.append(aircraftNo);
                notes.append("\n");
              }
              if (flightStops != null && !flightStops.isEmpty()) {
                notes.append("Stops: ");
                notes.append(flightStops);
                notes.append("\n");
              }
              flightVO.setNotes(notes.toString());

              flights.add(flightVO);
              if (cabin != null) {
                flightCabins.put(flightVO.getName(), cabin);
              }
              duration = "0hr00";
              departAirport = null;
              arriveAirport = null;
              departTime = null;
              arriveTime = null;
              departTerminal = null;
              arriveTerminal = null;
              status = null;
              flightNum = null;
              cabin = null;
              flightStops = null;
              aircraftNo = null;
              departDate = null;
              arriveDate = null;
              departCity = null;
              departCountry = null;
              arriveCity = null;
              arriveCountry = null;

            }catch (Exception e){
              e.printStackTrace();
            }
            foundArrAirport = false;
            foundDepAirport = false;
            foundArrDate = false;
            foundDepDate = false;
            foundArrTime = false;
            foundDepTime = false;
            foundArrTerminal = false;
            foundDepTerminal = false;
          }

        }
        else if (s.contains(seatsMarker1)) {
          String[] seatNos = s.substring(s.indexOf(':')+1).trim().split(" ");
          seatMap.put(seatKey, Arrays.asList(seatNos));
          foundSeats = true;
          numOfSeats = seatNos.length;
        }

        if (s.matches(passengerLine) && s.contains("Ticket Number:")) {
          passFullName = s.substring(s.indexOf(':')+1, s.indexOf(" : ")).trim();

          names = passFullName.split(" ");
          int p = 0, q;
          if (names[p].matches("(Mr|Mrs|Ms|Dr|Miss).*")){
            title = names[p];
            p++;
          }
          firstName = names[p++];
          foundFName = true;
          for (q = p; q < names.length - 1; q++){
            midNames += names[q];
          }
          lastName = names[q];
          foundLName = true;
          numOfPass++;


          ticketNo = s.substring(s.indexOf("Ticket Number:")+14).trim();
          foundTix = true;
        }
        else if (s.contains("Name:") && s.contains("Ticket")) {
          passFullName = s.substring(s.indexOf("Name: ")+5, s.indexOf("Ticket")).trim();

          names = passFullName.split(" ");
          int p = 0, q;
          if (names[p].matches("(Mr|Mrs|Ms|Dr|Miss).*")){
            title = names[p];
            p++;
          }
          firstName = names[p++];
          foundFName = true;
          for (q = p; q < names.length - 1; q++){
            midNames += names[q];
          }
          lastName = names[q];
          foundLName = true;
          numOfPass++;

          ticketNo = s.substring(s.indexOf("Ticket number:")+14).trim();
          foundTix = true;
        }
        else if (s.contains("Aeroplan") && s.contains("Meal Preference:")){
          flyerProgNo = s.substring(s.indexOf(" : ") + 3, s.indexOf("Meal")).trim();
          mealPref = s.substring(s.indexOf("Meal Preference:")+16);
        }
        else if (s.contains(seatsMarker2)) {
          String[] seatNos = s.substring(s.indexOf(':')+1).trim().split(",");
          for (String st : seatNos) {
            String[] seatSplit = st.trim().split(" ");
            if (seatMap.containsKey(seatSplit[0])){
              seatMap.get(seatSplit[0]).add(seatSplit[1]);
            }else {
              List<String> l = new ArrayList<>();
              l.add(seatSplit[1]);
              seatMap.put(seatSplit[0], l);
            }
          }
        }
        else if (s.startsWith("Frequent Flyer Pgm:")) {
          flyerProg = s.substring(s.indexOf(":")+1, s.indexOf("Program number:")).trim();
          flyerProgNo = s.substring(s.indexOf("Program number:") + "Program number:".length()).trim();
        }
        else if (s.startsWith("AERO REW TKT")){
          aeroRewTkt = s.substring(s.indexOf('/')+1);
          if (flyerProgNo == null) {
            flyerProgNo = aeroRewTkt;
          }
        }
        else if (s.startsWith("Passenger:")) {
          passFullName = s.substring(s.indexOf("M")).trim();

          names = passFullName.split(" ");
          int p = 0, q;
          if (names[p].matches("(Mr|Mrs|Ms|Dr|Miss).*")){
            title = names[p];
            p++;
          }
          firstName = names[p++];
          foundFName = true;
          for (q = p; q < names.length - 1; q++){
            midNames += names[q];
          }
          lastName = names[q];
          foundLName = true;
          numOfPass++;
        } else if (s.startsWith("Ticket number:")) {
          ticketNo = s.substring(s.indexOf("Ticket number:")+14).trim();
          foundTix = true;
        }
        if (foundFName && foundLName && foundTix) {

          for (FlightVO f : flights) {

            PassengerVO passenger = new PassengerVO();
            passenger.setTitle(title);
            passenger.setFirstName(firstName);
            passenger.setLastName(lastName);
            passenger.setFullName(passFullName);
            if (ticketNo != null && !ticketNo.isEmpty()) {
              passenger.seteTicketNumber(ticketNo);
            }
            if (flyerProgNo != null && !flyerProgNo.isEmpty()) {
              passenger.setFrequentFlyer(flyerProgNo);
            }
            passenger.setRank(rank);
            if (mealPref != null && !mealPref.isEmpty()) {
              passenger.setMeal(mealPref);
            }

            String cabin1 = flightCabins.get(f.getName());
            if (cabin1 != null) {
              passenger.setSeatClass(cabin1);
            }
            rank++;


            passengers.add(passenger);

            f.addPassenger(passenger);
          }

          foundFName = false;
          foundLName = false;
          foundTix = false;
          foundSeats = false;

          names = null;
          passFullName = "";
          title = "";
          firstName = "";
          midNames = "";
          lastName = "";
          ticketNo = "";
          mealPref = "";
          paymentCard = "";
          specialNeeds = "";
          flyerProg = "";
          flyerProgNo = "";
        }


      }

      for (FlightVO f : flights) {
        int c = 0;
        if (seatMap.containsKey(f.getName())) {
          while (seatMap != null && f.getName() != null && c < seatMap.get(f.getName()).size()) {
            if (f.getPassengers() != null && f.getPassengers().get(c) != null && seatMap.get(f.getName()).get(c) != null) {
              f.getPassengers().get(c).setSeat(seatMap.get(f.getName()).get(c));
              c++;
            }
          }
        }
      }

      for (FlightVO f : flights) {
        if (f.getDepartureAirport() != null && f.getDepatureTime() != null && f.getCode() != null) {
          trip.addFlight(f);
        }
      }



      trip.setImportSrc(BookingSrc.ImportSrc.AEROPLAN_PDF);
      trip.setImportSrcId(reservationCode);
      trip.setImportTs(System.currentTimeMillis());


      document.close();
      return trip;

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (input != null) {
        input.close();
      }
    }

    return null;
  }

  private  String getDepartDate (String s) {
    if (s != null) {
      int count = 0;
      Matcher m = dayDatePattern.matcher(s);
      while (m.find()) {
        if (count ==0 ) {
          String departDate = m.group();
          return departDate;
        }
        count++;
      }

      m = dayDatePattern1.matcher(s);
      while (m.find()) {
        if (count ==0 ) {
          String departDate = m.group();
          return departDate;
        }
        count++;
      }
    }

    //try the second pattern

    return null;

  }

  private  String getArrivalDate (String d, int offset) {
    if (d != null) {
      try {
        Timestamp t = new Timestamp(du.parseDate(d,dateTimeFormat).getTime());
        long newTimestamp = t.getTime() + (offset * 24 * 60 * 60 * 1000);

        String dateFormat = "E dd MMM yyyy";

        Date date = new Date(newTimestamp);

        DateTime dt = new DateTime(date);
        if (dt.getYear() == 70 || dt.getYear() == 1970) {
          return "";
        }

        SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
        return dateFormatter.format(date);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    return null;

  }

  private Timestamp getTimestamp (String date, String time) {
    try {
      if (date != null && date.contains("Sept")) {
        date = date.replace("Sept", "Sep");
      }
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime() );
      return t;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public ArrayList<ParseError> getParseErrors()
  {
    return null;
  }

  @Override
  public void postProcessingImpl(String tripId, String userId) {
  }

  public static void main(String[] args) {
    try {
      String dir = "/home/twong/Downloads/ETIR_10-Aug-17.pdf";//ETIR_6-Feb-16.pdf";//ETIR_10-Aug-17"/Volumes/data2/Downloads/Mickey - AC paid t ticket -  flight YYZ-FRA_ CDG-YYZ - as of Dec 9.pdf";//"/Volumes/data2/Downloads/ETIR_21-Dec-15 (1).pdf";
      File file = new File(dir);
      InputStream ins = new FileInputStream(file);

      //System.out.println(getPDFContentAsString(ins));


      AeroplanExtractor p = new AeroplanExtractor();
      p.initImpl(null);
      TripVO t = p.extractData(ins);
      System.out.println(t);



    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

}
