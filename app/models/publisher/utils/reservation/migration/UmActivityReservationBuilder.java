package models.publisher.utils.reservation.migration;

import com.umapped.persistence.reservation.activity.UmActivityReservation;

import models.publisher.ActivityBooking;
import models.publisher.TmpltActivity;
import models.publisher.TmpltDetails;
import models.publisher.TripDetail;

public class UmActivityReservationBuilder {

  public UmActivityReservation build(TripDetail entity, ActivityBooking activity) {
    UmActivityReservation reservation = new UmActivityReservation();
    
    if (activity != null) {
      reservation.getActivity().name = activity.name;
    }

    reservation.getActivity().getOrganizedBy().contactPoint = activity.contact;
    reservation.getActivity().umDocumentId = activity.destinationid;
    
    reservation.setNotesPlainText(entity.getComments());
    
    reservation.setStartDateTime(entity.getStarttimestamp());
    reservation.setEndDateTime(entity.getEndtimestamp());
    
    return reservation;
  }

  public UmActivityReservation build(TmpltDetails entity, TmpltActivity activity) {
    UmActivityReservation reservation = new UmActivityReservation();
    
    if (activity != null) {
      reservation.getActivity().name = activity.name;
    }

    reservation.getActivity().getOrganizedBy().contactPoint = activity.contact;
    reservation.getActivity().umDocumentId = activity.destinationid;
    
    reservation.setNotesPlainText(entity.getComments());
    
    reservation.setStartDateTime(entity.getStarttimestamp());
    reservation.setEndDateTime(entity.getEndtimestamp());
    
    return reservation;
  }
}
