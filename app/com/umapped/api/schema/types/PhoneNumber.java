package com.umapped.api.schema.types;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL) @Generated("org.jsonschema2pojo")
@JsonPropertyOrder({"phone-type", "privacy", "number"})
public class PhoneNumber implements Serializable{

  @JsonProperty("phone-type")
  private PhoneNumber.PhoneType phoneType;
  /**
   * Public - visible to the traveller, Private not visible to the traveller
   */
  @JsonProperty("privacy")
  private Privacy privacy;
  /**
   * Pattern from: http://stackoverflow.com/questions/2113908/what-regular-expression-will-match-valid
   * -international-phone-numbers
   */
  @JsonProperty("number")
  private String number;

  @Generated("org.jsonschema2pojo")
  public static enum PhoneType {

    MAIN("main"),
    TOLLFREE("tollfree"),
    DAY("day"),
    EVENING("evening"),
    NIGHT("night"),
    EMERGENCY("emergency"),
    BUSINESS("business"),
    PERSONAL("personal"),
    MOBILE("mobile"),
    FAX("fax"),
    EXT("extension");
    private static Map<String, PhoneType> constants = new HashMap<String, PhoneType>();

    static {
      for (PhoneNumber.PhoneType c : values()) {
        constants.put(c.value, c);
      }
    }

    private final String value;

    private PhoneType(String value) {
      this.value = value;
    }

    @JsonCreator
    public static PhoneNumber.PhoneType fromValue(String value) {
      PhoneNumber.PhoneType constant = constants.get(value);
      if (constant == null) {
        throw new IllegalArgumentException(value);
      }
      else {
        return constant;
      }
    }

    @JsonValue @Override
    public String toString() {
      return this.value;
    }

  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if ((obj == null) || (obj.getClass() != this.getClass())) {
      return false;
    }

    return this.hashCode() == obj.hashCode();
  }

  public PhoneNumber() {
  }

  @Override
  public int hashCode() {
    int hash = 1;

    if (privacy != null && privacy.hashCode() != 0) {
      hash *= privacy.hashCode();
    }

    if (phoneType != null && phoneType.hashCode() != 0) {
      hash *= phoneType.hashCode();
    }

    if (number != null && number.hashCode() != 0) {
      hash *= number.hashCode();
    }

    return hash;
  }

  public PhoneNumber(String number, PhoneType type, Privacy privacy) {
    this.number = number;
    this.privacy = privacy;
    this.phoneType = type;
  }

  @Override
  public String toString() {
    return "[" + privacy.name() + "@" + number + "]" + phoneType.name() + ";;";
  }

  /**
   * @return The phoneType
   */
  @JsonProperty("phone-type")
  public PhoneNumber.PhoneType getPhoneType() {
    return phoneType;
  }

  /**
   * @param phoneType The phone-type
   */
  @JsonProperty("phone-type")
  public void setPhoneType(PhoneNumber.PhoneType phoneType) {
    this.phoneType = phoneType;
  }

  public PhoneNumber withPhoneType(PhoneNumber.PhoneType phoneType) {
    this.phoneType = phoneType;
    return this;
  }

  /**
   * Public - visible to the traveller, Private not visible to the traveller
   *
   * @return The privacy
   */
  @JsonProperty("privacy")
  public Privacy getPrivacy() {
    return privacy;
  }

  /**
   * Public - visible to the traveller, Private not visible to the traveller
   *
   * @param privacy The privacy
   */
  @JsonProperty("privacy")
  public void setPrivacy(Privacy privacy) {
    this.privacy = privacy;
  }

  public PhoneNumber withPrivacy(Privacy privacy) {
    this.privacy = privacy;
    return this;
  }

  /**
   * Pattern from: http://stackoverflow.com/questions/2113908/what-regular-expression-will-match-valid
   * -international-phone-numbers
   *
   * @return The number
   */
  @JsonProperty("number")
  public String getNumber() {
    return number;
  }

  /**
   * Pattern from: http://stackoverflow.com/questions/2113908/what-regular-expression-will-match-valid-international
   * -phone-numbers
   *
   * @param number The number
   */
  @JsonProperty("number")
  public void setNumber(String number) {
    this.number = number;
  }

  public PhoneNumber withNumber(String number) {
    this.number = number;
    return this;
  }


}
