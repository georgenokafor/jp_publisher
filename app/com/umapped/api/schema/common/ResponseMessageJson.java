package com.umapped.api.schema.common;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.JsonNode;
import com.umapped.api.schema.types.ReturnCode;
import play.libs.Json;

/**
 * Created by surge on 2014-10-28.
 */
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseMessageJson {
  /**
   * Request message header
   */
  public ResponseHeaderJson header;

  /**
   * Request message body
   */
  @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY)
  public IBodyJson body;

  public ResponseMessageJson() {}

  /**
   * Builds response header based on the request header
   * @param reqHeader
   */
  public ResponseMessageJson(RequestHeaderJson reqHeader) {
    if (reqHeader != null) {
      header = new ResponseHeaderJson(reqHeader.getReqId(), ReturnCode.SUCCESS);
    }
  }

  /**
   * Builds response header based on the request header
   * @param reqHeader
   */
  public ResponseMessageJson(RequestHeaderJson reqHeader, ReturnCode rc) {
    if (reqHeader != null) {
      header = new ResponseHeaderJson(reqHeader.getReqId(), rc);
    } else {
      header = new ResponseHeaderJson("ITINERARY", rc);
    }
  }

  public ResponseMessageJson(String requestId, ReturnCode rc) {
    header = new ResponseHeaderJson(requestId, rc);
  }

  public JsonNode toJson() {
    return Json.toJson(this);
  }
}
