package com.mapped.publisher.form;

import play.data.validation.Constraints;

/**
 * Created by surge on 2015-12-23.
 */
public class UserProfileForm {
  Long inAccountId;
  @Constraints.Required
  String inUserId;
  @Constraints.Email
  String inUserEmail;
  //@Constraints.Required
  String inUserFirstName;
  //@Constraints.Required
  String inUserLastName;
  String inUserPhone;
  String inUserMobile;

  /* Phones */
  String inUserFax;
  String inUserFacebook;
  String inUserTwitter;
  String inUserExtension;

  /* Social */
  String inUserWeb;

  public Long getInAccountId() {
    return inAccountId;
  }

  public void setInAccountId(Long inAccountId) {
    this.inAccountId = inAccountId;
  }

  public String getInUserId() {
    return inUserId;
  }

  public void setInUserId(String inUserId) {
    this.inUserId = inUserId;
  }

  public String getInUserEmail() {
    return inUserEmail;
  }

  public void setInUserEmail(String inUserEmail) {
    this.inUserEmail = inUserEmail;
  }

  public String getInUserFirstName() {
    return inUserFirstName;
  }

  public void setInUserFirstName(String inUserFirstName) {
    this.inUserFirstName = inUserFirstName;
  }

  public String getInUserLastName() {
    return inUserLastName;
  }

  public void setInUserLastName(String inUserLastName) {
    this.inUserLastName = inUserLastName;
  }

  public String getInUserPhone() {
    return inUserPhone;
  }

  public void setInUserPhone(String inUserPhone) {
    this.inUserPhone = inUserPhone;
  }

  public String getInUserMobile() {
    return inUserMobile;
  }

  public void setInUserMobile(String inUserMobile) {
    this.inUserMobile = inUserMobile;
  }

  public String getInUserFax() {
    return inUserFax;
  }

  public void setInUserFax(String inUserFax) {
    this.inUserFax = inUserFax;
  }

  public String getInUserFacebook() {
    return inUserFacebook;
  }

  public void setInUserFacebook(String inUserFacebook) {
    this.inUserFacebook = inUserFacebook;
  }

  public String getInUserTwitter() {
    return inUserTwitter;
  }

  public void setInUserTwitter(String inUserTwitter) {
    this.inUserTwitter = inUserTwitter;
  }

  public String getInUserWeb() {
    return inUserWeb;
  }

  public void setInUserWeb(String inUserWeb) {
    this.inUserWeb = inUserWeb;
  }

  public String getInUserExtension() {
    return inUserExtension;
  }

  public void setInUserExtension(String inUserExtension) {
    this.inUserExtension = inUserExtension;
  }
}
