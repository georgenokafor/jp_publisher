package com.umapped.persistence.reservation.cruise;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.umapped.persistence.reservation.UmReservation;
import com.umapped.persistence.reservation.UmTraveler;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;
/**
 * Created by surge on 2016-11-25.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmCruiseReservation
    extends UmReservation {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  UmCruiseShip       cruise;
  List<UmCruiseStop> stops;

  public UmCruiseShip getCruise() {
    if (cruise == null) {
      cruise = new UmCruiseShip();
    }
    return cruise;
  }

  public UmCruiseReservation setCruise(UmCruiseShip cruise) {
    this.cruise = cruise;
    return this;
  }

  public List<UmCruiseStop> getStops() {
    return stops;
  }

  public UmCruiseReservation setStops(List<UmCruiseStop> stops) {
    this.stops = stops;
    return this;
  }
  
  /**
   * As we currently only have only one traveler's info,
   * this provides a convenient method to return the first traveler in the list
   * @return
   */
  @JsonIgnore
  public UmCruiseTraveler getCruiseTraveler() {
    List<UmTraveler> travelers = getTravelers();
    if (travelers == null) {
      travelers = new ArrayList<UmTraveler>(1); 
      setTravelers(travelers);
    }
    if (travelers.size() == 0) {
      travelers.add(new UmCruiseTraveler());
    }
    return cast(travelers.get(0), UmCruiseTraveler.class);   
  }
}
