package com.mapped.publisher.view;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2015-02-27.
 */
public class DataTablesView<DataRowType> {
  public static class BaseRow {
    public String DT_RowId;
    public String DT_RowClass;
  };

  public int draw;
  public int recordsTotal;
  public int recordsFiltered;
  public List<DataRowType> data;
  public String error;
  public String searchTerm;
  public int selectStatus;
  public boolean allTrips;
  public boolean searchArchives;
  public boolean inSearchTitleOnly;
  public int filterDate;
  public int destType;
  public int filterConsortium;


  public DataTablesView(int draw) {
    this.draw = draw;
    data = new ArrayList<>();
  }
}
