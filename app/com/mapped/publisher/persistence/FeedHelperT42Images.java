package com.mapped.publisher.persistence;

import com.amazonaws.services.s3.model.CopyObjectResult;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import models.publisher.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Logic to handle t42 image feed
 * Created by surge on 2016-11-04.
 */
public class FeedHelperT42Images
    extends FeedHelperPOI {

  public final static String ENSEMBLE_T42_FEED_NAME = "Travel42 Image Feed";
  public final static String ENSEMBLE_T42_IMAGES_S3_PREFIX = "feeds/T42Images/";
  private static Pattern imagePattern = Pattern.compile(ENSEMBLE_T42_IMAGES_S3_PREFIX +
                                                        "([0-9]+)_([0-9]+)_(.+)\\.jpg$",
                                                        Pattern.CASE_INSENSITIVE);
  private final static boolean DEBUG = true;

  @Override public List<FeedResource> getResources() {
    List<FeedResource> result = new ArrayList<>();

    FeedResource fr = new FeedResource();
    fr.setDate(Date.from(Instant.now()));
    fr.setName(ENSEMBLE_T42_IMAGES_S3_PREFIX);
    fr.setOrigin(FeedResource.Origin.AWS_S3);
    fr.setSize(-1l);
    result.add(fr);

    return result;
  }

  @Override public Map<PoiFeedUtil.FeedResult, Integer> load(String basePath) {
    Map<PoiFeedUtil.FeedResult, Integer> results = new HashMap<>();
    Account acc = Account.find.byId(Account.AID_PUBLISHER);
    FileSrc src = FileSrc.find.byId(FileSrc.FileSrcType.IMG_VENDOR_T42);
    String bucket = src.getRealBucketName();
    Map<String, LstFileType> fileTypes = new HashMap<>();
    fileTypes.put("jpg", LstFileType.find.byId("jpg"));
    fileTypes.put("png", LstFileType.find.byId("png"));

    AtomicInteger imageCounter = new AtomicInteger(0);
    AtomicInteger errorCounter = new AtomicInteger(0);
    S3Util.forEachObject(S3Util.getPDFBucketName(), ENSEMBLE_T42_IMAGES_S3_PREFIX, (S3ObjectSummary c) -> {
      //We need no chance of this procedure crashing
      try {
        if(c.getKey().equals(ENSEMBLE_T42_IMAGES_S3_PREFIX)) {
          return;
        }
        //During debug only first 10 images are transfered
        if(DEBUG && (imageCounter.get() > 10 || errorCounter.get() > 10)) {
          //return;
        }
        Matcher m = imagePattern.matcher(c.getKey());
        if(m.matches()) {
          Integer geoPlaceKey = Integer.parseInt(m.group(1));
          Integer t42ImageId = Integer.parseInt(m.group(2));

          String destinationName = m.group(3).replace('_', ' ');
          String filename = c.getKey().substring(ENSEMBLE_T42_IMAGES_S3_PREFIX.length());
          String filenameNormalized = S3Util.normalizeFilename(filename);
          String ext = FilenameUtils.getExtension(filename).toLowerCase();

          //. Check if we have image for this destination already
          T42DestinationImage dImg = T42DestinationImage.findByDestinationAndId(geoPlaceKey, t42ImageId);

          //. If image with for this destination and this id exists then just update that we saw it
          if(dImg != null) {
            dImg.markSeen();
            if(DEBUG) {
              Log.debug("FeedHelperT42Images:load(): already imported:" + filename);
            }
            return; //Returning since nothing to do here
          }

          //. Checking if this image is already in the database
          FileInfo dup = FileInfo.byHashAndBucket(c.getETag(), bucket);
          if(dup != null) {
            //We already have file with this hash in this bucket but not destination, let's link existing
            FileImage image = FileImage.find.byId(dup.getPk());
            dImg = T42DestinationImage.build(geoPlaceKey, t42ImageId, image);
            dImg.markSeen();
            dImg.save();
            if(DEBUG) {
              Log.debug("FeedHelperT42Images:load(): already in S3:" + filename + " but not in T42DestinationImage");
            }
            return; //Returning since record was created now
          }


          //Finally time to build new record as file is not present and is not associated with anything
          //New image, it never expires
          FileImage image = FileImage.build(acc, src, 0);
          FileInfo file = image.getFile();
          dImg = T42DestinationImage.build(geoPlaceKey, t42ImageId, image);

          //Do we have other images for this destination?
          T42DestinationImage prevDImage = T42DestinationImage.findHighestRanked(geoPlaceKey);
          if(prevDImage == null) {
            dImg.setRank(0);
          } else {
            dImg.setRank(prevDImage.getRank() + 1); //New images always have higher rank
          }

          //Now populating all needed fields
          image.setPublic(false);
          image.setProcessed(false);
          image.setType(FileImage.ImageType.ORIGINAL);
          image.setSearchTerm(destinationName);

          file.setFiletype(fileTypes.get(ext));
          file.setHash(c.getETag());
          file.setFilesize(c.getSize());
          file.setSearchWords(destinationName);
          file.setFilename(filename);

          String newKey = ImageHelper.getImageKey(image,filenameNormalized, acc);
          file.setFilepath(newKey);
          file.setUrl(S3Util.generatePublicUrl(bucket, newKey));


          //Copied object should have long shelf (cache) life as it just an image
          ObjectMetadata om = new ObjectMetadata();
          S3Util.setCacheControl(om, TimeUnit.DAYS.toSeconds(365), false, true); //T42 Image will never get updated

          CopyObjectResult res = S3Util.s3CopyObject(c.getBucketName(), c.getKey(), bucket, newKey, om);
          if (res != null) {
            if (S3Util.setObjectPublic(bucket, newKey)) {
              image.superSave();
              dImg.save();
              imageCounter.incrementAndGet();
              Log.info("FeedHelperT42Images:load(): imported file:" + filename + " as: " + file.getUrl());
              return;
            }
            Log.err("FeedHelperT42Images:load(): Failed to set Object public with URL: " + file.getUrl());
          }
          else {
            Log.err("FeedHelperT42Images:load():  Failed to copy T42 image: " + c.getKey());
          }
        } else {
          Log.err("FeedHelperT42Images:load(): Image is not matched:" + c.getKey() + " Pattern: "+ imagePattern.pattern());
        }

      }catch (Exception e) {
        Log.err("FeedHelperT42Images:load(): Exception:", e);
      }
      errorCounter.incrementAndGet();
    });

    results.put(PoiFeedUtil.FeedResult.SUCCESS, imageCounter.get());
    results.put(PoiFeedUtil.FeedResult.ERROR, errorCounter.get());
    return results;
  }

  @Override public PoiImportRS.ImportState associate(PoiImportRS rs) {
    return null;
  }

  @Override public PoiSrcFeed.FeedSyncResult synchronize(PoiSrcFeed psf) {
    return null;
  }

  @Override public List<Pair<PoiRS, Integer>> getRankedPois(PoiImportRS importRS, boolean ignoreCountry) {
    return null;
  }

  @Override public PoiSrcFeed.FeedSyncResult clearAmenities(PoiSrcFeed psf) {
    return null;
  }

}
