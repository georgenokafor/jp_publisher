package controllers;

import com.google.inject.Inject;
import com.mapped.common.CoreConstants;
import com.mapped.common.EmailMgr;
import com.mapped.persistence.Users;
import com.mapped.persistence.UsersMgr;
import com.mapped.persistence.UsersMgrExt;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.form.ResetPwdForm;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.BaseView;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-06-12
 * Time: 9:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class MobileUserController
    extends Controller {

  @Inject
  FormFactory formFactory;

  public Result resetPwd() {
    BaseView view = new BaseView();

    return ok(views.html.mobile.resetMobilePwd.render(view));
  }

  public Result resetMobilePwd() {
    //bind html form to form bean
    Form<ResetPwdForm> form = formFactory.form(ResetPwdForm.class).bindFromRequest();
    //use bean validator framework
    if (form.hasErrors()) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.mobile.resetMobilePwd.render(baseView));
    }

    try {
      ResetPwdForm resetInfo = form.get();

      Users u = UsersMgrExt.getByEmail(resetInfo.getInEmail().toLowerCase());
      if (u != null && u.getStatus() != APPConstants.STATUS_DELETED) {
        String resetId = Utils.getUniqueId();
        u.setAuthCode(resetId);
        UsersMgr.update(u);

        //send email
        try {

          String smtp         = ConfigMgr.getAppParameter(CoreConstants.EMAIL_SMTP_HOST);
          String user         = ConfigMgr.getAppParameter(CoreConstants.EMAIL_SMTP_USER_ID);
          String pwd          = ConfigMgr.getAppParameter(CoreConstants.EMAIL_SMTP_PASSWORD);
          String fromEmail    = ConfigMgr.getAppParameter(CoreConstants.EMAIL_PWD_RESET_FROM);
          String subject      = ConfigMgr.getAppParameter(CoreConstants.EMAIL_MOBILE_PWD_RESET_SUBJECT);
          String hostUrl      = ConfigMgr.getAppParameter(CoreConstants.HOST_URL);
          String emailEnabled = ConfigMgr.getAppParameter(CoreConstants.EMAIL_ENABLED);

          if (emailEnabled != null && emailEnabled.equalsIgnoreCase("Y")) {
            //initialize email mgr
            EmailMgr emailMgr = new EmailMgr(smtp, user, pwd);
            emailMgr.init();
            List<Map<String, String>> body  = new ArrayList<Map<String, String>>();
            Map<String, String>       token = new HashMap<String, String>();
            token.put(CoreConstants.TEXT, "Your password has been reset");
            //body.add(token);

            token = new HashMap<String, String>();
            token.put(CoreConstants.HTML,
                      "<p>You have requested a password reset.</p> <p> Click <a href='" + hostUrl +
                      "/mobile/resetMobilePwdLink?inLinkId=" + resetId + "' > here </a> to reset your password </p>" +
                      " <p>If you did not reset your password, please contact our support desk at help@umapped.com" +
                      ".</p>");
            body.add(token);

            token = new HashMap<>();
            token.put(CoreConstants.TEXT,
                      "If you did not reset your password, please contact our support desk at help@umapped.com.");
            //body.add(token);

            emailMgr.send(fromEmail, "UMapped", resetInfo.getInEmail(), subject, body);
          }
        }
        catch (Exception e) {
          BaseView view1 = new BaseView();
          view1.message = "System Error - please retry.";
          Log.log(LogLevel.ERROR, "resetPwd - Cannot send email. " + resetInfo.getInEmail(), e);

          return ok(views.html.mobile.resetMobilePwd.render(view1));
        }


        BaseView view1 = new BaseView();
        view1.message = "You will receive an email to complete your password reset shortly.";
        return ok(views.html.mobile.resetMobilePwd.render(view1));
      }
    }
    catch (Exception e) {
      BaseView view1 = new BaseView();
      view1.message = "Invalid email - please retry or contact us.";
      return ok(views.html.mobile.resetMobilePwd.render(view1));
    }

    BaseView view1 = new BaseView();
    view1.message = "Invalid email/password combination - please retry";
    return ok(views.html.mobile.resetMobilePwd.render(view1));
  }

  public Result resetMobilePwdLink() {
    //bind html form to form bean
    Form<ResetPwdForm> form = formFactory.form(ResetPwdForm.class).bindFromRequest();

    //use bean validator framework
    if (form.hasErrors()) {
      BaseView baseView = new BaseView();
      baseView.message = "Invalid Link.";
      return ok(views.html.mobile.resetMobilePwdDone.render(baseView));
    }

    try {
      ResetPwdForm resetInfo = form.get();
      if (resetInfo.getInLinkId() != null) {
        Users u = UsersMgrExt.getByAuthCode(resetInfo.getInLinkId());
        if (u != null) {
          BaseView baseView = new BaseView();
          baseView.message = "Please change your password.";
          baseView.linkId = resetInfo.getInLinkId();
          return ok(views.html.mobile.resetMobilePwdLink.render(baseView));
        }
      }


    }
    catch (Exception e) {
      BaseView view1 = new BaseView();
      view1.message = "Invalid link - please retry or contact your administrator.";
      return ok(views.html.mobile.resetMobilePwdDone.render(view1));

    }

    BaseView view1 = new BaseView();
    view1.message = "Invalid link - please retry or contact your administrator.";
    return ok(views.html.mobile.resetMobilePwdDone.render(view1));
  }

  public Result resetMobileNewPwd() {
    //bind html form to form bean
    Form<ResetPwdForm> form = formFactory.form(ResetPwdForm.class).bindFromRequest();
    if(form.hasErrors()) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.mobile.resetMobilePwdDone.render(baseView));
    }

    try {
      ResetPwdForm resetInfo = form.get();

      if (resetInfo.getInLinkId() != null && resetInfo.getInLinkId().trim().length() > 10) {
        Users u = UsersMgrExt.getByAuthCode(resetInfo.getInLinkId());
        if (u != null) {
          if (resetInfo.getInNewPwd() != null &&
              (resetInfo.getInNewPwd().length() < 6 || resetInfo.getInNewPwd().length() > 20)) {
            BaseView view1 = new BaseView();
            view1.linkId = resetInfo.getInLinkId();
            view1.message = "Password must be between 6 and 20 characters.";
            return ok(views.html.mobile.resetMobilePwdLink.render(view1));
          }
          else if (resetInfo.getInConfirmPwd() == null ||
                   resetInfo.getInNewPwd() == null ||
                   !resetInfo.getInConfirmPwd().equals(resetInfo.getInNewPwd())) {
            BaseView view1 = new BaseView();
            view1.linkId = resetInfo.getInLinkId();
            view1.message = "Your passwords do not match. Please retry.";
            return ok(views.html.mobile.resetMobilePwdLink.render(view1));
          }
          else {
            try {
              u.setPwd(Utils.hashPwd(resetInfo.getInNewPwd(), Long.toString(u.getCreateTimestamp())));
              u.setAuthCode("");
              u.setLastUpdateTimestamp(System.currentTimeMillis());
              u.setModifiedBy("System");
              UsersMgr.update(u);
              BaseView view1 = new BaseView();
              view1.message = "Your password has been changed.";
              return ok(views.html.mobile.resetMobilePwdDone.render(view1));

            }
            catch (Exception e) {
              e.printStackTrace();
            }
          }

        }
      }


    }
    catch (Exception e) {
      BaseView view1 = new BaseView();
      view1.message = "Invalid link - please retry.";
      return ok(views.html.mobile.resetMobilePwdDone.render(view1));

    }



    BaseView view1 = new BaseView();
    view1.message = "Invalid link - please retry.";
    return ok(views.html.mobile.resetMobilePwdDone.render(view1));

  }

}
