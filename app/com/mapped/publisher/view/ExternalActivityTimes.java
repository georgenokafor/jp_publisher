package com.mapped.publisher.view;

/**
 * Created by twong on 15-06-09.
 */
public class ExternalActivityTimes extends BaseView{
  public String code;
  public String startTime;
  public String endTime;
  public float duration;
  public String description;
}
