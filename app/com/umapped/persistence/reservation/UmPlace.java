package com.umapped.persistence.reservation;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmPlace
    extends UmThing {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Physical address of the item.
   */
  public UmPostalAddress address;

  public String aggregateRating;

  public UmPlace containedIn;

  public String faxNumber;

  public UmGeoCoordinates geo;

  public String globalLocationNumber;

  public String hasMap;

  public String interactionCount;

  public String isicV4;

  public UmImageObject logo;

  public String map;

  public String maps;

  public String openingHoursSpecific;

  /**
   * A photograph of this place. Supersedes photos.
   * photo  Photograph  or ImageObject
   */
  public List<UmImageObject> photo;

  public String review;

  public String reviews;

  /**
   * [Text] The telephone number.
   */
  public String telephone;
  public String email;

  public String getContactInfo() {
    StringBuilder sb = new StringBuilder();
    if (this.telephone != null && this.telephone.trim().length() > 0) {
      sb.append("Telephone: ");
      sb.append(this.telephone);
      sb.append("\n");
    }
    if (this.faxNumber != null && this.faxNumber.trim().length() > 0) {
      sb.append("Fax: ");
      sb.append(this.faxNumber);
      sb.append("\n");
    }
    if (this.email != null && this.email.trim().length() > 0) {
      sb.append("Email: ");
      sb.append(this.telephone);
      sb.append("\n");
    }
    return sb.toString();
  }
}
