package com.mapped.publisher.persistence;

import com.mapped.publisher.utils.Log;
import com.mapped.publisher.view.FileView;
import org.apache.commons.lang3.StringUtils;

import java.net.URLDecoder;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by george on 2017-02-17.
 */
public class FileMgr {

  private static String USER_PAGE_ATTACH_SEARCH = "select distinct fileid, destinationguideid, name, attachname, attachUrl, comments, createdby," +
      "file_pk from destination_guide_attach where " +
      "attachtype = '0' and status = 0 and destinationguideid in (select g.destinationguideid " +
      "from destination_guide g, destination d where d" +
      ".cmpyid = ANY(?) and d.status = 0 and d.destinationid = g.destinationid and g" +
      ".status = 0 and (d.access_level = 'PUB' or (d.access_level = 'PRV' and d.createdby = ?))) and (upper(attachname) like ? or upper(comments) like ?) limit 20;";

  private static String ADMIN_PAGE_ATTACH_SEARCH = "select distinct fileid, destinationguideid, name, attachname, attachUrl, comments, createdby," +
      "file_pk from destination_guide_attach where " +
      "attachtype = '0' and status = 0 and destinationguideid in (select g.destinationguideid " +
      "from destination_guide g, destination d where d" +
      ".cmpyid = ANY(?) and d.status = 0 and d.destinationid = g.destinationid and g" +
      ".status = 0) and (upper(attachname) like ? or upper(comments) like ?) limit 20;";


  public static HashMap<String, FileView> searchFiles(String keyword, List<String> cmpies, String user, Connection conn, boolean isCmpyAdmin)
      throws SQLException {

    HashMap<String, FileView> results = new HashMap<>();
    if (keyword == null) {
      return results;
    }


    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;

    String[] searchParts = StringUtils.split(keyword, "+ ");
    String searchVector = StringUtils.join(searchParts,'&');

    keyword = keyword.replace("+", "%");
    keyword = keyword.replace(" ", "%");

    keyword = keyword.toUpperCase();
    keyword = "%" + keyword + "%";

    try {
      if (isCmpyAdmin) {
        prep = conn.prepareStatement(ADMIN_PAGE_ATTACH_SEARCH);
        prep.setArray(1, conn.createArrayOf("text", cmpies.toArray()));
        prep.setString(2, keyword);
        prep.setString(3, keyword);
      } else {
        prep = conn.prepareStatement(USER_PAGE_ATTACH_SEARCH);
        prep.setArray(1, conn.createArrayOf("text", cmpies.toArray()));
        prep.setString(2, user);
        prep.setString(3, keyword);
        prep.setString(4, keyword);
      }





      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          String name = rs.getString("name");
          String attachUrl = rs.getString("attachUrl");
          if (name != null && attachUrl != null) {
            FileView view = new FileView();
            view.attachId = rs.getString("fileid");
            view.title = name;
            view.pageId = rs.getString("destinationguideid");
            view.attachUrl = attachUrl;
            view.note = rs.getString("comments");
            view.createdBy = rs.getString("createdby");
            view.origFileId = rs.getLong("file_pk");
            view.fileName = rs.getString("attachName");
            results.put(name, view);
          }
        }
        rs.close();
        prep.close();
      }

    }
    catch(Exception e) {
      Log.err("Error while searching for files:" + e.getMessage());
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
    return results;
  }

}
