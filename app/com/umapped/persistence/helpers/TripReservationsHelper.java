package com.umapped.persistence.helpers;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.parse.schemaorg.*;
import com.mapped.publisher.persistence.CountriesInfo;
import com.mapped.publisher.persistence.PoiRS;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.migration.HotelMigrationViewUtil;
import com.umapped.api.schema.types.Address;
import com.umapped.api.schema.types.Coordinates;
import com.umapped.api.schema.types.Email;
import com.umapped.api.schema.types.PhoneNumber;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.UmPostalAddress;
import com.umapped.persistence.reservation.UmRate;
import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;
import com.umapped.persistence.reservation.accommodation.UmAccommodationTraveler;
import com.umapped.persistence.reservation.accommodation.UmLodgingBusiness;
import com.umapped.persistence.reservation.activity.UmActivityReservation;
import com.umapped.persistence.reservation.cruise.UmCruiseReservation;
import com.umapped.persistence.reservation.cruise.UmCruiseTraveler;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.flight.UmFlightTraveler;
import com.umapped.persistence.reservation.transfer.UmTransferReservation;

import controllers.BookingController;
import controllers.BookingNoteController;
import controllers.PoiController;
import models.publisher.*;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;


/**
 * Helper class to assist transforming Umapped Models to Schema.org Reservations
 * Created by surge on 2016-06-27.
 */
public class TripReservationsHelper {

  Trip             trip;
  List<TripDetail> bookings;
  List<TripNote>   notes;

  public static TripReservationsHelper build() {
    TripReservationsHelper rh = new TripReservationsHelper();
    return rh;
  }

  public List<TripNote> getNotes() {
    return notes;
  }

  public TripReservationsHelper setNotes(List<TripNote> notes, Trip trip) {
    //add filter logic for magnated
    this.notes = filterMagnatech(notes, trip);
    return this;
  }

  public List<TripDetail> getBookings() {
    return bookings;
  }

  public TripReservationsHelper setBookings(List<TripDetail> bookings) {
    this.bookings = bookings;
    return this;
  }

  public Trip getTrip() {
    return trip;
  }

  public TripReservationsHelper setTrip(Trip trip) {
    this.trip = trip;
    return this;
  }

  public ReservationPackage prepareReservationPackage(boolean sorted) {
    List<TripNote> lastNotes = new ArrayList<>();
    ReservationPackage rp = new ReservationPackage();

    rp.umId = trip.getTripid();
    rp.creator = new Organization();
    rp.creator.umId = trip.getCmpyid();             //OLD style Company ID
    rp.creator.contactPoint = trip.getCreatedby();  //More thinking about this field needed

    rp.subReservation = new ArrayList<>();

    if (bookings != null) {
      bookings.forEach((td) -> rp.subReservation.add(tripDetailToReservation(td)));
    }

    if (notes != null) {
      for (TripNote note : notes) {
        if (note.getTag() != null && note.getTag().contains(APPConstants.NOTE_TAG_LAST_ITEM)) {
          lastNotes.add(note);
        } else {
          rp.subReservation.add(tripNoteToReservationNote(note));
        }
      }
    }

    if (sorted) {
      sort(rp);
    }

    if (lastNotes != null && lastNotes.size() > 0) {
      ListItem lastItem = rp.sorted.itemListElement.get(rp.sorted.itemListElement.size() -1);
      for (TripNote tn: lastNotes) {
        ListItem item = new ListItem();
        Reservation r = tripNoteToReservationNote(tn);
       // item.previousItem = lastItem;
        item.item = r;
        item.group = lastItem.group;
        item.position = lastItem.position + 1;
        lastItem.nextItem = item;

        lastItem = item;
        rp.sorted.addListItem(item);


      }
    }

    return rp;
  }

  private Reservation findReservationById(ReservationPackage rp, String id) {
    for(Reservation r: rp.subReservation) {
      if(r.umId.equals(id)) {
        return r;
      }
    }
    return null;
  }

  /**
   * Transforms reservation package from unordered list into sorted (ordered)
   *
   * @param rp
   */
  public void sort(ReservationPackage rp) {
    rp.sorted = new ItemList();

    //preprocessing to move bookings that belong after a cross time zone flight that went back a day
    //these bookings initially will be before the flight on the day before
    List<Reservation> crossDateFlights = new ArrayList<>();
    Map<Reservation, DateTime> origTaggedReservations = new HashMap<>();

    for(Reservation r : rp.subReservation) {
      if (r.additionalType.equals("https://schema.org/FlightReservation") && r.isEndBeforeStart()
          && r.getStartDateTime() != null && r.getFinishDateTime() != null
          && !r.getStartDateTime().getISODate().equals(r.getFinishDateTime().getISODate())) {
        crossDateFlights.add(r);
      }
    }
    if (crossDateFlights.size() > 0) {
      for (Reservation cr: crossDateFlights) {
        List<Reservation> taggedReservations = new ArrayList<>();
        for(Reservation r : rp.subReservation) {
          if (!r.additionalType.equals("https://umapped.com/NoteReservation")
              && r.tag != null && r.tag.contains(APPConstants.TRIP_TAG_CROSS_DATE_FLIGHT)
              && r.getStartDateTime() != null
              && (r.getStartDateTime().getISODate().equals(cr.getFinishDateTime().getISODate())
                  || r.getStartDateTime().getISODate().equals(cr.getStartDateTime().getISODate()))) {
            taggedReservations.add(r);
            origTaggedReservations.put(r, r.getStartDateTime());
          }
        }
        if (taggedReservations.size() > 0) {
          taggedReservations.sort((l, r) -> {
            //If we are in different groups
            if (l.getStartDateTime() != null && r.getStartDateTime() != null) {
              return l.getStartDateTime().getTimestamp().compareTo(r.getStartDateTime().getTimestamp()); //Dates are in ISO format - hence sortable, not most efficient but works
            }
            return 0;
          });
          //now that they are sorted, we are going to use the crosss flight departure time and increment it by 1 tick for each so
          //all the tagged reservations will be sorted in order after the cross flight
          int i = 1;
          for (Reservation r: taggedReservations) {
            LocalDateTime newDateTime = cr.getStartDateTime().getTime().plusMinutes(i++);
            if (r instanceof LodgingReservation) {
              LodgingReservation lr = (LodgingReservation) r;
              lr.checkinTime.setTime(newDateTime);
            } else if (r instanceof TransferReservation) {
              TransferReservation lr = (TransferReservation) r;
              lr.reservationFor.pickupTime.setTime(newDateTime);
            } else if (r instanceof ActivityReservation) {
              ActivityReservation lr = (ActivityReservation) r;
              lr.startTime.setTime(newDateTime);
            } else if (r instanceof CruiseShipReservation) {
              CruiseShipReservation lr = (CruiseShipReservation) r;
              lr.reservationFor.departTime.setTime(newDateTime);
            } else if (r instanceof RentalCarReservation) {
              RentalCarReservation lr = (RentalCarReservation) r;
              lr.pickupTime.setTime(newDateTime);
            } else if (r instanceof TaxiReservation) {
              TaxiReservation lr = (TaxiReservation) r;
              lr.pickupTime.setTime(newDateTime);
            } else if (r instanceof EventReservation) {
              EventReservation lr = (EventReservation) r;
              lr.reservationFor.startDate.setTime(newDateTime);
            } else if (r instanceof TrainReservation) {
              TrainReservation lr = (TrainReservation) r;
              lr.reservationFor.departTime.setTime(newDateTime);
            } else if (r instanceof BusReservation) {
              BusReservation lr = (BusReservation) r;
              lr.reservationFor.departureTime.setTime(newDateTime);
            } else if (r instanceof FoodEstablishmentReservation) {
              FoodEstablishmentReservation lr = (FoodEstablishmentReservation) r;
              lr.startTime.setTime(newDateTime);
            }
          }
        }
      }
    }

      boolean isFromRobot = true; // Bookings/notes were added by robots and rank was not set

    //1. Insert all notes and bookings into the common list

    for(Reservation r : rp.subReservation) {
      if (r == null) {
        Log.err("TripReservationHelper: Found null entry where none were expected (in sort())");
        return;
      }

      ListItem li = new ListItem();
      rp.sorted.addListItem(li);
      li.item = r;

      isFromRobot &= (r.position == 0);



      DateTime dt = (r.getStartDateTime() != null) ? r.getStartDateTime() : r.getFinishDateTime();
      if (dt != null) {
        li.group = dt.getISODate(); //This will allow to group
      }
      //Notes that are linked to reservervations are a special case
      else if(r.additionalType.equals("https://umapped.com/NoteReservation") && r.reservationId != null) {
        Reservation linkedReservation = findReservationById(rp, r.reservationId);
        if(linkedReservation != null) {
          DateTime linkedDT = (linkedReservation.getStartDateTime() != null) ?
                              linkedReservation.getStartDateTime() :
                              linkedReservation.getFinishDateTime();
          if(linkedDT != null) {
            li.group = linkedDT.getISODate(); //This will allow to group
          }
          Note note = (Note) r;
          r.bookingTime = linkedReservation.bookingTime;
        }
      }

      if(li.group == null) {
        li.group = "FIRST";
      }
    }

    //This flag indicates that all bookings/notes were inserted via any method but a publisher,
    //if this is the case then notes should always get converted into linked bookings.
    final boolean fIsFromRobot = isFromRobot;

    rp.sorted.sort((l, r) -> {

      //If we are in different groups
      if(!l.group.equals(r.group)) {
        if(l.group.equals("FIRST")) {
          return -1;
        }
        if(r.group.equals("FIRST")) {
          return 1;
        }
        return l.group.compareTo(r.group); //Dates are in ISO format - hence sortable, not most efficient but works
      }

      Reservation lR = (Reservation) l.item;
      Reservation rR = (Reservation) r.item;



      //Both sides are a note but one is linked to a linked reservation
      if (lR.additionalType.equals("https://umapped.com/NoteReservation") &&
              rR.additionalType.equals("https://umapped.com/NoteReservation") &&
              lR.reservationId == null &&
              rR.reservationId != null && !rR.reservationId.isEmpty()) {
        if (lR.getStartDateTime() != null && rR.bookingTime != null) {
          return lR.getStartDateTime().getTimestamp().compareTo(rR.bookingTime.getTimestamp());
        }

        return -1;
      }
//Both sides are a note but one is linked to a linked reservation
      if (lR.additionalType.equals("https://umapped.com/NoteReservation") &&
              rR.additionalType.equals("https://umapped.com/NoteReservation") &&
              rR.reservationId == null &&
              lR.reservationId != null && !lR.reservationId.isEmpty()) {
        if (rR.getStartDateTime() != null && lR.bookingTime != null) {
          return lR.bookingTime.getTimestamp().compareTo(rR.getStartDateTime().getTimestamp());
        }

        return 1;
      }

      //One side is a note and another is a linked reservation -- sorting between the two via sort order
      if (lR.additionalType.equals("https://umapped.com/NoteReservation") &&
              !rR.additionalType.equals("https://umapped.com/NoteReservation") &&
              rR.umId.equals(lR.reservationId)) {

        return 1;//lR.position - rR.position;
      }


      if (rR.additionalType.equals("https://umapped.com/NoteReservation") &&
          !lR.additionalType.equals("https://umapped.com/NoteReservation") &&
          lR.umId.equals(rR.reservationId)) {


        return -1;
      }


      //Both sides are the notes linked to the same reservation -- sorting between the two via sort order
      if(lR.additionalType.equals("https://umapped.com/NoteReservation") &&
         rR.additionalType.equals("https://umapped.com/NoteReservation") &&
         rR.reservationId != null && rR.reservationId.equals(lR.reservationId)) {
        return lR.position - rR.position;
      }

      //Left side is a note and right is a reservation not linked to a note or we are in a mode that nothing has a rank
      // - then replace left side with linked reservation
      if(lR.additionalType.equals("https://umapped.com/NoteReservation") &&
         ((lR.reservationId != null &&
           !rR.additionalType.equals("https://umapped.com/NoteReservation") &&
           !rR.umId.equals(lR.reservationId)) ||
          fIsFromRobot))
      {
        Reservation tmplR = findReservationById(rp, lR.reservationId);
        if(tmplR != null) {
          lR = tmplR;
        }
      }

      //Right side is a note and left is a reservation not linked to a note  or we are in a mode that nothing has a rank
      // - then replace right side with linked
      //reservation
      if(rR.additionalType.equals("https://umapped.com/NoteReservation") &&
         ((rR.reservationId != null &&  !lR.additionalType.equals("https://umapped.com/NoteReservation") &&
         !lR.umId.equals(rR.reservationId)) ||
          fIsFromRobot))
      {
        Reservation tmprR = findReservationById(rp, rR.reservationId);
        if(tmprR != null) {
          rR = tmprR;
        }
      }

      DateTime lDt = (lR.getStartDateTime() != null)?lR.getStartDateTime():lR.getFinishDateTime();
      DateTime rDt = (rR.getStartDateTime() != null)?rR.getStartDateTime():rR.getFinishDateTime();

      //for all bookings, if the time is midnight, then we set the time to 23:59 to it is the last booking of the day
      if (!lR.additionalType.equals("https://umapped.com/NoteReservation")) {
        lDt = resetMidnightDateForHotel(lDt);
      }
      if (!rR.additionalType.equals("https://umapped.com/NoteReservation")) {
        rDt = resetMidnightDateForHotel(rDt);
      }


      if (lR.additionalType.equals("https://schema.org/FlightReservation") && lR.isEndBeforeStart() &&
          lR.getStartDateTime().getISODate().equals(lR.getFinishDateTime().getISODate())) {
          lDt = lR.getFinishDateTime();
      }

      if (rR.additionalType.equals("https://schema.org/FlightReservation") && rR.isEndBeforeStart() &&
          rR.getStartDateTime().getISODate().equals(rR.getFinishDateTime().getISODate())) {
        rDt = rR.getFinishDateTime();
      }

      //Items are not sorted, so determine based on time
      if((lR.position == 0 && rR.position == 0) ||
         (rR.additionalType.equals("https://umapped.com/NoteReservation") && rR.reservationId == null) ||
         (lR.additionalType.equals("https://umapped.com/NoteReservation") && lR.reservationId == null) ) {
        //If one of the items does not have start time - it goes to the top (i.e. values are less)
        if ((lDt == null || lDt.getTime() == null) && (rDt != null && rDt.getTime() != null)) {
          return -1;
        }
        if ((rDt == null || rDt.getTime() == null) && (lDt != null && lDt.getTime() != null)) {
          return 1;
        }

        //Comparing actual times within the same group
        if (lDt != null && rDt != null && lDt.getTime() != null && rDt.getTime() != null) {
          int timeCmpResult = lDt.getTime().compareTo(rDt.getTime());
          //If time is the same, comparing just IDs for consistent ranking
          if(timeCmpResult == 0) {
            if(lR.additionalType.equals("https://umapped.com/NoteReservation") &&
               !rR.additionalType.equals("https://umapped.com/NoteReservation")) {
              return -1;
            } else if (!lR.additionalType.equals("https://umapped.com/NoteReservation") &&
                       rR.additionalType.equals("https://umapped.com/NoteReservation")) {
              return 1;
            }

            if(!lR.position.equals(rR.position)) {
              return lR.position - rR.position;
            }

            return lR.umId.compareTo(rR.umId);
          }
          return timeCmpResult;
        }
      }
      else if (lR.position.equals(rR.position) &&
               lDt != null && lDt.getTime() != null &&
               rDt != null && rDt.getTime() != null){
        int timeCmpResult = lDt.getTime().compareTo(rDt.getTime());
        //If time is the same, comparing just IDs for consistent ranking
        if(timeCmpResult == 0) {
          return lR.umId.compareTo(rR.umId);
        }
        return timeCmpResult;
      } else {
        return lR.position - rR.position;
      }

      Log.info("TripReservationHelper: sort(): reached unhandled reservation comparison zone. LID:" + lR.umId + " " +
               "RID:" + rR.umId);
      return 0;
    });


    //if there were some date movements due to cross date line flights, put them back to the original dates
    if (origTaggedReservations.size() > 0) {
      for (Reservation r: origTaggedReservations.keySet()) {
        LocalDateTime newDateTime = origTaggedReservations.get(r).getTime();
        if (r instanceof LodgingReservation) {
          LodgingReservation lr = (LodgingReservation) r;
          lr.checkinTime.setTime(newDateTime);
        } else if (r instanceof TransferReservation) {
          TransferReservation lr = (TransferReservation) r;
          lr.reservationFor.pickupTime.setTime(newDateTime);
        } else if (r instanceof ActivityReservation) {
          ActivityReservation lr = (ActivityReservation) r;
          lr.startTime.setTime(newDateTime);
        } else if (r instanceof CruiseShipReservation) {
          CruiseShipReservation lr = (CruiseShipReservation) r;
          lr.reservationFor.departTime.setTime(newDateTime);
        } else if (r instanceof RentalCarReservation) {
          RentalCarReservation lr = (RentalCarReservation) r;
          lr.pickupTime.setTime(newDateTime);
        } else if (r instanceof TaxiReservation) {
          TaxiReservation lr = (TaxiReservation) r;
          lr.pickupTime.setTime(newDateTime);
        } else if (r instanceof EventReservation) {
          EventReservation lr = (EventReservation) r;
          lr.reservationFor.startDate.setTime(newDateTime);
        } else if (r instanceof TrainReservation) {
          TrainReservation lr = (TrainReservation) r;
          lr.reservationFor.departTime.setTime(newDateTime);
        } else if (r instanceof BusReservation) {
          BusReservation lr = (BusReservation) r;
          lr.reservationFor.departureTime.setTime(newDateTime);
        } else if (r instanceof FoodEstablishmentReservation) {
          FoodEstablishmentReservation lr = (FoodEstablishmentReservation) r;
          lr.startTime.setTime(newDateTime);
        }
      }
    }

    //Wipe source
    rp.subReservation = null;
  }

  /**
   * Helper to convert models into Schema compliant structures
   *
   * @param tn
   * @return
   */
  public Note tripNoteToReservationNote(TripNote tn) {
    Note n = new Note();

    n.umId = tn.getNoteId().toString();
    n.name = tn.getName();
    n.reservationId = tn.getTripDetailId();
    n.reservationFor = new Place();
    n.reservationFor.geo = new GeoCoordinates();
    n.reservationFor.geo.latitude = (tn.getLocLat() != 0.0) ? Float.toString(tn.getLocLat()) : null;
    n.reservationFor.geo.longitude = (tn.getLocLong() != 0.0) ? Float.toString(tn.getLocLong()) : null;
    n.reservationFor.address = new PostalAddress();
    n.reservationFor.address.addressLocality = tn.getCity();
    n.reservationFor.address.addressRegion = tn.getState();
    n.reservationFor.address.addressCountry = tn.getCountry();
    n.reservationFor.address.postalCode = tn.getZipcode();
    n.reservationFor.address.streetAddress = tn.getStreetAddr();
    n.reservationFor.name = tn.getLandmark();
    n.description = tn.getIntro();
    n.reservationFor.description = tn.getDescription();
    n.position = tn.getRank();
    n.noteTimestamp = tsToDateTime(tn.getNoteTimestamp());
    n.additionalType = getTypeUrlFromUmapped(ReservationType.NOTE);
    n.creator = new Organization();
    n.creator.umId = tn.getCreatedBy();

    //Gather attached media
    List<TripNoteAttach> attchmnts = TripNoteAttach.findByNoteId(tn.getNoteId());
    for (TripNoteAttach at : attchmnts) {
      switch (at.getAttachType()) {
        case PHOTO_LINK:
          ImageObject io = at.buildImageObject();
          n.addImage(io);
          break;
        case WEB_LINK:
          Thing t = new Thing();
          t.url = at.getAttachUrl();
          t.name = at.getName();
          t.description = at.getComments();
          n.addUrl(t);
          break;
        case VIDEO_LINK:
          VideoObject vo = new VideoObject();
          vo.url = at.getAttachUrl();
          vo.name = at.getName();
          vo.caption = at.getComments();
          n.addVideo(vo);
          break;
        case FILE_LINK:
          Thing f = new Thing();
          f.url = at.getAttachUrl();
          f.name = at.getName();
          f.description = at.getComments();
          n.addUrl(f);
          break;
      }
    }

    return n;
  }

  public Reservation tripDetailToReservation(TripDetail td) {
    Reservation reservation = null;
    switch (td.getDetailtypeid()) {
      case FLIGHT:
        reservation = tdToFlightReservation(td);     //DONE
        break;
      case HOTEL:
        reservation = tdToLodgingReservation(td);    //DONE
        break;
      case PRIVATE_TRANSFER:
      case PUBLIC_TRANSPORT:
      case TRANSPORT:
      case FERRY:
        reservation = tdToTransferReservation(td);   //DONE
        break;
      case TAXI:
        reservation = tdToTaxiReservation(td);       //DONE Was Transport -> Taxi (DONE)
        break;
      case BUS:
        reservation = tdToBusReservation(td);        //DONE Was Transport -> Bus (DONE)
        break;
      case TOUR:
      case SHORE_EXCURSION:
      case CRUISE_STOP:
      case SKI_LESSONS:
      case SKI_LIFT:
      case SKI_RENTALS:
      case ACTIVITY:
        reservation = tdToActivityReservation(td);   //DONE
        break;
      case CRUISE:
        reservation = tdToCruiseShipReservation(td);
        break;
      case EVENT:
        reservation = tdToEventReservation(td);      //DONE: Was Activity -> Event
        break;
      case CAR_RENTAL:
      case CAR_DROPOFF:
        reservation = tdToRentalCarReservation(td);  //DONE Was Transport -> RentalCar (DONE)
        break;
      case RAIL:
        reservation = tdToTrainReservation(td);      //DONE Was Transport -> Train (DONE)
        break;
      case RESTAURANT:
        reservation = tdToFoodEstablishmentReservation(td); //DONE Was Activity -> Restaurant
        break;
      case NOTE:
        Log.err("Note not support to be converted to reservation");
      case PACKAGE:
      default:
        Log.err("TripReservationHelper: tripDetailToReservation(): Unexpected TripDetail Type: " +
                td.getDetailtypeid());
    }
    if (reservation != null && td != null && td.getTag() != null) {
      reservation.tag = td.getTag();
    }
    return reservation;
  }

  public String getTypeUrlFromUmapped(ReservationType type) {
    switch (type) {
      case FLIGHT:
        return "https://schema.org/FlightReservation";
      case HOTEL:
        return "https://schema.org/LodgingReservation";
      case PRIVATE_TRANSFER:
        return "https://umapped.com/PrivateTransferReservation";
      case BUS:
        return "https://schema.org/BusReservation";
      case PUBLIC_TRANSPORT:
        return "https://umapped.com/PublicTransportReservation";
      case TRANSPORT:
        return "https://umapped.com/TransferReservation";
      case TAXI:
        return "https://schema.org/TaxiReservation";
      case ACTIVITY:
        return "https://umapped.com/ActivityReservation";
      case CRUISE:
        return "https://umapped.com/CruiseReservation";
      case EVENT:
        return "https://schema.org/EventReservation";
      case CAR_RENTAL:
        return "https://schema.org/RentalCarReservation";
      case RAIL:
        return "https://schema.org/TrainReservation";
      case RESTAURANT:
        return "https://schema.org/FoodEstablishmentReservation";
      case TOUR:
        return "https://umapped.com/TourReservation";
      case SHORE_EXCURSION:
        return "https://umapped.com/ShoreExcursionReservation";
      case CRUISE_STOP:
        return "https://umapped.com/CruiseStopReservation";
      case PACKAGE:
        return "https://schema.org/ReservationPackage";
      case NOTE:
        return "https://umapped.com/NoteReservation";
      case CAR_DROPOFF:
        return "https://umapped.com/RentalCarDropOffReservation";
      case SKI_LESSONS:
        return "https://umapped.com/SkiLessonsReservation";
      case SKI_LIFT:
        return "https://umapped.com/SkiLiftTickets";
      case SKI_RENTALS:
        return "https://umapped.com/SkiRentalReservation";
      case FERRY:
        return "https://umapped.com/FerryReservation";
      default:
        return "https://umapped.com/Unknown";
    }
  }

  public void tdToReservationCommon(TripDetail td, Reservation r, String oldName, String comment) {
    r.umId = td.getDetailsid();
    r.name        = (td.getName() != null && td.getName().length() > 0)?td.getName():oldName;

    r.description = comment;
    
    r.reservationNumber = td.getBookingnumber()!=null?td.getBookingnumber():"";
    r.additionalType = getTypeUrlFromUmapped(td.detailtypeid);
    r.position = td.getRank();

    r.creator = new Organization();
    r.creator.umId = td.getCreatedby();

    List<TripAttachment> tas =  TripAttachment.findForDetailId(td.getDetailsid());
    tas.forEach(ta -> {
      MediaObject mo = new MediaObject();
      mo.umId = ta.getPk();
      if (ta.getName() != null && ta.getName().length() > 0) {
        mo.name = ta.getName();
      }
      else {
        mo.name = ta.getOrigfilename();
      }
      mo.fileFormat = ta.getFiletype(); //Should MIME type, but we are cheaters
      mo.url = ta.getFileurl();
      r.addDownloadable(mo);
    });

    r.taxes = td.getReservation().getTaxes();
    r.fees = td.getReservation().getFees();
    r.subtotal = td.getReservation().getSubtotal();
    r.currency = td.getReservation().getCurrency();
    r.total = td.getReservation().getTotal();
  }

  public FlightReservation tdToFlightReservation(TripDetail td) {
    FlightReservation r  = new FlightReservation();
    UmFlightReservation reservation = cast(td.getReservation(), UmFlightReservation.class);
    
    String flightNumber = reservation != null ? reservation.getFlight().flightNumber : "";
    String comment = reservation != null ? reservation.getNotesPlainText() : "";
    
    tdToReservationCommon(td, r, flightNumber, comment);
    r.reservationFor = new Flight();

    //Deleting all non-digits for flight number
    if (flightNumber != null) {
      r.reservationFor.flightNumber = flightNumber.replaceAll("\\D+", "");
    }
    PoiRS airlinePrs = PoiController.getMergedPoi(td.getPoiId(), td.getPoiCmpyId());
    if(airlinePrs != null) {
      r.reservationFor.airline = poiToAirline(airlinePrs);
    }
    if (airlinePrs != null && airlinePrs.data != null) {
      if (airlinePrs.data.getAdditionalProperties().containsKey(APPConstants.BAGGAGE_URL)) {
        r.baggageUrl = airlinePrs.data.getAdditionalProperties().get(APPConstants.BAGGAGE_URL);
      }
      if (airlinePrs.data.getAdditionalProperties().containsKey(APPConstants.CHECKIN_URL)) {
        r.checkinUrl = airlinePrs.data.getAdditionalProperties().get(APPConstants.CHECKIN_URL);
      }
    }

    if (td.getLocStartPoiId() != null) {
      PoiRS dAirport = PoiController.getMergedPoi(td.getLocStartPoiId(), td.getLocStartPoiCmpyId());
      r.reservationFor.departureAirport = poiToAirport(dAirport);
    }
    else {
      r.reservationFor.departureAirport = new Airport();
      r.reservationFor.departureAirport.name = td.getLocStartName();
    }
    r.reservationFor.departureTime = tsToDateTime(td.getStarttimestamp());

    if (td.getLocFinishPoiId() != null) {
      PoiRS aAirport = PoiController.getMergedPoi(td.getLocFinishPoiId(), td.getLocFinishPoiCmpyId());
      r.reservationFor.arrivalAirport = poiToAirport(aAirport);
    }
    else {
      r.reservationFor.arrivalAirport = new Airport();
      r.reservationFor.arrivalAirport.name = td.getLocFinishName();
    }
    r.reservationFor.arrivalTime = tsToDateTime(td.getEndtimestamp());

    r.reservationFor.departureTerminal = reservation.getFlight().departureTerminal;
    r.reservationFor.arrivalTerminal = reservation.getFlight().arrivalTerminal;

    List<UmTraveler> travelers =  reservation.getTravelers();
    
    if (travelers != null) {
      travelers.stream()
      .forEach((t) -> {
        UmFlightTraveler ft = cast(t, UmFlightTraveler.class);
        if (ft != null) {
          FlightReservation sub = new FlightReservation();
          sub.underName = new Person();
          sub.underName.familyName = ft.getFamilyName();
          sub.underName.givenName = ft.getGivenName();
          sub.underName.name = ft.getName();
          
          sub.airplaneSeat = ft.getSeat();
          if (StringUtils.isNotEmpty(ft.getSeatClass().seatCategory)) {
            sub.airplaneSeatClass = new AirplaneSeatClass();
            sub.airplaneSeatClass.seatCategory = ft.getSeatClass().seatCategory;
          }
          sub.ticketToken = ft.getTicketNumber();
          if (StringUtils.isNotEmpty(ft.getProgram().membershipNumber)) {
            sub.programMembershipUsed = new ProgramMembership();
            sub.programMembershipUsed.membershipNumber = ft.getProgram().membershipNumber;
          }
          if (StringUtils.isNotEmpty(ft.getMeal())) {
            sub.reservationFor = new Flight();
            sub.reservationFor.mealService = ft.getMeal();
          }
          r.addSubReservation(sub);
        }
      }
      );
    }

    List<UmRate> rates = reservation.getRates();

    if(rates != null) {
      rates.stream().forEach((p) -> {
        FlightReservation sub = new FlightReservation();
        sub.rateName = p.getDisplayName();
        sub.ratePrice = p.getPrice();
        sub.rateDescription = p.getDescription();
        r.addRates(sub);
      });
    }

    r.cancellationPolicy = reservation.getCancellationPolicy();

    return r;
  }

  /**
   * Helper based on the "Enhanced Comment" concept
   *
   * @param 
   */
  /* Remove after migration
  public void updateFlightReservationFromComment(FlightReservation r) {
    if (r.description == null || r.description.length() == 0) {
      return;
    }

    String[]          tokens = r.description.split("\n");
    StringBuilder     sb     = new StringBuilder();
    FlightReservation subRes = null;

    boolean hasPassengers = r.description.contains("Passengers:");

    for (String token : tokens) {
      if (token.trim().length() == 0 || token.contains("Passengers:")) {
        sb.append(System.lineSeparator());
        continue;
      }
      if (token.startsWith("Departure Terminal:")) {
        r.reservationFor.departureTerminal = token.substring(token.indexOf("Departure Terminal:") + 19).trim();
        continue;
      }
      if (token.startsWith("Departure Terminal")) {
        r.reservationFor.departureTerminal = token.substring(token.indexOf("Departure Terminal") + 18).trim();
        continue;
      }

      if (token.startsWith("Arrival Terminal:")) {
        r.reservationFor.arrivalTerminal = token.substring(token.indexOf("Arrival Terminal:") + 17).trim();
        continue;
      }
      if (token.startsWith("Arrival Terminal")) {
        r.reservationFor.arrivalTerminal = token.substring(token.indexOf("Arrival Terminal") + 16).trim();
        continue;
      }

      if (hasPassengers) {
        if (token.startsWith("Name:")) {
          subRes = new FlightReservation();
          r.addSubReservation(subRes);
          subRes.underName = personFromName(getValue(token));
          continue;
        }

        if (token.startsWith("Seat") && subRes != null) {
          subRes.airplaneSeat = getValue(token);
          continue;
        }

        if (token.startsWith("Class") && subRes != null) {
          subRes.airplaneSeatClass = new AirplaneSeatClass();
          subRes.airplaneSeatClass.seatCategory = getValue(token);
          continue;
        }

        if (token.startsWith("eTicket") && subRes != null) {
          subRes.ticketToken = getValue(token);
          continue;
        }

        if (token.startsWith("Frequent Flyer") && subRes != null) {
          subRes.programMembershipUsed = new ProgramMembership();
          subRes.programMembershipUsed.membershipNumber = getValue(token);
          continue;
        }

        if (token.startsWith("Meal") && subRes != null) {
          subRes.reservationFor = new Flight();
          subRes.reservationFor.mealService = getValue(token);
          continue;
        }
      }

      sb.append(token).append(System.lineSeparator());
    }
    r.description = Utils.cleanSpecials(sb.toString());
  }

  public Person personFromName(String name) {
    Person p = new Person();

    if(name.split("\\w+").length>1){
      p.givenName = name.substring(name.lastIndexOf(" ")+1);
      p.familyName = name.substring(0, name.lastIndexOf(' '));
    }
    else{
      p.name = name;
    }
    return  p;
  }
*/
  public String getValue(String s) {
    if (s != null && s.contains(":")) {
      s = Utils.cleanSpecials(s);
      return s.substring(s.indexOf(":") + 1).trim();
    }
    return "";
  }

  public List<ImageObject> getReservationImages(TripDetail tripDetail, Long poiId, Integer cmpyId) {
    List<ImageObject> images    = new ArrayList<>();
    TripDetail.ExtraDetails ed = tripDetail.getExtraDetails();
    if (ed == null || ed.showPhotos == null || ed.showPhotos) {
      List<PoiFile>     poiImages = BookingController.getBookingFiles(tripDetail.getDetailsid(), poiId, cmpyId);
      if(poiImages != null && poiImages.size() > 0) {
        for (PoiFile pf : poiImages) {
          images.add(poiFileToImageObject(pf));
        }
      }
    }

    return images;
  }

  public LodgingReservation tdToLodgingReservation(TripDetail td) {
    UmAccommodationReservation hotelReseration = cast(td.getReservation(), UmAccommodationReservation.class);
    
    String hotelName = hotelReseration != null ? hotelReseration.getAccommodation().name : "";
    HotelMigrationViewUtil viewUtil = new HotelMigrationViewUtil();
    String comment = hotelReseration.getNotesPlainText();
    LodgingReservation r = new LodgingReservation();
    tdToReservationCommon(td, r, hotelName, comment);

    r.checkinTime = tsToDateTime(td.getStarttimestamp());
    r.checkoutTime = tsToDateTime(td.getEndtimestamp());

    PoiRS mainPoi = PoiController.getMergedPoi(td.getPoiId(), td.getPoiCmpyId());
    if (mainPoi != null) {
      r.reservationFor = poiToLodgingBusiness(mainPoi);
      r.image = getReservationImages(td, td.getPoiId(), td.getPoiCmpyId());
    } else {
      r.reservationFor = new LodgingBusiness();
      r.reservationFor.name = td.getName();
    }

    if(r.reservationFor.telephone == null || r.reservationFor.telephone.trim().isEmpty()) {
      r.reservationFor.telephone = hotelReseration.getAccommodation().telephone;
    }

    if(r.reservationFor.name == null) {
      r.reservationFor.name = hotelName;
    }

    if (hotelReseration!=null && hotelReseration.getAccommodation() != null) {
      UmLodgingBusiness lodgingBusiness = hotelReseration.getAccommodation();
      UmPostalAddress address = lodgingBusiness.getAddress();
      if (r.reservationFor.address == null && address != null) {
        r.reservationFor.address = new PostalAddress();
        r.reservationFor.address.streetAddress = address.streetAddress;
        r.reservationFor.address.addressLocality = address.addressLocality;
        r.reservationFor.address.addressRegion = address.addressRegion;
        r.reservationFor.address.addressCountry = address.addressCountry;
        r.reservationFor.address.postalCode = address.postalCode;
        r.reservationFor.address.a3Country = address.a3Country;
        r.reservationFor.address.postOfficeBoxNumber = address.postOfficeBoxNumber;
      }
    }
    if(r.description == null) {
      r.description = comment;
    }

    List<UmTraveler> travelers =  hotelReseration.getTravelers();

    if (travelers != null) {
      travelers.stream().forEach((t) -> {
        UmAccommodationTraveler at = cast(t, UmAccommodationTraveler.class);
          if (at != null) {
            LodgingReservation sub = new LodgingReservation();
            sub.underName = new Person();
            sub.underName.familyName = at.getFamilyName();
            sub.underName.givenName = at.getGivenName();
            sub.underName.name = at.getName();

            sub.lodgingUnitType = at.getRoomType();
            sub.bedding = at.getBedding();
            sub.amenities = at.getAmenities();
            sub.confirmStatus = at.getConfirmStatus();
            if(at.getProgram() != null) {
              sub.membershipId = at.getProgram().membershipNumber;
            }

            r.addSubReservation(sub);
          }
        }
      );
    }

    List<UmRate> rates = hotelReseration.getRates();

    if(rates != null) {
      rates.stream().forEach((p) -> {
        LodgingReservation sub = new LodgingReservation();
        sub.rateName = p.getDisplayName();
        sub.ratePrice = p.getPrice();
        sub.rateDescription = p.getDescription();
        r.addRates(sub);
      });
    }

    r.cancellationPolicy = hotelReseration.getCancellationPolicy();
    r.reservationStatus = hotelReseration.getReservationStatus();

    return r;
  }

  public TransferReservation tdToTransferReservation(TripDetail td) {
    UmTransferReservation reservation = cast(td.getReservation(), UmTransferReservation.class);

    String providerName = ""; 
    String comment = "";
    String pickupLocation = "";
    String dropOffLocation = "";
    String contactPoint = "";
    
    if (reservation != null) {
      providerName = reservation.getTransfer().getProvider().name;
      comment = reservation.getNotesPlainText();
      pickupLocation = reservation.getTransfer().getPickupLocation().name;
      dropOffLocation = reservation.getTransfer().getDropoffLocation().name;
      contactPoint = reservation.getTransfer().getProvider().contactPoint;
    }
    TransferReservation r  = new TransferReservation();
    
    tdToReservationCommon(td, r, providerName, comment);

    r.reservationFor = new Transfer();

    r.image = getReservationImages(td, td.getPoiId(), td.getPoiCmpyId());

    r.reservationFor.pickupTime = tsToDateTime(td.getStarttimestamp());
    r.reservationFor.pickupLocation = locationToPlace(td.getLocStartPoiId(),
                                                      td.getLocStartPoiCmpyId(),
                                                      td.getLocStartName(),
                                                      pickupLocation);
    r.reservationFor.dropoffTime = tsToDateTime(td.getEndtimestamp());
    r.reservationFor.dropoffLocation = locationToPlace(td.getLocFinishPoiId(),
                                                       td.getLocFinishPoiCmpyId(),
                                                       td.getLocFinishName(),
                                                       dropOffLocation);

    r.reservationFor.provider = contactToOrganization(td.getPoiId(), td.getPoiCmpyId(), contactPoint);

    List<UmRate> rates = reservation.getRates();

    if(rates != null) {
      rates.stream().forEach((p) -> {
        TransferReservation sub = new TransferReservation();
        sub.rateName = p.getDisplayName();
        sub.ratePrice = p.getPrice();
        sub.rateDescription = p.getDescription();
        r.addRates(sub);
      });
    }

    r.serviceType = reservation.getServiceType();
    r.cancellationPolicy = reservation.getCancellationPolicy();

    return r;
  }

  public RentalCarReservation tdToRentalCarReservation(TripDetail td) {
    UmTransferReservation reservation = cast(td.getReservation(), UmTransferReservation.class);
    String providerName = ""; 
    String comment = "";
    String pickupLocation = "";
    String dropOffLocation = "";
    String contactPoint = "";
    
    if (reservation != null) {
      providerName = reservation.getTransfer().getProvider().name;
      comment = reservation.getNotesPlainText();
      pickupLocation = reservation.getTransfer().getPickupLocation().name;
      dropOffLocation = reservation.getTransfer().getDropoffLocation().name;
      contactPoint = reservation.getTransfer().getProvider().contactPoint;
    }
    
    RentalCarReservation r  = new RentalCarReservation();
    tdToReservationCommon(td, r, providerName, comment);

    r.reservationFor = new RentalCar();
    if (td.detailtypeid == ReservationType.CAR_RENTAL) {
      r.pickupTime = tsToDateTime(td.getStarttimestamp());
      r.pickupLocation = locationToPlace(td.getLocStartPoiId(),
                                         td.getLocStartPoiCmpyId(),
                                         td.getLocStartName(),
                                         pickupLocation);
      r.dropoffTime = tsToDateTime(td.getEndtimestamp());
      r.dropoffLocation = locationToPlace(td.getLocFinishPoiId(),
                                          td.getLocFinishPoiCmpyId(),
                                          td.getLocFinishName(),
                                          dropOffLocation);
    }
    else {
      r.dropoffTime = tsToDateTime(td.getStarttimestamp());
      r.dropoffLocation = locationToPlace(td.getLocStartPoiId(),
                                          td.getLocStartPoiCmpyId(),
                                          td.getLocStartName(),
                                          reservation.getTransfer().getPickupLocation().name);
    }

    r.image = getReservationImages(td, td.getPoiId(), td.getPoiCmpyId());

    r.reservationFor.rentalCompany = contactToOrganization(td.getPoiId(), td.getPoiCmpyId(), contactPoint);

    List<UmRate> rates = reservation.getRates();

    if(rates != null) {
      rates.stream().forEach((p) -> {
        TransferReservation sub = new TransferReservation();
        sub.rateName = p.getDisplayName();
        sub.ratePrice = p.getPrice();
        sub.rateDescription = p.getDescription();
        r.addRates(sub);
      });
    }

    r.serviceType = reservation.getServiceType();
    r.cancellationPolicy = reservation.getCancellationPolicy();

    return r;
  }

  public TrainReservation tdToTrainReservation(TripDetail td) {
    UmTransferReservation reservation = cast(td.getReservation(), UmTransferReservation.class);
    String providerName = ""; 
    String comment = "";
    String pickupLocation = "";
    String dropOffLocation = "";
    String contactPoint = "";
    
    if (reservation != null) {
      providerName = reservation.getTransfer().getProvider().name;
      comment = reservation.getNotesPlainText();
      pickupLocation = reservation.getTransfer().getPickupLocation().name;
      dropOffLocation = reservation.getTransfer().getDropoffLocation().name;
      contactPoint = reservation.getTransfer().getProvider().contactPoint;
    }
    
    TrainReservation r  = new TrainReservation();

    tdToReservationCommon(td, r, providerName, comment);

    r.reservationFor = new TrainTrip();
    r.image = getReservationImages(td, td.getPoiId(), td.getPoiCmpyId());
    r.reservationFor.departTime = tsToDateTime(td.getStarttimestamp());
    r.reservationFor.departStation = locationToPlace(td.getLocStartPoiId(),
                                                     td.getLocStartPoiCmpyId(),
                                                     td.getLocStartName(),
                                                     pickupLocation);

    r.reservationFor.arrivalTime = tsToDateTime(td.getEndtimestamp());
    r.reservationFor.arrivalStation = locationToPlace(td.getLocFinishPoiId(),
                                                      td.getLocFinishPoiCmpyId(),
                                                      td.getLocFinishName(),
                                                      dropOffLocation);

    r.reservationFor.provider = contactToOrganization(td.getPoiId(), td.getPoiCmpyId(), contactPoint);

    List<UmRate> rates = reservation.getRates();

    if(rates != null) {
      rates.stream().forEach((p) -> {
        TransferReservation sub = new TransferReservation();
        sub.rateName = p.getDisplayName();
        sub.ratePrice = p.getPrice();
        sub.rateDescription = p.getDescription();
        r.addRates(sub);
      });
    }

    r.serviceType = reservation.getServiceType();
    r.cancellationPolicy = reservation.getCancellationPolicy();

    return r;
  }

  public BusReservation tdToBusReservation(TripDetail td) {
    UmTransferReservation reservation = cast(td.getReservation(), UmTransferReservation.class);
    String providerName = ""; 
    String comment = "";
    String pickupLocation = "";
    String dropOffLocation = "";
    String contactPoint = "";
    
    if (reservation != null) {
      providerName = reservation.getTransfer().getProvider().name;
      comment = reservation.getNotesPlainText();
      pickupLocation = reservation.getTransfer().getPickupLocation().name;
      dropOffLocation = reservation.getTransfer().getDropoffLocation().name;
      contactPoint = reservation.getTransfer().getProvider().contactPoint;
    }
    
    BusReservation   r  = new BusReservation();
    tdToReservationCommon(td, r, providerName, comment);

    r.reservationFor = new BusTrip();
    r.image = getReservationImages(td, td.getPoiId(), td.getPoiCmpyId());
    r.reservationFor.departureTime = tsToDateTime(td.getStarttimestamp());
    r.reservationFor.departureBusStop = new BusStop();

    updatePlaceFromLocation(r.reservationFor.departureBusStop,
                            td.getLocStartPoiId(),
                            td.getLocStartPoiCmpyId(),
                            td.getLocStartName(),
                            pickupLocation);
    r.reservationFor.arrivalTime = tsToDateTime(td.getEndtimestamp());
    r.reservationFor.arrivalBusStop = new BusStop();
    updatePlaceFromLocation(r.reservationFor.arrivalBusStop,
                            td.getLocFinishPoiId(),
                            td.getLocFinishPoiCmpyId(),
                            td.getLocFinishName(),
                            dropOffLocation);

    r.reservationFor.provider = contactToOrganization(td.getPoiId(), td.getPoiCmpyId(), contactPoint);

    List<UmRate> rates = reservation.getRates();

    if(rates != null) {
      rates.stream().forEach((p) -> {
        TransferReservation sub = new TransferReservation();
        sub.rateName = p.getDisplayName();
        sub.ratePrice = p.getPrice();
        sub.rateDescription = p.getDescription();
        r.addRates(sub);
      });
    }

    r.serviceType = reservation.getServiceType();
    r.cancellationPolicy = reservation.getCancellationPolicy();

    return r;
  }

  public Organization contactToOrganization(Long poiId, Integer cmpyId, String contact) {
    Organization o = new Organization();
    if (poiId != null) {
      PoiRS prs = PoiController.getMergedPoi(poiId, cmpyId);
      if (prs != null) {
        poiToOrganization(prs, o);
      }
    }
    if (StringUtils.isNotEmpty(contact)) {
      o.name = contact;
    }
    return o;
  }

  public TaxiReservation tdToTaxiReservation(TripDetail td) {
    UmTransferReservation reservation = cast(td.getReservation(), UmTransferReservation.class);
    String providerName = ""; 
    String comment = "";
    String pickupLocation = "";
    String contactPoint = "";
    
    if (reservation != null) {
      if (reservation.getTransfer().getProvider() != null) {
        providerName = reservation.getTransfer().getProvider().name;
        contactPoint = reservation.getTransfer().getProvider().contactPoint;
      }
      comment = reservation.getNotesPlainText();
      if (reservation.getTransfer().getPickupLocation() != null) {
        pickupLocation = reservation.getTransfer().getPickupLocation().name;
      }
    }
    
    TaxiReservation  r  = new TaxiReservation();
    tdToReservationCommon(td, r, providerName, comment);

    r.reservationFor = new Taxi();
    r.image = getReservationImages(td, td.getPoiId(), td.getPoiCmpyId());
    r.pickupTime = tsToDateTime(td.getStarttimestamp());
    r.pickupLocation = locationToPlace(td.getLocStartPoiId(),
                                       td.getLocStartPoiCmpyId(),
                                       td.getLocStartName(),
                                       pickupLocation);
    r.reservationFor.provider = contactToOrganization(td.getPoiId(), td.getPoiCmpyId(), contactPoint);

    List<UmRate> rates = reservation.getRates();

    if(rates != null) {
      rates.stream().forEach((p) -> {
        TransferReservation sub = new TransferReservation();
        sub.rateName = p.getDisplayName();
        sub.ratePrice = p.getPrice();
        sub.rateDescription = p.getDescription();
        r.addRates(sub);
      });
    }

    r.serviceType = reservation.getServiceType();
    r.cancellationPolicy = reservation.getCancellationPolicy();

    return r;
  }

  public ActivityReservation tdToActivityReservation(TripDetail td) {
    UmActivityReservation activity = cast(td.getReservation(), UmActivityReservation.class);
    String name = ""; 
    String comment = "";
    String contactPoint = "";
    
    if (activity != null) {
      name = activity.getActivity().name;
      comment = activity.getNotesPlainText();
      if (activity.getActivity().getOrganizedBy() != null) {
        contactPoint = activity.getActivity().getOrganizedBy().contactPoint;
      }
    }
    ActivityReservation r  = new ActivityReservation();
    tdToReservationCommon(td, r, name, comment);
    r.reservationFor = new Activity();
    r.startTime = tsToDateTime(td.getStarttimestamp());
    r.startLocation = locationToPlace(td.getLocStartPoiId(), td.getLocStartPoiCmpyId(), td.getLocStartName(), null);
    r.finishTime = tsToDateTime(td.getEndtimestamp());
    r.finishLocation = locationToPlace(td.getLocFinishPoiId(), td.getLocFinishPoiCmpyId(), td.getLocFinishName(), null);
    r.image = getReservationImages(td, td.getPoiId(), td.getPoiCmpyId());

    PoiRS prs = PoiController.getMergedPoi(td.getPoiId(), td.getPoiCmpyId());
    r.reservationFor.organizedBy = new Organization();
    if(prs != null) {
      poiToOrganization(prs, r.reservationFor.organizedBy);
      // If the activity provides the contact point, use it instead of POI
      if (!StringUtils.isEmpty(contactPoint)) {
        r.reservationFor.organizedBy.name = contactPoint;
      }
    } else {
      if (activity.getActivity().getOrganizedBy().contactPoint != null) {
        r.reservationFor.organizedBy.name = contactPoint;
      }

      if (activity.getActivity().getOrganizedBy().getAddress() != null) {
        r.reservationFor.organizedBy.address = new PostalAddress();
        if (activity.getActivity().getOrganizedBy().getAddress().a3Country != null) {
          r.reservationFor.organizedBy.address.a3Country = activity.getActivity().getOrganizedBy().getAddress().a3Country;
        }
        if (activity.getActivity().getOrganizedBy().getAddress().addressCountry != null) {
          r.reservationFor.organizedBy.address.addressCountry = activity.getActivity().getOrganizedBy().getAddress().addressCountry;
        }
        if (activity.getActivity().getOrganizedBy().getAddress().addressLocality != null) {
          r.reservationFor.organizedBy.address.addressLocality = activity.getActivity().getOrganizedBy().getAddress().addressLocality;
        }
        if (activity.getActivity().getOrganizedBy().getAddress().addressRegion != null) {
          r.reservationFor.organizedBy.address.addressRegion = activity.getActivity().getOrganizedBy().getAddress().addressRegion;
        }
        if (activity.getActivity().getOrganizedBy().getAddress().postalCode != null) {
          r.reservationFor.organizedBy.address.postalCode = activity.getActivity().getOrganizedBy().getAddress().postalCode;
        }
        if (activity.getActivity().getOrganizedBy().getAddress().streetAddress != null) {
          r.reservationFor.organizedBy.address.streetAddress = activity.getActivity().getOrganizedBy().getAddress().streetAddress;
        }
      }
    }
    List<UmRate> rates = activity.getRates();

    if(rates != null) {
      rates.stream().forEach((p) -> {
        ActivityReservation sub = new ActivityReservation();
        sub.rateName = p.getDisplayName();
        sub.ratePrice = p.getPrice();
        sub.rateDescription = p.getDescription();
        r.addRates(sub);
      });
    }

    r.serviceType = activity.getServiceType();
    r.cancellationPolicy = activity.getCancellationPolicy();

    return r;
  }

  public EventReservation tdToEventReservation(TripDetail td) {
    UmActivityReservation activity = cast(td.getReservation(), UmActivityReservation.class);
    String name = ""; 
    String comment = "";
    String contactPoint = "";
    
    if (activity != null) {
      name = activity.getActivity().name;
      comment = activity.getNotesPlainText();
      contactPoint = activity.getActivity().getOrganizedBy().contactPoint;
    }
    EventReservation r  = new EventReservation();

    tdToReservationCommon(td, r, name, comment);

    r.reservationFor = new Event();
    r.reservationFor.startDate = tsToDateTime(td.getStarttimestamp());
    r.reservationFor.location = locationToPlace(td.getLocStartPoiId(),
                                                td.getLocStartPoiCmpyId(),
                                                td.getLocStartName(),
                                                null);
    r.reservationFor.endDate = tsToDateTime(td.getEndtimestamp());

    PoiRS prs = PoiController.getMergedPoi(td.getPoiId(), td.getPoiCmpyId());
    r.reservationFor.organizer = new Organization();
    if(prs != null) {
      poiToOrganization(prs, r.reservationFor.organizer);
    } else {
      if (activity.getActivity().getOrganizedBy().contactPoint != null) {
        r.reservationFor.organizer.name = contactPoint;
      }

      if (activity.getActivity().getOrganizedBy().getAddress() != null) {
        r.reservationFor.organizer.address = new PostalAddress();
        if (activity.getActivity().getOrganizedBy().getAddress().a3Country != null) {
          r.reservationFor.organizer.address.a3Country = activity.getActivity().getOrganizedBy().getAddress().a3Country;
        }
        if (activity.getActivity().getOrganizedBy().getAddress().addressCountry != null) {
          r.reservationFor.organizer.address.addressCountry = activity.getActivity().getOrganizedBy().getAddress().addressCountry;
        }
        if (activity.getActivity().getOrganizedBy().getAddress().addressLocality != null) {
          r.reservationFor.organizer.address.addressLocality = activity.getActivity().getOrganizedBy().getAddress().addressLocality;
        }
        if (activity.getActivity().getOrganizedBy().getAddress().addressRegion != null) {
          r.reservationFor.organizer.address.addressRegion = activity.getActivity().getOrganizedBy().getAddress().addressRegion;
        }
        if (activity.getActivity().getOrganizedBy().getAddress().postalCode != null) {
          r.reservationFor.organizer.address.postalCode = activity.getActivity().getOrganizedBy().getAddress().postalCode;
        }
        if (activity.getActivity().getOrganizedBy().getAddress().streetAddress != null) {
          r.reservationFor.organizer.address.streetAddress = activity.getActivity().getOrganizedBy().getAddress().streetAddress;
        }
      }
    }

    if (td.getPoiId() != null) {
      r.image = getReservationImages(td, td.getPoiId(), td.getPoiCmpyId());
    }

    List<UmRate> rates = activity.getRates();

    if(rates != null) {
      rates.stream().forEach((p) -> {
        ActivityReservation sub = new ActivityReservation();
        sub.rateName = p.getDisplayName();
        sub.ratePrice = p.getPrice();
        sub.rateDescription = p.getDescription();
        r.addRates(sub);
      });
    }

    r.serviceType = activity.getServiceType();
    r.cancellationPolicy = activity.getCancellationPolicy();

    return r;
  }

  public FoodEstablishmentReservation tdToFoodEstablishmentReservation(TripDetail td) {
    UmActivityReservation activity = cast(td.getReservation(), UmActivityReservation.class);
    String name = ""; 
    String comment = "";
    String contactPoint = "";
    
    if (activity != null) {
      name = activity.getActivity().name;
      comment = activity.getNotesPlainText();
      contactPoint = activity.getActivity().getOrganizedBy().contactPoint;
    }

    FoodEstablishmentReservation r  = new FoodEstablishmentReservation();
    
    tdToReservationCommon(td, r, name, comment);

    r.reservationFor = new FoodEstablishment();

    if(activity.getActivity().getOrganizedBy().getAddress() != null) {
      r.reservationFor.address = new PostalAddress();
      if (activity.getActivity().getOrganizedBy().getAddress().a3Country != null) {
        r.reservationFor.address.a3Country = activity.getActivity().getOrganizedBy().getAddress().a3Country;
      }
      if (activity.getActivity().getOrganizedBy().getAddress().addressCountry != null) {
        r.reservationFor.address.addressCountry = activity.getActivity().getOrganizedBy().getAddress().addressCountry;
      }
      if(activity.getActivity().getOrganizedBy().getAddress().addressLocality != null) {
        r.reservationFor.address.addressLocality = activity.getActivity().getOrganizedBy().getAddress().addressLocality;
      }
      if(activity.getActivity().getOrganizedBy().getAddress().addressRegion != null) {
        r.reservationFor.address.addressRegion = activity.getActivity().getOrganizedBy().getAddress().addressRegion;
      }
      if(activity.getActivity().getOrganizedBy().getAddress().postalCode != null) {
        r.reservationFor.address.postalCode = activity.getActivity().getOrganizedBy().getAddress().postalCode;
      }
      if(activity.getActivity().getOrganizedBy().getAddress().streetAddress != null) {
        r.reservationFor.address.streetAddress = activity.getActivity().getOrganizedBy().getAddress().streetAddress;
      }
    }

    if (td.getPoiId() != null) {
      PoiRS prs = PoiController.getMergedPoi(td.getPoiId(), td.getPoiCmpyId());
      poiToOrganization(prs, r.reservationFor);
      r.image = getReservationImages(td, td.getPoiId(), td.getPoiCmpyId());
    }

    r.location = locationToPlace(td.getLocStartPoiId(), td.getLocStartPoiCmpyId(), td.getLocStartName(), null);
    r.startTime = tsToDateTime(td.getStarttimestamp());
    r.endTime = tsToDateTime(td.getEndtimestamp());

    r.provider = new Organization();
    if (activity.getActivity().getOrganizedBy().contactPoint != null) {
      r.provider.name = contactPoint;
    }

    List<UmRate> rates = activity.getRates();

    if(rates != null) {
      rates.stream().forEach((p) -> {
        ActivityReservation sub = new ActivityReservation();
        sub.rateName = p.getDisplayName();
        sub.ratePrice = p.getPrice();
        sub.rateDescription = p.getDescription();
        r.addRates(sub);
      });
    }

    r.serviceType = activity.getServiceType();
    r.cancellationPolicy = activity.getCancellationPolicy();

    return r;
  }

  public LodgingBusiness poiToLodgingBusiness(PoiRS p) {
    if (p == null) {
      return null;
    }
    LodgingBusiness lb = new LodgingBusiness();
    poiToOrganization(p, lb);

    if (p.locLat != 0.0 || p.locLong != 0.0) {
      lb.geo = new GeoCoordinates();
      lb.geo.latitude = Float.toString(p.locLat);
      lb.geo.longitude = Float.toString(p.locLong);
    }

    return lb;
  }

  public void poiToOrganization(PoiRS p, Organization o) {
    o.umId = Long.toString(p.getId());
    o.name = p.getName();
    o.description = p.data.getDesc();

    if (p.getMainAddress() != null) {
      o.address = poiAddressToPostalAddress(p.getMainAddress());
    }

    Email e = p.getMainEmail();
    if (e != null) {
      o.email = e.getAddress();
    }

    PhoneNumber ph = p.getMainPhone();
    if (ph != null) {
      o.telephone = p.getMainPhone().getNumber();
    }

    ph = p.data.getFax();
    if (ph != null) {
      o.faxNumber = ph.getNumber();
    }

    String url = p.getMainUrl();
    o.url = url;

    List<PoiFile> fs = PoiFile.getCmpyForPoi(p.getId(), p.getCmpyId());
    if (fs != null) {
      for (PoiFile f : fs) {
        o.addImage(poiFileToImageObject(f));
      }
    }
  }

  public ImageObject poiFileToImageObject(PoiFile pf) {
    if (pf == null) {
      return null;
    }
    ImageObject io = new ImageObject();
    io.url = pf.getUrl();
    io.caption = pf.getDescription();
    io.position = pf.getPriority();
    return io;
  }

  public CruiseShipReservation tdToCruiseShipReservation(TripDetail td) {
    UmCruiseReservation cr = cast(td.getReservation(), UmCruiseReservation.class);
    
    CruiseShipReservation r  = new CruiseShipReservation();
    tdToReservationCommon(td, r, cr != null ? cr.getCruise().name : "", cr.getNotesPlainText());

    r.reservationFor = new CruiseShip();
    
    UmCruiseTraveler traveler = cr.getCruiseTraveler();
    
    r.category = traveler.getCategory();
    r.cabinNumber = traveler.getCabinNumber();
    r.deckNumber = traveler.getDeckNumber();
    r.diningPlan = traveler.getDiningPlan();
    r.bedding = traveler.getBedding();

    r.reservationFor.departTime = tsToDateTime(td.getStarttimestamp());
    r.reservationFor.arrivalTime = tsToDateTime(td.getEndtimestamp());
    r.reservationFor.departPort = locationToPlace(td.getLocStartPoiId(),
                                                  td.getLocStartPoiCmpyId(),
                                                  td.getLocStartName(),
                                                  null);
    r.reservationFor.arrivalPort = locationToPlace(td.getLocFinishPoiId(),
                                                   td.getLocFinishPoiCmpyId(),
                                                   td.getLocFinishName(),
                                                   null);
    if (td.getPoiId() != null) {
      PoiRS prs = PoiController.getMergedPoi(td.getPoiId(), td.getPoiCmpyId());
      r.reservationFor.provider = new Organization();
      poiToOrganization(prs, r.reservationFor.provider);
      r.reservationFor.provider.name = cr.getCruise().getProvider().name;

      r.image = getReservationImages(td, td.getPoiId(), td.getPoiCmpyId());
    }


    r.stops = buildCruiseStops(td.starttimestamp, td.endtimestamp);

    List<UmRate> rates = cr.getRates();

    if(rates != null) {
      rates.stream().forEach((p) -> {
        CruiseShipReservation sub = new CruiseShipReservation();
        sub.rateName = p.getDisplayName();
        sub.ratePrice = p.getPrice();
        sub.rateDescription = p.getDescription();
        r.addRates(sub);
      });
    }

    r.cancellationPolicy = cr.getCancellationPolicy();

    return r;
  }

  public List<CruiseStop> buildCruiseStops(long cruiseStart, long cruiseEnd) {
    List<CruiseStop> r = new ArrayList<>();

    for (TripDetail stop : bookings) {
      //Skipping non-cruise activities
      if (stop.detailtypeid != ReservationType.CRUISE_STOP ||
          stop.starttimestamp < cruiseStart ||
          stop.endtimestamp > cruiseEnd) {
        continue;
      }

      CruiseStop cs = new CruiseStop();
      cs.umId = stop.getDetailsid();
      cs.name = stop.getName();
      cs.port = locationToPlace(stop.getLocStartPoiId(),
                                stop.getLocStartPoiCmpyId(),
                                stop.getLocStartName(),
                                stop.getName());
      cs.arrivalTime = tsToDateTime(stop.getStarttimestamp());
      cs.departTime = tsToDateTime(stop.getEndtimestamp());
      r.add(cs);
    }

    return r;
  }

  public Place locationToPlace(Long poiId, Integer cmpyId, String poiName, String oldName) {
    Place p = new Place();
    updatePlaceFromLocation(p, poiId, cmpyId, poiName, oldName);
    return p;
  }

  public DateTime tsToDateTime(Long ts) {
    if (ts == null) {
      return null;
    }
    Instant       i   = Instant.ofEpochMilli(ts);
    LocalDateTime ldt = LocalDateTime.ofInstant(i, ZoneId.of("UTC"));
    DateTime      dt  = new DateTime(ldt);
    return dt;
  }

  public void updatePlaceFromLocation(Place p, Long poiId, Integer cmpyId, String poiName, String oldName) {
    if (poiId != null) {
      PoiRS prs = PoiController.getMergedPoi(poiId, cmpyId);
      p.name = prs.getName();
      p.address = poiAddressToPostalAddress(prs.getMainAddress());
      if (prs.getMainAddress().getCoordinates() != null) {
        p.geo = poiCoordinatesToGeoCoordinates(prs.getMainAddress().getCoordinates());
      }
      else if (prs.locLat > 0.0 && prs.locLong > 0.0) {
        p.geo = new GeoCoordinates();
        p.geo.latitude = Float.toString(prs.locLat);
        p.geo.longitude = Float.toString(prs.locLong);
      }
    }
    else {
      p.name = (poiName != null && poiName.length() >= 0) ? poiName : oldName;
    }
  }

  public String tsToISO8601(Long ts) {
    if (ts == null) {
      return "";
    }
    Instant i = Instant.ofEpochMilli(ts);
    return i.toString(); //Should output ISO8601 standard
  }

  public PostalAddress poiAddressToPostalAddress(Address a) {
    if (a == null) {
      return null;
    }

    PostalAddress pa = new PostalAddress();
    if (a.getCountryCode() != null) {
      pa.a3Country = a.getCountryCode();
      CountriesInfo.Country c = CountriesInfo.Instance().byAlpha3(pa.a3Country);
      pa.addressCountry = c.getName(CountriesInfo.LangCode.EN);
    }
    //City, Town, etc
    pa.addressLocality = a.getLocality();
    pa.addressRegion = a.getRegion();
    pa.postalCode = a.getPostalCode();
    pa.streetAddress = a.getStreetAddress();

    return pa;
  }

  public GeoCoordinates poiCoordinatesToGeoCoordinates(Coordinates c) {
    if (c == null || c.getLongitude() == null || c.getLatitude() == null) {
      return null;
    }

    try {
      GeoCoordinates gc = new GeoCoordinates();
      gc.latitude = Float.toString(c.getLatitude());
      gc.longitude = Float.toString(c.getLongitude());
      return gc;
    } catch (Exception e) {
      return null; // Ignoring exception
    }
  }


  public Airline poiToAirline(PoiRS p) {
    Airline a = new Airline();
    a.umId = Long.toString(p.getId());
    a.name = p.getName();
    a.iataCode = p.getCode();
    a.description = p.data.getDesc();
    return a;
  }

  public Airport poiToAirport(PoiRS p) {
    Airport a = new Airport();
    a.umId = Long.toString(p.getId());
    a.name = p.getName();
    a.iataCode = p.getCode();
    a.description = p.data.getDesc();

    a.address = poiAddressToPostalAddress(p.getMainAddress());
    if (p.getMainAddress().getCoordinates() != null) {
      a.geo = poiCoordinatesToGeoCoordinates(p.getMainAddress().getCoordinates());
    }
    else if (p.locLat > 0.0 && p.locLong > 0.0) {
      a.geo = new GeoCoordinates();
      a.geo.latitude = Float.toString(p.locLat);
      a.geo.longitude = Float.toString(p.locLong);
    }
    return a;
  }

  public DateTime resetMidnightDateForHotel (DateTime dt) {
    if (dt != null) {
      Calendar date = new GregorianCalendar();
      date.setTimeInMillis(dt.getTimestamp().getTime());
      if (date.get(Calendar.HOUR_OF_DAY) == 0 && date.get(Calendar.MINUTE) == 0) {
        date.set(Calendar.HOUR_OF_DAY, 23);
        date.set(Calendar.MINUTE, 59);
        dt = tsToDateTime(date.getTimeInMillis());
      }
    }
    return dt;

  }


  /*
    Stupid logic added to remove Magnatech garbage for Teplis for now
   */
  public static List<TripNote> filterMagnatech(List<TripNote> notes, Trip trip) {
    try {
      if (notes != null && notes.size() > 0) {
        boolean foundNote = false;
        for (TripNote note : notes) {
          //let's see if we find the specific string we are looking for
          if (BookingNoteController.isAffectedMagnatecNote(note.getIntro())) {
            foundNote = true;
            break;
          }
        }
        //if we found it, let's make sure that this belongs to a company that we want to filter for
        if (foundNote) {
          if (BookingNoteController.shouldMagnatechNoteBeCleaned(trip)) {
            //now we process the notes... we are making sure there is only 1 top note and the top note does not have the passenger info
            TripNote noteToClean = null;
            List<TripNote> cleaned = new ArrayList<>();
            for (TripNote note : notes) {
              //let's see if we find the specific string we are looking for
              if (BookingNoteController.isAffectedMagnatecNote(note.getIntro())) {
                if (noteToClean == null) { //only clean first one - ignore the rest
                  noteToClean = note;
                  note.setName("Company Details");
                  note.setIntro(BookingNoteController.getMagnatecCleannote(note.getIntro()));
                  cleaned.add(note);
                }
              } else {
                cleaned.add(note);
              }
            }
            if (!cleaned.isEmpty()) {
              return cleaned;
            }
          }
        }

      }
    } catch (Exception e) {
      Log.err("BookingNoteController: filterMagnatech Error: trip: " + trip.tripid, e);
    }
    return notes;
  }
}
