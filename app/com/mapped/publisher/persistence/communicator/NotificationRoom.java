package com.mapped.publisher.persistence.communicator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.firebase.client.Firebase;

import java.util.Map;

/**
 * Created by surge on 2015-08-04.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationRoom
    extends FirebaseObject {
  public String name;
  public Map<String, Notification> events;
  public Long lastActivity;


  public Firebase update(Firebase ref) {
    //Log.debug("Updating name record for:" + ref.toString());
    ref.child("name").setValue(name);
    /*
    Map<String, Object> uVals = new HashMap<>();
    uVals.put("name",name);
    ref.updateChildren(uVals);
    */
    return ref;
  }
}
