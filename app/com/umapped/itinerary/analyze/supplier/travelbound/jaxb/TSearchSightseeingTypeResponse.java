//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.04.17 at 04:55:33 PM EDT 
//


package com.umapped.itinerary.analyze.supplier.travelbound.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_SearchSightseeingTypeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_SearchSightseeingTypeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="SightseeingTypes" type="{}t_SightseeingTypes" minOccurs="0"/>
 *           &lt;element name="Errors" type="{}t_Errors" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_SearchSightseeingTypeResponse", propOrder = {
    "sightseeingTypes",
    "errors"
})
public class TSearchSightseeingTypeResponse {

    @XmlElement(name = "SightseeingTypes")
    protected TSightseeingTypes sightseeingTypes;
    @XmlElement(name = "Errors")
    protected TErrors errors;

    /**
     * Gets the value of the sightseeingTypes property.
     * 
     * @return
     *     possible object is
     *     {@link TSightseeingTypes }
     *     
     */
    public TSightseeingTypes getSightseeingTypes() {
        return sightseeingTypes;
    }

    /**
     * Sets the value of the sightseeingTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link TSightseeingTypes }
     *     
     */
    public void setSightseeingTypes(TSightseeingTypes value) {
        this.sightseeingTypes = value;
    }

    /**
     * Gets the value of the errors property.
     * 
     * @return
     *     possible object is
     *     {@link TErrors }
     *     
     */
    public TErrors getErrors() {
        return errors;
    }

    /**
     * Sets the value of the errors property.
     * 
     * @param value
     *     allowed object is
     *     {@link TErrors }
     *     
     */
    public void setErrors(TErrors value) {
        this.errors = value;
    }

}
