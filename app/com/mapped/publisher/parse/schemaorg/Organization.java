package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Type name: http://schema.org/Organization
 * <p>
 * Created by surge on 2015-05-06.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Organization
    extends Thing {

  /**
   * [PostalAddress]
   * Physical address of the item.
   */
  public PostalAddress address;
  /**
   * [AggregateRating]
   * The overall rating, based on a collection of reviews or ratings, of the item.
   */
  public String aggregateRating;
  /**
   * [Brand]  or [Organization]
   * The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business
   * person.
   */
  public String brand;
  /**
   * [ContactPoint]
   * A contact point for a person or organization. Supersedes contactPoints.
   */
  public String contactPoint;
  public String contactPoints;
  /**
   * [Organization]
   * A relationship between an organization and a department of that organization, also described as an organization
   * (allowing different urls, logos, opening hours). For example: a store with a pharmacy, or a bakery with a cafe.
   */
  public String department;
  /**
   * [Date]
   * The date that this organization was dissolved.
   */
  public String dissolutionDate;
  /**
   * [Text]
   * The Dun & Bradstreet DUNS number for identifying an organization or business person.
   */
  public String duns;
  /**
   * [Text]
   * Email address.
   */
  public String email;
  /**
   * [Person]
   * Someone working for this organization. Supersedes employees.
   */
  public String employee;
  public String employees;
  /**
   * [Event]
   * Upcoming or past event associated with this place, organization, or action. Supersedes events.
   */
  public String event;
  public String events;
  /**
   * [Text]
   * The fax number.
   */
  public String faxNumber;
  /**
   * [Person]
   * A person who founded this organization. Supersedes founders.
   */
  public String founder;
  public String founders;
  /**
   * [Date]
   * The date that this organization was founded.
   */
  public String foundingDate;
  /**
   * [Place]
   * The place where the Organization was founded.
   */
  public String foundingLocation;
  /**
   * [Text]
   * The Global Location Number (GLN, sometimes also referred to as International Location Number or ILN) of the
   * respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical
   * locations.
   */
  public String globalLocationNumber;
  /**
   * [Place]
   * Points-of-Sales operated by the organization or person.
   */
  public String hasPOS;
  /**
   * [Text]
   * A count of a specific user interactions with this item—for example, 20 UserLikes, 5 UserComments, or 300
   * UserDownloads. The user interaction type should be one of the sub types of UserInteraction.
   */
  public String interactionCount;
  public String isicV4;
  public String legalName;
  public PostalAddress location;
  /**
   * An associated logo.
   * [ImageObject]
   */
  public ImageObject logo;
  public String makesOffer;
  public Organization member;
  public Organization memberOf;
  public Organization members;
  public String naics;
  public Product owns;
  public String review;
  public String reviews;
  public String seeks;
  public String subOrganization;
  public String taxId;
  /**
   * [Text]	The telephone number.
   */
  public String telephone;
  public String vatId;

  public Organization() {
  }

  public Organization(String name) {
    this.name = name;
  }
/*


isicV4	Text 	The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4
code for a particular organization, business person, or place.
legalName	Text 	The official name of the organization, e.g. the registered company name.
location	PostalAddress  or
Place 	The location of the event, organization or action.

makesOffer	Offer 	A pointer to products or services offered by the organization or person.
member	Person  or
Organization 	A member of an Organization or a ProgramMembership. Organizations can be members of organizations;
ProgramMembership is typically for individuals. Supersedes members, musicGroupMember.
Inverse property: memberOf.
memberOf	Organization  or
ProgramMembership 	An Organization (or ProgramMembership) to which this Person or Organization belongs.
Inverse property: member.
naics	Text 	The North American Industry Classification System (NAICS) code for a particular organization or business
person.
owns	Product  or
OwnershipInfo 	Products owned by the organization or person.
review	Review 	A review of the item. Supersedes reviews.
seeks	Demand 	A pointer to products or services sought by the organization or person (demand).
subOrganization	Organization 	A relationship between two organizations where the first includes the second, e.g., as a
 subsidiary. See also: the more specific 'department' property.
taxID	Text 	The Tax / Fiscal ID of the organization or person, e.g. the TIN in the US or the CIF/NIF in Spain.

vatID	Text 	The Value-added Tax ID of the organization or person.
  */
}
