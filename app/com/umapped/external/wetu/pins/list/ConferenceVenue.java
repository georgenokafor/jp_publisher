package com.umapped.external.wetu.pins.list;

/**
 * Created by george on 2016-06-07.
 */

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "features",
    "venues",
    "packages"
})
public class ConferenceVenue extends Pin {
  @JsonProperty("features")
  private Features_ConferenceVenue features;
  @JsonProperty("venues")
  private ConferenceVenueRoom venues;
  @JsonProperty("packages")
  private ConferencePackage packages;

  @JsonProperty("features")
  public Features_ConferenceVenue getFeatures() {
    return features;
  }

  @JsonProperty("features")
  public void setFeatures(Features_ConferenceVenue features) {
    this.features = features;
  }

  @JsonProperty("venues")
  public ConferenceVenueRoom getVenues() {
    return venues;
  }

  @JsonProperty("venues")
  public void setVenues(ConferenceVenueRoom venues) {
    this.venues = venues;
  }

  @JsonProperty("packages")
  public ConferencePackage getPackages() {
    return packages;
  }

  @JsonProperty("packages")
  public void setPackages(ConferencePackage packages) {
    this.packages = packages;
  }
}
