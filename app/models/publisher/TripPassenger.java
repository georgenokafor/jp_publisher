package models.publisher;

import com.avaje.ebean.*;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-14
 * Time: 9:31 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "trip_participants")
public class TripPassenger
    extends Model {
  public static Model.Finder<String, TripPassenger> find = new Finder(TripPassenger.class);
  public String comments;
  public String groupid;
  @Id
  public String pk;
  @Constraints.Required
  public String tripid;
  public String email;
  public String name;
  public int    status;
  public Long   createdtimestamp;
  public Long   lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int    version;
  /**
   * Mobile Station International Subscriber Directory Number, i.e. mobile phone number with SIM card
   */
  String msisdn;
  /**
   * Direct Inward Dial phone number, i.e. landline phone
   */
  String did;
  /**
   * Whether or not MSISDN is confirmed as active and correct
   */
  Boolean msisdnConfirmed;

  public static TripPassenger findByPK(String pk) {
    return find.byId(pk);
  }

  public static List<TripPassenger> findAll() {
    return find.all();
  }

  public static List<TripPassenger> findAllActive() {
    return find.where().eq("status", 0).findList();
  }


  public static List<TripPassenger> findActiveByTrip(String tripId) {
    return find.where().eq("tripid", tripId).eq("status", 0).findList();
  }

  public static List<TripPassenger> findActiveByTripDefaultGroup(String tripId) {
    return find.where().eq("tripid", tripId).isNull("groupid").eq("status", 0).findList();
  }

  public static List<TripPassenger> findActiveByTripEmail(String tripId, String email) {
    return find.where().eq("tripid", tripId).ieq("email", email).eq("status", 0).findList();
  }

  public static List<TripPassenger> findActiveByTripGroup(String tripId, String groupId) {
    return find.where().eq("tripid", tripId).eq("groupid", groupId).eq("status", 0).findList();
  }

  public static int deleteByGroupId(String tripId, String groupId, String userId) {
    String s = "UPDATE trip_participants set status = -1, lastupdatedtimestamp = :timestamp, " + "modifiedby = " +
               ":userid  where tripid = :tripid and groupId = :groupid and status = 0";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("timestamp", System.currentTimeMillis());
    update.setParameter("userid", userId);
    update.setParameter("tripid", tripId);
    update.setParameter("groupid", groupId);

    return Ebean.execute(update);
  }

  public static List<TripPassenger> passengersForAlert(long alertId) {
    String sql = "SELECT tp.pk, tp.tripid, tp.email, tp.name, tp.status, tp.createdtimestamp, " +
                 "       tp.lastupdatedtimestamp, tp.createdby, tp.modifiedby, tp.version, tp.comments, tp.groupid " +
                 "FROM trip_participants tp " +
                 "WHERE tp.tripid IN " +
                 "      (SELECT tripid " +
                 "       FROM trip_detail " +
                 "       WHERE detailsid IN " +
                 "              (SELECT details_id" +
                 "               FROM flight_alert_booking" +
                 "               WHERE flight_alert_id = :flightAlertId) " +
                 "             AND status = 0 ) " +
                 "       AND tp.status = 0;";

    RawSql rawSql = RawSqlBuilder
        .parse(sql)
        .columnMapping("tp.pk", "pk")
        .columnMapping("tp.tripid", "tripid")
        .columnMapping("tp.email", "email")
        .columnMapping("tp.name", "name")
        .columnMapping("tp.status", "status")
        .columnMapping("tp.createdtimestamp", "createdtimestamp")
        .columnMapping("tp.lastupdatedtimestamp", "lastupdatedtimestamp")
        .columnMapping("tp.createdby", "createdby")
        .columnMapping("tp.modifiedby", "modifiedby")
        .columnMapping("tp.version", "version")
        .columnMapping("tp.comments", "comments")
        .columnMapping("tp.groupid", "groupid")
        .create();

    Query<TripPassenger> query = Ebean.find(TripPassenger.class);
    query.setRawSql(rawSql).setParameter("flightAlertId", alertId);
    return query.findList();
  }

  public Boolean getMsisdnConfirmed() {
    return msisdnConfirmed;
  }

  public void setMsisdnConfirmed(Boolean msisdnConfirmed) {
    this.msisdnConfirmed = msisdnConfirmed;
  }

  public String getMsisdn() {
    return msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }

  public String getDid() {
    return did;
  }

  public void setDid(String did) {
    this.did = did;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getPk() {
    return pk;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public String getTripid() {
    return tripid;
  }

  public void setTripid(String tripid) {
    this.tripid = tripid;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getGroupid() {
    return groupid;
  }

  public void setGroupid(String groupId) {
    this.groupid = groupId;
  }
}
