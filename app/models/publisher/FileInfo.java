package models.publisher;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.avaje.ebean.Model;
import com.google.common.primitives.Longs;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.persistence.RecordState;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import com.mapped.publisher.utils.Utils;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;

/**
 * Created by surge on 2015-11-04.
 */
@Entity
@Table(name = "file")
public class FileInfo
    extends Model {

  public static final Long UNKNOWN_FILESIZE = -1L;

  public final static Model.Finder<Long, FileInfo> find = new Finder(FileInfo.class);
  @Id
  Long pk;
  /**
   * Original filename)
   */
  String filename;
  /**
   * Mime filetype)
   */
  @ManyToOne
  @JoinColumn(name = "filetype", nullable = false, referencedColumnName = "extension")
  LstFileType filetype;
  /**
   * Source
   */
  @ManyToOne
  @JoinColumn(name = "source", nullable = false, referencedColumnName = "name")
  FileSrc     source;
  /**
   * File size in bytes (should be equal to the value as S3 stores it)
   */
  Long   filesize;
  /**
   * MD5 Unique file hash in order to avoid duplicates
   */
  byte[] hash;
  /**
   * File description that will appear to the user
   */
  String description;
  /**
   * Search terms that might be used to find this file, i.e. some keywords extracted from inside
   */
  String searchWords;
  /**
   * (FUTURE USE) Tags to identify file, say type of the invoice, etc
   */
  @Transient
  Map<String, String> tags;
  /**
   * Umapped internal filepath
   */
  String filepath;
  /**
   * S3 Bucket name where the file is stored
   */
  String bucket;
  /**
   * Publicly Accessible URL
   */
  String url;
  /**
   * State of the file, file might be marked as deleted and preserved for future use
   */
  @Enumerated(value = EnumType.STRING)
  RecordState state;
  /**
   * Timestamp when the file should expire, be deleted
   */
  Timestamp expire;
  Timestamp createdTs;
  Long      createdBy;
  @Version
  int version;

  public static FileInfo buildFileInfo(Account account, FileSrc.FileSrcType srcType, int expireDays) {
    FileSrc src = FileSrc.find.byId(srcType);
    return buildFileInfo(account, src, expireDays);
  }

  public String getEncodedPk() {
    return Utils.base64encode(getPk());
  }

  public FileInfo findByEncodedPk(String spk) {
    Long lpk = Utils.base64decode(spk);
    return find.byId(lpk);
  }

  public static FileInfo buildFileInfo(Account account, FileSrc source, int expireDays) {
    return buildFileInfo(account.getUid(), source, expireDays);
  }

  /**
   * Pending file records are needed during uploads
   * @param aid
   * @param source
   * @return
   */
  public static FileInfo buildPending(Long aid, FileSrc source) {
    FileInfo fi = buildFileInfo(aid, source, 1); //Expire in 1 day
    fi.setState(RecordState.PENDING);
    //Temporary MD5 based on File PK
    byte[] pkBytes = Longs.toByteArray(fi.getPk());
    byte[] md5 =  DigestUtils.md5(pkBytes);
    fi.setHash(md5);
    fi.setFilesize(-1l); //Placeholder showing it is pending;
    return fi;
  }

  public static FileInfo buildFileInfo(Long aid, FileSrc source, int expireDays) {
    FileInfo file = new FileInfo();
    file.setPk(DBConnectionMgr.getUniqueLongId());
    file.setCreatedBy(aid);
    file.setSource(source);

    Instant i = Instant.now();
    file.setCreatedTs(Timestamp.from(i));

    LocalDateTime ldt = LocalDateTime.ofInstant(Instant.now(), ZoneId.of("UTC"));
    ldt.plusDays(expireDays);

    file.setExpire(Timestamp.from(ldt.toInstant(ZoneOffset.UTC)));
    file.setBucket(source.getRealBucketName());
    file.setState(RecordState.ACTIVE);
    return file;
  }

  public FileInfo setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public static FileInfo buildFileInfo(Long aid, FileSrc.FileSrcType srcType, int expireDays) {
    FileSrc src = FileSrc.find.byId(srcType);
    return buildFileInfo(aid, src, expireDays);
  }

  public static boolean isPresent(byte[] hash) {
    return find.where().eq("hash", hash).findCount() != 0;
  }

  public static FileInfo byHash(byte[] hash) {
    List<FileInfo> files = find.where().eq("hash", hash).findList();
    if (files != null && files.size() > 0) {
      return files.get(0);
    }
    return null;
  }

  public static FileInfo byHashAndBucket(final byte[] hash, final String bucket) {
    return find.where().eq("hash", hash).eq("bucket", bucket).findUnique();
  }

  public static FileInfo byHash(String hexString) {
    if (hexString == null) {
      return null;
    }
    try {
      Hex hex = new Hex();
      return byHash((byte[]) hex.decode(hexString));
    }
    catch (DecoderException de) {
      Log.err("FileInfo: Failed to decode MD5 String representation: " + hexString, de);
      return null;
    }
  }

  public static FileInfo byHashAndBucket(String hexString, String bucket) {
    if (hexString == null || bucket == null) {
      return null;
    }
    try {
      Hex hex = new Hex();
      return byHashAndBucket((byte[]) hex.decode(hexString), bucket);
    }
    catch (DecoderException de) {
      Log.err("FileInfo: Failed to decode MD5 String representation: " + hexString, de);
      return null;
    }
  }


  public static FileInfo byBucketAndKey(String bucket, String key) {
    return find.where().eq("bucket", bucket).eq("filepath", key).findUnique();
  }

  public static FileInfo byBucketAndKeyFromSource(FileSrc srcType, String bucket, String key) {
    return find.where().eq("bucket", bucket).eq("filepath", key).eq("source", srcType).findUnique();
  }

  public static FileInfo hasRecord(byte[] hash) {
    return find.where().eq("hash", hash).findUnique();
  }

  public Timestamp getExpire() {
    return expire;
  }

  public FileInfo setExpire(Timestamp expire) {
    this.expire = expire;
    return this;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public FileInfo setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
    return this;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public FileInfo setCreatedBy(Account createdBy) {
    this.createdBy = createdBy.getUid();
    return this;
  }

  public int getVersion() {
    return version;
  }

  public FileInfo setVersion(int version) {
    this.version = version;
    return this;
  }

  public RecordState getState() {
    return state;
  }

  public FileInfo setState(RecordState state) {
    this.state = state;
    return this;
  }

  public String getSearchWords() {
    return searchWords;
  }

  public FileInfo setSearchWords(String searchWords) {
    this.searchWords = searchWords;
    return this;
  }

  public Long getPk() {
    return pk;
  }

  public void setPk(Long pk) {
    this.pk = pk;
  }

  public String getFilename() {
    return filename;
  }

  public FileInfo setFilename(String filename) {
    this.filename = filename;
    return this;
  }

  public LstFileType getFiletype() {
    return filetype;
  }

  public FileInfo setFiletype(LstFileType filetype) {
    this.filetype = filetype;
    return this;
  }

  public FileSrc getSource() {
    return source;
  }

  public FileInfo setSource(FileSrc source) {
    this.source = source;
    return this;
  }

  public Long getFilesize() {
    return filesize;
  }

  public FileInfo setFilesize(Long filesize) {
    this.filesize = filesize;
    return this;
  }

  public byte[] getHash() {
    return hash;
  }

  public FileInfo setHash(byte[] hash) {
    this.hash = hash;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public FileInfo setDescription(String description) {
    this.description = description;
    return this;
  }

  public Map<String, String> getTags() {
    return tags;
  }

  public FileInfo setTags(Map<String, String> tags) {
    this.tags = tags;
    return this;
  }

  public String getUrl() {
    return url;
  }

  public FileInfo setUrl(String url) {
    this.url = url;
    return this;
  }

  /**
   * Provided S3 ObjectMetadata object updates file size and MD5
   *
   * @param meta S3 ObjectMetadata
   */
  public boolean updateFromMetadata(ObjectMetadata meta) {
    if (meta != null) {
      setFilesize(meta.getContentLength());
      return setHash(meta.getETag());
    }
    return false;
  }

  /**
   * Sets hash from HEX encoded MD5 representation
   *
   * @param hexString String HEX encoded MD5
   */
  public boolean setHash(String hexString) {
    if (hexString == null) {
      return false;
    }
    try {
      Hex hex = new Hex();
      setHash((byte[]) hex.decode(hexString));
      return true;
    }
    catch (DecoderException de) {
      Log.err("FileInfo: Failed to decode MD5 String representation: " + hexString, de);
      return false;
    }
  }

  /**
   * If bucket allows public access - then just create public url
   */
  public FileInfo setAsPublicUrl() {
    setUrl(S3Util.generatePublicUrl(getBucket(), getFilepath()));
    return this;
  }

  public String getBucket() {
    return bucket;
  }

  public FileInfo setBucket(String bucket) {
    this.bucket = bucket;
    return this;
  }

  public String getFilepath() {
    return filepath;
  }

  public FileInfo setFilepath(String filepath) {
    this.filepath = filepath;
    return this;
  }

}
