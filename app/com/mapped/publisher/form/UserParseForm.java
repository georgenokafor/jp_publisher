package com.mapped.publisher.form;

/**
 * Created by ryan on 29/04/17.
 */
public class UserParseForm {

    private String inUserId;
    private String inCmpyId;
    public String inCmpyName;

    private String inUserEmail;
    private Boolean inCmpyMode;

    public UserParseForm() {

    }

    public Boolean getInCmpyMode() {
        return this.inCmpyMode;
    }

    public void setInCmpyMode(Boolean inCmpyMode) {
        this.inCmpyMode = inCmpyMode;
    }

    public String getInUserId() {
        return this.inUserId;
    }

    public void setInUserId(String inUserId) {
        this.inUserId = inUserId;
    }

    public String getInCmpyId() {
        return this.inCmpyId;
    }

    public void setInCmpyId(String inCmpyId) {
        this.inCmpyId = inCmpyId;
    }

    public String getInUserEmail() {
        return this.inUserEmail;
    }

    public void setInUserEmail(String inUserEmail) {
        this.inUserEmail = inUserEmail;
    }
}
