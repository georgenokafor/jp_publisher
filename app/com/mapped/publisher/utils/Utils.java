package com.mapped.publisher.utils;

import com.google.common.primitives.Longs;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.view.AttachmentView;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.HeadMethod;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.DateFormatSymbols;


/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 12-07-23
 * Time: 10:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class Utils {
  private static final char[] symbols = new char[36];

  static final Pattern TEL_PATTERN = Pattern.compile("[\\+\\(0-9]+[' '-\\.0-9]{5,}[0-9]");

  private static final Pattern FlightNumPattern = Pattern.compile("(\\d+)$");

  static {
    for (int idx = 0; idx < 10; ++idx) {
      symbols[idx] = (char) ('0' + idx);
    }
    for (int idx = 10; idx < 36; ++idx) {
      symbols[idx] = (char) ('a' + idx - 10);
    }
  }

  public static boolean compareTrimmedIgnoreCase(String a, String b) {
    return a != null && b != null && a.trim().equalsIgnoreCase(b.trim());
  }

  public static String randomString(int length) {
    Random random = new Random();
    char[] buf = new char[length];
    for (int idx = 0; idx < buf.length; ++idx) {
      buf[idx] = symbols[random.nextInt(symbols.length)];
    }
    return new String(buf);
  }

  public static boolean isValidEmailAddress(String email) {
    Pattern p = Pattern.compile(".+@.+\\.[a-z,A-Z]+");
    Matcher m = p.matcher(email);
    boolean matchFound = m.matches();

    if (matchFound) {
      return true;
    }
    else {
      return false;
    }
  }

  public static String hashPwd(String pwd, String salt) {
    try {
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      md.reset();
      md.update(salt.getBytes("UTF-8"));

      byte[] hash = md.digest(pwd.getBytes("UTF-8"));

      StringBuffer hexString = new StringBuffer();
      for (int i = 0; i < hash.length; i++) {
        String hex = Integer.toHexString(0xff & hash[i]);
        if (hex.length() == 1) {
          hexString.append('0');
        }
        hexString.append(hex);
      }
      return hexString.toString();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public static String formatTimestamp(String millisStr) {
    if (millisStr == null) {
      return "";
    }

    long millis = 0;
    try {
      millis = Long.parseLong(millisStr);
    }
    catch (Exception ne) {

      return "";
    }

    return formatDateTimePrint(millis);
  }

  public static String formatDate(String millisStr) {
    if (millisStr == null) {
      return "";
    }

    long millis = 0;
    try {
      millis = Long.parseLong(millisStr);
    }
    catch (Exception ne) {

      return "";
    }

    String dateFormat = "MMM dd, yyyy";

    Date now = new Date(millis);
    DateTime dt = new DateTime(now);
    if (dt.getYear() == 70 || dt.getYear() == 1970) {
      return "";
    }

    // if (dt.getHourOfDay() == 0 ) {
    SimpleDateFormat timestampFormat = new SimpleDateFormat(dateFormat);
    return timestampFormat.format(now);

    // }

  }

  public static String getDateStringPrint(long timestamp) {
    try {
      Date d = new Date(timestamp);
      String dateFormat = "MMM dd, yyyy";

      SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
      return dateFormatter.format(d);
    }
    catch (Exception e) {
      return null;
    }
  }

  public static String getDayMonthString(long timestamp) {
    try {
      Date d = new Date(timestamp);
      String dateFormat = "MMM dd";

      SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
      return dateFormatter.format(d);
    }
    catch (Exception e) {
      return null;
    }
  }

  public static String formatTimestampPrint(Long millis) {
    if (millis == null || millis == 0) {
      return "";
    }


    String dateTimeFormat = "yyyy-MM-dd HH:mm";
    String dateFormat = "yyyy-MM-dd";

    Date now = new Date(millis.longValue());

    DateTime dt = new DateTime(now);
    if (dt.getYear() == 70 || dt.getYear() == 1970) {
      return "";
    }

    if (dt.getHourOfDay() == 0 && dt.getMinuteOfDay() == 0) {
      SimpleDateFormat timestampFormat = new SimpleDateFormat(dateFormat);
      return timestampFormat.format(now);

    }
    else {
      SimpleDateFormat timestampFormat = new SimpleDateFormat(dateTimeFormat);
      return timestampFormat.format(now);

    }
  }

  /**
   * Applies offset to the timestamp and formats output ready for web display
   *
   * @param ts           - timestampt to print
   * @param offsetMillis - signed offset to the timestamp
   * @return Formatted date & time output as provided via input
   */
  public static String formatOffsetDateTimePrint(Timestamp ts, long offsetMillis) {
    if (ts == null) {
      return "";
    }

    long offsetTS = ts.getTime() + offsetMillis;
    return formatDateTimePrint(offsetTS);
  }

  public static String formatOffsetDateTimePrint(long ts, long offsetMillis) {
    return formatDateTimePrint(ts + offsetMillis);
  }

  public static String formatDateTimePrint(long millis) {
    String dateTimeFormat = "EEE MMM dd, yyyy - HH:mm";
    String dateFormat = "EEE MMM dd, yyyy";

    Date date = new Date(millis);

    DateTime dt = new DateTime(date);
    if (dt.getYear() == 70 || dt.getYear() == 1970) {
      return "";
    }

    if (dt.getHourOfDay() == 0 && dt.getMinuteOfDay() == 0) {
      SimpleDateFormat timestampFormat = new SimpleDateFormat(dateFormat);
      return timestampFormat.format(date);

    }
    else {
      SimpleDateFormat timestampFormat = new SimpleDateFormat(dateTimeFormat);
      return timestampFormat.format(date);
    }
  }

  public static String formatDateTimeSecondsPrint(long millis) {
    String dateTimeFormat = "EEE MMM dd, yyyy - HH:mm:ss";
    String dateFormat = "EEE MMM dd, yyyy";

    Date date = new Date(millis);

    DateTime dt = new DateTime(date);
    if (dt.getYear() == 70 || dt.getYear() == 1970) {
      return "";
    }

    if (dt.getHourOfDay() == 0 && dt.getMinuteOfDay() == 0) {
      SimpleDateFormat timestampFormat = new SimpleDateFormat(dateFormat);
      return timestampFormat.format(date);

    }
    else {
      SimpleDateFormat timestampFormat = new SimpleDateFormat(dateTimeFormat);
      return timestampFormat.format(date);
    }
  }

  public static String formatDatePrint(long millis) {
    String dateFormat = "EEE MMM dd, yyyy";

    Date date = new Date(millis);

    DateTime dt = new DateTime(date);
    if (dt.getYear() == 70 || dt.getYear() == 1970) {
      return "";
    }

    SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
    return dateFormatter.format(date);

  }

  public static String formatDatePrint(long millis, Locale locale) {
    String dateFormat = "EEE MMM dd, yyyy";

    Date date = new Date(millis);

    DateTime dt = new DateTime(date);
    if (dt.getYear() == 70 || dt.getYear() == 1970) {
      return "";
    }

    SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat, locale);
    return dateFormatter.format(date);

  }

  public static String formatTime24(Long millis) {
    if (millis == null || millis == 0) {
      return "";
    }


    String timeFormat = "HH:mm";

    Date now = new Date(millis.longValue());

    DateTime dt = new DateTime(now);
    if (dt.getYear() == 70 || dt.getYear() == 1970) {
      return "";
    }

    if (dt.getHourOfDay() == 0 && dt.getMinuteOfDay() == 0) {
      return "";
    }
    else {
      SimpleDateFormat timestampFormat = new SimpleDateFormat(timeFormat);
      return timestampFormat.format(now);

    }
  }

  public static String formatTime12(Long millis) {
    if (millis == null || millis == 0) {
      return "";
    }


    String timeFormat = "h:mm a";

    Date now = new Date(millis.longValue());

    DateTime dt = new DateTime(now);
    if (dt.getYear() == 70 || dt.getYear() == 1970) {
      return "";
    }

    if (dt.getHourOfDay() == 0 && dt.getMinuteOfDay() == 0) {
      return "";
    }
    else {
      SimpleDateFormat timestampFormat = new SimpleDateFormat(timeFormat);
      return timestampFormat.format(now);

    }
  }

  public static String formatTime12NoIndicator(Long millis) {
    if (millis == null || millis == 0) {
      return "";
    }


    String timeFormat = "h:mm";

    Date now = new Date(millis.longValue());

    DateTime dt = new DateTime(now);
    if (dt.getYear() == 70 || dt.getYear() == 1970) {
      return "";
    }

    if (dt.getHourOfDay() == 0 && dt.getMinuteOfDay() == 0) {
      return "";
    }
    else {
      SimpleDateFormat timestampFormat = new SimpleDateFormat(timeFormat);
      return timestampFormat.format(now);

    }
  }

  public static String formatTime12Indicator(Long millis) {
    if (millis == null || millis == 0) {
      return "";
    }


    String timeFormat = "a";

    Date now = new Date(millis.longValue());

    DateTime dt = new DateTime(now);
    if (dt.getYear() == 70 || dt.getYear() == 1970) {
      return "";
    }

    if (dt.getHourOfDay() == 0 && dt.getMinuteOfDay() == 0) {
      return "";
    }
    else {
      SimpleDateFormat timestampFormat = new SimpleDateFormat(timeFormat);
      return timestampFormat.format(now);

    }
  }

  public static String formatDateTimePrint(Long millis) {
    if (millis == null) {
      return "";
    }

    return formatDateTimePrint(millis.longValue());
  }

  public static String formatDayMonthPagePrint(Long millis) {
    if (millis == null || millis == 0) {
      return "";
    }


    String dateTimeFormat = "MMMMM dd";

    Date now = new Date(millis.longValue());

    DateTime dt = new DateTime(now);
    if (dt.getYear() == 70 || dt.getYear() == 1970) {
      return "";
    }

    SimpleDateFormat timestampFormat = new SimpleDateFormat(dateTimeFormat);
    return timestampFormat.format(now);

  }

  public static String formatDateTimePagePrint(Long millis) {
    if (millis == null || millis == 0) {
      return "";
    }


    String dateTimeFormat = "MMMMM dd, yyyy @ HH:mm";
    String dateFormat = "MMMMM dd, yyyy";

    Date now = new Date(millis.longValue());

    DateTime dt = new DateTime(now);
    if (dt.getYear() == 70 || dt.getYear() == 1970) {
      return "";
    }

    if (dt.getHourOfDay() == 0 && dt.getMinuteOfDay() == 0) {
      SimpleDateFormat timestampFormat = new SimpleDateFormat(dateFormat);
      return timestampFormat.format(now);

    }
    else {
      SimpleDateFormat timestampFormat = new SimpleDateFormat(dateTimeFormat);
      return timestampFormat.format(now);

    }
  }

  public static String formatDateTimePagePrint(String timestamp) {
    if (timestamp == null || timestamp.length() == 0) {
      return "";
    }

    long millis = getMilliSecs(timestamp);


    String dateTimeFormat = "MMM dd, yyyy, hh:mm a";
    String dateFormat = "MMMMM dd, yyyy";

    Date now = new Date(millis);

    DateTime dt = new DateTime(now);
    if (dt.getYear() == 70 || dt.getYear() == 1970) {
      return "";
    }

    if (dt.getHourOfDay() == 0 && dt.getMinuteOfDay() == 0) {
      SimpleDateFormat timestampFormat = new SimpleDateFormat(dateFormat);
      return timestampFormat.format(now);

    }
    else {
      SimpleDateFormat timestampFormat = new SimpleDateFormat(dateTimeFormat);
      return timestampFormat.format(now);

    }
  }

  public static String getYear(Long millis) {
    if (millis == null || millis <= 0) {
      return "";
    }

    String dateFormat = "yyyy";
    SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
    Date d = new Date(millis);
    return dateFormatter.format(d);
  }

  public static String formatDateControl(Long millis) {
    if (millis == null || millis <= 0) {
      return "";
    }

    String dateFormat = "MM/dd/yy";
    SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
    Date d = new Date(millis);
    return dateFormatter.format(d);
  }

  public static String formatDateControlYYYY(Long millis) {
    if (millis == null || millis <= 0) {
      return "";
    }

    String dateFormat = "MM/dd/yyyy";
    SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);

    if (millis > 0) {
      Date d = new Date(millis);
      return dateFormatter.format(d);
    }
    return "";
  }

  public static String authorizeS3Get(String filePath) {

    return S3Util.authorizeS3GetSigned(S3Util.BUCKET_NAME, filePath);

  }

  public static String getISO8601Date(Timestamp timestamp) {
    if(timestamp == null) {
      return "";
    }
    try {

      java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ISO_LOCAL_DATE
                                                                                       .withZone(ZoneId.of("UTC"));
      return formatter.format(timestamp.toInstant());
    }
    catch (DateTimeException e) {
      return "";
    }
  }


  public static String getISO8601Date(Long timestamp) {
    if(timestamp == null) {
      return "";
    }
    try {
      java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ISO_LOCAL_DATE
                                                    .withZone(ZoneId.of("UTC"));
      return formatter.format(Instant.ofEpochMilli(timestamp));
    }
    catch (DateTimeException e) {
      return "";
    }
  }

  public static String getISO8601Timestamp(Long timestamp) {
    if (timestamp == null) {
      return "";
    }
    return getISO8601Timestamp(timestamp.longValue());
  }


  public static String getISO8601Timestamp(long timestamp) {
    Date d = new Date(timestamp);
    return getISO8601Timestamp(d);
  }

  public static String getISO8601Timestamp(Date d) {
    try {
      SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
      return dateFormatter.format(d);
    }
    catch (Exception e) {
      return "";
    }
  }

  public static Date iso8601toDate(String iso8601ts) {
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    Date date = null;
    try {
      date = df.parse(iso8601ts);
    } catch (ParseException pe) {
      Log.log(LogLevel.ERROR, "Failed to parse date ISO8601 timestamp:" + iso8601ts);
    }
    return date;
  }

  public static long getMilliSecs(String timestamp) {
    //Timestamp length of 5 needs to be modified if a shorter template is acceptable
    if (timestamp == null || timestamp.length() < 5) {
      Log.err("Attempt to interpret >" +timestamp+ "< as timestamp has failed");
      return -1;
    }

    try {
      String[] formatters = {"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'","yyyy-MM-dd'T'HH:mm:ss.SSS",
        "MM/dd/yy",
        "MM/dd/yy hh:mm a",
        "MM/dd/yyyy",
        "yyyy-MM-dd",
        "EEE MMM dd, yyyy",
        "MMM dd, yyyy hh:mm a",
        "MMM dd, yyyy",
        "ddd MMM dd,",
        "hh:mm a",
        "HH:mm"
      };

      Date date = DateUtils.parseDate(timestamp, formatters);
      return date.getTime();
    }
    catch (Exception e) {
      System.out.println("===== " + timestamp);
      e.printStackTrace();
      return -1;
    }
  }


  public static long getCurrentYearDate(long ts, long currTs) {
    DateTime dateTime = new DateTime(ts);
    DateTime currDT = new DateTime(currTs);
    DateTime.Property year = dateTime.year();
    int currYear = currDT.getYear();
    int yearsToAdd = currYear - year.get();
    dateTime = dateTime.plusYears(yearsToAdd);
    return dateTime.getMillis();
  }

  /**
   * Return the same date (Month, day) but next closest occurance after currTs
   * @param currTs - current year's timestamp (any date)
   * @return
   */
  public static long getNextDateOccurrence(long ts, long currTs) {
    DateTime dateTime = new DateTime(ts);
    DateTime currDT = new DateTime(currTs);
    DateTime dateThisYear = dateTime.withYear(currDT.getYear());
    if(dateThisYear.isBefore(currDT)) { //Date passed so we need to add another year
      return dateThisYear.plusYears(1).getMillis();
    } else {
      return dateTime.getMillis();
    }
  }

  public static long addYearsToTs(long ts, int years) {
    DateTime dateTime = new DateTime(ts);
    dateTime = dateTime.plusYears(years);
    return dateTime.getMillis();
  }

  public static long addDaysToTs(long ts, int days) {
    DateTime dateTime = new DateTime(ts);
    dateTime = dateTime.plusDays(days);
    return dateTime.getMillis();
  }


  public static int getDaysBetween(Long tsBefore, Long tsAfter) {
    if (tsBefore == null || tsAfter == null) {
      return 0;
    }

    Date date = new Date(tsBefore);
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    int yyyyBefore = cal.get(Calendar.YEAR);
    int ddBefore = cal.get(Calendar.DAY_OF_YEAR);

    date = new Date(tsAfter);
    cal.setTime(date);
    int yyyyAfter = cal.get(Calendar.YEAR);
    int ddAfter = cal.get(Calendar.DAY_OF_YEAR);

    int result = (ddAfter + yyyyAfter * 365) - (ddBefore + yyyyBefore*365);
    if (result < 0) {
      Log.log(LogLevel.ERROR, "Likely error in source dates");
    }
    return result;
  }

  public static long extractHoursMinutes(Long timestamp) {
    if (timestamp == null) {
      return 0;
    }

    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    cal.setTimeInMillis(timestamp);
    long hh = cal.get(Calendar.HOUR_OF_DAY);
    long mm = cal.get(Calendar.MINUTE);
    return (hh*60 + mm)*60000;
  }

  public static String getDateString(long timestamp) {
    try {
      Date d = new Date(timestamp);
      String dateFormat = "MM/dd/yy";

      SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
      return dateFormatter.format(d);
    }
    catch (Exception e) {
      return null;
    }
  }

  public static String getDateStringFullPrint(long timestamp) {
    try {
      Date d = new Date(timestamp);
      String dateFormat = "EEEE MMMM dd, yyyy";

      SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
      return dateFormatter.format(d);
    }
    catch (Exception e) {
      return null;
    }
  }

  public static String formatTimestamp(long timestamp, String dateFormat) {
    try {
      Date d = new Date(timestamp);
      SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
      return dateFormatter.format(d);
    }
    catch (Exception e) {
      return null;
    }
  }

  public static String formatTimestamp(long timestamp, String dateFormat, String lang) {

    Locale locale = null;
    try{
      //TODO - remove restriction as we support more locales
      if (lang !=  null && lang.toLowerCase().equals("pt")) {
        locale = new Locale("pt");
      } else if (lang !=  null && lang.toLowerCase().equals("es")) {
        locale = new Locale("es");
      } else if (lang !=  null && lang.toLowerCase().equals("fr")) {
      locale = new Locale("fr");
      }
    } catch (Exception e) {

    }
    if (locale == null) {
      locale = new Locale("en");
    }

    try {

      Date d = new Date(timestamp);
      SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat, locale);
      return dateFormatter.format(d);
    }
    catch (Exception e) {
      return null;
    }
  }

  public static String getMonthStringPrint(long timestamp) {
    try {
      Date d = new Date(timestamp);
      String dateFormat = "MMMM yyyy";

      SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
      return dateFormatter.format(d);
    }
    catch (Exception e) {
      return null;
    }
  }

  public static String getDayMonthStringPrint(long timestamp) {
    try {
      Date d = new Date(timestamp);
      String dateFormat = "MMM dd";

      SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
      return dateFormatter.format(d);
    }
    catch (Exception e) {
      return null;
    }
  }

  public static String getTimeString(String timestamp) {
    try {
      long t = Long.parseLong(timestamp);
      return getTimeString(t);
    }
    catch (Exception e) {
      return "";
    }
  }

  public static String getTimeString(Long timestamp) {
    if (timestamp == null) {
      return "";
    }
    return getTimeString(timestamp.longValue());
  }

  public static String getTimeString(long timestamp) {
    try {
      Date d = new Date(timestamp);
      String dateFormat = "hh:mm a";


      SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
      String s = dateFormatter.format(d);
      if (!s.equals("12:00 AM")) {
        return s;
      }
    }
    catch (Exception e) {

    }
    return "";
  }

  public static String getUniqueId() {
    return DBConnectionMgr.getUniqueId();
  }

  public static String hash(String s) {
    try {
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      byte[] hash = md.digest(s.getBytes("UTF-8"));

      StringBuffer hexString = new StringBuffer();
      for (int i = 0; i < hash.length; i++) {
        String hex = Integer.toHexString(0xff & hash[i]);
        if (hex.length() == 1) {
          hexString.append('0');
        }
        hexString.append(hex);
      }
      return hexString.toString();

    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "DBConnectionMgr:getUniqueId", e);
    }
    return (DBConnectionMgr.getUniqueId());
  }

  //return a shorten url type string
  public static String shortenNumber(long l) {
    if (l > 0) {
      List chars = ConfigMgr.getInstance().getCharList();


      boolean flag = true;
      long number = l;
      StringBuffer sb = new StringBuffer();
      while (flag) {

        int rem = (int) (number % 62); //26 + 10
        number = (long) (number / 62);

        sb.insert(0, chars.get(rem));

        if (number < 62) {
          flag = false;
          int i = (int) number;
          sb.insert(0, chars.get(i));
        }
      }
      return sb.toString();
    }

    return null;
  }

  /**
   * Helper given epoch millisecond timestamp and number of days check if
   * timestamp is older than number of days.
   *
   * @param timestampMs
   * @param numberOfDays
   * @return
   */
  public static boolean isOlderThan(long timestampMs, int numberOfDays) {
    return System.currentTimeMillis() - numberOfDays * 24 * 3600 * 1000l  > timestampMs;
  }


  //return a shorten url type string
  public static long expandNumber(String sb) {
    if (sb != null) {
      List chars = ConfigMgr.getInstance().getCharList();


      long num = 0;
      int pow = 0;
      for (int i = (sb.length() - 1); i >= 0; i--) {
        int digit = chars.indexOf(sb.charAt(i));
        if (digit < 0) {
          return -1; //unknown char
        }
        if (pow == 0) {
          num = digit;
        }
        else {
          num += (digit * ((long) Math.pow(62, pow)));
        }

        pow++;
      }
      return num;
    }

    return -1;
  }

  //return a shorten url type string
  // use base 62 with repeating alphabet set all lower case for historical reasons
  public static String shortenLCNumber(long l) {
    if (l > 0) {
      List chars = ConfigMgr.getInstance().getLowerCharList();


      boolean flag = true;
      long number = l;
      StringBuffer sb = new StringBuffer();
      while (flag) {

        int rem = (int) (number % 36); //26 + 10
        number = (long) (number / 36);

        sb.insert(0, chars.get(rem));

        if (number < 36) {
          flag = false;
          int i = (int) number;
          sb.insert(0, chars.get(i));
        }
      }
      return sb.toString();
    }

    return null;
  }

  //return a shorten url type string
  // use base 62 with repeating alphabet set all lower case for historical reasons
  public static long expandLCNumber(String sb) {
    if (sb != null) {
      List chars = ConfigMgr.getInstance().getLowerCharList();


      long num = 0;
      int pow = 0;
      for (int i = (sb.length() - 1); i >= 0; i--) {
        int digit = chars.indexOf(sb.charAt(i));
        if (digit < 0) {
          return -1; //unknown char
        }
        if (pow == 0) {
          num = digit;
        }
        else {
          num += (digit * ((long) Math.pow(36, pow)));
        }

        pow++;
      }
      return num;
    }

    return -1;
  }

  /**
   * Replaces Unicode curly single and double quotes with ASCII equivalent ones additionally trips the string
   *
   * @param src
   * @return
   */
  public static String curlyQuotesToAscii(final String src) {
    return src.replace("/[\u2018\u2019]/g", "'")
              .replace("/[\u201C\u201D]/g", "\"").trim();
  }


  public static String escapeHtml(String s) {
    if (s != null && !s.isEmpty()) {
      String newString = s.replaceAll("<br/>", "\n");
      newString = newString.replaceAll("<br>", "\n");
      newString = newString.replaceAll("<wbr>", "\n");

      newString = StringUtils.replaceEach(newString,
                                              new String[]{"&", "\"", "<", ">"},
                                              new String[]{"&amp;", "&quot;", "&lt;", "&gt;"});
      newString = newString.replaceAll("\n", "<br/>");
      newString = newString.replaceAll("\r", "");
      //fix to allow for display of multiple white spaces
      //HTMl spec will supress more than 1 space... so we place every 2 space wih the <wbr>&nbsp;... <wbr> is needed to make sure the line wraps properly since &nbsp; is non-wrapping
      //newString = newString.replaceAll("  ", " <wbr>&nbsp;");

            /*
            try {
                newString = new Markdown4jProcessor().process(newString);
            } catch (Exception e) {
              e.printStackTrace();
            }
            */
      return newString;
    }

    return s;

  }

  public static String escapeHtmlBr(String s) {
    if (s != null && !s.isEmpty()) {
      String newString = s.replaceAll("<br/>", "\n");
      newString = newString.replaceAll("<br>", "\n");
      newString = newString.replaceAll("<wbr>", "\n");
      newString = newString.replaceAll("&nbsp;", " ");

      newString = StringUtils.replaceEach(newString,
                                          new String[]{"&", "\"", "<", ">"},
                                          new String[]{"&amp;", "&quot;", "&lt;", "&gt;"});
      newString = newString.replaceAll("\r", "");
      return newString;
    }

    return s;

  }


  public static String escapeFileName(String f) {
    if (f != null) {
      f = f.replace("%", "__");
      f = f.replace("_-_", "__");  //historical hack - we are using _-_ for a /
    }

    return f;
  }


  /**
   * Creates an HTML policy and applies it to the input parameter
   * Using https://code.google.com/p/owasp-java-html-sanitizer/ for sanitation based on the following:
   *
   * - Provides 4X the speed of AntiSamy sanitization in DOM mode and 2X the speed of AntiSamy in SAX mode.
   * - Very easy to use. It allows for simple programmatic POSITIVE policy configuration (see below). No XML config.
   * - Actively maintained by Mike Samuel from Google's AppSec team!
   * - Passing 95+% of AntiSamy's unit tests plus many more.
   * - This is code from the Caja project that was donated by Google. It is rather high performance and low memory utilization.
   * - Java 1.5+
   *
   * Documentation for this sanitizer is availalbe at:
   * http://owasp-java-html-sanitizer.googlecode.com/svn/trunk/distrib/javadoc/org/owasp/html/HtmlPolicyBuilder.html
   * @param untrustedHTML
   * @return
   */
  public static String sanitizeHtmlFromCKEditor(String untrustedHTML) {
    PolicyFactory policy = new HtmlPolicyBuilder().allowCommonBlockElements()
                                                  .allowCommonInlineFormattingElements()
                                                  .allowStandardUrlProtocols()
                                                  .allowStyling()
                                                  .allowElements("table", "caption", "tbody", "tr","td", "hr", "em", "pre", "cite", "kbd", "samp", "var", "q","a")
                                                  .allowAttributes("href").onElements("a")
                                                  .allowAttributes("face").onElements("font")
                                                  .allowAttributes("class", "dir").onElements("span")
                                                  .allowAttributes("align", "border", "cellpadding").onElements("table")
                                                  .toFactory();
    return policy.sanitize(untrustedHTML);
  }

  public static String sanitizeRemoveAllHtml(String untrustedHTML) {
    PolicyFactory policy = new HtmlPolicyBuilder().toFactory();
    return policy.sanitize(untrustedHTML);
  }


  public static String trim (String s) {
    if (s != null) {
      return s.trim();
    }
    return null;
  }

  public static String getDomainName(String url)
  {
    try {
      URI uri = new URI(url, false);
      String domain = uri.getHost();
      return domain.startsWith("www.") ? domain.substring(4) : domain;
    }
    catch (URIException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static  DateTime getDateTimeFromISO (String s) {
    try {
      if (s != null && s.contains("T")) {
        if (s.endsWith("Z")) {
          DateTimeFormatter formatter = ISODateTimeFormat.dateTime();
          return formatter.parseDateTime(s);
        }
        else {
          DateTimeFormatter formatter = ISODateTimeFormat.dateHourMinuteSecondFraction();
          return formatter.parseDateTime(s);
        }
      }

    }
    catch (Exception e) {
      Log.err("Problem formatting date for ISO format");
      e.printStackTrace();
    }
    return null;
  }

  //sort a Map by value
  public static <K, V extends Comparable<? super V>> Map<K, V> sortMapByValue( Map<K, V> map )
  {
    List<Map.Entry<K, V>> list =
        new LinkedList<>( map.entrySet() );
    Collections.sort( list, new Comparator<Map.Entry<K, V>>()
    {
      @Override
      public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
      {
        return (o1.getValue()).compareTo( o2.getValue() );
      }
    } );

    Map<K, V> result = new LinkedHashMap<>();
    for (Map.Entry<K, V> entry : list)
    {
      result.put( entry.getKey(), entry.getValue() );
    }
    return result;
  }


  /**
   * Returns the first millisecond of the month, can be used to provide range parameter to DB.
   * @param year
   * @param month 1-12 months (i.e. months start with 1 not 0!)
   * @return
   */
  public static long getFirstMsOfMonth(int year, int month) {
    DateTime dateTime  = new DateTime(year, month, 1, 0, 0);
    return dateTime.getMillis();
  }

  /**
   * Returns the first millisecond of the current month
   * @return
   */
  public static long getFirstMsOfCurrentMonth() {
    DateTime dateTime  = DateTime.now();
    dateTime = dateTime.dayOfMonth().withMinimumValue();
    dateTime = dateTime.millisOfDay().withMinimumValue();
    return dateTime.getMillis();
  }


  /**
   * Returns the last millisecond of the month, can be used to provide range parameter to DB.
   * @param year
   * @param month 1-12 months (i.e. months start with 1 not 0!)
   * @return
   */
  public static long getLastMsOfMonth(int year, int month) {
    DateTime dateTime  = new DateTime(year, month, 1, 0, 0);
    dateTime = dateTime.dayOfMonth().withMaximumValue();
    dateTime = dateTime.millisOfDay().withMaximumValue();
    return dateTime.getMillis();
  }

  /**
   * Returns the last millisecond of the current month.
   * @return
   */
  public static long getLastMsOfCurrentMonth() {
    DateTime dateTime  = DateTime.now();
    dateTime = dateTime.dayOfMonth().withMaximumValue();
    dateTime = dateTime.millisOfDay().withMaximumValue();
    return dateTime.getMillis();
  }

  /**
   * Checks if two unix timestamps have the same calendar month, i.e.
   * Feb 5, 1980 and Feb 28, 2015 will return true as both are in February
   *
   * @param tsA
   * @param tsB
   * @return
   */
  public static boolean isMonthEqual(long tsA, long tsB) {
    DateTime dtA = new DateTime(tsA);
    DateTime dtB = new DateTime(tsB);
    return dtA.getMonthOfYear() == dtB.getMonthOfYear();
  }


  /**
   * Round to certain number of decimals
   * @note: Code from: http://stackoverflow.com/questions/8911356/whats-the-best-practice-to-round-a-float-to-2-decimals
   *
   * @param d
   * @param decimalPlace
   * @return
   */
  public static float round(float d, int decimalPlace) {
    BigDecimal bd = new BigDecimal(Float.toString(d));
    bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
    return bd.floatValue();
  }


  /**
   * Round float to two number of decimals
   * @note: Code from: http://stackoverflow.com/questions/8911356/whats-the-best-practice-to-round-a-float-to-2-decimals
   * @param d
   * @return double value
   */
  public static double roundTwoDecimals(double d) {
    DecimalFormat twoDForm = new DecimalFormat("#.##");
    return Double.valueOf(twoDForm.format(d));
  }


  public static String getFilenameFromUlr(final String url) {
    //Assuming file is the last parameter of url
    String wip = url;
    int params = wip.lastIndexOf('?');
    if (params != -1) {
      wip = url.substring(0, params);
    }

    int lastSlash = wip.lastIndexOf('/');
    if (lastSlash != -1) {
      wip = wip.substring(lastSlash + 1);
    }
    return wip;
  }

  /**
   * Helper function to retrieve from the host HTTP Header to identify MIME content type.
   * This is helpful for URLs that do not have clearly identifiable file name and extension.
   *
   * @param url - unescaped URL
   * @return null if URL is null or if MIME content is not known as one of the common image formats
   */
  public static String getExtension(String url) {
    if (url == null) {
      return null;
    }

    try {
      HttpClient client = new HttpClient();
      HeadMethod head = new HeadMethod();
      URI uri = new URI(url, false, "UTF-8"); //2nd parameter shows that we are passing unescaped URL
      head.setURI(uri);

      client.executeMethod(head);
      // Retrieve just the last modified header value.
      String mimeType = head.getResponseHeader("Content-Type").getValue();

      if (mimeType == null) {
        return null;
      }

      switch (mimeType.toLowerCase()) {
        case "image/bmp":
        case "image/x-windows-bmp":
          return "bmp";
        case "image/gif":
          return "gif";
        case "image/pjpeg":
        case "image/jpeg":
          return "jpg";
        case "image/x-jps":
          return "jps";
        case "image/png":
          return "png";
        case "image/x-portable-anymap":
          return "pnm";
        case "image/tiff":
        case "image/x-tiff":
          return "tif";
        default:
          Log.err("Unknown MIME (Content-Type) type:" + mimeType);
          return null;
      }

    } catch (Exception e) {
      Log.err("Failure to retrieve HTTP Header for " + url);
      e.printStackTrace();
    }

    return null;
  }

  public static String pojo2Xml(Object object, JAXBContext context) throws JAXBException {
    Marshaller marshaller = context.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    StringWriter writer = new StringWriter();
    marshaller.marshal(object, writer);
    String xmlStringData = writer.toString();
    return xmlStringData;
  }

  public static String getJSDateFormat(String timestamp) {
    //Timestamp length of 5 needs to be modified if a shorter template is acceptable
    if (timestamp == null || timestamp.length() < 5) {
      Log.err("Attempt to interpret >" + timestamp + "< as timestamp has failed");
      return "";
    }

    String dateFormat = "EEE MMM dd, yyyy";
    Locale[] locales = DateFormat.getAvailableLocales();
    for (Locale locale : locales) {
      try {
        SimpleDateFormat timestampFormat = new SimpleDateFormat(dateFormat, locale);
        Date date = timestampFormat.parse(timestamp);
        return formatTimestamp(date.getTime(), "yyyy-MM-dd");
      }
      catch (Exception ex) {
      }
    }

    StringBuilder sb = new StringBuilder("Error Stack Trace:\n");
    for(StackTraceElement el : Thread.currentThread().getStackTrace()) {
      sb.append(el.toString());
    }
    Log.err("Failed to parse timestamp: " + timestamp);
    Log.err(sb.toString());

    return "";
  }


  /**
   * This method guarantees that garbage collection is
   * done unlike <code>{@link System#gc()}</code>
   */
  public static void gc() {

    Object        obj = new Object();
    WeakReference ref = new WeakReference<Object>(obj);
    obj = null;
    while(ref.get() != null) {
      System.gc();
      Log.info("Requesting System garbage collection");
    }
  }

  public static String addTelHref (String s) {
/*
    if (s != null && !s.isEmpty()) {
      //let's see if we can identify phones and replace them with telephony linksthe
      Matcher telPattern = TEL_PATTERN.matcher(s);
      ArrayList<String> phoneList = new ArrayList<>();
      if (telPattern != null) {
        while (telPattern.find()) {
          String tel = telPattern.group();
          if (tel.contains("   ")) {
            String[] phones = tel.split("   ");
            for (String tel1: phones) {
              if (!s.contains(tel1+"_") && !s.contains(tel1+"&") && !s.contains("/" + tel1) && !s.contains("=" + tel1)) {
                if ((tel1.length() > 6 && !tel1.contains(".")) || (tel1.length() > 6 && tel1.contains(".") && tel1.indexOf(
                    ".") != tel1.lastIndexOf("."))) {
                  phoneList.add(tel1);
                }
              }
            }
          } else {
            if (!s.contains(tel+"_") && !s.contains(tel+"&") && !s.contains("/" + tel) && !s.contains("=" + tel)) {
              if ((tel.length() > 6 && !tel.contains(".")) || (tel.length() > 6 && tel.contains(".") && tel.indexOf(".") != tel.lastIndexOf("."))) {
                phoneList.add(tel);
              }
            }
          }

        }
      }

      if (phoneList != null && !phoneList.isEmpty()) {
        StringBuilder sb = new StringBuilder();
        String[] lines = null;
        lines = s.split("\n");

        int i = 0;
        for (String l: lines) {
          i++;
          if (!l.isEmpty()) {
            for (String p: phoneList) {
              if (l.contains(p)) {
                l = StringUtils.replace(l, p, "<a href='tel:" + p + "'>" + p + "</a>");
              }
            }
          }
          sb.append(l);
          //if we are not at the end, add the delimiter
          if (i < lines.length) {
            sb.append("\n");
          }

        }
        return  sb.toString();
      }
    }
    */
    return s;
  }


  /**
   * Cleanup all special unprintable or printable characters
   * @param s
   * @return
   */
  public static String cleanSpecials(String s) {
    return s.replaceAll("\uFEFF|\uFFFE|\u2002", "")
            .replaceAll("(\\h|/)", " ")
            .replace("\u00A0", " ");
  }

  public static String cleanSpecials1(String s) {
    return s.replaceAll("\uFEFF|\uFFFE|\u2002", "")
            .replaceAll("(\\h)", " ")
            .replace("\u00A0", " ");
  }

  /**
   * Checking if string is an integer number
   * @param str
   * @return
   */
  public static boolean isInteger(String str) {
    if (str == null) {
      return false;
    }
    char[] data = str.toCharArray();
    if (data.length <= 0) {
      return false;
    }
    int index = 0;
    if (data[0] == '-' && data.length > 1) {
      index = 1;
    }
    for (; index < data.length; index++) {
      if (data[index] < '0' || data[index] > '9') // Character.isDigit() can go here too.
      {
        return false;
      }
    }
    return true;
  }

  public static String formatDateTimeForCal(long millis) {
    if (millis == 0) {
      return "";
    }


    String dateTimeFormat = "yyyyMMdd'T'HHmmss";

    Date now = new Date(millis);

    DateTime dt = new DateTime(now);
    if (dt.getYear() == 70 || dt.getYear() == 1970) {
      return "";
    }


    SimpleDateFormat timestampFormat = new SimpleDateFormat(dateTimeFormat);
    return timestampFormat.format(now);
  }


  public static String base64encode(final Long l){
    byte[] bytes = Longs.toByteArray(l);
    return Base64.getUrlEncoder().withoutPadding().encodeToString(bytes);
  }

  public static Long base64decode(String b64) {
    if(b64 == null) {
      return null;
    }
    byte[] bytes = Base64.getUrlDecoder().decode(b64);
    return Longs.fromByteArray(bytes);
  }




  public static List<String> getTimezones() {
    String[] timezonesIds = TimeZone.getAvailableIDs();
    List<TimeZone> timezoneOffsets = new ArrayList<>();
    List<String> timezones = new ArrayList<>();
    for (String id : timezonesIds) {
      TimeZone zone = TimeZone.getTimeZone(id);
      timezoneOffsets.add(zone);
      /*long hours = TimeUnit.MILLISECONDS.toHours(zone.getRawOffset());
      long minutes = TimeUnit.MILLISECONDS.toMinutes(zone.getRawOffset()) - TimeUnit.HOURS.toMinutes(hours);
      minutes = Math.abs(minutes);
      String result = "";
      if (hours > 0) {
        result = String.format("(GMT+%d:%02d) %s", hours, minutes, zone.getID());
      } else {
        result = String.format("(GMT%d:%02d) %s", hours, minutes, zone.getID());
      }
      timezones.add(result);*/
    }
    Collections.sort(timezoneOffsets, new Comparator<TimeZone>() {
      @Override
      public int compare(TimeZone s1, TimeZone s2) {
        return s1.getRawOffset() - s2.getRawOffset();
      }
    });

    for(TimeZone tz: timezoneOffsets) {
      long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
      long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset()) - TimeUnit.HOURS.toMinutes(hours);
      minutes = Math.abs(minutes);
      String result = "";
      if (hours > 0) {
        result = String.format("(GMT+%d:%02d) %s", hours, minutes, tz.getID());
      } else {
        result = String.format("(GMT%d:%02d) %s", hours, minutes, tz.getID());
      }
      timezones.add(result);
    }

    return timezones;
  }

  public static String getMonthString(String month) {
    int m = Integer.parseInt(month);
    return new DateFormatSymbols().getMonths()[m-1].substring(0,3);
  }

  /**
   * Break the airline code and flight number from one string. The flight number
   * must be in the end with all digits, and the rest is the airline code. If
   * airline code is not digit or the flight number is not found, return null.
   * Otherwise return the airline code and flight number as a pair.
   *
   * @param flightNumber
   * @return Pair (airline code and flight number)
   */
  public static Pair<String, String> parseFlightNumber(String flightNumber) {
    String trimmedInfo = StringUtils.trim(flightNumber);

    Matcher matcher = FlightNumPattern.matcher(trimmedInfo);
    if (matcher.find())  {
      String flightNumberPart = matcher.group(1);
      int end = StringUtils.length(trimmedInfo) - StringUtils.length(flightNumberPart);
      String airlineCodePart = StringUtils.trim(StringUtils.substring(trimmedInfo, 0, end));
      if (StringUtils.length(airlineCodePart) == 2) {
        return new ImmutablePair<>(airlineCodePart, flightNumberPart);
      }
    }
    return null;
  }

  public static long getCurrentClientDate(int offsetMinutes) {
    ZoneOffset offset = ZoneOffset.ofTotalSeconds(offsetMinutes * 60);
    ZoneId zoneId = ZoneId.ofOffset("", offset);
    ZonedDateTime current = ZonedDateTime.now(zoneId).truncatedTo(ChronoUnit.DAYS);

    ZonedDateTime currentAsUTC = ZonedDateTime.of(current.getYear(), current.getMonthValue(), current.getDayOfMonth(), 0, 0, 0, 0, ZoneId.of("UTC"));
    long time =  (currentAsUTC.toInstant().toEpochMilli());

    Log.debug("Current client time in UTC: " + time);
    return time;
  }

  public static String maskCC (String cc) {
    if (cc != null && cc.length() >  7) {
      cc = "******" + cc.substring(6);
    }
    return cc;
  }

  public static String escapeS3Umapped_Prd(String s) {
    //hack to workaround an issue with _ in hostname... java url no longer likes it...
    //change url from https://umapped_prd.s3.amazonaws.com/1669687355080016339_escapes.jpg
    // to https://s3.amazonaws.com/umapped_prd/1669687355080016339_escapes.jpg
    if (s != null && s.contains("https://umapped_prd.s3.amazonaws.com") ) {
      s = s.replace("https://umapped_prd.s3.amazonaws.com","https://s3.amazonaws.com/umapped_prd");
    }
    if (s != null) {
      s = s.replace(" ","%20");
    }
    return s;
  }
}
