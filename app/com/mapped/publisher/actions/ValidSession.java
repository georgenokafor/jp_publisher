package com.mapped.publisher.actions;

import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.common.SessionConstants;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.utils.Log;
import org.apache.commons.lang3.time.StopWatch;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by twong on 2016-11-03.
 */
public class ValidSession  extends Action.Simple {

  public CompletionStage<Result> call(Http.Context ctx) {
    //set no cache
    ctx.response().setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    StopWatch sw = new StopWatch();
    sw.start();
    SessionMgr sessionMgr = new SessionMgr(ctx.session());
    if (sessionMgr.isSessionValid()) {
      ctx.args.put(SessionMgr.CONTEXT_KEY, sessionMgr);
      Log.debug("Authentication SUCCESS in " + sw.getTime() + "ms for context: ", ctx.toString());
      return delegate.call(ctx);
    }
    else {
      if (sessionMgr.isSessionIdMismatch()) {
        sessionMgr.setParam(SessionConstants.SESSION_PARAM_MSG, "Your session has expired - please log in again and" +
                                                                " make sure you are not logged in from two locations.");
        Log.info("Authentication FAILED in " + sw.getTime() + "ms - multiple login - for context: ", ctx.toString());
      } else if (sessionMgr.isSessionExpired()){
        sessionMgr.setParam(SessionConstants.SESSION_PARAM_MSG, "Your session has expired - please log in again.");
        Log.info("Authentication FAILED in " + sw.getTime() + "ms - session expired - for context: ", ctx.toString());
      } else {
        Log.info("Authentication FAILED in " + sw.getTime() + "ms for context: ", ctx.toString());
      }

      if(ConfigMgr.isProduction() || ConfigMgr.isLocalDev()) {
        return CompletableFuture.completedFuture(redirect("https://" + ctx.request().host() + "/login"));
      } else {
        return CompletableFuture.completedFuture(redirect("http://" + ctx.request().host() + "/login"));
      }
    }
  }
}
