package com.umapped.helper;

import actors.*;
import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.audit.AuditModuleType;
import com.mapped.publisher.audit.event.AuditCollaboration;
import com.mapped.publisher.audit.event.AuditTrip;
import com.mapped.publisher.audit.event.AuditTripTraveller;
import com.mapped.publisher.common.*;
import com.mapped.publisher.parse.extractor.booking.email.ReservationsHolder;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.parse.schemaorg.MediaObject;
import com.mapped.publisher.parse.schemaorg.Person;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.mapped.publisher.persistence.RecordStatus;
import com.mapped.publisher.persistence.TripBrandingMgr;
import com.mapped.publisher.persistence.VOModeller;
import com.mapped.publisher.persistence.bookingapi.BookingAPIMgr;
import com.mapped.publisher.persistence.bookingapi.BookingRS;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.persistence.communicator.MessengerUpdateHelper;
import com.mapped.publisher.persistence.redis.RedisKeys;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.*;
import com.mapped.publisher.view.*;
import com.umapped.api.AutoPublishProp;
import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.segment.TripSegmentAnalyzeResult;
import com.umapped.persistence.accountprop.PreferenceMgr;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.service.offer.OfferProviderRepository;
import com.umapped.service.offer.OfferService;
import com.umapped.service.offer.UMappedCity;
import com.umapped.service.offer.UMappedCityLookup;
import controllers.*;
import it.innove.play.pdf.PdfGenerator;
import models.publisher.*;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.time.StopWatch;
import play.db.DB;
import play.i18n.Lang;
import play.libs.Json;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.sql.Connection;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by twong on 2016-08-24.
 */

@Singleton
public class TripPublisherHelper {
  public enum TravelerResponse {
    OK,
    ERR_EMAIL,
    ERR_DUPLICATE,
    ERR_UNDEFINED
  }

  @Inject
  RedisMgr redisMgr;

  @Inject
  OfferService offerService;

  @Inject
  UMappedCityLookup umCityLookup;

  @Inject
  private OfferProviderRepository offerProviderRepository;
  /*
   * Queue the trip to be published in Redis
   */
  public void autoPublishTrip (Trip trip, String groupId) {
    if (trip != null) {
      String key = trip.getTripid();
      if (groupId != null)
        key += groupId;

      Optional<TripPublisherActor.Command> tripCache = redisMgr.hashGetBin(RedisKeys.H_TRIPS_TO_PUBLISH, null, key, TripPublisherActor.Command.class);

      if (!tripCache.isPresent()) {
        //schedule the publishing
        TripPublisherActor.Command cmd = new TripPublisherActor.Command(trip.getTripid(), groupId);
        redisMgr.hashSet(RedisKeys.H_TRIPS_TO_PUBLISH, null, key, cmd, RedisMgr.WriteMode.FAST);
        Log.debug("TripController:autoPublishTrip  Trip " + trip.getTripid() + " scheduled to be published");
      } else {
        Log.debug("TripController:autoPublishTrip  Trip " + trip.getTripid() + " already scheduled to be published");
      }
    }
  }

  /*
   * Queue the trip to auto republish and also send an email to the agent if needed
   */
  public void autoPublishTrip (Trip trip, String groupId, TripAutoCreatePublishActor.Command cmd) {
    autoPublishTrip(trip, groupId);
    // let's see if this API request requires an agent email notification
    if (cmd != null && cmd.getAutoPublishProp() != null && cmd.getAgent() != null && cmd.getCmpy() != null &&cmd.getAutoPublishProp().notifyAgentRePublish) {
      TripAutoCreatePublishActor.sendNotificationEmail(trip, cmd);
    }
  }

  /*
   * Process all trips in Redis that's ready to be published
   */

  public void processAutoPublishTrips () {
    Optional<Map<String, TripPublisherActor.Command>> allTrips =  redisMgr.hashGetAll(RedisKeys.H_TRIPS_TO_PUBLISH, null, TripPublisherActor.Command.class );
    if (allTrips.isPresent()) {
      Map<String, TripPublisherActor.Command> allKeys = allTrips.get();

      for (String key: allKeys.keySet()) {
        TripPublisherActor.Command cmd = allKeys.get(key);
        if (cmd != null && cmd.getTripId() != null) {
          ActorsHelper.tell(SupervisorActor.UmappedActor.TRIP_PUBLISHER, cmd);
        }

        redisMgr.hashDel(RedisKeys.H_TRIPS_TO_PUBLISH, null, RedisMgr.WriteMode.FAST, key);
      }
    }
  }


  //common publishing function
  public static void publishTrip(Trip trip,
                                 UserProfile up,
                                 String mapPk,
                                 AgentView agentView,
                                 Company cmpy,
                                 boolean published,
                                 boolean republished,
                                 boolean skipTravelerEmail,
                                 boolean selfRegistration,
                                 String comments,
                                 TripGroup group,
                                 Lang lang,
                                 boolean autoPublished)
      throws Exception {
    Log.log(LogLevel.DEBUG, "TripPublisherHelper - start publishing Trip: " + trip.getTripid());


    StopWatch sw11 = new StopWatch();
    sw11.start();
    List<String> actorIds = new ArrayList<>();
    long timestamp     = System.currentTimeMillis();
    long endTimestamp1 = timestamp;
    long endTimestamp2 = timestamp;

    TripBrandingView mainBrandingView = null;


    if (up.email != null && up.email.length() > 0) {
      agentView.agentEmail = up.email;
    }



    //cmpy information
    //check for cobranding and set the main company - this is bad historical implementation
    Company                mainCompany       = null;
    List<TripBrandingView> tripBrandingViews = TripBrandingMgr.findActiveBrandedCmpies(trip);
    TripBrandingView overwritten = null;

    if (tripBrandingViews != null && tripBrandingViews.size() > 0) {
      mainCompany = TripBrandingMgr.getMainContact(tripBrandingViews);
      //if this is overwritten by tag for the same company e.g. ski.com
      overwritten = TripBrandingMgr.getOverwrittenByTag(tripBrandingViews, trip);
    }
    if (overwritten == null) {
      if (mainCompany == null) {
        mainCompany = cmpy;
        mainBrandingView = new TripBrandingView();
        mainBrandingView.cmpyId = mainCompany.cmpyid;
        mainBrandingView.cmpyName = mainCompany.name;
        mainBrandingView.cmpyLogoName = mainCompany.logoname;
        mainBrandingView.cmpyLogoUrl = Utils.escapeS3Umapped_Prd(mainCompany.logourl);
        List<CmpyAddress> cmpyAddrs = CmpyAddress.findMainActiveByCmpyId(mainCompany.cmpyid);
        if (cmpyAddrs != null && cmpyAddrs.size() > 0) {
          CmpyAddress cmpyAddress = cmpyAddrs.get(0);
          if (cmpyAddress.web != null && cmpyAddress.web.length() > 0) {
            if (mainBrandingView != null) {
              mainBrandingView.cmpyEmail = cmpyAddress.email;
              mainBrandingView.cmpyFacebook = cmpyAddress.facebook;
              mainBrandingView.cmpyPhone = TripBrandingMgr.getCompanyExtension(Account.findActiveByLegacyId(up.getUserid()), cmpyAddress.phone);
              mainBrandingView.cmpyTwitter = cmpyAddress.twitter;
              mainBrandingView.cmpyWebsite = cmpyAddress.web;
            }
          }
        }
      }
    } else {
      mainCompany = cmpy;
      mainBrandingView = overwritten;
      mainCompany.setName(overwritten.cmpyName);
      mainCompany.setLogoname(overwritten.cmpyLogoName);
      mainCompany.setLogourl(Utils.escapeS3Umapped_Prd(overwritten.cmpyLogoUrl));
      agentView.agentPhone = TripBrandingMgr.getCompanyExtension(Account.findActiveByLegacyId(up.getUserid()), overwritten.cmpyPhone);
      agentView.agentMobile = "";
    }


    agentView.cmpyName = mainCompany.getName();









    String phone = null;
    if (up.mobile != null && up.mobile.length() > 0) {
      phone = up.mobile;
    } else if (up.phone != null && up.phone.length() > 0) {
      phone = up.phone;
    } else if (mainBrandingView != null && mainBrandingView.cmpyPhone != null && !mainBrandingView.cmpyPhone.isEmpty()) {
      phone = mainBrandingView.cmpyPhone;
    }

    if (phone != null) {
      agentView.agentPhone = phone;
    }

    //overwritte cmpoy website/facebook/phone/twitter is set from the user profile
    if (mainBrandingView != null) {
      if (up.getFacebook() != null && !up.getFacebook().isEmpty()) {
        mainBrandingView.cmpyFacebook = up.getFacebook();
      }
      if (up.getTwitter() != null && !up.getTwitter().isEmpty()) {
        mainBrandingView.cmpyTwitter = up.getTwitter();
      }
      if (up.getWeb() != null && !up.getWeb().isEmpty()) {
        mainBrandingView.cmpyWebsite = up.getWeb();
      }
      if (up.getPhone() != null && !up.getPhone().isEmpty()) {
        mainBrandingView.cmpyPhone = up.getPhone();
      }

      if (mainBrandingView.cmpyWebsite != null && !mainBrandingView.cmpyWebsite.isEmpty()) {
        agentView.cmpyWeb = mainBrandingView.cmpyWebsite;
      }
    }

    //create share  + invite
    List<AccountTripLink> tripLinks = null;
    List<Account> travelers = new ArrayList<>();

    if (group != null) {
      tripLinks = AccountTripLink.findByTripGroup(trip.getTripid(), group.groupid);
    }
    else {
      tripLinks = AccountTripLink.findByTripNoGroup(trip.getTripid());
    }


    if (tripLinks != null && tripLinks.size() > 0) {
      for (AccountTripLink tripLink : tripLinks) {
        Account traveler = Account.find.byId(tripLink.getPk().getUid());
        travelers.add(traveler);
      }
    }

    HashMap<String, String> tours = new HashMap<String, String>();

    //get bookings in chronological order
    List<TripDetail> bookings = TripDetail.findActiveByTripId(trip.getTripid());
    //        Collections.sort(bookings, TripDetail.TripDetailComparator);
    List<TripBookingDetailView> notes = BookingNoteController.getAllTripNotesView(trip, null, false);

    List<TripAttachment> attachments = TripAttachment.findByTripId(trip.getTripid());

    // check for tours - if tours are found found, get the tour details and add it to the list
    List<TripDetail> tourDetails = new ArrayList<TripDetail>();

    if (bookings == null) {
      bookings = new ArrayList<TripDetail>();
    }

    bookings.addAll(tourDetails);
    //sort everything chronologically
    Collections.sort(bookings, TripDetail.TripDetailComparator);

    for (TripDetail t : bookings) {
      if (t.detailtypeid == ReservationType.FLIGHT && published && SecurityMgr.hasCapability(trip, Capability.FLIGHT_NOTIFICATIONS)) {
        //check to see if we can track the flight
        FlightTrackActor.trackFlight(t);
      }
    }

    //Create Firebase chat room
    String firebaseActorId = Utils.getUniqueId();
    CacheMgr.set(APPConstants.CACHE_PUB_ACTOR + firebaseActorId, firebaseActorId);
    actorIds.add(firebaseActorId);

    //get guides
    //build the guides
    //
    List<TripDestination> guideList = TripDestination.getActiveByTripId(trip.getTripid());

    for (String tourId : tours.keySet()) {
      List<TripDestination> tripDests = TripDestination.getActiveByTripId(tourId);
      if (tripDests != null) {
        guideList.addAll(tripDests);
      }
    }
    Map<String, TripDestination> guides = new HashMap<String, TripDestination>();
    for (TripDestination td : guideList) {
      if (td.getStatus() == APPConstants.STATUS_ACTIVE && !guides.containsKey(td.getDestinationid())) {
        guides.put(td.getDestinationid(), td);
      }
    }

    //if there are no bookings and no guides - do not publish
    if ((bookings == null || bookings.size() == 0) &&
        (guideList == null || guideList.size() == 0) &&
        (attachments == null || attachments.size() == 0) &&
        (notes == null || notes.size() == 0)) {
      return;
    }

    TripBookingView bookingView = new TripBookingView();
    bookingView.lang = lang;
    bookingView.comments = comments;
    bookingView.logoUrl = mainCompany.logoname;
    bookingView.agentView = agentView;
    //escape html and allow for carriage return
    bookingView.comments = Utils.escapeHtml(bookingView.comments);
    //add branding as required
    bookingView.tripBranding = tripBrandingViews;
    if (cmpy != null) {
      bookingView.display12Hr = cmpy.display12hrClock();
    }




    try {
      Ebean.beginTransaction();

      if (group != null) {
        if (selfRegistration) {
          group.setAllowselfregistration(APPConstants.ALLOW_SELF_REGISTRATION);
          trip.setVisibility(APPConstants.ALLOW_SELF_REGISTRATION);
        }
        else {
          group.setAllowselfregistration(APPConstants.PREVENT_SELF_REGISTRATION);
          trip.setVisibility(APPConstants.PREVENT_SELF_REGISTRATION);
        }

        if (published) {
          group.setPublishstatus(APPConstants.STATUS_PUBLISHED);
          trip.setStatus(APPConstants.STATUS_PUBLISHED);
        }
        else {
          group.setPublishstatus(APPConstants.STATUS_PUBLISHED_REVIEW);
          trip.setStatus(APPConstants.STATUS_PUBLISHED_REVIEW);
        }

        group.setModifiedby(up.getUserid());
        group.setLastupdatedtimestamp(timestamp);
        group.save();

        trip.setModifiedby(up.getUserid());
        trip.setLastupdatedtimestamp(timestamp);
        trip.save();
      }
      else {
        if (selfRegistration) {
          trip.setVisibility(APPConstants.ALLOW_SELF_REGISTRATION);
        }
        else {
          trip.setVisibility(APPConstants.PREVENT_SELF_REGISTRATION);
        }

        if (published) {
          trip.setStatus(APPConstants.STATUS_PUBLISHED);
        }
        else {
          trip.setStatus(APPConstants.STATUS_PUBLISHED_REVIEW);
        }

        trip.setModifiedby(up.getUserid());
        trip.setLastupdatedtimestamp(timestamp);
        trip.save();
      }

      //Create audit log
      String publishedUserId = null;
      if (!autoPublished) {
        publishedUserId = up.getUserid();
      }

      Account acc = Account.findActiveByLegacyId(up.getUserid());
      final PublishFirebaseActor.Command cmd = new PublishFirebaseActor.Command(trip, bookings, firebaseActorId,
                                                                                acc.getUid());
      ActorsHelper.tell(SupervisorActor.UmappedActor.PUBLISH_FIRABASE, cmd);

      audit:
      {
        TripAudit ta = TripAudit
            .buildRecord(AuditModuleType.TRIP, AuditActionType.MODIFY, AuditActorType.WEB_USER)
            .withTrip(trip)
            .withCmpyid(trip.cmpyid)
            .withUserid(publishedUserId);

        AuditTrip.TripStatus auditTrip = (trip.getStatus() == APPConstants.STATUS_PUBLISHED) ?
                                         AuditTrip.TripStatus.PUBLISHED :
                                         AuditTrip.TripStatus.PUBLISHED_REVIEW;
        ((AuditTrip) ta.getDetails())
            .withName(trip.getName())
            .withId(trip.getTripid())
            .withStatus(auditTrip)
            .withAction(republished ?
                        AuditTrip.TripAction.REPUBLISHED :
                        (published ? AuditTrip.TripAction.PUBLISHED : AuditTrip.TripAction.SENT_FOR_REVIEW))
            .withGroupId((group != null) ? group.getGroupid() : "")
            .withGroupName((group != null) ? group.getName() : "Default");
        ta.save();
      }


      TripPublishHistory publishHistory = new TripPublishHistory();
      publishHistory.setPk(Utils.getUniqueId());
      publishHistory.setComments(comments);
      publishHistory.setTripid(trip.getTripid());
      if (group != null) {
        publishHistory.setGroupid(group.groupid);
      }
      if (published) {
        publishHistory.setStatus(APPConstants.STATUS_PUBLISHED);
      }
      else {
        publishHistory.setStatus(APPConstants.STATUS_PUBLISHED_REVIEW);
      }

      publishHistory.setCreatedby(up.getUserid());
      publishHistory.setCreatedtimestamp(timestamp);
      publishHistory.setModifiedby(up.getUserid());
      publishHistory.setLastupdatedtimestamp(timestamp);
      publishHistory.save();
      Ebean.commitTransaction();

      //see if we need to generate a PDF - this will be default at some point in time - this is a sync process for now
      // we need to make sure the PDF is generated before emails are sent.
      if (trip != null && trip.getTag() != null && trip.getTag().contains(APPConstants.CMPY_TAG_API_ATTACH_ITINERARY_PDF_EMAIL)) {
        TripPDFActor.Command pdfcmd = new TripPDFActor.Command(trip, TripPDFActor.getFileName(trip), Lang.forCode("EN"), false, true, Long.parseLong(publishHistory.getPk()));
        TripPDFActor.generatePdf(pdfcmd);

      }

      long endTimestamp3 = System.currentTimeMillis();
      Log.log(LogLevel.DEBUG, "Review Duration- Trip: " + trip.getTripid() + " - " + (endTimestamp3 - timestamp));
      Log.log(LogLevel.DEBUG, "Review S3 Duration- Trip: " + trip.getTripid() + " - " + (endTimestamp2 - endTimestamp1));

      if (tripLinks != null && tripLinks.size() > 0 && !skipTravelerEmail && !autoPublished) {
        int returnCode = TripController.sendUMappedEmailNotitication(travelers, trip, up, comments, group);
        if (returnCode != 0) {
          Log.err("TripController:publishTrip Cannot send notification emails to the travelers. TripId: " + trip.getTripid());
        }
      }

      //check if there are any agents to notify
      List<TripAgent> agents = null;
      if (group != null) {
        agents = TripAgent.getByTripIdGroupId(trip.getTripid(), group.getGroupid());
      }
      else {
        agents = TripAgent.getByTripIdDefaultGroup(trip.getTripid());
      }

      if (agents != null && agents.size() > 0 && !autoPublished) {
        int returnCode = TripController.sendUMappedAgentEmailNotitication(agents, trip, up, comments, group);
        if (returnCode != 0) {
          Log.err("TripController:publishTrip Cannot send notification emails to the travel agents. TripId: " + trip.getTripid());

        }
      }

      if (!autoPublished) {
        if (TripController.sendCollaboratorsEmailNotification(trip, up, comments, group) != 0) {
          Log.err("TripController:publishTrip Cannot send notification emails to the collaborators. TripId: " + trip.getTripid());

        }
      }

      Log.info("TripController:Publish Trip: " + trip.getTripid() , "Execution Time of sync request: " + sw11.getTime());

      Log.info("TripController:Publish Trip: " + trip.getTripid(), "Total Execution Time of sync + async requests: " + sw11.getTime());



    }
    catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
      throw  e;
    }
  }

  public static Trip createTripFromReservationPackage (Company c, String tripName, List<BookingRS> results, Account account ) {

    List <ReservationPackage> reservationPackages =  new ArrayList<>();
    HashMap<String, ArrayList<BookingRS>> resultsByPK = new HashMap<>();
    List<Long> pks = new ArrayList<>();
    ReservationPackage reservationPackage = null;


    List<Destination> destinations = new ArrayList<>();
    String coverURL =  null;
    String coverFileName = null;
    FileImage image = null;
    String resortName = null;
    String tag = null;
    BaseView v = new BaseView();

    long currentTs = System.currentTimeMillis();

    TripVO tripVO = null;

    for (BookingRS result: results) {
      if (result.bookingType != ReservationType.PACKAGE.ordinal()) {
        if (result.getSrcPk() != null) {
          //we group all the bookings that have the same src PK
          ArrayList<BookingRS> bookings = resultsByPK.get(result.getSrcPk());
          if (bookings == null) {
            bookings =  new ArrayList<>();
          }
          bookings.add(result);
          resultsByPK.put(result.getSrcPk(), bookings);
        }

        ReservationsHolder rh = parseJson(result.getData(), BkApiSrc.getName(result.getSrcId()), result.pk);
        if (tripVO == null) {
          tripVO = new TripVO();
          tripVO.setSrc(BkApiSrc.getName(result.getSrcId()));
          tripVO.setSrcId(result.pk);
          tripVO.setImportSrc(BookingSrc.ImportSrc.API);
          tripVO.setImportSrcId(result.getSrcRecLocator());
        }
        if (rh.count()  >  0) {
          tripVO = rh.toTripVO(tripVO);
          tripVO.setSrc(BkApiSrc.getName(result.getSrcId()));
          tripVO.setSrcId(result.pk);
          tripVO.setImportSrc(BookingSrc.ImportSrc.API);
          tripVO.setImportSrcId(result.getSrcRecLocator());
        }
        pks.add(result.getPk());
      } else {
        ReservationsHolder rh = parseJson(result.getData(), BkApiSrc.getName(result.getSrcId()), result.pk);
        if (rh != null && rh.reservationPackage != null) {
          reservationPackage = rh.reservationPackage;
          reservationPackages.add(rh.reservationPackage);
          if (resortName == null && rh.reservationPackage.umId != null && rh.reservationPackage.umId.equals("ski.com")) {
            //special processing for ski.com
            //parse the name of the reservation pacakage to get the resort
            //then look up a doc to link the doc and get the cover photo
            if (rh.reservationPackage.name.contains("-")) {
              resortName = rh.reservationPackage.name.substring(0, rh.reservationPackage.name.indexOf("-")).trim();
              List<Destination> docs = Destination.findActiveByNameCmpy(resortName, c.getCmpyid());
              if (docs != null && docs.size() >0) {
                destinations.add(docs.get(0));
                String docPK = docs.get(0).destinationid;
                coverURL = docs.get(0).getCoverurl();
                image = docs.get(0).getFileImage();
                List<DestinationGuideAttach> photos = DestinationGuideAttach.getAllPhotosByDestId(docPK);
                if (photos != null && photos.size() > 0) {
                  //let's randomly set the cover
                  int i = ThreadLocalRandom.current().nextInt(0, photos.size() + 1);
                  if (i < photos.size()) {
                    coverURL = photos.get(i).getAttachurl();
                    coverFileName = photos.get(i).getAttachname();
                    image = photos.get(i).getFileImage();
                  }
                }
              }
            }
            if (rh.reservationPackage.additionalType != null) {
              String type = rh.reservationPackage.additionalType.toLowerCase();
              tag = TripBrandingMgr.getWhiteLabelTag(type);
            }
          } else if (rh.reservationPackage.downloadable != null && !rh.reservationPackage.downloadable.isEmpty()) {
            destinations.addAll(TripPublisherHelper.getDocuments(rh.reservationPackage, c));
          }
        }
      }
    }
    Trip trip = new Trip();
    trip.setTripid(DBConnectionMgr.getUniqueId());
    trip.setCmpyid(c.getCmpyid());
    trip.setName(tripName);
    trip.setStarttimestamp(currentTs);
    trip.setEndtimestamp(currentTs);
    trip.setCreatedby(account.getLegacyId());
    trip.setModifiedby(account.getLegacyId());
    trip.setCreatedtimestamp(currentTs);
    trip.setLastupdatedtimestamp(currentTs);
    trip.setStatus(APPConstants.STATUS_PENDING);
    if (tag == null) {
      tag = "";
    }
    if (results.size()  > 0){
        tag += " " + results.get(0).getSrcRecLocator();
    }

    trip.setTag(tag);
    if (coverURL != null) {
      trip.setCoverurl(coverURL);
      trip.setCoverfilename(coverFileName);
      trip.setFileImage(image);
    } else if (reservationPackage != null && reservationPackage.image != null && !reservationPackage.image.isEmpty()) {
      TripPublisherHelper.addCover(trip, reservationPackage, account);
    }
    trip.save();
    try {
      //Create audit log
      audit:
      {
        TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP, AuditActionType.ADD, AuditActorType.SYSTEM)
                                .withTrip(trip)
                                .withCmpyid(trip.cmpyid)
                                .withUserid(trip.getCreatedby());

        ((AuditTrip) ta.getDetails()).withName(trip.getName())
                                     .withId(trip.getTripid())
                                     .withStatus(AuditTrip.TripStatus.PENDING);
        ta.save();
      }    } catch (Exception e) {
      Log.err("TripPublisherHelper: cannot create audit for trip: " + trip.getTripid());
    }

    VOModeller voModeller = new VOModeller(trip, account, true);


    if (tripVO != null) {
      voModeller.buildTripVOModels(tripVO);
      //before we save, we try to link notes to bookings where the notes src pk is equal to the booking src pk
      linkNotesToBookings(voModeller, resultsByPK);
    }

    try {
      voModeller.saveAllModels();
      BookingAPIMgr.updateAddToTrip(account.getLegacyId(), pks);

      //let see if we need to add the reservation package to the trip
      for (ReservationPackage rp: reservationPackages) {
        ApiBookingController.processTripNoteForReservationPackage(rp, trip.tripid, account.getLegacyId());
      }

      //see if we need to automatically link a doc to this trip
      if (destinations != null && !destinations.isEmpty()) {
        TripPublisherHelper.addDocuments(destinations, trip, account);
      }
      Log.info("TripPublisherHelper:createTrip - Trip Created: " + trip.getTripid());


    } catch (Exception e) {
      Log.err("TripPublisherHelper:createTrip", e);
      return null;
    }

    return trip;
  }

  //utility method
  public static void linkNotesToBookings (VOModeller voModeller, HashMap<String, ArrayList<BookingRS>> resultsByPK) {
    //before we save- we will try to link the notes to the booking if the srcPK of the note match the srcPK of a different booking
    if (voModeller.getNotes() != null && resultsByPK != null) {
      for (TripNote tripNote : voModeller.getNotes()) {
        if (tripNote.getImportSrcId() != null) {
          List<BookingRS> bookings = new ArrayList<>();
          if (resultsByPK.containsKey(tripNote.getImportSrcId())) {
            bookings.addAll(resultsByPK.get(tripNote.getImportSrcId()));
          }
          for (String key : resultsByPK.keySet()) {
            if (!key.equals(tripNote.getImportSrcId()) && key.startsWith(tripNote.getImportSrcId())) {
              //add all of these since flights have special deduplicating logic to generate its own src id that is not the original src id passed in to the api
              bookings.addAll(resultsByPK.get(key));

            }
          }
          if (bookings != null && bookings.size() > 1) {
            for (BookingRS rs : bookings) {
              if (rs.getBookingType() != ReservationType.NOTE.ordinal()) {
                if (rs.getBookingType() == ReservationType.FLIGHT.ordinal() && voModeller.getFlights() != null) {
                  for (TripDetail td : voModeller.getFlights()) {
                    if (td.getBkApiSrcId() != null && (td.getBkApiSrcId().equals(rs.getPk()) || td.getBkApiSrcId().toString().startsWith(String.valueOf(rs.getPk())))) {
                      tripNote.setTripDetailId(td.getDetailsid());
                      break;
                    }
                  }
                }
                else if (rs.getBookingType() == ReservationType.HOTEL.ordinal() && voModeller.getHotels() != null) {

                  for (TripDetail td : voModeller.getHotels()) {
                    if (td.getBkApiSrcId() != null && td.getBkApiSrcId().equals(rs.getPk())) {
                      tripNote.setTripDetailId(td.getDetailsid());
                      break;
                    }
                  }
                }
                else if ((rs.getBookingType() == ReservationType.TRANSPORT.ordinal() || rs.getBookingType() == ReservationType.CAR_RENTAL.ordinal()) && voModeller.getTransports() != null) {
                  for (TripDetail td : voModeller.getTransports()) {
                    if (td.getBkApiSrcId() != null && td.getBkApiSrcId().equals(rs.getPk())) {
                      tripNote.setTripDetailId(td.getDetailsid());
                      break;
                    }
                  }
                }
                else if (rs.getBookingType() == ReservationType.ACTIVITY.ordinal() && voModeller.getActivities
                    () != null) {
                  for (TripDetail td : voModeller.getActivities()) {
                    if (td.getBkApiSrcId() != null && td.getBkApiSrcId().equals(rs.getPk())) {
                      tripNote.setTripDetailId(td.getDetailsid());
                      break;
                    }
                  }
                }
                else if (rs.getBookingType() == ReservationType.CRUISE.ordinal() && voModeller.getCruises() != null) {
                  for (TripDetail td : voModeller.getCruises()) {
                    if (td.getBkApiSrcId() != null && td.getBkApiSrcId().equals(rs.getPk())) {
                      tripNote.setTripDetailId(td.getDetailsid());
                      break;
                    }
                  }
                }


                break;
              }
            }
          }
        }

      }
    }
  }


  public void addTravelerAgenttoTrip(ReservationPackage reservationPackage, Trip trip, Company cmpy, String pnr, int srcId, AutoPublishProp prop) {
    Account agentAccount = Account.findByLegacyId(trip.getCreatedby());
    UserProfile agentUp = UserProfile.findByPK(trip.getCreatedby());
    List<Account> travelersToNotify = new ArrayList<>();
    //now that we have thr trip published, let's see if we need to add traveler and collaborator
    if (reservationPackage != null) {
      boolean sendEmail = true;
      if (prop != null && prop.noEmail) {
        sendEmail = false;
      }
      //for traveler, we look for an email in the UnderName section of the reservation package
      if (reservationPackage.underName != null && reservationPackage.underName.email != null && !reservationPackage.underName.email.isEmpty()) {
        List<AccountTripLink> tripLinks = AccountTripLink.findByEmailTrip(reservationPackage.underName.email, trip.getTripid());
        if (tripLinks == null || tripLinks.size() == 0) {
          //new traveler
           try {
            Person traveler = reservationPackage.underName;
            TripPublisherHelper.TravelerResponse response = TripPublisherHelper.addPassenger(traveler.givenName,
                                                                                             traveler.familyName,
                                                                                             traveler.email,
                                                                                             null,
                                                                                             agentAccount.getLegacyId(),
                                                                                             trip,
                                                                                             null,
                                                                                             sendEmail,
                                                                                             reservationPackage.emailNote);
            if (response != TripPublisherHelper.TravelerResponse.OK) {
              Log.err("AutoCreatePublishTripActor: cannot add traveler for: " + cmpy.getName() + " RecLocator: " + pnr + " SrcId: " + srcId);

            } else {
              try {
                audit:
                {
                  TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_TRAVELLER,
                                                       AuditActionType.ADD,
                                                       AuditActorType.SYSTEM).withTrip(trip).withCmpyid(trip.cmpyid).withUserid(agentAccount.getLegacyId());

                  ((AuditTripTraveller) ta.getDetails()).withName(traveler.givenName + traveler.familyName)
                                                        .withId("")
                                                        .withEmail(traveler.email)
                                                        .withGroupId("")
                                                        .withGroupName("Default");

                  ta.save();
                }
              } catch (Exception e) {

              }
            }


          }
          catch (Exception e) {
            Log.err("AutoCreatePublishTripActor: cannot add traveler for: " + cmpy.getName() + " RecLocator: " + pnr + " SrcId: " + srcId);
          }
        } else if (sendEmail && reservationPackage.underName.potentialAction != null && reservationPackage.underName.potentialAction.equals("NOTIFY")) {
          Account traveler = Account.find.byId(tripLinks.get(0).getPk().getUid());
          if (traveler != null) {
            travelersToNotify.add(traveler);
            try {
              audit:
              {
                TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_TRAVELLER,
                                                     AuditActionType.MODIFY,
                                                     AuditActorType.SYSTEM).withTrip(trip).withCmpyid(trip.cmpyid).withUserid(agentAccount.getLegacyId());

                ((AuditTripTraveller) ta.getDetails()).withName(traveler.getFullName())
                                                      .withId(String.valueOf(traveler.getUid()))
                                                      .withEmail(traveler.getEmail())
                                                      .withGroupId("")
                                                      .withGroupName("Default");

                ta.save();
              }
            } catch (Exception e) {

            }
          }
        }
      }

      //if the token allows traveler control
      removeTravelers(reservationPackage, trip, agentAccount, prop);

      //do we have more travelers?
      if (reservationPackage.additionalTravelers != null && reservationPackage.additionalTravelers.size() > 0) {
        for (Person traveler1: reservationPackage.additionalTravelers) {
          if (traveler1.givenName != null && traveler1.familyName != null && traveler1.email != null) {
            List<AccountTripLink> tripLinks = AccountTripLink.findByEmailTrip(traveler1.email, trip.getTripid());
            if (tripLinks == null || tripLinks.size() == 0) {
              try {
                TripPublisherHelper.TravelerResponse response = TripPublisherHelper.addPassenger(traveler1.givenName,
                                                                                                 traveler1.familyName,
                                                                                                 traveler1.email,
                                                                                                 null,
                                                                                                 agentAccount.getLegacyId(),

                                                                                                 trip,
                                                                                                 null,
                                                                                                 sendEmail,
                                                                                                 reservationPackage.emailNote);
                if (response != TripPublisherHelper.TravelerResponse.OK) {
                  Log.err("AutoCreatePublishTripActor: cannot add traveler for: " + cmpy.getName() + " RecLocator: " + pnr + " SrcId: " + srcId + " Taveler: " + traveler1.email);
                } else {
                  try {
                    audit:
                    {
                      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_TRAVELLER,
                                                           AuditActionType.ADD,
                                                           AuditActorType.SYSTEM)
                                              .withTrip(trip)
                                              .withCmpyid(trip.cmpyid)
                                              .withUserid(agentAccount.getLegacyId());

                      ((AuditTripTraveller) ta.getDetails()).withName(traveler1.givenName + traveler1.familyName)
                                                            .withId("")
                                                            .withEmail(traveler1.email)
                                                            .withGroupId("")
                                                            .withGroupName("Default");

                      ta.save();
                    }
                  } catch (Exception e) {

                  }
                }
              }
              catch (Exception e) {
                Log.err("AutoCreatePublishTripActor: cannot add traveler for: " + cmpy.getName() + " RecLocator: " + pnr + " SrcId: " + srcId);
              }
            } else if (sendEmail && traveler1.potentialAction != null && traveler1.potentialAction.equals("NOTIFY")) {
              Account traveler = Account.find.byId(tripLinks.get(0).getPk().getUid());
              if (traveler != null) {
                travelersToNotify.add(traveler);
                try {
                  audit:
                  {
                    TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_TRAVELLER,
                                                         AuditActionType.MODIFY,
                                                         AuditActorType.SYSTEM).withTrip(trip).withCmpyid(trip
                                                                                                                                                          .cmpyid).withUserid(agentAccount.getLegacyId());

                    ((AuditTripTraveller) ta.getDetails()).withName(traveler.getFullName())
                                                          .withId(String.valueOf(traveler.getUid()))
                                                          .withEmail(traveler.getEmail())
                                                          .withGroupId("")
                                                          .withGroupName("Default");

                    ta.save();
                  }
                } catch (Exception e) {

                }
              }
            }
          }
        }
      }



      //if we need to resend the itinerary email
      if (travelersToNotify.size() > 0 && sendEmail) {
        TripController.sendUMappedEmailNotitication(travelersToNotify,trip, agentUp, reservationPackage.emailNote, null);
        //also notfiy any agents on the trip
        TripController.sendCollaboratorsEmailNotification(trip, agentUp, null, null );
      }

      //for collaborator, we look for an email in the Broker section of the reservation package
      if (reservationPackage.broker != null && reservationPackage.broker.email != null && !reservationPackage.broker.email.isEmpty()) {
        Person broker = reservationPackage.broker;
        try {
          InternetAddress emailAddr = new InternetAddress(broker.email);
          emailAddr.validate();

          Account collab = Account.findByEmail(broker.email);
          if (collab != null && collab.getAccountType() == Account.AccountType.PUBLISHER && collab.getState() == RecordStatus.ACTIVE) {
            TripShare tripShare = TripShare.getSharedTripForUser(trip.tripid, collab.getLegacyId());
            if (tripShare == null) {
              // found an existing user... so make sure this account does not belong to the same company as an admin
              AccountCmpyLink cmpyLink = AccountCmpyLink.findActiveAccountAndCmpy(collab.getUid(), trip.getCmpyid());
              if (cmpyLink == null || cmpyLink.getLinkType() != AccountCmpyLink.LinkType.ADMIN) {
                //existing publisher account... so let's invite that user
                TripPublisherHelper.inviteAgent(agentAccount, agentUp, collab, trip, "", SecurityMgr.AccessLevel.APPEND_N_PUBLISH);
              }
            }
          } else {
            UserInvite userInvite = UserInvite.findByEmail(broker.email);
            TripInvite tripInvite = null;
            if (userInvite != null) {
              tripInvite = TripInvite.findByTripAndInvitee(trip.tripid, userInvite.getPk());
            }

            if (tripInvite == null) {
              // no user found matching the email.. so we need to send a new user invite
              String firstName = broker.givenName;
              String lastName = broker.familyName;
              if (lastName == null) {
                lastName = "";
              }
              if ((firstName == null || firstName.isEmpty()) && broker.name != null && !broker.name.isEmpty()) {
                if (broker.name.trim().indexOf(" ") > 0) {
                  firstName = broker.name.substring(0, broker.name.indexOf(" ")).trim();
                  lastName = broker.name.substring(broker.name.indexOf(" ") + 1).trim();
                }
              }

              if (firstName != null && lastName != null) {
                TripPublisherHelper.TravelerResponse response = TripPublisherHelper.addCollaborator(firstName, lastName, broker.email, "", trip, agentUp, SecurityMgr.AccessLevel.APPEND_N_PUBLISH);
                if (response != TripPublisherHelper.TravelerResponse.OK) {
                  Log.err("AutoCreatePublishTripActor: cannot add collaborator for: " + cmpy.getName() + " RecLocator: " + pnr + " SrcId: " + srcId);
                }
              }
            }
          }

        } catch (AddressException ex) {
          Log.err("AutoCreatePublishTripActor: invalid email for collaborator: "  + cmpy.getName() + " RecLocator: " + pnr + " SrcId: " + srcId );
        }
      }

      //do we need to notify any cc
      if (reservationPackage.ccPersons != null && sendEmail) {
        for (Person cc: reservationPackage.ccPersons) {
          if (cc.email != null && Utils.isValidEmailAddress(cc.email)) {
            String fullName = cc.email;
            if (cc.givenName != null && cc.familyName != null) {
              fullName = cc.givenName + " " + cc.familyName;
            }
            TripController.sendCCUMappedEmailNotitication(fullName, cc.email, trip, agentUp, null);
          }
        }
      }
    }

  }

  public void removeTravelers(ReservationPackage reservationPackage, Trip trip, Account agentAccount, AutoPublishProp prop) {
    //do we need to remove anyone - so they are in the account trip link but not in the reservation package
    if (prop.controlTravelers) {
      List<AccountTripLink> tripLinks = AccountTripLink.findByTrip(trip.getTripid());
      List<String> emailsInRP = new ArrayList<>();
      if (reservationPackage.underName != null && reservationPackage.underName.email != null && !reservationPackage.underName.email.isEmpty()) {
        emailsInRP.add(reservationPackage.underName.email);
      }
      if (reservationPackage.additionalTravelers != null) {
        reservationPackage.additionalTravelers.stream().forEach(p -> {if (p.email != null && !p.email.isEmpty()) {emailsInRP.add(p.email.toLowerCase());}});
      }
      if (tripLinks != null && tripLinks.size() > 0) {
        for (AccountTripLink tl : tripLinks) {
          Account a = Account.find.byId(tl.getPk().getUid());
          if (a != null && a.getEmail() != null && !emailsInRP.contains(a.getEmail().toLowerCase())) {
            Log.debug("TripPublisherHelper: Removing traveler: " + a.getUid() + " from " + trip.getTripid());
            tl.delete();
            try {
              audit:
              {
                TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_TRAVELLER,
                                                     AuditActionType.DELETE,
                                                     AuditActorType.SYSTEM).withTrip(trip).withCmpyid(trip.cmpyid).withUserid(agentAccount.getLegacyId());

                ((AuditTripTraveller) ta.getDetails()).withName(a.getFullName())
                                                      .withId(String.valueOf(a.getUid()))
                                                      .withEmail(a.getEmail())
                                                      .withGroupId("")
                                                      .withGroupName("Default");

                ta.save();
              }
            } catch (Exception e) {

            }
          }
        }
      }
    }
  }

  public static TravelerResponse addPassenger(String firstName, String lastName, String email, String mobile, String agentUserId, Trip trip, TripGroup group, boolean sendEmail, String note) throws Exception {
    try {
      InternetAddress emailAddr = new InternetAddress(email);
      emailAddr.validate();
    } catch (AddressException ex) {
      return TravelerResponse.ERR_UNDEFINED;
    }

    List<AccountTripLink> tripLinks = AccountTripLink.findByEmailTrip(email, trip.getTripid());
    if (tripLinks == null || tripLinks.size() == 0) {
      String groupId = null;
      if (group != null) {
        groupId = group.getGroupid();
      }
      String name = null;
      AccountTripLink tripLink = null;

      if (firstName != null && lastName != null) {
        firstName = firstName.trim();
        lastName = lastName.trim();
        tripLink = AccountController.addTravelerToTrip("system", groupId, trip.tripid, firstName, lastName, email.toLowerCase().trim(), mobile);
      }
      else {
        String[] epart = email.split("@");
        name = epart[0];
        tripLink = AccountController.addTravelerToTrip("system", groupId, trip.tripid, name, email.toLowerCase().trim(), mobile);
      }

      if (tripLink != null) {
        Account traveler = Account.find.byId(tripLink.getPk().getUid());

        MessengerUpdateHelper.build(trip, Account.AID_PUBLISHER)
                .modifyUserTripAccessState(tripLink.getLegacyId(), true);

        //send notification email
        List<Account> passengers = new ArrayList<>();
        passengers.add(traveler);
        UserProfile up;
        TripPublishHistory tp = TripPublishHistory.getLastPublished(trip.tripid, null);
        if (tp != null) {
          up = UserProfile.findByPK(tp.createdby);
        } else {
          up = UserProfile.findByPK(trip.getCreatedby());
        }


        if (sendEmail) {
          if (TripController.sendUMappedEmailNotitication(passengers, trip, up, null, group) != 0) {
            return TravelerResponse.ERR_EMAIL;
          }
        }

        return TravelerResponse.OK;
      } else {
        return TravelerResponse.ERR_UNDEFINED;
      }
    }
    else {
      return TravelerResponse.ERR_DUPLICATE;
    }

  }

  public static TravelerResponse addCollaborator(String firstName, String lastName, String email, String note, Trip tripModel, UserProfile agent, SecurityMgr.AccessLevel accessLevel) {
    /* Building user invite object */
    UserInvite userInvite = UserInvite.findByEmail(email);
    TripInvite tripInvite = null;
    if (userInvite != null) {
      tripInvite = TripInvite.findByTripAndInvitee(tripModel.tripid, userInvite.getPk());
    }

    //New invite
    boolean newInvite = false;
    if (tripInvite == null) {
      if (userInvite == null) {
        userInvite = UserInvite.buildUserInvite(agent.getUserid(), firstName,
                lastName, email);
        userInvite.save();
      }
      tripInvite = TripInvite.buildTripInvite(agent.getUserid(), tripModel, userInvite,
              accessLevel, note);
      newInvite = true;
      tripInvite.save();
    }
    else {
      tripInvite.setAccesslevel(accessLevel);
      tripInvite.setModifiedby(agent.getUserid());
      tripInvite.setStatus(APPConstants.STATUS_ACTIVE);
      tripInvite.setLastupdatedtimestamp(System.currentTimeMillis());
      if (note != null && note.length() > 0) {
        tripInvite.setNote(note);
      }
      tripInvite.update();
    }

    //Create audit log
    audit:
    {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_COLLABORATION,
              newInvite ? AuditActionType.ADD : AuditActionType.MODIFY,
              AuditActorType.WEB_USER)
              .withTrip(tripModel)
              .withCmpyid(tripModel.cmpyid)
              .withUserid(agent.getUserid());

      ((AuditCollaboration) ta.getDetails()).withAction(AuditCollaboration.CollaboraionAction.INVITE)
              .withTripInvite(tripInvite);
      ta.save();;
    }

    UserController.sendNewUserTripShareInviteEmail(false, tripInvite, tripModel, agent);


    if (userInvite != null) {

      //if the trip is already published, send the trip itinerary email
      try {
        ArrayList agents = new ArrayList<>();
        TripAgent tripAgent = new TripAgent();
        tripAgent.setName(userInvite.getFirstname() + " " + userInvite.getLastname());
        tripAgent.setStatus(APPConstants.STATUS_ACTIVE);
        tripAgent.setEmail(userInvite.getEmail());

        agents.add(tripAgent);
        int returnCode = TripController.sendUMappedAgentEmailNotitication(agents, tripModel, agent, null, null);
        if (returnCode != 0) {
          return TravelerResponse.ERR_EMAIL;
        }
        return TravelerResponse.OK;
      } catch (Exception e) {

      }
    }

    return TravelerResponse.OK;

  }


  public static TravelerResponse inviteAgent(Account agentAccount, UserProfile agent, Account collaborator, Trip tripModel, String note, SecurityMgr.AccessLevel accessLevel) {
    TripShare tripShare = TripShare.getSharedTripForUser(tripModel.tripid, collaborator.getLegacyId());

    UserProfile collabUp = UserProfile.findByPK(collaborator.getLegacyId());
    boolean isNewShare = false;
    //No previous trip share found - build a new one
    if (tripShare == null) {
      tripShare = TripShare.buildTripShare(agent.getUserid(), tripModel, collabUp, note, accessLevel);
      tripShare.save();
      isNewShare = true;
    }
    else {
      tripShare.setModifiedby(agent.getUserid());
      tripShare.setLastupdatedtimestamp(System.currentTimeMillis());
      tripShare.setSenttimestamp(System.currentTimeMillis());
      tripShare.setAccesslevel(accessLevel);
      tripShare.setStatus(APPConstants.STATUS_ACTIVE);
      if (note != null && note.length() > 0) {
        tripShare.setNote(note);
      }
      tripShare.update();
    }

    SecurityMgr.addSharedTrip(collaborator, tripModel.tripid, accessLevel);

    //Create audit log
    audit:
    {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_COLLABORATION,
              isNewShare?AuditActionType.ADD:AuditActionType.MODIFY,
              AuditActorType.WEB_USER)
              .withTrip(tripModel)
              .withCmpyid(tripModel.cmpyid)
              .withUserid(agent.getUserid());

      ((AuditCollaboration) ta.getDetails()).withAction(AuditCollaboration.CollaboraionAction.SHARE)
              .withTripShare(tripShare);
      ta.save();
    }

    SharedTripsController.sendUmappedUserTripShareEmail(tripShare, tripModel, agent);
    MessengerUpdateHelper.build(tripModel, agentAccount.getUid())
            .setAddDefaultRooms(true)
            .modifyUserTripAccessState(collabUp.getUserid(), true);


    //if the trip is already published, also send the itinerary
    try {
      //if trip has already been published, send the email notification now
      //create share  + invite
      ArrayList agents = new ArrayList<>();
      TripAgent tripAgent = new TripAgent();
      tripAgent.setName(collaborator.getFullName());
      tripAgent.setStatus(APPConstants.STATUS_ACTIVE);
      tripAgent.setEmail(collaborator.getEmail());


      agents.add(tripAgent);
      int returnCode = TripController.sendUMappedAgentEmailNotitication(agents, tripModel, agent, null, null);
      if (returnCode != 0) {
        return TravelerResponse.ERR_EMAIL;
      }
      return TravelerResponse.OK;
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return TravelerResponse.ERR_UNDEFINED;
  }


  public static ReservationsHolder parseJson (String json, String srcId, long srcPk) {
    ReservationsHolder rh = new ReservationsHolder(srcId, srcPk);
    rh.parseJson(json);
    return rh;
  }
  public static Trip addCover (Trip trip, ReservationPackage rp, Account account) {
    if (rp != null && trip != null && rp.image != null && !rp.image.isEmpty()) {
      String imgUrl = rp.image.get(0).url;
      FileImage image = FileImage.bySourceUrl(imgUrl);
      if (image == null) {
        String fileName = rp.image.get(0).name;
        if (fileName == null || fileName.isEmpty()) {
          fileName = imgUrl.substring(imgUrl.lastIndexOf("/") + 1);

        }
        try {
          image = ImageController.downloadAndSaveImage(imgUrl,
                  fileName,
                  FileSrc.FileSrcType.IMG_USER_DOWNLOAD,
                  null,
                  account,
                  false);
        } catch (Exception e) {
          Log.err("TripPublisherHelper:addCover - cannot download image for trip: " + trip.getTripid() + " url: " + imgUrl);
        }
      }
      if (image != null) {
        trip.setFileImage(image);
      }
    }
    return trip;
  }

  public static List<Destination> getDocuments (ReservationPackage rp, Company c) {
    List<Destination> destinations = new ArrayList<>();
    if (c != null && rp != null && rp.downloadable != null && !rp.downloadable.isEmpty()) {
      for (MediaObject mediaObject : rp.downloadable) {
        if (mediaObject.umId != null) {
          Destination destination = Destination.find.byId(mediaObject.umId);
          if (destination != null && destination.getStatus() == 0 && destination.getCmpyid().equals(c.getCmpyid())) {
            destinations.add(destination);
          }
        }
      }
    }
    return destinations;
  }

  public static void  addDocuments (List<Destination> destinations, Trip trip, Account account) {
    for (Destination d: destinations) {
      try {
        TripDestination tripDest = new TripDestination();
        tripDest.setTripdestid(Utils.hash(d.getDestinationid() + trip.getTripid()));
        tripDest.setDestinationid(d.getDestinationid());
        tripDest.setTripid(trip.getTripid());
        tripDest.setCreatedtimestamp(System.currentTimeMillis());
        tripDest.setCreatedby(account.getLegacyId());
        tripDest.setName("");
        tripDest.setStatus(APPConstants.STATUS_ACTIVE);
        tripDest.setModifiedby(account.getLegacyId());
        tripDest.setLastupdatedtimestamp(System.currentTimeMillis());
        tripDest.save();
      } catch (Exception e) {

      }
    }
  }

  public String getCoverPhoto (Trip trip) {
    try {
      if (trip != null) {
        ItineraryAnalyzeResult r = offerService.analyzeTrip(trip.getTripid());
        List<String> urls = new ArrayList<>();
        Location.GeoLocation startLoc = null;
        if (r != null && r.getSegmentResults() != null && r.getSegmentResults().size() > 0) {
          if (r.getSegmentResults().get(0).getSegment().getStartLocation() != null) {
            startLoc = r.getSegmentResults().get(0).getSegment().getStartLocation().getGeoLocation();
          }
          int i = 0;
          for (TripSegmentAnalyzeResult s : r.getSegmentResults()) {
            if (r.getSegmentResults().size() > 1) {
              i++;
            }
            if (i < r.getSegmentResults().size() && s.getSegment().getEndLocation() != null && s.getSegment()
                                                                                                .getEndLocation()
                                                                                                .getGeoLocation() != null) {

              if (startLoc == null || startLoc.getLatitude() != s.getSegment().getEndLocation().getGeoLocation()
                                                                 .getLatitude() || startLoc.getLongitude() != s.getSegment()
                                                                                                                                                              .getEndLocation()
                                                                                                                                                              .getGeoLocation()
                                                                                                                                                              .getLongitude()) {
                UMappedCity city = umCityLookup.getCity(s.getSegment().getEndLocation().getGeoLocation());
                if (city != null && city.getExtraDetail() != null) {
                  UMappedCityLookup.ExtraDetail detail = city.getExtraDetailJson();
                  if (detail != null && detail.photos != null) {
                    urls.addAll(detail.photos);
                  }
                }
              }

            }
          }
        }
        if (urls != null && urls.size() > 0) {
          //randomly pick 1
          String url = null;
          for (int i = 0; i < 5; i++) {
            int randomNum = ThreadLocalRandom.current().nextInt(0, urls.size());
            url = urls.get(randomNum);
            ImageView v = trip.getCoverView();
            if (trip.getCoverView() == null || (trip.getCoverView() != null && trip.getCoverView().url != null && !trip.getCoverView().url
                .equals(url))) {
              return url;
            }
          }
          return url;

        }
        //see if there is a default cover photo
        Company c = Company.find.byId(trip.getCmpyid());
        if (c != null && c.getTag() != null && c.getTag().contains(APPConstants.CMPY_TAG_DEFAULT_PHOTO)) {
          if (c.getTag().contains(APPConstants.CMPY_TAG_DEFAULT_PHOTO+":")) {
            String defUrl = c.getTag().substring(c.getTag().indexOf(APPConstants.CMPY_TAG_DEFAULT_PHOTO));
            if (defUrl.contains("http")) {
              if (defUrl.contains(" ")) {
                defUrl = defUrl.substring(defUrl.indexOf("http"), defUrl.indexOf(" "));
              } else {
                defUrl = defUrl.substring(defUrl.indexOf("http"));
              }
              return defUrl;
            }
          } else {
            return "https://um-image-prd.s3.amazonaws.com/vendor/wikipedia/F-5GS8Tvg6I/1280px-currambine_skyscape_scattered_clouds_blue_sky.jpg";
          }
        }
      }
    } catch (Exception e) {
      Log.err("TripPublisherHelper:getCoverPhoto + cannot process: " + e, e);
    }


    return null;
  }

  public OfferService getOfferService() {
    return offerService;
  }

  public UMappedCityLookup getUmCityLookup() {
    return umCityLookup;
  }

  public OfferProviderRepository getOfferProviderRepository() {
    return offerProviderRepository;
  }
}
