package com.mapped.publisher.view;

import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.utils.Log;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;
import com.umapped.persistence.reservation.accommodation.UmAccommodationTraveler;
import com.umapped.persistence.reservation.accommodation.UmLodgingBusiness;
import models.publisher.TripDetail;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

/**
 * Created by ryan on 16/03/17.
 */
public class UmAccommodationReservationView extends TripBookingDetailView
        implements Serializable{

    private List<PassengerInfo> passengers;
    public String numNights;

    public static class PassengerInfo
            implements Serializable {
        public UmAccommodationTraveler traveler;

        public String getName() {
            return traveler.getName();
        }

        public String getGivenName() {
            return traveler.getGivenName();
        }

        public String getFamilyName() {
            return traveler.getFamilyName();
        }

        public String getRoomType() {
            return traveler.getRoomType();
        }

        public String getAmenities() {
            return traveler.getAmenities();
        }

        public String getStatus() {
            return traveler.getConfirmStatus();
        }

        public String getMembershipId() {
            return traveler.getProgram().membershipNumber;
        }

        public String getBedding() { return traveler.getBedding(); }
    }

    public UmAccommodationReservationView() {
        this.detailsTypeId = ReservationType.HOTEL;
    }

    public void buildViewInfo(String tripId, TripDetail tripDetail, boolean printDateFormat, SecurityMgr.AccessLevel tripAccessLevel) {
        super.buildViewInfo(tripId, tripDetail, printDateFormat, tripAccessLevel);

        UmAccommodationReservation hotelReservation = cast(tripDetail.getReservation(), UmAccommodationReservation.class);
        if (hotelReservation != null) {
            if (this.poiMain == null && this.providerName == null) {
                this.providerName = hotelReservation.getAccommodation().name;
                this.name = hotelReservation.getAccommodation().name;
            }
        }
        else {
            Log.err("Database records missing no hotel details for trip " + tripId);
        }
    }

    public void buildViewInfo(TripDetail tripDetail, boolean printDateFormat, SecurityMgr.AccessLevel tripAccessLevel) {
        super.buildViewInfo(tripDetail, printDateFormat, tripAccessLevel);
    }

    public static List<UmAccommodationReservationView.PassengerInfo> getPassengerInfo(TripBookingDetailView view) {
        List<UmAccommodationReservationView.PassengerInfo> passengers = new ArrayList<>();
        UmAccommodationReservation hotel = cast(view.reservation, UmAccommodationReservation.class);

        if (view != null && hotel != null && hotel.getTravelers() != null) {

            for (UmTraveler t : hotel.getTravelers()) {
                UmAccommodationTraveler at = cast(t, UmAccommodationTraveler.class);
                if (at != null) {
                    PassengerInfo pi = new PassengerInfo();
                    pi.traveler = at;
                    passengers.add(pi);
                }
            }
        }
        return passengers;
    }

    public List<PassengerInfo> getPassengers() {
        this.passengers = getPassengerInfo(this);
        return passengers;
    }
}
