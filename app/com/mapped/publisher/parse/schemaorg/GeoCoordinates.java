package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by ryan on 15-08-17.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GeoCoordinates
    extends Thing {

  public String elevation;

  public String latitude;

  public String longitude;
}
