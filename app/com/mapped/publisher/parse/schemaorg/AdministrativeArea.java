package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by surge on 2016-06-28.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AdministrativeArea
    extends Place {
}
