package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class InvitesMgr {

	 private static final String SELECT_ALL = "Select Pk, InviteeEmail, UsersId, Status, LastUpdateTimestamp, CreateTimestamp from Invites;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, InviteeEmail, UsersId, Status, LastUpdateTimestamp, CreateTimestamp from Invites where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from Invites where Pk = ?;";
	 private static final String INSERT = "insert into Invites (Pk, InviteeEmail, UsersId, Status, LastUpdateTimestamp, CreateTimestamp) values (?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update Invites SET InviteeEmail=?, UsersId=?, Status=?, LastUpdateTimestamp=? WHERE Pk = ?; ";
	 private static final String SELECT_ALL_BY_USERS_ID = "Select Pk, InviteeEmail, UsersId, Status, LastUpdateTimestamp, CreateTimestamp from Invites where UsersId=? ";
	 private static final String DELETE_BY_USERS_ID = "delete from Invites where UsersId=? ";
	 public static ArrayList <Invites> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return InvitesMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static Invites getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<Invites> results = InvitesMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (Invites c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getInviteeEmail());
				 prep.setString(3, c.getUsersId());
				 prep.setLong(4, c.getStatus());
				 prep.setLong(5, c.getLastUpdateTimestamp());
				 prep.setLong(6, c.getCreateTimestamp());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (Invites c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getInviteeEmail());
				 prep.setString(2, c.getUsersId());
				 prep.setLong(3, c.getStatus());
				 prep.setLong(4, c.getLastUpdateTimestamp());
				 prep.setString(5, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <Invites> getAllByUsersId(String id) throws SQLException {
		 ResultSet rs = null;
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_USERS_ID);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 return InvitesMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void deleteByUsersId (String id) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_USERS_ID);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <Invites> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <Invites> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 Invites c = new Invites();
			 c.setPk(rs.getString("Pk"));
			 c.setInviteeEmail(rs.getString("InviteeEmail"));
			 c.setUsersId(rs.getString("UsersId"));
			 c.setStatus(rs.getLong("Status"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 results.add(c);
		 }
		 return results;
	 }
}
