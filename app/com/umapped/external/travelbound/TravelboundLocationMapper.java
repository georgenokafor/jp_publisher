package com.umapped.external.travelbound;

import com.google.inject.Singleton;
import com.umapped.external.travelbound.api.*;
import org.apache.commons.lang.StringUtils;

/**
 * Created by wei on 2017-05-16.
 */
@Singleton public class TravelboundLocationMapper {

  public String mapAirport(TAirport airport) {
    if (airport != null) {
      return airport.getValue();
    }
    else {
      return null;
    }
  }

  public String mapAddress(TAlternateAddress address) {
    if (address != null) {
      StringBuilder builder = new StringBuilder();
      builder.append(StringUtils.trimToEmpty(address.getAddressLine1()));
      builder.append(", ");
      builder.append(StringUtils.trimToEmpty(address.getAddressLine2()));
      builder.append(", ");
      builder.append(StringUtils.trimToEmpty(address.getCity().getValue()));
      builder.append(" ");
      builder.append(StringUtils.trimToEmpty(address.getCounty()));
      return builder.toString();
    }
    else {
      return null;
    }
  }

  public String mapWhere(TWhere where) {
    if (where.getAirport() != null) {
      return mapAirport(where.getAirport());
    }

    if (where.getHotel() != null) {
      return mapItem(where.getHotel());
    }

    if (where.getApartment() != null) {
      return mapItem(where.getApartment());
    }

    if (where.getMeetingPoint() != null) {
      return mapItem(where.getMeetingPoint());
    }

    if (where.getStation() != null) {
      return mapStation(where.getStation());
    }
    return null;
  }

  public String mapStation(TStation station) {
    return station.getValue();
  }

  public String mapItem(TItem item) {
    return item.getValue();
  }

  public String mapMeetingPoint(TMeetingPoint meetingPoint) {
    return meetingPoint.getValue();
  }
}
