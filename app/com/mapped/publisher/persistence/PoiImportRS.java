package com.mapped.publisher.persistence;

import java.io.Serializable;

/**
 * Created by surge on 2015-01-30.
 */
public class PoiImportRS
      implements Cloneable, Serializable {

  public enum ImportState {
    UNPROCESSED,
    MATCH,
    MISMATCH,
    UNSURE;
    public static ImportState fromIdx(int idx) {
      for (ImportState s : ImportState.values()) {
        if (s.ordinal() == idx) {
          return s;
        }
      }
      return null;
    }
  }

  public static PoiImportRS buildRecord(final String sourceName,final String typeName) {
    PoiImportRS rec = new PoiImportRS();
    rec.state = ImportState.UNPROCESSED;
    rec.srcId = FeedSourcesInfo.byName(sourceName);
    rec.timestamp = System.currentTimeMillis();
    rec.typeId = PoiTypeInfo.Instance().byName(typeName).getId();
    return rec;
  }

  public long   importId;
  public ImportState state;
  public Integer srcId;
  public Long   poiId;
  public Integer typeId;
  public String hash;
  public Long   timestamp;
  public String srcReference;
  public String srcFilename;
  public String name;
  public String countryName;
  public String countryCode;
  public String phone;
  public String region;
  public String city;
  public String address;
  public String postalCode;
  public String raw;
  public Float  latitude;
  public Float  longitude;
  public Integer score;
  public int    version;
}
