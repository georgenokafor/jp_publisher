package com.umapped.persistence.reservation;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmRentalCar
    extends UmProduct {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public UmThing brand;

  /**
   * [Organization] The rental company, e.g. 'Hertz'.
   */
  public UmOrganization rentalCompany;

}
