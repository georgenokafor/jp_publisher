package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class UserSession { 
 
	 public final static String UserSessionTypeId = "80"; 
	 protected static String pkTypeId =  "81"; 
	 private String pk; 
	 protected static String usersIdTypeId =  "82"; 
	 private String usersId; 
	 protected static String devIdTypeId =  "83"; 
	 private String devId; 
	 protected static String oSVerTypeId =  "85"; 
	 private String oSVer; 
	 protected static String devVerTypeId =  "86"; 
	 private String devVer; 
	 protected static String devTypeId =  "87"; 
	 private String dev; 
	 protected static String syncTimestampTypeId =  "88"; 
	 private long syncTimestamp; 
	 protected static String lastUpdateTimestampTypeId =  "89"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "90"; 
	 private long createTimestamp; 
	 protected static String notifyTypeId =  "91"; 
	 private String notify; 

 	 // Contructor 
	 public UserSession () {} 
	 public UserSession ( String pk,  String usersId,  String devId,  String oSVer,  String devVer,  String dev,  long syncTimestamp,  long lastUpdateTimestamp,  long createTimestamp,  String notify ) { 
		 this.pk=pk; 
		 this.usersId=usersId; 
		 this.devId=devId; 
		 this.oSVer=oSVer; 
		 this.devVer=devVer; 
		 this.dev=dev; 
		 this.syncTimestamp=syncTimestamp; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.notify=notify; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getUsersId () { 
	 	 return usersId; 
 	 } 
 
 	 public void setUsersId(String usersId) { 
	 	 this.usersId = usersId; 
 	 } 
 
 	 public  String getDevId () { 
	 	 return devId; 
 	 } 
 
 	 public void setDevId(String devId) { 
	 	 this.devId = devId; 
 	 } 
 
 	 public  String getOSVer () { 
	 	 return oSVer; 
 	 } 
 
 	 public void setOSVer(String oSVer) { 
	 	 this.oSVer = oSVer; 
 	 } 
 
 	 public  String getDevVer () { 
	 	 return devVer; 
 	 } 
 
 	 public void setDevVer(String devVer) { 
	 	 this.devVer = devVer; 
 	 } 
 
 	 public  String getDev () { 
	 	 return dev; 
 	 } 
 
 	 public void setDev(String dev) { 
	 	 this.dev = dev; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  String getNotify () { 
	 	 return notify; 
 	 } 
 
 	 public void setNotify(String notify) { 
	 	 this.notify = notify; 
 	 } 
 
 	 public Users getUsers () throws Exception{ 
		 return UsersMgr.getByPk(usersId); 
	}
	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: UserSession"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 UsersId: ").append(this.usersId );  
		 sb.append("	 DevId: ").append(this.devId );  
		 sb.append("	 OSVer: ").append(this.oSVer );  
		 sb.append("	 DevVer: ").append(this.devVer );  
		 sb.append("	 Dev: ").append(this.dev );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 Notify: ").append(this.notify );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", UserSessionTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (usersId != null) {
			 memberNode.put(usersIdTypeId, usersId);
		 }
		 if (devId != null) {
			 memberNode.put(devIdTypeId, devId);
		 }
		 if (oSVer != null) {
			 memberNode.put(oSVerTypeId, oSVer);
		 }
		 if (devVer != null) {
			 memberNode.put(devVerTypeId, devVer);
		 }
		 if (dev != null) {
			 memberNode.put(devTypeId, dev);
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (notify != null) {
			 memberNode.put(notifyTypeId, notify);
		 }
		 return classNode;
	 } 
	 public static UserSession parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 UserSession  c= new UserSession(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.usersId = fields.findPath(usersIdTypeId).textValue();
			 c.devId = fields.findPath(devIdTypeId).textValue();
			 c.oSVer = fields.findPath(oSVerTypeId).textValue();
			 c.devVer = fields.findPath(devVerTypeId).textValue();
			 c.dev = fields.findPath(devTypeId).textValue();
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 c.notify = fields.findPath(notifyTypeId).textValue();
			 return c;
		} else
			 return null;
	 } 
} 
 
