
package com.umapped.external.wetu.content;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "destination_content_entity_id",
    "country_content_entity_id",
    "latitude",
    "longitude",
    "driving_latitude",
    "driving_longitude",
    "country",
    "region",
    "area",
    "location",
    "destination"
})
public class Position {

    @JsonProperty("destination_content_entity_id")
    private int destinationContentEntityId;
    @JsonProperty("country_content_entity_id")
    private int countryContentEntityId;
    @JsonProperty("latitude")
    private double latitude;
    @JsonProperty("longitude")
    private double longitude;
    @JsonProperty("driving_latitude")
    private double drivingLatitude;
    @JsonProperty("driving_longitude")
    private double drivingLongitude;
    @JsonProperty("country")
    private String country;
    @JsonProperty("region")
    private String region;
    @JsonProperty("area")
    private String area;
    @JsonProperty("location")
    private String location;
    @JsonProperty("destination")
    private String destination;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("destination_content_entity_id")
    public int getDestinationContentEntityId() {
        return destinationContentEntityId;
    }

    @JsonProperty("destination_content_entity_id")
    public void setDestinationContentEntityId(int destinationContentEntityId) {
        this.destinationContentEntityId = destinationContentEntityId;
    }

    @JsonProperty("country_content_entity_id")
    public int getCountryContentEntityId() {
        return countryContentEntityId;
    }

    @JsonProperty("country_content_entity_id")
    public void setCountryContentEntityId(int countryContentEntityId) {
        this.countryContentEntityId = countryContentEntityId;
    }

    @JsonProperty("latitude")
    public double getLatitude() {
        return latitude;
    }

    @JsonProperty("latitude")
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("longitude")
    public double getLongitude() {
        return longitude;
    }

    @JsonProperty("longitude")
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @JsonProperty("driving_latitude")
    public double getDrivingLatitude() {
        return drivingLatitude;
    }

    @JsonProperty("driving_latitude")
    public void setDrivingLatitude(double drivingLatitude) {
        this.drivingLatitude = drivingLatitude;
    }

    @JsonProperty("driving_longitude")
    public double getDrivingLongitude() {
        return drivingLongitude;
    }

    @JsonProperty("driving_longitude")
    public void setDrivingLongitude(double drivingLongitude) {
        this.drivingLongitude = drivingLongitude;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("region")
    public String getRegion() {
        return region;
    }

    @JsonProperty("region")
    public void setRegion(String region) {
        this.region = region;
    }

    @JsonProperty("area")
    public String getArea() {
        return area;
    }

    @JsonProperty("area")
    public void setArea(String area) {
        this.area = area;
    }

    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(String location) {
        this.location = location;
    }

    @JsonProperty("destination")
    public String getDestination() {
        return destination;
    }

    @JsonProperty("destination")
    public void setDestination(String destination) {
        this.destination = destination;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("destinationContentEntityId", destinationContentEntityId).append("countryContentEntityId", countryContentEntityId).append("latitude", latitude).append("longitude", longitude).append("drivingLatitude", drivingLatitude).append("drivingLongitude", drivingLongitude).append("country", country).append("region", region).append("area", area).append("location", location).append("destination", destination).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(region).append(drivingLongitude).append(drivingLatitude).append(location).append(countryContentEntityId).append(destination).append(country).append(area).append(additionalProperties).append(destinationContentEntityId).append(longitude).append(latitude).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Position) == false) {
            return false;
        }
        Position rhs = ((Position) other);
        return new EqualsBuilder().append(region, rhs.region).append(drivingLongitude, rhs.drivingLongitude).append(drivingLatitude, rhs.drivingLatitude).append(location, rhs.location).append(countryContentEntityId, rhs.countryContentEntityId).append(destination, rhs.destination).append(country, rhs.country).append(area, rhs.area).append(additionalProperties, rhs.additionalProperties).append(destinationContentEntityId, rhs.destinationContentEntityId).append(longitude, rhs.longitude).append(latitude, rhs.latitude).isEquals();
    }

}
