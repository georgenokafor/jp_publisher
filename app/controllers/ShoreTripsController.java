package controllers;

import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.*;
import com.mapped.publisher.parse.shoretrips.IntegrationResponse;
import com.mapped.publisher.parse.shoretrips.ObjectFactory;
import com.mapped.publisher.persistence.PageAttachType;
import com.mapped.publisher.persistence.PoiMgr;
import com.mapped.publisher.persistence.PoiRS;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import models.publisher.*;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTimeConstants;
import play.data.FormFactory;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.util.*;
import java.util.concurrent.CompletionStage;

/**
 * Created by twong on 15-06-08.
 */
public class ShoreTripsController
    extends Controller {
  private static String[] dateTimeFormat = {"MMM dd, yyyy hh:mm a"};

  @Inject
  WSClient    ws;
  @Inject
  FormFactory formFactory;

  @With({Authenticated.class, Authorized.class})
  public CompletionStage<Result> getShoreTripByPort(String portCode, String activityId) {
    BaseView   view     = new BaseView();
    TripDetail activity = TripDetail.findByPK(activityId);

    return ws.url("https://www.shoretrips.com/integration/gateway/LOCATIONEXCURSIONS/" + portCode +
                  "?partner=UMAPPED&accesskey=IP-UM-20150612")
             .setRequestTimeout(50000L)
             .get()
             .thenApply((WSResponse response) -> {
               try {

                 if (response.getStatus() == 200) {
                   ExternalActivities externalActivity = new ExternalActivities();
                   externalActivity.bookingDetailsId = activity.detailsid;
                   externalActivity.tripId = activity.tripid;
                   externalActivity.name = activity.getName();

                   JAXBContext  jaxbContext  = JAXBContext.newInstance(ObjectFactory.class);
                   Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                   IntegrationResponse integrationResponse = (IntegrationResponse) unmarshaller.unmarshal(response
                                                                                                              .getBodyAsStream());
                   if (integrationResponse.getExcursions() != null) {
                     if (integrationResponse.getExcursions().getRegions() != null) {
                       if (integrationResponse.getExcursions().getRegions().getRegion() != null) {
                         if (integrationResponse.getExcursions().getRegions().getRegion().getPorts() != null) {
                           if (integrationResponse.getExcursions()
                                                  .getRegions()
                                                  .getRegion()
                                                  .getPorts()
                                                  .getLocation() != null) {
                             List<IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup>
                                 tripGroupList = integrationResponse
                                 .getExcursions()
                                 .getRegions()
                                 .getRegion()
                                 .getPorts()
                                 .getLocation()
                                 .getTripGroups()
                                 .getTripGroup();
                             if (tripGroupList != null) {
                               for (IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup
                                   tripGroup : tripGroupList) {
                                 if (tripGroup.getTrips() != null && tripGroup.getTrips().getTrip() != null &&
                                     tripGroup
                                                                                                                   .getTrips()
                                                                                                                   .getTrip()
                                                                                                                   .size() > 0) {
                                   for (IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups
                                       .TripGroup.Trips.Trip trip : tripGroup
                                       .getTrips()
                                       .getTrip()) {
                                     if (isTripAvailable(trip, activity.getStarttimestamp())) {

                                       ExternalActivity shoreTrip = new ExternalActivity();
                                       shoreTrip.addImg(trip.getImageUrl());
                                       shoreTrip.addImg(trip.getImageUrl2());
                                       shoreTrip.url = trip.getUrl();
                                       shoreTrip.name = trip.getName();
                                       shoreTrip.providerType = ExternalActivity.ProviderType.SHORETRIPS;
                                       if (trip.getCode() != null) {
                                         shoreTrip.tripCode = String.valueOf(trip.getCode());
                                       }
                                       StringBuilder sb = new StringBuilder();

                                       if (trip.getDescription() != null && trip.getDescription().trim().length() > 0) {
                                         sb.append(trip.getDescription());
                                         sb.append("<br/>");
                                       }
                                       if (trip.getIntroduction() != null && trip.getIntroduction()
                                                                                 .trim()
                                                                                 .length() > 0) {
                                         sb.append(trip.getIntroduction());
                                         sb.append("<br/>");
                                       }
                                       if (trip.getNarrative() != null && trip.getNarrative().trim().length() > 0) {
                                         sb.append(trip.getNarrative());
                                         sb.append("<br/>");
                                       }
                                       if (trip.getClosing() != null && trip.getClosing().trim().length() > 0) {
                                         sb.append(trip.getClosing());
                                         sb.append("<br/>");
                                       }

                                       if (trip.getMeetingInstructions() != null && trip.getMeetingInstructions()
                                                                                        .trim()
                                                                                        .length() > 0) {
                                         if (sb.toString().length() > 0) {
                                           sb.append("<br/>");
                                         }
                                         sb.append(trip.getMeetingInstructions());
                                         sb.append("<br/>");
                                       }
                                       if (trip.getSpecialInstructions() != null && trip.getSpecialInstructions()
                                                                                        .trim()
                                                                                        .length() > 0) {
                                         if (sb.toString().length() > 0) {
                                           sb.append("<br/>");
                                         }
                                         sb.append(trip.getSpecialInstructions());
                                       }
                                       if (trip.getRestrictions() != null && trip.getRestrictions()
                                                                                 .trim()
                                                                                 .length() > 0) {
                                         if (sb.toString().length() > 0) {
                                           sb.append("<br/>");
                                         }
                                         sb.append(trip.getRestrictions());
                                         sb.append("<br/>");
                                       }
                                       shoreTrip.description = sb.toString();

                                       // add prices
                                       if (trip.getPrices() != null && trip.getPrices()
                                                                           .getTripPrice() != null && trip.getPrices()
                                                                                                          .getTripPrice()
                                                                                                          .size() > 0) {
                                         for (IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups
                                             .TripGroup.Trips.Trip.Prices.TripPrice tripPrice : trip
                                             .getPrices()
                                             .getTripPrice()) {
                                           ExternalActivityPrice price = new ExternalActivityPrice();
                                           price.amount = tripPrice.getAmount();
                                           if (tripPrice.getCode() != null) {
                                             price.code = String.valueOf(tripPrice.getCode());
                                           }
                                           price.description = tripPrice.getDescription();
                                           price.name = tripPrice.getName();
                                           price.qty = tripPrice.getOrder();
                                           shoreTrip.addPrice(price);
                                         }
                                       }

                                       // add time
                                       if (trip.getTimes() != null && trip.getTimes()
                                                                          .getTripTime() != null && trip.getTimes()
                                                                                                        .getTripTime()
                                                                                                        .size() > 0) {
                                         for (IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups
                                             .TripGroup.Trips.Trip.Times.TripTime tripTime : trip
                                             .getTimes()
                                             .getTripTime()) {

                                           ExternalActivityTimes time = new ExternalActivityTimes();
                                           if (tripTime.getCode() != null) {
                                             time.code = String.valueOf(tripTime.getCode());
                                           }
                                           time.description = tripTime.getDescription();
                                           time.duration = tripTime.getDuration();
                                           time.startTime = tripTime.getDeparture();
                                           shoreTrip.addTimes(time);

                                         }
                                       }

                                       if (trip.getDaysOfWeekMon() != null && trip.getDaysOfWeekMon().equals("true")) {
                                         shoreTrip.addDay(DateTimeConstants.MONDAY);
                                       }
                                       if (trip.getDaysOfWeekTue() != null && trip.getDaysOfWeekTue().equals("true")) {
                                         shoreTrip.addDay(DateTimeConstants.TUESDAY);
                                       }
                                       if (trip.getDaysOfWeekWed() != null && trip.getDaysOfWeekWed().equals("true")) {
                                         shoreTrip.addDay(DateTimeConstants.WEDNESDAY);
                                       }
                                       if (trip.getDaysOfWeekThu() != null && trip.getDaysOfWeekThu().equals("true")) {
                                         shoreTrip.addDay(DateTimeConstants.THURSDAY);
                                       }
                                       if (trip.getDaysOfWeekFri() != null && trip.getDaysOfWeekFri().equals("true")) {
                                         shoreTrip.addDay(DateTimeConstants.FRIDAY);
                                       }
                                       if (trip.getDaysOfWeekSat() != null && trip.getDaysOfWeekSat().equals("true")) {
                                         shoreTrip.addDay(DateTimeConstants.SATURDAY);
                                       }
                                       if (trip.getDaysOfWeekSun() != null && trip.getDaysOfWeekSun().equals("true")) {
                                         shoreTrip.addDay(DateTimeConstants.SUNDAY);
                                       }
                                       shoreTrip.locLat = integrationResponse.getExcursions()
                                                                             .getRegions()
                                                                             .getRegion()
                                                                             .getLat();
                                       shoreTrip.locLong = integrationResponse.getExcursions()
                                                                              .getRegions()
                                                                              .getRegion()
                                                                              .getLng();

                                       externalActivity.addActivity(shoreTrip);
                                       if (shoreTrip.tripCode != null) {
                                         CacheMgr.set(APPConstants.CACHE_SHORE_TRIP_PREFIX + shoreTrip.tripCode,
                                                      shoreTrip);
                                       }

                                     }
                                   }
                                 }
                               }
                             }
                           }
                         }
                       }
                     }
                   }
                   return ok(views.html.shoreTrip.shoretripModal.render(externalActivity));
                 }

                 return ok(views.html.common.message.render(view));

               }
               catch (Exception e) {
                 e.printStackTrace();
               }

               return ok(views.html.common.message.render(view));

             });
  }

  private static boolean isTripAvailable(IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups
                                             .TripGroup.Trips.Trip trip,
                                         long timestamp) {


    org.joda.time.DateTime dateTime = new org.joda.time.DateTime(timestamp);
    int                    day      = dateTime.getDayOfWeek();
    switch (day) {
      case DateTimeConstants.MONDAY:
        if (trip.getDaysOfWeekMon() != null && trip.getDaysOfWeekMon().toLowerCase().equals("true")) {
          return true;
        }
      case DateTimeConstants.TUESDAY:
        if (trip.getDaysOfWeekMon() != null && trip.getDaysOfWeekTue().toLowerCase().equals("true")) {
          return true;
        }
      case DateTimeConstants.WEDNESDAY:
        if (trip.getDaysOfWeekMon() != null && trip.getDaysOfWeekWed().toLowerCase().equals("true")) {
          return true;
        }
      case DateTimeConstants.THURSDAY:
        if (trip.getDaysOfWeekMon() != null && trip.getDaysOfWeekThu().toLowerCase().equals("true")) {
          return true;
        }
      case DateTimeConstants.FRIDAY:
        if (trip.getDaysOfWeekMon() != null && trip.getDaysOfWeekFri().toLowerCase().equals("true")) {
          return true;
        }
      case DateTimeConstants.SATURDAY:
        if (trip.getDaysOfWeekMon() != null && trip.getDaysOfWeekSat().toLowerCase().equals("true")) {
          return true;
        }
      case DateTimeConstants.SUNDAY:
        if (trip.getDaysOfWeekMon() != null && trip.getDaysOfWeekSun().toLowerCase().equals("true")) {
          return true;
        }

    }


    return false;
  }

  @With({Authenticated.class, Authorized.class})
  public CompletionStage<Result> getShoreTripByPorCode(String portCode,
                                                       String tripId,
                                                       String startDate,
                                                       String startTime) {
    BaseView view = new BaseView();

     long ts = 0;
    if (startDate != null && startTime != null) {
      ts = Utils.getMilliSecs(startDate + " " + startTime);
      System.out.println(" @@@@@@@@@@ Timestamp: " + ts);
    }

    WSRequest urlRequest = ws.url("https://www.shoretrips.com/integration/gateway/LOCATIONEXCURSIONS/" + portCode +
                                  "?partner=UMAPPED&accesskey=IP-UM-20150612")
                             .setRequestTimeout(50000L);
    final long timestamp = ts;
    return urlRequest.get().thenApplyAsync((WSResponse response) -> {

      try {

        if (response.getStatus() == 200) {
          ExternalActivities externalActivity = new ExternalActivities();
          externalActivity.tripId = tripId;

          JAXBContext  jaxbContext  = JAXBContext.newInstance(ObjectFactory.class);
          Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
          IntegrationResponse integrationResponse = (IntegrationResponse) unmarshaller.unmarshal(response
                                                                                                     .getBodyAsStream
                                                                                                         ());
          if (integrationResponse.getExcursions() != null) {
            if (integrationResponse.getExcursions().getRegions() != null) {
              if (integrationResponse.getExcursions().getRegions().getRegion() != null) {
                if (integrationResponse.getExcursions().getRegions().getRegion().getPorts() != null) {
                  if (integrationResponse.getExcursions().getRegions().getRegion().getPorts().getLocation() != null) {
                    List<IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup>
                        tripGroupList = integrationResponse
                        .getExcursions()
                        .getRegions()
                        .getRegion()
                        .getPorts()
                        .getLocation()
                        .getTripGroups()
                        .getTripGroup();
                    if (tripGroupList != null) {
                      for (IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup
                          tripGroup : tripGroupList) {
                        if (tripGroup.getTrips() != null && tripGroup.getTrips()
                                                                     .getTrip() != null && tripGroup.getTrips()
                                                                                                    .getTrip()
                                                                                                    .size() > 0) {
                          for (IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup
                              .Trips.Trip trip : tripGroup
                              .getTrips()
                              .getTrip()) {


                            ExternalActivity shoreTrip = new ExternalActivity();
                            shoreTrip.addImg(trip.getImageUrl());
                            shoreTrip.addImg(trip.getImageUrl2());
                            shoreTrip.url = trip.getUrl();
                            shoreTrip.name = trip.getName();
                            shoreTrip.providerType = ExternalActivity.ProviderType.SHORETRIPS;
                            if (trip.getCode() != null) {
                              shoreTrip.tripCode = String.valueOf(trip.getCode());
                            }
                            StringBuilder sb = new StringBuilder();

                            if (trip.getDescription() != null && trip.getDescription().trim().length() > 0) {
                              sb.append(trip.getDescription());
                              sb.append("<br/>");
                            }
                            if (trip.getIntroduction() != null && trip.getIntroduction().trim().length() > 0) {
                              sb.append(trip.getIntroduction());
                              sb.append("<br/>");
                            }
                            if (trip.getNarrative() != null && trip.getNarrative().trim().length() > 0) {
                              sb.append(trip.getNarrative());
                              sb.append("<br/>");
                            }
                            if (trip.getClosing() != null && trip.getClosing().trim().length() > 0) {
                              sb.append(trip.getClosing());
                              sb.append("<br/>");
                            }

                            if (trip.getMeetingInstructions() != null && trip.getMeetingInstructions()
                                                                             .trim()
                                                                             .length() > 0) {
                              if (sb.toString().length() > 0) {
                                sb.append("<br/>");
                              }
                              sb.append(trip.getMeetingInstructions());
                              sb.append("<br/>");
                            }
                            if (trip.getSpecialInstructions() != null && trip.getSpecialInstructions()
                                                                             .trim()
                                                                             .length() > 0) {
                              if (sb.toString().length() > 0) {
                                sb.append("<br/>");
                              }
                              sb.append(trip.getSpecialInstructions());
                            }
                            if (trip.getRestrictions() != null && trip.getRestrictions().trim().length() > 0) {
                              if (sb.toString().length() > 0) {
                                sb.append("<br/>");
                              }
                              sb.append(trip.getRestrictions());
                              sb.append("<br/>");
                            }
                            shoreTrip.description = sb.toString();

                            // add prices
                            if (trip.getPrices() != null && trip.getPrices().getTripPrice() != null && trip.getPrices()
                                                                                                           .getTripPrice()
                                                                                                           .size() >
                                                                                                       0) {
                              for (IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup
                                  .Trips.Trip.Prices.TripPrice tripPrice : trip
                                  .getPrices()
                                  .getTripPrice()) {
                                ExternalActivityPrice price = new ExternalActivityPrice();
                                price.amount = tripPrice.getAmount();
                                if (tripPrice.getCode() != null) {
                                  price.code = String.valueOf(tripPrice.getCode());
                                }
                                price.description = tripPrice.getDescription();
                                price.name = tripPrice.getName();
                                price.qty = tripPrice.getOrder();
                                shoreTrip.addPrice(price);
                              }
                            }

                            // add time
                            if (trip.getTimes() != null && trip.getTimes().getTripTime() != null && trip.getTimes()
                                                                                                        .getTripTime()
                                                                                                        .size() > 0) {
                              for (IntegrationResponse.Excursions.Regions.Region.Ports.Location.TripGroups.TripGroup
                                  .Trips.Trip.Times.TripTime tripTime : trip
                                  .getTimes()
                                  .getTripTime()) {

                                ExternalActivityTimes time = new ExternalActivityTimes();
                                if (tripTime.getCode() != null) {
                                  time.code = String.valueOf(tripTime.getCode());
                                }
                                time.description = tripTime.getDescription();
                                time.duration = tripTime.getDuration();
                                time.startTime = tripTime.getDeparture();
                                shoreTrip.addTimes(time);

                              }
                            }

                            if (trip.getDaysOfWeekMon() != null && trip.getDaysOfWeekMon().equals("true")) {
                              shoreTrip.addDay(DateTimeConstants.MONDAY);
                            }
                            if (trip.getDaysOfWeekTue() != null && trip.getDaysOfWeekTue().equals("true")) {
                              shoreTrip.addDay(DateTimeConstants.TUESDAY);
                            }
                            if (trip.getDaysOfWeekWed() != null && trip.getDaysOfWeekWed().equals("true")) {
                              shoreTrip.addDay(DateTimeConstants.WEDNESDAY);
                            }
                            if (trip.getDaysOfWeekThu() != null && trip.getDaysOfWeekThu().equals("true")) {
                              shoreTrip.addDay(DateTimeConstants.THURSDAY);
                            }
                            if (trip.getDaysOfWeekFri() != null && trip.getDaysOfWeekFri().equals("true")) {
                              shoreTrip.addDay(DateTimeConstants.FRIDAY);
                            }
                            if (trip.getDaysOfWeekSat() != null && trip.getDaysOfWeekSat().equals("true")) {
                              shoreTrip.addDay(DateTimeConstants.SATURDAY);
                            }
                            if (trip.getDaysOfWeekSun() != null && trip.getDaysOfWeekSun().equals("true")) {
                              shoreTrip.addDay(DateTimeConstants.SUNDAY);
                            }
                            shoreTrip.locLat = integrationResponse.getExcursions().getRegions().getRegion().getLat();
                            shoreTrip.locLong = integrationResponse.getExcursions().getRegions().getRegion().getLng();

                            externalActivity.addActivity(shoreTrip);
                            if (shoreTrip.tripCode != null) {
                              CacheMgr.set(APPConstants.CACHE_SHORE_TRIP_PREFIX + shoreTrip.tripCode, shoreTrip);
                            }

                          }

                        }
                      }
                    }
                  }
                }
              }
            }
          }
          return ok(views.html.shoreTrip.shoretripTripsModal.render(externalActivity, timestamp));
        }
        return ok(views.html.common.message.render(view));
      }
      catch (Exception e) {
        e.printStackTrace();
      }
      return ok(views.html.common.message.render(view));
    });
  }

  @With({Authenticated.class, Authorized.class})
  public Result searchShoreTrips(String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Trip       trip       = Trip.find.byId(tripId);
    if (trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {
      List<PoiRS> ports = PoiMgr.findPortWithCode();
      if (ports != null && ports.size() > 0) {
        List<String>        portCodes = new ArrayList();
        Map<String, String> portsMap  = new HashMap<>();
        for (PoiRS poi : ports) {
          portCodes.add(poi.getName());
          portsMap.put(poi.getName(), poi.getCode());
        }
        return ok
            (views.html.shoreTrip.shoretripSearchModal.render(portCodes,
                                                              portsMap,
                                                              trip.tripid,
                                                              Utils.formatDateControl(trip.starttimestamp)
                                                             ));
      }
    }

    BaseView view = new BaseView();
    view.message = "You are not authorized to import a booking for this trip";
    return ok(views.html.common.message.render(view));
  }

  /*
    Importing shore trip and linking to a cruise stop
   */
  @With({Authenticated.class, Authorized.class})
  public Result addShoreTripToItinerary(String shoreTripCode, String tripDetailsId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    DateUtils  du         = new DateUtils();

    ExternalActivity shoreTrip = (ExternalActivity) CacheMgr.get(APPConstants.CACHE_SHORE_TRIP_PREFIX + shoreTripCode);
    if (shoreTrip != null) {
      TripDetail cruiseStop = TripDetail.findByPK(tripDetailsId);
      if (cruiseStop != null) {
        Trip trip = Trip.find.byId(cruiseStop.getTripid());
        if (trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {
          //let's create the activity for the same time as the trip stop
          TripNote tripNote = buildTripNote(trip, sessionMgr, shoreTrip, shoreTripCode);

          if (shoreTrip.times != null) {
            long startTime = 0;
            long endTime   = 0;
            for (ExternalActivityTimes time : shoreTrip.times) {
              try {
                String date    = Utils.formatDate(String.valueOf(cruiseStop.getStarttimestamp())) + " " + time
                    .startTime;
                Date   t       = du.parseDate(date, dateTimeFormat);
                long   newTime = t.getTime();
                if (newTime > cruiseStop.getStarttimestamp()) {
                  if (startTime == 0 || startTime < newTime) {
                    startTime = newTime;
                    if (time.duration > 0) {
                      long durationMS = (long) (time.duration * (60 * 60 * 60 * 1000L));
                      endTime = startTime + durationMS;
                    }
                  }
                }

              }
              catch (Exception e) {

              }
            }
            if (startTime > 0) {
              tripNote.setNoteTimestamp(startTime);
            }
          }
          tripNote.save();
          //let's see if we need to save the photo
          if (shoreTrip.imageUrls != null) {
            saveShoreTripAttachments(shoreTrip, tripNote, sessionMgr);
          }
          BookingNoteController.invalidateCache(trip.tripid);
          flash(SessionConstants.SESSION_PARAM_MSG, tripNote.getName() + " added successfully");

          return redirect(routes.BookingNoteController.getNote(trip.tripid, tripNote.getNoteId(), null, null));
        }
      }
    }


    BaseView view = new BaseView();
    view.message = "Error adding this ShoreTrip excursion to your itinerary. Please try again.";
    return ok(views.html.common.message.render(view));

  }

  private static TripNote buildTripNote(Trip trip,
                                        SessionMgr sessionMgr,
                                        ExternalActivity shoreTrip,
                                        String shoreTripCode) {
    //let's create the activity for the same time as the trip stop
    TripNote tripNote = TripNote.buildTripNote(trip, sessionMgr.getUserId());
    int maxRow = TripNote.tripNoteCount(trip.tripid);
    tripNote.setRank(++maxRow);
    tripNote.setIntro(shoreTrip.description);
    tripNote.setLocLat(shoreTrip.locLat);
    tripNote.setLocLong(shoreTrip.locLong);
    tripNote.setName(shoreTrip.name);
    tripNote.setImportSrc(BookingSrc.ImportSrc.SHORE_TRIPS);
    tripNote.setImportSrcId(shoreTripCode);
    tripNote.setImportTs(System.currentTimeMillis());
    return tripNote;
  }

  private static void saveShoreTripAttachments(ExternalActivity shoreTrip, TripNote tripNote, SessionMgr sessionMgr) {
    Account account;
    if(sessionMgr.getAccount().isPresent()){
      account = sessionMgr.getAccount().get();
    } else {
      account = Account.find.byId(sessionMgr.getAccountId());
    }

    //let's see if we need to save the photo
    if (shoreTrip.imageUrls != null) {
      for (String url : shoreTrip.imageUrls) {
        try {
          String fileName = url.substring(url.lastIndexOf("/") + 1).trim();
          FileImage image = ImageController.downloadAndSaveImage(url,
                                                                 fileName,
                                                                 FileSrc.FileSrcType.IMG_VENDOR_VIRTUOSO,
                                                                 null,
                                                                 account,
                                                                 false);
          if (image != null) {
            TripNoteAttach attach = TripNoteAttach.build(tripNote, account.getLegacyId());
            attach.setStatus(APPConstants.STATUS_ACCEPTED);
            attach.setAttachType(PageAttachType.PHOTO_LINK);
            attach.setFileImage(image);
            //set rank
            int maxRow = TripNoteAttach.maxPageRankByNoteId(tripNote.getNoteId(), attach.getAttachType().getStrVal());
            attach.setRank(++maxRow);
            attach.setAttachUrl(image.getUrl());
            attach.setName(fileName);
            attach.setAttachName(fileName);
            attach.save();
          }
        }
        catch (Exception e) {

        }
      }
    }
  }

  /*
  Importing shore trip directly to an itinerary
 */
  @With({Authenticated.class, Authorized.class})
  public Result importShoreTripToItinerary(String shoreTripCode, String tripId, Long timestamp) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    ExternalActivity shoreTrip = (ExternalActivity) CacheMgr.get(APPConstants.CACHE_SHORE_TRIP_PREFIX + shoreTripCode);
    if (shoreTrip != null) {
      Trip trip = Trip.find.byId(tripId);
      if (trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {
        //let's create the activity for the same time as the trip stop
        TripNote tripNote = buildTripNote(trip, sessionMgr, shoreTrip, shoreTripCode);

        if (timestamp > 0 && timestamp >= trip.starttimestamp) {
          tripNote.setNoteTimestamp(timestamp);
        }
        tripNote.save();


        //let's see if we need to save the photo
        if (shoreTrip.imageUrls != null) {
          saveShoreTripAttachments(shoreTrip, tripNote, sessionMgr);
        }
        BookingNoteController.invalidateCache(trip.tripid);
        flash(SessionConstants.SESSION_PARAM_MSG, tripNote.getName() + " added successfully");
        return redirect(routes.BookingNoteController.getNote(trip.tripid, tripNote.getNoteId(), null, null));
      }
    }

    BaseView view = new BaseView();
    view.message = "Error adding this ShoreTrip excursion to your itinerary. Please try again.";
    return ok(views.html.common.message.render(view));

  }
}
