package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.EnumValue;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.Ebean;

/**
 * Created by surge on 2014-10-16.
 */
@Entity
@Table(name = "consortium_company")
public class ConsortiumCompany
    extends Model {
  public final static int NO_CONSORTIUM = 0;

  public static Finder<ConsortiumCmpyId, ConsortiumCompany> find = new Finder(ConsortiumCompany.class);
  @Version
  public  int              version;
  @Id
  private ConsortiumCmpyId consCmpyId;
  private Long             createdtimestamp;
  private Long             lastupdatedtimestamp;
  private String           createdby;
  private String           modifiedby;

  @Embeddable
  public static class ConsortiumCmpyId {
    @Column(name = "cons_id")
    public int consId;
    @Column(name = "cmpy_id")
    public int cmpyId;

    public int getConsId() {
      return consId;
    }

    public void setConsId(int consId) {
      this.consId = consId;
    }

    public int getCmpyId() {
      return cmpyId;
    }

    public void setCmpyId(int cmpyId) {
      this.cmpyId = cmpyId;
    }

    @Override
    public int hashCode() {
      return consId * cmpyId;
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof ConsortiumCmpyId)) {
        return false;
      }
      ConsortiumCmpyId ok = (ConsortiumCmpyId) o;
      if (!(ok.consId == consId)) {
        return false;
      }
      if (!(ok.cmpyId == cmpyId)) {
        return false;
      }
      return true;
    }
  }

  public static ConsortiumCompany findUniqueById(int consId, int cmpyId) {
    ConsortiumCmpyId ccid = new ConsortiumCmpyId();
    ccid.consId = consId;
    ccid.cmpyId = cmpyId;
    return find.byId(ccid);
  }

  public static ConsortiumCompany buildConsortiumCompany(Integer cons_id, Integer cmpy_id, String userId) {
    ConsortiumCompany cc   = new ConsortiumCompany();
    ConsortiumCmpyId  ccid = new ConsortiumCmpyId();
    ccid.consId = cons_id;
    ccid.cmpyId = cmpy_id;
    cc.setConsCmpyId(ccid);

    cc.setCreatedby(userId);
    cc.setCreatedtimestamp(System.currentTimeMillis());
    cc.setModifiedby(userId);
    cc.setLastupdatedtimestamp(cc.createdtimestamp);

    return cc;
  }

  public static List<ConsortiumCompany> findByConsortiumId(Integer consId) {
    return find.where().eq("cons_id", consId).findList();
  }

  public static List<ConsortiumCompany> findByCmpyId(Integer cmpyId) {
    return find.where().eq("cmpy_id", cmpyId).findList();
  }

  public static List<ConsortiumCompany> findByConsortiumFromList(List<Integer> consIds) {
    return find.where().in("cons_id", consIds).findList();
  }

  /**
   * Retrieves all Consortia based on all the companies user is linked to
   * @param userId
   * @return a List of Integers - consId
   */
  public static List<Integer> findUserConsortiaFromAllCompanies (String userId) {
    com.avaje.ebean.Query<UserCmpyLink> userCmpyLinkQuery = Ebean.createQuery(UserCmpyLink.class)
            .select("cmpy.cmpyid")
            .where()
            .eq("userid", userId)
            .query();

    com.avaje.ebean.Query<Company> companyQuery = Ebean.createQuery(Company.class)
            .select("cmpyId")
            .where()
            .in("cmpyid", userCmpyLinkQuery)
            .query();

    List<ConsortiumCompany> ccList = find.where().in("cmpy_id", companyQuery).findList();

    List<Integer> consortias = new ArrayList<>();
    if (ccList != null && ccList.size() > 0) {
      for (ConsortiumCompany cc : ccList) {
        consortias.add(cc.getConsId());
      }
    }
    return consortias;
  }

  private static final String SQL_SEARCH_CONS_COMPANY_BY_TERM = "SELECT cc.cons_id, cc.cmpy_id, cc.createdtimestamp, " +
      "cc.lastupdatedtimestamp" +
      "    FROM consortium_company cc, company c1, consortium c2" +
      "    where cc.cons_id = c2.cons_id and cc.cmpy_id = c1.cmpy_id and c1.status = 0" +
      "    and (c1.name ilike :keyword or c2.name ilike :keyword) offset :start limit :maxRows";

  public static List<ConsortiumCompany> findByTerm2(String keyword, int start, int maxRows) {
    RawSql rsql = RawSqlBuilder.parse(SQL_SEARCH_CONS_COMPANY_BY_TERM)
        .columnMapping("cc.cons_id", "consCmpyId.consId")
        .columnMapping("cc.cmpy_id", "consCmpyId.cmpyId")
        .columnMapping("cc.createdtimestamp", "createdtimestamp")
        .columnMapping("cc.lastupdatedtimestamp", "lastupdatedtimestamp")
        .create();

    com.avaje.ebean.Query<ConsortiumCompany> query = Ebean.createQuery(ConsortiumCompany.class);
    query.setRawSql(rsql);
    query.setParameter("keyword", keyword);
    query.setParameter("start", start);
    query.setParameter("maxRows", maxRows);

    return query.findList();
  }

  public static List<ConsortiumCompany> findByTerm(String keyword, int consortiumId, int start, int maxRows) {
    com.avaje.ebean.Query<Company> cmpies = Ebean
        .createQuery(Company.class)
        .select("cmpyId")
        .where()
        .ilike("name", keyword)
        .eq("status", 0)
        .query();

    com.avaje.ebean.Query<Consortium> consortia = Ebean
        .createQuery(Consortium.class)
        .select("consId")
        .where()
        .query();

    if (consortiumId == -1) {
      return find
          .where()
          .in("consCmpyId.cmpyId", cmpies)
          .in("consCmpyId.consId", consortia)
          .setFirstRow(start)
          .setMaxRows(maxRows)
          .findList();
    } else {
      return find
          .where()
          .eq("consCmpyId.consId", consortiumId)
          .in("consCmpyId.cmpyId", cmpies)
          .in("consCmpyId.consId", consortia)
          .setFirstRow(start)
          .setMaxRows(maxRows)
          .findList();
    }
  }

  public static int countFilteredConsortia(String keyword, int consortiumId) {
    com.avaje.ebean.Query<Company> cmpies = Ebean
        .createQuery(Company.class)
        .select("cmpyId")
        .where()
        .ilike("name", keyword)
        .eq("status", 0)
        .query();

    com.avaje.ebean.Query<Consortium> consortia = Ebean
        .createQuery(Consortium.class)
        .select("consId")
        .where()
        .query();

    if (consortiumId == -1) {
      return find
          .where()
          .in("consCmpyId.cmpyId", cmpies)
          .in("consCmpyId.consId", consortia)
          .findCount();
    } else {
      return find
          .where()
          .eq("consCmpyId.consId", consortiumId)
          .in("consCmpyId.cmpyId", cmpies)
          .in("consCmpyId.consId", consortia)
          .findCount();
    }
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public ConsortiumCmpyId getConsCmpyId() {
    return consCmpyId;
  }

  public void setConsCmpyId(ConsortiumCmpyId consCmpyId) {
    this.consCmpyId = consCmpyId;
  }

  public int getConsId() {
    return consCmpyId.consId;
  }

  public int getCompId() {
    return consCmpyId.cmpyId;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

}
