package controllers;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.avaje.ebean.Ebean;
import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Subscription;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mapped.common.EmailMgr;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.*;
import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.audit.AuditModuleType;
import com.mapped.publisher.audit.event.AuditCollaboration;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.*;
import com.mapped.publisher.form.user.FormChangeTripInviteAccessLevel;
import com.mapped.publisher.form.user.FormInviteSharedTripUser;
import com.mapped.publisher.form.user.FormTripInviteChangeRequest;
import com.mapped.publisher.parse.classicvacations.CVCredentials;
import com.mapped.publisher.persistence.RecordState;
import com.mapped.publisher.persistence.UserProfileMgr;
import com.mapped.publisher.persistence.UserProfileRS;
import com.mapped.publisher.persistence.billing.BraintreeSunbscription;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.utils.*;
import com.mapped.publisher.view.*;
import com.mapped.publisher.view.billing.*;
import com.mapped.publisher.view.preferences.PreferencesView;
import com.umapped.api.schema.common.RequestMessageJson;
import com.umapped.api.schema.common.ResponseMessageJson;
import com.umapped.api.schema.local.CompanyJson;
import com.umapped.api.schema.types.Operation;
import com.umapped.api.schema.types.PhoneNumber;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.helper.BraintreeGatewayFactory;
import com.umapped.persistence.accountprop.AccountBilling;
import com.umapped.persistence.accountprop.EligiblePlans;
import com.umapped.persistence.accountprop.TripPrefs;
import models.publisher.*;
import org.apache.commons.lang3.time.StopWatch;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-19
 * Time: 8:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserController
    extends Controller {
  @Inject BraintreeGatewayFactory braintreeFactory;

  @Inject
  FormFactory formFactory;

  @With({Authenticated.class, AdminAccess.class})
  public Result usersAdmin() {
    BaseView bv = new BaseView();
    return ok(views.html.user.usersAdmin.render(bv));
  }

  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, AdminAccess.class})
  public Result usersAdminDT() {
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);

    class UserRow {
      public String userId;
      public String name;
      public String email;
      public String contact;
      public String bilPlanName;
      public List<String> bilAddons;

      public Map<String, String> actions;

      public UserRow() {
        actions = new TreeMap<>();
        bilAddons = new ArrayList<>();
      }
    }

    String sortOrder = "userid asc";
    if (dtRequest.columns != null && dtRequest.columns.size() > 0 && dtRequest.order != null && dtRequest.order.size() > 0 && dtRequest.order.get(0).column != null) {
      if (dtRequest.columns.get(dtRequest.order.get(0).column).name != null && !dtRequest.columns.get(dtRequest.order.get(0).column).name.isEmpty()) {
        sortOrder = dtRequest.columns.get(dtRequest.order.get(0).column).name;
        if (dtRequest.order.get(0).dir != null) {
          sortOrder += " " + dtRequest.order.get(0).dir;
        }
      }
    }

    DataTablesView<UserRow> dtv = new DataTablesView<>(dtRequest.draw);
    dtv.recordsTotal    = UserProfile.getRowCount();
    dtv.recordsFiltered = UserProfile.getRowCountFiltered(dtRequest.search.value);
    List<UserProfile> recs = UserProfile.getPage(dtRequest.start, dtRequest.length, dtRequest.search.value, sortOrder);

    for (UserProfile up : recs) {
      UserRow row = new UserRow();
      row.userId  = up.userid;
      row.name    = up.firstname + " " + up.lastname;
      row.email   = up.email;
      row.contact = getContactShort(up);
      row.actions.put("Edit", controllers.routes.UserController.user(up.userid, (String)null, false).url());

      //Get billing plan name
      BillingSettingsForUser bsu = BillingSettingsForUser.findSettingsByUserId(up.userid);
      if(bsu == null) {
        row.bilPlanName = "NOT CONFIGURED";
      } else {
        if(bsu.getPlan() != null) {
          row.bilPlanName = bsu.getPlan().getName();
        } else {
          switch (bsu.getSchedule().getName()) {
            case COMPANY:
              BillingSettingsForCmpy ucbs = BillingSettingsForCmpy.find.byId(bsu.getCmpyid());
              if(ucbs == null || ucbs.getPlan() == null) {
                row.bilPlanName = "PARENT CMPY SETTINGS NOT CONFIGURED";
              } else {
                row.bilPlanName = ucbs.getPlan().getName();
              }
              break;
            case AGENT:
              BillingSettingsForUser uabs = BillingSettingsForUser.findSettingsByUserId(bsu.getAgent());
              if(uabs == null || uabs.getPlan() == null) {
                row.bilPlanName = "PARENT AGENT SETTINGS NOT CONFIGURED";
              } else {
                row.bilPlanName = uabs.getPlan().getName();
              }
              break;
          }
        }
      }

      List<BillingAddonsForUser> addons = BillingAddonsForUser.getAddonsForUser(up.userid);
      for(BillingAddonsForUser a: addons) {
        BillingPlan ap = BillingPlan.find.byId(a.getPk().getPlanId());
        row.bilAddons.add((a.getEnabled()?"+":"-") + ap.getName());
      }

      dtv.data.add(row);
    }

    return  ok(Json.toJson(dtv));
  }

  /**
   * Ugly helper for now, rework later to send data and format on frontend
   * @param up
   * @return
   */
  private static String getContactShort(UserProfile up) {
    StringBuilder sb = new StringBuilder("<address>");

    if (up.phone != null && up.phone.length() > 0) {
      sb.append("<abbr title=\"Phone\">Phone: </abbr>");
      sb.append(up.phone);
      sb.append("<br/>");
    }

    if(up.mobile != null && up.mobile.length() > 0) {
      sb.append("<abbr title=\"Mobile\">Mobile: </abbr>");
      sb.append(up.mobile);
      sb.append("<br/>");
    }

    if(up.fax != null && up.fax.length() > 0) {
      sb.append("<abbr title=\"Fax\">Fax: </abbr>");
      sb.append(up.fax);
      sb.append("<br/>");
    }

    if(up.web != null && up.web.length() > 0) {
      sb.append("<a href=\"");
      sb.append(up.web);
      sb.append("\" target=\"_blank\"><h5>Website</h5></a>");
    }

    if(up.facebook != null && up.facebook.length() > 0) {
      sb.append("<abbr title=\"Facebook\">Facebook: </abbr> ");
      sb.append(up.facebook);
      sb.append("<br/>");
    }

    if(up.twitter != null && up.twitter.length() > 0) {
      sb.append("<abbr title=\"Twitter\">Twitter: </abbr> ");
      sb.append(up.twitter);
      sb.append("<br/>");
    }

    sb.append("</address>");
    return sb.toString();
  }


  private static String getAccessDisplay(int cmpyLinkType, List<String> groupList) {
    StringBuilder sb = new StringBuilder("<h5>");
    if (cmpyLinkType == UserCmpyLink.LinkType.ADMIN.getIdx()) {
      sb.append("Administrator");
    } else if (cmpyLinkType == UserCmpyLink.LinkType.MEMBER.getIdx()) {
      sb.append("Member");
    } else if (cmpyLinkType == UserCmpyLink.LinkType.POWER.getIdx()) {
      sb.append("Power Member");
    }
    sb.append("</h5>");

    if (groupList != null && groupList.size() > 0) {
      sb.append("<ul class=\"inline\">");

      sb.append("<h5> Group Access: </h5>");

      for (String g : groupList) {
        sb.append("<li>");
        sb.append(g);
        sb.append("</li>");
      }
      sb.append("</ul>");
    }

    return sb.toString();
  }


  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result usersDT(String cmpyId, boolean pageMode) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    if (!sessionMgr.getCredentials().isUmappedAdmin() && !sessionMgr.getCredentials().isCmpyAdmin(cmpyId)) {
      BaseView view = new BaseView();
      view.message = "System Error";
      return ok(views.html.common.message.render(view));
    }
    
    JsonNode jsonRequest = request().body().asJson();
    DataTablesForm dtRequest = Json.fromJson(jsonRequest, DataTablesForm.class);

    class UserRow {
      public String userId;
      public String name;
      public String email;
      public String contact;
      public String admin;
      public String access;

      public Map<String, String> actions;

      public UserRow() {
        actions = new TreeMap<>();
      }
    }



    DataTablesView<UserRow> dtv = new DataTablesView<>(dtRequest.draw);
    List<UserProfileRS> recs = null;
    Connection conn = null;
    String keyword = "%"+dtRequest.search.value+"%";
    try {
      conn = DBConnectionMgr.getConnection4Publisher();
      dtv.recordsTotal    = UserProfileMgr.countByTermAndCmpy("%", cmpyId, conn);
      dtv.recordsFiltered = UserProfileMgr.countByTermAndCmpy(keyword, cmpyId, conn);
      recs = UserProfileMgr.findByTermAndCmpyForDT(keyword, cmpyId, dtRequest.start, dtRequest.length, conn);

    }
    catch (Exception e) {
      e.printStackTrace();
      BaseView view = new BaseView();
      view.message = "System Error";
      return ok(views.html.common.message.render(view));
    }
    finally {
      try {
        if (conn != null) {
          conn.close();
        }
      }
      catch (Exception s) {
        s.printStackTrace();
      }
    }

    Company cmpy = null;

    if (cmpyId != null) {
      cmpy = Company.find.byId(cmpyId);
    }


    for (UserProfileRS up : recs) {
      UserRow row = new UserRow();
      row.userId  = up.userid;
      row.name    = up.firstname + " " + up.lastname;
      row.email   = up.email;
      row.contact = getContactShort(up);
      row.actions.put("Edit", controllers.routes.UserController.user(up.userid, cmpyId, pageMode).url());

      if(up.cmpyLinkType == UserCmpyLink.LinkType.ADMIN.getIdx()){
        row.admin = "Yes";
      } else {
        row.admin = "No";
      }

      List<String> groupList = new ArrayList<>();
      if (cmpy != null && up.getLinkId() != null && up.getLinkId().length() > 0) {
        HashMap<String, String> groups = new HashMap<>();
        List<CmpyGroupMember> members = CmpyGroupMember.findByLinkId(up.getLinkId());
        for (CmpyGroupMember member : members) {
          String groupName = groups.get(member.groupid);
          if (groupName == null) {
            CmpyGroup group = CmpyGroup.find.byId(member.groupid);
            groups.put(group.getGroupid(), group.getName());
            groupName = group.getName();
          }
          if (!groupList.contains(groupName)) {
            groupList.add(groupName);
          }
        }
      }

      row.access = getAccessDisplay(up.getCmpyLinkType(), groupList);

      dtv.data.add(row);
    }

    return  ok(Json.toJson(dtv));
  }

  @With({Authenticated.class, Authorized.class})
  public Result users() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    UsersView usersView = new UsersView();
    usersView.users = new ArrayList<>();
    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      usersView.message = msg;
    }

    final DynamicForm form = formFactory.form().bindFromRequest();
    String term = form.get("inTerm");
    String cmpyId = form.get("inCmpyId");
    boolean isCmpyPage = Boolean.parseBoolean(form.get("inCmpyMode"));

    if (cmpyId == null || cmpyId.isEmpty()) {
      //let's try to get the info from the flash session
      cmpyId = flash("inCmpyId");
      isCmpyPage = Boolean.parseBoolean(flash("inCmpyMode"));
    }

    usersView.pageMode = isCmpyPage;

    List<UserProfileRS> users = null;
    Company cmpy = null;

    if (cmpyId != null) {
      cmpy = Company.find.byId(cmpyId);
    }
    if (!isCmpyPage && !SecurityMgr.isUmappedAdmin(sessionMgr)) {
      List<UserCmpyLink> links = UserCmpyLink.findActiveByUserId(sessionMgr.getUserId());
      if (links != null) {
        HashMap<String, String> agencyList = new HashMap<String, String>();
        for (UserCmpyLink u : links) {
          if (u.getCmpy() != null &&
              u.getCmpy().getStatus() == APPConstants.STATUS_ACTIVE &&
              SecurityMgr.isCmpyAdmin(u.getCmpy(), sessionMgr)) {
            agencyList.put(u.getCmpy().getCmpyid(), u.getCmpy().getName());
          }
        }
        usersView.tripAgencyList = agencyList;

      }
    }
    if (cmpy != null) {
      usersView.cmpyName = cmpy.name;
    }
    if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
      usersView.isUMappedAdmin = true;
    }
    else {
      usersView.isUMappedAdmin = false;
    }

    if ((term == null || term.length() == 0) &&
        cmpy != null && isCmpyPage &&
        (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr) ||
         SecurityMgr.isUmappedAdmin(sessionMgr))) {
      usersView.cmpyId = cmpy.cmpyid;
      usersView.cmpyType = cmpy.type;

      Connection conn = null;
      try {
        conn = DBConnectionMgr.getConnection4Publisher();
        users = UserProfileMgr.findByCmpy(cmpyId, conn);
      }
      catch (Exception e) {
        e.printStackTrace();
        BaseView view = new BaseView();
        view.message = "System Error";
        return ok(views.html.common.message.render(view));
      }
      finally {
        try {
          if (conn != null) {
            conn.close();
          }
        }
        catch (Exception s) {
          s.printStackTrace();
        }
      }
    }
    else if (term != null && term.length() > 0) {
      usersView.term = term;
      term = "%" + term + "%";

      if (cmpy != null && SecurityMgr.isCmpyAdmin(cmpy, sessionMgr)) {
        usersView.cmpyId = cmpy.cmpyid;
        Connection conn = null;
        try {
          conn = DBConnectionMgr.getConnection4Publisher();
          users = UserProfileMgr.findByTermAndCmpy(term, cmpyId, conn);
        }
        catch (Exception e) {
          e.printStackTrace();
          BaseView view = new BaseView();
          view.message = "System Error";
          return ok(views.html.common.message.render(view));
        }
        finally {
          try {
            if (conn != null) {
              conn.close();
            }
          }
          catch (Exception s) {
            s.printStackTrace();
          }

        }
      }
      else if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
        Connection conn = null;
        try {
          conn = DBConnectionMgr.getConnection4Publisher();
          if (cmpy != null) {
            users = UserProfileMgr.findByTermAndCmpy(term, cmpyId, conn);
          } else {
            users = UserProfileMgr.findByTerm(term, conn);
          }
        }
        catch (Exception e) {
          e.printStackTrace();
          BaseView view = new BaseView();
          view.message = "System Error";
          return ok(views.html.common.message.render(view));
        }
        finally {
          try {
            if (conn != null) {
              conn.close();
            }
          }
          catch (Exception s) {
            s.printStackTrace();
          }
        }
      }
    }

    if (users != null) {
      ArrayList<UserView> userViews = new ArrayList<>();
      for (UserProfileRS u : users) {
        UserView uv = new UserView();
        uv.userId = u.getUserid();
        uv.firstName = u.getFirstname();
        uv.lastName = u.getLastname();
        uv.email = u.getEmail();
        uv.phone = u.getPhone();
        uv.mobile = u.getMobile();
        uv.fax = u.getFax();
        uv.web = u.getWeb();
        uv.facebook = u.getFacebook();
        uv.twitter = u.getTwitter();
        uv.profilePhoto = u.getProfilephoto();
        uv.profilePhotoUrl = u.getProfilephotourl();

        Timestamp lastLoginTs = AccountAuth.getLastSuccessLoginTs(uv.userId);
        uv.lastLoginTimestamp = lastLoginTs != null ? String.valueOf(lastLoginTs.getTime()) : "";
        if (lastLoginTs == null) {
          uv.lastLoginPrint = "No Logins Yet.";
        }
        else {
          uv.lastLoginPrint = Utils.formatTimestampPrint(lastLoginTs.getTime());
        }

        if (uv.firstName != null) {
          uv.name = uv.firstName + " ";
        }
        else {
          uv.name = "";
        }

        if (u.isCmpyIdOwner == 1 && u.cmpyId.equals(cmpy.cmpyid)) {
          uv.cmpyIdOwner = u.cmpyId;
        }
        else {
          uv.cmpyIdOwner = "";
        }

        uv.cmpyLinkType = u.getCmpyLinkType();

        if (uv.lastName != null) {
          uv.name = uv.name + uv.lastName;
        }

        //if cmpy id != null. get group membership

        if (cmpy != null && u.getLinkId() != null && u.getLinkId().length() > 0) {
          HashMap<String, String> groups = new HashMap<>();
          uv.groups = new ArrayList<>();
          List<CmpyGroupMember> members = CmpyGroupMember.findByLinkId(u.getLinkId());
          for (CmpyGroupMember member : members) {
            String groupName = groups.get(member.groupid);
            if (groupName == null) {
              CmpyGroup group = CmpyGroup.find.byId(member.groupid);
              groups.put(group.getGroupid(), group.getName());
              groupName = group.getName();
            }
            if (!uv.groups.contains(groupName)) {
              uv.groups.add(groupName);
            }
          }
        }

        userViews.add(uv);

      }
      usersView.users = userViews;
      if (usersView != null && userViews.size() == 0) {
        usersView.message = "No results found.";
      }
    }

    return ok(views.html.user.users.render(usersView));

  }


  @With({Authenticated.class, Authorized.class})
  public Result billing(String userId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    if(!sessionMgr.getUserId().equals(userId)) {
      BaseView view = new BaseView();
      view.message = "Unauthorized Access";
      return ok(views.html.common.message.render(view));
    }

    UserBillingView ubv = new UserBillingView();
    ubv.message = flash(SessionConstants.SESSION_PARAM_MSG);

    ubv.up = UserProfile.findByPK(userId);
    ubv.fullName = ubv.up.getFullName();
    ubv.userBilling = BillingSettingsForUser.findSettingsByUserId(userId);
    if(ubv.userBilling == null) {
      BaseView view = new BaseView();
      view.message = "Billing settings are not correctly configured. Please contact support@umapped.com";
      return ok(views.html.common.message.render(view));
    }

    switch (ubv.userBilling.getBillTo().getName()) {
      case USER:
        ubv.plan = ubv.userBilling.getPlan();
        break;
      case COMPANY:
        ubv.cmpyBilling = BillingSettingsForCmpy.find.byId(ubv.userBilling.getCmpyid());
        ubv.billedCmpy = Company.find.byId(ubv.cmpyBilling.getCmpyId());
        ubv.plan = ubv.cmpyBilling.getPlan();
        break;
      case AGENT:
        ubv.agentBilling = BillingSettingsForUser.findSettingsByUserId(ubv.userBilling.getAgent());
        ubv.billedAgent = UserProfile.findByPK(ubv.userBilling.getAgent());
        ubv.plan = ubv.agentBilling.getPlan();
        break;
    }

    if(ubv.plan == null) {
      BaseView view = new BaseView();
      view.message = "Billing settings are mis-configured. Please contact support@umapped.com";
      return ok(views.html.common.message.render(view));
    }

    List<BillingAddonsForUser> addons = BillingAddonsForUser.getAddonsForUser(userId);
    for(BillingAddonsForUser a: addons) {
      BillingPlan ap = BillingPlan.find.byId(a.getPk().planId);
      //If addon is enabled add it to the list or if addon is disabled
      if(a.getEnabled() ||
         (!a.getEnabled() && ap.getEnabled() && ubv.plan.getSchedule().equals(ap.getSchedule()))) {
        ubv.addAddon(ap, a);
      }
    }

    //Availalbe addons not yet added to the user
    for(BillingPlan p: BillingPlan.newAddonsForUser(userId, ubv.plan.getPlanId(), true)){
      if(p.getSchedule() == ubv.plan.getSchedule()) {
        ubv.addAddon(p, BillingAddonsForUser.buildAddonForUser(userId, p.getPlanId(), ""));
      }
    }
    return ok(views.html.user.billing.render(ubv));
  }

  @With({Authenticated.class, Authorized.class})
  public Result preferences(String userId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    Account account = Account.findActiveByLegacyId(sessionMgr.getUserId());
    AccountProp prop = AccountProp.findByPk(account.getUid(), AccountProp.PropertyType.TRIP_PREFERENCE);


    PreferencesView view = new PreferencesView();
    view.loggedInUserId = sessionMgr.getUserId();
    view.message = msg;
    if (prop != null && prop.tripPreferences != null && prop.tripPreferences.hasPreferences()){
      view.printFriendlyPdf = prop.tripPreferences.isPrintPDF();
      view.pdfTemplate = prop.tripPreferences.getTripPdfTmplt();
      view.display24Hr = !prop.tripPreferences.isIs12Hr();
    }


    //system features
    List<SysFeature> features = SysFeature.publicEnabled();
    if (features != null) {
      for (SysFeature feature: features) {
        List<SysFeatureCapability> capabilities = SysFeatureCapability.findByFeature(feature.getPk());
        if (capabilities != null) {
          for (SysFeatureCapability capability: capabilities) {
            SysCapability sysCapability = SysCapability.find.byId(capability.getPk().getCapPk());
            if (sysCapability != null && sysCapability.getEnabled() && sysCapability.getUserControlled()) {
              PreferencesView.CapabilityPrefs capabilityPrefs = view.new CapabilityPrefs();
              capabilityPrefs.capabilityPK = sysCapability.getPk();
              capabilityPrefs.name = sysCapability.getSummary();
              capabilityPrefs.description = sysCapability.getDescription();
              UserCapability.UserCapabilityID userCapabilityID =  new UserCapability.UserCapabilityID();
              userCapabilityID.setCapPk(sysCapability.getPk());
              userCapabilityID.setUserId(sessionMgr.getUserId());
              UserCapability userCapability = UserCapability.find.byId(userCapabilityID);
              if (userCapability != null) {
                capabilityPrefs.enabled = userCapability.getEnabled();
              } else {
                if (feature.getDescription() != null && feature.getDescription().toLowerCase().contains("default: false")) {
                  capabilityPrefs.enabled = false; //default to false;
                } else {
                  capabilityPrefs.enabled = true; //default to true;
                }
              }
              view.capabilities.add(capabilityPrefs);

            }
          }

        }
      }
    }
    //add flight notification capability if it is available in a plan
    List<SysCapability> capsFromPlan = SysCapability.getUserPlanCapabilities(userId);
    for (SysCapability s: capsFromPlan) {
      if (s.getCapability()==Capability.FLIGHT_NOTIFICATIONS && s.getEnabled()) {
        //let's allow the user set the flight notification capability
        UserCapability.UserCapabilityID id = new UserCapability.UserCapabilityID();
        id.setCapPk(s.getPk());
        id.setUserId(sessionMgr.getCredentials().getUserId());
        PreferencesView.CapabilityPrefs capabilityPrefs = view.new CapabilityPrefs();
        capabilityPrefs.capabilityPK = s.getPk();
        capabilityPrefs.name = s.getSummary();
        capabilityPrefs.description = s.getDescription();

        UserCapability.UserCapabilityID userCapabilityID =  new UserCapability.UserCapabilityID();
        userCapabilityID.setCapPk(s.getPk());
        userCapabilityID.setUserId(sessionMgr.getUserId());
        UserCapability userCapability = UserCapability.find.byId(userCapabilityID);
        if (userCapability != null) {
          capabilityPrefs.enabled = userCapability.getEnabled();
        } else {
          capabilityPrefs.enabled = true;
        }
        view.capabilities.add(capabilityPrefs);
        break;
      }
    }

    return ok(views.html.user.preferences.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result savePreferences(String userId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    try {

      final DynamicForm form = formFactory.form().bindFromRequest();

      TripPrefs.TRIP_PDF_TMPLT pdfTmplt = TripPrefs.TRIP_PDF_TMPLT.valueOf(form.get("inPdfTemplate"));
      boolean isPrintFriendly = Boolean.parseBoolean(form.get("inIsPrintFriendly"));
      boolean display24Hour = Boolean.parseBoolean(form.get("inDisplay24Hr"));


      Account account = Account.findActiveByLegacyId(sessionMgr.getUserId());
      if (account != null) {
        AccountProp tripPrefs = AccountProp.findByPk(account.getUid(), AccountProp.PropertyType.TRIP_PREFERENCE);
        if (tripPrefs == null) {
          tripPrefs = AccountProp.build(account.getUid(), AccountProp.PropertyType.TRIP_PREFERENCE, account.getUid());
        }
        tripPrefs.tripPreferences.setPrintPDF(isPrintFriendly);
        tripPrefs.tripPreferences.setIs12Hr(!display24Hour);
        tripPrefs.tripPreferences.setTripPdfTmplt(pdfTmplt);
        tripPrefs.setModifiedBy(account.getUid());
        tripPrefs.setModifiedTs(Timestamp.from(Instant.now()));
        tripPrefs.prePersist();
        tripPrefs.save();
        CacheMgr.set(APPConstants.CACHE_TRIP_PREFS + account.getUid(), null);


        //handle the system capabilities
        //system features
        List<SysFeature> features = SysFeature.publicEnabled();
        boolean featuresChange = false;
        if (features != null) {
          for (SysFeature feature: features) {
            List<SysFeatureCapability> capabilities = SysFeatureCapability.findByFeature(feature.getPk());
            if (capabilities != null) {
              for (SysFeatureCapability capability: capabilities) {
                SysCapability sysCapability = SysCapability.find.byId(capability.getPk().getCapPk());
                if (sysCapability != null && sysCapability.getEnabled()) {
                  boolean updateRec = false;
                  boolean enabled = Boolean.parseBoolean(form.get("inCapability_" + sysCapability.getPk()));
                  UserCapability.UserCapabilityID userCapabilityID =  new UserCapability.UserCapabilityID();
                  userCapabilityID.setCapPk(sysCapability.getPk());
                  userCapabilityID.setUserId(sessionMgr.getUserId());
                  UserCapability userCapability = UserCapability.find.byId(userCapabilityID);
                  if (userCapability == null) {
                    userCapability = new UserCapability();
                    userCapability.setPk(userCapabilityID);
                    userCapability.setCreatedBy(sessionMgr.getUserId());
                    userCapability.setCreatedTs(System.currentTimeMillis());
                    updateRec = true;
                  } else if (userCapability.getEnabled().booleanValue() != enabled) {
                    updateRec = true;
                  }
                  if (updateRec) {
                    userCapability.setModifiedBy(sessionMgr.getUserId());
                    userCapability.setModifiedTs(System.currentTimeMillis());
                    userCapability.setEnabled(enabled);
                    userCapability.save();
                    if (!featuresChange) {
                      featuresChange = true;
                    }
                  }
                }
              }
            }
          }
        }
        //add flight notification capability if it is available in a plan for the user
        List<SysCapability> capsFromPlan = SysCapability.getUserPlanCapabilities(userId);
        for (SysCapability s: capsFromPlan) {
          if (s.getCapability()==Capability.FLIGHT_NOTIFICATIONS && s.getEnabled()) {
            boolean updateRec = false;
            boolean enabled = Boolean.parseBoolean(form.get("inCapability_" + s.getPk()));
            UserCapability.UserCapabilityID userCapabilityID =  new UserCapability.UserCapabilityID();
            userCapabilityID.setCapPk(s.getPk());
            userCapabilityID.setUserId(sessionMgr.getUserId());
            UserCapability userCapability = UserCapability.find.byId(userCapabilityID);
            if (userCapability == null) {
              userCapability = new UserCapability();
              userCapability.setPk(userCapabilityID);
              userCapability.setCreatedBy(sessionMgr.getUserId());
              userCapability.setCreatedTs(System.currentTimeMillis());
              updateRec = true;
            } else if (userCapability.getEnabled().booleanValue() != enabled) {
              updateRec = true;
            }
            if (updateRec) {
              userCapability.setModifiedBy(sessionMgr.getUserId());
              userCapability.setModifiedTs(System.currentTimeMillis());
              userCapability.setEnabled(enabled);
              userCapability.save();
              if (!featuresChange) {
                featuresChange = true;
              }
            }
            break;
          }
        }

        if (featuresChange) {
          Credentials cred = sessionMgr.getCredentials();
          cred.setCapabilities(SecurityMgr.getCapabilities(sessionMgr.getUserId()));
          cred.updateCache();
        }


        CacheMgr.set(APPConstants.CACHE_ACCOUNT_PREFS + account.getUid(), null);
        CacheMgr.set(APPConstants.CACHE_PREFS_CHECKED + account.getUid(), new Boolean(true));

        flash(SessionConstants.SESSION_PARAM_MSG, "Preferences saved successfully");
      }
    } catch (Exception e) {
      Log.log(LogLevel.ERROR,"UserController:savePreferences",e);
      flash(SessionConstants.SESSION_PARAM_MSG, "Error saving preferences");

    }
    return redirect(routes.UserController.preferences(userId));
  }

  @With({Authenticated.class, Authorized.class})
  public Result profile(String userId) {

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    if(!sessionMgr.getUserId().equals(userId) && !SecurityMgr.isUmappedAdmin(sessionMgr)) {
      BaseView view = new BaseView();
      view.message = "Unauthorized Access";
      return ok(views.html.common.message.render(view));
    }

    UserProfileView userView = new UserProfileView();
    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      userView.message = msg;
    }

    userView.up = UserProfile.find.byId(userId);
    List<UserCmpyLink> cmpyLinks = UserCmpyLink.findActiveByUserId(userId);
    if (cmpyLinks.size() >= 1) {
      userView.cmpy = cmpyLinks.get(0).getCmpy();
      userView.cmpyLinkType = cmpyLinks.get(0).getLinktype();
    }
    else {
      BaseView view = new BaseView();
      view.message = "User profile error. Please contact support@umapped.com";
      return ok(views.html.common.message.render(view));
    }

    userView.isOwner = true;

    List<EmailToken> tkns = EmailToken.getCompanyActiveTokens(userView.cmpy.getCmpyid());
    if (userView.cmpy.allowapi && tkns != null && tkns.size() == 1) {
      EmailToken tkn = tkns.get(0);
      if (ConfigMgr.getInstance().isProd()) {
        userView.cmpyParseEmailAddr = tkn.getToken() + "@api.umapped.com";
      }
      else {
        userView.cmpyParseEmailAddr = tkn.getToken() + "@testapi.umapped.com";
      }

    }

    userView.memberSince = Utils.formatDatePrint(userView.up.getCreatedtimestamp());
    Long lastSuccessLoginTs = getLastLoginTime(sessionMgr, userId);
    if (lastSuccessLoginTs == null) {
      userView.lastLoginOn = "No Logins Yet.";
    }
    else {
      userView.lastLoginOn = Utils.formatDateTimePrint(lastSuccessLoginTs);
    }


    /* Booking API User Links */
    List<ApiCmpyTokenView>  apiCmpyTokenViews = ApiBookingController.getApiCmpyTokens(userView.cmpy.getCmpyid());
    if (apiCmpyTokenViews != null && !apiCmpyTokenViews.isEmpty()) {
      userView.apiCmpyTokens.addAll(apiCmpyTokenViews);
    }

    if (!userView.apiCmpyTokens.isEmpty()) {
      userView.apiUserLinks = ApiBookingController.getApiUserLink(userView.up.getUserid());
    }

    //check if user is logged in to classic
    CVCredentials credentials = (CVCredentials) CacheMgr.get(APPConstants.CACHE_CLASSIC_VACATIONS_AUTH_PREFIX + userView.up.getUserid());
    if (credentials != null) {
      userView.isLoggedinClassicVacations = true;
    }

    //check for new fields in account prop
    Account agent = Account.findActiveByLegacyId(userId);
    if (agent != null) {
      AccountProp prop = AccountProp.findByPk(agent.getUid(), AccountProp.PropertyType.CONTACT);
      if (prop != null && prop.accountContact != null && prop.accountContact.phones != null) {
        PhoneNumber ext = prop.accountContact.phones.get(PhoneNumber.PhoneType.EXT);
        if (ext != null && ext.getNumber() != null && !ext.getNumber().isEmpty()) {
          userView.extension = ext.getNumber();
        }
      }
    }

    userView.fullName = userView.up.getFullName();
    return ok(views.html.user.profile.render(userView));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result parseUser(String userId, String cmpyId, Boolean cmpyMode) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Credentials cred = sessionMgr.getCredentials();

    UserParseView userParseView = new UserParseView();
    userParseView.pageMode = (cmpyMode == null)?false:cmpyMode;
    userParseView.isUMappedAdmin = SecurityMgr.isUmappedAdmin(sessionMgr);
    Company cmpy = null;
    if (cmpyId != null && cmpyId.length() > 0) {
      cmpy = Company.find.byId(cmpyId);
      if (cmpy != null && (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr) || SecurityMgr.isUmappedAdmin(sessionMgr))) {
        userParseView.cmpyId = cmpy.cmpyid;
        userParseView.cmpyName = cmpy.name;
        if (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr)) {
          userParseView.isCmpyAdmin = true;
        }
        else if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
          userParseView.isUMappedAdmin = true;
        }
      }
      else {
        BaseView view = new BaseView();
        view.message = "Unauthorized Access";
        return ok(views.html.common.message.render(view));
      }
    }
    else if (!SecurityMgr.isUmappedAdmin(sessionMgr)) {
      if (userId != null && !userId.equals(sessionMgr.getUserId())) {
        BaseView view = new BaseView();
        view.message = "Unauthorized Access";
        return ok(views.html.common.message.render(view));
      }
    }
    if (userId != null) {
      UserProfile u = UserProfile.find.byId(userId);
      userParseView.userId = u.getUserid();
    }

    return ok(views.html.user.userParse.render(userParseView));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result parseUserForm() {
    Form<UserParseForm> userForm = formFactory.form(UserParseForm.class);
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Credentials cred = sessionMgr.getCredentials();
    UserParseForm userParseForm = userForm.bindFromRequest().get();

    UserView userView = new UserView();
    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      userView.message = msg;
    }

    userView.pageMode = (userParseForm.getInCmpyMode() == null)?false:userParseForm.getInCmpyMode();
    userView.isUMappedAdmin = SecurityMgr.isUmappedAdmin(sessionMgr);
    Company cmpy = null;
    String cmpyId = userParseForm.getInCmpyId();
    String userId = null;
    if (cmpyId != null && cmpyId.length() > 0) {
      cmpy = Company.find.byId(cmpyId);
      if (cmpy != null && (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr) || SecurityMgr.isUmappedAdmin(sessionMgr))) {
        userView.cmpyId = cmpy.cmpyid;
        userView.cmpyName = cmpy.name;
        if (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr)) {
          userView.isCmpyAdmin = true;
        }
        else if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
          userView.isUMappedAdmin = true;
        }
      }
      else {
        BaseView view = new BaseView();
        view.message = "Unauthorized Access";
        return ok(views.html.common.message.render(view));
      }
    }
    else if (!SecurityMgr.isUmappedAdmin(sessionMgr)) {
      if (userId != null && !userId.equals(sessionMgr.getUserId())) {
        BaseView view = new BaseView();
        view.message = "Unauthorized Access";
        return ok(views.html.common.message.render(view));
      }
    }

    /* BILLING */
    if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
        BillingInfoView biv = new BillingInfoView((BillingSettingsForUser)null);
        biv.availableEntities = BillingEntity.find.all();
        biv.availableSchedules = BillingSchedule.getSchedulesForUser();
        biv.availablePlans = BillingPlan.plansPerSchedule(biv.availableSchedules.get(0).getName());
        userView.billing = biv;
    }
    /* END OF BILLING */

    String str = "";
    String emailParse = userParseForm.getInUserEmail();
    BufferedReader reader = new BufferedReader(
            new StringReader(emailParse));
    try {
      while ((str = reader.readLine()) != null) {
        if (str.length() > 0) {
          if (!emailParse.contains("First Name") && emailParse.contains("Name")) {
            if (str.startsWith("Name")) {
              if(str.contains(",")) {
                if(str.contains(":") && str.contains(",") && str.indexOf(":") + 1 <= str.lastIndexOf(",")) {
                  userView.firstName = str.trim().substring(str.lastIndexOf(",") + 1, str.length()).trim();
                  userView.lastName = str.trim().substring(str.lastIndexOf(":") + 1, str.lastIndexOf(",")).trim();
                }
              } else {
                if(str.contains(":") && str.indexOf(":") + 1 <= str.lastIndexOf(" ")) {
                  userView.firstName = str.trim().substring(str.indexOf(":") + 1, str.lastIndexOf(" ")).trim();
                  userView.lastName = str.trim().substring(str.lastIndexOf(" ") + 1, str.length()).trim();
                }
              }
            }
            else if(str.startsWith("From")) {
              if(str.contains("<") && str.contains(">")) {
                userView.email = str.trim().substring(str.indexOf("<") + 1, str.lastIndexOf(">")).trim();
              }
            }
          } else if (emailParse.contains("First Name")) {
            if (str.startsWith("First Name")) {
              if(str.contains(":") && str.indexOf(":") + 1 <= str.length()) {
                userView.firstName = str.trim().substring(str.indexOf(":") + 1, str.length()).trim();
              }
            } else if (str.startsWith("Last Name")) {
              if(str.contains(":") && str.indexOf(":") + 1 <= str.length()) {
                userView.lastName = str.trim().substring(str.indexOf(":") + 1, str.length()).trim();
              }
            } else if(str.startsWith("Email")) {
              if(str.contains(":") && str.indexOf(":") + 1 <= str.length()) {
                userView.email = str.trim().substring(str.indexOf(":") + 1, str.length()).trim();
              }
            }
          } else {
            if(str.startsWith("From")) {
              if(str.contains(",")) {
                if(str.contains(":") && str.indexOf(":") + 1 <= str.lastIndexOf("<")) {
                  String[] name = str.trim().substring(str.indexOf(":") + 1, str.lastIndexOf("<")).trim().split(",");
                  userView.firstName = name[1];
                  userView.lastName = name[0];
                }
              } else {
                if(str.contains(":") && str.indexOf(":") + 1 <= str.lastIndexOf("<")) {
                  String[] name = str.trim().substring(str.indexOf(":") + 1, str.lastIndexOf("<")).trim().split(" ");
                  userView.firstName = name[0];
                  userView.lastName = name[1];
                }
              }
              if(str.contains("<") && str.contains(">")) {
                userView.email = str.trim().substring(str.indexOf("<") + 1, str.lastIndexOf(">")).trim();
              }
            }
          }
        }
      }
    } catch(IOException e) {
      e.printStackTrace();
    }

    int cmpy_Id = cmpy.getCmpyId();
    ConsortiumCompany cc = null;
    Consortium consortium = null;
    if(ConsortiumCompany.findByCmpyId(cmpy_Id).size() > 0) {
      cc = ConsortiumCompany.findByCmpyId(cmpy_Id).get(0);
      consortium = Consortium.find.byId(cc.getConsId());
    }

    for(int i = 0; i < userView.billing.availableEntities.size(); i++) {
      if(userView.billing.availableEntities.get(i).getName().name().equals("USER")) {
        userView.billing.defaultEntity = userView.billing.availableEntities.get(i);
        break;
      }
    }

    LocalDateTime currentTime = LocalDateTime.now();

    if(consortium != null) {
      if (cmpy.getName().equals("Vision Travel Solutions") || cmpy.getName().equals("Groupe Ideal, a division of Vision") || cmpy.getName().equals("New Wave Travel")
              || emailParse.toUpperCase().contains("VISION TRAVEL SOLUTIONS") || emailParse.toUpperCase().contains("GROUPE IDEAL, A DIVISION OF VISION") || emailParse.toUpperCase().contains("NEW WAVE TRAVEL")) {
        for (int i = 0; i < userView.billing.availableSchedules.size(); i++) {
          if (userView.billing.availableSchedules.get(i).getLongName().equals("Per 1 Year")) {
            userView.billing.defaultSchedule = userView.billing.availableSchedules.get(i);
            break;
          }
        }

        userView.billing.defaultStartTs = currentTime.toLocalDate().format(ISO_LOCAL_DATE);
        userView.billing.availablePlans = BillingPlan.plansPerSchedule(userView.billing.defaultSchedule.getName());

        for(int i = 0; i < userView.billing.availablePlans.size(); i++) {
          if(userView.billing.availablePlans.get(i).getName().equals("Vision Travel Pro Plan")) {
            userView.billing.defaultPlan = userView.billing.availablePlans.get(i);
            break;
          }
        }
      } else if(cmpy.getName().equals("Montrose Travel") || emailParse.toUpperCase().contains("MONTROSE TRAVEL")) {
        for (int i = 0; i < userView.billing.availableSchedules.size(); i++) {
          if (userView.billing.availableSchedules.get(i).getLongName().equals("Per 1 Month")) {
            userView.billing.defaultSchedule = userView.billing.availableSchedules.get(i);
            break;
          }
        }

        userView.billing.defaultStartTs = currentTime.toLocalDate().format(ISO_LOCAL_DATE);
        userView.billing.defaultCutoffTs = currentTime.toLocalDate().plusDays(30).format(ISO_LOCAL_DATE);
        userView.billing.availablePlans = BillingPlan.plansPerSchedule(userView.billing.defaultSchedule.getName());

        for(int i = 0; i < userView.billing.availablePlans.size(); i++) {
          if(userView.billing.availablePlans.get(i).getName().equals("MTravel 30 Day Trial")) {
            userView.billing.defaultPlan = userView.billing.availablePlans.get(i);
            break;
          }
        }
      } else if (cmpy.getName().equals("Altour") || cmpy.getName().equals("The Travel Authority")
              || emailParse.toUpperCase().contains("ALTOUR") || emailParse.toUpperCase().contains("THE TRAVEL AUTHORITY")){
        for (int i = 0; i < userView.billing.availableSchedules.size(); i++) {
          if (userView.billing.availableSchedules.get(i).getLongName().equals("Per 1 Month")) {
            userView.billing.defaultSchedule = userView.billing.availableSchedules.get(i);
            break;
          }
        }

        userView.billing.defaultStartTs = currentTime.toLocalDate().format(ISO_LOCAL_DATE);
        userView.billing.availablePlans = BillingPlan.plansPerSchedule(userView.billing.defaultSchedule.getName());

        for(int i = 0; i < userView.billing.availablePlans.size(); i++) {
          if(userView.billing.availablePlans.get(i).getName().equals("Altour Pro")) {
            userView.billing.defaultPlan = userView.billing.availablePlans.get(i);
            break;
          }
        }
      } else if(cmpy.getName().equals("Direct Travel") || emailParse.toUpperCase().contains("DIRECT TRAVEL")) {
        for (int i = 0; i < userView.billing.availableSchedules.size(); i++) {
          if (userView.billing.availableSchedules.get(i).getLongName().equals("Per 1 Year")) {
            userView.billing.defaultSchedule = userView.billing.availableSchedules.get(i);
            break;
          }
        }

        userView.billing.defaultStartTs = currentTime.toLocalDate().format(ISO_LOCAL_DATE);
        userView.billing.availablePlans = BillingPlan.plansPerSchedule(userView.billing.defaultSchedule.getName());

        for(int i = 0; i < userView.billing.availablePlans.size(); i++) {
          if(userView.billing.availablePlans.get(i).getName().equals("Direct Travel Pro Plan")) {
            userView.billing.defaultPlan = userView.billing.availablePlans.get(i);
            break;
          }
        }
      } else {
        for (int i = 0; i < userView.billing.availableSchedules.size(); i++) {
          if (userView.billing.availableSchedules.get(i).getLongName().equals("Trial")) {
            userView.billing.defaultSchedule = userView.billing.availableSchedules.get(i);
            break;
          }
        }

        userView.billing.availablePlans = BillingPlan.plansPerSchedule(userView.billing.defaultSchedule.getName());

        for(int i = 0; i < userView.billing.availablePlans.size(); i++) {
          if(emailParse.toUpperCase().contains("@TPI.CA") && userView.billing.availablePlans.get(i).getName().equals("TPI 30 Day Trial")) {
            userView.billing.defaultPlan = userView.billing.availablePlans.get(i);
            userView.billing.defaultCutoffTs = currentTime.toLocalDate().plusDays(31).format(ISO_LOCAL_DATE);
          }
          else if((consortium.getName().equals("Virtuoso") || emailParse.toUpperCase().contains("VIRTUOSO")) && userView.billing.availablePlans.get(i).getName().equals("30 Day Virtuoso Trial")) {
            userView.billing.defaultPlan = userView.billing.availablePlans.get(i);
            userView.billing.defaultCutoffTs = currentTime.toLocalDate().plusDays(31).format(ISO_LOCAL_DATE);
            break;
          } else if((consortium.getName().equals("Ensemble") || emailParse.toUpperCase().contains("ENSEMBLE")) && userView.billing.availablePlans.get(i).getName().equals("30 Day Ensemble Trial")) {
            userView.billing.defaultPlan = userView.billing.availablePlans.get(i);
            userView.billing.defaultCutoffTs = currentTime.toLocalDate().plusDays(31).format(ISO_LOCAL_DATE);
            break;
          } else if((consortium.getName().equals("Travel Leaders Network") || consortium.getName().equals("Vacation.com") || emailParse.toUpperCase().contains("TRAVEL LEADERS NETWORK") || emailParse.toUpperCase().contains("VACATION.COM") || emailParse.toUpperCase().contains("TRAVEL LEADERS")) && userView.billing.availablePlans.get(i).getName().equals("30 Day Travel Leaders Trial")) {
            userView.billing.defaultPlan = userView.billing.availablePlans.get(i);
            userView.billing.defaultCutoffTs = currentTime.toLocalDate().plusDays(31).format(ISO_LOCAL_DATE);
            break;
          } else if((cmpy.getName().equals("Tzell Travel") || emailParse.toUpperCase().contains("TZELL TRAVEL")) && userView.billing.availablePlans.get(i).getName().equals("Tzell 30 Day Trial")) {
            userView.billing.defaultPlan = userView.billing.availablePlans.get(i);
            userView.billing.defaultCutoffTs = currentTime.toLocalDate().plusDays(31).format(ISO_LOCAL_DATE);
            break;
          } else if((cmpy.getName().equals("Protravel International") || emailParse.toUpperCase().contains("PROTRAVEL INTERNATIONAL")) && userView.billing.availablePlans.get(i).getName().equals("Protravel 30 Day Trial")) {
            userView.billing.defaultPlan = userView.billing.availablePlans.get(i);
            userView.billing.defaultCutoffTs = currentTime.toLocalDate().plusDays(31).format(ISO_LOCAL_DATE);
            break;
          } else if((cmpy.getName().equals("Travel Professionals International") || emailParse.toUpperCase().contains("TRAVEL PROFESSIONALS INTERNATIONAL")) && userView.billing.availablePlans.get(i).getName().equals("TPI 30 Day Trial")) {
            userView.billing.defaultPlan = userView.billing.availablePlans.get(i);
            userView.billing.defaultCutoffTs = currentTime.toLocalDate().plusDays(31).format(ISO_LOCAL_DATE);
            break;
          }
        }

        if(userView.billing.defaultPlan == null) {
          userView.billing.defaultPlan = userView.billing.availablePlans.get(0);
          userView.billing.defaultCutoffTs = currentTime.toLocalDate().plusDays(15).format(ISO_LOCAL_DATE);
        }
      }
    } else {
      for(int i = 0; i < userView.billing.availableSchedules.size(); i++) {
        if(userView.billing.availableSchedules.get(i).getLongName().equals("Trial")) {
          userView.billing.defaultSchedule = userView.billing.availableSchedules.get(i);
          break;
        }
      }

      userView.billing.availablePlans = BillingPlan.plansPerSchedule(userView.billing.defaultSchedule.getName());

      if(emailParse.toUpperCase().contains("YTP AGENT SIGN UP") || emailParse.toUpperCase().contains("J.MAK AGENT SIGN UP")) {
        userView.billing.defaultCutoffTs = currentTime.toLocalDate().plusDays(31).format(ISO_LOCAL_DATE);

        for(int i = 0; i < userView.billing.availablePlans.size(); i++) {
          if(userView.billing.availablePlans.get(i).getName().equals("30 Day Preferred Trial")) {
            userView.billing.defaultPlan = userView.billing.availablePlans.get(i);
            break;
          }
        }
      } else {
        userView.billing.defaultCutoffTs = currentTime.toLocalDate().plusDays(15).format(ISO_LOCAL_DATE);

        for(int i = 0; i < userView.billing.availablePlans.size(); i++) {
          if(userView.billing.availablePlans.get(i).getName().equals("14-Day Trial")) {
            userView.billing.defaultPlan = userView.billing.availablePlans.get(i);
            break;
          }
        }
      }
      /*for(int i = 0; i < userView.billing.availableEntities.size(); i++) {
        if(userView.billing.availableEntities.get(i).getName().equals("COMPANY")) {
          userView.billing.defaultEntity = userView.billing.availableEntities.get(i);
          break;
        }
      }
      for(int i = 0; i < userView.billing.availableSchedules.size(); i++) {
        if(userView.billing.availableSchedules.get(i).getName().equals("Company Plan")) {
          userView.billing.defaultSchedule = userView.billing.availableSchedules.get(i);
          break;
        }
      }

      userView.billing.billableCmpyName = cmpy.getName();*/
    }

    if (userView.email != null) {
      userView.userId = RegistrationController.getUserId(userView.email);
      if(userView.userId.length() > 15) {
        userView.userId = userView.userId.substring(0, 16);
      }
    }

    if(UserCmpyLink.findActiveByCmpyId(cmpyId).size() == 0) {
      userView.userType = 1;
    } else {
      userView.userType = 3;
    }

    return ok(views.html.user.user.render(userView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result user(String userId, String cmpyId, Boolean cmpyMode) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Credentials cred = sessionMgr.getCredentials();

    UserView userView = new UserView();
    String msg = flash(SessionConstants.SESSION_PARAM_MSG);
    if (msg != null) {
      userView.message = msg;
    }

    userView.pageMode = (cmpyMode == null)?false:cmpyMode;
    userView.isUMappedAdmin = SecurityMgr.isUmappedAdmin(sessionMgr);
    Company cmpy = null;
    if (cmpyId != null && cmpyId.length() > 0) {
      cmpy = Company.find.byId(cmpyId);
      if (cmpy != null && (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr) || SecurityMgr.isUmappedAdmin(sessionMgr))) {
        userView.cmpyId = cmpy.cmpyid;
        userView.cmpyName = cmpy.name;
        if (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr)) {
          userView.isCmpyAdmin = true;
        }
        else if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
          userView.isUMappedAdmin = true;
        }
      }
      else {
        BaseView view = new BaseView();
        view.message = "Unauthorized Access";
        return ok(views.html.common.message.render(view));
      }
    }
    else if (!SecurityMgr.isUmappedAdmin(sessionMgr)) {
      if (userId != null && !userId.equals(sessionMgr.getUserId())) {
        BaseView view = new BaseView();
        view.message = "Unauthorized Access";
        return ok(views.html.common.message.render(view));
      }
    }

    if (userId != null) {
      UserProfile u = UserProfile.find.byId(userId);
      List<UserCmpyLink> cmpyLink = null;
      if (cmpyId != null && cmpyId.length() > 0) {
        cmpyLink = UserCmpyLink.findActiveByUserIdCmpyId(userId, cmpyId);
        if (u == null || cmpyLink == null || cmpyLink.size() == 0) {
          BaseView view = new BaseView();
          view.message = "Unauthorized Access";
          return ok(views.html.common.message.render(view));
        }
        else if (cmpyLink.get(0).getIsowner() == 0 &&
                 !SecurityMgr.isUmappedAdmin(sessionMgr)) {  //profile not created by
          // this cmpy - so no acccess unless umapped admin.
          BaseView view = new BaseView();
          view.message = "Unauthorized Access";
          return ok(views.html.common.message.render(view));
        }
      }
      else if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
        //get list of cmpies this user belongs to
        userView.cmpyLinks = new ArrayList<>();
        List<UserCmpyLink> userCmpyLinks = UserCmpyLink.findActiveByUserId(userId);
        if (userCmpyLinks != null && userCmpyLinks.size() > 0) {
          for (UserCmpyLink userLink : userCmpyLinks) {
            UserCmpyLinkView linkView = new UserCmpyLinkView();
            linkView.cmpyId = userLink.getCmpy().getCmpyid();
            linkView.cmpyName = userLink.getCmpy().getName();
            linkView.UserCmpyLinkId = userLink.getPk();
            linkView.linkTypeId = userLink.getLinktype().getIdx();
            if (linkView.linkTypeId == UserCmpyLink.LinkType.ADMIN.getIdx()) {
              linkView.linkType = "Administrator";
            } else   if (linkView.linkTypeId == UserCmpyLink.LinkType.POWER.getIdx()) {
              linkView.linkType = "Power Member";
            } else {
              linkView.linkType = "Member";
            }
            linkView.userId = userId;

            userView.cmpyLinks.add(linkView);
          }
        }
      }

      if (u.getUserid().equals(sessionMgr.getUserId())) {
        userView.isOwner = true;

        if (cred.hasCompany()) {
          Company c = cred.getCompany();
          List<EmailToken> tkns = EmailToken.getCompanyActiveTokens(c.getCmpyid());
          if (c.allowapi && tkns != null &&tkns.size() == 1) {
            EmailToken tkn = tkns.get(0);
            String email;
            if (ConfigMgr.getInstance().isProd()) {
              email = tkn.getToken() + "@api.umapped.com";
            }
            else {
              email = tkn.getToken() + "@testapi.umapped.com";
            }
            if (email != null) {
              if (userView.cmpyApis == null) {
                userView.cmpyApis = new HashMap<>();
              }
              userView.cmpyApis.put(c.name, email);
            }
          }
        }
      }
      else {
        userView.isOwner = false;
      }

      userView.userId = u.getUserid();
      userView.firstName = u.getFirstname();
      userView.lastName = u.getLastname();
      userView.email = u.getEmail();
      userView.phone = u.getPhone();
      userView.mobile = u.getMobile();
      userView.fax = u.getFax();
      userView.web = u.getWeb();
      userView.facebook = u.getFacebook();
      userView.twitter = u.getTwitter();
      userView.profilePhoto = u.getProfilephoto();
      userView.profilePhotoUrl = u.getProfilephotourl();
      Long lastLoginTs = getLastLoginTime(sessionMgr, userView.userId);
      userView.lastLoginTimestamp = lastLoginTs != null ? String.valueOf(lastLoginTs) : "";
      userView.createdon = Utils.formatDatePrint(u.getCreatedtimestamp());


      Account a = Account.findByLegacyId(userId);
      userView.accountId = a.getUid();
      //check for new fields in account prop
      if (a != null) {
        AccountProp prop = AccountProp.findByPk(a.getUid(), AccountProp.PropertyType.CONTACT);
        if (prop != null && prop.accountContact != null && prop.accountContact.phones != null) {
          PhoneNumber ext = prop.accountContact.phones.get(PhoneNumber.PhoneType.EXT);
          if (ext != null && ext.getNumber() != null && !ext.getNumber().isEmpty()) {
            userView.extension = ext.getNumber();
          }
        }
      }
      /* BILLING */
      if (SecurityMgr.isUmappedAdmin(sessionMgr)) {

        BillingSettingsForUser bsUser = BillingSettingsForUser.find.byId(u.getUserid());
        BillingInfoView biv = new BillingInfoView(bsUser);
        biv.availableEntities = BillingEntity.find.all();
        biv.availableSchedules = BillingSchedule.getSchedulesForUser();

        BillingSchedule.Type bsType = null;
        AccountProp accountProp = AccountProp.findByPk(a.getUid(), AccountProp.PropertyType.BILLING_INFO);
        AccountProp billToAgentProp = null;

        if (bsUser == null) { //User with no billing record
          biv.availablePlans = BillingPlan.plansPerSchedule(biv.availableSchedules.get(0).getName());

        } else {
          biv.availablePlans = BillingPlan.plansPerSchedule(biv.bsUser.getSchedule().getName());

          bsType = bsUser.getSchedule().getName();
          BillingPlan addonRootPlan = bsUser.getPlan();

          switch (bsUser.getBillTo().getName()) {
            case COMPANY:
              Company bsCmpy = Company.find.byId(bsUser.getCmpyid());
              if (bsCmpy != null) {
                biv.billableCmpyName = bsCmpy.name;
                BillingSettingsForCmpy bsc = BillingSettingsForCmpy.find.byId(bsCmpy.getCmpyid());
                bsType = bsc.getSchedule().getName();
                addonRootPlan = bsc.getPlan();
              }
              break;
            case AGENT:
              Account billAgent = Account.findActiveByLegacyId(bsUser.getAgent());
              if (billAgent != null) {
                biv.billableAgentName = billAgent.getFullName()  + " (" + billAgent.getLegacyId()+")";
                BillingSettingsForUser bsa = BillingSettingsForUser.find.byId(bsUser.getAgent());
                bsType = bsa.getSchedule().getName();
                addonRootPlan = bsa.getPlan();
                billToAgentProp = AccountProp.findByPk(billAgent.getUid(), AccountProp.PropertyType.BILLING_INFO);
              }
              break;
          }

          biv.addons = buildUserAddonView(addonRootPlan, u.getUserid(), bsType, false);

          // check to see if this user has a credit card payment
          userView.creditCardInfo = new CreditCardInfoView();
          if (accountProp != null || billToAgentProp != null) {
            if (accountProp != null && accountProp.billingProperties != null && accountProp.billingProperties.braintree != null && accountProp.billingProperties.braintree.hasPayment()) {
              StringBuilder sb = new StringBuilder();
              sb.append(a.getFullName()); sb.append(" ("); sb.append(a.getLegacyId()); sb.append(" )"); sb.append(" has a Braintree payment method setup");
              userView.creditCardInfo.userBillingInfo = sb.toString();
              userView.creditCardInfo.isCreditCardEligible = true;
            }
            if (billToAgentProp != null && billToAgentProp.billingProperties != null && billToAgentProp.billingProperties.braintree != null && billToAgentProp.billingProperties.braintree.hasPayment()) {
              StringBuilder sb = new StringBuilder();
              sb.append("Billing agent: ");
              sb.append(biv.billableAgentName); sb.append(" has a Braintree payment method setup");
              userView.creditCardInfo.agentBillingInfo = sb.toString();
              userView.creditCardInfo.isCreditCardEligible = true;
            }

          }
          //check if this user has credit card subscription set up
          BraintreeSunbscription bsUserBraintree = bsUser.getBraintree();
          if (bsUserBraintree != null && bsUserBraintree.subToken != null && !bsUserBraintree.subToken.isEmpty()) {
            CreditCardSubView subView = new CreditCardSubView();
            subView.subId = bsUserBraintree.subToken;
            subView.custId = bsUserBraintree.custToken;
            subView.planId = bsUserBraintree.planId;
            subView.isActive = bsUserBraintree.isActive();
            subView.braintreeStatus = bsUserBraintree.braintreeStatus;
            subView.braintreeStatusAsOf = bsUserBraintree.lastUpdatedTS;

            try  {
              BraintreeGateway braintreeGateway = braintreeFactory.getInstance();
              Subscription subscription = braintreeGateway.subscription().find(bsUserBraintree.subToken);
              if (subscription != null) {
                if (subscription.getDescriptor() != null) {
                  subView.name = subscription.getDescriptor().getName();
                }
                subView.amount = subscription.getPrice().toString();
                subView.createdOn = Utils.formatDatePrint(subscription.getCreatedAt().getTimeInMillis());
                subView.firstBillDate = Utils.formatDatePrint(subscription.getFirstBillingDate().getTimeInMillis());
                if (subscription.getPaidThroughDate() != null) {
                  subView.paidThru = Utils.formatDatePrint(subscription.getPaidThroughDate().getTimeInMillis());
                }

                if (subscription.getNextBillingDate() != null) {
                  subView.nextBillDate = Utils.formatDatePrint(subscription.getNextBillingDate().getTimeInMillis());
                }
                if (subscription.getFailureCount() > 0) {
                  subView.failureCount = subscription.getFailureCount();
                }
                if (subscription.getDaysPastDue() != null) {
                  subView.daysPastDue = subscription.getDaysPastDue();
                }
              }
              //let's get addons subscriptions if any
            } catch (Exception e) {
              Log.err("BRAINTREE: Error getting subscription details - User: " + bsUser.getUserId() + " Sub Id: " + bsUser.getBraintree().subToken, e);
            }
            userView.creditCardInfo.addCreditCardSubView(subView);
            userView.creditCardInfo.hasPlanSub = true;
          } else {
            userView.creditCardInfo.hasPlanSub = false;
          }
          List<BillingAddonsForUser> userAddons = BillingAddonsForUser.getAddonsForUser(bsUser.getUserId());
          if (userAddons != null) {
            BraintreeGateway braintreeGateway = braintreeFactory.getInstance();
            for (BillingAddonsForUser userAddon: userAddons) {
              BraintreeSunbscription aoUserBraintree = userAddon.getBraintree();
              if (aoUserBraintree != null && aoUserBraintree.subToken != null) {
                CreditCardSubView subView = new CreditCardSubView();
                subView.subId = aoUserBraintree.subToken;
                subView.custId = aoUserBraintree.custToken;
                subView.planId = aoUserBraintree.planId;
                subView.isAddOn = true;
                subView.isActive = aoUserBraintree.isActive();
                subView.braintreeStatus = aoUserBraintree.braintreeStatus;
                subView.braintreeStatusAsOf = aoUserBraintree.lastUpdatedTS;
                try {
                  Subscription subscription = braintreeGateway.subscription().find(aoUserBraintree.subToken);
                  if (subscription != null) {
                    if (subscription.getDescriptor() != null) {
                      subView.name = subscription.getDescriptor().getName();
                    }
                    subView.amount = subscription.getPrice().toString();
                    subView.createdOn = Utils.formatDatePrint(subscription.getCreatedAt().getTimeInMillis());
                    subView.firstBillDate = Utils.formatDatePrint(subscription.getFirstBillingDate().getTimeInMillis());
                    if (subscription.getPaidThroughDate() != null) {
                      subView.paidThru = Utils.formatDatePrint(subscription.getPaidThroughDate().getTimeInMillis());
                    }

                    if (subscription.getNextBillingDate() != null) {
                      subView.nextBillDate = Utils.formatDatePrint(subscription.getNextBillingDate().getTimeInMillis());
                    }
                    if (subscription.getFailureCount() > 0) {
                      subView.failureCount = subscription.getFailureCount();
                    }
                    if (subscription.getDaysPastDue() != null) {
                      subView.daysPastDue = subscription.getDaysPastDue();
                    }
                  }
                } catch (Exception e) {
                  Log.err("BRAINTREE: Error getting add-on details - User: " + bsUser.getUserId() + " Sub Id: " + userAddon.getBraintree().subToken, e);
                  e.printStackTrace();
                }
                userView.creditCardInfo.addCreditCardSubView(subView);

              }
            }
          }

          userView.stillOnTrial = false;
          if (bsUser.getSchedule().getName() == BillingSchedule.Type.TRIAL) {
            userView.stillOnTrial = true;
          }

        }
        userView.billing = biv;

        /* ELIGIBLE PLANS */


        List<Integer> userConsortia = new ArrayList<>();
        if (userId != null) {
          userConsortia = ConsortiumCompany.findUserConsortiaFromAllCompanies(userId);
          //If the list of Copany Consortia returns empty then User's Company(ies) must be independent
          //So we add the ID for Independent Consortia to the list and proceed.
          if (userConsortia.size() < 1) {
            userConsortia.add(0);
          }
        }

        if (accountProp != null && accountProp.billingProperties!= null && accountProp.billingProperties.eligiblePlans != null) {
          userView.eligiblePlans =  accountProp.billingProperties.eligiblePlans;
        }

        userView.monthlyBasicPlanList = BillingPlan.findPlansPerScheduleFeatures(BillingSchedule.Type.MONTHLY, false, BillingPlan.BillType.CC, userConsortia);
        userView.monthlyProPlanList = BillingPlan.findPlansPerScheduleFeatures(BillingSchedule.Type.MONTHLY, true, BillingPlan.BillType.CC, userConsortia);
        userView.yearlyBasicPlanList = BillingPlan.findPlansPerScheduleFeatures(BillingSchedule.Type.YEARLY, false, BillingPlan.BillType.CC, userConsortia);
        userView.yearlyProPlanList = BillingPlan.findPlansPerScheduleFeatures(BillingSchedule.Type.YEARLY, true, BillingPlan.BillType.CC, userConsortia);
        /* END OF ELIGIBLE PLANS */
      }
      /* END OF BILLING */
      /* API User Link */
      List<String> apiCmpyIds = new ArrayList<>();
      if (cmpyId != null && cmpyId.trim().length() > 0) {
        apiCmpyIds.add(cmpyId);
        //add support for linked cmpies - ie. user can also see the token from the parent company - this is only available to umapped admin
        if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
          if (cmpy != null && cmpy.getParentCmpyId() != null) {
            apiCmpyIds.add(cmpy.getParentCmpyId());
          }
          //let's see also get all the child companies - ie companies where this company is the parent
          List<Company> children = Company.getChildCmpies(cmpy.getCmpyid());
          if (children != null) {
            for (Company child: children) {
              if (child.getStatus() == APPConstants.STATUS_ACTIVE) {
                apiCmpyIds.add(child.getCmpyid());
              }
            }
          }

        }
      } else  if( cmpyId == null && userView.userId.equals(sessionMgr.getUserId())) {
        if (cred.hasCompany()) {
          apiCmpyIds.add(cred.getCmpyId());
          Company c = Company.find.byId(cred.getCmpyId());
          //add support for linked cmpies - ie. user can also see the token from the parent company - for umapped admin only
          if (c != null && c.getParentCmpyId() != null && SecurityMgr.isUmappedAdmin(sessionMgr)) {
            apiCmpyIds.add(c.getParentCmpyId());
          }
        }
      } else if (userView.cmpyLinks != null && !userView.cmpyLinks.isEmpty() && userView.isUMappedAdmin) {
        for (UserCmpyLinkView l: userView.cmpyLinks) {
          apiCmpyIds.add(l.cmpyId);
          //add support for linked cmpies - ie. user can also see the token from the parent company - for umapped admin only
          Company c = Company.find.byId(l.cmpyId);
          if (c != null && c.getParentCmpyId() != null && SecurityMgr.isUmappedAdmin(sessionMgr)) {
            apiCmpyIds.add(c.getParentCmpyId());
          }
        }
      }
      userView.apiCmpyTokens = new ArrayList<>();
      for (String cId: apiCmpyIds) {
        List<ApiCmpyTokenView>  apiCmpyTokenViews = ApiBookingController.getApiCmpyTokens(cId);
        if (apiCmpyTokenViews != null && !apiCmpyTokenViews.isEmpty()) {
          userView.apiCmpyTokens.addAll(apiCmpyTokenViews);
        }
      }

      if (userView.apiCmpyTokens != null && !userView.apiCmpyTokens.isEmpty()) {
        userView.apiUserLinks = ApiBookingController.getApiUserLink(userView.userId);
      }

      if (u.createdby != null && u.createdby.equalsIgnoreCase("system")) {
        //find the invite
        UserInvite ui = UserInvite.findByEmail(u.getEmail());
        userView.isViaCollab = false;
        if (ui != null) {
          userView.createdby = ui.getCreatedby();
          userView.isViaCollab = true;
        }
      } else {
        userView.createdby = u.getCreatedby();
      }
      if (lastLoginTs == null) {
        userView.lastLoginPrint = "No Logins Yet.";
      }
      else {
        userView.lastLoginPrint = Utils.formatDateTimePrint(lastLoginTs);
      }

      //check to display resend of welcome email - only for users who have never signed in
      if (u.getCreatedtimestamp().longValue() == u.getLastlogintimestamp().longValue() &&
          SecurityMgr.isUmappedAdmin(sessionMgr)) {
        userView.canSendWelcomeEmail = true;
      }
      else {
        userView.canSendWelcomeEmail = false;
      }

      userView.status = u.status;

      userView.intUserType = u.getUsertype();
      if (cmpyLink != null) {
        userView.userType = cmpyLink.get(0).getLinktype().getIdx();
      }
      userView.isExistingUser = true;
      if (userView.firstName != null) {
        userView.name = userView.firstName + " ";
      }
      else {
        userView.name = "";
      }

      if (userView.lastName != null) {
        userView.name = userView.name + userView.lastName;
      }
    } else {
       /* BILLING */
      if (SecurityMgr.isUmappedAdmin(sessionMgr)) {

        BillingInfoView biv = new BillingInfoView((BillingSettingsForUser)null);
        biv.availableEntities = BillingEntity.find.all();
        biv.availableSchedules = BillingSchedule.getSchedulesForUser();
        biv.availablePlans = BillingPlan.plansPerSchedule(biv.availableSchedules.get(0).getName());
        userView.billing = biv;
      }
      /* END OF BILLING */
    }
    return ok(views.html.user.user.render(userView));
  }

  private static List<UserAddonView> buildUserAddonView(BillingPlan currentPlan,
                                                        String userId,
                                                        BillingSchedule.Type schedType, boolean hideInvisible) {
    List<UserAddonView> view = new ArrayList<>();

    if(currentPlan == null || schedType == null || userId == null) {
      return view;
    }

    List<BillingAddonsForUser> userAddons = BillingAddonsForUser.getAddonsForUser(userId);
    for(BillingAddonsForUser ua: userAddons) {
      UserAddonView uav = new UserAddonView();
      uav.addonParams = ua;
      uav.addon = BillingPlan.find.byId(ua.getPk().planId);
      uav.startDate = Utils.getISO8601Date(ua.getStartTs());
      uav.cutoffDate = Utils.getISO8601Date(ua.getCutoffTs());
      view.add(uav);
    }

    for(BillingPlan p: BillingPlan.newAddonsForUser(userId, currentPlan.getPlanId(), hideInvisible)){
      if(p.getSchedule() == schedType) {
        UserAddonView uav = new UserAddonView();
        uav.addonParams = BillingAddonsForUser.buildAddonForUser(userId, p.getPlanId(), "");
        uav.addon = p;
        uav.startDate = "";
        uav.cutoffDate = "";
        view.add(uav);
      }
    }
    return view;
  }

  public static class AddonConfigRequest {
    public Long addonId;
    public String userId;
    public String startDate;
    public String cutoffDate;
    public Boolean enabled;
  }

  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class})
  public Result addonEdit() {
    StopWatch sw = new StopWatch();
    sw.start();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    AddonConfigRequest     config     = Json.fromJson(request().body().asJson(), AddonConfigRequest.class);
    BillingPlan            bp         = BillingPlan.find.byId(config.addonId);
    BillingAddonsForUser   afu        = BillingAddonsForUser.getByPk(config.addonId, config.userId);
    BillingSettingsForUser bs         = BillingSettingsForUser.find.byId(config.userId);

    if(!sessionMgr.getUserId().equals(config.userId)
       && !SecurityMgr.isUmappedAdmin(sessionMgr)
        && (bs.getAgent() != null && !sessionMgr.getUserId().equals(bs.getAgent()))) {
      return unauthorized("Unauthorized Access. Contact support@umapped.com");

    }

    long firstMsOfMonth = Utils.getFirstMsOfCurrentMonth();
    long lastMsOfMonth  = Utils.getLastMsOfCurrentMonth();

    long startDate = (config.startDate != null && config.startDate.length() > 0) ?
                     Utils.getMilliSecs(config.startDate) :
                     0l;

    long cutoffDate = (config.cutoffDate != null && config.cutoffDate.length() > 0) ?
                      Utils.getMilliSecs(config.cutoffDate) :
                      0l;

    //No billing settings - we will not handle this request
    if(bs == null) {
      return badRequest("No user billing details found...");
    }

    BillingSchedule.Type bsType = bs.getSchedule().getName();
    if (bs.getBillTo().getName() == BillingEntity.Type.AGENT &&
        bs.getAgent() != null &&
        bs.getAgent().trim().length() > 0) {
      BillingSettingsForUser bsa = BillingSettingsForUser.find.byId(bs.getAgent());
      bsType = bsa.getSchedule().getName();
    }

    if (bs.getBillTo().getName() == BillingEntity.Type.COMPANY &&
        bs.getCmpyid() != null &&
        bs.getCmpyid().trim().length() > 0) {
      BillingSettingsForCmpy bsc = BillingSettingsForCmpy.find.byId(bs.getCmpyid());
      bsType = bsc.getSchedule().getName();
    }

    if (afu == null) { //New Addon config
      afu = BillingAddonsForUser.buildAddonForUser(config.userId, config.addonId, sessionMgr.getUserId());
      afu.setEnabled(config.enabled);
      afu.setTrialCount(bp.getTrialLength());

      switch (bsType) {
        case YEARLY:
          if (startDate == 0 && config.enabled) {
            startDate = Utils.addDaysToTs(System.currentTimeMillis(), bp.getTrialLength());
          }
          break;
        case MONTHLY:
          if (startDate == 0 && config.enabled) {
            startDate = Utils.addDaysToTs(System.currentTimeMillis(), bp.getTrialLength());
          }
          break;
        default:
          return badRequest("Addon billing schedule is not supported");
      }
      afu.setStartTs(startDate > 0 ? startDate : null);
      afu.setCutoffTs(cutoffDate > 0 ? cutoffDate : null);
    }
    else { //Existing configuration
      //Enabling addon
      if (config.enabled && !afu.getEnabled()) {
        if (afu.getCutoffTs() != null && afu.getCutoffTs() > System.currentTimeMillis()) {
          //this add on is still active - so do not allow the user to reenable it
          StringBuilder sb = new StringBuilder();
          sb.append("The ");
          if(bp != null && bp.getName() != null) {
            sb.append(bp.getName());
          } else {
            sb.append("add-on");
          }
          sb.append(" is still valid until ");
          sb.append(Utils.formatDatePrint(afu.getCutoffTs()));
          sb.append(". You can re-enable the add-on after this date.");
          flash(SessionConstants.SESSION_PARAM_MSG,sb.toString());
          return ok();
        }
        cutoffDate = 0;
        //Trial was not used before
        if (afu.getTrialCount() == 0) {
          afu.setTrialCount(bp.getTrialLength());
          if (startDate == 0 || afu.getStartTs().equals(startDate)) {
            startDate = Utils.addDaysToTs(System.currentTimeMillis(), bp.getTrialLength());
          }
        }
        //If previous start date was in current month, preserve it
        if (afu.getStartTs() != null && afu.getStartTs().longValue() >= firstMsOfMonth) {
          startDate = afu.getStartTs();
        }
        else if (afu.getCutoffTs() != null && afu.getCutoffTs() > System.currentTimeMillis()) {
          startDate = afu.getCutoffTs();
        }
        //If start date not specified - then it is today
        if (startDate == 0) {
          startDate = System.currentTimeMillis();
        }
      }

      //Difference between plan when disabling addons
      switch (bsType) {
        case YEARLY:
          //Disabling
          if (!config.enabled && afu.getEnabled()) {
            if (afu.getStartTs() > System.currentTimeMillis()) { //We are during free period
              cutoffDate = afu.getStartTs();
              startDate = 0;
            }
            else {
              cutoffDate = Utils.addYearsToTs(bs.getStartTs(), 1);
            }
          }
          break;
        case MONTHLY:
          //Disabling addon
          if (!config.enabled && afu.getEnabled()) {
            if (afu.getStartTs() > System.currentTimeMillis()) { //We are during free period
              cutoffDate = afu.getStartTs();
              startDate = 0;
            }
            else {
              cutoffDate = lastMsOfMonth;
            }
          }
          break;
        default:
          return badRequest("Addon billing schedule is not supported");
      }

      afu.setCutoffTs(cutoffDate > 0 ? cutoffDate : null);
      afu.setStartTs(startDate > 0 ? startDate : null);
      afu.setEnabled(config.enabled);
      afu.markModified(sessionMgr.getUserId());
    }

    afu.save();

    Account a = Account.findByLegacyId(config.userId);
    Credentials cred = Credentials.getFromCache(a.getUid());
    if(cred == null) {
      cred = SecurityMgr.getCredentials(a);
    } else {
      cred.setCapabilities(SecurityMgr.getCapabilities(config.userId));
    }
    if (cred !=  null) {
      cred.updateCache();
    }

    //let's process billing
    if (config.enabled) {
      //for add-on enable - if this user or the agent this user bills to has a valid credit card, we bill to it
      AccountBilling accountBilling = null;
      BillingSettingsForUser billingSettingsForUser = BillingSettingsForUser.findSettingsByUserId(config.userId);
      if (billingSettingsForUser != null && (billingSettingsForUser.getBillTo().getName().ordinal() == BillingEntity.Type.USER.ordinal() || billingSettingsForUser.getBillTo().getName().ordinal() == BillingEntity.Type.AGENT.ordinal())) {
        if (billingSettingsForUser.getBillTo().getName().ordinal() == BillingEntity.Type.AGENT.ordinal()) {
          billingSettingsForUser = BillingSettingsForUser.findSettingsByUserId(billingSettingsForUser.getAgent());
        }
      }
      if (billingSettingsForUser != null) {
        Account account = Account.findActiveByLegacyId(billingSettingsForUser.getUserId());
        AccountProp accountProp = AccountProp.findByPk(account.getUid(), AccountProp.PropertyType.BILLING_INFO);
        if (accountProp != null && accountProp.billingProperties != null && accountProp.billingProperties.braintree != null && accountProp.billingProperties.braintree.hasPayment()) {
          try {
            com.braintreegateway.Result <Subscription> result =  BillingApiController.processSingleAddonsForUser(accountProp.billingProperties,
                                                                                        afu,
                                                                                        braintreeFactory.getInstance(),
                                                                                        sessionMgr.getCredentials().getAccount(),
                                                                                        account,
                                                                                        a,
                                                                                        DBConnectionMgr.getUniqueLongId(),
                                                                                        braintreeFactory);
          } catch (Exception e) {

          }
        }
      }

    } else {
      //for add-on disable - if this add-on is already billed, we cancel the billing
      BraintreeGateway braintreeGateway = braintreeFactory.getInstance();
      if (afu.getBraintree() != null && afu.getBraintree().subToken != null) {
        try {
          AccountBilling accountBilling = null;
          Account account = a;
          BillingSettingsForUser billingSettingsForUser = BillingSettingsForUser.findSettingsByUserId(config.userId);
          if (billingSettingsForUser != null && (billingSettingsForUser.getBillTo().getName().ordinal() == BillingEntity.Type.USER.ordinal() || billingSettingsForUser.getBillTo().getName().ordinal() == BillingEntity.Type.AGENT.ordinal())) {
            if (billingSettingsForUser.getBillTo().getName().ordinal() == BillingEntity.Type.AGENT.ordinal()) {
              billingSettingsForUser = BillingSettingsForUser.findSettingsByUserId(billingSettingsForUser.getAgent());
            }
          }
          if (billingSettingsForUser != null) {
            account = Account.findActiveByLegacyId(billingSettingsForUser.getUserId());
          }
          com.braintreegateway.Result<Subscription> result = BillingApiController.cancelSingleAddonSubscription(afu,
                                                                                                                braintreeGateway,
                                                                                                                sessionMgr.getCredentials().getAccount(),
                                                                                                                account,
                                                                                                                a,
                                                                                                                DBConnectionMgr.getUniqueLongId());

        } catch (Exception e) {

        }
      }
    }


    sw.stop();
    Log.debug("Updated user " + config.userId + " addon settings in :" + sw.getTime() + "ms");
    flash(SessionConstants.SESSION_PARAM_MSG, "Successfully Updated");
    return ok();
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result umappedUser() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    UserView userView = new UserView();
    userView.message = flash(SessionConstants.SESSION_PARAM_MSG);

    Form<UserForm> userForm = formFactory.form(UserForm.class).bindFromRequest();
    UserForm userInfoForm = null;
    if(!userForm.hasErrors()) {
      userInfoForm = userForm.get();
    }

    if (userInfoForm != null && userInfoForm.getInUserId() != null) {
      userView.pageMode = Boolean.parseBoolean(userInfoForm.getInCmpyMode()); //TODO: Should this be outside?
      userView.isUMappedAdmin = true;

      UserProfile u = UserProfile.find.byId(userInfoForm.getInUserId());
      if (u.getUserid().equals(sessionMgr.getUserId())) {
        userView.isOwner = true;
      }
      else {
        userView.isOwner = false;
      }

      userView.userId = u.getUserid();
      userView.firstName = u.getFirstname();
      userView.lastName = u.getLastname();
      userView.email = u.getEmail();
      userView.phone = u.getPhone();
      userView.mobile = u.getMobile();
      userView.fax = u.getFax();
      userView.web = u.getWeb();
      userView.facebook = u.getFacebook();
      userView.twitter = u.getTwitter();
      userView.profilePhoto = u.getProfilephoto();
      userView.profilePhotoUrl = u.getProfilephotourl();

      Long lastLoginTs = getLastLoginTime(sessionMgr, u.getUserid());

      userView.lastLoginTimestamp = lastLoginTs != null ? String.valueOf(lastLoginTs) : "";
      userView.lastLoginPrint = lastLoginTs != null ? Utils.formatTimestampPrint(lastLoginTs) : "";

      userView.isExistingUser = true;
      if (userView.firstName != null) {
        userView.name = userView.firstName + " ";
      }
      else {
        userView.name = "";
      }

      if (userView.lastName != null) {
        userView.name = userView.name + userView.lastName;
      }
    }

    return ok(views.html.user.umappeduser.render(userView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result profileUpdate(String userId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Form<UserProfileForm> form = formFactory.form(UserProfileForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }
    UserProfileForm userForm = form.get();
    if(!userForm.getInUserId().equals(sessionMgr.getUserId()) || !userId.equals(sessionMgr.getUserId())) {
      return UserMessage.message(ReturnCode.AUTH_ADMIN_FAIL);
    }

    Account a = Account.findByLegacyId(userId);
    if(a == null) {
      return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND, "Account not found.");
    }

    UserProfile u = UserProfile.find.byId(userForm.getInUserId());
    AccountController.AccountHelper ah = AccountController.AccountHelper.build(sessionMgr.getUserId());



    boolean res = ah.setOperation(Operation.UPDATE).setAccount(a).setUserProfile(u).processAccountForm(userForm);
    if(res) {
      flash(SessionConstants.SESSION_PARAM_MSG, userForm.getInUserId() + " updated successfully.");
      return redirect(routes.UserController.profile(userId));
    }
    return UserMessage.message(ReturnCode.LOGICAL_ERROR, "Unknown error while updating profile...");
  }


  @With({Authenticated.class, Authorized.class})
  public Result editUser() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    UserView userView = new UserView();

    Form<UserForm> userForm = formFactory.form(UserForm.class);
    UserForm userInfoForm = userForm.bindFromRequest().get();
    userView.pageMode = Boolean.parseBoolean(userInfoForm.getInCmpyMode());
    boolean isExistingUser = Boolean.parseBoolean(userInfoForm.getInEditMode());

    //Form validation
    if(userInfoForm.getInUserEmail() == null || userInfoForm.getInUserId().length() == 0) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA, "Email address not specified");
    }

    if(userInfoForm.getInUserId() == null || userInfoForm.getInUserId().length() == 0) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA, "User ID not specified");
    }

    if (userInfoForm.getInUserId() == null || userInfoForm.getInUserId().length() < 5) {
      return UserMessage.message("User name too short - (min 5 chars). Please choose another.");
    }

    if(!isExistingUser) { //If a new user - let's validate
      Matcher m = Account.usernameValidationRe.matcher(userInfoForm.getInUserId());
      if (!m.matches()) {
        return UserMessage.message("Username contains illegal characters. Only a-z, A-Z, 0-9 and _ are allowed. " +
                                   "With length from 5 to 15 characters.");
      }

      if (userInfoForm.getInUserId().toLowerCase().contains("system") ||
          userInfoForm.getInUserId().toLowerCase().contains("umapped") ||
          (!SecurityMgr.isUmappedAdmin(sessionMgr) && userInfoForm.getInUserId().toLowerCase().contains("admin"))) {
        return UserMessage.message("Please make sure the userid does not contain the words 'admin', 'system' or " +
                                   "'umapped')");
      }

      Account dup = Account.findByLegacyId(userInfoForm.getInUserId());
      if(dup != null) {
        BaseView view = new BaseView("This user name is already in use. Please choose another username.");
        String userId = userInfoForm.getInUserId();

        int count = Account.getContUserProfiles(userId);
        if (count > 0) {
          userId += count;
        }

        if (!userId.equals(userInfoForm.getInUserId())) {
          view.message += " You can try - " + userId;
        }
        return ok(views.html.common.message.render(view));
      }

      dup = Account.findByEmail(userInfoForm.getInUserEmail().trim());
      if(dup != null && !dup.isTraveler()) {
        return UserMessage.message("This email is already associated with a Umapped account. " +
                                   "Please choose another email address");
      }
    }

    UserProfile u = null;
    Account a = null;

    if (isExistingUser) {
      u = UserProfile.find.byId(userInfoForm.getInUserId());
      a = Account.findByLegacyId(userInfoForm.getInUserId());

      if(a == null) {
        return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND, "Account not found.");
      }
      if(u == null) {
        return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND, "User ID not found.");
      }

      if (a.getAccountType() != null && a.getAccountType() == Account.AccountType.UMAPPED_ADMIN) {
        if (!userInfoForm.getInUserEmail().toLowerCase().contains("@umapped.com")) {
          return UserMessage.message("Umapped Administrator can only have an @umapped.com email. Please try again.");
        }

        if (!SecurityMgr.isUmappedAdmin(sessionMgr)) {
          return UserMessage.message(ReturnCode.AUTH_ACCOUNT_EDIT_FAIL, "Unauthorized Access");
        }
      }

      //
      if (a.getEmail() != null && userInfoForm.getInUserEmail() != null && !a.getEmail().trim().toLowerCase().equals(userInfoForm.getInUserEmail().trim())) {
        //check if the new email is already in use.
        Account dup = Account.findByEmail(userInfoForm.getInUserEmail().trim());
        if(dup != null && !a.getUid().equals(dup.getUid())) {
          return UserMessage.message("This email is already associated with a UMapped account. " +
                                     "Please choose another email address");
        }
      }

    }

    /* If existing user - preserving case for the username, otherwise forcing lower-case */
    if (u == null) {
      userInfoForm.setInUserId(userInfoForm.getInUserId().trim().toLowerCase());
    }

    //Company mode
    if (userInfoForm.getInCmpyId() != null &&
        userInfoForm.getInCmpyId().length() > 0) {

      Company cmpy = Company.find.byId(userInfoForm.getInCmpyId());

      if(cmpy == null || !(SecurityMgr.isCmpyAdmin(cmpy, sessionMgr) || SecurityMgr.isUmappedAdmin(sessionMgr))) {
        return UserMessage.message(ReturnCode.AUTH_ACCOUNT_EDIT_FAIL, "Unauthorized access");
      }

      if (userView.pageMode &&
          (userInfoForm.getInUserType() == null ||
           (!userInfoForm.getInUserType().equalsIgnoreCase(String.valueOf(UserCmpyLink.LinkType.ADMIN.getIdx())) &&
            !userInfoForm.getInUserType().equalsIgnoreCase(String.valueOf(UserCmpyLink.LinkType.MEMBER.getIdx())) &&
            !userInfoForm.getInUserType().equalsIgnoreCase(String.valueOf(UserCmpyLink.LinkType.POWER.getIdx())) &&
            !userInfoForm.getInUserType().equalsIgnoreCase(String.valueOf(APPConstants.USER_ACCOUNT_TYPE_UMAPPED))))) {
        return UserMessage.message("Please select a user type.");
      }

      if (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr)) {
        //check if we exceed the limit
        int activeCount = UserCmpyLink.activeCountByCmpyId(cmpy.cmpyid);
        if (activeCount >= cmpy.numaccounts) {
          return UserMessage.message("You have exceeded the maximum number of users - please remove some users or contact " +
                                     "Umapped support.");
        }
      }

      AccountController.AccountHelper ah = AccountController.AccountHelper.build(sessionMgr.getUserId());

      if (isExistingUser) {
        AccountCmpyLink acl = AccountCmpyLink.findActiveAccountAndCmpy(a.getUid(), userInfoForm.getInCmpyId());
        if (acl == null) {
          return UserMessage.message( "Unauthorized Access");
        } else {
          UserCmpyLink.LinkType linkType = UserCmpyLink.LinkType.fromInt(Integer.parseInt(userInfoForm.getInUserType()));
          if (!acl.getLinkType().equals(linkType)) {
            //update the link if needed

            List<UserCmpyLink> userCmpyLinks = UserCmpyLink.findActiveByUserIdCmpyId(userInfoForm.getInUserId(), cmpy.getCmpyid());
            if (userCmpyLinks == null && userCmpyLinks.size() == 0) {
              return UserMessage.message( "Unauthorized Access");
            }
            UserCmpyLink cmpyLink = userCmpyLinks.get(0);
            cmpyLink.setLinktype(linkType);
            cmpyLink.save();

            acl.setLinkType(AccountCmpyLink.LinkType.fromUserCmpyLinkType(linkType));
            acl.save();

          }
        }

        ah.setOperation(Operation.UPDATE).setAccount(a).setUserProfile(u);
        if(!ah.processAccountForm(userInfoForm)){
          return UserMessage.message(ReturnCode.UNHANDLED_EXCEPTION, "Unknown error.");
        }

        flash(SessionConstants.SESSION_PARAM_MSG, userInfoForm.getInUserId() + " updated successfully.");

      } else {
        ah.setOperation(Operation.ADD).setAccountType(Account.AccountType.PUBLISHER);

        if(!ah.processAccountForm(userInfoForm)){
          return UserMessage.message(ReturnCode.UNHANDLED_EXCEPTION, "Unknown error.");
        }
        ah.linkToCompany(cmpy, userInfoForm.getInUserType());

        //Umapped admins have to send welcome emails manually for some reason
        if(!SecurityMgr.isUmappedAdmin(sessionMgr) && userInfoForm.getSendEmail()) {
          boolean res = ah.sendWelcomeEmail();
          if(!res) {
            return UserMessage.message(ReturnCode.UNHANDLED_EXCEPTION,
                                       "Failure to send Welcome email for the new account.");
          }
        }


        if (SecurityMgr.isUmappedAdmin(sessionMgr) || !userInfoForm.sendEmail) {
          flash(SessionConstants.SESSION_PARAM_MSG, userInfoForm.getInUserId() + " created successfully.");
        }
        else {
          flash(SessionConstants.SESSION_PARAM_MSG,
                userInfoForm.getInUserId() + " created successfully. An email has been sent to the user with " +
                "password instructions.");
        }
      }

      if(SecurityMgr.isUmappedAdmin(sessionMgr)) {
        boolean rc = processBillingForm(userInfoForm, ah.getAccount().getLegacyId(), sessionMgr);
        if (!rc) {
          BaseView errView = new BaseView();
          errView.message = flash(SessionConstants.SESSION_PARAM_MSG);
          return ok(views.html.common.message.render(errView));
        } else {
          userView.message = flash(SessionConstants.SESSION_PARAM_MSG);
        }
      }

      if (!isExistingUser) {
        if (!SecurityMgr.isUmappedAdmin(sessionMgr)) {
          //this is a new user created externally - so we set the new user's plan to be the same as the company plan
          userInfoForm.billingCmpyId = cmpy.getCmpyid();
          userInfoForm.billingSchedule = BillingSchedule.Type.COMPANY;
          userInfoForm.billingBilledTo = BillingEntity.Type.COMPANY;
          boolean rc = processBillingForm(userInfoForm, ah.getAccount().getLegacyId(), sessionMgr);
          if (!rc) {
            BaseView errView = new BaseView();
            errView.message = flash(SessionConstants.SESSION_PARAM_MSG);
            return ok(views.html.common.message.render(errView));
          } else {
            userView.message = flash(SessionConstants.SESSION_PARAM_MSG);
          }
        }
        //let's set the default web itinerary
        SysCapability sysCapability = SysCapability.getCapabilityByName(Capability.BETA_WEB_ITINERARY);
        if (sysCapability != null) {
          UserCapability.UserCapabilityID userCapabilityID = new UserCapability.UserCapabilityID();
          userCapabilityID.setCapPk(sysCapability.getPk());
          userCapabilityID.setUserId(ah.getAccount().getLegacyId());
          UserCapability userCapability = new UserCapability();
          userCapability.setPk(userCapabilityID);
          userCapability.setEnabled(true);
          userCapability.setCreatedBy(sessionMgr.getUserId());
          userCapability.setModifiedBy(sessionMgr.getUserId());
          userCapability.setCreatedTs(System.currentTimeMillis());
          userCapability.setModifiedTs(System.currentTimeMillis());
          userCapability.save();
        }
      }



      flash("inCmpyId", userInfoForm.getInCmpyId());
      flash("inCmpyMode", userInfoForm.getInCmpyMode());
      return redirect(routes.UserController.users());
    }
    else if (userInfoForm.getInUserId().equals(sessionMgr.getUserId()) ||
             SecurityMgr.isUmappedAdmin(sessionMgr)) {

      if(a != null) {
        AccountController.AccountHelper ah = AccountController.AccountHelper.build(sessionMgr.getUserId());
        boolean res = ah.setOperation(Operation.UPDATE)
                        .setAccount(a)
                        .setUserProfile(u)
                        .processAccountForm(userInfoForm);
        if(!res) {
          return UserMessage.message(ReturnCode.LOGICAL_ERROR);
        }

        if(SecurityMgr.isUmappedAdmin(sessionMgr)) {
          boolean rc = processBillingForm(userInfoForm, u.getUserid(), sessionMgr);
          if (!rc) {
            BaseView errView = new BaseView();
            errView.message = flash(SessionConstants.SESSION_PARAM_MSG);
            return ok(views.html.common.message.render(errView));
          }
        }
        UserController.invalidateCache(u.userid);

        flash(SessionConstants.SESSION_PARAM_MSG, userInfoForm.getInUserId() + " updated successfully.");
        return redirect(routes.UserController.user(u.userid, null, false));
      }
    }

    return UserMessage.message(ReturnCode.LOGICAL_ERROR, "Unauthorized Access");
  }

  @With(ValidSession.class)
  public Result userSubscription() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    UserView view = new UserView();
    String userId = sessionMgr.getUserId();

    if (userId != null) {
      BillingSettingsForUser bsUser = BillingSettingsForUser.find.byId(userId);
      if (bsUser == null || bsUser.getCutoffTs() != null || bsUser.getSchedule().getName() == BillingSchedule.Type.TRIAL) {
        Account account = Account.findActiveByLegacyId(userId);
        if (account != null) {
          view.userId = userId;
          view.firstName = account.getFirstName();
          view.lastName = account.getLastName();
          view.name = account.getFullName();

          AccountProp userBillingProp = AccountProp.findByPk(account.getUid(), AccountProp.PropertyType.BILLING_INFO);
          if (userBillingProp != null && userBillingProp.billingProperties != null && userBillingProp.billingProperties.eligiblePlans != null) {
            EligiblePlans eligiblePlans = userBillingProp.billingProperties.eligiblePlans;
            if (eligiblePlans.hasEligblePlan()) {
              view.monthlyBasicPlan = SubscriptionPlanView.buildView(eligiblePlans.monthlyBasicPlanId);
              view.monthlyProPlan = SubscriptionPlanView.buildView(eligiblePlans.monthlyProPlanId);
              view.yearlyBasicPlan = SubscriptionPlanView.buildView(eligiblePlans.yearlyBasicPlanId);
              view.yearlyProPlan = SubscriptionPlanView.buildView(eligiblePlans.yearlyProPlanId);

              //Set the Umapped Pricing Url for the Consortia of the eligible plans
              view.setUmConsortiumPricingUrl();

              String msg = flash(SessionConstants.SESSION_PARAM_MSG);
              if (msg != null) {
                view.message = msg;
              }
              return ok(views.html.userSubscription.render(view));
            } else {
              flash(SessionConstants.SESSION_PARAM_MSG, "Your account is not active - Please contact Umapped Support to subscribe to a plan");
            }
          } else {
            flash(SessionConstants.SESSION_PARAM_MSG, "No billing account exists for this User");
          }
        } else {
          flash(SessionConstants.SESSION_PARAM_MSG, "User account does not exist - Please contact Umapped Support");
        }
      } else {
        view.message = "You already subscribed to a billing plan - Please contact Umapped Support to change plans";
        return ok(views.html.userSubscription.render(view));
      }
    } else {
      flash(SessionConstants.SESSION_PARAM_MSG, "User does not exist - Please contact Umapped Support");
    }
    return redirect("/login");
  }

  @With(ValidSession.class)
  public Result saveSubscription() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Form<UserForm> userForm = formFactory.form(UserForm.class);
    UserForm form = userForm.bindFromRequest().get();
    UserView view = new UserView();

    if (form.getInUserId() == null || form.getInUserId().isEmpty()) {
      flash(SessionConstants.SESSION_PARAM_MSG, "Unauthorized Access - Please contact Umapped Support");
      //return redirect(routes.UserController.userSubscription(null));
      //return ok(views.html.userSubscription.render(view));
      return redirect("/login");
    }
    String userId = sessionMgr.getUserId();

    if (form.getBillingPlan() == null || form.getBillingPlan() < 1L) {
      //view.message = "Error - No Plan selected";
      flash(SessionConstants.SESSION_PARAM_MSG, "Error - No Plan selected");
      return redirect(routes.UserController.userSubscription());
    }
    Long planId = form.getBillingPlan();

    boolean newBilling = false;

    BillingPlan selectedPlan = BillingPlan.find.byId(planId);
    if (selectedPlan != null) {
      BillingSettingsForUser bsUser = BillingSettingsForUser.find.byId(userId);
      BillingSchedule bs = BillingSchedule.find.byId(selectedPlan.getSchedule());
      if (bsUser == null) {
        bsUser = BillingSettingsForUser.buildUserSettings(userId, userId);
        newBilling = true;
      }
      try {
        Ebean.beginTransaction();

        bsUser.setSchedule(bs);
        bsUser.setPlan(selectedPlan);
        long bStartTs = System.currentTimeMillis();
        bsUser.setStartTs(bStartTs);
        bsUser.setCutoffTs(null);
        if (selectedPlan.getBillingType() != null && selectedPlan.getBillingType() == BillingPlan.BillType.CC) {
          BillingPmntMethod bpm = BillingPmntMethod.find.byId(BillingPmntMethod.Type.CREDIT_CARD);
          bsUser.setPmntType(bpm);
        }


        if (newBilling) {
          bsUser.save();
        } else {
          bsUser.markModified(userId);
          bsUser.update();
        }
        Ebean.commitTransaction();
        Credentials cred = sessionMgr.getCredentials();
        sessionMgr.setSessionId(cred.getSessionId());
        cred.setSubscribedToBilling(true);
        if (!cred.isUmappedAdmin()) {
          cred.setBillingStatus(BillingApiController.getBillingState(cred));
        }
        cred.updateCache();
        flash(SessionConstants.SESSION_PARAM_MSG, "You have successfully subscribed to: " + selectedPlan.getName());
        return redirect(routes.SecurityController.creditCardSetup());
      } catch (Exception e) {
        Log.err("System Error during Database update/insert - Rolling back Transcation", e);
        Ebean.rollbackTransaction();
        flash(SessionConstants.SESSION_PARAM_MSG, "System Error - Please contact Umapped Support");
        return redirect(routes.UserController.userSubscription());
      }
    }
    flash(SessionConstants.SESSION_PARAM_MSG, "Error - selected Plan does not exist");
    return redirect(routes.UserController.userSubscription());
  }


  @With({Authenticated.class, Authorized.class})
  public Result addEligiblePlans() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    UserView userView = new UserView();

    Form<UserForm> userForm = formFactory.form(UserForm.class);
    UserForm form = userForm.bindFromRequest().get();
    userView.pageMode = Boolean.parseBoolean(form.getInCmpyMode());
    boolean isExistingUser = Boolean.parseBoolean(form.getInEditMode());

    //Form validation
    if(form.getInUserId() == null || form.getInUserId().length() == 0) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA, "User ID not specified");
    }
    String userId = form.getInUserId();
    String cmpyId = form.getInCmpyId();
    boolean cmpyMode = Boolean.parseBoolean(form.getInCmpyMode());

    Account account = Account.findActiveByLegacyId(userId);
    if (account != null) {
      AccountProp userBillingProp = AccountProp.findByPk(account.getUid(), AccountProp.PropertyType.BILLING_INFO);
      boolean newBilling = false;
      if (userBillingProp == null) {
        userBillingProp = AccountProp.build(account.getUid(), AccountProp.PropertyType.BILLING_INFO, sessionMgr.getAccountId());
        newBilling = true;
      }
      EligiblePlans eligiblePlans = new EligiblePlans();
      if (form.getMonthlyBasicPlanId() != null && !form.getMonthlyBasicPlanId().isEmpty()) {
        eligiblePlans.monthlyBasicPlanId = Long.parseLong(form.getMonthlyBasicPlanId());
      }
      if (form.getMonthlyProPlanId() != null && !form.getMonthlyProPlanId().isEmpty()) {
        eligiblePlans.monthlyProPlanId = Long.parseLong(form.getMonthlyProPlanId());
      }
      if (form.getYearlyBasicPlanId() != null && !form.getYearlyBasicPlanId().isEmpty()) {
        eligiblePlans.yearlyBasicPlanId = Long.parseLong(form.getYearlyBasicPlanId());
      }
      if (form.getYearlyProPlanId() != null && !form.getYearlyProPlanId().isEmpty()) {
        eligiblePlans.yearlyProPlanId = Long.parseLong(form.getYearlyProPlanId());
      }
      if (userBillingProp.billingProperties == null) {
        userBillingProp.billingProperties = new AccountBilling();
      }
      userBillingProp.billingProperties.eligiblePlans = eligiblePlans;
      userBillingProp.setModifiedBy(sessionMgr.getAccountId());
      userBillingProp.setModifiedTs(Timestamp.from(Instant.now()));
      userBillingProp.prePersist();
      if (newBilling) {
        userBillingProp.save();
      } else {
        userBillingProp.update();
      }
    }

    flash(SessionConstants.SESSION_PARAM_MSG, form.getInUserId() + " updated successfully.");
    return redirect(routes.UserController.user(form.getInUserId(), cmpyId, cmpyMode));
  }

  /**
   *
   * @param userInfoForm
   * @return false if error occured - message is in teh FLASH
   */
  private static boolean processBillingForm(UserForm userInfoForm, String userId, SessionMgr sessionMgr) {
    if (userInfoForm.billingSchedule != null) {
      BillingSettingsForUser bsUser = BillingSettingsForUser.find.byId(userId);
      boolean newBilling = false;
      if (bsUser == null) {
        bsUser = BillingSettingsForUser.buildUserSettings(userId, sessionMgr.getUserId());
        newBilling = true;
      }

      BillingEntity be = BillingEntity.find.byId(userInfoForm.billingBilledTo);
      bsUser.setBillTo(be);
      switch(userInfoForm.billingBilledTo) {
        case COMPANY:
          userInfoForm.billingAgentId = null;
          userInfoForm.billingPmntMethod = null;
          break;
        case USER:
          break;
        case AGENT:
          userInfoForm.billingCmpyId = null;
          userInfoForm.billingPmntMethod = null;
          break;
      }

      BillingSchedule bs = BillingSchedule.find.byId(userInfoForm.billingSchedule);
      bsUser.setSchedule(bs);
      switch(bs.getName()) {
        case COMPANY: //Company plan
          userInfoForm.billingAgentId = null;
          userInfoForm.billingPlan = null; //Effectively there is no plan
          break;
        case AGENT: //Agent plan
          userInfoForm.billingCmpyId = null;
          userInfoForm.billingPlan = null; //Effectively there is no plan
          break;
        default:
          break;
      }

      //Validation
      if(userInfoForm.billingBilledTo == BillingEntity.Type.COMPANY &&
         (userInfoForm.billingCmpyId == null ||userInfoForm.billingCmpyId.trim().length() == 0)) {
        flash(SessionConstants.SESSION_PARAM_MSG, "Error: User billable company is not set");
        return false;
      }

      if(userInfoForm.billingBilledTo == BillingEntity.Type.AGENT &&
         (userInfoForm.billingAgentId == null || userInfoForm.billingAgentId.trim().length() == 0)) {
        flash(SessionConstants.SESSION_PARAM_MSG, "Error: User billable agent is not set");
        return false;
      }

      bsUser.setAgent(userInfoForm.billingAgentId);
      bsUser.setCmpyid(userInfoForm.billingCmpyId);

      if (userInfoForm.billingPlan != null) {
        BillingPlan bp = BillingPlan.find.byId(userInfoForm.billingPlan);
        bsUser.setPlan(bp);
      } else {
        bsUser.setPlan(null);
      }

      if(userInfoForm.billingPmntMethod != null) {
        BillingPmntMethod bpm = BillingPmntMethod.find.byId(userInfoForm.billingPmntMethod);
        bsUser.setPmntType(bpm);
      }


      long bStartTs = System.currentTimeMillis();

      if(userInfoForm.billingStartDate != null && userInfoForm.billingStartDate.trim().length() > 0) {
        bStartTs = Utils.getMilliSecs(userInfoForm.billingStartDate);
        if (bStartTs == -1) {
          bStartTs = System.currentTimeMillis();
          flash(SessionConstants.SESSION_PARAM_MSG, "Warning: Billing Start Date was not set - using current date");
        }
      }
      bsUser.setStartTs(bStartTs);

      if(userInfoForm.serviceCutoffDate != null && userInfoForm.serviceCutoffDate.trim().length() > 0) {
        bsUser.setCutoffTs(Utils.getMilliSecs(userInfoForm.serviceCutoffDate));
      } else {
        bsUser.setCutoffTs(null);
      }

      bsUser.setRecurrent(userInfoForm.billingRecurrent);

      if(newBilling){
        bsUser.save();
      } else {
        bsUser.markModified(sessionMgr.getUserId());
        bsUser.update();
      }
      return true;
    }

    return false;
  }

  /**
   * Umapped Admin account creation
   * @return
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result newUMappedUser() {

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Form<UserForm> userForm = formFactory.form(UserForm.class);
    UserForm userInfoForm = userForm.bindFromRequest().get();

    if (!SecurityMgr.isUmappedAdmin(sessionMgr)) {
      BaseView view = new BaseView("Only Umapped Admins can spawn other Umapped admin.");
      return ok(views.html.common.message.render(view));
    }

    if (userInfoForm.getInUserId() == null || userInfoForm.getInUserId().length() < 5) {
      BaseView view = new BaseView("User name too short - (min 5 chars). Please choose another.");
      return ok(views.html.common.message.render(view));
    }

    if (!userInfoForm.getInUserEmail().toLowerCase().contains("@umapped.com")) {
      BaseView view = new BaseView("Umapped Administrator can only have an @umapped.com email. Please try again.");
      return ok(views.html.common.message.render(view));
    }

    Account a = Account.findByLegacyId(userInfoForm.getInUserId());
    if(a != null) {
      BaseView view = new BaseView("User with this username already exists");
      return ok(views.html.common.message.render(view));
    }

    Account prev = Account.findByEmail(userInfoForm.getInUserEmail());
    if(prev != null) {
      BaseView view = new BaseView("User with this email address already exists");
      return ok(views.html.common.message.render(view));
    }

    Matcher m = Account.usernameValidationRe.matcher(userInfoForm.getInUserId());
    if(!m.matches()) {
      BaseView view = new BaseView("Username contains illegal characters. Only a-z, A-Z, 0-9 and _ are allowed.");
      return ok(views.html.common.message.render(view));
    }

    AccountController.AccountHelper ah = AccountController.AccountHelper.build(sessionMgr.getUserId());
    boolean res = ah.setOperation(Operation.ADD)
      .setAccountType(Account.AccountType.UMAPPED_ADMIN)
      .processAccountForm(userInfoForm);

    if(!res) {
      BaseView emailSendErrorView = new BaseView("System Error - please retry or contact your administrator.");
      return ok(views.html.resetPwd.render(emailSendErrorView));
    }

    res = ah.sendEmail();
    if(res) {
      flash(SessionConstants.SESSION_PARAM_MSG, "Umapped user: " + userInfoForm.getInUserId() + " added " +
                                                "successfully.");
      BaseView successView = new BaseView("You will receive an email to complete your password reset shortly.");
      return ok(views.html.login.render(successView));
    } else {
      BaseView emailSendErrorView = new BaseView("System Error - please retry or contact your administrator.");
      return ok(views.html.resetPwd.render(emailSendErrorView));
    }
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result deleteUser() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<UserForm> userForm = formFactory.form(UserForm.class);
    UserForm userInfoForm = userForm.bindFromRequest().get();

    if (userInfoForm.getInUserId() != null && SecurityMgr.isUmappedAdmin(sessionMgr)) {
      AccountController.AccountHelper ah = AccountController.AccountHelper.build(sessionMgr.getUserId());

      if (ah.fromLegacyId(userInfoForm.getInUserId())) {
        boolean res = ah.deleteUser();
        if (!res) {
          flash(SessionConstants.SESSION_PARAM_MSG,
                "Failed to delete: " + ah.getUserProfile().getFullName() + ". Please" +
                " contact umapped support support@umapped.com");
        }
        else {
          UserController.invalidateCache(ah.getUserProfile().userid);
          flash(SessionConstants.SESSION_PARAM_MSG, ah.getUserProfile().getFullName() + " deleted successfully.");
        }

        return users();
      }
    }

    BaseView view = new BaseView();
    view.message = "System Error - cannot delete";
    return ok(views.html.common.message.render(view));
  }


  @With({Authenticated.class, Authorized.class})
  public Result deleteUserLink() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<UserForm> userForm = formFactory.form(UserForm.class);
    UserForm userInfoForm = userForm.bindFromRequest().get();

    if (userInfoForm.getInCmpyId() == null || userInfoForm.getInUserId() == null) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA, "Missing information in request. Please retry.");
    }

    Company cmpy = Company.find.byId(userInfoForm.getInCmpyId());
    if(cmpy == null) {
      return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND, "Company not found");
    }

    if (!(SecurityMgr.isCmpyAdmin(cmpy, sessionMgr) || SecurityMgr.isUmappedAdmin(sessionMgr))) {
      return UserMessage.message(ReturnCode.AUTH_ACCOUNT_EDIT_FAIL, "Insufficient privileges to unlink account.");
    }

    try {
      Account a = Account.findByLegacyId(userInfoForm.getInUserId());
      if(a == null) {
        return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND, "User not found");
      }

      AccountController.AccountHelper ah = AccountController.AccountHelper.build(sessionMgr.getUserId());
      ah.setAccount(a).setAccountCmpy(cmpy);
      boolean res = ah.unlinkFromCompany();
      if(!res) {
        return UserMessage.message(ReturnCode.UNHANDLED_EXCEPTION);
      }

//      res = ah.sendUnlinkEmail();
      if(!res) {
        flash(SessionConstants.SESSION_PARAM_MSG, "User - " + a.getFullName() + " removed successfully, but email " +
                                                  "notification failed.");
      }
      else {
        flash(SessionConstants.SESSION_PARAM_MSG, "User - " + a.getFullName() + " removed successfully. User will be " +
                                                  "notified by email");
      }

      return users();
    }
    catch (Exception e) {
      Log.err("deleteUserLink(): Unepxected error.", e);
      return UserMessage.message(ReturnCode.UNHANDLED_EXCEPTION);
    }
  }


  /**
   * @param userId
   * @param photoName
   * @param cmpyId
   * @return
   */
  @With({Authenticated.class, Authorized.class})
  public Result signS3UserPhoto(String userId, String photoName, Boolean isProfile, String cmpyId, Boolean cmpyMode) {

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    if (photoName.length() == 0) {
      ObjectNode result = Json.newObject();
      result.put("msg", "Error - please resubmit1");
      return ok(result);
    }

    Account a;
    if(sessionMgr.getAccount().isPresent()) {
      a = sessionMgr.getAccount().get();
    } else {
      a = Account.findByLegacyId(userId);
    }

    if (a == null) {
      ObjectNode result = Json.newObject();
      result.put("msg", "User not found.");
      return ok(result);
    }

    if (!(a.getLegacyId().equals(userId) || SecurityMgr.isUmappedAdmin(sessionMgr))) {
      ObjectNode result = Json.newObject();
      result.put("msg", "Not Authorized");
      return ok(result);
    }

    try {
      FileSrc fileSrc = FileSrc.find.byId(FileSrc.FileSrcType.ACCOUNT);

      FileInfo file = FileInfo.buildFileInfo(sessionMgr.getAccount().get(), fileSrc, 0);
      file.setExpire(null); //Does not expire
      file.setState(RecordState.PENDING);
      String newKey = Utils.shortenNumber(a.getUid()) + "/" + Utils.shortenNumber(file.getPk()) + "_" + photoName;
      file.setFilepath(newKey);
      file.setFilename(photoName);
      file.setAsPublicUrl();
      file.setFiletype(LstFileType.findFromFileName(photoName));
      file.setFilesize(FileInfo.UNKNOWN_FILESIZE);

      String signedPutUrl = S3Util.authorizeS3Put(fileSrc.getRealBucketName(), newKey);
      S3Data s3Data       = S3Util.authorizeS3(fileSrc.getRealBucketName(), newKey);

      if (signedPutUrl == null) {
        ObjectNode result = Json.newObject();
        result.put("msg", "S3 Signing error");
        return ok(result);
      }
      file.save();
      String nextUrl = routes.UserController.confirmS3PhotoUpload(userId, file.getPk(), isProfile, cmpyId, cmpyMode).url();
      Log.debug("signS3UserPhoto(): Authorized upload for new S3 bucket: "+fileSrc.getRealBucketName()+
                " key:" + newKey);
      ObjectNode result = Json.newObject();
      result.put("signed_request", signedPutUrl);
      result.put("url", nextUrl);
      result.put("policy", s3Data.policy);
      result.put("accessKey", s3Data.accessKey);
      result.put("key", s3Data.key);
      result.put("s3Bucket", s3Data.s3Bucket);
      result.put("signature", s3Data.signature);
      Log.debug("Upload signed response: ", result.toString());
      return ok(result);
    }
    catch (Exception e) {
      Log.err("signS3UserPhoto(): Failed.", e);
      BaseView baseView = new BaseView();
      baseView.message = "System Error - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result confirmS3PhotoUpload(String userId, Long fileId, Boolean isProfile, String cmpyId, Boolean cmpyMode) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Result resRedirect;
    if(isProfile == null) {
      resRedirect = redirect(routes.UserController.profile(userId));
    } else {
      resRedirect = redirect(routes.UserController.user(userId, cmpyId, cmpyMode));
    }

    FileInfo file = FileInfo.find.byId(fileId);
    if(file == null) {
      return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND);
    }

    if(file.getState() != RecordState.PENDING) {
      flash(SessionConstants.SESSION_PARAM_MSG, "File confirmation failed. Wrong file state.");
      return resRedirect;
    }

    if(!sessionMgr.getUserId().equals(userId) || SecurityMgr.isUmappedAdmin(sessionMgr)) {
      UserMessage.message(ReturnCode.AUTH_ACCOUNT_EDIT_FAIL);
    }

    AccountController.AccountHelper ah = AccountController.AccountHelper.build(sessionMgr.getUserId());
    boolean res = ah.fromLegacyId(userId);

    if(!res) {
      flash(SessionConstants.SESSION_PARAM_MSG, "System error. Please try again later.");
      return resRedirect;
    }

    ObjectMetadata fileMeta = S3Util.getMetadata(file.getSource().getRealBucketName(), file.getFilepath());
    if (fileMeta != null) {
      FileInfo otherFile = FileInfo.byHash(fileMeta.getETag());
      if (otherFile != null) { //Someone already has file with this hash
        Log.info("confirmS3PhotoUpload(): Duplicate Photo Upload - using old photo instance");
        otherFile.setState(RecordState.ACTIVE);
        ah.setProfilePhoto(otherFile);
        otherFile.update();
        try {
          S3Util.deleteS3File(file.getSource().getRealBucketName(), file.getFilepath());
          file.delete(); //Since this file is a duplicate of another... deleting this one
        } catch (Exception e) {
          Log.err("Failing to delete S3 bucket: " + file.getSource().getRealBucketName() +
                  " file: " + file.getFilepath() + " fileId: " + file.getPk(), e);
        }
      } else {
        file.setState(RecordState.ACTIVE);
        ah.setProfilePhoto(file);
        file.setHash(fileMeta.getETag());
        file.setFilesize(fileMeta.getContentLength());
        file.update();
        //Making file publicly accessible
        S3Util.setObjectPublic(file.getSource().getRealBucketName(), file.getFilepath());
      }
      flash(SessionConstants.SESSION_PARAM_MSG, "Profile photo upload succeeded.");
    }
    else {
      //Upload failed
      Log.info("confirmS3PhotoUpload(): Upload failed user:" + userId +
               " bucket: " + file.getSource().getRealBucketName() + " path: " + file.getFilepath());
      file.delete();
      flash(SessionConstants.SESSION_PARAM_MSG, "Profile photo upload failed. Please try again");
    }
    return resRedirect;
  }


  @With({Authenticated.class, Authorized.class})
  public Result addExistingUser() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Form<UserForm> userForm = formFactory.form(UserForm.class);
    UserForm userInfoForm = userForm.bindFromRequest().get();

    if (userInfoForm.getInCmpyId() == null ||
        userInfoForm.getInCmpyId().length() == 0 ||
        userInfoForm.getInUserId() == null ||
        userInfoForm.getInUserId().length() == 0) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA);
    }

    Company cmpy = Company.find.byId(userInfoForm.getInCmpyId());
    if (cmpy == null || !(SecurityMgr.isCmpyAdmin(cmpy, sessionMgr) || SecurityMgr.isUmappedAdmin(sessionMgr))) {
      return UserMessage.message(ReturnCode.AUTH_ACCOUNT_EDIT_FAIL);
    }

    if (SecurityMgr.isCmpyAdmin(cmpy, sessionMgr)) {
      //check if we exceed the limit
      int activeCount = AccountCmpyLink.activeCountByCmpyId(cmpy.cmpyid);
      if (activeCount >= cmpy.numaccounts) {
        return UserMessage.message("You have exceeded the maximum number of users - please remove some users or" +
                                   "contact us.");
      }
    }

    Account a = Account.findActiveByLegacyId(userInfoForm.getInUserId());
    if(a == null) {
      return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND);
    }

    if(a.getAccountType() != Account.AccountType.PUBLISHER) {
      return UserMessage.message("Invalid user id. Please correct and try again.");
    }

    //Checking if user belongs to some other comapny already
    AccountCmpyLink acl = AccountCmpyLink.find.byId(a.getUid());
    if(acl != null) {
      if(acl.getCmpyid().equals(cmpy.getCmpyid())) {
        return UserMessage.message("User: " + a.getLegacyId() + " is already linked to this company.");
      } else {
        return UserMessage.message("User: " + a.getLegacyId() + " is already linked to another company.");
      }
    }

    AccountController.AccountHelper ah = AccountController.AccountHelper.build(sessionMgr.getUserId());
    ah.fromLegacyId(a.getLegacyId());

    boolean res = ah.setAccount(a).linkToCompany(cmpy, userInfoForm.getInUserType());

    if(!res) {
      return UserMessage.message(ReturnCode.UNHANDLED_EXCEPTION);
    }

    //res = ah.sendLinkEmail();

    if(!res) {
      flash(SessionConstants.SESSION_PARAM_MSG, "User: " + userInfoForm.getInUserId() + " linked to " + cmpy.name + "" +
                                                " but email notification failed.");

    } else {
      flash(SessionConstants.SESSION_PARAM_MSG, "User: " + userInfoForm.getInUserId() + " linked to " + cmpy.name +
                                                "and notified via Email");
    }

    flash("inCmpyId", userInfoForm.getInCmpyId());
    flash("inCmpyMode", userInfoForm.getInCmpyMode());
    return redirect(routes.UserController.users());
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result newUserWelcomEmail() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<UserForm> userForm = formFactory.form(UserForm.class);
    UserForm userInfoForm = userForm.bindFromRequest().get();

    if (userInfoForm.getInUserId() == null || !SecurityMgr.isUmappedAdmin(sessionMgr)) {
      return UserMessage.message(ReturnCode.AUTH_ACCOUNT_EDIT_FAIL);
    }

    UserProfile u = UserProfile.find.byId(userInfoForm.getInUserId());
    Account a = Account.findByLegacyId(userInfoForm.getInUserId());
    if(a == null || u== null) {
      return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND, "User not found");
    }

    if(u.status == APPConstants.STATUS_DELETED) {
      return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND, "User not found");
    }


    if (u.lastlogintimestamp.longValue() != u.createdtimestamp.longValue()) {
      return UserMessage.message("User Active - cannot send welcome email");
    }

    AccountController.AccountHelper ah = AccountController.AccountHelper.build(sessionMgr.getUserId());
    ah.setAccount(a).setUserProfile(u);
    ah.lockAccount();
    boolean res = ah.sendWelcomeEmail();
    if(!res) {
      return UserMessage.message("Failure while sending Welcome message");
    }

    return UserMessage.message(ReturnCode.SUCCESS, "Welcome Email sent successfully.");
  }

  /*
   * search for all invites
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result invites() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    UserLinkInviteForm form = (formFactory.form(UserLinkInviteForm.class)).bindFromRequest().get();
    InviteView view = new InviteView();
    if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
      view.isUMappedAdmin = true;
    }

    if (form.getInTerm() != null && form.getInTerm().trim().length() > 0) {
      view.term = form.getInTerm();
      List<UserInvite> invites = UserInvite.findActiveByKeyword(form.getInTerm().trim());
      if (invites != null && invites.size() > 0) {
        view.invites = new ArrayList<>();
        long currentTimestamp = System.currentTimeMillis();
        for (UserInvite invite : invites) {
          InviteInfoView inviteInfoView = new InviteInfoView();
          inviteInfoView.inviteId = invite.getPk();
          inviteInfoView.email = invite.getEmail();
          inviteInfoView.firstName = invite.getFirstname();
          inviteInfoView.lastName = invite.getLastname();
          inviteInfoView.expiryDate = Utils.getDateStringPrint(invite.getExpirytimestamp());
          inviteInfoView.createdTimestamp = Utils.formatDateTimePrint(invite.getCreatedtimestamp());
          if (invite.getExpirytimestamp() < currentTimestamp)  {
            inviteInfoView.isExpired = true;
          }
          inviteInfoView.createdBy = invite.getCreatedby();
          view.invites.add(inviteInfoView);
        }

      } else {
        view.message = "No Invites found - please try again.";
      }
    }

    return ok(views.html.user.invites.render(view));
  }

  /*
   * search for all invites
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result invite() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    List<UserProfileRS> users = null;


    //bind html form to form bean
    UserLinkInviteForm form = (formFactory.form(UserLinkInviteForm.class)).bindFromRequest().get();
    InviteView view = new InviteView();
    if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
      view.isUMappedAdmin = true;
    }

    if (form.getInInviteId() != null) {
      UserInvite invite = UserInvite.find.byId(form.getInInviteId());
      if (invite != null) {
        InviteInfoView inviteInfoView = new InviteInfoView();
        inviteInfoView.inviteId = invite.getPk();
        inviteInfoView.email = invite.getEmail();
        inviteInfoView.firstName = invite.getFirstname();
        inviteInfoView.lastName = invite.getLastname();
        inviteInfoView.expiryDate = Utils.getDateStringPrint(invite.getExpirytimestamp());
        inviteInfoView.createdTimestamp = Utils.formatDateTimePrint(invite.getCreatedtimestamp());
        if (invite.getExpirytimestamp() < System.currentTimeMillis())  {
          inviteInfoView.isExpired = true;
        }
        inviteInfoView.createdBy = invite.getCreatedby();
        view.selectedInvite = inviteInfoView;
      }
    }

    if (form.getInTerm() != null && form.getInTerm().trim().length() > 0) {
      view.term = form.getInTerm();
      Connection conn = null;
      try {
        conn = DBConnectionMgr.getConnection4Publisher();
        users = UserProfileMgr.findByTerm(form.getInTerm(), conn);
      }
      catch (Exception e) {
        e.printStackTrace();
        view.message = "System Error";
        return ok(views.html.common.message.render(view));
      }
      finally {
        try {
          if (conn != null) {
            conn.close();
          }
        }
        catch (Exception s) {
          s.printStackTrace();
        }
      }
      if (users != null && users.size() > 0) {
        ArrayList<UserView> userViews = new ArrayList<UserView>();
        for (UserProfileRS u : users) {
          if (u.getUsertype() != APPConstants.USER_ACCOUNT_TYPE_UMAPPED) {
            UserView uv = new UserView();
            uv.userId = u.getUserid();
            uv.firstName = u.getFirstname();
            uv.lastName = u.getLastname();
            uv.email = u.getEmail();
            uv.phone = u.getPhone();
            uv.mobile = u.getMobile();
            uv.fax = u.getFax();
            uv.web = u.getWeb();
            uv.facebook = u.getFacebook();
            uv.twitter = u.getTwitter();
            uv.profilePhoto = u.getProfilephoto();
            uv.profilePhotoUrl = u.getProfilephotourl();
            Long lastLoginTs = getLastLoginTime(sessionMgr, u.getUserid());

            uv.lastLoginTimestamp = lastLoginTs != null ? String.valueOf(lastLoginTs) : "";
            if (lastLoginTs == null) {
              uv.lastLoginPrint = "No Logins Yet.";
            }
            else {
              uv.lastLoginPrint =  Utils.formatTimestampPrint(lastLoginTs);
            }

            if (uv.firstName != null) {
              uv.name = uv.firstName + " ";
            }
            else {
              uv.name = "";
            }

            uv.cmpyLinkType = u.getCmpyLinkType();

            if (uv.lastName != null) {
              uv.name = uv.name + uv.lastName;
            }
            userViews.add(uv);
          }
        }
        view.users = userViews;

      }
      if (view.users == null || view.users.size() == 0) {
        view.message = "No Users found - please try again.";
      }
    }

    return ok(views.html.user.linkUserInvite.render(view));
  }

  /**
   * Link a user to an invite and update all trip records for collaboration
   */
  @With({Authenticated.class, AdminAccess.class})
  public Result doLinkUserInvite() {

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    UserLinkInviteForm form = (formFactory.form(UserLinkInviteForm.class)).bindFromRequest().get();
    InviteView view = new InviteView();
    if (SecurityMgr.isUmappedAdmin(sessionMgr)) {
      view.isUMappedAdmin = true;
    }

    if (form.getInInviteId() == null || form.getInUserId() == null ) {
      view.message = "Invalid Request";
      return ok(views.html.user.invites.render(view));
    }
    UserInvite invite = UserInvite.find.byId(form.getInInviteId());
    UserProfile user = UserProfile.find.byId(form.getInUserId());

    if (invite == null || invite.getState() == UserInvite.InviteState.EXPIRED || invite.getState() == UserInvite.InviteState.ACCEPTED) {
      view.message = "Invalid Request";
      return ok(views.html.user.invites.render(view));
    }

    if (user == null || user.getStatus() == APPConstants.STATUS_DELETED || user.getUsertype() == APPConstants.USER_ACCOUNT_TYPE_UMAPPED) {
      view.message = "Invalid Request";
      return ok(views.html.user.invites.render(view));
    }

    int count = 0;

    try {
      Ebean.beginTransaction();
      //update invite
      invite.setNewuserid(user.getUserid());
      //Forced state
      invite.setState(UserInvite.InviteState.ACCEPTED);
      invite.setModifiedtimestamp(System.currentTimeMillis());
      invite.save();

      //update all trips
      count = UserController.linkTripsToUser(user.getUserid(), invite.getPk(), sessionMgr.getUserId());

      Ebean.commitTransaction();

    } catch (Exception e) {
      Ebean.rollbackTransaction();
      view.message = "A System Error has occurred. Please try again.";
      return ok(views.html.user.invites.render(view));
    }
    view.message = "Invitation for " + invite.getFirstname() + " " + invite.getLastname()  + " succesfully linked to " + user
        .getUserid() + " and " + count + " trips have been linked";
    return ok(views.html.user.invites.render(view));
  }

  public static UserView getUserView(String userId) {
    UserView userView = (UserView) CacheMgr.get(APPConstants.CACHE_USER_VIEW_PREFIX + userId);

    if (userView == null && userId != null) {
      UserProfile u = UserProfile.find.byId(userId);
      if (u != null) {
        userView = new UserView();
        userView.userId = u.getUserid();
        userView.firstName = u.getFirstname();
        userView.lastName = u.getLastname();
        userView.email = u.getEmail();
        userView.phone = u.getPhone();
        userView.mobile = u.getMobile();
        userView.fax = u.getFax();
        userView.web = u.getWeb();
        userView.facebook = u.getFacebook();
        userView.twitter = u.getTwitter();
        userView.profilePhoto = u.getProfilephoto();
        userView.profilePhotoUrl = u.getProfilephotourl();
        Timestamp lastLoginTs = AccountAuth.getLastSuccessLoginTs(userId);
        userView.lastLoginTimestamp = lastLoginTs != null ? String.valueOf(lastLoginTs.getTime()) : "" ;
        if (lastLoginTs == null) {
          userView.lastLoginPrint = "No Logins Yet.";
        }
        else {
          userView.lastLoginPrint = Utils.formatTimestampPrint(lastLoginTs.getTime());
        }

        userView.status = u.status;
        userView.intUserType = u.getUsertype();
        userView.isExistingUser = true;
        if (userView.firstName != null) {
          userView.name = userView.firstName + " ";
        }
        else {
          userView.name = "";
        }

        if (userView.lastName != null) {
          userView.name = userView.name + userView.lastName;
        }



        try {
          CacheMgr.set(APPConstants.CACHE_USER_VIEW_PREFIX + userId, userView);
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }
    }

    return userView;

  }

  public static void invalidateCache(String userId) {
    if (userId != null) {
      try {
        CacheMgr.set(APPConstants.CACHE_USER_VIEW_PREFIX + userId, null, APPConstants.CACHE_EXPIRY_SECS);
      }
      catch (Exception e) {
        Log.err("invalidateCache (users): " + userId, e);
      }
    }
  }

  //associate all trips to a user id from an inviteid
  public static int linkTripsToUser (String userId, String inviteId, String modifyUserId) {
    int count = 0;
    int failed = 0;

    List <TripInvite> invites = TripInvite.getAllActiveInvites(inviteId);

    if (invites != null) {
      for (TripInvite tripInvite:invites) {
        try {
          Trip trip = Trip.findByPK(tripInvite.getTripid());
          TripShare tripShare = new TripShare();
          tripShare.setPk(Utils.getUniqueId());
          tripShare.setTripid(tripInvite.tripid);
          tripShare.setUser(UserProfile.findByPK(userId));
          tripShare.setStatus(APPConstants.STATUS_ACTIVE);
          tripShare.setAccesslevel(tripInvite.getAccesslevel());
          tripShare.setLastupdatedtimestamp(System.currentTimeMillis());
          tripShare.setModifiedby(modifyUserId);
          tripShare.setCreatedby(tripInvite.getCreatedby());
          tripShare.setNote("");
          tripShare.setSenttimestamp(System.currentTimeMillis());
          tripShare.setCreatedtimestamp(System.currentTimeMillis());
          tripShare.save();
          count++;
        } catch (Exception e) {
          Log.err("linkTripsToUser- Cannot link Trip: " + tripInvite.tripid + " to " + userId, e);
          failed++;
        }
      }
      //mark all invites as completed.
      TripInvite.updateAllInviteId(inviteId, modifyUserId);
    }

    if (failed > 0) {
      Log.err("linkTripsToUser- link Trip Failues: " + failed + " UserId: " + userId + " InviteId: " + inviteId);

    }

    return count;
  }

  @With({Authenticated.class, Authorized.class})
  public Result findUser() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());


    final DynamicForm form = formFactory.form().bindFromRequest();
    String inTripId = form.get("inTripId");
    String inUsername = form.get("inUsername");
    String inDivName = form.get("inDivName");
    if (inDivName == null || inDivName.length() <= 0) {
      inDivName = "agentSearchResults";
    }

    if (inTripId == null || inUsername == null || inTripId.length() <= 0 || inUsername.length() <= 0) {
      return ok(views.html.trip.collaborate.searchError.render(inDivName, "Error handling the search request."));
    }

    Trip tripModel = Trip.findByPK(inTripId);
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND_N_PUBLISH) ||
        !SecurityMgr.canInviteOthers(inTripId,sessionMgr.getUserId(), accessLevel)) {
      return ok(views.html.trip.collaborate.searchError.render            (inDivName,
                                                               "You are not authorized to invite collaborators for " +
                                                               "this trip"));
    }


    UserProfile userProfile = null;
    if (Utils.isValidEmailAddress(inUsername)) {
      List<UserProfile> userList = UserProfile.findActiveByEmail(inUsername);
      if (userList != null && userList.size() > 0) {
        userProfile = userList.get(0);
      }
    }
    else {
      userProfile = UserProfile.findUserCaseInsentive(inUsername);
    }
    //User found => Allow to proceed adding to the trip
    if (userProfile != null) {
      List<UserCmpyLink> cmpyAdmins = UserCmpyLink.getCompanyAdministrators(tripModel.cmpyid);
      for (UserCmpyLink cl : cmpyAdmins) {
        if (cl.user.userid.equals(userProfile.userid)) {
          return ok(views.html.trip.collaborate.searchError.render                                                                       (inDivName,
                                                                   "You can not invite company administrators to collaborate on the trip"));
        }
      }

      if (userProfile.userid.equals(tripModel.createdby)) {
        return ok(views.html.trip.collaborate.searchError.render                                                              (inDivName,
                                                                 "You can not invite trip creators to collaborate on the trip"));
      }


      CollaboratorInviteView collaboratorInviteView = new CollaboratorInviteView();
      collaboratorInviteView.userProfile = userProfile;
      collaboratorInviteView.targetDivId = inDivName;
      collaboratorInviteView.currUserAccessLevel = accessLevel;
      return ok(views.html.trip.collaborate.profile.render(collaboratorInviteView));
    }
    else //User not found => Invite
    {
      CollaboratorUserInviteView collaboratorUserInviteView = new CollaboratorUserInviteView();
      collaboratorUserInviteView.targetDivId = inDivName;
      collaboratorUserInviteView.email = inUsername;
      collaboratorUserInviteView.currUserAccessLevel  =accessLevel;
      return ok(views.html.trip.collaborate.invite.render(collaboratorUserInviteView));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result inviteSharedTripUser() {
    /*
     * Steps to add user to a trip:
     * 1. Find trip from session
     * 2. Find user from "shareUserId" form value
     * 3. Find AccessLevel from "inAccessLevel"
     * 4. Find note to the user from "inShareNote"
     * 5. Build new trip share object with those parameters
     * 6. Save to the db
     * 7. If shared users' session exists append shared trip to his shared trip list
     * 8. Send e-mail and if trip is published, send itinerary email
     * 9. Display share result
     */

    FormInviteSharedTripUser form       = (formFactory.form(FormInviteSharedTripUser.class)).bindFromRequest().get();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Trip tripModel = Trip.findByPK(sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID));
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND_N_PUBLISH) ||
        !SecurityMgr.canInviteOthers(tripModel.tripid,sessionMgr.getUserId(),accessLevel)) {
      //Clearly someone exploits the system here as all previous search forms, etc check OWNER access level
      return ok("<div id='inviteResult' class='alert alert-error'>No permission to add user for this trip</div>");
    }



    UserProfile senderUser = UserProfile.findByPK(sessionMgr.getUserId());

    if (!form.isValid() || senderUser == null) {
      return ok("<div id='inviteResult' class='alert alert-warning'><em>Error parsing request.</em> Refresh browser and try again.</div>");
    }

    if (!Utils.isValidEmailAddress(form.inEmail)) {
      return ok(views.html.trip.collaborate.searchError.render("inviteResult", "Invalid e-mail submitted"));
    }

    //check to see if this email address is already linked to a user
    List<UserProfile> users = UserProfile.findActiveByEmail(form.inEmail);
    if (users != null && users.size() > 0) {
      if (users.size() == 1) {
        //process as a regular invite
        flash(SessionConstants.SESSION_PARAM_USERID, users.get(0).getUserid());
        flash(SessionConstants.SESSION_PARAM_NOTES, form.inNote);
        flash(SessionConstants.SESSION_PARAM_ACCESS_LEVEL, form.inAccessLevel.name());
        return redirect(routes.SharedTripsController.shareTripWithUser());
      } else {
        //should not happen
        return ok(views.html.trip.collaborate.searchError.render("inviteResult", "An account with this email already exists. Please try your search again."));

      }
    }

    if (form.inAccessLevel.gt(accessLevel)) {
      return ok("<div id='inviteResult' class='alert alert-error'>Can not invite others with access rights more permissive than your own.</div>");
    }

    /* Building user invite object */
    UserInvite userInvite = UserInvite.findByEmail(form.inEmail);
    TripInvite tripInvite = null;
    if (userInvite != null) {
      tripInvite = TripInvite.findByTripAndInvitee(tripModel.tripid, userInvite.getPk());
    }

    //New invite
    boolean newInvite = false;
    if (tripInvite == null) {
      if (userInvite == null) {
        userInvite = UserInvite.buildUserInvite(sessionMgr.getUserId(), form.inFirstName,
                                                form.inLastName, form.inEmail);
        userInvite.save();
      }
      tripInvite = TripInvite.buildTripInvite(sessionMgr.getUserId(), tripModel, userInvite,
                                              form.inAccessLevel, form.inNote);
      newInvite = true;
      tripInvite.save();
    }
    else {
      tripInvite.setAccesslevel(form.inAccessLevel);
      if (form.inNote.length() > 0) {
        tripInvite.setNote(form.inNote);
      }
      tripInvite.setModifiedby(sessionMgr.getUserId());
      tripInvite.setStatus(APPConstants.STATUS_ACTIVE);
      tripInvite.setLastupdatedtimestamp(System.currentTimeMillis());
      tripInvite.update();
    }

    //Create audit log
    audit:
    {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_COLLABORATION,
                                           newInvite ? AuditActionType.ADD : AuditActionType.MODIFY,
                                           AuditActorType.WEB_USER)
                              .withTrip(tripModel)
                              .withCmpyid(tripModel.cmpyid)
                              .withUserid(sessionMgr.getUserId());

      ((AuditCollaboration) ta.getDetails()).withAction(AuditCollaboration.CollaboraionAction.INVITE)
                                            .withTripInvite(tripInvite);
      ta.save();;
    }

    sendNewUserTripShareInviteEmail(sessionMgr.getCredentials().isCmpyMember(tripModel.getCmpyid()), tripInvite, tripModel, senderUser);


    if (userInvite != null) {

      //if the trip is already published, send the trip itinerary email
      try {
        //if trip has already been published, send the email notification now
        //create share  + invite

        if (TripPublishHistory.countPublished(tripModel.getTripid()) > 0) {
          ArrayList agents = new ArrayList<>();
          TripAgent tripAgent = new TripAgent();
          tripAgent.setName(userInvite.getFirstname() + " " + userInvite.getLastname());
          tripAgent.setStatus(APPConstants.STATUS_ACTIVE);
          tripAgent.setEmail(userInvite.getEmail());

          agents.add(tripAgent);
          int returnCode = TripController.sendUMappedAgentEmailNotitication(agents, tripModel, senderUser, null, null);
          if (returnCode != 0) {
            flash(SessionConstants.SESSION_PARAM_MSG, "Error - cannot send notification email");
          }
        }



    }
    catch(Exception e){

    }
  }

    return ok("<div id='inviteResult' class='alert alert-success'>User added to the trip</div>");
  }

  @With({Authenticated.class, Authorized.class})
  public Result resendTripInviteEmail() {
    FormTripInviteChangeRequest form       = (formFactory.form(FormTripInviteChangeRequest.class)).bindFromRequest().get();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Trip tripModel = Trip.findByPK(sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID));
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND_N_PUBLISH)) {
      //Clearly someone exploits the system here as all previous search forms, etc check OWNER access level
      BaseView baseView = new BaseView();
      baseView.message = "Permission Error: No permission to resend invitation to this user for this trip.";
      return ok(views.html.common.message.render(baseView));
    }

    if(!form.isValid()){
      BaseView baseView = new BaseView();
      baseView.message = "Error parsing request. Refresh browser and try again.";
      return ok(views.html.common.message.render(baseView));
    }

    UserProfile senderUser = UserProfile.findByPK(sessionMgr.getUserId());

    TripInvite tripInvite = TripInvite.findByPk(form.inInviteId);

    if (accessLevel.lt(SecurityMgr.AccessLevel.OWNER) && !tripInvite.createdby.equals(sessionMgr.getUserId())) {
      BaseView baseView = new BaseView();
      baseView.message = "Permission Error: No permission to resend invitation to users invited by others.";
      return ok(views.html.common.message.render(baseView));
    }

    if (tripInvite == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Error parsing request. Refresh browser and try again.";
      return ok(views.html.common.message.render(baseView));
    }

    long currTime = System.currentTimeMillis();

    tripInvite.setModifiedby(senderUser.getUserid());
    tripInvite.setLastupdatedtimestamp(currTime);
    tripInvite.userInvite.setModifiedtimestamp(currTime);
    tripInvite.userInvite.setExpirytimestamp(currTime + UserInvite.expireOffset);
    tripInvite.userInvite.setSenttimestamp(currTime);
    tripInvite.userInvite.transitionState(UserInvite.InviteEvents.E_REINVITE);
    tripInvite.userInvite.update();
    if (form.inInviteNote != null && form.inInviteNote.trim().length() > 0) {
      tripInvite.setNote(form.inInviteNote.trim());
    }
    tripInvite.update();

    sendNewUserTripShareInviteEmail(sessionMgr.getCredentials().isCmpyMember(tripModel.getCmpyid()), tripInvite, tripModel, senderUser);

    //Create audit log
    audit:
    {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_COLLABORATION,
                                           AuditActionType.MODIFY,
                                           AuditActorType.WEB_USER)
                              .withTrip(tripModel)
                              .withCmpyid(tripModel.cmpyid)
                              .withUserid(sessionMgr.getUserId());

      ((AuditCollaboration) ta.getDetails()).withAction(AuditCollaboration.CollaboraionAction.RESEND_INVITE)
                                            .withTripInvite(tripInvite);
      ta.save();;
    }

    BaseView baseView = new BaseView();
    baseView.message = "Trip invitation has been resent.";
    return ok(views.html.common.message.render(baseView));
  }

  /**
   * Send trip sharing information to the user
   *
   * @param tripInvite information about invite
   * @param tripModel  trip details, including company to which it belongs
   * @param senderUser sender user profile
   * @return
   */
  public static boolean sendNewUserTripShareInviteEmail(boolean isCmpyMember,
                                                        TripInvite tripInvite,
                                                        Trip tripModel,
                                                        UserProfile senderUser) {

    if (tripInvite == null || tripModel == null || senderUser == null) {
      return false;
    }

    String smtp = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_HOST);
    String user = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_USER_ID);
    String pwd = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_PASSWORD);

    String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);
    String emailEnabled = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_ENABLED);


    if (emailEnabled == null || !emailEnabled.equalsIgnoreCase("Y")) {
      Log.log(LogLevel.INFO, "Email disabled");
      return false;
    }

    String sender = null;
    String umappedShifted = null;

    /* If current user is a member of the company who created the trip send company name in the invite,
     * otherwise just send his/her name */
    if (isCmpyMember) {
      Company cmpy = Company.find.byId(tripModel.getCmpyid());
      if (cmpy != null && cmpy.name != null && cmpy.name.trim().length() > 0) {
        umappedShifted = cmpy.name;
        sender = cmpy.name;
      }
    }

    if (umappedShifted == null) {
      umappedShifted = senderUser.getFirstname() + " " + senderUser.getLastname();
      sender = umappedShifted;
    }

    sender += " via Umapped";
    BusinessCardView bcv = new BusinessCardView();

    String emailBody = views.html.trip.collaborate.emailBodyInvite.render        (bcv, tripModel,
                                                                          tripInvite,
                                                                          senderUser,
                                                                          umappedShifted,
                                                                          hostUrl).toString();
    String fromAddress = "no-reply@umapped.com";//senderUser.getEmail();
    List<String> toEmails = new ArrayList<String>();
    toEmails.add(tripInvite.getUserInvite().getEmail());
    String subject = senderUser.getFullName() + " wants to collaborate with you";


    List<Map<String, String>> body = new ArrayList<Map<String, String>>();
    Map<String, String> token = new HashMap<String, String>();
    token.put(com.mapped.common.CoreConstants.HTML, emailBody);
    body.add(token);

    Log.log(LogLevel.DEBUG,
            "Sending trip share invite e-mail to:<" + tripInvite.getUserInvite()
                                                                .getEmail() + "> tripId:" + tripModel.tripid + " " +
            "accessLevel:" + tripInvite.accesslevel);

    EmailMgr emailMgr = new EmailMgr(smtp, user, pwd);
    try {
      emailMgr.init();
      emailMgr.send(fromAddress, sender, toEmails, subject, body);
    }
    catch (Exception e) {
      Log.err("Failure sending trip share e-mail about trip: " + tripModel.tripid, e);
      return false;
    }

    return true;
  }

  @With({Authenticated.class, Authorized.class})
  public Result revokeTripInviteFromNewUser() {
    /*
     * Steps to revoke user's access to the trip:
     * 1. Check that user has rights to revoke share from this specific user (TODO: What are the rules?)
     * 2. Find trip
     * 3. Retrieve trip invite information
     * 4. Mark status INACTIVE, access level to None
     * 5. Save tripinvite
     * 6. Redirect to trip info page
     */

    FormTripInviteChangeRequest form = (formFactory.form(FormTripInviteChangeRequest.class)).bindFromRequest().get();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Trip tripModel = Trip.findByPK(sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID));
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND_N_PUBLISH)) {
      //Clearly someone exploits the system here as all previous search forms, etc check OWNER access level
      BaseView baseView = new BaseView();
      baseView.message = "Permission Error: You are not permitted to revoke access rights for this user.";
      return ok(views.html.common.message.render(baseView));
    }

    if(!form.isValid()){
      BaseView baseView = new BaseView();
      baseView.message = "System Error: Request can not be understood.";
      return ok(views.html.common.message.render(baseView));
    }

    TripInvite tripInvite = TripInvite.findByPk(form.inInviteId);

    if (accessLevel.lt(SecurityMgr.AccessLevel.OWNER) && !tripInvite.createdby.equals(sessionMgr.getUserId())) {
      BaseView baseView = new BaseView();
      baseView.message = "Permission Error: You are not permitted to revoke access rights for users invited by others.";
      return ok(views.html.common.message.render(baseView));
    }

    if (tripInvite== null) {
      BaseView baseView = new BaseView();
      baseView.message = "Internal Error. Please refresh this page and resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    tripInvite.setAccesslevel(SecurityMgr.AccessLevel.NONE);
    tripInvite.setStatus(APPConstants.STATUS_DELETED);
    tripInvite.setModifiedby(sessionMgr.getUserId());
    tripInvite.setLastupdatedtimestamp(System.currentTimeMillis());
    tripInvite.update();

    //Create audit log
    audit:
    {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_COLLABORATION,
                                           AuditActionType.DELETE,
                                           AuditActorType.WEB_USER)
                              .withTrip(tripModel)
                              .withCmpyid(tripModel.cmpyid)
                              .withUserid(sessionMgr.getUserId());

      ((AuditCollaboration) ta.getDetails()).withAction(AuditCollaboration.CollaboraionAction.REVOKE_INVITE)
                                            .withTripInvite(tripInvite);
      ta.save();;
    }

    flash(SessionConstants.SESSION_PARAM_MSG, "Successfully revoked the invite for " +
                                              tripInvite.userInvite.getFirstname() + " " +
                                              tripInvite.userInvite.getLastname());
    return redirect(routes.TripController.tripInfo(tripModel.tripid));
  }

  @With({Authenticated.class, Authorized.class})
  public Result changeTripInviteAccessLevel() {

    FormChangeTripInviteAccessLevel form       = (formFactory.form(FormChangeTripInviteAccessLevel.class)).bindFromRequest().get();
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Trip tripModel = Trip.findByPK(sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID));
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND_N_PUBLISH)) {
      //Clearly someone exploits the system here as all previous search forms, etc check OWNER access level
      BaseView baseView = new BaseView();
      baseView.message = "Permission Error: You are not permitted to chance access rights for this user.";
      return ok(views.html.common.message.render(baseView));
    }

    if(!form.isValid()){
      BaseView baseView = new BaseView();
      baseView.message = "System Error: Request can not be understood.";
      return ok(views.html.common.message.render(baseView));
    }




    TripInvite tripInvite = TripInvite.findByPk(form.inInviteId);
    if (tripInvite == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Internal Error. Please refresh this page and resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    if (accessLevel.lt(SecurityMgr.AccessLevel.OWNER) ||
        form.inAccessLevel.gt(accessLevel) ||
        !tripInvite.createdby.equals(sessionMgr.getUserId())) {
      BaseView baseView = new BaseView();
      baseView.message = "Permission Error: You are not permitted to chance access rights for users invited by others or to be more permissive than your own rights.";
      return ok(views.html.common.message.render(baseView));
    }

    tripInvite.setAccesslevel(form.inAccessLevel);
    tripInvite.setModifiedby(sessionMgr.getUserId());
    tripInvite.setLastupdatedtimestamp(System.currentTimeMillis());
    tripInvite.update();

    //Create audit log
    audit:
    {
      TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_COLLABORATION,
                                           AuditActionType.MODIFY,
                                           AuditActorType.WEB_USER)
                              .withTrip(tripModel)
                              .withCmpyid(tripModel.cmpyid)
                              .withUserid(sessionMgr.getUserId());

      ((AuditCollaboration) ta.getDetails()).withAction(AuditCollaboration.CollaboraionAction.CHANGE_INVITE_ACCESS)
                                            .withTripInvite(tripInvite);
      ta.save();;
    }

    flash(SessionConstants.SESSION_PARAM_MSG, "Successfully changed access permissions for " +
                                              tripInvite.userInvite.getFirstname() + " " +
                                              tripInvite.userInvite.getLastname());
    return redirect(routes.TripController.tripInfo(tripModel.tripid));
  }

  /**
   * Return list of companies user belongs to
   */
  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result userCompanies() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    RequestMessageJson jsonRequest = RequestMessageJson.fromJson(request().body().asJson());
    ResponseMessageJson jsonResponse = null;

    CompanyJson companyJson = (CompanyJson) jsonRequest.body;
    switch(companyJson.operation) {
      case ADD:
      case COPY:
        jsonResponse = new ResponseMessageJson(jsonRequest.header, ReturnCode.NOT_IMPLEMENTED);
        break;
      case UPDATE:
        jsonResponse = new ResponseMessageJson(jsonRequest.header, ReturnCode.NOT_IMPLEMENTED);
        break;
      case DELETE:
        jsonResponse = new ResponseMessageJson(jsonRequest.header, ReturnCode.NOT_IMPLEMENTED);
        break;
      case LIST:
      case SEARCH:
        Credentials creds = sessionMgr.getCredentials();
        jsonResponse = new ResponseMessageJson(jsonRequest.header, ReturnCode.SUCCESS);
        CompanyJson compBody =  new CompanyJson();
        jsonResponse.body = compBody;
        compBody.operation = Operation.LIST;
        compBody.searchTerm = sessionMgr.getUserId();

        CompanyJson.CompanyDetailsJson cdj = new CompanyJson.CompanyDetailsJson();
        cdj.cmpyId = Company.PUBLIC_COMPANY_ID;
        cdj.name = "Public";
        cdj.legacyId = String.valueOf(Company.PUBLIC_COMPANY_ID);
        if (creds.isUmappedAdmin()) {
          cdj.userLinkType = UserCmpyLink.LinkType.ADMIN;
        } else {
          cdj.userLinkType = UserCmpyLink.LinkType.SUSPENDED;
        }

        compBody.addCompany(cdj);

        if(creds.hasCompany()) {
          cdj = new CompanyJson.CompanyDetailsJson();
          cdj.cmpyId = creds.getCmpyIdInt();
          cdj.name = creds.getCmpyName();
          cdj.legacyId = creds.getCmpyId();
          cdj.userLinkType = UserCmpyLink.LinkType.fromAccountLinkType(creds.getCmpyLinkType());
          compBody.addCompany(cdj);
        }

        break;
    }

    Log.debug("Resp:" + jsonResponse.toJson());
    return ok(jsonResponse.toJson());
  }

  /**
   * jQuery Autocomplete Style Result
   * @param userId
   * @return
   */
  @With({Authenticated.class, Authorized.class})
  public Result userCoworkersAutocomplete(String userId, String term, String cmpyId) {
    ObjectNode result = Json.newObject();
    ArrayNode array = result.arrayNode();
    List<UserProfile> cWorkers;
    if(cmpyId != null) {
      cWorkers = UserProfile.getCoworkersByCmpyAndName(cmpyId,term);
    } else {
      cWorkers = UserProfile.getCoworkersByIdandName(userId, term);
    }
    for (UserProfile c : cWorkers) {
      ObjectNode classNode = Json.newObject();
      classNode.put("label", c.getFirstname() + " " + c.getLastname() + " ("+ c.getUserid()+")");
      classNode.put("id",   c.getUserid());
      array.add(classNode);
    }

    return ok(array);
  }

  /**
   * jQuery Autocomplete Style Result
   * @param userId
   * @return
   */
  @With({Authenticated.class, Authorized.class})
  public Result userCompaniesAutocomplete(String userId, String term, String cmpyId) {
    ObjectNode result = Json.newObject();
    ArrayNode array = result.arrayNode();

    if (cmpyId != null) {
      Company c = Company.find.byId(cmpyId);
      ObjectNode classNode = Json.newObject();
      classNode.put("label", c.getName());
      classNode.put("id", cmpyId);
      array.add(classNode);
      if(c.getParentCmpyId() != null) {
        classNode = Json.newObject();
        Company p = Company.find.byId(c.getParentCmpyId());
        classNode.put("label", p.getName() + " [PARENT]");
        classNode.put("id", p.getCmpyid());
        array.add(classNode);
      }
    } else {
      List<UserCmpyLink> cLinks = UserCmpyLink.findActiveByUserIdAndCmpyName(userId, term);
      for (UserCmpyLink l : cLinks) {
        ObjectNode classNode = Json.newObject();
        classNode.put("label", l.cmpy.getName());
        classNode.put("id", l.cmpy.getCmpyid());
        array.add(classNode);
        if(l.cmpy.getParentCmpyId() != null) {
          classNode = Json.newObject();
          Company p = Company.find.byId(l.cmpy.getParentCmpyId());
          classNode.put("label", p.getName() + " [PARENT]");
          classNode.put("id", p.getCmpyid());
          array.add(classNode);
        }
      }
    }
    return ok(array);
  }

  private static Long getLastLoginTime(SessionMgr sessionMgr, String userId) {
    Timestamp lastSuccessLoginTs = AccountAuth.getLastSuccessLoginTs(userId);

    if (lastSuccessLoginTs != null) {
      long localTimezone = lastSuccessLoginTs.getTime() + (sessionMgr.getTimezoneOffsetMins() * 60 * 1000);
      return localTimezone;
    } else {
      return null;
    }
  }
}
