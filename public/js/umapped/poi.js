var currEditorUI = null;
var initPois = function() {
  clearUserCompanies();
  var editorUI = Object.create(PoiEditorUI);
  currEditorUI = editorUI; //TODO: Temporary hack to access via Global variable
  editorUI.init('poiEditRegion', false);
  editorUI.hide();

  //Initialize poi storage and request recently updated if no past searches
  poiStorageInit(); //Must init storage first

  //Top menu initialization
  $('#poiMenu').unbind("click").click(function(){
    //Requests the last executed search query again, will remove newly added pois compared to previous behaviour
    poiStorageRefresh();
    editorUI.hide();
    //hide any messages
    headLineAlertHide();
    return false;
  });

  $('#newPoiMenu').unbind("click").click(function(){
    editorUI.newPoi();
    return false;
  });

  $('#poiButtonNew').unbind("click").click(function(){
    editorUI.newPoi();
    return false;
  });

  $('#poiSearchTypes').change(poiSearch);
  $('#inCmpyId').change(poiSearch);

  //For search line using search as you type method with delay of 1/2 second after last letter was typed to perform
  //next search request
  var poiTimeout;
  $("#poiSearchTerm").on("keyup", function () {
    clearTimeout(poiTimeout);
    poiTimeout = setTimeout(function(){
      poiSearch();
    }, 500);
  });
};

var FormState = {
  NEW       : 0,
  READ_ONLY : 1,
  EDITABLE  : 2,
  DELETABLE : 3
};

var PoiEditorButtonsUI = {

  init: function(idPrefix, isEmbedded) {
    this.btnAddPhotoArea  = $('#' + idPrefix + '_poiButtonAreaAddPhoto');
    this.btnAddPhoto      = $('#' + idPrefix + '_poiButtonAddPhoto');
    this.btnDeleteArea    = $('#' + idPrefix + '_poiButtonAreaDelete');
    this.btnDelete        = $('#' + idPrefix + '_poiButtonDelete');
    this.btnSaveArea      = $('#' + idPrefix + '_poiButtonAreaSave');
    this.btnSave          = $('#' + idPrefix + '_poiButtonSave');
    this.btnCreateArea    = $('#' + idPrefix + '_poiButtonAreaCreate');
    this.btnCreate        = $('#' + idPrefix + '_poiButtonCreate');
    this.btnCloseArea     = $('#' + idPrefix + '_poiButtonAreaClose');
    this.btnClose         = $('#' + idPrefix + '_poiButtonClose');
    this.btnsPersist      = $('#' + idPrefix + '_poiButtonCreate, #' + idPrefix + '_poiButtonSave');
    this.btnCloseText     = this.btnClose.find('span');

    if(!isEmbedded) {
      this.btnCloseText.text('Close');
    }
  },

  setState: function (state) {
    switch (state) {
      case FormState.NEW:
        this.btnDeleteArea.hide();
        this.btnAddPhotoArea.hide();
        this.btnSaveArea.hide();
        this.btnCreateArea.addClass('offset5');
        this.btnCreateArea.show();
        this.btnCloseArea.show();
        break;
      case FormState.READ_ONLY:
        this.btnAddPhotoArea.hide();
        this.btnDeleteArea.hide();
        this.btnSaveArea.hide();
        this.btnCreateArea.hide();
        this.btnCloseArea.show();
        break;
      case FormState.EDITABLE:
        this.btnAddPhotoArea.addClass('offset3');
        this.btnAddPhotoArea.show();
        this.btnDeleteArea.hide();
        //Removing text sub-elements
        this.btnSaveArea.show();
        this.btnCloseArea.show();
        this.btnCreateArea.hide();
        break;
      case FormState.DELETABLE:
        this.btnAddPhotoArea.removeClass("offset3");
        this.btnAddPhotoArea.show();
        this.btnDeleteArea.show();
        //Removing text sub-elements
        this.btnSaveArea.show();
        this.btnCloseArea.show();
        this.btnCreateArea.hide();
        break;
    }
  }
};

var PoiEditorDataUI = {
  init: function(idPrefix) {
    this.tabBtnInfo     = $('#' + idPrefix + '_infoTabBtn');
    this.tabBtnAddress  = $('#' + idPrefix + '_addressTabBtn');
    this.tabBtnContact  = $('#' + idPrefix + '_contactTabBtn');
    this.tabBtnFeatures = $('#' + idPrefix + '_featuresTabBtn');

    this.tabInfo        = $('#' + idPrefix + '_infoTab');
    this.tabAddress     = $('#' + idPrefix + '_addressTab');
    this.tabContact     = $('#' + idPrefix + '_contactTab');
    this.tabFeatures    = $('#' + idPrefix + '_featuresTab');

    //POI Data - Info Tab
    this.cmpyId         = $('#' + idPrefix + '_poiCmpyId');
    this.poiId          = $('#' + idPrefix + '_poiId');
    this.name           = $('#' + idPrefix + '_poiName');
    this.type           = $('#' + idPrefix + '_poiType');
    this.codeLabel      = $('#' + idPrefix + '_poiCodeLabel');
    this.code           = $('#' + idPrefix + '_poiCode');
    this.tagsLabel      = $('#' + idPrefix + '_poiTagsLabel');
    this.tags           = $('#' + idPrefix + '_poiTags');

    //POI Data - Address Tab (Main Address)
    this.landmark       = $('#' + idPrefix + '_poiLandmarkName');
    this.streetAddress  = $('#' + idPrefix + '_poiAddressStreetAddress');
    this.locality       = $('#' + idPrefix + '_poiAddressLocality');
    this.postalCode     = $('#' + idPrefix + '_poiAddressPostalCode');
    this.region         = $('#' + idPrefix + '_poiAddressRegion');
    this.country        = $('#' + idPrefix + '_poiAddressCountry');
    this.latitude       = $('#' + idPrefix + '_poiAddressCoordsLat');
    this.longitude      = $('#' + idPrefix + '_poiAddressCoordsLong');

    //POI Data - Contact Tab
    this.email          = $('#' + idPrefix + '_poiEmail');
    this.url            = $('#' + idPrefix + '_poiUrl');
    this.phone          = $('#' + idPrefix + '_poiPhoneMain');
    this.fax            = $('#' + idPrefix + '_poiPhoneFax');
    this.facebook       = $('#' + idPrefix + '_poiSocialLinkFacebook');
    this.twitter        = $('#' + idPrefix + '_poiSocialLinkTwitter');
    this.description    = $('#' + idPrefix + '_poiDesc');

    //POI Data - Benefits and Features
    this.features       = $('#' + idPrefix + '_featuresTabContent');


    this.setupNameLookup();
    this.setupLandmarkLookup();
    this.setupLocalityLookup();
    this.setupCodeLabel();
  },

  setupCodeLabel: function() {
    this.type.change(function () {
      var providerType = $('option:selected', this).text(); //TODO: text is Less reliable - change to codes later
      if (providerType == "Airport") {
        this.codeLabel.text('IATA Airport Code *');
        this.code.removeClass().addClass('required input-block-level');
        this.code.attr('placeholder', 'IATA Airport Code');
        this.code.attr('type', 'text');
      }
      else if (providerType == "Airline") {
        this.codeLabel.text('IATA Airline Code *');
        this.code.removeClass().addClass('required input-block-level');
        this.code.attr('placeholder', 'IATA Airline Code');
        this.code.attr('type', 'text');
      }
      else {
        this.codeLabel.text('Location Short Code (Unique Identifier)');
        this.code.removeClass().addClass('input-block-level');
        this.code.attr('placeholder', 'Location Short Code (Unique Identifier)');
        this.code.attr('type', 'text');
      }
    });
  },

  /**
   * Initializing city location lookup
   */
  setupLocalityLookup: function(){
    this.locality.autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: "http://api.geonames.org/searchJSON",
          dataType: "jsonp",
          data: {
            username: "umapped",
            featureClass: "P",
            style: "full",
            maxRows: 12,
            name_startsWith: request.term
          },
          success: function( data ) {
            response( $.map( data.geonames, function( item ) {
              return {
                label: item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName,
                value: item.name,
                name: item.lng + "," + item.lat
              }
            }));
          }
        });
      },
      minLength: 3,
      select: function( event, ui ) {
        var array = ui.item.label.split(',');
        var gps = ui.item.name.split(',');

        if (array[2] == null && array[1] != null) {
          this.region.val("");
          this.setCountryByName(array[1]);
        } else {
          this.region.val(array[1]);
          this.setCountryByName(array[2]);
        }

        if (gps[0] != null && gps[1] != null) {
          this.latitude.val(gps[1]);
          this.longitude.val(gps[0]);

        } else {
          this.latitude.val("");
          this.longitude.val("");
        }
      },
      open: function() {
        $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
      },
      close: function() {
        $(this).removeClass("ui-corner-top").addClass( "ui-corner-all");
      }
    });
  },

  setupLandmarkLookup: function () {
    var editor = this;
    /** Initializing Google Location Lookup for Address (Landmark) Field */
    var poiLandComp = new google.maps.places.Autocomplete(this.landmark[0]);
    google.maps.event.addListener(poiLandComp, 'place_changed', function () {
      var place = poiLandComp.getPlace();
      var poi = editor.gmPlaceToPoi(place);
      if (!place.geometry) {
        editor.landmark.attr('class', 'notfound required input-block-level');
        return;
      }
      editor.clearAddress();
      editor.setPoiData(poi, false);
      editor.landmark.attr('class', 'required input-block-level');
      editor.landmark.blur();
      setTimeout(function () {
        editor.landmark.val(place.name);
      }, 10);
    });
  },

  /**
   * Configures completion for Name Field
   */
  setupNameLookup: function() {
    var editor = this;
    /** Initializing Google Location Lookup */
    var poiNameAutocomp = new google.maps.places.Autocomplete(this.name[0]);
    google.maps.event.addListener(poiNameAutocomp, 'place_changed', function() {

      var place = poiNameAutocomp.getPlace();
      if (!place.geometry) {

        editor.name.attr('class', 'notfound required input-block-level');
        return;
      }
      editor.name.attr('class', 'required input-block-level');
      var poi = editor.gmPlaceToPoi(place);
      editor.setPoiData(poi, true);
      editor.name.blur();
      setTimeout(function(){
        editor.name.val(place.name);
      }, 10);
    });
  },

  /** Previously known as poiEditResetTab */
  resetTab: function () {
    //reset active tab
    //console.log("PoiEditorDataUI : resetTab()");
    if (this.tabAddress.hasClass('active')) {
      this.tabAddress.removeClass('active');
      this.tabBtnAddress.removeClass('active');
    }
    else if (this.tabContact.hasClass('active')) {
      this.tabBtnContact.removeClass('active');
      this.tabContact.removeClass('active');
    }

    if (!this.tabInfo.hasClass('active')) {
      this.tabBtnInfo.addClass('active');
      this.tabInfo.addClass('active');
    }
  },

  /**
   * Previously known as poiEditorClearData
   * Clears all editor content and initializes core data (not poi data)
   * @param
   */
  clear: function (keepInternal) {
    //console.log("PoiEditorDataUI: clear(" + keepInternal + ")");
    //do not reset the poi type, poi desc and poi code if this is from a google place search
    if (keepInternal == null || !keepInternal) {
      this.type.find('option[value=3]').attr('selected', true); //TODO: Temporarily using numeric value for Hotel
      this.code.val("");
      this.description.val("");
      this.tags.val("");
    }
    this.name.val("");
    this.poiId.val("");
    this.clearAddress();
    this.landmark.val("");
    this.email.val("");
    this.url.val("");
    this.phone.val("");
    this.fax.val("");
    this.facebook.val("");
    this.twitter.val("");
    this.features.children().remove();
    this.tabBtnFeatures.hide();
  },

  /**
   * Previously known as poiEditorClearAddress
   */
  clearAddress: function () {
    this.streetAddress.val("");
    this.locality.val("");
    this.postalCode.val("");
    this.region.val("");
    this.setCountry("UNK");
    this.latitude.val("");
    this.longitude.val("");
  },

  /**
   * Previously known as poiEditorGetCountry
   * @returns {*}
   */
  getCountry: function() {
    return this.country.val();
  },

  /**
   * Previously known as poiEditorSetCountry
   * @param alpha3 - ISO 3 letter country code
   */
  setCountry: function (alpha3) {
    this.country.find('option[value=' + alpha3 + ']').attr('selected', true);
  },

  /**
   * Previsouly known as poiEditorSetCountryByName
   * @param name
   */
  setCountryByName : function(name) {
    utilGetCountryByName(name, function(cntry){
      this.setCountry(cntry.alpha3);
    });
  },

  /**
   * Previously known as poiEditUIFeatures
   */
  setFeatures: function(poiRs) {
    var feats = poiRs.features;
    for (var idx = 0; idx < feats.length; idx++) {
      var header = $('<h4>').text(feats[idx].name);
      var desc = feats[idx].desc;
      desc = desc.replace(/&amp;/g, '&');
      var p = $('<p>').text(desc);


      this.features.append(header);
      this.features.append(p);
    }
    this.tabBtnFeatures.show();
  },

  /**
   * Previously known as poiEditUISetPoi
   * Sets POI Editor UI fields from the provided object, only fields that are not set but are in the POI object get
   * overwritten.
   * @param poiRs
   * @param overwrite - if 'true', then all fields from POI will be overwritten. POI ID is never overwritten
   */
  setPoiData: function (poiRs, overwrite) {
    if ((this.name.val() == "" || overwrite) && poiRs['name']) {
      var name = poiRs['name']; //This pill cures sick IE, no questions must be asked...
      this.name.val(name);
    }

    if (poiRs['cmpy-id']) {
      this.cmpyId.find('option[value=' + poiRs['cmpy-id'] + ']').attr('selected', true);
    }

    if (this.poiId.val() == "" && poiRs['poi-id-js']) {
      this.poiId.val(poiRs['poi-id-js']);
    }

    if ((this.tags.val() == "" || overwrite) && poiRs.tags.length > 0) {
      var tagsString = "";
      $.each(poiRs.tags, function (k, v) {
        if (v.length > 0) {
          tagsString += v.trim() + ', ';
        }
      });
      this.tags.val(tagsString);
    }

    if (poiRs["poi-type-id"]) {
      this.setType(poiRs["poi-type-id"]);
    }

    if ((this.code.val() == "" || overwrite) && poiRs['code']) {
      this.code.val(poiRs['code']);
    }

    var editor = this;
    $.each(poiRs.addresses, function (k, v) {
      if (v.address_type == EnumAddressTypes.ADDR_MAIN) {
        if ((editor.streetAddress.val() == "" || overwrite) && v.street_address) {
          editor.streetAddress.val(v.street_address);
        }
        if ((editor.locality.val() == "" || overwrite) && v.locality) {
          editor.locality.val(v.locality);
        }

        if ((editor.region.val() == "" || overwrite) && v.region) {
          editor.region.val(v.region);
        }

        if ((editor.postalCode.val() == "" || overwrite) && v.postal_code) {
          editor.postalCode.val(v.postal_code);
        }

        if (v.country_code) {
          editor.setCountry(v.country_code);
        }

        if (v.coordinates) {
          editor.latitude.val(v.coordinates.latitude);
          editor.longitude.val(v.coordinates.longitude);
        }
      }
    });

    $.each(poiRs.urls, function (k, v) {
      if (editor.url.val() == "" || overwrite) {
        editor.url.val(v);
      }
    });

    $.each(poiRs['phone-numbers'], function (k, v) {
      switch (v['phone-type']) {
        case EnumPhoneTypes.PHONE_MAIN:
          if (editor.phone.val() == "" || overwrite) {
            editor.phone.val(v.number);
          }
          break;
        case EnumPhoneTypes.PHONE_FAX:
          if (editor.fax.val() == "" || overwrite) {
            editor.fax.val(v.number);
          }
          break;
      }
    });

    $.each(poiRs.emails, function (k, v) {
      switch (v.email_type) {
        case EnumEmailTypes.MAIN:
          if (editor.email.val() == "" || overwrite) {
            editor.email.val(v.address);
          }
          break;
      }
    });

    $.each(poiRs['social-links'], function (k, v) {
      switch (v.service_name) {
        case 'Facebook':
          if (editor.facebook.val() == "") {
            editor.facebook.val(v.service_direct_url);
          }
          break;
        case 'Twitter':
          if (editor.twitter.val() == "") {
            editor.twitter.val(v.service_direct_url);
          }
          break;
      }
    });

    if ((this.description.val() == "" || overwrite) && poiRs.desc) {
      this.description.val(poiRs.desc);
    }

    if ((this.landmark.val() == "" || overwrite) && poiRs['landmark_name']) {
      this.landmark.val(poiRs['landmark_name']);
    }

    if (poiRs.features && poiRs.features.length > 0) {
      this.setFeatures(poiRs);
    }
  },

  /**
   * Previously known as getPoiFromEditor
   * Captures details from the poi in the editor
   */
  getPoi: function () {
    var poi = new Poi(); //New object

    var poiId = this.poiId.val();
    if (poiId.length != 0) {
      poi['poi-id-js'] = poiId;
    }

    poi['cmpy-id']      = this.cmpyId.val();
    poi['name']         = this.name.val();
    poi['poi-type-id']  = this.type.val();
    poi['code']         = this.code.val();
    if (this.tags.val().length > 1) {
      poi.tags = this.tags.val().trim().split(',');
      $.each(poi.tags, function (k, v) {
        poi.tags[k] = v.trim();
      });
    }

    var poiAddress = new buildAddress(EnumPrivacy.PUBLIC, EnumAddressTypes.ADDR_MAIN);
    poiAddress.street_address = this.streetAddress.val();
    poiAddress.locality       = this.locality.val();
    poiAddress.region         = this.region.val();
    poiAddress.postal_code    = this.postalCode.val();

    poiAddress.country_code           = this.getCountry();
    poiAddress.coordinates.latitude   = this.latitude.val();
    poiAddress.coordinates.longitude  = this.longitude.val();
    poi.addAddress(poiAddress);

    //TODO: Add url correctness verification
    if (this.url.val().length > 0) {
      poi.addUrl(this.url.val());
    }

    if (this.phone.val().length > 0) {
      var phone = new buildPhoneNumber(EnumPrivacy.PUBLIC, EnumPhoneTypes.PHONE_MAIN, this.phone.val());
      poi.addPhoneNumber(phone);
    }

    if (this.fax.val().length > 0) {
      var phone = new buildPhoneNumber(EnumPrivacy.PUBLIC, EnumPhoneTypes.PHONE_FAX, this.fax.val());
      poi.addPhoneNumber(phone);
    }

    if (this.email.val().length > 0) {
      var email = new buildEmail(EnumPrivacy.PUBLIC, EnumEmailTypes.MAIN, this.email.val());
      poi.addEmail(email);
    }

    if (this.twitter.val().length > 0) {
      var socialLink = new buildSocialLink("Twitter", "", this.twitter.val());
      poi.addSocialLink(socialLink);
    }
    if (this.facebook.val().length > 0) {
      var socialLink = new buildSocialLink("Facebook", "", this.facebook.val());
      poi.addSocialLink(socialLink);
    }

    if (this.description.val().length > 0) {
      poi.desc = this.description.val();
    }

    if (this.landmark.val().length > 0) {
      poi['landmark_name'] = this.landmark.val();
    }
    return poi;
  },

  /**
   * Helper function converting a place returned by Google Maps Places API to POI object
   *
   * @param place Google Maps Places object
   * @returns {Poi}
   */
  gmPlaceToPoi: function (place) {
    var poi = new Poi();
    poi.name = place.name;

    var poiAddress = new buildAddress(EnumPrivacy.PUBLIC, EnumAddressTypes.ADDR_MAIN);

    for (var i = 0; i < place.address_components.length; i++) {
      var addr = place.address_components[i];
      if (addr.types[0] == 'country') {
        utilGetCountryByCode(addr.short_name, function (cntry) {
          poiAddress.country_code = cntry.alpha3;
        });
      }
      else if (addr.types[0] == 'postal_code') {
        poiAddress.postal_code = addr.long_name;
      }
      else if (addr.types[0] == 'administrative_area_level_1') {
        poiAddress.region = addr.long_name;
      }
      else if (addr.types[0] == 'locality') {
        poiAddress.locality = addr.long_name;
      }
      else if (addr.types[0] == 'street_address') {
        poiAddress.street_address = addr.long_name;
      }
      else if (addr.types[0] == 'street_number') {
        poiAddress.street_address = addr.long_name;
      }
      else if (addr.types[0] == 'route') {
        if (poiAddress.street_address) {
          poiAddress.street_address = poiAddress.street_address + " " + addr.long_name;
        }
        else {
          poiAddress.street_address = addr.long_name;
        }
      }
    }

    if (poiAddress.street_address && poiAddress.street_address.length == 0 && place.formatted_address) {
      poiAddress.street_address = place.formatted_address;
    }

    poiAddress.coordinates.latitude = place.geometry.location.lat();
    poiAddress.coordinates.longitude = place.geometry.location.lng();

    poi.addAddress(poiAddress);

    if (place.international_phone_number != null) {
      var phone = new buildPhoneNumber(EnumPrivacy.PUBLIC,
          EnumPhoneTypes.PHONE_MAIN,
          place.international_phone_number);
      poi.addPhoneNumber(phone);
    }

    if (place.website != null) {
      poi.addUrl(place.website);
    }
    return poi;
  },

  setType: function(typeId, enabled) {
    this.type.find('option[value=' + typeId + ']').attr('selected', true);
    if (enabled) {
      this.type.attr('disabled', false);
    } else {
      this.type.attr('disabled', true);
    }
  }
};

var PoiEditorPhotosUI = {
  init: function(idPrefix) {
    //console.log("PoiEditorPhotosUI::init()")
    this.main           = $('#' + idPrefix + '_poiPhotosRegion');
    this.photos         = $('#' + idPrefix + '_poiPhotos');
    this.noPhotos       = $('#' + idPrefix + '_poiPhotosRegionNone');
  },
  hide: function() {
    this.main.hide();
  }
};

var PoiEditorPhotoDeleteModal = {
  init: function (){
    //console.log("PoiEditorPhotoDeleteModal::init");
    this.main           = $('#poiPhotoDeleteModal');
    this.submit         = $('#poiPhotoDeleteSubmit');
  }
};

var PoiEditorDeleteModal = {
  init: function () {
    //console.log("PoiEditorDeleteModal::init()");
    this.main           = $('#poiDeleteModal');
    this.submit         = $('#poiButtonDeleteConfirmed');
  }
};

var PoiEditorPhotoModal = {
  init: function() {
    //console.log("PoiEditorPhotoModal::init()");
    this.main           = $('#poiPhotosModal');
    this.btnsClose      = $('#poiImgSearchModalClose, #poiImgSearchModalCloseTop');
    this.alert          = $('#poiImgAlertBox');
    this.btnHotels      = $('#hotelPhotosTabArea');
    this.btnHotelsLnk   = this.btnHotels.find('a');
    this.btnLibrary     = $('#imgLibraryTabArea');
    this.btnLibraryLnk  = this.btnLibrary.find('a');
    this.tabHotels      = $('#poiHotelPhotosTab');
    this.tabLibrary     = $('#poiImgLibTab');
    this.tabWeb         = $('#poiImgWebTab');
    this.tabUpload      = $('#poiImgUploadTab');
    this.spinner        = $('#poiImgSpinner');

    //Hotel Tab
    this.hotelMain      = $('#hotelPhotosArea');
    this.hotelIcePhotos = $('#iceThumbnails');
    this.hotelSuggest   = $('#hotelSuggestionArea');
    this.hotelIceTable  = $('#iceRecordsTable');
    this.hotelIceRecs   = $('#iceRecordsTableData');

    //Library Tab
    this.libForm        = $('#poiImgSearchForm');
    this.libCmpyId      = $('#poiImgSearchForm #inCmpyId');
    this.libPoiId       = $('#poiImgSearchForm #inVendorId');
    this.libMFiles      = $('#poiImgSearchForm #inMultipleFiles');
    this.libKeyword     = $('#poiImgSearchForm #inKeyword');
    this.libBtnSearch   = $('#poiPhotoLibrarySearch');
    this.libResults     = $('#poiImgSearchResults');

    //Web Tab
    this.webForm        = $('#poiImgWebSearchForm');
    this.webCmpyId      = $('#poiImgWebSearchForm #inCmpyId');
    this.webPoiId       = $('#poiImgWebSearchForm #inVendorId');
    this.webMFiles      = $('#poiImgWebSearchForm #inMultipleFiles');
    this.webKeywords    = $('#poiImgWebSearchForm #inKeyword');
    this.webUrl         = $('#inWebUrl');
    this.webBtnSearch   = $('#poiPhotoWebSearch');
    this.webResults     = $('#poiImgWebSearchResults');

    //Upload Tab
    this.upForm         = $('#poiPhotoUploadForm');
    this.upAwsKey       = $('#key');
    this.upAwsAcesssId  = $('#AWSAccessKeyId');
    this.upAwsPolicy    = $('#policy');
    this.upAwsSignature = $('#signature');
    this.upAwsStatus    = $('#success_action_status');
    this.upFilePath     = $('#poiPhotoFile');
    this.upBtnSubmit    = $('#poiPhotoUploadSubmit');
    this.upCaption      = $('#poiPhotoCaption');
    this.upSpinner      = $('#uploadSpinner');
    this.upProgress     = $('#progressUpload');
    this.allForms       = this.main.find('form');

    //Setting up submit buttons
    var photoModal = this;
    this.libBtnSearch.unbind("click").click(function () {
      photoModal.spinner.show();
      photoModal.libResults.children().remove();
      doSearchForm('/img/search', 'poiImgSearchForm', 'poiImgSearchResults', 'poiImgSpinner');
      return false;
    });

    this.webBtnSearch.unbind("click").click(function () {
      photoModal.spinner.show();
      photoModal.webResults.children().remove();
      doSearchForm('/img/search', 'poiImgWebSearchForm', 'poiImgWebSearchResults', 'poiImgSpinner');
      return false;
    });

    this.allForms.keypress(function(event){
      if(event.keyCode == 13) {
        event.preventDefault();
        return false;
      }
    });

    var unbindLegacyPreviewHandler = function() {
      $('#imgModal').unbind('hide', legacyPreviewHideHandler);
    };

    var legacyPreviewHideHandler = function(){
      setTimeout(function(){
        photoModal.main.modal('show');
        unbindLegacyPreviewHandler();
      }, 250); //After a short delay show main photos modal again
    };

    var registerEvents = function(e){
      if(e.relatedTarget) { return; }
      $('#imgModal').unbind('hide', legacyPreviewHideHandler);
      $('#imgModal').on('hide', legacyPreviewHideHandler);

      photoModal.btnsClose.unbind('click', unbindLegacyPreviewHandler);
      photoModal.btnsClose.click(unbindLegacyPreviewHandler);
    };

    this.main.unbind('shown', registerEvents);
    this.main.on('shown', registerEvents);
  },

  /**
   * Resetting various fields in the modal
   */
  reset: function() {
    this.libResults.children().remove();
    this.libKeyword.val("");
    this.libCmpyId.val("");
    this.libPoiId.val("");

    this.webResults.children().remove();
    this.webKeywords.val("");
    this.webCmpyId.val("");
    this.webPoiId.val("");
    this.webUrl.val("");
  },

  configureValues: function(legacyCmpyId, poiId) {
    if (legacyCmpyId && poiId) {
      this.poiId = poiId;
      this.legacyCmpyId = legacyCmpyId;
    }
    this.libCmpyId.val(this.legacyCmpyId);
    this.webCmpyId.val(this.legacyCmpyId);
    this.libPoiId.val(this.poiId);
    this.webPoiId.val(this.poiId);
  },

  /**
   * Previously known as poiEditorIcePhotos
   * @param enable
   */
  icePhotosEnable: function(enable) {
    if (enable == true) {
      this.btnHotels.show();
      this.btnHotelsLnk.tab('show');
      this.tabHotels.addClass('active');
    } else {
      this.btnLibraryLnk.tab('show');
      this.btnHotels.hide();
      this.tabLibrary.addClass('active');
    }
  },

  clear: function() {
    //Clearing photo upload alerts
    this.alert.hide();
    this.alert.find('#alertMsg').remove();
  }
};

var PoiEditorPhotoPreviewModal = {
  init : function() {
    //console.log("PoiEditorPhotoPreviewModal");
    this.main         = $('#poiPhotoModal');
    this.btnClose     = $('#poiPhotoModalCloseTop, #poiPhotoModalClose');
    this.spinner      = $('#poiPhotoModalSpinner');
    this.disclamer    = $('#poiPhotoDisclaimer');
    this.license      = $('#poiPhotoLicence');
    this.captionArea  = $('#inImgPhotoCaptionDiv');
    this.caption      = $('#poiPhotoPreviewCaption');
    this.btnAdd       = $('#poiPhotoAdd');
    this.image        = $('#poiPhotoPreview');
  },

  /**
   * Previously known as poiLoadImagePreview
   * @param url
   * @param caption
   */
  showImage: function(url, caption) {
    this.image.attr('src', url);
    this.image.attr('alt', caption);
    this.caption.val(caption);
    this.main.modal('show');
  }
};

var PoiEditorUI = {
  init: function(rootSelectorId, isEmbedded) {
    //console.log("PoiEditorUI::init()");
    this.rootSelectorId = rootSelectorId;

    if(isEmbedded && isEmbedded == true) {
      this.isEmbedded = true;
    } else if (isEmbedded == undefined || isEmbedded == null || isEmbedded == false) {
      this.isEmbedded = false;
    }
    this.btns       = Object.create(PoiEditorButtonsUI);
    this.editor     = Object.create(PoiEditorDataUI);
    this.photos     = Object.create(PoiEditorPhotosUI);
    this.mPhotoDel  = Object.create(PoiEditorPhotoDeleteModal);
    this.mDelete    = Object.create(PoiEditorDeleteModal);
    this.mPhoto     = Object.create(PoiEditorPhotoModal);
    this.mPreview   = Object.create(PoiEditorPhotoPreviewModal);

    this.btns.init(rootSelectorId, this.isEmbedded);
    this.editor.init(rootSelectorId);
    this.photos.init(rootSelectorId);
    this.mPhotoDel.init(rootSelectorId);
    this.mDelete.init(rootSelectorId);
    this.mPhoto.init(rootSelectorId);
    this.mPreview.init(rootSelectorId);

    var ui = this;
    //Editor - Delete Modal - confirmation button was clicked
    this.mDelete.submit.unbind("click").click(function() {
      ui.poiDelete(function (data) {
        ui.clear();
        ui.hide();
      });
      ui.mDelete.main.modal('hide');
    });

    //Editor - save or update button was clicked
    this.btns.btnsPersist.unbind("click").click(function () {
      ui.poiSubmit(function (data) {
        if (data.body.pois.length > 0) {
          var poi = data.body.pois[0];
          ui.setPoi(poi['poi-id-js'], poi['cmpy-id']);
          ui.icePhotosCheck(poi, poi['cmpy-id']);
        }
      });
    });

    //Editor - Close button is being pressed
    this.btns.btnClose.unbind("click").click(function () {
      ui.onClose();
    });

    //Photos modal is being closed
    this.mPhoto.btnsClose.unbind("click").click(function(){
      var poi = new Poi();
      poi['poi-id-js'] = ui.editor.poiId.val();
      poi['cmpy-id']   = ui.editor.cmpyId.val();
      poiStorageDelete(poi);
      ui.setPoi(poi['poi-id-js'], poi['cmpy-id']);
    });

    //Photos - upload start
    this.mPhoto.upBtnSubmit.unbind("click").click(function () {
      var poiId  = ui.editor.poiId.val();
      var cmpyId = ui.editor.cmpyId.val();

      if (poiId == null || poiId == 0 || cmpyId == null) {
        return false;
      }
      ui.fileUpload(poiId, cmpyId);
      return false;
    });

    this.mPhoto.upFilePath.change(function() {
      var lastDot   = ui.mPhoto.upFilePath.val().lastIndexOf('.')
      var extension = ui.mPhoto.upFilePath.val().substring(lastDot);
      var ValidFileType = ".jpeg, .jpg , .png , .gif";
      if (ValidFileType.toLowerCase().indexOf(extension.toLowerCase()) < 0) {
        alert("Please only select an image file of type (JPG, GIF, PNG)");
        ui.mPhoto.upBtnSubmit.attr('disabled', true);
        return;
      }

      if ( !$.browser.msie && $.browser.mozilla > 11 ) {
        var size = this.files[0].size;
        if (size > 1024000) {
          alert("The image file must be less than 1MB");
          ui.mPhoto.upBtnSubmit.attr('disabled', true);
          return;
        }
      }
      ui.mPhoto.upBtnSubmit.attr('disabled', false);
    });

    this.mPreview.btnClose.unbind('click').click(function(){
      ui.mPhoto.main.modal('show');
    });

    this.btns.btnAddPhoto.unbind('click').click(function(){
    //ui.mPhoto.init(); //This is required as modal is shared
    //ui.mPhoto.configureValues();
    ui.mPhoto.main.modal('show');
    });
  },
  onClose: function() {
    this.hide();
  },

  getRootSelectorId: function() {
    return this.rootSelectorId;
  },

  /**
   * Clearing all fields and elements
   */
  clear: function() {
    this.editor.clear();
    this.mPhoto.clear();
  },

  /**
   * Previously known as poiEditorShow()
   */
  show: function() {
    if (!this.isEmbedded) {
      activateMenu('#newPoiMenu');
      $('#' + this.rootSelectorId).show();
      $('#poiSearchRegion').hide();
    }
  },

  /**
   * Previously known as poiEditorHide
   */
  hide: function() {
    //Clear image adding form
    this.mPhoto.reset();
    if(!this.isEmbedded) {
      activateMenu('#poiMenu');
      $('#' + this.rootSelectorId).hide();
      $('#poiSearchRegion').show(); //TODO: Figure out how to inject outer UI
    }
  },

  newPoi: function() {
    //console.log("PoiEdtiroUI : newPoi()");
    //Clearing any previous alerts
    this.clear();
    this.editor.cmpyId.find('option').remove();
    headLineAlertHide();
    //reset active tab
    this.editor.resetTab();

    var ui = this;
    getUserCompanies(function (cmpys) {
      $.each(cmpys, function (k, v) {
        var isEditable = ['ADMIN', 'POWER'].indexOf(v.user_link_type) > -1;
        if (isEditable) {
          ui.editor.cmpyId.append($('<option>', {
            value: v.id,
            text: v.name
          }));
        }
      });

      ui.btns.setState(FormState.NEW);
      //make sure the poi type and cmpy select boxes are active
      ui.editor.type.attr('disabled', false);
      ui.editor.cmpyId.attr('disabled', false);
    });

    //hide the photos tab
    this.photos.hide();
    this.show();
  },

  /**
   * Previously known as poiEditUI
   * Creating new POI
   * @param poiId
   * @param cmpyId
   */
  setPoi: function (poiId, cmpyId) {
    //console.log("PoiEditUI : setPoi(" + poiId + ", " + cmpyId + ")");
    if(poiId == null || cmpyId == null) {
      return this.newPoi();
    }
    if (poiId != this.editor.poiId.val()) {
      this.editor.resetTab();
    }

    //Clearing any previous alerts
    this.clear();
    this.editor.cmpyId.find('option').remove();
    var ui = this;
    getUserCompanies(function (cmpys) {
      poiStorageGet(poiId, cmpyId, function (poiRs) {
        ui.filesLoad(poiId, cmpyId);
        ui.editor.cmpyId.append($('<option>', {
          value: cmpys[cmpyId].id,
          text: cmpys[cmpyId].name
        }));

        //Setup image search/upload form input values
        var legacyId = null;
        $.each(cmpys, function (k, v) {
          if (v.id == cmpyId) {
            legacyId = v.legacy_id;
          }
        });

        ui.mPhoto.configureValues(legacyId, poiId);
        ui.editor.setPoiData(poiRs, false);

        if (poiRs["poi-type-name"] == 'Accommodations') {
           ui.travelboundPhotosCheck(poiRs, cmpyId);
           //ui.icePhotosCheck(poiRs, cmpyId);
        }
        else {
          ui.mPhoto.icePhotosEnable(false);
        }

        //Now need to add to the history at the top
        //addRecentHistory(key, pId, pName, js)
        addRecentHistory('RecentPoi',
                poiRs['poi-id-js'],
                poiRs['name'] + " (" + poiRs['poi-type-name'] + ")",
                "currEditorUI.setPoi('" + poiRs['poi-id-js'] + "'," + cmpyId + ")"); //TODO: Figure out how to handle better

        //Setup save/update/delete/add-photo buttons based on permissions
        var cmpy = cmpys[cmpyId];
        var isEditable = ['ADMIN', 'POWER'].indexOf(cmpy.user_link_type) > -1;
        var isDeletable = (poiRs['cmpy-id'] != 0 && isEditable);

        if (isEditable) {
          if (isDeletable) {
            ui.btns.setState(FormState.DELETABLE);
          }
          else {
            ui.btns.setState(FormState.EDITABLE)
          }
        }
        else {
          ui.btns.setState(FormState.READ_ONLY)
        }

      });
      //disable the dropdown select for poi-type and company
      ui.editor.type.attr('disabled', true);
      ui.editor.cmpyId.attr('disabled', true);

      ui.show();
    });
  },

/**
   * Previously known as poiIcePhotosCheck
   * @param poiRs
   * @param cmpyId
   */
  travelboundPhotosCheck: function(poiRs, cmpyId) {
    //console.log("travelboundPhotosCheck");
    var request = new ApiMessage();
    request['@type']  = 'TRAVELBOUNDPHOTOS';
    request.header = new ApiRequestHeader();
    request.body = new IcePhotos(EnumOperations.SEARCH);
    request.body.searchId = poiRs['poi-id-js'];
    request.body.cmpyId   = cmpyId;
    var ui = this;
    ExecuteCommand(request, "/poi/travelbound/execute.json", function(data){
      if (data.body.photos.length > 0) {
        ui.travelboundRenderThumbs(data.body);
      }

      if (data.body.photos.length > 0) {
        ui.mPhoto.icePhotosEnable(true);
      } else {
        ui.mPhoto.icePhotosEnable(false);
      }
    });
  },

  travelboundRenderThumbs: function (body) {
    this.mPhoto.hotelMain.show();
    this.mPhoto.hotelSuggest.hide();
    this.mPhoto.hotelIcePhotos.children().remove();
    var ui = this;

    for (var idx = 0; idx < body.photos.length; idx++) {
       var currPhoto = body.photos[idx];
       var li = $('<li>', {
         "style": "list-style-type: none;display: inline;margin:0px 2px 0px 2px;"
       });

       var img = $('<img>', {
         "src": currPhoto.thumb,
         "style": "width:100px;",
         "title": currPhoto.caption
       });

       var showImg = function (photo) {
         return function () { //Closure for array loop
           ui.mPreview.showImage(photo.url, photo.caption);
           ui.mPreview.btnAdd.unbind('click').click(function () {
             ui.travelboundAddPhoto(body.searchId, body.cmpyId, photo);
           });
         };
       };

       img.click(showImg(currPhoto));
       li.append(img);
       this.mPhoto.hotelIcePhotos.append(li);
     }
   },

  travelboundAddPhoto: function(poiId, cmpyId, photo) {
    //console.log("iceAddPhoto");
    var request = new ApiMessage();
    request['@type']  = 'TRAVELBOUNDPHOTOS';
    request.header = new ApiRequestHeader();
    request.body = new IcePhotos(EnumOperations.ADD);
    request.body.searchId = poiId;
    request.body.cmpyId = cmpyId;
    var photo = photo;
    photo.caption = this.mPreview.caption.val();
    request.body.photos.push(photo);
    this.mPreview.spinner.show();

    var ui = this;
    ExecuteCommand(request, "/poi/travelbound/execute.json", function(data){
          ui.travelboundRenderThumbs(data.body);
          ui.filesLoad(poiId,cmpyId);
          ui.mPreview.spinner.hide();
          ui.mPhoto.main.modal('show');
        },
        function(data){
          ui.mPreview.spinner.hide();
          ui.mPreview.main.modal('hide');
          headLineAlertDisplay(data.header.msg + ' (' + data.header.resp_code + ')');
        });
    return false;
  },

  /**
   * Previously known as poiFilesLoad
   * @param poiId
   * @param cmpyId
   */
  filesLoad : function(poiId, cmpyId) {
    //console.log("PoiEditorUI: filesLoad: " + poiId + " for:" + cmpyId);
    var request = new ApiMessage();
    request['@type']  = 'POIFILE';
    request.header = new ApiRequestHeader();
    request.body = new PoiFileBody('LIST');
    request.body.poi_id = poiId;
    request.body.cmpy_id = cmpyId;

    //Clean-up area before requesting data
    this.photos.photos.children().remove();

    var ui = this;
    ExecuteCommand(request, "/poi/files/execute.json", function(data){
      $.each(data.body.files, function(k, v){
        ui.addPhoto(poiId, cmpyId, v.file_id, v.url, v.desc);
      });

      poiStorageGet(data.body.poi_id, data.body.cmpy_id, function(poi) {
        if ( poi['file-count'] != data.body.files.length) {
          poi['file-count'] = data.body.files.length;
          poiStorageUpdate(poi);
        }
      });

      if (data.body.files.length > 0) {
        ui.photos.main.show();
      } else {
        ui.photos.main.hide();
      }
    });
  },

  /**
   * Previously known as poiFileDelete
   * @param poiId
   * @param cmpyId
   * @param fileId
   * @returns {boolean}
   */
  fileDelete: function(poiId, cmpyId, fileId){
    //console.log("fileDelete");
    var request = new ApiMessage();
    request['@type']  = 'POIFILE';
    request.header = new ApiRequestHeader();
    request.body = new PoiFileBody('DELETE');
    request.body.poi_id = poiId;
    request.body.cmpy_id = cmpyId;

    var file = new PoiFile();
    file.file_id = fileId;
    file.cmpy_id = cmpyId;
    request.body.files.push(file);

    var ui = this;
    ExecuteCommand(request, "/poi/files/execute.json", function(data){
      ui.photos.photos.children().remove();
      $.each(data.body.files, function(k, v){
        ui.addPhoto(poiId, cmpyId, v.file_id, v.url, v.desc);
      });
      poiStorageGet(data.body.poi_id, data.body.cmpy_id, function(poi) {
        poi['file-count'] = data.body.files.length;
        poiStorageUpdate(poi);
      });
      ui.mPhotoDel.main.modal('hide');
      headLineAlertDisplay('Photo Deleted');
    });

    return false;
  },

  /**
   * Previously known as poiFileUpload
   * If client successfully uploaded file to S3, it needs to be added to the system
   */
  fileUpload: function(poiId, cmpyId) {
    //console.log("upSpinner");
    this.mPhoto.upSpinner.show();

    var ui = this;
    var s3upload = new APIS3Upload({
      file_dom_selector: ui.mPhoto.upFilePath.selector.replace('#', ''),  //TODO: Selector is deprecated, need another way
      api_request_endpoint: '/poi/files/execute.json',
      api_request_type: 'POIFILE',
      file_caption: ui.mPhoto.upCaption.val(),
      upload_form: ui.mPhoto.upForm.selector.replace('#', ''),           //TODO: Selector is deprecated, need another way
      api_request_body_builder: function(operation) {
        var poiFiles = new PoiFileBody(operation);
        poiFiles.poi_id = poiId;
        poiFiles.cmpy_id = cmpyId;
        return poiFiles;
      },
      onProgress: function(percent, message) {
        if (!$.browser.msie) {
          ui.mPhoto.upProgress.text(percent + "%");
        }
      },
      file_upload_success_callback: function(data) {
        ui.photos.photos.children().remove();
        $.each(data.body.files, function(k, v) {
          ui.addPhoto(poiId, cmpyId, v.file_id, v.url, v.desc);
        });
        ui.mPhoto.main.modal('hide');
        ui.mPhoto.upProgress.hide();
        ui.mPhoto.spinner.hide();
        ui.mPhoto.upFilePath.val("");
        ui.mPhoto.upCaption.val("");
        ui.filesLoad(poiId, cmpyId);
        ui.mPhoto.upSpinner.hide();
        poiSearch(poiId, cmpyId, true);
      },
      onError: function(status) {
        alert('Upload failed: ' + status);
        ui.mPhoto.main.modal.modal('hide');
        ui.mPhoto.upProgress.hide();
        ui.mPhoto.spinner.hide();
        ui.mPhoto.upFilePath.val("");
        ui.mPhoto.upCaption.val("");
        ui.mPhoto.upSpinner.hide();
      }
    });
  },

  /**
   * Previously known as poiIcePhotosCheck
   * @param poiRs
   * @param cmpyId
   */
  icePhotosCheck: function(poiRs, cmpyId) {
    //console.log("icePhotosCheck");
    var request = new ApiMessage();
    request['@type']  = 'ICEPHOTOS';
    request.header = new ApiRequestHeader();
    request.body = new IcePhotos(EnumOperations.SEARCH);
    request.body.searchId = poiRs['poi-id-js'];
    request.body.cmpyId   = cmpyId;
    var ui = this;
    ExecuteCommand(request, "/poi/ice/execute.json", function(data){
      if (data.body.photos.length > 0) {
        ui.iceRenderThumbs(data.body);
      }
      if (data.body.suggestions.length > 0) {
        ui.iceRenderSuggestions(poiRs, data.body, cmpyId);
      }
      if (data.body.photos.length > 0 || data.body.suggestions.length > 0) {
        ui.mPhoto.icePhotosEnable(true);
      } else {
        ui.mPhoto.icePhotosEnable(false);
      }
    });
  },

  /**
   * Previously known as poiRenderIceSuggestions
   * @param poiRs
   * @param body
   * @param cmpyId
   */
  iceRenderSuggestions: function(poiRs, body, cmpyId) {
    if (isDataTable(this.mPhoto.hotelIceTable[0])) {
      this.mPhoto.hotelIceTable.DataTable().destroy();
    }

    this.mPhoto.hotelSuggest.show();
    this.mPhoto.hotelMain.hide();
    this.mPhoto.hotelIceRecs.children().remove();

    var ui = this;
    for (var idx = 0; idx < body.suggestions.length; idx++) {
      var rec = body.suggestions[idx];
      var row = $('<tr>');
      var name = $('<td>').text(rec.name);
      var country = $('<td>');
      utilGetCountryByCode(rec.countryCode, function(cObj) {
        return function(cntry) {
          cntry.text(cObj.name);
        }(country);
      });

      var city = $('<td>').text(rec.locality);
      var address = $('<td>').text(rec.address);
      var postalCode = $('<td>').text(rec.postalCode);
      var button = $('<a>', {
        "role":"button",
        "class":"btn btn-info input-block-level"
      }).text("View Photos");


      var linkAction = function(suggestion, prs) {
        return function(){
          ui.iceListPhotos(suggestion, prs, cmpyId);
        }
      };

      button.click(linkAction(rec, poiRs['poi-id-js']));
      var buttonCol = $('<td>');
      buttonCol.append(button);
      row.append(name);
      row.append(country);
      row.append(city);
      row.append(address);
      row.append(postalCode);
      row.append(buttonCol);
      ui.mPhoto.hotelIceRecs.append(row);
    }

    this.mPhoto.hotelIceTable.DataTable({
      "paging": true,
      "searching": false,
      "lengthChange": false,
      "ordering": false
    });
  },

  /**
   * Previously known as poiIceListPhotos
   * @param suggestion
   * @param poiId
   * @param cmpyId
   * @returns {boolean}
   */
  iceListPhotos: function (suggestion, poiId, cmpyId) {
    //console.log('iceListPhotos');
    var request = new ApiMessage();
    request['@type'] = 'ICEPHOTOS';
    request.header = new ApiRequestHeader();
    request.body = new IcePhotos(EnumOperations.LIST);
    request.body.searchId = poiId;
    request.body.cmpyId = cmpyId;

    var suggestion = suggestion;
    suggestion.poiId = poiId;
    request.body.suggestions.push(suggestion);
    this.mPhoto.spinner.show();

    var ui = this;
    ExecuteCommand(request, "/poi/ice/execute.json", function (data) { //Success of link, should return all images
          ui.iceRenderThumbs(data.body);
          ui.mPhoto.spinner.hide();
        }, function (data) { //When error closing modal to view error message
          ui.mPhoto.spinner.hide();
          ui.mPhoto.main.modal('hide');
          headLineAlertDisplay(data.header.msg + ' (' + data.header.resp_code + ')');
        });
    return false;
  },

  /**
   * Previously known as poiRenderIceThumbs
   * @param body
   */
  iceRenderThumbs: function (body) {
    this.mPhoto.hotelMain.show();
    this.mPhoto.hotelSuggest.hide();
    this.mPhoto.hotelIcePhotos.children().remove();
    var ui = this;
    for (var idx = 0; idx < body.photos.length; idx++) {
      var currPhoto = body.photos[idx];
      var li = $('<li>', {
        "style": "list-style-type: none;display: inline;margin:0px 2px 0px 2px;"
      });

      var img = $('<img>', {
        "src": currPhoto.thumb,
        "style": "width:100px;",
        "title": currPhoto.caption
      });

      var showImg = function (photo) {
        return function () { //Closure for array loop
          ui.mPreview.showImage(photo.url, photo.caption);
          ui.mPreview.btnAdd.unbind('click').click(function () {
            ui.iceAddPhoto(body.searchId, body.cmpyId, photo);
          });
        };
      };

      img.click(showImg(currPhoto));
      li.append(img);
      this.mPhoto.hotelIcePhotos.append(li);
    }
  },

  /**
   * Previously known as poiAddIcePhoto
   * @param poiId
   * @param cmpyId
   * @param photo
   * @returns {boolean}
   */
  iceAddPhoto: function(poiId, cmpyId, photo) {
    //console.log("iceAddPhoto");
    var request = new ApiMessage();
    request['@type']  = 'ICEPHOTOS';
    request.header = new ApiRequestHeader();
    request.body = new IcePhotos(EnumOperations.ADD);
    request.body.searchId = poiId;
    request.body.cmpyId = cmpyId;
    var photo = photo;
    photo.caption = this.mPreview.caption.val();
    request.body.photos.push(photo);
    this.mPreview.spinner.show();

    var ui = this;
    ExecuteCommand(request, "/poi/ice/execute.json", function(data){
          ui.iceRenderThumbs(data.body);
          ui.filesLoad(poiId,cmpyId);
          ui.mPreview.spinner.hide();
          ui.mPhoto.main.modal('show');
        },
        function(data){
          ui.mPreview.spinner.hide();
          ui.mPreview.main.modal('hide');
          headLineAlertDisplay(data.header.msg + ' (' + data.header.resp_code + ')');
        });
    return false;
  },

  /**
   * Previously known as poiSubmit
   * @param callback
   */
  poiSubmit: function(callback) {
    //console.log("poiSubmit");
    var request = new ApiMessage();
    request['@type']  = 'POI';
    request.header = new ApiRequestHeader();
    var poi = this.editor.getPoi();
    if (poi.addresses[0].country_code == null) {
      alert("Country information missing");
      return;
    }

    if (poi['poi-id-js'] && poi['poi-id-js'].length > 0) {
      request.body = new PoiBody('UPDATE');
    } else {
      request.body = new PoiBody('ADD');
    }
    request.body.addPoi(poi);
    request.body.cmpy_id = poi['cmpy-id'];

    ExecuteCommand(request, "/poi/execute.json", function(data){
      if (data.body.pois.length == 1) { //Must receive exactly one poi
        poiStorageUpdate(data.body.pois[0]);
      }

      if (poi['poi-id-js'] && poi['poi-id-js'].length > 0) {
        headLineAlertDisplay('Vendor updated');
      } else {
        headLineAlertDisplay('Vendor saved');
      }

      if(callback) {
        callback(data);
      }
    });
  },

/**
 * Previously known as poiDelete
 * @param callback
 */
  poiDelete: function(callback) {
    //console.log("poiDelete");
    var request = new ApiMessage();
    request['@type']  = 'POI';
    request.header = new ApiRequestHeader();
    var poi = this.editor.getPoi();

    if (!poi['poi-id-js'] || poi['poi-id-js'].length == 0) {
      return; //Nothing to do here who called this function?
    } else {
      request.body = new PoiBody('DELETE');
    }
    request.body.addPoi(poi);

    ExecuteCommand(request, "/poi/execute.json", function(data){
      poiStorageRefresh();
      headLineAlertDisplay('Vendor deleted');
      if(callback) {
        callback(data);
      }
    });
  },

  /**
   * Previously known as buildPhotoView
   * @param poiId
   * @param cmpyId
   * @param fileId
   * @param fileUrl
   * @param desc
   * @returns {*|jQuery}
   */
  addPhoto: function(poiId, cmpyId, fileId, fileUrl, desc, onDelete){
    var table = $('<table>', {
      id : "poiPhotoTable_" + fileId
    }).addClass('table');
    var rowPhoto = $('<tr>');

    $('<a>', {
      href: fileUrl,
      'data-lightbox': "image-1",
      'data-title': desc,
      'target':'_blank'
    }).append($('<img>',{
      class:"img-rounded span12",
      height:"200px",
      src:fileUrl
    }))
        .appendTo($('<td>').appendTo(rowPhoto));

    var rowDesc  = $('<tr>');
    //Nothing to add here for now


    var rowRemove= $('<tr>');
    var ui = this;
    $('<a>', {
      href: ui.mPhotoDel.main.selector,
      'data-toggle':'modal',
      class:"btn btn-danger btn-small"
    })
        .click(function(){
          ui.mPhotoDel.submit.unbind("click").click(function(){
            ui.fileDelete(poiId, cmpyId, fileId); //TODO: Check if this kind of approach works
          });
        })
        .append($('<i>',{
          class:"fa fa-times"
        }).append('&nbsp;Delete'))
        .appendTo($('<td>',{
          style:"text-align:center;",
          class:"pull-right"
        }).appendTo(rowRemove));


    table.append(rowPhoto)
        .append(rowDesc)
        .append(rowRemove);

    ui.photos.photos.append($('<div>',{style: "margin:auto"})
        .addClass("span3 ")
        .append(table));
  }
};


var poiInputFieldSetValues = function(inputNameId, inputItemId, name, identifier, clearCb) {
  $("#" + inputNameId).addClear({
    right:10,
    top:8,
    showOnLoad: true,
    color: "#666666",
    onClear: function(){
      if (clearCb) {
        clearCb(inputNameId, inputItemId);
      }
      $("#" + inputNameId).val('');
      $("#" + inputItemId).val('');
      $("#" + inputNameId).prop('readonly', false);
    }
  });

  $("#" + inputNameId).val(name);
  $("#" + inputItemId).val(identifier);
  $("#" + inputItemId).change();
  $("#" + inputNameId).prop('readonly',  (name.length != 0 && identifier.length != 0));
};

var poiAutocompleteRegister = function (inputNameId, inputItemId, cmpyId, typeId, autoclose, clearCb) {
  if ($("#" + inputItemId).val().length > 0) {
    poiInputFieldSetValues(inputNameId, inputItemId,$("#" + inputNameId).val(), $("#" + inputItemId).val(), clearCb);
  }

  $('#' + inputNameId).autocomplete({
    source: "/poi/search/" + typeId + "/" + cmpyId,
    minLength: 2,
    delay: 500,
    select: function (event, ui) {
      poiInputFieldSetValues(inputNameId, inputItemId, ui.item.label, ui.item.id, clearCb);
      return false;
    },
    response: function (event, ui) {
      if (ui.content.length == 1 && autoclose) {
        poiInputFieldSetValues(inputNameId, inputItemId, ui.content[0].label, ui.content[0].id, clearCb);
        $(this).autocomplete("close");
      }
    }
  });
};

var flightAutocompleteRegister = function (inputNameId, inputItemId, cmpyId, typeId, autoclose, clearCb) {
  $('#' + inputNameId).autocomplete
};

var poiSearchRequest = null;
/**
 * @param term (Optional) if specified will ignore
 * @param cmpyId (Optional, Required if term is specified) if specified will ignore form value and search for requested
 */
var poiSearch = function(term, cmpyId, keepEditor) {
  //console.log("poiSearch term:" + term + " cmpyId:" + cmpyId + " keepEditor:" + keepEditor);
  var request = new ApiMessage();
  request.header = new ApiRequestHeader();
  request.body = new PoiBody('LIST');
  request['@type'] = 'POI';

  if(poiSearchRequest) {
    //console.log("Aborting prev req status:" + poiSearchRequest.status + " code:" + poiSearchRequest.statusCode());
    poiSearchRequest.abort();
    $('#poiSearchSpinner').hide();
  }

  if (term && (cmpyId != undefined)) {
    request.body.search_term = term;
    request.body.cmpy_id = cmpyId;
  }
  else {
    request.body.search_term = $('#poiSearchTerm').val().trim();
    if (request.body.search_term.slice(-1) == ',' || request.body.search_term.length == 0) {
      return; //Doing nothing, waiting for some location or term
    }
    request.body.cmpy_id = parseInt($('#inCmpyId :selected').val());
    $('#poiSearchTypes input:checked').each(function() {
      request.body.selected_types.push(parseInt($(this).attr('value')));
    });
  }

  var validSearchRE = /.*([\u00C0-\u1FFF\u2C00-\uD7FF\w]+).*/; //Matching any character, including Unicode
  if (!validSearchRE.test(request.body.search_term)){
    return; //User entered some garbage
  }

  $('#poiSearchSpinner').show();
  poiSearchRequest = ExecuteCommand(request, "/poi/execute.json", function(data){
    if (!keepEditor) {
      currEditorUI.hide();
    }
    var search_term = null;
    if (data.body.search_term != undefined && data.body.search_term.length != 0) {
      search_term = data.body.search_term;
      if (data.body.pois.length > 0) { //Only adding if something was found
        addRecentHistory('RecentPoiSearches', search_term, search_term, "poiSearch('" + search_term + "',"+data.body.cmpy_id+", false)");
      }
    }
    if (data.body.cmpy_id) {
      $('#inCmpyId option[value=' + data.body.cmpy_id + ']').attr('selected', true);
    }
    poiStorageStore(data.body.pois, search_term, data.body.cmpy_id);
    $('#poiSearchSpinner').hide();
  });
};

/**
 * @param poiId id of the poi to load from the server
 * @param cmpyId (Optional) if specified will restrict searching only to this company
 * @param callback function accepting poi object as returned from the server
 */
var poiSearchById = function(poiId, cmpyId, callback) {
  //console.log("poiSearchById: " + poiId + " " + cmpyId);
  //console.trace();
  var request = new ApiMessage();
  request.header = new ApiRequestHeader();
  request.body = new PoiBody('LIST');
  request['@type'] = 'POI';
  request.body.search_id = poiId;
  request.body.cmpy_id = cmpyId;

  ExecuteCommand(request, "/poi/execute.json", function(data){
    if (data.body.pois.length == 1) {
      poiStorageUpdate(data.body.pois[0]);
      if (callback) {
        callback(data.body.pois[0]);
      }
    }
  });
};

/** Render pois stored in the storage */
var poiStorageRender = function() {
  //console.log("poiStorageRender");
  //console.trace();
  /* Rendering only if we are on poi page */
  var poiRegion = $('#poiSearchRegion');
  if (!poiRegion[0]) {
    return; //Not on poi page now, probably some other tab updated content
  }

  //Helper functions follow
  /*
   <th width="30%">NAME</th>
   <th width="25%">ADDRESS</th>
   <th width="20%">CONTACT</th>
   <th width="15%">COMPANY</th>
   <th width="5%"></th>
   <th width="5%"></th>
   */
  var buildPoiTableRow = function(poiRs, poiSearchCmpyId, cmpys) {
    var tdName = $('<td>').text(poiRs.name);
    var tdAddress = $('<td>').append(getAddressView(poiRs.addresses));
    var tdContact = $('<td>').append(getContactView(poiRs));
    var tdCompany = $('<td>');
    getCompanyView(poiRs, cmpys, function(data){
      tdCompany.append(data);
    });
    var tdFiles = $('<td>').append(getPoiRowFilesButton(poiRs));
    var tdEdit =  $('<td>');

    getPoiRowEditButton(poiRs, poiSearchCmpyId, cmpys, function(data){
      tdEdit.append(data)
    });
    var row = $('<tr>').append(
        tdName,
        tdAddress,
        tdContact,
        tdCompany,
        tdFiles,
        tdEdit
    );
    return row;
  };

  /**
   * Return address in the proper way
   * @param address array of addresses
   */
  var getAddressView = function(addresses, type) {
    if (addresses.length == 0) {
      return;
    }
    var addressToShow = null;
    for(var aIdx = 0; aIdx < addresses.length; aIdx++) {
      if (addresses[aIdx].address_type == 'main') {
        addressToShow = addresses[aIdx];
      }
    }
    if (!addressToShow) {
      addressToShow = addresses[0];
    }

    var result = $('<address>').append(addressToShow.street_address).append($('<br/>'));
    if (addressToShow.locality) {
      result.append(addressToShow.locality);
    }
    if (addressToShow.postal_code) {
      result.append("&nbsp;"+addressToShow.postal_code);
    }
    result.append($('<br/>'));
    if (addressToShow.country_code) {
      var countryIdx = countryCodes3.indexOf(addressToShow.country_code);
      if (countryIdx > 0) {
        result.append(countries[countryIdx]);
      }
    }
    return result;
  };

  /**
   * Return address in the proper way
   * @param address array of addresses
   */
  var getContactView = function(poiRs) {

    var phoneNumbers = poiRs['phone-numbers'];
    var result = $('<address>');

    for (var pIdx = 0; pIdx < phoneNumbers.length; pIdx++) {
      var phone = phoneNumbers[pIdx];
      var phoneLine = $('<div>');
      switch(phone['phone-type']) {
        case "tollfree":
          phoneLine.text('Tellfree: ');
          break;
        case "day":
          phoneLine.text('Day Phone: ');
          break;
        case "evening":
          phoneLine.text('Evening Phone: ');
          break;
        case "night":
          phoneLine.text('Night Phone: ');
          break;
        case "emergency":
          phoneLine.text('Emergency Line: ');
          break;
        case "business":
          phoneLine.text('Business Phone: ');
          break;
        case "personal":
          phoneLine.text('Personal Phone: ');
          break;
        case "mobile":
          phoneLine.text('Mobile: ');
          break;
        case "fax":
          phoneLine.text('Fax: ');
          break;
        case "main":
        default:
          phoneLine.text('Phone: ');
          break;
      }
      $('<a>', {href:'callto:' + phone.number})
          .text(phone.number)
          .appendTo(phoneLine);
      result.append(phoneLine);
    }

    var urls = poiRs['urls'];
    for (var uIdx = 0; uIdx < urls.length; uIdx++) {
      var url = urls[uIdx];
      if (url.length == 0) {
        continue;
      }
      if (uIdx == 0) {
        var urlDiv = $('<div>').text('Website: ');
        if (url.indexOf('http') == -1) {
          url = 'http://' + url;
        }
        $('<a>', {
          href: url,
          target: "_blank"}).text(url).appendTo(urlDiv);

        result.append(urlDiv);
      }
    }

    return result;
  };

  var getCompanyView = function(poiRs, cmpies, callback) {
    var result;
    var cmpy = cmpies[poiRs['cmpy-id']];
    if (cmpy) {
      result = $('<div>').text(cmpy.name);
    } else {
      result = $('<div>').text("Unknown Company");
    }
    callback(result);
  };

  //ASYNC Function: callback
  var getPoiRowEditButton = function(poiRs, poiSearchCmpyId, cmpies, callback) {
    var cmpy = cmpies[poiSearchCmpyId];
    var isEditable = false;
    if (cmpy) {
      isEditable = ['ADMIN', 'POWER'].indexOf(cmpy.user_link_type) > -1;
    }
    //If record is public and admin/power for currently searched company

    var result = $('<button>', {
      class:"btn btn-info pull-right",
      id:'poi_' + poiRs['poi-id-js']
    })
        .text(isEditable?"Edit":"Details")
        .click(function(){
          //coming in the first time - so hide msg and reset active tab
          headLineAlertHide();
          currEditorUI.setPoi(poiRs['poi-id-js'], poiSearchCmpyId);
          return false;
        });

    callback(result);
  };

  var getPoiRowFilesButton = function(poiRs) {
    var result;
    if (poiRs['file-count'] > 0) {
      result = $('<button>', {
        type: "button",
        class: "btn btn-primary",
        id: 'poi' + poiRs['poi-id-js']
      }).text("Photos").click(function () {
        getModalPage('/poi/photos/' + poiRs['poi-id-js'] + '/' + poiRs['cmpy-id']);
        return false;
      });
    }
    return result;
  };

  var clearCurrentSearchResults = function() {
    $('#poiTabs li').hide();
    $.each($('table').filter(function(){ return this.id.match('tablePoiType[0-9]+')}), function(k,v) {
      $(v).find('tbody > tr').remove();
    });
  };

  //Actual rendering of the data in the storage
  clearCurrentSearchResults();

  var poiMap = $.jStorage.get(getJStorageKey('um_poi_cache'));
  var poiSearchTerm =  $.jStorage.get(getJStorageKey('um_poi_search_term'));
  var poiSearchCmpyId =  $.jStorage.get(getJStorageKey('um_poi_search_cmpy_id'));

  $('#inCmpyId option[value='+ poiSearchCmpyId + ']').attr('selected', true);

  if (poiSearchTerm == null) {
    $('#searchMsg').text('RECENTLY UPDATED');
  } else {
    $('#searchMsg').text('SEARCH RESULTS: ' + poiSearchTerm);
    //$('#poiSearchTerm').val(poiSearchTerm); //Commented out as it keeps overwriting
  }

  getUserCompanies(function(cmpies) {
    $.each(poiMap, function (idx, val) {
      $('#tablePoiType' + val['poi-type-id']).append(buildPoiTableRow(val, poiSearchCmpyId, cmpies));
      $('#poiType' + val['poi-type-id'] + 'Tab').show(); //TODO: Think how to optimize this
      $('#poiTypeCount' + val['poi-type-id']).text($('#tablePoiType' + val['poi-type-id'] + ' tr').length - 1);
    });

    for (i = 1; i <= $('#poiTabs li').length; i++) {
      if ($('#poiType' + i + 'Tab').is(":visible")) {
        $('#poiType' + i + 'Tab a').tab('show');
        break;
      }
    }
  });
};

/**  Initializes poi storage and subscribes to key changes */
var poiStorageInit = function() {
  var poiMap = $.jStorage.get(getJStorageKey('um_poi_cache'));
  if (poiMap == null) {
    poiMap = {};
    $.jStorage.set(getJStorageKey('um_poi_cache'), poiMap);
  } else {
    poiStorageRender();
  }

  $.jStorage.stopListening(getJStorageKey('um_poi_cache'));
  $.jStorage.listenKeyChange(getJStorageKey('um_poi_cache'), function(key, action){
    //console.log("jStorage um_poi_cache event: " + action + " key:" + key);
    if (action == 'updated') {
      poiStorageRender();
    }
  });

  if (Object.keys(poiMap).length == 0) {//NOT IE 8 compatible
    poiSearch();
  }
};

/** Store array of pois as it arrives from the server */
var poiStorageStore = function (poiArray, search_term, cmpyId) {
  //console.log("poiStorageStore term:" + search_term + " cmpy:" + cmpyId);
  var poiMap = {}; //Map of poi ids to poi record data
  $.each(poiArray, function (idx, val) {
    poiMap[val['poi-id-js']] = val;
  });
  if (search_term != null) {
    $.jStorage.set(getJStorageKey('um_poi_search_term'), search_term);
  } else {
    $.jStorage.deleteKey(getJStorageKey('um_poi_search_term'));
  }

  if (cmpyId != null) {
    $.jStorage.set(getJStorageKey('um_poi_search_cmpy_id'), cmpyId);
  }

  $.jStorage.set(getJStorageKey('um_poi_cache'), poiMap);
};

/** Refreshes storage based on previous search values */
var poiStorageRefresh = function() {
  //console.log("poiStorageRefresh");
  var s_term   = $.jStorage.get(getJStorageKey('um_poi_search_term'));
  var s_cmpyId =  $.jStorage.get(getJStorageKey('um_poi_search_cmpy_id'));

  if (s_term) {
    poiSearch(s_term, s_cmpyId, true);
  } else {
    poiSearch(null, null, true);
  }
};

/** Add or update poi stored locally */
var poiStorageUpdate = function(poi) {
  //console.log("poiStorageUpdate: " + poiLogOut(poi));
  var poiMap = $.jStorage.get(getJStorageKey('um_poi_cache'));
  if (!poiMap) {
    poiMap = {};
  }
  poiMap[poi['poi-id-js']] = poi;
  $.jStorage.set(getJStorageKey('um_poi_cache'), poiMap);
};

/** Delete poi stored locally */
var poiStorageDelete = function(poi) {
  poiStorageDeleteById(poi['poi-id-js']);
};

var poiStorageDeleteById = function(poiId) {
  //console.log("poiStorageDelete: " + poiLogOut(poi));
  var poiMap = $.jStorage.get(getJStorageKey('um_poi_cache'));
  if (!poiMap) {
    poiMap = {};
  }
  delete poiMap[poiId];
  $.jStorage.set(getJStorageKey('um_poi_cache'), poiMap);
};

/** Clears all stored records */
var poiStorageClear = function() {
  $.jStorage.deleteKey(getJStorageKey('um_poi_cache'));
  $.jStorage.deleteKey(getJStorageKey('um_poi_search_term'));
  $.jStorage.deleteKey(getJStorageKey('um_poi_search_cmpy_id'));
};

/** Return specific poi information */
var poiStorageGet = function (poiId, cmpyId, callback) {
  //console.log("poiStorageGet:" + poiId + " cmpy:" + cmpyId);
  //console.trace();
  var poiMap = $.jStorage.get(getJStorageKey('um_poi_cache'));
  var poiSearchCmpy = $.jStorage.get(getJStorageKey('um_poi_search_cmpy_id'));
  var poi = null;
  if (poiMap) {
    poi = poiMap[poiId];
  }
  if (!poi || (poiSearchCmpy != cmpyId)) {
    poiSearchById(poiId, cmpyId, callback);
  }
  else {
    callback(poi);
  }
};

var poiLogOut = function(poi) {
  return poi['poi-id-js'] + " for " + poi['cmpy-id'];
};
