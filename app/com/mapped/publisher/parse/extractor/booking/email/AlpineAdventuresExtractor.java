package com.mapped.publisher.parse.extractor.booking.email;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.EmailBodyExtractor;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by twong on 15-10-18.
 */
public class AlpineAdventuresExtractor  extends EmailBodyExtractor {
  private static String[] dateTimeFormat = {"MM/dd/yyyy HH:mm", "MM/dd/yyyy h:mma","MM/dd/yyyy hh:mma"};
  private static DateUtils du = new DateUtils();

  @Override
  protected void initImpl(Map<String, String> params) {

  }

  @Override
  protected TripVO extractDataImpl(String input) throws Exception {
    TripVO tripVO = new TripVO();
    NoteVO noteVO= null;
    Document doc = Jsoup.parse(input);

    for (Element e: doc.select("table")) {
      Elements rows = e.select("tr");
      if (rows.size() > 0) {
        Elements firstCols = rows.get(0).select("td");
        if(firstCols != null) {
          String s = firstCols.toString();
          if ((s.contains("colspan=\"4\"") || (s.contains("colspan=\"2\"") && !s.contains("width"))) && rows.size() > 1) {
            firstCols = rows.get(1).select("td");
          }
        }
        if (firstCols.size()==1 && firstCols.get(0).text().contains("Itinerary")) {
          processIntro(tripVO, getText(e));
        } else if (firstCols.size() >1 && firstCols.get(0).text().contains("Item") && firstCols.get(firstCols.size() -1).text().contains("Total")) {
          for (int i = 1; i < rows.size(); i++) {
            Element row = rows.get(i);
            Elements cols = row.select("td");
            Element col = cols.get(0);
            Elements tables = col.select("table");

            if (tables != null && tables.size() == 1) {
              String price = "";
              String taxes = "";
              String total = getText(cols.get(cols.size() - 1));

              if (cols.size() == 4) {
                price = getText(cols.get(1));
                taxes = getText(cols.get(2));
              }


              String line = getText(tables.get(0).select("tr").first());
              String lowerLine = line.toLowerCase();
              if (line.contains("Check In") && line.contains("Check Out") && !line.contains("Travel Agent Fee") && !line
                  .contains("Mis Line")) {
                parseHotel(tripVO, line, price, taxes, total);
              } else if (line.contains("Pick Up:") && line.contains("Return:")) {
                parseCar(tripVO, line, price, taxes, total);
              } else if (line.contains("Transfer ") || line.contains("Transfers ") || lowerLine.contains("one-way") || lowerLine.contains("one way") || lowerLine.contains("one way") || lowerLine.contains("suv") || lowerLine.contains("shared ride")) {
                parseTransfer(tripVO, line, price, taxes, total);
              }
              else if (line.contains("Travel Agent Fee") || line.contains("Mis Line") || line.contains("Total Package")) {
                parseNote(tripVO, line);
              }
              else if (line.contains("Air Itinerary")) {
                Element airTable = col.select("table").first();
                Elements airRows = airTable.select("tr");
                int j = 0;
                String recLocator = null;
                for (Element r: airRows) {
                  Elements airCols = r.select("td");
                  j++;
                  if (j == 1) {
                    String s = airCols.get(0).text();
                    if (s.contains("Itinerary") && s.contains(":")) {
                      recLocator = s.substring(s.indexOf(":") + 1).trim();
                    }
                  } else {
                    processFlight(tripVO, airCols, recLocator);
                  }
                }
              }
              else if (line.contains("Date:")) {
                parseActivity(tripVO, line, price, taxes, total);
              }

            } else if (row.text().startsWith("Total ") && tripVO.hasValidBookings()) {
              noteVO = parseTotal(tripVO, row);
            }
          }
        } else if (firstCols.text().contains("Upcoming Payments")) {
          if (noteVO != null) {
            noteVO.setNote(noteVO.getNote() +"<br/>" + e.toString() );
            noteVO.setName("Payment Information");
          }
        }

      }

    }

    if (noteVO != null) {
      tripVO.addNoteVO(noteVO);
    }
    tripVO.setImportSrc(BookingSrc.ImportSrc.ALPINE);
    tripVO.setImportTs(System.currentTimeMillis());


    return tripVO;
  }

  private void processFlight (TripVO tripVO, Elements cols, String recLocator) {
    FlightVO flightVO = new FlightVO();
    flightVO.setReservationNumber(recLocator);
    DateTime departDateTime = null;
    String details = getText(cols.get(0));
    String[] tokens = details.split("\n");
    StringBuilder sb = new StringBuilder();
    int i = 0;
    for (String s: tokens) {
      i++;
      if (i == 1) {
        flightVO.setCode(s.trim());
      } else if (s.contains("Flight Number:")) {
        flightVO.setNumber(s.substring(s.indexOf("Flight Number:") + "Flight Number:".length()));
      } else {
        sb.append(s); sb.append("\n");
      }
    }

    String depart = getText(cols.get(1));
    tokens = depart.split("\n");
    i = 0;
    AirportVO departVO = new AirportVO();
    String departDate = null;
    String departTime = null;
    for (String s: tokens) {
      i++;
      if (i == 1) {
        if (s.contains("(") && s.contains(")")) {
          departVO.setCode(s.substring(s.indexOf("(") + 1, s.indexOf(")")).trim());
          if (s.indexOf("(") > 5)
            departVO.setName(s);

        } else {
          departVO.setName(s);
        }

      } else if (i==2) {
        departDate = s.trim();
      } else if (i == 3){
        departTime = s.trim();
      }
    }
    if (departDate != null && departTime != null) {
      flightVO.setDepartureAirport(departVO);
      flightVO.setDepatureTime(getTimestamp(departDate, departTime));
      if (flightVO.getDepatureTime() != null) {
         departDateTime = new DateTime(flightVO.getDepatureTime());
      }
    }

    String arrive = getText(cols.get(2));
    tokens = arrive.split("\n");
    i = 0;
    AirportVO arriveVO = new AirportVO();
    String arriveDate = null;
    String arriveTime = null;
    for (String s: tokens) {
      i++;
      if (i == 1) {
        if (s.contains("(") && s.contains(")")) {
          arriveVO.setCode(s.substring(s.indexOf("(") + 1, s.indexOf(")")).trim());
          if (s.indexOf("(") > 5)
            arriveVO.setName(s);

        } else {
          arriveVO.setName(s);
        }
      } else if (i==2) {
        arriveDate = s.trim();
      } else if (i == 3){
        arriveTime = s.trim();
      }
    }
    if (arriveDate != null && arriveTime != null) {
      if (arriveDate.toUpperCase().contains("NEXT DAY") && departDateTime != null) {
        //we move forward 1 day from the depart date
        arriveDate = Utils.formatTimestamp(departDateTime.plusDays(1).getMillis(), "MM/dd/yyyy");
      } else if (arriveDate.toUpperCase().contains("DAY") && departDateTime != null) {
        //we move back 1 day from the depart date
        arriveDate = Utils.formatTimestamp(departDateTime.minusDays(1).getMillis(), "MM/dd/yyyy");
      }
      flightVO.setArrivalTime(getTimestamp(arriveDate, arriveTime));
    }
    flightVO.setArrivalAirport(arriveVO);

    flightVO.setNotes(sb.toString());


    if (flightVO.isValid()) {
      tripVO.addFlight(flightVO);
    }


  }


  private void parseHotel (TripVO tripVO, String line, String price, String taxes, String total) {
    String[] tokens = line.split("\n");
    HotelVO hotelVO = new HotelVO();
    StringBuilder sb = new StringBuilder();
    int i = 0;
    for (String s : tokens) {
      s = s.trim();
      i++;
      if (i == 2 && hotelVO.getName() == null) {
        hotelVO.setName(s);
      } else if (s.contains("Confirmation:")) {
        hotelVO.setHotelName(s.substring(0, s.indexOf("Confirmation:")));
        hotelVO.setConfirmation(s.substring(s.indexOf("Confirmation:") + "Confirmation:".length()));
        if (hotelVO.getHotelName().contains("CONFIRMED")) {
          String hotelName = hotelVO.getHotelName().replaceAll("CONFIRMED","").replaceAll("\\*","");
          hotelVO.setName(hotelName);
        }
      } else if (s.contains("Check In:")) {
        hotelVO.setCheckin(getTimestamp(s.substring(s.indexOf("Check In:") + "Check In:".length()), "15:00"));

      } else if (s.contains("Check Out:")) {
        hotelVO.setCheckout(getTimestamp(s.substring(s.indexOf("Check Out:") + "Check Out:".length()), "12:00"));

      } else {
        sb.append(s);
        sb.append("\n");
      }
    }

    if (price != null && !price.isEmpty() && (line == null || !line.contains(price))) {
      sb.append("\n");
      sb.append("Price:"); sb.append(price);sb.append("\n");
    }
    if (taxes != null && !taxes.isEmpty()) {
      sb.append("Taxes:"); sb.append(taxes);sb.append("\n");
    }
    if (total != null && !total.isEmpty()) {
      sb.append("\n");
      sb.append("Total:"); sb.append(total);sb.append("\n");
    }
    hotelVO.setNote(sb.toString());


    if (hotelVO.isValid()) {
      tripVO.addHotel(hotelVO);
    }

  }

  private void parseActivity (TripVO tripVO, String line, String price, String taxes, String total) {
    String[] tokens = line.split("\n");
    int i = 0;
    StringBuilder sb = new StringBuilder();
    ActivityVO activityVO = new ActivityVO();

    for (String s : tokens) {
      i++;
      if (i == 2) {
        activityVO.setName(s);
        if (activityVO.getName().contains("CONFIRMED")) {
          String activityName = activityVO.getName().replaceAll("CONFIRMED","").replaceAll("\\*","");
          activityVO.setName(activityName);
        }
      } else if (s.contains("Date:")) {
        s = s.trim();
        if (s.length() > 16) {
          sb.append(s.substring(16));
          s = s.substring(0, 16);
        }
        activityVO.setStartDate(getTimestamp(s.substring(s.indexOf("Date:") + "Date:".length()).trim(), "00:00"));
      } else {
        sb.append(s);
        sb.append("\n");
      }
    }
    if (price != null && !price.isEmpty()  && (line == null || !line.contains(price))) {
      sb.append("\n");
      sb.append("Price:"); sb.append(price);sb.append("\n");
    }
    if (taxes != null && !taxes.isEmpty()) {
      sb.append("Taxes:"); sb.append(taxes);sb.append("\n");
    }
    if (total != null && !total.isEmpty()) {
      sb.append("\n");
      sb.append("Total:"); sb.append(total);sb.append("\n");
    }
    activityVO.setNote(sb.toString());
    if (activityVO.isValid()) {
      tripVO.addActivity(activityVO);
    }

  }

  private void parseTransfer (TripVO tripVO, String line, String price, String taxes, String total) {
    String[] tokens = line.split("\n");
    int i = 0;
    StringBuilder sb = new StringBuilder();
    TransportVO transportVO = new TransportVO();
    if (line.toLowerCase().contains("rail")) {
      transportVO.setBookingType(ReservationType.RAIL);

    } else {
      transportVO.setBookingType(ReservationType.TRANSPORT);
    }

    for (String s : tokens) {
      i++;
      if (i == 2) {
        transportVO.setName(s);
        if (transportVO.getName().contains("CONFIRMED")) {
          String activityName = transportVO.getName().replaceAll("CONFIRMED","").replaceAll("\\*","");
          transportVO.setName(activityName);
        }
      } else if (s.contains("Date:")) {
        if (s.length() > 16) {
          sb.append(s.substring(16));
          s = s.substring(0, 16);
        }
        transportVO.setPickupDate(getTimestamp(s.substring(s.indexOf("Date:") + "Date:".length()).trim(), "00:00"));
      } else {
        sb.append(s);
        sb.append("\n");
      }
    }
    if (price != null && !price.isEmpty() && (line == null || !line.contains(price))) {
      sb.append("\n");
      sb.append("Price:"); sb.append(price);sb.append("\n");
    }
    if (taxes != null && !taxes.isEmpty()) {
      sb.append("Taxes:"); sb.append(taxes);sb.append("\n");
    }
    if (total != null && !total.isEmpty()) {
      sb.append("\n");
      sb.append("Total:"); sb.append(total);sb.append("\n");
    }
    transportVO.setNote(sb.toString());
    if (transportVO.isValid()) {
      tripVO.addTransport(transportVO);
    }
  }

  private void parseCar (TripVO tripVO, String line, String price, String taxes, String total) {
    String[] tokens = line.split("\n");
    int i = 0;
    StringBuilder sb = new StringBuilder();
    TransportVO transportVO = new TransportVO();
   transportVO.setBookingType(ReservationType.CAR_RENTAL);


    for (String s : tokens) {
      i++;
      if (s.contains("Confirmation:")) {
        transportVO.setConfirmationNumber(s.substring(s.indexOf("Confirmation:") + "Confirmation:".length()));
        sb.append(s.replace("Confirmation:", " Confirmation:"));sb.append("\n");
      } else if (i == 3) {
        transportVO.setName(s);
      } else if (s.contains("Pick Up:")) {
        String date = s.substring(s.indexOf("Pick Up:")+"Pick Up:".length());
        transportVO.setPickupDate(getTimestamp(date, null));
      }  else if (s.contains("Return:")) {
        String date = s.substring(s.indexOf("Return:")+"Return:".length());
        transportVO.setDropoffDate(getTimestamp(date, null));
      } else {
        sb.append(s);
        sb.append("\n");
      }
    }
    if (price != null && !price.isEmpty() && (line == null || !line.contains(price))) {
      sb.append("\n");
      sb.append("Price:"); sb.append(price);sb.append("\n");
    }
    if (taxes != null && !taxes.isEmpty()) {
      sb.append("Taxes:"); sb.append(taxes);sb.append("\n");
    }
    if (total != null && !total.isEmpty()) {
      sb.append("\n");
      sb.append("Total:"); sb.append(total);sb.append("\n");
    }
    transportVO.setNote(sb.toString());
    if (transportVO.isValid()) {
      tripVO.addTransport(transportVO);
    }
  }

  private void parseNote (TripVO tripVO, String line) {
    String[] tokens = line.split("\n");
    int i = 0;
    StringBuilder sb = new StringBuilder();
    NoteVO noteVO = new NoteVO();

    for (String s : tokens) {
      i++;
      if (i == 1 && s.contains("CONFIRMED")) {
        if (s.contains(")")) {
          String s1 = s.substring(s.indexOf(")") + 1).trim();
          if (s1 != null && s1.trim().length() > 0) {
            noteVO.setName(s1);
          }
        }
        sb.append(s);
        sb.append("<br");
      } else if (i == 2 && noteVO.getName() == null) {
        noteVO.setName(s);
      } else if (s.contains("Date:")){
        String date = s.substring(s.indexOf("Date:") + 5).trim();
        if (date != null) {
          if (date.length() > 11) {
            noteVO.setTimestamp(getTimestamp(date, null));
          } else {
            noteVO.setTimestamp(getTimestamp(date, "00:00"));
          }
        }

      } else if (s.contains("Check In:")){
        String date = s.substring(s.indexOf("Check In:") + 9).trim();
        if (date != null) {
          if (date.length() > 11) {
            noteVO.setTimestamp(getTimestamp(date, null));
          } else {
            noteVO.setTimestamp(getTimestamp(date, "00:00"));
          }
        }

      } else {
        sb.append(s);
        sb.append("<br>");
      }
    }

    noteVO.setNote(sb.toString());
    if (noteVO.getName() != null  && noteVO.getNote().trim().length() > 0) {
      tripVO.addNoteVO(noteVO);
    }

  }
  private void processIntro (TripVO tripVO, String line) {
    String[] tokens = line.split("\n");
    int i = 0;
    StringBuilder sb = new StringBuilder();
    NoteVO noteVO = new NoteVO();

    for (String s : tokens) {
      i++;
      if (i == 1) {
        noteVO.setName(s);
      } else {
        if (s.contains("Travel Agent Information") || s.contains("Payment Information")) {
          sb.append("<br>");
        }
        sb.append(s);
        sb.append("<br>");
      }
    }

    noteVO.setNote(sb.toString());
    if (noteVO.getName() != null  && noteVO.getNote().trim().length() > 0) {
      tripVO.addNoteVO(noteVO);
    }

  }

  private NoteVO parseTotal (TripVO tripVO, Element row) {
    int i = 0;
    StringBuilder sb = new StringBuilder();
    NoteVO noteVO = new NoteVO();
    noteVO.setName("Total");

    Elements cols =  row.select("td");
    if (cols.size() == 4) {
      sb.append("Price: ");sb.append(cols.get(1).text());sb.append("<br>");
      sb.append("Taxes/Fees: ");sb.append(cols.get(2).text());sb.append("<br>");
      sb.append("Total: ");sb.append(cols.get(3).text());sb.append("<br>");
    }

    noteVO.setNote(sb.toString());
    return noteVO;

  }


  @Override
  public ArrayList<ParseError> getParseErrors()
  {
    return null;
  }

  public static void main (String[] args) {
    try {
      String s = FileUtils.readFileToString(new File("/home/twong/Dropbox/sync/wip/pnrs/skimax/sample10.txt"));
      AlpineAdventuresExtractor extractor = new AlpineAdventuresExtractor();
      s = s.replaceAll("\\\\n","");
      s = s.replaceAll("\\\\t","");
      TripVO tripVO = extractor.extractData(s);
      System.out.println(tripVO.toString());
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  public static String getText(Element parentElement) {
    StringBuilder working = new StringBuilder();
    for (Node child : parentElement.childNodes()) {
      if (child instanceof TextNode) {
        working.append(((TextNode) child).text());
      }
      if (child instanceof Element) {
        Element childElement = (Element)child;
        // do more of these for p or other tags you want a new line for
        if (childElement.tag().getName().equalsIgnoreCase("br") || childElement.tag().getName().equalsIgnoreCase("tr")|| childElement.tag().getName().equalsIgnoreCase("div")) {
          working.append("\n");
        }

        working.append(getText(childElement));
      }
    }

    return working.toString().trim();
  }

  private static Timestamp getTimestamp (String date, String time) {
    Timestamp t = null;
    if (date != null) {
      date = date.trim();
    }
    if (time != null) {
      time = time.trim();
    }
    try {
      if (time != null) {
        t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime());
      } else {
        t = new Timestamp(du.parseDate(date, dateTimeFormat).getTime());
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return t;
  }

}
