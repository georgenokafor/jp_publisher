package com.mapped.publisher.msg.flightstat.status;

/**
 * Created by twong on 2015-02-10.
 */
public class Equipment {
  private String iata;
  private String name;
  private String turboProp;
  private String jet;
  private String widebody;
  private String regional;

  public String getIata() {
    return iata;
  }

  public void setIata(String iata) {
    this.iata = iata;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTurboProp() {
    return turboProp;
  }

  public void setTurboProp(String turboProp) {
    this.turboProp = turboProp;
  }

  public String getJet() {
    return jet;
  }

  public void setJet(String jet) {
    this.jet = jet;
  }

  public String getWidebody() {
    return widebody;
  }

  public void setWidebody(String widebody) {
    this.widebody = widebody;
  }

  public String getRegional() {
    return regional;
  }

  public void setRegional(String regional) {
    this.regional = regional;
  }
}
