package com.umapped.persistence.reservation.accommodation;

import com.umapped.persistence.reservation.UmDateTime;
import com.umapped.persistence.reservation.UmTraveler;

public class UmAccommodationTraveler
    extends UmTraveler {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  /**
   * 	The earliest someone may check into a lodging establishment.
   */
  private UmDateTime checkinTime;
  /**
   * 	The latest someone may check out of a lodging establishment.
   */
  private UmDateTime checkoutTime;
  private String   bedding;
  private String   roomNumber;
  private String   roomType;
  /**
   * [Number] Number of adults who will be staying in the lodging unit.
   */
  private Integer  numAdults;
  /**
   * [Number]	Number of children who will be staying in the lodging unit.
   */
  private Integer  numChildren;
  private String   amenities;
  /****************************************************************************
   * Fields from ticket: PUB-922                                              *
   ****************************************************************************/
  private String   checkinDetails;

  public UmDateTime getCheckinTime() {
    return checkinTime;
  }

  public UmAccommodationTraveler setCheckinTime(UmDateTime checkinTime) {
    this.checkinTime = checkinTime;
    return this;
  }

  public UmDateTime getCheckoutTime() {
    return checkoutTime;
  }

  public UmAccommodationTraveler setCheckoutTime(UmDateTime checkoutTime) {
    this.checkoutTime = checkoutTime;
    return this;
  }

  public String getBedding() {
    return bedding;
  }

  public UmAccommodationTraveler setBedding(String bedding) {
    this.bedding = bedding;
    return this;
  }

  public String getRoomNumber() {
    return roomNumber;
  }

  public UmAccommodationTraveler setRoomNumber(String roomNumber) {
    this.roomNumber = roomNumber;
    return this;
  }

  public String getRoomType() {
    return roomType;
  }

  public UmAccommodationTraveler setRoomType(String roomType) {
    this.roomType = roomType;
    return this;
  }

  public Integer getNumAdults() {
    return numAdults;
  }

  public UmAccommodationTraveler setNumAdults(Integer numAdults) {
    this.numAdults = numAdults;
    return this;
  }

  public Integer getNumChildren() {
    return numChildren;
  }

  public UmAccommodationTraveler setNumChildren(Integer numChildren) {
    this.numChildren = numChildren;
    return this;
  }

  public String getAmenities() {
    return amenities;
  }

  public UmAccommodationTraveler setAmenities(String amenities) {
    this.amenities = amenities;
    return this;
  }

  public String getCheckinDetails() {
    return checkinDetails;
  }

  public UmAccommodationTraveler setCheckinDetails(String checkinDetails) {
    this.checkinDetails = checkinDetails;
    return this;
  }
}
