package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-02
 * Time: 7:45 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "trip_cmpy")
public class TripCmpy
    extends Model {

  @Id
  public String pk;

  @Constraints.Required
  public String tripid;
  public String comments;
  public int status;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;

  @OneToOne
  @JoinColumn(name = "cmpyid")
  public Company cmpy;

  @Version
  public int version;

  public static Model.Finder<String, TripCmpy> find = new Finder(TripCmpy.class);

  public static List<TripCmpy> findByTripId(String tripId) {
    return find.where().eq("tripid", tripId).eq("status", 0).findList();
  }

  public static List<TripCmpy> findByCmpyId(String cmpyId) {
    return find.where().eq("cmpyid", cmpyId).eq("status", 0).findList();
  }

  public static List<TripCmpy> findByTripIDCmpyId(String tripId, String cmpyId) {
    return find.where().eq("cmpyid", cmpyId).eq("tripid", tripId).eq("status", 0).findList();
  }

  public static int countByCmpyIds(List<String> cmpies, String tourId) {
    return find.where().eq("tripid", tourId).eq("status", 0).in("cmpyid", cmpies).findCount();
  }

  public static int mergeCmpy(String srcCmpyId, String targetCmpyId, String userId) {
    String s = "UPDATE trip_cmpy set cmpyid = :targetCmpyId, lastupdatedtimestamp = :timestamp, " +
               "" + "modifiedby = :userid  where cmpyid = :srcCmpyId and status = 0";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("timestamp", System.currentTimeMillis());
    update.setParameter("userid", userId);
    update.setParameter("srcCmpyId", srcCmpyId);
    update.setParameter("targetCmpyId", targetCmpyId);

    return Ebean.execute(update);
  }

  public String getPk() {
    return pk;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }


  public String getTripid() {
    return tripid;
  }

  public void setTripid(String tripid) {
    this.tripid = tripid;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public Company getCmpy() {
    return cmpy;
  }

  public void setCmpy(Company cmpy) {
    this.cmpy = cmpy;
  }
}
