package com.mapped.publisher.actions;

import com.mapped.publisher.common.ConfigMgr;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by surge on 2015-11-30.
 */
public class Secured
    extends Action.Simple {

  @Override public CompletionStage<Result> call(Http.Context ctx) {


    if (ConfigMgr.isProduction() ) {
      if (ctx.request().headers().get("X-Forwarded-Proto") != null) {
        String[] values = ctx.request().headers().get("X-Forwarded-Proto");
        for (String s:values) {
          if (s.trim().equalsIgnoreCase("http")) {
            return CompletableFuture.completedFuture(redirect("https://" + ctx.request().host() + ctx.request().uri()));
          }
        }
      }
    }

    return delegate.call(ctx);
  }
}
