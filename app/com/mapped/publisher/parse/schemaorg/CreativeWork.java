package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by surge on 2016-06-28.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CreativeWork
    extends Thing {
  /**
   * Place	The location of the content.
   */
  public Place contentLocation;

  /**
   * Media type (aka MIME format, see IANA site) of the content e.g. application/zip of a SoftwareApplication binary.
   * In cases where a CreativeWork has several media type representations, 'encoding' can be used to indicate each
   * MediaObject alongside particular fileFormat information.
   * Text
   */
  public String fileFormat;

  /**
   * Keywords or tags used to describe this content. Multiple entries in a keywords list are typically delimited by
   * commas.
   * Text
   */
  public String keywords;

  /**
   * The position of an item in a series or sequence of items.
   * 	Integer or Text
   */
  public Integer position;


  /*
  @formatter:off
  about	Thing	The subject matter of the content.
  accessibilityAPI	Text	Indicates that the resource is compatible with the referenced accessibility API (WebSchemas wiki lists possible values).
  accessibilityControl	Text	Identifies input methods that are sufficient to fully control the described resource (WebSchemas wiki lists possible values).
  accessibilityFeature	Text	Content features of the resource, such as accessible media, alternatives and supported enhancements for accessibility (WebSchemas wiki lists possible values).
  accessibilityHazard	Text	A characteristic of the described resource that is physiologically dangerous to some users. Related to WCAG 2.0 guideline 2.3 (WebSchemas wiki lists possible values).
  accountablePerson	Person	Specifies the Person that is legally accountable for the CreativeWork.
  aggregateRating	AggregateRating	The overall rating, based on a collection of reviews or ratings, of the item.
  alternativeHeadline	Text	A secondary title of the CreativeWork.
  associatedMedia	MediaObject	A media object that encodes this CreativeWork. This property is a synonym for encoding.
  audience	Audience	The intended audience of the item, i.e. the group for whom the item was created.
  audio	AudioObject	An embedded audio object.
  author	Organization or Person	The author of this content. Please note that author is special in that HTML 5 provides a special mechanism for indicating authorship via the rel tag. That is equivalent to this and may be used interchangeably.
  award	Text	An award won by this person or for this creative work.
  awards	Text	Awards won by this person or for this creative work.
  character	Person	Fictional person connected with a creative work.
  citation	CreativeWork or Text	A citation or reference to another creative work, such as another publication, web page, scholarly article, etc.
  comment	Comment or UserComments	Comment on the RSVP.
  commentCount	Integer	The number of comments this CreativeWork (e.g. Article, Question or Answer) has received. This is most applicable to works published in Web sites with commenting system; additional comments may exist elsewhere.
  contentRating	Text	Official rating of a piece of content—for example,'MPAA PG-13'.
  contributor	Organization or Person	A secondary contributor to the CreativeWork.
  copyrightHolder	Organization or Person	The party holding the legal copyright to the CreativeWork.
  copyrightYear	Number	The year during which the claimed copyright for the CreativeWork was first asserted.
  creator	Organization or Person	The person / organization that made the reservation.
  dateCreated	Date	The date on which the CreativeWork was created.
  dateModified	Date or DateTime	Time the reservation was last modified.
  datePublished	Date	Date of first broadcast/publication.
  discussionUrl	URL	A link to the page containing the comments of the CreativeWork.
  editor	Person	Specifies the Person who edited the CreativeWork.
  educationalAlignment	AlignmentObject	An alignment to an established educational framework.
  educationalUse	Text	The purpose of a work in the context of education; for example, 'assignment', 'group work'.
  encoding	or MediaObject	How to encode the action into the http request when the method is POST.
  encodings	MediaObject	A media object that encodes this CreativeWork.
  exampleOfWork	CreativeWork	A creative work that this work is an example/instance/realization/derivation of.
  genre	Text	Genre of the creative work or group.
  hasPart	CreativeWork	Indicates a CreativeWork that is (in some sense) a part of this CreativeWork.
  headline	Text	Headline of the article.
  inLanguage	Text	The language of the content. please use one of the language codes from the IETF BCP 47 standard.
  interactionCount	Text	A count of a specific user interactions with this item—for example, 20 UserLikes, 5 UserComments, or 300 UserDownloads. The user interaction type should be one of the sub types of UserInteraction.
  interactivityType	Text	The predominant mode of learning supported by the learning resource. Acceptable values are 'active', 'expositive', or 'mixed'.
  isBasedOnUrl	URL	A resource that was used in the creation of this resource. This term can be repeated for multiple sources. For example, http://example.com/great-multiplication-intro.html.
  isFamilyFriendly	Boolean	Indicates whether this content is family friendly.
  isPartOf	CreativeWork	Indicates a CreativeWork that this CreativeWork is (in some sense) part of.
  learningResourceType	Text	The predominant type or kind characterizing the learning resource. For example, 'presentation', 'handout'.
  license	CreativeWork or URL	A license document that applies to this content, typically indicated by URL.
  mentions	Thing	Indicates that the CreativeWork contains a reference to, but is not necessarily about a concept.
  offers	Offer	An offer to provide this item—for example, an offer to sell a product, rent the DVD of a movie, or give away tickets to an event.
  producer	Organization or Person	The person or organization who produced the work (e.g. music album, movie, tv/radio series etc.).
  provider	Organization or Person	The organization providing the reservation.
  publisher	Organization	The publisher of the creative work.
  publishingPrinciples	URL	Link to page describing the editorial principles of the organization primarily responsible for the creation of the CreativeWork.
  recordedAt	Event	The Event where the CreativeWork was recorded. The CreativeWork may capture all or part of the event.
  releasedEvent	PublicationEvent	The place and time the release was issued, expressed as a PublicationEvent.
  review	Review	The review.
  reviews	Review	Review of the item.
  sourceOrganization	Organization	The Organization on whose behalf the creator was working.
  text	Text	The textual content of this CreativeWork.
  thumbnailUrl	URL	A thumbnail image relevant to the Thing.
  timeRequired	Duration	Approximate or typical time it takes to work with or through this learning resource for the typical intended target audience, e.g. 'P30M', 'P1H25M'.
  translator	Organization or Person	Organization or person who adapts a creative work to different languages, regional differences and technical requirements of a target market.
  typicalAgeRange	Text	The typical expected age range, e.g. '7-9', '11-'.
  version	Number	The version of the CreativeWork embodied by a specified resource.
  video	VideoObject	An embedded video object.
  workExample	CreativeWork	Example/instance/realization/derivation of the concept of this creative work. eg. The paperback edition, first edition, or eBook.

  @formatter:on
  */
}
