var countries = ['Afghanistan',
'Aland Islands',
'Albania',
'Algeria',
'American Samoa',
'Andorra',
'Angola',
'Anguilla',
'Antarctica',
'Antigua And Barbuda',
'Argentina',
'Armenia',
'Aruba',
'Australia',
'Austria',
'Azerbaijan',
'Bahamas',
'Bahrain',
'Bangladesh',
'Barbados',
'Belarus',
'Belgium',
'Belize',
'Benin',
'Bermuda',
'Bhutan',
'Bolivia Plurinational State Of',
'Bonaire Sint Eustatius And Saba',
'Bosnia And Herzegovina',
'Botswana',
'Bouvet Island',
'Brazil',
'British Indian Ocean Territory',
'Brunei Darussalam',
'Bulgaria',
'Burkina Faso',
'Burundi',
'Cambodia',
'Cameroon',
'Canada',
'Cape Verde',
'Cayman Islands',
'Central African Republic',
'Chad',
'Chile',
'China',
'Christmas Island',
'Cocos Keeling) Islands',
'Colombia',
'Comoros',
'Congo',
'Congo The Democratic Republic Of The',
'Cook Islands',
'Costa Rica',
'Cote D\'Ivoire',
'Croatia',
'Cuba',
'Curacao',
'Cyprus',
'Czech Republic',
'Denmark',
'Djibouti',
'Dominica',
'Dominican Republic',
'Ecuador',
'Egypt',
'El Salvador',
'Equatorial Guinea',
'Eritrea',
'Estonia',
'Ethiopia',
'Falkland Islands Malvinas',
'Faroe Islands',
'Fiji',
'Finland',
'France',
'French Guiana',
'French Polynesia',
'French Southern Territories',
'Gabon',
'Gambia',
'Georgia',
'Germany',
'Ghana',
'Gibraltar',
'Greece',
'Greenland',
'Grenada',
'Guadeloupe',
'Guam',
'Guatemala',
'Guernsey',
'Guinea',
'Guinea-Bissau',
'Guyana',
'Haiti',
'Heard Island And Mcdonald Islands',
'Holy See Vatican City State',
'Honduras',
'Hong Kong',
'Hungary',
'Iceland',
'India',
'Indonesia',
'Iran Islamic Republic Of',
'Iraq',
'Ireland',
'Isle Of Man',
'Israel',
'Italy',
'Jamaica',
'Japan',
'Jersey',
'Jordan',
'Kazakhstan',
'Kenya',
'Kiribati',
'Korea Democratic People\'S Republic Of',
'Korea Republic Of',
'Kuwait',
'Kyrgyzstan',
'Lao People\'S Democratic Republic',
'Latvia',
'Lebanon',
'Lesotho',
'Liberia',
'Libyan Arab Jamahiriya',
'Liechtenstein',
'Lithuania',
'Luxembourg',
'Macao',
'Macedonia The Former Yugoslav Republic Of',
'Madagascar',
'Malawi',
'Malaysia',
'Maldives',
'Mali',
'Malta',
'Marshall Islands',
'Martinique',
'Mauritania',
'Mauritius',
'Mayotte',
'Mexico',
'Micronesia Federated States Of',
'Moldova Republic Of',
'Monaco',
'Mongolia',
'Montenegro',
'Montserrat',
'Morocco',
'Mozambique',
'Myanmar',
'Namibia',
'Nauru',
'Nepal',
'Netherlands',
'New Caledonia',
'New Zealand',
'Nicaragua',
'Niger',
'Nigeria',
'Niue',
'Norfolk Island',
'Northern Mariana Islands',
'Norway',
'Oman',
'Pakistan',
'Palau',
'Palestinian Territory Occupied',
'Panama',
'Papua New Guinea',
'Paraguay',
'Peru',
'Philippines',
'Pitcairn',
'Poland',
'Portugal',
'Puerto Rico',
'Qatar',
'Reunion',
'Romania',
'Russian Federation',
'Rwanda',
'Saint Barthelemy',
'Saint Helena Ascension And Tristan Da Cunha',
'Saint Kitts And Nevis',
'Saint Lucia',
'Saint Martin French Part',
'Saint Pierre And Miquelon',
'Saint Vincent And The Grenadines',
'Samoa',
'San Marino',
'Sao Tome And Principe',
'Saudi Arabia',
'Senegal',
'Serbia',
'Seychelles',
'Sierra Leone',
'Singapore',
'Sint Maarten Dutch Part',
'Slovakia',
'Slovenia',
'Solomon Islands',
'Somalia',
'South Africa',
'South Georgia And The South Sandwich Islands',
'South Sudan',
'Spain',
'Sri Lanka',
'Sudan',
'Suriname',
'Svalbard And Jan Mayen',
'Swaziland',
'Sweden',
'Switzerland',
'Syrian Arab Republic',
'Taiwan Province Of China',
'Tajikistan',
'Tanzania United Republic Of',
'Thailand',
'Timor-Leste',
'Togo',
'Tokelau',
'Tonga',
'Trinidad And Tobago',
'Tunisia',
'Turkey',
'Turkmenistan',
'Turks And Caicos Islands',
'Tuvalu',
'Uganda',
'Ukraine',
'United Arab Emirates',
'United Kingdom',
'United States',
'United States Minor Outlying Islands',
'Uruguay',
'Uzbekistan',
'Vanuatu',
'Venezuela Bolivarian Republic Of',
'Viet Nam',
'Virgin Islands British',
'Virgin Islands U.S.',
'Wallis And Futuna',
'Western Sahara',
'Yemen',
'Zambia',
'Zimbabwe'];

    var countryCodes2 = ['AF','AX','AL','DZ','AS','AD','AO','AI','AQ','AG','AR','AM','AW','AU','AT','AZ','BS','BH','BD','BB','BY','BE','BZ','BJ','BM','BT','BO','BQ','BA','BW','BV','BR','IO','BN','BG','BF','BI','KH','CM','CA','CV','KY','CF','TD','CL','CN','CX','CC','CO','KM','CG','CD','CK','CR','CI','HR','CU','CW','CY','CZ','DK','DJ','DM','DO','EC','EG','SV','GQ','ER','EE','ET','FK','FO','FJ','FI','FR','GF','PF','TF','GA','GM','GE','DE','GH','GI','GR','GL','GD','GP','GU','GT','GG','GN','GW','GY','HT','HM','VA','HN','HK','HU','IS','IN','ID','IR','IQ','IE','IM','IL','IT','JM','JP','JE','JO','KZ','KE','KI','KP','KR','KW','KG','LA','LV','LB','LS','LR','LY','LI','LT','LU','MO','MK','MG','MW','MY','MV','ML','MT','MH','MQ','MR','MU','YT','MX','FM','MD','MC','MN','ME','MS','MA','MZ','MM','NA','NR','NP','NL','NC','NZ','NI','NE','NG','NU','NF','MP','NO','OM','PK','PW','PS','PA','PG','PY','PE','PH','PN','PL','PT','PR','QA','RE','RO','RU','RW','BL','SH','KN','LC','MF','PM','VC','WS','SM','ST','SA','SN','RS','SC','SL','SG','SX','SK','SI','SB','SO','ZA','GS','SS','ES','LK','SD','SR','SJ','SZ','SE','CH','SY','TW','TJ','TZ','TH','TL','TG','TK','TO','TT','TN','TR','TM','TC','TV','UG','UA','AE','GB','US','UM','UY','UZ','VU','VE','VN','VG','VI','WF','EH','YE','ZM','ZW'];
    var countryCodes3 = ['AFG','ALA','ALB','DZA','ASM','AND','AGO','AIA','ATA','ATG','ARG','ARM','ABW','AUS','AUT','AZE','BHS','BHR','BGD','BRB','BLR','BEL','BLZ','BEN','BMU','BTN','BOL','BES','BIH','BWA','BVT','BRA','IOT','BRN','BGR','BFA','BDI','KHM','CMR','CAN','CPV','CYM','CAF','TCD','CHL','CHN','CXR','CCK','COL','COM','COG','COD','COK','CRI','CIV','HRV','CUB','CUW','CYP','CZE','DNK','DJI','DMA','DOM','ECU','EGY','SLV','GNQ','ERI','EST','ETH','FLK','FRO','FJI','FIN','FRA','GUF','PYF','ATF','GAB','GMB','GEO','DEU','GHA','GIB','GRC','GRL','GRD','GLP','GUM','GTM','GGY','GIN','GNB','GUY','HTI','HMD','VAT','HND','HKG','HUN','ISL','IND','IDN','IRN','IRQ','IRL','IMN','ISR','ITA','JAM','JPN','JEY','JOR','KAZ','KEN','KIR','PRK','KOR','KWT','KGZ','LAO','LVA','LBN','LSO','LBR','LBY','LIE','LTU','LUX','MAC','MKD','MDG','MWI','MYS','MDV','MLI','MLT','MHL','MTQ','MRT','MUS','MYT','MEX','FSM','MDA','MCO','MNG','MNE','MSR','MAR','MOZ','MMR','NAM','NRU','NPL','NLD','NCL','NZL','NIC','NER','NGA','NIU','NFK','MNP','NOR','OMN','PAK','PLW','PSE','PAN','PNG','PRY','PER','PHL','PCN','POL','PRT','PRI','QAT','REU','ROU','RUS','RWA','BLM','SHN','KNA','LCA','MAF','SPM','VCT','WSM','SMR','STP','SAU','SEN','SRB','SYC','SLE','SGP','SXM','SVK','SVN','SLB','SOM','ZAF','SGS','SSD','ESP','LKA','SDN','SUR','SJM','SWZ','SWE','CHE','SYR','TWN','TJK','TZA','THA','TLS','TGO','TKL','TON','TTO','TUN','TUR','TKM','TCA','TUV','UGA','UKR','ARE','GBR','USA','UMI','URY','UZB','VUT','VEN','VNM','VGB','VIR','WLF','ESH','YEM','ZMB','ZWE'];
    var request;
    var currentPage;


    <!-- ajax get -->
    function getPage(urlStr) {
        if (request) {
            request.abort();
        }
        $('#spinner').show();

        request = $.ajax({
                         type: "GET",
                         cache:false,
                         url: urlStr,
                         dataType: "html"
         });


         // callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR){
             currentPage = urlStr;
             window.location.hash = urlStr;
             displayPage(response);
             displayHelp();
        });

         // callback handler that will be called on failure
         request.fail(function (jqXHR, textStatus, errorThrown){
            // log the error to the console
            if(textStatus != 'abort') {
                $('#alertMsg').text("A System Error has occurred - " + textStatus);
                $('#alertBox').show();
            }
         });

         // callback handler that will be called regardless
         // if the request failed or succeeded
         request.always(function () {
            $('#spinner').hide();

          });

    }

    function doPost(urlStr) {
        if (request) {
            request.abort();
        }
        $('#spinner').show();

        request = $.ajax({
            type: "POST",
            url: urlStr,
            dataType: "html"
        });

        // callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR){

            displayPage(response);

        });

        // callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown){
            // log the error to the console

            $('#alertMsg').text("A System Error has occurred - " + textStatus);
            $('#alertBox').show();
        });

        // callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            $('#spinner').hide();
        });
    }

    <!-- ajax post with serialized post data -->
    function postForm(urlStr, formId) {
    var $form = $("#"+formId);

    if (!$form.valid()) {
        return;
    }

    if (request) {
            request.abort();
        }
        $('#spinner').show();


        // let's select and cache all the fields
        var $inputs = $form.find("input, select, button, textarea");
        // serialize the data in the form
        var serializedData = $form.serialize();
        // let's disable the inputs for the duration of the ajax request
        $inputs.prop("disabled", true);


        request = $.ajax({
            type: "POST",
            url: urlStr,
            data: serializedData
        });

        // callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR){
            displayPage(response);

        });

        // callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown){

            // log the error to the console

            $('#alertMsg').text("A System Error has occurred - " + textStatus);
            $('#alertBox').show();
        });

        // callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            $('#spinner').hide();
            $inputs.prop("disabled", false);

        });
    }

<!-- ajax post with serialized post data -->


    function doSearchForm(urlStr, formId, divId) {
            if (divId == null || divId.length == 0) {
                divId = 'searchResults';
            }
            var $form = $("#"+formId);

            if (!$form.valid()) {
            	//hide the image spinner
            	$('#imgSearchSpinner').hide();
            	$('#fileSearchSpinner').hide();
            	$('#spinner').hide();

                return;
            }

            if (request) {
                request.abort();
            }
            $('#spinner').show();


            // let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");
            // serialize the data in the form
            var serializedData = $form.serialize();
            // let's disable the inputs for the duration of the ajax request
            $inputs.prop("disabled", true);


            request = $.ajax({
                type: "GET",
                cache:false,
                url: urlStr,
                data: serializedData
            });

            // callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR){

                displaySearchResults(response, divId);
            });

            // callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown){

                // log the error to the console
                $('#alertMsg').text("A System Error has occurred - " + textStatus);
                $('#alertBox').show();
            });

            // callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                $('#spinner').hide();
            	$('#imgSearchSpinner').hide();
            	$('#fileSearchSpinner').hide();
                $inputs.prop("disabled", false);
            });
    }

    <!-- handle ajax HTML response pageHeader pageBody pageFooter-->
    function displayPage(html) {
      $('#spinner').hide();
        <!-- hide alert -->
        $("#alertBox").hide();

    <!-- remove existing divs before writing html to DOM    -->
    var alertMsg = html.match(/<--alertMsg-->([\s\S]*?)<--endAlertMsg-->/g);
    if (alertMsg!=null) {
        var a = alertMsg[0].replace("<--alertMsg-->",'');
        a = a.replace("<--endAlertMsg-->",'');
        $('#alertMsg').remove();

        $('#alertBox').append(a);
        if (a.length > 2) {
        $("#alertBox").show();
        }
    }


    <!-- remove pageTitle divs before writing html to DOM script not escaped -->
    var pageTitle = html.match(/<--pageTitle-->([\s\S]*?)<--endPageTitle-->/g);
    if (pageTitle != null) {
        var a= pageTitle[0].replace("<--pageTitle-->","");
        a =a.replace("<--endPageTitle-->","");
        $("#pageTitle").remove();
        $("#contentTitle").append(a);
    }


    <!-- remove pageHeader existing divs before writing html to DOM    -->
    var pageHeader = html.match(/<--pageHeader-->([\s\S]*?)<--endPageHeader-->/g);
    if (pageHeader != null) {
        var a = pageHeader[0].replace("<--pageHeader-->","");
        a = a.replace("<--endPageHeader-->","");
        $("#pageHeader").remove();
        $("#contentHeader").append(a);
    }

    <!-- remove pageBody existing divs before writing html to DOM    -->
    var pageBody = html.match(/<--pageBody-->([\s\S]*?)<--endPageBody-->/g);
    if (pageBody != null) {
        var a = pageBody[0].replace("<--pageBody-->","");
        a = a.replace("<--endPageBody-->","");
        $("#pageBody").remove();

    $("#contentBody").append(a);
    }

    <!-- remove existing divs before writing html to DOM    -->
    var pageFooter = html.match(/<--pageFooter-->([\s\S]*?)<--endPageFooter-->/g);
    if (pageFooter != null) {
        var a = pageFooter[0].replace("<--pageFooter-->","");
        a = a.replace("<--endPageFooter-->","");
        $("#pageFooter").remove();
        $("#contentFooter").append(a);
    }

    if (alertMsg ==null && pageTitle == null && pageHeader == null && pageBody == null && pageFooter == null) {
        //redirect to login
        window.location.href="/home";

    }


 //       $('html, body').animate({scrollTop:0}, 'slow');
    }

    <!-- handle ajax HTML search results -->
    function displaySearchResults(html, divId) {
        $("#" + divId+ "Table").remove();
        $("#" +divId).append(html);
    }


<!-- do search call -->
function getSearchResult(urlStr, tableName, interruptable) {

  interruptable = typeof  interruptable !== "undefined" ? interruptable : true;

  var localRequest;

  if (request && interruptable) {
    request.abort();
  }

  $('#spinner').show();

  localRequest = $.ajax({
                          type: "GET",
                          cache: false,
                          url: urlStr,
                          dataType: "html"
                        });

  if (interruptable){
    request = localRequest
  }


  // callback handler that will be called on success
  localRequest.done(function (response, textStatus, jqXHR) {
    currentPage = urlStr;
    window.location.hash = urlStr;
    displaySearchTable(response, tableName);
  });

  // callback handler that will be called on failure
  localRequest.fail(function (jqXHR, textStatus, errorThrown) {
    // log the error to the console

    $('#alertMsg').text("A System Error has occurred - " + textStatus);
    $('#alertBox').show();
  });

  // callback handler that will be called regardless
  // if the request failed or succeeded
  localRequest.always(function () {
    $('#spinner').hide();
  });
}

    <!-- handle ajax HTML search results -- apend rows to current table-->
    function displaySearchTable(response, tableName) {

           if (response ==null || response.indexOf('<html') > 0) {
                   //redirect to login
                   window.location.href="/home";

           } else {
                $('#' + tableName + ' > tbody:last').append(response);


           }

            //$('#' + tableName + ' tr:last').after(response);
    }


    $('.alert .close').live("click", function(e) {
        $(this).parent().hide();
    });

    $(function(){
        $("#alertBox").hide();
    });


    $(function(){
        var sidebar = $('.sidebar-nav');  // cache sidebar to a variable for performance

        sidebar.delegate('.nav-list','click',function(){
            if($(this).hasClass('active')){
                $(this).removeClass('active');
                $(this).addClass('inactive');
            }else{
                sidebar.find('.active').addClass('inactive');
                sidebar.find('.active').removeClass('active');
                $(this).removeClass('inactive');
                $(this).addClass('active');
            }
        });
    });

    function selectSideBar(menuItem) {
        var sidebar = $('.sidebar-nav');
        sidebar.find('.active').addClass('inactive');
        sidebar.find('.active').removeClass('active');

        $('#'+menuItem).removeClass('inactive');
        $('#'+menuItem).addClass('active');
    }


<!-- get and display content into a dynamic modal form -->
    function getModalPage(urlStr) {
        if (request) {
            request.abort();
        }
        $('#spinner').show();

        request = $.ajax({
            type: "GET",
            cache:false,
            url: urlStr,
            dataType: "html"
        });

        // callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR){
            displayModal(response);
        });

        // callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown){
            // log the error to the console

            $('#alertMsg').text("A System Error has occurred - " + textStatus);
            $('#alertBox').show();
        });

        // callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            $('#spinner').hide();
        });
    }


    <!-- handle ajax HTML search results -->
    function displayModal(html) {
        $("#dynamicModalBody").remove();
        $("#dynamicModal").append(html);
        $('#dynamicModalBody').modal();
    }

   
    
    function reloadPage(reqParam) {
    	var hashUrl =  window.location.hash.substring(1);
    	if (window.location.hash != "" && hashUrl != "" ){
    		//check to see if we need to append the reqParam to the url
    		if (reqParam != null) {
    			if (hashUrl.indexOf(reqParam) == -1) {
    				//req param is not part of the url so append if
    				if (hashUrl.indexOf("?") == -1) {
    					hashUrl += "?" + reqParam;
    				} else {
    				    hashUrl += "&" + reqParam;
					}
    			}
    		}
    		getPage(hashUrl);

    	}
    }



    //IE file upload support
    var uploadSuccessUrl;
    var uploadDiv;


        function signedS3 (url, callback, s3divId, divId) {
            if (divId == null || divId.length == 0) {
                divId="uploadPhoto";
            }
            if (s3divId == null || s3divId.length == 0) {
                            s3divId="";
            }

            var  xhr;
            xhr = new XMLHttpRequest();
            xhr.open('POST', url, true);

            xhr.onreadystatechange = function(e) {
                var result;
                if (this.readyState === 4 && this.status === 200) {
                    try {
                        result = JSON.parse(this.responseText);
                    } catch (error) {
                        this_s3upload.onError('Signing server returned some ugly/empty JSON: "' + this.responseText + '"');
                        window.location.href="/home"
                        return false;
                    }
                    $("#key" + s3divId).val(result.key);
                    $("#policy" + s3divId).val(result.policy);
                    $("#signature" + s3divId).val(result.signature);
                    $("#AWSAccessKeyId" + s3divId).val(result.accessKey);

                    return callback(result.signed_request, decodeURIComponent(result.url), result.s3Bucket, divId);
                } else if (this.readyState === 4 && this.status !== 200) {
                    return this_s3upload.onError('Could not contact request signing server. Status = ' + this.status);
                }
            };
            return xhr.send();
        };

        function iFrameUpload(signedUrl, successUrl, s3Bucket, divId) {
            if (divId == null || divId.length == 0) {
                        divId="uploadPhoto";
            }
            //do ajax post form
            uploadSuccessUrl = successUrl;
            uploadDiv = divId;
            var options  = { url:        s3Bucket,
                            iframe: true,
                            success:       showResponse,
                            error: function (response, textStatus, xhr, form, divId) {
                                $('#' + divId + 'Modal' ).modal('hide');
                                 alert('Upload Failed');
                            }
            };

            $('#' + divId + 'Form').ajaxSubmit(options);

            return false;

        }

        // post-submit callback
        function showResponse(responseText, statusText, xhr, $form, divId)  {
            $('#'  + uploadDiv + 'Modal').modal('hide');
            doPost(uploadSuccessUrl);
        }
        
        
        // Image Library support
        // prepare the image picker modal
        var multipleFilesMode = 0;
        var photoUploadedCount = 0;

        function onImageClick(imgUrl, title, licenceUrl, fileName, cmpyId, tripId, docId, pageId, vendorId) {
            $('#imgErrorAlertBox').hide();
            $("#imgBody").show();
            

        	$("#fullImg").attr('src', imgUrl);
        	$("#fullImg").attr('title', title);
            $("#fullImg").attr('alt', title);

             $("#inImgCmpyId").val(cmpyId);
             $("#inImgTripId").val(tripId);
             $("#inImgDocId").val(docId);
             $("#inImgPageId").val(pageId);
             $("#inImgVendorId").val(vendorId);

            $("#inImgUrl").val(imgUrl);
            $("#inImgFileName").val(fileName);
        	$("#inImgPhotoCaption").val('');
        
        	$("#imgTitle").text(title);
            	$('#imgModal').modal('show');

            	if (licenceUrl != null ) {
            		//if this is from commons, display a different disclaimer
            		 if (licenceUrl.indexOf("wikimedia.org") > 0 || licenceUrl.indexOf("wikipedia.org") > 0) {
            		    $("#imgDisclaimer").text('Note: This is image is from the WikiMedia Commons Library and is free to use commercially under the terms of the Creative Commons Licence.');
            		    $("#imgLicence").text('Click here for details about the licence.');

					 } else  {
            		    $("#imgDisclaimer").text('Note: UMapped is not the copyright holder of this image; please ensure you are authorized to use this image commercially.');
            		    $("#imgLicence").text('Click here for details about the image.');
					 }
                	 $('#imgLicence').attr('href', licenceUrl);
                	$('#imgLicence').show();
            	}   else {
                	$('#imgLicence').hide();
            	}
            	

        }

        function postImgForm(urlStr, formId) {
        	var $form = $("#"+formId);

        	if (!$form.valid()) {
        		return;
        	}

        	if (request) {
        		request.abort();
        	}
        	$('#imgAddSpinner').show();


        	// let's select and cache all the fields
        	var $inputs = $form.find("input, select, button, textarea");
        	// serialize the data in the form
        	var serializedData = $form.serialize();
        	// let's disable the inputs for the duration of the ajax request
        	$inputs.prop("disabled", true);


        	request = $.ajax({
        					type: "POST",
        					url: urlStr,
        					data: serializedData
        			});

        	// callback handler that will be called on success
        	request.done(function (response, textStatus, jqXHR){
        		displayImgResponse(response, jqXHR.status);
			});

        	// callback handler that will be called on failure
        	request.fail(function (jqXHR, textStatus, errorThrown){

        	// log the error to the console
				$('#imgErrorAlertBox').text("A System Error has occurred - " + textStatus);
        		$('#imgErrorAlertBox').show();
        	});

        	// callback handler that will be called regardless
        	// if the request failed or succeeded
        	request.always(function () {
        		$('#imgAddSpinner').hide();
        		$inputs.prop("disabled", false);
			});
        }

        <!-- handle ajax HTML response pageHeader pageBody pageFooter-->
        function displayImgResponse(html, status) {
            photoUploadedCount++;

            $('#imgAddSpinner').hide();
            <!-- hide alert -->
            $("#imgAlertBox").hide();
            <!-- remove existing divs before writing html to DOM    -->
            var alertMsg = html.match(/<--alertMsg-->([\s\S]*?)<--endAlertMsg-->/g);
            if (alertMsg!=null) {
                var a = alertMsg[0].replace("<--alertMsg-->",'');
                a = a.replace("<--endAlertMsg-->",'');
                $('#alertMsg').remove();

                if(multipleFilesMode == 0){
                    $('#alertBox').append(a  );
                    if (a.length > 2) {
                        $("#alertBox").show();
                    }
                    $('#imgModal').modal('hide');
                    reloadNeeded();
                } else {
                    $('#imgAlertBox').append(a  );
                    if (a.length > 2) {
                        $("#imgAlertBox").show();
                        $('#imgModal').modal('hide');
                        $('#uploadPhotoModal').modal('show');
                    }
                }
            }

        }

