
package com.umapped.external.wetu.itinerary.list;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "type",
    "identifier",
    "identifier_key",
    "days",
    "name",
    "reference_number",
    "client_name",
    "client_email",
    "start_date",
    "last_modified",
    "access_count",
    "is_disabled",
    "categories",
    "last_viewed",
    "booking_status"
})
public class Itinerary {

    @JsonProperty("type")
    private String type;
    @JsonProperty("identifier")
    private String identifier;
    @JsonProperty("identifier_key")
    private String identifierKey;
    @JsonProperty("days")
    private int days;
    @JsonProperty("name")
    private String name;
    @JsonProperty("reference_number")
    private String referenceNumber;
    @JsonProperty("client_name")
    private String clientName;
    @JsonProperty("client_email")
    private String clientEmail;
    @JsonProperty("start_date")
    private String startDate;
    @JsonProperty("last_modified")
    private String lastModified;
    @JsonProperty("access_count")
    private int accessCount;
    @JsonProperty("is_disabled")
    private boolean isDisabled;
    @JsonProperty("categories")
    private List<String> categories = new ArrayList<String>();
    @JsonProperty("last_viewed")
    private String lastViewed;
    @JsonProperty("booking_status")
    private String bookingStatus;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The identifier
     */
    @JsonProperty("identifier")
    public String getIdentifier() {
        return identifier;
    }

    /**
     * 
     * @param identifier
     *     The identifier
     */
    @JsonProperty("identifier")
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * 
     * @return
     *     The identifierKey
     */
    @JsonProperty("identifier_key")
    public String getIdentifierKey() {
        return identifierKey;
    }

    /**
     * 
     * @param identifierKey
     *     The identifier_key
     */
    @JsonProperty("identifier_key")
    public void setIdentifierKey(String identifierKey) {
        this.identifierKey = identifierKey;
    }

    /**
     * 
     * @return
     *     The days
     */
    @JsonProperty("days")
    public int getDays() {
        return days;
    }

    /**
     * 
     * @param days
     *     The days
     */
    @JsonProperty("days")
    public void setDays(int days) {
        this.days = days;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The referenceNumber
     */
    @JsonProperty("reference_number")
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * 
     * @param referenceNumber
     *     The reference_number
     */
    @JsonProperty("reference_number")
    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    /**
     * 
     * @return
     *     The clientName
     */
    @JsonProperty("client_name")
    public String getClientName() {
        return clientName;
    }

    /**
     * 
     * @param clientName
     *     The client_name
     */
    @JsonProperty("client_name")
    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    /**
     * 
     * @return
     *     The clientEmail
     */
    @JsonProperty("client_email")
    public String getClientEmail() {
        return clientEmail;
    }

    /**
     * 
     * @param clientEmail
     *     The client_email
     */
    @JsonProperty("client_email")
    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    /**
     * 
     * @return
     *     The startDate
     */
    @JsonProperty("start_date")
    public String getStartDate() {
        return startDate;
    }

    /**
     * 
     * @param startDate
     *     The start_date
     */
    @JsonProperty("start_date")
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * 
     * @return
     *     The lastModified
     */
    @JsonProperty("last_modified")
    public String getLastModified() {
        return lastModified;
    }

    /**
     * 
     * @param lastModified
     *     The last_modified
     */
    @JsonProperty("last_modified")
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * 
     * @return
     *     The accessCount
     */
    @JsonProperty("access_count")
    public int getAccessCount() {
        return accessCount;
    }

    /**
     * 
     * @param accessCount
     *     The access_count
     */
    @JsonProperty("access_count")
    public void setAccessCount(int accessCount) {
        this.accessCount = accessCount;
    }

    /**
     * 
     * @return
     *     The isDisabled
     */
    @JsonProperty("is_disabled")
    public boolean isIsDisabled() {
        return isDisabled;
    }

    /**
     * 
     * @param isDisabled
     *     The is_disabled
     */
    @JsonProperty("is_disabled")
    public void setIsDisabled(boolean isDisabled) {
        this.isDisabled = isDisabled;
    }

    /**
     * 
     * @return
     *     The categories
     */
    @JsonProperty("categories")
    public List<String> getCategories() {
        return categories;
    }

    /**
     * 
     * @param categories
     *     The categories
     */
    @JsonProperty("categories")
    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    /**
     * 
     * @return
     *     The lastViewed
     */
    @JsonProperty("last_viewed")
    public String getLastViewed() {
        return lastViewed;
    }

    /**
     * 
     * @param lastViewed
     *     The last_viewed
     */
    @JsonProperty("last_viewed")
    public void setLastViewed(String lastViewed) {
        this.lastViewed = lastViewed;
    }

    /**
     * 
     * @return
     *     The bookingStatus
     */
    @JsonProperty("booking_status")
    public String getBookingStatus() {
        return bookingStatus;
    }

    /**
     * 
     * @param bookingStatus
     *     The booking_status
     */
    @JsonProperty("booking_status")
    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(type).append(identifier).append(identifierKey).append(days).append(name).append(referenceNumber).append(clientName).append(clientEmail).append(startDate).append(lastModified).append(accessCount).append(isDisabled).append(categories).append(lastViewed).append(bookingStatus).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Itinerary) == false) {
            return false;
        }
        Itinerary rhs = ((Itinerary) other);
        return new EqualsBuilder().append(type, rhs.type).append(identifier, rhs.identifier).append(identifierKey, rhs.identifierKey).append(days, rhs.days).append(name, rhs.name).append(referenceNumber, rhs.referenceNumber).append(clientName, rhs.clientName).append(clientEmail, rhs.clientEmail).append(startDate, rhs.startDate).append(lastModified, rhs.lastModified).append(accessCount, rhs.accessCount).append(isDisabled, rhs.isDisabled).append(categories, rhs.categories).append(lastViewed, rhs.lastViewed).append(bookingStatus, rhs.bookingStatus).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
