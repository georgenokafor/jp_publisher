package com.umapped.itinerary.analyze.supplier.musement;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.umapped.itinerary.analyze.recommendation.RecommendedItem;
import com.umapped.itinerary.analyze.supplier.musement.response.MusementCityLookup;
import com.umapped.service.offer.ActivityFilter;
import com.umapped.service.offer.ActivityOfferProvider;
import com.umapped.service.offer.OfferResult;
import com.umapped.service.offer.UMappedCity;
import org.apache.commons.lang3.tuple.Pair;


import java.util.List;

/**
 * Created by wei on 2017-06-14.
 */
@Singleton
public class MusementOfferProvider implements ActivityOfferProvider {
  @Inject
  private MusementWebService service;

  @Inject
  private MusementCityLookup cityLookup;

  @Override public OfferResult getActivityOffers(ActivityFilter filter) {
    UMappedCity city = filter.getLocation();
    if (city != null) {
      Pair<String, String> mCity = cityLookup.getCity(city.getId());
      if (mCity != null) {
        List<RecommendedItem> items = service.getEvents(mCity.getLeft(), mCity.getRight(), null);
        return new OfferResult(filter.getStayPeriods(), items);
      }
    }
    return null;
  }
}
