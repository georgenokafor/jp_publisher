
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MediaLanguage.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MediaLanguage">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="English"/>
 *     &lt;enumeration value="EnglishUK"/>
 *     &lt;enumeration value="EnglishCA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MediaLanguage", namespace = "http://enterprise.globusandcosmos.com/ebl/")
@XmlEnum
public enum MediaLanguage {

    @XmlEnumValue("English")
    ENGLISH("English"),
    @XmlEnumValue("EnglishUK")
    ENGLISH_UK("EnglishUK"),
    @XmlEnumValue("EnglishCA")
    ENGLISH_CA("EnglishCA");
    private final String value;

    MediaLanguage(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MediaLanguage fromValue(String v) {
        for (MediaLanguage c: MediaLanguage.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
