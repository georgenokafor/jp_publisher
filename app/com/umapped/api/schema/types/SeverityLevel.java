package com.umapped.api.schema.types;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Severity levels for different error codes. Current will be used in JSON return codes
 *
 * Created by surge on 2014-09-24.
 */
public enum SeverityLevel {
  /** Most important error. Platform is not functional. Developers/admins should contacted */
  FATAL,
  /** System level error affecting users. Admins should be contacted  */
  CRITICAL,
  /** Recoverable but severe error that affects users */
  SEVERE,
  /** Recoverable error */
  ERROR,
  /** Abnormal conditions that are not errors, usually should be logged */
  WARNING,
  /** Nice to have information on the operation of the system, might be not logged */
  INFO,
  /** Report log only when in debug mode */
  DEBUG,
  /** No logging/reporting required under any log level */
  OFF;

  @JsonValue @Override
  public String toString() {
    return this.name();
  }
}
