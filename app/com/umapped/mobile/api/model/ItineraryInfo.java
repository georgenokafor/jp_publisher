package com.umapped.mobile.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.view.BusinessCardView;

/**
 * Created by wei on 2017-06-30.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ItineraryInfo {
  public String           id;
  public ItineraryStatus  status;
  public String           name;
  public String           coverUrl;
  public Long             startTs;
  public Long             finishTs;
  public String           note;
  public BusinessCardView bc;
  public SecurityMgr.AccessLevel access;
  public boolean          collab;
}
