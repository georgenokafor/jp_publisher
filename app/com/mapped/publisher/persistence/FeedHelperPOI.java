package com.mapped.publisher.persistence;

import com.umapped.api.schema.types.Address;
import models.publisher.PoiSrcFeed;
import org.apache.commons.codec.language.Metaphone;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

/**
 * The interface that can be passed to one of the functions iterating over
 * ResultSet of specified POJO objects.
 *
 * Designed after Strategy Design Pattern.
 *
 * Created by surge on 2015-02-02.
 */
public abstract class FeedHelperPOI {
  protected final static int METAPHONE_LEN = 20;


  abstract public List<FeedResource> getResources();
  abstract public Map<PoiFeedUtil.FeedResult, Integer> load(String parameter);
  abstract public PoiImportRS.ImportState associate(PoiImportRS rs);
  abstract public models.publisher.PoiSrcFeed.FeedSyncResult synchronize(models.publisher.PoiSrcFeed psf);
  abstract public List<Pair<PoiRS, Integer>> getRankedPois(PoiImportRS importRS, boolean ignoreCountry);
  abstract public PoiSrcFeed.FeedSyncResult clearAmenities(PoiSrcFeed psf);


  public void processDuplicateRecord(int srcId) {

  }

  protected int compareNames(String a, String b) {
    a = simplifyName(a);
    b = simplifyName(b);

    List<String> aParts = Arrays.asList(a.split(" "));
    List<String> bParts = Arrays.asList(b.split(" "));

    Set<String> aSet = new HashSet<>();
    Set<String> bSet = new HashSet<>();

    //Removing duplicates, need unique words only
    aSet.addAll(aParts);
    bSet.addAll(bParts);

    int matched = 0;

    for(String ap : aSet) {
      for(String bp: bSet) {
        if(ap.equals(bp)) {
          ++matched;
        }
      }
    }

    int result = -100;
    if(aSet.size() == matched || bParts.size() == matched) {
      result = 0;
    }

    if (result != 0) {
      Metaphone metaphone = new Metaphone();
      metaphone.setMaxCodeLen(METAPHONE_LEN);

      a = StringUtils.join(aSet, " ");
      b = StringUtils.join(bSet, " ");

      String am = metaphone.metaphone(a);
      String bm = metaphone.metaphone(b);

      if(am.equals(bm)) {
        result = 0;
        //Log.debug("A: " + am + " B: " + bm);
        //Log.debug("A: " + a + " B: " + b + " MATCHED: " + matched + " RESULT: " + result);
      }

      if (result != 0 && ((aParts.size() == (matched + 1)) || (bParts.size() == (matched + 1)))) {
        result = -1;
      }
    }

    return result;
  }

  protected String simplifyName(String name) {
    name = name.toLowerCase()
               .replace("&", " all ")
               .replace("the ", "")
               .replaceAll("\\W+", " ")
               .replaceAll("\\s+", " ");
    return name;
  }

  /**
   * Ranks feed name as compared to POI name (from 0 to 35)
   *
   * @param feedName
   * @param poiName
   * @return  0, if no match
   *          25, if POI name contains feed name
   *          30, if Feed name contains POI name
   *          35, if names are equal
   */
  public static int rankName(String feedName, String poiName) {
    feedName = feedName.toLowerCase().replaceAll("\\W+", "");
    poiName = poiName.toLowerCase().replaceAll("\\W+", "");

    if (feedName.length() == 0 || poiName.length() == 0) {
      return 0;
    }

    if (feedName.equals(poiName)) {
      return 45;
    }

    if  (feedName.length() > poiName.length()) {
      if (feedName.contains(poiName)) {
        return 30;
      }
    }
    else {
      if (poiName.contains(feedName)) {
        return 25;
      }
    }
    return 0;
  }

  /**
   * Ranks feed address as compared to POI address (from 0 to 40)
   * @param nameA
   * @param nameB
   * @return  0, if no match
   *          30, if addresses are fuzzy matching
   *          40, if one of the addresses contains another or they are equal
   */
  public static int rankAddress(String nameA, String nameB) {
    nameA = nameA.toLowerCase().replaceAll("\\W+", "");
    nameB = nameB.toLowerCase().replaceAll("\\W+", "");

    if (nameA.length() == 0 || nameB.length() == 0) {
      return 0;
    }

    if  (nameA.length() > nameB.length() ? nameA.contains(nameB) : nameB.contains(nameA)) {
      return 40;
    }

    //Relaxing restrictions
    Metaphone metaphone = new Metaphone();

    metaphone.setMaxCodeLen(METAPHONE_LEN);

    return metaphone.isMetaphoneEqual(nameA, nameB)?30:0;
  }

  /**
   * Ranks feed locality (city, village) as compared to POI
   * @param locA
   * @param locB
   * @return  -5, if names are different
   *           0, if no match or one of the strings is empty
   *           5, if names sound similarly
   *          10, if one of the strings contains another
   *          15, if names are precisely equal
   */
  public static int rankLocality(String locA, String locB) {
    if (locA == null || locB == null) {
      return 0;
    }

    locA = locA.toLowerCase().replaceAll("\\W+", "");
    locB = locB.toLowerCase().replaceAll("\\W+", "");

    if (locA.length() == 0 || locB.length() == 0) {
      return 0;
    }

    if (locA.equals(locB)) {
      return 15;
    }

    if  (locA.length() > locB.length() ? locA.contains(locB) : locB.contains(locA)) {
      return 10;
    }

    //Relaxing restrictions
    Metaphone metaphone = new Metaphone();
    return metaphone.isMetaphoneEqual(locA, locB)?5:-5;
  }

  /**
   * Ranks feed phone as compared to POI phone (from 0 to 100)
   * @param phoneA
   * @param phoneB
   * @return  0, if phones don't match
   *          100, if phones match or one contains all digits of the other
   */
  protected int rankPhone(String phoneA, String phoneB) {
    if (phoneA == null || phoneB == null) {
      return 0;
    }
    phoneA = phoneA.replaceAll("\\D+", "");
    phoneB = phoneB.replaceAll("\\D+", "");

    if (phoneA.length() == 0 || phoneB.length() == 0) {
      return 0;
    }

    if (phoneA.length() > phoneB.length() ? phoneA.contains(phoneB) : phoneB.contains(phoneA)) {
      return 100;
    }
    return 0;
  }

  /**
   * Ranks feed postal code as compared to POI postal code (from -30 to 50)
   * @param codeA
   * @param codeB
   * @return  -30, if postal codes don't match
   *           50, if postal codes match
   */
  protected int rankPostalCode(String codeA, String codeB) {
    if (codeA == null || codeB == null) {
      return -30;
    }
    codeA = codeA.toLowerCase().replaceAll("\\s+", "");
    codeB = codeB.toLowerCase().replaceAll("\\s+", "");
    return codeA.equals(codeB)?50:-30;
  }

  protected int rankPoi(String cleanName, PoiImportRS importRS, PoiRS prs) {
    int score = 0;
    score += rankName(cleanName, prs.getName());

    if (prs.getMainPhone() != null) {
      score += rankPhone(prs.getMainPhone().getNumber(), importRS.phone);
    }

    Address address = prs.getMainAddress();
    if (address != null) {

      //Checking postal code
      if (importRS.postalCode != null && importRS.postalCode.length() > 0) {

        if (address.getPostalCode() != null && address.getPostalCode().length() > 0) {
          score += rankPostalCode(address.getPostalCode(), importRS.postalCode);
        }//Sometimes postal code is not in the field of its own.
        else if (address.getStreetAddress() != null &&
                 address.getStreetAddress().toLowerCase().contains(importRS.postalCode.toLowerCase())) {
          score += 50;
        }
      }

      //Checking street address
      if (address.getStreetAddress() != null &&
          importRS.address != null) {
        score += rankAddress(address.getStreetAddress(), importRS.address);
      }

      score += rankLocality(address.getLocality(), importRS.city);

      if (!StringUtils.equals(prs.countryCode, importRS.countryCode)) {
        score -= 20;
      }
    }

    //If record has some resemblance, give it a notch if it is a public record.
    if (score > 0 && prs.getCmpyId() == 0) {
      score += 5;
    }

    return score;
  }
}
