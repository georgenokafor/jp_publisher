package com.umapped.api.schema.local;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.umapped.api.schema.common.IBodyJson;
import com.umapped.api.schema.types.Operation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by wei on 2017-05-22.
 */
public class PoiPhotosJson implements IBodyJson {

  @JsonProperty(required = false, value = "operation")
  public Operation operation;

  @JsonProperty(required = false, value = "searchId")
  public String searchId;

  @JsonProperty(required = false, value = "cmpyId")
  public Integer cmpyId;

  @JsonProperty(required = false, value = "photos")
  public List<PoiPhoto> photos;


  public PoiPhotosJson() {
    photos = new ArrayList<>();
  }


  public PoiPhoto addPhoto(String thumbUrl, String fullUrl, String caption, String fileName, Long importId) {
    PoiPhoto photo = new PoiPhoto();
    photo.thumb = thumbUrl;
    photo.url = fullUrl;
    photo.caption = caption;
    photo.fileName = fileName;
    photo.importId = importId;

    photos.add(photo);
    return photo;
  }

  public void removePhoto(String url) {
    for (Iterator<PoiPhoto> it = photos.iterator(); it.hasNext(); ) {
      PoiPhoto currItem = it.next();
      if (currItem.url.equals(url)) {
        it.remove();
      }
    }
  }
}
