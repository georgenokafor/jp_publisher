package com.umapped.itinerary.analyze.location;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import modules.ItineraryAnalyzeModule;
import com.umapped.itinerary.analyze.model.Itinerary;
import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.model.Location;

import java.io.File;
import java.util.concurrent.CompletableFuture;

/**
 * Created by wei on 2017-03-22.
 */
public class LocationEnhancer {

  private TimeZoneLookupService timeZoneLookupService;


  @Inject public LocationEnhancer(TimeZoneLookupService timeZoneLookupService) {
    this.timeZoneLookupService = timeZoneLookupService;
  }

  private CompletableFuture asyncGetLocation(CompletableFuture previous, Location location) {
    CompletableFuture current = null;
    if (location != null && location.getGeoLocation() != null) {
      current = CompletableFuture.supplyAsync(() -> timeZoneLookupService.lookup(location.getGeoLocation()))
                                 .whenComplete((zoneId, throwable) -> {
                                   location.setTimezone(zoneId);
                                 })
                                 .toCompletableFuture();

    }
    if (previous != null) {
      if (current != null) {
        return previous.allOf(previous, current);
      }
      else {
        return previous;
      }
    }
    else {
      return current;
    }
  }

  private void printLocation(Location location) {
    System.out.println(location.getDescription() + ":" + location.getGeoLocation() + ":" + location.getTimezone());
  }

  private void printTimeZone(Itinerary itinerary) {
    for (ItineraryItem item : itinerary.getItems()) {
      printLocation(item.getEnhancedStartLocation());
      printLocation(item.getEnhancedEndLocation());
    }
  }


  public void getAsyncLocation(Itinerary itinerary) {
    final long t1 = System.currentTimeMillis();
    CompletableFuture allTimeZones = null;
    for (ItineraryItem item : itinerary.getItems()) {
      allTimeZones = asyncGetLocation(allTimeZones, item.getEnhancedStartLocation());
      allTimeZones = asyncGetLocation(allTimeZones, item.getEnhancedEndLocation());
    }

    if (allTimeZones == null) {
      printTimeZone(itinerary);
      System.out.println("Done");
    }
    else {
      allTimeZones.whenComplete(((o, throwable) -> {
        System.out.println("Done : " + (System.currentTimeMillis() - t1));
        printTimeZone(itinerary);
      }));
      allTimeZones.join();
    }
  }

  public void getSyncLocation(Itinerary itinerary) {
    final long t1 = System.currentTimeMillis();
    for (ItineraryItem item : itinerary.getItems()) {
      if (item.getEnhancedStartLocation() != null && item.getEnhancedStartLocation().getGeoLocation() != null) {
        item.getEnhancedStartLocation().setTimezone(timeZoneLookupService.lookup(item.getEnhancedStartLocation().getGeoLocation()));
      }

      if (item.getEnhancedEndLocation() != null && item.getEnhancedEndLocation().getGeoLocation() != null) {
        item.getEnhancedEndLocation().setTimezone(timeZoneLookupService.lookup(item.getEnhancedEndLocation().getGeoLocation()));
      }
    }
    System.out.println("Sync done: " + (System.currentTimeMillis()-t1));
    printTimeZone(itinerary);

  }

  public static void main(String[] args)
      throws Exception {
    Injector container = Guice.createInjector(new ItineraryAnalyzeModule());

    ObjectMapper om = container.getInstance(ObjectMapper.class);
    Itinerary itinerary = om.readValue(new File(
        "/Users/wei/DevResources/data/trips/itinerary/2017-03-09/1320929702090001363.json"), Itinerary.class);
    LocationEnhancer enhancer = container.getInstance(LocationEnhancer.class);
    enhancer.getAsyncLocation(itinerary);
    enhancer.getSyncLocation(itinerary);
  }
}
