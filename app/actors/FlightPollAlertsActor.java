package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.mapped.common.CoreConstants;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.*;
import com.mapped.publisher.msg.flightstat.status.Airline;
import com.mapped.publisher.msg.flightstat.status.Airport;
import com.mapped.publisher.msg.flightstat.status.FlightStatus;
import com.mapped.publisher.msg.flightstat.status.FlightStatusMsg;
import com.mapped.publisher.persistence.PoiMgr;
import com.mapped.publisher.persistence.PoiRS;
import com.mapped.publisher.persistence.PoiTypeInfo;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.UmappedEmail;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.FlightAlertView;
import com.umapped.api.schema.local.PoiJson;
import com.umapped.helper.TripPublisherHelper;
import com.umapped.helper.UmappedEmailFactory;
import com.umapped.persistence.reservation.flight.UmFlightReservation;

import controllers.BookingController;
import controllers.TourController;
import controllers.routes;
import models.publisher.*;
import org.apache.commons.lang3.time.StopWatch;
import org.joda.time.DateTime;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

/**
 * Created by twong on 2015-02-10.
 */
public class FlightPollAlertsActor
    extends UntypedActor {

  @Inject
  TripPublisherHelper tripPublisherHelper;
  @Inject
  WSClient ws;

  public interface Factory {
    public Actor create();
  }

  /**
   * Akka Actor Properties
   */
  public final static Props props = Props.create(FlightPollAlertsActor.class);

  public static String APPNAME = "FlightTracking";
  private static boolean DEBUG_COMMUNICATOR_NOTIFICATIONS = false;


  public static class Command {
    private DateTime timestamp;

    public Command(DateTime time) {
      this.timestamp = time;
    }

    public DateTime getTimestamp() {
      return timestamp;
    }
  }

  @Override
  public void onReceive(Object message)
      throws Exception {
    try {
      List<String> processes = (List<String>) CacheMgr.getMemcached(APPConstants.CACHE_SYS_ACTIVE_PROCESSES);
      if (processes == null) {
        processes = new ArrayList<>();
      }

      if (!processes.contains(APPNAME)) {
        processes.add(APPNAME);
        CacheMgr.setMemcached(APPConstants.CACHE_SYS_ACTIVE_PROCESSES, processes, APPConstants.CACHE_EXPIRY_SECS);
      }
      CacheMgr.setMemcached(APPConstants.CACHE_SYS_EXCEPTION_PREFX + APPNAME,
                   new ArrayList<String>(),
                   APPConstants.CACHE_EXPIRY_SECS);
      CacheMgr.setMemcached(APPConstants.CACHE_SYS_HEARTBEAT_PREFX + APPNAME,
                   String.valueOf(System.currentTimeMillis()),
                   APPConstants.CACHE_EXPIRY_SECS);

      StopWatch sw = new StopWatch();
      sw.start();
      DateTime startTime = new DateTime(System.currentTimeMillis());

      if (message instanceof Command) {
        Log.info("P===>>> Got flight tracking request...");
        Command msg = (Command) message;
        if (msg != null && msg.timestamp != null) {
          startTime = msg.timestamp;
        }

        int pCount = 0;
        int aCount = 0;
        long pTime = 0;
        long aTime = 0;
        //get all the alerts that we have not processed yet in a rolling 24 hour window
        List<FlightAlert> pendingAlerts = FlightAlert.findByStatusAndOrigDepart(FlightAlert.FlightAlertStatus.PENDING,
                                                                                startTime.minusDays(1).getMillis(),
                                                                                startTime.plusDays(1).getMillis());
        if (pendingAlerts != null) {
          for (FlightAlert alert : pendingAlerts) {
            //make the first call to FlightStats to make sure the flight can be tracked and get the local timestamp
            getFlightStatus(alert);
          }
          pCount = pendingAlerts.size();
          pTime = sw.getTime();
          Log.info(" Processed pending alerts: " + pCount + " in " + pTime + " ms");
        }

        //get all the alerts for active flights in the next 6 hour window using UTC timestamps for the flights
        sw.split();
        List<FlightAlert> activeAlerts = FlightAlert.findActiveUTCDepart(startTime.plusMinutes(30).getMillis(),
                                                                         startTime.plusHours(6).getMillis());
        if (activeAlerts != null) {
          for (FlightAlert alert : activeAlerts) {
            //only track if the flight is leaving in more than 30 mins - presumably you are in the airport then
            getFlightStatus(alert);
          }
          aCount = activeAlerts.size();
          aTime = sw.getTime();
          sw.stop();
          Log.info(" Processed active alerts: " + activeAlerts.size() + " in " + sw.getTime() + " ms");
        }

        StringBuilder sb = new StringBuilder();
        sb.append("Process ")
          .append(pCount)
          .append(" pending flights in ")
          .append(pTime)
          .append(" ms and ")
          .append(aCount)
          .append(" active flights in ")
          .append(aTime)
          .append(" ms at ")
          .append(startTime);

        CacheMgr.setMemcached(APPConstants.CACHE_SYS_STATUS_PREFX + APPNAME, sb.toString(), APPConstants.CACHE_EXPIRY_SECS);
        CacheMgr.setMemcached(APPConstants.CACHE_SYS_HEARTBEAT_PREFX + APPNAME,
                     String.valueOf(System.currentTimeMillis()),
                     APPConstants.CACHE_EXPIRY_SECS);
      }
    }
    catch (Exception e) {
      Log.err("FlightPollAlertsActor: Error", e);
      e.printStackTrace();
    }
  }

  public  void getFlightStatus(final FlightAlert alert)
      throws Exception {
    DateTime departTimestamp = new DateTime(alert.origDepartTime);

    // curl -v  -X GET "https://api.flightstats
    // .com/flex/flightstatus/rest/v2/json/flight/status/CI/9327/dep/2015/2/6?appId=aeae0c80&appKey
    // =8106152200b485839efba94368e7fb36&utc=false&airport=yyz"

    StringBuilder urlQuery = new StringBuilder(
        "https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/status/");
    urlQuery.append(alert.airlineCode);
    urlQuery.append("/");
    urlQuery.append(alert.flightId);
    urlQuery.append("/dep/");
    urlQuery.append(departTimestamp.getYear());
    urlQuery.append("/");
    urlQuery.append(departTimestamp.getMonthOfYear());
    urlQuery.append("/");
    urlQuery.append(departTimestamp.getDayOfMonth());

    String url = urlQuery.toString();// URLEncoder.encode( urlQuery.toString(),"ISO-8859-1");
    ws.url(url)
      .setQueryParameter("appId", "aeae0c80")
      .setQueryParameter("appKey", "8106152200b485839efba94368e7fb36")
      .setQueryParameter("utc", "false")
      .setQueryParameter("airport", alert.departAirport)
      .get()
      .thenApply((WSResponse response) -> {
          JsonNode json = response.asJson();

          try {
            ObjectMapper mapper = new ObjectMapper();
            com.fasterxml.jackson.databind.ObjectReader r = mapper.readerFor(FlightStatusMsg.class);
            FlightStatusMsg jsonResp = r.readValue(json);

            if (jsonResp != null &&
                jsonResp.getError() == null &&
                jsonResp.getFlightStatuses() != null &&
                jsonResp.getFlightStatuses().size() > 0) {
              FlightStatus fs = jsonResp.getFlightStatuses().get(0);
              FlightAlert newAlert = processFlightResponse(alert, jsonResp, json);

              if (alert.getStatus() != FlightAlert.FlightAlertStatus.PENDING.ordinal()) {
                processNotification(alert, newAlert, fs, json.toString());
              }
            }
            return json;
          }
          catch (Exception e) {
            e.printStackTrace();
            Log.err("FlightProcessPendingAlertsActor - Error parsing response AlertId: " + alert.flightAlertId);
            Log.err("FlightProcessPendingAlertsActor - Error parsing response" + json.toString());
          }
          return null;
      });
  }

  public  FlightAlert processFlightResponse(FlightAlert origAlert, FlightStatusMsg jsonResp, JsonNode json)
      throws Exception {

    if (jsonResp != null && jsonResp.getError() == null &&
        jsonResp.getFlightStatuses() != null &&
        jsonResp.getFlightStatuses().size() > 0) {
      FlightStatus fs = jsonResp.getFlightStatuses().get(0);

      //read the alert again for concurrency reasons since this is an async
      FlightAlert alert = FlightAlert.find.byId(origAlert.flightAlertId);
      if (alert != null) {
        //find the depart times
        DateTime departDate = Utils.getDateTimeFromISO(fs.getDepartureDate().getDateLocal());
        DateTime departDateUtc = Utils.getDateTimeFromISO(fs.getDepartureDate().getDateUtc());

        //find the arrive times
        DateTime arriveDate = Utils.getDateTimeFromISO(fs.getArrivalDate().getDateLocal());
        DateTime arriveDateUtc = Utils.getDateTimeFromISO(fs.getArrivalDate().getDateUtc());

        if(fs.getOperationalTimes() != null) {
          if (fs.getOperationalTimes().getEstimatedGateDeparture() != null &&
              fs.getOperationalTimes().getEstimatedGateDeparture().getDateLocal() != null &&
              fs.getOperationalTimes().getEstimatedGateDeparture().getDateLocal().trim().length() > 0 &&
              fs.getOperationalTimes().getEstimatedGateDeparture().getDateUtc() != null) {
            departDate = Utils.getDateTimeFromISO(fs.getOperationalTimes().getEstimatedGateDeparture().getDateLocal());
            departDateUtc = Utils.getDateTimeFromISO(fs.getOperationalTimes().getEstimatedGateDeparture().getDateUtc());
          }
          else if (fs.getOperationalTimes().getScheduledGateDeparture() != null &&
                   fs.getOperationalTimes().getScheduledGateDeparture().getDateLocal() != null &&
                   fs.getOperationalTimes().getScheduledGateDeparture().getDateLocal().trim().length() > 0 &&
                   fs.getOperationalTimes().getScheduledGateDeparture().getDateUtc() != null) {
            departDate = Utils.getDateTimeFromISO(fs.getOperationalTimes().getScheduledGateDeparture().getDateLocal());
            departDateUtc = Utils.getDateTimeFromISO(fs.getOperationalTimes().getScheduledGateDeparture().getDateUtc());
          }
          else if (fs.getOperationalTimes().getPublishedDeparture() != null &&
                   fs.getOperationalTimes().getPublishedDeparture().getDateLocal() != null &&
                   fs.getOperationalTimes().getPublishedDeparture().getDateUtc() != null) {
            departDate = Utils.getDateTimeFromISO(fs.getOperationalTimes().getPublishedDeparture().getDateLocal());
            departDateUtc = Utils.getDateTimeFromISO(fs.getOperationalTimes().getPublishedDeparture().getDateUtc());
          }

          if (fs.getOperationalTimes().getEstimatedGateArrival() != null &&
              fs.getOperationalTimes().getEstimatedGateArrival().getDateLocal() != null &&
              fs.getOperationalTimes().getEstimatedGateArrival().getDateLocal().trim().length() > 0 &&
              fs.getOperationalTimes().getEstimatedGateArrival().getDateUtc() != null) {
            arriveDate = Utils.getDateTimeFromISO(fs.getOperationalTimes().getEstimatedGateArrival().getDateLocal());
            arriveDateUtc = Utils.getDateTimeFromISO(fs.getOperationalTimes().getEstimatedGateArrival().getDateUtc());
          }
          else if (fs.getOperationalTimes().getScheduledGateArrival() != null &&
                   fs.getOperationalTimes().getScheduledGateArrival().getDateLocal() != null &&
                   fs.getOperationalTimes().getScheduledGateArrival().getDateLocal().trim().length() > 0 &&
                   fs.getOperationalTimes().getScheduledGateArrival().getDateUtc() != null) {
            arriveDate = Utils.getDateTimeFromISO(fs.getOperationalTimes().getScheduledGateArrival().getDateLocal());
            arriveDateUtc = Utils.getDateTimeFromISO(fs.getOperationalTimes().getScheduledGateArrival().getDateUtc());
          }
          else if (fs.getOperationalTimes().getPublishedArrival() != null &&
                   fs.getOperationalTimes().getPublishedArrival().getDateLocal() != null &&
                   fs.getOperationalTimes().getPublishedArrival().getDateUtc() != null) {
            arriveDate = Utils.getDateTimeFromISO(fs.getOperationalTimes().getPublishedArrival().getDateLocal());
            arriveDateUtc = Utils.getDateTimeFromISO(fs.getOperationalTimes().getPublishedArrival().getDateUtc());
          }
        }

        if (departDate != null && departDateUtc != null) {
          if(fs.getAirportResources() != null) {
            if (fs.getAirportResources().getDepartureGate() != null) {
              alert.setNewDepartGate(fs.getAirportResources().getDepartureGate());
            }
            if (fs.getAirportResources().getDepartureTerminal() != null) {
              alert.setNewDepartTerminal(fs.getAirportResources().getDepartureTerminal());
            }
            if (fs.getAirportResources().getArrivalGate() != null) {
              alert.setNewArriveGate(fs.getAirportResources().getArrivalGate());
            }
            if (fs.getAirportResources().getArrivalTerminal() != null) {
              alert.setNewArriveTerminal(fs.getAirportResources().getArrivalTerminal());
            }
          }

          alert.setNewDepartTime(departDate.getMillis());
          alert.setNewDepartTimeUtc(departDateUtc.getMillis());
          alert.setNewArriveTime(arriveDate.getMillis());
          alert.setNewArriveTimeUtc(arriveDateUtc.getMillis());

          //set arrival airport code:
          alert.setArrivalAirport(fs.getArrivalAirportFsCode());

          alert.setLastupdatedtimestamp(System.currentTimeMillis());
          if (origAlert.getStatus() == FlightAlert.FlightAlertStatus.PENDING.ordinal()) {
            alert.setStatus(FlightAlert.FlightAlertStatus.CONFIRMED.ordinal());
          }
          else {
            alert.setStatus(FlightAlert.FlightAlertStatus.ACTIVE.ordinal());
          }
          alert.setResponse(json.toString());
          //optional stuff
          if (jsonResp.getAppendix() != null) {
            if (jsonResp.getAppendix().getAirlines() != null && jsonResp.getAppendix().getAirlines().size() > 0) {
              String airlineName = "";
              //handle codeshares - try to find the right airline name
              for (Airline a : jsonResp.getAppendix().getAirlines()) {
                if (a.getIata() != null && a.getIata().equals(alert.getAirlineCode()) && a.getName() != null) {
                  airlineName = a.getName();
                }
              }
              alert.setAirlineName(airlineName);
            }
            if (jsonResp.getAppendix().getAirports() != null && jsonResp.getAppendix().getAirports().size() > 0) {
              String arrivalAirport = null;
              String arrivalTimezone = null;
              String arrivalName = null;

              for (Airport airport : jsonResp.getAppendix().getAirports()) {
                if (airport.getIata() != null && airport.getIata().equalsIgnoreCase(alert.getDepartAirport())) {
                  alert.setDepartAirportName(airport.getName());
                  alert.setDepartTimeZone(airport.getTimeZoneRegionName());
                }
                else {
                  if (fs.getArrivalAirportFsCode() != null && airport.getFs().equals(fs.getArrivalAirportFsCode())) {
                    alert.setArrivalAirport(fs.getArrivalAirportFsCode());
                    alert.setArrivalAirportName(airport.getName());
                    alert.setArriveTimeZone(airport.getTimeZoneRegionName());
                  }
                  else {
                    arrivalAirport = airport.getFs();
                    arrivalName = airport.getName();
                    arrivalTimezone = airport.getTimeZoneRegionName();
                  }
                }
              }

              //check if we did not match the arrival name to populate the field.
              if (alert.getArrivalAirportName() == null || alert.getArrivalAirportName().trim().length() == 0) {
                //set all the arrival info
                alert.setArrivalAirportName(arrivalName);
                alert.setArrivalAirportName(arrivalTimezone);
                alert.setArrivalAirport(arrivalAirport);
              }
            }
          }
          alert.save();
        }
      }

      if (origAlert.getStatus() == FlightAlert.FlightAlertStatus.PENDING.ordinal()) {
        Log.info("FlightTrackActor - Pending Flight Confirmed: " + origAlert.airlineCode + " " +
                 origAlert.flightId + " from " +
                 origAlert.departAirport + " on " + Utils.formatDateTimePrint(alert.newDepartTime));
      }
      else {
        Log.info("FlightTrackActor - Flight Status Updated: " + origAlert.airlineCode + " "+
                 origAlert.flightId + " " + "from " +
                 origAlert.departAirport + " on " + Utils.formatDateTimePrint(alert.newDepartTime));
      }

      return alert;
    }
    else if (jsonResp != null && jsonResp.getError() != null) {
      Log.err("FlightProcessPendingAlertsActor - Cannot Get Status: Error AlertId: " + origAlert.flightAlertId + "\n"
              + json.toString());
      //read the alert again for concurrency reasons since this is an async
      FlightAlert alert = FlightAlert.find.byId(origAlert.flightId);
      if (alert != null && alert.status == FlightAlert.FlightAlertStatus.PENDING.ordinal()) {
        alert.setLastupdatedtimestamp(System.currentTimeMillis());
        alert.setStatus(FlightAlert.FlightAlertStatus.ERROR.ordinal());
        alert.setResponse(json.toString());
        alert.save();
      }

    }

    return null;
  }


  public void processNotification(FlightAlert origAlert, FlightAlert newAlert, FlightStatus fs, String json) {
    FlightAlertEvent.FlightAlertType notificationType = FlightAlertEvent.FlightAlertType.UNKNOWN;
    boolean shouldSendNotification = true;
    if (fs.getStatus().contains("C") || fs.getStatus().contains("NO")) {
      notificationType = FlightAlertEvent.FlightAlertType.CANCELLED;
      FlightAlertEvent lastAlert = FlightAlertEvent.findLatestByAlertId(newAlert.flightAlertId);
      if (lastAlert != null && lastAlert.getAlertType() == notificationType) {
        shouldSendNotification = false;
      }
    }
    else if (fs.getStatus().contains("S") || fs.getStatus().contains("U")) {
      if (origAlert.getStatus() == FlightAlert.FlightAlertStatus.CONFIRMED.ordinal()) {
        notificationType = FlightAlertEvent.FlightAlertType.PRE_DEPARTURE_STATUS;
      }
      else if (origAlert.getNewDepartTime().longValue() < newAlert.getNewDepartTime().longValue()) {
        //this compares with the last depart timestamps we have - this ensures we notify only if we have another time
        // change
        notificationType = FlightAlertEvent.FlightAlertType.DEPARTURE_DELAY;
      }
      else if (origAlert.getNewDepartTime().longValue() > newAlert.getNewDepartTime().longValue()) {
        //this compares with the last depart timestamps we have - this ensures we notify only if we have another time
        // change
        notificationType = FlightAlertEvent.FlightAlertType.TIME_ADJUSTMENT;
      }
      else if (newAlert.getNewDepartTerminal() != null &&
               (origAlert.getNewDepartTerminal() == null ||
                !origAlert.getNewDepartTerminal().equals(newAlert.getNewDepartTerminal()))) {
        notificationType = FlightAlertEvent.FlightAlertType.GATE_ADJUSTMENT;
      }
      else if (newAlert.getNewDepartGate() != null &&
               (origAlert.getNewDepartGate() == null ||
                !origAlert.getNewDepartGate().equals(newAlert.getNewDepartGate()))) {
        notificationType = FlightAlertEvent.FlightAlertType.GATE_ADJUSTMENT;
      }
    }

    //create the flight alert event for recording keeping
    FlightAlertEvent event = new FlightAlertEvent();
    event.setAlertTimestamp(System.currentTimeMillis());
    event.setAlertType(notificationType);
    event.setArriveGate(newAlert.getNewArriveGate());
    event.setArriveTimestamp(newAlert.getNewArriveTime());
    event.setArriveTerminal(newAlert.getNewArriveTerminal());
    event.setCreatedby("system");
    event.setCreatedtimestamp(System.currentTimeMillis());
    event.setDepartGate(newAlert.getNewDepartGate());
    event.setDepartTerminal(newAlert.getNewDepartTerminal());
    event.setDepartTimestamp(newAlert.getNewDepartTime());
    event.setEventId(DBConnectionMgr.getUniqueLongId());
    event.setFlightAlert(newAlert);
    event.setResponse(json);
    event.setStatus(APPConstants.STATUS_ACTIVE);
    event.save();

    //if we need to notify get the passengers
    if (notificationType != FlightAlertEvent.FlightAlertType.UNKNOWN) {
      //temp fix try to delay by 1ms to see if this causes the actors to send the proper message
      try {
        Thread.sleep(1);
      } catch (Exception e) {
        e.printStackTrace();
      }
      if (shouldSendNotification) {
        //Send communicator messages
        FirebaseActor.sendCmdNotificationFlight(newAlert, event);
        //get travellers
        List<FlightAlertBooking> bookings = FlightAlertBooking.findByFlightAlert(newAlert);
        if (bookings != null && bookings.size() > 0) {
          for (FlightAlertBooking booking: bookings) {
            TripDetail td = booking.getTripDetail();
            processTripBooking(td, event, newAlert);
          }
        }
      }
    }
  }

  public void sendMobileNotification(List<Account> passengers,
                                            FlightAlertEvent alertEvent,
                                            FlightAlert alert) {

    if (passengers == null ||
        passengers.size() == 0 ||
        alertEvent.getAlertType() == FlightAlertEvent.FlightAlertType.UNKNOWN) {
      return;
    }

    StringBuilder departInfo = new StringBuilder();
    if (alert.newDepartTime != null && alert.newDepartTime > 0) {
      departInfo.append("Departing at ");departInfo.append(Utils.formatDateTimePrint(alert.newDepartTime));;
    }
    if (alert.newDepartTerminal != null && !alert.newDepartTerminal.isEmpty()) {
      departInfo.append(" From Terminal ");departInfo.append(alert.newDepartTerminal);;
    }
    if (alert.newDepartGate != null && !alert.newDepartGate.isEmpty()) {
      departInfo.append(" Gate ");departInfo.append(alert.newDepartGate);;
    }

    StringBuilder notificationMsg = new StringBuilder();
    String notificationTitle = "";

    String subject = alert.airlineName + " - " + alert.airlineCode + " " + alert.flightId + " from " +
                     alert.departAirportName + "\n";
    if (alertEvent.alertType == null) {
      notificationTitle = "Flight Update";
      notificationMsg.append (subject);
      notificationMsg.append(departInfo);
    }
    else if (alertEvent.alertType == FlightAlertEvent.FlightAlertType.CANCELLED) {
      notificationTitle="Flight Cancellation";
      notificationMsg.append (subject);
    }
    else if (alertEvent.alertType == FlightAlertEvent.FlightAlertType.DEPARTURE_DELAY) {
      notificationTitle = "Flight Delayed";
      notificationMsg.append (subject);
      notificationMsg.append("Estimated Departure:\n");
      notificationMsg.append(departInfo);
    }
    else if (alertEvent.alertType == FlightAlertEvent.FlightAlertType.PRE_DEPARTURE_STATUS) {
      notificationTitle = "Flight Reminder";
      notificationMsg.append (subject);
      notificationMsg.append(departInfo);
    }
    else {
      notificationTitle = "Flight Update";
      notificationMsg.append (subject);
      notificationMsg.append(departInfo);
    }

    for (Account traveler : passengers) {
      if (traveler != null && traveler.getEmail() != null && Utils.isValidEmailAddress(traveler.getEmail() )) {
        final MobilePushNotificationActor.Command cmd2 = new MobilePushNotificationActor.Command(traveler.getEmail(), notificationTitle, notificationMsg.toString(), DBConnectionMgr.getUniqueLongId());
        ActorsHelper.tell(SupervisorActor.UmappedActor.MOBILE_PUSH_NOTIFY, cmd2);
      }
    }
  }

  public void sendEmailNotification(Account traveler,
                                           FlightAlertEvent alertEvent,
                                           FlightAlert alert,
                                           Trip trip,
                                           String groupId,
                                           boolean isAgent) {
    //process all events and then process all passengers

    if (traveler == null ||
        alertEvent.getAlertType() == FlightAlertEvent.FlightAlertType.UNKNOWN) {
      return;
    }
    //send email notifications
    //send email
    try {
      FlightAlertView view = new FlightAlertView();

      String sender = "Flight Notification";

      String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);
      String tripPK = trip.tripid;
      if (groupId != null && !groupId.isEmpty()) {
        tripPK = TourController.getCompositePK(trip.tripid, groupId);
      }

      String emailEnabled = ConfigMgr.getAppParameter(CoreConstants.EMAIL_ENABLED);

      if (emailEnabled != null && emailEnabled.equalsIgnoreCase("Y")) {
        view.airlineCode = alert.airlineCode;
        view.airlineName = alert.airlineName;
        view.departAirportCode = alert.departAirport;
        view.departAirportName = alert.departAirportName;
        view.arriveAirportCode = alert.arrivalAirport;
        view.arriveAirportName = alert.arrivalAirportName;
        view.arriveGate = alert.newArriveGate;
        view.arriveTerminal = alert.newArriveTerminal;
        view.departGate = alert.newDepartGate;
        view.departTerminal = alert.newDepartTerminal;
        view.origDepartTime = Utils.formatDateTimePrint(alert.newDepartTime);
        view.flightId = String.valueOf(alert.flightId);
        view.tripName = trip.getName();
        if (traveler.getUid() == null || isAgent) {
          view.tripUrl = hostUrl + controllers.routes.WebItineraryController.index(tripPK, null, 0l);
        } else {
          view.tripUrl = hostUrl + routes.WebItineraryController.index(tripPK, null, traveler.getUid());
        }
        view.isAgent = isAgent;


        StringBuilder titleSB = new StringBuilder();
        if (alert.airlineName != null) {
          titleSB.append(alert.airlineName);
          titleSB.append(" - ");
        }
        titleSB.append(alert.airlineCode);
        titleSB.append(" ");
        titleSB.append(alert.flightId);
        if (alert.departAirportName != null) {
          titleSB.append(" departing ");
          titleSB.append(alert.departAirportName);
        }

        view.title = titleSB.toString();



        view.events = new ArrayList<>();
        FlightAlertView.FlightAlertEventView eventView = view.new FlightAlertEventView();
        eventView.alertType = alertEvent.alertType;
        eventView.departTime = Utils.formatDateTimePrint(alertEvent.departTimestamp);
        eventView.arriveTime = Utils.formatDateTimePrint(alertEvent.arriveTimestamp);
        eventView.timestamp = Utils.formatDateTimePagePrint(alertEvent.alertTimestamp);
        view.events.add(eventView);

        view.alertType = alertEvent.alertType;

        String subject = alert.airlineName + " - " + alert.airlineCode + " " + alert.flightId + " from " +
                         alert.departAirportName;
        if (view.alertType == null) {
          subject = "Flight Update: " + subject;
        }
        else if (view.alertType == FlightAlertEvent.FlightAlertType.CANCELLED) {
          subject = "Flight Cancelled: " + subject;
        }
        else if (view.alertType == FlightAlertEvent.FlightAlertType.DEPARTURE_DELAY) {
          subject = "Flight Delayed: " + subject;
        }
        else if (view.alertType == FlightAlertEvent.FlightAlertType.PRE_DEPARTURE_STATUS) {
          subject = "Flight Reminder: " + subject;

          //check airline poll to see if we have a checkin url
          List<Integer> poiTypes = new ArrayList<Integer>();
          poiTypes.add(PoiTypeInfo.Instance().byName("Airline").getId());
          Set<Integer> cmpyIds = new HashSet<>();
          cmpyIds.add(0);
          Map<Long, List<PoiRS>> airlines = PoiMgr.findByCode(alert.airlineCode, poiTypes, cmpyIds, 1);
          boolean found =  false;
          if (airlines != null) {
            for (List<PoiRS> list : airlines.values()) {
              for (PoiRS rs: list) {
                PoiJson data = rs.data;
                Map<String, String> additionalParams = data.getAdditionalProperties();
                if (additionalParams != null && additionalParams.containsKey(APPConstants.CHECKIN_URL)) {
                  view.checkinUrl = additionalParams.get(APPConstants.CHECKIN_URL);
                }
                break;
              }
              break;
            }
          }
        }
        else {
          subject = "Flight Update: " + subject;
        }

        String emailBody = views.html.email.flightNotification.render(view).toString();

        List<String> emails = new ArrayList<>();
        emails.add(traveler.getEmail());

        //initialize email mgr
        UmappedEmail emailMgr = UmappedEmailFactory.getInstance(trip.getCmpyid());
        emailMgr.addTo(traveler.getEmail());
        emailMgr.withViaFromName(sender)
                .withSubject(subject)
                .withHtml(emailBody)
                .withTripId(trip.tripid)
                .withToList(emails)
                .withEmailType(EmailLog.EmailTypes.FLIGHT_NOTIFICIATION)
                .withAccountUid(traveler.getUid())
                .buildAndSend();

      }
      Log.info("FlightNotifyActor : Notifications sent: " + alert.getFlightAlertId());

    }
    catch (Exception e) {
      e.printStackTrace();
      Log.err("Cannot send trip email. AlertId: " + alert.getFlightAlertId(), e);
    }
  }


  public void processTripBooking (TripDetail td, FlightAlertEvent alertEvent, FlightAlert alert) {
    if (td != null && td.getStatus() == APPConstants.STATUS_ACTIVE) {
      Trip t = Trip.find.byId(td.getTripid());
      if (t != null && t.getStatus() == APPConstants.STATUS_ACTIVE && SecurityMgr.hasCapability(t, Capability.FLIGHT_NOTIFICATIONS)) {
        //get all passengers to send to
        List<AccountTripLink> passengers = AccountTripLink.findByTrip(t.getTripid());
        if (passengers != null) {

          //update the trip detail
          try {
            UmFlightReservation reservation = cast(td.getReservation(), UmFlightReservation.class);
            
            if (reservation != null) {
              String newComments = FlightAlertsHelper.generateComment(reservation, alertEvent, alert);
              reservation.setNotesPlainText(newComments);
            }

            td.setStarttimestamp(alert.newDepartTime);
            td.setEndtimestamp(alert.newArriveTime);

            td.setModifiedby("system");
            td.setLastupdatedtimestamp(Instant.now().toEpochMilli());
            td.update();
            BookingController.invalidateCache(t.getTripid(), td.getDetailsid());

            //auto publish the trip
            List<TripGroup> tripgroups = TripGroup.findActiveByTripId(t.getTripid());
            if (tripgroups != null && tripgroups.size() > 0) {
              for (TripGroup tripGroup: tripgroups) {
                tripPublisherHelper.autoPublishTrip(t, tripGroup.getGroupid());
              }
            }
            tripPublisherHelper.autoPublishTrip(t, null);

            //notify the user
            List<Account> travelers = new ArrayList<>();
            for (AccountTripLink tripLink: passengers) {
              Account traveler = Account.find.byId(tripLink.getPk().getUid());
              sendEmailNotification(traveler, alertEvent, alert, t, tripLink.getLegacyGroupId(), false);
              travelers.add(traveler);

            }
            //notify the agent if the flight is delayed or cancelled
            if (alertEvent != null && (alertEvent.alertType == FlightAlertEvent.FlightAlertType.CANCELLED || alertEvent.alertType == FlightAlertEvent.FlightAlertType.DEPARTURE_DELAY)) {
              TripPublishHistory publishHistory = TripPublishHistory.getLastPublished(t.getTripid(), null);
              if (publishHistory != null) {
                Account agent = Account.findByLegacyId(publishHistory.getCreatedby());
                if (agent != null) {
                  sendEmailNotification(agent, alertEvent, alert, t, null, true);
                }
              }
            }
            //send mobile notifications
            sendMobileNotification(travelers, alertEvent, alert);

          } catch (Exception e) {
            //in case of concurrent modification
          }




        }
      }
    }
  }

}
