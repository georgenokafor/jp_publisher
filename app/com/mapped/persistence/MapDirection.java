package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class MapDirection { 
 
	 public final static String MapDirectionTypeId = "280"; 
	 protected static String pkTypeId =  "281"; 
	 private String pk; 
	 protected static String mapsIdTypeId =  "282"; 
	 private String mapsId; 
	 protected static String startMilestonesIdTypeId =  "283"; 
	 private String startMilestonesId; 
	 protected static String endMilestonesIdTypeId =  "284"; 
	 private String endMilestonesId; 
	 protected static String rankTypeId =  "285"; 
	 private String rank; 
	 protected static String tagTypeId =  "286"; 
	 private String tag; 
	 protected static String typeTypeId =  "287"; 
	 private String type; 
	 protected static String lastUpdateTimestampTypeId =  "288"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "289"; 
	 private long createTimestamp; 
	 protected static String syncTimestampTypeId =  "290"; 
	 private long syncTimestamp; 
	 protected static String modifiedByTypeId =  "291"; 
	 private String modifiedBy; 
	 protected static String createdByTypeId =  "292"; 
	 private String createdBy; 
	 protected static String noteTypeId =  "293"; 
	 private String note; 

 	 // Contructor 
	 public MapDirection () {} 
	 public MapDirection ( String pk,  String mapsId,  String startMilestonesId,  String endMilestonesId,  String rank,  String tag,  String type,  long lastUpdateTimestamp,  long createTimestamp,  long syncTimestamp,  String modifiedBy,  String createdBy,  String note ) { 
		 this.pk=pk; 
		 this.mapsId=mapsId; 
		 this.startMilestonesId=startMilestonesId; 
		 this.endMilestonesId=endMilestonesId; 
		 this.rank=rank; 
		 this.tag=tag; 
		 this.type=type; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.syncTimestamp=syncTimestamp; 
		 this.modifiedBy=modifiedBy; 
		 this.createdBy=createdBy; 
		 this.note=note; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getMapsId () { 
	 	 return mapsId; 
 	 } 
 
 	 public void setMapsId(String mapsId) { 
	 	 this.mapsId = mapsId; 
 	 } 
 
 	 public  String getStartMilestonesId () { 
	 	 return startMilestonesId; 
 	 } 
 
 	 public void setStartMilestonesId(String startMilestonesId) { 
	 	 this.startMilestonesId = startMilestonesId; 
 	 } 
 
 	 public  String getEndMilestonesId () { 
	 	 return endMilestonesId; 
 	 } 
 
 	 public void setEndMilestonesId(String endMilestonesId) { 
	 	 this.endMilestonesId = endMilestonesId; 
 	 } 
 
 	 public  String getRank () { 
	 	 return rank; 
 	 } 
 
 	 public void setRank(String rank) { 
	 	 this.rank = rank; 
 	 } 
 
 	 public  String getTag () { 
	 	 return tag; 
 	 } 
 
 	 public void setTag(String tag) { 
	 	 this.tag = tag; 
 	 } 
 
 	 public  String getType () { 
	 	 return type; 
 	 } 
 
 	 public void setType(String type) { 
	 	 this.type = type; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  String getModifiedBy () { 
	 	 return modifiedBy; 
 	 } 
 
 	 public void setModifiedBy(String modifiedBy) { 
	 	 this.modifiedBy = modifiedBy; 
 	 } 
 
 	 public  String getCreatedBy () { 
	 	 return createdBy; 
 	 } 
 
 	 public void setCreatedBy(String createdBy) { 
	 	 this.createdBy = createdBy; 
 	 } 
 
 	 public  String getNote () { 
	 	 return note; 
 	 } 
 
 	 public void setNote(String note) { 
	 	 this.note = note; 
 	 } 
 
 	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: MapDirection"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 MapsId: ").append(this.mapsId );  
		 sb.append("	 StartMilestonesId: ").append(this.startMilestonesId );  
		 sb.append("	 EndMilestonesId: ").append(this.endMilestonesId );  
		 sb.append("	 Rank: ").append(this.rank );  
		 sb.append("	 Tag: ").append(this.tag );  
		 sb.append("	 Type: ").append(this.type );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 ModifiedBy: ").append(this.modifiedBy );  
		 sb.append("	 CreatedBy: ").append(this.createdBy );  
		 sb.append("	 Note: ").append(this.note );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", MapDirectionTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (mapsId != null) {
			 memberNode.put(mapsIdTypeId, mapsId);
		 }
		 if (startMilestonesId != null) {
			 memberNode.put(startMilestonesIdTypeId, startMilestonesId);
		 }
		 if (endMilestonesId != null) {
			 memberNode.put(endMilestonesIdTypeId, endMilestonesId);
		 }
		 if (rank != null) {
			 memberNode.put(rankTypeId, rank);
		 }
		 if (tag != null) {
			 memberNode.put(tagTypeId, tag);
		 }
		 if (type != null) {
			 memberNode.put(typeTypeId, type);
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (modifiedBy != null) {
			 memberNode.put(modifiedByTypeId, modifiedBy);
		 }
		 if (createdBy != null) {
			 memberNode.put(createdByTypeId, createdBy);
		 }
		 if (note != null) {
			 memberNode.put(noteTypeId, note);
		 }
		 return classNode;
	 } 
	 public static MapDirection parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 MapDirection  c= new MapDirection(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.mapsId = fields.findPath(mapsIdTypeId).textValue();
			 c.startMilestonesId = fields.findPath(startMilestonesIdTypeId).textValue();
			 c.endMilestonesId = fields.findPath(endMilestonesIdTypeId).textValue();
			 c.rank = fields.findPath(rankTypeId).textValue();
			 c.tag = fields.findPath(tagTypeId).textValue();
			 c.type = fields.findPath(typeTypeId).textValue();
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 c.modifiedBy = fields.findPath(modifiedByTypeId).textValue();
			 c.createdBy = fields.findPath(createdByTypeId).textValue();
			 c.note = fields.findPath(noteTypeId).textValue();
			 return c;
		} else
			 return null;
	 } 
} 
 
