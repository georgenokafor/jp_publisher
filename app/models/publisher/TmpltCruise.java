package models.publisher;

import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created by surge on 2014-08-11.
 */
@Entity @Table(name = "tmplt_cruise")
public class TmpltCruise
    extends Model {

  public static Model.Finder<String, TmpltCruise> find = new Finder(TmpltCruise.class);

  @Id
  private String detailsid;
  @Constraints.Required
  private String name;
  private String cmpyName;
  private Long createdtimestamp;
  private Long lastupdatedtimestamp;
  private String createdby;
  private String modifiedby;
  @Version
  private int version;

  public static TmpltCruise buildCruise(String detailsId, String userId) {
    TmpltCruise tmpltCruise = new TmpltCruise();
    tmpltCruise.setDetailsid(detailsId);
    tmpltCruise.setCreatedtimestamp(System.currentTimeMillis());
    tmpltCruise.setLastupdatedtimestamp(tmpltCruise.createdtimestamp);
    tmpltCruise.setCreatedby(userId);
    tmpltCruise.setModifiedby(userId);
    return tmpltCruise;
  }

  public String getDetailsid() {
    return detailsid;
  }

  public void setDetailsid(String detailsid) {
    this.detailsid = detailsid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCmpyName() {
    return cmpyName;
  }

  public void setCmpyName(String cmpyName) {
    this.cmpyName = cmpyName;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }


}
