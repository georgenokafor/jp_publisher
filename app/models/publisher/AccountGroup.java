package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbJsonB;
import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.persistence.util.DBConnectionMgr;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * Created by surge on 2016-02-24.
 */
@Entity
@Table(name = "account_group")
public class AccountGroup
    extends Model {

  @Transient
  public static final ObjectMapper om;

  public enum GroupType {
    @EnumValue("FAM")
    FAMILY
  }

  public static Model.Finder<Long, AccountGroup> find = new Finder(AccountGroup.class);

  static {
    om = new ObjectMapper();
    //Perform any kind of configuration for object mapper in this area
  }

  @Id
  Long pk;
  String name;
  @Enumerated
  GroupType groupType;
  @DbJsonB
  JsonNode  meta;
  Timestamp createdTs;
  Long      createdBy;
  Timestamp modifiedTs;
  Long      modifiedBy;
  @Version
  int version;

  public static AccountGroup build(Long createdBy) {
    AccountGroup a = new AccountGroup();
    a.setPk(DBConnectionMgr.getUniqueLongId());
    a.setCreatedTs(Timestamp.from(Instant.now()));
    a.setModifiedTs(a.createdTs);
    a.setCreatedBy(createdBy);
    a.setModifiedBy(createdBy);
    return a;
  }

  public GroupType getGroupType() {
    return groupType;
  }

  public void setGroupType(GroupType groupType) {
    this.groupType = groupType;
  }

  public JsonNode getMeta() {
    return meta;
  }

  public void setMeta(JsonNode meta) {
    this.meta = meta;
  }

  @PostLoad
  public void postLoad() {
    /*
    try {
      ObjectReader or = om.readerFor(PoiJson.class);
      json = or.treeToValue(data, PoiJson.class);
      //json = om.treeToValue(data, PoiJson.class);
    }
    catch (JsonProcessingException e) {
      Log.err("Poi: Failed to parse JSON tree");
    }
    */
  }

  @PrePersist
  public void prePersist() {
    /*
    json.prepareForDb();
    data = om.valueToTree(json);
    */
  }

  public Long getPk() {
    return pk;
  }

  public void setPk(Long pk) {
    this.pk = pk;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
