package com.mapped.publisher.persistence;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-02
 * Time: 10:12 AM
 * To change this template use File | Settings | File Templates.
 */
public class TourCmpyRS {
   private String tourId;
   private String tourName;
    private String tourComments;

    private String cmpyId;
   private String cmpyName;

   private String cmpyContact;
    private String fName;
    private String lName;
    private long startTimestamp;
   private long endTimestamp;

    private String createdBy;


    public String getTourId() {
        return tourId;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public String getTourName() {
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }

    public String getCmpyId() {
        return cmpyId;
    }

    public void setCmpyId(String cmpyId) {
        this.cmpyId = cmpyId;
    }

    public String getCmpyName() {
        return cmpyName;
    }

    public void setCmpyName(String cmpyName) {
        this.cmpyName = cmpyName;
    }

    public long getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(long startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public long getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(long endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    public String getCmpyContact() {
        return cmpyContact;
    }

    public void setCmpyContact(String cmpyContact) {
        this.cmpyContact = cmpyContact;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getTourComments() {
        return tourComments;
    }

    public void setTourComments(String tourComments) {
        this.tourComments = tourComments;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
