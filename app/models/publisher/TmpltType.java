package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.EnumValue;

import javax.persistence.*;

/**
 * Created by surge on 2015-05-05.
 */
@Entity @Table(name = "tmplt_type")
public class TmpltType
    extends Model {

  public static Finder<Type, TmpltType> find = new Finder(TmpltType.class);
  @Id @Enumerated(value = EnumType.STRING) @Column(name = "name", length = 15)
  private Type name;

  /**
   * Template types, some are not visible in to user
   */
  public static enum Type {
    /**
     * User content created from other trips or from scratch
     */
    @EnumValue("USER_CONTENT")
    USER_CONTENT(true),
    /**
     * Tours
     */
    @EnumValue("TOUR")
    TOUR(true),
    @EnumValue("CRUISE_ENSEMBLE")
    CRUISE_ENSEMBLE(false),
    /**
     * User himself is responsible for bills
     */
    @EnumValue("CRUISE_VIRTUOSO")
    CRUISE_VIRTUOSO(false);
    private boolean userVisible;

    private Type(boolean isVisible) {
      userVisible = isVisible;
    }

    public boolean isUserVisible() {
      return userVisible;
    }

    public void setUserVisible(boolean userVisible) {
      this.userVisible = userVisible;
    }
  }

  public Type getName() {
    return name;
  }

  public void setName(Type name) {
    this.name = name;
  }
}
