package models.publisher;

import com.avaje.ebean.BeanState;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-19
 * Time: 3:07 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "hotel_booking")
public class HotelBooking
    extends Model {
  public static Model.Finder<String, HotelBooking> find = new Finder(HotelBooking.class);
  public String comments;
  @Id
  public String detailsid;
  public String name;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;

  /**
   * Takes this object makes it possible to re-insert as new record
   * @return
   */
  public HotelBooking prepareForReuse(String createdBy, String modifiedBy) {
    BeanState state = Ebean.getBeanState(this);
    state.resetForInsert();
    setDetailsid(DBConnectionMgr.getUniqueId());
    setModifiedby(modifiedBy);
    setCreatedby(createdBy);
    setLastupdatedtimestamp(System.currentTimeMillis());
    setCreatedtimestamp(lastupdatedtimestamp);
    return this;
  }


  public static HotelBooking build(String detailId, String userId) {
    HotelBooking hotelBooking = new HotelBooking();
    hotelBooking.setDetailsid(detailId);
    hotelBooking.setCreatedby(userId);
    hotelBooking.setModifiedby(userId);
    hotelBooking.setCreatedtimestamp(System.currentTimeMillis());
    hotelBooking.setLastupdatedtimestamp(hotelBooking.createdtimestamp);
    hotelBooking.setVersion(0);
    return hotelBooking;
  }

  public static HotelBooking findByPK(String pk) {
    return find.byId(pk);
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getDetailsid() {
    return detailsid;
  }

  public void setDetailsid(String detailsid) {
    this.detailsid = detailsid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
