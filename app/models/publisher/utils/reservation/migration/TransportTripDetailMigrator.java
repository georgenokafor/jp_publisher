package models.publisher.utils.reservation.migration;

import com.mapped.publisher.utils.Log;
import com.umapped.persistence.reservation.transfer.UmTransferReservation;

import models.publisher.TransportBooking;
import models.publisher.TripDetail;
import models.utils.migration.Migrator;

public class TransportTripDetailMigrator implements Migrator<TripDetail> {

  private UmTransferReservationBuilder builder = new UmTransferReservationBuilder();
  
  @Override
  public boolean migrate(TripDetail entity) {
    
    TransportBooking transport = TransportBooking.find.byId(entity.detailsid);
    Log.info("Migrate a transport trip_detail");
    UmTransferReservation reservation = builder.build(entity, transport);
    entity.setReservation(reservation);
    return true;
  }

}
