
package com.umapped.external.wetu.pins.list;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "general_description",
    "extended_description",
    "contact_information",
    "images",
    "youtube_videos",
    "panoramas"
})
public class Content {

    @JsonProperty("general_description")
    private String generalDescription;
    @JsonProperty("extended_description")
    private String extendedDescription;
    @JsonProperty("contact_information")
    private ContactInformation contactInformation;
    @JsonProperty("images")
    private List<Image> images = new ArrayList<Image>();
    @JsonProperty("youtube_videos")
    private List<Video> youtubeVideos = new ArrayList<Video>();
    @JsonProperty("panoramas")
    private List<Panorama> panoramas = new ArrayList<Panorama>();

    @JsonProperty("teaser_description")
    private String teaserDescription;
    @JsonProperty("logo")
    private Image logo;


    /**
     * 
     * @return
     *     The generalDescription
     */
    @JsonProperty("general_description")
    public String getGeneralDescription() {
        return generalDescription;
    }

    /**
     * 
     * @param generalDescription
     *     The general_description
     */
    @JsonProperty("general_description")
    public void setGeneralDescription(String generalDescription) {
        this.generalDescription = generalDescription;
    }

    /**
     * 
     * @return
     *     The extendedDescription
     */
    @JsonProperty("extended_description")
    public String getExtendedDescription() {
        return extendedDescription;
    }

    /**
     * 
     * @param extendedDescription
     *     The extended_description
     */
    @JsonProperty("extended_description")
    public void setExtendedDescription(String extendedDescription) {
        this.extendedDescription = extendedDescription;
    }

    /**
     * 
     * @return
     *     The contactInformation
     */
    @JsonProperty("contact_information")
    public ContactInformation getContactInformation() {
        return contactInformation;
    }

    /**
     * 
     * @param contactInformation
     *     The contact_information
     */
    @JsonProperty("contact_information")
    public void setContactInformation(ContactInformation contactInformation) {
        this.contactInformation = contactInformation;
    }

    /**
     * 
     * @return
     *     The images
     */
    @JsonProperty("images")
    public List<Image> getImages() {
        return images;
    }

    /**
     * 
     * @param images
     *     The images
     */
    @JsonProperty("images")
    public void setImages(List<Image> images) {
        this.images = images;
    }

    /**
     * 
     * @return
     *     The youtubeVideos
     */
    @JsonProperty("youtube_videos")
    public List<Video> getYoutubeVideos() {
        return youtubeVideos;
    }

    /**
     * 
     * @param youtubeVideos
     *     The youtube_videos
     */
    @JsonProperty("youtube_videos")
    public void setYoutubeVideos(List<Video> youtubeVideos) {
        this.youtubeVideos = youtubeVideos;
    }

    /**
     * 
     * @return
     *     The panoramas
     */
    @JsonProperty("panoramas")
    public List<Panorama> getPanoramas() {
        return panoramas;
    }

    /**
     * 
     * @param panoramas
     *     The panoramas
     */
    @JsonProperty("panoramas")
    public void setPanoramas(List<Panorama> panoramas) {
        this.panoramas = panoramas;
    }

    @JsonProperty("teaser_description")
    public String getTeaserDescription() {
        return teaserDescription;
    }

    @JsonProperty("teaser_description")
    public void setTeaserDescription(String teaserDescription) {
        this.teaserDescription = teaserDescription;
    }

    @JsonProperty("logo")
    public Image getLogo() {
        return logo;
    }

    @JsonProperty("logo")
    public void setLogo(Image logo) {
        this.logo = logo;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(generalDescription).append(extendedDescription).append(contactInformation).append(images).append(youtubeVideos).append(panoramas).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Content) == false) {
            return false;
        }
        Content rhs = ((Content) other);
        return new EqualsBuilder().append(generalDescription, rhs.generalDescription).append(extendedDescription, rhs.extendedDescription).append(contactInformation, rhs.contactInformation).append(images, rhs.images).append(youtubeVideos, rhs.youtubeVideos).append(panoramas, rhs.panoramas).isEquals();
    }

}
