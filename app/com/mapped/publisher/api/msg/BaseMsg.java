package com.mapped.publisher.api.msg;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mapped.publisher.api.BaseJson;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-10
 * Time: 8:57 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseMsg
    implements BaseJson {

  public abstract ObjectNode toJson();

  public abstract boolean parseJson(JsonNode node);
}
