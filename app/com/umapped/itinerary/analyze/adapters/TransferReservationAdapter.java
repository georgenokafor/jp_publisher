package com.umapped.itinerary.analyze.adapters;

import com.mapped.publisher.parse.schemaorg.Reservation;
import com.mapped.publisher.parse.schemaorg.TransferReservation;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.Location;

/**
 * Created by wei on 2017-03-07.
 */
public class TransferReservationAdapter
    extends AbstractReservationItinearyItemAdapter {
  @Override protected ItineraryItemType getItemType(Reservation reservation) {
    return ItineraryItemType.ShortTransfer;
  }

  @Override protected Location getStartLocation(Reservation reservation) {
    TransferReservation r = (TransferReservation) reservation;
    return getLocationFromPlace(r.reservationFor.pickupLocation);
  }

  @Override protected Location getEndLocation(Reservation reservation) {
    TransferReservation r = (TransferReservation) reservation;
    return getLocationFromPlace(r.reservationFor.dropoffLocation);
  }
}
