package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Model;
import com.avaje.ebean.QueryEachWhileConsumer;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.persistence.FeedHelperPOI;
import com.mapped.publisher.persistence.PoiImportRS;
import com.mapped.publisher.utils.Log;
import org.apache.commons.lang3.time.StopWatch;

import javax.persistence.*;
import javax.xml.bind.DatatypeConverter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.avaje.ebean.Expr.eq;

/**
 * Created by surge on 2015-02-03.
 */
@Entity
@Table(name = "poi_src_feed")
public class PoiSrcFeed
    extends Model {

  public enum FeedSyncResult {
    NEW,
    UPDATED,
    UNCHANGED,
    DELETED,
    FAIL
  }
  public static Finder<PoiFeedId, PoiSrcFeed> find = new Finder<>(PoiSrcFeed.class);
  @Id
  private PoiFeedId feedId;
  private String    raw;
  @Lob
  private byte[]    hash;
  private Long      importTs;
  private boolean   isSynced;
  private Long      syncTs;
  @Version
  private int       version;
  private String    srcReference;
  private String    linkedBy;
  private Long      linkedTs;

  @Embeddable
  public static class PoiFeedId {
    @Column(name = "poi_id")
    public long poiId;
    @Column(name = "src_id")
    public int  srcId;

    public long getPoiId() {
      return poiId;
    }

    public void setPoiId(long poiId) {
      this.poiId = poiId;
    }

    public int getSrcId() {
      return srcId;
    }

    public void setSrcId(int srcId) {
      this.srcId = srcId;
    }

    @Override
    public int hashCode() {
      return (int) poiId * srcId;
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof PoiFeedId)) {
        return false;
      }
      PoiFeedId ok = (PoiFeedId) o;
      return (ok.poiId == poiId) && (ok.srcId == srcId);
    }
  }

  public static PoiSrcFeed buildNewRecord(PoiImportRS importRS, boolean ignoreMatch, String userId) {
    PoiSrcFeed psf = new PoiSrcFeed();

    if (importRS.poiId == null || importRS.poiId == 0 || ignoreMatch) {
      psf.setFeedId(DBConnectionMgr.getUniqueLongId(), importRS.srcId);
    }
    else {
      psf.setFeedId(importRS.poiId, importRS.srcId);
    }
    psf.setRaw(importRS.raw);
    psf.setHash(DatatypeConverter.parseHexBinary(importRS.hash));
    psf.setImportTs(importRS.timestamp);
    psf.setLinkedTs(System.currentTimeMillis());
    psf.setLinkedBy(userId);
    psf.setSrcReference(importRS.srcReference);
    psf.setSynced(false);
    return psf;
  }

  public PoiSrcFeed setFeedId(long poiId, int srcId) {
    PoiFeedId psi = new PoiFeedId();
    psi.poiId = poiId;
    psi.srcId = srcId;
    this.feedId = psi;
    return this;
  }

  public static PoiSrcFeed buildNewRecord(long poiId, int srcId) {
    PoiSrcFeed psf = new PoiSrcFeed();
    psf.setFeedId(poiId, srcId);
    psf.setSynced(false);
    return psf;
  }

  public static List<PoiSrcFeed> getSourceRemovedRecords(int srcId) {
    com.avaje.ebean.Query<PoiImport> subQuery = Ebean
        .createQuery(PoiImport.class)
        .select("srcReference")
        .where()
        .eq("src_id", srcId)
        .query();
    return find.where().eq("src_id", srcId).not(Expr.in("src_reference", subQuery)).findList();
  }

  /**
   * Performs scan through
   *
   * @param srcId
   * @param helper
   * @return
   */
  public static Map<FeedSyncResult, Integer> syncEachWithPoi(int srcId, final FeedHelperPOI helper) {
    final Map<FeedSyncResult, Integer> result = new HashMap<>();
    result.put(FeedSyncResult.NEW, 0);
    result.put(FeedSyncResult.UPDATED, 0);
    result.put(FeedSyncResult.DELETED, 0);
    result.put(FeedSyncResult.FAIL, 0);

    find.where().and(eq("src_id", srcId), eq("is_synced", false)).findEachWhile(new QueryEachWhileConsumer<PoiSrcFeed>() {
      @Override
      public boolean accept(PoiSrcFeed bean) {
        StopWatch sw = new StopWatch();
        sw.start();

        FeedSyncResult currRes = helper.synchronize(bean);

        result.put(currRes, result.get(currRes) + 1);

        sw.stop();
        Log.debug("Synchronized " + bean.feedId.poiId + " in " + sw.getTime() + "ms");
        return true;
      }
    });
    return result;
  }

  public static PoiSrcFeed findUniqueById(long poiId, int srcId) {
    PoiFeedId pfi = new PoiFeedId();
    pfi.poiId = poiId;
    pfi.srcId = srcId;
    return find.byId(pfi);
  }

  public static PoiSrcFeed buildConsortiumCompany(long poiId, int srcId) {
    PoiSrcFeed result = new PoiSrcFeed();
    result.feedId = new PoiFeedId();
    result.feedId.poiId = poiId;
    result.feedId.srcId = srcId;
    return result;
  }

  public Long getLinkedTs() {
    return linkedTs;
  }

  public void setLinkedTs(Long linkedTs) {
    this.linkedTs = linkedTs;
  }

  public String getLinkedBy() {
    return linkedBy;
  }

  public void setLinkedBy(String linkedBy) {
    this.linkedBy = linkedBy;
  }

  public String getSrcReference() {
    return srcReference;
  }

  public void setSrcReference(String srcReference) {
    this.srcReference = srcReference;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public PoiFeedId getFeedId() {
    return feedId;
  }

  public void setFeedId(PoiFeedId feedId) {
    this.feedId = feedId;
  }

  public boolean isSynced() {
    return isSynced;
  }

  public void setSynced(boolean isSynced) {
    this.isSynced = isSynced;
  }

  public Long getSyncTs() {
    return syncTs;
  }

  public void setSyncTs(Long syncTs) {
    this.syncTs = syncTs;
  }

  public String getRaw() {
    return raw;
  }

  public void setRaw(String raw) {
    this.raw = raw;
  }

  public byte[] getHash() {
    return hash;
  }

  public void setHash(byte[] hash) {
    this.hash = hash;
  }

  public Long getImportTs() {
    return importTs;
  }

  public void setImportTs(Long importTs) {
    this.importTs = importTs;
  }
}
