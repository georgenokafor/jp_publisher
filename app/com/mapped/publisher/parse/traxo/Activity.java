package com.mapped.publisher.parse.traxo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by twong on 10/06/17.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Activity extends Segment {
    public String activity_type;
    public String activity_name;
    public String start_name;
    public String start_admin_code;
    public String start_address1;
    public String start_address2;
    public String start_city_name;
    public String start_country;
    public String start_lat;
    public String start_lon;
    public String start_postal_code;
    public String end_name;
    public String end_admin_code;
    public String end_address1;
    public String end_address2;
    public String end_city_name;
    public String end_country;
    public String end_lat;
    public String end_lon;
    public String end_postal_code;
    public String start_datetime;
    public String end_datetime;
    public String time_zone_id;

}
