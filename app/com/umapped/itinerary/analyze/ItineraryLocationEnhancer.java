package com.umapped.itinerary.analyze;

import com.umapped.itinerary.analyze.location.LocationEnhanceService;
import com.umapped.itinerary.analyze.model.Itinerary;
import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by wei on 2017-03-29.
 */
@Singleton
public class ItineraryLocationEnhancer {
  private static final Logger LOG = LoggerFactory.getLogger(ItineraryLocationEnhancer.class);

  private static final Set<ItineraryItemType> ImportantTypes;

  static {
    ImportantTypes = new HashSet<>();
    ImportantTypes.add(ItineraryItemType.Accommodation);

    // Turn off lookup for CruiseStop from google service
    //ImportantTypes.add(ItineraryItemType.CruiseStop);
    ImportantTypes.add(ItineraryItemType.Cruise);
  }
  private LocationEnhanceService locationEnhanceService;

  @Inject
  public ItineraryLocationEnhancer(LocationEnhanceService locationEnhanceService) {
    this.locationEnhanceService = locationEnhanceService;
  }

  public void enhanceItineraryLocation(Itinerary itinerary) {
    int notEnhanced = 0;
    for (ItineraryItem item : itinerary.getItems()) {
      boolean important = ImportantTypes.contains(item.getType());
      Location enhancedStartLocation = enhanceLocation(item.getEnhancedStartLocation(), important);
      item.setEnhancedStartLocation(enhancedStartLocation);
      Location enhancedEndLocation = enhanceLocation(item.getEnhancedEndLocation(), important);
      item.setEnhancedEndLocation(enhancedEndLocation);
      // Not much use to have this stats
      //notEnhanced += checkNotEnhanced(enhancedStartLocation, item.getType());
      //notEnhanced += checkNotEnhanced(enhancedEndLocation, item.getType());
    }
    if (notEnhanced > 0) {
      LOG.debug( String.format("Total location not enhanced: %s", notEnhanced));
    }
  }

  private int checkNotEnhanced(Location location, ItineraryItemType type) {
    if (location == null) {
      return 1;
    }
    if (location.getGeoLocation() == null) {
      LOG.debug(String.format("Location not enhanced (%s): %s, %s ", type, location.getUmPoidId(), location.getDescription()));
      return 1;
    } else {
      return 0;
    }
  }

  private Location enhanceLocation(Location location, boolean important) {
    try {
      if (location == null) {
        return null;
      }
      Location enhancedLocation = locationEnhanceService.enhanceLocation(location, important);
      return enhancedLocation;
    } catch (RuntimeException e) {
      LOG.error("Fail to enhance location: " + location, e);
      throw e;
    }
  }
}
