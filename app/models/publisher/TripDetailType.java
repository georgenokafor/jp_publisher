package models.publisher;

import com.avaje.ebean.Model;
import com.umapped.persistence.enums.ReservationType;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-19
 * Time: 3:03 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "detail_type")
public class TripDetailType
    extends Model {

  public static Finder<Integer, TripDetailType> find = new Finder(TripDetailType.class);
  @Id
  @Enumerated(EnumType.ORDINAL)
  private ReservationType detailtypeid;
  @Constraints.Required
  public  String          name;
  public  int             status;
  public  TripDetailType  parent;
  public  int             priority;
  @Version
  public  int             version;

  public static Finder<Integer, TripDetailType> getFind() {
    return find;
  }

  public static void setFind(Finder<Integer, TripDetailType> find) {
    TripDetailType.find = find;
  }

  public static List<TripDetailType> findActive() {
    return find.where().eq("status", 0).orderBy("rank asc").findList();
  }

  public TripDetailType getParent() {
    return parent;
  }

  private void setParent(TripDetailType parent) {
    this.parent = parent;
  }

  public int getPriority() {
    return priority;
  }

  private void setPriority(int priority) {
    this.priority = priority;
  }

  public ReservationType getDetailtypeid() {
    return detailtypeid;
  }

  public void setDetailtypeid(ReservationType detailtypeid) {
    this.detailtypeid = detailtypeid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
