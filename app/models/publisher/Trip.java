package models.publisher;

import com.avaje.ebean.*;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.persistence.IUmappedModel;
import com.mapped.publisher.persistence.IUmappedModelWithImage;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.FilterType;
import com.mapped.publisher.view.ImageView;
import org.apache.commons.lang3.time.StopWatch;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-14
 * Time: 10:01 AM
 * To change this template use File | Settings | File Templates.
 */

@Entity
public class Trip
    extends UmappedModelWithImage
    implements IUmappedModel<String, Trip> {

  public static Model.Finder<String, Trip> find = new Finder(Trip.class);

  @Id
  public String tripid;
  public int    triptype;
  public int    visibility;
  public String accesscode;
  public String tag;
  public String cmpyid;
  public String name;
  public String comments;
  public int    status;
  public Long   starttimestamp;
  public Long   endtimestamp;
  public Long   createdtimestamp;
  public Long   lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  String coverurl;
  String coverfilename;
  @ManyToOne
  @JoinColumn(name = "cover_image_pk", nullable = true, referencedColumnName = "pk")
  FileImage coverImage;

  public Trip clone() {
    return buildTrip(getCreatedby(), getCmpyid(), getName()).setCoverImage(getCoverImage())
                                                                  .setCoverurl(getCoverurl())
                                                                  .setCoverfilename(getCoverfilename())
                                                                  .setAccesscode(getAccesscode())
                                                                  .setTriptype(getTriptype())
                                                                  .setVisibility(getVisibility())
                                                                  .setTag(getTag())
                                                                  .setComments(getComments())
                                                                  .setStatus(getStatus())
                                                                  .setStarttimestamp(getStarttimestamp())
                                                                  .setEndtimestamp(getEndtimestamp());
  }

  public static List<Trip> findAll() {
    return find.all();
  }

  public static Trip findByPK(String pk) {
    return find.byId(pk);
  }

  public static int messengerTripsToCleanCount(long createdAfter, long endedBefore) {
    com.avaje.ebean.Query<MessengerConference> subQuery = Ebean.createQuery(MessengerConference.class)
                                                               .select("tripid")
                                                               .where()
                                                               .eq("removed", true)
                                                               .query();

    return find.where()
               .ge("createdtimestamp", createdAfter)
               .le("endtimestamp", endedBefore)
               .notIn("tripid", subQuery)
               .findCount();
  }

  public static void messengerTripsToCleanRun(long createdAfter, long endedBefore, QueryEachConsumer<Trip> consumer) {

    com.avaje.ebean.Query<MessengerConference> subQuery = Ebean.createQuery(MessengerConference.class)
                                                               .select("tripid")
                                                               .where()
                                                               .eq("removed", true)
                                                               .query();

    find.where()
        .ge("createdtimestamp", createdAfter)
        .le("endtimestamp", endedBefore)
        .notIn("tripid", subQuery)
        .findEach(consumer);
  }

  public static List<Trip> findActiveByUserId(String userId,
                                              int maxCount,
                                              List<String> cmpies,
                                              String term,
                                              int startPos) {
    if (term == null || term.length() == 0 || term.equals("%")) {
      return find.where()
                 .eq("triptype", 0)
                 .eq("createdBy", userId)
                 .eq("status", 0)
                 .in("cmpyId", cmpies)
                 .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
                 .gt("endTimestamp", System.currentTimeMillis())
                 .orderBy("starttimestamp asc, name asc")
                 .setFirstRow(startPos)
                 .setMaxRows(maxCount)
                 .findList();
    }
    else {
      return find.where()
                 .eq("triptype", 0)
                 .eq("createdBy", userId)
                 .eq("status", 0)
                 .in("cmpyId", cmpies)
                 .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
                 .orderBy("starttimestamp asc, name asc")
                 .setFirstRow(startPos)
                 .setMaxRows(maxCount)
                 .findList();
    }
  }

  public static List<Trip> findPendingByUserId(String userId,
                                               int maxCount,
                                               List<String> cmpies,
                                               String term,
                                               int startPos) {
    return find.where()
               .eq("triptype", 0)
               .eq("createdBy", userId)
               .eq("status", 1)
               .in("cmpyId", cmpies)
               .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
               .orderBy("starttimestamp asc, name asc")
               .setFirstRow(startPos)
               .setMaxRows(maxCount)
               .findList();
  }

  public static List<Trip> findPendingByCmpyIds(String userId,
                                                int maxCount,
                                                List<String> cmpies,
                                                String term,
                                                int startPos) {
    return find.where()
               .eq("triptype", 0)
               .not(Expr.eq("createdBy", userId))
               .eq("status", 1)
               .in("cmpyId", cmpies)
               .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
               .orderBy("starttimestamp asc, name asc")
               .setFirstRow(startPos)
               .setMaxRows(maxCount)
               .findList();
  }

  public static List<Trip> findActiveByCmpyIds(String userId,
                                               int maxCount,
                                               List<String> cmpies,
                                               String term,
                                               int startPos) {
    if (term == null || term.length() == 0 || term.equals("%")) {
      return find.where()
                 .eq("triptype", 0)
                 .not(Expr.eq("createdBy", userId))
                 .eq("status", 0)
                 .in("cmpyId", cmpies)
                 .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
                 .gt("endTimestamp", System.currentTimeMillis())
                 .orderBy("starttimestamp asc, name asc")
                 .setFirstRow(startPos)
                 .setMaxRows(maxCount)
                 .findList();
    }
    else {
      return find.where()
                 .eq("triptype", 0)
                 .not(Expr.eq("createdBy", userId))
                 .eq("status", 0)
                 .in("cmpyId", cmpies)
                 .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
                 .orderBy("starttimestamp asc, name asc")
                 .setFirstRow(startPos)
                 .setMaxRows(maxCount)
                 .findList();
    }
  }

  public static List<Trip> findActivePendingByCmpyIds(String userId,
                                                      int maxCount,
                                                      List<String> cmpies,
                                                      String term,
                                                      List<Integer> statuses,
                                                      int startPos,
                                                      String sortCol,
                                                      String sortOrder,
                                                      FilterType filterDate,
                                                      long currentClientTime) {

    String colName = "";

    long timestamp = 0;
    long startTimestamp = 0;

    if(filterDate == FilterType.INPROGRESS) {
      startTimestamp = currentClientTime;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.FUTURE) {
      startTimestamp = currentClientTime;
      timestamp = Long.MAX_VALUE;
    }
    else if(filterDate == FilterType.PAST) {
      startTimestamp = 0;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.ACTIVE) {
      startTimestamp = 0;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.ALL) {
      startTimestamp = 0;
      timestamp = Long.MAX_VALUE;
    }

    if(sortCol.equals("imgUrl") || sortCol.equals("tripName")) {
      colName = "name";
    }
    else {
      colName = "starttimestamp";
    }

    if(filterDate == FilterType.INPROGRESS) {
      return find.where()
              .in("status", statuses)
              .in("cmpyId", cmpies)
              .or(com.avaje.ebean.Expr.ilike("name", term),
                      com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term),
                              com.avaje.ebean.Expr.ilike("tag", term)))
              .le("starttimestamp", startTimestamp)
              .ge("endtimestamp", timestamp)
              .orderBy(colName + " " + sortOrder)
              .setFirstRow(startPos)
              .setMaxRows(maxCount)
              .findList();
    }
    else if(filterDate == FilterType.ACTIVE) {
      return find.where()
              .in("status", statuses)
              .in("cmpyId", cmpies)
              .or(com.avaje.ebean.Expr.ilike("name", term),
                      com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term),
                              com.avaje.ebean.Expr.ilike("tag", term)))
              .ge("endtimestamp", timestamp)
              .orderBy(colName + " " + sortOrder)
              .setFirstRow(startPos)
              .setMaxRows(maxCount)
              .findList();
    }
    else {
      return find.where()
              .in("status", statuses)
              .in("cmpyId", cmpies)
              .or(com.avaje.ebean.Expr.ilike("name", term),
                      com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term),
                              com.avaje.ebean.Expr.ilike("tag", term)))
              .ge("starttimestamp", startTimestamp)
              .le("endtimestamp", timestamp)
              .orderBy(colName + " " + sortOrder)
              .setFirstRow(startPos)
              .setMaxRows(maxCount)
              .findList();
    }
  }

  public static int countActivePendingByCmpyIds(String userId,
                                                      List<String> cmpies,
                                                      String term,
                                                      List<Integer> statuses,
                                                      FilterType filterDate,
                                                      long currentClientTime) {

    long timestamp = 0;
    long startTimestamp = 0;

    if(filterDate == FilterType.INPROGRESS) {
      startTimestamp = currentClientTime;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.FUTURE) {
      startTimestamp = currentClientTime;
      timestamp = Long.MAX_VALUE;
    }
    else if(filterDate == FilterType.PAST) {
      startTimestamp = 0;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.ACTIVE) {
      startTimestamp = 0;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.ALL) {
      startTimestamp = 0;
      timestamp = Long.MAX_VALUE;
    }

    if(filterDate == FilterType.INPROGRESS) {
      return find.where()
              .in("status", statuses)
              .in("cmpyId", cmpies)
              .or(com.avaje.ebean.Expr.ilike("name", term),
                      com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term),
                              com.avaje.ebean.Expr.ilike("tag", term)))
              .le("starttimestamp", startTimestamp)
              .ge("endtimestamp", timestamp)
              .orderBy("name asc")
              .findCount();
    }
    else if(filterDate == FilterType.ACTIVE) {
      return find.where()
              .in("status", statuses)
              .in("cmpyId", cmpies)
              .or(com.avaje.ebean.Expr.ilike("name", term),
                      com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term),
                              com.avaje.ebean.Expr.ilike("tag", term)))
              .ge("endtimestamp", timestamp)
              .orderBy("name asc")
              .findCount();
    }
    else {
      return find.where()
              .in("status", statuses)
              .in("cmpyId", cmpies)
              .or(com.avaje.ebean.Expr.ilike("name", term),
                      com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term),
                              com.avaje.ebean.Expr.ilike("tag", term)))
              .ge("starttimestamp", startTimestamp)
              .le("endtimestamp", timestamp)
              .orderBy("name asc")
              .findCount();
    }

  }

  public static List<Trip> findAllMyTripsByTime(String userId,
                                                int maxCount,
                                                List<String> cmpies,
                                                String term,
                                                List<Integer> statuses,
                                                int startPos,
                                                boolean allTrips,
                                                String sortCol,
                                                String sortOrder,
                                                FilterType filterDate,
                                                long currentClientTime) {
    //Thu, 01 Jan 1970 01:00:00 GMT
    long timestamp = 0;
    long startTimestamp = 0;

    if(filterDate == FilterType.INPROGRESS) {
      startTimestamp = currentClientTime;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.FUTURE) {
      startTimestamp = currentClientTime;
      timestamp = Long.MAX_VALUE;
    }
    else if(filterDate == FilterType.PAST) {
      startTimestamp = 0;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.ACTIVE) {
      startTimestamp = 0;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.ALL) {
      startTimestamp = 0;
      timestamp = Long.MAX_VALUE;
    }

    String colName;
    if(sortCol.equals("imgUrl") || sortCol.equals("tripName")) {
      colName = "name";
    }
    else {
      colName = "starttimestamp";
    }

    if (!allTrips) {
      if(filterDate == FilterType.INPROGRESS) {
        return find.where()
                .in("status", statuses)
                .in("cmpyId", cmpies)
                .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term), com.avaje.ebean.Expr.ilike("tag", term)))
                .eq("createdby", userId)
                .le("starttimestamp", startTimestamp)
                .ge("endtimestamp", timestamp)
                .orderBy(colName + " " + sortOrder)
                .setFirstRow(startPos)
                .setMaxRows(maxCount)
                .findList();
      }
      else if(filterDate == FilterType.ACTIVE) {
        return find.where()
                .in("status", statuses)
                .in("cmpyId ", cmpies)
                .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term), com.avaje.ebean.Expr.ilike("tag", term)))
                .eq("createdby", userId)
                .ge("endtimestamp", timestamp)
                .orderBy(colName + " " + sortOrder)
                .setFirstRow(startPos)
                .setMaxRows(maxCount)
                .findList();
      }
      else {
        return find.where()
                .in("status", statuses)
                .in("cmpyId", cmpies)
                .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term), com.avaje.ebean.Expr.ilike("tag", term)))
                .eq("createdby", userId)
                .ge("starttimestamp", startTimestamp)
                .le("endtimestamp", timestamp)
                .orderBy(colName + " " + sortOrder)
                .setFirstRow(startPos)
                .setMaxRows(maxCount)
                .findList();
      }
    } else {
      if(filterDate == FilterType.INPROGRESS) {
        return find.where()
                .in("status", statuses)
                .in("cmpyId", cmpies)
                .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term), com.avaje.ebean.Expr.ilike("tag", term)))
                .le("starttimestamp", startTimestamp)
                .ge("endtimestamp", timestamp)
                .orderBy(colName + " " + sortOrder)
                .setFirstRow(startPos)
                .setMaxRows(maxCount)
                .findList();
      }
      else if(filterDate == FilterType.ACTIVE) {
        return find.where()
                .in("status", statuses)
                .in("cmpyId", cmpies)
                .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term), com.avaje.ebean.Expr.ilike("tag", term)))
                .ge("endtimestamp", timestamp)
                .orderBy(colName + " " + sortOrder)
                .setFirstRow(startPos)
                .setMaxRows(maxCount)
                .findList();
      }
      else {
        return find.where()
                .in("status", statuses)
                .in("cmpyId", cmpies)
                .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term), com.avaje.ebean.Expr.ilike("tag", term)))
                .ge("starttimestamp", startTimestamp)
                .le("endtimestamp", timestamp)
                .orderBy(colName + " " + sortOrder)
                .setFirstRow(startPos)
                .setMaxRows(maxCount)
                .findList();
      }
    }
  }

  public static int countAllMyTripsByTime(String userId,
                                                List<String> cmpies,
                                                String term,
                                                List<Integer> statuses,
                                                boolean allTrips,
                                                FilterType filterDate,
                                                long currentClientTime) {
    //Thu, 01 Jan 1970 01:00:00 GMT
    long timestamp = 0;
    long startTimestamp = 0;

    if(filterDate == FilterType.INPROGRESS) {
      startTimestamp = currentClientTime;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.FUTURE) {
      startTimestamp = currentClientTime;
      timestamp = Long.MAX_VALUE;
    }
    else if(filterDate == FilterType.PAST) {
      startTimestamp = 0;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.ACTIVE) {
      startTimestamp = 0;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.ALL) {
      startTimestamp = 0;
      timestamp = Long.MAX_VALUE;
    }

    if (!allTrips) {
      if(filterDate == FilterType.INPROGRESS) {
        return find.where()
                .in("status", statuses)
                .in("cmpyId", cmpies)
                .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term), com.avaje.ebean.Expr.ilike("tag", term)))
                .eq("createdby", userId)
                .le("starttimestamp", startTimestamp)
                .ge("endtimestamp", timestamp)
                .orderBy("name asc")
                .findCount();
      }
      else if(filterDate == FilterType.ACTIVE) {
        return find.where()
                .in("status", statuses)
                .in("cmpyId", cmpies)
                .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term), com.avaje.ebean.Expr.ilike("tag", term)))
                .eq("createdby", userId)
                .ge("endtimestamp", timestamp)
                .orderBy("name asc")
                .findCount();
      }
      else {
        return find.where()
                .in("status", statuses)
                .in("cmpyId", cmpies)
                .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term), com.avaje.ebean.Expr.ilike("tag", term)))
                .eq("createdby", userId)
                .ge("starttimestamp", startTimestamp)
                .le("endtimestamp", timestamp)
                .orderBy("name asc")
                .findCount();
      }
    } else {
      if(filterDate == FilterType.INPROGRESS) {
        return find.where()
                .in("status", statuses)
                .in("cmpyId", cmpies)
                .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term), com.avaje.ebean.Expr.ilike("tag", term)))
                .le("starttimestamp", startTimestamp)
                .ge("endtimestamp", timestamp)
                .orderBy("name asc")
                .findCount();
      }
      else if(filterDate == FilterType.ACTIVE) {
        return find.where()
                .in("status", statuses)
                .in("cmpyId", cmpies)
                .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term), com.avaje.ebean.Expr.ilike("tag", term)))
                .ge("endtimestamp", timestamp)
                .orderBy("name asc")
                .findCount();
      }
      else {
        return find.where()
                .in("status", statuses)
                .in("cmpyId", cmpies)
                .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.or(com.avaje.ebean.Expr.ilike("createdby", term), com.avaje.ebean.Expr.ilike("tag", term)))
                .ge("starttimestamp", startTimestamp)
                .le("endtimestamp", timestamp)
                .orderBy("name asc")
                .findCount();
      }
    }
  }

  public static List<Trip> findPendingByUserIds(int maxCount,
                                                List<String> userIds,
                                                String term,
                                                int startPos,
                                                List<String> cmpies) {
    return find.where()
               .eq("triptype", 0)
               .eq("status", 1)
               .in("cmpyId", cmpies)
               .in("createdBy", userIds)
               .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
               .orderBy("startTimestamp asc, name asc")
               .setFirstRow(startPos)
               .setMaxRows(maxCount)
               .findList();
  }

  public static List<Trip> findActiveByUserIds(int maxCount,
                                               List<String> userIds,
                                               String term,
                                               int startPos,
                                               List<String> cmpies) {
    if (term == null || term.length() == 0 || term.equals("%")) {
      return find.where()
                 .eq("triptype", 0)
                 .in("cmpyId", cmpies)
                 .eq("status", 0)
                 .in("createdBy", userIds)
                 .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
                 .gt("endTimestamp", System.currentTimeMillis())
                 .orderBy("starttimestamp asc, name asc")
                 .setFirstRow(startPos)
                 .setMaxRows(maxCount)
                 .findList();
    }
    else {
      return find.where()
                 .eq("triptype", 0)
                 .in("cmpyId", cmpies)
                 .eq("status", 0)
                 .in("createdBy", userIds)
                 .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
                 .orderBy("starttimestamp asc, name asc")
                 .setFirstRow(startPos)
                 .setMaxRows(maxCount)
                 .findList();
    }

  }

  public static List<Trip> findActiveToursByUserIds(int maxCount,
                                                    List<String> userIds,
                                                    String term,
                                                    int startPos,
                                                    List<String> cmpies) {
    if (term == null || term.length() == 0 || term.equals("%")) {
      return find.where()
                 .eq("status", 0)
                 .in("cmpyId", cmpies)
                 .in("createdBy", userIds)
                 .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
                 .gt("endTimestamp", System.currentTimeMillis())
                 .orderBy("starttimestamp asc, name asc")
                 .setFirstRow(startPos)
                 .setMaxRows(maxCount)
                 .findList();
    }
    else {
      return find.where()
                 .eq("status", 0)
                 .in("cmpyId", cmpies)
                 .in("createdBy", userIds)
                 .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
                 .orderBy("starttimestamp asc, name asc")
                 .setFirstRow(startPos)
                 .setMaxRows(maxCount)
                 .findList();
    }

  }

  public static List<Trip> findActiveToursByCmpyIds(String userId,
                                                    int maxCount,
                                                    List<String> cmpies,
                                                    String term,
                                                    int startPos) {
    if (term == null || term.length() == 0 || term.equals("%")) {
      return find.where()
                 .not(Expr.eq("createdBy", userId))
                 .eq("status", 0)
                 .in("cmpyId", cmpies)
                 .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
                 .gt("endTimestamp", System.currentTimeMillis())
                 .orderBy("starttimestamp asc, name asc")
                 .setFirstRow(startPos)
                 .setMaxRows(maxCount)
                 .findList();
    }
    else {
      return find.where()
                 .not(Expr.eq("createdBy", userId))
                 .eq("status", 0)
                 .in("cmpyId", cmpies)
                 .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
                 .orderBy("starttimestamp asc, name asc")
                 .setFirstRow(startPos)
                 .setMaxRows(maxCount)
                 .findList();
    }
  }

  public static List<Trip> findByUserIdDate(String userId, long startDate, long endDate, int status) {
    return find.where()
               .eq("createdBy", userId)
               .eq("status", status)
               .or(com.avaje.ebean.Expr.and(com.avaje.ebean.Expr.ge("startTimestamp", startDate),
                                            com.avaje.ebean.Expr.le("startTimestamp", endDate)),
                   com.avaje.ebean.Expr.and(com.avaje.ebean.Expr.ge("endTimestamp", startDate),
                                            com.avaje.ebean.Expr.lt("startTimestamp", startDate)))
               .and(com.avaje.ebean.Expr.gt("startTimestamp", 0), com.avaje.ebean.Expr.gt("endTimestamp", 0))
               .orderBy("lastupdatedtimestamp desc, name asc")
               .findList();
  }

  public static List<Trip> findByUserIdsDate(List<String> userIds, long startDate, long endDate, int status) {
    return find.where()
               .in("createdBy", userIds)
               .eq("status", status)
               .or(com.avaje.ebean.Expr.and(com.avaje.ebean.Expr.ge("startTimestamp", startDate),
                                            com.avaje.ebean.Expr.le("startTimestamp", endDate)),
                   com.avaje.ebean.Expr.and(com.avaje.ebean.Expr.ge("endTimestamp", startDate),
                                            com.avaje.ebean.Expr.lt("startTimestamp", startDate)))
               .and(com.avaje.ebean.Expr.gt("startTimestamp", 0), com.avaje.ebean.Expr.gt("endTimestamp", 0))
               .orderBy("lastupdatedtimestamp desc, name asc")
               .findList();
  }

  public static List<Trip> findByCmpyIdsDate(String userId,
                                             List<String> cmpies,
                                             long startDate,
                                             long endDate,
                                             int status) {
    return find.where()
               .not(Expr.eq("createdBy", userId))
               .eq("status", status)
               .in("cmpyId", cmpies)
               .or(com.avaje.ebean.Expr.and(com.avaje.ebean.Expr.ge("startTimestamp", startDate),
                                            com.avaje.ebean.Expr.le("startTimestamp", endDate)),
                   com.avaje.ebean.Expr.and(com.avaje.ebean.Expr.ge("endTimestamp", startDate),
                                            com.avaje.ebean.Expr.lt("startTimestamp", startDate)))
               .and(com.avaje.ebean.Expr.gt("startTimestamp", 0), com.avaje.ebean.Expr.gt("endTimestamp", 0))
               .orderBy("lastupdatedtimestamp desc, name asc")
               .findList();
  }

  public static List<Trip> findActiveTourByUserId(String userId,
                                                  int maxCount,
                                                  List<String> cmpies,
                                                  String term,
                                                  int startPos) {
    StopWatch sw = new StopWatch();
    sw.start();
    List<Trip> result = null;

    if (term == null || term.length() == 0 || term.equals("%")) {
      result = find.where()
                   .gt("endtimestamp", System.currentTimeMillis())
                   .eq("createdBy", userId)
                   .eq("status", 0)
                   .in("cmpyId", cmpies)
                   .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
                   .orderBy("starttimestamp desc, name asc")
                   .setFirstRow(startPos)
                   .setMaxRows(maxCount)
                   .findList();
    }
    else {
      result = find.where()
                   .eq("createdBy", userId)
                   .eq("status", 0)
                   .in("cmpyId", cmpies)
                   .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
                   .orderBy("starttimestamp desc, name asc")
                   .setFirstRow(startPos)
                   .setMaxRows(maxCount)
                   .findList();
    }
    sw.stop();
    Log.debug("Dashboard: findActiveTourByUserId() took: " + sw.getTime() + "ms");
    return result;
  }

  public static List<Trip> findPendingTourByUserId(String userId,
                                                   int maxCount,
                                                   List<String> cmpies,
                                                   String term,
                                                   int startPos) {
    return find.where()
               .eq("createdBy", userId)
               .eq("status", 1)
               .in("cmpyId", cmpies)
               .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
               .orderBy("starttimestamp desc, name asc")
               .setFirstRow(startPos)
               .setMaxRows(maxCount)
               .findList();
  }

  public static List<Trip> findPendingTourByCmpyIds(String userId, int maxCount, List<String> cmpies, String term) {
    return find.where()
               .not(Expr.eq("createdBy", userId))
               .eq("status", 1)
               .in("cmpyId", cmpies)
               .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
               .orderBy("lastupdatedtimestamp desc, name asc")
               .setMaxRows(maxCount)
               .findList();
  }

  public static List<Trip> findActiveTourByCmpyIds(String userId,
                                                   int maxCount,
                                                   List<String> cmpies,
                                                   String term,
                                                   int startPos) {
    return find.where()
               .eq("status", 0)
               .not(Expr.eq("createdBy", userId))
               .in("cmpyId", cmpies)
               .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
               .orderBy("starttimestamp desc, name asc")
               .setFirstRow(startPos)
               .setMaxRows(maxCount)
               .findList();
  }

  public static List<Trip> findOtherPendingTourByCmpyIds(String userId,
                                                         int maxCount,
                                                         List<String> cmpies,
                                                         String term,
                                                         int startPos) {
    return find.where()
               .not(Expr.eq("createdBy", userId))
               .eq("status", 1)
               .in("cmpyId", cmpies)
               .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
               .orderBy("starttimestamp desc, name asc")
               .setFirstRow(startPos)
               .setMaxRows(maxCount)
               .findList();
  }

  public static List<Trip> findPendingToursByUserIds(int maxCount,
                                                     List<String> userIds,
                                                     String term,
                                                     int startPos,
                                                     List<String> cmpies) {
    return find.where()
               .eq("status", 1)
               .in("cmpyId", cmpies)
               .in("createdBy", userIds)
               .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
               .orderBy("starttimestamp desc, name asc")
               .setFirstRow(startPos)
               .setMaxRows(maxCount)
               .findList();
  }

  public static List<Trip> findOtherActiveTourByCmpyIds(String userId, int maxCount, List<String> cmpies, String term) {
    return find.where()
               .not(Expr.eq("createdBy", userId))
               .eq("status", 0)
               .in("cmpyId", cmpies)
               .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
               .gt("starttimestamp", System.currentTimeMillis())
               .orderBy("lastupdatedtimestamp desc, name asc")
               .setMaxRows(maxCount)
               .findList();
  }

  public static List<Trip> findPendingTourByUserIds(int maxCount, List<String> userIds, String term) {
    return find.where()
               .eq("status", 1)
               .in("createdBy", userIds)
               .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
               .orderBy("starttimestamp asc, name asc")
               .setMaxRows(maxCount)
               .findList();
  }

  public static List<Trip> findActiveTourByUserIds(int maxCount, List<String> userIds, String term) {
    return find.where()
               .eq("status", 0)
               .in("createdBy", userIds)
               .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
               .gt("starttimestamp", System.currentTimeMillis())
               .orderBy("lastupdatedtimestamp desc, name asc")
               .setMaxRows(maxCount)
               .findList();

  }

  public static List<Trip> findByUserIdTourDate(String userId, long startDate, long endDate, int status) {
    return find.where()
               .eq("createdBy", userId)
               .eq("status", status)
               .ge("startTimestamp", startDate)
               .le("startTimestamp", endDate)
               .orderBy("lastupdatedtimestamp desc, name asc")
               .findList();
  }

  public static List<Trip> findByUserIdsTourDate(List<String> userIds, long startDate, long endDate, int status) {
    return find.where()
               .in("createdBy", userIds)
               .eq("status", status)
               .ge("startTimestamp", startDate)
               .le("startTimestamp", endDate)
               .orderBy("lastupdatedtimestamp desc, name asc")
               .findList();
  }

  public static List<Trip> findByCmpyIdsTourDate(String userId,
                                                 List<String> cmpies,
                                                 long startDate,
                                                 long endDate,
                                                 int status) {
    return find.where()
               .not(Expr.eq("createdBy", userId))
               .eq("status", status)
               .in("cmpyId", cmpies)
               .ge("startTimestamp", startDate)
               .le("startTimestamp", endDate)
               .orderBy("lastupdatedtimestamp desc, name asc")
               .findList();
  }

  public static int numOfTrips(String cmpyid) {
    return find.where().eq("triptype", 0).eq("cmpyId", cmpyid).findCount();
  }

  public static List<Trip> findByAccessCode(String accessCode, String cmpyId) {
    return find.where()
               .eq("accesscode", accessCode)
               .eq("cmpyId", cmpyId)
               .eq("status", 0)
               .ge("endTimestamp", System.currentTimeMillis())
               .findList();
  }

  public static List<Trip> findPendingByCmpyNameType(String name, String cmpyId, int triptype) {
    return find.where().ieq("name", name).eq("cmpyId", cmpyId).eq("status", 1).findList();
  }

  public static List<Trip> findActiveNameCmpyId(int maxCount, String cmpyid, String term) {
    if (term != null && term.length() > 0) {
      return find.where()
                 .or(com.avaje.ebean.Expr.eq("status", 0), com.avaje.ebean.Expr.eq("status", 1))
                 .eq("cmpyId", cmpyid)
                 .or(com.avaje.ebean.Expr.ilike("name", term), com.avaje.ebean.Expr.ilike("tag", term))
                 .gt("endTimestamp", System.currentTimeMillis())
                 .orderBy("name asc, starttimestamp asc")
                 .setMaxRows(maxCount)
                 .findList();
    }
    return null;
  }

  public static int mergeCmpy(String srcCmpyId, String targetCmpyId, String userId) {
    String s = "UPDATE trip set cmpyid = :targetCmpyId, lastupdatedtimestamp = :timestamp, " + "modifiedby = :userid " +
               "" + "" + "" + " where cmpyid = :srcCmpyId ";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("timestamp", System.currentTimeMillis());
    update.setParameter("userid", userId);
    update.setParameter("srcCmpyId", srcCmpyId);
    update.setParameter("targetCmpyId", targetCmpyId);

    return Ebean.execute(update);
  }

  public static int getARowCountByUserUpcomingTrips(String userid, long currentClientTime) {
    return find.where()
               .eq("status", 0)
               .eq("createdby", userid)
               .lt("endtimestamp", currentClientTime)
               .findCount();
  }

  public static int getARowCountByUserUpcomingTripsFiltered(String userid, String term, long currentClientTime) {
    return find.where()
               .eq("status", 0)
               .eq("createdby", userid)
               .ge("endtimestamp", currentClientTime)
               .or(Expr.icontains("name", term), Expr.icontains("tag", term))
               .findCount();
  }

  public static List<Trip> getPublishedTripPage(String userid, String term, int start, int count, long currentClientTime) {
    return find.where()
               .eq("status", 0)
               .eq("createdby", userid)
               .ge("endtimestamp", currentClientTime)
               .or(Expr.icontains("name", term), Expr.icontains("tag", term))
               .orderBy("starttimestamp asc,name asc")
               .setFirstRow(start)
               .setMaxRows(count)
               .findList();
  }

  public static int getARowCountByUserPendingTrips(String userid) {
    return find.where().eq("status", 1).eq("createdby", userid).findCount();
  }

  public static int getARowCountByUserPendingTripsFiltered(String userid, String term) {
    return find.where()
               .eq("status", 1)
               .eq("createdby", userid)
               .or(Expr.icontains("name", term), Expr.icontains("tag", term))
               .findCount();
  }

  public static List<Trip> getPendingTripPage(String userid, String term, int start, int count) {
    return find.where()
               .eq("status", 1)
               .eq("createdby", userid)
               .or(Expr.icontains("name", term), Expr.icontains("tag", term))
               .orderBy("starttimestamp desc,name asc")
               .setFirstRow(start)
               .setMaxRows(count)
               .findList();
  }

  public static List<Trip> findPublishedTripsByTag(String cmpyid, String tag) {
    return find.where()
            .eq("status", 0)
            .eq("cmpyid", cmpyid)
            .contains("tag", tag)
            .order("createdtimestamp desc")
            .setMaxRows(10)
            .findList();
  }

  public static Trip buildTrip(String createdBy, String cmpyId, String tripName) {
    Trip trip = new Trip();
    trip.setCmpyid(cmpyId);
    trip.setCreatedby(createdBy);
    trip.setCreatedtimestamp(System.currentTimeMillis());
    trip.setModifiedby(createdBy);
    trip.setLastupdatedtimestamp(trip.createdtimestamp);
    trip.setName(tripName);
    trip.setTriptype(APPConstants.TOUR_TYPE);
    trip.setTripid(DBConnectionMgr.getUniqueId());
    trip.setStatus(APPConstants.STATUS_PENDING);
    trip.setStarttimestamp(System.currentTimeMillis());
    trip.setEndtimestamp(System.currentTimeMillis());
    return trip;
  }

  public String getCreatedby() {
    return createdby;
  }

  public Trip setCreatedby(String createdby) {
    this.createdby = createdby;
    return this;
  }

  public String getCmpyid() {
    return cmpyid;
  }

  public Trip setCmpyid(String cmpyid) {
    this.cmpyid = cmpyid;
    return this;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public FileImage getCoverImage() {
    return coverImage;
  }

  @Deprecated
  public String getCoverurl() {
    return coverurl;
  }

  public String getAccesscode() {
    return accesscode;
  }

  public Trip setAccesscode(String accesscode) {
    this.accesscode = accesscode;
    return this;
  }
  /* BILLING END */

  @Deprecated
  public Trip setCoverurl(String coverurl) {
    this.coverurl = coverurl;
    return this;
  }

  public Trip setCoverImage(FileImage coverImage) {
    this.coverImage = coverImage;
    return this;
  }

  public void markModified(String legacyId) {
    setModifiedby(legacyId);
    setLastupdatedtimestamp(System.currentTimeMillis());
  }

  public String getComments() {
    return comments;
  }

  public Trip setComments(String comments) {
    this.comments = comments;
    return this;
  }

  public int getStatus() {
    return status;
  }

  public Trip setStatus(int status) {
    this.status = status;
    return this;
  }

  public Long getStarttimestamp() {
    return starttimestamp;
  }

  public Trip setStarttimestamp(Long starttimestamp) {
    this.starttimestamp = starttimestamp;
    return this;
  }

  public Long getEndtimestamp() {
    return endtimestamp;
  }

  public Trip setEndtimestamp(Long endtimestamp) {
    this.endtimestamp = endtimestamp;
    return this;
  }

  public int getTriptype() {
    return triptype;
  }

  public Trip setTriptype(int triptype) {
    this.triptype = triptype;
    return this;
  }

  public int getVisibility() {
    return visibility;
  }

  public Trip setVisibility(int visibility) {
    this.visibility = visibility;
    return this;
  }

  public String getTag() {
    return tag;
  }

  public Trip setTag(String tag) {
    this.tag = tag;
    return this;
  }

  @Override
  public String getImageUrl() {
    if (getFileImage() == null) {
      return getCoverurl();
    }
    else {
      return getFileImage().getUrl();
    }
  }

  @Override
  public FileImage getFileImage() {
    return getCoverImage();
  }

  @Override
  public String getMobileImageFilename() {
    FileImage fi = getFileImage();
    if (fi != null) {
      FileInfo f = fi.getFile();
      return Utils.base64encode(f.getPk()) + '.' + f.getFiletype().getExtension();
    }
    else {
      return getCoverfilename();
    }
  }

  @Deprecated
  public String getCoverfilename() {
    return coverfilename;
  }

  public Trip setCoverfilename(String coverfilename) {
    this.coverfilename = coverfilename;
    return this;
  }

  public ImageView getCoverView() {
    if (getFileImage() != null) {
      return getFileImage().toImageView();
    }
    if (this.coverurl == null || this.coverurl.length() < 10) {
      return null;
    }
    ImageView iv = new ImageView();
    iv.url = this.coverurl;
    iv.fileName = this.coverfilename;
    return iv;
  }

  @Override
  public IUmappedModelWithImage setFileImage(FileImage image) {
    setCoverImage(image);
    if(image != null) {
      setCoverurl(image.getUrl());
      setCoverfilename(image.getFilename());
    }
    return this;
  }

  @Override
  public Trip getByPk(String pk) {
    return find.byId(pk);
  }

  @Override
  public String getPk() {
    return getTripid();
  }

  public String getTripid() {
    return tripid;
  }

  public Trip setTripid(String tripid) {
    this.tripid = tripid;
    return this;
  }

  @Override
  public String getPkAsString() {
    return getTripid();
  }

  @Override
  public Long getCreatedByPk() {
    return Account.findByLegacyId(getCreatedby()).getUid();
  }

  @Override
  public Long getModifiedByPk() {
    return Account.findByLegacyId(getModifiedby()).getUid();
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public Trip setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
    return this;
  }

  @Override
  public String getCreatedByLegacy() {
    return getCreatedby();
  }

  @Override
  public String getModifiedByLegacy() {
    return getModifiedby();
  }

  @Override
  public Long getCreatedEpoch() {
    return getCreatedtimestamp();
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public Trip setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
    return this;
  }

  @Override
  public Timestamp getCreatedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getCreatedtimestamp()));
  }

  @Override
  public Long getModifiedEpoch() {
    return getLastupdatedtimestamp();
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public Trip setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
    return this;
  }

  @Override
  public Timestamp getModifiedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getLastupdatedtimestamp()));
  }
}
