package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-01
 * Time: 11:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class LoginForm {
    private String inUserId;
    private String inPwd;
    private String confirmPwd;
    private int timezoneOffset;

    public String validate() {
        if(inUserId == null || inPwd == null || !(inUserId.length() > 0 && inPwd.length() > 0)) {
            return "Invalid email or password";
        }
        return null;
    }


    public String getInUserId() {
        return inUserId;
    }

    public void setInUserId(String inUserId) {
        this.inUserId = inUserId;
    }

    public String getInPwd() {
        return inPwd;
    }

    public void setInPwd(String inPwd) {
        this.inPwd = inPwd;
    }

    public String getConfirmPwd() {
        return confirmPwd;
    }

    public void setConfirmPwd(String confirmPwd) {
        this.confirmPwd = confirmPwd;
    }

    public int getTimezoneOffset() {
        return timezoneOffset;
    }

    public void setTimezoneOffset(int timezoneOffset) {
        this.timezoneOffset = timezoneOffset;
    }
}
