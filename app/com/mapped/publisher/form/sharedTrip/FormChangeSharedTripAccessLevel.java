package com.mapped.publisher.form.sharedTrip;

import com.mapped.publisher.common.SecurityMgr;

/**
 * Created by surge on 2016-01-04.
 */
public class FormChangeSharedTripAccessLevel {
  public String                  inShareUserId;
  public SecurityMgr.AccessLevel inAccessLevel;
  public String                  inOriginPage;

  public boolean isValid() {
    return inAccessLevel != null && inShareUserId != null && inShareUserId.length() > 0;
  }
}
