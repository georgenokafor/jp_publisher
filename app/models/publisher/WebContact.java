package models.publisher;

import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-06-17
 * Time: 11:55 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="web_contact")
public class WebContact extends Model {
    public String name;
    public String phone;
    public String email;
    public String note;

    @Id
    public String pk;

    @Constraints.Required
    public int status;
    public Long createdtimestamp;
    public Long lastupdatedtimestamp;
    public String createdby;
    public String modifiedby;

    @Version
    public int version;


    public static Model.Finder<String,WebContact> find = new Finder(WebContact.class);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getCreatedtimestamp() {
        return createdtimestamp;
    }

    public void setCreatedtimestamp(Long createdtimestamp) {
        this.createdtimestamp = createdtimestamp;
    }

    public Long getLastupdatedtimestamp() {
        return lastupdatedtimestamp;
    }

    public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
        this.lastupdatedtimestamp = lastupdatedtimestamp;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
