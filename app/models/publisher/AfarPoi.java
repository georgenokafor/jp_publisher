package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbJsonB;
import com.avaje.ebean.*;
import com.mapped.publisher.persistence.RecordState;
import com.umapped.external.afar.CategoryList;
import com.umapped.external.afar.Highlight;
import com.umapped.external.afar.Pois;
import com.umapped.itinerary.analyze.model.Location;
import org.postgis.Point;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by george on 2017-11-01.
 */
@Entity @Table(name = "afar_poi")
public class AfarPoi extends Model implements JsonSupport{

    public static Model.Finder<Integer, AfarPoi> find = new Finder<>(AfarPoi.class);


    //To be deleted
    //private String afarPoiId;
    //To be deleted
    //private String type;

    @Id
    private int afarId;
    private int afarCityId;
    private int afarCountryId;
    private int afarListId;
    private String slug;
    private String title;
    private String placeName;
    private String description;
    @DbJsonB
    private Map<String, Object> types;
    @Column(name="location")
    private Point location;

    @DbJsonB
    private Map<String, Object> data;

    //To be deleted
    public String getAfarPoiId() {
        return slug;
    }

    //To be deleted
    public void setAfarPoiId(String afarPoiId) {
        this.slug = afarPoiId;
    }

    //To be deleted
    public String getType() {
        return "";
    }

    //To be deleted
    public void setType(String type) {
        //this.type = type;
    }

    public int getAfarId() {
        return afarId;
    }

    public void setAfarId(int afarId) {
        this.afarId = afarId;
    }

    public int getAfarCityId() {
        return afarCityId;
    }

    public void setAfarCityId(int afarCityId) {
        this.afarCityId = afarCityId;
    }

    public int getAfarCountryId() {
        return afarCountryId;
    }

    public void setAfarCountryId(int afarCountryId) {
        this.afarCountryId = afarCountryId;
    }

    public int getAfarListId() {
        return afarListId;
    }

    public void setAfarListId(int afarListId) {
        this.afarListId = afarListId;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, Object> getTypes() {
        return types;
    }

    public void setTypes(Map<String, Object> types) {
        this.types = types;
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public static AfarPoi findUniquePoi(int afarId, int listId) {
        return find.where().eq("afar_id", afarId).eq("afar_list_id", listId).findUnique();
    }

    public static AfarPoi findById(String id) {
        return find.where().eq("afar_poi_id", id).findUnique();
    }

    public static List<AfarPoi> findUnlinkedAfarPois(List<String> ids) {
        return find.where().isNull("afar_city_id").in("afar_poi_id", ids).findList();
    }

    public static AfarPoi findUnlinkedAfarPoi(String afarPoiId) {
        return find.where().isNull("afar_city_id").eq("afar_poi_id", afarPoiId).findUnique();
    }

    public static AfarPoi findLinkedIncompleteAfarPoi(String afarPoiId) {
        return find.where().eq("afar_poi_id", afarPoiId).findUnique();
    }

    public static List<AfarPoi> findPoisByCity(String city) {
        return find.where().eq("afar_city_id", city).findList();
    }

    public static List<AfarPoi> findPoisByKeywordDestination(Integer locationId, String keyword, boolean isCountry, Integer startPos, Integer maxRows, String type) {
        if (isCountry) {
            if (maxRows == null || maxRows < 1) {
                return find
                    .where()
                    .eq("afar_country_id", locationId)
                    .or(Expr.ilike("title", keyword), Expr.ilike("description", keyword))
                    .raw("upper(cast(types->'categories' as text)) LIKE ?", type)
                    .setFirstRow(startPos)
                    .findList();
            } else {
                return find
                    .where()
                    .eq("afar_country_id", locationId)
                    .or(Expr.ilike("title", keyword), Expr.ilike("description", keyword))
                    .raw("upper(cast(types->'categories' as text)) LIKE ?", type)
                    .setFirstRow(startPos)
                    .setMaxRows(maxRows)
                    .findList();
            }
        } else {
            if (maxRows == null || maxRows < 1) {
                return find
                    .where()
                    .eq("afar_city_id", locationId)
                    .or(Expr.ilike("title", keyword), Expr.ilike("description", keyword))
                    .raw("upper(cast(types->'categories' as text)) LIKE ?", type)
                    .setFirstRow(startPos)
                    .findList();
            } else {
                return find
                    .where()
                    .eq("afar_city_id", locationId)
                    .or(Expr.ilike("title", keyword), Expr.ilike("description", keyword))
                    .raw("upper(cast(types->'categories' as text)) LIKE ?", type)
                    .setFirstRow(startPos)
                    .setMaxRows(maxRows)
                    .findList();
            }
        }
    }

    public static int countPoisByKeywordDestination(Integer locationId, String keyword, boolean isCountry, String type) {
        if (isCountry) {
            return find
                .where()
                .eq("afar_country_id", locationId)
                .or(Expr.ilike("title", keyword), Expr.ilike("description", keyword))
                .raw("upper(cast(types->'categories' as text)) LIKE ?", type)
                .findCount();
        } else {
            return find
                .where()
                .eq("afar_city_id", locationId)
                .or(Expr.ilike("title", keyword), Expr.ilike("description", keyword))
                .raw("upper(cast(types->'categories' as text)) LIKE ?", type)
                .findCount();
        }
    }

    public Map<String, Object> marshalTypes(CategoryList h) {
        return marshal(h);
    }

    public CategoryList unmarshalTypes(Map<String, Object> data) {
        return unmarshal(data, CategoryList.class);
    }

    public Map<String, Object> marshalData(Highlight h) {
        return marshal(h);
    }

    public Highlight unmarshalData(Map<String, Object> data) {
        return unmarshal(data, Highlight.class);
    }

    //to be deleted

    /*public void setAfarCityId(String afarCityId) {
        this.afarCityId = Integer.valueOf(afarCityId);
    }*/
}
