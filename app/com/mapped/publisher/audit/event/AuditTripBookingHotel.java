package com.mapped.publisher.audit.event;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.sql.Timestamp;

/**
 * Created by Serguei Moutovkin on 2014-05-21.
 */
@JsonTypeName("TRIP_BOOKING_HOTEL")
public class AuditTripBookingHotel
    extends AuditEvent {

  public String bookingId;
  public String name;
  public Timestamp checkIn;
  public Timestamp checkOut;

  public AuditTripBookingHotel withBookingId(String id) {
    bookingId = id;
    return this;
  }

  public AuditTripBookingHotel withName(String name) {
    this.name = name;
    return this;
  }

  public AuditTripBookingHotel withCheckIn(long inDT) {
    this.checkIn = new Timestamp(inDT);
    return this;
  }

  public AuditTripBookingHotel withCheckOut(long outDT) {
    this.checkOut = new Timestamp(outDT);
    return this;
  }
}
