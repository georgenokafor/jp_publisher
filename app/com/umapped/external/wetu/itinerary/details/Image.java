package com.umapped.external.wetu.itinerary.details;

/**
 * Created by george on 2017-06-05.
 */

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "url",
    "url_fragment"
})
public class Image {

  @JsonProperty("url")
  private String url;
  @JsonProperty("url_fragment")
  private String urlFragment;

  /**
   *
   * @return
   *     The url
   */
  @JsonProperty("url")
  public String getUrl() {
    return url;
  }

  /**
   *
   * @param url
   *     The url
   */
  @JsonProperty("url")
  public void setUrl(String url) {
    this.url = url;
  }

  /**
   *
   * @return
   *     The urlFragment
   */
  @JsonProperty("url_fragment")
  public String getUrlFragment() {
    return urlFragment;
  }

  /**
   *
   * @param urlFragment
   *     The url_fragment
   */
  @JsonProperty("url_fragment")
  public void setUrlFragment(String urlFragment) {
    this.urlFragment = urlFragment;
  }


  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(url).append(urlFragment).toHashCode();
  }

  @Override
  public boolean equals(Object other) {
    if (other == this) {
      return true;
    }
    if ((other instanceof Image) == false) {
      return false;
    }
    Image rhs = ((Image) other);
    return new EqualsBuilder().append(url, rhs.url).append(urlFragment, rhs.urlFragment).isEquals();
  }

}
