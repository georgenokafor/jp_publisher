package com.mapped.publisher.msg.flightstat.event;

/**
 * Created by twong on 2015-02-03.
 */
public class Delay {
  private String departureGateDelayMinutes;
  private String departureRunwayDelayMinutes;
  private String arrivalGateDelayMinutes;
  private String arrivalRunwayDelayMinutes;

  public String getDepartureGateDelayMinutes() {
    return departureGateDelayMinutes;
  }

  public void setDepartureGateDelayMinutes(String departureGateDelayMinutes) {
    this.departureGateDelayMinutes = departureGateDelayMinutes;
  }

  public String getDepartureRunwayDelayMinutes() {
    return departureRunwayDelayMinutes;
  }

  public void setDepartureRunwayDelayMinutes(String departureRunwayDelayMinutes) {
    this.departureRunwayDelayMinutes = departureRunwayDelayMinutes;
  }

  public String getArrivalGateDelayMinutes() {
    return arrivalGateDelayMinutes;
  }

  public void setArrivalGateDelayMinutes(String arrivalGateDelayMinutes) {
    this.arrivalGateDelayMinutes = arrivalGateDelayMinutes;
  }

  public String getArrivalRunwayDelayMinutes() {
    return arrivalRunwayDelayMinutes;
  }

  public void setArrivalRunwayDelayMinutes(String arrivalRunwayDelayMinutes) {
    this.arrivalRunwayDelayMinutes = arrivalRunwayDelayMinutes;
  }
}
