package com.mapped.publisher.view;


/**
 * Created by george on 2016-08-22.
 */
public class EmailConfigView {
  public int consortium;
  public String cmpyid;
  public String host;
  public int port;
  public String username;
  public String password;
  public String emailDefault;
  public String emailNotice;
}
