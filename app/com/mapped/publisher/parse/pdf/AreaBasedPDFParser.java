package com.mapped.publisher.parse.pdf;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripperByArea;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class AreaBasedPDFParser implements PDFParser {

	private  Rectangle area;
    private PDDocument document = null;
    private List<?> allPages;
	
	public AreaBasedPDFParser(Rectangle rectangle) {
		this.area = rectangle;
	}
	
	@Override
	public String parse(InputStream input) throws Exception {
		StringBuffer buffer = new StringBuffer();

		try {


			try {
				document = PDDocument.load(input);
				PDFTextStripperByArea stripper = new PDFTextStripperByArea();
				stripper.setSortByPosition(true);
				Rectangle rect = new Rectangle(area.x, area.y, area.width,
						area.height);
				stripper.addRegion("body", rect);
				allPages = document.getDocumentCatalog().getAllPages();

				for (Object pdPage : allPages) {
					stripper.extractRegions((PDPage) pdPage);
					buffer.append(stripper.getTextForRegion("body"));
				}
			} finally {


			}
		} catch (IOException e) {
			throw e;
		}
		
		return buffer.toString();
	}

    public PDDocument getPDDocument () {
        return document;
    }

    public List<?> getAllPages () {
        return allPages;
    }


}
