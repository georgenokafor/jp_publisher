//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.05.08 at 02:53:54 PM EDT 
//


package com.umapped.external.travelbound.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_SearchCityResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_SearchCityResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="CityDetails" type="{}t_CityDetails"/>
 *           &lt;element name="Errors" type="{}t_Errors"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{}a_SearchCity"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_SearchCityResponse", propOrder = {
    "errors",
    "cityDetails"
})
public class TSearchCityResponse {

    @XmlElement(name = "Errors")
    protected TErrors errors;
    @XmlElement(name = "CityDetails")
    protected TCityDetails cityDetails;
    @XmlAttribute(name = "ISO")
    protected Boolean iso;
    @XmlAttribute(name = "CountryCode")
    protected String countryCode;

    /**
     * Gets the value of the errors property.
     * 
     * @return
     *     possible object is
     *     {@link TErrors }
     *     
     */
    public TErrors getErrors() {
        return errors;
    }

    /**
     * Sets the value of the errors property.
     * 
     * @param value
     *     allowed object is
     *     {@link TErrors }
     *     
     */
    public void setErrors(TErrors value) {
        this.errors = value;
    }

    /**
     * Gets the value of the cityDetails property.
     * 
     * @return
     *     possible object is
     *     {@link TCityDetails }
     *     
     */
    public TCityDetails getCityDetails() {
        return cityDetails;
    }

    /**
     * Sets the value of the cityDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link TCityDetails }
     *     
     */
    public void setCityDetails(TCityDetails value) {
        this.cityDetails = value;
    }

    /**
     * Gets the value of the iso property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isISO() {
        return iso;
    }

    /**
     * Sets the value of the iso property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setISO(Boolean value) {
        this.iso = value;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

}
