package com.umapped.service.offer;

import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.recommendation.RecommendedItemType;

import java.util.List;

/**
 * Created by wei on 2017-06-13.
 */
public class OfferFilter {
  private List<StayPeriod> stayPeriods;
  private UMappedCity location;
  private RecommendedItemType offerType;
  private int startPos = 0;
  private int limit = 0;
  private String keyword;

  public List<StayPeriod> getStayPeriods() {
    return stayPeriods;
  }

  public OfferFilter setStayPeriods(List<StayPeriod> stayPeriods) {
    this.stayPeriods = stayPeriods;
    return this;
  }

  public UMappedCity getLocation() {
    return location;
  }

  public OfferFilter setLocation(UMappedCity location) {
    this.location = location;
    return this;
  }

  public RecommendedItemType getOfferType() {
    return offerType;
  }

  public OfferFilter setOfferType(RecommendedItemType offerType) {
    this.offerType = offerType;
    return this;
  }

  public OfferFilter setStartPos(int pos) {
    this.startPos = pos;
    return this;
  }
  public OfferFilter setLimit(int limit) {
    this.limit = limit;
    return this;
  }
  public OfferFilter setKeyword(String k) {
    this.keyword = k;
    return this;
  }

  public int getStartPos() {
    return startPos;
  }

  public String getKeyword() {
    return keyword;
  }
}
