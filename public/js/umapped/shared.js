/**
 * ASYNC: Calls callback function with array of company objects
 * @param callback function accepting array of company information objects
 *        company object consists of several properties:
 *        - id - integer company id
 *        - legacyId - string company id
 *        - name - string name
 *        - user_link_type - type of the relationship of the current user to the company
 */
var getUserCompanies = function(callback) {
  var cmpies = $.jStorage.get(getJStorageKey('um_user_companies'), null);
  if (cmpies) {
    if (callback) {
      callback(cmpies);
    }
    return;
  }

  var request = new ApiMessage();
  request.header = new ApiRequestHeader();
  request.body = new CompanyBody('LIST');
  request['@type']  = 'COMPANY';

  ExecuteCommand(request, "/user/companies.json", function(data){
    var userCompanies = {};
    for(var idx = 0; idx < data.body.companies.length; idx++) {
      var cmpy = data.body.companies[idx];
      userCompanies[cmpy.id] = cmpy;
    }
    $.jStorage.set(getJStorageKey('um_user_companies'), userCompanies);
    callback(userCompanies);
  });
};

var clearUserCompanies = function() {
  $.jStorage.deleteKey(getJStorageKey('um_user_companies'));
};


/**
 * Util (ASYNC): Loading country list
 * @param callback function accepting list array of country objects;
 */
var utilGetCountries = function(callback) {
  var countries = $.jStorage.get('um_countries', null);
  if (countries != null) {
    callback(countries);
    return;
  }

  $.ajax({
    type: "GET",
    url: '/utils/countries.json',
    dataType: "json"})
      .always(function(data, textStatus, jqXHR) {
        if (textStatus == 'success') {
          var isSuccess = processResponseHeader(data.header);
          if (isSuccess) {
            var umCountries = [];
            $.each(data.body.countries, function(k,v){
              umCountries.push(v);
            });

            umCountries.sort(function(a,b){
              if (a.name < b.name)
                return -1;
              if (a.name > b.name)
                return 1;
              return 0;
            });

            $.jStorage.set('um_countries', umCountries);
            callback(umCountries);
          } else {
            $('#alertMsg').remove();
            $('#alertBox').append($('<div>').attr('id','alertMsg').text(data.header.msg + ' (' + data.header.resp_code + ')'));
            $('#alertBox').show();
          }
        } else if (textStatus == 'abort') {
          $('#alertMsg').text("Unknown System Error has occurred - " + textStatus);
          $('#alertBox').show();
        }
      });
};


/**
 * Util: (ASYNC): Get country name based on code (Alpha3 or Alpha2)
 * @param code 2-3 letter country code (as per ISO 3166)
 * @param function that will be called with the country object
 *        (country object consists of of 3 properties 'alpha3', 'alpha2', 'name'
 */
var utilGetCountryByCode = function(code, callback) {
  utilGetCountries(function(countries){
    if (code && code.length == 3) {
      for (var cIdx = 0; cIdx < countries.length; cIdx++) {
        if (countries[cIdx].alpha3 == code) {
          callback(countries[cIdx]);
          return;
        }
      }
    }

    if (code && code.length == 2) {
      for (var cIdx = 0; cIdx < countries.length; cIdx++) {
        if (countries[cIdx].alpha2 == code) {
          callback(countries[cIdx]);
          return;
        }
      }
    }
  });
};


/**
 * Util: (ASYNC): Get country based on name lookup
 * @param country name
 */
var utilGetCountryByName = function(name, callback) {
  utilGetCountries(function(countries){
    var trimmedName = name.trim();
    for (var cIdx = 0; cIdx < countries.length; cIdx++) {
      if (countries[cIdx].name == trimmedName) {
        callback(countries[cIdx]);
        return;
      }
    }
    //console.error("Failed to find country name :" + trimmedName);
  });
};

/**
 * Helper function to check if the table was initialized as DataTable
 * @param nTable
 * @returns {boolean}
 */
function isDataTable (nTable)
{
  var settings = $.fn.dataTableSettings;
  for ( var i=0, iLen=settings.length ; i<iLen ; i++ )
  {
    if ( settings[i].nTable == nTable )
    {
      return true;
    }
  }
  return false;
}
