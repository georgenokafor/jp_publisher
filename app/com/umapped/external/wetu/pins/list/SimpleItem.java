package com.umapped.external.wetu.pins.list;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by george on 2016-06-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class SimpleItem {
  public String id;
  public String name;
  public String description;
  public ArrayList<Image> images;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ArrayList<Image> getImages() {
    return images;
  }

  public void setImages(ArrayList<Image> images) {
    this.images = images;
  }

  @Override
  public String toString() {
    return "id='" + id + '\'' +
        "\n name='" + name + '\'' +
        "\n description='" + description + '\'' +
        "\n images=" + images;
  }
}
