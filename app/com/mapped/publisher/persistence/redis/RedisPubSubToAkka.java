package com.mapped.publisher.persistence.redis;

import actors.ActorsHelper;
import actors.SupervisorActor;
import com.mapped.publisher.utils.Log;
import redis.clients.jedis.JedisPubSub;

/**
 * Allows to associate Redis Subscriber in PubSub with an Akka Actor
 * <p>
 * Created by surge on 2016-02-01.
 */
public class RedisPubSubToAkka
    extends JedisPubSub {

  SupervisorActor.UmappedActor actor;

  public RedisPubSubToAkka(SupervisorActor.UmappedActor actor) {
    this.actor = actor;
  }

  @Override
  public void onMessage(String channel, String message) {
    Log.debug("RedisBridge: Message Received to " + channel + " Msg: " + message);
    RedisAkkaProtocol command = new RedisAkkaProtocol(channel, message);
    ActorsHelper.tell(actor, command);
  }

  @Override
  public void onSubscribe(String channel, int subscribedChannels) {
    Log.debug("RedisBridge: Subscribed to " + channel + " Total Subscribed: " + subscribedChannels);
  }

  @Override
  public void onUnsubscribe(String channel, int subscribedChannels) {
    Log.debug("RedisBridge: Unsubscribed from " + channel + " Total Subscribed: " + subscribedChannels);
  }

  @Override
  public void onPSubscribe(String pattern, int subscribedChannels) {
    Log.debug("RedisBridge: PSubscribed to " + pattern + " Total Subscribed: " + subscribedChannels);
  }

  @Override
  public void onPUnsubscribe(String pattern, int subscribedChannels) {
    Log.debug("RedisBridge: PUnsubscribed from " + pattern + " Total Subscribed: " + subscribedChannels);

  }

}
