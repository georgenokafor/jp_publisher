package models.publisher;

import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-21
 * Time: 8:54 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="user_audit")
public class UserAudit extends Model {
    @Id
    public String pk;

    @Constraints.Required
    public int activitytype;
    public Long createdtimestamp;
    public String createdby;

    @Version
    public int version;

    public static Model.Finder<String,UserAudit> find = new Finder(UserAudit.class);


    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public int getActivitytype() {
        return activitytype;
    }

    public void setActivitytype(int activitytype) {
        this.activitytype = activitytype;
    }

    public Long getCreatedtimestamp() {
        return createdtimestamp;
    }

    public void setCreatedtimestamp(Long createdtimestamp) {
        this.createdtimestamp = createdtimestamp;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
