package com.mapped.publisher.persistence.bookingapi;

import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by twong on 15-08-14.
 */
public class TestMgr {
  final static String SQL_INSERT_REC;


  static {
    SQL_INSERT_REC = "INSERT INTO TEST_JSON (PK, RAW) " +
                     "VALUES (?,cast(? as jsonb));";
  }

  public static boolean save(int pk, String  json) {
    boolean result = false;
    Connection conn = getConnection();
    if (conn == null) {
      return result;
    }

    try {
      PreparedStatement query = conn.prepareStatement(SQL_INSERT_REC);
      int paramCounter = 0;
      //SQL Order:
      //poi_id, consortium_id, cmpy_id, type_id, name, code, tags, src_id, src_reference, " +
      //country_code, loc_lat, loc_long, file_count, data, state, search_words, createdtimestamp," +
      //lastupdatedtimestamp, createdby, modifiedby, version)

      query.setInt(++paramCounter, pk);
      query.setString(++paramCounter, json);
      query.executeUpdate();
      result = true;
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      releaseConnection(conn);
    }
    return result;
  }

    /**
     * Using this specific routine to get connection.
     *
     * Future optimization can be to implement a naive connection pool for vendors.
     * @return Database connection
     */
  private static Connection getConnection() {

    Connection conn = null;

    try {
      conn = DBConnectionMgr.getConnection4Publisher();
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Failed to obtain database connection for POI operation:" + e.getMessage());
      e.printStackTrace();
    }

    return conn;
  }

  /**
   * Returning connection
   */
  private static void releaseConnection(Connection conn) {
    try {
      conn.close();
    } catch (SQLException e) {
      Log.log(LogLevel.ERROR, "Failed to release database connection:" + e.getMessage());
      e.printStackTrace();
    }
  }
}
