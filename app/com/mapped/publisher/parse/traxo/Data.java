package com.mapped.publisher.parse.traxo;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by twong on 10/06/17.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Data {
    public DataObject object;
}
