
package com.umapped.itinerary.analyze.supplier.musement.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "uuid",
    "city",
    "saves",
    "title",
    "relevance",
    "relevance_venue",
    "max_confirmation_time",
    "must_see",
    "last_chance",
    "top_seller",
    "print_voucher",
    "temporary",
    "description",
    "about",
    "meeting_point",
    "opening_hours",
    "duration",
    "validity",
    "has_price_info_on_date",
    "open",
    "ticket_not_included",
    "likely_to_sell_out",
    "special_offer",
    "exclusive",
    "daily",
    "languages",
    "group_size",
    "food",
    "services",
    "features",
    "highlights",
    "included",
    "not_included",
    "is_available_today",
    "is_available_tomorrow",
    "cover_image_url",
    "retail_price",
    "net_price",
    "discount",
    "categories",
    "reviews_number",
    "reviews_avg",
    "reviews_aggregated_info",
    "latitude",
    "longitude",
    "url",
    "flavours",
    "verticals",
    "giftable",
    "has_passenger_info",
    "has_extra_customer_data",
    "buy_multiplier"
})
public class Event {

    @JsonProperty("uuid")
    private String uuid;
    @JsonProperty("city")
    private City city;
    @JsonProperty("saves")
    private Integer saves;
    @JsonProperty("title")
    private String title;
    @JsonProperty("relevance")
    private Integer relevance;
    @JsonProperty("relevance_venue")
    private Integer relevanceVenue;
    @JsonProperty("max_confirmation_time")
    private String maxConfirmationTime;
    @JsonProperty("must_see")
    private Boolean mustSee;
    @JsonProperty("last_chance")
    private Boolean lastChance;
    @JsonProperty("top_seller")
    private Boolean topSeller;
    @JsonProperty("print_voucher")
    private Boolean printVoucher;
    @JsonProperty("temporary")
    private Boolean temporary;
    @JsonProperty("description")
    private String description;
    @JsonProperty("about")
    private String about;
    @JsonProperty("meeting_point")
    private String meetingPoint;
    @JsonProperty("opening_hours")
    private String openingHours;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("validity")
    private String validity;
    @JsonProperty("has_price_info_on_date")
    private Boolean hasPriceInfoOnDate;
    @JsonProperty("open")
    private Boolean open;
    @JsonProperty("ticket_not_included")
    private Boolean ticketNotIncluded;
    @JsonProperty("likely_to_sell_out")
    private Boolean likelyToSellOut;
    @JsonProperty("special_offer")
    private Boolean specialOffer;
    @JsonProperty("exclusive")
    private Boolean exclusive;
    @JsonProperty("daily")
    private Boolean daily;
    @JsonProperty("languages")
    private List<Language> languages = null;
    @JsonProperty("group_size")
    private List<GroupSize> groupSize = null;
    @JsonProperty("food")
    private List<Object> food = null;
    @JsonProperty("services")
    private List<Service> services = null;
    @JsonProperty("features")
    private List<Object> features = null;
    @JsonProperty("highlights")
    private List<Object> highlights = null;
    @JsonProperty("included")
    private List<String> included = null;
    @JsonProperty("not_included")
    private List<String> notIncluded = null;
    @JsonProperty("is_available_today")
    private Boolean isAvailableToday;
    @JsonProperty("is_available_tomorrow")
    private Boolean isAvailableTomorrow;
    @JsonProperty("cover_image_url")
    private String coverImageUrl;
    @JsonProperty("retail_price")
    private RetailPrice retailPrice;
    @JsonProperty("net_price")
    private NetPrice netPrice;
    @JsonProperty("discount")
    private Integer discount;
    @JsonProperty("categories")
    private List<Category> categories = null;
    @JsonProperty("reviews_number")
    private Integer reviewsNumber;
    @JsonProperty("reviews_avg")
    private Integer reviewsAvg;
    @JsonProperty("reviews_aggregated_info")
    private ReviewsAggregatedInfo reviewsAggregatedInfo;
    @JsonProperty("latitude")
    private Double latitude;
    @JsonProperty("longitude")
    private Double longitude;
    @JsonProperty("url")
    private String url;
    @JsonProperty("flavours")
    private List<Flavour> flavours = null;
    @JsonProperty("verticals")
    private List<Vertical> verticals = null;
    @JsonProperty("giftable")
    private Boolean giftable;
    @JsonProperty("has_passenger_info")
    private Boolean hasPassengerInfo;
    @JsonProperty("has_extra_customer_data")
    private Boolean hasExtraCustomerData;
    @JsonProperty("buy_multiplier")
    private Integer buyMultiplier;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("uuid")
    public String getUuid() {
        return uuid;
    }

    @JsonProperty("uuid")
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @JsonProperty("city")
    public City getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(City city) {
        this.city = city;
    }

    @JsonProperty("saves")
    public Integer getSaves() {
        return saves;
    }

    @JsonProperty("saves")
    public void setSaves(Integer saves) {
        this.saves = saves;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("relevance")
    public Integer getRelevance() {
        return relevance;
    }

    @JsonProperty("relevance")
    public void setRelevance(Integer relevance) {
        this.relevance = relevance;
    }

    @JsonProperty("relevance_venue")
    public Integer getRelevanceVenue() {
        return relevanceVenue;
    }

    @JsonProperty("relevance_venue")
    public void setRelevanceVenue(Integer relevanceVenue) {
        this.relevanceVenue = relevanceVenue;
    }

    @JsonProperty("max_confirmation_time")
    public String getMaxConfirmationTime() {
        return maxConfirmationTime;
    }

    @JsonProperty("max_confirmation_time")
    public void setMaxConfirmationTime(String maxConfirmationTime) {
        this.maxConfirmationTime = maxConfirmationTime;
    }

    @JsonProperty("must_see")
    public Boolean getMustSee() {
        return mustSee;
    }

    @JsonProperty("must_see")
    public void setMustSee(Boolean mustSee) {
        this.mustSee = mustSee;
    }

    @JsonProperty("last_chance")
    public Boolean getLastChance() {
        return lastChance;
    }

    @JsonProperty("last_chance")
    public void setLastChance(Boolean lastChance) {
        this.lastChance = lastChance;
    }

    @JsonProperty("top_seller")
    public Boolean getTopSeller() {
        return topSeller;
    }

    @JsonProperty("top_seller")
    public void setTopSeller(Boolean topSeller) {
        this.topSeller = topSeller;
    }

    @JsonProperty("print_voucher")
    public Boolean getPrintVoucher() {
        return printVoucher;
    }

    @JsonProperty("print_voucher")
    public void setPrintVoucher(Boolean printVoucher) {
        this.printVoucher = printVoucher;
    }

    @JsonProperty("temporary")
    public Boolean getTemporary() {
        return temporary;
    }

    @JsonProperty("temporary")
    public void setTemporary(Boolean temporary) {
        this.temporary = temporary;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("about")
    public String getAbout() {
        return about;
    }

    @JsonProperty("about")
    public void setAbout(String about) {
        this.about = about;
    }

    @JsonProperty("meeting_point")
    public String getMeetingPoint() {
        return meetingPoint;
    }

    @JsonProperty("meeting_point")
    public void setMeetingPoint(String meetingPoint) {
        this.meetingPoint = meetingPoint;
    }

    @JsonProperty("opening_hours")
    public String getOpeningHours() {
        return openingHours;
    }

    @JsonProperty("opening_hours")
    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    @JsonProperty("validity")
    public String getValidity() {
        return validity;
    }

    @JsonProperty("validity")
    public void setValidity(String validity) {
        this.validity = validity;
    }

    @JsonProperty("has_price_info_on_date")
    public Boolean getHasPriceInfoOnDate() {
        return hasPriceInfoOnDate;
    }

    @JsonProperty("has_price_info_on_date")
    public void setHasPriceInfoOnDate(Boolean hasPriceInfoOnDate) {
        this.hasPriceInfoOnDate = hasPriceInfoOnDate;
    }

    @JsonProperty("open")
    public Boolean getOpen() {
        return open;
    }

    @JsonProperty("open")
    public void setOpen(Boolean open) {
        this.open = open;
    }

    @JsonProperty("ticket_not_included")
    public Boolean getTicketNotIncluded() {
        return ticketNotIncluded;
    }

    @JsonProperty("ticket_not_included")
    public void setTicketNotIncluded(Boolean ticketNotIncluded) {
        this.ticketNotIncluded = ticketNotIncluded;
    }

    @JsonProperty("likely_to_sell_out")
    public Boolean getLikelyToSellOut() {
        return likelyToSellOut;
    }

    @JsonProperty("likely_to_sell_out")
    public void setLikelyToSellOut(Boolean likelyToSellOut) {
        this.likelyToSellOut = likelyToSellOut;
    }

    @JsonProperty("special_offer")
    public Boolean getSpecialOffer() {
        return specialOffer;
    }

    @JsonProperty("special_offer")
    public void setSpecialOffer(Boolean specialOffer) {
        this.specialOffer = specialOffer;
    }

    @JsonProperty("exclusive")
    public Boolean getExclusive() {
        return exclusive;
    }

    @JsonProperty("exclusive")
    public void setExclusive(Boolean exclusive) {
        this.exclusive = exclusive;
    }

    @JsonProperty("daily")
    public Boolean getDaily() {
        return daily;
    }

    @JsonProperty("daily")
    public void setDaily(Boolean daily) {
        this.daily = daily;
    }

    @JsonProperty("languages")
    public List<Language> getLanguages() {
        return languages;
    }

    @JsonProperty("languages")
    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    @JsonProperty("group_size")
    public List<GroupSize> getGroupSize() {
        return groupSize;
    }

    @JsonProperty("group_size")
    public void setGroupSize(List<GroupSize> groupSize) {
        this.groupSize = groupSize;
    }

    @JsonProperty("food")
    public List<Object> getFood() {
        return food;
    }

    @JsonProperty("food")
    public void setFood(List<Object> food) {
        this.food = food;
    }

    @JsonProperty("services")
    public List<Service> getServices() {
        return services;
    }

    @JsonProperty("services")
    public void setServices(List<Service> services) {
        this.services = services;
    }

    @JsonProperty("features")
    public List<Object> getFeatures() {
        return features;
    }

    @JsonProperty("features")
    public void setFeatures(List<Object> features) {
        this.features = features;
    }

    @JsonProperty("highlights")
    public List<Object> getHighlights() {
        return highlights;
    }

    @JsonProperty("highlights")
    public void setHighlights(List<Object> highlights) {
        this.highlights = highlights;
    }

    @JsonProperty("included")
    public List<String> getIncluded() {
        return included;
    }

    @JsonProperty("included")
    public void setIncluded(List<String> included) {
        this.included = included;
    }

    @JsonProperty("not_included")
    public List<String> getNotIncluded() {
        return notIncluded;
    }

    @JsonProperty("not_included")
    public void setNotIncluded(List<String> notIncluded) {
        this.notIncluded = notIncluded;
    }

    @JsonProperty("is_available_today")
    public Boolean getIsAvailableToday() {
        return isAvailableToday;
    }

    @JsonProperty("is_available_today")
    public void setIsAvailableToday(Boolean isAvailableToday) {
        this.isAvailableToday = isAvailableToday;
    }

    @JsonProperty("is_available_tomorrow")
    public Boolean getIsAvailableTomorrow() {
        return isAvailableTomorrow;
    }

    @JsonProperty("is_available_tomorrow")
    public void setIsAvailableTomorrow(Boolean isAvailableTomorrow) {
        this.isAvailableTomorrow = isAvailableTomorrow;
    }

    @JsonProperty("cover_image_url")
    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    @JsonProperty("cover_image_url")
    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    @JsonProperty("retail_price")
    public RetailPrice getRetailPrice() {
        return retailPrice;
    }

    @JsonProperty("retail_price")
    public void setRetailPrice(RetailPrice retailPrice) {
        this.retailPrice = retailPrice;
    }

    @JsonProperty("net_price")
    public NetPrice getNetPrice() {
        return netPrice;
    }

    @JsonProperty("net_price")
    public void setNetPrice(NetPrice netPrice) {
        this.netPrice = netPrice;
    }

    @JsonProperty("discount")
    public Integer getDiscount() {
        return discount;
    }

    @JsonProperty("discount")
    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    @JsonProperty("categories")
    public List<Category> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @JsonProperty("reviews_number")
    public Integer getReviewsNumber() {
        return reviewsNumber;
    }

    @JsonProperty("reviews_number")
    public void setReviewsNumber(Integer reviewsNumber) {
        this.reviewsNumber = reviewsNumber;
    }

    @JsonProperty("reviews_avg")
    public Integer getReviewsAvg() {
        return reviewsAvg;
    }

    @JsonProperty("reviews_avg")
    public void setReviewsAvg(Integer reviewsAvg) {
        this.reviewsAvg = reviewsAvg;
    }

    @JsonProperty("reviews_aggregated_info")
    public ReviewsAggregatedInfo getReviewsAggregatedInfo() {
        return reviewsAggregatedInfo;
    }

    @JsonProperty("reviews_aggregated_info")
    public void setReviewsAggregatedInfo(ReviewsAggregatedInfo reviewsAggregatedInfo) {
        this.reviewsAggregatedInfo = reviewsAggregatedInfo;
    }

    @JsonProperty("latitude")
    public Double getLatitude() {
        return latitude;
    }

    @JsonProperty("latitude")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("longitude")
    public Double getLongitude() {
        return longitude;
    }

    @JsonProperty("longitude")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("flavours")
    public List<Flavour> getFlavours() {
        return flavours;
    }

    @JsonProperty("flavours")
    public void setFlavours(List<Flavour> flavours) {
        this.flavours = flavours;
    }

    @JsonProperty("verticals")
    public List<Vertical> getVerticals() {
        return verticals;
    }

    @JsonProperty("verticals")
    public void setVerticals(List<Vertical> verticals) {
        this.verticals = verticals;
    }

    @JsonProperty("giftable")
    public Boolean getGiftable() {
        return giftable;
    }

    @JsonProperty("giftable")
    public void setGiftable(Boolean giftable) {
        this.giftable = giftable;
    }

    @JsonProperty("has_passenger_info")
    public Boolean getHasPassengerInfo() {
        return hasPassengerInfo;
    }

    @JsonProperty("has_passenger_info")
    public void setHasPassengerInfo(Boolean hasPassengerInfo) {
        this.hasPassengerInfo = hasPassengerInfo;
    }

    @JsonProperty("has_extra_customer_data")
    public Boolean getHasExtraCustomerData() {
        return hasExtraCustomerData;
    }

    @JsonProperty("has_extra_customer_data")
    public void setHasExtraCustomerData(Boolean hasExtraCustomerData) {
        this.hasExtraCustomerData = hasExtraCustomerData;
    }

    @JsonProperty("buy_multiplier")
    public Integer getBuyMultiplier() {
        return buyMultiplier;
    }

    @JsonProperty("buy_multiplier")
    public void setBuyMultiplier(Integer buyMultiplier) {
        this.buyMultiplier = buyMultiplier;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
