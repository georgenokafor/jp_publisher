package models.publisher;

import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.persistence.IUmappedModel;
import com.mapped.publisher.persistence.IUmappedModelWithImage;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * Table to link images to mobilized pages
 * Created by surge on 2016-11-07.
 */
@Entity
@Table(name = "library_page_image")
public class LibraryPageImage
    extends UmappedModelWithImage
    implements IUmappedModel<Long, LibraryPageImage> {

  public static Model.Finder<Long, LibraryPageImage> find = new Finder(LibraryPageImage.class);

  @Id
  Long pk;
  String pageId;
  @ManyToOne
  @JoinColumn(name = "image_pk", nullable = false, referencedColumnName = "pk")
  FileImage fileImage;

  public static LibraryPageImage build(String pageId, FileImage image) {
    LibraryPageImage lpi = new LibraryPageImage();
    lpi.setPk(DBConnectionMgr.getUniqueLongId());
    lpi.setFileImage(image);
    lpi.setPageId(pageId);
    return lpi;
  }

  public static List<LibraryPageImage> byPageId(final String pageId) {
    return find.where().eq("page_id", pageId).findList();
  }

  @Override
  public LibraryPageImage getByPk(Long pk) {
    return find.byId(pk);
  }

  @Override
  public Long getPk() {
    return pk;
  }

  public LibraryPageImage setPk(Long pk) {
    this.pk = pk;
    return this;
  }

  public String getPageId() {
    return pageId;
  }

  public LibraryPageImage setPageId(String pageId) {
    this.pageId = pageId;
    return this;
  }

  @Override
  public FileImage getFileImage() {
    return fileImage;
  }

  public IUmappedModelWithImage setFileImage(FileImage image) {
    fileImage = image;
    return this;
  }

  @Override
  public String getPkAsString() {
    return pk.toString();
  }

  @Override
  public Long getCreatedByPk() {
    return Account.AID_PUBLISHER;
  }

  @Override
  public Long getModifiedByPk() {
    return Account.AID_PUBLISHER;
  }

  @Override
  public String getCreatedByLegacy() {
    return Account.LEGACY_PUBLISHER_USERID;
  }

  @Override
  public String getModifiedByLegacy() {
    return Account.LEGACY_PUBLISHER_USERID;
  }

  @Override
  public Long getCreatedEpoch() {
    return 0l;
  }

  @Override
  public Timestamp getCreatedTs() {
    return null;
  }

  @Override
  public Long getModifiedEpoch() {
    return 0l;
  }

  @Override
  public Timestamp getModifiedTs() {
    return null;
  }
}
