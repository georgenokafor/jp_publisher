package models.publisher;

import com.mapped.publisher.parse.schemaorg.ImageObject;
import com.mapped.publisher.persistence.IUmappedBaseModel;
import com.mapped.publisher.persistence.IUmappedModelWithImage;
import com.mapped.publisher.utils.Utils;

import javax.persistence.MappedSuperclass;

/**
 * Created by surge on 2016-11-03.
 */
@MappedSuperclass
public abstract class UmappedModelWithImage
    extends UmappedBaseModel
    implements IUmappedModelWithImage, IUmappedBaseModel {

  public Long getImageFileId() {
    return (getFileImage() != null) ? getFileImage().getFile().getPk() : null;
  }

  public String getImageUrl() {
    return getFileImage().getUrl();
  }

  public ImageObject buildImageObject(){
    ImageObject io = new ImageObject();
    FileImage fi = getFileImage();
    if (fi != null) {
      io.name = fi.getFile().getFilename();
      io.caption = fi.getFile().getDescription();
      io.url = fi.getUrl();
    }
    return io;
  }

  public String getMobileImageFilename() {
    FileImage fi = getFileImage();
    if (fi != null) {
      FileInfo f = fi.getFile();
      return Utils.base64encode(f.getPk()) + '.' + f.getFiletype().getExtension();
    }
    return null;
  }
}
