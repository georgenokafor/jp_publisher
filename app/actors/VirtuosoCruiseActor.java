package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.google.common.collect.ImmutableMap;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.parse.virtuoso.cruise.CruiseParser;
import com.mapped.publisher.parse.virtuoso.cruise.CruiseVO;
import com.mapped.publisher.utils.Log;
import org.apache.commons.lang3.time.StopWatch;

/**
 * Created by twong on 15-05-19.
 */
public class VirtuosoCruiseActor
    extends UntypedActor {

  public interface Factory {
    public Actor create();
  }

  /**
   * Akka Actor Properties
   */
  public final static Props props = Props.create(VirtuosoCruiseActor.class);

  public void onReceive(Object message)
      throws Exception {

    if (message instanceof ImmutableMap) {
      StopWatch sw = new StopWatch();
      sw.start();
      int savedCount = 0;

      Long id = DBConnectionMgr.getUniqueLongId();
      ImmutableMap<String, String> urls = (ImmutableMap<String, String>) message;
      for (String key : urls.keySet()) {
        try {
          CruiseVO cruiseVO = CruiseParser.getCruise(urls.get(key));
          long ret = cruiseVO.save();
          if (ret > 0) {
            savedCount++;
          }
        }
        catch (Exception e) {
          Log.err("VirtuosoCruiseActor - cannot download: " + urls.get(key), e);
        }
      }

      sw.stop();
      Log.debug("Cruise Download - actor id:" + id + " processed: " + urls.size() + " saved:" + savedCount + " in " + sw
          .getTime() + " for an avg " + ((sw.getTime() / urls.size()) / 1000) + " secs per url");
    }
  }
}
