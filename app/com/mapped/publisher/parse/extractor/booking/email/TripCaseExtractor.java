package com.mapped.publisher.parse.extractor.booking.email;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.EmailBodyExtractor;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.parse.schemaorg.Activity;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by twong on 2015-02-04.
 */
public class TripCaseExtractor extends EmailBodyExtractor {
  private static final char NBSP =  '\u00a0';

  protected void initImpl(Map<String, String> params) {

  }

  protected TripVO extractDataImpl(String htmlInput) throws Exception {
    if (htmlInput != null) {
      Document emailDoc = Jsoup.parse(htmlInput.replaceAll("=\n","").replaceAll("3D",""));
      Elements anchors = emailDoc.select("a");
      if (anchors != null && anchors.size() > 0) {
        for (int i =0; i < anchors.size(); i++){
          String href = anchors.get(i).attr("href");

          if (href.contains("tcprintitin_unreg")) {
            Document printDoc = Jsoup.connect(href).timeout(5*1000).followRedirects(true).get();
            if (printDoc != null) {
              Elements links = printDoc.select("a");
              if (links != null && links.size() > 0) {
                String printhref = links.get(0).attr("href");
                if (printhref.contains("tcprintitin")) {
                  Document printDoc1 = Jsoup.connect(printhref).timeout(5*1000).followRedirects(true).get();
                  if (printDoc1 != null && printDoc1.hasText() && printDoc1.title().contains("Print Your Itinerary")) {
                    return getInfo(printDoc1, "");
                  }
                }
              }
            }
          } else if (href.contains("docsWithItin.html")) {
            Document doc = Jsoup.connect(href).followRedirects(true).get();
            if (doc != null && doc.title().contains("TripCase - Document List")) {
              Element link = doc.getElementById("printItinerary");
              if (link != null) {
                Elements links = link.select("a");
                if (links != null && links.size() > 0) {
                  String printhref = links.get(0).attr("href");
                  Document printDoc = Jsoup.connect(printhref).timeout(5*1000).followRedirects(true).get();
                  if (printDoc != null && printDoc.hasText() && printDoc.title().contains("Print Your Itinerary")) {
                    return getInfo(printDoc, "");
                  }
                }
              }
            }
          }
        }
      }
    }
    return null;
  }


  @Override
  public ArrayList<ParseError> getParseErrors()
  {
    return null;
  }

  private static String[] dateTimeFormat = {"MMM d yyyy HH:mm", "MMM dd yyyy HH:mm","MMM d yyyy h:mma", "MMM dd yyyy h:mma","MMM d yyyy hh:mma", "MMM dd yyyy hh:mma", "d MMM yyyy h:mma", "dd MMM yyyy h:mma","d MMM yyyy hh:mma", "dd MMM yyyy hh:mma", "EEEE dd MMM yyyy h:mma", "EEEE MMM dd yyyy h:mma", "M/dd/yyyy", "M/dd/yyyy h:mm:ss a", "dd MMM yyyy", "dd MMM yyyy ", "E dd MMM yyyy h:mma",  "E dd MMM yyyy", "E dd MMM yyyy ", "E dd MMMMM yyyy HH:mm" , "dd MMM yyyy HH:mm a", "E dd MMM yyyy HH:mm" , "dd MMMMM yyyy HH:mm", "E, MMMMM dd, yyyy HH:mm", "E, MMMMM dd, yyyy", "E, MMMMM dd, yyyy hh:mm a", "EEE dd MMM yyyy h:mm a", "EEE dd MMM yyyy 0h:mm a", "MMM dd yyyy h:mma"} ;

  private static DateUtils du = new DateUtils();



  public static TripVO getInfo(Document doc, String locator) {
    if (locator == null || locator.isEmpty()) {
      String html = doc.toString();
      if (html.contains("RESERVATION CODE")) {
        String s1 = html.substring(html.indexOf("RESERVATION CODE"));
        if (s1.contains("<span")) {
          String s2 = s1.substring(s1.indexOf("<span"));
          String airlineReservationCode = s2.substring(s2.indexOf(">") + 1, s2.indexOf("</span>"))
                                            .replaceAll("&nbsp;", "");
          if (airlineReservationCode != null && airlineReservationCode.trim().length() == 6) {
            locator = airlineReservationCode;
          }
        }
      }

    }

    TripVO trip = new TripVO();
    List <FlightVO> flights = parseFlights(doc, locator);
    for (FlightVO vo: flights) {
      trip.addFlight(vo);
    }
    List <HotelVO> hotels = parseHotels(doc, locator);
    for (HotelVO vo: hotels) {
      trip.addHotel(vo);
    }
    List <CruiseVO> cruises = parseCruises(doc, locator);
    for (CruiseVO vo: cruises) {
      trip.addCruise(vo);
    }

    List <TransportVO> cars = parseCars(doc, locator);
    for (TransportVO vo: cars) {
      trip.addTransport(vo);
    }

    List <TransportVO> trains = parseRails(doc, locator);
    for (TransportVO vo: trains) {
      trip.addTransport(vo);
    }

    List <ActivityVO> activities = parseTour(doc, locator);
    for (ActivityVO vo: activities) {
      trip.addActivity(vo);
    }

    List <NoteVO> notes = parseOth(doc, locator);
    for (NoteVO vo: notes) {
      trip.addNoteVO(vo);
    }

    List<Element> itineraryComments = doc.getElementsByAttributeValue("class", "itineraryComment");
    if (itineraryComments != null && itineraryComments.size() >0) {
      for (Element itineraryComment: itineraryComments) {
        List<Element> titles = itineraryComment.getElementsByAttributeValue("class", "title");
        List<Element> messages = itineraryComment.getElementsByAttributeValue("class", "message");
        if (titles != null && titles.size() == 1 && messages != null && messages.size() == 1) {
          NoteVO noteVO = new NoteVO();
          noteVO.setName(titles.get(0).text());
          noteVO.setNote(getText(messages.get(0)));
          if (noteVO.getName() != null && !noteVO.getName().isEmpty() && noteVO.getNote() != null && !noteVO.getNote().isEmpty()) {
            noteVO.setNote(noteVO.getNote().replace("\n","<br/>"));
            trip.addNoteVO(noteVO);
          }
        }

       }
    }

    trip.setImportSrc(BookingSrc.ImportSrc.SABRE_TRIPCASE);
    trip.setImportSrcId(locator);
    trip.setImportTs(System.currentTimeMillis());

    return trip;
  }

  private static List <HotelVO> parseHotels (Document doc, String locator) {
    List<HotelVO> hotels = new ArrayList<>();
    int i = 1;
    DateTime dt = new DateTime();
    while (true) {
      Element hotelSegmentElm = doc.getElementById("segmentHOTEL_" + i);
      if (hotelSegmentElm != null && hotelSegmentElm.hasText()) {
        HotelVO hotel = new HotelVO();
        Element hotelNameElm = doc.getElementById("HotelNameHOTEL_" + i);
        Element hotelCheckinElm = doc.getElementById("CheckInDateHOTEL_" + i);
        Element hotelCheckoutElm = doc.getElementById("CheckOutDateHOTEL_" + i);
        Element hotelPhoneElm = doc.getElementById("HotelPhoneHOTEL_" + i);
        Element hotelFaxElm = doc.getElementById("HotelFaxHOTEL_" + i);
        Element hotelAddrElm = doc.getElementById("HotelAddressHOTEL_" + i);
        Element hotelConfirmElm = doc.getElementById("ConfirmationNumberHOTEL_" + i);

        Element hotelRoomTypeElm = doc.getElementById("RoomTypeHOTEL_" + i);
        Element hotelRoomsElm = doc.getElementById("NoOfRoomsHOTEL_" + i);
        Element hotelGuestsElm = doc.getElementById("NoOfGuestsHOTEL_" + i);
        Element hotelCancelElm = doc.getElementById("CancellationInformationHOTEL_" + i);
        Element hotelFactsElm = doc.getElementById("HotelFactsHOTEL_" + i);
        Element hotelNotesElm = doc.getElementById("NotesHOTEL_" + i);
        Element hotelBasicRateType = doc.getElementById("BasicRateHOTEL_" + i);
        Elements hotelApproxPriceElm = hotelSegmentElm.getElementsByClass("approx-total-price");

        String hotelApproxPrice = null;
        if (hotelApproxPriceElm != null && !hotelApproxPriceElm.isEmpty()) {
          hotelApproxPrice = getText(hotelApproxPriceElm.first());
        } else {
          //bug in sabre - they have 2 classes
          Elements entries = hotelSegmentElm.getElementsByClass("entry");
          if (entries != null) {
            for (Element entry : entries) {
              if (entry.hasText() && entry.text().contains("Approx. Total Price")) {
                hotelApproxPrice = getText(entry);
                break;
              }
            }
          }
        }
        String hotelCheckinDate ="";
        String hotelCheckoutDate = "";

        if (hotelNameElm != null) {
          hotel.setHotelName(hotelNameElm.text());
        }

        if (hotelCheckinElm != null) {
          hotelCheckinDate = hotelCheckinElm.text();
        }

        if (hotelCheckoutElm != null) {
          hotelCheckoutDate = hotelCheckoutElm.text();
        }
        if (hotelConfirmElm != null) {
          hotel.setConfirmation(hotelConfirmElm.text().trim());
        }

        StringBuilder sb = new StringBuilder();

        if (hotelPhoneElm != null) {
          sb.append("Phone: "); sb.append(hotelPhoneElm.text()); sb.append("\n");
        }

        if (hotelFaxElm != null) {
          sb.append("Fax: "); sb.append(hotelFaxElm.text()); sb.append("\n");
        }

        if (hotelAddrElm != null) {
          sb.append("Address: \n"); sb.append(getText(hotelAddrElm)); sb.append("\n\n");
        }

        if (hotelRoomTypeElm != null) {
          sb.append("Room Details: \n"); sb.append(getText(hotelRoomTypeElm)); sb.append("\n\n");
        }

        if (hotelRoomsElm != null) {
          sb.append("No Of Rooms: "); sb.append(hotelRoomsElm.text()); sb.append("\n");
        }

        if (hotelGuestsElm != null) {
          sb.append("No Of Guests: "); sb.append(hotelGuestsElm.text()); sb.append("\n");
        }


        if (hotelBasicRateType != null) {
          sb.append("Rate: "); sb.append(getText(hotelBasicRateType));sb.append("\n");
          if (hotelApproxPrice != null && !hotelApproxPrice.isEmpty()) {
            sb.append(hotelApproxPrice); sb.append("\n");
          }
          sb.append("\n");
        }
        if (hotelCancelElm != null) {
          sb.append("Cancellation Policy: \n"); sb.append(getText(hotelCancelElm)); sb.append("\n\n");
        }


        if (hotelFactsElm != null) {
          sb.append("About: \n"); sb.append(getText(hotelFactsElm)); sb.append("\n\n");
        }

        if (hotelNotesElm != null) {
          sb.append("Notes: \n"); sb.append(getText(hotelNotesElm)); sb.append("\n\n");
        }
        hotel.setNote(sb.toString());

        hotel.setCheckin(getTimestamp(hotelCheckinDate + " " + dt.getYear(), "12:00AM"));
        hotel.setCheckout(getTimestamp(hotelCheckoutDate + " " + dt.getYear(), "12:00AM"));

        if (hotel.isValid()) {
          if (hotel.getConfirmation() == null || hotel.getConfirmation().isEmpty()) {
            hotel.setConfirmation(locator);
          }
          hotels.add(hotel);
        }

        i++;
      }
      else {
        break;
      }
    }
    return hotels;
  }

  private static List <CruiseVO> parseCruises (Document doc, String locator) {
    List<CruiseVO> cruises = new ArrayList<>();
    int i = 1;
    DateTime dt = new DateTime();
    while (true) {
      Element cruiseSegmentElm = doc.getElementById("segmentSEA_" + i);
      if (cruiseSegmentElm != null && cruiseSegmentElm.hasText()) {
        CruiseVO cruise = new CruiseVO();

        Elements h2 = cruiseSegmentElm.select("h2");
        if (h2 != null && h2.size() > 1) {
          cruise.setCompanyName(h2.get(0).text().trim());
        }
        Element cruiseNameElm = doc.getElementById("CruiseNameSEA_" + i);
        Element cruiseConfirmationElm = doc.getElementById("ConfirmationNumberSEA_" + i);
        Element boardDateElm = doc.getElementById("PickUpDateSEA_" + i);
        Element boardTimeElm = doc.getElementById("DepartingAtTimeValueSEA_" + i);
        Element arriveDateElm = doc.getElementById("DropOffDateSEA_" + i);
        Element arriveTimeElm = doc.getElementById("ArrivingAtTimeValueSEA_" + i);
        Element departPortElm = doc.getElementById("DepartureCityNameSEA_" + i);
        Element arrivePortElm = doc.getElementById("ArrivalCityNameSEA_" + i);
        Element cabinElm = doc.getElementById("CabinSEA_" + i);
        Element factsElm = doc.getElementById("FactsSEA_" + i);
        Element notesElm = doc.getElementById("NotesSEA_" + i);

        String cruiseBoardDate  = "";
        String cruiseBoardTime  = "";
        String cruiseArriveDate  = "";
        String cruiseArriveTime  = "";

        if(cruiseNameElm != null)
          cruise.setName(cruiseNameElm.text());

        if(cruiseConfirmationElm != null)
          cruise.setConfirmationNo(cruiseConfirmationElm.text());

        if(boardDateElm != null)
          cruiseBoardDate = boardDateElm.text();

        if(boardTimeElm != null)
          cruiseBoardTime = boardTimeElm.text();

        if(arriveDateElm != null)
          cruiseArriveDate = arriveDateElm.text();

        if(arriveTimeElm != null)
          cruiseArriveTime = arriveTimeElm.text();

        if(departPortElm != null)
          cruise.setPortDepart(departPortElm.text());

        if(arrivePortElm != null)
          cruise.setPortArrive(arrivePortElm.text());

        if(cabinElm != null)
          cruise.setCabinNumber(cabinElm.text());

        cruise.setTimestampDepart(getTimestamp(cruiseBoardDate + " " + dt.getYear(), cruiseBoardTime));
        cruise.setTimestampArrive(getTimestamp(cruiseArriveDate + " " + dt.getYear(), cruiseArriveTime));

        StringBuffer sb = new StringBuffer();
        if (factsElm != null) {
          sb.append(getText(factsElm));
        }

        if (notesElm != null) {
          if (sb.toString().length() > 0) {
            sb.append("\n");
          }
          sb.append(getText(notesElm));
        }
        cruise.setNote(sb.toString());

        if (cruise.isValid()) {
          if (cruise.getConfirmationNo() == null || cruise.getConfirmationNo().isEmpty()) {
            cruise.setConfirmationNo(locator);
          }
          cruises.add(cruise);
        }
        i++;
      } else {
        break;
      }

    }


    return cruises;
  }

  private static  List <FlightVO> parseFlights(Document doc, String locator) {
    List<FlightVO> flights = new ArrayList<>();
    String html = doc.toString();
    Map<String, String> reservationCodes = new HashMap<>();

    if (html.contains("AIRLINE RESERVATION CODE")) {
      String s1 = html.substring(html.indexOf("AIRLINE RESERVATION CODE"));
      if (s1.contains("<span>")) {

        String s2 = s1.substring(s1.indexOf("<span>"));
        if (s2.contains("</td>")) {
          s2 = s2.substring(0, s2.indexOf("</td>"));
          String[] tokens1 = s2.split("<span>");
          for (String token1: tokens1) {
            if (token1.contains("</span>")) {
              String airlineReservationCode = token1.substring(0, token1.indexOf("</span>"))
                      .replaceAll("&nbsp;", "");
              if (airlineReservationCode != null) {
                if (airlineReservationCode.contains("(") && airlineReservationCode.contains(")")) {
                  String[] tokens = airlineReservationCode.split("\\)");
                  String previousReserve = null;
                  for (String token : tokens) {
                    token = token.replace(",", "").trim();
                    if (token.startsWith("(") && previousReserve != null) {
                      String reserve = previousReserve;
                      String airline = token.substring(token.indexOf("(") + 1);
                      reservationCodes.put(airline, reserve);

                    } else {
                      String reserve = token.substring(0, token.indexOf("(")).trim();
                      String airline = token.substring(token.indexOf("(") + 1);
                      previousReserve = reserve;
                      reservationCodes.put(airline, reserve);
                    }
                  }

                }
              }
            }
          }
        }



      }
    }

    int i = 1;
    DateTime dt = new DateTime();

    while (true) {
      //get all the flights
      Element flightSegmentElm = doc.getElementById("segmentAIR_"+i);
      if (flightSegmentElm != null && flightSegmentElm.hasText()) {
        Element departureTimeElm = doc.getElementById("departing-time-value-AIR_" + i);
        Element departureDateElm = doc.getElementById("departure-date-AIR_" + i);
        Element departureAirportCodeElm = doc.getElementById("departure-city-code-AIR_" + i);
        Element departureCityElm = doc.getElementById("departure-city-name-AIR_" + i);
        Element departingTerminalElm = doc.getElementById("departing-terminal-value-AIR_" + i);
        Element arrivalDateElm = doc.getElementById("arriving-date-value-AIR_" + i);
        Element arrivalTimeElm = doc.getElementById("arriving-time-value-AIR_" + i);
        Element arrivalAirportCodeElm = doc.getElementById("arrival-city-code-AIR_" + i);
        Element arrivalCityElm = doc.getElementById("arrival-city-name-AIR_" + i);
        Element arrivalTerminalElm = doc.getElementById("arriving-terminal-label-AIR_" + i);
        Element flightNumberElm = doc.getElementById("flight-number-AIR_" + i);
        Element airlineNameElm = doc.getElementById("operating-airline-name-AIR_" + i);
        Element marketingAirlineNameElm = doc.getElementById("marketing-airline-name-AIR_" + i);
        Element aircraftTypeElm = doc.getElementById("airline-aircraft-value-AIR_" + i);
        Element distanceElm = doc.getElementById("milage-value-AIR_" + i);
        Element stopsElm = doc.getElementById("stops-value-AIR_" + i);
        Element operatingAirElm = doc.getElementById("operating-airline-name-value-AIR_" + i);
        Element flightClassElm = doc.getElementById("flight-class-value-AIR_" + i);
        Element mealElm = doc.getElementById("meals-value-AIR_" + i);
        Element durationElm = doc.getElementById("flight-duration-value-AIR_" + i);

        String departureDate = "";
        String arrivalDate = "";
        String departureTime = "";
        String arrivalTime = "";

        FlightVO flight = new FlightVO();
        flight.setDepartureAirport(new AirportVO());
        flight.setArrivalAirport(new AirportVO());
        flight.setPassengers(new ArrayList<PassengerVO>());

        if (departureDateElm != null) {
          departureDate = departureDateElm.text() + " " + dt.getYear();
        }

        if (arrivalDateElm != null) {
          arrivalDate = arrivalDateElm.text() + " " + dt.getYear();
        } else if (departureDateElm != null)
          arrivalDate = departureDateElm.text() + " " + dt.getYear();

        if (departureTimeElm != null) {
          String[] fullTime = departureTimeElm.text().split(" ");
          departureTime = fullTime[0];

          if (fullTime.length > 1) {
            StringBuilder sb = new StringBuilder();
            for (int j = 1; j < fullTime.length; j++) {
              if(fullTime[j].contains("Tue"))
                fullTime[j] = fullTime[j].replace("Tue","Tues");
              else if (fullTime[j].contains("Thu"))
                fullTime[j] = fullTime[j].replace("Thu","Thurs");

              if (j == fullTime.length - 1)
                sb.append(fullTime[j]);
              else
                sb.append(fullTime[j] + " ");
            }
            String newDepartureDate = sb.toString();
            newDepartureDate = newDepartureDate.replace("(", "");
            newDepartureDate = newDepartureDate.replace(")", "");
            newDepartureDate = newDepartureDate.replace(",", "day");
            departureDate = newDepartureDate + " " + dt.getYear();
          }
        }
        flight.setDepatureTime(getTimestamp(departureDate, departureTime));

        if (arrivalTimeElm != null) {
          String[] fullTime = arrivalTimeElm.text().split(" ");
          arrivalTime = fullTime[0];

          if (fullTime.length > 1) {
            StringBuilder sb = new StringBuilder();
            for (int j = 1; j < fullTime.length; j++) {
              if(fullTime[j].equals("Tue"))
                fullTime[j] = "Tues";
              else if (fullTime[j].equals("Thu"))
                fullTime[j] = "Thurs";

              if (j == fullTime.length - 1)
                sb.append(fullTime[j]);
              else
                sb.append(fullTime[j] + " ");
            }
            String newArrivalDate = sb.toString();
            newArrivalDate = newArrivalDate.replace("(", "");
            newArrivalDate = newArrivalDate.replace(")", "");
            newArrivalDate = newArrivalDate.replace(",", "day");
            arrivalDate = newArrivalDate + " " + dt.getYear();
          }
        }
        flight.setArrivalTime(getTimestamp(arrivalDate, arrivalTime));


        if (departureAirportCodeElm != null)
          flight.getDepartureAirport().setCode(departureAirportCodeElm.text().trim());

        if (departureCityElm != null)
          flight.getDepartureAirport().setCity(departureCityElm.text().trim());

        if (departingTerminalElm != null)
          flight.getDepartureAirport().setTerminal(departingTerminalElm.text().trim());

        if (arrivalAirportCodeElm != null)
          flight.getArrivalAirport().setCode(arrivalAirportCodeElm.text().trim());

        if (arrivalCityElm != null)
          flight.getArrivalAirport().setCity(arrivalCityElm.text().trim());

        if (arrivalTerminalElm != null)
          flight.getArrivalAirport().setTerminal(arrivalTerminalElm.text().trim());

        if (flightNumberElm != null) {
          String flightNumber = flightNumberElm.text().trim().replaceAll(" ","");
          if (flightNumber != null) {
            flightNumber = flightNumber.replace(NBSP, ' ');
          }
          if(flightNumber.length() > 3) {
            flight.setCode(flightNumber.substring(0,2).trim());
            flight.setNumber(flightNumber.substring(2).trim());
            if (reservationCodes.containsKey(flight.getCode())) {
              flight.setReservationNumber(reservationCodes.get(flight.getCode()));
            }
          }
        }

        if (airlineNameElm != null)
          flight.setName(airlineNameElm.text());

        if (marketingAirlineNameElm != null)
          flight.setName(marketingAirlineNameElm.text());

        StringBuilder notes = new StringBuilder();
        if (operatingAirElm != null && operatingAirElm.hasText()) {
          notes.append("This flight is operated By: "); notes.append(operatingAirElm.text());notes.append("\n\n");
        }


        if (aircraftTypeElm != null && aircraftTypeElm.hasText()) {
          notes.append("Aircraft: "); notes.append(aircraftTypeElm.text()); notes.append("\n");
        }

        if (distanceElm != null && distanceElm.hasText()) {
          notes.append("Distance: "); notes.append(distanceElm.text()); notes.append("\n");
        }

        if (stopsElm != null && stopsElm.hasText()) {
          notes.append("Stops: "); notes.append(stopsElm.text()); notes.append("\n");
        }

        if (durationElm != null && durationElm.hasText()) {
          notes.append("Duration: "); notes.append(durationElm.text()); notes.append("\n");
        }

        //process all passengers for this flights
        Element passengerTable = doc.getElementById("segment-details_AIR_" + i);
        if (passengerTable != null) {
          //get the headers do we know the column mapping
          int name = -1; int seat = -1; int sclass= -1; int ticket = -1; int meal = -1; int freqflier = -1; int status = -1;
          Elements rows = passengerTable.select("tr");
          if (rows.size() > 1) {
            //first row are the heading
            Element headerRow = rows.get(0);
            Elements headings = headerRow.select("th");
            for (int x = 0; x < headings.size(); x++) {
              String s = headings.get(x).text();
              if (s.contains("Passenger")) {
                name = x;
              } else if (s.contains("Seats")) {
                seat = x;
              } else if (s.contains("Class")) {
                sclass = x;
              } else if (s.contains("Status")) {
                status = x;
              } else if (s.contains("Frequent")) {
                freqflier =x;
              } else if (s.contains("eTicket")) {
                ticket = x;
              } else if (s.contains("Meal")) {
                meal = x;
              }
            }

            for (int r = 1; r < rows.size(); r++) {
              Element row = rows.get(r);
              Elements cols = row.select("td");
              PassengerVO pVO = new PassengerVO();
              if (name > -1) {
                pVO.setFullName(cols.get(name).text().replaceAll("» ", "").trim());
              }
              if (seat > -1) {
                pVO.setSeat(cols.get(seat).text().trim());
              }
              if (sclass > -1) {
                pVO.setSeatClass(cols.get(sclass).text().trim());
              } else if (flightClassElm != null && flightClassElm.hasText()) {
                pVO.setSeatClass(flightClassElm.text().trim());
              }
              if (ticket > -1) {
                pVO.seteTicketNumber(cols.get(ticket).text().trim());
              }
              if (freqflier > -1) {
                pVO.setFrequentFlyer(cols.get(freqflier).text().trim());
              }
              if (meal > -1) {
                pVO.setMeal(cols.get(meal).text().trim());
              } else if (mealElm != null && mealElm.hasText()) {
                pVO.setMeal(mealElm.text().trim());
              }
              flight.addPassenger(pVO);
            }
          }
        } else {
          passengerTable = doc.getElementById("segment-details-AIR_" + i);
          if (passengerTable != null) {
            for (int j = 0; j < 10; j++) {
              Element name = doc.getElementById("passenger-name-AIR_" + i + "-" + j);
              if (name != null) {
                Element seat = doc.getElementById("seats-AIR_" + i + "-" + j);
                Element classair = doc.getElementById("class-AIR_" + i + "-" + j);
                Element ticket = doc.getElementById("tickets-AIR_" + i + "-" + j);
                Element meal = doc.getElementById("meal-AIR_" + i + "-" + j);
                Element status = doc.getElementById("status-AIR_" + i + "-" + j);
                Element freqflier = doc.getElementById("frequent-flyer-AIR_" + i + "-" + j);
                StringBuilder seatSB = new StringBuilder();

                PassengerVO pVO = new PassengerVO();
                pVO.setFullName(name.text());
                if (seat != null) {
                  seatSB.append(seat.text().trim());
                }

                pVO.setSeat(seatSB.toString());
                if (classair != null) {
                  pVO.setSeatClass(classair.text().trim());
                }
                if (ticket != null) {
                  pVO.seteTicketNumber(ticket.text().trim());
                }
                if (freqflier != null) {
                  pVO.setFrequentFlyer(freqflier.text().trim());
                }
                if (meal != null) {
                  pVO.setMeal(meal.text().trim());
                }
                flight.addPassenger(pVO);
              } else {
                break;
              }
            }

          }
        }

        if (flight.isValid()) {
          if (flight.getReservationNumber() == null || flight.getReservationNumber().isEmpty()) {
            flight.setReservationNumber(locator);
          }
          flight.setNotes(notes.toString());
          flights.add(flight);
        }
      } else {
        break;//no more flights to process
      }

      i++;
    }

    return flights;

  }


  private static  List <TransportVO> parseCars(Document doc, String locator) {
    List<TransportVO> transports = new ArrayList<>();
    int i = 1;
    DateTime dt = new DateTime();
    while (true) {
      Element carsSegmentElm = doc.getElementById("segmentCAR_" + i);
      if (carsSegmentElm != null && carsSegmentElm.hasText()) {
        TransportVO car = new TransportVO();
        car.setBookingType(ReservationType.CAR_RENTAL);

        Elements h2 = carsSegmentElm.select("h2");
        if (h2 != null && h2.size() > 0) {
          car.setName(h2.get(0).text().trim());
        }
        Element carTypeNameElm = doc.getElementById("CarTypeInfoCAR_" + i);
        Element numOfDaysElm = doc.getElementById("NumberOfDaysCAR_" + i);
        Element confirmationNumElm = doc.getElementById("ConfirmationNumberCAR_" + i);
        Element statusElm = doc.getElementById("StatusCAR_" + i);



        Element pickupDateElm = doc.getElementById("PickUpDateCAR_" + i);
        Element pickupTimeElm = doc.getElementById("PickUpTimeCAR_" + i);
        Element dropoffDateElm = doc.getElementById("DropOffDateCAR_" + i);
        Element dropoffTimeElm = doc.getElementById("DropOffTimeCAR_" + i);

        Element pickupLocElm = doc.getElementById("PickUpCityCodeCAR_" + i);
        Element pickupAddrElm = doc.getElementById("PickUpCityNameCAR_" + i);
        Element pickupLocationElm = doc.getElementById("PickUpLocationCAR_" + i);

        Element dropoffLocElm = doc.getElementById("DropOffCityCodeCAR_" + i);
        Element dropoffAddrElm = doc.getElementById("DropOffCityNameCAR_" + i);
        Element dropoffLocationElm = doc.getElementById("DropOffLocationCAR_" + i);


        Element carTypeElm = doc.getElementById("CarTypeInfoCAR_" + i);
        Element notesElm = doc.getElementById("NotesCAR_" + i);

        Element approxTotalPriceElm = doc.getElementById("ApproxTotalPriceCAR_" + i);
        Element approxRateElm = doc.getElementById("segment-details_CAR_" + i);
        Element carRateElm = doc.getElementById("RateCAR_" + i);




        String pickupDate  = "";
        String pickupTime  = "12:00AM";
        String dropoffDate  = "";
        String dropoffTime  = "12:00AM";
        String pickupLocation = "";
        String dropOffLocation = "";
        StringBuilder sb = new StringBuilder();
        StringBuilder name = new StringBuilder();

        if(numOfDaysElm != null) {
          sb.append("Duration: ");
          sb.append(numOfDaysElm.text().replaceAll("&nbsp;", ""));
          sb.append("\n");
        }

        if(statusElm != null) {
          sb.append("Booking Status: ");

          sb.append(statusElm.text().replaceAll("&nbsp;", ""));
          sb.append("\n");
        }

        if(carTypeElm != null) {
          sb.append("Car Type: ");
          sb.append(carTypeElm.text().replaceAll("&nbsp;", ""));
          sb.append("\n");
        }


        if(confirmationNumElm != null) {
          car.setConfirmationNumber(confirmationNumElm.text());
        }

        if(pickupDateElm != null)
          pickupDate = pickupDateElm.text();

        if(pickupTimeElm != null && pickupTimeElm.hasText())
          pickupTime = pickupTimeElm.text();

        if(dropoffDateElm != null)
          dropoffDate = dropoffDateElm.text();

        if(dropoffTimeElm != null && dropoffTimeElm.hasText())
          dropoffTime = dropoffTimeElm.text();



        if(pickupLocationElm != null && !pickupLocationElm.text().isEmpty()) {
          pickupLocation = pickupLocationElm.text().replaceAll("&nbsp;", "") + "";
        } else {
          if(pickupAddrElm != null)
            pickupLocation = pickupAddrElm.text().replaceAll("&nbsp;","");

          if(pickupLocElm != null)
            pickupLocation += " (" + pickupLocElm.text().replaceAll("&nbsp;","") + ")";
        }
        pickupLocation = pickupLocation.trim();



        if(dropoffLocationElm != null && !dropoffLocationElm.text().isEmpty()) {
          dropOffLocation = dropoffLocationElm.text().replaceAll("&nbsp;", "") + "";
        } else {
          if(dropoffAddrElm != null)
            dropOffLocation = dropoffAddrElm.text().replaceAll("&nbsp;","");

          if(dropoffLocElm != null)
            dropOffLocation += " (" + dropoffLocElm.text().replaceAll("&nbsp;","") + ")";
        }
        dropOffLocation = dropOffLocation.trim();

        car.setPickupDate(getTimestamp(pickupDate + " " + dt.getYear(), pickupTime));
        car.setDropoffDate(getTimestamp(dropoffDate + " " + dt.getYear(), dropoffTime));


        car.setPickupLocation(pickupLocation);
        car.setDropoffLocation(dropOffLocation);



        if (notesElm != null) {
          if (sb.toString().length() > 0) {
            sb.append("\n");
          }
          sb.append(getText(notesElm));
        }
        if (approxTotalPriceElm != null) {
          if (sb.toString().length() > 0) {
            sb.append("\n\n");
          }
          sb.append("Approx Price: ");sb.append(getText(approxTotalPriceElm));
        }
        if (approxRateElm != null) {
          Elements rows = approxRateElm.select("tr");
          if (rows != null && rows.size() > 0) {
            if (sb.toString().length() > 0) {
              sb.append("\n\n");
            }
            for (Element row: rows) {
              sb.append(getText(row));sb.append("\n");
            }
          }
        }
        if (carRateElm != null) {
          if (sb.toString().length() > 0) {
            sb.append("\n\n");
          }
          sb.append("Rate: ");sb.append(getText(carRateElm));
        }
        car.setNote(sb.toString());

        if (car.isValid()) {
          transports.add(car);
        }
        i++;
      } else {
        break;
      }

    }

    return transports;
  }

  private static  List <TransportVO> parseRails(Document doc, String locator) {
    List<TransportVO> transports = new ArrayList<>();
    int i = 1;
    DateTime dt = new DateTime();
    while (true) {
      Element carsSegmentElm = doc.getElementById("segmentRAL_" + i);
      if (carsSegmentElm != null && carsSegmentElm.hasText()) {
        TransportVO rail = new TransportVO();
        rail.setBookingType(ReservationType.RAIL);
        String cmpyName = "";
        Elements h2 = carsSegmentElm.select("h2");
        if (h2 != null && h2.size() > 0) {
          cmpyName = h2.get(0).text().trim();
        }

        Element coachRailElm = doc.getElementById("CoachRAL_" + i);
        Element confirmationNumElm = doc.getElementById("ConfirmationNumberRAL_" + i);

        Element departureCityCode = doc.getElementById("DepartureCityCodeRAL" + i);
        Element departureCity = doc.getElementById("DepartureCityNameRAL_" + i);
        Element departureTimeElm = doc.getElementById("DepartingAtRAL_" + i);

        Element arrivalCityCode = doc.getElementById("ArrivalCityCodeRAL_" + i);
        Element arrivalCity = doc.getElementById("ArrivalCityNameRAL_" + i);
        Element arrivalTimeElm = doc.getElementById("ArrivingAtRAL_" + i);

        Element trainNumElm = doc.getElementById("TrainRAL_" + i);
        Element facts = doc.getElementById("FactsRAL_" + i);


        Element departureDateElm = doc.getElementById("DepartureDateRAL_" + i);
        Element arrivalDateElm = doc.getElementById("ArrivalDateRAL_" + i);




        Element notesElm = doc.getElementById("NotesRAL_" + i);

        Element passengers = doc.getElementById("segment-details_RAL_" + i);




        String pickupDate  = "";
        String pickupTime  = "12:00AM";
        String dropoffDate  = "";
        String dropoffTime  = "12:00AM";
        String pickupLocation = "";
        String dropOffLocation = "";
        StringBuilder sb = new StringBuilder();
        StringBuilder name = new StringBuilder();



        if(facts != null) {
          sb.append("Seats: ");
          sb.append(facts.text().replaceAll("&nbsp;", ""));
          sb.append("\n");
        }


       if (trainNumElm != null) {
         cmpyName = cmpyName + " - " + trainNumElm.text();
       }

        rail.setName(cmpyName);

        if (coachRailElm != null) {
          sb.append(coachRailElm.text());
          sb.append("\n");
        }

        if(confirmationNumElm != null) {
          rail.setConfirmationNumber(confirmationNumElm.text());
        }

        if(departureDateElm != null)
          pickupDate = departureDateElm.text();

        if(departureTimeElm != null && departureTimeElm.hasText()) {
          String departTime = departureTimeElm.text();
          if (departTime.contains("Departing At:")) {
            departTime = departTime.substring(14);
          }
          pickupTime = departTime;
        }

        if(arrivalDateElm != null )
          dropoffDate = arrivalDateElm.text();

        if(arrivalTimeElm != null && arrivalTimeElm.hasText()) {
          String arriveTime = arrivalTimeElm.text();
          if (arriveTime.contains("Arriving At:")) {
            arriveTime = arriveTime.substring(13);
          }
          dropoffTime = arriveTime;
        }

        if(departureCity != null)
          pickupLocation = departureCity.text().replaceAll("&nbsp;","");

        if(departureCityCode != null && departureCityCode.hasText())
          pickupLocation += " (" + departureCityCode.text().replaceAll("&nbsp;","") + ")";

        if(arrivalCity != null)
          dropOffLocation = arrivalCity.text().replaceAll("&nbsp;","");

        if(arrivalCityCode != null && arrivalCityCode.hasText())
          dropOffLocation += " (" + arrivalCityCode.text().replaceAll("&nbsp;","") + ")";


        rail.setPickupDate(getTimestamp(pickupDate + " " + dt.getYear(), pickupTime));
        rail.setDropoffDate(getTimestamp(dropoffDate + " " + dt.getYear(), dropoffTime));


        rail.setPickupLocation(pickupLocation);
        rail.setDropoffLocation(dropOffLocation);



        if (notesElm != null) {
          if (sb.toString().length() > 0) {
            sb.append("\n");
          }
          sb.append(getText(notesElm));
        }

        if (passengers != null) {
          Elements rows = passengers.select("tr");
          if (rows != null && rows.size() > 0) {
            if (sb.toString().length() > 0) {
              sb.append("\n\n");
            }
            for (Element row: rows) {
              sb.append(getText(row));sb.append("\n");
            }
          }
        }
        rail.setNote(sb.toString());

        if (rail.isValid()) {
          transports.add(rail);
        }
        i++;
      } else {
        break;
      }

    }

    return transports;
  }

  private static List <ActivityVO> parseTour (Document doc, String locator) {
    List<ActivityVO> activities = new ArrayList<>();
    int i = 1;
    DateTime dt = new DateTime();
    while (true) {
      String cityCode = "";
      Element tourSegmentElm = doc.getElementById("segmentTOR_" + i);
      if (tourSegmentElm != null && tourSegmentElm.hasText()) {
        ActivityVO activity = new ActivityVO();
        Element tourNameElm = doc.getElementById("TourNameTOR_" + i);
        Element tourStartDateElm = doc.getElementById("DepartureDateTOR_" + i);
        Element tourStartLocElm = doc.getElementById("DepartureCityCodeTOR_" + i);


        Element tourFactsElm = doc.getElementById("FactsTOR_" + i);
        Element tourNotesElm = doc.getElementById("NotesTOR_" + i);

        Element tourCodeElm = doc.getElementById("TourCodeTOR_" + i);

        StringBuilder sb = new StringBuilder();
        activity.setName(tourNameElm.text());

        if (tourCodeElm != null && tourCodeElm.text() != null) {
          sb.append("Code: ");
          sb.append(tourCodeElm.text());
          sb.append("\n");
        }

        if (tourNotesElm != null && tourNotesElm.text() != null) {
          sb.append(getText(tourNotesElm));
          sb.append("\n");

        }

        if (tourFactsElm != null && tourFactsElm.text() != null) {
          sb.append(getText(tourFactsElm));
        }
        activity.setNote(sb.toString());
        activity.setBookingType(ReservationType.TOUR);



        if (tourStartLocElm != null && tourStartLocElm.text() != null) {
          if (tourStartLocElm != null && tourStartLocElm.parent() != null && tourStartLocElm.hasClass("cityCode") && tourStartLocElm.parent().hasClass("location")) {
            cityCode = tourStartLocElm.text();
            tourStartLocElm = tourStartLocElm.parent();
            if (tourStartLocElm.children() != null) {
              for (Element  e: tourStartLocElm.children()) {
                if (e.hasClass("cityName")) {
                  tourStartLocElm = e;
                  break;
                }
              }
            }
          }
          activity.setStartLocation(tourStartLocElm.text().trim());
        }

        if (tourStartDateElm != null && tourStartDateElm.text() != null) {
          activity.setStartDate(getTimestamp(tourStartDateElm.text() + " " + dt.getYear(), "12:00AM"));
        }

        if (activity.isValid()) {
          activities.add(activity);
        }

        i++;
      }
      else {
        break;
      }
    }
    return activities;
  }

  private static  List <NoteVO> parseOth (Document doc, String locator) {
    List<NoteVO> notes = new ArrayList<>();
    int i = 1;

    DateTime dt = new DateTime();
    while (true) {
      Element noteSegmentElm = doc.getElementById("segmentOTH_" + i);
      if (noteSegmentElm != null && noteSegmentElm.hasText()) {
        try {
          NoteVO note = new NoteVO();
          StringBuilder sb = new StringBuilder();
          StringBuilder title = new StringBuilder();
          boolean hasText = false;
          if (noteSegmentElm.getElementById("ArrivingDateOTH_" + i) != null && noteSegmentElm.getElementById(
              "ArrivingDateOTH_" + i).hasText()) {
            String date = noteSegmentElm.getElementById("ArrivingDateOTH_" + i).text();
            Timestamp ts = getTimestamp(date + " " + dt.getYear(), "12:00am");
            note.setTimestamp(ts);
          }
          if (noteSegmentElm.getElementById("DepartingAirportNameOTH_" + i) != null && noteSegmentElm.getElementById(
              "DepartingAirportNameOTH_" + i).hasText()) {
            title.append(noteSegmentElm.getElementById("DepartingAirportNameOTH_" + i).text());
            if (title.toString().length() > 0) {
              hasText = true;
            }
          }
          if (noteSegmentElm.getElementById("DepartingCityCodeOTH_" + i) != null && noteSegmentElm.getElementById(
              "DepartingCityCodeOTH_" + i).hasText()) {
            if (hasText) {
              title.append(" (");
            }
            title.append(noteSegmentElm.getElementById("DepartingCityCodeOTH_" + i).text());
            if (hasText) {
              title.append(")");
            }
          }

          if (noteSegmentElm.getElementById("StatusCodeOTH_" + i) != null && noteSegmentElm.getElementById(
              "StatusCodeOTH_" + i).hasText()) {
            sb.append("Status: ");
            sb.append(noteSegmentElm.getElementById("StatusCodeOTH_" + i).text());
            sb.append("<br/><br/>");
          }

          if (noteSegmentElm.getElementById("InformationOTH_" + i) != null && noteSegmentElm.getElementById(
              "InformationOTH_" + i).hasText()) {
            sb.append(noteSegmentElm.getElementById("InformationOTH_" + i).text());
            sb.append("<br/>");
          }

          note.setName(title.toString());
          note.setNote(sb.toString());
          notes.add(note);
        } catch (Exception e) {
          e.printStackTrace();
        }



          i++;
      } else {
        break;
      }
    }
    return notes;
  }


    private static Timestamp getTimestamp (String date, String time) {
    try {
      date = date.substring(date.indexOf(" ")).trim();//filter out the day of the week
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime() );
      if (t.getTime() < System.currentTimeMillis()) {
        //move the year to next year
        DateTime dt = new DateTime(t.getTime());
        t = new Timestamp(dt.plusYears(1).getMillis());
      }
      return t;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  private static String getText(Element parentElement) {
    String working = "";
    for (Node child : parentElement.childNodes()) {
      if (child instanceof TextNode) {
        working += ((TextNode) child).text();
      }
      if (child instanceof Element) {
        Element childElement = (Element)child;
        // do more of these for p or other tags you want a new line for
        if (childElement.tag().getName().equalsIgnoreCase("br")) {
          working += "\n";
        }

        working += getText(childElement);
      }
    }

    return working.trim();
  }

  public static void main(String[] args) {
    try {
      String s = FileUtils.readFileToString(new File("/home/twong/Dropbox/sync/wip/pnrs/tripcase/VIWXNJ.html"));
      Document doc = Jsoup.parse(s);

      TripVO tripVO = getInfo(doc, "");
      System.out.println(tripVO);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
