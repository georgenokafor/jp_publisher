package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import com.mapped.publisher.utils.Log;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * A combination of date and time of day in the form
 * [-]CCYY-MM-DDThh:mm:ss[Z|(+|-)hh:mm] (see Chapter 5.4 of ISO 8601).
 * Created by surge on 2015-05-11.
 */
public class DateTime
    implements Serializable {

  public String value;
  LocalDateTime time;

  public DateTime() {
  }

  public DateTime(String dateTime) {
    value = dateTime;
  }

  public DateTime(LocalDateTime ldt) {
    time = ldt;
    value = ldt.toInstant(ZoneOffset.UTC).toString();
  }

  @JsonValue
  public String getValue() {
    return value;
  }

  public DateTime setValue(String value) {
    this.value = value;
    return this;
  }

  public String toString() {
    return value;
  }

  @JsonIgnore
  public LocalDateTime getTime() {
    return time;
  }

  public DateTime setTime(LocalDateTime time) {
    this.time = time;
    return this;
  }

  /**
   * Converts ISO formatted DateTime string to java.sql.Timestamp used in Umapped VO
   *
   * @return
   */
  @JsonIgnore
  public Timestamp getTimestamp() {
    if (value == null) {
      return null;
    }
    try {
      Instant inst = Instant.parse(value);
      return Timestamp.from(inst);
    }
    catch (DateTimeParseException e) {
      Log.err("Date: Schema.org encountered wrong date format:", value);
    }
    return null;
  }

  /**
   * Converts to ISO8601 Date YYYY-MM-DD
   */
  @JsonIgnore
  public String getISODate() {
    if (time == null) {
      return "";
    }
    return time.format(DateTimeFormatter.ISO_LOCAL_DATE);
  }
}
