package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-07-24
 * Time: 2:33 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "tmplt_activity")
public class TmpltActivity
    extends Model {
  public static Model.Finder<String, TmpltActivity> find = new Finder(TmpltActivity.class);
  public String comments;
  public String contact;
  public String destinationid;
  @Id
  public String detailsid;
  public String name;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;
  private String pickuplocation;
  private String dropofflocation;

  public static TmpltActivity buildActivity(String detailsId, String userId) {
    TmpltActivity tmpltActivity = new TmpltActivity();
    tmpltActivity.setDetailsid(detailsId);
    tmpltActivity.setCreatedtimestamp(System.currentTimeMillis());
    tmpltActivity.setLastupdatedtimestamp(tmpltActivity.createdtimestamp);
    tmpltActivity.setCreatedby(userId);
    tmpltActivity.setModifiedby(userId);
    tmpltActivity.setVersion(0);
    return tmpltActivity;
  }

  public static TmpltActivity findForDetail(String tmpltDetailsId) {
    return find.where().eq("detailsid", tmpltDetailsId).findUnique();
  }

  public String getPickuplocation() {
    return pickuplocation;
  }

  public void setPickuplocation(String pickuplocation) {
    this.pickuplocation = pickuplocation;
  }

  public String getDropofflocation() {
    return dropofflocation;
  }

  public void setDropofflocation(String dropofflocation) {
    this.dropofflocation = dropofflocation;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public String getDestinationid() {
    return destinationid;
  }

  public void setDestinationid(String destinationid) {
    this.destinationid = destinationid;
  }

  public String getDetailsid() {
    return detailsid;
  }

  public void setDetailsid(String detailsid) {
    this.detailsid = detailsid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
