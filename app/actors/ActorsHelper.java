package actors;

import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import com.google.inject.Inject;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

/**
 * Created by surge on 2016-11-21.
 */
public class ActorsHelper {

  @Inject
  static ActorSystem actorSystem;

  /**
   * Helper function to send a message to one of the system actors
   *
   * @param actor
   * @param msg
   */
  public static void tell(SupervisorActor.UmappedActor actor, final Object msg) {
    ActorSelection as = actorSystem.actorSelection(actor.getPath());
    as.tell(msg, null);
  }

  /*
     Send a message to the actor on a delay
     */
  public static void tell(final SupervisorActor.UmappedActor actor, final Object msg, int millisDelay) {
    actorSystem.scheduler().scheduleOnce(Duration.create(millisDelay, TimeUnit.MILLISECONDS), () -> {
      ActorSelection as = actorSystem.actorSelection(actor.getPath());
      as.tell(msg, null);
    }, actorSystem.dispatcher());
  }
}
