package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-27
 * Time: 11:32 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "trip_publish_history")
public class TripPublishHistory
    extends Model {
  public static Model.Finder<String, TripPublishHistory> find = new Finder(TripPublishHistory.class);
  public String comments;
  public String groupid;
  @Id
  public String pk;
  @Constraints.Required
  public String tripid;
  public int status;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;

  public static List<TripPublishHistory> findByTrip(String tripId) {
    return find.where().eq("tripid", tripId).orderBy("lastupdatedtimestamp desc").findList();
  }

  public static List<TripPublishHistory> findByTripDefaultGroup(String tripId) {
    return find.where().eq("tripid", tripId).isNull("groupid").orderBy("lastupdatedtimestamp desc").findList();
  }

  public static List<TripPublishHistory> findByTripGroupId(String tripId, String groupId) {
    return find.where().eq("tripid", tripId).eq("groupId", groupId).orderBy("lastupdatedtimestamp desc").findList();
  }

  public static int countPublished(String tripId) {
    return find.where().eq("tripid", tripId).findCount();
  }

  public static int countPublishedDefaultGroup(String tripId) {
    return find.where().eq("tripid", tripId).isNull("groupId").findCount();
  }

  public static int countPublishedGroup(String tripId, String groupId) {
    return find.where().eq("tripid", tripId).eq("groupId", groupId).findCount();
  }

  public static TripPublishHistory getLastPublished(String tripid, String groupId) {
    List<TripPublishHistory> results;
    if (groupId != null && groupId.trim().length() > 0) {
      results = find
          .where()
          .eq("tripid", tripid)
          .eq("groupId", groupId)
          .orderBy("createdtimestamp desc")
          .setMaxRows(1)
          .findList();
    }
    else {
      results = find
          .where()
          .eq("tripid", tripid)
          .isNull("groupId")
          .orderBy("createdtimestamp desc")
          .setMaxRows(1)
          .findList();
    }

    if (results != null && results.size() > 0) {
      return results.get(0);
    }
    else {
      return null;
    }
  }

  public static TripPublishHistory getPublishedDefaultGroup(String tripid) {
    List<TripPublishHistory> results;
    results = find.where()
              .eq("tripid", tripid)
              .eq("status", 0)
              .isNull("groupId")
              .orderBy("createdtimestamp desc")
              .setMaxRows(1)
              .findList();
    if (results != null && results.size() > 0) {
      return results.get(0);
    }
    else {
      return null;
    }
  }

  public static boolean hasPublishedGroups(String tripid) {
    List<TripPublishHistory> results  = find.where()
                                        .eq("tripid", tripid)
                                        .eq("status", 0)
                                        .isNotNull("groupId")
                                        .orderBy("createdtimestamp desc")
                                        .findList();

    if (results != null && results.size() > 0) {
      return true;
    }
    else {
      return false;
    }
  }

  private static final String SQL_PUB_IN_RANGE =
      "SELECT pk, tph.tripid, comments, status, createdtimestamp, createdby, version, groupid  " +
      "FROM trip_publish_history tph JOIN ( " +
      "SELECT tripid, first_ts FROM ( " +
      "    SELECT  tripid, MIN(createdtimestamp) as first_ts  " +
      "    FROM trip_publish_history " +
      "    WHERE createdby = :creator  " +
      "    GROUP BY tripid " +
      ") as q " +
      "WHERE first_ts > :startts AND first_ts < :finishts) r " +
      "ON r.tripid = tph.tripid AND r.first_ts = tph.createdtimestamp";

  public static List<TripPublishHistory> getFirstPublishedInRange(String userid, long startTs, long finishTs) {
    RawSql rsql = RawSqlBuilder.parse(SQL_PUB_IN_RANGE)
                               .columnMapping("pk", "pk")
                               .columnMapping("tph.tripid", "tripid")
                               .columnMapping("comments", "comments")
                               .columnMapping("status", "status")
                               .columnMapping("createdtimestamp", "createdtimestamp")
                               .columnMapping("createdby", "createdby")
                               .columnMapping("version", "version")
                               .columnMapping("groupid", "groupid")
                               .create();

    com.avaje.ebean.Query<TripPublishHistory> query = Ebean.createQuery(TripPublishHistory.class);
    query.setRawSql(rsql);
    query.setParameter("creator", userid);
    query.setParameter("startts", startTs);
    query.setParameter("finishts", finishTs);

    return query.findList();
  }


  private static final String SQL_CREATED_AND_PUBLISHED_BY_USER_AND_MINIONS =
      "SELECT pk, tph.tripid, comments, status, createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, " +
      "version, groupid  " +
      "FROM trip_publish_history tph JOIN ( " +
      "    SELECT tripid, first_ts FROM ( " +
      "        SELECT  tripid, MIN(createdtimestamp) as first_ts  " +
      "        FROM trip_publish_history " +
      "        WHERE tripid IN ( " +
      "            SELECT tripid  " +
      "            FROM trip  " +
      "            WHERE createdby IN (SELECT userid  " +
      "                                FROM bil_settings_user " +
      "                                WHERE userid = :agent OR (agent = :agent AND bill_to = 'AGENT')) " +
      "        ) " +
      "        GROUP BY tripid " +
      "    ) as q " +
      "    WHERE first_ts > :startts AND first_ts < :finishts) r  " +
      "    ON r.tripid = tph.tripid AND r.first_ts = tph.createdtimestamp";

  /**
   * Trips that were created by agent and all if his/her minions and that were published during specified time interval.
   * @param userid
   * @param startTs
   * @param finishTs
   * @return
   */
  public static List<TripPublishHistory> getAgentAndMinionsPublishedTrips(String userid, long startTs, long finishTs) {
    RawSql rsql = RawSqlBuilder.parse(SQL_CREATED_AND_PUBLISHED_BY_USER_AND_MINIONS)
                               .columnMapping("pk", "pk")
                               .columnMapping("tph.tripid", "tripid")
                               .columnMapping("comments", "comments")
                               .columnMapping("status", "status")
                               .columnMapping("createdtimestamp", "createdtimestamp")
                               .columnMapping("createdby", "createdby")
                               .columnMapping("version", "version")
                               .columnMapping("groupid", "groupid")
                               .create();

    com.avaje.ebean.Query<TripPublishHistory> query = Ebean.createQuery(TripPublishHistory.class);
    query.setRawSql(rsql);
    query.setParameter("agent", userid);
    query.setParameter("startts", startTs);
    query.setParameter("finishts", finishTs);

    return query.findList();
  }



  private static final String SQL_CREATED_AND_PUBLISHED_BY_CMPY_USERS =
      "SELECT pk, tph.tripid, comments, status, createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, " +
      "version, groupid  " +
      "FROM trip_publish_history tph JOIN ( " +
      "    SELECT tripid, first_ts FROM ( " +
      "        SELECT  tripid, MIN(createdtimestamp) as first_ts  " +
      "        FROM trip_publish_history " +
      "        WHERE tripid IN ( " +
      "            SELECT tripid  " +
      "            FROM trip  " +
      "            WHERE createdby IN ( " +
      "                SELECT userid " +
      "                FROM bil_settings_user " +
      "                WHERE bill_to = 'COMPANY' AND cmpyid = :company" +
      "            ) " +
      "        ) " +
      "        GROUP BY tripid " +
      "    ) as q " +
      "    WHERE first_ts > :startts AND first_ts < :finishts) r  " +
      "    ON r.tripid = tph.tripid AND r.first_ts = tph.createdtimestamp";

  /**
   * Trips that were created by agents that bill the company and published during timeframe.
   * @param cmpyid
   * @param startTs
   * @param finishTs
   * @return
   */
  public static List<TripPublishHistory> getCmpyPublishedTrips(String cmpyid, long startTs, long finishTs) {
    RawSql rsql = RawSqlBuilder.parse(SQL_CREATED_AND_PUBLISHED_BY_CMPY_USERS)
                               .columnMapping("pk", "pk")
                               .columnMapping("tph.tripid", "tripid")
                               .columnMapping("comments", "comments")
                               .columnMapping("status", "status")
                               .columnMapping("createdtimestamp", "createdtimestamp")
                               .columnMapping("createdby", "createdby")
                               .columnMapping("version", "version")
                               .columnMapping("groupid", "groupid")
                               .create();

    com.avaje.ebean.Query<TripPublishHistory> query = Ebean.createQuery(TripPublishHistory.class);
    query.setRawSql(rsql);
    query.setParameter("company", cmpyid);
    query.setParameter("startts", startTs);
    query.setParameter("finishts", finishTs);

    return query.findList();
  }


  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getPk() {
    return pk;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public String getTripid() {
    return tripid;
  }

  public void setTripid(String tripid) {
    this.tripid = tripid;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getGroupid() {
    return groupid;
  }

  public void setGroupid(String groupid) {
    this.groupid = groupid;
  }
}
