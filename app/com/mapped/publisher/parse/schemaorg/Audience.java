package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by surge on 2016-06-28.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Audience
    extends Thing {
  /**
   * The target group associated with a given audience (e.g. veterans, car owners, musicians etc.).
   */
  String audienceType;
  /**
   * The geographic area associated with the audience.
   */
  AdministrativeArea geographicArea;
}
