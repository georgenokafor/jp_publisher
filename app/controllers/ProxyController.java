package controllers;

import com.google.inject.Inject;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.utils.Log;
import play.libs.Jsonp;
import play.libs.ws.*;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by surge on 2015-01-09.
 */
public class ProxyController
    extends Controller {
  private final static String FRESHDESK_GS_CATEGORY_ID = "4000002337";
  private final static String FRESHDESK_GS_FOLDER_ID   = "4000008849";
  private final static String FRESHDESK_USERNAME       = "LCR1U8vHxliYYJJrYEjU";
  private final static String FRESHDESK_PASSWORD       = "X";
  private final static String FRESHDESK_GS_URL         = "https://umapped.freshdesk.com/solution/categories/" +
                                                         FRESHDESK_GS_CATEGORY_ID + "/folders/" +
                                                         FRESHDESK_GS_FOLDER_ID + ".json";

  @Inject
  WSClient ws;

  public CompletionStage<Result> freshdesk(final String callback) {

    String jsonpString = (String) CacheMgr.get(APPConstants.CACHE_FRESHDESK_GETTING_STARTED);

    if (jsonpString != null) {
      return CompletableFuture.completedFuture(ok(jsonpString).as("text/javascript; charset=utf-8"));
    }

    WSRequest request = ws.url(FRESHDESK_GS_URL)
                          .setRequestTimeout(1000)
                          .setAuth(FRESHDESK_USERNAME, FRESHDESK_PASSWORD, WSAuthScheme.BASIC);

    CompletionStage<WSResponse> respPromise = request.get();

    CompletionStage<Result> result = respPromise.thenApplyAsync((WSResponse wsResponse) -> {
      Jsonp jsonp = Jsonp.jsonp(callback, wsResponse.asJson());
      CacheMgr.set(APPConstants.CACHE_FRESHDESK_GETTING_STARTED,
                   jsonp.body(),
                   APPConstants.CACHE_FRESHDESK_EXPIRY_SECS);
      return ok(jsonp);
    });

    /** RECOVERY DOES NOT WORK ACCORDING TO THE DOCS
     * https://www.playframework.com/documentation/2.3.x/JavaWS#Exception-recovery
     */
    result.exceptionally((Throwable throwable) -> {
      Log.info("FRESHDESK: Exception on Getting Started pull: " + throwable.getMessage());
      return ok((String) CacheMgr.get(APPConstants.CACHE_FRESHDESK_GETTING_STARTED)).as("text/javascript; " +
                                                                                        "charset=utf-8");
    });
    return result;
  }
}
