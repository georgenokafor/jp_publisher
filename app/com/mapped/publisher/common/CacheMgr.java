package com.mapped.publisher.common;

import com.google.inject.Inject;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import net.spy.memcached.AddrUtil;
import net.spy.memcached.ConnectionFactoryBuilder;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.auth.AuthDescriptor;
import net.spy.memcached.auth.PlainCallbackHandler;
import play.cache.Cache;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Created by twong on 2014-12-03.
 */
public class CacheMgr {
  private static MemcachedClient c = null;

  @Inject
  static RedisMgr redis;

  public static RedisMgr getRedis(){
    return redis;
  }

  public static Object get(final String key) {
    if (key == null) {
      return null;
    }
    return redis.getBin(key);
  }

  /**
   * @param key
   * @param value
   * @param expiry The exp value is passed along to cache implementation exactly as given,
   *               and will be processed per the cached protocol specification:
   *               <p>
   *               The actual value sent may either be Unix time (number of seconds since January 1, 1970,
   *               as a 32-bit value), or a number of seconds starting from current time. In the latter case,
   *               this number of seconds may not exceed 60*60*24*30 (number of seconds in 30 days); if the number
   *               sent by a client is larger than that, the server will consider it to be real Unix time value
   *               rather than an offset from current time.</p>
   */
  public static void set(final String key, final Object value, final int expiry) {
    if(key == null) {
      return;
    }

    if(value == null || expiry <= 0) {
      redis.delKey(key);
      return;
    }

    redis.setUnsafe(key, value, expiry);
  }


  public static void setBlocking(final String key, final Object value, final int expiry) {
    if(key == null) {
      return;
    }

    if(value == null || expiry <= 0) {
      redis.delKey(key);
      return;
    }

    redis.setBlocking(key, value, expiry);
  }

  public static void set(final String key, final String suffix, final Object value) {
    set(key + suffix, value);
  }


  /**
   * Set with default expiration for the key (currently set as 1 hour)
   * @param key
   * @param value
   */
  public static void set(final String key, final Object value) {
    set(key, value, APPConstants.CACHE_EXPIRY_SECS);
  }


  /*
   Legacy support for Memcached for extenal monitoring of processes...
   Processes running out of Play keep their state in Memcached
   And the Application:healthcheck service is called externally to monitor the state of these processes
   This legacy support will be temporary until Memcached is replaced in all the external processes
   */
  public static boolean init(String server, String user, String pwd) {
    try {
      if (user == null) {
        c = new MemcachedClient(new ConnectionFactoryBuilder().setTranscoder(new CustomSerializingTranscoder())
                                                              .setProtocol(ConnectionFactoryBuilder.Protocol.BINARY)
                                                              .build(), AddrUtil.getAddresses(server));
      }
      else {
        AuthDescriptor ad = new AuthDescriptor(new String[]{"PLAIN"}, new PlainCallbackHandler(user, pwd));
        c = new MemcachedClient(new ConnectionFactoryBuilder().setProtocol(ConnectionFactoryBuilder.Protocol.BINARY)
                                                              .setAuthDescriptor(ad)
                                                              .setTranscoder(new CustomSerializingTranscoder())
                                                              .build(), AddrUtil.getAddresses(server));
      }
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "CacheMgr: cannot connect - " + server, e);
      return false;
    }

    return true;
  }

  public static Object getMemcached(String key) {
    if (key == null) {
      return null;
    }

    if (c == null) {
      return Cache.get(key);
    }
    else {
      //async get with a timer
      // Try to get a value, for up to 3 seconds, and cancel if it doesn't return
      Object myObj = null;
      Future<Object> f = c.asyncGet(key);
      try {
        myObj = f.get(3, TimeUnit.SECONDS);
      }
      catch (Exception e) {
        // Since we don't need this, go ahead and cancel the operation.  This
        // is not strictly necessary, but it'll save some work on the server.
        f.cancel(false);
        // Do other timeout related stuff
        Log.log(LogLevel.ERROR, "CacheMgr:get(" + key + ")", e);
      }
      return myObj;
      /*
      try {
        return c.get(key);
      } catch (Exception e) {
        Log.log(LogLevel.ERROR, "CacheMgr:get(" +key + ")", e);
      }
      */
    }


  }

  /**
   * @param key
   * @param value
   * @param expiry The exp value is passed along to memcached exactly as given,
   *               and will be processed per the memcached protocol specification:
   *               <p>
   *               The actual value sent may either be Unix time (number of seconds since January 1, 1970,
   *               as a 32-bit value), or a number of seconds starting from current time. In the latter case,
   *               this number of seconds may not exceed 60*60*24*30 (number of seconds in 30 days); if the number
   *               sent by a client is larger than that, the server will consider it to be real Unix time value
   *               rather than an offset from current time.</p>
   */
  public static void setMemcached(String key, Object value, int expiry) {
    if (key != null) {
      if (c == null) {
        Cache.set(key, value, expiry);
      }
      else {
        try {
          if (value == null) {
            c.delete(key);
          }
          else {
            c.set(key, expiry, value);
          }
        }
        catch (Exception e) {
          Log.log(LogLevel.ERROR, "Error CacheMgr:set( " + key + ")", e);

        }
      }
    }
  }

  public static void setMemcached(String key, String suffix, Object value) {
    setMemcached(key + suffix, value);
  }

  public static void setMemcached(String key, Object value) {
    if (key != null) {
      if (c == null) {
        Cache.set(key, value, APPConstants.CACHE_EXPIRY_SECS);
      }
      else {
        try {
          if (value == null) {
            c.delete(key);
          }
          else {
            c.set(key, APPConstants.CACHE_EXPIRY_SECS, value);
          }

        }
        catch (Exception e) {
          Log.log(LogLevel.ERROR, "Error CacheMgr:set( " + key + ")", e);
        }
      }
    }
  }



}
