package com.umapped.itinerary.analyze.segment.feature;

import com.google.inject.Singleton;
import org.apache.commons.collections.CollectionUtils;

import java.util.EnumMap;
import java.util.Optional;

/**
 * Created by wei on 2017-03-10.
 */
@Singleton public class DefaultFeatureDetectorRegistory
    implements SegmentFeatureDetectorRegistry {

  private EnumMap<SegmentFeature, SegmentFeatureDetector> detectorMap;

  public DefaultFeatureDetectorRegistory() {
    detectorMap = new EnumMap<SegmentFeature, SegmentFeatureDetector>(SegmentFeature.class);
    init();
  }

  @Override public Optional<SegmentFeatureDetector> getDetector(SegmentFeature feature) {
    return Optional.ofNullable(detectorMap.get(feature));
  }

  protected void init() {
    add(SegmentFeature.DurationHours, (context -> (int) context.getCurrent().getDurationHours()));
    add(SegmentFeature.OverNightHours, (context -> SegmentFeatureDetectorCollection.overnightSpan(context)));
    add(SegmentFeature.AccommodationStay,
        (context -> SegmentFeatureDetectorCollection.getAccomondationStayPeriod(context.getCurrent())));
    add(SegmentFeature.DatesWithoutAccommodation,
        (context -> SegmentFeatureDetectorCollection.daysWithoutAccommodation(context.getCurrent())));
    add(SegmentFeature.HasCancellableAccomodation,
        (context -> SegmentFeatureDetectorCollection.accomodationCancellable(context)));
    add(SegmentFeature.LocationSequence, (context -> SegmentFeatureDetectorCollection.getLocationSequence(context)));

    add(SegmentFeature.HasTransfers, (context -> !CollectionUtils.isEmpty(context.getCurrent().getTransfers())));
    add(SegmentFeature.HasAccommondations, (context -> !CollectionUtils.isEmpty(context.getCurrent().getAccommodations())));
    add(SegmentFeature.HasActivities, (context -> !CollectionUtils.isEmpty(context.getCurrent().getActivities())));
    add(SegmentFeature.DatesWihoutActivities, (context -> SegmentFeatureDetectorCollection.daysWithoutActivites(context.getCurrent())));
    add(SegmentFeature.HasCruiseStops, (context -> SegmentFeatureDetectorCollection.hasCruiseStops(context.getCurrent())));
  }

  protected void add(SegmentFeature feature, SegmentFeatureDetector detector) {
    detectorMap.put(feature, detector);
  }

}
