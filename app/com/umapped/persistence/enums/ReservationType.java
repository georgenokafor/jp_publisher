package com.umapped.persistence.enums;

import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * When adding new types make sure the following places are also updated:
 * - Communicator.getRoomName()
 */
public enum ReservationType {
  ROOT(0, "ROOT", null),
  FLIGHT(1, "Flight", ROOT),
  HOTEL(2, "Accommodations", ROOT),
  TRANSPORT(3, "Transportation", ROOT),
  ACTIVITY(4, "Activity", ROOT),
  TOUR(5, " Tour", ACTIVITY),
  CRUISE(6, "Cruise", ROOT),
  RAIL(7, "Rail", TRANSPORT),
  CAR_RENTAL(8, "Car Rental", TRANSPORT),
  PRIVATE_TRANSFER(9, "Transfer", TRANSPORT),
  TAXI(10, "Taxi", TRANSPORT),
  BUS(11, "Bus", TRANSPORT),
  PUBLIC_TRANSPORT(12, "Public Transport", TRANSPORT),
  EVENT(13, "Event", ACTIVITY),
  RESTAURANT(14, "Restaurant", ACTIVITY),
  SHORE_EXCURSION(15, "Shore Excursion", ACTIVITY),
  CRUISE_STOP(16, "Cruise Stop", ACTIVITY),
  PACKAGE(17, "Reservation Package", ROOT),
  NOTE(18, "Note", ROOT),
  CAR_DROPOFF(19, "Car Rental Drop-Off", TRANSPORT),
  SKI_LESSONS(20, "Ski Lessons", ACTIVITY),
  SKI_LIFT(21, "Ski Lift Tickets", ACTIVITY),
  SKI_RENTALS(22, "Ski Rentals", ACTIVITY),
  FERRY(23, "Ferry", TRANSPORT);
 /*
  SPA(24, "Spa", ACTIVITY),
  APPOINTMENT(25, "Appointment", ACTIVITY),
  MEETING(26, "Meeting", ACTIVITY); */

  private int             dbIdx;
  private ReservationType parentCategory;
  private String          label;

  /**
   * Class to allow specifying parameter from enum in Play Framework.
   */
  public static class Bound
      implements QueryStringBindable<Bound>, PathBindable<Bound> {
    private ReservationType value;

    /**
     * This empty constructor must be there, implicit one is not recognized
     * when other constructors are present.
     */
    public Bound() {
      this.value = null;
    }

    public Bound(ReservationType booking) {
      this.value = booking;
    }

    @Override
    public Bound bind(String key, String txt) {
      this.value = ReservationType.valueOf(txt);
      return this;
    }

    @Override
    public Optional<Bound> bind(String key, Map<String, String[]> params) {
      String[] arr = params.get(key);
      if (arr != null && arr.length > 0) {
        this.value = ReservationType.valueOf(arr[0]);
        return Optional.of(this);
      }
      else {
        return Optional.empty();
      }
    }

    @Override
    public String unbind(String key) {
      return this.value.name();
    }

    @Override
    public String javascriptUnbind() {
      return this.value.name();
    }

    public ReservationType value() {
      return this.value;
    }
  }

  private ReservationType(int dbIdx, String label, ReservationType parent) {
    this.dbIdx = dbIdx;
    this.parentCategory = parent;
    this.label = label;
  }

  public static ReservationType fromStringInt(String strVal) {
    try {
      int val = Integer.parseInt(strVal);
      return fromInt(val);
    }
    catch (Exception e) {
      return ROOT;
    }
  }

  public static ReservationType fromInt(int value) {
    if (value > 0 && value <= values().length) {
      return ReservationType.values()[value];
    }
    return ROOT;
  }

  public String getLabel() {
    return label;
  }

  public int getIdx() {
    return this.dbIdx;
  }

  public ReservationType getRootLevelType() {
    if (this.parentCategory != ReservationType.ROOT) {
      return this.parentCategory;
    }
    else {
      return this;
    }
  }

  public List<ReservationType> getSubtree() {
    List<ReservationType> result = new ArrayList<ReservationType>();
    result.add(this);
    if (parentCategory == ROOT) {
      for (ReservationType b : ReservationType.values()) {
        if (b.getParent() == this) {
          result.add(b);
        }
      }
    }
    return result;
  }

  public ReservationType getParent() {
    return this.parentCategory;
  }
}
