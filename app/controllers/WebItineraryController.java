package controllers;

import com.google.inject.Inject;
import com.mapped.publisher.actions.Secured;
import com.mapped.publisher.common.*;
import com.mapped.publisher.persistence.TripBrandingMgr;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.umapped.persistence.accountprop.PreferenceMgr;
import com.umapped.persistence.accountprop.TripPrefs;
import com.umapped.persistence.enums.WebItineraryProxy;
import com.umapped.persistence.enums.ReservationType;
import models.publisher.*;
import org.apache.commons.lang3.math.NumberUtils;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.db.DB;
import play.i18n.Lang;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;
import com.avaje.ebean.Ebean;
import scala.App;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by twong on 2014-08-06.
 */


public class WebItineraryController
    extends Controller {

  @Inject
  PreferenceMgr preferenceMgr;

  @Inject
  FormFactory formFactory;

  private static HashMap<String, Method>                   cachedCustomWebItineraryLoginTemplates    = new HashMap<>();
  private static ArrayList<String>                         cmpIdsForDefaultWebItineraryLoginTemplate = new
      ArrayList<>();
  private static HashMap<String, java.lang.reflect.Method> cachedCustomWebItineraryTemplates         = new HashMap<>();
  private static ArrayList<String>                         cmpIdsForDefaultWebItineraryTemplate      = new
      ArrayList<>();

  @With({Secured.class})
  public Result beta(String tripId, String travelerId) {
    return redirect(WebItineraryApiController.getItineraryUrl(tripId));
  }

  @With({Secured.class})
  public Result index(String compositeId, String travellerId, Long accountId) {
    TripInfoView view = new TripInfoView();
    List<Lang> lang = request().acceptLanguages();

    String tripId =  WebItineraryController.getTripIdFromComposite(compositeId);
    String groupId = WebItineraryController.getGroupIdFromComposite(compositeId);

    if (compositeId == null) {
      tripId = flash(SessionConstants.SESSION_PARAM_TRIPID);
      groupId = flash(SessionConstants.SESSION_PARAM_GROUPID);
    }

    Account traveler = null;
    AccountTripLink tripLink = null;

    view.travellerId = travellerId;

    java.lang.reflect.Method render = null;
    String message = flash().get(SessionConstants.SESSION_PARAM_MSG);

    if (tripId != null) {
      Trip trip = Trip.find.byId(tripId);
      TripGroup group = null;

      if (groupId != null) {
        group = TripGroup.findByPK(groupId);
        if (group == null || group.groupid.equals(tripId)) {
          view.message = "This link is not valid. Please contact support@umapped.com";
          return ok(views.html.error.error404.render(view));
        }
      }

      int publishCount = 0;
      if (group != null) {
        publishCount = TripPublishHistory.countPublishedGroup(tripId, groupId);
      }
      else {
        publishCount = TripPublishHistory.countPublishedDefaultGroup(tripId);
      }

      if (trip != null && trip.status != APPConstants.STATUS_DELETED && publishCount > 0) {
        if (accountId != null && accountId > 0L) {
          tripLink = AccountTripLink.findByUid(accountId, trip.getTripid());
          if (tripLink == null) {
            //not linked - so no access
            view.message = "This link is not valid. Please contact support@umapped.com";
            return ok(views.html.error.error404.render(view));
          } else {

            //First, increment Click counts for webIntinerary and set Timestamps
            try {
              Ebean.beginTransaction();
              if (tripLink.getClickCounter() == 0) {
                tripLink.setFirstClickTs(new Timestamp(System.currentTimeMillis()));
              }
              tripLink.setLastClickTs(new Timestamp(System.currentTimeMillis()));
              tripLink.setClickCounter(tripLink.getClickCounter() + 1);
              tripLink.update();
              Ebean.commitTransaction();
            } catch (Exception e) {
              e.printStackTrace();
              Ebean.rollbackTransaction();
            }

            view.travellerId = Communicator.getTravelerId(tripLink);
          }
        }
        //if itineray check is disable, redirect to the itinerary page directly
        Company cmpy = Company.find.byId(trip.cmpyid);

        //let's check if the agent has enabled the beta web itinerary
        //Checking if IE11 or other browsers
        //https://blogs.msdn.microsoft.com/ieinternals/2013/09/21/internet-explorer-11s-many-user-agent-strings/
        String userAgent = ctx().request().getHeader("User-Agent");
        if (userAgent == null) {
          Log.info("WebItineraryController - UserAgent missing. " + tripId);
        }
        if(userAgent != null && !userAgent.contains("MSIE") ) {
          EnumSet<Capability> capabilities = SecurityMgr.getTripCapabilities(trip);
          if (capabilities != null && capabilities.contains(Capability.BETA_WEB_ITINERARY)) {
            if (cmpy.getTag() != null && cmpy.getTag().contains(APPConstants.CMPY_TAG_API_WEB_ITINERARY_AUTO_LOGIN)) {
              return redirect(WebItineraryApiController.getItineraryUrl(trip.tripid, cmpy) + "?redirect=true");
            } else {
              return redirect(WebItineraryApiController.getItineraryUrl(trip.tripid, cmpy));
            }
          }
        }

        view.tripId = tripId;
        if (group != null) {
          view.groupId = group.groupid;
        }
        view.cover = trip.getCoverView();


        if (!cmpy.itinerarycheck) {
          //send a web redirect to the web itinerary - a redirect is used to we can get the timezone from the browser
          view.tripStartDate = Utils.getDateString(trip.starttimestamp);
          view.tripEndDate = Utils.getDateString(trip.endtimestamp);
          view.tripStartDatePrint = Utils.formatTimestamp(trip.getStarttimestamp(), "MMM dd, yyyy",lang().language());
          view.tripEndDatePrint = Utils.formatTimestamp(trip.getEndtimestamp(), "MMM dd, yyyy",lang().language());
        }

        //check for custom templates
        try {
          if (!cmpIdsForDefaultWebItineraryLoginTemplate.contains(trip.getCmpyid())) {
            render = cachedCustomWebItineraryLoginTemplates.get(trip.getCmpyid());
            if (render == null) {
              CmpyCustomTemplate customTemplate = CmpyCustomTemplate.findActiveByCmpyType(trip.getCmpyid(),
                                                                                          CmpyCustomTemplate.TEMPLATE_TYPE.WEB_LOGIN.ordinal());
              if (customTemplate != null && customTemplate.isfiletemplate &&
                  customTemplate.getPath() != null &&
                  customTemplate.getPath().startsWith("views")) {
                final Class<?> clazz = Class.forName(customTemplate.getPath());
                render = clazz.getDeclaredMethod("render", TripInfoView.class);
                cachedCustomWebItineraryLoginTemplates.put(trip.getCmpyid(), render);
              }
            }
          }
        }
        catch (Exception e) {
          e.printStackTrace();
        }

        if (render == null && !cmpIdsForDefaultWebItineraryLoginTemplate.contains(trip.getCmpyid())) {
          cmpIdsForDefaultWebItineraryLoginTemplate.add(trip.getCmpyid());
        }

        long s = System.currentTimeMillis();

        view.tripName = trip.getName();
        view.tripStartDatePrint = Utils.formatTimestamp(trip.getStarttimestamp(), "MMM dd, yyyy",lang().language());
        view.tripEndDatePrint = Utils.formatTimestamp(trip.getEndtimestamp(), "MMM dd, yyyy",lang().language());

        UserProfile up = UserProfile.find.byId(trip.createdby);
        if (up != null) {
          view.agentName = up.firstname + " " + up.lastname;
          view.agentEmail = TripBrandingMgr.getWhieLabelEmail(trip,up.getEmail()); //allow overwrite of email domain for ski.com
          view.agentPhone = up.phone;
          if (up.phone == null || up.phone.trim().length() == 0) {
            view.agentPhone = up.mobile;
          }
          view.agentProfilePhoto = up.profilephotourl;
        }

        //check for co-branding - if there is 0 or 1 cmpy, default to the trip cmpy
        Company mainCmpy = null;
        List<TripBrandingView> brandedCmpies = TripBrandingMgr.findActiveBrandedCmpies(trip);
        TripBrandingView overwritten = null;

        if (brandedCmpies != null && brandedCmpies.size() > 0) {
          mainCmpy = TripBrandingMgr.getMainContact(brandedCmpies);
          view.tripBranding = brandedCmpies;
          //if this is overwritten by tag for the same company e.g. ski.com
          overwritten = TripBrandingMgr.getOverwrittenByTag(brandedCmpies, trip);
        }

        if (mainCmpy == null) {
          mainCmpy = cmpy;
        }

        if (overwritten == null) {
          if (mainCmpy != null) {
            view.agencyName = mainCmpy.name;
            view.agencyLogo = mainCmpy.logourl;
            List<CmpyAddress> addrs = CmpyAddress.findMainActiveByCmpyId(mainCmpy.cmpyid);
            if (addrs != null && addrs.size() > 0) {
              view.agencyEmail = addrs.get(0).email;
              view.agencyPhone = TripBrandingMgr.getCompanyExtension(Account.findActiveByLegacyId(up.getUserid()), addrs.get(0).phone);
              view.agencyWebsite = addrs.get(0).getWeb();
              view.agencyTwitter = addrs.get(0).getTwitter();
              view.agencyFacebook = addrs.get(0).getFacebook();
            }
          }
        } else {
          view.agencyName = overwritten.cmpyName;
          view.agencyLogo = overwritten.cmpyLogoUrl;
          view.agencyEmail = overwritten.cmpyEmail;
          view.agencyPhone = TripBrandingMgr.getCompanyExtension(Account.findActiveByLegacyId(up.getUserid()), overwritten.cmpyPhone);
          view.agencyWebsite = overwritten.cmpyWebsite;
          view.agencyTwitter = overwritten.cmpyTwitter;
          view.agencyFacebook = overwritten.cmpyFacebook;
        }
      }
      else {
        view.message = "Please contact support@umapped.com";
      }
    }
    else {
      view.message = "Please contact support@umapped.com";
    }

    if (message != null && view.message == null) {
      view.message = message;
    }


    if (view.tripId == null) {
      return ok(views.html.error.error404.render(view));
    }
    else {

      if (render != null) {
        try {
          return ok((Html) render.invoke(null, view));
        }
        catch (Exception e) {
          return ok(views.html.webItinerary.index.render(view));
        }
      }
      else {
        return ok(views.html.webItinerary.index.render(view));
      }
    }
  }

  public static String getTripIdFromComposite(String compositeId) {
    if (compositeId != null && compositeId.trim().length() > 0) {
      int index = compositeId.indexOf(APPConstants.TRIP_GROUP_SEPARATOR);
      if (index > 0) {
        return compositeId.substring(0, index);
      }
    }
    return compositeId;
  }

  public static String getGroupIdFromComposite(String compositeId) {
    if (compositeId != null && compositeId.trim().length() > 0) {
      int index = compositeId.indexOf(APPConstants.TRIP_GROUP_SEPARATOR);
      if (index > 0 && (index + APPConstants.TRIP_GROUP_SEPARATOR.length()) < compositeId.length()) {
        return compositeId.substring(index + APPConstants.TRIP_GROUP_SEPARATOR.length());
      }
    }
    return null;
  }

  public Result home(String tripId, String groupId, String passengerId) {
    try {
      final DynamicForm form               = formFactory.form().bindFromRequest();
      String            s                  = form.get("timezoneOffset");
      int               timezoneOffsetMins = 0;
      if (s != null) {
        try {
          timezoneOffsetMins = Integer.parseInt(s) * -1;
        }
        catch (Exception e) {
          timezoneOffsetMins = 0;
        }
      }

      String startDate = form.get("inStartDate");
      String endDate   = form.get("inEndDate");

      if (tripId != null && tripId.length() > 0) {
        Trip trip = Trip.find.byId(tripId);
        if (trip != null && trip.status != APPConstants.STATUS_DELETED) {
          boolean   valid = false;
          TripGroup group = null;
          if (groupId != null && groupId.trim().length() > 0) {
            group = TripGroup.findByPK(groupId);
            if (group == null || !group.tripid.equals(trip.tripid)) {
              TripInfoView view = new TripInfoView();
              view.message = "System Error: tc4438";
              return ok(views.html.error.error404.render(view));
            }
            else if (group.getPublishstatus() != APPConstants.STATUS_PUBLISHED && group.getPublishstatus() !=
                                                                                  APPConstants
                                                                                      .STATUS_PUBLISHED_REVIEW) {
              TripInfoView view = new TripInfoView();
              view.tripId = tripId;
              view.message = "The  link is not valid for this trip. Please try again.";
              flash(SessionConstants.SESSION_PARAM_TRIPID, tripId);
              return ok(views.html.error.error404.render(view));
            }

          }
          List<AccountTripLink> tripLinks = null;
          if (group != null) {
            tripLinks = AccountTripLink.findByTripGroup(tripId, group.groupid);
          }
          else {
            tripLinks = AccountTripLink.findByTripNoGroup(tripId);

          }
          Company cmpy = Company.find.byId(trip.cmpyid);
          //enforce itinerary check depening on company setting
          if (cmpy.itinerarycheck) {
            String tripStartDate = Utils.formatDateControlYYYY(trip.starttimestamp.longValue());
            String tripEndDate   = Utils.formatDateControlYYYY(trip.endtimestamp.longValue());
            if (startDate == null || tripStartDate == null || !startDate.equals(tripStartDate) || endDate == null ||
                tripEndDate == null || !endDate.equals(tripEndDate)) { // || System.currentTimeMillis() > trip
              // .endtimestamp) {

              flash(SessionConstants.SESSION_PARAM_MSG, "The  dates are invalid for this trip. Please try again.");
              flash(SessionConstants.SESSION_PARAM_TRIPID, tripId);
              if (group != null) {
                flash(SessionConstants.SESSION_PARAM_GROUPID, groupId);
              }
              return redirect(routes.WebItineraryController.index(tripId, null, 0L));
            }
          }

          ArrayList<TripBookingDetailView> bookingLocations  = new ArrayList<>();
          ArrayList<DestinationGuideView>  documentLocations = new ArrayList<>();

          long minStartTimestamp = 0;

          TripPreviewView previewView = new TripPreviewView();
          previewView.addCapabilities(SecurityMgr.getTripCapabilities(trip));
          previewView.tripName = trip.name;
          previewView.tripStatus = String.valueOf(trip.status);
          previewView.tripStartDate = Utils.getDateString(trip.starttimestamp);
          previewView.tripEndDate = Utils.getDateString(trip.endtimestamp);
          previewView.tripStartDatePrint = Utils.formatTimestamp(trip.getStarttimestamp(),
                                                                 "MMM dd, yyyy",
                                                                 lang().language());
          previewView.tripEndDatePrint = Utils.formatTimestamp(trip.getEndtimestamp(),
                                                               "MMM dd, yyyy",
                                                               lang().language());
          previewView.tripType = trip.triptype;
          previewView.tripEndDateMs = String.valueOf(trip.endtimestamp);
          previewView.tripStartDateMs = String.valueOf(trip.starttimestamp);
          previewView.tripCoverUrl = trip.getImageUrl();
          previewView.display12Hr = cmpy.display12hrClock();

          previewView.wasPublished = (TripPublishHistory.countPublished(trip.tripid) > 0); //George: toggle for draft
          // B/G in PDF

          if (NumberUtils.isNumber(passengerId)) {
            try {
              AccountTripLink tripLink = AccountTripLink.findByLegacyId(passengerId);
              previewView.communicatorToken = Communicator.Instance().getTravellerFirebaseToken(trip, tripLink);
              previewView.communicatorURI = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.FIREBASE_URI);
              previewView.travellerId = passengerId;
            }
            catch (Exception e) {
              previewView.communicatorToken = null;
              Log.err("User tried to fake ID");
            }
          }
          else {
            Log.info("Web Itinerary rendering: traveller not specified - no Communicator");
          }

          String mapPK = trip.tripid;
          if (group != null) {
            if (group.getAllowselfregistration() == APPConstants.ALLOW_SELF_REGISTRATION) {
              previewView.allowSelfRegistration = true;
            }
            else {
              previewView.allowSelfRegistration = false;
            }
            previewView.groupId = group.groupid;
            mapPK = TourController.getCompositePK(trip.tripid, group.groupid);
          }
          else {
            if (trip.getVisibility() == APPConstants.ALLOW_SELF_REGISTRATION) {
              previewView.allowSelfRegistration = true;
            }
            else {
              previewView.allowSelfRegistration = false;
            }
          }

          if (trip.tag != null && !trip.tag.isEmpty() && TripBrandingMgr.hasOverwriteBrandingTag(trip.tag)) {
            //check for Ski.com tags - do not display app icons for United and Delta
            previewView.allowSelfRegistration = false;

          }

          previewView.onUMapped = false;
          //check if trip is in umapped mobile
          if (previewView.wasPublished) {
            previewView.onUMapped = true;
          }

          //save passengers
          if (tripLinks != null && tripLinks.size() > 0) {
            List<TripPassengerView> passengerInfoViews = new ArrayList<TripPassengerView>();
            for (AccountTripLink tripLink : tripLinks) {
              Account traveler = Account.find.byId(tripLink.getPk().getUid());
              TripPassengerView passengerInfoView = new TripPassengerView();
              passengerInfoView.id = String.valueOf(traveler.getUid());
              passengerInfoView.email = traveler.getEmail();

              passengerInfoViews.add(passengerInfoView);
            }
            previewView.passengers = passengerInfoViews;
          }

          previewView.businessCard = getTripBusinessCard(trip, groupId);

          previewView.tripId = trip.tripid;
          previewView.tripStatus = String.valueOf(trip.status);

          if (trip.starttimestamp > 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(trip.starttimestamp);
            previewView.startYear = String.valueOf(cal.get(Calendar.YEAR));
            previewView.startMonth = String.valueOf(cal.get(Calendar.MONTH));
            previewView.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
            previewView.tripStartDateMs = String.valueOf(trip.starttimestamp);
          }

          if (trip.endtimestamp > 0) {
            previewView.tripEndDateMs = String.valueOf(trip.endtimestamp);
          }

          List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
          if (tripDetails != null) {

            TripBookingView tbv = buildTripBookingView(trip, tripDetails, bookingLocations, passengerId == null);

            //From already created Trip Bookings View creating list of locations
            if (tripDetails != null && tripDetails.size() > 0) {
              for (TripDetail tripDetail : tripDetails) {
                if (minStartTimestamp == 0 || minStartTimestamp > tripDetail.getStarttimestamp()) {
                  minStartTimestamp = tripDetail.getStarttimestamp();
                }
              }
            }


            if (!bookingLocations.isEmpty()) {
              previewView.bookingsLocations = bookingLocations;
            }

            DestinationSearchView dests = TripController.getGuidesImproved(tripId);
            previewView.destinations = dests;
            if (dests != null) {
              for (DestinationView dest : dests.destinationList) {
                if (dest.guides != null) {
                  for (DestinationGuideView guide : dest.guides) {
                    if (guide.groupedViews != null && guide.groupedViews.size() > 0) {
                      for (DestinationGuideView guide1 : guide.groupedViews) {
                        if (guide1.locLat != null &&
                            Float.parseFloat(guide1.locLat) != 0 &&
                            guide1.locLong != null &&
                            Float.parseFloat(guide1.locLong) != 0) {
                          guide1.destinationName = dest.name;
                          documentLocations.add(guide1);
                        }
                      }
                    }
                    else {
                      if (guide.locLat != null && Float.parseFloat(guide.locLat) != 0 && guide.locLong != null &&
                          Float.parseFloat(
                          guide.locLong
                                          ) != 0) {
                        guide.destinationName = dest.name;
                        documentLocations.add(guide);

                      }

                    }
                  }
                  if (!documentLocations.isEmpty()) {
                    previewView.documentLocations = documentLocations;
                  }
                }
              }
            }

            HashMap<Integer, String> destinationTypeDesc = new HashMap<>();
            List<DestinationType>    destinationTypes    = DestinationType.findActive();
            if (destinationTypes != null) {
              ArrayList<GenericTypeView> destinationTypeList = new ArrayList<>();
              for (DestinationType u : destinationTypes) {
                GenericTypeView typeView = new GenericTypeView();
                typeView.id = String.valueOf(u.getDestinationtypeid());
                typeView.name = u.getName();
                destinationTypeList.add(typeView);
                destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
              }
              previewView.destinationTypeList = destinationTypeList;
            }

            String msg = flash(SessionConstants.SESSION_PARAM_MSG);
            previewView.message = msg;
            String activeTab = flash(SessionConstants.SESSION_PARAM_ACTIVE_TAB);
            if (activeTab != null) {
              previewView.activeTab = activeTab;
            }

            //check for file attachments
            //get trip attachments
            previewView.tripAttachments = getTripAttachments(trip.getTripid(), timezoneOffsetMins);

            //set staring date for calendar
            if (minStartTimestamp > 0) {
              previewView.setStartTimestamp(minStartTimestamp);
            }
            else if (trip.starttimestamp > 0) {
              previewView.setStartTimestamp(trip.starttimestamp);
            }

            //if we are within the trip, set the start date to be the current date
            if (System.currentTimeMillis() > trip.starttimestamp &&
                System.currentTimeMillis() < trip.endtimestamp) {
              Calendar cal = Calendar.getInstance();
              cal.setTimeInMillis(System.currentTimeMillis());
              previewView.startYear = String.valueOf(cal.get(Calendar.YEAR));
              previewView.startMonth = String.valueOf(cal.get(Calendar.MONTH));
              previewView.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
            }

            previewView.bookings = tbv;

            //check user preferences for overrides
            TripPublishHistory publishHistory = TripPublishHistory.getLastPublished(tripId, groupId);
            if (publishHistory != null) {
              Account account = Account.findActiveByLegacyId(publishHistory.getCreatedby());
              TripPrefs prefs = preferenceMgr.getTripPref(account);
              if (prefs != null) {
                previewView.display12Hr = prefs.isIs12Hr();
                if (prefs.isPrintPDF() && previewView.bookings != null) {
                  previewView.bookings.printFriendlyPDF = prefs.isPrintPDF;
                }
              }
            }


            //handle cross timezone booking
            if (previewView.bookings != null && previewView.bookings.flights != null &&
                previewView.bookings.flights.size() > 0) {
              previewView.bookings.handleCrosTimezoneFlights();
            }

            String cacheId = previewView.tripId;
            if (previewView.groupId != null) {
              cacheId = cacheId + previewView.groupId;
            }

            CacheMgr.set(APPConstants.CACHE_WEB_ITINERARY_PREFX + cacheId, previewView);

            try {
              if (!cmpIdsForDefaultWebItineraryTemplate.contains(cmpy.getCmpyid())) {
                java.lang.reflect.Method render = cachedCustomWebItineraryTemplates.get(cmpy.getCmpyid());
                if (render == null) {
                  CmpyCustomTemplate customTemplate = CmpyCustomTemplate.findActiveByCmpyType(cmpy.getCmpyid(),
                                                                                              CmpyCustomTemplate
                                                                                                  .TEMPLATE_TYPE
                                                                                                  .WEB_MAIN
                                                                                                  .ordinal());
                  if (customTemplate != null && customTemplate.isfiletemplate && customTemplate.getPath() != null &&
                      customTemplate.getPath().startsWith("views")) {
                    final Class<?> clazz = Class.forName(customTemplate.getPath());
                    render = clazz.getDeclaredMethod("render", TripPreviewView.class);
                    cachedCustomWebItineraryTemplates.put(cmpy.getCmpyid(), render);
                  }
                }
                if (render != null) {
                  return ok((Html) render.invoke(null, previewView));
                }
              }
            }
            catch (Exception e) {
              e.printStackTrace();
            }
            if (!cmpIdsForDefaultWebItineraryTemplate.contains(cmpy.getCmpyid())) {
              cmpIdsForDefaultWebItineraryTemplate.add(cmpy.getCmpyid());
            }

            //Checking if IE11 or other browsers
            //https://blogs.msdn.microsoft.com/ieinternals/2013/09/21/internet-explorer-11s-many-user-agent-strings/
            String userAgent = ctx().request().getHeader("User-Agent");
            if(!userAgent.contains("MSIE") && previewView.communicatorToken != null && previewView.communicatorToken.length() > 0) {
              previewView.isModernBrowser = true;
            }


            //String htmlResult = views.html.webItinerary.home.render(previewView).toString();
            //return ok(htmlResult);
            return ok(views.html.webItinerary.home.render(previewView));
          }
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      TripInfoView view = new TripInfoView();
      view.message = "System Error: tc4882";
      return ok(views.html.error.error404.render(view));
    }
    TripInfoView view = new TripInfoView();
    view.message = "System Error: tc4886";
    return ok(views.html.error.error404.render(view));
  }

  /**
   * @param tripId (must be non-null)
   * @param timezoneOffsetMins timezone offset if we want to specify user timezone timestamp
   * @return never null, return list of attachments
   */
  public static List<AttachmentView> getTripAttachments(String tripId, int timezoneOffsetMins) {
    List<AttachmentView> result = new ArrayList<>();

    List<TripAttachment> attachments = TripAttachment.findByTripId(tripId);

    for (TripAttachment attach : attachments) {
      result.add(BookingController.getAttachmentView(attach, timezoneOffsetMins));
    }

    return result;
  }


  /**
   * Branded trip business card
   * @return
   */
  public static BusinessCardView getTripBusinessCard(Trip trip, String groupId){
    BusinessCardView bcv = new BusinessCardView();
    UserProfile up;

    TripPublishHistory lastPublished = TripPublishHistory.getLastPublished(trip.getTripid(), groupId);
    if (lastPublished != null) {
      up = UserProfile.find.byId(lastPublished.createdby);
    } else {
      up = UserProfile.find.byId(trip.createdby);
    }

    bcv.setFirstName(up.firstname);
    bcv.setLastName(up.lastname);
    bcv.email = TripBrandingMgr.getWhieLabelEmail(trip,up.getEmail()); //allow overwrite of email domain for ski.com
    bcv.phone = up.phone;
    bcv.mobile = up.mobile;
    bcv.profilePhotoUrl = up.profilephoto;
    bcv.webUrl = up.getWeb();
    bcv.socialFacebookUrl = up.facebook;
    bcv.socialTwitterUrl = up.twitter;
    bcv.fax = up.fax;


    //check for co-branding - if there is 0 or 1 cmpy, default to the trip cmpy
    Company                mainCmpy      = null;
    List<TripBrandingView> brandedCmpies = TripBrandingMgr.findActiveBrandedCmpies(trip);
    TripBrandingView overwritten = null;

    if (brandedCmpies != null && brandedCmpies.size() > 0) {
      mainCmpy = TripBrandingMgr.getMainContact(brandedCmpies);
      bcv.brands.addAll(brandedCmpies);
      //if this is overwritten by tag for the same company e.g. ski.com
      overwritten = TripBrandingMgr.getOverwrittenByTag(brandedCmpies, trip);
    }

    if (mainCmpy == null) {
      mainCmpy = Company.find.byId(trip.cmpyid);;
    }

    if (overwritten == null) {
      if (mainCmpy != null) {
        bcv.companyName = mainCmpy.name;
        bcv.companyLogoUrl = mainCmpy.logourl;
        bcv.companyId = mainCmpy.cmpyid;
        bcv.cmpyId = mainCmpy.getCmpyId();
        bcv.parentCompanyId = mainCmpy.parentCmpyId;
        List<CmpyAddress> addrs = CmpyAddress.findMainActiveByCmpyId(mainCmpy.cmpyid);
        if (addrs != null && addrs.size() > 0) {
          if (bcv.phone == null || bcv.phone.isEmpty()) {
            bcv.phone = TripBrandingMgr.getCompanyExtension(Account.findActiveByLegacyId(up.getUserid()), addrs.get(0).phone);
          }
          if (bcv.webUrl == null || bcv.webUrl.isEmpty()) {
            bcv.webUrl = addrs.get(0).web;
          }
        }
      }
    } else {
      bcv.whitelabel = true;
      bcv.companyName = overwritten.cmpyName;
      bcv.companyLogoUrl = overwritten.cmpyLogoUrl;
      bcv.companyEmail = overwritten.cmpyEmail;
      bcv.companyPhone = "";
      bcv.companyUrl = overwritten.cmpyWebsite;
      bcv.phone = TripBrandingMgr.getCompanyExtension(Account.findActiveByLegacyId(up.getUserid()), overwritten.cmpyPhone);

      if (bcv.webUrl == null || bcv.webUrl.isEmpty()) {
        bcv.webUrl = overwritten.cmpyWebsite;
      }
    }

    //if this company has the tag AUTO_LOGIN, then set the whitelabel to true so that the app icons do no show
    if (mainCmpy != null && mainCmpy.getTag() != null && mainCmpy.getTag().contains(APPConstants.CMPY_TAG_API_NO_MOBILE_LINKS)) {
      bcv.whitelabel = true;
    }


    //check to see if th company is part of a consortium
    List<ConsortiumCompany> consortiums = null;
    if (bcv.cmpyId != null && bcv.cmpyId > 0) {
      consortiums = ConsortiumCompany.findByCmpyId(bcv.cmpyId);
    } else {
      consortiums = ConsortiumCompany.findByCmpyId(Company.find.byId(trip.getCmpyid()).getCmpyId());
    }
    if (consortiums != null) {
      for (ConsortiumCompany c : consortiums) {
        Consortium consortium = Consortium.find.byId(c.getConsId());
        if (consortium != null) {
          bcv.addConsortiumNames(consortium.getName());
        }
      }
    }


    if (bcv.socialFacebookUrl == null || bcv.socialFacebookUrl.isEmpty()) {
      bcv.socialFacebookUrl = bcv.companyFacebookUrl;
    }
    if (bcv.socialTwitterUrl == null || bcv.socialTwitterUrl.isEmpty()) {
      bcv.socialTwitterUrl = bcv.companyTwitterUrl;
    }
    return bcv;
  }

  public static TripBookingView buildTripBookingView(Trip trip, List<TripDetail> tripDetails,
                                                     boolean removeTravelerNotes) {

    return buildTripBookingView(trip, tripDetails, null, removeTravelerNotes);

  }

  public static TripBookingView buildTripBookingView(Trip trip,
                                                     List<TripDetail> tripDetails,
                                                     List<TripBookingDetailView> bookingLocations,
                                                     boolean removeTravelerNotes) {
    TripBookingView tripBookingView = new TripBookingView();
    List<Lang> lang = request().acceptLanguages();
    tripBookingView.lang =ctx().lang();
    if (lang != null && lang.size() > 0) {
      tripBookingView.lang = lang.get(0);
    }

    if (trip.starttimestamp > 0) {
      tripBookingView.tripStartDatePrint = Utils.formatTimestamp(trip.getStarttimestamp(),
                                                                 "MMM dd, yyyy",
                                                                 lang().language());
      tripBookingView.tripStartDate = String.valueOf(trip.starttimestamp);
    }

    if (trip.endtimestamp > 0) {
      tripBookingView.tripEndDatePrint = Utils.formatTimestamp(trip.getEndtimestamp(), "MMM dd, yyyy", lang().language());
      tripBookingView.tripEndDate = Utils.getDateString(trip.endtimestamp);
    }

    tripBookingView.tripName = trip.name;
    tripBookingView.tripStatus = trip.status;

    tripBookingView.tripId = trip.tripid;
    tripBookingView.tripCmpyId = trip.cmpyid;
    tripBookingView.comments = Utils.escapeHtml(trip.comments);



    //sort everything chronologically
    Collections.sort(tripDetails, TripDetail.TripDetailComparator);
    for (TripDetail tripDetail : tripDetails) {
      TripBookingDetailView bookingDetails = BookingController.buildBookingView(trip.tripid, tripDetail, true);
      if (bookingDetails != null) {
        if (bookingLocations != null && bookingDetails.locStartLat != null && bookingDetails.locStartLat != 0.0 &&
            bookingDetails.locStartLong != null && bookingDetails.locStartLong != 0.0) {
          bookingLocations.add(bookingDetails);
        }
        if (tripDetail.starttimestamp > 0) {
          Calendar cal = Calendar.getInstance();
          cal.setTimeInMillis(tripDetail.starttimestamp);
          bookingDetails.startYear = String.valueOf(cal.get(Calendar.YEAR));
          bookingDetails.startMonth = String.valueOf(cal.get(Calendar.MONTH));
          bookingDetails.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
          bookingDetails.startHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
          bookingDetails.startMin = String.valueOf(cal.get(Calendar.MINUTE));
          bookingDetails.startDatePrint = Utils.formatTimestamp(tripDetail.starttimestamp,
                                                                "EEE MMM dd, YYYY",
                                                                lang().language());
        }

        if (tripDetail.endtimestamp != null && tripDetail.starttimestamp > 0 && tripDetail.endtimestamp > 0) {
          if ((tripDetail.detailtypeid != null && tripDetail.detailtypeid == ReservationType.FLIGHT) || !tripDetail.endtimestamp.equals(tripDetail.starttimestamp)) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(tripDetail.endtimestamp);
            bookingDetails.endYear = String.valueOf(cal.get(Calendar.YEAR));
            bookingDetails.endMonth = String.valueOf(cal.get(Calendar.MONTH));
            bookingDetails.endDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
            bookingDetails.endHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
            bookingDetails.endMin = String.valueOf(cal.get(Calendar.MINUTE));
            bookingDetails.endDatePrint = Utils.formatTimestamp(tripDetail.endtimestamp,
                "EEE MMM dd, YYYY",
                lang().language());
          }
        }
        tripBookingView.addBookingDetailView(tripDetail.getDetailtypeid(), bookingDetails);
      }
    }
    tripBookingView.notes = BookingNoteController.getAllTripNotesView(trip, null, removeTravelerNotes);
    if (tripBookingView.notes != null && bookingLocations != null) {
      for (TripBookingDetailView note: tripBookingView.notes) {
        if (note.locStartLong != 0.0 && note.locStartLat != 0.0) {
          bookingLocations.add(note);
        }
      }
    }
    if(trip.starttimestamp > 0) {
      tripBookingView.tripStartDatePrint = Utils.formatTimestamp(trip.getStarttimestamp(),
                                                                 "MMM dd, yyyy",
                                                                 lang().language());
      tripBookingView.tripStartDate = String.valueOf(trip.starttimestamp);
    }
    return tripBookingView;
  }


  public Result bookings(String tripId, String groupId, String travellerId) {
    try {
      if (tripId != null) {
        TripPreviewView view = getPreviewView(tripId, groupId);
        if (view != null) {
          return ok(views.html.webItinerary.bookings.allBookings.render(view));
        }
        else {
          BaseView tripView = new BaseView();
          tripView.message = controllers.routes.WebItineraryController.index(tripId, null, 0L).url();
          return ok(views.html.webItinerary.tripid.render(tripView));
        }
      }
    }
    catch (Exception e) {
      Log.err("Error while rendering web itinerary");
      e.printStackTrace();
    }
    BaseView view = new BaseView();
    view.message = "Please contact support@umapped.com";
    return ok(views.html.error.error404.render(view));
  }

  private static TripPreviewView getPreviewView(String tripId, String groupId) {
    String cacheId = tripId;
    if (groupId != null) {
      cacheId = cacheId + groupId;
    }

    TripPreviewView view = (TripPreviewView) CacheMgr.get(APPConstants.CACHE_WEB_ITINERARY_PREFX + cacheId);
    return view;
  }

  public Result calendar(String tripId, String groupId, String travellerId) {
    try {
      if (tripId != null) {
        TripPreviewView view = getPreviewView(tripId, groupId);
        if (view != null) {
          return ok(views.html.webItinerary.calendar.render(view));
        }
        else {
          BaseView tripView = new BaseView();
          tripView.message = controllers.routes.WebItineraryController.index(tripId, null, 0L).url();
          return ok(views.html.webItinerary.tripid.render(tripView));
        }
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }
    BaseView view = new BaseView();
    view.message = "Please contact support@umapped.com";
    return ok(views.html.error.error404.render(view));
  }

  public Result documents(String tripId, String groupId, String travellerId) {
    try {
      if (tripId != null) {
        TripPreviewView view = getPreviewView(tripId, groupId);
        if (view != null) {
          return ok(views.html.webItinerary.documents.render(view));
        }
        else {
          BaseView tripView = new BaseView();
          tripView.message = controllers.routes.WebItineraryController.index(tripId, null, 0L).url();
          return ok(views.html.webItinerary.tripid.render(tripView));
        }
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }
    BaseView view = new BaseView();
    view.message = "Please contact support@umapped.com";
    return ok(views.html.error.error404.render(view));
  }

  public Result document(String tripId, String groupId, String guideId, String travellerId) {
    try {
      if (tripId != null && guideId != null) {
        TripPreviewView view = getPreviewView(tripId, groupId);
        if (view != null && view.destinations != null && view.destinations.destinationList != null) {
          for (DestinationView d : view.destinations.destinationList) {
            if (d.id.equals(guideId)) {
              d.tripId = tripId;
              return ok(views.html.webItinerary.document.render(d));

            }
          }
          BaseView tripView = new BaseView();
          tripView.message = controllers.routes.WebItineraryController.index(tripId, null, 0L).url();
          ;
          return ok(views.html.webItinerary.tripid.render(tripView));

        }
        else {
          BaseView tripView = new BaseView();
          tripView.message = controllers.routes.WebItineraryController.index(tripId, null, 0L).url();
          return ok(views.html.webItinerary.tripid.render(tripView));
        }
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }
    BaseView view = new BaseView();
    view.message = "Please contact support@umapped.com";
    return ok(views.html.error.error404.render(view));
  }

  public Result mobile(String tripId, String groupId, String travellerId, Boolean displayCover) {
    try {
      if (tripId != null) {
        TripPreviewView view = getPreviewView(tripId, groupId);
        if (view != null) {
          view.displayCover = displayCover.booleanValue();
          return ok(views.html.webItinerary.mobile.render(view));
        }
        else {
          BaseView tripView = new BaseView();
          tripView.message = controllers.routes.WebItineraryController.index(tripId, null, 0L).url();
          return ok(views.html.webItinerary.tripid.render(tripView));
        }
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }
    BaseView view = new BaseView();
    view.message = "Please contact support@umapped.com";
    return ok(views.html.error.error404.render(view));
  }

  public Result map(String tripId, String groupId, String travellerId) {
    try {
      if (tripId != null) {
        TripPreviewView view = getPreviewView(tripId, groupId);
        if (view != null) {
          return ok(views.html.webItinerary.map.render(view));
        }
        else {
          BaseView tripView = new BaseView();
          tripView.message = controllers.routes.WebItineraryController.index(tripId, null, 0L).url();
          return ok(views.html.webItinerary.tripid.render(tripView));
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    BaseView view = new BaseView();
    view.message = "Please contact support@umapped.com";
    return ok(views.html.error.error404.render(view));
  }


  //allow external access to a webitinerary for a trip created by the API
  //the lookup using the external unique key to resolve the trip instead of the tripid
  //cmpy is resolved using the api_cmpy_token table - the name for the webitinerary api will match the proxy
  public Result proxyWebItinerary (String proxy, String proxyTripId) {

    BaseView view = new BaseView();
    if (proxy != null && !proxy.isEmpty()) {
      Trip trip = getTripByProxy(proxy, proxyTripId);
      if (trip != null) {
        return redirect(routes.WebItineraryController.index(trip.getTripid(), null, 0L));
      } else {
        Log.err("WebItineraryController:proxyWebItinerary - unknown proxy: " + proxy + "  proxytripid: " +proxyTripId);
      }
    }

    return ok(views.html.error.error404.render(view));
  }

  //allow external access to a email webitinerary for a trip created by the API
  //the lookup using the external unique key to resolve the trip instead of the tripid
  //cmpy is resolved using the api_cmpy_token table - the name for the webitinerary api will match the proxy
  public Result proxyEmailItinerary (String proxy, String proxyTripId) {

    BaseView view = new BaseView();
    if (proxy != null && !proxy.isEmpty()) {
      Trip trip = getTripByProxy(proxy, proxyTripId);
      if (trip != null) {
        String emailBody = buildEmailItineraryBody(trip);
        return ok(emailBody).as("text/html");
      } else {
        Log.err("WebItineraryController:proxyWebItinerary - unknown proxy: " + proxy + "  proxytripid: " +proxyTripId);
      }
    }

    return ok(views.html.error.error404.render(view));

  }

  public Result getEmailItinerary(String tripId) {
    BaseView view = new BaseView();

    Trip trip = Trip.findByPK(tripId);
    if (trip == null ) {
      view.message = "Trip does not exist!!";
      return ok(views.html.error.error404.render(view));
    }

    if (trip != null && trip.status == 0) {
      String emailBody = buildEmailItineraryBody(trip);
      return ok(emailBody).as("text/html");
    }
    view.message = "Trip is not published. Please contact support@umapped.com";
    return ok(views.html.error.error404.render(view));
  }

  private Trip getTripByProxy(String proxy, String proxyTripId) {
    if (proxy != null && proxyTripId != null) {
      ApiCmpyToken token = ApiCmpyToken.findCmpyTokenByTypSrcCmpy(proxy.toUpperCase(), BkApiSrc.getId("WEBITINERARY"));
      if (token != null && proxyTripId != null) {
        Company c = Company.find.byId(token.getuCmpyId());
        if (c != null) {
          //lets find the trip
          List<Trip> trips = Trip.findPublishedTripsByTag(c.getCmpyid(), proxyTripId);
          if (trips != null && trips.size() > 0) {
            return trips.get(0);
          } else {
            Log.err("WebItineraryController:proxyWebItinerary - unknown trip: " + proxy + proxyTripId);
          }
        }
      } else {
        Log.err("WebItineraryController:proxyWebItinerary - unknown proxy: " + proxy + proxyTripId);
      }
    }
    return null;
  }

  public String buildEmailItineraryBody(Trip trip) {

    //Trip trip  = Trip.findByPK(tripId);
    PreferenceMgr preferenceMgr = new PreferenceMgr();
    TripPrefs prefs = null;
    play.api.i18n.Lang lang = new play.api.i18n.Lang("en", "US");
    String emailBody = "";

    try {
      TripPreviewView previewView = getFullEmailView(trip);
      boolean embeddedDeepTables = checkEmbeddedTable(previewView.bookings.notes);

      Html html = (Html) views.html.trip.emailWebItinerary.render(previewView, lang.code(), embeddedDeepTables);
      emailBody = html.toString();

      return emailBody;
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return emailBody;

  }

  public static TripPreviewView getFullEmailView(Trip trip) {

    //Trip trip  = Trip.findByPK(tripId);
    PreferenceMgr preferenceMgr = new PreferenceMgr();
    TripPrefs prefs = null;
    play.api.i18n.Lang lang = new play.api.i18n.Lang("en", "US");

    try {
      TripPreviewView previewView = new TripPreviewView();
      Company cmpy = null;

      previewView.tripCoverUrl = trip.getImageUrl();
      previewView.wasPublished = (TripPublishHistory.countPublished(trip.tripid) > 0); //George: toggle for draft
      // B/G in PDF
      previewView.isPrinterFriendly = true; //Flag to ignore or display booking photos

      //get agent info
      UserProfile up = null;
      TripPublishHistory tp = TripPublishHistory.getLastPublished(trip.tripid, null);
      if (tp != null) {
        up = UserProfile.findByPK(tp.createdby);

      }
      else {
        up = UserProfile.find.byId(trip.createdby);
      }

      if (up != null) {
        AgentView agentView = new AgentView();
        previewView.agent = agentView;
        StringBuffer sb = new StringBuffer();
        if (up.firstname != null) {
          sb.append(up.firstname);
        }
        if (sb.length() > 0 && up.lastname != null) {
          sb.append(" ");
          sb.append(up.lastname);
        }
        agentView.agentName = sb.toString();
        agentView.agentEmail = TripBrandingMgr.getWhieLabelEmail(trip, up.getEmail()); //allow overwrite of email domain
        // for ski.com
        agentView.agentPhone = up.phone;
        agentView.agentMobile = up.mobile;
        agentView.cmpyWeb = up.web;
        agentView.agentFax = up.fax;
        agentView.agentFacebook = up.facebook;
        agentView.agentTwitter = up.twitter;

        TripBrandingView overwritten = null;
        List<TripBrandingView> brandingViews = TripBrandingMgr.findActiveBrandedCmpies(trip);
        if (brandingViews != null && brandingViews.size() > 0) {
          cmpy = TripBrandingMgr.getMainContact(brandingViews);
          previewView.tripBranding = brandingViews;
          //if this is overwritten by tag for the same company e.g. ski.com
          overwritten = TripBrandingMgr.getOverwrittenByTag(brandingViews, trip);
        }

        if (cmpy == null) {
          cmpy = Company.find.byId(trip.cmpyid);
        }

        if (overwritten == null) {
          if (cmpy != null) {
            agentView.cmpyName = cmpy.name;
            if (cmpy.getLogoname() != null &&
                cmpy.getLogoname().length() > 0 &&
                cmpy.getLogourl() != null &&
                cmpy.getLogourl().length() > 0) {
              agentView.cmpyLogo = Utils.escapeS3Umapped_Prd(cmpy.getLogourl());
              agentView.cmpyTag = cmpy.getTag();
            }
          }
          List<CmpyAddress> cmpyAddrs = CmpyAddress.findMainActiveByCmpyId(cmpy.cmpyid);
          if (cmpyAddrs != null && cmpyAddrs.size() > 0) {
            CmpyAddress cmpyAddress = cmpyAddrs.get(0);
            if (agentView.agentPhone == null || agentView.agentPhone.isEmpty()) {
              agentView.agentPhone = TripBrandingMgr.getCompanyExtension(Account.findActiveByLegacyId(up.getUserid()),
                                                                         cmpyAddress.phone);
            }
            if (agentView.cmpyWeb == null || agentView.cmpyWeb.isEmpty()) {
              agentView.cmpyWeb = cmpyAddress.web;
            }

          }
        }
        else {
          agentView.cmpyName = overwritten.cmpyName;
          agentView.cmpyLogo = Utils.escapeS3Umapped_Prd(overwritten.cmpyLogoUrl);
          agentView.cmpyEmail = overwritten.cmpyEmail;
          agentView.cmpyPhone = TripBrandingMgr.getCompanyExtension(Account.findActiveByLegacyId(up.getUserid()),
                                                                    overwritten.cmpyPhone);
          agentView.cmpyTag = overwritten.cmpyTag;
          agentView.cmpyFax = overwritten.cmpyFax;
          agentView.cmpyFacebook = overwritten.cmpyFacebook;
          agentView.cmpyTwitter = overwritten.cmpyTwitter;
          agentView.agentMobile = "";
          if (agentView.agentPhone == null || agentView.agentPhone.length() < 1) {
            agentView.agentPhone = agentView.cmpyPhone;
          }
          if (agentView.cmpyWeb == null || agentView.cmpyWeb.length() < 1) {
            agentView.cmpyWeb = overwritten.cmpyWebsite;
          }
        }
      }

      previewView.businessCard = WebItineraryController.getTripBusinessCard(trip, null);

      ArrayList<TripBookingDetailView> bookingLocations = new ArrayList<TripBookingDetailView>();

      previewView.tripName = trip.name;
      previewView.tripStatus = String.valueOf(trip.status);
      previewView.tripStartDate = Utils.getDateString(trip.starttimestamp);
      previewView.tripEndDate = Utils.getDateString(trip.endtimestamp);
      previewView.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
      previewView.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
      previewView.tripStartDatePrintFull = Utils.getDateStringFullPrint(trip.starttimestamp);
      previewView.tripEndDatePrintFull = Utils.getDateStringFullPrint(trip.endtimestamp);
      previewView.tripStartDateMs = String.valueOf(trip.starttimestamp);
      previewView.tripEndDateMs = String.valueOf(trip.endtimestamp);
      previewView.tripNote = trip.comments;
      if (cmpy != null) {
        previewView.display12Hr = cmpy.display12hrClock();
      }
      //check user preferences for overrides
      if (tp != null) {
        Account account = Account.findActiveByLegacyId(tp.getCreatedby());
        prefs = preferenceMgr.getTripPref(account);
      }
      else {
        Account account = Account.findActiveByLegacyId(trip.getCreatedby());
        prefs = preferenceMgr.getTripPref(account);
      }
      if (prefs != null) {
        previewView.display12Hr = prefs.isIs12Hr();
      }

      previewView.tripTag = trip.tag;


      TripBookingView view = new TripBookingView();
      view.lang = lang;

      previewView.bookings = view;

      previewView.tripId = trip.tripid;
      previewView.tripStatus = String.valueOf(trip.status);
      view.comments = Utils.escapeHtml(trip.comments);

      view.tripId = trip.tripid;
      view.tripCmpyId = trip.cmpyid;

      if (trip.starttimestamp > 0) {
        view.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
        view.tripStartDate = String.valueOf(trip.starttimestamp);


        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(trip.starttimestamp);
        previewView.startYear = String.valueOf(cal.get(Calendar.YEAR));
        previewView.startMonth = String.valueOf(cal.get(Calendar.MONTH));
        previewView.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
      }


      if (trip.endtimestamp > 0) {
        view.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
        view.tripEndDate = Utils.getDateString(trip.endtimestamp);
      }
      view.tripName = trip.name;
      view.tripStatus = trip.status;

      List<TripDetail> tripDetails = TripDetail.findActiveByTripId(trip.tripid);
      if (tripDetails != null) {
        for (TripDetail tripDetail : tripDetails) {
          TripBookingDetailView bookingDetails = BookingController.buildBookingView(trip.tripid, tripDetail, true);
          if (bookingDetails != null) {
            if (bookingDetails.locStartLat != null && bookingDetails.locStartLat != 0.0 && bookingDetails
                                                                                               .locStartLong != null
                && bookingDetails.locStartLong != 0.0) {
              bookingLocations.add(bookingDetails);
            }

            if (tripDetail.starttimestamp > 0) {
              Calendar cal = Calendar.getInstance();
              cal.setTimeInMillis(tripDetail.starttimestamp);
              bookingDetails.startYear = String.valueOf(cal.get(Calendar.YEAR));
              bookingDetails.startMonth = String.valueOf(cal.get(Calendar.MONTH));
              bookingDetails.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
              bookingDetails.startHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
              bookingDetails.startMin = String.valueOf(cal.get(Calendar.MINUTE));

            }
            if (bookingDetails.startDatePrint != null && bookingDetails.startDateMs > 0) {
              bookingDetails.startDatePrint = Utils.formatDatePrint(bookingDetails.startDateMs, lang.toLocale());
            }

            if (tripDetail.endtimestamp > 0) {
              Calendar cal = Calendar.getInstance();
              cal.setTimeInMillis(tripDetail.endtimestamp);
              bookingDetails.endYear = String.valueOf(cal.get(Calendar.YEAR));
              bookingDetails.endMonth = String.valueOf(cal.get(Calendar.MONTH));
              bookingDetails.endDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
              bookingDetails.endHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
              bookingDetails.endMin = String.valueOf(cal.get(Calendar.MINUTE));
            }
            if (bookingDetails.endDatePrint != null && bookingDetails.endDateMs > 0) {
              bookingDetails.endDatePrint = Utils.formatDatePrint(bookingDetails.endDateMs, lang.toLocale());
            }

            view.addBookingDetailView(tripDetail.getDetailtypeid(), bookingDetails);
          }
        }

        if (!bookingLocations.isEmpty()) {
          previewView.bookingsLocations = bookingLocations;
        }
      }
      HashMap<Integer, String> destinationTypeDesc = new HashMap<>();
      List<DestinationType> destinationTypes = DestinationType.findActive();
      if (destinationTypes != null) {
        ArrayList<GenericTypeView> destinationTypeList = new ArrayList<GenericTypeView>();
        for (DestinationType u : destinationTypes) {
          GenericTypeView typeView = new GenericTypeView();
          typeView.id = String.valueOf(u.getDestinationtypeid());
          typeView.name = u.getName();
          destinationTypeList.add(typeView);
          destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());

        }
        previewView.destinationTypeList = destinationTypeList;
      }

      //get trip notes
      previewView.bookings.notes = BookingNoteController.getAllTripNotesView(trip, null, true);


      ArrayList<DestinationView> guides = new ArrayList<>();
      DestinationSearchView dests = TripController.getGuides(view);
      previewView.destinations = dests;
      if (previewView.destinations != null && previewView.destinations.destinationList != null) {
        for (DestinationView v : previewView.destinations.destinationList) {
          DestinationView destView = DestinationController.getDestView(v.id, destinationTypeDesc);
          guides.add(destView);

        }
        previewView.destinations.destinationList = guides;
      }

      //handle cross timezone booking
      if (previewView.bookings != null && previewView.bookings.flights != null && previewView.bookings.flights.size()
                                                                                  > 0) {
        previewView.bookings.handleCrosTimezoneFlights();
      }


      previewView.isPrinterFriendly = true;
      return previewView;
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return null;


  }

  private static boolean checkEmbeddedTable(List<TripBookingDetailView> tripNoteViews) {
    if (tripNoteViews == null) {
      return false;
    }
    for (TripBookingDetailView noteView : tripNoteViews) {
      if (checkEmbeddedTable(noteView.intro)) {
        return true;
      }
    }
    return false;
  }

  public static boolean checkEmbeddedTable(String html) {
    try {
      if (StringUtils.isEmpty(html)) {
        return false;
      }
      Document document = Jsoup.parse(html);
      Elements elements = document.select("table table");
      if (elements != null && elements.size() > 0) {
        return true;
      }
    } catch (Exception e) {
      Log.err("Parsing html failed", e);
    }
    return false;
  }

}
