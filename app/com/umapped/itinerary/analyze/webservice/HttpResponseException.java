package com.umapped.itinerary.analyze.webservice;

/**
 * Created by wei on 2017-06-14.
 */
public class HttpResponseException extends Exception {
  private int statusCode;

  public HttpResponseException(int statusCode) {
    super();
    this.statusCode = statusCode;
  }

  public int getStatusCode() {
    return statusCode;
  }
}
