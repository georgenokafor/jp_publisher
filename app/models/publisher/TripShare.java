package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.utils.Utils;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 2014-05-26.
 */
@Entity @Table(name = "trip_share")
public class TripShare
    extends Model {
  public static Model.Finder<String, TripShare> find = new Finder(TripShare.class);
  @Id
  public String pk;

  @Constraints.Required @ManyToOne @JoinColumn(name = "userid")
  public UserProfile user;
  @Constraints.Required
  public String tripid;
  @Enumerated(value = EnumType.STRING)
  public SecurityMgr.AccessLevel accesslevel;
  public int status;
  public String note;
  public Long senttimestamp;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;

  /**
   * Builds a new TripShare object based on input parameters.
   *
   * @param sessionMgr
   * @param note
   * @param accessLevel
   * @param tripModel
   * @param userProfile
   * @return
   */
  public static TripShare buildTripShare(String userId,
                                         Trip tripModel,
                                         UserProfile userProfile,
                                         String note,
                                         SecurityMgr.AccessLevel accessLevel) {
    TripShare tripShare = new TripShare();

    tripShare.pk = Utils.getUniqueId();
    tripShare.createdby = userId;
    tripShare.modifiedby = tripShare.createdby;
    tripShare.createdtimestamp = System.currentTimeMillis();
    tripShare.lastupdatedtimestamp = tripShare.createdtimestamp;
    tripShare.status = APPConstants.STATUS_ACTIVE;
    tripShare.accesslevel = accessLevel;
    tripShare.tripid = tripModel.getTripid();
    tripShare.senttimestamp = tripShare.createdtimestamp;
    tripShare.user = userProfile;
    tripShare.note = note;

    return tripShare;
  }

  public static List<TripShare> getSharedTrips(String userid) {
    return find.where().eq("userid", userid).eq("status", APPConstants.STATUS_ACTIVE).findList();
  }

  public static List<TripShare> getTripCollaborators(Trip trip) {
    return find.where().eq("tripid", trip.getTripid()).eq("status", APPConstants.STATUS_ACTIVE).findList();
  }

  public static TripShare getSharedTripForUser(String tripId, String userId) {
    return find.where().eq("tripid", tripId).eq("userid", userId).findUnique();
  }

  public static TripShare getParentShare(String tripid, String userId) {
    //TODO: Serguei: this needs to be optimized later
    TripShare tripShare = getSharedTripForUser(tripid, userId);
    if (tripShare != null) {
      return find.where().eq("tripid", tripid).eq("userid", tripShare.createdby).findUnique();
    }
    return null;
  }

  public Long getSenttimestamp() {
    return senttimestamp;
  }

  public void setSenttimestamp(Long senttimestamp) {
    this.senttimestamp = senttimestamp;
  }

  public UserProfile getUser() {
    return user;
  }

  public void setUser(UserProfile user) {
    this.user = user;
  }

  public SecurityMgr.AccessLevel getAccesslevel() {
    return accesslevel;
  }

  public void setAccesslevel(SecurityMgr.AccessLevel accesslevel) {
    this.accesslevel = accesslevel;
  }

  public String getPk() {
    return pk;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }


  public String getTripid() {
    return tripid;
  }

  public void setTripid(String tripid) {
    this.tripid = tripid;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }


  private final static String SHARED_CMPIES =
      "select distinct(c.cmpyid) as cmpyid from trip_share ts, user_cmpy_link ucl, company c where \n" +
      "ts.tripid = :tripid and ts.userid = ucl.userid and ucl.status = 0 and ucl.cmpyid = c.cmpyid and c.status = 0;";



  /**
   * Creates list of cmpies based on shared trips
   *
   * @param tripId        tripid
   * @return Empty list if passed parameters are wrong or no activity for the specified parameters, full list otherwise
   */
  public static List<String> findSharedCmpies(String tripId) {
    List<String> cmpies = new ArrayList<String>();

    //Nothing to see here, wrong parameters to call this perform this query
    if (tripId == null) {
      return cmpies;
    }

    SqlQuery sqlQuery;
    sqlQuery = Ebean.createSqlQuery(SHARED_CMPIES);
    sqlQuery.setParameter("tripid", tripId);



    List<SqlRow> rs = sqlQuery.findList();

    for (SqlRow sr : rs) {
      String cmpyId = sr.getString("cmpyid");
      if (cmpyId != null) {
        cmpies.add(cmpyId);
      }
    }

    return cmpies;
  }
}
