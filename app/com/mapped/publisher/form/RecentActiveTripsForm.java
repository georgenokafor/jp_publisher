package com.mapped.publisher.form;

import com.mapped.publisher.persistence.ActiveTrips;

/**
 * Created by surge on 2014-05-27.
 */
public class RecentActiveTripsForm {

  private int daysAgo = 0;
  private ActiveTrips.TripSearchType searchType;

  public String getInTableName() {
    return inTableName;
  }

  public void setInTableName(String inTableName) {
    this.inTableName = inTableName;
  }

  private String inTableName;

  public int getDaysAgo() {
    return daysAgo;
  }

  public void setDaysAgo(int daysAgo) {
    this.daysAgo = daysAgo;
  }

  public ActiveTrips.TripSearchType getSearchType() {
    return searchType;
  }

  public void setSearchType(String searchType) {
    this.searchType = ActiveTrips.TripSearchType.valueOf(searchType);
  }

  public void setSearchType(ActiveTrips.TripSearchType searchType) {
    this.searchType = searchType;
  }

}
