package com.umapped.itinerary.analyze.model.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Created by wei on 2017-03-29.
 */
public class LocalDateJsonMarshaller {
  private static DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE;

  /**
   * Created by wei on 2017-03-01.
   */
  public static class LocalDateDeserializer extends StdDeserializer<LocalDate> {
    public LocalDateDeserializer() {
      super(LocalDate.class);
    }

    public LocalDateDeserializer(Class<?> vc) {
      super(vc);
    }

    public LocalDateDeserializer(JavaType valueType) {
      super(valueType);
    }

    public LocalDateDeserializer(StdDeserializer<?> src) {
      super(src);
    }

    @Override public LocalDate deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
        throws IOException, JsonProcessingException {
      String text = jsonParser.getText();
      if (StringUtils.isEmpty(text)) {
        return null;
      } else {
        return LocalDate.parse(text, formatter);
      }
    }
  }

  /**
   * Created by wei on 2017-02-28.
   */
  public static class LocalDateSerializer
      extends StdSerializer<LocalDate> {

    public LocalDateSerializer() {
      super(LocalDate.class);
    }

    public LocalDateSerializer(Class<LocalDate> t) {
      super(t);
    }

    public LocalDateSerializer(JavaType type) {
      super(type);
    }

    public LocalDateSerializer(Class<?> t, boolean dummy) {
      super(t, dummy);
    }

    public LocalDateSerializer(StdSerializer<?> src) {
      super(src);
    }

    @Override
    public void serialize(LocalDate LocalDate, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
        throws IOException {
      if (LocalDate != null) {
        jsonGenerator.writeString(formatter.format(LocalDate));
      }
    }
  }
}
