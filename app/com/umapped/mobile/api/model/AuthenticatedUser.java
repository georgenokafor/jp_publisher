package com.umapped.mobile.api.model;

import java.io.Serializable;

/**
 * Created by wei on 2017-07-16.
 */
public class AuthenticatedUser implements Serializable {
  protected String jwtToken;
  protected String sessionId;
  protected String accountId;
  protected String firstName;
  protected String lastName;
  protected String email;


  protected MobileUserRole userRole;

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getJwtToken() {
    return jwtToken;
  }

  public void setJwtToken(String jwtToken) {
    this.jwtToken = jwtToken;
  }

  public String getAccountId() {
    return accountId;
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public MobileUserRole getUserRole() {
    return userRole;
  }

  public void setUserRole(MobileUserRole userRole) {
    this.userRole = userRole;
  }
}
