package com.mapped.publisher.form.template;

import models.publisher.Template;
import play.data.validation.Constraints;

import javax.validation.constraints.NotNull;

/**
 * Created by surge on 2016-01-04.
 */
public class FormUpdateTemplate {
  public String              inTemplateId;
  public String              inTemplateName;
  public String              inTemplateCmpyId;
  public String              inTemplateNote;
  @Constraints.Required
  @NotNull
  public Template.Visibility inTemplateShare;
  public int                 inDuration;
}
