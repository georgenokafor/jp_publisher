package com.umapped.itinerary.analyze.adapters;

import com.mapped.publisher.parse.schemaorg.DateTime;
import com.mapped.publisher.parse.schemaorg.Place;
import com.mapped.publisher.parse.schemaorg.Reservation;
import com.umapped.itinerary.analyze.model.ItineraryItem;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.Location;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by wei on 2017-03-06.
 */
public abstract class AbstractReservationItinearyItemAdapter
    implements ReservationItineraryItemAdapter {
  private static Pattern lastElement = Pattern.compile("[^/]*$");

  @Override public ItineraryItem adapt(Reservation reservation) {
    ItineraryItem item = new ItineraryItem();
    item.setUmId(reservation.umId);
    Matcher m = lastElement.matcher(reservation.additionalType);
    if (m.find()) {
      item.setReservationType(m.group(0));
    }
    item.setType(getItemType(reservation));
    item.setStartDateTime(getStartDateTime(reservation));
    item.setStartLocation(getStartLocation(reservation));
    item.setEndDateTime(getEndDateTime(reservation));
    item.setEndLocation(getEndLocation(reservation));
    return item;
  }


  abstract protected ItineraryItemType getItemType(Reservation reservation);

  protected ZonedDateTime getStartDateTime(Reservation reservation) {
    return getZonedDateTime(reservation.getStartDateTime(), null);
  }

  protected ZonedDateTime getEndDateTime(Reservation reservation) {
    return getZonedDateTime(reservation.getFinishDateTime(), null);
  }

  protected ZonedDateTime getZonedDateTime(DateTime dt, ZoneId zoneId) {
    ZoneId zId = zoneId;
    if (zoneId == null) {
      zId = ZoneId.of("GMT");
    }
    if (dt == null || dt.getTimestamp() == null) {
      return null;
    }
    return ZonedDateTime.ofInstant(dt.getTimestamp().toInstant(), zId);
  }

  protected Location getStartLocation(Reservation reservation) {
    return null;
  }

  protected Location getEndLocation(Reservation reservation) {
    return null;
  }

  protected Location getLocationFromPlace(Place place) {
    Location location = new Location();
    Location.Address address = new Location.Address();
    location.setAddress(address);
    if (place != null) {
      location.setUmPoidId(place.umId);
      location.fromPostalAddress(place.address);
      location.setGeoLocation(Location.GeoLocation.build(place.geo));
      location.setDescription(place.name);
    }
    return location;
  }

  protected long duration(Reservation reservation) {
    DateTime sdt = reservation.getStartDateTime();
    DateTime edt = reservation.getFinishDateTime();
    if (sdt != null && edt != null ) {
      Duration d = Duration.between(sdt.getTime(), edt.getTime());
      return d.toHours();
    } else {
      return -1;
    }
  }
}
