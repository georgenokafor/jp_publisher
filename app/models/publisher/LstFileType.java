package models.publisher;

import com.mapped.publisher.utils.Utils;
import org.apache.commons.io.FilenameUtils;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by surge on 2014-10-16.
 */
@Entity @Table(name = "lst_file_type")
public class LstFileType
    extends Model {

  public static Model.Finder<String, LstFileType> find = new Finder(LstFileType.class);

  @Id
  private String extension;
  private String mimeType;
  private String mimeSubtype;
  private String name;

  public static LstFileType byMine(final String mime){
    if(mime == null){
      return find.byId("uki");
    }
    int slash = mime.indexOf('/');
    if(slash == -1) {
      return find.byId("uki");
    }
    String type = mime.substring(0, slash);
    String subtype = mime.substring(slash+1);
    return find.where().eq("mime_type", type).eq("mime_subtype", subtype).setMaxRows(1).findUnique();
  }

  public  static LstFileType findFromUrl(final String url) {
    //Assuming file is the last parameter of url
    return findFromFileName(Utils.getFilenameFromUlr(url));
  }

  public static  LstFileType findFromFileName(String fileName) {
    String ext = FilenameUtils.getExtension(fileName).toLowerCase();
    int garbageIdx = ext.indexOf('?');
    if (garbageIdx > -1) {
      ext = ext.substring(0, garbageIdx);
    }
    garbageIdx = ext.indexOf('&');
    if (garbageIdx > -1) {
      ext = ext.substring(0, garbageIdx);
    }

    LstFileType result = find.byId(ext);
    if (result == null) {
      result = find.byId("uki"); //User is always right - let's deal with it later
    }
    return result;
  }

  public static List<LstFileType> getImageExtensions() {
    return find.where().eq("mime_type", "image").findList();
  }

  public String getExtension() {
    return extension;
  }

  public void setExtension(String extension) {
    this.extension = extension;
  }

  public String getMimeType() {
    return mimeType;
  }

  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  public String getMimeSubtype() {
    return mimeSubtype;
  }

  public void setMimeSubtype(String mimeSubtype) {
    this.mimeSubtype = mimeSubtype;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getProperMimeType() {
    return mimeType + "/" + mimeSubtype;
  }
}
