package com.umapped.itinerary.analyze.supplier.wcities;

/**
 * Created by wei on 2017-03-31.
 */
public interface Constants {
  String GET_EVENTS = "/event_api/getEvents.php";
  String GET_RECORDS = "/record_api/getRecords.php";
  String GET_GUIDES = "/cityguides_api/getCityGuides.php";

  String OAUTH_TOKEN = "oauth_token";
  String API_KEY = "22fe28392506005b58e449c469c55ec1";
  String LATITUDE = "lat";
  String LONGITUDE = "lon";
  String RADIUS_IN_MILE = "miles";
  String START_DATE = "start";
  String END_DATE = "end";
  String CATEGORY = "cat";
  String CITY_ID = "cityId";
  String SEARCH_FOR = "searchFor";
  String LIMIT = "limit";
  String SORT="sort";
  String SEARCH_TAG = "searchTag";
  int MAX_W_CITIES = 10;


  String EVENT_CATEGORY_CONCERT = "900";
  String EVENT_CATEGORY_THEATER = "901";
  String EVENT_CATEGORY_SPORT = "902";
  String EVENT_CATEGORY_MUSEUM = "903";
  String EVENT_CATEGORY_DANCE = "904";
  String EVENT_CATEGORY_CLUB = "905";
  String EVENT_CATEGORY_EDUCATION = "906";
  String EVENT_CATEGORY_FESTIVAL = "907";
  String EVENT_CATEGORY_FAMILY = "908";
  String EVENT_CATEGORY_COMMUNITY = "909";
  String EVENT_CATEGORY_BUSINESS = "910";
  String EVENT_CATEGORY_TOUR = "911";

  String RECORD_CATEGORY_ACCOMONDATION = "1";
  String RECORD_CATEGORY_ATTACTION = "5";

  String ACCOMONDATION_TYPE = "accomType";

  String FEATURE_NEARBY = "NearBy";
  String FEATURE_CITY = "UMappedCity";

  String FEATURE_GUIDED_TOUR = "GuidedTour";
  String FEATURE_ENTERTAINMENT = "Entertainment";
  String FEATURE_CHILDREN = "Children";
  String FEATURE_MUSEUM = "Museum";

}
