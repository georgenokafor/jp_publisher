package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2012-11-22
 * Time: 9:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class PageMgrExt extends PageMgr {
    private static final String SELECT_ALL_BY_OWNER_LASTUPATE = "Select p.Pk, p.MapsId, p.Name, p.Note, p.PageDate, p.PageRank, p.TemplateId, p.ModifiedBy, p.CreatedBy, p.Status, p.SyncTimestamp, p.LastUpdateTimestamp, p.CreateTimestamp, p.MilestonesId, p.PageType, p.PageTag from Page p, Maps m where m.createdby = ? and p.mapsid = m. and (p.SyncTimestamp = 0 or p.SyncTimestamp > ?);";
    private static final String SELECT_SHARED_MAPS_BY_USER_LASTUPATE = "Select Pk, MapsId, Name, Note, PageDate, PageRank, TemplateId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp, MilestonesId, PageType, PageTag from Page where MapsId in (select mapsId from sharedmaps where shareuserid = ? and status = 0) and (SyncTimestamp = 0 or SyncTimestamp > ?);";
    private static final String SELECT_NEW_SHARED_MAPS_BY_USER_LASTUPATE = "Select Pk, MapsId, Name, Note, PageDate, PageRank, TemplateId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp, MilestonesId, PageType, PageTag from Page where MapsId in (select mapsId from sharedmaps where shareuserid = ? and status = 0 and (SyncTimestamp = 0 or SyncTimestamp > ?) and createtimestamp = lastupdatetimestamp);";
    private static final String SELECT_ALL_ACTIVE_BY_MAPS_ID = "Select m.Pk, m.MapsId, m.Name, m.Note, m.PageDate, m.PageRank, m.TemplateId, m.ModifiedBy, m.CreatedBy, m.Status, m.SyncTimestamp, m.LastUpdateTimestamp, m.CreateTimestamp, m.MilestonesId, m.PageType, m.PageTag from Page m, Milestones mi where m.MapsId=? and m.Status = 0 and m.MilestonesId is not null and m.MilestonesId = mi.pk ORDER BY NULLIF(mi.startdate, '')::bigint, m.PageRank, m.CreateTimestamp;";
    private static final String INSERT = "insert into Page (Pk, MapsId, Name, Note, PageDate, PageRank, TemplateId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp, MilestonesId, PageType, PageTag) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String UPDATE = "update Page SET MapsId=?, Name=?, Note=?, PageDate=?, PageRank=?, TemplateId=?, ModifiedBy=?, Status=?, SyncTimestamp=?, LastUpdateTimestamp=?, MilestonesId=?, PageType=?, PageTag=? WHERE Pk = ?; ";

    public static ArrayList<Page> getAllByOwnerSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            prep = conn.prepareStatement(SELECT_ALL_BY_OWNER_LASTUPATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            rs = prep.executeQuery();
            return PageMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList<Page> getSharedByUserSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            prep = conn.prepareStatement(SELECT_SHARED_MAPS_BY_USER_LASTUPATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            rs = prep.executeQuery();
            return PageMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList<Page> getNewSharedByUserSinceLastUpdate(String userId, long lastUpdate) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            prep = conn.prepareStatement(SELECT_NEW_SHARED_MAPS_BY_USER_LASTUPATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdate);
            rs = prep.executeQuery();
            return PageMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <Page> getAllActiveByMapsId(String id) throws SQLException {
        ResultSet rs = null;
        Connection conn= null;
        PreparedStatement prep= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            prep = conn.prepareStatement(SELECT_ALL_ACTIVE_BY_MAPS_ID);
            prep.setString(1, id);
            rs = prep.executeQuery();
            return PageMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static void insert (Page c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(INSERT);
            prep.setString(1, c.getPk());
            prep.setString(2, c.getMapsId());
            prep.setString(3, c.getName());
            prep.setString(4, c.getNote());
            prep.setString(5, c.getPageDate());
            prep.setString(6, c.getPageRank());
            prep.setString(7, c.getTemplateId());
            prep.setString(8, c.getModifiedBy());
            prep.setString(9, c.getCreatedBy());
            prep.setLong(10, c.getStatus());
            prep.setLong(11, c.getSyncTimestamp());
            prep.setLong(12, c.getLastUpdateTimestamp());
            prep.setLong(13, c.getCreateTimestamp());
            prep.setString(14, c.getMilestonesId());
            prep.setString(15, c.getPageType());
            prep.setString(16, c.getPageTag());

            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

    public static int update (Page c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(UPDATE);
            prep.setString(1, c.getMapsId());
            prep.setString(2, c.getName());
            prep.setString(3, c.getNote());
            prep.setString(4, c.getPageDate());
            prep.setString(5, c.getPageRank());
            prep.setString(6, c.getTemplateId());
            prep.setString(7, c.getModifiedBy());
            prep.setLong(8, c.getStatus());
            prep.setLong(9, c.getSyncTimestamp());
            prep.setLong(10, c.getLastUpdateTimestamp());
            prep.setString(11, c.getMilestonesId());
            prep.setString(12, c.getPageType());
            prep.setString(13, c.getPageTag());
            prep.setString(14, c.getPk());
            return prep.executeUpdate();
        } finally {
            if (prep != null)
                prep.close();
        }
    }



}
