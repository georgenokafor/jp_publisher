package com.mapped.publisher.persistence;

import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.view.FilterType;
import models.publisher.Trip;

import java.sql.*;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-04-15
 * Time: 9:19 AM
 * To change this template use File | Settings | File Templates.
 */
public class TripMgr {

  public final static String HAS_COLLABORTORS = "select count(*) as c" +
                                                " from trip_audit  " +
                                                " where userid not in (select userid from user_cmpy_link where cmpyid = ?) and tripid = ? " +
                                                " and userid not in (select userid from user_profile where usertype = 0);";

  public final static String MY_TRIPS = "select * from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike" +
                                        " ? or createdby ilike ? or tag ilike ?) and starttimestamp >= ? and endtimestamp <= ?  and (createdby = ? or createdby in (select cl" +
                                        ".userid from cmpy_group_members gm, user_cmpy_link cl where gm.groupid in " +
                                        "(select cg.groupid from cmpy_group cg, cmpy_group_members gm, " +
                                        "user_cmpy_link cl where cl.userid = ? and cl.pk = gm.usercmpylinkpk and gm" +
                                        ".groupid = cg.groupid and cg.cmpyid = ANY(?) and cl.status = 0 and cg.status" +
                                        " = 0 and gm.status = 0) and cl.pk = gm.usercmpylinkpk and cl.userid <> ?)) " +
                                        "order by %s %s OFFSET ? ";

  public final static String MY_TRIPS_IN_PROGRESS = "select * from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike" +
          " ? or createdby ilike ? or tag ilike ?) and starttimestamp <= ? and endtimestamp >= ? and (createdby = ? or createdby in (select cl" +
          ".userid from cmpy_group_members gm, user_cmpy_link cl where gm.groupid in " +
          "(select cg.groupid from cmpy_group cg, cmpy_group_members gm, " +
          "user_cmpy_link cl where cl.userid = ? and cl.pk = gm.usercmpylinkpk and gm" +
          ".groupid = cg.groupid and cg.cmpyid = ANY(?) and cl.status = 0 and cg.status" +
          " = 0 and gm.status = 0) and cl.pk = gm.usercmpylinkpk and cl.userid <> ?)) " +
          "order by %s %s OFFSET ? ";

  public final static String MY_TRIPS_ACTIVE = "select * from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike" +
          " ? or createdby ilike ? or tag ilike ?) and endtimestamp >= ? and (createdby = ? or createdby in (select cl" +
          ".userid from cmpy_group_members gm, user_cmpy_link cl where gm.groupid in " +
          "(select cg.groupid from cmpy_group cg, cmpy_group_members gm, " +
          "user_cmpy_link cl where cl.userid = ? and cl.pk = gm.usercmpylinkpk and gm" +
          ".groupid = cg.groupid and cg.cmpyid = ANY(?) and cl.status = 0 and cg.status" +
          " = 0 and gm.status = 0) and cl.pk = gm.usercmpylinkpk and cl.userid <> ?)) " +
          "order by %s %s OFFSET ? ";

  public final static String MY_TRIPS_INC_SHARE = "select * from trip where status = ANY(?) and  endtimestamp > ? and ((cmpyid = ANY(?) and (name ilike" +
                                        " ? or createdby ilike ?)  and (createdby = ? or createdby in (select cl" +
                                        ".userid from cmpy_group_members gm, user_cmpy_link cl where gm.groupid in " +
                                        "(select cg.groupid from cmpy_group cg, cmpy_group_members gm, " +
                                        "user_cmpy_link cl where cl.userid = ? and cl.pk = gm.usercmpylinkpk and gm" +
                                        ".groupid = cg.groupid and cg.cmpyid = ANY(?) and cl.status = 0 and cg.status" +
                                        " = 0 and gm.status = 0) and cl.pk = gm.usercmpylinkpk and cl.userid <> ?))) or  " +
                                        " (tripid in (select tripid from trip_share where status = 0 and userid = ? and accesslevel in ('APPEND','APPEND_N_PUBLISH','OWNER')) and name ilike ?)) "  +
                                        "order by name OFFSET ? ";

  public final static String ADMIN_TRIPS_INC_SHARE = "select * from trip where status = ANY(?) and  endtimestamp > ? and ((cmpyid = ANY(?) and name ilike ?) " +
                                                  " or  " +
                                                  " (tripid in (select tripid from trip_share where status = 0 and userid = ? and accesslevel in ('APPEND','APPEND_N_PUBLISH','OWNER')) and name ilike ?)) "  +
                                                  "order by name OFFSET ? ";

  public final static String TIMED_TRIPS_CREATED_BY_ME = "select * from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike ? or tag ilike ?)" +
      " and starttimestamp >= ? and endtimestamp <= ? and createdby = ? " +
      "order by %s %s OFFSET ? ";

  public final static String TIMED_TRIPS_CREATED_BY_ME_IN_PROGRESS = "select * from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike ? or tag ilike ?)" +
          " and starttimestamp <= ? and endtimestamp >= ? and createdby = ? " +
          "order by %s %s OFFSET ? ";

  public final static String TIMED_TRIPS_CREATED_BY_ME_ACTIVE = "select * from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike ? or tag ilike ?)" +
          " and endtimestamp >= ? and createdby = ? " +
          "order by %s %s OFFSET ? ";

  public final static String MY_TRIPS_INCLUDE_ARCHIVES = "select * from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike" +
      " ? or createdby ilike ? or tag ilike ?) and starttimestamp >= ? and endtimestamp <= ? and (createdby = ? or createdby in (select cl" +
      ".userid from cmpy_group_members gm, user_cmpy_link cl where gm.groupid in " +
      "(select cg.groupid from cmpy_group cg, cmpy_group_members gm, " +
      "user_cmpy_link cl where cl.userid = ? and cl.pk = gm.usercmpylinkpk and gm" +
      ".groupid = cg.groupid and cg.cmpyid = ANY(?) and cl.status = 0 and cg.status" +
      " = 0 and gm.status = 0) and cl.pk = gm.usercmpylinkpk and cl.userid <> ?)) " +
      "order by %s %s OFFSET ? ";

  public final static String MY_TRIPS_INCLUDE_ARCHIVES_IN_PROGRESS = "select * from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike" +
          " ? or createdby ilike ? or tag ilike ?) and starttimestamp <= ? and endtimestamp >= ? and (createdby = ? or createdby in (select cl" +
          ".userid from cmpy_group_members gm, user_cmpy_link cl where gm.groupid in " +
          "(select cg.groupid from cmpy_group cg, cmpy_group_members gm, " +
          "user_cmpy_link cl where cl.userid = ? and cl.pk = gm.usercmpylinkpk and gm" +
          ".groupid = cg.groupid and cg.cmpyid = ANY(?) and cl.status = 0 and cg.status" +
          " = 0 and gm.status = 0) and cl.pk = gm.usercmpylinkpk and cl.userid <> ?)) " +
          "order by %s %s OFFSET ? ";

  public final static String MY_TRIPS_INCLUDE_ARCHIVES_ACTIVE = "select * from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike" +
          " ? or createdby ilike ? or tag ilike ?) and endtimestamp >= ? and (createdby = ? or createdby in (select cl" +
          ".userid from cmpy_group_members gm, user_cmpy_link cl where gm.groupid in " +
          "(select cg.groupid from cmpy_group cg, cmpy_group_members gm, " +
          "user_cmpy_link cl where cl.userid = ? and cl.pk = gm.usercmpylinkpk and gm" +
          ".groupid = cg.groupid and cg.cmpyid = ANY(?) and cl.status = 0 and cg.status" +
          " = 0 and gm.status = 0) and cl.pk = gm.usercmpylinkpk and cl.userid <> ?)) " +
          "order by %s %s OFFSET ? ";

  public final static String MY_TRIPS_COUNT = "select count(*) from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike" +
      " ? or createdby ilike ? or tag ilike ?) and starttimestamp >= ? and endtimestamp <= ? and (createdby = ? or createdby in (select cl" +
      ".userid from cmpy_group_members gm, user_cmpy_link cl where gm.groupid in " +
      "(select cg.groupid from cmpy_group cg, cmpy_group_members gm, " +
      "user_cmpy_link cl where cl.userid = ? and cl.pk = gm.usercmpylinkpk and gm" +
      ".groupid = cg.groupid and cg.cmpyid = ANY(?) and cl.status = 0 and cg.status" +
      " = 0 and gm.status = 0) and cl.pk = gm.usercmpylinkpk and cl.userid <> ?)) ";

  public final static String MY_TRIPS_COUNT_IN_PROGRESS = "select count(*) from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike" +
          " ? or createdby ilike ? or tag ilike ?) and starttimestamp <= ? and endtimestamp >= ? and (createdby = ? or createdby in (select cl" +
          ".userid from cmpy_group_members gm, user_cmpy_link cl where gm.groupid in " +
          "(select cg.groupid from cmpy_group cg, cmpy_group_members gm, " +
          "user_cmpy_link cl where cl.userid = ? and cl.pk = gm.usercmpylinkpk and gm" +
          ".groupid = cg.groupid and cg.cmpyid = ANY(?) and cl.status = 0 and cg.status" +
          " = 0 and gm.status = 0) and cl.pk = gm.usercmpylinkpk and cl.userid <> ?)) ";

  public final static String MY_TRIPS_COUNT_ACTIVE = "select count(*) from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike" +
          " ? or createdby ilike ? or tag ilike ?) and endtimestamp >= ? and (createdby = ? or createdby in (select cl" +
          ".userid from cmpy_group_members gm, user_cmpy_link cl where gm.groupid in " +
          "(select cg.groupid from cmpy_group cg, cmpy_group_members gm, " +
          "user_cmpy_link cl where cl.userid = ? and cl.pk = gm.usercmpylinkpk and gm" +
          ".groupid = cg.groupid and cg.cmpyid = ANY(?) and cl.status = 0 and cg.status" +
          " = 0 and gm.status = 0) and cl.pk = gm.usercmpylinkpk and cl.userid <> ?)) ";

  public final static String TIMED_TRIPS_CREATED_BY_ME_COUNT = "select count(*) from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike ? or tag ilike ?)" +
      " and starttimestamp >= ? and endtimestamp <= ? and createdby = ? ";

  public final static String TIMED_TRIPS_CREATED_BY_ME_COUNT_IN_PROGRESS = "select count(*) from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike ? or tag ilike ?)" +
          " and starttimestamp <= ? and endtimestamp >= ? and createdby = ? ";

  public final static String TIMED_TRIPS_CREATED_BY_ME_COUNT_ACTIVE = "select count(*) from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike ? or tag ilike ?)" +
          " and endtimestamp >= ? and createdby = ? ";

  public final static String MY_TRIPS_INCLUDE_ARCHIVES_COUNT = "select count(*) from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike" +
      " ? or createdby ilike ? or tag ilike ?) and starttimestamp >= ? and endtimestamp <= ? and (createdby = ? or createdby in (select cl" +
      ".userid from cmpy_group_members gm, user_cmpy_link cl where gm.groupid in " +
      "(select cg.groupid from cmpy_group cg, cmpy_group_members gm, " +
      "user_cmpy_link cl where cl.userid = ? and cl.pk = gm.usercmpylinkpk and gm" +
      ".groupid = cg.groupid and cg.cmpyid = ANY(?) and cl.status = 0 and cg.status" +
      " = 0 and gm.status = 0) and cl.pk = gm.usercmpylinkpk and cl.userid <> ?)) ";

  public final static String MY_TRIPS_INCLUDE_ARCHIVES_COUNT_IN_PROGRESS = "select count(*) from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike" +
          " ? or createdby ilike ? or tag ilike ?) and starttimestamp <= ? and endtimestamp >= ? and (createdby = ? or createdby in (select cl" +
          ".userid from cmpy_group_members gm, user_cmpy_link cl where gm.groupid in " +
          "(select cg.groupid from cmpy_group cg, cmpy_group_members gm, " +
          "user_cmpy_link cl where cl.userid = ? and cl.pk = gm.usercmpylinkpk and gm" +
          ".groupid = cg.groupid and cg.cmpyid = ANY(?) and cl.status = 0 and cg.status" +
          " = 0 and gm.status = 0) and cl.pk = gm.usercmpylinkpk and cl.userid <> ?)) ";

  public final static String MY_TRIPS_INCLUDE_ARCHIVES_COUNT_ACTIVE = "select count(*) from trip where status = ANY(?) and cmpyid = ANY(?) and (name ilike" +
          " ? or createdby ilike ? or tag ilike ?) and endtimestamp >= ? and (createdby = ? or createdby in (select cl" +
          ".userid from cmpy_group_members gm, user_cmpy_link cl where gm.groupid in " +
          "(select cg.groupid from cmpy_group cg, cmpy_group_members gm, " +
          "user_cmpy_link cl where cl.userid = ? and cl.pk = gm.usercmpylinkpk and gm" +
          ".groupid = cg.groupid and cg.cmpyid = ANY(?) and cl.status = 0 and cg.status" +
          " = 0 and gm.status = 0) and cl.pk = gm.usercmpylinkpk and cl.userid <> ?)) ";

  public final static String SHARED_TRIPS = "select   t.tripid as tripid, t.name as name, t.cmpyid as cmpyid, " +
                                            "t.accesscode as accesscode, t.comments as comments, " +
                                            "t.coverfilename as coverfilename, t.coverurl as coverurl, " +
                                            "t.createdby as createdby, t.createdtimestamp as createdtimestamp, " +
                                            "t.endtimestamp as endtimestamp, " +
                                            "t.lastupdatedtimestamp as lastupdatedtimestamp, t.modifiedby, " +
                                            "t.starttimestamp as starttimestamp, t.status as status, " +
                                            "t.triptype as triptype, t.version as version, " +
                                            "t.visibility as visibility, ts.accesslevel from trip t, trip_share ts where t.status = ANY(?) and t.cmpyid = ANY(?) and (t.name " +
                                            "ilike ? or t.createdby ilike ? or t.tag ilike ?)  and (t.tripid in (select tripid from " +
                                            "trip_share where status = 0 and userid = ?)) and ts.tripid = t.tripid and ts.userid =? order by  starttimestamp desc, name asc OFFSET ? ";

  public final static String RECENT_SHARED_TRIPS = "select   t.tripid as tripid, t.name as name, t.cmpyid as cmpyid, " +
                                                   "t.accesscode as accesscode, t.comments as comments, " +
                                                   "t.coverfilename as coverfilename, t.coverurl as coverurl, " +
                                                   "t.createdby as createdby, t.createdtimestamp as createdtimestamp," +
                                                   " t.endtimestamp as endtimestamp, " +
                                                   "t.lastupdatedtimestamp as lastupdatedtimestamp, t.modifiedby, " +
                                                   "t.starttimestamp as starttimestamp, t.status as status, " +
                                                   "t.triptype as triptype, t.version as version, " +
                                                   "t.visibility as visibility, ts.accesslevel from trip t, trip_share ts where t.status <> -1  and (t.tripid in (select " +
                                                  "tripid from " + "trip_share where status = 0 and userid = ?)) and ts.tripid = t.tripid and ts.userid = ? " +
                                                  "order by starttimestamp desc, name asc OFFSET ? ";

  public final static String DASHBOARD_SHARED_TRIPS = "select   t.tripid as tripid, t.name as name, t.cmpyid as cmpyid, " +
                                                   "t.accesscode as accesscode, t.comments as comments, " +
                                                   "t.coverfilename as coverfilename, t.coverurl as coverurl, " +
                                                   "t.createdby as createdby, t.createdtimestamp as createdtimestamp," +
                                                   " t.endtimestamp as endtimestamp, " +
                                                   "t.lastupdatedtimestamp as lastupdatedtimestamp, t.modifiedby, " +
                                                   "t.starttimestamp as starttimestamp, t.status as status, " +
                                                   "t.triptype as triptype, t.version as version, " +
                                                   "t.visibility as visibility, ts.accesslevel from trip t, trip_share ts where t.status <> -1  and (t.tripid in (select " +
                                                   "tripid from " + "trip_share where status = 0 and userid = ?)) and ts.tripid = t.tripid and ts.userid = ? and (upper(t.name) like ? or upper(t.tag) like ?) " +
                                                   "order by %s %s, name asc OFFSET ? ";

  public final static String DASHBOARD_SHARED_TRIPS_COUNT = "select   count(t.tripid) from trip t, trip_share ts where t.status <> -1  and (t.tripid in (select " +
                                                            "tripid from " + "trip_share where status = 0 and userid = ?)) and ts.tripid = t.tripid and ts.userid = ? " ;

  public final static String DASHBOARD_SHARED_TRIPS_COUNT_FILTERED = "select   count(t.tripid) from trip t, trip_share ts where t.status <> -1  and (t.tripid in (select " +
                                                                     "tripid from " + "trip_share where status = 0 and userid = ?)) and ts.tripid = t.tripid and ts.userid = ? and (upper(t.name) like ? or upper(t.tag) like ?) " ;


  public static boolean hasCollaboratorEntries (String tripId, String cmpyId) throws SQLException{
    Connection conn = null;
    ArrayList<Trip> results = new ArrayList<>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    try {
      conn = DBConnectionMgr.getConnection4Publisher();

      prep = conn.prepareStatement(HAS_COLLABORTORS);
      prep.setString(1, cmpyId);
      prep.setString(2, tripId);
      rs = prep.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          int count = rs.getInt("c");
          if (count > 0) {
            return true;
          }
        }
      }
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }

      try {
        if (conn != null) {
          conn.close();
        }
      }catch (Exception e) {
        e.printStackTrace();
      }
    }

    return false;
  }

  public static List<Trip> findMyTrips(String term,
                                       List<String> cmpyIds,
                                       String userId,
                                       int maxRows,
                                       int startpos,
                                       ArrayList<Integer> statuses,
                                       Connection conn,
                                       String sortCol,
                                       String sortOrder,
                                       FilterType filterDate,
                                       long currentClientTime)
      throws SQLException {
    ArrayList<Trip> results = new ArrayList<>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {
      String colName = "";
      if(sortCol.equals("imgUrl") || sortCol.equals("tripName")) {
        colName = "name";
      }
      else {
        colName = "starttimestamp";
      }

      long timestamp = 0;
      long startTimestamp = 0;

      if(filterDate == FilterType.INPROGRESS) {
        startTimestamp = currentClientTime;
        timestamp = currentClientTime;
      }
      else if(filterDate == FilterType.FUTURE) {
        startTimestamp = currentClientTime;
        timestamp = Long.MAX_VALUE;
      }
      else if(filterDate == FilterType.PAST) {
        startTimestamp = 0;
        timestamp = currentClientTime;
      }
      else if(filterDate == FilterType.ACTIVE) {
        startTimestamp = 0;
        timestamp = currentClientTime;
      }
      else if(filterDate == FilterType.ALL) {
        startTimestamp = 0;
        timestamp = Long.MAX_VALUE;
      }


      if(filterDate == FilterType.INPROGRESS) {
        String sql = String.format(MY_TRIPS_IN_PROGRESS, colName, sortOrder);
        prep = conn.prepareStatement(sql);
        prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
        prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
        prep.setString(3, term);
        prep.setString(4, term);
        prep.setString(5, term);
        prep.setLong(6, startTimestamp);
        prep.setLong(7, timestamp);
        prep.setString(8, userId);
        prep.setString(9, userId);
        prep.setArray(10, conn.createArrayOf("text", cmpyIds.toArray()));
        prep.setString(11, userId);
        prep.setInt(12, startpos);
      }
      else if (filterDate == FilterType.ACTIVE){
        String sql = String.format(MY_TRIPS_ACTIVE, colName, sortOrder);
        prep = conn.prepareStatement(sql);
        prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
        prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
        prep.setString(3, term);
        prep.setString(4, term);
        prep.setString(5, term);
        prep.setLong(6, timestamp);
        prep.setString(7, userId);
        prep.setString(8, userId);
        prep.setArray(9, conn.createArrayOf("text", cmpyIds.toArray()));
        prep.setString(10, userId);
        prep.setInt(11, startpos);
      }
      else {
        String sql = String.format(MY_TRIPS, colName, sortOrder);
        prep = conn.prepareStatement(sql);
        prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
        prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
        prep.setString(3, term);
        prep.setString(4, term);
        prep.setString(5, term);
        prep.setLong(6, startTimestamp);
        prep.setLong(7, timestamp);
        prep.setString(8, userId);
        prep.setString(9, userId);
        prep.setArray(10, conn.createArrayOf("text", cmpyIds.toArray()));
        prep.setString(11, userId);
        prep.setInt(12, startpos);
      }

      prep.setMaxRows(maxRows);

      rs = prep.executeQuery();
      if (rs == null) {
        return results;
      }

      return handleResultSet(rs);


    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static List<Trip> findMyTripsIncShare(String term,
                                       List<String> cmpyIds,
                                       String userId,
                                       int maxRows,
                                       int startpos,
                                       ArrayList<Integer> statuses,
                                       Connection conn)
      throws SQLException {
    ArrayList<Trip> results = new ArrayList<>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {

      prep = conn.prepareStatement(MY_TRIPS_INC_SHARE);
      prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
      prep.setLong(2, Instant.now().toEpochMilli());
      prep.setArray(3, conn.createArrayOf("text", cmpyIds.toArray()));
      prep.setString(4, term);
      prep.setString(5, term);
      prep.setString(6, userId);
      prep.setString(7, userId);
      prep.setArray(8, conn.createArrayOf("text", cmpyIds.toArray()));
      prep.setString(9, userId);
      prep.setString(10, userId);
      prep.setString(11, term);

      prep.setInt(12, startpos);



      prep.setMaxRows(maxRows);


      rs = prep.executeQuery();
      if (rs == null) {
        return results;
      }

      return handleResultSet(rs);


    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static List<Trip> findAdminTripsIncShare(String term,
                                               List<String> cmpyIds,
                                               String userId,
                                               int maxRows,
                                               int startpos,
                                               ArrayList<Integer> statuses,
                                               Connection conn)
      throws SQLException {
    ArrayList<Trip> results = new ArrayList<>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;
    try {

      prep = conn.prepareStatement(ADMIN_TRIPS_INC_SHARE);
      prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
      prep.setLong(2, Instant.now().toEpochMilli());
      prep.setArray(3, conn.createArrayOf("text", cmpyIds.toArray()));
      prep.setString(4, term);
      prep.setString(5, userId);
      prep.setString(6, term);

      prep.setInt(7, startpos);



      prep.setMaxRows(maxRows);


      rs = prep.executeQuery();
      if (rs == null) {
        return results;
      }

      return handleResultSet(rs);


    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static List<Trip> findMyTrips2(String term,
                                       List<String> cmpyIds,
                                       String userId,
                                       int maxRows,
                                       int startpos,
                                       ArrayList<Integer> statuses,
                                       Connection conn,
                                       boolean alltrips,
                                       String sortCol,
                                       String sortOrder,
                                       FilterType filterDate,
                                       long currentClientTime)
      throws SQLException {
    ArrayList<Trip> results = new ArrayList<>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    Statement stat = null;

    //Thu, 01 Jan 1970 01:00:00 GMT
    long timestamp = 0;
    long startTimestamp = 0;

    if(filterDate == FilterType.INPROGRESS) {
      startTimestamp = currentClientTime;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.FUTURE) {
      startTimestamp = currentClientTime;
      timestamp = Long.MAX_VALUE;
    }
    else if(filterDate == FilterType.PAST) {
      startTimestamp = 0;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.ACTIVE) {
      startTimestamp = 0;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.ALL) {
      startTimestamp = 0;
      timestamp = Long.MAX_VALUE;
    }

    try {
      String colName = "";
      if(sortCol.equals("imgUrl") || sortCol.equals("tripName")) {
        colName = "name";
      }
      else {
        colName = "starttimestamp";
      }

      if (!alltrips) {
        if(filterDate == FilterType.INPROGRESS) {
          String sql = String.format(TIMED_TRIPS_CREATED_BY_ME_IN_PROGRESS, colName, sortOrder);
          prep = conn.prepareStatement(sql);
          prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
          prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(3, term);
          prep.setString(4, term);
          prep.setLong(5, startTimestamp);
          prep.setLong(6, timestamp);
          prep.setString(7, userId);
          prep.setInt(8, startpos);
        }
        else if(filterDate == FilterType.ACTIVE) {
          String sql = String.format(TIMED_TRIPS_CREATED_BY_ME_ACTIVE, colName, sortOrder);
          prep = conn.prepareStatement(sql);
          prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
          prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(3, term);
          prep.setString(4, term);
          prep.setLong(5, timestamp);
          prep.setString(6, userId);
          prep.setInt(7, startpos);
        }
        else {
          String sql = String.format(TIMED_TRIPS_CREATED_BY_ME, colName, sortOrder);
          prep = conn.prepareStatement(sql);
          prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
          prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(3, term);
          prep.setString(4, term);
          prep.setLong(5, startTimestamp);
          prep.setLong(6, timestamp);
          prep.setString(7, userId);
          prep.setInt(8, startpos);
        }
      }
      else {
        if(filterDate == FilterType.INPROGRESS) {
          String sql = String.format(MY_TRIPS_INCLUDE_ARCHIVES_IN_PROGRESS, colName, sortOrder);
          prep = conn.prepareStatement(sql);
          prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
          prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(3, term);
          prep.setString(4, term);
          prep.setString(5, term);
          prep.setLong(6, startTimestamp);
          prep.setLong(7, timestamp);
          prep.setString(8, userId);
          prep.setString(9, userId);
          prep.setArray(10, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(11, userId);
          prep.setInt(12, startpos);
        }
        else if(filterDate == FilterType.ACTIVE) {
          String sql = String.format(MY_TRIPS_INCLUDE_ARCHIVES_ACTIVE, colName, sortOrder);
          prep = conn.prepareStatement(sql);
          prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
          prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(3, term);
          prep.setString(4, term);
          prep.setString(5, term);
          prep.setLong(6, timestamp);
          prep.setString(7, userId);
          prep.setString(8, userId);
          prep.setArray(9, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(10, userId);
          prep.setInt(11, startpos);
        }
        else {
          String sql = String.format(MY_TRIPS_INCLUDE_ARCHIVES, colName, sortOrder);
          prep = conn.prepareStatement(sql);
          prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
          prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(3, term);
          prep.setString(4, term);
          prep.setString(5, term);
          prep.setLong(6, startTimestamp);
          prep.setLong(7, timestamp);
          prep.setString(8, userId);
          prep.setString(9, userId);
          prep.setArray(10, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(11, userId);
          prep.setInt(12, startpos);
        }
      }

      prep.setMaxRows(maxRows);

      rs = prep.executeQuery();

      if (rs == null) {
        return results;
      }

      return handleResultSet(rs);


    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static int countMyTrips(String term,
                                       List<String> cmpyIds,
                                       String userId,
                                       ArrayList<Integer> statuses,
                                       Connection conn,
                                       FilterType filterDate,
                                       long currentClientTime)
      throws SQLException {
    ResultSet rs = null;
    PreparedStatement prep = null;
    try {

      long timestamp = 0;
      long startTimestamp = 0;

      if(filterDate == FilterType.INPROGRESS) {
        startTimestamp = currentClientTime;
        timestamp = currentClientTime;
      }
      else if(filterDate == FilterType.FUTURE) {
        startTimestamp = currentClientTime;
        timestamp = Long.MAX_VALUE;
      }
      else if(filterDate == FilterType.PAST) {
        startTimestamp = 0;
        timestamp = currentClientTime;
      }
      else if(filterDate == FilterType.ACTIVE) {
        startTimestamp = 0;
        timestamp = currentClientTime;
      }
      else if(filterDate == FilterType.ALL) {
        startTimestamp = 0;
        timestamp = Long.MAX_VALUE;
      }

      if(filterDate == FilterType.INPROGRESS) {
        prep = conn.prepareStatement(MY_TRIPS_COUNT_IN_PROGRESS);
        prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
        prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
        prep.setString(3, term);
        prep.setString(4, term);
        prep.setString(5, term);
        prep.setLong(6, startTimestamp);
        prep.setLong(7, timestamp);
        prep.setString(8, userId);
        prep.setString(9, userId);
        prep.setArray(10, conn.createArrayOf("text", cmpyIds.toArray()));
        prep.setString(11, userId);

      }
      else if(filterDate == FilterType.ACTIVE) {
        prep = conn.prepareStatement(MY_TRIPS_COUNT_ACTIVE);
        prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
        prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
        prep.setString(3, term);
        prep.setString(4, term);
        prep.setString(5, term);
        prep.setLong(6, timestamp);
        prep.setString(7, userId);
        prep.setString(8, userId);
        prep.setArray(9, conn.createArrayOf("text", cmpyIds.toArray()));
        prep.setString(10, userId);
      }
      else {
        prep = conn.prepareStatement(MY_TRIPS_COUNT);
        prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
        prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
        prep.setString(3, term);
        prep.setString(4, term);
        prep.setString(5, term);
        prep.setLong(6, startTimestamp);
        prep.setLong(7, timestamp);
        prep.setString(8, userId);
        prep.setString(9, userId);
        prep.setArray(10, conn.createArrayOf("text", cmpyIds.toArray()));
        prep.setString(11, userId);
      }

      rs = prep.executeQuery();
      if (rs == null) {
        return 0;
      }

      rs.next();
      return rs.getInt(1);

    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static int countMyTrips2(String term,
                                        List<String> cmpyIds,
                                        String userId,
                                        ArrayList<Integer> statuses,
                                        Connection conn,
                                        boolean alltrips,
                                        FilterType filterDate,
                                        long currentClientTime)
      throws SQLException {
    ResultSet rs = null;
    PreparedStatement prep = null;

    //Thu, 01 Jan 1970 01:00:00 GMT
    long timestamp = 0;
    long startTimestamp = 0;

    if(filterDate == FilterType.INPROGRESS) {
      startTimestamp = currentClientTime;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.FUTURE) {
      startTimestamp = currentClientTime;
      timestamp = Long.MAX_VALUE;
    }
    else if(filterDate == FilterType.PAST) {
      startTimestamp = 0;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.ACTIVE) {
      startTimestamp = 0;
      timestamp = currentClientTime;
    }
    else if(filterDate == FilterType.ALL) {
      startTimestamp = 0;
      timestamp = Long.MAX_VALUE;
    }

    try {
      if (!alltrips) {
        if(filterDate == FilterType.INPROGRESS) {
          prep = conn.prepareStatement(TIMED_TRIPS_CREATED_BY_ME_COUNT_IN_PROGRESS);
          prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
          prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(3, term);
          prep.setString(4, term);
          prep.setLong(5, startTimestamp);
          prep.setLong(6, timestamp);
          prep.setString(7, userId);
        }
        else if (filterDate == FilterType.ACTIVE) {
          prep = conn.prepareStatement(TIMED_TRIPS_CREATED_BY_ME_COUNT_ACTIVE);
          prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
          prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(3, term);
          prep.setString(4, term);
          prep.setLong(5, timestamp);
          prep.setString(6, userId);
        }
        else {
          prep = conn.prepareStatement(TIMED_TRIPS_CREATED_BY_ME_COUNT);
          prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
          prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(3, term);
          prep.setString(4, term);
          prep.setLong(5, startTimestamp);
          prep.setLong(6, timestamp);
          prep.setString(7, userId);
        }
      }
      else {
        if(filterDate == FilterType.INPROGRESS) {
          prep = conn.prepareStatement(MY_TRIPS_INCLUDE_ARCHIVES_COUNT_IN_PROGRESS);
          prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
          prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(3, term);
          prep.setString(4, term);
          prep.setString(5, term);
          prep.setLong(6, startTimestamp);
          prep.setLong(7, timestamp);
          prep.setString(8, userId);
          prep.setString(9, userId);
          prep.setArray(10, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(11, userId);
        }
        else if (filterDate == FilterType.ACTIVE) {
          prep = conn.prepareStatement(MY_TRIPS_INCLUDE_ARCHIVES_COUNT_ACTIVE);
          prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
          prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(3, term);
          prep.setString(4, term);
          prep.setString(5, term);
          prep.setLong(6, timestamp);
          prep.setString(7, userId);
          prep.setString(8, userId);
          prep.setArray(9, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(10, userId);
        }
        else {
          prep = conn.prepareStatement(MY_TRIPS_INCLUDE_ARCHIVES_COUNT);
          prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
          prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(3, term);
          prep.setString(4, term);
          prep.setString(5, term);
          prep.setLong(6, startTimestamp);
          prep.setLong(7, timestamp);
          prep.setString(8, userId);
          prep.setString(9, userId);
          prep.setArray(10, conn.createArrayOf("text", cmpyIds.toArray()));
          prep.setString(11, userId);
        }
      }

      rs = prep.executeQuery();
      if (rs == null) {
        return 0;
      }

      rs.next();
      return rs.getInt(1);

    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static List<SharedTripRS> findSharedTrips(String term,
                                           List<String> cmpyIds,
                                           String userId,
                                           int maxRows,
                                           int startpos,
                                           ArrayList<Integer> statuses,
                                           Connection conn)
      throws SQLException {
    ArrayList<SharedTripRS> results = new ArrayList<>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    try {

      prep = conn.prepareStatement(SHARED_TRIPS);
      prep.setArray(1, conn.createArrayOf("int4", statuses.toArray()));
      prep.setArray(2, conn.createArrayOf("text", cmpyIds.toArray()));
      prep.setString(3, term);
      prep.setString(4, term);
      prep.setString(5, term);
      prep.setString(6, userId);
      prep.setString(7, userId);
      prep.setInt(8, startpos);


      prep.setMaxRows(maxRows);


      rs = prep.executeQuery();
      if (rs == null) {
        return results;
      }

      return handleResultSet2(rs);


    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }



  public static List<SharedTripRS> findRecentSharedTrips(String userId, int maxRows, int startpos, Connection conn)
      throws SQLException {
    ArrayList<SharedTripRS> results = new ArrayList<>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    try {
      prep = conn.prepareStatement(RECENT_SHARED_TRIPS);
      prep.setString(1, userId);
      prep.setString(2, userId);
      prep.setInt(3, startpos);

      prep.setMaxRows(maxRows);

      rs = prep.executeQuery();
      if (rs == null) {
        return results;
      }

      return handleResultSet2(rs);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static List<SharedTripRS> findDashboardSharedTrips(String userId, int maxRows, int startpos, String term, Connection conn, String sortCol, String sortOrder)
      throws SQLException {
    ArrayList<SharedTripRS> results = new ArrayList<>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    if (term != null) {
      term = "%" + term.toUpperCase() + "%";
    } else {
      term = "%";
    }
    try {
      String colName = "";
      if(sortCol.equals("imgUrl") || sortCol.equals("tripName")) {
        colName = "name";
      }
      else {
        colName = "starttimestamp";
      }

      String sql = String.format(DASHBOARD_SHARED_TRIPS, colName, sortOrder);
      prep = conn.prepareStatement(sql);
      prep.setString(1, userId);
      prep.setString(2, userId);
      prep.setString(3, term);
      prep.setString(4, term);
      prep.setInt(5, startpos);

      prep.setMaxRows(maxRows);

      rs = prep.executeQuery();
      if (rs == null) {
        return results;
      }

      return handleResultSet2(rs);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static int findDashboardSharedTripsCountFiltered (String userId, String term, Connection conn)
      throws SQLException {
    ArrayList<SharedTripRS> results = new ArrayList<>();
    ResultSet rs = null;
    PreparedStatement prep = null;
    if (term != null) {
      term = "%" + term.toUpperCase() + "%";
    } else {
      term = "%";
    }
    try {
      prep = conn.prepareStatement(DASHBOARD_SHARED_TRIPS_COUNT_FILTERED);
      prep.setString(1, userId);
      prep.setString(2, userId);
      prep.setString(3, term);
      prep.setString(4, term);


      rs = prep.executeQuery();
      if (rs == null) {
        return 0;
      }
      rs.next();

      return rs.getInt(1);

    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static int findDashboardSharedTripsCount (String userId, Connection conn)
      throws SQLException {
    ArrayList<SharedTripRS> results = new ArrayList<>();
    ResultSet rs = null;
    PreparedStatement prep = null;

    try {
      prep = conn.prepareStatement(DASHBOARD_SHARED_TRIPS_COUNT);
      prep.setString(1, userId);
      prep.setString(2, userId);



      rs = prep.executeQuery();
      if (rs == null) {
        return 0;
      }
      rs.next();
      return rs.getInt(1);

    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
    }
  }

  public static ArrayList<Trip> handleResultSet(ResultSet rs)
      throws SQLException {
    ArrayList<Trip> results = new ArrayList<>();
    while (rs.next()) {
      Trip c = buildRS(rs);

      results.add(c);
    }
    return results;

  }

  public static ArrayList<SharedTripRS> handleResultSet2(ResultSet rs)
      throws SQLException {
    ArrayList<SharedTripRS> results = new ArrayList<>();
    while (rs.next()) {
      SharedTripRS c = new SharedTripRS();
      c.trip = buildRS(rs);
      String accesslevel = rs.getString("accesslevel");
      c.setAccessLevel(SecurityMgr.AccessLevel.valueOf(accesslevel));

      results.add(c);
    }
    return results;

  }

  public static Trip buildRS (ResultSet rs) throws SQLException{
    Trip c = new Trip();

    c.setTripid(rs.getString("tripid"));
    c.setName(rs.getString("name"));
    c.setCmpyid(rs.getString("cmpyid"));
    c.setAccesscode(rs.getString("accesscode"));
    c.setComments(rs.getString("comments"));
    c.setCoverfilename(rs.getString("coverfilename"));
    c.setCoverurl(rs.getString("coverurl"));
    c.setCreatedby(rs.getString("createdby"));
    c.setCreatedtimestamp(rs.getLong("createdtimestamp"));
    c.setEndtimestamp(rs.getLong("endtimestamp"));
    c.setLastupdatedtimestamp(rs.getLong("lastupdatedtimestamp"));
    c.setModifiedby(rs.getString("modifiedby"));
    c.setStarttimestamp(rs.getLong("starttimestamp"));
    c.setStatus(rs.getInt("status"));
    c.setTriptype(rs.getInt("triptype"));
    c.setVersion(rs.getInt("version"));
    c.setVisibility(rs.getInt("visibility"));
    return c;
  }



}
