package com.umapped.itinerary.analyze.recommendation;

import java.util.List;
import java.util.Map;

/**
 * Created by wei on 2017-03-31.
 */
public interface RankingStrategy<T extends RecommendedItem> {
  List<T> rank(Map<String, List<T>> items);
}
