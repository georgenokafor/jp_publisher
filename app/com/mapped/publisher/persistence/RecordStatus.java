package com.mapped.publisher.persistence;
import com.avaje.ebean.annotation.EnumValue;
import com.mapped.publisher.common.APPConstants;

/**
 * Created by surge on 2014-10-20.
 */
public enum RecordStatus {
  @EnumValue("ACT")
  ACTIVE,
  @EnumValue("PEN")
  PENDING,
  @EnumValue("EXP")
  EXPIRED,
  @EnumValue("DEL")
  DELETED,
  @EnumValue("LCK")
  LOCKED;

  public static RecordStatus fromStatus(int status) {
    switch (status) {
      case APPConstants.STATUS_DELETED:
        return  DELETED;
      case APPConstants.STATUS_PENDING:
        return PENDING;
      case APPConstants.STATUS_ACCOUNT_LOCKED:
        return LOCKED;
      case APPConstants.STATUS_ACTIVE:
      default:
        return ACTIVE;
    }
  }

  public String getShort(){
    switch (this) {
      case ACTIVE:
        return "ACT";
      case PENDING:
        return "PEN";
      case EXPIRED:
        return "EXP";
      case DELETED:
        return "DEL";
      case LOCKED:
        return "LCK";
      default:
        return  "UNK";
    }
  }
}
