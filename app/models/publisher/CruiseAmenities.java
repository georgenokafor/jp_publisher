package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Update;
import com.mapped.persistence.util.DBConnectionMgr;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created by ryan on 15-06-24.
 */
@Entity @Table(name = "cruise_amenities_feed")
public class CruiseAmenities extends Model{

  public static Model.Finder<String, CruiseAmenities> find = new Finder(CruiseAmenities.class);

  public String ship_name;
  public String amenities;
  public byte[] hash;
  public String raw;
  public long poi_id;

  @Id
  public long pk;

  @Version
  public int version;

  @Constraints.Required
  public String src_reference_id;
  public Long consortium_id;
  public String cruise_name;
  public String cmpy_name;
  public Long start_ts;
  public Long end_ts;
  public Long created_ts;
  public Long last_updated_ts;
  public String created_by;
  public String modified_by;

  public static int wipeConsortiumFeed(long consortium_id) {
    Update<CruiseAmenities> upd = Ebean.createUpdate(CruiseAmenities.class, "DELETE FROM cruise_amenities_feed WHERE consortium_id=:consId");
    upd.set("consId", consortium_id);
    return upd.execute();
  }

  public static CruiseAmenities buildCruiseAmenities(String name, String userId) {
    CruiseAmenities ca = new CruiseAmenities();
    ca.setPk(DBConnectionMgr.getUniqueLongId());
    ca.setCruise_name(name);
    ca.setCreated_by(userId);
    ca.setCreated_ts(System.currentTimeMillis());
    ca.setModified_by(userId);
    ca.setLast_updated_ts(ca.getCreated_ts());
    return ca;
  }

  public static CruiseAmenities findByPK(String pk) {
    return find.byId(pk);
  }

  public static CruiseAmenities findBySource_ConsortiumId(String source_ref_id, Long consortium_id){
    List<CruiseAmenities> cruiseAmenities = find.where()
                                                .eq("src_reference_id", source_ref_id)
                                                .eq("consortium_id", consortium_id)
                                                .findList();

    if(!cruiseAmenities.isEmpty())
      return cruiseAmenities.get(0);
    else
      return null;
  }

  public static CruiseAmenities getAmenitiesPoiId_StartTimeEndTime(int consortiumId, long poi_id, long start_ts, long end_ts) {
    List<CruiseAmenities> cruiseAmenities = find.where()
                                                .eq("consortium_id", consortiumId)
                                                .eq("start_ts", start_ts)
                                                .eq("end_ts", end_ts)
                                                .eq("poi_id", poi_id)
                                                .findList();
    if(!cruiseAmenities.isEmpty())
      return cruiseAmenities.get(0);
    else
      return null;
  }

  public static CruiseAmenities getAmenitiesShipName_StartTimeEndTime(int consortiumId, String ship_name, long start_ts, long end_ts) {
    List<CruiseAmenities> cruiseAmenities = find.where()
                                                .eq("consortium_id", consortiumId)
                                                .ilike("ship_name", ship_name)
                                                .eq("start_ts", start_ts)
                                                .eq("end_ts", end_ts)
                                                .findList();

    if(!cruiseAmenities.isEmpty())
      return cruiseAmenities.get(0);
    else
      return null;
  }

  public String getShip_name() {
    return ship_name;
  }

  public void setShip_name(String ship_name) {
    this.ship_name = ship_name;
  }

  public String getAmenities() {
    return amenities;
  }

  public void setAmenities(String amenities) {
    this.amenities = amenities;
  }

  public byte[] getHash() {
    return hash;
  }

  public void setHash(byte[] hash) {
    this.hash = hash;
  }

  public String getRaw() {
    return raw;
  }

  public void setRaw(String raw) {
    this.raw = raw;
  }

  public long getPk() {
    return pk;
  }

  public void setPk(long pk) {
    this.pk = pk;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getSrc_reference_id() {
    return src_reference_id;
  }

  public void setSrc_reference_id(String src_reference_id) {
    this.src_reference_id = src_reference_id;
  }

  public Long getConsortium_id() {
    return consortium_id;
  }

  public void setConsortium_id(Long consortium_id) {
    this.consortium_id = consortium_id;
  }

  public String getCruise_name() {
    return cruise_name;
  }

  public void setCruise_name(String cruise_name) {
    this.cruise_name = cruise_name;
  }

  public String getCmpy_name() {
    return cmpy_name;
  }

  public void setCmpy_name(String cmpy_name) {
    this.cmpy_name = cmpy_name;
  }

  public Long getStart_ts() {
    return start_ts;
  }

  public void setStart_ts(Long start_ts) {
    this.start_ts = start_ts;
  }

  public Long getEnd_ts() {
    return end_ts;
  }

  public void setEnd_ts(Long end_ts) {
    this.end_ts = end_ts;
  }

  public Long getCreated_ts() {
    return created_ts;
  }

  public void setCreated_ts(Long created_ts) {
    this.created_ts = created_ts;
  }

  public Long getLast_updated_ts() {
    return last_updated_ts;
  }

  public void setLast_updated_ts(Long last_updated_ts) {
    this.last_updated_ts = last_updated_ts;
  }

  public String getCreated_by() {
    return created_by;
  }

  public void setCreated_by(String created_by) {
    this.created_by = created_by;
  }

  public String getModified_by() {
    return modified_by;
  }

  public void setModified_by(String modified_by) {
    this.modified_by = modified_by;
  }

  public long getPoi_id() {
    return poi_id;
  }

  public void setPoi_id(long poi_id) {
    this.poi_id = poi_id;
  }
}
