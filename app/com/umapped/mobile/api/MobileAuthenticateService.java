package com.umapped.mobile.api;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Transaction;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.persistence.RecordStatus;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.umapped.mobile.api.model.*;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import models.publisher.Account;
import models.publisher.AccountAuth;
import models.publisher.AccountSession;
import org.apache.commons.lang.StringUtils;
import play.libs.Json;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

/**
 * Created by wei on 2017-06-28.
 */
@Singleton public class MobileAuthenticateService {

  private static int PASSWORD_MATCH = 0;
  private static int PASSWORD_MISMATCH = 1;
  private static int NO_PASSWORD = -1;

  @Inject LegacyMobileUserQueries legacyMobileUserQueries;

  public AuthenticationResponse authenticate(String email, String password) {
    return authenticate(email, password, null, null);
  }

  public AuthenticationResponse authenticate(String email, String password, String pnToken, AppDeviceInfo appDevice) {
    Account account = Account.findByEmail(email);
    if (account != null) {
      switch (account.getAccountType()) {
        case TRAVELER:
        case PUBLISHER:
          int result = validPassword(account, password);
          if (result == PASSWORD_MATCH) {
            AuthenticationResponse response = new AuthenticationResponse(AuthenticationStatus.SUCCESSFUL);
            Long sessionId = null;
            if (appDevice != null) {
              sessionId = generateSession(account.getUid(), pnToken, appDevice);
            }
            if (sessionId == null) {
              Log.err("No session id can be generated for device: " + appDevice.getAppBundleId());
              return new AuthenticationResponse((AuthenticationStatus.UNEXPECTED_ERROR));
            }
            populateUserInformation(response, account, sessionId);
            return response;
          }
          else if (result == PASSWORD_MISMATCH) {
            return new AuthenticationResponse(AuthenticationStatus.USER_PASSWORD_NOT_MATCH);
          }
          else {
            return authenticateLegacyUser(email, password, pnToken, appDevice);
          }
        default:
          return new AuthenticationResponse(AuthenticationStatus.USER_ROLE_INVALID);
      }
    }
    else {
      return authenticateLegacyUser(email, password, pnToken, appDevice);
    }
  }

  private AuthenticationResponse authenticateLegacyUser(String email,
                                                        String password,
                                                        String pnToken,
                                                        AppDeviceInfo appDevice) {
    RegistrationRequest userInfo = legacyMobileUserQueries.findUser(email, password);

    if (userInfo == null) {
      return new AuthenticationResponse(AuthenticationStatus.USER_NOT_FOUND);
    }
    Log.debug("Found user from legacy mobile database");
    RegistrationResponse registrationResponse = register(userInfo, pnToken, appDevice);
    if (RegistrationStatus.SUCCESSFUL.equals(registrationResponse.getStatus())) {
      AuthenticationResponse response = new AuthenticationResponse(AuthenticationStatus.SUCCESSFUL);
      response.setAccountId(registrationResponse.getAccountId());
      response.setFirstName(registrationResponse.getFirstName());
      response.setLastName(registrationResponse.getLastName());
      response.setEmail(registrationResponse.getEmail());
      response.setJwtToken(registrationResponse.getJwtToken());
      response.setSessionId(registrationResponse.getSessionId());
      response.setAccountId(registrationResponse.getAccountId());
      response.setUserRole(registrationResponse.getUserRole());
      return response;
    }
    else {
      Log.err("Fail to register the legacy user. Email: " + email + "; status: " + registrationResponse.getStatus());
      return new AuthenticationResponse(AuthenticationStatus.UNEXPECTED_ERROR);
    }
  }

  public RegistrationResponse register(RegistrationRequest inRequest) {
    return register(inRequest, null, null);
  }

  public RegistrationResponse register(RegistrationRequest inRequest, String pnToken, AppDeviceInfo appDevice) {
    RegistrationRequest request = normalize(inRequest);

    RegistrationStatus validationStatus = validateRegirationRequest(request);

    // validate the request
    if (!validationStatus.equals(RegistrationStatus.SUCCESSFUL)) {
      return new RegistrationResponse(validationStatus);
    }

    try {
      // 1. check if the email has been used
      Account account = Account.findByEmail(StringUtils.trim(request.getEmail()));

      if (account != null) {
        if (!Account.AccountType.TRAVELER.equals(account.getAccountType())) {
          //  An account exists for this email and it is not a traveler
          //  then couldn't register
          return new RegistrationResponse(RegistrationStatus.EMAIL_USED_BY_EXISTING_USER);
        }
        AccountAuth accountAuth = AccountAuth.getByType(account.getUid(), AccountAuth.AAuthType.PASSWORD);
        if (accountAuth != null) {
          return new RegistrationResponse(RegistrationStatus.EMAIL_USED_BY_EXISTING_TRAVELER);
        }
        else {
          // create AccountAuth
          createAccountAuth(account, request.getPassword());
          Log.debug("Create an Account Auth for existing account");
        }
      }
      else {
        Transaction t = Ebean.beginTransaction();

        account = createAccount(request.getFirstName(), request.getLastName(), request.getEmail());
        Log.debug("Create an Account ");
        createAccountAuth(account, request.getPassword());
        Log.debug("Create an Account Auth for new account");
        t.commit();
      }
      RegistrationResponse response = new RegistrationResponse(RegistrationStatus.SUCCESSFUL);

      Long sessionId = null;
      if (appDevice != null) {
        sessionId = generateSession(account.getUid(), pnToken, appDevice);
      }
      populateUserInformation(response, account, sessionId);

      //
      Log.debug("Login after successful registration");
      return response;
    }
    catch (Throwable e) {
      Log.err("Registration mobile user, unexpected error", e);
      return new RegistrationResponse(RegistrationStatus.UNEXPECTED_ERROR);
    }
  }

  private AuthenticatedUser populateUserInformation(AuthenticatedUser user, Account account, Long sessionId) {
    user.setFirstName(account.getFirstName());
    user.setLastName(account.getLastName());
    user.setAccountId(account.getUid().toString());
    user.setEmail(account.getEmail());

    // TODO: remove jwtToken
    user.setJwtToken(generateToken(account.getUid()));

    user.setSessionId(sessionId == null ? null : sessionId.toString());

    // Only support Traveler and Publisher role
    if (Account.AccountType.TRAVELER.equals(account.getAccountType())) {
      user.setUserRole(MobileUserRole.Traveler);
    }
    else {
      user.setUserRole(MobileUserRole.Publisher);
    }
    return user;
  }

  /**
   * Normalize registration information:
   * 1) email to lower case
   * 2) trim all fields
   *
   * @param request
   * @return
   */
  private RegistrationRequest normalize(RegistrationRequest request) {
    RegistrationRequest formatted = new RegistrationRequest();
    formatted.setEmail(StringUtils.lowerCase(StringUtils.trimToNull(request.getEmail())));
    formatted.setFirstName(StringUtils.trimToNull(request.getFirstName()));
    formatted.setLastName(StringUtils.trimToNull(request.getLastName()));
    formatted.setPassword(StringUtils.trimToNull(request.getPassword()));
    return formatted;
  }

  private RegistrationStatus validateRegirationRequest(RegistrationRequest request) {
    if (request.getEmail() == null) {
      return RegistrationStatus.EMAIL_REQUIRED;
    }
    if (request.getFirstName() == null) {
      return RegistrationStatus.FIRST_NAME_REQUIRED;
    }
    if (request.getLastName() == null) {
      return RegistrationStatus.LAST_NAME_REQUIRED;
    }
    if (request.getPassword() == null) {
      return RegistrationStatus.PASSWORD_REQUIRED;
    }
    if (!MobileAPISerivceHelper.validateEmailAddress(request.getEmail())) {
      return RegistrationStatus.EMAIL_INVALID_FORMAT;
    }
    return RegistrationStatus.SUCCESSFUL;
  }


  private Account createAccount(String fistName, String lastName, String email) {
    Account account = new Account();
    account.setUid(DBConnectionMgr.getUniqueLongId());
    account.setCreatedBy(account.getUid());
    account.setModifiedBy(account.getUid());
    account.setCreatedTs(Timestamp.from(Instant.now()));
    account.setModifiedTs(account.getCreatedTs());
    account.setAccountType(Account.AccountType.TRAVELER);
    account.setState(RecordStatus.ACTIVE);
    account.setFirstName(fistName);
    account.setLastName(lastName);
    account.setEmail(email);
    account.save();
    return account;
  }

  private void createAccountAuth(Account account, String password) {
    AccountAuth auth = AccountAuth.build(account.getUid());
    auth.setAuthType(AccountAuth.AAuthType.PASSWORD);
    auth.setUid(account.getUid());
    auth.setSalt(UUID.randomUUID().toString());
    String hashedPwd = Utils.hashPwd(password, String.valueOf(auth.getSalt()));
    auth.setValue(hashedPwd);
    auth.setCreatedTs(Timestamp.from(Instant.now()));
    auth.setModifiedTs(auth.getCreatedTs());
    auth.setCreatedBy(account.getUid());
    auth.setModifiedBy(account.getUid());
    auth.save();
  }

  protected String generateToken(Long uid) {
    Map<String, Object> claims = new HashMap<>();
    //claims.put("sub", "uid");
    claims.put("uid", uid);
    SecretKey key = getKey(uid);
    String token = Jwts.builder()
                       .setHeaderParam("typ", "JWT") //Library does not set this by default
                       .setClaims(claims)
                       .setSubject("uid")            //Has to go after other claims or will be overwritten
                       .signWith(SignatureAlgorithm.HS256, key)
                       .compact();
    return token;

  }

  int validPassword(Account account, String password) {
    AccountAuth auth = AccountAuth.getByType(account.getUid(), AccountAuth.AAuthType.PASSWORD);
    if (auth != null) {
      String hashedPwd = Utils.hashPwd(password, String.valueOf(auth.getSalt()));
      if (hashedPwd.equals(auth.getValue())) {
        return PASSWORD_MATCH;
      }
      else {
        return PASSWORD_MISMATCH;
      }
    }
    return NO_PASSWORD;
  }

  private SecretKey getKey(Long accountUid) {
    AccountAuth token = null;
    List<AccountAuth> tokens = AccountAuth.getAllByType(accountUid, AccountAuth.AAuthType.JWT);
    if (tokens.size() == 1) {
      token = tokens.get(0);
    }
    else if (tokens.size() > 1) {
      //Starting from scratch - error somewhere.
      for (AccountAuth tkn : tokens) {
        tkn.delete();
      }
    }

    // Thank you humans at SO:
    // http://stackoverflow.com/questions/5355466/converting-secret-key-into-a-string-and-vice-versa
    // Also Java 8 base64 is very fast!: http://java-performance.info/base64-encoding-and-decoding-performance/
    SecretKey key;
    if (token == null) {
      //Generate new key
      key = MacProvider.generateKey(SignatureAlgorithm.HS256);
      String encodedKey = Base64.getEncoder().encodeToString(key.getEncoded());

      token = AccountAuth.build(accountUid)
                         .setUid(accountUid)
                         .setAuthType(AccountAuth.AAuthType.JWT)
                         .setValue(encodedKey);
      token.save();
    }
    else {
      byte[] decodedKey = Base64.getDecoder().decode(token.getValue());
      // rebuild key using SecretKeySpec
      key = new SecretKeySpec(decodedKey, 0, decodedKey.length, SignatureAlgorithm.HS256.getValue());
    }
    return key;
  }

  public boolean setPassword(Account account, String newPassword) {
    Transaction t = Ebean.beginTransaction();

    try {

      AccountAuth aa = AccountAuth.getPassword(account.getUid());
      if (aa == null) {
        aa = AccountAuth.build(account.getUid());
        aa.setUid(account.getUid());
      }
      else {
        aa.markModified(account.getUid());
      }
      aa.setAuthType(AccountAuth.AAuthType.PASSWORD);
      aa.setSalt(UUID.randomUUID().toString());
      aa.setValue(Utils.hashPwd(newPassword, String.valueOf(aa.getSalt())));
      aa.setResetToken(null);
      aa.save();
      t.commit();
    }
    catch (Exception ex) {
      Log.err("Fail to set password for traveler", ex);
      return false;
    }
    return true;
  }

  public Long generateSession(Long accountUid, String pnToken, AppDeviceInfo appDevice) {
    String appId = appDevice == null ? null : BundleToAppMap.getApp(appDevice.getAppBundleId());

    // missing information to generate session
    if (StringUtils.isEmpty(appId) || accountUid == null || appDevice == null || appDevice.getDeviceUid() == null) {
      Log.err(String.format("Missing information to generate session: appId = %s, accountUid = %s, deviceUid = %s",
                            appId,
                            accountUid,
                            appDevice == null ? null : appDevice.getDeviceUid()));
      return null;
    }

    AccountSession session = AccountSession.findByKey(accountUid, appDevice.getDeviceUid(), appId);
    if (session == null) {
      session = new AccountSession();
      session.setSessionUid(DBConnectionMgr.getUniqueLongId());
    }

    session.setAccountUid(accountUid);
    session.setAppId(appId);
    session.setDeviceUid(appDevice.getDeviceUid());
    session.setNotificationToken(pnToken);
    long now = Instant.now().toEpochMilli();
    session.setCreatedTs(now);
    session.setModifiedTs(now);
    session.setSyncTs(0L);
    session.setDetail(Json.toJson(appDevice));
    session.save();
    return session.getSessionUid();
  }

  public static void main(String[] args) {
    System.out.println(Utils.hashPwd("password", "1383103698370"));
  }
}