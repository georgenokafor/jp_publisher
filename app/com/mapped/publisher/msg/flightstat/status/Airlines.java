
package com.mapped.publisher.msg.flightstat.status;

import com.mapped.publisher.msg.flightstat.status.Airline;

import java.util.List;

public class Airlines{
   	private List<Airline> airline;

 	public List<Airline> getAirline(){
		return this.airline;
	}
	public void setAirline(List<Airline> airline){
		this.airline = airline;
	}
}
