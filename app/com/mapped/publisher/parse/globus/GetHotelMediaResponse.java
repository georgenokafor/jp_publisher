
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetHotelMediaResult" type="{http://webapi.globusandcosmos.com/}HotelMedia" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getHotelMediaResult"
})
@XmlRootElement(name = "GetHotelMediaResponse")
public class GetHotelMediaResponse {

    @XmlElement(name = "GetHotelMediaResult")
    protected HotelMedia getHotelMediaResult;

    /**
     * Gets the value of the getHotelMediaResult property.
     * 
     * @return
     *     possible object is
     *     {@link HotelMedia }
     *     
     */
    public HotelMedia getGetHotelMediaResult() {
        return getHotelMediaResult;
    }

    /**
     * Sets the value of the getHotelMediaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelMedia }
     *     
     */
    public void setGetHotelMediaResult(HotelMedia value) {
        this.getHotelMediaResult = value;
    }

}
