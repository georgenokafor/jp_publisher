package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Created by surge on 2016-06-28.
 */
@JsonTypeName("TaxiReservation")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TaxiReservation
    extends Reservation {
  /**
   * Number of people the reservation should accommodate.
   */
  public Integer  partySize;
  /**
   * Where a taxi will pick up a passenger or a rental car can be picked up.
   */
  public Place    pickupLocation;
  /**
   * DateTime 	When a taxi will pickup a passenger or a rental car can be picked up.
   */
  public DateTime pickupTime;

  /**
   * Taxi details object
   */
  public Taxi reservationFor;

  @Override
  public DateTime getStartDateTime() {
    return pickupTime;
  }

  @Override
  public DateTime getFinishDateTime() {
    return null;
  }
}
