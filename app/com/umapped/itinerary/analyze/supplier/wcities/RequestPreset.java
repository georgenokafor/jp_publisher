package com.umapped.itinerary.analyze.supplier.wcities;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

/**
 * Created by wei on 2017-03-31.
 */
public class RequestPreset
    implements Constants {
  private Map<String, String> presets;

  public static RequestPreset Children = new RequestPreset(CATEGORY,
                                                           new String[]{EVENT_CATEGORY_FAMILY,
                                                                        EVENT_CATEGORY_EDUCATION});
  public static RequestPreset GuidedTour = new RequestPreset(CATEGORY,
                                                             new String[]{EVENT_CATEGORY_TOUR,
                                                                          EVENT_CATEGORY_EDUCATION},
                                                             SEARCH_FOR,
                                                             new String[]{"Guided Tour"});
  public static RequestPreset Museum = new RequestPreset(CATEGORY, new String[]{EVENT_CATEGORY_MUSEUM});

  public static RequestPreset Entertainment = new RequestPreset(CATEGORY,
                                                                new String[]{EVENT_CATEGORY_CONCERT,
                                                                             EVENT_CATEGORY_THEATER,
                                                                             EVENT_CATEGORY_DANCE});

  public static RequestPreset Sports = new RequestPreset(CATEGORY, new String[] { EVENT_CATEGORY_SPORT});

  public static RequestPreset Hotel = new RequestPreset(CATEGORY, new String[] {RECORD_CATEGORY_ACCOMONDATION},
                                                        ACCOMONDATION_TYPE, new String[] {"hotel"});

  public static RequestPreset ContentHotel = new RequestPreset(CATEGORY, new String[] { "1"});
  public static RequestPreset ContentAttraction = new RequestPreset(CATEGORY, new String[] { "6,7,,5,10"});

  public static RequestPreset ContentNightlife = new RequestPreset(CATEGORY, new String[] { "3"});
  public static RequestPreset ContentRestaurant = new RequestPreset(CATEGORY, new String[] { "2","4"});
  public static RequestPreset ContentShopping = new RequestPreset(CATEGORY, new String[] { "8"});
//6 Museums
//7 Theater
//10 outdoor
// 5 family

  public RequestPreset(String key, String[] values) {
    HashMap<String, String> map = new HashMap();
    map.put(key, StringUtils.join(values, ","));
    presets = Collections.unmodifiableMap(map);
  }

  public RequestPreset(String key1, String[] values1, String key2, String[] values2) {
    HashMap<String, String> map = new HashMap();
    map.put(key1, StringUtils.join(values1, ","));
    map.put(key2, StringUtils.join(values2, ","));
    presets = Collections.unmodifiableMap(map);
  }

  public RequestPreset() {
    HashMap<String, String> map = new HashMap();

    String[] values = new String[]{EVENT_CATEGORY_FAMILY,
            EVENT_CATEGORY_EDUCATION, EVENT_CATEGORY_TOUR,EVENT_CATEGORY_MUSEUM, EVENT_CATEGORY_CONCERT, EVENT_CATEGORY_THEATER, EVENT_CATEGORY_DANCE, EVENT_CATEGORY_SPORT };
    map.put(CATEGORY, StringUtils.join(values, ","));
    presets = Collections.unmodifiableMap(map);
  }

  public Set<String> getKeys() {
    return presets.keySet();
  }

  public String getValue(String key) {
    return presets.get(key);
  }
}
