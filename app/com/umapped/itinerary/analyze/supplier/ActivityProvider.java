package com.umapped.itinerary.analyze.supplier;

import com.umapped.itinerary.analyze.TripFeature;
import com.umapped.itinerary.analyze.recommendation.RecommendedItem;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeature;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wei on 2017-04-20.
 */
public interface ActivityProvider {
  List<RecommendedItem> findActivities(DecisionResult decisionResult,
                                                    EnumMap<TripFeature, Object> tripFeatures,
                                                    EnumMap<SegmentFeature, Object> segmentFeatures);
}
