package com.mapped.publisher.parse.tramada;

import com.braintreegateway.util.Http;
import com.mapped.common.CoreConstants;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.ConfigMgr;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.SystemDefaultCredentialsProvider;

import java.io.*;
import java.sql.Connection;
import java.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.parse.extractor.booking.email.ReservationsHolder;
import com.mapped.publisher.parse.schemaorg.*;
import com.mapped.publisher.persistence.bookingapi.BookingRS;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import controllers.ApiBookingController;
import models.publisher.BkApiSrc;
import org.apache.commons.lang3.time.DateUtils;

import javax.rmi.CORBA.Util;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;
import com.mapped.publisher.parse.tramada.DocumentInformation.Booking.ItineraryItems.ItineraryItem;
import com.mapped.publisher.parse.tramada.DocumentInformation.Booking.BookingDetails.BookingPassengers.TicketNumbers.Ticket;
import com.mapped.publisher.parse.tramada.DocumentInformation.Booking.CostingsItems.CostingItem;

/**
 * Created by george on 2017-08-08.
 */
public class TramadaHelper {
  private static Pattern salutationPattern = Pattern.compile("(\\s)(MR|MRS|MS|DR|MISS)");
  long startTimestamp = 0;
  long endTimestamp = 0;


  public List<ItineraryItem> flights;
  public List<ItineraryItem> hotels;
  public List<ItineraryItem> cruises;
  public List<ItineraryItem> cars;
  public List<ItineraryItem> transfers;
  public List<ItineraryItem> trains;
  public List<ItineraryItem> activities;
  public List<ItineraryItem> buses;
  public List<ItineraryItem> notes;
  public List<ItineraryItem> miscItems;
  public DocumentInformation docInfo;
  public DocumentInformation.Booking.CostingsItems.DocumentTotals documentTotals;
  public DocumentInformation.DocumentDetails.DocumentHeaders docHeaders;
  public DocumentInformation.DocumentDetails.DocumentFooters docFooters;
  public DocumentInformation.DocumentDetails.DocumentContactDetails contactDetails;
  public String tripStartDate;
  public String tripEndDate;
  Map<String, Ticket> ticketMap;
  Map<String, List<CostingItem>> costingMap;
  String currency;

  List<Person> defaultUnderNames;

  public TramadaHelper() {
    flights = new ArrayList<>(1);
    hotels = new ArrayList<>(1);
    cars = new ArrayList<>(1);
    cruises = new ArrayList<>(1);
    transfers = new ArrayList<>(1);
    trains = new ArrayList<>(1);
    activities = new ArrayList<>(1);
    buses  = new ArrayList<>(1);
    notes = new ArrayList<>(1);
    miscItems = new ArrayList<>(1);
    ticketMap = new HashMap<>(1);
    costingMap = new HashMap<>();
    defaultUnderNames = new ArrayList<>();
  }


  public int callBookingApi(ReservationPackage reservationPackage, String token) {
    int code = 0;
    try {
      if (reservationPackage != null) {
        HttpClient c = HttpClientBuilder.create().build();
        HttpPost p = new HttpPost(ConfigMgr.getAppParameter(CoreConstants.HOST_URL) + "/api/booking");
        p.setHeader("Content-Type", "text/plain");//application/json
        p.setHeader("Authorization", "Bearer " + token);

        ObjectMapper om = new ObjectMapper();
        StringEntity se = new StringEntity(om.writeValueAsString(reservationPackage));
        p.setEntity(se);

        HttpResponse r = c.execute(p);

        ResponseHandler<String> handler = new BasicResponseHandler();
        code = r.getStatusLine().getStatusCode();
        String body = handler.handleResponse(r);
      }
    } catch (Exception e) {
      Log.err("TramadaHelper:callBookingAPi - Error calling bookingAPI " + reservationPackage.reservationNumber, e);
    }

    return code;
  }



  public boolean parseXMLPayload(String xml) {
    boolean result = true;
    try {


      JAXBContext jaxbContext = JAXBContext.newInstance(DocumentInformation.class);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      StringReader reader = new StringReader(xml);

      DocumentInformation documentInformation = (DocumentInformation) jaxbUnmarshaller.unmarshal(reader);

      if (documentInformation != null) {

        docInfo = documentInformation;
        parseReservation(docInfo);
      }

    } catch (Exception e) {
      Log.err("ReservationHolder: Error parsing XML", e);
      result = false;
    }
    return result;
  }

  public void parseReservation(DocumentInformation documentInformation) {
    //ITINERARY ITEM MAPPINGS
    if (documentInformation.booking != null && documentInformation.booking.itineraryItems != null && documentInformation.booking.itineraryItems.itineraryItem != null) {
      for (ItineraryItem itineraryItem : documentInformation.booking.itineraryItems.itineraryItem) {
        assignReservation(itineraryItem);
      }

      //TICKET NUMBER MAPPINGS
      if (documentInformation.booking.bookingDetails != null && documentInformation.booking.bookingDetails.bookingPassengers != null
          && documentInformation.booking.bookingDetails.bookingPassengers.ticketNumbers != null
          && documentInformation.booking.bookingDetails.bookingPassengers.ticketNumbers.getTicket().size() > 0) {
        for (Ticket tkt : documentInformation.booking.bookingDetails.bookingPassengers.ticketNumbers.getTicket()) {
          String key = tkt.getPassengerName().getValue().trim() + "-" + tkt.getCarrierCode().getValue().trim();
          ticketMap.put(key, tkt);
        }
      }

      //DEFAULT Traveller Names
      if (docInfo.booking != null && docInfo.booking.bookingDetails != null && docInfo.booking.bookingDetails.bookingPassengers != null
          && docInfo.booking.bookingDetails.bookingPassengers.bookingPassenger != null && docInfo.booking.bookingDetails.bookingPassengers.bookingPassenger.size() > 0) {
        for (DocumentInformation.Booking.BookingDetails.BookingPassengers.BookingPassenger passenger : docInfo.booking.bookingDetails.bookingPassengers.bookingPassenger) {
          if (passenger != null && passenger.passengerName != null && passenger.passengerName.getValue() != null) {
            Person p = parsePassenger(passenger.passengerName.getValue());
            if (p != null) {
              defaultUnderNames.add(p);
            }
          }
        }
      } else {
        Person p = parsePassenger("TRAVELLER/A MR");
        if (p != null) {
          defaultUnderNames.add(p);
        }
      }

      //INVOICE MAPPINGS
      if (documentInformation.booking.costingsItems != null && documentInformation.booking.costingsItems.getCostingItem().size() > 0) {
        for (CostingItem cost : documentInformation.booking.costingsItems.getCostingItem()) {
          String key = null;
          if (cost.segmentTypeCode.getValue().contains("Package")) { //Packages
            if (cost.sourceReloc != null && cost.sourceReloc.getValue() != null) {
              key = cost.sourceReloc.getValue();
            } else if (cost.confirmationNo != null && cost.confirmationNo.getValue() != null) {
              key = cost.confirmationNo.getValue();
            }
          } else if (cost.segmentTypeCode.getValue().contains("Cruise")) { //Cruises
            key = "Cruise";
            if (cost.startCity != null && cost.startCity.getValue() != null) {
              key += "-" + cost.startCity.getValue();
            }
            if (cost.sourceReloc != null && cost.sourceReloc.getValue() != null) {
              key += "-" + cost.sourceReloc.getValue();
            } else if (cost.confirmationNo != null && cost.confirmationNo.getValue() != null) {
              key += "-" + cost.confirmationNo.getValue();
            }

          } else if (cost.segmentTypeCode.getValue().contains("Misc") || cost.segmentTypeCode.getValue().contains("Car")) { //Car Rental
            key = "Car";
            if (cost.startCity != null && cost.startCity.getValue() != null) {
              key += "-" + cost.startCity.getValue();
            }

          } else if (cost.segmentTypeCode.getValue().contains("Ticket") || cost.segmentTypeCode.getValue().contains("Flight")) { //Flights
            key = "Flight";
            if (cost.tktSpecific != null && cost.tktSpecific.airlineCode != null && cost.tktSpecific.airlineCode.getValue() != null) {
              key += "-" + cost.tktSpecific.airlineCode.getValue();
            }
            if (cost.sourceReloc != null && cost.sourceReloc.getValue() != null) {
              key += "-" + cost.sourceReloc.getValue();
            } else if (cost.confirmationNo != null && cost.confirmationNo.getValue() != null) {
              key += "-" + cost.confirmationNo.getValue();
            }
          } else { // Hotels, Transfers etc
            key = cost.segmentTypeCode.getValue();
            if (cost.startCity != null && cost.startCity.getValue() != null) {
              key += "-" + cost.startCity.getValue();
            }
            if (cost.sourceReloc != null && cost.sourceReloc.getValue() != null) {
              key += "-" + cost.sourceReloc.getValue();
            } else if (cost.confirmationNo != null && cost.confirmationNo.getValue() != null) {
              key += "-" + cost.confirmationNo.getValue();
            }
          }

          if (key != null) {
            if (costingMap.containsKey(key)) {
              costingMap.get(key).add(cost);
            } else {
              List costList = new ArrayList();
              costList.add(cost);
              costingMap.put(key, costList);
            }
          }
        }
      }

      //GET CURRENCY
      if (documentInformation.booking.costingsItems != null && documentInformation.booking.costingsItems.documentTotals != null) {
        documentTotals = documentInformation.booking.costingsItems.documentTotals;
        currency = documentTotals.currency.getValue();
      }

      //GET CONTACT DETAILS
      if (documentInformation.documentDetails.documentContactDetails != null) {
        DocumentInformation.DocumentDetails.DocumentContactDetails cd = documentInformation.documentDetails.documentContactDetails;
        if ((cd.emergencyContact != null && cd.emergencyContact.getValue() != null)
            || (cd.afterHours != null && cd.afterHours.getValue() != null)
            || (cd.tollFree != null && cd.tollFree.getValue() != null))
        contactDetails = documentInformation.documentDetails.documentContactDetails;
      }

      //GET DOCUMENT HEADERS
      if (documentInformation.documentDetails.documentHeaders != null
          && documentInformation.documentDetails.documentHeaders.documentHeader != null
          && documentInformation.documentDetails.documentHeaders.documentHeader.size() > 0) {
        docHeaders = documentInformation.documentDetails.documentHeaders;
      }

      //GET DOCUMENT FOOTERS
      if (documentInformation.documentDetails.documentFooters != null
          && documentInformation.documentDetails.documentFooters.documentFooter != null
          && documentInformation.documentDetails.documentFooters.documentFooter.size() > 0) {
        docFooters = documentInformation.documentDetails.documentFooters;
      }
    }
  }

  public void assignReservation(ItineraryItem reservation) {
    if (reservation.getSegmentTypeCode().getLabel().toUpperCase().equals("FLIGHT")) {
      flights.add(reservation);
    } else if (reservation.getSegmentTypeCode().getLabel().toUpperCase().equals("HOTEL")) {
      hotels.add(reservation);
    } else if (reservation.getSegmentTypeCode().getLabel().toUpperCase().equals("CRUISE")) {
      cruises.add(reservation);
    } else if (reservation.getSegmentTypeCode().getLabel().toUpperCase().equals("TRANSFER") 
        || reservation.getSegmentTypeCode().getLabel().toUpperCase().equals("BUS") 
        || reservation.getSegmentTypeCode().getLabel().toUpperCase().equals("FERRY")) {
      transfers.add(reservation);
    } else if (reservation.getSegmentTypeCode().getLabel().toUpperCase().equals("CAR")) {
      cars.add(reservation);
    } else if (reservation.getSegmentTypeCode().getLabel().toUpperCase().equals("COMMENT")) {
      notes.add(reservation);
    } else if (reservation.getSegmentTypeCode().getLabel().toUpperCase().equals("TRAIN")) {
      trains.add(reservation);
    } else if (reservation.getSegmentTypeCode().getLabel().toUpperCase().equals("ACTIVITY")
        || reservation.getSegmentTypeCode().getLabel().toUpperCase().equals("TOUR")
        || reservation.getSegmentTypeCode().getLabel().toUpperCase().equals("LEISURE")) {
      activities.add(reservation);
    } else { //MISC and all other label
      assignMiscReservation(reservation);
    }

  }

  public void assignMiscReservation(ItineraryItem reservation) {
    String supplierType = "";
    String startLabel = "";
    String endLabel = "";
    String itemLabel = "";
    String serviceTypeLabel = "";
    String serviceClassLabel = "";
    String startLocDescLabel = "";
    String endLocDescLabel = "";

    if (reservation.supplier != null && reservation.supplier.supplierType != null && reservation.supplier.supplierType.getValue() != null) {
      supplierType = reservation.supplier.supplierType.getValue();
    }
    if (reservation.start != null && reservation.start.startLabelValue != null && reservation.start.startLabelValue.getValue() != null) {
      startLabel = reservation.start.startLabelValue.getValue();
    }
    if (reservation.end != null && reservation.end.endLabelValue != null && reservation.end.endLabelValue.getValue() != null) {
      endLabel = reservation.end.endLabelValue.getValue();
    }
    if (reservation.noOfItems != null && reservation.noOfItems.getLabel() != null) {
      itemLabel = reservation.noOfItems.getLabel();
    }
    if (reservation.serviceTypeDescription != null && reservation.serviceTypeDescription.getLabel() != null) {
      serviceTypeLabel = reservation.serviceTypeDescription.getLabel().toLowerCase();
    }
    if (reservation.serviceClassDescription != null && reservation.serviceClassDescription.getLabel() != null) {
      serviceClassLabel = reservation.serviceClassDescription.getLabel().toLowerCase();
    }
    if (reservation.start != null && reservation.start.locationDescription != null && reservation.start.locationDescription.getLabel() != null) {
      startLabel = reservation.start.locationDescription.getLabel().toLowerCase();
    }
    if (reservation.end != null && reservation.end.locationDescription != null && reservation.end.locationDescription.getLabel() != null) {
      endLabel = reservation.end.locationDescription.getLabel().toLowerCase();
    }

    if (supplierType.contains("Flight")) {
      flights.add(reservation);
    } else if (supplierType.contains("Hotel")) {
      hotels.add(reservation);
    } else if (supplierType.contains("Cruise")) {
      cruises.add(reservation);
    } else if (supplierType.contains("Ferry")) {
      transfers.add(reservation);
    } else if (supplierType.contains("Transfer")) {
      transfers.add(reservation);
    } else if (supplierType.contains("Car")) {
      cars.add(reservation);
    } else if (supplierType.contains("Bus")) {
      transfers.add(reservation);
    } else if (supplierType.contains("Activity")) {
      activities.add(reservation);
    } else if (supplierType.contains("Train")) {
      trains.add(reservation);
    } else if (supplierType.contains("Comment")) {
      notes.add(reservation);
    } else if (supplierType.contains("Leisure")) {
      activities.add(reservation);
    } else if (supplierType.contains("Tour")) {
      activities.add(reservation);
    } else { //supplier type is null or doesn't match any above
      if (serviceTypeLabel.contains("aircraft") || serviceClassLabel.contains("class") || startLocDescLabel.contains("terminal") || endLocDescLabel.contains("terminal")) {
        flights.add(reservation);
      } else if (serviceTypeLabel.contains("cabin") || serviceClassLabel.contains("cabin") || startLocDescLabel.contains("port") || endLocDescLabel.contains("port")) {
        cruises.add(reservation);
      } else if (reservation.flightSpecificDetails != null) { //check if flights and cruises

        String airRelocLabel = "";
        String fareLabel = "";
        String codeShareLabel = "";
        if (reservation.flightSpecificDetails.airlineReloc != null && reservation.flightSpecificDetails.airlineReloc.getLabel() != null) {
          airRelocLabel = reservation.flightSpecificDetails.airlineReloc.getLabel().toLowerCase();
        }
        if (reservation.flightSpecificDetails.fareBasis != null && reservation.flightSpecificDetails.fareBasis.getLabel() != null) {
          fareLabel = reservation.flightSpecificDetails.fareBasis.getLabel().toLowerCase();
        }
        if (reservation.flightSpecificDetails.codeShare != null && reservation.flightSpecificDetails.codeShare.getLabel() != null) {
          codeShareLabel = reservation.flightSpecificDetails.codeShare.getLabel().toLowerCase();
        }

        if (airRelocLabel.contains("airline") || reservation.flightSpecificDetails.legs != null) {
          flights.add(reservation);
        } else if (airRelocLabel.contains("cabin") || fareLabel.contains("cruise") || codeShareLabel.contains("ship")) {
          cruises.add(reservation);
        } else {
          miscItems.add(reservation);
        }
      } else {
        if (startLabel.contains("Check In") || endLabel.contains("Check Out") || itemLabel.contains("No. Of Rooms")) {
          hotels.add(reservation);
        } else if (startLabel.contains("Embark") || endLabel.contains("Disembark")) {
          if (itemLabel.contains("No. Of Units")) {
            trains.add(reservation);
          } else if (itemLabel.contains("No. Of Passengers")) {
            transfers.add(reservation);
          } else {
            miscItems.add(reservation);
          }
        } else if (startLabel.contains("Pick Up") || endLabel.contains("Drop Off")) {
          if (itemLabel.contains("No. Of Cars")) {
            cars.add(reservation);
          } else if (itemLabel.contains("No. Of Passengers")) {
            transfers.add(reservation);
          } else {
            miscItems.add(reservation);
          }
        } else {
          miscItems.add(reservation);
        }
      }
    }
  }

  public ReservationPackage toReservationPackage() {
    ReservationPackage rp = new ReservationPackage();
    rp.reservationNumber = docInfo.getBooking().getBookingDetails().getBookingNumber().getValue();

    if(docInfo.getDocumentDetails() != null && docInfo.getDocumentDetails().getDocumentTitle() != null && docInfo.getDocumentDetails().getDocumentTitle().getLabel() != null) {
      rp.name = docInfo.getDocumentDetails().getDocumentTitle().getValue();
    } else if(docInfo.getBooking() != null && docInfo.getBooking().getBookingDetails() != null && docInfo.getBooking().getBookingDetails().getBookingNumber() != null) {
      rp.name = docInfo.getBooking().getBookingDetails().getBookingNumber().getValue();
    }else {
      rp.name = "Tramada";
    }
    

    rp.reservation.addAll(toFlightReservations());
    rp.reservation.addAll(toHotelReservations());
    rp.reservation.addAll(toCruiseReservations());
    rp.reservation.addAll(toTransferReservations());
    rp.reservation.addAll(toCarReservations());
    rp.reservation.addAll(toRailReservations());
    rp.reservation.addAll(toActivityReservations());
    rp.reservation.addAll(miscToReservationNotes());
    /* Need to process and store regular itinerary notes first, in order to obtain complete itinerary dates
     * We will need these complete dates to process the generated header and footer notes
     * But we should add all header and footer notes first to maintain order (as per Reservation Package logic)
     * Then finally add the already processed notes from earlier
     */
    List<Note> noteList = toReservationNotes();
    rp.reservation.addAll(toHeaderFooterNotes());
    rp.reservation.addAll(noteList);

    if (rp.reservation.size() > 0) {

      return rp;
    }
    return null;
  }

  public List<Note> toReservationNotes() {
    List<Note> noteReservations = new ArrayList<>();
    for (ItineraryItem note: notes) {
      Note n = new Note();

      if (note.serviceTypeDescription != null) {
        n.name = note.serviceTypeDescription.getValue();
      }
      if (note.itemNo != null) {
        n.reservationId = note.itemNo.toString();
      }
      if (note.start != null && note.start.locationDescription != null && note.start.cityCode != null
          && note.start.cityName != null && note.start.address != null) {
        n.reservationFor = getAddress(note.start.locationDescription.getValue(),
            note.start.cityName.getValue(),
            note.start.cityCode.getValue(),
            note.start.address);
      }

      StringBuilder sb = new StringBuilder();
      if (note.itineraryNotes != null) {
        sb.append(getNotes(note.itineraryNotes));
        if (note.itineraryNotes.fareRuleNotes != null && note.itineraryNotes.fareRuleNotes.getValue() != null) {
          sb.append("\n\n");
          sb.append("Fare Rules: ");
          sb.append("\n");
          sb.append(note.itineraryNotes.fareRuleNotes.getValue());
        }
      }

      n.description = sb.toString();
      
      String startDate = "";
      if (note.start.serviceDate != null && note.start.serviceDate.getValue() != null) {
        startDate = note.start.serviceDate.getValue();
        if (note.start.serviceTime != null && note.start.serviceTime.getValue() != null) {
          startDate += " " + note.start.serviceTime.getValue();
        } else {
          startDate += " 00:00";
        }
      }

      if (startDate != null && !startDate.isEmpty()) {
        n.noteTimestamp = getDateTime(startDate);
      }
      
      noteReservations.add(n);
    }

    return noteReservations;
  }

  public List<Note> miscToReservationNotes() {
    List<Note> noteReservations = new ArrayList<>();
    for (ItineraryItem misc: miscItems) {
      Note n = new Note();

      n.name = "Miscellaneous";
      if (misc.segmentTypeCode != null && misc.segmentTypeCode.getValue() != null) {
        n.name += " - " + misc.segmentTypeCode.getValue();
      }
      if (misc.itemNo != null) {
        n.reservationId = misc.itemNo.toString();
        n.name += " - Item no. " + misc.itemNo.toString();

      }
      if (misc.start != null && misc.start.locationDescription != null && misc.start.cityCode != null
          && misc.start.cityName != null && misc.start.address != null) {
        n.reservationFor = getAddress(misc.start.locationDescription.getValue(),
            misc.start.cityName.getValue(),
            misc.start.cityCode.getValue(),
            misc.start.address);
      }

      StringBuilder sb = new StringBuilder();
      if (misc.segmentTypeCode != null && misc.segmentTypeCode.getValue() != null) {
        sb.append("<b>Segment Type Code: </b>").append(misc.segmentTypeCode.getValue()).append("\n");
      }
      if (misc.status != null && misc.status.getValue() != null) {
        sb.append("<b>Status: </b>").append(misc.status.getValue()).append("\n");
      } else {
        sb.append("<b>Status:</b> Confirmed").append("\n");
      }
      if (misc.sourceReloc != null && misc.sourceReloc.getValue() != null) {
        sb.append("<b>Source Reloc: </b>").append(misc.sourceReloc.getValue()).append("\n");
      }
      if (misc.passengerDetails != null && misc.passengerDetails.passengerNameList != null && misc.passengerDetails.passengerNameList.getValue() != null) {
        sb.append("<b>Passenger Details: </b>").append(misc.passengerDetails.passengerNameList.getValue()).append("\n");
      }
      if (misc.supplier != null) {
        if (misc.supplier.supplierType != null && misc.supplier.supplierType.getValue() != null) {
          sb.append("<b>Supplier Type: </b>").append(misc.supplier.supplierType.getValue()).append("\n");
        }
        if (misc.supplier.supplierName != null && misc.supplier.supplierName.getValue() != null) {
          sb.append("<b>Supplier Name: </b>").append(misc.supplier.supplierName.getValue()).append("\n");
        }
      }
      if (misc.serviceTypeDescription != null && misc.serviceTypeDescription.getValue() != null) {
        if (misc.serviceTypeDescription.getLabel() != null) {
          sb.append("<b>").append(misc.serviceTypeDescription.getLabel()).append(": </b>");
        } else {
          sb.append("<b>Service Type Description:</b> ");
        }
        sb.append(misc.serviceTypeDescription.getValue()).append("\n");
      }
      if (misc.serviceIdentifier != null && misc.serviceIdentifier.getValue() != null) {
        sb.append("<b>Service Identifier: </b>").append(misc.serviceIdentifier.getValue()).append("\n");
      }
      if (misc.serviceClassDescription != null && misc.serviceClassDescription.getValue() != null) {
        if (misc.serviceClassDescription.getLabel() != null) {
          sb.append("<b>").append(misc.serviceClassDescription.getLabel()).append(": </b>");
        } else {
          sb.append("<b>Service Class Description: </b>");
        }
        sb.append(misc.serviceClassDescription.getValue()).append("\n");
      }
      if (misc.noOfItems != null && misc.noOfItems.getValue() != null) {
        if (misc.noOfItems.getLabel() != null) {
          sb.append("<b>").append(misc.noOfItems.getLabel()).append(": </b>");
        } else {
          sb.append("<b>No Of Items: </b>");
        }
        sb.append(misc.noOfItems.getValue()).append("\n");
      }
      if (misc.noOfdays != null && misc.noOfdays.getValue() != null) {
        sb.append("<b>No Of Days: </b>").append(misc.noOfdays.getValue()).append("\n");
      }
      sb.append("\n");

      if (misc.start != null) {
        boolean hasItem = false;
        if (misc.start.startLabelValue != null && misc.start.startLabelValue.getValue() != null) {
          sb.append(misc.start.startLabelValue.getValue()).append("\n");
          hasItem = true;
        }
        if (misc.start.serviceDate != null && misc.start.serviceDate.getValue() != null) {
          String startDate = misc.start.serviceDate.getValue();
          if (misc.start.serviceTime != null && misc.start.serviceTime.getValue() != null) {
            startDate += " " + misc.start.serviceTime.getValue();
          } else {
            startDate += " 00:00";
          }
          n.noteTimestamp = getDateTime(startDate);
          sb.append("<b>Start Date: </b>").append(startDate).append("\n");
          hasItem = true;
        }
        if (misc.start.cityCode != null && misc.start.cityCode.getValue() != null) {
          sb.append("<b>Start City Code: </b>").append(misc.start.cityCode.getValue()).append("\n");
          hasItem = true;
        }
        if (misc.start.cityName != null && misc.start.cityName.getValue() != null) {
          sb.append("<b>Start City Name: </b>").append(misc.start.cityName.getValue()).append("\n");
          hasItem = true;
        }
        if (misc.start.locationDescription != null && misc.start.locationDescription.getValue() != null) {
          if (misc.start.locationDescription.getLabel() != null) {
            sb.append("<b>").append(misc.start.locationDescription.getLabel()).append(": </b>");
          } else {
            sb.append("<b>Start Location: </b>");
          }
          sb.append(misc.start.locationDescription.getValue()).append("\n");
          hasItem = true;
        }
        if (hasItem) {
          sb.append("\n");
        }
      }

      if (misc.end != null) {
        boolean hasItem = false;
        if (misc.end.endLabelValue != null && misc.end.endLabelValue.getValue() != null) {
          sb.append(misc.end.endLabelValue.getValue()).append("\n");
          hasItem = true;
        }
        if (misc.end.serviceDate != null && misc.end.serviceDate.getValue() != null) {
          String endDate = misc.end.serviceDate.getValue();
          if (misc.end.serviceTime != null && misc.end.serviceTime.getValue() != null) {
            endDate += " " + misc.end.serviceTime.getValue();
          } else {
            endDate += " 00:00";
          }
          n.noteTimestamp = getDateTime(endDate);
          sb.append("<b>End Date: </b>").append(endDate).append("\n");
          hasItem = true;
        }
        if (misc.end.cityCode != null && misc.end.cityCode.getValue() != null) {
          sb.append("<b>End City Code: </b>").append(misc.end.cityCode.getValue()).append("\n");
          hasItem = true;
        }
        if (misc.end.cityName != null && misc.end.cityName.getValue() != null) {
          sb.append("<b>End City Name: </b>").append(misc.end.cityName.getValue()).append("\n");
        }
        if (misc.end.locationDescription != null && misc.end.locationDescription.getValue() != null) {
          if (misc.end.locationDescription.getLabel() != null) {
            sb.append("<b>").append(misc.end.locationDescription.getLabel()).append(": </>");
          } else {
            sb.append("<b>End Location: </b>");
          }
          sb.append(misc.end.locationDescription.getValue()).append("\n");
          hasItem = true;
        }
        if (hasItem) {
          sb.append("\n");
        }
      }
      
      if (misc.flightSpecificDetails != null) {
        boolean hasItem = false;

        if (misc.flightSpecificDetails.codeShare != null && misc.flightSpecificDetails.codeShare.getValue() != null && misc.flightSpecificDetails.codeShare.getLabel() != null) {
          sb.append("<b>").append(misc.flightSpecificDetails.codeShare.getLabel()).append(": </b>").append(misc.flightSpecificDetails.codeShare.getValue()).append("\n");
          hasItem = true;
        }
        if (misc.flightSpecificDetails.fareBasis != null && misc.flightSpecificDetails.fareBasis.getValue() != null && misc.flightSpecificDetails.fareBasis.getLabel() != null) {
          sb.append("<b>").append(misc.flightSpecificDetails.fareBasis.getLabel()).append(": </b>").append(misc.flightSpecificDetails.fareBasis.getValue()).append("\n");
          hasItem = true;
        }
        if (misc.flightSpecificDetails.airlineReloc != null && misc.flightSpecificDetails.airlineReloc.getValue() != null && misc.flightSpecificDetails.airlineReloc.getLabel() != null) {
          sb.append("<b>").append(misc.flightSpecificDetails.airlineReloc.getLabel()).append(": </b>").append(misc.flightSpecificDetails.airlineReloc.getValue()).append("\n");
          hasItem = true;
        }
        if (misc.flightSpecificDetails.legs != null && misc.flightSpecificDetails.legs.legsSummary != null && misc.flightSpecificDetails.legs.legsSummary.getValue() != null) {
          sb.append("<b>Leg Summary: </b>").append(misc.flightSpecificDetails.legs.legsSummary.getValue()).append("\n");
          hasItem = true;
        }

        if (hasItem) {
          sb.append("\n");
        }

      }
      
      if (misc.accountDetails != null) {
        sb.append("\n");
        if (misc.accountDetails.cancelConditions != null && misc.accountDetails.cancelConditions.getValue() != null) {
          sb.append("<b>Cancel Conditions: </b>").append("\n").append(misc.accountDetails.cancelConditions.getValue()).append("\n\n");
        }
        if (misc.accountDetails.cancellationDescription != null && misc.accountDetails.cancellationDescription.getValue() != null) {
          sb.append("<b>Cancellation Description: </b>").append("\n").append(misc.accountDetails.cancellationDescription.getValue()).append("\n\n");
        }
      }

      if (misc.itineraryNotes != null) {
        sb.append(getNotes(misc.itineraryNotes));
        if (misc.itineraryNotes.fareRuleNotes != null && misc.itineraryNotes.fareRuleNotes.getValue() != null) {
          sb.append("\n\n");
          sb.append("</b>Fare Rules: <b>");
          sb.append("\n");
          sb.append(misc.itineraryNotes.fareRuleNotes.getValue());
        }
      }

      n.description = sb.toString();

      noteReservations.add(n);
    }

    return noteReservations;
  }

  public List<Note> toHeaderFooterNotes() {
    List<Note> noteReservations = new ArrayList<>();

    //CREATE CONTACTS AND HEADERS AS FIRST NOTE AND ADD TO LIST
    if (contactDetails != null || docHeaders != null) {
      Note n = new Note();
      n.name = "Important Notes";
      n.reservationId = "Important Notes";

      StringBuilder sb = new StringBuilder();
      if (contactDetails != null) {
        if (contactDetails.emergencyContact != null && contactDetails.emergencyContact.getValue() != null) {
          if (contactDetails.emergencyContact.getLabel() != null) {
            sb.append("<b>");
            sb.append(contactDetails.emergencyContact.getLabel());
            sb.append(" </b> ");
          } else {
            sb.append("<b>Emergency Contact: </b> ");
          }
          sb.append(contactDetails.emergencyContact.getValue());
          sb.append("\n");
        }
        if (contactDetails.afterHours != null && contactDetails.afterHours.getValue() != null) {
          if (contactDetails.afterHours.getLabel() != null) {
            sb.append("<b>");
            sb.append(contactDetails.afterHours.getLabel());
            sb.append(" </b> ");
          } else {
            sb.append("<b>After Hours Contact: </b> ");
          }
          sb.append(contactDetails.afterHours.getValue());
          sb.append("\n");
        }
        if (contactDetails.tollFree != null && contactDetails.tollFree.getValue() != null) {
          if (contactDetails.tollFree.getLabel() != null) {
            sb.append("<b>");
            sb.append(contactDetails.tollFree.getLabel());
            sb.append(" </b> ");
          } else {
            sb.append("<b>Toll Free Contact: </b> ");
          }
          sb.append(contactDetails.tollFree.getValue());
          sb.append("\n");
        }
        sb.append("\n\n");
      }

      if (docHeaders != null) {
        for (DocumentInformation.DocumentDetails.DocumentHeaders.DocumentHeader dh : docHeaders.documentHeader) {
          if (dh.getValue() != null) {
            if (dh.getHeaderTitle() != null) {
              sb.append("<h2><b>");
              sb.append(dh.getHeaderTitle());
              sb.append(" </b></h2>");
            }
            sb.append(dh.getValue());
            sb.append("\n\n\n");
          }
        }
      }
      n.description = sb.toString();
      noteReservations.add(n);
    }

    //CREATE INVOICE AS SECOND NOTE AND ADD TO LIST
    if (documentTotals != null) {
      Note n = new Note();
      n.name = "Invoice";
      n.reservationId = "Invoice";
      n.description = buildInvoice();
      noteReservations.add(n);
    }


    //CREATE FOOTERS AS LAST NOTE AND ADD TO LIST
    if (docFooters != null) {
      Note n = new Note();
      n.name = "Terms and Conditions";
      n.reservationId = "Terms and Conditions";
      n.potentialAction = APPConstants.NOTE_TAG_LAST_ITEM;

      StringBuilder sb = new StringBuilder();
      for (DocumentInformation.DocumentDetails.DocumentFooters.DocumentFooter df : docFooters.documentFooter) {
        if (df.getValue() != null) {
          if (df.getFooterTitle() != null) {
            sb.append("<h2><b>");
            sb.append(df.getFooterTitle());
            sb.append(" </b></h2>");
          }
          sb.append(df.getValue());
          sb.append("\n\n\n");
        }
      }
      n.description = sb.toString();
      noteReservations.add(n);
    }
    return noteReservations;
  }
  

  public List<FlightReservation> toFlightReservations() {
    List<FlightReservation> flightReservations = new ArrayList<>();
    for (ItineraryItem air: flights) {
      FlightReservation fr = new FlightReservation();
      if (air.itemNo != null) {
        fr.reservationId = air.itemNo.toString();
      }
      if (air.sourceReloc != null && air.sourceReloc.getValue() != null) {
        fr.reservationNumber = air.sourceReloc.getValue();
      } else if (air.confirmationNo != null && air.confirmationNo.getValue() != null) {
        fr.reservationNumber = air.confirmationNo.getValue();
      }
      fr.reservationStatus = "Confirmed";
      fr.reservationFor = new com.mapped.publisher.parse.schemaorg.Flight();
      fr.reservationFor.airline = new Airline();
      if (air.supplier != null && air.supplier.supplierName != null) {
        fr.reservationFor.airline.name = air.supplier.supplierName.getValue();
      }
      if (air.serviceIdentifier != null && air.serviceIdentifier.getValue() != null && air.serviceIdentifier.getValue().length() > 2) {
        fr.reservationFor.airline.iataCode = air.serviceIdentifier.getValue().substring(0, 2);
        fr.reservationFor.flightNumber = air.serviceIdentifier.getValue().substring(2);
      }

      fr.reservationFor.arrivalAirport = new Airport();
      if (air.end != null) {
        if (air.end.cityCode != null && air.end.cityCode.getValue() != null) {
          fr.reservationFor.arrivalAirport.iataCode = air.end.cityCode.getValue();
        }
        if (air.end.cityName != null && air.end.cityName.getValue() != null) {
          fr.reservationFor.arrivalAirport.name = air.end.cityName.getValue();
        }
        if (air.end.locationDescription != null && air.end.locationDescription.getValue() != null) {
          fr.reservationFor.arrivalTerminal = air.end.locationDescription.getValue();
        }
      }

      fr.reservationFor.departureAirport = new Airport();
      if (air.start != null) {
        if (air.start.cityCode != null && air.start.cityCode.getValue() != null) {
          fr.reservationFor.departureAirport.iataCode = air.start.cityCode.getValue();
        }
        if (air.start.cityName != null && air.start.cityName.getValue() != null) {
          fr.reservationFor.departureAirport.name = air.start.cityName.getValue();
        }
        if (air.start.locationDescription != null && air.start.locationDescription.getValue() != null) {
          fr.reservationFor.departureTerminal = air.start.locationDescription.getValue();
        }
      }

      String startDate = "";
      String endDate = "";

      if (air.start.serviceDate != null && air.start.serviceDate.getValue() != null) {
        startDate = air.start.serviceDate.getValue();
        if (air.start.serviceTime != null && air.start.serviceTime.getValue() != null) {
          startDate += " " + air.start.serviceTime.getValue();
        } else {
          startDate += " 00:00";
        }
      }
      if (air.end.serviceDate != null && air.end.serviceDate.getValue() != null) {
        endDate = air.end.serviceDate.getValue();
        if (air.end.serviceTime != null && air.end.serviceTime.getValue() != null) {
          endDate += " " + air.end.serviceTime.getValue();
        } else {
          endDate += " 00:00";
        }
      }
      fr.reservationFor.departureTime = getDateTime(startDate);
      fr.reservationFor.arrivalTime = getDateTime(endDate);
      
      if (air.accountDetails != null) {
        fr.cancellationPolicy = getCancellationPolicy(air.accountDetails);
      }

      if (air.passengerDetails != null && air.passengerDetails.passenger != null && air.passengerDetails.passenger.size() > 0) {
        for (ItineraryItem.PassengerDetails.Passenger p : air.passengerDetails.passenger) {
          if (p.passengerName != null && p.passengerName.getValue() != null) {
            FlightPassenger sub = new FlightPassenger();
            String psngrName = p.getPassengerName().getValue();
            sub.underName = parsePassenger(psngrName);
            if (p.flightSpecific != null) {
              if (p.flightSpecific.seatNo != null && p.flightSpecific.seatNo.getValue() != null) {
                sub.seat = p.flightSpecific.seatNo.getValue();
              }
              if (p.flightSpecific.seatTypeDescription != null && p.flightSpecific.seatTypeDescription.getValue() != null) {
                sub.seatingType = p.flightSpecific.seatTypeDescription.getValue();
              }
              if (p.flightSpecific.mealRequestedDescription != null && p.flightSpecific.mealRequestedDescription.getValue() != null) {
                sub.meal = p.flightSpecific.mealRequestedDescription.getValue();
              }
            }

            String key = psngrName.trim() + "-" + fr.reservationFor.airline.iataCode;
            if (ticketMap.get(key) != null) {
              Ticket tkt = ticketMap.get(key);
              sub.ticketNumber = tkt.getTicketNumber().getValue().trim();
            }

            sub.airplaneSeatClass = new AirplaneSeatClass();
            if (air.serviceClassDescription != null) {
              sub.airplaneSeatClass.name = air.serviceClassDescription.getValue();
            } else if (air.serviceClassCode !=  null) {
              sub.airplaneSeatClass.name = air.serviceClassCode.getValue();
            }
            fr.addPassenger(sub);
          }
        }

      } else if (air.passengerDetails != null && air.passengerDetails.getPassengerNameList() != null 
          && air.passengerDetails.getPassengerNameList().getValue() != null && air.passengerDetails.getPassengerNameList().getValue().length() > 1) {
        String[] nameList = air.passengerDetails.getPassengerNameList().getValue().split(",");
        for (String s : nameList) {
          FlightPassenger sub = new FlightPassenger();
          sub.underName = parsePassenger(s);

          String key = s.trim()+"-"+fr.reservationFor.airline.iataCode;
          if (ticketMap.get(key) != null) {
            Ticket tkt = ticketMap.get(key);
            sub.ticketNumber = tkt.getTicketNumber().getValue().trim();
          }

          sub.airplaneSeatClass = new AirplaneSeatClass();
          if (air.serviceClassDescription != null) {
            sub.airplaneSeatClass.name = air.serviceClassDescription.getValue();
          } else if (air.serviceClassCode !=  null) {
            sub.airplaneSeatClass.name = air.serviceClassCode.getValue();
          }
          fr.addPassenger(sub);
        }
      } else {
        for (Person p : defaultUnderNames) {
          FlightPassenger sub = new FlightPassenger();
          sub.underName = p;
          sub.airplaneSeatClass = new AirplaneSeatClass();
          if (air.serviceClassCode != null) {
            sub.airplaneSeatClass.name = air.serviceClassCode.getValue();
          } else if (air.serviceClassDescription != null) {
            sub.airplaneSeatClass.name = air.serviceClassDescription.getValue();
          }
          fr.addPassenger(sub);
        }
      }

      String key = "Flight";
      if (fr.reservationFor.airline.iataCode != null && !fr.reservationFor.airline.iataCode.isEmpty()) {
        key += "-" + fr.reservationFor.airline.iataCode;
      }
      if (air.sourceReloc != null && air.sourceReloc.getValue() != null) {
        key += "-" + air.sourceReloc.getValue();
      } else if (air.confirmationNo != null && air.confirmationNo.getValue() != null) {
        key += "-" + air.confirmationNo.getValue();
      }
      if (air.sourceReloc != null && costingMap.containsKey(air.sourceReloc.getValue())) {
        key = air.sourceReloc.getValue();
      }
      if (costingMap.containsKey(key)) {
        for (CostingItem cost : costingMap.get(key)) {
          String cityCodes = air.start.cityCode.getValue() + "-" + air.end.cityCode.getValue();
          if (cost.shortItinerary.getValue().contains(cityCodes)) {
            addPrice(cost, fr);
            break;
          }
        }
      }

      StringBuilder sb = new StringBuilder();
      if (air.flightSpecificDetails != null && air.flightSpecificDetails.legs != null && air.flightSpecificDetails.legs.legsSummary != null
          && air.flightSpecificDetails.legs.legsSummary.getValue() != null) {
        sb.append("Flight Summary: ");
        sb.append("\n");
        sb.append(air.flightSpecificDetails.legs.legsSummary.getValue());
        sb.append("\n");
      }
      if (air.itineraryNotes != null) {
        sb.append(getNotes(air.itineraryNotes));
      }
      fr.description = sb.toString();

      flightReservations.add(fr);
    }
    return flightReservations;
  }

  public List<LodgingReservation> toHotelReservations() {
    List<LodgingReservation> lodgingReservations = new ArrayList<>();
    for (ItineraryItem hotel : hotels) {
      LodgingReservation hr = new LodgingReservation();

      String startDate = "";
      String endDate = "";

      if (hotel.start.serviceDate != null) {
        startDate = hotel.start.serviceDate.getValue();
        if (hotel.start.serviceTime != null && hotel.start.serviceTime.getValue() != null) {
          startDate += " " + hotel.start.serviceTime.getValue();
        } else {
          startDate += " 00:00";
        }
      }
      if (hotel.end.serviceDate != null) {
        endDate = hotel.end.serviceDate.getValue();
        if (hotel.end.serviceTime != null && hotel.end.serviceTime.getValue() != null) {
          endDate += " " + hotel.end.serviceTime.getValue();
        } else {
          endDate += " 00:00";
        }
      }
      hr.checkinTime = getDateTime(startDate);
      hr.checkoutTime = getDateTime(endDate);

      if (hotel.itemNo != null) {
        hr.reservationId = hotel.itemNo.toString();
      }
      hr.reservationStatus = "Confirmed";
      hr.reservationFor = new LodgingBusiness();

      if (hotel.supplier != null) {
        if (hotel.supplier.supplierName != null) {
          hr.reservationFor.name = hotel.supplier.supplierName.getValue();
        }
        if (hotel.supplier.supplierAddress != null && hotel.supplier.supplierAddress.address != null) {
          hr.reservationFor.address = new PostalAddress();
          if (hotel.supplier.supplierAddress.address.getAddress1() != null && hotel.supplier.supplierAddress.address.getAddress1().getValue() != null) {
            hr.reservationFor.address.streetAddress = hotel.supplier.supplierAddress.address.getAddress1().getValue();
          }
          if (hotel.supplier.supplierAddress.address.getAddress2() != null && hotel.supplier.supplierAddress.address.getAddress2().getValue() != null) {
            hr.reservationFor.address.streetAddress += hotel.supplier.supplierAddress.address.getAddress2().getValue();
          }
          if (hotel.supplier.supplierAddress.address.getAddress3() != null && hotel.supplier.supplierAddress.address.getAddress3().getValue() != null) {
            hr.reservationFor.address.streetAddress += hotel.supplier.supplierAddress.address.getAddress3().getValue();
          }
          if (hotel.supplier.supplierAddress.address.state != null && hotel.supplier.supplierAddress.address.state.getValue() != null) {
            hr.reservationFor.address.addressRegion = hotel.supplier.supplierAddress.address.state.getValue();
          }
          if (hotel.supplier.supplierAddress.address.postCode != null && hotel.supplier.supplierAddress.address.postCode.getValue() != null) {
            hr.reservationFor.address.postalCode = hotel.supplier.supplierAddress.address.getPostCode().getValue();
          }
          if (hotel.supplier.supplierAddress.address.country != null && hotel.supplier.supplierAddress.address.country.getValue() != null) {
            hr.reservationFor.address.addressCountry = hotel.supplier.supplierAddress.address.country.getValue();
          }
        }
      }

      if (hotel.sourceReloc != null && hotel.sourceReloc.getValue() != null) {
        hr.reservationNumber = hotel.sourceReloc.getValue();
      } else if (hotel.confirmationNo != null && hotel.confirmationNo.getValue() != null) {
        hr.reservationNumber = hotel.confirmationNo.getValue();
      }
      if (hotel.serviceTypeDescription != null && hotel.serviceTypeDescription.getValue() != null) {
        hr.lodgingUnitType = hotel.serviceTypeDescription.getValue();
      }

      if (hotel.accountDetails != null) {
        hr.cancellationPolicy = getCancellationPolicy(hotel.accountDetails);
      }

      StringBuilder sb = new StringBuilder();
      if (hotel.serviceTypeDescription != null && hotel.serviceTypeDescription.getValue() != null) {
        sb.append("Room Type: ");
        sb.append(hotel.serviceTypeDescription.getValue());
        sb.append("\n");

      }
      if (hotel.noOfItems != null && hotel.noOfItems.getValue() != null) {
        sb.append("\n");
        sb.append("No of Rooms: ");
        sb.append(hotel.noOfItems.getValue());
        sb.append("\n");
      }
      if (hotel.noOfdays != null && hotel.noOfdays.getValue() != null) {
        sb.append("\n");
        sb.append("Duration: ");
        sb.append(hotel.noOfdays.getValue());
        sb.append("\n");
      }
      if (hotel.itineraryNotes != null) {
        sb.append(getNotes(hotel.itineraryNotes));
      }
      hr.description = sb.toString();


      if (hotel.passengerDetails != null && hotel.passengerDetails.passenger != null && hotel.passengerDetails.passenger.size() > 0) {
        for (ItineraryItem.PassengerDetails.Passenger p : hotel.passengerDetails.passenger) {
          if (p.passengerName != null && p.passengerName.getValue() != null) {
            RoomType sub = new RoomType();
            String psngrName = p.getPassengerName().getValue();
            sub.underName = parsePassenger(psngrName);
            if (hotel.serviceTypeDescription != null && hotel.serviceTypeDescription.getValue() != null) {
              sub.lodgingUnitType = hotel.serviceTypeDescription.getValue();
            }
            if (hotel.status != null && hotel.status.getValue() != null) {
              sub.confirmStatus = hotel.status.getValue();
            }
            hr.addRoom(sub);
          }
        }

      } else if (hotel.passengerDetails != null && hotel.passengerDetails.getPassengerNameList() != null 
          && hotel.passengerDetails.getPassengerNameList().getValue() != null && hotel.passengerDetails.getPassengerNameList().getValue().length() > 1) {
        String[] nameList = hotel.passengerDetails.getPassengerNameList().getValue().split(",");
        for (String s : nameList) {
          RoomType sub = new RoomType();
          sub.underName = parsePassenger(s);
          if (hotel.serviceTypeDescription != null && hotel.serviceTypeDescription.getValue() != null) {
            sub.lodgingUnitType = hotel.serviceTypeDescription.getValue();
          }
          if (hotel.status != null && hotel.status.getValue() != null) {
            sub.confirmStatus = hotel.status.getValue();
          }
          hr.addRoom(sub);
        }
      } else {
        for (Person p : defaultUnderNames) {
          RoomType sub = new RoomType();
          sub.underName = p;
          if (hotel.serviceTypeDescription != null && hotel.serviceTypeDescription.getValue() != null) {
            sub.lodgingUnitType = hotel.serviceTypeDescription.getValue();
          }
          sub.confirmStatus = "Confirmed";
          hr.addRoom(sub);
        }
      }

      String key = hotel.segmentTypeCode.getValue();
      if (hotel.start.cityCode != null && hotel.start.cityCode.getValue() != null) {
        key += "-" + hotel.start.cityCode.getValue();
      }
      if (hotel.sourceReloc != null && hotel.sourceReloc.getValue() != null) {
        key += "-" + hotel.sourceReloc.getValue();
      } else if (hotel.confirmationNo != null && hotel.confirmationNo.getValue() != null) {
        key += "-" + hotel.confirmationNo.getValue();
      }
      if (hotel.sourceReloc != null && costingMap.containsKey(hotel.sourceReloc.getValue())) {
        key = hotel.sourceReloc.getValue();
      }
      if (costingMap.containsKey(key)) {
        for (CostingItem cost : costingMap.get(key)) {
          if (cost.startCity.equals(hotel.start.cityCode.getValue())) {
            addPrice(cost, hr);
            break;
          }
        }
      }

      lodgingReservations.add(hr);

    }
    return lodgingReservations;
  }

  public List<CruiseShipReservation> toCruiseReservations() {
    List<CruiseShipReservation> cruiseReservations = new ArrayList<>();
    for (ItineraryItem cruise : cruises) {
      CruiseShipReservation csr = new CruiseShipReservation();
      csr.reservationId = cruise.itemNo.toString();
      if (cruise.sourceReloc != null && cruise.sourceReloc.getValue() != null) {
        csr.reservationNumber = cruise.sourceReloc.getValue();
      } else if (cruise.confirmationNo != null && cruise.confirmationNo.getValue() != null) {
        csr.reservationNumber = cruise.confirmationNo.getValue();
      }

      csr.name = cruise.supplier.supplierName.getValue();
      if (cruise.flightSpecificDetails != null && cruise.flightSpecificDetails.fareBasis != null && cruise.flightSpecificDetails.fareBasis.getValue() != null) {
        csr.name = cruise.flightSpecificDetails.fareBasis.getValue();
      }

      if (cruise.status != null) {
        csr.reservationStatus = cruise.status.getValue();
      }
      csr.reservationFor = new CruiseShip();
      if (cruise.supplier != null && cruise.supplier.supplierName != null) {
        csr.category = cruise.supplier.supplierName.getValue();
      }

      if (cruise.flightSpecificDetails != null && cruise.flightSpecificDetails.airlineReloc != null && cruise.flightSpecificDetails.airlineReloc.getValue() != null) {
        csr.cabinNumber = cruise.flightSpecificDetails.airlineReloc.getValue();
      }
      if (cruise.serviceClassDescription != null && cruise.serviceClassDescription.getValue() != null) {
        csr.deckNumber = cruise.serviceClassDescription.getValue();
      }
      if (cruise.serviceTypeDescription != null && cruise.serviceTypeDescription.getValue() != null) {
        csr.cabinType = cruise.serviceTypeDescription.getValue();
      }
      //csr.diningPlan = cruise.getDiningPlan();
      //csr.bedding = cruise.getBedding();

      String startDate = "";
      String endDate = "";

      if (cruise.start.serviceDate != null) {
        startDate = cruise.start.serviceDate.getValue();
        if (cruise.start.serviceTime != null && cruise.start.serviceTime.getValue() != null) {
          startDate += " " + cruise.start.serviceTime.getValue();
        } else {
          startDate += " 00:00";
        }
      }
      if (cruise.end.serviceDate != null) {
        endDate = cruise.end.serviceDate.getValue();
        if (cruise.end.serviceTime != null && cruise.end.serviceTime.getValue() != null) {
          endDate += " " + cruise.end.serviceTime.getValue();
        } else {
          endDate += " 00:00";
        }
      }
      
      csr.reservationFor.departTime = getDateTime(startDate);
      csr.reservationFor.arrivalTime = getDateTime(endDate);

      if (cruise.start != null && cruise.start.cityName != null) {
        csr.reservationFor.departPort = getAddress(cruise.start.cityName.getValue(), null, null, null);
      }
      if (cruise.end != null && cruise.end.cityName != null) {
        csr.reservationFor.arrivalPort = getAddress(cruise.end.cityName.getValue(), null, null, null);
      }

      csr.reservationFor.provider = new Organization();

      if (cruise.supplier != null && cruise.supplier.supplierName != null) {
        csr.reservationFor.provider.name = cruise.supplier.supplierName.getValue();
      }

      //csr.stops = buildCruiseStops(td.starttimestamp, td.endtimestamp);

      if (cruise.accountDetails != null) {
        csr.cancellationPolicy = getCancellationPolicy(cruise.accountDetails);
      }

      StringBuilder sb = new StringBuilder();
      if (cruise.flightSpecificDetails != null && cruise.flightSpecificDetails.codeShare != null && cruise.flightSpecificDetails.codeShare.getValue() != null) {
        sb.append("Ship Name: ");
        sb.append(cruise.flightSpecificDetails.codeShare.getValue());
        sb.append("\n");
      }
      if (csr.subReservation != null && csr.subReservation.size() > 0) {
        sb.append("No of Passengers: ");
        sb.append(csr.subReservation.size());
        sb.append("\n");
      } else if (cruise.noOfItems != null && cruise.noOfItems.getLabel() != null && cruise.noOfItems.getLabel().toLowerCase() == "no. of passengers" && cruise.noOfItems.getValue() != null) {
        sb.append("\n");
        sb.append("No of Passengers: ");
        sb.append(cruise.noOfItems.getValue());
        sb.append("\n");
      }
      if (cruise.noOfdays != null && cruise.noOfdays.getValue() != null) {
        sb.append("\n");
        sb.append("Duration: ");
        sb.append(cruise.noOfdays.getValue());
        sb.append("\n");
      }
      if (cruise.itineraryNotes != null) {
        sb.append(getNotes(cruise.itineraryNotes));
      }
      csr.description = sb.toString();

      if (cruise.passengerDetails != null && cruise.passengerDetails.passenger != null && cruise.passengerDetails.passenger.size() > 0) {
        for (ItineraryItem.PassengerDetails.Passenger p : cruise.passengerDetails.passenger) {
          if (p.passengerName != null && p.passengerName.getValue() != null) {
            CruiseShipReservation sub = new CruiseShipReservation();
            String psngrName = p.getPassengerName().getValue();
            sub.underName = parsePassenger(psngrName);
            if (cruise.status != null) {
              sub.reservationStatus = cruise.status.getValue();
            }
            csr.addSubReservation(sub);
          }
        }

      } else if (cruise.passengerDetails != null && cruise.passengerDetails.getPassengerNameList() != null 
          && cruise.passengerDetails.getPassengerNameList().getValue() != null && cruise.passengerDetails.getPassengerNameList().getValue().length() > 1) {
        String[] nameList = cruise.passengerDetails.getPassengerNameList().getValue().split(",");
        for (String s : nameList) {
          CruiseShipReservation sub = new CruiseShipReservation();
          sub.underName = parsePassenger(s);
          if (cruise.status != null) {
            sub.reservationStatus = cruise.status.getValue();
          }
          csr.addSubReservation(sub);
        }
      } else {
        for (Person p : defaultUnderNames) {
          CruiseShipReservation sub = new CruiseShipReservation();
          sub.underName = p;
          sub.reservationStatus = "Confirmed";
          csr.addSubReservation(sub);
        }
      }

      String key = "Cruise";
      if (cruise.start.cityCode != null && cruise.start.cityCode.getValue() != null) {
        key += "-" + cruise.start.cityCode.getValue();
      }
      if (cruise.sourceReloc != null && cruise.sourceReloc.getValue() != null) {
        key += "-" + cruise.sourceReloc.getValue();
      } else if (cruise.confirmationNo != null && cruise.confirmationNo.getValue() != null) {
        key += "-" + cruise.confirmationNo.getValue();
      }
      if (cruise.sourceReloc != null && costingMap.containsKey(cruise.confirmationNo.getValue())) {
        key = cruise.confirmationNo.getValue();
      }
      if (costingMap.containsKey(key)) {
        for (CostingItem cost : costingMap.get(key)) {
          if (cost.startCity.equals(cruise.start.cityCode.getValue())) {
            addPrice(cost, csr);
            break;
          }
        }
      }

      cruiseReservations.add(csr);

    }
    return cruiseReservations;
  }

  public List<TransferReservation> toTransferReservations() {
    List<TransferReservation> transferReservations = new ArrayList<>();
    for (ItineraryItem transfer : transfers) {
      TransferReservation tr = new TransferReservation();
      if (transfer.itemNo != null) {
        tr.reservationId = transfer.itemNo.toString();
      }
      if (transfer.sourceReloc != null && transfer.sourceReloc.getValue() != null) {
        tr.reservationNumber = transfer.sourceReloc.getValue();
      } else if (transfer.confirmationNo != null && transfer.confirmationNo.getValue() != null) {
        tr.reservationNumber = transfer.confirmationNo.getValue();
      }

      if (transfer.status != null) {
        tr.reservationStatus = transfer.status.getValue();
      }

      tr.reservationFor = new Transfer();
      String startDate = "";
      String endDate = "";

      if (transfer.start.serviceDate != null) {
        startDate = transfer.start.serviceDate.getValue();
        if (transfer.start.serviceTime != null && transfer.start.serviceTime.getValue() != null) {
          startDate += " " + transfer.start.serviceTime.getValue();
        } else {
          startDate += " 00:00";
        }
      }
      if (transfer.end.serviceDate != null) {
        endDate = transfer.end.serviceDate.getValue();
        if (transfer.end.serviceTime != null && transfer.end.serviceTime.getValue() != null) {
          endDate += " " + transfer.end.serviceTime.getValue();
        } else {
          endDate += " 00:00";
        }
      }
      
      tr.reservationFor.pickupTime = getDateTime(startDate);
      if (transfer.start != null && transfer.start.cityName != null && transfer.start.cityCode != null && transfer.start.address != null) {
        tr.reservationFor.pickupLocation = getAddress(transfer.start.locationDescription != null ? transfer.start.locationDescription.getValue() : "",
            transfer.start.cityName.getValue(),
            transfer.start.cityCode.getValue(),
            transfer.start.address);
      }
      
      tr.reservationFor.dropoffTime = getDateTime(endDate);
      if (transfer.end != null && transfer.end.cityName != null && transfer.end.cityCode != null && transfer.end.address != null) {
        tr.reservationFor.dropoffLocation = getAddress(transfer.end.locationDescription != null ? transfer.end.locationDescription.getValue() : "",
            transfer.end.cityName.getValue(),
            transfer.end.cityCode.getValue(),
            transfer.end.address);
      }
      
      tr.reservationFor.provider = new Organization();
      if (transfer.supplier != null && transfer.supplier.supplierName != null) {
        tr.reservationFor.provider.name = transfer.supplier.supplierName.getValue();
      }
      
      if (transfer.serviceTypeDescription != null && transfer.serviceTypeDescription.getValue() != null) {
        tr.serviceType = transfer.serviceTypeDescription.getValue();
      }
      
      if (transfer.accountDetails != null) {
        tr.cancellationPolicy = getCancellationPolicy(transfer.accountDetails);
      }

      StringBuilder sb = new StringBuilder();
      if (tr.subReservation != null && tr.subReservation.size() > 0) {
        sb.append("No of Passengers: ");
        sb.append(tr.subReservation.size());
        sb.append("\n");
      } else if (transfer.noOfItems != null && transfer.noOfItems.getLabel() != null && transfer.noOfItems.getLabel().toLowerCase() == "no. of passengers" && transfer.noOfItems.getValue() != null) {
        sb.append("No of Passengers: ");
        sb.append(transfer.noOfItems.getValue());
        sb.append("\n");
      }
      if (transfer.noOfdays != null && transfer.noOfdays.getValue() != null) {
        sb.append("\n");
        sb.append("Duration: ");
        sb.append(transfer.noOfdays.getValue());
        sb.append("\n");
      }
      if (transfer.itineraryNotes != null) {
        sb.append(getNotes(transfer.itineraryNotes));
      }
      tr.description = sb.toString();

      if (transfer.passengerDetails != null && transfer.passengerDetails.passenger != null && transfer.passengerDetails.passenger.size() > 0) {
        for (ItineraryItem.PassengerDetails.Passenger p : transfer.passengerDetails.passenger) {
          if (p.passengerName != null && p.passengerName.getValue() != null) {
            TransferReservation sub = new TransferReservation();
            String psngrName = p.getPassengerName().getValue();
            sub.underName = parsePassenger(psngrName);
            if (transfer.status != null) {
              sub.reservationStatus = transfer.status.getValue();
            }
            tr.addSubReservation(sub);
          }
        }

      } else if (transfer.passengerDetails != null && transfer.passengerDetails.getPassengerNameList() != null 
          && transfer.passengerDetails.getPassengerNameList().getValue() != null && transfer.passengerDetails.getPassengerNameList().getValue().length() > 1) {
        String[] nameList = transfer.passengerDetails.getPassengerNameList().getValue().split(",");
        for (String s : nameList) {
          TransferReservation sub = new TransferReservation();
          sub.underName = parsePassenger(s);
          if (transfer.status != null) {
            sub.reservationStatus = transfer.status.getValue();
          }
          tr.addSubReservation(sub);
        }
      } else {
        for (Person p : defaultUnderNames) {
          TransferReservation sub = new TransferReservation();
          sub.underName = p;
          sub.reservationStatus = "Confirmed";
          tr.addSubReservation(sub);
        }
      }

      String key = transfer.segmentTypeCode.getValue();
      if (transfer.start.cityCode != null && transfer.start.cityCode.getValue() != null) {
        key += "-" + transfer.start.cityCode.getValue();
      }
      if (transfer.sourceReloc != null && transfer.sourceReloc.getValue() != null) {
        key += "-" + transfer.sourceReloc.getValue();
      } else if (transfer.confirmationNo != null && transfer.confirmationNo.getValue() != null) {
        key += "-" + transfer.confirmationNo.getValue();
      }
      if (transfer.sourceReloc != null && costingMap.containsKey(transfer.sourceReloc.getValue())) {
        key = transfer.sourceReloc.getValue();
      }
      if (costingMap.containsKey(key)) {
        for (CostingItem cost : costingMap.get(key)) {
          if (cost.startCity.equals(transfer.start.cityCode.getValue())) {
            addPrice(cost, tr);
            break;
          }
        }
      }

      transferReservations.add(tr);

    }
    return transferReservations;
  }

  public List<RentalCarReservation> toCarReservations() {
    List<RentalCarReservation> carReservations = new ArrayList<>();
    for (ItineraryItem car : cars) {
      RentalCarReservation cr = new RentalCarReservation();
      if (car.itemNo != null) {
        cr.reservationId = car.itemNo.toString();
      }
      if (car.sourceReloc != null && car.sourceReloc.getValue() != null) {
        cr.reservationNumber = car.sourceReloc.getValue();
      } else if (car.confirmationNo != null && car.confirmationNo.getValue() != null) {
        cr.reservationNumber = car.confirmationNo.getValue();
      }

      if (car.status != null) {
        cr.reservationStatus = car.status.getValue();
      }
      String startDate = "";
      String endDate = "";
      
      if (car.start.serviceDate != null) {
        startDate = car.start.serviceDate.getValue();
        if (car.start.serviceTime != null && car.start.serviceTime.getValue() != null) {
          startDate += " " + car.start.serviceTime.getValue();
        } else {
          startDate += " 00:00";
        }
      }
      if (car.end.serviceDate != null) {
        endDate = car.end.serviceDate.getValue();
        if (car.end.serviceTime != null && car.end.serviceTime.getValue() != null) {
          endDate += " " + car.end.serviceTime.getValue();
        } else {
          endDate += " 00:00";
        }
      }

      cr.pickupTime = getDateTime(startDate);
      if (car.start != null && car.start.cityName != null && car.start.cityCode != null && car.start.address != null) {
        cr.pickupLocation = getAddress(car.start.locationDescription != null ? car.start.locationDescription.getValue() : "",
            car.start.cityName.getValue(),
            car.start.cityCode.getValue(),
            car.start.address);
      }

      cr.dropoffTime = getDateTime(endDate);

      if (car.end != null && car.end.cityName != null && car.end.cityCode != null && car.end.address != null) {
        cr.dropoffLocation = getAddress(car.end.locationDescription != null ? car.end.locationDescription.getValue() : "",
            car.end.cityName.getValue(),
            car.end.cityCode.getValue(),
            car.end.address);
      }

      cr.reservationFor = new RentalCar();
      if (car.serviceTypeDescription != null && car.serviceTypeDescription.getValue() != null) {
        cr.reservationFor.model = car.serviceTypeDescription.getValue();
      }
      cr.reservationFor.rentalCompany = new Organization();
      cr.reservationFor.rentalCompany.name = car.supplier.supplierName.getValue();

      if (car.passengerDetails != null && car.passengerDetails.passenger != null && car.passengerDetails.passenger.size() > 0) {
        for (ItineraryItem.PassengerDetails.Passenger p : car.passengerDetails.passenger) {
          if (p.passengerName != null && p.passengerName.getValue() != null) {
            RentalCarReservation sub = new RentalCarReservation();
            String psngrName = p.getPassengerName().getValue();
            sub.underName = parsePassenger(psngrName);
            if (car.status != null) {
              sub.reservationStatus = car.status.getValue();
            }
            cr.addSubReservation(sub);
          }
        }

      } else if (car.passengerDetails != null && car.passengerDetails.getPassengerNameList() != null 
          && car.passengerDetails.getPassengerNameList().getValue() != null && car.passengerDetails.getPassengerNameList().getValue().length() > 1) {
        String[] nameList = car.passengerDetails.getPassengerNameList().getValue().split(",");
        for (String s : nameList) {
          RentalCarReservation sub = new RentalCarReservation();
          sub.underName = parsePassenger(s);
          if (car.status != null) {
            sub.reservationStatus = car.status.getValue();
          }
          cr.addSubReservation(sub);
        }
      } else {
        //Car reservations typically needs only one underName
        RentalCarReservation sub = new RentalCarReservation();
        sub.underName = defaultUnderNames.get(0);
        sub.reservationStatus = "Confirmed";
        cr.addSubReservation(sub);
      }

      String key = "Car";
      if (car.start.cityCode != null && car.start.cityCode.getValue() != null) {
        key += "-" + car.start.cityCode.getValue();
      }

      if (car.sourceReloc != null && costingMap.containsKey(car.sourceReloc.getValue())) {
        key = car.sourceReloc.getValue();
      }
      if (costingMap.containsKey(key)) {
        for (CostingItem cost : costingMap.get(key)) {
          if (cost.startCity.equals(car.start.cityCode.getValue())) {
            addPrice(cost, cr);
            break;
          }
        }
      }

      StringBuilder sb = new StringBuilder();
      if (car.noOfItems != null && car.noOfItems.getValue() != null) {
        sb.append("No of Units: ");
        sb.append(car.noOfItems.getValue());
        sb.append("\n");
      }
      if (car.noOfdays != null && car.noOfdays.getValue() != null) {
        sb.append("\n");
        sb.append("Duration: ");
        sb.append(car.noOfdays.getValue());
        sb.append("\n");
      }
      if (car.itineraryNotes != null) {
        sb.append(getNotes(car.itineraryNotes));
      }
      cr.description = sb.toString();

      carReservations.add(cr);

    }
    return carReservations;
  }

  public List<TrainReservation> toRailReservations() {
    List<TrainReservation> trainReservations = new ArrayList<>();
    for (ItineraryItem rail : trains) {
      TrainReservation rr = new TrainReservation();
      if (rail.itemNo != null) {
        rr.reservationId = rail.itemNo.toString();
      }
      if (rail.sourceReloc != null && rail.sourceReloc.getValue() != null) {
        rr.reservationNumber = rail.sourceReloc.getValue();
      } else if (rail.confirmationNo != null && rail.confirmationNo.getValue() != null) {
        rr.reservationNumber = rail.confirmationNo.getValue();
      }

      if (rail.status != null) {
        rr.reservationStatus = rail.status.getValue();
      }
      rr.reservationFor = new TrainTrip();

      if (rail.supplier != null && rail.supplier.supplierName != null) {
        rr.name = rail.supplier.supplierName.getValue();
        rr.reservationFor.trainName = rail.supplier.supplierName.getValue();
      }
      if (rail.serviceIdentifier != null && rail.serviceIdentifier.getValue() != null) {
        rr.reservationFor.trainNumber = rail.serviceIdentifier.getValue();
      }

      String startDate = "";
      String endDate = "";

      if (rail.start.serviceDate != null) {
        startDate = rail.start.serviceDate.getValue();
        if (rail.start.serviceTime != null && rail.start.serviceTime.getValue() != null) {
          startDate += " " + rail.start.serviceTime.getValue();
        } else {
          startDate += " 00:00";
        }
      }
      if (rail.end.serviceDate != null) {
        endDate = rail.end.serviceDate.getValue();
        if (rail.end.serviceTime != null && rail.end.serviceTime.getValue() != null) {
          endDate += " " + rail.end.serviceTime.getValue();
        } else {
          endDate += " 00:00";
        }
      }
      rr.reservationFor.departTime = getDateTime(startDate);
      rr.reservationFor.arrivalTime = getDateTime(endDate);

      if (rail.start != null && rail.start.cityName != null && rail.start.cityCode != null && rail.start.address != null) {
        rr.reservationFor.departStation = getAddress(rail.start.locationDescription != null ? rail.start.locationDescription.getValue() : "",
            rail.start.cityName.getValue(),
            rail.start.cityCode.getValue(),
            rail.start.address);
      }

      if (rail.end != null && rail.end.cityName != null && rail.end.cityCode != null && rail.end.address != null) {
        rr.reservationFor.arrivalStation = getAddress(rail.end.locationDescription != null ? rail.end.locationDescription.getValue() : "",
            rail.end.cityName.getValue(),
            rail.end.cityCode.getValue(),
            rail.end.address);
      }

      if (rail.passengerDetails != null && rail.passengerDetails.passenger != null && rail.passengerDetails.passenger.size() > 0) {
        for (ItineraryItem.PassengerDetails.Passenger p : rail.passengerDetails.passenger) {
          if (p.passengerName != null && p.passengerName.getValue() != null) {
            TrainReservation sub = new TrainReservation();
            String psngrName = p.getPassengerName().getValue();
            sub.underName = parsePassenger(psngrName);
            if (rail.status != null) {
              sub.reservationStatus = rail.status.getValue();
            }
            rr.addSubReservation(sub);
          }
        }

      } else if (rail.passengerDetails != null && rail.passengerDetails.getPassengerNameList() != null 
          && rail.passengerDetails.getPassengerNameList().getValue() != null && rail.passengerDetails.getPassengerNameList().getValue().length() > 1) {
        String[] nameList = rail.passengerDetails.getPassengerNameList().getValue().split(",");
        for (String s : nameList) {
          TrainReservation sub = new TrainReservation();
          sub.underName = parsePassenger(s);
          if (rail.status != null) {
            sub.reservationStatus = rail.status.getValue();
          }
          rr.addSubReservation(sub);
        }
      } else {
        for (Person p : defaultUnderNames) {
          TrainReservation sub = new TrainReservation();
          sub.underName = p;
          sub.reservationStatus = "Confirmed";
          rr.addSubReservation(sub);
        }
      }

      String key = rail.segmentTypeCode.getValue();
      if (rail.start.cityCode != null && rail.start.cityCode.getValue() != null) {
        key += "-" + rail.start.cityCode.getValue();
      }
      if (rail.sourceReloc != null && rail.sourceReloc.getValue() != null) {
        key += "-" + rail.sourceReloc.getValue();
      } else if (rail.confirmationNo != null && rail.confirmationNo.getValue() != null) {
        key += "-" + rail.confirmationNo.getValue();
      }
      if (rail.sourceReloc != null && costingMap.containsKey(rail.sourceReloc.getValue())) {
        key = rail.sourceReloc.getValue();
      }
      if (costingMap.containsKey(key)) {
        for (CostingItem cost : costingMap.get(key)) {
          if (cost.startCity.equals(rail.start.cityCode.getValue())) {
            addPrice(cost, rr);
            break;
          }
        }
      }

      StringBuilder sb = new StringBuilder();
      if (rr.subReservation != null && rr.subReservation.size() > 0) {
        sb.append("No of Passengers: ");
        sb.append(rr.subReservation.size());
        sb.append("\n");
      } else if (rail.noOfItems != null && rail.noOfItems.getLabel() != null && rail.noOfItems.getLabel().toLowerCase() == "no. of passengers" && rail.noOfItems.getValue() != null) {
        sb.append("No of Passengers: ");
        sb.append(rail.noOfItems.getValue());
        sb.append("\n");
      }
      if (rail.noOfdays != null && rail.noOfdays.getValue() != null) {
        sb.append("\n");
        sb.append("Duration: ");
        sb.append(rail.noOfdays.getValue());
        sb.append("\n");
      }
      if (rail.itineraryNotes != null) {
        sb.append(getNotes(rail.itineraryNotes));
      }
      rr.description = sb.toString();

      trainReservations.add(rr);
    }
    return trainReservations;
  }

  public List<BusReservation> toBusReservation() {
    List<BusReservation> busReservations = new ArrayList<>();
    for (ItineraryItem bus : buses) {
      BusReservation br = new BusReservation();

      if (bus.itemNo != null) {
        br.reservationId = bus.itemNo.toString();
      }
      if (bus.status != null) {
        br.reservationStatus = bus.status.getValue();
      }
      if (bus.sourceReloc != null && bus.sourceReloc.getValue() != null) {
        br.reservationNumber = bus.sourceReloc.getValue();
      } else if (bus.confirmationNo != null && bus.confirmationNo.getValue() != null) {
        br.reservationNumber = bus.confirmationNo.getValue();
      }

      String startDate = "";
      String endDate = "";

      if (bus.start.serviceDate != null) {
        startDate = bus.start.serviceDate.getValue();
        if (bus.start.serviceTime != null && bus.start.serviceTime.getValue() != null) {
          startDate += " " + bus.start.serviceTime.getValue();
        } else {
          startDate += " 00:00";
        }
      }
      if (bus.end.serviceDate != null) {
        endDate = bus.end.serviceDate.getValue();
        if (bus.end.serviceTime != null && bus.end.serviceTime.getValue() != null) {
          endDate += " " + bus.end.serviceTime.getValue();
        } else {
          endDate += " 00:00";
        }
      }

      br.reservationFor = new BusTrip();
      br.reservationFor.provider = new Organization();
      if (bus.supplier != null && bus.supplier.supplierName != null) {
        br.name = bus.supplier.supplierName.getValue();
        br.reservationFor.provider.name = bus.supplier.supplierName.getValue();
      }
      if (bus.supplier.supplierAddress.address != null) {
        br.reservationFor.provider.address = new PostalAddress();
        if (bus.supplier.supplierAddress.address.getAddress1() != null) {
          br.reservationFor.provider.address.streetAddress = bus.supplier.supplierAddress.address.getAddress1().getValue();
        }
        if (bus.supplier.supplierAddress.address.getAddress2() != null) {
          br.reservationFor.provider.address.streetAddress += bus.supplier.supplierAddress.address.getAddress2().getValue();
        }
        if (bus.supplier.supplierAddress.address.getAddress3() != null) {
          br.reservationFor.provider.address.streetAddress += bus.supplier.supplierAddress.address.getAddress3().getValue();
        }
        if (bus.supplier.supplierAddress.address.state != null) {
          br.reservationFor.provider.address.addressRegion = bus.supplier.supplierAddress.address.state.getValue();
        }
        if (bus.supplier.supplierAddress.address.postCode != null) {
          br.reservationFor.provider.address.postalCode = bus.supplier.supplierAddress.address.getPostCode().getValue();
        }
        if (bus.supplier.supplierAddress.address.country != null) {
          br.reservationFor.provider.address.addressCountry = bus.supplier.supplierAddress.address.country.getValue();
        }
      }
      
      br.reservationFor.departureTime = getDateTime(startDate);
      br.reservationFor.departureBusStop = new BusStop();
      if (bus.start != null && bus.start.cityName != null && bus.start.cityCode != null && bus.start.address != null) {
        br.reservationFor.departureBusStop.address = getAddress(bus.start.locationDescription != null ? bus.start.locationDescription.getValue() : "",
            bus.start.cityName.getValue(),
            bus.start.cityCode.getValue(),
            bus.start.address).address;
      }
      
      br.reservationFor.arrivalTime = getDateTime(endDate);
      br.reservationFor.arrivalBusStop = new BusStop();
      if (bus.end != null && bus.end.cityName != null && bus.end.cityCode != null && bus.end.address != null) {
        br.reservationFor.arrivalBusStop.address = getAddress(bus.end.locationDescription != null ? bus.end.locationDescription.getValue() : "",
            bus.end.cityName.getValue(),
            bus.end.cityCode.getValue(),
            bus.end.address).address;
      }

      if (bus.supplier.supplierType != null) {
        br.serviceType = bus.supplier.supplierType.getValue();
      }
      if (bus.accountDetails != null) {
        br.cancellationPolicy = getCancellationPolicy(bus.accountDetails);
      }


      if (bus.passengerDetails != null && bus.passengerDetails.passenger != null && bus.passengerDetails.passenger.size() > 0) {
        for (ItineraryItem.PassengerDetails.Passenger p : bus.passengerDetails.passenger) {
          if (p.passengerName != null && p.passengerName.getValue() != null) {
            BusReservation sub = new BusReservation();
            String psngrName = p.getPassengerName().getValue();
            sub.underName = parsePassenger(psngrName);
            if (bus.status != null) {
              sub.reservationStatus = bus.status.getValue();
            }
            br.addSubReservation(sub);
          }
        }

      } else if (bus.passengerDetails != null && bus.passengerDetails.getPassengerNameList() != null 
          && bus.passengerDetails.getPassengerNameList().getValue() != null && bus.passengerDetails.getPassengerNameList().getValue().length() > 1) {
        String[] nameList = bus.passengerDetails.getPassengerNameList().getValue().split(",");
        for (String s : nameList) {
          BusReservation sub = new BusReservation();
          sub.underName = parsePassenger(s);
          if (bus.status != null) {
            sub.reservationStatus = bus.status.getValue();
          }
          br.addSubReservation(sub);
        }
      } else {
        for (Person p : defaultUnderNames) {
          BusReservation sub = new BusReservation();
          sub.underName = p;
          sub.reservationStatus = "Confirmed";
          br.addSubReservation(sub);
        }
      }

      String key = bus.segmentTypeCode.getValue();
      if (bus.start.cityCode != null && bus.start.cityCode.getValue() != null) {
        key += "-" + bus.start.cityCode.getValue();
      }
      if (bus.sourceReloc != null && bus.sourceReloc.getValue() != null) {
        key += "-" + bus.sourceReloc.getValue();
      } else if (bus.confirmationNo != null && bus.confirmationNo.getValue() != null) {
        key += "-" + bus.confirmationNo.getValue();
      }

      if (bus.sourceReloc != null && costingMap.containsKey(bus.sourceReloc.getValue())) {
        key = bus.sourceReloc.getValue();
      }
      if (costingMap.containsKey(key)) {
        for (CostingItem cost : costingMap.get(key)) {
          if (cost.startCity.equals(bus.start.cityCode.getValue())) {
            addPrice(cost, br);
            break;
          }
        }
      }

      StringBuilder sb = new StringBuilder();
      if (br.subReservation != null && br.subReservation.size() > 0) {
        sb.append("No of Passengers: ");
        sb.append(br.subReservation.size());
        sb.append("\n");
      } else if (bus.noOfItems != null && bus.noOfItems.getLabel() != null && bus.noOfItems.getLabel().toLowerCase() == "no. of passengers" && bus.noOfItems.getValue() != null) {
        sb.append("No of Passengers: ");
        sb.append(bus.noOfItems.getValue());
        sb.append("\n");
      }
      if (bus.noOfdays != null && bus.noOfdays.getValue() != null) {
        sb.append("\n");
        sb.append("Duration: ");
        sb.append(bus.noOfdays.getValue());
        sb.append("\n");
      }
      if (bus.itineraryNotes != null) {
        sb.append(getNotes(bus.itineraryNotes));
      }
      br.description = sb.toString();

      busReservations.add(br);
    }
    return busReservations;
  }
  

  public List<ActivityReservation> toActivityReservations() {
    List<ActivityReservation> activityReservations = new ArrayList<>();
    for (ItineraryItem act : activities) {
      ActivityReservation ar = new ActivityReservation();

      if (act.itemNo != null) {
        ar.reservationId = act.itemNo.toString();
      }
      if (act.status != null) {
        ar.reservationStatus = act.status.getValue();
      }
      if (act.sourceReloc != null && act.sourceReloc.getValue() != null) {
        ar.reservationNumber = act.sourceReloc.getValue();
      } else if (act.confirmationNo != null  && act.confirmationNo.getValue() != null) {
        ar.reservationNumber = act.confirmationNo.getValue();
      }
      if (act.supplier != null && act.supplier.supplierName != null) {
        ar.name = act.supplier.supplierName.getValue();
      }

      String startDate = "";
      String endDate = "";

      if (act.start.serviceDate != null) {
        startDate = act.start.serviceDate.getValue();
        if (act.start.serviceTime != null && act.start.serviceTime.getValue() != null) {
          startDate += " " + act.start.serviceTime.getValue();
        } else {
          startDate += " 00:00";
        }
      }
      if (act.end.serviceDate != null) {
        endDate = act.end.serviceDate.getValue();
        if (act.end.serviceTime != null && act.end.serviceTime.getValue() != null) {
          endDate += " " + act.end.serviceTime.getValue();
        } else {
          endDate += " 00:00";
        }
      }
      ar.startTime = getDateTime(startDate);
      ar.finishTime = getDateTime(endDate);

      if (act.start != null && act.start.cityName != null && act.start.cityCode != null && act.start.address != null) {
        ar.startLocation = getAddress(act.start.locationDescription != null ? act.start.locationDescription.getValue() : "",
            act.start.cityName.getValue(),
            act.start.cityCode.getValue(),
            act.start.address);
      }

      if (act.end != null && act.end.cityName != null && act.end.cityCode != null && act.end.address != null) {
        ar.finishLocation = getAddress(act.end.locationDescription != null ? act.end.locationDescription.getValue() : "",
            act.end.cityName.getValue(),
            act.end.cityCode.getValue(),
            act.end.address);
      }

      if (act.passengerDetails != null && act.passengerDetails.passenger != null && act.passengerDetails.passenger.size() > 0) {
        for (ItineraryItem.PassengerDetails.Passenger p : act.passengerDetails.passenger) {
          if (p.passengerName != null && p.passengerName.getValue() != null) {
            ActivityReservation sub = new ActivityReservation();
            String psngrName = p.getPassengerName().getValue();
            sub.underName = parsePassenger(psngrName);
            if (act.status != null) {
              sub.reservationStatus = act.status.getValue();
            }
            ar.addSubReservation(sub);
          }
        }

      } else if (act.passengerDetails != null && act.passengerDetails.getPassengerNameList() != null 
          && act.passengerDetails.getPassengerNameList().getValue() != null && act.passengerDetails.getPassengerNameList().getValue().length() > 1) {
        String[] nameList = act.passengerDetails.getPassengerNameList().getValue().split(",");
        for (String s : nameList) {
          ActivityReservation sub = new ActivityReservation();
          sub.underName = parsePassenger(s);
          if (act.status != null) {
            sub.reservationStatus = act.status.getValue();
          }
          ar.addSubReservation(sub);

        }
      } else {
        for (Person p : defaultUnderNames) {
          ActivityReservation sub = new ActivityReservation();
          sub.underName = p;
          sub.reservationStatus = "Confirmed";
          ar.addSubReservation(sub);
        }
      }

      String key = act.segmentTypeCode.getValue();
      if (act.start.cityCode != null && act.start.cityCode.getValue() != null) {
        key += "-" + act.start.cityCode.getValue();
      }
      if (act.sourceReloc != null && act.sourceReloc.getValue() != null) {
        key += "-" + act.sourceReloc.getValue();
      } else if (act.confirmationNo != null && act.confirmationNo.getValue() != null) {
        key += "-" + act.confirmationNo.getValue();
      }
      if (act.sourceReloc != null && costingMap.containsKey(act.sourceReloc.getValue())) {
        key = act.sourceReloc.getValue();
      }
      if (costingMap.containsKey(key)) {
        for (CostingItem cost : costingMap.get(key)) {
          if (cost.startCity.equals(act.start.cityCode.getValue())) {
            addPrice(cost, ar);
            break;
          }
        }
      }

      StringBuilder sb = new StringBuilder();
      if (act.itineraryNotes != null) {
        sb.append(getNotes(act.itineraryNotes));
      }
      ar.description = sb.toString();

      activityReservations.add(ar);
    }
    return activityReservations;
  }
  

  public void addPrice (CostingItem cost, Reservation reservation) {
    if (currency != null) {
      reservation.currency = currency;
    }
    if (cost.costingFinancials.baseFare != null && cost.costingFinancials.baseFare.getValue() != null) {
      reservation.price = cost.costingFinancials.baseFare.getValue();
    }
    if (cost.costingFinancials.clientDue != null && cost.costingFinancials.clientDue.getValue() != null) {
      reservation.totalPrice = cost.costingFinancials.clientDue.getValue();
    }
    double taxExcGst = 0.0;
    double gst = 0.0;
    double tax = 0.0;
    if (cost.costingFinancials.taxesAndFeesExGST != null && cost.costingFinancials.taxesAndFeesExGST.getValue() != null) {
      taxExcGst = Double.valueOf(cost.costingFinancials.taxesAndFeesExGST.getValue().trim());
    }
    if (cost.costingFinancials.gst != null && cost.costingFinancials.gst.getValue() != null) {
      gst = Double.valueOf(cost.costingFinancials.gst.getValue().trim());
    }
    tax = taxExcGst + gst;
    reservation.taxes = String.valueOf(tax);

  }

  public String buildInvoice() {
    if (documentTotals != null) {
      StringBuilder sb = new StringBuilder();
      if (documentTotals.totalPayDirect != null && documentTotals.totalPayDirect.total != null) {
        String baseFare = "0.00";
        String discount = "0.00";
        String taxesAndFeesExcGST = "0.00";
        String gst = "0.00";
        String clientDue = "0.00";

        if (documentTotals.totalPayDirect.total.baseFare != null && documentTotals.totalPayDirect.total.baseFare.getValue() != null) {
          baseFare = documentTotals.totalPayDirect.total.baseFare.getValue();
        }
        if (documentTotals.totalPayDirect.total.discount != null && documentTotals.totalPayDirect.total.discount.getValue() != null) {
          discount = documentTotals.totalPayDirect.total.discount.getValue();
        }
        if (documentTotals.totalPayDirect.total.taxesAndFeesExcGST != null && documentTotals.totalPayDirect.total.taxesAndFeesExcGST.getValue() != null) {
          taxesAndFeesExcGST = documentTotals.totalPayDirect.total.taxesAndFeesExcGST.getValue();
        }
        if (documentTotals.totalPayDirect.total.gst != null && documentTotals.totalPayDirect.total.gst.getValue() != null) {
          gst = documentTotals.totalPayDirect.total.gst.getValue();
        }
        if (documentTotals.totalPayDirect.total.clientDue != null && documentTotals.totalPayDirect.total.clientDue.getValue() != null) {
          clientDue = documentTotals.totalPayDirect.total.clientDue.getValue();
        }
        sb.append("--- Total (Pay-Direct) ---");
        sb.append("\n");
        sb.append(" Base Fare: ");
        sb.append(baseFare);
        sb.append("\n");
        sb.append(" Discount: ");
        sb.append(discount);
        sb.append("\n");
        sb.append(" Taxes & Fees Excl. GST: ");
        sb.append(taxesAndFeesExcGST);
        sb.append("\n");
        sb.append(" GST: ");
        sb.append(gst);
        sb.append("\n");
        sb.append(" Amount Due: ");
        sb.append(clientDue);
        sb.append("\n\n");
      }
      //TotalExclPayDirect
      if (documentTotals.totalExcPayDirect != null && documentTotals.totalExcPayDirect.total != null) {
        String baseFare = "0.00";
        String discount = "0.00";
        String taxesAndFeesExcGST = "0.00";
        String gst = "0.00";
        String clientDue = "0.00";

        if (documentTotals.totalExcPayDirect.total.baseFare != null && documentTotals.totalExcPayDirect.total.baseFare.getValue() != null) {
          baseFare = documentTotals.totalExcPayDirect.total.baseFare.getValue();
        }
        if (documentTotals.totalExcPayDirect.total.discount != null && documentTotals.totalExcPayDirect.total.discount.getValue() != null) {
          discount = documentTotals.totalExcPayDirect.total.discount.getValue();
        }
        if (documentTotals.totalExcPayDirect.total.taxesAndFeesExcGST != null && documentTotals.totalExcPayDirect.total.taxesAndFeesExcGST.getValue() != null) {
          taxesAndFeesExcGST = documentTotals.totalExcPayDirect.total.taxesAndFeesExcGST.getValue();
        }
        if (documentTotals.totalExcPayDirect.total.gst != null && documentTotals.totalExcPayDirect.total.gst.getValue() != null) {
          gst = documentTotals.totalExcPayDirect.total.gst.getValue();
        }
        if (documentTotals.totalExcPayDirect.total.clientDue != null && documentTotals.totalExcPayDirect.total.clientDue.getValue() != null) {
          clientDue = documentTotals.totalExcPayDirect.total.clientDue.getValue();
        }
        sb.append("--- Total (Excl. Pay-Direct) ---");
        sb.append("\n");
        sb.append(" Base Fare: ");
        sb.append(baseFare);
        sb.append("\n");
        sb.append(" Discount: ");
        sb.append(discount);
        sb.append("\n");
        sb.append(" Taxes & Fees Excl. GST: ");
        sb.append(taxesAndFeesExcGST);
        sb.append("\n");
        sb.append(" GST: ");
        sb.append(gst);
        sb.append("\n");
        sb.append(" Amount Due: ");
        sb.append(clientDue);
        sb.append("\n\n");
      }
      //TotalInclPayDirect
      if (documentTotals.totalIncPayDirect != null && documentTotals.totalIncPayDirect.total != null) {
        String baseFare = "0.00";
        String discount = "0.00";
        String taxesAndFeesExcGST = "0.00";
        String gst = "0.00";
        String clientDue = "0.00";

        if (documentTotals.totalIncPayDirect.total.baseFare != null && documentTotals.totalIncPayDirect.total.baseFare.getValue() != null) {
          baseFare = documentTotals.totalIncPayDirect.total.baseFare.getValue();
        }
        if (documentTotals.totalIncPayDirect.total.discount != null && documentTotals.totalIncPayDirect.total.discount.getValue() != null) {
          discount = documentTotals.totalIncPayDirect.total.discount.getValue();
        }
        if (documentTotals.totalIncPayDirect.total.taxesAndFeesExcGST != null && documentTotals.totalIncPayDirect.total.taxesAndFeesExcGST.getValue() != null) {
          taxesAndFeesExcGST = documentTotals.totalIncPayDirect.total.taxesAndFeesExcGST.getValue();
        }
        if (documentTotals.totalIncPayDirect.total.gst != null && documentTotals.totalIncPayDirect.total.gst.getValue() != null) {
          gst = documentTotals.totalIncPayDirect.total.gst.getValue();
        }
        if (documentTotals.totalIncPayDirect.total.clientDue != null && documentTotals.totalIncPayDirect.total.clientDue.getValue() != null) {
          clientDue = documentTotals.totalIncPayDirect.total.clientDue.getValue();
        }
        sb.append("--- Total (Incl. Pay-Direct) ---");
        sb.append("\n");
        sb.append(" Base Fare: ");
        sb.append(baseFare);
        sb.append("\n");
        sb.append(" Discount: ");
        sb.append(discount);
        sb.append("\n");
        sb.append(" Taxes & Fees Excl. GST: ");
        sb.append(taxesAndFeesExcGST);
        sb.append("\n");
        sb.append(" GST: ");
        sb.append(gst);
        sb.append("\n");
        sb.append(" Amount Due: ");
        sb.append(clientDue);
        sb.append("\n\n");
      }

      if (documentTotals.totalPaid != null || documentTotals.totalOutstandingExcPayDirect != null
          || documentTotals.totalOutstandingIncPayDirect != null) {
        sb.append("----------------------------------------------------------------------\n");
        if (documentTotals.totalPaid != null && documentTotals.totalPaid.getValue() != null) {
          String total = documentTotals.totalPaid.getValue();
          sb.append("Total Paid : ");
          sb.append(total);
          sb.append("\n");
        }
        if (documentTotals.totalOutstandingExcPayDirect != null && documentTotals.totalOutstandingExcPayDirect.getValue() != null) {
          String total = documentTotals.totalOutstandingExcPayDirect.getValue();
          sb.append("Total Outstanding (Excl. Pay-Direct) : ");
          sb.append(total);
          sb.append("\n");
        }
        if (documentTotals.totalOutstandingIncPayDirect != null && documentTotals.totalOutstandingIncPayDirect.getValue() != null) {
          String total = documentTotals.totalOutstandingIncPayDirect.getValue();
          sb.append("Total Outstanding (Incl. Pay-Direct) : ");
          sb.append(total);
          sb.append("\n");
        }
        sb.append("=========================================");
      }

      return sb.toString();
    }
    return null;
  }


  public String getNotes(ItineraryItem.ItineraryNotes itineraryNotes) {
    StringBuilder sb = new StringBuilder();
    if (itineraryNotes.segmentNotes != null && itineraryNotes.segmentNotes.getValue() != null) {
      sb.append("\n");
      sb.append("Notes: ");
      sb.append("\n");
      sb.append(itineraryNotes.segmentNotes.getValue());
      sb.append("\n\n");
    }
    return sb.toString();
  }

  public Place getAddress(String location, String cityName, String cityCode, Address address) {
    Place place = new Place();
    place.name = "";
    if (location != null) {
      place.name = location;
    } else if (place.name.isEmpty() && cityCode != null) {
      place.name = cityCode;
    } else if (place.name.isEmpty() && cityCode == null && cityName != null) {
      place.name = cityName;
    } 
    
    place.address = new PostalAddress();
    if (address != null) {
      StringBuilder sb = new StringBuilder();
      if (address.getAddress1() != null && address.address1.getValue() != null) {
        sb.append(address.getAddress1().getValue());
      }
      if (address.getAddress2() != null && address.address2.getValue() != null) {
        sb.append(", ");
        sb.append(address.getAddress2().getValue());
      }
      if (address.getAddress3() != null && address.address3.getValue() != null) {
        sb.append(", ");
        sb.append(address.getAddress3().getValue());
      }
      place.address.streetAddress = sb.toString();
      if (address.state != null && address.state.getValue() != null) {
        place.address.addressRegion = address.state.getValue();
      }
      if (place.address.addressRegion == null && cityName != null && cityName.contains(",")) {
        place.address.addressRegion = cityName.substring(0, cityName.indexOf(',')).trim();
      }
      if (address.postCode != null && address.postCode.getValue() != null) {
        place.address.postalCode = address.getPostCode().getValue();
      }
      if (address.country != null && address.country.getValue() != null) {
        place.address.addressCountry = address.country.getValue();
      }
      if (place.address.addressCountry == null && cityName != null && cityName.contains(",")) {
        place.address.addressCountry = cityName.substring(cityName.indexOf(',') + 1).trim();
      }
    }

    return place;
  }

  public String getCancellationPolicy(ItineraryItem.AccountDetails accountDetails) {
    String policy = null;
    if (accountDetails.cancelConditions != null && accountDetails.cancelConditions.getValue() != null) {
      policy = accountDetails.cancelConditions.getValue();
      if (accountDetails.cancellationDescription != null && accountDetails.cancellationDescription.getValue() != null) {
        policy += "\n\n";
      }
    }
    if (accountDetails.cancellationDescription != null && accountDetails.cancellationDescription.getValue() != null) {
      policy += accountDetails.cancellationDescription.getValue();
    }
    return policy;
  }

  public Person parsePassenger(String passenger) {
    Person person = new Person();
    String[] nameList = passenger.split(",");
    String[] nameSplit;
    for (String s : nameList) {
      String name = s.trim();
      if (!name.contains("/") && name.contains(" ")) {
        nameSplit = name.split(" ");
        person.givenName = name;
        if (nameSplit.length == 2) {
          person.givenName = nameSplit[0].trim();
          person.familyName = nameSplit[1].trim();
        } else {
          person.givenName = nameSplit[1].trim();
          person.familyName = nameSplit[2].trim();
        }

      } else if (name.contains("/")) {
        nameSplit = name.split("/");
        int index = nameSplit[1].length();
        Matcher m = salutationPattern.matcher(nameSplit[1]);
        if (m.find()) {
          index = m.start();
        }
        person.familyName = nameSplit[0].trim();
        person.givenName = nameSplit[1].substring(0, index).trim();
      } else {
        person.givenName = passenger;
      }
    }
    return person;
  }

  public DateTime getDateTime(String datetimestamp) {
    if (datetimestamp != null) {

      SimpleDateFormat dateFormatTramada = new SimpleDateFormat("EEE dd MMM yy HH:mm");
      String[] dateFormatsTramada = {"EEE dd MMM yy HH:mm", "EEE dd MMM yy hh:mm a", "EEE MMM dd yy HH:mm", "EEE MMM dd yy hh:mm a"};
      SimpleDateFormat dateFormatUmapped = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
      Date d1 = null;

      try {
        d1 = DateUtils.parseDate(datetimestamp, dateFormatsTramada);
      } catch (ParseException pe) {
        Log.log(LogLevel.ERROR, "Failed to parse date ISO8601 timestamp:" + datetimestamp);
      }

      String timestamp = dateFormatUmapped.format(d1);


      DateTime dateTime = null;
      dateTime = new DateTime(timestamp);

      long t = 0;
      if (dateTime != null) {
        t = dateTime.getTimestamp().toInstant().toEpochMilli();
      }
      if (startTimestamp == 0 || t < startTimestamp) {
        startTimestamp = t;
      }

      if (endTimestamp == 0 || t > endTimestamp) {
        endTimestamp = t;
      }
      return dateTime;

    }
    return null;
  }
}

