
package com.mapped.publisher.parse.globus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfAvailableMediaTour complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ArrayOfAvailableMediaTour">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AvailableMediaTour" type="{http://webapi.globusandcosmos.com/AvailableTours.xsd}AvailableMediaTour" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfAvailableMediaTour", namespace = "http://webapi.globusandcosmos.com/AvailableTours.xsd", propOrder = {
    "availableMediaTours"
})
public class ArrayOfAvailableMediaTour {

    @XmlElement(name = "AvailableMediaTour", nillable = true)
    protected ArrayList<AvailableMediaTour> availableMediaTours;

    /**
     * Gets the value of the availableMediaTours property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the availableMediaTours property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAvailableMediaTours().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AvailableMediaTour }
     *
     *
     */
    public ArrayList<AvailableMediaTour> getAvailableMediaTours() {
        if (availableMediaTours == null) {
            availableMediaTours = new ArrayList<AvailableMediaTour>();
        }
        return this.availableMediaTours;
    }

}
