package com.mapped.publisher.view;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-14
 * Time: 2:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class TripPassengerInfoView
    extends BaseView {
  public String tripId;
  public String tripName;
  public String tripStartDate;
  public String tripEndDate;
  public int    tripStatus;

  public String  id;
  public String  name;
  public String  email;
  public String  msisdn;
  public int     numberOfBookings;
  public boolean isUmappedUser;

  public WebItineraryStatsView webItineraryStats;

  public List<TripPassengerView> passengers;
}
