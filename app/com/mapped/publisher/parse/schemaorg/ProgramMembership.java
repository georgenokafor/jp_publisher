package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Used to describe membership in a loyalty programs (e.g. "StarAliance"),
 * traveler clubs (e.g. "AAA"), purchase clubs ("Safeway Club"), etc.
 * https://schema.org/ProgramMembership
 *
 * Created by surge on 2015-05-11.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ProgramMembership
    extends Thing {
  public Organization hostingOrganization;

  /**
   * A member of an Organization or a ProgramMembership. Organizations can be members of organizations;
   * ProgramMembership is typically for individuals. Supersedes members, musicGroupMember.
   * Inverse property: memberOf.
   */
  public Person member;

  public Person members;

  /**
   * A unique identifier for the membership.
   */
  public String membershipNumber;

  /**
   * The program providing the membership.
   */
  public String programName;
  public String program;
}
