package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2015-05-12.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LodgingReservation
    extends Reservation {

  /**
   * [DateTime] Schema Version:	The earliest someone may check into a lodging establishment.
   */
  public DateTime checkinTime;
  /**
   * [DateTime] Google Version: The earliest someone may check into a lodging establishment.
   */
  public DateTime checkinDate;

  /**
   * [DateTime] Schema Version:	The latest someone may check out of a lodging establishment.
   */
  public DateTime checkoutTime;

  /**
   * [DateTime] Schema Version:	The latest someone may check out of a lodging establishment.
   */
  public DateTime checkoutDate;

  /**
   * [Text] A full description of the lodging unit.
   */
  public String lodgingUnitDescription;

  /**
   * [Text] Total price of the LodgingReservation.
   */
  public String  price;
  /**
   * [Text] The currency (in 3-letter ISO 4217 format) of the LodgingReservation's price.
   */
  public String  priceCurrency;
  /**
   * [Number] Number of adults who will be staying in the lodging unit.
   */
  public Integer numAdults;
  /**
   * [Number]	Number of children who will be staying in the lodging unit.
   */
  public Integer numChildren;

  /**
   * [Text] Textual description of the unit type (including suite vs. room, size of bed, etc.).
   */
  public String lodgingUnitType;

  /**
   * Below are new fields relating to accommodation travelers 16/05/2017
   */
  public String bedding;
  public String amenities;
  public String confirmStatus;
  public String membershipId;

  public List<RoomType> rooms;

  /**
   * (required) The lodging the reservation is at.
   */
  public LodgingBusiness reservationFor;

  @Override
  public DateTime getStartDateTime() {
    return (checkinTime != null) ? checkinTime : checkinDate;
  }

  @Override
  public DateTime getFinishDateTime() {
    return (checkoutTime != null) ? checkoutTime : checkoutDate;
  }

  public void addRoom(RoomType r) {
    if (this.rooms == null) {
      this.rooms = new ArrayList<>();
    }
    rooms.add(r);
  }

}
