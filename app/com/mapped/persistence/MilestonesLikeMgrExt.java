package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2012-11-22
 * Time: 9:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class MilestonesLikeMgrExt extends MilestonesLikeMgr {
    private static final String SELECT_ALL_BY_MAP_OWNER_LASTUPDATE = "Select ml.Pk, ml.MilestonesId, ml.UsersId, ml.Notes, ml.ModifiedBy, ml.CreatedBy, ml.Status, ml.SyncTimestamp, ml.LastUpdateTimestamp, ml.CreateTimestamp from MilestonesLike ml , Milestones mi, Maps m where m.createdby = ? and m.pk = mi.mapsid and mi.pk = ml.MilestonesId and (ml.SyncTimestamp = 0 or ml.SyncTimestamp > ?);";
    private static final String SELECT_SHARED_BY_USER_LASTUPDATE = "Select ml.Pk, ml.MilestonesId, ml.UsersId, ml.Notes, ml.ModifiedBy, ml.CreatedBy, ml.Status, ml.SyncTimestamp, ml.LastUpdateTimestamp, ml.CreateTimestamp from MilestonesLike ml , Milestones mi, Maps m where m.pk in (select mapsid from sharedmaps where shareuserid = ? and status = 0) and m.pk = mi.mapsid and mi.pk = ml.MilestonesId and (ml.SyncTimestamp = 0 or ml.SyncTimestamp > ?);";
    private static final String SELECT_NEWSHARED_BY_USER_LASTUPDATE = "Select ml.Pk, ml.MilestonesId, ml.UsersId, ml.Notes, ml.ModifiedBy, ml.CreatedBy, ml.Status, ml.SyncTimestamp, ml.LastUpdateTimestamp, ml.CreateTimestamp from MilestonesLike ml , Milestones mi, Maps m where m.pk in (select mapsid from sharedmaps where shareuserid = ? and status = 0 and (SyncTimestamp = 0 or SyncTimestamp > ?) and createtimestamp = lastupdatetimestamp) and m.pk = mi.mapsid and mi.pk = ml.MilestonesId;";

    public static ArrayList<MilestonesLike> getAllByMapOwnerSinceLastUpdate(String userId, long lastUpdateTimestamp) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            prep = conn.prepareStatement(SELECT_ALL_BY_MAP_OWNER_LASTUPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdateTimestamp);
            rs = prep.executeQuery();
            return MilestonesLikeMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <MilestonesLike> getSharedByUserSinceLastUpdate(String userId, long lastUpdateTimestamp) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            prep = conn.prepareStatement(SELECT_SHARED_BY_USER_LASTUPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdateTimestamp);
            rs = prep.executeQuery();
            return MilestonesLikeMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <MilestonesLike> getNewSharedByUserSinceLastUpdate(String userId, long lastUpdateTimestamp) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            prep = conn.prepareStatement(SELECT_NEWSHARED_BY_USER_LASTUPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdateTimestamp);
            rs = prep.executeQuery();
            return MilestonesLikeMgr.handleResults(rs);
        } finally {
            if (prep != null)
                prep.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

}
