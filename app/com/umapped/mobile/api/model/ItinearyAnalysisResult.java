package com.umapped.mobile.api.model;

import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wei on 2017-09-13.
 */
public class ItinearyAnalysisResult implements Serializable {
  private List<TripSegmentData> tripSegments;

  public ItinearyAnalysisResult() {
  }

  public ItinearyAnalysisResult(List<TripSegmentData> tripSegments) {
    this.tripSegments = tripSegments;
  }

  public List<TripSegmentData> getTripSegments() {
    return tripSegments;
  }

  public void setTripSegments(List<TripSegmentData> tripSegments) {
    this.tripSegments = tripSegments;
  }
}
