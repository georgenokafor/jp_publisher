package com.umapped.persistence.reservation.flight;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.umapped.persistence.reservation.UmReservation;

/**
 * From PUB-922:
 *
 * Common:
 * Taxes & Fees (per reservation)
 * Total (per reservation)
 * Currency (per reservation)
 * Status (confirmed or unconfirmed) (per reservation)
 * Cancellation policy (per reservation)
 * <p>
 * Flight Level:
 * Aircraft (in flight object)
 * Terminal (in flight object)
 * Airport Advisories
 * Length of Flight (in flight object as flightDistance and
 * estimatedFlightDuration)
 * Operated by (in flight object)
 * <p>
 * Passenger Level:
 * Baggage Information (baggage allowance, 1st Bag fees+size, 2nd bag fees+size)
 * [per Umapped UmTravelerFlight]
 * Cary-on Baggage Allowance [per Umapped UmTravelerFlight]
 * Fare Type [per Umapped UmTravelerFlight]
 * Fare [per Umapped UmTravelerFlight]
 * Passengers (as part of UmTravelerFlight)
 * Extras [per Umapped UmTraveler]
 * Included (breakfast, wifi) [as part of UmTraveler]
 * Upgrades Available [as part of UmTraveler]
 * Special requests [as part of UmTraveler]
 * Global Entry Number (Marcy Kalish) [as part of UmTraveler as
 * expeditedSecurityType and expeditedSecurityNumber]
 * Seat # [as part of UmTravelerFlight]
 * Class [as part of UmTravelerFlight]
 * eTicket Receipt(s) [as part of UmTraveler]
 * Frequent Flyer (as part of UmTravelerFlight)
 * Meal (as part of UmTravelerFlight)
 * Rate [as part of UmTraveler]
 * <p>
 * Created by surge on 2016-11-25.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmFlightReservation
    extends UmReservation {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  /**
   * [URL] (recommended for Google Now/Search Answers) Webpage where the
   * passenger can check in.
   */
  private String checkinUrl;
  /**
   * Umapped  extension place to keep baggage URL
   * [URL]
   */
  private String baggageUrl;
  private UmFlight flight;

  /****************************************************************************
   * Fields from ticket: PUB-922 *
   ****************************************************************************/
  private String departureAirportAdvisories;
  private String arrivalAirportAdvisories;

  public String getCheckinUrl() {
    return checkinUrl;
  }

  public UmFlightReservation setCheckinUrl(String checkinUrl) {
    this.checkinUrl = checkinUrl;
    return this;
  }

  public String getBaggageUrl() {
    return baggageUrl;
  }

  public UmFlightReservation setBaggageUrl(String baggageUrl) {
    this.baggageUrl = baggageUrl;
    return this;
  }

  public String getDepartureAirportAdvisories() {
    return departureAirportAdvisories;
  }

  public UmFlightReservation setDepartureAirportAdvisories(String departureAirportAdvisories) {
    this.departureAirportAdvisories = departureAirportAdvisories;
    return this;
  }

  public String getArrivalAirportAdvisories() {
    return arrivalAirportAdvisories;
  }

  public UmFlightReservation setArrivalAirportAdvisories(String arrivalAirportAdvisories) {
    this.arrivalAirportAdvisories = arrivalAirportAdvisories;
    return this;
  }

  public UmFlight getFlight() {
    if (flight == null) {
      flight = new UmFlight();
    }
    return flight;
  }

  public UmFlightReservation setFlight(UmFlight flight) {
    this.flight = flight;
    return this;
  }
}
