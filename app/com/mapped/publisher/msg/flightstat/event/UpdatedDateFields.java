
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class UpdatedDateFields{
   	private List<UpdatedDateField> updatedDateField;

 	public List<UpdatedDateField> getUpdatedDateField(){
		return this.updatedDateField;
	}
	public void setUpdatedDateField(List<UpdatedDateField> updatedDateField){
		this.updatedDateField = updatedDateField;
	}
}
