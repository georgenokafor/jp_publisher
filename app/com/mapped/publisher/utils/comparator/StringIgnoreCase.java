package com.mapped.publisher.utils.comparator;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-04-30
 * Time: 2:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class StringIgnoreCase  implements Comparator<String> {
    public int compare(String s1, String s2) {

        return s1.toLowerCase().compareTo(s2.toLowerCase());
    }

}
