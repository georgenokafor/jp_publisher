package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-11-24.
 */
@Entity @Table(name = "t42_destination")
public class T42Destination
    extends Model {
  public static Model.Finder<Integer, T42Destination> find = new Finder(T42Destination.class);

  @Id
  Integer geoPlaceKey;
  Integer parentGeoPlaceKey;
  Integer relatedGeoPlaceKey;
  String  type;
  String  cityCode;
  @Column(name = "iso_code")
  String ISOCode;
  String  name;
  String  display;
  Float   latitude;
  Float   longitude;
  Boolean active;
  @Version
  int version;

  public static T42Destination findCountryByCode(String code) {
    List<T42Destination> r = find.where().eq("iso_code", code).eq("type", "Country").findList();
    if (r != null && r.size() > 0) {
      return r.get(0);
    }
    return null;
  }

  public static T42Destination findByKey(String placeKey) {
    Integer key = null;
    try {
      key = Integer.parseInt(placeKey);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
      return null;
    }
    return find.byId(key);
  }

  public static List<T42Destination> findByTerm(String term) {

    com.avaje.ebean.Query<T42Destination> matchQuery = Ebean
        .createQuery(T42Destination.class)
        .select("geoPlaceKey")
        .where().ilike("display","%"+term+"%")
        .query();

    com.avaje.ebean.Query<T42Guide> withGuides = Ebean
        .createQuery(T42Guide.class)
        .select("id.geoPlaceKey")
        .where().in("geo_place_key", matchQuery)
//        .having().gt("count(*)",0)
        .query();

    return find.where().in("geo_place_key", withGuides).findList();
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public Integer getGeoPlaceKey() {
    return geoPlaceKey;
  }

  public void setGeoPlaceKey(String geoPlaceKey) {
    if (geoPlaceKey.length() == 0) {
      return;
    }
    try {
      this.geoPlaceKey = Integer.parseInt(geoPlaceKey);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public void setGeoPlaceKey(Integer geoPlaceKey) {
    this.geoPlaceKey = geoPlaceKey;
  }

  public Integer getParentGeoPlaceKey() {
    return parentGeoPlaceKey;
  }

  public void setParentGeoPlaceKey(String parentGeoPlaceKey) {
    if (parentGeoPlaceKey.length() == 0) {
      return;
    }
    try {
      this.parentGeoPlaceKey = Integer.parseInt(parentGeoPlaceKey);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public void setParentGeoPlaceKey(Integer parentGeoPlaceKey) {
    this.parentGeoPlaceKey = parentGeoPlaceKey;
  }

  public Integer getRelatedGeoPlaceKey() {
    return relatedGeoPlaceKey;
  }

  public void setRelatedGeoPlaceKey(String relatedGeoPlaceKey) {
    if (relatedGeoPlaceKey.length() == 0) {
      return;
    }
    try {
      this.relatedGeoPlaceKey = Integer.parseInt(relatedGeoPlaceKey);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public void setRelatedGeoPlaceKey(Integer relatedGeoPlaceKey) {
    this.relatedGeoPlaceKey = relatedGeoPlaceKey;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getCityCode() {
    return cityCode;
  }

  public void setCityCode(String cityCode) {
    this.cityCode = cityCode;
  }

  public String getISOCode() {
    return ISOCode;
  }

  public void setISOCode(String ISOCode) {
    this.ISOCode = ISOCode;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDisplay() {
    return display;
  }

  public void setDisplay(String display) {
    this.display = display;
  }

  public Float getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    if (latitude.length() == 0) {
      return;
    }
    try {
      this.latitude = Float.parseFloat(latitude);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public void setLatitude(Float latitude) {
    this.latitude = latitude;
  }

  public Float getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    if (longitude.length() == 0) {
      return;
    }
    try {
      this.longitude = Float.parseFloat(longitude);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public void setLongitude(Float longitude) {
    this.longitude = longitude;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
