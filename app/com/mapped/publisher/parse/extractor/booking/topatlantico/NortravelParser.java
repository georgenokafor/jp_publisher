package com.mapped.publisher.parse.extractor.booking.topatlantico;

/**
 * Created by twong on 15-04-07.
 */
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import org.apache.commons.lang3.time.DateUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ryan on 15-04-02.
 */
public class NortravelParser {
  private String file;
  private TripVO trip;

  private static String[] dateTimeFormat = {"yyyy-MM-dd HH:mm", "ddMMMyy HH:mm"} ;
  private static DateUtils du = new DateUtils();
  private static Calendar now = Calendar.getInstance();
  private static Pattern datePattern = Pattern.compile("[0-9]{4}-[0-9]{2}-[0-9]{2}");
  private static Pattern airportCode = Pattern.compile("\\([A-Z]{3}\\)");

  private static Pattern flightDatePattern = Pattern.compile("[0-9]{2}[A-Z]{3}[0-9]{2}");
  private static Pattern flightDatePattern1 = Pattern.compile("[0-9]{2}[A-Z]{3}");

  private static Pattern flightTimePattern = Pattern.compile("[0-9]{2}:[0-9]{2}");

  public NortravelParser(String file1) {
    file = file1;

    //remove know text
    file = file.replaceAll("NORTRAVEL - AGÊNCIA DE VIAGENS E TURISMO, SA ","");
    file = file.replaceAll("Rua Pedro Homem de Melo, 55 - 2º sala 206 \\| 4150-599 Porto ","");
    file = file.replaceAll("Tel\\. :\\+351 225 320 450 \\| Fax\\. :\\+351 226 168 925/27 ","");
    file = file.replaceAll("R.Joaquim António Aguiar, 66 - 3º Dto | 1070-153 Lisboa ","");
    file = file.replaceAll("Tel\\.:\\+351 213 808 220 \\| Fax\\. :\\+351 213 808 229 ","");

    trip = new TripVO();
  }

  public TripVO getTrip() {

    if (file.contains("Noites")) {
      parseHotel();
    } else if (file.contains("Voos")) {
      parseFlight();
    }
    trip.setImportSrc(BookingSrc.ImportSrc.TA_NORTRAVEL_PDF);
    trip.setImportTs(System.currentTimeMillis());
    return trip;
  }

  public void parseHotel() {

    String [] tokens = file.split("\n");
    HotelVO hotelVO = new HotelVO();

    StringBuilder addr = new StringBuilder();
    boolean processAddr = true;
    boolean processPassenger = false;


    for (String s: tokens) {
      if (s.contains("BOOKED AND PAYMENT BY:") || s.contains("www.optigest.net")) {
        break;
      }
      Matcher matcher = datePattern.matcher(s);
      if (matcher.find()) {
        String date = matcher.group();
        if (hotelVO.getCheckin() == null) {
          hotelVO.setCheckin(getTimestamp(date, "15:00"));
          if (s.length() > date.length()) {
            hotelVO.setHotelName(s.replaceAll(date,"").trim());
          }
        } else if (hotelVO.getCheckout() == null) {
          hotelVO.setCheckout(getTimestamp(date, "11:00"));

        }
      } else if (processAddr) {
        if (s.contains("Passageiro")) {
          processAddr = false;
          processPassenger = true;
          addr.append("\n");
        } else if (!s.contains("Original")  && !s.contains("Entrada")  && !s.contains("Saída")  && !s.contains("Processo")) {
          addr.append(s);addr.append("\n");
          if (s.contains("Voucher") && s.length() > 10) {
            hotelVO.setConfirmation(s.substring(10).trim());
          }
        } else if (s.contains("Saída") && s.length() > 17 ) {
          addr.append(s.substring(17).trim());
          addr.append("\n");
        }
      } else if (!processAddr && !processPassenger) {
        if (processPassenger)
        if (!s.contains("Emissão")) {
          addr.append(s);addr.append("\n");

        }
      } else if (processPassenger && s.contains("Serviços")) {
        processPassenger = false;
      }

    }

    if (hotelVO.isValid()) {
      hotelVO.setNote(addr.toString());
      trip.addHotel(hotelVO);
    }

  }

  public void parseFlight() {

    String[] reservations = file.split("Informação da Reserva / Reservation Info\n");
    HashMap<String, PassengerVO> passengers = new HashMap<>();

    for (String s:reservations) {
      if (s.startsWith("Num. Bilhete")) {
        PassengerVO passengerVO = new PassengerVO();
        StringBuilder pNote = new StringBuilder();
        String [] tokens = s.split("\n");
        for (int i =0; i < tokens.length; i++) {
          String line = tokens[i];

          if (line.startsWith("Num. Bilhete")) {
            pNote.append(line);pNote.append("\n");
          } else if (line.startsWith("Número de Reserva")) {
            pNote.append(line);pNote.append("\n");
            if (line.contains("Nome Passageiro :")) {
              String name = line.substring(line.indexOf("Nome Passageiro :")+17).trim();
              if (name.contains(",")) {
                passengerVO.setFirstName(name.substring(name.indexOf(",")+1).trim());
                passengerVO.setLastName(name.substring(0, name.indexOf(",")).trim());
              } else {
                passengerVO.setFirstName(name);
                passengerVO.setLastName("");
              }
            }
          } else if (line.startsWith("Voo :")) {
            FlightVO flight = new FlightVO();
            //start processing the flight
            String year = "";
            while (true) {
              if (line.startsWith("Arrive")) {
                break;
              }

              if (line.startsWith("Voo :")) {
                flight.setCode(line.substring(line.indexOf("(") + 1,line.indexOf(")")));
                String s1 = line.substring(line.indexOf(")") + 1).trim();
                flight.setNumber(s1.substring(0, s1.indexOf(" ")));
                flight.setReservationNumber(s1.substring(s1.indexOf(":") + 1).trim());
              } else if (line.startsWith("Partida")) {
                Matcher m = airportCode.matcher(line);
                if (m.find()) {
                  String airportCode=m.group().replaceAll("\\(","").replaceAll("\\)","");
                  AirportVO airport = new AirportVO();
                  airport.setCode(airportCode);
                  flight.setDepartureAirport(airport);
                }
                Matcher d = flightDatePattern.matcher(line);
                if (d.find()) {
                  String date = d.group();
                  year = date.substring(5);
                  Matcher t = flightTimePattern.matcher(line);
                  if (t.find()) {
                    String time = t.group();
                    flight.setDepatureTime(getTimestamp(date,time));

                  }
                }

              } else if (line.startsWith("Chegada")) {
                Matcher m = airportCode.matcher(line);
                if (m.find()) {
                  String airportCode=m.group().replaceAll("\\(","").replaceAll("\\)","");
                  AirportVO airport = new AirportVO();
                  airport.setCode(airportCode);
                  flight.setArrivalAirport(airport);
                }
                Matcher d = flightDatePattern1.matcher(line);
                if (d.find()) {
                  String date = d.group() + year;
                  Matcher t = flightTimePattern.matcher(line);
                  if (t.find()) {
                    String time = t.group();
                    flight.setArrivalTime(getTimestamp(date,time));

                  }
                }
              }


              i++;
              if ((i+1) >= tokens.length) {
                break;
              } else {
                line = tokens[i];
              }
            }
            if (flight.isValid()) {
              //check to see if flight already exists
              if (trip.getFlights() != null) {
                boolean foundFlight = false;
                for (FlightVO fvo : trip.getFlights()) {
                  if (fvo.getNumber().equals(flight.getNumber()) && fvo.getCode().equals(flight.getCode()) && fvo.getDepatureTime().equals(flight.getDepatureTime())
                      && fvo.getDepartureAirport() != null && fvo.getDepartureAirport().getCode() != null
                      && flight.getDepartureAirport() != null && flight.getDepartureAirport().getCode() != null
                      && fvo.getDepartureAirport().getCode().equals(flight.getDepartureAirport().getCode()) && fvo.getReservationNumber() != null && flight.getReservationNumber() != null
                      && fvo.getReservationNumber().equals(flight.getReservationNumber())) {
                    if (passengerVO.getFirstName() != null) {
                      passengerVO.setNote(pNote.toString());
                      fvo.addPassenger(passengerVO);
                      foundFlight = true;
                      break;
                    }
                  }
                }
                if (!foundFlight) {
                  if (passengerVO.getFirstName() != null) {
                    passengerVO.setNote(pNote.toString());
                    flight.addPassenger(passengerVO);
                  }
                  trip.addFlight(flight);
                }

              }


            }
          }
        }
      }
    }



  }

  private Timestamp getTimestamp (String date, String time) {
    try {
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime() );
      return t;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
