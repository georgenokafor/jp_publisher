package com.umapped.itinerary.analyze.supplier.wcities;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.umapped.itinerary.analyze.TripFeature;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.recommendation.RecommendedItem;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeature;
import com.umapped.itinerary.analyze.supplier.AccommondationProvider;
import com.umapped.itinerary.analyze.supplier.ActivityProvider;
import com.umapped.itinerary.analyze.supplier.ContentProvider;
import com.umapped.itinerary.analyze.supplier.ProviderHelper;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * Created by wei on 2017-04-20.
 */
@Singleton public class WCitiesProivder
    implements AccommondationProvider, ActivityProvider, ContentProvider {

  private static final Logger LOG = LoggerFactory.getLogger(WCitiesProivder.class);

  private WCitiesWebService wCitiesWebService;
  private ProviderHelper providerHelper;
  private WCitiesCityLookup cityLookup;

  @Inject
  public WCitiesProivder(WCitiesWebService wCitiesWebService,
                         ProviderHelper providerHelper,
                         WCitiesCityLookup cityLookup) {
    this.wCitiesWebService = wCitiesWebService;
    this.providerHelper = providerHelper;
    this.cityLookup = cityLookup;
  }

  @Override
  public List<RecommendedItem> findContents(DecisionResult decisionResult,
                                            EnumMap<TripFeature, Object> tripFeatures,
                                            EnumMap<SegmentFeature, Object> segmentFeatures) {
    Location location = providerHelper.findLocation(decisionResult);
    if (location == null) {
      return Collections.emptyList();
    }
    return wCitiesWebService.getGuides(location);
  }

  @Override
  public List<RecommendedItem> findActivities(DecisionResult decisionResult,
                                              EnumMap<TripFeature, Object> tripFeatures,
                                              EnumMap<SegmentFeature, Object> segmentFeatures) {
    Map<String, List<RecommendedItem>> eventMap = new HashMap<>();

    Location location = providerHelper.findLocation(decisionResult);
    if (location == null) {
      return Collections.emptyList();
    }



    LocalDate startDate = decisionResult.getStartDate();
    LocalDate endDate = decisionResult.getEndDate();
    StayPeriod period = new StayPeriod(startDate, endDate);

    List<CompletableFuture> searches = new ArrayList<>();


    searches.add(CompletableFuture.supplyAsync(() -> new ImmutablePair<>(Constants.FEATURE_GUIDED_TOUR,
                                                                         wCitiesWebService.getEvents(period,
                                                                                                     location,
                                                                                                     RequestPreset
                                                                                                         .GuidedTour)
    )));
    searches.add(CompletableFuture.supplyAsync(() -> new ImmutablePair<>(Constants.FEATURE_ENTERTAINMENT,
                                                                         wCitiesWebService.getEvents(period,
                                                                                                     location,
                                                                                                     RequestPreset
                                                                                                         .Entertainment))));
    searches.add(CompletableFuture.supplyAsync(() -> new ImmutablePair<>(Constants.FEATURE_CHILDREN,
                                                                         wCitiesWebService.getEvents(period,
                                                                                                     location,
                                                                                                     RequestPreset
                                                                                                         .Children))));
    searches.add(CompletableFuture.supplyAsync(() -> new ImmutablePair<>(Constants.FEATURE_MUSEUM,
                                                                         wCitiesWebService.getEvents(period,
                                                                                                     location,
                                                                                                     RequestPreset
                                                                                                         .Museum))));

    CompletableFuture[] performSearches = new CompletableFuture[searches.size()];
    searches.toArray(performSearches);
    try {
      return CompletableFuture.allOf(performSearches).thenApply((ignored) -> {
        Map<String, List<RecommendedItem>> resultMap = new HashMap<>();
        for (CompletableFuture<Pair<String, List<RecommendedItem>>> s : performSearches) {
          Pair<String, List<RecommendedItem>> result = s.join();
          resultMap.put(result.getLeft(), result.getRight());
        }
        return providerHelper.getRankingStrategy(tripFeatures, decisionResult).rank(resultMap);
      }).get();
    }
    catch (Exception e) {
      LOG.error("Fail to combine multiple searches", e);
    }

    return Collections.emptyList();
  }

  @Override
  public List<RecommendedItem> findAccommondations(DecisionResult decisionResult,
                                                   EnumMap<TripFeature, Object> tripFeatures,
                                                   EnumMap<SegmentFeature, Object> segmentFeatures) {

    Location location = providerHelper.findLocation(decisionResult);
    if (location == null) {
      return Collections.emptyList();
    }

    CompletableFuture<List<RecommendedItem>> cityHotels = CompletableFuture.supplyAsync(() -> cityLookup.getCityId(
        location.getGeoLocation())).thenApply(cityId -> wCitiesWebService.getHotelsByCity(cityId));

    CompletableFuture<List<RecommendedItem>> nearByHotels = CompletableFuture.supplyAsync(() -> wCitiesWebService.
                                                                                                                     getHotelsByLocation(
                                                                                                                         location));
    try {
      List<RecommendedItem> recommendedItems = cityHotels.thenCombine(nearByHotels, (c, n) -> {
        Map<String, List<RecommendedItem>> resultMap = new HashMap<>();
        resultMap.put(Constants.FEATURE_CITY, c);
        resultMap.put(Constants.FEATURE_NEARBY, n);
        return providerHelper.getRankingStrategy(tripFeatures, decisionResult).rank(resultMap);
      }).get();
      return recommendedItems;
    }
    catch (Exception e) {
      LOG.error("Fail to combine the cit and nearyby hotels");
      return Collections.emptyList();
    } finally {

    }
  }
//
//  public static void main(String[] args) {
//    EBeanServerConfig.init();
//    Injector container = Guice.createInjector(new ItineraryAnalyzeModule(), new WebServiceModule());
//
//    WCitiesProivder service = container.getInstance(WCitiesProivder.class);
//
//    LocalDate start = LocalDate.now().plusDays(10);
//    LocalDate end = start.plusDays(10);
//
//
//    Location location = new Location();
//    location.setGeoLocation(new Location.GeoLocation(51.4775, -0.461389));
//
//    DecisionResult dr = new DecisionResult(ItineraryItemType.Accommodation, location, start);
//
//    System.out.println(service.findActivities(dr, null, null));
//
//    System.out.println(service.findAccommondations(dr, null, null));
//  }

}
