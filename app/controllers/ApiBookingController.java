package controllers;

import actors.ActorsHelper;
import actors.SupervisorActor;
import actors.TripAutoCreatePublishActor;
import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.AdminAccess;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.api.AddCmpyTokenForm;
import com.mapped.publisher.form.api.AddUserLinkForm;
import com.mapped.publisher.parse.classicvacations.datesearch.Booking;
import com.mapped.publisher.parse.extractor.booking.email.ReservationsHolder;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.parse.schemaorg.*;
import com.mapped.publisher.persistence.VOModeller;
import com.mapped.publisher.persistence.bookingapi.BookingAPIMgr;
import com.mapped.publisher.persistence.bookingapi.BookingRS;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.persistence.communicator.MessengerUpdateHelper;
import com.mapped.publisher.persistence.communicator.RoomMessage;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.ApiCmpyTokenView;
import com.mapped.publisher.view.ApiUserLinkView;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.UserMessage;
import com.umapped.BodyParsers.TolerantText1Mb;
import com.umapped.api.AutoPublishProp;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.helper.TripPublisherHelper;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.flight.UmFlightTraveler;
import com.umapped.persistence.reservation.utils.UmReservationUtils;
import models.publisher.*;
import org.apache.commons.lang3.time.StopWatch;
import org.owasp.html.Sanitizers;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by twong on 15-09-14.
 */
public class ApiBookingController
    extends Controller {

  @Inject
  FormFactory formFactory;

  @Inject
  TripPublisherHelper tripPublisherHelper;


  @With({Authenticated.class, Authorized.class})
  public Result addApiUserLink() {
    BaseView baseView = new BaseView();

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Form<AddUserLinkForm> linkForm   = formFactory.form(AddUserLinkForm.class);
    AddUserLinkForm       linkInfo   = linkForm.bindFromRequest().get();
    ApiCmpyToken          cmpyToken  = ApiCmpyToken.find.byId(linkInfo.inCmpyTokenPk);
    if (cmpyToken == null || linkInfo.inSrcUserId == null || linkInfo.inSrcUserId.length() == 0) {
      baseView.message = "Error - cannot add the user api link";
      return ok(views.html.common.message.render(baseView));

    } else if (!sessionMgr.getCredentials().isUmappedAdmin() &&
               SecurityMgr.isCmpyAdmin(linkInfo.inCmpyId, sessionMgr)) {
      List<UserCmpyLink> cmpyLinks = UserCmpyLink.findActiveByUserIdCmpyId(linkInfo.inUserId,
                                                                           linkInfo.inCmpyId);
      if (cmpyLinks == null || cmpyLinks.size() == 0) {
        BaseView view = new BaseView();
        view.message = "Unauthorized Access";
        return ok(views.html.common.message.render(view));
      }
    }

    try {
      if (!ApiUserLink.doesUserIdExist(linkInfo.inUserId, linkInfo.inCmpyTokenPk, linkInfo.inSrcUserId)) {
        ApiUserLink rec = new ApiUserLink();
        rec.setPk(DBConnectionMgr.getUniqueLongId());
        rec.setuUserId(linkInfo.inUserId);
        rec.setSrcUserId(linkInfo.inSrcUserId);
        rec.setApiCmpyTokenPk(linkInfo.inCmpyTokenPk);
        rec.setCreatedBy(sessionMgr.getUserId());
        rec.setCreatedTs(System.currentTimeMillis());
        rec.setModifiedBy(sessionMgr.getUserId());
        rec.setUpdatedTs(System.currentTimeMillis());
        rec.save();

        //update all recs in the bk_api_rec
        BookingAPIMgr.updateUserMapping(rec.getuUserId(), cmpyToken.getSrcCmpyId(), rec.getSrcUserId(), cmpyToken.getBkApiSrcId(), sessionMgr.getUserId());
        flash(SessionConstants.SESSION_PARAM_MSG, "Link successfully added");
        return redirect(routes.UserController.user(linkInfo.inUserId, linkInfo.inCmpyId, true));
      } else {
        baseView.message = "Error - This external user id is already mapped for this external company. Please try again or contact Umapped support.";

      }

    } catch (Exception e) {
      baseView.message = "Error - cannot add the user api link";
    }
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteApiUserLink(Long linkId, String inCmpyId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Credentials cred = sessionMgr.getCredentials();

    try {
      ApiUserLink rec = ApiUserLink.find.byId(linkId);
      if (rec ==  null ) {
        return UserMessage.message(ReturnCode.DB_RECORD_NOT_FOUND);
      }

      ApiCmpyToken cmpyToken = ApiCmpyToken.find.byId(rec.getApiCmpyTokenPk());

      if (!(cred.isUmappedAdmin() ||
            cred.getCmpyId().equals(cmpyToken.getuCmpyId()) ||
            sessionMgr.getUserId().equals(rec.getuUserId()))) {
        return UserMessage.message(ReturnCode.LOGICAL_ERROR, "Error - cannot delete the user api link");
      }
      rec.delete();

      BookingAPIMgr.updateUserMapping(null, cmpyToken.getSrcCmpyId(), rec.getSrcUserId(), cmpyToken.getBkApiSrcId(), sessionMgr.getUserId());

      //clear cache
      CacheMgr.set(APPConstants.CACHE_API_USER_LINK_PREFIX + cmpyToken.getPk() + rec.getSrcUserId(), null);
      flash(SessionConstants.SESSION_PARAM_MSG, "Link successfully deleted");
      return redirect(routes.UserController.user(rec.getuUserId(), inCmpyId, true));

    } catch (Exception e) {
      return UserMessage.message(ReturnCode.LOGICAL_ERROR, "Error - cannot delete the user api link");
    }
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result addCmpyTokenLink() {
    BaseView baseView = new BaseView();

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Form<AddCmpyTokenForm> linkForm   = formFactory.form(AddCmpyTokenForm.class);
    AddCmpyTokenForm       linkInfo   = linkForm.bindFromRequest().get();

    if (linkInfo.inSrcCmpyId == null || linkInfo.inSrcCmpyId.isEmpty()) {
      baseView.message = "Error - cannot add the cmpy token with no cmpy name";
      return ok(views.html.common.message.render(baseView));
    }
    if (linkInfo.tokenType == null || linkInfo.tokenType.isEmpty() || linkInfo.tokenType.equals("CUSTOM-RANDOM")) {
      linkInfo.token = Utils.shortenNumber(DBConnectionMgr.getUniqueKey());
      linkInfo.tokenType = "CUSTOM-RANDOM";
    } else  if (linkInfo.tokenType != null && linkInfo.tokenType.equals("EXTERNAL")){
      if (linkInfo.token == null || linkInfo.token.isEmpty()) {
        baseView.message = "Error - Please enter an external token";
        return ok(views.html.common.message.render(baseView));
      }
      linkInfo.tokenType = "EXTERNAL";
    }
    if (linkInfo.expiryTs == 0) {
      //default to 10 years
      org.joda.time.DateTime dateTime = new org.joda.time.DateTime();
      linkInfo.expiryTs = dateTime.plusYears(10).getMillis();
    } else if (linkInfo.expiryTs < System.currentTimeMillis()) {
      baseView.message = "Error - cannot add the cmpy token with a past expiry";
      return ok(views.html.common.message.render(baseView));
    }


    try {
      if (linkInfo.inPk != null && linkInfo.inPk > 0) {
        ApiCmpyToken rec = ApiCmpyToken.find.byId(linkInfo.inPk);
        if (rec != null && rec.getuCmpyId().equals(linkInfo.inCmpyId) && rec.getSrcCmpyId().equals(linkInfo.inSrcCmpyId)) {
          rec.setTag(linkInfo.tag);
          rec.setModifiedBy(sessionMgr.getUserId());
          rec.update();
          CacheMgr.set(APPConstants.CACHE_API_CMPY_TOKEN_PREFIX + rec.getToken(), rec);
          flash(SessionConstants.SESSION_PARAM_MSG, "Tag updated for token " + linkInfo.token);
          return redirect(routes.CompanyController.viewCompany(linkInfo.inCmpyId));
        } else {
          baseView.message = "Error - Cannot update the API rec - " + linkInfo.inPk;

        }

      } else {
        if (BkApiSrc.getName(linkInfo.inSrcTypeId) != null && !ApiCmpyToken.doesCmpyTokenExist(linkInfo.inCmpyId, linkInfo.inSrcTypeId, linkInfo.inSrcCmpyId)) {
          ApiCmpyToken rec = new ApiCmpyToken();
          rec.setPk(DBConnectionMgr.getUniqueLongId());
          rec.setuCmpyId(linkInfo.inCmpyId);
          rec.setSrcCmpyId(linkInfo.inSrcCmpyId);
          rec.setToken(linkInfo.token);
          rec.setTokenType(linkInfo.tokenType);
          rec.setExpiryTs(linkInfo.expiryTs);
          rec.setBkApiSrcId(linkInfo.inSrcTypeId);
          rec.setCreatedBy(sessionMgr.getUserId());
          rec.setCreatedTs(System.currentTimeMillis());
          rec.setModifiedBy(sessionMgr.getUserId());
          rec.setUpdatedTs(System.currentTimeMillis());
          rec.setTag(linkInfo.tag);
          rec.save();

          flash(SessionConstants.SESSION_PARAM_MSG, "Link successfully added");
          return redirect(routes.CompanyController.viewCompany(linkInfo.inCmpyId));
        } else {
          baseView.message = "Error - duplicate external cmpy link";
        }
      }

    } catch (Exception e) {
      baseView.message = "Error - cannot add the cmpy token link";
    }
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result deleteCmpyTokenLink(Long linkId) {
    BaseView baseView = new BaseView();

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Ebean.beginTransaction();
    try {
      ApiCmpyToken rec = ApiCmpyToken.find.byId(linkId);
      if (rec !=  null ) {
        ApiUserLink.deleteByTokenPk(rec.getPk());
        rec.delete();
        CacheMgr.set(APPConstants.CACHE_API_CMPY_TOKEN_PREFIX + rec.getSrcCmpyId() + "_" + rec.getBkApiSrcId(), null);

        Ebean.commitTransaction();
        flash(SessionConstants.SESSION_PARAM_MSG, "Link successfully deleted");
        return redirect(routes.CompanyController.viewCompany(rec.getuCmpyId()));
      }

    } catch (Exception e) {
      Ebean.rollbackTransaction();
      baseView.message = "Error - cannot delete the user api link";
    }
    return ok(views.html.common.message.render(baseView));
  }

  public static List<ApiUserLinkView> getApiUserLink(String userId) {
    List<ApiUserLink> links = ApiUserLink.findByUserId(userId);
    List<ApiUserLinkView> linkViews =  new ArrayList<>();
    if (links != null && !links.isEmpty()) {
      for (ApiUserLink link: links) {
        ApiCmpyToken cmpyToken = ApiCmpyToken.find.byId(link.getApiCmpyTokenPk());
        if (cmpyToken != null) {
          ApiUserLinkView v = new ApiUserLinkView();
          v.pk = link.getPk();
          v.srcUserId = link.getSrcUserId();
          v.srcName = BkApiSrc.getName(cmpyToken.getBkApiSrcId());
          v.cmpyId = cmpyToken.getuCmpyId();
          v.cmpyName = Company.find.byId(v.cmpyId).getName();
          v.externalCmpyName = cmpyToken.getSrcCmpyId();
          v.apiCmpyTokenPK = cmpyToken.getPk();
          linkViews.add(v);
        }
      }
    }
    return linkViews;
  }

  public static List<ApiCmpyTokenView> getApiCmpyTokens(String cmpyId) {
    List<ApiCmpyToken> links = ApiCmpyToken.findByCmpyId(cmpyId);
    List<ApiCmpyTokenView> linkViews =  new ArrayList<>();
    if (links != null && !links.isEmpty()) {
      for (ApiCmpyToken link: links) {
        ApiCmpyTokenView v = new ApiCmpyTokenView();
        v.pk = link.getPk();
        v.srcId = link.getBkApiSrcId();
        v.srcCmpyId = link.getSrcCmpyId();
        v.srcName = BkApiSrc.getName(link.getBkApiSrcId());
        v.expiryTS = link.getExpiryTs();
        v.token = link.getToken();
        v.tokenType = link.getTokenType();
        v.tag = link.getTag();

        linkViews.add(v);
      }

    }
    return linkViews;
  }

  @BodyParser.Of(TolerantText1Mb.class)
  public Result booking() {
    //TO DO check the json token
    //make use the token is valid
    try {
      StopWatch sw = new StopWatch();
      sw.start();
      String authToken = request().getHeader("Authorization");
      if (authToken != null && authToken.contains("Bearer")) {
        authToken = authToken.replace("Bearer", "").trim();
      }
      ApiCmpyToken cmpyToken = null;
      //security validation
      cmpyToken = (ApiCmpyToken) CacheMgr.get(APPConstants.CACHE_API_CMPY_TOKEN_PREFIX + authToken);
      if (cmpyToken == null) {
        cmpyToken = ApiCmpyToken.findByToken(authToken);
        if (cmpyToken != null) {
          CacheMgr.set(APPConstants.CACHE_API_CMPY_TOKEN_PREFIX + cmpyToken.getToken(), cmpyToken);
        }
      }

      StringBuilder debugMsg = new StringBuilder();
      ObjectMapper om1 = new ObjectMapper();
      String requestText = request().body().asText();

      // Log the request
      if (!ConfigMgr.isProduction()) {
        Log.debug("ApiBookingController:booking request: " + requestText);
      }


      JsonNode jsonRequest = om1.readTree(requestText); ///request().body().asJson();

      ReservationsHolder rh = new ReservationsHolder();

      try {
        boolean parseResult = rh.parseJson(jsonRequest.toString());

        if (parseResult && rh.hasValidReservations() &&
            rh.reservationPackage != null &&
            rh.reservationPackage.umId != null) {
          if (cmpyToken != null && cmpyToken.getExpiryTs() > System.currentTimeMillis() && cmpyToken.getSrcCmpyId().equals(rh.reservationPackage.umId)) {
            Log.info("ApiBookingController:booking Received request: umId:" + rh.reservationPackage.umId + " PNR: " + rh.reservationPackage.reservationNumber);

            String umCmpyId = cmpyToken.getuCmpyId();
            String srcCmpyId = cmpyToken.getSrcCmpyId();
            int srcId = cmpyToken.getBkApiSrcId();
            String pnr = rh.reservationPackage.reservationNumber;
            String umUserId = null;
            String srcUserId = null;

            if (rh.reservationPackage.bookingAgent != null && rh.reservationPackage.bookingAgent.umId != null) {
              srcUserId = rh.reservationPackage.bookingAgent.umId;
              //see if we have this mapped
              ApiUserLink userLink = (ApiUserLink) CacheMgr.get(APPConstants.CACHE_API_USER_LINK_PREFIX + cmpyToken.getToken() + rh.reservationPackage.bookingAgent.umId);
              if (userLink == null) {
                userLink = ApiUserLink.findByTokenPkSrcId(cmpyToken.getPk(), rh.reservationPackage.bookingAgent.umId);
                if (userLink != null) {
                  CacheMgr.set(APPConstants.CACHE_API_USER_LINK_PREFIX + cmpyToken.getToken() + rh.reservationPackage.bookingAgent.umId, userLink);
                }
              }
              if (userLink !=null) {
                umUserId = userLink.getuUserId();
              }
            }


            //UserProfile up = UserProfile.findByPK(umUserId);
            Company cmpy = null;
            Account a = null;
            if (umUserId != null) {
              a = Account.findActiveByLegacyId(umUserId);
            }
            //

            if (umCmpyId != null) {
              cmpy = Company.find.byId(umCmpyId);
            }

            AutoPublishProp autoPublishProp = new AutoPublishProp(cmpy, cmpyToken);
            if (autoPublishProp.autoPublish) {
              //audit the api call
              try {
                BkAPIAudit bkAPIAudit = BkAPIAudit.build(srcCmpyId, rh.reservationPackage.reservationNumber, srcId, requestText);
                if (bkAPIAudit != null) {
                  bkAPIAudit.save();
                }
              } catch (Exception e) {
                Log.err("ApiBookingController: cannot audit: " + requestText, e);
              }
            }
            //if this is autopublishing and we do not have a match for the userid, we try to default to a default userid
            if (autoPublishProp.autoPublish && a == null) {
              //see if we have this mapped to a default - so it always autopublishes
              ApiUserLink userLink = (ApiUserLink) CacheMgr.get(APPConstants.CACHE_API_USER_LINK_PREFIX + cmpyToken.getToken() + APPConstants.CMPY_AUTOPUBLISH_DEFAULT_USER);
              if (userLink == null) {
                userLink = ApiUserLink.findByTokenPkSrcId(cmpyToken.getPk(), cmpyToken.getToken()); //default user will be mapped using the company token
                if (userLink != null) {
                  CacheMgr.set(APPConstants.CACHE_API_USER_LINK_PREFIX + cmpyToken.getToken() + APPConstants.CMPY_AUTOPUBLISH_DEFAULT_USER, userLink);
                }
              }
              if (userLink !=null) {
                a = Account.findByLegacyId(userLink.getuUserId());
                umUserId = userLink.getuUserId();
              }
            }

            //insert or update the reservation
            List<BookingRS> newRecs = getBookingRS(srcId, srcCmpyId, srcUserId, umCmpyId, umUserId, rh);
            List<BookingRS> existingRecs = BookingAPIMgr.findByPnrSrcId(rh.reservationPackage.reservationNumber, srcId, umCmpyId, srcCmpyId);
            List<BookingRS> toBeDeleted = new ArrayList<>();
            HashMap<Long, BookingRS> existingRecsMap = new HashMap<>();
            if (existingRecs != null) {
              for (BookingRS r: existingRecs) {
                existingRecsMap.put(r.pk, r);
              }
            }
            TripAutoCreatePublishActor.Command cmd = new TripAutoCreatePublishActor.Command(rh.reservationPackage.name, a, cmpy, pnr, srcId, srcCmpyId, autoPublishProp);



            int insert = 0;
            int update = 0;
            int delete = 0;
            int failed = 0;
            int unchanged = 0;


            if (existingRecs.isEmpty()) {
              int countActiveRecs = 0;
              for (BookingRS rs : newRecs) {
                if (!rs.getState().equals("DELETED") && rs.getBookingType() != ReservationType.PACKAGE.ordinal()) {
                  countActiveRecs++;
                }
              }
              if (countActiveRecs > 0) {
                //do all inserts - for packages that have at least 1 booking
                for (BookingRS rs : newRecs) {
                  if (!rs.getState().equals("DELETED")) {
                    try {
                      if (rs.createdBy == null) {
                        rs.createdBy = "system";
                      }
                      if (rs.modifiedBy == null) {
                        rs.modifiedBy = "system";
                      }
                      BookingAPIMgr.insert(rs);
                      insert++;
                    }
                    catch (Exception e) {
                      Log.log(LogLevel.ERROR, "Booking API: Cannot insert reservation ", rs.toString());
                      Log.err("APIBookingController: Insert reservation failure", e);
                      failed++;
                    }
                  }
                }
                //if the company is configured for auto trip creation and publishing - then we go ahead and create the trip and publish it
                if (autoPublishProp.autoPublish) {
                  if (a != null) {
                    //let's call this sync until we can have a proper actor recovery process in place
                    //ActorsHelper.tell(SupervisorActor.UmappedActor.TRIP_AUTO_CREATE_PUBLISH, cmd);
                    TripAutoCreatePublishActor.publishSyncTrip(cmd);
                  } else {
                    Log.log(LogLevel.ERROR, "Booking API: Cannot auto publish reservation - no user mapped. Company: " + cmpy.getCmpyid() + " RecLocator: " + pnr + " SrcId: " + srcId);
                  }
                }
              }
            }
            else {
              //there are existing records - so we need to do some additional processing
              //1. insert net new recs
              //2. update existing recs if hash changed
              //3. delete any recs that are no longer on the PNRs
              //4. update any bookings that are affected by the changes - additions/updates
              List<BookingRS> toBeUpdated = new ArrayList<>();

              HashMap<String, Trip> republishTrips = new HashMap<>();

              Map<String, List<BookingRS>> newRSMapList = new HashMap<>();
              //this is in case there are duplicate pks coming in via the API
              //if there are duplicates, we treat all duplicate PKs as one and we delete all the recs
              //so we do not insert duplicates over and over
              for (BookingRS rs : newRecs) {
                String pk = rs.getBookingType() + "--" + rs.getSrcPk();
                List<BookingRS> bookingList = newRSMapList.get(pk);
                if (bookingList == null) {
                  bookingList = new ArrayList<>();
                }
                bookingList.add(rs);
                newRSMapList.put(pk, bookingList);

                if (rs.state.equals("DELETED")) {
                  String s = rs.getState();
                  System.out.println("STATE");
                }
              }
              //figure out which recs should be updated
              for (BookingRS rs : existingRecs) {
                BookingRS newRS = null;
                String existingReservationId = null;
                String cbDupReservationId = null;
                List<BookingRS> list = newRSMapList.get(rs.getBookingType() + "--" + rs.getSrcPk());
                if (list != null && !list.isEmpty()) {
                  if (BkApiSrc.getName(srcId).equals("CLIENTBASE") && rs.getBookingType() == ReservationType.FLIGHT.getIdx()) {
                    //hack to correct an initial bug from the clientbase gateway where the srcpk is not truly unique
                    //will be removed when the data is corrected
                    //additional check to find the reservation number from the data element
                    try {
                      existingReservationId = getReservationId(rs.getData());
                      for (BookingRS dupRS : list) {
                        String dupExistingReservationId = getReservationId(dupRS.getData());
                        if (existingReservationId.equals(dupExistingReservationId)) {
                          cbDupReservationId = dupExistingReservationId;
                          newRS = dupRS;
                          break;
                        }
                      }
                      if (newRS == null && list.size() > 0) {
                        newRS = list.get(0);
                      }
                    }catch (Exception e) {
                      e.printStackTrace();
                    }
                  } else {
                    newRS = list.get(0);
                  }
                }
                if (newRS != null) {
                  //check to see if the hash has changed
                  if (newRS.getState().equals("DELETED")) {
                    toBeDeleted.add(rs);
                    for (BookingRS r: list) {
                      if (existingReservationId == null || cbDupReservationId == null) {
                        newRecs.remove(r);
                      }  else {
                        String dupExistingReservationId = getReservationId(r.getData());
                        if (existingReservationId.equals(dupExistingReservationId)) {
                          newRecs.remove(r);
                        }
                      }
                    }
                    //if this is a reservation, we can check if there are any notes that should also be deleted
                    if (newRS.getBookingType() != ReservationType.NOTE.ordinal() && newRS.getBookingType() != ReservationType.PACKAGE.ordinal()) {
                      for (BookingRS r : existingRecs) {
                        if (r.getBookingType() == ReservationType.NOTE.ordinal() &&
                            r.getSrcPk() != null && r.getSrcPk().equals(newRS.getSrcPk())) {
                          toBeDeleted.add(r);
                        }
                      }
                    }

                  } else if (!(newRS.hash != null && rs.hash != null && newRS.hash.equals(rs.hash))) {
                    //process this as a booking update
                    toBeUpdated.add(rs);
                    for (BookingRS r: list) {
                      if (existingReservationId == null || cbDupReservationId == null) {
                        newRecs.remove(r);
                      }  else {
                        String dupExistingReservationId = getReservationId(r.getData());
                        if (existingReservationId.equals(dupExistingReservationId)) {
                          newRecs.remove(r);
                        }
                      }
                    }

                    rs.uUserId = newRS.uUserId;
                    rs.data = newRS.data;
                    rs.hash = newRS.hash;
                    rs.modifiedBy = newRS.modifiedBy;
                    rs.modifiedTS = newRS.modifiedTS;
                    rs.endTS = newRS.endTS;
                    rs.startTS = newRS.startTS;
                    rs.searchWords = newRS.searchWords;
                    rs.mainTraveler = newRS.mainTraveler;
                    try {
                      BookingAPIMgr.update(rs);
                      update++;
                    }
                    catch (Exception e) {
                      Log.log(LogLevel.ERROR, "Booking API: Cannot Update reservation ", rs.toString());
                      Log.err("APIBookingController: Updating Reservation failure", e);
                      failed++;
                    }
                  }
                  else {
                    unchanged++;
                    for (BookingRS r: list) {
                      if (existingReservationId == null || cbDupReservationId == null) {
                        newRecs.remove(r);
                      }  else {
                        String dupExistingReservationId = getReservationId(r.getData());
                        if (existingReservationId.equals(dupExistingReservationId)) {
                          newRecs.remove(r);
                        }
                      }
                    }
                  }


                } else if (autoPublishProp.fullReservationPacakage ||
                        (BkApiSrc.getName(srcId) != null && (BkApiSrc.getName(srcId).contains("SABRE") || BkApiSrc.getName(srcId).contains("SKI.COM") || BkApiSrc.getName(srcId).contains("CLIENTBASE")))) {
                  //if this is from sabre, then assume a missing item in a PNR is a deleted item since the PNR is send completely
                  //each time
                  toBeDeleted.add(rs);
                }
              }
              //insert anything that is new and not deleted
              for (BookingRS rs : newRecs) {
                if (!rs.getState().equals("DELETED")) {
                  try {
                    if (rs.createdBy == null) {
                      rs.createdBy = "system";
                    }
                    if (rs.modifiedBy == null) {
                      rs.modifiedBy = "system";
                    }
                    BookingAPIMgr.insert(rs);
                    insert++;

                    //we are now going to update the trips that are linked to this PNR and insert any new ones
                    toBeUpdated.add(rs);
                  }
                  catch (Exception e) {
                    Log.log(LogLevel.ERROR, "Booking API: Cannot insert reservation ", rs.toString());
                    Log.err("APIBookingController: insert reservation failure", e);
                    failed++;
                  }
                }
              }
              Log.info("ApiBookingController:booking BK_API_REC processed PNR: " + rh.reservationPackage.reservationNumber + " i:" + insert + " u:" + update + " d:" + delete + " same:" + unchanged);



              List<String> tripIds = BookingAPIMgr.findTrips(pnr, srcId, umCmpyId);
              if (tripIds == null || tripIds.isEmpty()) {
                Log.info("ApiBookingController:booking No Trips to update for PNR: " + rh.reservationPackage.reservationNumber);

              }
              /* the reservation package description is also saved as a note - so if it is not empty, let's update it */
              for (String s : tripIds) {
                processTripNoteForReservationPackage (rh.reservationPackage, s, "");
                Log.info("ApiBookingController:booking Trip to update  PNR: " + rh.reservationPackage.reservationNumber + " TripId: " + s);
              }

              //Delete existing bookings first so we do not conflict with a flight change where the srcpk is different but refers to the same user
              /// so we are going to remove all the bookings for these
              List<Long> pksToDelete = new ArrayList<>();
              List<String> notesSrcPksToDelete = new ArrayList<>();

              for (BookingRS rs : toBeDeleted) {
                try {
                  BookingAPIMgr.delete(rs);
                  if (rs.getBookingType() == ReservationType.NOTE.ordinal()) {
                    notesSrcPksToDelete.add(rs.getSrcPk());
                  } else {
                    pksToDelete.add(rs.getPk());
                  }
                  delete++;
                }
                catch (Exception e) {
                  Log.log(LogLevel.ERROR, "Booking API: Cannot delete reservation ", rs.toString());
                  Log.err("APIBookingController: delete reservation failure", e);
                  failed++;
                }
              }

              //find the list of bookings to delete
              if (pksToDelete.size() > 0) {
                for (String s : tripIds) {
                  Log.info("ApiBookingController:booking Processing Trip for PNR: " + rh.reservationPackage.reservationNumber + " TripId: " + s + " New/Changed:" + toBeUpdated.size() + " Deleted:" + toBeDeleted);
                  Trip tripModel = Trip.findByPK(s);
                  List<TripDetail> tripDetails = new ArrayList<>();

                  List<TripDetail> allActiveTripDetails = TripDetail.findActiveByTripId(s);
                  for (long deletedpk: pksToDelete) {
                    boolean found = false;
                    //see if the pk is a match
                    for (TripDetail td: allActiveTripDetails) {
                      if (td.getBkApiSrcId() != null && td.getBkApiSrcId() == deletedpk) {
                        found = true;
                        tripDetails.add(td);
                      }
                    }
                    if (!found) {
                      BookingRS rs = existingRecsMap.get(deletedpk);
                      if (rs != null && rs.bookingType == ReservationType.FLIGHT.ordinal()) {
                        for (TripDetail td: allActiveTripDetails) {
                          if (td.getDetailtypeid() == ReservationType.FLIGHT && td.getStarttimestamp() == rs.getStartTS() && td.getEndtimestamp() == rs.getEndTS()) {
                            tripDetails.add(td);
                          }
                        }
                      }
                    }
                  }


                  List<TripDetail> tripDetailsToDelete = new ArrayList<>();
                  for (TripDetail td : tripDetails) {
                    if (td.getDetailtypeid() == ReservationType.FLIGHT) {
                      //if this is a flight, we need to check if we need to just remove a passenger to delete the booking completely
                      UmFlightReservation flightReservation = UmReservationUtils.cast(td.getReservation(), UmFlightReservation.class) ;
                      if (flightReservation != null) {
                        List<UmTraveler> passengers = flightReservation.getTravelers();
                        List<UmFlightTraveler> travelerDeleted = new ArrayList<>();
                        HashMap<BookingRS, ReservationsHolder> bookingRSList = new HashMap<>();
                        for (BookingRS rs: toBeDeleted) {
                          if (td.getBkApiSrcId() != null && rs.getPk() == td.getBkApiSrcId()) {
                            bookingRSList.put(rs, TripPublisherHelper.parseJson(rs.getData(),"",0));
                          } else if (rs.bookingType == ReservationType.FLIGHT.ordinal() && rs.getStartTS() == td.getStarttimestamp() && rs.getEndTS() == td.getEndtimestamp()) {
                            //there is an old bug when multiple flight reservations were created for the same flight but with multiple travelers
                            //only 1 flight reservation for the bk_api_rec was linked to the trip_detail record... but the trip_detail record
                            //actually contained all the info from the other bk_api_rec records also
                            bookingRSList.put(rs, TripPublisherHelper.parseJson(rs.getData(),"",0));


                          }
                        }

                        if (passengers != null) {
                          for (UmTraveler passenger : passengers) {
                            UmFlightTraveler traveler = (UmFlightTraveler) passenger;

                            //1 reservation can contain multiple travelers now - we need to know exactly who to remove
                            for (BookingRS bookingRS : bookingRSList.keySet()) {
                              boolean found = false;
                              ReservationsHolder fh = bookingRSList.get(bookingRS);
                              if (fh != null && fh.flights != null && fh.flights.size() == 1 && fh.flights.get(0).passengers != null && fh.flights.get(0).passengers.size() > 0) {
                                for (FlightPassenger fp : fh.flights.get(0).passengers) {
                                  if (traveler.getFamilyName() != null && traveler.getGivenName() != null && fp.underName != null
                                          && fp.underName.familyName.toUpperCase().contains(traveler.getFamilyName().toUpperCase())
                                          && fp.underName.givenName.toUpperCase().contains(traveler.getGivenName().toUpperCase())) {
                                    travelerDeleted.add(traveler);
                                    found = true;
                                    break;
                                  }
                                }
                              }

                              if (!found && traveler.getFamilyName() != null && traveler.getGivenName() != null && bookingRS != null && bookingRS.getMainTraveler() != null
                                      && bookingRS.getMainTraveler().toUpperCase().contains(traveler.getFamilyName().toUpperCase())
                                      && bookingRS.getMainTraveler().toUpperCase().contains(traveler.getGivenName().toUpperCase())) {
                                travelerDeleted.add(traveler);
                              }
                            }
                          }
                        }

                        if (travelerDeleted.size() > 0 && passengers != null) {
                          for (UmFlightTraveler fp: travelerDeleted) {
                            passengers.remove(fp);
                          }
                        }
                        if (passengers != null && passengers.size() > 0) {
                          //remove on the traveler
                          updateTripBooking(td, srcId, pnr);
                          Log.info("APIBookingController: removed traveler from booking for trip for PNR: " + pnr + " from " + BkApiSrc.getName(srcId) + " TripId: " + td.getTripid() + " Details:" + td.getDetailsid());

                        } else {
                          //we delete the whole booking
                          deleteTripBooking(td, srcId, pnr);
                          tripDetailsToDelete.add(td);
                          Log.info("APIBookingController: deleted booking for trip for PNR: " + pnr + " from " + BkApiSrc.getName(srcId) + " TripId: " + td.getTripid() + " Details:" + td.getDetailsid());
                        }
                      }
                    } else {
                      deleteTripBooking(td, srcId, pnr);
                      tripDetailsToDelete.add(td);
                      Log.info("APIBookingController: deleted booking for trip for PNR: " + pnr + " from " + BkApiSrc.getName(srcId) + " TripId: " + td.getTripid() + " Details:" + td.getDetailsid());

                    }
                  }

                  MessengerUpdateHelper.build(tripModel, Account.AID_PUBLISHER)
                          .addRoomsToUpdate(tripDetailsToDelete)
                          .delete();
                  republishTrips.put(tripModel.getTripid(), tripModel);
                }
              }

              if (notesSrcPksToDelete.size() > 0) {
                for (String s : tripIds) {
                  Trip tripModel = Trip.findByPK(s);
                  List<TripNote> tripNotes = TripNote.getNotesByTripIdImportSrc(s, notesSrcPksToDelete);
                  for (TripNote td : tripNotes) {
                    td.setStatus(-1);
                    td.setLastUpdatedTimestamp(System.currentTimeMillis());
                    td.setModifiedBy("System");
                    td.save();
                    StringBuilder msgBody = new StringBuilder();
                    msgBody.append("PNR: " + pnr + " from " + BkApiSrc.getName(srcId) + " updates\n");
                    msgBody.append("Note - " + td.getName() + " deleted\n");
                    final RoomMessage msg = new RoomMessage();
                    msg.userId = "publisher";
                    msg.name = BkApiSrc.getName(srcId) + " Gateway";
                    msg.timestamp = System.currentTimeMillis();
                    msg.message = msgBody.toString();
                    Communicator c = Communicator.Instance();
                    c.sendMsg(tripModel.tripid, tripModel.tripid + "-internal", msg);
                    republishTrips.put(tripModel.getTripid(), tripModel);
                  }
                  BookingNoteController.invalidateCache(tripModel.getTripid());
                }
              }

              if (toBeUpdated.size() > 0) {
                for (String s : tripIds) {
                  Log.info("ApiBookingController:booking Processing Trip for PNR: " + rh.reservationPackage.reservationNumber + " TripId: " + s + " New/Changed:" + toBeUpdated.size());
                  List<Long> pks = new ArrayList<>();
                  Trip trip = Trip.findByPK(s);
                  UserProfile up = UserProfile.findByPK(trip.createdby);
                  VOModeller voModeller = new VOModeller(trip, up, false);
                  StringBuilder msgBody = new StringBuilder();
                  msgBody.append("PNR: " + pnr + " from " + BkApiSrc.getName(srcId) + " updated\n");
                  TripVO tripVO = null;
                  for (BookingRS result : toBeUpdated) {
                    if (result.bookingType != ReservationType.PACKAGE.ordinal()) {
                      pks.add(result.pk);
                      ReservationsHolder rh1 = TripPublisherHelper.parseJson(result.getData(),
                                                                             BkApiSrc.getName(result.getSrcId()),
                                                                             result.pk);
                      if (rh.count() > 0) {
                        if (tripVO == null) {
                          tripVO = new TripVO();
                        }
                        tripVO = rh1.toTripVO(tripVO);
                        tripVO.setSrc(BkApiSrc.getName(result.getSrcId()));
                        tripVO.setSrcId(result.pk);
                        tripVO.setImportSrc(BookingSrc.ImportSrc.API);
                        tripVO.setImportSrcId(result.getSrcRecLocator());
                      }
                    }
                  }
                  try {
                    if  (tripVO != null) {
                      voModeller.buildTripVOModels(tripVO);
                    }
                    //link any trip notes to bookings as needed - 2 scenarios
                    //1. trip booking and trip note are both new in this reservation package
                    //2. trip booking already exists and trip note is new
                    linkNoteToBooking(voModeller, existingRecs, toBeUpdated, trip);

                    voModeller.saveAllModels();
                    //should we update the trip name if different from reservation package
                    if (autoPublishProp.updateTripName && !trip.getName().equals(rh.reservationPackage.name)) {
                      Trip trip1 = Trip.findByPK(trip.getTripid());
                      trip1.setName(rh.reservationPackage.name);
                      trip1.update();
                    }

                    Log.info("APIBookingController: Updated trip for PNR: " + pnr + " from " + BkApiSrc.getName(srcId) + " TripId: " + trip.tripid);
                    BookingAPIMgr.updateAddToTrip(up.userid, pks);
                    //TODO push update msg to communicator
                    msgBody.append(insert + " new bookings \n");
                    msgBody.append(update + " updated bookings \n");
                    final RoomMessage msg = new RoomMessage();
                    msg.userId = "publisher";
                    msg.name = BkApiSrc.getName(srcId) + " Gateway";
                    msg.timestamp = System.currentTimeMillis();
                    msg.message = msgBody.toString();
                    Communicator c = Communicator.Instance();
                    c.sendMsg(trip.tripid, trip.tripid + "-internal", msg);
                    republishTrips.put(trip.getTripid(), trip);
                  } catch (Exception e) {
                    Log.err("APIBookingController: Error updating trip for PNR: " + pnr + " from " + BkApiSrc.getName(srcId) + " TripId: " + trip.tripid);
                    Log.err("APIBookingController: Updating trip failure", e);
                  }
                }
              }



              //if there were changes to trips for companies that have auto publish enabled, we just queue the trips to autopublish
              if (autoPublishProp.autoPublish) {
                if (republishTrips != null && !republishTrips.isEmpty()) {
                  for (Trip trip : republishTrips.values()) {
                    if(autoPublishProp.autoAddTripSummary && autoPublishProp.summaryTemplate != null && !autoPublishProp.summaryTemplate.isEmpty()) {
                      BookingNoteController.processTripSummary(trip, cmpy, a, null, autoPublishProp.summaryTemplate, tripPublisherHelper.getOfferService(), tripPublisherHelper.getUmCityLookup());
                    }
                    if(autoPublishProp.autoAddAfarGuides) {
                      AfarController.autoAddAllAfarGuides(trip, a, tripPublisherHelper.getOfferService());
                    }
                    if(autoPublishProp.autoAddWcities) {
                      OfferController.autoAddAllWcities(trip, a, tripPublisherHelper.getOfferService(), tripPublisherHelper.getOfferProviderRepository());
                    }
                    BookingNoteController.invalidateCache(trip.tripid);
                    tripPublisherHelper.autoPublishTrip(trip, null, cmd);
                  }
                }
                //maybe we need to add the traveler and/or agent to the trip
                if ((rh.reservationPackage.broker != null && rh.reservationPackage.broker.email != null
                     && !rh.reservationPackage.broker.email.isEmpty()) ||
                    (rh.reservationPackage.underName != null && rh.reservationPackage.underName.email != null &&
                     !rh.reservationPackage.underName.email.isEmpty()) ||
                    (rh.reservationPackage.ccPersons != null && rh.reservationPackage.ccPersons.size() > 0)) {
                  for (String tripId: tripIds) {
                    Trip trip = Trip.findByPK(tripId);
                    tripPublisherHelper.addTravelerAgenttoTrip(rh.reservationPackage, trip, cmpy, pnr, srcId, autoPublishProp);
                  }

                } else if (autoPublishProp.controlTravelers) {
                  //see if we need to delete any travelers
                  for (String tripId: tripIds) {
                    Trip trip = Trip.findByPK(tripId);
                    Account agent = Account.findActiveByLegacyId(trip.getCreatedby());
                    tripPublisherHelper.removeTravelers (rh.reservationPackage, trip, agent, autoPublishProp);
                  }
                }


              }
            }
            debugMsg.append("Processed PNR: ");
            debugMsg.append(rh.reservationPackage.reservationNumber);
            debugMsg.append(" from src: ");
            debugMsg.append(srcId);
            debugMsg.append(" Inserted: ");
            debugMsg.append(insert);
            debugMsg.append(" Updated: ");
            debugMsg.append(update);
            debugMsg.append(" Deleted: ");
            debugMsg.append(delete);
            debugMsg.append(" Unchanged: ");
            debugMsg.append(unchanged);
            debugMsg.append(" Failed: ");
            debugMsg.append(failed);
            debugMsg.append(" Duration: ");
            debugMsg.append(sw.getTime());
          } else {
            Log.log(LogLevel.ERROR, "Booking API: Security Exception - Cannot process reservation Package", jsonRequest.toString());
            return status(500, "{Security Exception - Cannot process reservation Package}");
          }
        }
        else {
          Log.log(LogLevel.ERROR, "Booking API: Cannot process reservation Package", jsonRequest.toString());
          return status(500, "{Internal Error - Cannot process reservation Package}");

        }
      }
      catch (Exception e) {
        e.printStackTrace();
        Log.log(LogLevel.ERROR, "Booking API: Cannot process reservation Package " + jsonRequest.toString() + " " + e.getMessage(), e);
        return status(500, "{Internal Error - }" + e.getMessage());
      }

      Log.log(LogLevel.DEBUG, debugMsg.toString());
      return ok();
    } catch (Exception e) {
      Log.log(LogLevel.ERROR, "Booking API: Cannot process API request", request().body().asText());
      Log.err("Booking API: processing API request failure", e);
    }

    return status(500,"{Error processing request}");
  }

  private static String getUniqueFlightIdentifier(FlightReservation f, int srcId) {
    // to ensure uniqueness, we need to use a composite key that consists of
    // 1 PNR, eticket num, passenger name, flight num, depart time
    StringBuilder sb = new StringBuilder();
    if (f.reservationId != null && f.reservationId.trim().length() > 0 && !BkApiSrc.getName(srcId).equals("SABRE") && !BkApiSrc.getName(srcId).equals("CLIENTBASE")) {
      sb.append(f.reservationId);
      sb.append("_");
    } else if (f.reservationNumber != null) {
      sb.append(f.reservationNumber);
      sb.append("_");
    }
    if (f.ticketNumber != null) {
      sb.append(f.ticketNumber);
      sb.append("_");
    }
    if (f.underName != null) {
      if (f.underName.givenName != null) {
        sb.append(f.underName.givenName);
        sb.append("_");
      }
      if (f.underName.familyName != null) {
        sb.append(f.underName.familyName);
        sb.append("_");
      }
    }
    if (f.reservationFor != null) {
      if (f.reservationFor.airline != null && f.reservationFor.airline.iataCode != null) {
        sb.append(f.reservationFor.airline.iataCode);
        sb.append("_");
      }
      if (f.reservationFor.flightNumber != null) {
        sb.append(f.reservationFor.flightNumber);
        sb.append("_");
      }
      if (f.reservationFor.departureTime != null) {
        sb.append(f.reservationFor.departureTime);
        sb.append("_");
      }
    }
    return sb.toString();
  }


   public List<BookingRS> getBookingRS(int srcId,
                                              String srcCmpyId,
                                              String srcUserId,
                                              String cmpyId,
                                              String userId,
                                              ReservationsHolder rh) {
    List<BookingRS> bookings = new ArrayList<>();
    ObjectMapper om = new ObjectMapper();
    BookingRS rp = getPackageBookingRS(srcId,
                                       srcCmpyId,
                                       srcUserId,
                                       cmpyId,
                                       userId,
                                       rh.reservationPackage,
                                       rh.startTimestamp,
                                       rh.endTimestamp);
    try {
      rp.setData(om.writeValueAsString(rh.reservationPackage));
      rp.setHash(Utils.hash(rp.getData()));
      bookings.add(rp);
    } catch (Exception e) {
      e.printStackTrace();
    }


    //flights
    for (FlightReservation f : rh.flights) {
      try {
        BookingRS rs = getBaseBookingRS(srcId,
                                        srcCmpyId,
                                        srcUserId,
                                        cmpyId,
                                        userId,
                                        rh.reservationPackage.reservationNumber,
                                        f.reservationStatus);
        if (f.reservationFor != null && f.reservationFor.arrivalTime != null && f.reservationFor.arrivalTime.getTimestamp() != null) {
          rs.setEndTS(f.reservationFor.arrivalTime.getTimestamp().getTime());
        }
        if (f.reservationFor != null && f.reservationFor.departureTime != null && f.reservationFor.departureTime.getTimestamp() != null) {
          rs.setStartTS(f.reservationFor.departureTime.getTimestamp().getTime());
        }
        rs.setSearchWords(rh.getKeywords(f));
        rs.setBookingType(ReservationType.FLIGHT.ordinal());


        rs.setSrcPk(getUniqueFlightIdentifier(f, srcId));
        rs.setData(om.writeValueAsString(f));
        rs.setMainTraveler(getMainTraveler(f));
        rs.setHash(Utils.hash(rs.getData()));

        bookings.add(rs);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }

    //hotels
    for (LodgingReservation f : rh.hotels) {
      try {
        BookingRS rs = getBaseBookingRS(srcId,
                                        srcCmpyId,
                                        srcUserId,
                                        cmpyId,
                                        userId,
                                        rh.reservationPackage.reservationNumber,
                                        f.reservationStatus);
        if (f.reservationFor != null && f.checkoutDate != null &&  f.checkoutDate.getTimestamp() != null) {
          rs.setEndTS(f.checkoutDate.getTimestamp().getTime());
        }
        if (f.reservationFor != null && f.checkinDate != null && f.checkinDate.getTimestamp() != null) {
          rs.setStartTS(f.checkinDate.getTimestamp().getTime());
        }
        rs.setSearchWords(rh.getKeywords(f));
        rs.setBookingType(ReservationType.HOTEL.ordinal());
        rs.setSrcPk(f.reservationId);
        rs.setData(om.writeValueAsString(f));
        rs.setMainTraveler(getMainTraveler(f));
        rs.setHash(Utils.hash(rs.getData()));

        bookings.add(rs);
      }
      catch (Exception e) {
        e.printStackTrace();
      }

    }

    //cruise
    for (CruiseShipReservation f : rh.cruises) {
      try {
        BookingRS rs = getBaseBookingRS(srcId,
                                        srcCmpyId,
                                        srcUserId,
                                        cmpyId,
                                        userId,
                                        rh.reservationPackage.reservationNumber,
                                        f.reservationStatus);
        if (f.reservationFor != null && f.reservationFor.arrivalTime != null && f.reservationFor.arrivalTime.getTimestamp() != null) {
          rs.setEndTS(f.reservationFor.arrivalTime.getTimestamp().getTime());
        }
        if (f.reservationFor != null && f.reservationFor.departTime != null && f.reservationFor.departTime.getTimestamp() != null) {
          rs.setStartTS(f.reservationFor.departTime.getTimestamp().getTime());
        }
        rs.setSearchWords(rh.getKeywords(f));
        rs.setBookingType(ReservationType.CRUISE.ordinal());
        rs.setSrcPk(f.reservationId);
        rs.setData(om.writeValueAsString(f));
        rs.setMainTraveler(getMainTraveler(f));
        rs.setHash(Utils.hash(rs.getData()));

        bookings.add(rs);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }

    //trains
    for (TrainReservation f : rh.trains) {
      try {
        BookingRS rs = getBaseBookingRS(srcId,
                                        srcCmpyId,
                                        srcUserId,
                                        cmpyId,
                                        userId,
                                        rh.reservationPackage.reservationNumber,
                                        f.reservationStatus);
        if (f.reservationFor != null && f.reservationFor.arrivalTime != null && f.reservationFor.arrivalTime.getTimestamp() != null) {
          rs.setEndTS(f.reservationFor.arrivalTime.getTimestamp().getTime());
        }
        if (f.reservationFor != null && f.reservationFor.departTime != null && f.reservationFor.departTime.getTimestamp() != null) {
          rs.setStartTS(f.reservationFor.departTime.getTimestamp().getTime());
        }
        rs.setSearchWords(rh.getKeywords(f));
        rs.setBookingType(ReservationType.RAIL.ordinal());
        rs.setSrcPk(f.reservationId);
        rs.setData(om.writeValueAsString(f));
        rs.setMainTraveler(getMainTraveler(f));
        rs.setHash(Utils.hash(rs.getData()));

        bookings.add(rs);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }

    //car rental
    for (RentalCarReservation f : rh.cars) {
      try {
        BookingRS rs = getBaseBookingRS(srcId,
                                        srcCmpyId,
                                        srcUserId,
                                        cmpyId,
                                        userId,
                                        rh.reservationPackage.reservationNumber,
                                        f.reservationStatus);
        if (f.dropoffTime != null && f.dropoffTime.getTimestamp() != null) {
          rs.setEndTS(f.dropoffTime.getTimestamp().getTime());
        }
        if (f.pickupTime != null && f.pickupTime.getTimestamp() != null) {
          rs.setStartTS(f.pickupTime.getTimestamp().getTime());
        }
        rs.setSearchWords(rh.getKeywords(f));
        rs.setBookingType(ReservationType.CAR_RENTAL.ordinal());
        rs.setSrcPk(f.reservationId);
        rs.setData(om.writeValueAsString(f));
        rs.setMainTraveler(getMainTraveler(f));
        rs.setHash(Utils.hash(rs.getData()));

        bookings.add(rs);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }

    //restaurants
    for (FoodEstablishmentReservation f : rh.restaurants) {
      try {
        BookingRS rs = getBaseBookingRS(srcId,
                                        srcCmpyId,
                                        srcUserId,
                                        cmpyId,
                                        userId,
                                        rh.reservationPackage.reservationNumber,
                                        f.reservationStatus);
        if (f.endTime != null) {
          rs.setEndTS(f.endTime.getTimestamp().getTime());
        }
        if (f.startTime != null) {
          rs.setStartTS(f.startTime.getTimestamp().getTime());
        }
        rs.setSearchWords(rh.getKeywords(f));
        rs.setBookingType(ReservationType.RESTAURANT.ordinal());
        rs.setSrcPk(f.reservationId);
        rs.setData(om.writeValueAsString(f));
        rs.setMainTraveler(getMainTraveler(f));
        rs.setHash(Utils.hash(rs.getData()));

        bookings.add(rs);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }

    //transfers
    for (TransferReservation f : rh.transfers) {
      try {
        BookingRS rs = getBaseBookingRS(srcId,
                                        srcCmpyId,
                                        srcUserId,
                                        cmpyId,
                                        userId,
                                        rh.reservationPackage.reservationNumber,
                                        f.reservationStatus);
        if (f.reservationFor != null && f.reservationFor.dropoffTime != null) {
          rs.setEndTS(f.reservationFor.dropoffTime.getTimestamp().getTime());
        }
        if (f.reservationFor != null && f.reservationFor.pickupTime != null) {
          rs.setStartTS(f.reservationFor.pickupTime.getTimestamp().getTime());
        }
        rs.setSearchWords(rh.getKeywords(f));
        rs.setBookingType(ReservationType.TRANSPORT.ordinal());
        rs.setSrcPk(f.reservationId);
        rs.setData(om.writeValueAsString(f));
        rs.setMainTraveler(getMainTraveler(f));
        rs.setHash(Utils.hash(rs.getData()));

        bookings.add(rs);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }

    //event
    for (EventReservation f : rh.events) {
      try {
        BookingRS rs = getBaseBookingRS(srcId,
                                        srcCmpyId,
                                        srcUserId,
                                        cmpyId,
                                        userId,
                                        rh.reservationPackage.reservationNumber,
                                        f.reservationStatus);
        if (f.reservationFor != null && f.reservationFor.endDate != null) {
          rs.setEndTS(f.reservationFor.endDate.getTimestamp().getTime());
        }
        if (f.reservationFor != null && f.reservationFor.startDate != null) {
          rs.setStartTS(f.reservationFor.startDate.getTimestamp().getTime());
        }
        rs.setSearchWords(rh.getKeywords(f));
        rs.setBookingType(ReservationType.EVENT.ordinal());
        rs.setSrcPk(f.reservationId);
        rs.setData(om.writeValueAsString(f));
        rs.setMainTraveler(getMainTraveler(f));
        rs.setHash(Utils.hash(rs.getData()));

        bookings.add(rs);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }

    //activity
    for (ActivityReservation f : rh.activities) {
      try {
        BookingRS rs = getBaseBookingRS(srcId,
                                        srcCmpyId,
                                        srcUserId,
                                        cmpyId,
                                        userId,
                                        rh.reservationPackage.reservationNumber,
                                        f.reservationStatus);
        if (f.finishTime != null) {
          rs.setEndTS(f.finishTime.getTimestamp().getTime());
        }
        if (f.startTime != null) {
          rs.setStartTS(f.startTime.getTimestamp().getTime());
        }
        rs.setSearchWords(rh.getKeywords(f));
        rs.setBookingType(ReservationType.ACTIVITY.ordinal());
        rs.setSrcPk(f.reservationId);
        rs.setData(om.writeValueAsString(f));
        rs.setMainTraveler(getMainTraveler(f));
        rs.setHash(Utils.hash(rs.getData()));

        bookings.add(rs);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }

    //note
    for (Note f : rh.notes) {
      try {
        BookingRS rs = getBaseBookingRS(srcId,
                                        srcCmpyId,
                                        srcUserId,
                                        cmpyId,
                                        userId,
                                        rh.reservationPackage.reservationNumber,
                                        f.reservationStatus);

        rs.setBookingType(ReservationType.NOTE.ordinal());
        rs.setSrcPk(f.reservationId);
        rs.setSearchWords(rh.getKeywords(f));
        rs.setData(om.writeValueAsString(f));
        rs.setMainTraveler(getMainTraveler(f));
        rs.setHash(Utils.hash(rs.getData()));

        bookings.add(rs);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }


    return bookings;

  }

  public static String getMainTraveler(Reservation r) {
    StringBuilder sb = new StringBuilder();
    if (r.underName != null) {
      if (r.underName.honorificPrefix != null) {
        sb.append(r.underName.honorificPrefix);
        sb.append(" ");
      }
      if (r.underName.givenName != null) {
        sb.append(r.underName.givenName);
        sb.append(" ");
      }
      if (r.underName.familyName != null) {
        sb.append(r.underName.familyName);
        sb.append(" ");
      }
    }
    return sb.toString();
  }

  public static BookingRS getPackageBookingRS(int srcId,
                                               String srcCmpyId,
                                               String srcUserId,
                                               String cmpyId,
                                               String userId,
                                               ReservationPackage reservationPackage,
                                               long startTimestamp,
                                               long endTimestamp) {
    BookingRS rs = getBaseBookingRS(srcId, srcCmpyId, srcUserId, cmpyId, userId, reservationPackage.reservationNumber, null);
    rs.setEndTS(endTimestamp);
    rs.setStartTS(startTimestamp);
    rs.setBookingType(ReservationType.PACKAGE.ordinal());
    if (reservationPackage.reservationId != null) {
      rs.setSrcPk(reservationPackage.reservationId);
    }
    rs.setData(reservationPackage.toString());
    rs.setMainTraveler(getMainTraveler(reservationPackage));

    return rs;
  }

  public static BookingRS getBaseBookingRS(int srcId,
                                            String srcCmpyId,
                                            String srcUserId,
                                            String cmpyId,
                                            String userId,
                                            String pnr,
                                            String reservationStatus) {
    BookingRS rs = new BookingRS();
    rs.setPk(DBConnectionMgr.getUniqueLongId());
    rs.setSrcId(srcId);
    rs.setSrcCmpyId(srcCmpyId);
    rs.setCreatedBy(userId);
    rs.setAddedToTrip(false);
    rs.setCreatedTS(System.currentTimeMillis());
    rs.setModifiedBy(userId);
    rs.setModifiedTS(System.currentTimeMillis());
    rs.setSearchWords("");
    rs.setSrcCmpyId(srcCmpyId);
    rs.setSrcRecLocator(pnr);
    rs.setSrcPk(pnr);
    rs.setSrcUserId(srcUserId);
    if (reservationStatus != null && reservationStatus.toLowerCase().contains("cancelled")) {
      rs.setState("DELETED");
    } else {
      rs.setState("ACTIVE");
    }
    rs.setuCmpyId(cmpyId);
    rs.setuUserId(userId);
    return rs;
  }



  public static String getMainTripNoteId (String id) {
    if (id != null ) {
      id += "RESERVATION_PACKAGE";
    }

    return id;
  }

  public static void processTripNoteForReservationPackage (ReservationPackage rp, String tripId, String userId) {
    TripNote tripNote = TripNote.getNotesByTripIdImportSrcId(tripId, getMainTripNoteId(rp.reservationNumber));
    if (tripNote != null && (rp.description == null || rp.description.isEmpty())) {
      tripNote.delete();
      BookingNoteController.invalidateCache(tripId);

    } else if (rp.description != null && !rp.description.isEmpty()) {
      rp.description = Sanitizers.FORMATTING.and(Sanitizers.LINKS).and(Sanitizers.STYLES).and(Sanitizers.BLOCKS).and(Sanitizers.TABLES).sanitize(rp.description.replaceAll("\n", "<br/>"));

      if (tripNote != null && !rp.description.equals(tripNote.getDescription())) {
        tripNote.setIntro(rp.description);
        tripNote.setLastUpdatedTimestamp(System.currentTimeMillis());
        tripNote.save();
        BookingNoteController.invalidateCache(tripId);
      } else if (tripNote == null) {
        TripNote tn = new TripNote();
        tn.setNoteId(DBConnectionMgr.getUniqueLongId());
        tn.setTrip(Trip.findByPK(tripId));
        tn.setCreatedBy("API");
        tn.setCreatedTimestamp(System.currentTimeMillis());
        if (rp.name != null) {
          tn.setName(rp.name);
        } else {
          tn.setName("About your trip");
        }
        tn.setIntro(rp.description);
        tn.setRank(0);
        tn.setImportSrc(BookingSrc.ImportSrc.API);
        tn.setImportSrcId(getMainTripNoteId(rp.reservationNumber));
        tn.setImportTs(System.currentTimeMillis());
        tn.setModifiedBy(userId);
        tn.setLastUpdatedTimestamp(System.currentTimeMillis());
        BookingNoteController.invalidateCache(tripId);

        tn.save();
      }
    }
  }

  private String getReservationId (String data) {
    try {
      String s = data.substring(data.indexOf("reservationId")).trim();
      if (s.indexOf(":") > 0) {
        if (s.indexOf(",") > 0)
          s = s.substring(s.indexOf(":") + 1, s.indexOf(",")).trim();
        else
          s = s.substring(s.indexOf(":") + 1);
        if (s.contains("}")) {
          s = s.replace("}","");
        }
        s = s.replace("\"", "");
        return s;
      }
    } catch (Exception e) {

    }
    return "";
  }

  private void linkNoteToBooking (VOModeller voModeller, List<BookingRS> existingRecs, List<BookingRS> toBeUpdated, Trip trip) {
    //additional logic to link any new note to  bookings
    //so we need to find the existing booking based on the original src_pk to match it
    if (voModeller.getNotes() != null && voModeller.getNotes().size() > 0 ) {
      for (TripNote tripNote: voModeller.getNotes()) {
        if (tripNote.getImportSrcId() != null && !tripNote.getImportSrcId().isEmpty() && (tripNote.getTripDetailId() == null || tripNote.getTripDetailId().isEmpty())) {
          boolean done = false;
          //scenario is the booking is already in the system because the reservation package did not contain the note and has been added to a trip
          if (existingRecs != null && existingRecs.size() > 0) {
            for (BookingRS rec : existingRecs) {
              if (rec.bookingType != ReservationType.PACKAGE.ordinal() && rec.bookingType != ReservationType.NOTE
                  .ordinal() && rec.getSrcPk() != null && rec.getSrcPk().equals(tripNote.getImportSrcId())) {
                //find the corresponding booking and link the note as needed
                List<Long> apiPks = new ArrayList<>();
                apiPks.add(rec.getPk());
                List<TripDetail> tripDetails = TripDetail.findByAPIPk(trip.getTripid(), apiPks);
                if (tripDetails != null && tripDetails.size() > 0) {
                  tripNote.setTripDetailId(tripDetails.get(0).getDetailsid());
                }
                done = true;
                break;
              }
            }
          }
          if (!done && toBeUpdated != null && toBeUpdated.size() > 0) {
            //the note and the booking is coming now in an update to the reservation package

            for (BookingRS rs: toBeUpdated) {
              if (rs.bookingType != ReservationType.PACKAGE.ordinal() && rs.bookingType != ReservationType.NOTE
                  .ordinal() && rs.getSrcPk() != null && rs.getSrcPk().equals(tripNote.getImportSrcId())) {
                //find the corresponding booking and link the note as needed
                if (rs.getBookingType() == ReservationType.FLIGHT.ordinal() && voModeller.getFlights() != null) {
                  for (TripDetail td : voModeller.getFlights()) {
                    if (td.getBkApiSrcId() != null && td.getBkApiSrcId().equals(rs.getPk())) {
                      tripNote.setTripDetailId(td.getDetailsid());
                      break;
                    }
                  }
                }
                else if (rs.getBookingType() == ReservationType.HOTEL.ordinal() && voModeller.getHotels() != null) {
                  for (TripDetail td : voModeller.getHotels()) {
                    if (td.getBkApiSrcId() != null && td.getBkApiSrcId().equals(rs.getPk())) {
                      tripNote.setTripDetailId(td.getDetailsid());
                      break;
                    }
                  }
                }
                else if ((rs.getBookingType() == ReservationType.TRANSPORT.ordinal() || rs.getBookingType() == ReservationType.CAR_RENTAL.ordinal()) && voModeller.getTransports() != null) {
                  for (TripDetail td : voModeller.getTransports()) {
                    if (td.getBkApiSrcId() != null && td.getBkApiSrcId().equals(rs.getPk())) {
                      tripNote.setTripDetailId(td.getDetailsid());
                      break;
                    }
                  }
                }
                else if (rs.getBookingType() == ReservationType.ACTIVITY.ordinal() && voModeller.getActivities
                    () != null) {
                  for (TripDetail td : voModeller.getActivities()) {
                    if (td.getBkApiSrcId() != null && td.getBkApiSrcId().equals(rs.getPk())) {
                      tripNote.setTripDetailId(td.getDetailsid());
                      break;
                    }
                  }
                }
                else if (rs.getBookingType() == ReservationType.CRUISE.ordinal() && voModeller.getCruises() != null) {
                  for (TripDetail td : voModeller.getCruises()) {
                    if (td.getBkApiSrcId() != null && td.getBkApiSrcId().equals(rs.getPk())) {
                      tripNote.setTripDetailId(td.getDetailsid());
                      break;
                    }
                  }
                }
                break;
              }
            }
          }
        }
      }
    }
  }

  public void deleteTripBooking (TripDetail td, int srcId, String pnr) {
    td.setStatus(-1);
    td.setLastupdatedtimestamp(System.currentTimeMillis());
    td.setModifiedby("System");
    td.save();
    StringBuilder msgBody = new StringBuilder();
    msgBody.append("PNR: " + pnr + " from " + BkApiSrc.getName(srcId) + " updates\n");
    msgBody.append("Booking - " + td.getName() + " deleted\n");
    final RoomMessage msg = new RoomMessage();
    msg.userId = "publisher";
    msg.name = BkApiSrc.getName(srcId) + " Gateway";
    msg.timestamp = System.currentTimeMillis();
    msg.message = msgBody.toString();
    Communicator.Instance().sendMsg(td.tripid, td.tripid + "-internal", msg);
  }

  public void updateTripBooking (TripDetail td, int srcId, String pnr) {
    td.setLastupdatedtimestamp(System.currentTimeMillis());
    td.setModifiedby("System");
    td.save();
    StringBuilder msgBody = new StringBuilder();
    msgBody.append("PNR: " + pnr + " from " + BkApiSrc.getName(srcId) + " updates\n");
    msgBody.append("Booking - " + td.getName() + " updated\n");
    final RoomMessage msg = new RoomMessage();
    msg.userId = "publisher";
    msg.name = BkApiSrc.getName(srcId) + " Gateway";
    msg.timestamp = System.currentTimeMillis();
    msg.message = msgBody.toString();
    Communicator.Instance().sendMsg(td.tripid, td.tripid + "-internal", msg);
  }

}
