package com.mapped.publisher.actions;

import com.mapped.publisher.js.BaseJsonResponse;
import com.mapped.publisher.utils.Log;
import com.umapped.api.schema.types.ReturnCode;
import io.jsonwebtoken.*;
import models.publisher.AccountAuth;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.time.StopWatch;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Base64;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Relevant documentation:
 * https://www.playframework.com/documentation/2.5.x/JavaActionsComposition
 * <p>
 * Created by surge on 2016-04-05.
 */
public class AccountJwtAuth
    extends Action.Simple {
  @Override
  public CompletionStage<Result> call(Http.Context ctx) {
    StopWatch sw = new StopWatch();
    sw.start();
    try {
      Claims claims = parseFromContext(ctx);
      ctx.args.put("uid", claims.get("uid", Long.class));
    }
    catch (Exception e) {
      Log.err("JWT Validation Failure: " + e.getMessage()); //Only message printed, no stack plz
      return CompletableFuture.completedFuture(BaseJsonResponse.codeResponse(ReturnCode.AUTH_ACCOUNT_FAIL));
    }
    sw.stop();
    Log.debug("JWT Authorization took: " + sw.getTime() + "ms");
    return delegate.call(ctx);
  }

  /**
   * Helper that can be used outside of this annotation if header needs to be parsed again
   *
   * @param ctx
   * @return
   * @throws Exception
   */
  public static Claims parseFromContext(Http.Context ctx)
      throws Exception {
    String jwtToken = ctx.request().getHeader("Authorization");
    if (jwtToken == null || !jwtToken.contains("Bearer ")) {
      Log.err("Faulty token received: " + jwtToken);
      throw new Exception("Invalid Authorization header in the Request");
    }
    jwtToken = jwtToken.substring(7); //Skipping "Bearer "

    SigningKeyResolver keyResolver = new SigningKeyResolver() {
      @Override
      public Key resolveSigningKey(JwsHeader header, Claims claims) {
        //Getting account ID
        String sub = claims.getSubject();
        if (sub.equals("uid")) { //Account JWT
          Long        uid        = claims.get("uid", Long.class);
          //StopWatch sw1 = new StopWatch();
          //sw1.start();
          AccountAuth aa         = AccountAuth.getByType(uid, AccountAuth.AAuthType.JWT);
          //sw1.stop();
          //Log.info("JWT Auth DB request took: " + sw1.getTime() + "ms");
          byte[]      decodedKey = Base64.getDecoder().decode(aa.getValue());
          return new SecretKeySpec(decodedKey, 0, decodedKey.length, SignatureAlgorithm.HS256.getValue());
        }
        return null;
      }

      @Override
      public Key resolveSigningKey(JwsHeader header, String plaintext) {
        throw new NotImplementedException("JWT with plaintext payload is not supported");
      }
    };

    return Jwts.parser()
               .requireSubject("uid")
               .setSigningKeyResolver(keyResolver)
               .parseClaimsJws(jwtToken)
               .getBody();
  }
}
