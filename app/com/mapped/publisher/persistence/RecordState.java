package com.mapped.publisher.persistence;
import com.avaje.ebean.annotation.EnumValue;
import com.mapped.publisher.common.APPConstants;

/**
 * Created by surge on 2014-10-20.
 */
public enum RecordState {
  @EnumValue("ACTIVE")
  ACTIVE,
  @EnumValue("PENDING")
  PENDING,
  @EnumValue("EXPIRED")
  EXPIRED,
  @EnumValue("DELETED")
  DELETED,
  @EnumValue("LOCKED")
  LOCKED;

  public static RecordState fromStatus(int status) {
    switch (status) {
      case APPConstants.STATUS_DELETED:
        return  DELETED;
      case APPConstants.STATUS_PENDING:
        return PENDING;
      case APPConstants.STATUS_ACCOUNT_LOCKED:
        return LOCKED;
      case APPConstants.STATUS_ACTIVE:
      default:
        return ACTIVE;
    }
  }
}
