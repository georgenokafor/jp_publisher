package com.mapped.publisher.view;

/**
 * Created by twong on 15-10-15.
 */
public class ApiCmpyTokenView {
  public long pk;
  public String srcCmpyId;
  public String srcName;
  public int srcId;
  public String token;
  public String tokenType;
  public long expiryTS;
  public String tag;
}
