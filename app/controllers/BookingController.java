package controllers;

import com.amazonaws.services.s3.model.S3Object;
import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.audit.AuditModuleType;
import com.mapped.publisher.audit.event.*;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.FileForm;
import com.mapped.publisher.form.booking.*;
import com.mapped.publisher.js.*;
import com.mapped.publisher.parse.extractor.booking.BookingExtractor;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.persistence.*;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.persistence.communicator.MessengerUpdateHelper;
import com.mapped.publisher.persistence.redis.RedisKeys;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.*;
import com.mapped.publisher.view.*;
import com.mapped.publisher.view.migration.HotelMigrationViewUtil;
import com.umapped.api.schema.common.RequestMessageJson;
import com.umapped.api.schema.common.ResponseMessageJson;
import com.umapped.api.schema.local.BookingFileJson;
import com.umapped.api.schema.types.Address;
import com.umapped.api.schema.types.AddressType;
import com.umapped.api.schema.types.Operation;
import com.umapped.api.schema.types.ReturnCode;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.helpers.TripReservationsHelper;
import com.umapped.persistence.reservation.UmPostalAddress;
import com.umapped.persistence.reservation.UmRate;
import com.umapped.persistence.reservation.UmReservation;
import com.umapped.persistence.reservation.UmTraveler;
import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;
import com.umapped.persistence.reservation.accommodation.UmAccommodationTraveler;
import com.umapped.persistence.reservation.accommodation.UmLodgingBusiness;
import com.umapped.persistence.reservation.activity.UmActivityReservation;
import com.umapped.persistence.reservation.cruise.UmCruiseReservation;
import com.umapped.persistence.reservation.cruise.UmCruiseShip;
import com.umapped.persistence.reservation.cruise.UmCruiseTraveler;
import com.umapped.persistence.reservation.enums.UmRateType;
import com.umapped.persistence.reservation.flight.UmFlight;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.flight.UmFlightTraveler;
import com.umapped.persistence.reservation.transfer.UmTransferReservation;

import models.publisher.*;
import models.publisher.utils.reservation.migration.UmAccommodationReservationBuilder;

import models.publisher.utils.reservation.migration.UmFlightReservationBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import static controllers.PoiController.getMergedPoi;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-19
 * Time: 4:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class BookingController
    extends Controller {
  private static final boolean DEBUG = false;
  private static final char NBSP =  '\u00a0';

  @Inject
  private RedisMgr    redis;
  @Inject
  private FormFactory formFactory;

  private UmFlightReservationBuilder flightReservationBuilder = new UmFlightReservationBuilder();

  private UmAccommodationReservationBuilder accoumodationReservationBuilder = new UmAccommodationReservationBuilder();
  private HotelMigrationViewUtil hotelMigrationViewUtil = new HotelMigrationViewUtil();
  
  @With({Authenticated.class, Authorized.class})
  public Result createFlight() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    //bind html form to form bean
    Form<FormFlightBooking> form = formFactory.form(FormFlightBooking.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    FormFlightBooking       flightInfo     = form.get();

    flash(SessionConstants.SESSION_PARAM_ACTIVE_TAB, TabType.FLIGHTS.name());

    if (flightInfo.inFlightNumber == null || flightInfo.inFlightNumber.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Flight Number is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));

    }
    else if (flightInfo.inFlightDate == null || flightInfo.inFlightDate.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Departure Date is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));

    }
    else if (flightInfo.inFlightTime == null || flightInfo.inFlightTime.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Departure Time is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));

    }
    else if (flightInfo.inFlightDepartAirport == null ||
             flightInfo.inFlightDepartAirport.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Departure Airport is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));

    }
    else if (flightInfo.inFlightArrivalAirport == null ||
             flightInfo.inFlightArrivalAirport.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Arrival Airport is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));

    }

    Trip tripModel = Trip.findByPK(flightInfo.inTripId);
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to edit this trip";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();

      TripDetail detail = null;
      if (flightInfo.inTripDetailId != null) {
        detail = TripDetail.find.byId(flightInfo.inTripDetailId);
        if (detail == null || !detail.tripid.equals(flightInfo.inTripId)) {
          BaseView baseView = new BaseView();
          baseView.message = "You are not authorized to edit this trip";
          return ok(views.html.common.message.render(baseView));
        }

        if (accessLevel == SecurityMgr.AccessLevel.APPEND && !detail.createdby.equals(sessionMgr.getUserId())){
          BaseView baseView = new BaseView();
          baseView.message = "Permission Error - You are not authorised to modify bookings created by other users";
          return ok(views.html.common.message.render(baseView));
        }
      }

      if (detail == null) {
        detail = new TripDetail();
        detail.setDetailsid(Utils.getUniqueId());
        detail.setCreatedtimestamp(System.currentTimeMillis());
        detail.setCreatedby(sessionMgr.getUserId());
      }
     
      // set the data into the reservation from comment
      // TODO: (Wei) needs to migrate to new UI component to get the structured data
      // instead of parsing unstrucutre data in the future
      UmFlightReservation reservation = cast(detail.getReservation(), UmFlightReservation.class);
      if (reservation == null) {
        reservation = new UmFlightReservation();
        detail.setReservation(reservation);
      }
      
      reservation.getFlight().departureTerminal = flightInfo.inFlightDepartTerminal;
      reservation.getFlight().arrivalTerminal = flightInfo.inFlightArrivalTerminal;
      
      List<UmTraveler> travelers = new ArrayList<>();
      if (flightInfo.passengers != null) {
        flightInfo.passengers.stream()
        .forEach((p) -> {
          if (!p.isEmpty()) {
            UmFlightTraveler t = new UmFlightTraveler();
            t.setFamilyName(StringUtils.trim(p.lastName));
            t.setGivenName(StringUtils.trim(p.firstName));
            t.setSeat(StringUtils.trim(p.seat));
            t.setTicketNumber(StringUtils.trim(p.eticket));
            t.getSeatClass().seatCategory = StringUtils.trim(p.fClass);
            t.getProgram().membershipNumber = StringUtils.trim(p.frequentFlyer);
            t.setMeal(StringUtils.trim(p.meal));
            travelers.add(t);
          }
        });
      }

      reservation.setTravelers(travelers);
      reservation.setNotesPlainText(flightInfo.inFlightNote);

      List<UmRate> rates = new ArrayList<>();
      if(flightInfo.rates != null) {
        flightInfo.rates.stream().forEach((p) -> {
          if(!p.isEmpty()) {
            UmRate rate = new UmRate();
            rate.setName(StringUtils.trim(p.rateName));
            rate.setDescription(StringUtils.trim(p.rateDescription));
            rate.setPrice(p.ratePrice);
            rate.setDisplayName(StringUtils.trim(UmRateType.valueOf(p.rateName).getValue()));
            rates.add(rate);
          }
        });
      }
      reservation.setRates(rates);

      reservation.setTaxes(flightInfo.inTaxes);
      reservation.setCurrency(flightInfo.inCurrency);
      reservation.setTotal(flightInfo.inTotal);
      reservation.setSubtotal(flightInfo.inSubtotal);
      reservation.setFees(flightInfo.inFees);
      reservation.setCancellationPolicy(flightInfo.inFlightCancellation);
      reservation.setImportant(flightInfo.inFlightImportant);
      reservation.setStartLocationTimeZone(flightInfo.inFlightStartTimezone);
      reservation.setFinishLocationTimeZone(flightInfo.inFlightFinishTimezone);
      
      detail.setBookingnumber(flightInfo.inFlightBooking);
      detail.setDetailtypeid(ReservationType.FLIGHT);
      detail.setTripid(flightInfo.inTripId);
      detail.setStatus(APPConstants.STATUS_ACTIVE);
      detail.setStarttimestamp(Utils.getMilliSecs(flightInfo.inFlightDate + " " +
                                                  flightInfo.inFlightTime));

      List<PoiRS> pois = buildTripDetailPois(tripModel, detail,
                                             flightInfo.inFlightAirlineId, flightInfo.inFlightNumber,
                                             flightInfo.inFlightDepartAirportId,  flightInfo.inFlightDepartAirport,
                                             flightInfo.inFlightArrivalAirportId, flightInfo.inFlightArrivalAirport,
                                             sessionMgr.getCredentials());

      PoiRS poiMain = null;

      if (flightInfo.inFlightNumber != null) {
        flightInfo.inFlightNumber = flightInfo.inFlightNumber.replace(NBSP,' ');

        detail.setName(flightInfo.inFlightNumber);
        Pair<String, String> airlineFlightNum = Utils.parseFlightNumber(flightInfo.inFlightNumber);
        if (airlineFlightNum != null) {
          String airlineCode = airlineFlightNum.getLeft();

          int pt = PoiTypeInfo.Instance().byName("Airline").getId();

          if (detail.getPoiId() != null) {
            poiMain = PoiController.findAndMergeByCode(airlineCode, pt, detail.getPoiCmpyId());
          } else {
            Set<Integer> userCmpies = new HashSet<>();
            userCmpies.add(sessionMgr.getCredentials().getCmpyIdInt());
            poiMain = PoiController.findAndMergeByCode(airlineCode, pt, userCmpies);
            if (poiMain != null) {
              poiMain.setCmpyId((Integer) userCmpies.toArray()[0]);
            }
          }
        }
      }

      if (poiMain != null) {
        if (detail.getPoiId() == null || poiMain.getId() != detail.getPoiId()) {
          detail.setPoiCmpyId(poiMain.getCmpyId());
          detail.setPoiId(poiMain.getId());
        }

        if ((detail.getName() == null || detail.getName().equals(flightInfo.inFlightNumber)) &&
            poiMain.getName() != null &&
            poiMain.getName().trim().length() > 0) {
          detail.setName(poiMain.getName());
        }
      } else {
        //reset main poi
        detail.setPoiCmpyId(null);
        detail.setPoiId(null);
      }

      if (detail.starttimestamp == -1) {
        Ebean.rollbackTransaction();

        //check to make sure dates are good
        BaseView baseView = new BaseView();
        baseView.message = "Arrival Airport is a required field - please correct and resubmit";
        return ok(views.html.common.message.render(baseView));
      }

      if (flightInfo.inFlightArrivalDate != null && flightInfo.inFlightArrivalDate.length() > 0) {
        if (flightInfo.inFlightTime != null && flightInfo.inFlightTime.length() > 0) {
          detail.setEndtimestamp(Utils.getMilliSecs(flightInfo.inFlightArrivalDate + " " +
                                                    flightInfo.inFlightArrivalTime));
        }
        else {
          detail.setEndtimestamp(Utils.getMilliSecs(flightInfo.inFlightArrivalDate));
        }

      }
      else {
        detail.setEndtimestamp(Utils.getMilliSecs(flightInfo.inFlightDate + " " +
                                                  flightInfo.inFlightTime));
      }

      detail.setLastupdatedtimestamp(System.currentTimeMillis());
      detail.setModifiedby(sessionMgr.getUserId());
      
      UmFlight flight = reservation.getFlight();

      flight.flightNumber = flightInfo.inFlightNumber;
      //update cross date flight
      updateCrossDateTag(flightInfo, detail);

      detail.save();
      processBookingFiles(detail, flightInfo);

      //this is an existing flight - we need to check if the tail number has been changed so that we can untrack the existing one
      //if the company had flight tracking - delete the tracking record
      if (tripModel != null && tripModel.status == APPConstants.STATUS_PUBLISHED && 
          flight != null && !StringUtils.equals(flight.flightNumber, flightInfo.inFlightNumber)) {
        Company c = Company.find.byId(tripModel.cmpyid);
        if (c != null && c.isFlighttracking()) {
          //delete the tracking record
          FlightAlertBooking.deleteByBookingId(detail.detailsid);
        }
      }

      Ebean.commitTransaction();


      //Creating Communicator room to discuss
      Communicator fc = Communicator.Instance();
      MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId())
                           .addRoomToUpdate(detail)
                           .update();

      //Create audit log
      audit:
      {
        TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_FLIGHT,
                                             (flightInfo.inTripDetailId == null)?AuditActionType.ADD:AuditActionType.MODIFY,
                                             AuditActorType.WEB_USER)
                                .withTrip(tripModel)
                                .withCmpyid(tripModel.cmpyid)
                                .withUserid(sessionMgr.getUserId());

        ((AuditTripBookingFlight) ta.getDetails()).withFlightNumber(reservation.getFlight().flightNumber)
                                           .withBookingId(detail.getDetailsid())
                                           .withDeparture(detail.getStarttimestamp());
        ta.save();
      }

      if (flightInfo.inTripDetailId == null) {
        flash(SessionConstants.SESSION_PARAM_MSG, flightInfo.inFlightNumber + " Added");
      }
      else {
        flash(SessionConstants.SESSION_PARAM_MSG, flightInfo.inFlightNumber + " Updated");
      }
      flash(SessionConstants.SESSION_PARAM_SCROLL_TO_DIV, detail.detailsid);
      BookingController.invalidateCache(detail.getTripid(), detail.getDetailsid());

      return redirect(routes.BookingController.bookings(tripModel.tripid, new TabType.Bound(TabType.ITINERARY), null));
    }
    catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "A system error has occurred";
      return ok(views.html.common.message.render(baseView));
    }
  }

  public static void invalidateCache(String tripId, String tripDetailId) {
    //If only trip ID is specified - wipe the whole trip
    if(tripId == null) {
      return;
    }

    if(tripDetailId == null) {
      CacheMgr.getRedis().delete(RedisKeys.H_TRIP, tripId, RedisMgr.WriteMode.BLOCKING);
      return; //We deleted everything
    }

    //Deleting all fields associated with TRIP key
    CacheMgr.getRedis().hashDel(RedisKeys.H_TRIP, tripId,
                                RedisMgr.WriteMode.UNSAFE,
                                RedisKeys.HF_TRIP_BOOKING_DETAIL_VIEW.getKey(tripDetailId),
                                RedisKeys.HF_TRIP_RESERVATION_PACKAGE.getKey(),
                                RedisKeys.HF_TRIP_MAP.getKey(),
                                RedisKeys.HF_TRIP_MAP_MOBILE.getKey(),
                                RedisKeys.HF_TRIP_CALENDAR_EVENTS.getKey());
  }

  @With({Authenticated.class, Authorized.class})
  public Result newFiles() {
    SessionMgr sessionMgr = new SessionMgr(session());
    UserProfile userProfile = UserProfile.findByPK(sessionMgr.getUserId());

    //bind html form to form bean //workaround for multiple select bug in play form
    List<String> inFiles = new ArrayList<String>();
    Map<String, String> newData = new HashMap<String, String>();
    Map<String, String[]> urlFormEncoded = play.mvc.Controller.request().body().asFormUrlEncoded();
    if (urlFormEncoded != null) {
      for (String key : urlFormEncoded.keySet()) {
        String[] value = urlFormEncoded.get(key);
        if (value.length == 1) {
          if (key.equalsIgnoreCase("inFiles")) {
            inFiles.add(value[0]);
          }
          newData.put(key, value[0]);
        }
        else if (value.length > 1) {
          if (key.equalsIgnoreCase("inFiles")) {
            for (int i = 0; i < value.length; i++) {
              if (!inFiles.contains(value[i])) {
                inFiles.add(value[i]);
              }
            }
          }
          for (int i = 0; i < value.length; i++) {
            newData.put(key + "[" + i + "]", value[i]);
          }

        }
      }
    }
    // bind to the MyEntity form object

    Form<FileForm> fileForm = formFactory.form(FileForm.class).bind(newData);
    FileForm fileInfoForm = fileForm.bindFromRequest().get();
    fileInfoForm.setInFiles(inFiles);

    if (fileInfoForm.getInNoteId() != null) {
      TripNote note = TripNote.find.byId(fileInfoForm.getInNoteId());
      if (note != null) {
        try {
          Ebean.beginTransaction();


          if (fileInfoForm.getInFiles() != null && fileInfoForm.getInFiles().size() > 0) {
            for (String attachId : fileInfoForm.getInFiles()) {
              TripAttachment link = TripAttachment.find.byId(attachId);
              if (link != null) {
                TripNoteAttach tripNoteAttach = TripNoteAttach.findByNoteIdFileUrl(fileInfoForm.getInNoteId(), link.getFileurl());
                if (tripNoteAttach == null) {
                  TripNoteAttach attach = TripNoteAttach.build(note, userProfile.getUserid());
                  attach.setFileInfo(link.getFileInfo());
                  attach.setAttachType(PageAttachType.FILE_LINK);
                  attach.setAttachUrl(link.getFileurl());
                  attach.setName(link.filename);
                  attach.setAttachName(link.getOrigfilename());
                  attach.setVersion(link.getVersion());
                  int maxRow = TripNoteAttach.maxPageRankByNoteId(note.getNoteId(), PageAttachType.FILE_LINK.getStrVal());
                  attach.setRank(++maxRow);
                  attach.save();
                }
              }
            }
          }


          Ebean.commitTransaction();
          flash(SessionConstants.SESSION_PARAM_MSG, "Files added successfully.");
          return redirect(routes.BookingNoteController.getStructuredNote(note.getTrip().getTripid(), note.getNoteId(), note.getTripDetailId(), null, note.getType().name()));

        } catch (Exception e) {
          e.printStackTrace();
          Ebean.rollbackTransaction();
          BaseView baseView = new BaseView();
          baseView.message = "System Error - please resubmit";
          return ok(views.html.common.message.render(baseView));
        }
      }
    } else {
      if (fileInfoForm.getInDetailId() != null) {
        TripDetail detail = TripDetail.findByPK(fileInfoForm.getInDetailId());
        if (detail != null) {
          try {
            Ebean.beginTransaction();


            if (fileInfoForm.getInFiles() != null && fileInfoForm.getInFiles().size() > 0) {
              for (String attachId : fileInfoForm.getInFiles()) {
                TripDetailAttach.TripDetailAttachId tdaId = new TripDetailAttach.TripDetailAttachId();
                tdaId.setDetailsId(detail.getDetailsid());
                tdaId.setAttachId(attachId);
                //List<TripDetailAttach> tripDetailAttach = TripDetailAttach.findActiveByDetailIdandAttachId(detail.getDetailsid(), userId);
                TripDetailAttach tripDetailAttach = TripDetailAttach.find.byId(tdaId);
                if (tripDetailAttach == null) {
                  TripAttachment link = TripAttachment.find.byId(attachId);
                  if (link != null) {
                    TripDetailAttach attach = new TripDetailAttach();
                    attach.setDetailAttachId(detail.getDetailsid(), attachId);
                    //attach.getDetailAttachId().setAttachId(userId);
                    //attach.getDetailAttachId().setDetailsId(detail.getDetailsid());
                    attach.setVersion(link.getVersion());
                    int maxRow = TripDetailAttach.maxPageRankByDetailId(detail.getDetailsid());
                    attach.setRank(++maxRow);
                    attach.save();
                  }
                }
              }
            }


            Ebean.commitTransaction();
            flash(SessionConstants.SESSION_PARAM_MSG, "Files added successfully.");
            return redirect(routes.BookingController.bookings(detail.getTripid(), new TabType.Bound(TabType.fromBookingType(detail.getDetailtypeid())), detail.detailsid));

          } catch (Exception e) {
            e.printStackTrace();
            Ebean.rollbackTransaction();
            BaseView baseView = new BaseView();
            baseView.message = "System Error - please resubmit";
            return ok(views.html.common.message.render(baseView));
          }
        }
      }
    }

    BaseView baseView = new BaseView();
    baseView.message = "Unauthorized Action";
    return ok(views.html.common.message.render(baseView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteFile(String detailId, String fileId, String tripId, String attachName) {
    SessionMgr sessionMgr = new SessionMgr(session());
    TripDetail detail = TripDetail.findByPK(detailId);



    if (detailId == null || fileId == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    Trip tripModel = Trip.findByPK(tripId);
    TripDetailAttach.TripDetailAttachId tdaId = new TripDetailAttach.TripDetailAttachId();
    tdaId.setDetailsId(detailId);
    tdaId.setAttachId(fileId);
    TripDetailAttach detailAttach = TripDetailAttach.find.byId(tdaId);


    if (detailAttach == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    TripDetailAttach.deleteFile(detailId, fileId);

    invalidateCache(tripId, null); //Invalidating whole trip cache
    flash(SessionConstants.SESSION_PARAM_MSG, attachName + " deleted successfully");

    return redirect(routes.BookingController.bookings(tripId, new TabType.Bound(TabType.fromBookingType(detail.getDetailtypeid())), detailId));
  }



  @With({Authenticated.class, Authorized.class})
  public Result bookings(String inTripId, TabType.Bound inActiveTab, String inTripDetailsId) {
    try {
      SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
      Credentials cred = sessionMgr.getCredentials();
      String tripId = inTripId;

      if (tripId == null) {
        tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID); //TODO: Serguei: Probably can be removed completely
      }

      if (tripId != null) {
        Trip trip = Trip.find.byId(tripId);
        //Anyone with read access can access all bookings
        SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
        if (accessLevel.ge(SecurityMgr.AccessLevel.READ)) {
          sessionMgr.setParam(SessionConstants.SESSION_PARAM_TRIPID, tripId);
          Company cmpy = Company.find.byId(trip.cmpyid);

          TripBookingView view = new TripBookingView();
          view.lang = ctx().lang();
          view.accessLevel = accessLevel;
          view.loggedInUserId = sessionMgr.getUserId();
          //check to see if we need to display the add all WCities guide automatically
          //right now mostly for flight center
          if (cmpy.getTag() != null && cmpy.getTag().contains(APPConstants.CMPY_TAG_WCITIES_FILTER_FLIGHTCENTER)) {
            view.addWCitiesGuideBtn = true;
          }
          view.setupPoiView(sessionMgr);
          String msg = flash(SessionConstants.SESSION_PARAM_MSG);
          if (msg != null && msg.length() > 0) {
            view.message = msg;
          }
          view.addCapabilities(sessionMgr.getCredentials().getCapabilities());

          //check if the user has any moblized external domains
          if (cmpy.getName().contains("Indagare")) {
            //indigare
            view.addMobilizedDomain("www.indagare.com", "Indagare");
          } else {
            for(SysContentSite s : MobilizerController.getContentSites()) {
              view.addMobilizedDomain(s.getDomain(), s.getName());
            }
          }
          final DynamicForm form = formFactory.form().bindFromRequest();
          String scrollDiv = form.get("inScrollTo");
          if (scrollDiv == null) {
            scrollDiv = flash(SessionConstants.SESSION_PARAM_SCROLL_TO_DIV);
          }
          if (scrollDiv != null && scrollDiv.length() > 0) {
            view.scrollToId = scrollDiv;
          }

          //check for active tab
          if (inActiveTab == null) {
            view.activeTab = TabType.fromString(flash(SessionConstants.SESSION_PARAM_ACTIVE_TAB));
          } else {
            view.activeTab = inActiveTab.value();
          }
          if (view.activeTab == TabType.ITINERARY) {
            view.activeTab = inActiveTab.value();
          }

          //check for modal url - in case we need to pop up a modal
          view.modalUrl = null;
          String modalUrl = session(SessionConstants.SESSION_MODAL_URL);
          if (modalUrl != null && modalUrl.length() > 0) {
            view.modalUrl = modalUrl;
            session().remove(SessionConstants.SESSION_MODAL_URL);
          }

          view.tripId = trip.tripid;
          view.tripCmpyId = trip.cmpyid;
          //fix to support collaboration
          if  (SecurityMgr.canAccessCmpy(cmpy, sessionMgr)) {
            view.tripCmpyIdInt = cmpy.getCmpyId();
            view.isCollablorator = false;
            List<EmailToken> tkns = EmailToken.getCompanyActiveTokens(cmpy.getCmpyid());
            if (tkns != null && tkns.size() == 1) {
              EmailToken tkn = tkns.get(0);
              if (ConfigMgr.getInstance().isProd()) {
                view.tripCmpyEmail = tkn.getToken() + "@api.umapped.com";
              }
              else {
                view.tripCmpyEmail = tkn.getToken() + "@testapi.umapped.com";
              }
            }
          } else {
            //as a collaborator - pick another cmpy
            if (cred.hasCompany()) {
              view.tripCmpyIdInt = cred.getCmpyIdInt();
            }
            view.isCollablorator = true;
          }
          view.display12Hr = cmpy.display12hrClock();

          if (trip.starttimestamp > 0) {
            view.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
            view.tripStartDate = Utils.formatDateControlYYYY(trip.starttimestamp);
          }

          if (trip.endtimestamp > 0) {
            view.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
            view.tripEndDate = Utils.getDateString(trip.endtimestamp);
          }
          view.tripName = trip.name;
          view.tripStatus = trip.status;

          /* Create list of allowed parsers */
          List<BookingExtractor.Parsers> parsers = (List<BookingExtractor.Parsers>) CacheMgr.get(APPConstants.CACHE_USER_API_PARSERS + sessionMgr.getAccountId());

          if (parsers == null) {
            view.parsers = new ArrayList<>();
                /* First adding open (public) parsers */
            for (BookingExtractor.Parsers parser : BookingExtractor.Parsers.values()) {
              if (parser.isOpen()) {
                view.parsers.add(parser);
              }
            }
            List<CmpyApiParser> apiParsers = CmpyApiParser.findAllUserAPIs(sessionMgr.getCredentials());
            if (apiParsers != null) {
              for (CmpyApiParser apiParser : apiParsers) {
                BookingExtractor.Parsers parser = BookingExtractor.Parsers.fromClass(apiParser.parser);
                if (parser != null && !view.parsers.contains(parser)) {
                  view.parsers.add(parser);
                }
              }
            }
            CacheMgr.set(APPConstants.CACHE_USER_API_PARSERS + sessionMgr.getAccountId(), view.parsers);
          }
          else {
            view.parsers = parsers;
          }

          Collections.sort(view.parsers, new Comparator<BookingExtractor.Parsers>() {
            public int compare(BookingExtractor.Parsers v1, BookingExtractor.Parsers v2) {
              return v1.name().compareTo(v2.name());
            }
          });

          //get any notes
          view.notes = BookingNoteController.getAllTripNotesView(trip, sessionMgr, true);

          //check for edit
          if (view.activeTab == TabType.UPLOADS || view.activeTab == TabType.RECLOCATORS) {
            //redirect to the upload or rec locator tab
            //if tab is upload, try to see if we get a default parser specifed
            view.defaultParser = form.get("inPdfParser");
            return ok(views.html.trip.bookings.bookingUpload.render(view));

          } else if (inTripDetailsId != null && inTripDetailsId.length() > 0) {
            //we are editing a booking - so only get that one
            TripDetail tripDetail = TripDetail.find.byId(inTripDetailsId);
            if (tripDetail != null) {
              if (tripDetail.status == APPConstants.STATUS_ACTIVE && tripDetail.tripid.equals(tripId)) {
                view.bookingDetail = BookingController.buildBookingView(tripId, tripDetail, false, accessLevel);
                if (view.bookingDetail != null) {
                  view.activeTab = TabType.fromBookingType(view.bookingDetail.detailsTypeId);
                  if (tripDetail.detailtypeid == ReservationType.HOTEL) {
                    // set the reservation back to comments:
                    view.bookingDetail.origComments = hotelMigrationViewUtil.reservationToComment(cast(tripDetail.getReservation(), UmAccommodationReservation.class));
                  }
                  //if this is a cruise, let's find all the cruise stops
                  if (tripDetail.detailtypeid == ReservationType.CRUISE) {
                    LocalDate startDateTime = new LocalDate(tripDetail.starttimestamp);
                    LocalDate endDateTime = new LocalDate(tripDetail.endtimestamp);
                    endDateTime = endDateTime.plusDays(1);
                    List<TripDetail> cruiseStops = TripDetail.findCruiseStopsByDateRange(trip.tripid,
                                                                                         startDateTime.toDate().getTime(),
                                                                                         endDateTime.toDate().getTime());

                    if (cruiseStops != null) {
                      for (TripDetail td: cruiseStops) {
                        TripBookingDetailView stopView = BookingController.buildBookingView(tripId, td, false,
                                                                                            accessLevel);
                        view.activities.add(stopView);
                      }
                    }
                    //this is a cruise - let's see if the user belongs to a consortia and see if we can see amenities
                    //hardcoded to virtuoso for now
                    CruiseAmenities amenity = null;
                    int cmpyToUSe = getCmpyIdToUse(sessionMgr.getCredentials(), trip);
                    List<ConsortiumCompany> cmpies = ConsortiumCompany.findByCmpyId(cmpyToUSe);
                    ConsortiumCompany consortiumCompany = null;
                    if (cmpies != null && cmpies.size() > 0) {
                      consortiumCompany = cmpies.get(0);
                    }
                    if (tripDetail.getStarttimestamp() > 0 && tripDetail.getEndtimestamp() > 0 && consortiumCompany != null) {
                      DateTime s = (new DateTime(tripDetail.getStarttimestamp())).withTimeAtStartOfDay();
                      DateTime e = (new DateTime(tripDetail.getEndtimestamp())).withTimeAtStartOfDay();
                      if (tripDetail.getPoiId() != null && tripDetail.getPoiId() > 0) {
                        amenity = CruiseAmenities.getAmenitiesPoiId_StartTimeEndTime(consortiumCompany.getConsId(),
                                                                                     tripDetail.getPoiId(),
                                                                                     s.getMillis(),
                                                                                     e.getMillis());
                      }
                      else {
                        amenity = CruiseAmenities.getAmenitiesShipName_StartTimeEndTime(consortiumCompany.getConsId(),tripDetail.getName(), s.getMillis(), e.getMillis());
                      }
                      if (amenity != null) {

                        view.bookingDetail.consortiumAmenities = amenity.getAmenities().replaceAll("<br.*./>","\n");
                        Consortium consortium = Consortium.find.byId(consortiumCompany.getConsId());
                        view.bookingDetail.consortiumName = consortium.getName();
                      }
                    }

                  }
                  // add the attachment list
                  view.tripAttachments = WebItineraryController.getTripAttachments(view.tripId,
                                                                                   sessionMgr.getTimezoneOffsetMins());
                }
                else { //Want to make sure that trip detail is null
                  tripDetail = null;
                }
              }
            }

            if (tripDetail == null || (tripDetail != null && tripDetail.status == APPConstants.STATUS_DELETED)) {
              view.message = "The requested booking has been previously deleted or does not exist.";
            }


            //return the edit page
            return ok(views.html.trip.bookings.editBooking.render(view));

          } else if (view.activeTab == TabType.FLIGHTS || view.activeTab == TabType.HOTELS ||
                     view.activeTab == TabType.CRUISES || view.activeTab == TabType.TRANSPORTS ||
                     view.activeTab == TabType.ACTIVITIES || view.activeTab == TabType.NOTES ) {
            // go to the edit page of ths booking
            //return the edit page
            return ok(views.html.trip.bookings.editBooking.render(view));
          }

          List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
          if (tripDetails != null) {
              for (TripDetail tripDetail : tripDetails) {
              TripBookingDetailView bookingDetails = BookingController.buildBookingView(tripId, tripDetail, true, accessLevel);
              view.addBookingDetailView(tripDetail.getDetailtypeid(), bookingDetails);
            }

            //handle cross timezone booking
            if (view != null && view.flights != null && view.flights.size() > 0) {
              view.handleCrosTimezoneFlights();
            }


            return ok(views.html.trip.bookings.booking.render(view));
          }
        }
      }
      BaseView view = new BaseView();
      view.message = "System Error";
      return ok(views.html.common.message.render(view));
    }
    catch (Exception e) {
      e.printStackTrace();
      BaseView view = new BaseView();
      view.message = "System Error";
      return ok(views.html.common.message.render(view));
    }
  }

  /** Minimal dataset for showing bookings in the calendar, can be expanded to include additional detail */
  @With({Authenticated.class, Authorized.class})
  public Result bookingsJson(String tripId, Long timeStart, Long timeEnd) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Trip tripModel = Trip.findByPK(tripId);
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.READ)) {
      BaseJsonResponse bj = new BaseJsonResponse(ReturnCode.AUTH_TRIP_FAIL);
      return ok(bj.toJson());
    }

    Company company = Company.find.byId(tripModel.getCmpyid());
    TripBookingsJson tbj = new TripBookingsJson(tripModel.getTripid(), tripModel.getName());
    tbj.cmpyId = company.cmpyid;
    tbj.cmpyName = company.name;
    tbj.canEdit = accessLevel.ge(SecurityMgr.AccessLevel.APPEND);

    List<TripDetail> details = TripDetail.findActiveByTripIdandDateRange(tripModel.getTripid(), timeStart, timeEnd);
    for (TripDetail detail : details) {
      BookingDetailsJson jsonDetails = new BookingDetailsJson();
      jsonDetails.type = detail.getDetailtypeid();
      jsonDetails.rootType = detail.getDetailtypeid().getRootLevelType();
      jsonDetails.tripDetailId = detail.getDetailsid();
      jsonDetails.cssClassName = ViewUtils.getClassTypeForBookingType(detail.getDetailtypeid());
      jsonDetails.timeStart = Utils.getISO8601Timestamp(detail.starttimestamp);
      jsonDetails.timeEnd = Utils.getISO8601Timestamp(detail.endtimestamp);
      jsonDetails.typeLabel = detail.getDetailtypeid().getLabel();
      jsonDetails.detailsEditUrl = controllers.routes.BookingController.bookings(tripModel.getTripid(),
                                                                     new TabType.Bound(TabType.fromBookingType(
                                                                         detail.getDetailtypeid())),
                                                                     detail.getDetailsid()).url();

      switch (detail.detailtypeid.getRootLevelType()) {
        case FLIGHT:
          // (Wei) Migrate to using reservation
          // FlightBooking flightBooking = FlightBooking.findByPK(detail.getDetailsid());
          UmFlightReservation flight = cast(detail.getReservation(), UmFlightReservation.class);
          if (flight != null && flight.getFlight() != null) {
            jsonDetails.name = flight.getFlight().flightNumber;
          }
          break;
        case CRUISE:
          UmCruiseReservation reservation = cast(detail.getReservation(), UmCruiseReservation.class);
          if (reservation != null) {
            jsonDetails.name = reservation.getCruise().name;
          }
          break;
        case HOTEL:
          UmAccommodationReservation hotel = cast(detail.getReservation(), UmAccommodationReservation.class);
          if (hotel != null) {
            jsonDetails.name = hotel.getAccommodation().name;
          }
          break;
        case ACTIVITY:
          UmActivityReservation activity = cast(detail.getReservation(), UmActivityReservation.class);
          if (activity != null) {
            jsonDetails.name = activity.getActivity().name;
          }
          break;
        case TRANSPORT:
          UmTransferReservation transport = cast(detail.getReservation(), UmTransferReservation.class);
          if (transport != null) {
            jsonDetails.name = transport.getTransfer().getProvider().name;
          }
          break;
      }
      tbj.addBookingDetails(jsonDetails);
    }

    return ok(tbj.toJson());
  }

  public static TripBookingDetailView buildBookingView(String tripId, TripDetail tripDetail, boolean printDateFormat) {
    return buildBookingView(tripId, tripDetail, printDateFormat, SecurityMgr.AccessLevel.NONE);
  }

  public static TripBookingDetailView buildBookingView(String tripId, TripDetail tripDetail, boolean printDateFormat,
                                                       SecurityMgr.AccessLevel tripAccessLevel) {
    String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);
    TripBookingDetailView bookingDetails = null;
    if (!DEBUG) {
      Optional<TripBookingDetailView> optBookingDetail = CacheMgr.getRedis()
                                    .hashGetBin(RedisKeys.H_TRIP,
                                                tripId,
                                                RedisKeys.HF_TRIP_BOOKING_DETAIL_VIEW.getKey(tripDetail.getDetailsid()),
                                                TripBookingDetailView.class);
      if (optBookingDetail.isPresent()) {
        bookingDetails = optBookingDetail.get();
      }
    }

    // TODO: (Wei) Get the new reservation and set it to view
    // consider to refactor it hide it from view
    UmReservation reservation = tripDetail.getReservation();
    //Get all 3 POI objects here - to get fresh data
    PoiRS poiMain   = getMergedPoi(tripDetail.getPoiId(), tripDetail.getPoiCmpyId());
    PoiRS poiStart  = getMergedPoi(tripDetail.getLocStartPoiId(), tripDetail.getLocStartPoiCmpyId());
    PoiRS poiFinish = getMergedPoi(tripDetail.getLocFinishPoiId(), tripDetail.getLocFinishPoiCmpyId());

    if (bookingDetails != null) {
      if (tripDetail.getTag() != null && tripDetail.getTag().contains(APPConstants.TRIP_TAG_CROSS_DATE_FLIGHT)) {
        bookingDetails.afterCrossDateFlight = true;
      }
      bookingDetails.buildViewInfo(tripDetail, printDateFormat, tripAccessLevel);
      //bookingDetails.poiMain        = poiMain;
      //bookingDetails.poiStart       = poiStart;
      //bookingDetails.poiFinish      = poiFinish;
      //bookingDetails.reservation    = reservation;
    }
    else {
      if(tripDetail.detailtypeid == ReservationType.FLIGHT) {
        bookingDetails = new UmFlightReservationView();
      }
      else if(tripDetail.detailtypeid == ReservationType.HOTEL) {
        bookingDetails = new UmAccommodationReservationView();
      }
      else if(tripDetail.detailtypeid == ReservationType.CRUISE) {
        bookingDetails = new UmCruiseReservationView();
      }
      else if(tripDetail.detailtypeid == ReservationType.ACTIVITY || tripDetail.detailtypeid == ReservationType.CRUISE_STOP || tripDetail.detailtypeid == ReservationType.RESTAURANT
              || tripDetail.detailtypeid == ReservationType.TOUR || tripDetail.detailtypeid == ReservationType.EVENT || tripDetail.detailtypeid == ReservationType.SHORE_EXCURSION
              || tripDetail.detailtypeid == ReservationType.SKI_LESSONS || tripDetail.detailtypeid == ReservationType.SKI_LIFT || tripDetail.detailtypeid == ReservationType.SKI_RENTALS) {
        bookingDetails = new UmActivityReservationView();
      }
      else if(tripDetail.detailtypeid == ReservationType.TRANSPORT || tripDetail.detailtypeid == ReservationType.RAIL || tripDetail.detailtypeid == ReservationType.CAR_RENTAL
              || tripDetail.detailtypeid == ReservationType.PRIVATE_TRANSFER || tripDetail.detailtypeid == ReservationType.TAXI || tripDetail.detailtypeid == ReservationType.BUS
              || tripDetail.detailtypeid == ReservationType.CAR_DROPOFF || tripDetail.detailtypeid == ReservationType.PUBLIC_TRANSPORT || tripDetail.detailtypeid == ReservationType.FERRY) {
        bookingDetails = new UmTransferReservationView();
      }
      else {
        bookingDetails = new TripBookingDetailView();
      }
      bookingDetails.buildViewInfo(tripId, tripDetail, printDateFormat, tripAccessLevel);
      if (tripDetail.getTag() != null && tripDetail.getTag().contains(APPConstants.TRIP_TAG_CROSS_DATE_FLIGHT)) {
        bookingDetails.afterCrossDateFlight = true;
      }

      /*bookingDetails.poiMain        = poiMain;
      bookingDetails.poiStart       = poiStart;
      bookingDetails.poiFinish      = poiFinish;
      bookingDetails.tripId         = tripId;
      bookingDetails.createdById    = tripDetail.createdby;
      bookingDetails.reservation    = reservation;

      if (Utils.isInteger(bookingDetails.createdById)) {
        try {
          Long acid = Long.parseLong(bookingDetails.createdById);
          Account a = Account.find.byId(acid);
          bookingDetails.createdByName = (a != null)?a.getFullName():bookingDetails.createdById;
          bookingDetails.accessLevel = SecurityMgr.AccessLevel.TRAVELER;
        } catch (Exception e) {} //Ignoring
      }
      else {
        Account a = Account.findByLegacyId(bookingDetails.createdById);
        bookingDetails.createdByName = (a != null)?a.getFullName():bookingDetails.createdById;
        bookingDetails.accessLevel = tripAccessLevel;
      }

      bookingDetails.status         = tripDetail.status;
      bookingDetails.detailsId      = tripDetail.detailsid;
      bookingDetails.detailsTypeId  = tripDetail.detailtypeid;
      bookingDetails.tripType       = tripDetail.triptype;
      bookingDetails.bookingNumber  = tripDetail.bookingnumber;
      bookingDetails.recordLocator  = tripDetail.getRecordLocator();
      // TODO: (Wei)   Use reservation.notePlainTet when everything is migrated
      // Currently it is done by each type below
      
      if (tripDetail.getReservation() == null) {
        bookingDetails.comments       = Utils.escapeHtml(tripDetail.comments);
        bookingDetails.origComments   = tripDetail.comments;
      } else {
        bookingDetails.comments       = Utils.escapeHtml(tripDetail.getReservation().getNotesPlainText());
        bookingDetails.origComments   = tripDetail.getReservation().getNotesPlainText();
      }
      
      bookingDetails.passengerCount = tripDetail.getPassengerCount();
      bookingDetails.name           = tripDetail.getName();
      bookingDetails.providerName   = tripDetail.getName(); //TODO: Serguei: Cleanup eventually
      bookingDetails.rank           = tripDetail.getRank();
      bookingDetails.createdTimestampMs = tripDetail.getCreatedtimestamp();
      bookingDetails.importSrc      = tripDetail.getImportSrc();
      bookingDetails.importSrcId    = tripDetail.getImportSrcId();

      if (tripDetail.starttimestamp != null && tripDetail.starttimestamp > 0) {
        bookingDetails.startDatePrint = Utils.formatDatePrint(tripDetail.starttimestamp);
        bookingDetails.startDate = Utils.getDateString(tripDetail.starttimestamp);
        bookingDetails.startTime = Utils.getTimeString(tripDetail.starttimestamp);
        bookingDetails.startDateMs = tripDetail.starttimestamp;
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(tripDetail.starttimestamp);
        bookingDetails.startYear = String.valueOf(cal.get(Calendar.YEAR));
        bookingDetails.startMonth = String.valueOf(cal.get(Calendar.MONTH));
        bookingDetails.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        bookingDetails.startHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
        bookingDetails.startMin = String.valueOf(cal.get(Calendar.MINUTE));
      }
      if (tripDetail.endtimestamp != null &&
          tripDetail.starttimestamp > 0 && tripDetail.endtimestamp > 0) {
        if ((tripDetail.detailtypeid != null && tripDetail.detailtypeid == ReservationType.FLIGHT) || !tripDetail.endtimestamp.equals(tripDetail.starttimestamp)) {
          bookingDetails.endDate = Utils.getDateString(tripDetail.endtimestamp);
          bookingDetails.endDatePrint = Utils.formatDatePrint(tripDetail.endtimestamp);
          bookingDetails.endTime = Utils.getTimeString(tripDetail.endtimestamp);
          bookingDetails.endDateMs = tripDetail.endtimestamp;
          Calendar cal = Calendar.getInstance();
          cal.setTimeInMillis(tripDetail.endtimestamp);
          bookingDetails.endYear = String.valueOf(cal.get(Calendar.YEAR));
          bookingDetails.endMonth = String.valueOf(cal.get(Calendar.MONTH));
          bookingDetails.endDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
          bookingDetails.endHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
          bookingDetails.endMin = String.valueOf(cal.get(Calendar.MINUTE));
        }
      }*/

      //All trip types are explicitly mentioned below, no need for root type
      switch (tripDetail.detailtypeid) {
        case FLIGHT:
          /*if (poiMain != null && poiMain.data != null) {
            if (poiMain.data.getAdditionalProperties().containsKey(APPConstants.BAGGAGE_URL) ) {
              bookingDetails.baggageUrl = poiMain.data.getAdditionalProperties().get(APPConstants.BAGGAGE_URL);
            }
            if (poiMain.data.getAdditionalProperties().containsKey(APPConstants.CHECKIN_URL) ) {
              bookingDetails.checkinUrl = poiMain.data.getAdditionalProperties().get(APPConstants.CHECKIN_URL);
            }
          }
          
          UmFlightReservation fligtReservation = cast(reservation, UmFlightReservation.class);
          
          // For the new reservation structure
          if (fligtReservation != null) {
            if (fligtReservation.getFlight() != null) {
              bookingDetails.flightId = fligtReservation.getFlight().flightNumber;
              bookingDetails.name = fligtReservation.getFlight().flightNumber; //In case of flights flight ID is the name and poi name is airline
            }
          }

          if (bookingDetails.poiStart != null) {
            bookingDetails.departureAirport     = bookingDetails.poiStart.getName();
            bookingDetails.departureAirportId   = bookingDetails.poiStart.getId();
            bookingDetails.departureAirportCode = bookingDetails.poiStart.getCode();
            Address address = bookingDetails.poiStart.getMainAddress();
            if (address != null) {
              bookingDetails.departCity = address.getLocality();
              String cc = (address.getCountryCode() != null) ? address.getCountryCode() : poiStart.countryCode;
              if (cc != null && CountriesInfo.Instance()
                                             .byAlpha3(cc) != null) {
                bookingDetails.departCountry = CountriesInfo.Instance()
                                                            .byAlpha3(cc)
                                                            .getName(CountriesInfo.LangCode.EN);
              }
            }
          } else {
            bookingDetails.departureAirport = tripDetail.getLocStartName();
          }

          if (bookingDetails.poiFinish != null) {
            bookingDetails.arrivalAirport     = bookingDetails.poiFinish.getName();
            bookingDetails.arrivalAirportId   = bookingDetails.poiFinish.getId();
            bookingDetails.arrivalAirportCode = bookingDetails.poiFinish.getCode();
            Address address = bookingDetails.poiFinish.getMainAddress();
            if (address != null) {
              bookingDetails.arriveCity = address.getLocality();
              String cc = (address.getCountryCode() != null) ? address.getCountryCode() : poiFinish.countryCode;
              if (cc != null && CountriesInfo.Instance()
                                             .byAlpha3(cc) != null) {
                bookingDetails.arriveCountry = CountriesInfo.Instance()
                                                            .byAlpha3(cc)
                                                            .getName(CountriesInfo.LangCode.EN);
              }
            }
          } else {
            bookingDetails.arrivalAirport = tripDetail.getLocFinishName();
          }
          if (poiMain != null && poiMain.getName() != null) {
            bookingDetails.providerName = poiMain.getName();
          }*/

          break;
        case CRUISE:
          /*UmCruiseReservation cruise = cast(tripDetail.getReservation(), UmCruiseReservation.class);
          if (cruise != null) {
            bookingDetails.cruise = new TripBookingDetailView.Cruise();
            UmCruiseTraveler traveler = cruise.getCruiseTraveler();
            if (traveler != null) {
              bookingDetails.cruise.bedding       = traveler.getBedding();
              bookingDetails.cruise.cabinNumber   = traveler.getCabinNumber();
              bookingDetails.cruise.category      = traveler.getCategory();
              bookingDetails.cruise.deck          = traveler.getDeckNumber();
              bookingDetails.cruise.meal          = traveler.getDiningPlan();
            }
            
            bookingDetails.cruise.cmpyName      = cruise.getCruise().getProvider().name;

            if (poiMain != null) {
              bookingDetails.cruise.name        = poiMain.getName();

            } else {
              bookingDetails.cruise.name        = (tripDetail.getName() != null)?tripDetail.getName():cruise.getCruise().name;
            }
            bookingDetails.name                 = bookingDetails.cruise.name;
            bookingDetails.providerName         = bookingDetails.cruise.name;

            if (bookingDetails.poiStart != null) {
              bookingDetails.cruise.portDepart = bookingDetails.poiStart.getName();
            } else {
              bookingDetails.cruise.portDepart = tripDetail.getLocStartName();
            }

            if (bookingDetails.poiFinish != null) {
              bookingDetails.cruise.portArrive = bookingDetails.poiFinish.getName();
            } else {
              bookingDetails.cruise.portArrive = tripDetail.getLocFinishName();
            }
          }
          else {
            Log.err("Database records missing no cruise details for trip " + tripId);
            return null;
          }*/
          break;
        case HOTEL:
          /*UmAccommodationReservation hotelReservation = cast(tripDetail.getReservation(), UmAccommodationReservation.class);
          if (hotelReservation != null) {
            if (bookingDetails.poiMain == null && bookingDetails.providerName == null) {
              bookingDetails.providerName = hotelReservation.getAccommodation().name;
              bookingDetails.name = hotelReservation.getAccommodation().name; 
            }
          }
          else {
            Log.err("Database records missing no hotel details for trip " + tripId);
            return null;
          }*/
          break;

        /** ACTIVITIES **/
        case ACTIVITY:
        case TOUR:
        case EVENT:
        case RESTAURANT:
        case CRUISE_STOP:
        case SHORE_EXCURSION:
        case SKI_LESSONS:
        case SKI_LIFT:
        case SKI_RENTALS:
          /*UmActivityReservation activity = cast(tripDetail.getReservation(), UmActivityReservation.class);
          if (activity != null) {
            if (bookingDetails.poiMain == null || bookingDetails.providerName == null) {
              bookingDetails.providerName = activity.getActivity().name;
              bookingDetails.name = activity.getActivity().name;
            }


            bookingDetails.contact = activity.getActivity().getOrganizedBy().contactPoint;
            if (activity.getActivity().umDocumentId != null) {
              Destination destination = Destination.find.byId(activity.getActivity().umDocumentId);
              bookingDetails.destination = destination.getName();
              bookingDetails.destinationId = destination.destinationid;
              bookingDetails.destinationUrl = hostUrl + routes.DestinationController.loadPdf() + "?inDestId=" + destination.destinationid + "&inTripId=" + tripId ;
            }
            if (poiStart != null && poiStart.getCode() != null) {
              bookingDetails.arrivalAirportCode = poiStart.getCode();
            }
          }
          else {
            Log.err("Database records missing no activity details for trip " + tripId);
            return null;
          }*/
          break;
        case TRANSPORT:
        case CAR_RENTAL:
        case RAIL:
        case PRIVATE_TRANSFER:
        case TAXI:
        case BUS:
        case PUBLIC_TRANSPORT:
        case CAR_DROPOFF:
        case FERRY:
          /*UmTransferReservation transport = cast(tripDetail.getReservation(), UmTransferReservation.class);
          if (transport != null) {
            if (bookingDetails.poiMain == null || bookingDetails.providerName == null) {
              bookingDetails.providerName = transport.getTransfer().getProvider().name;
              bookingDetails.name = transport.getTransfer().getProvider().name;
            }
            bookingDetails.contact = transport.getTransfer().getProvider().contactPoint;
          }
          else {
            Log.err("Database records missing no transport details for trip " + tripId);
            return null;
          }*/
          break;
      }

      CacheMgr.getRedis().hashSet(RedisKeys.H_TRIP,
                                  tripId,
                                  RedisKeys.HF_TRIP_BOOKING_DETAIL_VIEW.getKey(tripDetail.getDetailsid()),
                                  bookingDetails, RedisMgr.WriteMode.FAST);
    }

    //Updating poi information that might have changed. Not updating flights as they have custom naming consisting
    //of poi name and flight number.
    /*if (bookingDetails.poiMain != null &&
        bookingDetails.poiMain.getIdLong() != null) {
      bookingDetails.poiId              = bookingDetails.poiMain.getId();
      if (bookingDetails.poiMain.getName() != null) {
        bookingDetails.providerName = bookingDetails.poiMain.getName();
      }
      if (printDateFormat) {
        bookingDetails.providerComments = Utils.escapeHtml(bookingDetails.poiMain.data.getDesc());
      }
      else {
        bookingDetails.providerComments = bookingDetails.poiMain.data.getDesc();
      }
    } else if (bookingDetails.poiMain == null && tripDetail.detailtypeid == ReservationType.FLIGHT) {
      bookingDetails.providerName = null;
    }
    else if (tripDetail.getName() != null){
      bookingDetails.providerName       = tripDetail.getName();
    }*/


    //This is a kludge to support a historical feature for flight booking- will be fixed by PUB-249
    /*
    historically, there was a depart loc and start loc in the booking view
    for flights, depart loc was the departure airport and start loc was the arrival airport
    to limit the changes for all the maps and plots, when building the view for flights, we need to flip the departure and arrival airport i.e. depart airport becomes finish location and arrive airport becomes start location
    if not, we need to change all the maps and the mobile since the start loc used to be the arrival airport and is now the departure airport
     */
    /*if (tripDetail.detailtypeid == ReservationType.FLIGHT) {
      if (bookingDetails.poiStart != null) { //Updating data from probably up to date poi object
        bookingDetails.locFinishName     = bookingDetails.poiStart.getName();
        bookingDetails.locFinishPoi      = bookingDetails.poiStart.getId();
        bookingDetails.locFinishLat     = bookingDetails.poiStart.locLat;
        bookingDetails.locFinishLong     = bookingDetails.poiStart.locLong;
      } else {
        bookingDetails.locFinishName    = tripDetail.getLocStartName();
        bookingDetails.locFinishLat    = tripDetail.getLocStartLat();
        bookingDetails.locFinishLong     = tripDetail.getLocStartLong();
      }


      if (bookingDetails.poiFinish != null) {
        bookingDetails.locStartName     = bookingDetails.poiFinish.getName();
        bookingDetails.locStartPoi      = bookingDetails.poiFinish.getId();
        bookingDetails.locStartLat      = bookingDetails.poiFinish.locLat;
        bookingDetails.locStartLong     = bookingDetails.poiFinish.locLong;
      } else {
        bookingDetails.locStartName     = tripDetail.getLocFinishName();
        bookingDetails.locStartLat      = tripDetail.getLocFinishLat();
        bookingDetails.locStartLong     = tripDetail.getLocFinishLong();
      }

    } else {
      if (bookingDetails.poiStart != null) { //Updating data from probably up to date poi object
        //hack for cruise stops imported - we would have a poiid for gps coordinates but the name might not be 100 match for accuracy reason
        if (bookingDetails.detailsTypeId == ReservationType.CRUISE_STOP || bookingDetails.detailsTypeId == ReservationType.TOUR) {
          bookingDetails.locStartName = tripDetail.getLocStartName();
        } else {
          //bookingDetails.locStartName = bookingDetails.poiStart.getName();
        }
        //bookingDetails.locStartPoi      = bookingDetails.poiStart.getId();
        //bookingDetails.locStartLat      = bookingDetails.poiStart.locLat;
        //bookingDetails.locStartLong     = bookingDetails.poiStart.locLong;
      } else {
        //bookingDetails.locStartName     = tripDetail.getLocStartName();
        //bookingDetails.locStartLat      = tripDetail.getLocStartLat();
        //bookingDetails.locStartLong     = tripDetail.getLocStartLong();
      }


      /*if (bookingDetails.poiFinish != null) {
        bookingDetails.locFinishName     = bookingDetails.poiFinish.getName();
        bookingDetails.locFinishPoi      = bookingDetails.poiFinish.getId();
        bookingDetails.locFinishLat      = bookingDetails.poiFinish.locLat;
        bookingDetails.locFinishLong     = bookingDetails.poiFinish.locLong;
      } else {
        bookingDetails.locFinishName     = tripDetail.getLocFinishName();
        bookingDetails.locFinishLat      = tripDetail.getLocFinishLat();
        bookingDetails.locFinishLong     = tripDetail.getLocFinishLong();
      }
    }*/

    //Add Files to Flight booking
    //bookingDetails.files = getFilesForBooking(tripDetail.detailsid);


    //Extracting other misc data from whatever poi information is currently available (mainly for older records)
    /*PoiRS prs = null;
    bookingDetails.showPhotosBtn = false;
    if (bookingDetails.poiMain != null  && bookingDetails.poiMain.getIdLong() != null) {
      prs = bookingDetails.poiMain;

      TripDetail.ExtraDetails ed = tripDetail.getExtraDetails();

      bookingDetails.photos = getPhotosForBooking(tripDetail, prs.getId(), prs.getCmpyId(), true);
      if (bookingDetails.photos != null && bookingDetails.photos.size() > 0) {
        bookingDetails.showPhotosBtn = true;
      }
      if (ed != null && ed.showPhotos != null && !ed.showPhotos && bookingDetails.photos != null) {
        bookingDetails.photos.clear();
      }
    } else if (bookingDetails.poiStart != null) {
      prs = bookingDetails.poiStart;
    } else if (bookingDetails.poiFinish != null) {
      prs = bookingDetails.poiFinish;
    }

    if (prs != null) {
      Address a = prs.getMainAddress();
      if (a != null) {
        bookingDetails.formattedAddr = prs.getMainAddressString();
        bookingDetails.providerAddr1 = a.getStreetAddress();
        if (bookingDetails.providerAddr1 != null && bookingDetails.formattedAddr.contains(bookingDetails.providerAddr1) && bookingDetails.formattedAddr.length() > bookingDetails.providerAddr1.length()) {
           bookingDetails.providerAddr2 = bookingDetails.formattedAddr.substring(bookingDetails.providerAddr1.length()).trim();
          if (bookingDetails.providerAddr2.startsWith(",")) {
            bookingDetails.providerAddr2 = bookingDetails.providerAddr2.replaceFirst(",", "").trim();
          }

        }
        bookingDetails.city = a.getLocality();
        bookingDetails.state = a.getRegion();
        bookingDetails.zip = a.getPostalCode();

        String cc = (a.getCountryCode() != null) ? a.getCountryCode() : prs.countryCode;

        if (cc != null && !cc.equals("UNK")) {
          bookingDetails.country = CountriesInfo.Instance().byAlpha3(cc).getName(CountriesInfo.LangCode.EN);
        }
      }

      if (prs.data.getPhoneNumbers().size() > 0) {
        bookingDetails.providerPhone = prs.getMainPhone().getNumber();
      }

      if (prs.data.getUrls().size() > 0) {
        bookingDetails.providerWeb = prs.getMainUrl();
      }

      if (prs.data.getEmails().size() > 0) {
        bookingDetails.providerEmail = prs.getMainEmail().getAddress();
      }
    }*/
    return bookingDetails;
  }

  @With({Authenticated.class, Authorized.class})
  public Result createHotel() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<FormHotelBooking> form = formFactory.form(FormHotelBooking.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    FormHotelBooking       hotelInfo     = form.get();
    flash(SessionConstants.SESSION_PARAM_ACTIVE_TAB, TabType.HOTELS.name());

    Trip tripModel = Trip.findByPK(hotelInfo.inTripId);
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to edit this trip";
      return ok(views.html.common.message.render(baseView));
    }

    if (hotelInfo.inHotelName == null || hotelInfo.inHotelName.trim().length() == 0) {
      //check to make sure dates are good
      BaseView baseView = new BaseView();
      baseView.message = "Hotel Name is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    else if (hotelInfo.inHotelCheckInDate == null || hotelInfo.inHotelCheckInDate.trim().length() == 0) {
      //check to make sure dates are good
      BaseView baseView = new BaseView();
      baseView.message = "Check-In Date is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    else if (hotelInfo.inHotelCheckOutDate == null || hotelInfo.inHotelCheckOutDate.trim().length() == 0) {
      //check to make sure dates are good
      BaseView baseView = new BaseView();
      baseView.message = "Check-Out Date is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    else if (hotelInfo.inHotelCheckOutTime == null || hotelInfo.inHotelCheckOutTime.trim().length() == 0) {
      //check to make sure dates are good
      BaseView baseView = new BaseView();
      baseView.message = "Check-Out Time is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    else if (hotelInfo.inHotelCheckInTime == null || hotelInfo.inHotelCheckInTime.trim().length() == 0) {
      //check to make sure dates are good
      BaseView baseView = new BaseView();
      baseView.message = "Check-In Time is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();
      TripDetail detail = null;
      if (hotelInfo.inTripDetailId != null) {
        detail = TripDetail.find.byId(hotelInfo.inTripDetailId);
        if (detail == null || !detail.tripid.equals(hotelInfo.inTripId)) {
          BaseView baseView = new BaseView();
          baseView.message = "You are not authorized to edit this trip";
          return ok(views.html.common.message.render(baseView));
        }

        if (accessLevel == SecurityMgr.AccessLevel.APPEND && !detail.createdby.equals(sessionMgr.getUserId())){
          BaseView baseView = new BaseView();
          baseView.message = "Permission Error - You are not authorized to edit bookings created by other users";
          return ok(views.html.common.message.render(baseView));
        }
      }

      if (detail == null) {
        detail = new TripDetail();
        detail.setDetailsid(Utils.getUniqueId());
        detail.setCreatedtimestamp(System.currentTimeMillis());
        detail.setCreatedby(sessionMgr.getUserId());
      }
      
      detail.setBookingnumber(hotelInfo.inHotelBooking);
      detail.setDetailtypeid(ReservationType.HOTEL);
      detail.setTripid(hotelInfo.inTripId);
      detail.setStatus(APPConstants.STATUS_ACTIVE);

      detail.setStarttimestamp(Utils.getMilliSecs(hotelInfo.inHotelCheckInDate + " " +
                                                  hotelInfo.inHotelCheckInTime));
      detail.setEndtimestamp(Utils.getMilliSecs(hotelInfo.inHotelCheckOutDate + " " +
                                                hotelInfo.inHotelCheckOutTime));

      List<PoiRS> pois = buildTripDetailPois(tripModel, detail,
                                             hotelInfo.inHotelProviderId, hotelInfo.inHotelName,
                                             null, null,
                                             null, null,
                                             sessionMgr.getCredentials());

      PoiRS poiMain   = pois.get(0); //Main vendor

      //update cross date flight
      updateCrossDateTag(hotelInfo, detail);
      if (detail.starttimestamp == -1) {
        Ebean.rollbackTransaction();

        BaseView baseView = new BaseView();
        baseView.message = "Check-In Date/time is a required field - please correct and resubmit";
        return ok(views.html.common.message.render(baseView));
      }
      else if (detail.endtimestamp == -1) {
        Ebean.rollbackTransaction();

        BaseView baseView = new BaseView();
        baseView.message = "Check-Out Date/Time is a required field - please correct and resubmit";
        return ok(views.html.common.message.render(baseView));
      }

      UmAccommodationReservation reservation = cast(detail.getReservation(), UmAccommodationReservation.class);
      if (reservation == null) {
        reservation = new UmAccommodationReservation();
        detail.setReservation(reservation);
      }

      List<UmTraveler> travelers = new ArrayList<>();
      if (hotelInfo.passengers != null) {
        hotelInfo.passengers.stream()
                .forEach((p) -> {
                  if (!p.isEmpty()) {
                    UmAccommodationTraveler t = new UmAccommodationTraveler();
                    t.setFamilyName(StringUtils.trim(p.lastName));
                    t.setGivenName(StringUtils.trim(p.firstName));
                    t.setRoomType(StringUtils.trim(p.roomType));
                    t.setAmenities(StringUtils.trim(p.amenities));
                    t.setConfirmStatus(StringUtils.trim(p.status));
                    t.getProgram().membershipNumber = StringUtils.trim(p.membershipID);
                    t.setBedding(StringUtils.trim(p.bedding));
                    travelers.add(t);
                  }
                });
      }
      reservation.setTravelers(travelers);

      List<UmRate> rates = new ArrayList<>();
      if(hotelInfo.rates != null) {
        hotelInfo.rates.stream().forEach((p) -> {
          if(!p.isEmpty()) {
            UmRate rate = new UmRate();
            rate.setName(StringUtils.trim(p.rateName));
            rate.setDescription(StringUtils.trim(p.rateDescription));
            rate.setPrice(p.ratePrice);
            rate.setDisplayName(StringUtils.trim(UmRateType.valueOf(p.rateName).getValue()));
            rates.add(rate);
          }
        });
      }
      reservation.setRates(rates);

      reservation.setTaxes(hotelInfo.inTaxes);
      reservation.setCurrency(hotelInfo.inCurrency);
      reservation.setTotal(hotelInfo.inTotal);
      reservation.setFees(hotelInfo.inFees);
      reservation.setSubtotal(hotelInfo.inSubtotal);
      reservation.setCancellationPolicy(hotelInfo.inHotelCancellation);
      reservation.setImportant(hotelInfo.inHotelImportant);
      reservation.setStartLocationTimeZone(hotelInfo.inHotelStartTimezone);
      reservation.setFinishLocationTimeZone(hotelInfo.inHotelFinishTimezone);
      reservation.setReservationStatus(hotelInfo.inHotelResStatus);

      if (poiMain != null) {
        reservation.getAccommodation().name = poiMain.getName();
      } else {
        reservation.getAccommodation().name = hotelInfo.inHotelName;
      }
      
      // This should be replaced when the UI is build to take hotel information (outside of poi)
      accoumodationReservationBuilder.parseComments(reservation, hotelInfo.inHotelNote);

      detail.setLastupdatedtimestamp(System.currentTimeMillis());
      detail.setModifiedby(sessionMgr.getUserId());
      detail.save();
      processBookingFiles(detail, hotelInfo);

      Ebean.commitTransaction();

      //Creating Communicator room to discuss
      MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId())
                           .addRoomToUpdate(detail)
                           .update();

      //Create audit log
      audit:
      {
        TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_HOTEL,
                                             (hotelInfo.inTripDetailId == null) ? AuditActionType.ADD : AuditActionType.MODIFY,
                                             AuditActorType.WEB_USER)
                                .withTrip(tripModel)
                                .withCmpyid(tripModel.cmpyid)
                                .withUserid(sessionMgr.getUserId());

        ((AuditTripBookingHotel) ta.getDetails()).withBookingId(detail.getDetailsid())
                                          .withName(reservation.getAccommodation().name)
                                          .withCheckIn(detail.getStarttimestamp())
                                          .withCheckOut(detail.getEndtimestamp());
        ta.save();
      }

      if (hotelInfo.inTripDetailId == null) {
        flash(SessionConstants.SESSION_PARAM_MSG, hotelInfo.inHotelName + " Added");
      }
      else {
        flash(SessionConstants.SESSION_PARAM_MSG, hotelInfo.inHotelName + " Updated");
      }
      flash(SessionConstants.SESSION_PARAM_SCROLL_TO_DIV, detail.detailsid);
      BookingController.invalidateCache(detail.getTripid(), detail.getDetailsid());

      return redirect(routes.BookingController.bookings(tripModel.tripid, new TabType.Bound(TabType.ITINERARY), null));
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "A system error has occurred";
      e.printStackTrace();
      return ok(views.html.common.message.render(baseView));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result createCruise() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<FormBookingCruise> form = formFactory.form(FormBookingCruise.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    FormBookingCruise       cruiseInfo = form.get();
    flash(SessionConstants.SESSION_PARAM_ACTIVE_TAB, TabType.CRUISES.name());

    //Validating individual fields
    if (cruiseInfo.inCruiseName == null || cruiseInfo.inCruiseName.trim().length() == 0) {
      return UserMessage.error("Cruise Name is a required field - please correct and resubmit");
    }
    else if (cruiseInfo.inCruiseDepartDate == null || cruiseInfo.inCruiseDepartDate.trim().length() == 0) {
      return UserMessage.error("Cruise Departure Date is a required field - please correct and resubmit");

    }
    else if (cruiseInfo.inCruiseArriveDate == null || cruiseInfo.inCruiseArriveDate.trim().length() == 0) {
      return UserMessage.error("Cruise Arrival Date is a required field - please correct and resubmit");

    }
    else if (cruiseInfo.inCruiseBoardTime == null || cruiseInfo.inCruiseBoardTime.trim().length() == 0) {
      return UserMessage.error("Cruise Sailing Time is a required field - please correct and resubmit");

    }
    else if (cruiseInfo.inCruiseDisembarkTime == null || cruiseInfo.inCruiseDisembarkTime.trim().length() == 0) {
      return UserMessage.error("Cruise Disembark Time is a required field - please correct and resubmit");
    }

    Trip tripModel = Trip.findByPK(cruiseInfo.inTripId);
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to edit this trip";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      TripDetail tripDetail = null;
      if (cruiseInfo.inTripDetailId != null) {
        cruiseInfo.inTripDetailId = cruiseInfo.inTripDetailId.trim();

        tripDetail = TripDetail.find.byId(cruiseInfo.inTripDetailId);
        if (tripDetail == null || !tripDetail.tripid.equals(cruiseInfo.inTripId)) {
          BaseView baseView = new BaseView();
          baseView.message = "You are not authorized to edit this trip (trip mismatch)";
          return ok(views.html.common.message.render(baseView));
        }

        if (accessLevel == SecurityMgr.AccessLevel.APPEND && !tripDetail.createdby.equals(sessionMgr.getUserId())) {
          BaseView baseView = new BaseView();
          baseView.message = "Permission Error - You are not authorized to edit bookings created by other users";
          return ok(views.html.common.message.render(baseView));
        }
      }

      if (tripDetail == null) {
        tripDetail = new TripDetail();
        tripDetail.setDetailsid(Utils.getUniqueId());
        tripDetail.setCreatedtimestamp(System.currentTimeMillis());
        tripDetail.setCreatedby(sessionMgr.getUserId());
      }

      UmCruiseReservation reservation = cast(tripDetail.getReservation(), UmCruiseReservation.class);
      if (reservation == null) {
        reservation = new UmCruiseReservation();
        tripDetail.setReservation(reservation);
      }

      reservation.setNotesPlainText(cruiseInfo.inCruiseNote);

      List<UmRate> rates = new ArrayList<>();
      if(cruiseInfo.rates != null) {
        cruiseInfo.rates.stream().forEach((p) -> {
          if(!p.isEmpty()) {
            UmRate rate = new UmRate();
            rate.setName(StringUtils.trim(p.rateName));
            rate.setDescription(StringUtils.trim(p.rateDescription));
            rate.setPrice(p.ratePrice);
            rate.setDisplayName(StringUtils.trim(UmRateType.valueOf(p.rateName).getValue()));
            rates.add(rate);
          }
        });
      }
      reservation.setRates(rates);

      reservation.setTaxes(cruiseInfo.inTaxes);
      reservation.setCurrency(cruiseInfo.inCurrency);
      reservation.setTotal(cruiseInfo.inTotal);
      reservation.setFees(cruiseInfo.inFees);
      reservation.setSubtotal(cruiseInfo.inSubtotal);
      reservation.setCancellationPolicy(cruiseInfo.inCruiseCancellation);
      reservation.setImportant(cruiseInfo.inCruiseImportant);
      reservation.setStartLocationTimeZone(cruiseInfo.inCruiseStartTimezone);
      reservation.setFinishLocationTimeZone(cruiseInfo.inCruiseFinishTimezone);

      tripDetail.setBookingnumber(cruiseInfo.inCruiseBooking);
      tripDetail.setRecordLocator(cruiseInfo.inCruiseLocator);
      tripDetail.setDetailtypeid(ReservationType.CRUISE);
      tripDetail.setTripid(cruiseInfo.inTripId);
      tripDetail.setStatus(APPConstants.STATUS_ACTIVE);

      List<PoiRS> pois = buildTripDetailPois(tripModel, tripDetail,
                                             cruiseInfo.inCruiseProviderId, cruiseInfo.inCruiseName,
                                             cruiseInfo.inCruisePortDepartId, cruiseInfo.inCruisePortDepart,
                                             cruiseInfo.inCruisePortArriveId, cruiseInfo.inCruisePortArrive,
                                             sessionMgr.getCredentials());

      PoiRS poiMain   = pois.get(0); //Main vendor
      PoiRS poiStart  = pois.get(1); //Departure
      PoiRS poiFinish = pois.get(2); //Arrival

      tripDetail.setStarttimestamp(Utils.getMilliSecs(cruiseInfo.inCruiseDepartDate + " " +
                                                      cruiseInfo.inCruiseBoardTime));
      tripDetail.setEndtimestamp(Utils.getMilliSecs(cruiseInfo.inCruiseArriveDate + " " +
                                                    cruiseInfo.inCruiseDisembarkTime));

      if (tripDetail.starttimestamp == -1) {
        BaseView baseView = new BaseView();
        baseView.message = "Depart Date and Sailing time are required fields - please correct and resubmit";
        return ok(views.html.common.message.render(baseView));
      }
      else if (tripDetail.endtimestamp == -1) {
        BaseView baseView = new BaseView();
        baseView.message = "Arrival Date and Disembarkment Time are required fields - please correct and resubmit";
        return ok(views.html.common.message.render(baseView));
      }

      tripDetail.setLastupdatedtimestamp(System.currentTimeMillis());
      tripDetail.setModifiedby(sessionMgr.getUserId());
      //update cross date flight
      updateCrossDateTag(cruiseInfo, tripDetail);

      
      UmCruiseShip cruise = reservation.getCruise();
      
      if (poiMain != null) {
        cruise.name = poiMain.getName();
      }
      else {
        cruise.name = cruiseInfo.inCruiseName;
      }

      UmCruiseTraveler traveler = reservation.getCruiseTraveler();
      
      traveler.setBedding(cruiseInfo.inCruiseBedding);
      traveler.setCabinNumber(cruiseInfo.inCruiseCabinNumber);
      traveler.setCategory(cruiseInfo.inCruiseCategory);
      traveler.setDeckNumber(cruiseInfo.inCruiseDeck);
      traveler.setDiningPlan(cruiseInfo.inCruiseMeal);
      
      cruise.getProvider().name = cruiseInfo.inCruiseCmpyName;
 
      //Actually saving to the database right here
      Ebean.beginTransaction();

      tripDetail.save();
      processBookingFiles(tripDetail, cruiseInfo);

      //Creating Communicator room to discuss
      MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId())
                           .addRoomToUpdate(tripDetail)
                           .update();


      //Create audit log
      audit:
      {
        TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_CRUISE,
                                             (cruiseInfo.inTripDetailId == null) ?
                                             AuditActionType.ADD :
                                             AuditActionType.MODIFY,
                                             AuditActorType.WEB_USER)
                                .withTrip(tripModel)
                                .withCmpyid(tripModel.cmpyid)
                                .withUserid(sessionMgr.getUserId());
       
        ((AuditTripBookingCruise) ta.getDetails()).withBookingId(tripDetail.getDetailsid())
                                                  .withName(cruise.name)
                                                  .withDepartDT(tripDetail.getStarttimestamp())
                                                  .withArriveDT(tripDetail.getEndtimestamp());
        ta.save();
      }
      Ebean.commitTransaction();

      if (cruiseInfo.inTripDetailId == null) {
        flash(SessionConstants.SESSION_PARAM_MSG, cruiseInfo.inCruiseName + " Added");
      }
      else {
        flash(SessionConstants.SESSION_PARAM_MSG, cruiseInfo.inCruiseName + " Updated");
      }
      flash(SessionConstants.SESSION_PARAM_SCROLL_TO_DIV, tripDetail.detailsid);

      BookingController.invalidateCache(tripDetail.getTripid(), tripDetail.getDetailsid());

      return redirect(routes.BookingController.bookings(tripModel.tripid, new TabType.Bound(TabType.ITINERARY), null));
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "A system error has occurred";
      e.printStackTrace();
      return ok(views.html.common.message.render(baseView));
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result createTransport() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<FormBookingTransport> form = formFactory.form(FormBookingTransport.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    FormBookingTransport       transferInfo     = form.get();
    flash(SessionConstants.SESSION_PARAM_ACTIVE_TAB, TabType.TRANSPORTS.name());

    Trip tripModel = Trip.findByPK(transferInfo.inTripId);
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to edit this trip";
      return ok(views.html.common.message.render(baseView));
    }

    if (transferInfo.inTransferCmpy == null || transferInfo.inTransferCmpy.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Company Name is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    else if (transferInfo.inTransferDate == null || transferInfo.inTransferDate.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Pick-Up Date is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    else if (transferInfo.inTransferTime == null || transferInfo.inTransferTime.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Pick-Up Time is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();
      TripDetail detail = null;
      if (transferInfo.inTripDetailId != null) {
        transferInfo.inTripDetailId = transferInfo.inTripDetailId.trim();

        detail = TripDetail.find.byId(transferInfo.inTripDetailId);
        if (detail == null || !detail.tripid.equals(transferInfo.inTripId)) {
          BaseView baseView = new BaseView();
          baseView.message = "You are not authorized to edit this trip";
          return ok(views.html.common.message.render(baseView));
        }

        if (accessLevel == SecurityMgr.AccessLevel.APPEND && !detail.createdby.equals(sessionMgr.getUserId())){
          BaseView baseView = new BaseView();
          baseView.message = "Permission Error - You are not authorised to edit bookings created by other users";
          return ok(views.html.common.message.render(baseView));
        }
      }

      if (detail == null) {
        detail = new TripDetail();
        detail.setDetailsid(Utils.getUniqueId());
        detail.setCreatedtimestamp(System.currentTimeMillis());
        detail.setCreatedby(sessionMgr.getUserId());
      }

      detail.setBookingnumber(transferInfo.inTransferBooking);
      ReservationType activityType = ReservationType.valueOf(transferInfo.inBookingType);
      detail.setDetailtypeid(activityType);
      detail.setTripid(transferInfo.inTripId);
      detail.setStatus(APPConstants.STATUS_ACTIVE);

      List<PoiRS> pois = buildTripDetailPois(tripModel, detail,
                                             transferInfo.inTransferProviderId, transferInfo.inTransferCmpy,
                                             null, transferInfo.inTransferPickUp,
                                             null, transferInfo.inTransferDropOff,
                                             sessionMgr.getCredentials());

      PoiRS poiMain   = pois.get(0); //Main vendor
      PoiRS poiStart  = pois.get(1); //Departure
      PoiRS poiFinish = pois.get(2); //Arrival

      if (transferInfo.inTransferDate != null && transferInfo.inTransferDate.length() > 0) {
        if (transferInfo.inTransferTime != null) {
          detail.setStarttimestamp(Utils.getMilliSecs(transferInfo.inTransferDate + " " + transferInfo.inTransferTime));
        }
        else {
          detail.setStarttimestamp(Utils.getMilliSecs(transferInfo.inTransferDate));
        }
      }
      else {
        detail.setStarttimestamp(System.currentTimeMillis());
      }


      if (detail.getStarttimestamp() == -1) {
        Ebean.rollbackTransaction();
        BaseView baseView = new BaseView();
        baseView.message = "Pick-Up Date/Time is a required field - please correct and resubmit";
        return ok(views.html.common.message.render(baseView));
      }


      if (transferInfo.inTransferEndDate != null && transferInfo.inTransferEndDate.length() > 0) {
        if (transferInfo.inTransferEndTime != null) {
          detail.setEndtimestamp(Utils.getMilliSecs(transferInfo.inTransferEndDate + " " + transferInfo.inTransferEndTime));
        }
        else {
          detail.setEndtimestamp(Utils.getMilliSecs(transferInfo.inTransferEndDate));
        }
      }
      else {
        detail.setEndtimestamp(detail.starttimestamp);
      }

      detail.setLastupdatedtimestamp(System.currentTimeMillis());
      detail.setModifiedby(sessionMgr.getUserId());
      UmTransferReservation reservation = cast(detail.getReservation(), UmTransferReservation.class);
      if (reservation == null) {
        reservation = new UmTransferReservation();
        detail.setReservation(reservation);
        reservation.setStartDateTime(detail.getStarttimestamp());
        reservation.setEndDateTime(detail.getEndtimestamp());
      }
      
      reservation.setNotesPlainText(transferInfo.inTransferNote);

      List<UmRate> rates = new ArrayList<>();
      if(transferInfo.rates != null) {
        transferInfo.rates.stream().forEach((p) -> {
          if(!p.isEmpty()) {
            UmRate rate = new UmRate();
            rate.setName(StringUtils.trim(p.rateName));
            rate.setDescription(StringUtils.trim(p.rateDescription));
            rate.setPrice(p.ratePrice);
            rate.setDisplayName(StringUtils.trim(UmRateType.valueOf(p.rateName).getValue()));
            rates.add(rate);
          }
        });
      }
      reservation.setRates(rates);

      reservation.setTaxes(transferInfo.inTaxes);
      reservation.setCurrency(transferInfo.inCurrency);
      reservation.setTotal(transferInfo.inTotal);
      reservation.setFees(transferInfo.inFees);
      reservation.setSubtotal(transferInfo.inSubtotal);
      reservation.setCancellationPolicy(transferInfo.inTransferCancellation);
      reservation.setImportant(transferInfo.inTransferImportant);
      reservation.setStartLocationTimeZone(transferInfo.inTransferStartTimezone);
      reservation.setFinishLocationTimeZone(transferInfo.inTransferFinishTimezone);
      reservation.setServiceType(transferInfo.inTransferServiceType);

      if (poiMain != null) {
        reservation.getTransfer().getProvider().name = poiMain.getName();
      }
      else {
        reservation.getTransfer().getProvider().name = transferInfo.inTransferCmpy;
      }

      reservation.getTransfer().getProvider().contactPoint = transferInfo.inTransferContact;
      //update cross date flight
      updateCrossDateTag(transferInfo, detail);

      detail.save();

      processBookingFiles(detail, transferInfo);

      Ebean.commitTransaction();

      //Creating Communicator room to discuss
      MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId())
                           .addRoomToUpdate(detail)
                           .update();

      //Create audit log
      audit:http://localhost:8000/index.html
      {
        TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_TRANSPORT,
                                             (transferInfo.inTripDetailId == null) ? AuditActionType.ADD : AuditActionType.MODIFY,
                                             AuditActorType.WEB_USER)
                                .withTrip(tripModel)
                                .withCmpyid(tripModel.cmpyid)
                                .withUserid(sessionMgr.getUserId());

       
        ((AuditTripBookingTransport) ta.getDetails()).withBookingId(detail.getDetailsid())
                                          .withName(reservation.getTransfer().getProvider().name)
                                          .withStartTime(detail.getStarttimestamp());
        ta.save();
      }



      if (transferInfo.inTripDetailId == null) {
        flash(SessionConstants.SESSION_PARAM_MSG, transferInfo.inTransferCmpy + " Added");
      }
      else {
        flash(SessionConstants.SESSION_PARAM_MSG, transferInfo.inTransferCmpy + " Updated");
      }
      flash(SessionConstants.SESSION_PARAM_SCROLL_TO_DIV,  detail.detailsid);
      BookingController.invalidateCache(detail.getTripid(), detail.getDetailsid());

      return redirect(routes.BookingController.bookings(tripModel.tripid, new TabType.Bound(TabType.ITINERARY), null));
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "A system error has occurred";
      e.printStackTrace();
      return ok(views.html.common.message.render(baseView));
    }

  }

  private static int getCmpyIdToUse(Credentials cred, Trip tripModel) {
    return cred.getCmpyIdInt();
  }

  /**
   * Helper function to build poi objects for each of the booking types
   * @param tripModel
   * @param detail
   * @param poiIdMain
   * @param poiNameMain
   * @param poiIdStart
   * @param poiNameStart
   * @param poiIdFinish
   * @param poiNameFinish
   * @param cred
   * @return
   */
  public static List<PoiRS> buildTripDetailPois(Trip tripModel, TripDetail detail,
                                                Long poiIdMain, String poiNameMain,
                                                Long poiIdStart, String poiNameStart,
                                                Long poiIdFinish, String poiNameFinish,
                                                Credentials cred) {
    List<PoiRS> result = new ArrayList<>(3);
    PoiRS poiMain = null;   //Main cruise vendor
    PoiRS poiStart = null;  //Departure
    PoiRS poiFinish = null; //Arrival

    Integer cmpIdToUse = getCmpyIdToUse(cred, tripModel);

    //If main poi changed, need to clear all photos
    if ((poiIdMain == null && detail.getPoiId() != null) ||
        (poiIdMain != null && detail.getPoiId() == null) ||
        (poiIdMain != null && detail.getPoiCmpyId() != null && !poiIdMain.equals(detail.getPoiId()))) {
      TripDetailFile.deleteBookingFiles(detail.getDetailsid());
    }

    if (poiIdMain != null && (detail.getPoiId() == null ||
        !poiIdMain.equals(detail.getPoiId()))) { //New search for poi specific to the user
      poiMain = getMergedPoi(poiIdMain, cmpIdToUse);
      //If returned a public record, cmpy ID for this poi should be current user's default cmpy id from above
      if (poiMain != null && poiMain.getCmpyId() == Company.PUBLIC_COMPANY_ID) {
        poiMain.setCmpyId(cmpIdToUse);
      }
    } else {                              //Nothing changes old poi id
      poiMain = getMergedPoi(poiIdMain, detail.getPoiCmpyId());
      if (poiMain != null && poiMain.getCmpyId() == 0) {
        poiMain.setCmpyId(detail.getPoiCmpyId());
      }
    }

    if (poiIdStart != null && (detail.getLocStartPoiId() == null ||
        !poiIdStart.equals(detail.getLocStartPoiId()))) { //New search for poi specific to the user
      poiStart = getMergedPoi(poiIdStart, cmpIdToUse);
      //If returned a public record, cmpy ID for this poi should be current user's default cmpy id from above
      if (poiStart != null && poiStart.getCmpyId() == Company.PUBLIC_COMPANY_ID) {
        poiStart.setCmpyId(cmpIdToUse);
      }
    } else {                              //Nothing changes old poi id
      poiStart = getMergedPoi(poiIdStart, detail.getLocStartPoiCmpyId());
      if (poiStart != null  && poiStart.getCmpyId() == 0) {
        poiStart.setCmpyId(detail.getLocStartPoiCmpyId());
      }
    }

    if (poiIdFinish != null && (detail.getLocFinishPoiId() == null ||
        !poiIdFinish.equals(detail.getLocFinishPoiId()))) { //New search for poi specific to the user
      poiFinish = getMergedPoi(poiIdFinish, cmpIdToUse);
      //If returned a public record, cmpy ID for this poi should be current user's default cmpy id from above
      if (poiFinish != null && poiFinish.getCmpyId() == Company.PUBLIC_COMPANY_ID) {
        poiFinish.setCmpyId(cmpIdToUse);
      }
    } else {                              //Nothing changes old poi id
      poiFinish = getMergedPoi(poiIdFinish, detail.getLocFinishPoiCmpyId());
      if (poiFinish != null  && poiFinish.getCmpyId() == 0) {
        poiFinish.setCmpyId(detail.getLocFinishPoiCmpyId());
      }
    }

    if (poiMain != null) {
      detail.setPoiCmpyId(poiMain.getCmpyId());
      detail.setPoiId(poiMain.getId());
      detail.setLocStartLat(poiMain.locLat);
      detail.setLocStartLong(poiMain.locLong);
      detail.setName(poiMain.getName());
    } else {
      detail.setName(poiNameMain);
      detail.setPoiId(null);
      detail.setPoiCmpyId(null);
      detail.setLocStartLat(0);
      detail.setLocStartLong(0);
    }

    if (poiStart != null) {
      detail.setLocStartPoiCmpyId(poiStart.getCmpyId());
      detail.setLocStartPoiId(poiStart.getId());
      if ((detail.detailtypeid == ReservationType.CRUISE_STOP || detail.detailtypeid == ReservationType.TOUR) && poiNameStart != null) {
       //for a cruise stop, we might have imported the GPS loc but we will let the user update the port
        detail.setLocStartName(poiNameStart);
      } else {
        detail.setLocStartName(poiStart.getName());
      }
      detail.setLocStartLat(poiStart.locLat);
      detail.setLocStartLong(poiStart.locLong);
    }
    else {
      detail.setLocStartName(poiNameStart);
      detail.setLocStartPoiId(null);
      detail.setLocStartPoiCmpyId(null);
    }

    if (poiFinish != null) {
      detail.setLocFinishPoiCmpyId(poiFinish.getCmpyId());
      detail.setLocFinishPoiId(poiFinish.getId());
      detail.setLocFinishName(poiFinish.getName());
      detail.setLocFinishLat(poiFinish.locLat);
      detail.setLocFinishLong(poiFinish.locLong);
    }
    else {
      detail.setLocFinishName(poiNameFinish);
      detail.setLocFinishPoiId(null);
      detail.setLocFinishPoiCmpyId(null);
      detail.setLocFinishLat(0);
      detail.setLocFinishLong(0);
    }

    result.add(0, poiMain);
    result.add(1, poiStart);
    result.add(2, poiFinish);

    return result;
  }

  @With({Authenticated.class, Authorized.class})
  public Result createActivity() {
    //bind html form to form bean
    Form<FormBookingActivity> activityInfoForm = formFactory.form(FormBookingActivity.class);
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    flash(SessionConstants.SESSION_PARAM_ACTIVE_TAB, TabType.ACTIVITIES.name());
    //use bean validator framework
    if (activityInfoForm.hasErrors()) {
      return UserMessage.formErrors(activityInfoForm);
    }
    FormBookingActivity activityInfo = activityInfoForm.bindFromRequest().get();

    Trip tripModel = Trip.findByPK(activityInfo.inTripId);
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to edit this trip";
      return ok(views.html.common.message.render(baseView));
    }

    if (activityInfo.inActivityProviderName == null ||
        activityInfo.inActivityProviderName.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Activity Name is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    else if (activityInfo.inActivityStartDate == null ||
             activityInfo.inActivityStartDate.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Start Date is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    else if (activityInfo.inActivityStartTime == null ||
             activityInfo.inActivityStartTime.trim().length() == 0) {
      BaseView baseView = new BaseView();
      baseView.message = "Start Time is a required field - please correct and resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();
      TripDetail detail = null;
      if (activityInfo.inTripDetailId != null) {
        activityInfo.inTripDetailId = activityInfo.inTripDetailId.trim();

        detail = TripDetail.find.byId(activityInfo.inTripDetailId);
        if (detail == null || !detail.tripid.equals(activityInfo.inTripId)) {
          BaseView baseView = new BaseView();
          baseView.message = "You are not authorized to edit this trip";
          return ok(views.html.common.message.render(baseView));
        }

        if (accessLevel == SecurityMgr.AccessLevel.APPEND && !detail.createdby.equals(sessionMgr.getUserId())) {
          BaseView baseView = new BaseView();
          baseView.message = "Permission Error - You are not authorised to edit bookings created by other users";
          return ok(views.html.common.message.render(baseView));
        }
      }

      if (detail == null) {
        detail = new TripDetail();
        detail.setDetailsid(Utils.getUniqueId());
        detail.setCreatedtimestamp(System.currentTimeMillis());
        detail.setCreatedby(sessionMgr.getUserId());
      }

      UmActivityReservation reservation = cast(detail.getReservation(), UmActivityReservation.class);
      if (reservation == null) {
        reservation = new UmActivityReservation();
        detail.setReservation(reservation);
      }
      
      reservation.setNotesPlainText(activityInfo.inActivityNote);
      detail.setBookingnumber(activityInfo.inActivityBooking);

      List<UmRate> rates = new ArrayList<>();
      if(activityInfo.rates != null) {
        activityInfo.rates.stream().forEach((p) -> {
          if(!p.isEmpty()) {
            UmRate rate = new UmRate();
            rate.setName(StringUtils.trim(p.rateName));
            rate.setDescription(StringUtils.trim(p.rateDescription));
            rate.setPrice(p.ratePrice);
            rate.setDisplayName(StringUtils.trim(UmRateType.valueOf(p.rateName).getValue()));
            rates.add(rate);
          }
        });
      }
      reservation.setRates(rates);

      reservation.setTaxes(activityInfo.inTaxes);
      reservation.setCurrency(activityInfo.inCurrency);
      reservation.setTotal(activityInfo.inTotal);
      reservation.setFees(activityInfo.inFees);
      reservation.setSubtotal(activityInfo.inSubtotal);
      reservation.setCancellationPolicy(activityInfo.inActivityCancellation);
      reservation.setImportant(activityInfo.inActivityImportant);
      reservation.setStartLocationTimeZone(activityInfo.inActivityStartTimezone);
      reservation.setFinishLocationTimeZone(activityInfo.inActivityFinishTimezone);
      reservation.setServiceType(activityInfo.inActivityServiceType);

      ReservationType activityType = ReservationType.valueOf(activityInfo.inBookingType);
      detail.setDetailtypeid(activityType);
      detail.setTripid(activityInfo.inTripId);
      detail.setStatus(APPConstants.STATUS_ACTIVE);

      List<PoiRS> pois = buildTripDetailPois(tripModel, detail,
                                             activityInfo.inActivityProviderId, activityInfo.inActivityProviderName,
                                             activityInfo.inLocStartProvider, activityInfo.inLocStartName,
                                             activityInfo.inLocFinishProvider, activityInfo.inLocFinishName,
                                             sessionMgr.getCredentials());

      PoiRS poiMain   = pois.get(0); //Main vendor
      PoiRS poiStart  = pois.get(1); //Departure
      PoiRS poiFinish = pois.get(2); //Arrival


      if (activityInfo.inActivityStartDate != null && activityInfo.inActivityStartDate.length() > 0) {
        if (activityInfo.inActivityStartDate != null) {
          detail.setStarttimestamp(Utils.getMilliSecs(activityInfo.inActivityStartDate + " " + activityInfo.inActivityStartTime));
        }
        else {
          detail.setStarttimestamp(Utils.getMilliSecs(activityInfo.inActivityStartDate));
        }
      }
      else {
        detail.setStarttimestamp(System.currentTimeMillis());
      }

      if (detail.getStarttimestamp() == -1) {
        Ebean.rollbackTransaction();

        BaseView baseView = new BaseView();
        baseView.message = "Start Date/Time is a required field - please correct and resubmit";
        return ok(views.html.common.message.render(baseView));
      }

      if (activityInfo.inActivityEndDate != null && activityInfo.inActivityEndDate.length() > 0) {
        if (activityInfo.inActivityEndDate != null) {
          detail.setEndtimestamp(Utils.getMilliSecs(activityInfo.inActivityEndDate + " " + activityInfo.inActivityEndTime));
        }
        else {
          detail.setEndtimestamp(Utils.getMilliSecs(activityInfo.inActivityEndDate));
        }
      }
      else {
        detail.setEndtimestamp(detail.starttimestamp);
      }

      if (poiMain != null) {
        reservation.getActivity().name = poiMain.getName();
        if(reservation.getActivity().getOrganizedBy() != null) {
          UmPostalAddress address = new UmPostalAddress();
          if(poiMain.data != null) {
            for (Address a : poiMain.data.getAddresses()) {
              if (a.getAddressType() == AddressType.MAIN) {
                address.addressLocality = a.getLocality();
                if (a.getCountryCode() != null) {
                  address.a3Country = a.getCountryCode();
                  CountriesInfo.Country c = CountriesInfo.Instance().byAlpha3(address.a3Country);
                  address.addressCountry = c.getName(CountriesInfo.LangCode.EN);
                }
                address.addressRegion = a.getRegion();
                address.postalCode = a.getPostalCode();
                address.streetAddress = a.getStreetAddress();
              }
            }
          }
          reservation.getActivity().getOrganizedBy().setAddress(address);
        }
      }
      else {
        reservation.getActivity().name = activityInfo.inActivityProviderName;
      }

      if (activityInfo.inActivityDestinationId != null &&
          activityInfo.inActivityDestinationId.length() > 0 &&
          activityInfo.inActivityDestination != null &&
          activityInfo.inActivityDestination.length() > 0) {
        Destination destination = Destination.find.byId(activityInfo.inActivityDestinationId);
        if (destination != null && SecurityMgr.canAccessDestination(destination, sessionMgr)) {
          if (reservation.getActivity().umDocumentId == null ||
              !reservation.getActivity().umDocumentId.equals(activityInfo.inActivityDestinationId)) {
            //save the new destination guide..
            if (reservation.getActivity().umDocumentId != null && reservation.getActivity().umDocumentId.length() > 0) {
              //delete the existing guide.
              TripDestination tripDest = TripDestination.find.byId(Utils.hash(reservation.getActivity().umDocumentId + detail
                  .getTripid()));
              if (tripDest != null) {
                tripDest.setStatus(APPConstants.STATUS_DELETED);
                tripDest.setModifiedby(sessionMgr.getUserId());
                tripDest.setLastupdatedtimestamp(System.currentTimeMillis());
                tripDest.save();
              }
            }

            TripDestination tripDest = TripDestination.find.byId(Utils.hash(activityInfo.inActivityDestinationId +
                                                                            detail.getTripid()));
            if (tripDest == null) {
              tripDest = new TripDestination();
              tripDest.setTripdestid(Utils.hash(activityInfo.inActivityDestinationId + detail.getTripid()));
              tripDest.setDestinationid(activityInfo.inActivityDestinationId);
              tripDest.setTripid(detail.getTripid());
              tripDest.setCreatedtimestamp(System.currentTimeMillis());
              tripDest.setCreatedby(sessionMgr.getUserId());

              if (destination.name != null) {
                tripDest.setName(destination.name);
              }
              else {
                tripDest.setName("");
              }
              tripDest.rank = 0;
            }

            tripDest.setStatus(APPConstants.STATUS_ACTIVE);
            tripDest.setModifiedby(sessionMgr.getUserId());
            tripDest.setLastupdatedtimestamp(System.currentTimeMillis());
            tripDest.save();
          }


          reservation.getActivity().umDocumentId = destination.destinationid;
        }
      }
      else {
        if (reservation.getActivity().umDocumentId != null && reservation.getActivity().umDocumentId.length() > 0) {
          //delete the old one
          TripDestination tripDest = TripDestination.find.byId(Utils.hash(reservation.getActivity().umDocumentId + detail
              .getTripid()));
          if (tripDest != null) {
            tripDest.setStatus(APPConstants.STATUS_DELETED);
            tripDest.setModifiedby(sessionMgr.getUserId());
            tripDest.setLastupdatedtimestamp(System.currentTimeMillis());
            tripDest.save();
          }
        }
        reservation.getActivity().umDocumentId = null;
      }

      reservation.getActivity().getOrganizedBy().contactPoint = activityInfo.inActivityContact;
      detail.setLastupdatedtimestamp(System.currentTimeMillis());
      detail.setModifiedby(sessionMgr.getUserId());
      //update cross date flight
      updateCrossDateTag(activityInfo, detail);

      detail.save();
      processBookingFiles(detail, activityInfo);

      Ebean.commitTransaction();

      //Creating Communicator room to discuss
      MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId())
                           .addRoomToUpdate(detail)
                           .update();

      //Create audit log
      audit:
      {
        TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_BOOKING_ACTIVITY,
                                             (activityInfo.inTripDetailId == null) ? AuditActionType.ADD : AuditActionType.MODIFY,
                                             AuditActorType.WEB_USER)
                                .withTrip(tripModel)
                                .withCmpyid(tripModel.cmpyid)
                                .withUserid(sessionMgr.getUserId());

        ((AuditTripBookingActivity) ta.getDetails()).withBookingId(detail.getDetailsid())
                                             .withName(reservation.getActivity().name)
                                             .withStartTime(detail.getStarttimestamp());
        ta.save();
      }


      if (activityInfo.inTripDetailId == null) {
        flash(SessionConstants.SESSION_PARAM_MSG, activityInfo.inActivityProviderName + " Added");
      }
      else {
        flash(SessionConstants.SESSION_PARAM_MSG, activityInfo.inActivityProviderName + " Updated");
      }
      flash(SessionConstants.SESSION_PARAM_SCROLL_TO_DIV, detail.detailsid);

      BookingController.invalidateCache(detail.getTripid(), detail.getDetailsid());

      return redirect(routes.BookingController.bookings(tripModel.tripid, new TabType.Bound(TabType.ITINERARY), null));
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      BaseView baseView = new BaseView();
      baseView.message = "A system error has occurred";
      e.printStackTrace();
      return ok(views.html.common.message.render(baseView));
    }
  }

  private static ReturnCode deleteBooking(SessionMgr sessionMgr, Trip tripModel, TripDetail tripDetail) {
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(tripModel, sessionMgr);

    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND)) {
      return ReturnCode.AUTH_TRIP_FAIL;
    }

    if (tripDetail == null) {
      return ReturnCode.DB_RECORD_NOT_FOUND;
    }

    if (accessLevel == SecurityMgr.AccessLevel.APPEND && !tripDetail.getCreatedby().equals(sessionMgr.getUserId())) {
      return ReturnCode.AUTH_BOOKING_DEL_FAIL;
    }

    try {
      Ebean.beginTransaction();
      tripDetail.setStatus(APPConstants.STATUS_DELETED);
      tripDetail.setModifiedby(sessionMgr.getUserId());
      tripDetail.setLastupdatedtimestamp(System.currentTimeMillis());
      tripDetail.save();

      //if this is a cruise VO, let's also delete all the cruise stops vo during the same period
      if (tripDetail.getDetailtypeid() == ReservationType.CRUISE) {
        LocalDate startDateTime = new LocalDate(tripDetail.starttimestamp);
        LocalDate endDateTime = new LocalDate(tripDetail.endtimestamp);
        endDateTime = endDateTime.plusDays(1);

        Boolean dupeCruise = false;
        List<TripDetail> cruises = TripDetail.findActiveByTripId(tripModel.tripid);
        for(int i = 0; i < cruises.size(); i++) {
          if(cruises.get(i).getDetailtypeid() == ReservationType.CRUISE && tripDetail.getDetailsid() != cruises.get(i).getDetailsid()
                  && tripDetail.starttimestamp.compareTo(cruises.get(i).starttimestamp) == 0 && tripDetail.endtimestamp.compareTo(cruises.get(i).endtimestamp) == 0) {
            dupeCruise = true;
            break;
          }
        }

        if(!dupeCruise) {
          List<TripDetail> cruiseStops = TripDetail.findCruiseStopsByDateRange(tripDetail.tripid,
                  startDateTime.toDate().getTime(),
                  endDateTime.toDate().getTime());
          if (cruiseStops != null) {
            for (TripDetail td : cruiseStops) {
              td.setStatus(APPConstants.STATUS_DELETED);
              td.setModifiedby(sessionMgr.getUserId());
              td.setLastupdatedtimestamp(System.currentTimeMillis());
              td.save();
              MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId()).addRoomToUpdate(td).delete();
            }
          }
        }
      }


      //if the company had flight tracking - delete the tracking record
      Company c = Company.find.byId(tripModel.cmpyid);
      if (c != null && c.isFlighttracking() && tripDetail.getDetailtypeid() == ReservationType.FLIGHT) {
        //delete the tracking record
        FlightAlertBooking.deleteByBookingId(tripDetail.detailsid);
      }

      //Create audit log
      audit:
      {
        AuditModuleType auditModuleType = AuditModuleType.fromBookingType(tripDetail.getDetailtypeid());

        if (auditModuleType != AuditModuleType.UNSUPPORTED) {
          TripAudit ta = TripAudit.buildRecord(auditModuleType, AuditActionType.DELETE, AuditActorType.WEB_USER)
                                  .withTrip(tripModel)
                                  .withCmpyid(tripModel.cmpyid)
                                  .withUserid(sessionMgr.getUserId());

          switch (auditModuleType) {
            case TRIP_BOOKING_FLIGHT:
              // (Wei) migrate to using reservation
              //FlightBooking fDetail = FlightBooking.findByPK(tripDetail.getDetailsid());

              UmFlightReservation flight = cast(tripDetail.getReservation(), UmFlightReservation.class);
              
              if (flight != null && flight.getFlight().flightNumber != null) {
                ((AuditTripBookingFlight) ta.getDetails()).withFlightNumber(flight.getFlight().flightNumber)
                                                          .withBookingId(tripDetail.getDetailsid())
                                                          .withDeparture(tripDetail.getStarttimestamp());
                ta.save();
              }
              else {
                Log.log(LogLevel.ERROR,
                        "No matching flight booking record for trip detail =" + tripDetail.getDetailsid());
              }
              break;
            case TRIP_BOOKING_HOTEL:
              UmAccommodationReservation hotel = cast(tripDetail.getReservation(), UmAccommodationReservation.class);
              
              if (hotel != null && hotel.getAccommodation().name != null) {
                ((AuditTripBookingHotel) ta.getDetails()).withName(hotel.getAccommodation().name)
                                                         .withBookingId(tripDetail.getDetailsid())
                                                         .withCheckIn(tripDetail.getStarttimestamp())
                                                         .withCheckOut(tripDetail.getEndtimestamp());
                ta.save();
              }
              else {
                Log.log(LogLevel.ERROR,
                        "No matching hotel booking record for trip detail =" + tripDetail.getDetailsid());

              }
              break;
            case TRIP_BOOKING_CRUISE:
              UmCruiseReservation cruise = cast(tripDetail.getReservation(), UmCruiseReservation.class);
              String name = cruise != null ? cruise.getCruise().name : "";
              if (cruise != null) {
                ((AuditTripBookingCruise) ta.getDetails()).withName(name)
                                                          .withBookingId(tripDetail.getDetailsid())
                                                          .withDepartDT(tripDetail.getStarttimestamp())
                                                          .withArriveDT(tripDetail.getEndtimestamp());
                ta.save();
              }
              else {
                Log.log(LogLevel.ERROR,
                        "No matching cruise booking record for trip detail =" + tripDetail.getDetailsid());
              }
              break;
            case TRIP_BOOKING_TRANSPORT:
              UmTransferReservation transReservation = cast(tripDetail.getReservation(), UmTransferReservation.class);
              if (transReservation != null) {
                ((AuditTripBookingTransport) ta.getDetails()).withName(transReservation.getTransfer().getProvider().name)
                                                             .withBookingId(tripDetail.getDetailsid())
                                                             .withStartTime(tripDetail.getStarttimestamp());
                ta.save();
              }
              else {
                Log.log(LogLevel.ERROR,
                        "No matching transport booking record for trip detail =" + tripDetail.getDetailsid());
              }
              break;
            case TRIP_BOOKING_ACTIVITY:
              UmActivityReservation activity = cast(tripDetail.getReservation(), UmActivityReservation.class);
              
              if (activity != null) {
                ((AuditTripBookingActivity) ta.getDetails()).withName(activity.getActivity().name)
                                                            .withBookingId(tripDetail.getDetailsid())
                                                            .withStartTime(tripDetail.getStarttimestamp());
                ta.save();
              }
              else {
                Log.log(LogLevel.ERROR,
                        "No matching activity booking record for trip detail =" + tripDetail.getDetailsid());
              }
              break;
          }
        }
        else {
          Log.log(LogLevel.ERROR, "Unsupported booking type is being deleted: " + tripDetail.getDetailtypeid());
        }
      }


      if (tripDetail.getDetailtypeid().getRootLevelType() == ReservationType.ACTIVITY) {
        UmActivityReservation activityReservation = cast(tripDetail.getReservation(), UmActivityReservation.class);
        if (activityReservation != null && activityReservation.getActivity().umDocumentId != null) {
          Destination dest = Destination.find.byId(activityReservation.getActivity().umDocumentId);
          if (dest != null && dest.status == APPConstants.STATUS_ACTIVE) {
            TripDestination tripDest = TripDestination.find.byId(Utils.hash(dest.destinationid + tripDetail.tripid));
            if (tripDest != null && tripDest.status == APPConstants.STATUS_ACTIVE) {
              tripDest.setStatus(APPConstants.STATUS_DELETED);
              tripDest.setModifiedby(sessionMgr.getUserId());
              tripDest.setLastupdatedtimestamp(System.currentTimeMillis());
              tripDest.save();
            }
          }
        }
      }

      //delete any notes
      if (TripNote.resetTripDetailId(tripDetail.getDetailsid(), sessionMgr.getUserId(), tripDetail.getStarttimestamp()) > 0) {
        BookingNoteController.invalidateCache(tripDetail.tripid);
      }
      Ebean.commitTransaction();
      return ReturnCode.SUCCESS;
    }
    catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
      return ReturnCode.DB_TRANSACTION_FAIL;
    }
  }

  private static String getDeleteBookingMessage(ReturnCode rc, ReservationType type, String name) {
    String message;

    switch (rc) {
      case AUTH_TRIP_FAIL:
        message = "You are not authorized to delete this booking";
        break;
      case DB_RECORD_NOT_FOUND:
        message = "Booking you are trying to delete was not found";
        break;
      case AUTH_BOOKING_DEL_FAIL:
        message = "Permission Error - You are not authorised to delete bookings created by other users";
        break;
      case SUCCESS:
        //get info for confirmation message
        if (name != null) {
          message = type.getLabel() + " " + name + " Booking deleted";
        } else {
          message = type.getLabel() + " Booking deleted";

        }
        break;
      case DB_TRANSACTION_FAIL:
      default:
        message = rc.msg();
        break;
    }
    return message;
  }

  @With({Authenticated.class, Authorized.class})
  public Result copyBooking() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    String tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);

    //bind html form to form bean
    //check for edit
    final DynamicForm form = formFactory.form().bindFromRequest();
    String tripDetailId = form.get("inTripDetailId");
    String bookingNum = form.get("inBookingNum");
    String startDate = form.get("inBookingStartDate");
    String startTime = form.get("inBookingStartTime");
    String endDate = form.get("inBookingEndDate");
    String endTime = form.get("inBookingEndTime");
    String comments = form.get("inBookingNote");

    //use bean validator framework
    if (tripDetailId == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    tripDetailId = tripDetailId.trim();
    Trip tripModel = Trip.findByPK(tripId);
    TripDetail tripDetail = TripDetail.find.byId(tripDetailId);
    if (tripModel == null || tripDetail == null || !tripDetail.getTripid().equals(tripModel.getTripid())) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    try {
      Ebean.beginTransaction();

      String detailId = tripDetail.getDetailsid();
      tripDetail.prepareForReuse(sessionMgr.getUserId(),sessionMgr.getUserId());
      tripDetail.setTripid(tripDetail.getTripid());
      tripDetail.setBookingnumber(bookingNum);

      tripDetail.setStarttimestamp(Utils.getMilliSecs(startDate + " " + startTime));

      if (endDate != null && endDate.length() > 0) {
        if (endTime != null && endTime.length() > 0) {
          tripDetail.setEndtimestamp(Utils.getMilliSecs(endDate + " " + endTime));
        }
        else {
          tripDetail.setEndtimestamp(Utils.getMilliSecs(endDate));
        }

      }
      else {
        tripDetail.setEndtimestamp(Utils.getMilliSecs(startDate + " " + startTime));
      }
      tripDetail.setComments(comments);
      UmReservation reservation = null;

      switch (tripDetail.detailtypeid.getRootLevelType()) {
        case FLIGHT:
          reservation = cast(tripDetail.getReservation(), UmFlightReservation.class);
          break;
        case CRUISE:
          reservation = cast(tripDetail.getReservation(), UmCruiseReservation.class);
          break;
        case HOTEL:
          reservation = cast(tripDetail.getReservation(), UmAccommodationReservation.class);
          break;
        case ACTIVITY:
          reservation = cast(tripDetail.getReservation(), UmActivityReservation.class);
          break;
        case TRANSPORT:
          reservation = cast(tripDetail.getReservation(), UmTransferReservation.class);
          break;
      }
      reservation.setStartDateTime(tripDetail.getStarttimestamp());
      reservation.setEndDateTime(tripDetail.getEndtimestamp());
      reservation.setNotesPlainText(comments);
      tripDetail.insert();

      //copy poi booking overrides
      List <TripDetailFile> detailFiles = TripDetailFile.getBookingFiles(detailId);
      if (detailFiles != null) {
        for (TripDetailFile file: detailFiles) {
          file.prepareForReuse(tripDetail.getDetailsid(), file.getFileId().poiFileId);
          file.insert();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      Ebean.rollbackTransaction();
      throw e;
    } finally {
      Ebean.commitTransaction();
    }

    String message = "Booking duplicated successfully";

    flash(SessionConstants.SESSION_PARAM_ACTIVE_TAB, TabType.fromBookingType(tripDetail.detailtypeid).name());
    flash(SessionConstants.SESSION_PARAM_MSG, message);
    BookingController.invalidateCache(tripDetail.getTripid(), tripDetail.getDetailsid());
    return  redirect(routes.BookingController.bookings(tripModel.tripid, new TabType.Bound(TabType.ITINERARY), null));
  }

  @With({Authenticated.class, Authorized.class})
  public Result deleteBooking() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    String tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);

    //bind html form to form bean
    //check for edit
    final DynamicForm form = formFactory.form().bindFromRequest();
    String tripDetailId = form.get("inTripDetailId");

    //use bean validator framework
    if (tripDetailId == null) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }

    tripDetailId = tripDetailId.trim();
    Trip tripModel = Trip.findByPK(tripId);
    TripDetail tripDetail = TripDetail.find.byId(tripDetailId);
    if (tripModel == null || tripDetail == null || !tripDetail.getTripid().equals(tripModel.getTripid())) {
      BaseView baseView = new BaseView();
      baseView.message = "Errors on the form - please resubmit";
      return ok(views.html.common.message.render(baseView));
    }
    TripBookingDetailView bookingDetails = BookingController.buildBookingView(tripModel.getTripid(), tripDetail,
                                                                              true, null);
    ReturnCode rc = BookingController.deleteBooking(sessionMgr, tripModel, tripDetail);
    String message = BookingController.getDeleteBookingMessage(rc,
                                                               tripDetail.getDetailtypeid(),
                                                               bookingDetails.providerName);

    MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId()).addRoomToUpdate(tripDetail).delete();

    if (rc != ReturnCode.SUCCESS) {
      BaseView baseView = new BaseView();
      baseView.message = message;
      return ok(views.html.common.message.render(baseView));
    }

    flash(SessionConstants.SESSION_PARAM_ACTIVE_TAB, TabType.fromBookingType(tripDetail.detailtypeid).name());
    flash(SessionConstants.SESSION_PARAM_MSG, message);
    BookingController.invalidateCache(tripDetail.getTripid(), tripDetail.getDetailsid());
    return bookings(tripModel.tripid, new TabType.Bound(TabType.ITINERARY), null);
  }

  @With({Authenticated.class, Authorized.class})
  public Result addTemplate() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    final DynamicForm form = formFactory.form().bindFromRequest();
    String templateId = form.get("inTemplateId");
    String tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);

    Trip trip = Trip.findByPK(tripId);
    Template t = Template.find.byId(templateId);

    //If read or append permissions, template can not be created
    if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND) || t == null) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to access this trip";
      return ok(views.html.common.message.render(baseView));
    }
    else {
      try {
        Ebean.beginTransaction();
        long currTimeInMillis = System.currentTimeMillis();
        List<TmpltDetails> details = TmpltDetails.findByTemplateId(t.templateid);
        if (details != null) {
          for (TmpltDetails detail : details) {
            TripDetail tDetail = new TripDetail();
            tDetail.setDetailsid(DBConnectionMgr.getUniqueId());
            tDetail.setCreatedby(sessionMgr.getUserId());
            tDetail.setModifiedby(sessionMgr.getUserId());
            tDetail.setLastupdatedtimestamp(currTimeInMillis);
            tDetail.setCreatedtimestamp(currTimeInMillis);
            tDetail.setStatus(APPConstants.STATUS_ACTIVE);
            tDetail.setDetailtypeid(detail.detailtypeid);
            tDetail.setName(detail.getName());
            tDetail.setPoiId(detail.getPoiId());
            tDetail.setPoiCmpyId(detail.getPoiCmpyId());

            tDetail.setLocStartLat(detail.getLocStartLat());
            tDetail.setLocStartLong(detail.getLocStartLong());
            tDetail.setLocStartName(detail.getLocStartName());
            tDetail.setLocStartPoiCmpyId(detail.getLocStartPoiCmpyId());
            tDetail.setLocStartPoiId(detail.getLocStartPoiId());

            tDetail.setLocFinishLat(detail.getLocFinishLat());
            tDetail.setLocFinishLong(detail.getLocFinishLong());
            tDetail.setLocFinishName(detail.getLocFinishName());
            tDetail.setLocFinishPoiCmpyId(detail.getLocFinishPoiCmpyId());
            tDetail.setLocFinishPoiId(detail.getLocFinishPoiId());

            tDetail.setTripid(trip.tripid);
            tDetail.setTriptype(0);
            tDetail.setBookingnumber("");
            tDetail.setStarttimestamp(new Long(-1));
            tDetail.setEndtimestamp(new Long(-1));

            tDetail.setReservation(detail.getReservation());
            tDetail.save();

            if (detail.detailtypeid.getRootLevelType().equals(ReservationType.ACTIVITY)) {
                UmActivityReservation activity = cast(detail.getReservation(), UmActivityReservation.class);
                
                if (activity.getActivity().umDocumentId != null && activity.getActivity().umDocumentId.length() > 0) {
                  Destination destination = Destination.find.byId(activity.getActivity().umDocumentId);
                  if (destination != null && destination.cmpyid.equals(trip.cmpyid)) {
                    TripDestination tripDest = TripDestination.find.byId(Utils.hash(activity.getActivity().umDocumentId +
                                                                                    trip.getTripid()));
                    if (tripDest == null) {
                      tripDest = new TripDestination();
                      tripDest.setTripdestid(Utils.hash(activity.getActivity().umDocumentId + trip.getTripid()));
                      tripDest.setDestinationid(activity.getActivity().umDocumentId);
                      tripDest.setTripid(trip.getTripid());
                      tripDest.setCreatedtimestamp(currTimeInMillis);
                      tripDest.setCreatedby(sessionMgr.getUserId());

                      if (destination.name != null) {
                        tripDest.setName(destination.name);
                      }
                      else {
                        tripDest.setName("");
                      }
                      tripDest.rank = 0;
                    }

                    tripDest.setStatus(APPConstants.STATUS_ACTIVE);
                    tripDest.setModifiedby(sessionMgr.getUserId());
                    tripDest.setLastupdatedtimestamp(currTimeInMillis);
                    tripDest.save();
                  }
                }
            }
          }
        }
        //check for guides
        List<TmpltGuide> guides = TmpltGuide.findByTemplate(t.templateid);
        if (guides != null && guides.size() > 0) {
          for (TmpltGuide g : guides) {
            Destination dest = Destination.find.byId(g.destinationid);
            if (dest != null && dest.status == APPConstants.STATUS_ACTIVE) {
              TripDestination tripDest = TripDestination.find.byId(Utils.hash(g.destinationid + trip.getTripid()));
              if (tripDest == null) {
                tripDest = new TripDestination();
                tripDest.setTripdestid(Utils.hash(g.destinationid + trip.getTripid()));
                tripDest.setDestinationid(g.destinationid);
                tripDest.setTripid(trip.getTripid());
                tripDest.setCreatedtimestamp(currTimeInMillis);
                tripDest.setCreatedby(sessionMgr.getUserId());

                if (dest.name != null) {
                  tripDest.setName(dest.name);
                }
                else {
                  tripDest.setName("");
                }
                tripDest.rank = 0;
              }

              tripDest.setStatus(APPConstants.STATUS_ACTIVE);
              tripDest.setModifiedby(sessionMgr.getUserId());
              tripDest.setLastupdatedtimestamp(currTimeInMillis);
              tripDest.save();
            }

          }
        }

        Ebean.commitTransaction();
        flash(SessionConstants.SESSION_PARAM_MSG, "Trip Template: " + t.name + " applied to the trip.");

      }
      catch (Exception e) {
        e.printStackTrace();
        Ebean.rollbackTransaction();
        BaseView baseView = new BaseView();
        baseView.message = "Error processing the request - please resubmit";
        return ok(views.html.common.message.render(baseView));
      }
    }

    return bookings(tripId, new TabType.Bound(TabType.TEMPLATES), null);
  }

  @With({Authenticated.class, Authorized.class})
  public Result uploadItinerary() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    String tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
    Trip tripModel = Trip.findByPK(tripId);
    if (SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to upload files for this trip";
      return ok(views.html.common.message.render(baseView));
    }

    Form<FormUploadItinerary> form = formFactory.form(FormUploadItinerary.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    FormUploadItinerary pdfFileInfo = form.get();

    if (pdfFileInfo.inPDFFileName == null ||
        pdfFileInfo.inPDFFileName.length() == 0 ||
        pdfFileInfo.inFileProvider == null) {
      ObjectNode result = Json.newObject();
      result.put("msg", "Error - please resubmit");
      return ok(result);
    }

    UserProfile userProfile = UserProfile.find.byId(sessionMgr.getUserId());
    if (userProfile != null) {
      if (sessionMgr.getUserId().equals(userProfile.getUserid()) || SecurityMgr.isUmappedAdmin(sessionMgr)) {
        try {
          String id = Utils.getUniqueId();
          String filename =  tripModel.cmpyid + "_" + sessionMgr.getUserId() +"_" + id + "_" + pdfFileInfo.inPDFFileName;

          String signedPutUrl = S3Util.authorizeS3Put(filename);
          String signedGetUrl = S3Util.authorizeS3Get(filename);

          S3Data s3Data = S3Util.authorizeS3(filename);

          TripAttachment tripAttachment = TripAttachment.buildTripAttachment(userProfile,
                                                                tripModel,
                                                                pdfFileInfo.inPDFFileName,
                                                                filename,
                                                                signedGetUrl,
                                                                "Bookings Itinerary");
          tripAttachment.save();

          String nextUrl = controllers.routes.BookingController.parseItinerary(tripAttachment.pk, pdfFileInfo.inFileProvider.name()).url();
          ObjectNode result = Json.newObject();
          result.put("signed_request", signedPutUrl);
          result.put("url", nextUrl);
          result.put("policy", s3Data.policy);
          result.put("accessKey", s3Data.accessKey);
          result.put("key", s3Data.key);
          result.put("s3Bucket", s3Data.s3Bucket);
          result.put("signature", s3Data.signature);

          Log.log(LogLevel.INFO, "JSON Reply: ", result.toString());

          return ok(result);

        }
        catch (Exception e) {
          ObjectNode result = Json.newObject();
          result.put("msg", "System Error. Upload Authorization Failed. Please report to Umapped");
          return ok(result);
        }
      }
    }

    ObjectNode result = Json.newObject();
    result.put("msg", "Error - Unknown Error");
    return ok(result);
  }

  @With({Authenticated.class, Authorized.class})
  public Result parseItinerary(String fileId, String providerName) {

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    String tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
    Trip tripModel = Trip.findByPK(tripId);
    if (SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to process uploaded files for this trip";
      return ok(views.html.common.message.render(baseView));
    }

    TripAttachment tripAttachment = TripAttachment.find.byId(fileId);
    if (tripAttachment == null) {
        return ok("<div id='inviteResult' class='alert alert-error'>System error with uploaded file. Please contact UMapped.</div>");
    }


    List<BookingExtractor.Parsers> userParsers = (List<BookingExtractor.Parsers>) CacheMgr.get(APPConstants
                                                                                              .CACHE_USER_API_PARSERS +
                                                                                          sessionMgr.getAccountId());
    if (userParsers == null || !userParsers.contains(BookingExtractor.Parsers.valueOf(providerName)) ) {
      return ok("<div id='inviteResult' class='alert alert-error'>Wrong parser type specified.</div>");
    }

    UserProfile userProfile = UserProfile.findByPK(sessionMgr.getUserId());
    VOModeller voModeller = new VOModeller(tripModel, userProfile, false);
    BookingItineraryParseView bipv = new BookingItineraryParseView();

    Log.debug("ParsePDF Controller: Filename:", tripAttachment.origfilename);

    CmpyApiParser cmpyParser = null;
    List<CmpyApiParser> companyParsers = CmpyApiParser.findAllUserAPIs(sessionMgr.getCredentials());
    String currParserClass = BookingExtractor.Parsers.valueOf(providerName).getExtractorName();
    for (CmpyApiParser p : companyParsers) {
      if (currParserClass.contains(p.getParser())) {
        cmpyParser = p;
      }
    }

    try {
      S3Object s3Obj = S3Util.getS3File(tripAttachment.getFilename());
      BookingExtractor.Parsers parserType = BookingExtractor.Parsers.valueOf(providerName);
      BookingExtractor extractor = parserType.makeExtractor();
      extractor.setCmpyId(tripModel.getCmpyid());
      if (cmpyParser != null) {
        extractor.init(cmpyParser.getParameters());
      }
      TripVO tvo = extractor.extractData(s3Obj.getObjectContent());
      if (tvo!= null && tvo.getImportSrc() == null) {
        tvo.setImportSrc(BookingSrc.ImportSrc.UPLOAD_PDF);
        tvo.setImportTs(System.currentTimeMillis());
      }
      if (tvo != null  && tvo.getImportSrcId() == null) {
        tvo.setImportSrcId(tripAttachment.getFilename());
      }
      voModeller.buildTripVOModels(tvo);

      if (voModeller.getBookingsCount() > 0) {
        voModeller.addAttachment(tripAttachment);
        voModeller.saveAllModels();
        //call the extractor for any post processing
        if (voModeller.getTrip() != null && voModeller.getTrip().getTripid() != null) {
          extractor.postProcessing(voModeller.getTrip().getTripid(), voModeller.getAccount().getLegacyId());
        }
      }
      else {
        tripAttachment.setStatus(APPConstants.STATUS_DELETED);
        tripAttachment.setModifiedby(userProfile.userid);
        tripAttachment.setLastupdatedtimestamp(System.currentTimeMillis());
        tripAttachment.update();
        return ok("<div id='inviteResult' class='alert alert-error'>No bookings were found in the uploaded file. Ignoring the file.</div>");
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      Log.log(LogLevel.ERROR, "Failure while processing file:" + tripAttachment.origfilename);
      Log.log(LogLevel.ERROR, e.getMessage());
      StackTraceElement trace[] = e.getStackTrace();
      for (StackTraceElement t : trace) {
        Log.log(LogLevel.ERROR, t.toString());
      }
    }

    bipv.activeTab = TabType.FLIGHTS;
    if (voModeller.getFlights() == null || voModeller.getFlights().size() == 0) {
      if (voModeller.getHotels() == null || voModeller.getHotels().size() == 0) {
        if (voModeller.getCruises() == null || voModeller.getCruises().size() == 0) {
          if (voModeller.getTransports() == null || voModeller.getTransports().size() == 0) {
            if (voModeller.getActivities() != null && voModeller.getActivities().size() > 0) {
              bipv.activeTab = TabType.ACTIVITIES;
            }
          } else {
            bipv.activeTab = TabType.TRANSPORTS;
          }
        } else {
          bipv.activeTab =TabType.CRUISES;
        }
      } else {
        bipv.activeTab =TabType.HOTELS;
      }
    }
    bipv.parseData = voModeller;

    return ok(views.html.trip.bookings.bookingParseResults.render(bipv));
  }

  /**
   * This action should be used only from the modal parse result overview screen
   * @note permanently removes booking from the database, not just marks it as deleted
   * @param inDetailId
   * @return
   */
  @With({Authenticated.class, Authorized.class})
  public Result deleteParsedBooking(String inDetailId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    String tripId = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
    Trip tripModel = Trip.findByPK(tripId);
    if (SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to process uploaded files for this trip";
      return ok(views.html.common.message.render(baseView));
    }

    TripDetail tripDetail = TripDetail.findByPK(inDetailId);
    if (tripDetail == null) {
      return ok("Failed to find requested booking");
    }

    if (!tripDetail.createdby.equals(sessionMgr.getUserId())) {
      return ok("You can not delete bookings created by others");
    }

    tripDetail.delete();

    return ok("");
  }

  private static Pair<ReturnCode, String> copyTmpltBookingToTrip(SessionMgr sessionMgr,
                                                   Trip tripModel,
                                                   String tmpltDetailId,
                                                   Date timeStart,
                                                   Date timeEnd) {
    TmpltDetails tmpltDetails = TmpltDetails.find.byId(tmpltDetailId);
    Template template = Template.find.byId(tmpltDetails.getTemplateid());
    if (template == null || !SecurityMgr.canAccessTemplate(template, sessionMgr)) {
      return new ImmutablePair<>(ReturnCode.AUTH_TEMPLATE_FAIL, "");
    }

    TripDetail tripDetail = TripDetail.build(sessionMgr.getUserId(),
                                             tripModel.getTripid(),
                                             tmpltDetails.getDetailtypeid());
    tripDetail.setStatus(APPConstants.STATUS_ACTIVE);
    tripDetail.setDetailtypeid(tmpltDetails.detailtypeid);

    // copy the reservation data
    tripDetail.setReservation(tmpltDetails.getReservation());
    
    tripDetail.setName(tmpltDetails.getName());
    tripDetail.setPoiId(tmpltDetails.getPoiId());

    //if the poi_id is 0, then this is a public template - we now need to set the cmpy properly
    Company cmpy = Company.find.byId(sessionMgr.getCredentials().getCmpyId());
    if (tmpltDetails.getPoiCmpyId() != null && tmpltDetails.getPoiCmpyId() == 0 && cmpy != null) {
      tripDetail.setPoiCmpyId(cmpy.getCmpyId());
    } else {
      tripDetail.setPoiCmpyId(tmpltDetails.getPoiCmpyId());
    }

    tripDetail.setLocStartLat(tmpltDetails.getLocStartLat());
    tripDetail.setLocStartLong(tmpltDetails.getLocStartLong());
    tripDetail.setLocStartName(tmpltDetails.getLocStartName());
    tripDetail.setLocStartPoiId(tmpltDetails.getLocStartPoiId());
    if (tmpltDetails != null && tmpltDetails.getComments() != null && tmpltDetails.getComments().contains("Virtuoso")) {
      tripDetail.setComments(null);
      if (tripDetail.getReservation() != null) {
        tripDetail.getReservation().setNotesPlainText(null);
      }
    } else {
      tripDetail.setComments(tmpltDetails.getComments());
    }

    if (tmpltDetails.getLocStartPoiCmpyId() != null && tmpltDetails.getLocStartPoiCmpyId() == 0 && cmpy != null) {
      tripDetail.setLocStartPoiCmpyId(cmpy.getCmpyId());
    } else {
      tripDetail.setLocStartPoiCmpyId(tmpltDetails.getLocStartPoiCmpyId());
    }

    tripDetail.setLocFinishLat(tmpltDetails.getLocFinishLat());
    tripDetail.setLocFinishLong(tmpltDetails.getLocFinishLong());
    tripDetail.setLocFinishName(tmpltDetails.getLocFinishName());
    tripDetail.setLocFinishPoiId(tmpltDetails.getLocFinishPoiId());

    if (tmpltDetails.getLocFinishPoiCmpyId() != null && tmpltDetails.getLocFinishPoiCmpyId() == 0 && cmpy != null) {
      tripDetail.setLocFinishPoiCmpyId(cmpy.getCmpyId());
    } else {
      tripDetail.setLocFinishPoiCmpyId(tmpltDetails.getLocFinishPoiCmpyId());
    }

    tripDetail.setTripid(tripModel.getTripid());
    tripDetail.setTriptype(0);
    tripDetail.setBookingnumber("");
    if (timeStart != null) {
      tripDetail.setStarttimestamp(timeStart.getTime());
    }
    if (timeEnd != null) {
      tripDetail.setEndtimestamp(timeEnd.getTime());
    }
    tripDetail.setRank(tmpltDetails.rank);

    tripDetail.setImportSrc(BookingSrc.ImportSrc.UMAPPED_TEMPLATE);
    tripDetail.setImportSrcId(tmpltDetailId);
    tripDetail.setImportTs(System.currentTimeMillis());

    tripDetail.save();

    MessengerUpdateHelper.build(tripModel, sessionMgr.getAccountId())
                         .addRoomToUpdate(tripDetail)
                         .update();

    TripAudit ta = TripAudit.buildRecord(AuditModuleType.fromBookingType(tmpltDetails.getDetailtypeid()),
                                         AuditActionType.ADD,
                                         AuditActorType.WEB_USER)
                            .withTrip(tripModel)
                            .withCmpyid(tripModel.cmpyid)
                            .withUserid(sessionMgr.getUserId());

    switch (tmpltDetails.getDetailtypeid().getRootLevelType()) {
      case FLIGHT:
        UmFlightReservation flightReservation = cast(tripDetail.getReservation(), UmFlightReservation.class);
        String flightNumber = flightReservation != null ? flightReservation.getFlight().flightNumber : null;
        
        ((AuditTripBookingFlight) ta.getDetails()).withBookingId(tripDetail.getDetailsid())
                                                  .withFlightNumber(flightNumber)
                                                  .withDeparture(tripDetail.getStarttimestamp());
        break;
      case CRUISE:
        UmCruiseReservation cruiseReservation = cast(tripDetail.getReservation(), UmCruiseReservation.class);
        String name = cruiseReservation != null ? cruiseReservation.getCruise().name : "";
        
        ((AuditTripBookingCruise) ta.getDetails()).withBookingId(tripDetail.getDetailsid())
                                                  .withName(name)
                                                  .withDepartDT(tripDetail.getStarttimestamp())
                                                  .withArriveDT(tripDetail.getEndtimestamp());
        break;
      case HOTEL:
        UmAccommodationReservation hotelReserviation = cast(tmpltDetails.getReservation(), UmAccommodationReservation.class);
        String hotelName = hotelReserviation != null ? hotelReserviation.getAccommodation().name : "";
        UmLodgingBusiness accommodation = hotelReserviation.getAccommodation();

        ((AuditTripBookingHotel) ta.getDetails()).withBookingId(tripDetail.getDetailsid())
                                                    .withName(hotelName)
                                                    .withCheckIn(tripDetail.getStarttimestamp())
                                                    .withCheckOut(tripDetail.getEndtimestamp());

        break;
      case ACTIVITY:
        UmActivityReservation activityReservation = cast(tmpltDetails.getReservation(), UmActivityReservation.class);

        //Coping destination guide (document) to the trip - is this needed?
        if (activityReservation != null && activityReservation.getActivity().umDocumentId != null 
            && activityReservation.getActivity().umDocumentId.length() > 0) {
          Destination destination = Destination.find.byId(activityReservation.getActivity().umDocumentId);
          if (destination != null && destination.cmpyid.equals(tripModel.cmpyid)) {
            TripDestination tripDest = TripDestination.find.byId(Utils.hash(activityReservation.getActivity().umDocumentId + tripModel
                .getTripid()));
            if (tripDest == null) {
              tripDest = new TripDestination();
              tripDest.setTripdestid(Utils.hash(activityReservation.getActivity().umDocumentId + tripModel.getTripid()));
              tripDest.setDestinationid(activityReservation.getActivity().umDocumentId);
              tripDest.setTripid(tripModel.getTripid());
              tripDest.setCreatedtimestamp(System.currentTimeMillis());
              tripDest.setCreatedby(sessionMgr.getUserId());

              if (destination.name != null) {
                tripDest.setName(destination.name);
              }
              else {
                tripDest.setName("");
              }
              tripDest.rank = 0;
            }
            tripDest.setStatus(APPConstants.STATUS_ACTIVE);
            tripDest.setModifiedby(sessionMgr.getUserId());
            tripDest.setLastupdatedtimestamp(tripDest.getCreatedtimestamp());
            tripDest.save();
          }
        }

        ((AuditTripBookingActivity) ta.getDetails()).withBookingId(tripDetail.getDetailsid())
                                                    .withName(activityReservation.getActivity().name)
                                                    .withStartTime(tripDetail.getStarttimestamp());

        break;
      case TRANSPORT:
        UmTransferReservation transport = cast(tripDetail.getReservation(), UmTransferReservation.class);
        String providerName = transport != null ? transport.getTransfer().getProvider().name : "";
        ((AuditTripBookingTransport) ta.getDetails()).withBookingId(tripDetail.getDetailsid())
                                                    .withName(providerName)
                                                    .withStartTime(tripDetail.getStarttimestamp());
        break;
    }
    ta.save();




    return new ImmutablePair<>(ReturnCode.SUCCESS, tripDetail.getDetailsid());
  }

  /**
   * Accepts booking information from FullCalendar event object
   * @param tripId
   * @return
   */
  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result persistBookingJson(String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Trip tripModel = Trip.findByPK(tripId);
    if (SecurityMgr.getTripAccessLevel(tripModel, sessionMgr).lt(SecurityMgr.AccessLevel.APPEND)) {
      BaseJsonResponse jsResp = new BaseJsonResponse(ReturnCode.AUTH_TRIP_FAIL);
      return ok(jsResp.toJson());
    }

    JsonNode json = request().body().asJson();
    TripBookingRequestJson bookingReq = TripBookingRequestJson.fromJson(json);
    TripBookingResponseJson bookingResp = new TripBookingResponseJson(tripId, tripModel.getName());
    bookingResp.withOperation(bookingReq.operation);

    //TODO: Current implementation works with a single return code while assuming an array of items
    //TODO: was provided. Further improvement would be allow for "MIMO" bookings functionality
    try {
      for (BookingDetailsJson bookingDetails : bookingReq.bookings) {
        bookingResp.addBookingDetails(bookingDetails);

        Date timeStart = Utils.iso8601toDate(bookingDetails.timeStart);
        Date timeEnd = Utils.iso8601toDate(bookingDetails.timeEnd);


        switch (bookingReq.operation) {
          //Assuming that coping from template
          case COPY:
            Pair<ReturnCode, String> result = copyTmpltBookingToTrip(sessionMgr,
                                                                     tripModel,
                                                                     bookingDetails.tmpltDetailId,
                                                                     timeStart,
                                                                     timeEnd);
            bookingDetails.tripDetailId = result.getRight();
            bookingDetails.detailsEditUrl = controllers.routes.BookingController.bookings(tripId,
                                                                              new TabType.Bound(TabType.fromBookingType(
                                                                                  bookingDetails.type)),
                                                                              bookingDetails.tripDetailId).url();
            Log.log(LogLevel.DEBUG, "Created trip details: " + result.getRight());
            if (result.getLeft() == ReturnCode.SUCCESS) {
              return ok(bookingResp.withCode(result.getLeft()).withMsg("Copied booking " + bookingDetails.name + " to the trip." ).toJson());
            } else {
              return ok(bookingResp.withCode(result.getLeft()).toJson());
            }
          case UPDATE:
            //Update existing trip
            if (bookingDetails.tripDetailId == null) {
              BaseJsonResponse bj = new BaseJsonResponse(ReturnCode.HTTP_REQ_MISSING_DATA);
              ok(bj.toJson());
            }
            TripDetail upTripDetail = TripDetail.findByPK(bookingDetails.tripDetailId);
            if (upTripDetail == null) {
              BaseJsonResponse bj = new BaseJsonResponse(ReturnCode.DB_RECORD_NOT_FOUND).withMsg("Booking was not found");
              return ok(bj.toJson());
            }

            upTripDetail.setStarttimestamp(timeStart.getTime());
            upTripDetail.setEndtimestamp(timeEnd.getTime());
            upTripDetail.setLastupdatedtimestamp(System.currentTimeMillis());
            upTripDetail.setModifiedby(sessionMgr.getUserId());
            upTripDetail.getReservation().setStartDateTime(timeStart.getTime());
            upTripDetail.getReservation().setEndDateTime(timeEnd.getTime());
            upTripDetail.update();
            String updateMsg = bookingDetails.name + " updated successfully";
            BookingController.invalidateCache(upTripDetail.getTripid(), upTripDetail.getDetailsid());
            return ok(bookingResp.withCode(ReturnCode.SUCCESS).withMsg(updateMsg).toJson());
          case DELETE:
            //Delete existing trip
            if (bookingDetails.tripDetailId == null) {
              BaseJsonResponse bj = new BaseJsonResponse(ReturnCode.HTTP_REQ_MISSING_DATA);
              ok(bj.toJson());
            }
            TripDetail tripDetail = TripDetail.findByPK(bookingDetails.tripDetailId);
            if (tripDetail == null) {
              BaseJsonResponse bj = new BaseJsonResponse(ReturnCode.DB_RECORD_NOT_FOUND).withMsg("Booking was not found");
              return ok(bj.toJson());
            }

            ReturnCode rc = deleteBooking(sessionMgr, tripModel, tripDetail);
            String message = BookingController.getDeleteBookingMessage(rc, tripDetail.getDetailtypeid(),
                                                                       bookingDetails.name);
            BookingController.invalidateCache(tripDetail.getTripid(), tripDetail.getDetailsid());
            return ok(bookingResp.withCode(rc).withMsg(message).toJson());
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      return ok(bookingResp.withCode(ReturnCode.UNHANDLED_EXCEPTION).toJson());
    }
    //Should never reach this place
    return ok(bookingResp.withCode(ReturnCode.LOGICAL_ERROR).toJson());
  }

  /**
   * Perform operation on the request (Operations Router)
   */
  @BodyParser.Of(BodyParser.Json.class)
  @With({Authenticated.class, Authorized.class})
  public Result bkFileExecute() {
    StopWatch sw = new StopWatch();
    sw.start();
    RequestMessageJson bkFileRequest = RequestMessageJson.fromJson(request().body().asJson());
    ResponseMessageJson bkFileResponse = null;

    BookingFileJson poiFileJson = (BookingFileJson) bkFileRequest.body;
    Log.debug("BOOKING_FILE REQ: " + request().body().asJson().toString());
    switch(poiFileJson.operation) {
      case LIST:
        bkFileResponse = bkFileListResponse(bkFileRequest);
        break;
      case DELETE:
        bkFileResponse = bkFileDeleteResponse(bkFileRequest);
        break;
      case UPLOAD:
      case ADD:
      case COPY:
      case UPDATE:
      case SEARCH:
        bkFileResponse = new ResponseMessageJson(bkFileRequest.header, ReturnCode.NOT_SUPPORTED);
        break;
    }

    sw.stop();
    if (bkFileResponse.header.getRespCode() != 0) {
      Log.debug("BOOKING_FILE ERROR: Resp in " + sw.getTime() + "ms :" + bkFileResponse.toJson());
    } else {
      Log.debug("BOOKING_FILE: Resp in " + sw.getTime() + "ms");
    }
    return ok(bkFileResponse.toJson());
  }

  /**
   * Clearing files associated with the booking
   * @param req
   * @return
   */
  private static ResponseMessageJson bkFileDeleteResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = new SessionMgr(session());
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);
    BookingFileJson bkFileRequestJson = (BookingFileJson) req.body;
    BookingFileJson bkFileResponseJson = new BookingFileJson();
    bkFileResponseJson.operation = Operation.LIST;
    bkFileResponseJson.files = new ArrayList<>();
    responseMessageJson.body = bkFileResponseJson;
    bkFileResponseJson.poiId = bkFileRequestJson.poiId;
    bkFileResponseJson.cmpyId = bkFileRequestJson.cmpyId;
    bkFileResponseJson.detailsId = bkFileRequestJson.detailsId;

    if (bkFileRequestJson.detailsId == null || bkFileRequestJson.detailsId.length() == 0) {
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      Log.err("BOOKING_FILE Error: Booking details ID is not specified");
      return responseMessageJson;
    }

    TripDetail detail = TripDetail.findByPK(bkFileRequestJson.detailsId);
    if (detail == null) {
      Log.err("BOOKING_FILE Error: User :" + sessionMgr.getUserId() + " is requesting photos for fake trip detail :" + bkFileRequestJson.detailsId);
      responseMessageJson.header.withCode(ReturnCode.DB_RECORD_NOT_FOUND);
      return responseMessageJson;
    }

    Trip trip = Trip.findByPK(detail.tripid);
    SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
    if (accessLevel.lt(SecurityMgr.AccessLevel.APPEND)) {
      Log.err("BOOKING_FILE Error: User :" + sessionMgr.getUserId() + " requests photos without permission for detail :" + bkFileRequestJson.detailsId);
      responseMessageJson.header.withCode(ReturnCode.AUTH_BOOKING_EDIT_FAIL);
      return responseMessageJson;
    }

    int affected = TripDetailFile.deleteBookingFiles(bkFileRequestJson.detailsId);
    Log.debug("BOOKING_FILE: Cleared " + affected + " files for booking " +  bkFileRequestJson.detailsId);

    sw.stop();
    Log.debug("BOOKING_FILE DELETE: Response prepared in:" + sw.getTime() + "ms");
    responseMessageJson.header.withCode(ReturnCode.SUCCESS);
    return responseMessageJson;
  }

  /**
   * List files for a specific POI
   * @param req
   * @return
   */
  private static ResponseMessageJson bkFileListResponse(RequestMessageJson req) {
    StopWatch sw = new StopWatch();
    sw.start();

    SessionMgr sessionMgr = new SessionMgr(session());
    ResponseMessageJson responseMessageJson = new ResponseMessageJson(req.header);
    BookingFileJson bkFileRequestJson = (BookingFileJson) req.body;
    BookingFileJson bkFileResponseJson = new BookingFileJson();
    bkFileResponseJson.operation = Operation.LIST;
    bkFileResponseJson.files = new ArrayList<>();
    responseMessageJson.body = bkFileResponseJson;
    bkFileResponseJson.poiId = bkFileRequestJson.poiId;
    bkFileResponseJson.cmpyId = bkFileRequestJson.cmpyId;
    bkFileResponseJson.detailsId = bkFileRequestJson.detailsId;

    /**
     * For search functionality poi id and company id must be present
     */
    if (bkFileRequestJson.poiId == null || bkFileRequestJson.poiId.trim().length() == 0) {
      Log.err("BOOKING_FILE Error: PoiID is Null or Empty");
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    Long poiId;
    try {
      poiId = Long.parseLong(bkFileRequestJson.poiId);
    } catch (Exception e) {
      Log.err("BOOKING_FILE Error: PoiID is not a number");
      responseMessageJson.header.withCode(ReturnCode.HTTP_REQ_WRONG_DATA);
      return responseMessageJson;
    }

    Trip trip = null;
    TripDetail detail = null;
    String detailIdUseForSearch = null;
    if (bkFileRequestJson.detailsId != null && bkFileRequestJson.detailsId.length() > 0) {
      detail = TripDetail.findByPK(bkFileRequestJson.detailsId);
      if (detail == null) {
        Log.err("BOOKING_FILE Error: User :" + sessionMgr.getUserId() + " is requesting photos for fake trip detail :" + bkFileRequestJson.detailsId);
        responseMessageJson.header.withCode(ReturnCode.DB_RECORD_NOT_FOUND);
        return responseMessageJson;
      }

      TripDetail.ExtraDetails ed = detail.getExtraDetails();
      if (ed == null) {
        bkFileResponseJson.showPhotos = true;
      } else {
        bkFileResponseJson.showPhotos = ed.showPhotos;
      }

      trip = Trip.findByPK(detail.tripid);
      SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr);
      if (accessLevel.lt(SecurityMgr.AccessLevel.READ)) {
        Log.err("BOOKING_FILE Error: User :" + sessionMgr.getUserId() + " requests photos without permission for detail :" + bkFileRequestJson.detailsId);
        responseMessageJson.header.withCode(ReturnCode.AUTH_BOOKING_FAIL);
        return responseMessageJson;
      }

      //User selected a new POI for existing booking!!!!!
      if((detail.getPoiId() != null && !detail.getPoiId().equals(poiId)) ||
         detail.getPoiId() == null){
        bkFileResponseJson.cmpyId = getCmpyIdToUse(sessionMgr.getCredentials(), trip);
        detailIdUseForSearch = null;
      } else {
        bkFileResponseJson.cmpyId = detail.getPoiCmpyId();
        detailIdUseForSearch = bkFileRequestJson.detailsId;
      }
    } else {
      bkFileResponseJson.showPhotos = true;
      detailIdUseForSearch = null;
      bkFileResponseJson.cmpyId = sessionMgr.getCredentials().getCmpyIdInt(); //TODO: May be add trip_id to requests
      // to determine which company to select, if many
    }

    List<PoiFile> pfs = getBookingFiles(detailIdUseForSearch, poiId, bkFileResponseJson.cmpyId);

    for (PoiFile pf: pfs) {
      bkFileResponseJson.addFile(pf.getFileId(),
                                 pf.getCmpyId(),
                                 pf.getName(),
                                 pf.getUrl(),
                                 pf.getDescription(),
                                 pf.getPriority());
    }

    sw.stop();
    Log.debug("BOOKING_FILE LIST: Response prepared in:" + sw.getTime() + "ms");
    responseMessageJson.header.withCode(ReturnCode.SUCCESS);
    return responseMessageJson;
  }

  public static List<AttachmentView> getPhotosForBooking(TripDetail detail, Long poiId, Integer cmpyId, boolean showAll) {
    List<AttachmentView> photos = new ArrayList<>();
    try {
      TripDetail.ExtraDetails ed = detail.getExtraDetails();
      if (ed == null || ed.showPhotos || showAll) {

        List<PoiFile> files = getBookingFiles(detail.getDetailsid(), poiId, cmpyId);
        if (files != null) {
          for (PoiFile f : files) {
            AttachmentView v = new AttachmentView();
            v.id = Long.toString(f.getFileId());
            v.name = f.getName();
            v.attachName = f.getNameSys();
            v.attachUrl = f.getUrl();
            v.attachType = f.getExtension().getMimeType();
            v.comments = f.getDescription();
            photos.add(v);
          }
        }
      }
    }
    catch (Exception e) {
      Log.err("getPhotosForBooking: " + poiId, e.getMessage());
      e.printStackTrace();
    }
    return photos;
  }

  public static AttachmentView getAttachmentView(TripAttachment tripAttachment, int timezoneOffsetMins) {
    AttachmentView attachView = new AttachmentView();
    attachView.id = tripAttachment.pk;
    attachView.name = tripAttachment.getName();
    if (tripAttachment.getName() != null && tripAttachment.getName().length() > 0) {
      attachView.attachName = tripAttachment.getName();
    }
    else {
      attachView.attachName = tripAttachment.getOrigfilename();
    }
    attachView.attachUrl = tripAttachment.getFileurl();
    attachView.comments = tripAttachment.getDescription();

    //convert to local timestamp;
    long localTimestamp = tripAttachment.lastupdatedtimestamp + (timezoneOffsetMins * 60 * 1000);
    attachView.lastUpdateTimestampPrint = Utils.formatDateTimePrint(localTimestamp);

    attachView.attachType = tripAttachment.getFiletype();

    if (tripAttachment.filename.toLowerCase().endsWith("doc") ||
        tripAttachment.filename.toLowerCase().endsWith("docx")) {
      attachView.attachType = "doc";
    }
    else if (tripAttachment.filename.toLowerCase().endsWith("pdf")) {
      attachView.attachType = "pdf";
    }
    attachView.createdById = tripAttachment.getCreatedby();

    return attachView;
  }


  public static List<AttachmentView> getFilesForBooking(String detailId) {
    List<AttachmentView> files = new ArrayList<>();
    try {
      List<TripDetailAttach> detailAttach = TripDetailAttach.findActiveByDetailId(detailId);
      for (TripDetailAttach tda : detailAttach) {
        TripAttachment tripAttachment = TripAttachment.find.byId(tda.getDetailAttachId().attachId);
        if (tripAttachment != null) {
          files.add(getAttachmentView(tripAttachment, 0));
        }
      }
    }
    catch (Exception e) {
      Log.err("getFilesForBooking: " + detailId, e.getMessage());
      e.printStackTrace();
    }
    return files;
  }

  public static List<PoiFile> getBookingFiles(String detailsId, Long poiId, Integer cmpyId) {
    List<PoiFile> result = null;

    List<TripDetailFile> tdfs = TripDetailFile.getBookingFiles(detailsId);
    if (tdfs != null && tdfs.size() > 0) {
      result = new ArrayList<>(tdfs.size());
      for (TripDetailFile tdf : tdfs) {
        PoiFile pf = PoiFile.find.byId(tdf.getFileId().poiFileId);
        result.add(pf);
      }
    } else if (poiId != null && cmpyId != null) {
      result = PoiController.gatherFilesForPoi(poiId, cmpyId, "images");
    }
    return result;
  }

  public static void processBookingFiles(TripDetail detail, FormBookingBase form) {
    TripDetail.ExtraDetails ed = detail.getExtraDetails();

    if (form.inPhotosReset != null) {
      TripDetailFile.deleteBookingFiles(detail.getDetailsid());
    }

    if (form.inPhotosToggle != null) {
      if (ed == null) {
        ed = new TripDetail.ExtraDetails();
      }
      ed.showPhotos = form.inPhotosToggle;
      if (!ed.showPhotos) {
        TripDetailFile.deleteBookingFiles(detail.getDetailsid());
      }
      detail.setExtraDetails(ed);
      detail.save();
    }

    if (ed == null || ed.showPhotos) {
      if (form.inDetailPhotos != null && form.inDetailPhotos.size() > 0) {
        TripDetailFile.deleteBookingFiles(detail.getDetailsid());
        for (int idx = 0; idx < form.inDetailPhotos.size(); idx++) {
          TripDetailFile tdf = new TripDetailFile();
          tdf.setFileId(detail.getDetailsid(), form.inDetailPhotos.get(idx));
          tdf.setRank(idx);
          tdf.save();
        }
      }
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result bookingPhotos(int bookingType, String detailsId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    if (detailsId != null) {
      TripDetail td = TripDetail.findByPK(detailsId);
      if (td == null) {
        BaseView baseView = new BaseView();
        baseView.message = "No record found.";
        return badRequest(views.html.common.message.render(baseView));
      }
      PoiView view = new PoiView();
      view.tabType = TabType.fromBookingType(ReservationType.fromInt(bookingType)); 
      view.photos = getPhotosForBooking(td, td.getPoiId(), td.getPoiCmpyId(), false);
      view.detailsId = detailsId;
      view.tripId = td.getTripid();
      if (view.photos.size() > 0) {
        Trip trip = Trip.findByPK(td.getTripid());
        SecurityMgr.AccessLevel accessLevel = SecurityMgr.getTripAccessLevel(trip, sessionMgr.getCredentials());

        if (!Utils.isInteger(td.getCreatedby()) &&(accessLevel.gt(SecurityMgr.AccessLevel.OWNER) || (accessLevel.gt(SecurityMgr.AccessLevel.READ) && td.getCreatedby().equals(sessionMgr.getCredentials().getUserId())))){
          view.canManage = true;
        }
        return ok(views.html.poi.photoModal.render(view));
      }
    }
    BaseView baseView = new BaseView();
    baseView.message = "No photos available.";
    return ok(views.html.common.message.render(baseView));
  }


  @With({Authenticated.class, Authorized.class})
  public Result importCruiseTmpt() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    final DynamicForm form = formFactory.form().bindFromRequest();

    String tripId = form.get("tripId");
    String templateDetailId = form.get("tmpltDetailId");

    if (tripId != null && templateDetailId != null) {
      Trip trip = Trip.findByPK(tripId);
      TmpltDetails cruiseDetails = TmpltDetails.find.byId(templateDetailId);
      if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND) &&
          cruiseDetails.getStatus() == 0 &&
          cruiseDetails.getDetailtypeid() == ReservationType.CRUISE) {
        //check to see if this cruise is already part of
        List<TripDetail> bookings = TripDetail.findActiveByStartTimestampAndType(trip.tripid,
                                                                                 cruiseDetails.getStarttimestamp(),
                                                                                 ReservationType.CRUISE);
        if (bookings != null) {
          for (TripDetail td : bookings) {
            if (td.getName().equals(cruiseDetails.getName())) {
              BaseView view = new BaseView();
              view.message = "This cruise is already part of the itinerary.";
              return ok(views.html.common.message.render(view));
            }
          }
        }

        String bookingId = null;
        List<TmpltDetails> details = TmpltDetails.findByTemplateId(cruiseDetails.templateid);
        //find the first start date
        long startTimestamp = 0;
        long endTimestamp = 0;
        LocalDate startDate = null;
        LocalDate endDate = null;

        for (TmpltDetails detail : details) {
          if (detail.detailtypeid == ReservationType.CRUISE) {
            startTimestamp = detail.getStarttimestamp();
            endTimestamp = detail.getEndtimestamp();
            startDate = new LocalDate(startTimestamp);
            endDate = new LocalDate(endTimestamp);
          }
          else if (detail.detailtypeid == ReservationType.CRUISE_STOP ||
                   detail.detailtypeid == ReservationType.TOUR) {
            LocalDate dateTime = new LocalDate(detail.starttimestamp);
            if (dateTime.equals(startDate)) {
              if (detail.endtimestamp > 0) {
                LocalDate dateTime1 = new LocalDate(detail.endtimestamp);
                if (dateTime1.equals(startDate)) {
                  startTimestamp = detail.endtimestamp;
                }
              }
              else {
                startTimestamp = detail.starttimestamp;
              }
            }
            else if (dateTime.equals(endDate)) {
              endTimestamp = detail.starttimestamp;
            }
          }
        }

        for (TmpltDetails detail : details) {
          Pair<ReturnCode, String> result = null;
          if (detail.detailtypeid == ReservationType.CRUISE) {
            result = copyTmpltBookingToTrip(sessionMgr,
                                            trip,
                                            detail.detailsid,
                                            new Date(startTimestamp),
                                            new Date(endTimestamp));
            bookingId = result.getRight();
          }
          else {
            LocalDate dateTime = new LocalDate(detail.starttimestamp);
            if (!dateTime.equals(startDate)) {
              result = copyTmpltBookingToTrip(sessionMgr,
                                              trip,
                                              detail.detailsid,
                                              new Date(detail.getStarttimestamp()),
                                              new Date(detail.getEndtimestamp()));
            }
          }


          if (result != null) {
            Log.log(LogLevel.DEBUG, "Created trip details: " + result.getRight());

          }

        }
        if (bookingId != null) {
          return redirect(routes.BookingController.bookings(trip.getTripid(),
                                                            new TabType.Bound(TabType.CRUISES),
                                                            bookingId));
        }
        //Republishing whole trip as we need to add new user to every booking
        MessengerUpdateHelper.build(trip, sessionMgr.getAccountId()).update();
      }
    }
    BaseView view = new BaseView();
    view.message = "Error importing the cruise - please try again";
    return ok(views.html.common.message.render(view));

  }


  @With({Authenticated.class, Authorized.class})
  public Result importTourTmpt() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    final DynamicForm form = formFactory.form().bindFromRequest();

    String tripId = form.get("inTripId");
    String templateId = form.get("inTemplateId");
    String date = form.get("inStartDate");
    int srcId = Integer.parseInt(form.get("inSrcId"));
    FeedSrc src = FeedSrc.find.byId(srcId);

    if (tripId != null && templateId != null) {
      Trip trip = Trip.findByPK(tripId);
      Template template = Template.find.byId(templateId);
      if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND) &&
          template.getStatus() == 0 &&
          template.cmpyid.equals("0")  &&
          template.getTmpltType() == TmpltType.Type.TOUR) {

        DateTime startDateTime = new DateTime(Utils.getMilliSecs(date));
        try {
          Ebean.beginTransaction();
          //add the details to the template as a trip note with the cover photo
          //let's create the activity for the same time as the trip stop
          TripNote tripNote = TripNote.buildTripNote(trip, sessionMgr.getUserId());
          tripNote.setRank(0);
          tripNote.setNoteTimestamp(startDateTime.getMillis());
          tripNote.setIntro(template.description);
          tripNote.setName(template.name);

          switch(src.getName()) {
            case FeedHelperBigFive.BIG_FIVE_FEED_SRC_NAME:
              tripNote.setImportSrc(BookingSrc.ImportSrc.BIG_FIVE);
              break;
            case FeedHelperGlobusTours.GLOBUS_TOURS_FEED_SRC_NAME:
              tripNote.setImportSrc(BookingSrc.ImportSrc.GLOBUS_TOUR);
              break;
          }
          tripNote.setImportSrcId(templateId);
          tripNote.setImportTs(System.currentTimeMillis());
          tripNote.save();

          if (template.getImageUrl() != null) {
            //add the photo to the note
            TripNoteAttach attach = new TripNoteAttach();
            attach.setFileId(DBConnectionMgr.getUniqueLongId());
            attach.setTripNote(tripNote);
            attach.setStatus(APPConstants.STATUS_ACCEPTED);
            attach.setCreatedBy(sessionMgr.getUserId());
            attach.setCreatedTimestamp(System.currentTimeMillis());
            attach.setAttachType(PageAttachType.PHOTO_LINK);
            //set rank
            attach.setRank(0);
            attach.setAttachUrl(template.getImageUrl());
            attach.setName(template.name);
            attach.setAttachName(template.name);
            attach.setLastUpdatedTimestamp(System.currentTimeMillis());
            attach.setModifiedBy(sessionMgr.getUserId());
            attach.setCreatedBy(sessionMgr.getUserId());
            attach.setCreatedTimestamp(System.currentTimeMillis());
            attach.save();
          }

          //add all the details of the template details as booking activities - increment the date based on the offset
          List<TmpltDetails> tmpltDetails = TmpltDetails.findByTemplateId(template.templateid);
          if (tmpltDetails != null && !tmpltDetails.isEmpty()) {
            for (TmpltDetails details : tmpltDetails) {
              if (details.getDayOffset() > 0) {
                Date startDate = startDateTime.plusDays(details.getDayOffset() - 1).toDate();
                copyTmpltBookingToTrip(sessionMgr,
                                       trip,
                                       details.detailsid,
                                       startDate,
                                       startDate);
              }
            }
          }
          BookingNoteController.invalidateCache(trip.tripid);
          Ebean.commitTransaction();
          flash(SessionConstants.SESSION_PARAM_MSG, template.name + " imported successfully");
          return redirect(routes.BookingController.bookings(trip.getTripid(),
                                                            new TabType.Bound(TabType.ITINERARY),
                                                            null));
        } catch (Exception e) {
          Log.log(LogLevel.ERROR, "importTourTmpt: Trip: " + tripId + " TemplateId: " + templateId, e);
          Ebean.rollbackTransaction();
        }
      }
    }
    BaseView view = new BaseView();
    view.message = "Error importing the tour - please try again";
    return ok(views.html.common.message.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result importApproachGuide() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    final DynamicForm form = formFactory.form().bindFromRequest();

    String tripId = form.get("tripId");
    String userId = sessionMgr.getCredentials().getAccount().getLegacyId();
    String tripDate = form.get("tripDate");
    int noteType = Integer.parseInt(form.get("type"));

    TripNote tn = TripNote.buildTripNote(Trip.findByPK(tripId), userId);
    tn.setType(TripNote.NoteType.CONTENT);
    tn.setTag("approachguides");

    if(noteType == 1) {
      tn.setName("Italy by approachguides");
      tn.setIntro("<p><strong><a href=\"http://www.approachguides.com/italy\" target=\"_blank\" rel=\"noopener\">Approach Guides to Italy</a></strong><br />\n" +
              "<strong>View on ApproachGuides.com</strong> Get to know Italy&#8217;s art, architecture, food and wine with these travel guidebooks for the ultra curious. <em>By David Raezer and Jennifer Raezer.</em></p>\n" +
              "<p><strong><a href=\"http://amzn.to/2sVrr1n\" target=\"_blank\" rel=\"noopener\">Italian Baroque and Rococo Architecture</a></strong><br />\n" +
              "<strong>View on Amazon</strong> The best introduction to this dynamic architectural style, showcased throughout the country, from Sicily to Venice. Think of it as the &#8220;lite version&#8221; of Wittkower&#8217;s tome. <em>By John Varriano.</em></p>\n" +
              "<p><strong><a href=\"http://metmuseum.org/research/metpublications/The_Metropolitan_Museum_of_Art_Vol_4_The_Renaissance_in_Italy_and_Spain?Tag=&amp;title=spain&amp;author=&amp;pt=0&amp;tc=0&amp;dept=0&amp;fmt=0\" target=\"_blank\" rel=\"noopener\">&#8220;The Renaissance in Italy and Spain&#8221;</a></strong><br />\n" +
              "<strong>View on MetMuseum.org</strong> Presents the full range of artistic endeavor from the first awakenings of the Renaissance spirit to the climactic creations of Raphael, Michelangelo, Leonardo, Titian and Veronese, the masters of the High Renaissance.</p>\n" +
              "<p><strong><a href=\"https://youtu.be/679FGDpZBew?list=PLD1450DFDA859F694\" target=\"_blank\" rel=\"noopener\">Dante in Translation | Yale Lectures</a></strong><br />\n" +
              "<strong>Watch on YouTube</strong> Yale University Professor Giuseppe Mazzotta offers a complete course on Dante and his cultural milieu through a critical reading of the Divine Comedy and selected minor works.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/679FGDpZBew?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-1&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-1\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><strong><a href=\"https://open.spotify.com/user/tripreads/playlist/0vxpLyI2TXCq3Zm1H9FDHi\" target=\"_blank\" rel=\"noopener\">The Sounds of Italy | Napoli</a></strong><br />\n" +
              "<strong>Listen to the Spotify playlist</strong> Listen to a collection of Italian tracks, heavily weighted to Neapolitan classics, the perfect pre-trip soundtrack.</p>\n" +
              "<p><iframe src=\"https://open.spotify.com/embed?theme=white&amp;uri=https%3A%2F%2Fopen.spotify.com%2Fuser%2Ftripreads%2Fplaylist%2F0vxpLyI2TXCq3Zm1H9FDHi\" width=\"300\" height=\"380\" frameborder=\"0\"></iframe></p>\n" +
              "<p><strong><a href=\"http://amzn.to/2uk7SmM\" target=\"_blank\" rel=\"noopener\">Understanding Italian Opera</a></strong><br />\n" +
              "<strong>View on Amazon</strong> Shedding light on the creative collusions and collisions involved in bringing opera to the stage, the various, and varying, demands of the text and music, and the nature of its musical drama, Carter also shows how Italian opera has developed over the course of music history. <em>By Tim Carter.</em></p>\n" +
              "<p><strong><a href=\"https://soundcloud.com/monocle-24-the-urbanist/the-urbanist-tall-stories-20?in=monocle-24-the-urbanist/sets/tall-stories\" target=\"_blank\" rel=\"noopener\">Italy&#8217;s Tabaccheria Sign | The Urbanist</a></strong><br />\n" +
              "<strong>Listen on soundcloud</strong> The story of Italy&#8217;s omnipresent tabaccheria sign, composed of a white “T” against a dark background. Consistency is key.</p>\n" +
              "<p><iframe src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/279374041&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false\" width=\"100%\" height=\"166\" frameborder=\"no\" scrolling=\"no\"></iframe></p>\n" +
              "<p><strong><a href=\"https://youtu.be/vhGFWyTS0pk\" target=\"_blank\" rel=\"noopener\">The Making of Prosciutto di Parma</a></strong><br />\n" +
              "<strong>Watch on YouTube</strong> The complete process of making Italy&#8217;s most famous salt-cured ham.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/vhGFWyTS0pk?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-2&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-2\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><strong><a href=\"https://soundcloud.com/in-our-time-history/garibaldi-and-the-risorgimento\" target=\"_blank\" rel=\"noopener\">Garibaldi and the Risorgimento</a></strong><br />\n" +
              "<strong>Listen on soundcloud</strong> Born in Nice in 1807, one of Garibaldi’s aims in life was the unification of Italy and, in large part thanks to him, Italy was indeed united substantially in 1861 and entirely in 1870. With his distinctive red shirt and poncho, he was a hero of Romantic revolutionaries around the world. His fame was secured when, with a thousand soldiers, he invaded Sicily and toppled the monarchy in the Italian south. In this BBC podcast, Melvyn Bragg and guests discuss his legacy.</p>\n" +
              "<p><iframe src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/295720170&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false\" width=\"100%\" height=\"166\" frameborder=\"no\" scrolling=\"no\"></iframe></p>\n" +
              "<p><strong><a href=\"https://youtu.be/bxBFFIxU3hA\">Interview with Winemaker Piero Antinori</a></strong><br />\n" +
              "<strong>Watch on YouTube</strong> A conversation with Marchesi Piero Antinori, one of the true masters and movers of the Italian wine industry, on the greatness of the Sangiovese grape and the special terroir of the Chianti region.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/bxBFFIxU3hA?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-3&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-3\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"http://www.approachguides.com\"><img class=\"alignright\" src=\"https://live.approachguides.com/collection/approachguides-mastercurators-2x.png\" alt=\"“Approach Guides - Master Curators\" width=\"180\" height=\"“44”/\" border=\"0\" /></a></p>");
    } else if(noteType == 2) {
      tn.setName("New York City by approachguides");
      tn.setIntro("<div class=\"flex_column av_one_full  flex_column_div av-zero-column-padding first  avia-builder-el-0  el_before_av_one_fifth  avia-builder-el-first  \" style='border-radius:0px; '><div style='padding-bottom:10px;' class='av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-1  avia-builder-el-no-sibling   av-thin-font'><h1 class='av-special-heading-tag'  itemprop=\"headline\"  ><strong>New York City</strong></h1><div class ='av-subheading av-subheading_below ' style='font-size:20px;'><p>Destination Inspiration</p>\n" +
              "</div><div class='special-heading-border'><div class='special-heading-inner-border' ></div></div></div></div>\n" +
              "<div class=\"flex_column av_one_fifth  flex_column_div av-zero-column-padding first  avia-builder-el-2  el_after_av_one_full  el_before_av_three_fifth  column-top-margin\" style='border-radius:0px; '></div>\n" +
              "<div class=\"flex_column av_three_fifth  flex_column_div av-zero-column-padding   avia-builder-el-3  el_after_av_one_fifth  el_before_av_one_fifth  column-top-margin\" style='border-radius:0px; '><section class=\"av_textblock_section\"  itemscope=\"itemscope\" itemtype=\"https://schema.org/CreativeWork\" ><div class='avia_textblock '   itemprop=\"text\" ><p><a href=\"http://amzn.to/2t9dJvS\" target=\"_blank\" rel=\"noopener\"><strong>Architecture Guide to Soho &amp; Tribeca</strong></a><br />\n" +
              "<strong>View on Amazon</strong> Explore downtown NYC&#8217;s fantastic cast iron architecture with this guide to the best buildings. <em>By David Raezer and Jennifer Raezer</em>.</p>\n" +
              "<p><a href=\"http://amzn.to/2tkjaDz\" target=\"_blank\" rel=\"noopener\"><strong> Grand Central: How a Train Station Transformed America</strong></a><br />\n" +
              "<strong>View on Amazon</strong> The definitive book on the city&#8217;s most famous transportation landmark. <em>By Sam Roberts</em>.</p>\n" +
              "<p><a href=\"https://vimeo.com/88916863\" target=\"_blank\" rel=\"noopener\"><strong>Walking Contest</strong></a><br />\n" +
              "<strong>Watch on Vimeo</strong> Walking the sidewalks of NYC brings out everyone&#8217;s competitive nature.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe src=\"https://player.vimeo.com/video/88916863?api=1&#038;player_id=embed-video-1&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-1\" width=\"95%\" frameborder=\"0\" title=\"Walking Contest\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>\n" +
              "<p><a href=\"https://youtu.be/NMayu1dqR0Q\" target=\"_blank\" rel=\"noopener\"><strong>High-Rise-Window Washers of Manhattan | The New Yorker Magazine</strong></a><br />\n" +
              "<strong>Watch on YouTube</strong> Thirty years ago, John McDermott and John Wren were young daredevils in need of union benefits, so they signed up for one of New York’s most dangerous professions: high-rise-window washing.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/NMayu1dqR0Q?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-2&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-2\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"http://amzn.to/2tkotTh\" target=\"_blank\" rel=\"noopener\"><strong>Rao&#8217;s Cookbook: Over 100 Years of Italian Home Cooking</strong></a><br />\n" +
              "<strong>View on Amazon</strong> Rao&#8217;s, the hundred-year-old restaurant with a mere ten tables tucked in a corner of East Harlem (now with outposts in Las Vegas and Los Angeles), is one of the most sought-after restaurants in all of Manhattan. Here for the first time are recipes for all of Rao&#8217;s fabulous classics. <em>By Frank Pellegrino</em>.</p>\n" +
              "<p><a href=\"https://soundcloud.com/monocle-24-the-urbanist/the-urbanist-tall-stories-41\" target=\"_blank\" rel=\"noopener\"><strong> Tall Stories: Ellis Island</strong></a><br />\n" +
              "<strong>Listen on soundcloud</strong> For more than 60 years Ellis Island was the busiest immigrant inspection station in the US. In this The Urbanist podcast, we look at the space where more than 12 million immigrants got the opportunity to begin a new life.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"100%\" height=\"166\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?visual=true&#038;url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F305319637&#038;show_artwork=true&#038;maxwidth=1500&#038;maxheight=1000\"></iframe></div>\n" +
              "<p><a href=\"http://www.approachguides.com\"><img class=\"alignright\" src=\"https://live.approachguides.com/collection/approachguides-mastercurators-2x.png\" alt=\"“Approach Guides - Master Curators\" width=\"180\" height=\"“44”/\" border=\"0\" /></a></p>\n" +
              "</div></section></div>");
    } else if(noteType == 3) {
      tn.setName("South Africa by approachguides");
      tn.setIntro("<div class=\"flex_column av_one_full  flex_column_div av-zero-column-padding first  avia-builder-el-0  el_before_av_one_fifth  avia-builder-el-first  \" style='border-radius:0px; '><div style='padding-bottom:10px;' class='av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-1  avia-builder-el-no-sibling   av-thin-font'><h1 class='av-special-heading-tag'  itemprop=\"headline\"  ><strong>South Africa</strong></h1><div class ='av-subheading av-subheading_below ' style='font-size:20px;'><p>Destination Inspiration</p>\n" +
              "</div><div class='special-heading-border'><div class='special-heading-inner-border' ></div></div></div></div>\n" +
              "<div class=\"flex_column av_one_fifth  flex_column_div av-zero-column-padding first  avia-builder-el-2  el_after_av_one_full  el_before_av_three_fifth  column-top-margin\" style='border-radius:0px; '></div>\n" +
              "<div class=\"flex_column av_three_fifth  flex_column_div av-zero-column-padding   avia-builder-el-3  el_after_av_one_fifth  el_before_av_one_fifth  column-top-margin\" style='border-radius:0px; '><section class=\"av_textblock_section\"  itemscope=\"itemscope\" itemtype=\"https://schema.org/CreativeWork\" ><div class='avia_textblock '   itemprop=\"text\" ><p><strong><a href=\"http://amzn.to/2sVEek8\" target=\"_blank\" rel=\"noopener\">The Safari Companion</a><br />\n" +
              "View on Amazon</strong> Without doubt, the best field guide to observing and understanding the behavior of African mammals. Invaluable for safari-bound travelers. <em>By Richard D Estes</em>.</p>\n" +
              "<p><strong><a href=\"http://www.nytimes.com/2016/05/25/dining/braai.html\" target=\"_blank\" rel=\"noopener\">&#8220;South Africa, One Nation United by the Grill&#8221;</a><br />\n" +
              "View on NYTimes.com</strong> Even though South Africa encompasses many distinct cultures, the braai (grill) cuts across ethnicity, race and class. <em>By Julia Moskin</em>.</p>\n" +
              "<p><strong><a href=\"https://youtu.be/0wZtfqZ271w\" target=\"_blank\" rel=\"noopener\">Who We Are | Desmond Tutu</a><br />\n" +
              "</strong><strong>Watch on YouTube</strong> The South African social rights activist and retired Anglican bishop speaks on the topic of human uniqueness and the African spirit of Ubuntu: &#8220;A person is a person through other persons.&#8221;</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/0wZtfqZ271w?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-1&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-1\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><strong><a href=\"http://www.winemag.com/2012/05/24/chenin-blanc-struts-its-stuff/\" target=\"_blank\" rel=\"noopener\">&#8220;Chenin Blanc Struts Its Stuff&#8221;</a><br />\n" +
              "</strong><strong>View on winemag.com</strong> South Africa’s iconic white wine has evolved into a multifaceted star, molded by the hands of inspired winemakers. <em>By Lauren Buzzeo</em>.</p>\n" +
              "<p><strong><a href=\"https://soundcloud.com/radio-diaries/the-day-nelson-mandela-became-nelson-mandela\" target=\"_blank\" rel=\"noopener\">The Day Nelson Mandela Became Nelson Mandela</a><br />\n" +
              "</strong><strong>Listen on soundcloud</strong> An excellent recounting of the historical context surrounding Mandela&#8217;s assumption of resistance leadership.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"100%\" height=\"166\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?visual=true&#038;url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F145568358&#038;show_artwork=true&#038;maxwidth=1500&#038;maxheight=1000\"></iframe></div>\n" +
              "<p><strong><a href=\"https://youtu.be/lrK-XVCwGnI\" target=\"_blank\" rel=\"noopener\">Willie Komani describes the Xhosa &#8216;click&#8217; language | South Africa Walks | BBC</a><br />\n" +
              "</strong><strong>Watch on YouTube</strong> Julia Bradbury’s guide Willie Komani discusses and demonstrates the &#8216;click&#8217; consonants of the Xhosa language.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/lrK-XVCwGnI?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-2&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-2\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"http://www.approachguides.com\"><img class=\"alignright\" src=\"https://live.approachguides.com/collection/approachguides-mastercurators-2x.png\" alt=\"“Approach Guides - Master Curators\" width=\"180\" height=\"“44”/\" border=\"0\" /></a></p>\n" +
              "</div></section></div>");
    } else if(noteType == 4) {
      tn.setName("Las Vegas by approachguides");
      tn.setIntro("<div class=\"flex_column av_one_full  flex_column_div av-zero-column-padding first  avia-builder-el-0  el_before_av_one_fifth  avia-builder-el-first  \" style='border-radius:0px; '><div style='padding-bottom:10px;' class='av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-1  avia-builder-el-no-sibling   av-thin-font'><h1 class='av-special-heading-tag'  itemprop=\"headline\"  ><strong>Las Vegas</strong></h1><div class ='av-subheading av-subheading_below ' style='font-size:20px;'><p>Destination Inspiration</p>\n" +
              "</div><div class='special-heading-border'><div class='special-heading-inner-border' ></div></div></div></div>\n" +
              "<div class=\"flex_column av_one_fifth  flex_column_div av-zero-column-padding first  avia-builder-el-2  el_after_av_one_full  el_before_av_three_fifth  column-top-margin\" style='border-radius:0px; '></div>\n" +
              "<div class=\"flex_column av_three_fifth  flex_column_div av-zero-column-padding   avia-builder-el-3  el_after_av_one_fifth  el_before_av_one_fifth  column-top-margin\" style='border-radius:0px; '><section class=\"av_textblock_section\"  itemscope=\"itemscope\" itemtype=\"https://schema.org/CreativeWork\" ><div class='avia_textblock '   itemprop=\"text\" ><p><a href=\"http://www.newyorker.com/magazine/2010/01/11/water-music-2\" target=\"_blank\" rel=\"noopener\"><strong>&#8220;Water Music&#8221;</strong></a><br />\n" +
              "<strong>View on NewYorker.com</strong> The fountain architect who gave water a voice. He is the man behind the fountains at the Bellagio hotel and casino in Las Vegas, which Steven Spielberg has called “the greatest single piece of public entertainment on planet Earth.&#8221; <em>By John Seabrook</em>.</p>\n" +
              "<p><a href=\"http://amzn.to/2uffTKc\" target=\"_blank\" rel=\"noopener\"><strong> Bringing Down the House: The Inside Story of Six M.I.T. Students Who Took Vegas for Millions</strong></a><br />\n" +
              "<strong>View on Amazon</strong> The #1 national bestseller, now a major motion picture, 21—the amazing inside story about a gambling ring of M.I.T. students who beat the system in Vegas—and lived to tell how. <em>By Ben Mezrich</em>.</p>\n" +
              "<p><a href=\"https://youtu.be/tV9DS4g8_Hk\" target=\"_blank\" rel=\"noopener\"><strong>Illusionist David Copperfield Discusses the Business of Magic</strong></a><br />\n" +
              "<strong>Watch on YouTube</strong> Legendary illusionist David Copperfield talks to the Wall Street Journal about his ongoing show at the MGM Grand in Las Vegas and how he became one of the most recognizable performers in the world.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/tV9DS4g8_Hk?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-1&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-1\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"http://punchdrink.com/articles/history-of-swim-up-bar-vegas-best-pool-bar/\" target=\"_blank\" rel=\"noopener\"><strong>&#8220;The Surprising History of the Swim-Up Bar&#8221;</strong></a><br />\n" +
              "<strong>View on punchdrink.com</strong> Like most seemingly misguided drinking trends, the swim-up bar was born in — you know it — Las Vegas. <em>By Aaron Goldfarb</em>.</p>\n" +
              "<p><a href=\"https://www.wired.com/2010/09/ugly-vegas-carpets/\" target=\"_blank\" rel=\"noopener\"><strong>&#8220;Ugly Vegas Carpets Want You to Keep Playing&#8221;</strong></a><br />\n" +
              "<strong>View on wired.com</strong> Thought has been given to the carpeting by people who want to create this special atmosphere, one that defines Vegas as a gambling city. <em>By Pete Brook</em>.</p>\n" +
              "<p><a href=\"https://youtu.be/xVpdcpXIWyo\" target=\"_blank\" rel=\"noopener\"><strong> Streets by VICE: Las Vegas (Charleston Boulevard)</strong></a><br />\n" +
              "<strong>Watch on YouTube</strong> In this 17-minute episode, we head to Las Vegas and delve into life beyond The Strip by way of Charleston Boulevard, the city’s longest street running east to west. On the east end you might come across a few Mormons, who were some of the first people to settle in Vegas. On the west end you&#8217;ll find relics of the casinos established by the mob. Without the influence of these groups some say Las Vegas would look more like a truck stop than the glittering city it is today.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/xVpdcpXIWyo?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-2&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-2\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"https://audioboom.com/posts/6027035-las-vegas-stripped-bare\" target=\"_blank\" rel=\"noopener\"><strong> Las Vegas Stripped Bare</strong></a><br />\n" +
              "<strong>Listen on audioboom.com</strong> With its reputation for glitz, glamour and gambling, Las Vegas has become one of the world’s foremost tourist destinations, with over 40 million visitors a year. But the bright lights and breathtaking architecture conceal a murky past. This BBC World Service 50-minute podcast explores.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe class=\"wp-embedded-content\" sandbox=\"allow-scripts\" security=\"restricted\" width=\"750\" height=\"300\" frameborder=\"0\" scrolling=\"no\" src=\"//embeds.audioboom.com/posts/6027035-las-vegas-stripped-bare/embed/v4?eid=AQAAAM3sZ1kb91sA#?secret=PcmsJVeKCP\" data-secret=\"PcmsJVeKCP\" title=\"audioBoom player\"></iframe></div>\n" +
              "<p><a href=\"http://www.approachguides.com\"><img class=\"alignright\" src=\"https://live.approachguides.com/collection/approachguides-mastercurators-2x.png\" alt=\"“Approach Guides - Master Curators\" width=\"180\" height=\"“44”/\" border=\"0\" /></a></p>\n" +
              "</div></section></div>");
    } else if(noteType == 5) {
      tn.setName("Iceland by approachguides");
      tn.setIntro("<div class=\"flex_column av_one_full  flex_column_div av-zero-column-padding first  avia-builder-el-0  el_before_av_one_fifth  avia-builder-el-first  \" style='border-radius:0px; '><div style='padding-bottom:10px;' class='av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-1  avia-builder-el-no-sibling   av-thin-font'><h1 class='av-special-heading-tag'  itemprop=\"headline\"  ><strong>Iceland</strong></h1><div class ='av-subheading av-subheading_below ' style='font-size:20px;'><p>Destination Inspiration</p>\n" +
              "</div><div class='special-heading-border'><div class='special-heading-inner-border' ></div></div></div></div>\n" +
              "<div class=\"flex_column av_one_fifth  flex_column_div av-zero-column-padding first  avia-builder-el-2  el_after_av_one_full  el_before_av_three_fifth  column-top-margin\" style='border-radius:0px; '></div>\n" +
              "<div class=\"flex_column av_three_fifth  flex_column_div av-zero-column-padding   avia-builder-el-3  el_after_av_one_fifth  el_before_av_one_fifth  column-top-margin\" style='border-radius:0px; '><section class=\"av_textblock_section\"  itemscope=\"itemscope\" itemtype=\"https://schema.org/CreativeWork\" ><div class='avia_textblock '   itemprop=\"text\" ><p><strong><a href=\"http://amzn.to/2uf6DFJ\" target=\"_blank\" rel=\"noopener\">Independent People</a><br />\n" +
              "View on Amazon</strong> Winner of the 1955 Nobel Prize in Literature, the esteemed Icelandic author tells the tale of an early 20th century sheepherders&#8217; mythic journey to achieve independence. <em>By Halldor Laxness</em>.</p>\n" +
              "<p><a href=\"http://roadsandkingdoms.com/2015/zen-and-the-art-of-icelandic-cuisine/\" target=\"_blank\" rel=\"noopener\"><strong>&#8220;Zen and the Art of Icelandic Cuisine&#8221;</strong></a><br />\n" +
              "<strong>View on RoadsandKingdoms.com</strong> In Iceland, the elements have shaped the cuisine in ways that few other countries can compare. The cold and lack of drastic weather changes cause life to grow more slowly, infusing plants and shellfish with rich, deep flavors. <em>By Nicholas Gill</em>.</p>\n" +
              "<p><a href=\"https://vimeo.com/59934771\" target=\"_blank\" rel=\"noopener\"><strong>Northern Lights in Iceland</strong></a><br />\n" +
              "<strong>Watch on Vimeo</strong> Time-lapse sequences of the Northern Lights in Iceland, shot in Reykjanes, Thingvellir national park and Snaefellsnes Penisula.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe src=\"https://player.vimeo.com/video/59934771?api=1&#038;player_id=embed-video-1&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-1\" width=\"95%\" frameborder=\"0\" title=\"Northern Lights In Iceland V3\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>\n" +
              "<p><a href=\"http://www.nytimes.com/2016/04/24/magazine/icelands-water-cure.html?_r=2\" target=\"_blank\" rel=\"noopener\"><strong>&#8220;Iceland&#8217;s Water Cure&#8221;</strong></a><br />\n" +
              "<strong>View on NYTimes.com</strong> Can the secret to the country’s happiness be found in its communal pools? <em>By Dan Kois</em>.</p>\n" +
              "<p><a href=\"https://youtu.be/yBbeqXwihGE\" target=\"_blank\" rel=\"noopener\"><strong>Iceland: A Photographer&#8217;s Paradise (Jökulsárlón glacial lagoon) | New Yorker Magazine</strong></a><br />\n" +
              "<strong>Watch on YouTube</strong> The number of tourist visits to Iceland has tripled in the last decade. A main attraction is the Jökulsárlón glacial lagoon, where photographers jockey to capture the perfect picture.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/yBbeqXwihGE?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-2&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-2\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"https://soundcloud.com/in-our-time/iot-icelandic-sagas-09-may-13\" target=\"_blank\" rel=\"noopener\"><strong>The Icelandic Sagas</strong></a><br />\n" +
              "<strong>Listen on soundcloud</strong> The Icelandic Sagas were first written down in the 13th century and tell stories of the Norse settlers who began to arrive in Iceland 400 years before. In this BBC podcast, Melvyn Bragg and guests explore these writings, some of the richest and most extraordinary of the Middle Ages.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"100%\" height=\"166\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?visual=true&#038;url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F144627656&#038;show_artwork=true&#038;maxwidth=1500&#038;maxheight=1000\"></iframe></div>\n" +
              "<p><a href=\"https://youtu.be/_7JFwLyrdJw\" target=\"_blank\" rel=\"noopener\"><strong>Beautiful Tiny Turf House in Iceland &#8211; Full Tour &amp; Interview</strong></a><br />\n" +
              "<strong>Watch on YouTube</strong> Tour a traditional Icelandic turf house at the Islenski Baerinn Turf House museum, located 60 km west of Reykjavik.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/_7JFwLyrdJw?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-3&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-3\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"https://soundcloud.com/wsdocumentaries/iceland-rescue\" target=\"_blank\" rel=\"noopener\"><strong>Iceland Rescue</strong></a><br />\n" +
              "<strong>Listen on soundcloud</strong> A family stranded in a snowfield. A woman with vertigo on a mountain. A hiker falling in lava. These are just some of the jobs for Slysavarnafélagið Landsbjörg (Ice-SAR): the Icelandic Association for Search and Rescue. Ice-SAR is an elite national emergency militia with a gallant reputation in Iceland. This BBC documentary podcast explores the organization.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"100%\" height=\"166\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?visual=true&#038;url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F298598559&#038;show_artwork=true&#038;maxwidth=1500&#038;maxheight=1000\"></iframe></div>\n" +
              "<p><a href=\"http://www.approachguides.com\"><img class=\"alignright\" src=\"https://live.approachguides.com/collection/approachguides-mastercurators-2x.png\" alt=\"“Approach Guides - Master Curators\" width=\"180\" height=\"“44”/\" border=\"0\" /></a></p>\n" +
              "</div></section></div>");
    } else if(noteType == 6) {
      tn.setName("Andalucia by approachguides");
      tn.setIntro("<div class=\"flex_column av_one_full  flex_column_div av-zero-column-padding first  avia-builder-el-0  el_before_av_one_fifth  avia-builder-el-first  \" style='border-radius:0px; '><div style='padding-bottom:10px;' class='av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-1  avia-builder-el-no-sibling   av-thin-font'><h1 class='av-special-heading-tag'  itemprop=\"headline\"  ><strong>Andalucia</strong></h1><div class ='av-subheading av-subheading_below ' style='font-size:20px;'><p>Destination Inspiration</p>\n" +
              "</div><div class='special-heading-border'><div class='special-heading-inner-border' ></div></div></div></div>\n" +
              "<div class=\"flex_column av_one_fifth  flex_column_div av-zero-column-padding first  avia-builder-el-2  el_after_av_one_full  el_before_av_three_fifth  column-top-margin\" style='border-radius:0px; '></div>\n" +
              "<div class=\"flex_column av_three_fifth  flex_column_div av-zero-column-padding   avia-builder-el-3  el_after_av_one_fifth  el_before_av_one_fifth  column-top-margin\" style='border-radius:0px; '><section class=\"av_textblock_section\"  itemscope=\"itemscope\" itemtype=\"https://schema.org/CreativeWork\" ><div class='avia_textblock '   itemprop=\"text\" ><p><a href=\"http://amzn.to/2uohfTl\" target=\"_blank\" rel=\"noopener\"><strong>Guide to The Alhambra in Granada, Spain</strong></a><br />\n" +
              "<strong>View on Amazon</strong> Richly detailed guide to the Alhambra — a magnificent 14th century palace-city built by the Nasrids in the city of Granada — which stands as one of the greatest monuments of Islamic Spain, or Al-Andalus. <em>By David Raezer and Jennifer Raezer</em>.</p>\n" +
              "<p><a href=\"http://www.slate.com/blogs/browbeat/2014/02/18/spain_time_change_proposal_the_land_of_10_p_m_dinners_shouldn_t_change_a.html\" target=\"_blank\" rel=\"noopener\"><strong>&#8220;Spain Shouldn&#8217;t Change Its Mealtimes. We Should Change Ours&#8221;</strong></a><br />\n" +
              "<strong>View on Slate.com</strong> 10pm dinners aren&#8217;t such a bad idea. <em>By LV Anderson</em>.</p>\n" +
              "<p><a href=\"https://open.spotify.com/user/tripreads/playlist/57BxBZst4eqszK01wF0Bts\" target=\"_blank\" rel=\"noopener\"><strong>The Sounds of Andalucia | Flamenco</strong></a><br />\n" +
              "<strong>Listen to the Spotify playlist</strong> Listen to a collection of Andalucia&#8217;s signature music, flamenco. The perfect pre-trip soundtrack.</p>\n" +
              "<p><iframe src=\"https://open.spotify.com/embed?theme=white&amp;uri=https%3A%2F%2Fopen.spotify.com%2Fuser%2Ftripreads%2Fplaylist%2F57BxBZst4eqszK01wF0Bts\" width=\"300\" height=\"380\" frameborder=\"0\"></iframe></p>\n" +
              "<p><a href=\"http://amzn.to/2sVuA1e\" target=\"_blank\" rel=\"noopener\"><strong>Sherry, Manzanilla and Montilla</strong></a><br />\n" +
              "<strong>View on Amazon</strong> An expert shares his insights on this distinctive oxidized wine from southern Spain. Sure to improve your dining experiences. <em>By Peter Liem</em>.</p>\n" +
              "<p><a href=\"http://amzn.to/2uX965a\" target=\"_blank\" rel=\"noopener\"><strong>Ghosts of Spain</strong></a><br />\n" +
              "<strong>View on Amazon</strong> Part travel memoir, part social history and commentary, this engaging book highlights the unique aspects of Spain and how the past plays a critical role in the current culture. <em>By Giles Tremlett</em>.</p>\n" +
              "<p><a href=\"http://youtu.be/jZd-gw7Y2uQ\" target=\"_blank\" rel=\"noopener\"><strong>Mosque of Córdoba | A Walk to the Mezquita&#8217;s Mihrab</strong></a><br />\n" +
              "<strong>Watch on YouTube</strong> Talk a quick walk down the center aisle of the Mosque of Córdoba (la Mezquita) to view the spectacular mosaic-clad mihrab and unique eight-point dome over the maqsura. This section of the mosque was built by Al-Hakam II from 961-965.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/jZd-gw7Y2uQ?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-1&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-1\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"http://www.independent.co.uk/news/world/spains-roadside-bulls-live-to-fight-another-day-1439879.html\" target=\"_blank\" rel=\"noopener\"><strong>&#8220;Spain&#8217;s roadside bulls live to fight another day&#8221;</strong></a><br />\n" +
              "<strong>View on independent.co.uk</strong> After public outcry, a campaign by intellectuals and a political row, the gigantic black metal bulls that tower over Spanish highways will be allowed to stay put. <em>By Phil Davison</em>.</p>\n" +
              "<p><a href=\"https://soundcloud.com/desert-island-discs-96-00/paco-pena\" target=\"_blank\" rel=\"noopener\"><strong> Paco Peña Interview</strong></a><br />\n" +
              "<strong>Listen on soundcloud</strong> In this BBC podcast, the famous flamenco guitarist shares his perspective on the music, memories from growing up in Córdoba and his favorite records.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"100%\" height=\"166\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?visual=true&#038;url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F298596971&#038;show_artwork=true&#038;maxwidth=1500&#038;maxheight=1000\"></iframe></div>\n" +
              "<p><a href=\"http://www.approachguides.com\"><img class=\"alignright\" src=\"https://live.approachguides.com/collection/approachguides-mastercurators-2x.png\" alt=\"“Approach Guides - Master Curators\" width=\"180\" height=\"“44”/\" border=\"0\" /></a></p>\n" +
              "</div></section></div>");
    } else if(noteType == 7) {
      tn.setName("London by approachguides");
      tn.setIntro("<div class=\"flex_column av_one_full  flex_column_div av-zero-column-padding first  avia-builder-el-0  el_before_av_one_fifth  avia-builder-el-first  \" style='border-radius:0px; '><div style='padding-bottom:10px;' class='av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-1  avia-builder-el-no-sibling   av-thin-font'><h1 class='av-special-heading-tag'  itemprop=\"headline\"  ><strong>London</strong></h1><div class ='av-subheading av-subheading_below ' style='font-size:20px;'><p>Destination Inspiration</p>\n" +
              "</div><div class='special-heading-border'><div class='special-heading-inner-border' ></div></div></div></div>\n" +
              "<div class=\"flex_column av_one_fifth  flex_column_div av-zero-column-padding first  avia-builder-el-2  el_after_av_one_full  el_before_av_three_fifth  column-top-margin\" style='border-radius:0px; '></div>\n" +
              "<div class=\"flex_column av_three_fifth  flex_column_div av-zero-column-padding   avia-builder-el-3  el_after_av_one_fifth  el_before_av_one_fifth  column-top-margin\" style='border-radius:0px; '><section class=\"av_textblock_section\"  itemscope=\"itemscope\" itemtype=\"https://schema.org/CreativeWork\" ><div class='avia_textblock '   itemprop=\"text\" ><p><a href=\"http://amzn.to/2uoKxRW\" target=\"_blank\" rel=\"noopener\"><strong>84, Charing Cross Road</strong></a><br />\n" +
              "<strong>View on Amazon</strong> A charming story and collection of letters between Helene Hanff, a New York writer, and Frank Doel, a London bookseller. <em>By Helene Hanff</em>.</p>\n" +
              "<p><a href=\"http://amzn.to/2ufWmcq\" target=\"_blank\" rel=\"noopener\"><strong>A Guide to Wren&#8217;s Churches in London</strong></a><br />\n" +
              "<strong>View on Amazon</strong> An in-depth guide to the art and architecture of Christopher Wren&#8217;s top 10 churches in the old city. <em>By David Raezer and Jennifer Raezer</em>.</p>\n" +
              "<p><a href=\"http://youtu.be/LrObZ_HZZUc\" target=\"_blank\" rel=\"noopener\"><strong> The (Secret) City of London | History</strong></a><br />\n" +
              "<strong>Watch on YouTube</strong> An entertaining and thoughtful explanation of the difference between the City of London and London.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/LrObZ_HZZUc?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-1&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-1\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"http://www.bbc.com/future/story/20150928-the-hidden-world-that-moves-your-luggage\" target=\"_blank\" rel=\"noopener\"><strong>&#8220;The Hidden World That Moves Your Luggage&#8221;</strong></a><br />\n" +
              "<strong>View on bbc.com</strong> As we wander through duty-free at Heathrow on the way to our planes, a vast network is ensuring our bags travel with us. <em>By Jack Stewart</em>.</p>\n" +
              "<p><a href=\"https://youtu.be/0ed6lNl453E\" target=\"_blank\" rel=\"noopener\"><strong> The Great Fire of London | What impact did it have on the city?</strong></a><br />\n" +
              "<strong>Watch on YouTube</strong> 2016 marks the 350th anniversary of the Great Fire of London, which burned through the city for four days in September 1666. In this Kings College video, history professors Laura Gowing and Arthur Burns examine the impact of the fire on the city&#8217;s geography, housing and opportunities and consider how it affected the future of St Paul&#8217;s Cathedral.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/0ed6lNl453E?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-2&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-2\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"https://soundcloud.com/monocle-24-the-urbanist/the-urbanist-tall-stories-29\" target=\"_blank\" rel=\"noopener\"><strong>Tall Stories: London’s protected views</strong></a><br />\n" +
              "<strong>Listen on soundcloud</strong> In this The Urbanist podcast, we uncover the system of protected views that continues to stymie London’s vertical growth.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"100%\" height=\"166\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?visual=true&#038;url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F289714680&#038;show_artwork=true&#038;maxwidth=1500&#038;maxheight=1000\"></iframe></div>\n" +
              "<p><a href=\"http://amzn.to/2uoEUDf\" target=\"_blank\" rel=\"noopener\"><strong>Around the World in 80 Days</strong></a><br />\n" +
              "<strong>View on Amazon</strong> The timeless story about a legendary wager that prompts Phileas Fogg and his valet Passepartout to attempt a journey around the world in 80 days. <em>By Jules Verne</em>.</p>\n" +
              "<p><a href=\"https://www.nytimes.com/2017/07/04/world/europe/london-uk-brexit-uber-taxi.html\" target=\"_blank\" rel=\"noopener\"><strong> &#8220;On London’s Streets, Black Cabs and Uber Fight for a Future&#8221;</strong></a><br />\n" +
              "<strong>View on NYTimes.com</strong> London’s cabby wars are less about the disruptive power of an app, or a new business model, than about the disruption of Britain. <em>By Katrin Bennhold</em>.</p>\n" +
              "<p><a href=\"http://www.approachguides.com\"><img class=\"alignright\" src=\"https://live.approachguides.com/collection/approachguides-mastercurators-2x.png\" alt=\"“Approach Guides - Master Curators\" width=\"180\" height=\"“44”/\" border=\"0\" /></a></p>\n" +
              "</div></section></div>");
    } else if(noteType == 8) {
      tn.setName("Mexico by approachguides");
      tn.setIntro("<div class=\"flex_column av_one_full  flex_column_div av-zero-column-padding first  avia-builder-el-0  el_before_av_one_fifth  avia-builder-el-first  \" style='border-radius:0px; '><div style='padding-bottom:10px;' class='av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-1  avia-builder-el-no-sibling   av-thin-font'><h1 class='av-special-heading-tag'  itemprop=\"headline\"  ><strong>Mexico</strong></h1><div class ='av-subheading av-subheading_below ' style='font-size:20px;'><p>Destination Inspiration</p>\n" +
              "</div><div class='special-heading-border'><div class='special-heading-inner-border' ></div></div></div></div>\n" +
              "<div class=\"flex_column av_one_fifth  flex_column_div av-zero-column-padding first  avia-builder-el-2  el_after_av_one_full  el_before_av_three_fifth  column-top-margin\" style='border-radius:0px; '></div>\n" +
              "<div class=\"flex_column av_three_fifth  flex_column_div av-zero-column-padding   avia-builder-el-3  el_after_av_one_fifth  el_before_av_one_fifth  column-top-margin\" style='border-radius:0px; '><section class=\"av_textblock_section\"  itemscope=\"itemscope\" itemtype=\"https://schema.org/CreativeWork\" ><div class='avia_textblock '   itemprop=\"text\" ><p><a href=\"http://amzn.to/2tOpIhd\" target=\"_blank\" rel=\"noopener\"><strong>Like Water for Chocolate</strong></a><br />\n" +
              "<strong>View on Amazon</strong> Magical tale of family life in turn-of-the-century Mexico. <em>By Laura Esquivel</em></p>\n" +
              "<p><a href=\"http://amzn.to/2uko9YU\" target=\"_blank\" rel=\"noopener\"><strong>Mexico: The Cookbook</strong></a><br />\n" +
              "<strong>View on Amazon</strong> 700 authentic Mexican recipes that showcase the diverse flavors of Mexican cuisine. <em>By Margarita Carrillo Arronte</em>.</p>\n" +
              "<p><a href=\"https://vimeo.com/86991644\" target=\"_blank\" rel=\"noopener\"><strong>The Flower of Corn | Perennial Plate</strong></a><br />\n" +
              "<strong>Watch on Vimeo</strong> Beyond the taste of history and tradition, Amado, the &#8220;corn guru”, brings a poetic truth about the power of this ancient grain.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe src=\"https://player.vimeo.com/video/86991644?api=1&#038;player_id=embed-video-1&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-1\" width=\"95%\" frameborder=\"0\" title=\"The Flower of Corn\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>\n" +
              "<p><a href=\"http://amzn.to/2ufli3R\" target=\"_blank\" rel=\"noopener\"><strong>Guide to the Maya Ruins of Mexico</strong></a><br />\n" +
              "<strong>View on Amazon</strong> Insightful profiles of the top Maya sites in Mexico, as well as the Maya-connected civilizations. Featured sites include Chichen Itza, Tulum, Teotihuacan, Palenque, and more. <em>By David Raezer and Jennifer Raezer</em>.</p>\n" +
              "<p><a href=\"https://open.spotify.com/user/tripreads/playlist/2GATlXasl8N78WYyFtat5T\" target=\"_blank\" rel=\"noopener\"><strong>The Sounds of Mexico: Mariachi</strong></a><br />\n" +
              "<strong>Listen to the Spotify playlist</strong> Listen to a collection of Mexico&#8217;s signature music, mariachi, which arose in the region of Jalisco. The perfect pre-trip soundtrack.</p>\n" +
              "<p><iframe src=\"https://open.spotify.com/embed?theme=white&amp;uri=https%3A%2F%2Fopen.spotify.com%2Fuser%2Ftripreads%2Fplaylist%2F2GATlXasl8N78WYyFtat5T\" width=\"300\" height=\"380\" frameborder=\"0\"></iframe></p>\n" +
              "<p><a href=\"http://www.rollingstone.com/culture/features/el-chapo-speaks-20160109\" target=\"_blank\" rel=\"noopener\"><strong>&#8220;El Chapo Speaks&#8221;</strong></a><br />\n" +
              "<strong>View on Rollingstone.com</strong> The head of the Sinaloa drug cartel speaks with the famous American actor. <em>By Sean Penn</em>.</p>\n" +
              "<p><a href=\"https://youtu.be/T2BlqlR4a7A\" target=\"_blank\" rel=\"noopener\"><strong> Salma Hayek Teaches You Mexican Slang</strong></a><br />\n" +
              "<strong>Watch on YouTube</strong> In this Vanity Fair video, Salma Hayek walks through Mexican slang and translates the phrases from Spanish to English.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/T2BlqlR4a7A?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-2&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-2\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"http://www.approachguides.com\"><img class=\"alignright\" src=\"https://live.approachguides.com/collection/approachguides-mastercurators-2x.png\" alt=\"“Approach Guides - Master Curators\" width=\"180\" height=\"“44”/\" border=\"0\" /></a></p>\n" +
              "</div></section></div>");
    } else if(noteType == 9) {
      tn.setName("Portugal by approachguides");
      tn.setIntro("<div class=\"flex_column av_one_full  flex_column_div av-zero-column-padding first  avia-builder-el-0  el_before_av_one_fifth  avia-builder-el-first  \" style='border-radius:0px; '><div style='padding-bottom:10px;' class='av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-1  avia-builder-el-no-sibling   av-thin-font'><h1 class='av-special-heading-tag'  itemprop=\"headline\"  ><strong>Portugal</strong></h1><div class ='av-subheading av-subheading_below ' style='font-size:20px;'><p>Destination Inspiration</p>\n" +
              "</div><div class='special-heading-border'><div class='special-heading-inner-border' ></div></div></div></div>\n" +
              "<div class=\"flex_column av_one_fifth  flex_column_div av-zero-column-padding first  avia-builder-el-2  el_after_av_one_full  el_before_av_three_fifth  column-top-margin\" style='border-radius:0px; '></div>\n" +
              "<div class=\"flex_column av_three_fifth  flex_column_div av-zero-column-padding   avia-builder-el-3  el_after_av_one_fifth  el_before_av_one_fifth  column-top-margin\" style='border-radius:0px; '><section class=\"av_textblock_section\"  itemscope=\"itemscope\" itemtype=\"https://schema.org/CreativeWork\" ><div class='avia_textblock '   itemprop=\"text\" ><p><a href=\"http://amzn.to/2t8WdYw\" target=\"_blank\" rel=\"noopener\"><strong>Conquerors: How Portugal Forged the First Global Empire</strong></a><br />\n" +
              "<strong>View on Amazon</strong> A New York Times bestselling author tracks the rise of the Portuguese empire, aka Portuguese Overseas (Ultramar Português). <em>By Roger Crowley</em>.</p>\n" +
              "<p><a href=\"http://amzn.to/2sVOM30\" target=\"_blank\" rel=\"noopener\"><strong>Wines of Portugal</strong></a><br />\n" +
              "<strong>View on Amazon</strong> A detailed, thoughtful guide to the country&#8217;s distinctive grape varieties and wine-producing regions. <em>By David Raezer and Jennifer Raezer</em>.</p>\n" +
              "<p><a href=\"https://youtu.be/ZumdlxujMb0\" target=\"_blank\" rel=\"noopener\"><strong>Azulejos | Portugal&#8217;s Sensational Ceramic Tiles</strong></a><br />\n" +
              "<strong>Watch on YouTube</strong> Correspondent Martha Teichner travels throughout Portugal to tell the story of the country&#8217;s rich tradition of covering buildings with colorful ceramic tiles, called azulejos.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/ZumdlxujMb0?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-1&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-1\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"http://amzn.to/2sWgbBM\" target=\"_blank\" rel=\"noopener\"><strong>Baltasar and Blimunda</strong></a><br />\n" +
              "<strong>View on Amazon</strong> The Nobel Prize-winning author&#8217;s compelling love story set in eighteenth-century Portugal at the height of the Inquisition. <em>By José Saramago</em>.</p>\n" +
              "<p><a href=\"https://vimeo.com/7552407\" target=\"_blank\" rel=\"noopener\"><strong>Ginjinha sour-cherry brandy | Lisbon&#8217;s favourite tipple</strong></a><br />\n" +
              "<strong>Watch on Vimeo</strong> An introduction to Lisbon&#8217;s famous liqueur made by infusing sour cherries (ginja) in alcohol (aguardente).</p>\n" +
              "<div class='avia-iframe-wrap'><iframe src=\"https://player.vimeo.com/video/7552407?api=1&#038;player_id=embed-video-2&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-2\" width=\"95%\" frameborder=\"0\" title=\"Ginjinha - Lisbon&#039;s favourite tipple\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>\n" +
              "<p><a href=\"https://soundcloud.com/monocle-24-the-urbanist/the-urbanist-tall-stories-16\" target=\"_blank\" rel=\"noopener\"><strong>Tall Stories: Lisbon’s tram wires</strong></a><br />\n" +
              "<strong>Listen on soundcloud</strong> Scratching the blue sky above Lisbon, the electrical wires of the iconic yellow trams show us how the city has evolved over the years. The Urbanist explores.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"100%\" height=\"166\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?visual=true&#038;url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F275307255&#038;show_artwork=true&#038;maxwidth=1500&#038;maxheight=1000\"></iframe></div>\n" +
              "<p><a href=\"http://www.approachguides.com\"><img class=\"alignright\" src=\"https://live.approachguides.com/collection/approachguides-mastercurators-2x.png\" alt=\"“Approach Guides - Master Curators\" width=\"180\" height=\"“44”/\" border=\"0\" /></a></p>\n" +
              "</div></section></div>");
    } else if(noteType == 10) {
      tn.setName("Morocco by approachguides");
      tn.setIntro("<div class=\"flex_column av_one_full  flex_column_div av-zero-column-padding first  avia-builder-el-0  el_before_av_one_fifth  avia-builder-el-first  \" style='border-radius:0px; '><div style='padding-bottom:10px;' class='av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-1  avia-builder-el-no-sibling   av-thin-font'><h1 class='av-special-heading-tag'  itemprop=\"headline\"  ><strong>Morocco</strong></h1><div class ='av-subheading av-subheading_below ' style='font-size:20px;'><p>Destination Inspiration</p>\n" +
              "</div><div class='special-heading-border'><div class='special-heading-inner-border' ></div></div></div></div>\n" +
              "<div class=\"flex_column av_one_fifth  flex_column_div av-zero-column-padding first  avia-builder-el-2  el_after_av_one_full  el_before_av_three_fifth  column-top-margin\" style='border-radius:0px; '></div>\n" +
              "<div class=\"flex_column av_three_fifth  flex_column_div av-zero-column-padding   avia-builder-el-3  el_after_av_one_fifth  el_before_av_one_fifth  column-top-margin\" style='border-radius:0px; '><section class=\"av_textblock_section\"  itemscope=\"itemscope\" itemtype=\"https://schema.org/CreativeWork\" ><div class='avia_textblock '   itemprop=\"text\" ><p><a href=\"http://www.afar.com/magazine/moroccos-medina-marrakech-through-the-eyes-of-an-artist\" target=\"_blank\" rel=\"noopener\"><strong>&#8220;Marrakech Through the Eyes of An Artist&#8221;</strong></a><br />\n" +
              "<strong>View on Afar.com</strong> A local artist&#8217;s perspective on the city. By Gisela Williams.</p>\n" +
              "<p><a href=\"http://amzn.to/2t8GetM\" target=\"_blank\" rel=\"noopener\"><strong>Guide to the Architecture of Morocco</strong></a><br />\n" +
              "<strong>View on Amazon</strong> Morocco&#8217;s four imperial cities &#8211; Fez, Marrakech, Meknes and Rabat &#8211; hold a magnificent collection of Islamic art and architecture. Explore the top sights in each city with this destination guide. <em>By David Raezer and Jennifer Raezer</em>.</p>\n" +
              "<p><a href=\"https://youtu.be/2HwgPwXxjx8\" target=\"_blank\" rel=\"noopener\"><strong>Witness &#8211; A Marrakech Tale by Master Storyteller Ahmed Ezzarghani</strong></a><br />\n" +
              "<strong>Watch on YouTube</strong> Master storyteller Ahmed Ezzarghani and apprentice Sara are fighting to keep the Moroccan storytelling tradition alive.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/2HwgPwXxjx8?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-1&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-1\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"http://amzn.to/2sW3jeX\" target=\"_blank\" rel=\"noopener\"><strong>The Food of Morocco</strong></a><br />\n" +
              "<strong>View on Amazon</strong> Far and away, the best introduction to the country&#8217;s unrivaled cuisine. By Paula Wolfert.</p>\n" +
              "<p><a href=\"https://youtu.be/b4UFlldZuKk\" target=\"_blank\" rel=\"noopener\"><strong>Aziza Chaouni: How I Brought a River, and My City (Fez), Back to Life | TED Talks</strong></a><br />\n" +
              "<strong>Watch on YouTube</strong> The Fez River winds through the medina of Fez, Morocco—a mazelike medieval city that’s a World Heritage site. Once considered the “soul” of this celebrated city, the river succumbed to sewage and pollution, and in the 1950s was covered over bit by bit until nothing remained. TED Fellow Aziza Chaouni recounts her 20 year effort to restore this river to its former glory, and to transform her city in the process.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/b4UFlldZuKk?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-2&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-2\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"https://soundcloud.com/harvard-divinity-school/religion-gender-and-language-in-morocco\" target=\"_blank\" rel=\"noopener\"><strong>Religion, Gender, and Language in Morocco</strong></a><br />\n" +
              "<strong>Listen on soundcloud</strong> In this Harvard Divinity School lecture, academic Fatima Sadiqi sheds light on the pre-Islamic Berber language and religion in Morocco.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"100%\" height=\"166\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?visual=true&#038;url=https%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F150741249&#038;show_artwork=true&#038;maxwidth=1500&#038;maxheight=1000\"></iframe></div>\n" +
              "<p><a href=\"https://youtu.be/fOTPRbXUVbA\" target=\"_blank\" rel=\"noopener\"><strong>Leather Tannery | Fez</strong></a><br />\n" +
              "<strong>Watch on YouTube</strong> A look at the leather tanneries, including the process of curing and tanning hides, in Fes.</p>\n" +
              "<div class='avia-iframe-wrap'><iframe width=\"95%\" src=\"https://www.youtube.com/embed/fOTPRbXUVbA?feature=oembed&#038;enablejsapi=1&#038;player_id=embed-video-3&#038;origin=http%3A%2F%2Fwww.approachguides.com\" id=\"embed-video-3\" frameborder=\"0\" allowfullscreen></iframe></div>\n" +
              "<p><a href=\"http://www.approachguides.com\"><img class=\"alignright\" src=\"https://live.approachguides.com/collection/approachguides-mastercurators-2x.png\" alt=\"“Approach Guides - Master Curators\" width=\"180\" height=\"“44”/\" border=\"0\" /></a></p>\n" +
              "</div></section></div>");
    }

    if(tripDate != null && !tripDate.isEmpty()) {
      SimpleDateFormat f = new SimpleDateFormat("EEE MMM dd, yyyy");
      try {
        Date d = f.parse(tripDate);
        long milliseconds = d.getTime();
        tn.setNoteTimestamp(milliseconds);

      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    tn.save();
    BookingNoteController.invalidateCache(tripId);

    return redirect(routes.BookingController.bookings(tripId, new TabType.Bound(TabType.ITINERARY), null));
  }

  @With({Authenticated.class, Authorized.class})
  public Result parseTraveler() {
    String comment = request().body().asJson().textValue();


    ParseTravelerResponse pt = new ParseTravelerResponse();

    UmFlightReservation reservation = new UmFlightReservation();
    flightReservationBuilder.parseComment(reservation, comment);

    pt.departureTerminal = reservation.getFlight().departureTerminal;
    pt.arrivalTerminal = reservation.getFlight().arrivalTerminal;
    List<UmTraveler> travelers = reservation.getTravelers();
    if (travelers != null) {
      for (UmTraveler t : travelers) {
        UmFlightTraveler ft = cast(t, UmFlightTraveler.class);
        if (ft != null) {
          ParseTravelerResponse.Traveler traveler = new ParseTravelerResponse.Traveler();
          traveler.firstName = ft.getGivenName();
          traveler.lastName = ft.getFamilyName();
          traveler.eTicket = ft.getTicketNumber();
          traveler.frequentFlyerNumber = ft.getProgram().membershipNumber;
          traveler.seat = ft.getSeat();
          traveler.seatClass = ft.getSeatClass().seatCategory;
          traveler.meal = ft.getMeal();
          pt.addTraveler(traveler);
        }
      }
    }
    pt.comments = reservation.getNotesPlainText();
    return pt.toJsonResult();
  }

  @With({Authenticated.class, Authorized.class})
  public Result addBookingTags(String tripId, String detailsId, Boolean isNote) {
    JsonNode inTags = request().body().asJson();
    System.out.println("inTAGs: "+inTags);
    TripBookingDetailView.TripDetailTags tags = Json.fromJson(inTags, TripBookingDetailView.TripDetailTags.class);
    if (detailsId == null) {
      return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_FAIL);
    }
    try {
      Ebean.beginTransaction();
      if (isNote) {
        TripNote note = TripNote.find.byId(Long.parseLong(detailsId));
        if (note != null) {
          String newTag = processTags(note.getTag(), tags);
          note.setTag(newTag);
          note.update();
          BookingNoteController.invalidateCache(tripId);
        } else {
          return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_FAIL);
        }
      } else {
        TripDetail tripDetail = TripDetail.find.byId(detailsId);
        if (tripDetail != null) {
          String newTag = processTags(tripDetail.getTag(), tags);
          tripDetail.setTag(newTag);
          tripDetail.update();
          BookingController.invalidateCache(tripId, detailsId);
        } else {
          return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_FAIL);
        }
      }
      Ebean.commitTransaction();
    } catch (Exception e) {
      Ebean.rollbackTransaction();
      return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_FAIL);
    }

    //return BaseJsonResponse.codeResponse(ReturnCode.SUCCESS);
    return  ok(Json.toJson(tags));
  }

  public static String processTags (String prevTag, TripBookingDetailView.TripDetailTags tagCheck) {
    StringBuilder newTag = new StringBuilder();
    if (prevTag == null || prevTag.isEmpty()) {
      if (tagCheck.lastItem) {
        newTag.append(APPConstants.NOTE_TAG_LAST_ITEM);
      }
      if (tagCheck.pageBreakBefore) {
        if (newTag.length() > 0) {
          newTag.append(", ");
        }
        newTag.append(APPConstants.TAG_PAGE_BREAK_BEFORE);
      }
      if (tagCheck.pageBreakAfter) {
        if (newTag.length() > 0) {
          newTag.append(", ");
        }
        newTag.append(APPConstants.TAG_PAGE_BREAK_AFTER);
      }

    } else {
      newTag.append(prevTag);
      if (tagCheck.lastItem) {
        if (!newTag.toString().contains(APPConstants.NOTE_TAG_LAST_ITEM)) {
          newTag.append(", ");
          newTag.append(APPConstants.NOTE_TAG_LAST_ITEM);
        }
      } else {
        while (newTag.toString().contains(APPConstants.NOTE_TAG_LAST_ITEM)) {
          if (newTag.toString().contains(", " + APPConstants.NOTE_TAG_LAST_ITEM)) {
            int start = newTag.indexOf(", " + APPConstants.NOTE_TAG_LAST_ITEM);
            int end = start + APPConstants.NOTE_TAG_LAST_ITEM.length() + 2;
            newTag.delete(start, end);
          } else if (newTag.toString().contains(" " + APPConstants.NOTE_TAG_LAST_ITEM)) {
            int start = newTag.indexOf(" " + APPConstants.NOTE_TAG_LAST_ITEM);
            int end = start + APPConstants.NOTE_TAG_LAST_ITEM.length() + 1;
            newTag.delete(start, end);
          } else if (newTag.toString().contains(APPConstants.NOTE_TAG_LAST_ITEM)) {
            int start = newTag.indexOf(APPConstants.NOTE_TAG_LAST_ITEM);
            int end = start + APPConstants.NOTE_TAG_LAST_ITEM.length();
            newTag.delete(start, end);
          }
        }
      }

      if (tagCheck.pageBreakBefore) {
        if (!newTag.toString().contains(APPConstants.TAG_PAGE_BREAK_BEFORE)) {
          newTag.append(", ");
          newTag.append(APPConstants.TAG_PAGE_BREAK_BEFORE);
        }
      } else {
        while (newTag.toString().contains(APPConstants.TAG_PAGE_BREAK_BEFORE)) {
          if (newTag.toString().contains(", " + APPConstants.TAG_PAGE_BREAK_BEFORE)) {
            int start = newTag.indexOf(", " + APPConstants.TAG_PAGE_BREAK_BEFORE);
            int end = start + APPConstants.TAG_PAGE_BREAK_BEFORE.length() + 2;
            newTag.delete(start, end);
          } else if (newTag.toString().contains(" " + APPConstants.TAG_PAGE_BREAK_BEFORE)) {
            int start = newTag.indexOf(" " + APPConstants.TAG_PAGE_BREAK_BEFORE);
            int end = start + APPConstants.TAG_PAGE_BREAK_BEFORE.length() + 1;
            newTag.delete(start, end);
          } else if (newTag.toString().contains(APPConstants.TAG_PAGE_BREAK_BEFORE)) {
            int start = newTag.indexOf(APPConstants.TAG_PAGE_BREAK_BEFORE);
            int end = start + APPConstants.TAG_PAGE_BREAK_BEFORE.length();
            newTag.delete(start, end);
          }
        }
      }

      if (tagCheck.pageBreakAfter) {
        if (!newTag.toString().contains(APPConstants.TAG_PAGE_BREAK_AFTER)) {
          newTag.append(", ");
          newTag.append(APPConstants.TAG_PAGE_BREAK_AFTER);
        }
      } else {
        while (newTag.toString().contains(APPConstants.TAG_PAGE_BREAK_AFTER)) {
          if (newTag.toString().contains(", " + APPConstants.TAG_PAGE_BREAK_AFTER)) {
            int start = newTag.indexOf(", " + APPConstants.TAG_PAGE_BREAK_AFTER);
            int end = start + APPConstants.TAG_PAGE_BREAK_AFTER.length() + 2;
            newTag.delete(start, end);
          } else if (newTag.toString().contains(" " + APPConstants.TAG_PAGE_BREAK_AFTER)) {
            int start = newTag.indexOf(" " + APPConstants.TAG_PAGE_BREAK_AFTER);
            int end = start + APPConstants.TAG_PAGE_BREAK_AFTER.length() + 1;
            newTag.delete(start, end);
          } else if (newTag.toString().contains(APPConstants.TAG_PAGE_BREAK_AFTER)) {
            int start = newTag.indexOf(APPConstants.TAG_PAGE_BREAK_AFTER);
            int end = start + APPConstants.TAG_PAGE_BREAK_AFTER.length();
            newTag.delete(start, end);
          }
        }
      }
    }
    return newTag.toString();
  }

  private void updateCrossDateTag (FormBookingBase bookingBase, TripDetail tripDetail) {
    if (bookingBase.inCrossDateFlight) {
      String s = " " + APPConstants.TRIP_TAG_CROSS_DATE_FLIGHT;
      if (tripDetail.getTag() != null) {
        s = tripDetail.getTag() + s;
      }
      tripDetail.setTag(s.trim());
    } else if (tripDetail.getTag() != null && tripDetail.getTag().contains(APPConstants.TRIP_TAG_CROSS_DATE_FLIGHT)) {
      //remove the tag
      tripDetail.setTag(tripDetail.getTag().replaceAll(APPConstants.TRIP_TAG_CROSS_DATE_FLIGHT, "").trim());
    }
  }
}
