package com.umapped.external.afar;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by george on 2017-11-01.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "title",
    "slug",
    "category_type",
    "highlights",
    "destination_id",
    "city_id",
    "region_id",
    "country_id",
    "tag_id",
    "last_update"
})
public class AfarList {

  @JsonProperty("id")
  private Integer id;
  @JsonProperty("title")
  private String title;
  @JsonProperty("slug")
  private String slug;
  @JsonProperty("category_type")
  private String categoryType;
  @JsonProperty("highlights")
  private List<Highlight> highlights = null;
  @JsonProperty("destination_id")
  private Integer destinationId;
  @JsonProperty("city_id")
  private Integer cityId;
  @JsonProperty("region_id")
  private Integer regionId;
  @JsonProperty("country_id")
  private Integer countryId;
  @JsonProperty("tag_id")
  private Integer tagId;
  @JsonProperty("last_update")
  private String lastUpdate;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  @JsonProperty("id")
  public Integer getId() {
    return id;
  }

  @JsonProperty("id")
  public void setId(Integer id) {
    this.id = id;
  }

  @JsonProperty("title")
  public String getTitle() {
    return title;
  }

  @JsonProperty("title")
  public void setTitle(String title) {
    this.title = title;
  }

  @JsonProperty("slug")
  public String getSlug() {
    return slug;
  }

  @JsonProperty("slug")
  public void setSlug(String slug) {
    this.slug = slug;
  }

  @JsonProperty("category_type")
  public String getCategoryType() {
    return categoryType;
  }

  @JsonProperty("category_type")
  public void setCategoryType(String categoryType) {
    this.categoryType = categoryType;
  }

  @JsonProperty("highlights")
  public List<Highlight> getHighlights() {
    return highlights;
  }

  @JsonProperty("highlights")
  public void setHighlights(List<Highlight> highlights) {
    this.highlights = highlights;
  }

  @JsonProperty("destination_id")
  public Integer getDestinationId() {
    return destinationId;
  }

  @JsonProperty("destination_id")
  public void setDestinationId(Integer destinationId) {
    this.destinationId = destinationId;
  }

  @JsonProperty("city_id")
  public Integer getCityId() {
    return cityId;
  }

  @JsonProperty("city_id")
  public void setCityId(Integer cityId) {
    this.cityId = cityId;
  }

  @JsonProperty("region_id")
  public Integer getRegionId() {
    return regionId;
  }

  @JsonProperty("region_id")
  public void setRegionId(Integer regionId) {
    this.regionId = regionId;
  }

  @JsonProperty("country_id")
  public Integer getCountryId() {
    return countryId;
  }

  @JsonProperty("country_id")
  public void setCountryId(Integer countryId) {
    this.countryId = countryId;
  }

  @JsonProperty("tag_id")
  public Integer getTagId() {
    return tagId;
  }

  @JsonProperty("tag_id")
  public void setTagId(Integer tagId) {
    this.tagId = tagId;
  }

  @JsonProperty("last_update")
  public String getLastUpdate() {
    return lastUpdate;
  }

  @JsonProperty("last_update")
  public void setLastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

}
