package com.umapped.external.afar;


import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by george on 2017-11-02.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "slug",
    "region_id",
    "country_id",
    "lat",
    "lng",
    "last_update"
})
public class AfarCity {

  @JsonProperty("id")
  private Integer afarId;
  @JsonProperty("name")
  private String name;
  @JsonProperty("slug")
  private String slug;
  @JsonProperty("region_id")
  private Integer regionId;
  @JsonProperty("country_id")
  private Integer countryId;
  @JsonProperty("lat")
  private String lat;
  @JsonProperty("lng")
  private String lng;
  @JsonProperty("last_update")
  private String lastUpdate;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  @JsonProperty("id")
  public Integer getAfarId() {
    return afarId;
  }

  @JsonProperty("id")
  public void setAfarId(Integer afarId) {
    this.afarId = afarId;
  }

  @JsonProperty("name")
  public String getName() {
    return name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

  @JsonProperty("slug")
  public String getSlug() {
    return slug;
  }

  @JsonProperty("slug")
  public void setSlug(String slug) {
    this.slug = slug;
  }

  @JsonProperty("region_id")
  public Integer getRegionId() {
    return regionId;
  }

  @JsonProperty("region_id")
  public void setRegionId(Integer regionId) {
    this.regionId = regionId;
  }

  @JsonProperty("country_id")
  public Integer getCountryId() {
    return countryId;
  }

  @JsonProperty("country_id")
  public void setCountryId(Integer countryId) {
    this.countryId = countryId;
  }

  @JsonProperty("lat")
  public String getLat() {
    return lat;
  }

  @JsonProperty("lat")
  public void setLat(String lat) {
    this.lat = lat;
  }

  @JsonProperty("lng")
  public String getLng() {
    return lng;
  }

  @JsonProperty("lng")
  public void setLng(String lng) {
    this.lng = lng;
  }

  @JsonProperty("last_update")
  public String getLastUpdate() {
    return lastUpdate;
  }

  @JsonProperty("last_update")
  public void setLastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

}
