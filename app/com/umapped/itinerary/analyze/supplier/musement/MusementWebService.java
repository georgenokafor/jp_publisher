package com.umapped.itinerary.analyze.supplier.musement;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.name.Named;
import com.umapped.itinerary.analyze.recommendation.RecommendedItemType;
import com.umapped.itinerary.analyze.supplier.Supplier;
import com.umapped.itinerary.analyze.supplier.musement.response.Event;
import com.umapped.itinerary.analyze.supplier.musement.response.NetPrice;
import com.umapped.itinerary.analyze.supplier.musement.response.SearchEventResponse;
import com.umapped.itinerary.analyze.recommendation.RecommendedItem;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesWebService;
import com.umapped.itinerary.analyze.webservice.HttpResponseException;
import com.umapped.itinerary.analyze.webservice.WebAPIServiceRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.java8.FuturesConvertersImpl;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by wei on 2017-06-14.
 */
public class MusementWebService {
  private static final Logger LOG = LoggerFactory.getLogger(WCitiesWebService.class);

  private ObjectMapper objectMapper;
  private String authorizationCode;

  private static String EVENT_SEARCH_BY_CITY = "events/search-extended";

  private MusementWebServiceRequestHandler requestHandler;

  @Inject
  public MusementWebService(@Named("ItineraryAnalayzeObjectMapper")ObjectMapper objectMapper, MusementWebServiceRequestHandler requestHandler) {
    this.objectMapper = objectMapper;
    this.requestHandler = requestHandler;
  }

  public List<RecommendedItem> getEvents(String cityId, String cityName, List<String> verticals) {
    try {
      WebAPIServiceRequest request = buildRequest();
      request.addParam("city", cityId);

      SearchEventResponse result = requestHandler.get(EVENT_SEARCH_BY_CITY, request, SearchEventResponse.class);
      if (result.getData() != null) {
        List<RecommendedItem> items = new ArrayList<>();
        for (Event event : result.getData()) {
          items.add(map(event));
        }
        return items;
      } else {
        return Collections.emptyList();
      }
    } catch (HttpResponseException e) {
      throw new RuntimeException(e);
    }
  }

  private RecommendedItem map(Event event) {
    RecommendedItem item = new RecommendedItem();
    item.type = RecommendedItemType.Activity;
    item.name = event.getTitle();
    item.description = event.getDescription();
    item.supplier = Supplier.Musement;
    item.imageUrl = event.getCoverImageUrl();
    NetPrice netPrice = event.getNetPrice();
    item.prices = new ArrayList<>();
    item.prices.add(map(netPrice));
    return item;
  }

  private RecommendedItem.Price map(NetPrice netPrice) {
    return new RecommendedItem.Price(netPrice.getFormattedValue(), netPrice.getCurrency(), "");
  }

  private WebAPIServiceRequest buildRequest() {
    WebAPIServiceRequest request = new WebAPIServiceRequest();

    return request;
  }
}
