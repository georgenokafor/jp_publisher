package models.publisher;


import com.avaje.ebean.BeanState;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.utils.Utils;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-01
 * Time: 11:25 AM
 * To change this template use File | Settings | File Templates.
 */

@Entity @Table(name = "destination_guide")
public class DestinationGuide
    extends Model {
  public static Model.Finder<String, DestinationGuide> find = new Finder(DestinationGuide.class);
  public float loclat;
  public float loclong;
  public String tag;
  public String intro;
  public String description;
  public String streetaddr1;
  public String city;
  public String zipcode;
  public String state;
  public String country;
  public String templateid;
  public String landmark;
  public Long destguidetimestamp;
  @Id
  public String destinationguideid;
  @Constraints.Required
  public String name;
  public String destinationid;
  public int status;
  public int rank;
  public int recommendationtype;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;
  private String pageId;

  /**
   * Takes this object makes it possible to re-insert as new record
   * @return
   */
  public DestinationGuide prepareForReuse(String createdBy, String modifiedBy) {
    BeanState state = Ebean.getBeanState(this);
    state.resetForInsert();
    setDestinationguideid(DBConnectionMgr.getUniqueId());
    setModifiedby(modifiedBy);
    setCreatedby(createdBy);
    setLastupdatedtimestamp(System.currentTimeMillis());
    setCreatedtimestamp(lastupdatedtimestamp);
    return this;
  }

  public static DestinationGuide buildGuide(Destination dest, String userId){
    DestinationGuide dg = new DestinationGuide();
    dg.setDestinationguideid(DBConnectionMgr.getUniqueId());
    dg.setDestinationid(dest.getDestinationid());
    dg.setCreatedtimestamp(System.currentTimeMillis());
    dg.setLastupdatedtimestamp(dg.createdtimestamp);
    dg.setCreatedby(userId);
    dg.setModifiedby(userId);
    dg.status = APPConstants.STATUS_ACTIVE;
    dg.setRecommendationtype(APPConstants.RECOMMENDATION_TYPE_UNDEFINED);
    return dg;
  }


  public static int maxPageRankByDestId(String destinationid) {
    return find.where().eq("destinationid", destinationid).findCount();

  }

  public static int activeCount(String destinationid) {
    return find.where().eq("destinationid", destinationid).eq("status", 0).findCount();

  }

  public static List<DestinationGuide> findActiveDestGuides(String destinationid) {
    return find.where().eq("destinationid", destinationid).eq("status", 0).orderBy("rank asc").findList();

  }

  public static List<DestinationGuide> findMostRecentActiveForCmpy(List<Integer> types, String cmpyId, int startPos, SessionMgr sessionMgr) {
    com.avaje.ebean.Query<Destination> validDocs = null;

    if (SecurityMgr.isCmpyAdmin(cmpyId, sessionMgr)) {
      validDocs = Ebean.createQuery(Destination.class)
          .select("destinationid")
          .where()
          .eq("cmpyid", cmpyId)
          .eq("status", APPConstants.STATUS_ACTIVE)
          .in("destinationtypeid", types)
          .query().orderBy("name");
    } else {
      validDocs = Ebean.createQuery(Destination.class)
          .select("destinationid")
          .where()
          .eq("cmpyid", cmpyId)
          .or(Expr.isNull("access_level"), Expr.or(Expr.eq("access_level", Destination.AccessLevel.PUBLIC), Expr.and(Expr.eq("access_level", Destination.AccessLevel.PRIVATE), Expr.eq("createdby", sessionMgr.getCredentials().getUserId()))))
          .eq("status", APPConstants.STATUS_ACTIVE)
          .in("destinationtypeid", types)
          .query().orderBy("name");
    }

    return find.where()
        .in("destinationid", validDocs)
        .eq("status", APPConstants.STATUS_ACTIVE)
        .orderBy("lastupdatedtimestamp desc, name asc")
        .setFirstRow(startPos)
        .findList();
  }

  public static List<DestinationGuide> findDestGuidesByName(String name, String destinationid) {
    return find.where()
               .eq("destinationid", destinationid)
               .eq("status", 0)
               .ieq("name", name)
               .orderBy("rank asc")
               .findList();
  }

  public static List<DestinationGuide> findByNameForCompany(String term, String cmpyId, List<Integer> types, SessionMgr sessionMgr) {

    com.avaje.ebean.Query<Destination> validDocs = null;

    if (SecurityMgr.isCmpyAdmin(cmpyId, sessionMgr)) {
      validDocs = Ebean.createQuery(Destination.class)
          .select("destinationid")
          .where()
          .eq("cmpyid", cmpyId)
          .eq("status", APPConstants.STATUS_ACTIVE)
          .in("destinationtypeid", types)
          .query().orderBy("name");
    } else {
      validDocs = Ebean.createQuery(Destination.class)
          .select("destinationid")
          .where()
          .eq("cmpyid", cmpyId)
          .or(Expr.isNull("access_level"), Expr.or(Expr.eq("access_level", Destination.AccessLevel.PUBLIC), Expr.and(Expr.eq("access_level", Destination.AccessLevel.PRIVATE), Expr.eq("createdby", sessionMgr.getCredentials().getUserId()))))
          .eq("status", APPConstants.STATUS_ACTIVE)
          .in("destinationtypeid", types)
          .query().orderBy("name");
    }

    return find.where()
               .in("destinationid", validDocs)
               .eq("status", APPConstants.STATUS_ACTIVE)
               .or(Expr.ilike("tag", term), Expr.ilike("name", term))
               .orderBy("lastupdatedtimestamp desc, name asc")
               .findList();
  }

  public static List<DestinationGuide> findByNameForCompanyDT(String term, String cmpyId, List<Integer> types, int startPos, int maxRows, String orderBy, SessionMgr sessionMgr) {

    com.avaje.ebean.Query<Destination> validDocs = null;

    if (SecurityMgr.isCmpyAdmin(cmpyId, sessionMgr)) {
      validDocs = Ebean.createQuery(Destination.class)
          .select("destinationid")
          .where()
          .eq("cmpyid", cmpyId)
          .eq("status", APPConstants.STATUS_ACTIVE)
          .in("destinationtypeid", types)
          .query().orderBy("name");
    } else {
      validDocs = Ebean.createQuery(Destination.class)
          .select("destinationid")
          .where()
          .eq("cmpyid", cmpyId)
          .or(Expr.isNull("access_level"), Expr.or(Expr.eq("access_level", Destination.AccessLevel.PUBLIC), Expr.and(Expr.eq("access_level", Destination.AccessLevel.PRIVATE), Expr.eq("createdby", sessionMgr.getCredentials().getUserId()))))
          .eq("status", APPConstants.STATUS_ACTIVE)
          .in("destinationtypeid", types)
          .query().orderBy("name");
    }

    return find.where()
        .in("destinationid", validDocs)
        .eq("status", APPConstants.STATUS_ACTIVE)
        .or(Expr.ilike("tag", term), Expr.ilike("name", term))
        .setFirstRow(startPos)
        .setMaxRows(maxRows)
        .orderBy(orderBy)
        .findList();
  }

  public static int countByNameForCompany(String term, String cmpyId, List<Integer> types, SessionMgr sessionMgr) {

    com.avaje.ebean.Query<Destination> validDocs = null;

    if (SecurityMgr.isCmpyAdmin(cmpyId, sessionMgr)) {
      validDocs = Ebean.createQuery(Destination.class)
          .select("destinationid")
          .where()
          .eq("cmpyid", cmpyId)
          .eq("status", APPConstants.STATUS_ACTIVE)
          .in("destinationtypeid", types)
          .query().orderBy("name");
    } else {
      validDocs = Ebean.createQuery(Destination.class)
          .select("destinationid")
          .where()
          .eq("cmpyid", cmpyId)
          .or(Expr.isNull("access_level"), Expr.or(Expr.eq("access_level", Destination.AccessLevel.PUBLIC), Expr.and(Expr.eq("access_level", Destination.AccessLevel.PRIVATE), Expr.eq("createdby", sessionMgr.getCredentials().getUserId()))))
          .eq("status", APPConstants.STATUS_ACTIVE)
          .in("destinationtypeid", types)
          .query().orderBy("name");
    }

    return find.where()
        .in("destinationid", validDocs)
        .eq("status", APPConstants.STATUS_ACTIVE)
        .or(Expr.ilike("tag", term), Expr.ilike("name", term))
        .orderBy("lastupdatedtimestamp desc, name asc")
        .findCount();
  }

  public static int countByTermForCompany(String term, String cmpyId, List<Integer> types, SessionMgr sessionMgr) {

    com.avaje.ebean.Query<Destination> validDocs = null;

    if (SecurityMgr.isCmpyAdmin(cmpyId, sessionMgr)) {
      validDocs = Ebean.createQuery(Destination.class)
          .select("destinationid")
          .where()
          .eq("cmpyid", cmpyId)
          .eq("status", APPConstants.STATUS_ACTIVE)
          .in("destinationtypeid", types)
          .query().orderBy("name");
    } else {
      validDocs = Ebean.createQuery(Destination.class)
          .select("destinationid")
          .where()
          .eq("cmpyid", cmpyId)
          .or(Expr.isNull("access_level"), Expr.or(Expr.eq("access_level", Destination.AccessLevel.PUBLIC), Expr.and(Expr.eq("access_level", Destination.AccessLevel.PRIVATE), Expr.eq("createdby", sessionMgr.getCredentials().getUserId()))))
          .eq("status", APPConstants.STATUS_ACTIVE)
          .in("destinationtypeid", types)
          .query().orderBy("name");
    }

    return find.where()
        .in("destinationid", validDocs)
        .eq("status", APPConstants.STATUS_ACTIVE)
        .or(Expr.or(Expr.ilike("tag", term), Expr.ilike("name", term)),
            Expr.or(Expr.ilike("intro",term), Expr.ilike("description",term)))
        .orderBy("lastupdatedtimestamp desc, name asc")
        .findCount();
  }

  public static List<DestinationGuide> findByTermForCompany(String term, String cmpyId, List<Integer> types, SessionMgr sessionMgr) {

    com.avaje.ebean.Query<Destination> validDocs = null;

    if (SecurityMgr.isCmpyAdmin(cmpyId, sessionMgr)) {
      validDocs = Ebean.createQuery(Destination.class)
          .select("destinationid")
          .where()
          .eq("cmpyid", cmpyId)
          .eq("status", APPConstants.STATUS_ACTIVE)
          .in("destinationtypeid", types)
          .query().orderBy("name");
    } else {
      validDocs = Ebean.createQuery(Destination.class)
          .select("destinationid")
          .where()
          .eq("cmpyid", cmpyId)
          .or(Expr.isNull("access_level"), Expr.or(Expr.eq("access_level", Destination.AccessLevel.PUBLIC), Expr.and(Expr.eq("access_level", Destination.AccessLevel.PRIVATE), Expr.eq("createdby", sessionMgr.getCredentials().getUserId()))))
          .eq("status", APPConstants.STATUS_ACTIVE)
          .in("destinationtypeid", types)
          .query().orderBy("name");
    }

    return find.where()
               .in("destinationid", validDocs)
               .eq("status", APPConstants.STATUS_ACTIVE)
               .or(Expr.or(Expr.ilike("tag", term), Expr.ilike("name", term)),
                   Expr.or(Expr.ilike("intro",term), Expr.ilike("description",term)))
               .orderBy("lastupdatedtimestamp desc, name asc")
               .findList();
  }

  public static List<DestinationGuide> findByTermForCompanyDT(String term, String cmpyId, List<Integer> types, int startPos, int maxRows, String orderBy, SessionMgr sessionMgr) {

    com.avaje.ebean.Query<Destination> validDocs = null;

    if (SecurityMgr.isCmpyAdmin(cmpyId, sessionMgr)) {
      validDocs = Ebean.createQuery(Destination.class)
          .select("destinationid")
          .where()
          .eq("cmpyid", cmpyId)
          .eq("status", APPConstants.STATUS_ACTIVE)
          .in("destinationtypeid", types)
          .query().orderBy("name");
    } else {
      validDocs = Ebean.createQuery(Destination.class)
          .select("destinationid")
          .where()
          .eq("cmpyid", cmpyId)
          .or(Expr.isNull("access_level"), Expr.or(Expr.eq("access_level", Destination.AccessLevel.PUBLIC), Expr.and(Expr.eq("access_level", Destination.AccessLevel.PRIVATE), Expr.eq("createdby", sessionMgr.getCredentials().getUserId()))))
          .eq("status", APPConstants.STATUS_ACTIVE)
          .in("destinationtypeid", types)
          .query().orderBy("name");
    }

    return find.where()
        .in("destinationid", validDocs)
        .eq("status", APPConstants.STATUS_ACTIVE)
        .or(Expr.or(Expr.ilike("tag", term), Expr.ilike("name", term)),
            Expr.or(Expr.ilike("intro",term), Expr.ilike("description",term)))
        .setFirstRow(startPos)
        .setMaxRows(maxRows)
        .orderBy(orderBy)
        .findList();
  }


  public static List<DestinationGuide> findDestGuidesByNameTemplateId(String name,
                                                                      String templateid,
                                                                      String destinationid) {
    return find.where()
               .eq("destinationid", destinationid)
               .eq("status", 0)
               .eq("name", name)
               .eq("templateid", templateid)
               .orderBy("rank asc")
               .findList();

  }

  public String getPageId() {
    return pageId;
  }

  public void setPageId(String pageId) {
    this.pageId = pageId;
  }

  public float getLoclat() {
    return loclat;
  }

  public void setLoclat(float loclat) {
    this.loclat = loclat;
  }

  public float getLoclong() {
    return loclong;
  }

  public void setLoclong(float loclong) {
    this.loclong = loclong;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public String getIntro() {
    return intro;
  }

  public void setIntro(String intro) {
    this.intro = Utils.sanitizeHtmlFromCKEditor(intro);
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = Utils.sanitizeHtmlFromCKEditor(description);
  }

  public String getStreetaddr1() {
    return streetaddr1;
  }

  public void setStreetaddr1(String streetAddr1) {
    this.streetaddr1 = streetAddr1;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getTemplateid() {
    return templateid;
  }

  public void setTemplateid(String templateId) {
    this.templateid = templateId;
  }

  public String getDestinationguideid() {
    return destinationguideid;
  }

  public void setDestinationguideid(String destinationguideid) {
    this.destinationguideid = destinationguideid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDestinationid() {
    return destinationid;
  }

  public void setDestinationid(String destinationid) {
    this.destinationid = destinationid;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getLandmark() {
    return landmark;
  }

  public void setLandmark(String landmark) {
    this.landmark = landmark;
  }

  public int getRecommendationtype() {
    return recommendationtype;
  }

  public void setRecommendationtype(int recommendationtype) {
    this.recommendationtype = recommendationtype;
  }

  public Long getDestguidetimestamp() {
    return destguidetimestamp;
  }

  public void setDestguidetimestamp(Long destguidetimestamp) {
    this.destguidetimestamp = destguidetimestamp;
  }

  public Timestamp getCreatedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getCreatedtimestamp()));
  }

  public Timestamp getModifiedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getLastupdatedtimestamp()));
  }
}
