package com.umapped.external.wetu.itinerary.details;

/**
 * Created by george on 2017-06-05.
 */

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "mode",
    "first_content_entity_id",
    "second_content_entity_id",
    "agency",
    "vehicle",
    "reference_codes",
    "notes",
    "day",
    "first_time",
    "second_time",
    "directions",
    "contact_numbers",
    "first_location",
    "second_location",
    "ticket_class",
    "check_in_time",
    "duration",
    "distance"
})
public class TravelArrangement {
  @JsonProperty("mode")
  private String mode;
  @JsonProperty("first_content_entity_id")
  private int firstContentEntityId;
  @JsonProperty("second_content_entity_id")
  private int secondContentEntityId;
  @JsonProperty("agency")
  private String agency;
  @JsonProperty("vehicle")
  private String vehicle;
  @JsonProperty("reference_codes")
  private List<String> referenceCodes;
  @JsonProperty("notes")
  private String notes;
  @JsonProperty("day")
  private int day;
  @JsonProperty("first_time")
  private String firstTime;
  @JsonProperty("second_time")
  private String secondTime;
  @JsonProperty("directions")
  private List<String> directions;
  @JsonProperty("contact_numbers")
  private List<String> contactNumbers;
  @JsonProperty("first_location")
  private String firstLocation;
  @JsonProperty("second_location")
  private String secondLocation;
  @JsonProperty("ticket_class")
  private String ticketClass;
  @JsonProperty("check_in_time")
  private String checkInTime;
  @JsonProperty("duration")
  private String duration;
  @JsonProperty("distance")
  private double distance;

  @JsonProperty("mode")
  public String getMode() {
    return mode;
  }

  @JsonProperty("mode")
  public void setMode(String mode) {
    this.mode = mode;
  }

  @JsonProperty("first_content_entity_id")
  public int getFirstContentEntityId() {
    return firstContentEntityId;
  }

  @JsonProperty("first_content_entity_id")
  public void setFirstContentEntityId(int firstContentEntityId) {
    this.firstContentEntityId = firstContentEntityId;
  }

  @JsonProperty("second_content_entity_id")
  public int getSecondContentEntityId() {
    return secondContentEntityId;
  }

  @JsonProperty("second_content_entity_id")
  public void setSecondContentEntityId(int secondContentEntityId) {
    this.secondContentEntityId = secondContentEntityId;
  }

  @JsonProperty("agency")
  public String getAgency() {
    return agency;
  }

  @JsonProperty("agency")
  public void setAgency(String agency) {
    this.agency = agency;
  }

  @JsonProperty("vehicle")
  public String getVehicle() {
    return vehicle;
  }

  @JsonProperty("vehicle")
  public void setVehicle(String vehicle) {
    this.vehicle = vehicle;
  }

  @JsonProperty("reference_codes")
  public List<String> getReferenceCodes() {
    return referenceCodes;
  }

  @JsonProperty("reference_codes")
  public void setReferenceCodes(List<String> referenceCodes) {
    this.referenceCodes = referenceCodes;
  }

  @JsonProperty("notes")
  public String getNotes() {
    return notes;
  }

  @JsonProperty("notes")
  public void setNotes(String notes) {
    this.notes = notes;
  }

  @JsonProperty("day")
  public int getDay() {
    return day;
  }

  @JsonProperty("day")
  public void setDay(int day) {
    this.day = day;
  }

  @JsonProperty("first_time")
  public String getFirstTime() {
    return firstTime;
  }

  @JsonProperty("first_time")
  public void setFirstTime(String firstTime) {
    this.firstTime = firstTime;
  }

  @JsonProperty("second_time")
  public String getSecondTime() {
    return secondTime;
  }

  @JsonProperty("second_time")
  public void setSecondTime(String secondTime) {
    this.secondTime = secondTime;
  }

  @JsonProperty("directions")
  public List<String> getDirections() {
    return directions;
  }

  @JsonProperty("directions")
  public void setDirections(List<String> directions) {
    this.directions = directions;
  }

  @JsonProperty("contact_numbers")
  public List<String> getContactNumbers() {
    return contactNumbers;
  }

  @JsonProperty("contact_numbers")
  public void setContactNumbers(List<String> contactNumbers) {
    this.contactNumbers = contactNumbers;
  }

  @JsonProperty("first_location")
  public String getFirstLocation() {
    return firstLocation;
  }

  @JsonProperty("first_location")
  public void setFirstLocation(String firstLocation) {
    this.firstLocation = firstLocation;
  }

  @JsonProperty("second_location")
  public String getSecondLocation() {
    return secondLocation;
  }

  @JsonProperty("second_location")
  public void setSecondLocation(String secondLocation) {
    this.secondLocation = secondLocation;
  }

  @JsonProperty("ticket_class")
  public String getTicketClass() {
    return ticketClass;
  }

  @JsonProperty("ticket_class")
  public void setTicketClass(String ticketClass) {
    this.ticketClass = ticketClass;
  }

  @JsonProperty("check_in_time")
  public String getCheckInTime() {
    return checkInTime;
  }

  @JsonProperty("check_in_time")
  public void setCheckInTime(String checkInTime) {
    this.checkInTime = checkInTime;
  }

  @JsonProperty("duration")
  public String getDuration() {
    return duration;
  }

  @JsonProperty("duration")
  public void setDuration(String duration) {
    this.duration = duration;
  }

  @JsonProperty("distance")
  public double getDistance() {
    return distance;
  }

  @JsonProperty("distance")
  public void setDistance(double distance) {
    this.distance = distance;
  }
}
