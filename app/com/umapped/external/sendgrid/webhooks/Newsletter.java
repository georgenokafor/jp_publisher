
package com.umapped.external.sendgrid.webhooks;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "newsletter_user_list_id",
    "newsletter_id",
    "newsletter_send_id"
})
public class Newsletter {

    @JsonProperty("newsletter_user_list_id")
    private String newsletterUserListId;
    @JsonProperty("newsletter_id")
    private String newsletterId;
    @JsonProperty("newsletter_send_id")
    private String newsletterSendId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The newsletterUserListId
     */
    @JsonProperty("newsletter_user_list_id")
    public String getNewsletterUserListId() {
        return newsletterUserListId;
    }

    /**
     * 
     * @param newsletterUserListId
     *     The newsletter_user_list_id
     */
    @JsonProperty("newsletter_user_list_id")
    public void setNewsletterUserListId(String newsletterUserListId) {
        this.newsletterUserListId = newsletterUserListId;
    }

    /**
     * 
     * @return
     *     The newsletterId
     */
    @JsonProperty("newsletter_id")
    public String getNewsletterId() {
        return newsletterId;
    }

    /**
     * 
     * @param newsletterId
     *     The newsletter_id
     */
    @JsonProperty("newsletter_id")
    public void setNewsletterId(String newsletterId) {
        this.newsletterId = newsletterId;
    }

    /**
     * 
     * @return
     *     The newsletterSendId
     */
    @JsonProperty("newsletter_send_id")
    public String getNewsletterSendId() {
        return newsletterSendId;
    }

    /**
     * 
     * @param newsletterSendId
     *     The newsletter_send_id
     */
    @JsonProperty("newsletter_send_id")
    public void setNewsletterSendId(String newsletterSendId) {
        this.newsletterSendId = newsletterSendId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(newsletterUserListId).append(newsletterId).append(newsletterSendId).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Newsletter) == false) {
            return false;
        }
        Newsletter rhs = ((Newsletter) other);
        return new EqualsBuilder().append(newsletterUserListId, rhs.newsletterUserListId).append(newsletterId, rhs.newsletterId).append(newsletterSendId, rhs.newsletterSendId).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
