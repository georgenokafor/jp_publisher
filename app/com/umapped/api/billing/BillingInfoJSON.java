package com.umapped.api.billing;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.mapped.publisher.js.BaseJsonResponse;
import com.umapped.api.schema.types.ReturnCode;

import java.io.Serializable;
import java.util.List;

/**
 * Created by twong on 2016-10-18.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BillingInfoJSON
    extends BaseJsonResponse {
  public BillingInfoJSON(){

  }

  public BillingAddress bilAddr;
  public boolean hasPymt;

  public BillingPlan myBilPlan;

  public List<BillingPlan> billedToMe;

  public String btreeToken; //braintree JS token for authentication
  public String custId;

  public BillingInfoJSON(ReturnCode rc) {
    super(rc);
  }


}
