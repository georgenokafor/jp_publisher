package com.umapped.persistence.enums;

/**
 * Created by twong on 01/03/17.
 */
//public class WebItineraryProxy

import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import java.util.Map;
import java.util.Optional;

/** Types of tabs used in a variety of views */
public enum WebItineraryProxy {
    SKI,
    MAGNATECH,
    UNDEFINED;

    /**
     * Class to allow specifying parameter from enum in Play Framework.
     */
    public static class Bound
            implements QueryStringBindable<Bound>, PathBindable<Bound> {
        private WebItineraryProxy value;

        /**
         * This empty constructor must be there, implicit one is not recognized
         * when other constructors are present.
         */
        public Bound() {
            this.value = null;
        }

        public Bound(WebItineraryProxy proxy) {
            this.value = proxy;
        }

        @Override
        public Bound bind(String key, String txt) {
            this.value = WebItineraryProxy.valueOf(txt.toUpperCase());
            return this;
        }

        @Override
        public Optional<Bound> bind(String key, Map<String, String[]> params) {
            String[] arr = params.get(key);
            if (arr != null && arr.length > 0) {
                this.value = WebItineraryProxy.valueOf(arr[0]);
                return Optional.of(this);
            }
            else {
                return Optional.empty();
            }
        }

        @Override
        public String unbind(String key) {
            return this.value.name();
        }

        @Override
        public String javascriptUnbind() {
            return this.value.name();
        }

        public WebItineraryProxy value() {
            return this.value;
        }
    }


    public static WebItineraryProxy fromString(String name) {
        try {
            return WebItineraryProxy.valueOf(name.toUpperCase());
        }
        catch (Exception e) {
            //Nothing to do here.
        }
        return UNDEFINED;
    }
}