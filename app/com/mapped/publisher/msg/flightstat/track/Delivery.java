
package com.mapped.publisher.msg.flightstat.track;

import java.util.List;

public class Delivery{
   	private String destination;
   	private String format;

 	public String getDestination(){
		return this.destination;
	}
	public void setDestination(String destination){
		this.destination = destination;
	}
 	public String getFormat(){
		return this.format;
	}
	public void setFormat(String format){
		this.format = format;
	}
}
