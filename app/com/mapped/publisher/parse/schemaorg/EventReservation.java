package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by surge on 2015-05-12.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class EventReservation
    extends Reservation {

  /**
   * Can use Event, or any of the event subtypes, including BusinessEvent, ChildrenEvent, ComedyEvent, DanceEvent,
   * EducationEvent, Festival, FoodEvent, LiteraryEvent, MovieShowing, MusicEvent, SaleEvent, SocialEvent,
   * SportsEvent, TheaterEvent, VisualArtsEvent.
   */
  public Event reservationFor;
  /**
   * [Number] The number of seats.
   */
  public Integer numSeats;
  /**
   * Text	The seat number.
   */
  public String venueSeat;
  /**
   * Text	The seat's row.
   */
  public String venueRow;
  /**
   * Text	The seat's section.
   */
  public String venueSection;
  /**
   * Text	The number or id of the ticket.
   */
  public String ticketNumber;
  /**
   * URL	Where the ticket can be downloaded.
   */
  public String ticketDownloadUrl;
  /**
   * URL	Where the ticket can be printed.
   */
  public String ticketPrintUrl;
  /**
   * Text or URL	If the barcode image is hosted on your site, the value of the field is URL of the image,
   * or a barcode or QR URI, such as "barcode128:AB34" (ISO-15417 barcodes), "qrCode:AB34" (QR codes),
   * "aztecCode:AB34" (Aztec codes), "barcodeEAN:1234" (EAN codes) and "barcodeUPCA:1234" (UPCA codes).
   */
  public String ticketToken;
  /**
   * Text	Additional information about the ticket.
   */
  public String additionalTicketText;


  @Override
  public DateTime getStartDateTime() {
    return (reservationFor != null) ? reservationFor.startDate : null;
  }

  @Override
  public DateTime getFinishDateTime() {
    return (reservationFor != null) ? reservationFor.endDate : null;
  }
}
