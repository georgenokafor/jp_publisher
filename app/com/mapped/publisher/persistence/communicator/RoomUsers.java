package com.mapped.publisher.persistence.communicator;

import java.util.Map;

/**
 * Created by surge on 2016-02-05.
 */
public class RoomUsers {

  public static class CSession {
    public String id;
    public String name;
  }

  Map<String, Map<String,CSession>> sessions;

}
