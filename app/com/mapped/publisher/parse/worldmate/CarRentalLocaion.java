
package com.mapped.publisher.parse.worldmate;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for car-rental-locaion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="car-rental-locaion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="airport-code" type="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}airport-code" minOccurs="0"/>
 *         &lt;element name="local-date-time" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="utc-date-time" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="opening-hours" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="address" type="{http://www.worldmate.com/schemas/worldmate-api-v1.xsd}address"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "car-rental-locaion", namespace = "http://www.worldmate.com/schemas/worldmate-api-v1.xsd", propOrder = {

})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
public class CarRentalLocaion {

    @XmlElement(name = "airport-code")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected String airportCode;
    @XmlElement(name = "local-date-time", required = true)
    @XmlSchemaType(name = "dateTime")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected XMLGregorianCalendar localDateTime;
    @XmlElement(name = "utc-date-time", required = true)
    @XmlSchemaType(name = "dateTime")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected XMLGregorianCalendar utcDateTime;
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected String phone;
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected String fax;
    @XmlElement(name = "opening-hours")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected String openingHours;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    protected Address address;

    /**
     * Gets the value of the airportCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public String getAirportCode() {
        return airportCode;
    }

    /**
     * Sets the value of the airportCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setAirportCode(String value) {
        this.airportCode = value;
    }

    /**
     * Gets the value of the localDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public XMLGregorianCalendar getLocalDateTime() {
        return localDateTime;
    }

    /**
     * Sets the value of the localDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setLocalDateTime(XMLGregorianCalendar value) {
        this.localDateTime = value;
    }

    /**
     * Gets the value of the utcDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public XMLGregorianCalendar getUtcDateTime() {
        return utcDateTime;
    }

    /**
     * Sets the value of the utcDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setUtcDateTime(XMLGregorianCalendar value) {
        this.utcDateTime = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Gets the value of the fax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public String getFax() {
        return fax;
    }

    /**
     * Sets the value of the fax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setFax(String value) {
        this.fax = value;
    }

    /**
     * Gets the value of the openingHours property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public String getOpeningHours() {
        return openingHours;
    }

    /**
     * Sets the value of the openingHours property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setOpeningHours(String value) {
        this.openingHours = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2014-08-01T09:00:14-04:00", comments = "JAXB RI v2.2.4-2")
    public void setAddress(Address value) {
        this.address = value;
    }

}
