
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class Codeshare{
    private String carrier;
   	private String flightNumber;
   	private String fsCode;
   	private String relationship;

 	public String getFlightNumber(){
		return this.flightNumber;
	}
	public void setFlightNumber(String flightNumber){
		this.flightNumber = flightNumber;
	}
 	public String getFsCode(){
		return this.fsCode;
	}
	public void setFsCode(String fsCode){
		this.fsCode = fsCode;
	}
 	public String getRelationship(){
		return this.relationship;
	}
	public void setRelationship(String relationship){
		this.relationship = relationship;
	}

  public String getCarrier() {
    return carrier;
  }

  public void setCarrier(String carrier) {
    this.carrier = carrier;
  }
}
