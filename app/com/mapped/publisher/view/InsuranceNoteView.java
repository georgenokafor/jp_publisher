package com.mapped.publisher.view;

import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.notes.insurance.InsuranceNote;
import com.umapped.persistence.notes.insurance.InsuranceTraveler;
import com.umapped.persistence.reservation.UmTraveler;
import models.publisher.FileInfo;
import models.publisher.TripNote;
import models.publisher.TripNoteAttach;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.umapped.persistence.notes.utils.StructuredNoteUtils.cast;

/**
 * Created by george on 2017-03-22.
 */
public class InsuranceNoteView extends NoteBaseView
    implements Serializable {

  public String provider;
  public String policyNum;
  public String policyType;
  public String emergencyNum;
  public String importantInfo;
  public String policyDesc;
  public String startDate;
  public String startTime;
  public String tag;
  public String linkedTripDetailsId;

  //Overriden method from SuperClass BaseView.
  //Used to create view object to be rendered by template, for respective note type
  @Override
  public InsuranceNoteView buildView(TripNote note, BaseView inView) {
    InsuranceNote insuranceNote = cast(note.getNote(), InsuranceNote.class);
    InsuranceNoteView view = (InsuranceNoteView) inView;

    if (view.scrollToId == null) {
      view.scrollToId = String.valueOf(note.getNoteId());
    }
    view.noteId = note.getNoteId();
    if (insuranceNote != null) {
      view.provider = insuranceNote.getInsuranceProvider();
      view.policyNum = insuranceNote.getPolicyNumber();
      view.policyType = insuranceNote.getPolicyType();
      view.policyDesc = insuranceNote.getPolicyDescription();
      view.emergencyNum = insuranceNote.getEmergencyNumber();
      view.importantInfo = insuranceNote.getImportantInformation();
      view.createdById = note.getCreatedBy();
      if (note.getNoteTimestamp() != null && note.getNoteTimestamp() > 0L) {
        view.startTime = Utils.getTimeString(note.getNoteTimestamp());
        view.startDate = Utils.formatDateControlYYYY(note.getNoteTimestamp());
      }
      view.tag = note.getTag();
      view.linkedTripDetailsId = note.getTripDetailId();

      view.passengers = new ArrayList<>();

      if (insuranceNote.getTravelers() != null) {

        for (UmTraveler t : insuranceNote.getTravelers()) {
          InsuranceTraveler it = cast(t, InsuranceTraveler.class);
          if (it != null) {
            PassengerInfo passenger = new PassengerInfo();
            view.passengers.add(passenger);
            passenger.firstName = it.getGivenName();
            passenger.lastName = it.getFamilyName();
            passenger.name = it.getName();
            passenger.startDate = it.getStartDate();
            passenger.endDate = it.getEndDate();

          }
        }
      }

      //get any attachments
      List<TripNoteAttach> attachments = TripNoteAttach.findByNoteId(note.getNoteId());
      if (attachments != null) {
        List<AttachmentView> photoAttachments = new ArrayList<>();
        List<AttachmentView> linkAttachments = new ArrayList<>();
        List<AttachmentView> videoAttachments = new ArrayList<>();
        List<AttachmentView> fileAttachments = new ArrayList<>();
        for (TripNoteAttach attach : attachments) {
          AttachmentView attachView = new AttachmentView();
          attachView.id = String.valueOf(attach.fileId);
          attachView.attachId = attach.fileId;
          attachView.createdById = attach.createdBy;
          attachView.name = attach.name;
          attachView.comments = attach.comments;
          attachView.attachName = attach.getAttachName();
          attachView.attachUrl = attach.getAttachUrl();

          switch (attach.getAttachType()) {
            case PHOTO_LINK:
              attachView.image = attach.getFileImage();
              if (attachView.image != null) { //Overwriting if we have image data
                FileInfo file = attachView.image.getFile();
                attachView.name = file.getFilename();
                attachView.comments = file.getDescription();
                attachView.attachUrl = file.getUrl();

              }
              photoAttachments.add(attachView);
              break;
            case FILE_LINK:
              fileAttachments.add(attachView);
              //linkAttachments.add(attachView);
              break;
            case VIDEO_LINK:
              videoAttachments.add(attachView);
              if (attach.getAttachUrl().toLowerCase().contains("youtube.com") ||
                  attach.getAttachUrl().toLowerCase().contains("youtu.be")) {
                //try to get the static image
                attach.getAttachUrl().toLowerCase().contains("youtube.com");
                String pattern = "(?<=(?:v|i)=)[a-zA-Z0-9-]+(?=&)|(?<=(?:v|i)\\/)[^&\\n]+|(?<=embed\\/)[^\"&\\n]+|" +
                    "(?<=‌​(?:v|i)=)[^&\\n]+|(?<=youtu.be\\/)[^&\\n]+";

                Pattern compiledPattern = Pattern.compile(pattern);
                Matcher matcher         = compiledPattern.matcher(attach.getAttachUrl());

                if (matcher.find()) {
                  String videoId = matcher.group();
                  attachView.youTubeCover = "https://img.youtube.com/vi/" + videoId + "/0.jpg";
                  attachView.attachUrl = "https://www.youtube.com/embed/" + videoId + "?autoplay=true";
                }
              }
              break;
            case WEB_LINK:
              linkAttachments.add(attachView);
              break;
            default:
              break;
          }


          if (attachView.attachUrl != null && !attachView.attachUrl.toLowerCase().startsWith("http")) {
            attachView.attachUrl = "http://" + attachView.attachUrl;
          }

        }
        if (photoAttachments.size() > 0) {
          view.photos = photoAttachments;
        }
        if (videoAttachments.size() > 0) {
          view.videos = videoAttachments;
        }
        if (linkAttachments.size() > 0) {
          view.links = linkAttachments;
        }
        if (fileAttachments.size() > 0) {
          view.files = fileAttachments;
        }
      }

    }
    return view;
  }
}
