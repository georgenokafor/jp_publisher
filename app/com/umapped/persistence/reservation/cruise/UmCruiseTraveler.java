package com.umapped.persistence.reservation.cruise;

import com.umapped.persistence.reservation.UmTraveler;

/**
 * All parameters personal to a cruise traveler.
 * <p>
 * Created by surge on 2016-11-25.
 */
public class UmCruiseTraveler
    extends UmTraveler {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private String category;

  private String cabinNumber;

  private String deckNumber;

  private String diningPlan;

  private String cabinType;

  private String bedding;
  private String amenities;

  public String getCategory() {
    return category;
  }

  public UmCruiseTraveler setCategory(String category) {
    this.category = category;
    return this;
  }

  public String getCabinNumber() {
    return cabinNumber;
  }

  public UmCruiseTraveler setCabinNumber(String cabinNumber) {
    this.cabinNumber = cabinNumber;
    return this;
  }

  public String getDeckNumber() {
    return deckNumber;
  }

  public UmCruiseTraveler setDeckNumber(String deckNumber) {
    this.deckNumber = deckNumber;
    return this;
  }

  public String getDiningPlan() {
    return diningPlan;
  }

  public UmCruiseTraveler setDiningPlan(String diningPlan) {
    this.diningPlan = diningPlan;
    return this;
  }

  public String getCabinType() {
    return cabinType;
  }

  public UmCruiseTraveler setCabinType(String cabinType) {
    this.cabinType = cabinType;
    return this;
  }

  public String getBedding() {
    return bedding;
  }

  public UmCruiseTraveler setBedding(String bedding) {
    this.bedding = bedding;
    return this;
  }

  public String getAmenities() {
    return amenities;
  }

  public UmCruiseTraveler setAmenities(String amenities) {
    this.amenities = amenities;
    return this;
  }
}
