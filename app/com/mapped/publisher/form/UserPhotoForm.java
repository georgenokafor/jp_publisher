package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-20
 * Time: 9:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserPhotoForm {
    private String inCmpyId;
    private String inUserId;
    private String inCmpyMode;
    private String inEditMode;
    private String inUserPhotoName;

    public String getInCmpyId() {
        return inCmpyId;
    }

    public void setInCmpyId(String inCmpyId) {
        this.inCmpyId = inCmpyId;
    }

    public String getInUserId() {
        return inUserId;
    }

    public void setInUserId(String inUserId) {
        this.inUserId = inUserId;
    }

    public String getInCmpyMode() {
        return inCmpyMode;
    }

    public void setInCmpyMode(String inCmpyMode) {
        this.inCmpyMode = inCmpyMode;
    }

    public String getInEditMode() {
        return inEditMode;
    }

    public void setInEditMode(String inEditMode) {
        this.inEditMode = inEditMode;
    }

    public String getInUserPhotoName() {
        return inUserPhotoName;
    }

    public void setInUserPhotoName(String inUserPhotoName) {
        this.inUserPhotoName = inUserPhotoName;
    }
}
