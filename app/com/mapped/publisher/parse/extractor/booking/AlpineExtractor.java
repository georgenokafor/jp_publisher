package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripperByArea;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by twong on 15-10-05.
 */
public class AlpineExtractor extends BookingExtractor {

  private String file;
  private TripVO trip;

  private static String[] dateTimeFormat = {"dd-MMM-yyyy hh:mm a", "MM/dd/yyyy hh:mm a", "EEEE dd MMMM yyyy hhmm a"};
  private static DateUtils du = new DateUtils();
  private static Calendar now = Calendar.getInstance();

  private List<String> header = new ArrayList<>();
  private List<PassengerVO> travelers = new ArrayList<>();

  private String date = "";

  private int rank = 0;

  private enum BOOKING_TYPE {
    HOTEL,
    TOUR,
    TRANSFER,
    CAR,
    DATE,
    UNDEFINED
  }

  private static Pattern timePattern = Pattern.compile("[0-9]{1,2}:[0-9]{2}\\s[A-Z,a-z]{2}");
  private static Pattern datePattern = Pattern.compile("[0-9]{2}\\-[A-Z,a-z]{3}\\-[0-9]{4}");
  private static Pattern datePattern1 = Pattern.compile("[0-9]{2}/[0-9]{2}/[0-9]{4}");


  private static Pattern hotelCheckoutPattern = Pattern.compile(
      "Check-Out\\sDate:\\s[A-Za-z]{3}\\s[0-9]{2}\\s[A-Za-z]{3}\\s[0-9]{2}\\sat\\s[0-9]{2}:[0-9]{2}");



  protected void initImpl(Map<String, String> params) {

  }

  protected TripVO extractDataImpl(InputStream input)
      throws Exception {
    trip = new TripVO();
    file = getPDFContentAsString(input);

    //lets get the repeat header info
    processFile();

    return trip;
  }

  private void processFile() {
    List<String> frequentFlyer = new ArrayList();
    List<String> etickets = new ArrayList();

    String[] tokens = file.split("\n");


    for (int i = 0; i < tokens.length; i++) {
      String s = tokens[i];
      BOOKING_TYPE booking_type = getBookingType(s);
      if (booking_type != BOOKING_TYPE.UNDEFINED && booking_type != BOOKING_TYPE.DATE) {
        if (booking_type == BOOKING_TYPE.HOTEL) {
          i = parseHotel(++i, tokens);
        }
        else if (booking_type == BOOKING_TYPE.TRANSFER) {
          i = parseTransfers(++i, tokens);
        }  else if (booking_type == BOOKING_TYPE.CAR) {
          i = parseCar(++i, tokens);
        }
        else if (booking_type == BOOKING_TYPE.TOUR) {
          i = parseTours(++i, tokens);
        }

      }


    }





  }

  private BOOKING_TYPE getBookingType(String s) {
    if (s != null) {
      s = s.trim();
      if (s.startsWith("LODGING")) {
        return BOOKING_TYPE.HOTEL;
      }
      else if (s.startsWith("TRANSFERS")) {
        return BOOKING_TYPE.TRANSFER;
      }
      else if (s.startsWith("CAR RENTAL")) {
        return BOOKING_TYPE.CAR;
      }
      else if (s.startsWith("LIFTS") || s.startsWith("LESSONS") || s.startsWith("SKI RENTALS")) {
        return BOOKING_TYPE.TOUR;
      }


    }
    return BOOKING_TYPE.UNDEFINED;
  }

  private boolean containsDay(String s) {
    if (s != null) {
      s = s.toLowerCase();
      if (s.contains("monday") || s.contains("tuesday") || s.contains("wednesday") || s.contains("thursday") || s
          .contains(

              "friday") || s.contains("saturday") || s.contains("sunday")) {
        return true;
      }
    }

    return false;
  }

  @Override
  public ArrayList<ParseError> getParseErrors() {
    return null;
  }

  @Override
  public void postProcessingImpl(String tripId, String userId) {
  }



  public int parseHotel(int i, String[] tokens) {
    HotelVO hotel = new HotelVO();
    hotel.setRank(rank);
    String checkinDate = "";
    String checkoutDate = "";

    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (s.startsWith("Check-in:")) {
        checkinDate = parseDate(s);
      }  else if (s.startsWith("Check-out:")) {
        checkoutDate = parseDate(s);
      } else if (s.startsWith("Check-in time:")) {
        hotel.setCheckin(getTimestamp(checkinDate, parseTimestamp(s)));
      }  else if (s.startsWith("Check-out time:")) {
        hotel.setCheckout(getTimestamp(checkoutDate, parseTimestamp(s)));
      } else if (s.contains("Service: ")) {
        if (s.contains(" at ") && s.contains(" (") && s.indexOf(" (") > s.indexOf(" at ")) {
          hotel.setName(s.substring(s.indexOf(" at ") + 4, s.indexOf(" (")).trim());
        }
        sb.append(s);
        sb.append("\n");
      } else if (s.contains("Confirmation Number:")) {
        hotel.setConfirmation(s.substring(s.indexOf(":") + 1).trim());
      } else {
        sb.append(s);
        sb.append("\n");
      }

      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }
    hotel.setNote(sb.toString());

    if (hotel.isValid()) {
      trip.addHotel(hotel);
    }

    return i;


  }



  public String cleanFirstName  (String firstName) {
    firstName = firstName.replace("MISS","");
    firstName = firstName.replace("MS","");
    firstName = firstName.replace("MR","");
    firstName = firstName.replace("MRS", "");
    firstName = firstName.trim();
    return firstName;
  }

  public int parseTransfers(int i, String[] tokens) {
    TransportVO transport = new TransportVO();
    transport.setBookingType(ReservationType.TRANSPORT);
    transport.setRank(rank);
    String startDate = null;
    String endDate = null;
    Timestamp pickupDate = null;
    Timestamp dropoffDate = null;

    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];

      if (s.startsWith("Start:")) {
        startDate = s.substring(s.indexOf(":") + 1).trim();
      } else if (s.startsWith("End:")) {
        endDate = s.substring(s.indexOf(":") + 1).trim();
      } else if (s.startsWith("End:")) {
        endDate = s.substring(s.indexOf(":") + 1).trim();
      } else if (s.startsWith("Arrival pick up:")) {
        String date = null;
        String time = null;
        Matcher dateMatcher = datePattern1.matcher(s);
        if (dateMatcher.find()) {
          date = dateMatcher.group();
        }
        Matcher timeMatcher = timePattern.matcher(s);
        if (timeMatcher.find()) {
          time = timeMatcher.group();
        }

        if (date != null && time != null) {
          pickupDate = getTimestamp(date, time);
          transport.setPickupDate(pickupDate);
        }
      } else if (s.startsWith("Return Transfer Info:")) {
        String date = null;
        String time = null;
        Matcher dateMatcher = datePattern1.matcher(s);
        if (dateMatcher.find()) {
          date = dateMatcher.group();
        }
        Matcher timeMatcher = timePattern.matcher(s);
        if (timeMatcher.find()) {
          time = timeMatcher.group();
        }

        if (date != null && time != null) {
          dropoffDate = getTimestamp(date, time);
          if (startDate != null && endDate != null && startDate.equals(endDate)) {
            transport.setDropoffDate(dropoffDate);
          }
        }
      }  else if (s.contains("Confirmation Number:")) {
        transport.setConfirmationNumber(s.substring(s.indexOf(":") + 1).trim());
      } else if (s.startsWith("Service:")) {
        if (s.contains(": ") && s.contains(" (") && s.indexOf(" (") > s.indexOf(": ")) {
          transport.setName(s.substring(s.indexOf(": ") + 1, s.indexOf(" (")).trim());
        }
        sb.append(s);sb.append("\n");

      }  else {
        sb.append(s);
        sb.append("\n");
      }

      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }


    transport.setNote(sb.toString());

    if (transport.isValid()) {
      trip.addTransport(transport);

      if (startDate!= null && endDate != null && !startDate.equals(endDate) && dropoffDate != null) {
        //copy the transport
        TransportVO t = new TransportVO();
        t.setBookingType(transport.getBookingType());
        t.setCmpyName(transport.getCmpyName());
        t.setConfirmationNumber(transport.getConfirmationNumber());
        t.setDropoffDate(null);
        t.setDropoffLocation(transport.getPickupLocation());
        t.setPickupDate(dropoffDate);
        t.setPickupLocation(transport.getDropoffLocation());
        t.setName(transport.getName());
        t.setNote(transport.getNote());
        t.setRank(transport.getRank());
        trip.addTransport(t);
      }
    }

    return i;
  }

  public int parseCar(int i, String[] tokens) {
    TransportVO transport = new TransportVO();
    transport.setBookingType(ReservationType.CAR_RENTAL);
    transport.setRank(rank);


    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (s.contains("")) {
        s = s.replace("","-");
      }

      if (s.startsWith("Confirmation Number") && s.contains(":")) {
        transport.setConfirmationNumber(s.substring(s.indexOf(":") + 1).trim());
      } else if (s.startsWith("Service")) {
        if (s.contains(" with ") && s.contains(" (") && s.indexOf(" (") > s.indexOf(" with ")) {
          transport.setName(s.substring(s.indexOf(" with ") + 6, s.indexOf(" (")).trim());
        }
        sb.append(s);sb.append("\n");

      } else if (s.contains("Pick Up:")) {
        String date = null;
        String time = null;
        Matcher dateMatcher = datePattern1.matcher(s);
        if (dateMatcher.find()) {
          date = dateMatcher.group();
        }
        Matcher timeMatcher = timePattern.matcher(s);
        if (timeMatcher.find()) {
          time = timeMatcher.group();
        }

        if (date != null && time != null) {
          transport.setPickupDate(getTimestamp(date, time));
        }
        String s1 = s.substring(s.indexOf(":")+1, s.indexOf(date)).trim();
        transport.setPickupLocation(s1);
        sb.append(s); sb.append("\n");
      }  else if (s.contains("Return:")) {
        String date = null;
        String time = null;
        Matcher dateMatcher = datePattern1.matcher(s);
        if (dateMatcher.find()) {
          date = dateMatcher.group();
        }
        Matcher timeMatcher = timePattern.matcher(s);
        if (timeMatcher.find()) {
          time = timeMatcher.group();
        }

        if (date != null && time != null) {
          transport.setDropoffDate(getTimestamp(date, time));
        }
        String s1 = s.substring(s.indexOf(":")+1, s.indexOf(date)).trim();
        transport.setDropoffLocation(s1);
        sb.append(s); sb.append("\n");
      }
      else {
        sb.append(s);
        sb.append("\n");
      }

      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }
    }


    transport.setNote(sb.toString());

    if (transport.isValid()) {
      trip.addTransport(transport);
    }

    return i;
  }

  public int parseTours(int i, String[] tokens) {
    ActivityVO activity = new ActivityVO();
    activity.setBookingType(ReservationType.ACTIVITY);
    activity.setRank(rank);

    StringBuilder sb = new StringBuilder();
    while (true) {
      String s = tokens[i];
      if (s.startsWith("Start")) {
        activity.setStartDate(getTimestamp(parseDate(s), "12:00 AM"));
      } else if (s.startsWith("End")  && s.contains(":")) {
        activity.setEndDate(getTimestamp(parseDate(s), "12:00 AM"));
      } else if (s.startsWith("Service") && s.contains(":")) {
        activity.setName(s.substring(s.indexOf(":") + 1).trim());
      } else if (s.contains("Confirmation Number") && s.contains(":")) {
        activity.setConfirmation(s.substring(s.indexOf(":") + 1).trim());
      }else {
        sb.append(s);sb.append("\n");
      }
      if (i + 1 == tokens.length) {
        break;
      }
      else {
        String nextLine = tokens[i + 1];
        if (getBookingType(nextLine) != BOOKING_TYPE.UNDEFINED) {
          break;
        }
        else {
          i++;
        }

      }

    }

    activity.setNote(sb.toString());

    if (activity.isValid()) {
      System.out.println(activity);
      trip.addActivity(activity);
    }

    return i;

  }

  private String parseDate(String s) {
    if (s != null) {
      s = s.replaceAll(" ","");
      Matcher m = datePattern.matcher(s);
      if (m.find()) {
        return m.group();
      }

    }
    return "";
  }

  private String parseTimestamp(String s) {
    if (s != null) {


      Matcher m = timePattern.matcher(s);
      if (m.find()) {
        return m.group();
      }

    }
    return "12:00 am";
  }

  private Timestamp parseTimestamp(String s, String keyword) {
    if (s != null && keyword != null) {
      String date = s.substring(s.indexOf(keyword) + keyword.length());
      String time = "";
      if (date.contains(" at ")) {
        time = date.substring(date.indexOf(" at ") + 4).trim();
        if (time.length() > 5) {
          Matcher m = timePattern.matcher(time);
          if (m.find()) {
            time = m.group();
          }
        }
        date = date.substring(0, date.indexOf(" at "));
      }
      else if (date.length() > 13) {
        date = date.substring(0, 14).trim();
      }
      if (time.length() == 5) {
        return getTimestamp(date.trim(), time);
      }
      else {
        return getTimestamp(date.trim(), "00:00");
      }
    }
    return null;
  }

  private Timestamp getTimestamp(String date, String time) {
    try {

      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime());
      return t;
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Get the PDF content as String
   *
   * @param fileInputStream
   * @return pdf content as String
   */
  private static final String getPDFContentAsString(InputStream fis) {
    PDDocument pdfDocument = null;
    StringBuilder contents = new StringBuilder();
    try {
      PDFParser parser = new PDFParser(fis);
      parser.parse();

      pdfDocument = parser.getPDDocument();

      PDFTextStripperByArea stripper = new PDFTextStripperByArea();

      stripper.setSortByPosition(true);
      Rectangle rect = new Rectangle(0, 100, 650, 650);
      stripper.addRegion("body", rect);

      java.util.List<?> allPages = pdfDocument.getDocumentCatalog().getAllPages();

      for (Object pdPage : allPages) {
        stripper.extractRegions((PDPage) pdPage);
        contents.append(stripper.getTextForRegion("body"));
      }

    }
    catch (IOException e) {
      e.printStackTrace();
    }
    return contents.toString();
  }

  public static void main(String[] args) {
    try {
      String dir = "/users/twong/Dropbox/UMapped_Share/Companies/Alpine Adventures/";
      File file = new File(dir + "sample1.pdf");
      InputStream ins = new FileInputStream(file);

      //System.out.println(getPDFContentAsString(ins));


      AlpineExtractor p = new AlpineExtractor();
      p.initImpl(null);
      TripVO t = p.extractData(ins);
      System.out.println(t);



    }
    catch (Exception e) {
      e.printStackTrace();
    }

  }

}
