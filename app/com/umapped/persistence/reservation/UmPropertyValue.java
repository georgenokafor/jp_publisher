package com.umapped.persistence.reservation;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmPropertyValue
    extends UmThing {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   *  A commonly used identifier for the characteristic represented by the property,
   *  e.g. a manufacturer or a standard code for a property. propertyID can be (1) a prefixed string, mainly meant
   *  to be used with standards for product properties; (2) a site-specific, non-prefixed string (e.g. the primary
   *  key of the property or the vendor-specific id of the property), or (3) a URL indicating the type of the property,
   *  either pointing to an external vocabulary, or a Web resource that describes the property (e.g. a glossary entry).
   *  Standards bodies should promote a standard prefix for the identifiers of properties from their standards.
   */
  public String propertyID;

  /**
   * Boolean  or Number  or StructuredValue  or Text (Umapped supports only TEXT)
   * The value of the quantitative value or property value node.
   * For QuantitativeValue and MonetaryValue, the recommended type for values is 'Number'.
   * For PropertyValue, it can be 'Text;', 'Number', 'Boolean', or 'StructuredValue'.
   */
  public String value;


}
