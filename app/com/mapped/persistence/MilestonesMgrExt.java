package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.*;
import java.util.ArrayList;


public class MilestonesMgrExt extends MilestonesMgr {

    private static final String SELECT_STR = "Select a.Pk, a.Note, a.Proximity, a.ProximityAlert, a.Rank, a.Latitude, a.Longitude, a.Bearing, a.Status, a.LastUpdateTimestamp, a.CreateTimestamp, a.ModifiedBy, a.CreatedBy, a.Name, a.Address, a.StartDate, a.EndDate, a.MapsId, a.SyncTimestamp, a.tag, a.PublisherNote from Milestones a";
    private static final String SELECT_ALL_BY_MAP_OWNER_LASTUPDATE = SELECT_STR + ", Maps b where b.CreatedBy = ? and a.MapsId = b.Pk and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?);";
    private static final String SELECT_SHARED_BY_USER_LASTUPDATE = SELECT_STR + ", Maps b where b.Pk in (select mapsid from sharedmaps where shareuserid = ? and status = 0) and a.MapsId = b.Pk and (a.SyncTimestamp = 0 or a.SyncTimestamp > ?);";
    private static final String SELECT_NEW_SHARED_BY_USER_LASTUPDATE = SELECT_STR + ", Maps b where b.Pk in (select mapsid from sharedmaps where shareuserid = ? and status = 0 and (SyncTimestamp = 0 or SyncTimestamp > ?) and createtimestamp = lastupdatetimestamp) and a.MapsId = b.Pk;";
    private static final String SELECT_ALL_PAGES_BY_MAPSID = SELECT_STR + " where a.pk in (select milestonesid from page where status =0 and mapsid = ?) and a.status = 0;";
    private static final String SELECT_ALL_BY_PAGEID = SELECT_STR + " where a.pk in (select contentid from pagecontent where status =0 and pageid = ?) and a.status = 0;";
    private static final String INSERT = "insert into Milestones (Pk, Note, Proximity, ProximityAlert, Rank, Latitude, Longitude, Bearing, Status, LastUpdateTimestamp, CreateTimestamp, ModifiedBy, CreatedBy, Name, Address, StartDate, EndDate, MapsId, SyncTimestamp, Tag, PublisherNote) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String UPDATE = "update Milestones SET Note=?, Proximity=?, ProximityAlert=?, Rank=?, Latitude=?, Longitude=?, Bearing=?, Status=?, LastUpdateTimestamp=?, ModifiedBy=?, Name=?, Address=?, StartDate=?, EndDate=?, MapsId=?, SyncTimestamp=?, Tag=?, PublisherNote=? WHERE Pk = ?; ";

    public static ArrayList <Milestones> getAllByMapOwnerSinceLastUpdate(String userId, long lastUpdateTimestamp) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
             stat = conn.createStatement();
             prep = conn.prepareStatement(SELECT_ALL_BY_MAP_OWNER_LASTUPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdateTimestamp);
            rs = prep.executeQuery();
            return (MilestonesMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <Milestones> getSharedByUserSinceLastUpdate(String userId, long lastUpdateTimestamp) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
             stat = conn.createStatement();
             prep = conn.prepareStatement(SELECT_SHARED_BY_USER_LASTUPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdateTimestamp);
            rs = prep.executeQuery();
            return (MilestonesMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <Milestones> getNewSharedByUserSinceLastUpdate(String userId, long lastUpdateTimestamp) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_NEW_SHARED_BY_USER_LASTUPDATE);
            prep.setString(1, userId);
            prep.setLong(2, lastUpdateTimestamp);
            rs = prep.executeQuery();
            return (MilestonesMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <Milestones> getAllInAllPagesByMapsId(String mapsId) throws SQLException {
       ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_ALL_PAGES_BY_MAPSID);
            prep.setString(1, mapsId);
            rs = prep.executeQuery();
            return (MilestonesMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static ArrayList <Milestones> getAllInPage (String pageId) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
            stat = conn.createStatement();
            prep = conn.prepareStatement(SELECT_ALL_BY_PAGEID);
            prep.setString(1, pageId);
            rs = prep.executeQuery();
            return (MilestonesMgr.handleResults(rs));
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
    }

    public static void insert (Milestones c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(INSERT);
            prep.setString(1, c.getPk());
            prep.setString(2, c.getNote());
            prep.setString(3, c.getProximity());
            prep.setString(4, c.getProximityAlert());
            prep.setString(5, c.getRank());
            prep.setString(6, c.getLatitude());
            prep.setString(7, c.getLongitude());
            prep.setString(8, c.getBearing());
            prep.setLong(9, c.getStatus());
            prep.setLong(10, c.getLastUpdateTimestamp());
            prep.setLong(11, c.getCreateTimestamp());
            prep.setString(12, c.getModifiedBy());
            prep.setString(13, c.getCreatedBy());
            prep.setString(14, c.getName());
            prep.setString(15, c.getAddress());
            prep.setString(16, c.getStartDate());
            prep.setString(17, c.getEndDate());
            prep.setString(18, c.getMapsId());
            prep.setLong(19, c.getSyncTimestamp());
            prep.setString(20, c.getTag());
            prep.setString(21, c.getPublisherNote());
            prep.execute();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

    public static int update (Milestones c, Connection conn) throws SQLException {
        PreparedStatement prep= null;
        try {
            prep = conn.prepareStatement(UPDATE);
            prep.setString(1, c.getNote());
            prep.setString(2, c.getProximity());
            prep.setString(3, c.getProximityAlert());
            prep.setString(4, c.getRank());
            prep.setString(5, c.getLatitude());
            prep.setString(6, c.getLongitude());
            prep.setString(7, c.getBearing());
            prep.setLong(8, c.getStatus());
            prep.setLong(9, c.getLastUpdateTimestamp());
            prep.setString(10, c.getModifiedBy());
            prep.setString(11, c.getName());
            prep.setString(12, c.getAddress());
            prep.setString(13, c.getStartDate());
            prep.setString(14, c.getEndDate());
            prep.setString(15, c.getMapsId());
            prep.setLong(16, c.getSyncTimestamp());
            prep.setString(17, c.getTag());
            prep.setString(18, c.getPublisherNote());
            prep.setString(19, c.getPk());
            return prep.executeUpdate();
        } finally {
            if (prep != null)
                prep.close();
        }
    }

}
