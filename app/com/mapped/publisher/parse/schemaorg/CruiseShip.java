package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by ryan on 15-08-18.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CruiseShip
    extends Thing {

  public String arrivalPier;

  public Place arrivalPort;

  public DateTime arrivalTime;

  public String departPier;

  public Place departPort;

  public DateTime departTime;

  public DateTime boardingTime;

  public Organization provider;
}
