package com.mapped.publisher.form.template;

import play.data.validation.Constraints;

/**
 * Created by surge on 2016-01-04.
 */
public class FormTemplateTransport {
  @Constraints.Required
  public String inTripId;
  @Constraints.Required
  public String inBookingType;
  public String inTemplateDetailId;
  @Constraints.Required
  public String inTransferCmpy;
  public Long   inTransferProviderId;
  public String inTransferContact;
  public String inTransferNote;
  public int    inDayOffset;
  public int    inDuration;
  public String inStartTime;
  public String inEndTime;
  public String inTransferPickUp;
  public String inTransferDropOff;
}
