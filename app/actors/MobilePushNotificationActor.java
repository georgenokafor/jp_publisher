package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Sender;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import models.publisher.AccountSession;
import org.apache.commons.lang3.time.StopWatch;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wei on 2017-08-17.
 */
public class MobilePushNotificationActor extends UntypedActor {

  public final static ApnsService IOS_UMAPPED_SERVICE = (ConfigMgr.isProduction()) ?
                                                        (APNS.newService()
                                                             .withCert("conf/certs/prd/umapped-v2.p12", "Umapped!@#")
                                                             .withProductionDestination()
                                                             .build())
                                                        :
                                                        (APNS.newService()
                                                            .withCert("conf/certs/dev/umapped-v2-dev.p12", "Umapped!@#")
                                                            .withSandboxDestination()
                                                            .build());


  public final static ApnsService IOS_SMARTFLYER_SERVICE = (ConfigMgr.isProduction()) ?
                                                           APNS.newService()
                                                             .withCert("conf/certs/prd/smartflyer_prd.p12", "Umapped!@#")
                                                             .withProductionDestination()
                                                             .build()
                                                        :
                                                           (APNS.newService()
                                                                .withCert("conf/certs/dev/smartflyer_dev.p12", "Umapped!@#")
                                                                .withSandboxDestination()
                                                                .build());


  public final static ApnsService IOS_LOZANO_SERVICE = APNS.newService()
                                                            .withCert("conf/certs/prd/lozano_prd.p12", "Umapped!@#")
                                                            .withProductionDestination()
                                                            .build();


  public final static ApnsService IOS_ENSEMBLE_SERVICE = APNS.newService()
                                                             .withCert("conf/certs/prd/mytriproute_prod.p12", "Umapped!@#")
                                                             .withProductionDestination()
                                                             .build();

  public final static ApnsService IOS_PROTRIP_SERVICE = APNS.newService()
                                                            .withCert("conf/certs/prd/protrip_prd.p12", "Umapped!@#")
                                                            .withProductionDestination()
                                                            .build();

  public final static ApnsService IOS_TZELLTRIP_SERVICE = APNS.newService()
                                                              .withCert("conf/certs/prd/tzelltrip_prd.p12", "Umapped!@#")
                                                              .withProductionDestination()
                                                              .build();

  public final static ApnsService IOS_SKI_COM_SERVICE = APNS.newService()
                                                            .withCert("conf/certs/prd/ski_com_prd.p12", "Umapped!@#")
                                                            .withProductionDestination()
                                                            .build();

  public final static ApnsService IOS_ALTOUR_SERVICE = APNS.newService()
                                                           .withCert("conf/certs/prd/altour_prd.p12", "Umapped!@#")
                                                           .withProductionDestination()
                                                           .build();
  public final static  ApnsService IOS_TEPLIS_SERVICE = APNS.newService()
                                                            .withCert("conf/certs/prd/teplis_prd.p12", "Umapped!@#")
                                                            .withProductionDestination()
                                                            .build();

  public final static Sender ANDROID_UMAPPED_SENDER = new Sender("AIzaSyBcRXeFPh3IfSXK9RKltJqQAgIw8z0Q4rU");
  public final static Sender ANDROID_SMARTFLYER_SENDER = new Sender("AIzaSyBrZfQuW9H3GdaEL_x1_I0OcoGPEJyD2C8");
  public final static Sender ANDROID_LOZANO_SENDER = new Sender("AIzaSyBFogXKvebuMOdOUXwKepPf4rAhbhmr8e8");
  public final static Sender ANDROID_ALTOUR_SENDER = new Sender("AIzaSyAZ6C664dE4su4mXtzW5hUWs0EIX9ueGCw");

  public final static Sender ANDROID_SKI_COM_SENDER = new Sender("AIzaSyDAqhJjKV547AJQcBgDUOgyvLJ68mihYbs");
  public final static Sender ANDROID_ENSEMBLE_SENDER = new Sender("AIzaSyCvVWoapWb-jSjWwz9vcCAkwi83CvYOYjs");
  public final static Sender ANDROID_PROTRIP_SENDER = new Sender("AIzaSyCgEV481TTq-YRGAlxyZBxJEODCMoWJkzA");
  public final static Sender ANDROID_TZELLTRIP_SENDER = new Sender("AIzaSyB-95KgucU53oEsNeIgZFgQIfJ79JMgplE");
  public final static Sender ANDROID_TEPLIS_SENDER = new Sender("AIzaSyCAkarDDjp16DyOKeg7YOfo4iFj7TudP5Q");

  public interface Factory {
    public Actor create();
  }

  public final static Props props = Props.create(MobilePushNotificationActor.class);


  public static class Command
      implements Serializable {
    private final String email;
    private final String msgTitle;
    private final String msg;
    private final long msgId;
    private final String fullMsg;

    public Command(String email, String title, String msg, long msgId) {
      this.email = email;
      this.msgTitle = title;
      this.msg = msg;
      this.msgId = msgId;
      String s = "";
      if (msgTitle != null && !msgTitle.isEmpty()) {
        s = msgTitle;
      }
      if (s.length()  > 0 && msg != null) {
        s = "\n" + msg;
      }
      fullMsg = s;
    }

    public String getEmail() {
      return email;
    }

    public String getMsg() {
      return msg;
    }

    public long getMsgId() {
      return msgId;
    }

    public String getMsgTitle() {
      return msgTitle;
    }

    public String getFullMsg() {
      return fullMsg;
    }
  }

  @Override
  public void onReceive(Object message)
      throws Exception {

    // ONLY fire the notification in Non Production for now...
    if (message instanceof MobilePushNotificationActor.Command) {

      long msgId = ((MobilePushNotificationActor.Command) message).msgId;
      String email = ((MobilePushNotificationActor.Command) message).getEmail();
      String msgTitle = ((MobilePushNotificationActor.Command) message).getMsgTitle();
      String msgBody = ((MobilePushNotificationActor.Command) message).getMsg();
      String fullMsg = ((MobilePushNotificationActor.Command) message).getFullMsg();
      
      StopWatch sw = new StopWatch();
      sw.start();
      String className = "MobilePushNotificationActor - Actor Id: " + msgId;

      if (email != null && msgTitle != null && msgBody != null && ConfigMgr.isProduction()) {
        try {

          String iosPayload = APNS.newPayload().badge(0).alertBody(fullMsg).build();
          //android top-atlantico
          Message androidMsg = new Message.Builder().addData("title", msgTitle).addData("message", msgBody).build();
          Log.log(LogLevel.INFO, className + " Start Processing: " + email);
          int umappedIOS = 0, smfIOS =0,  umappedAnd = 0, smfAnd = 0, lzIOS = 0, lzAnd = 0;
          int skiIOS=0, skiAnd = 0, tzellIOS = 0, tzellAnd = 0, protripIOS = 0, protripAnd = 0;
          int mytripIOS = 0, mytripAnd = 0, altIOS = 0, altAnd = 0, tepIOS = 0, tepAnd = 0;


          List<AccountSession> accountSessions = AccountSession.findByEmail(email);
          if (accountSessions != null) {
            for (AccountSession session : accountSessions) {
              try {
                switch (session.getAppId()) {
                  case "IUM":
                    IOS_UMAPPED_SERVICE.push(session.getNotificationToken(), iosPayload);
                    umappedIOS++;
                    break;
                  case "AUM":
                    ANDROID_UMAPPED_SENDER.send(androidMsg, session.getNotificationToken(), 1);
                    umappedAnd++;
                    break;
                  case "ISM":
                    IOS_SMARTFLYER_SERVICE.push(session.getNotificationToken(), iosPayload);
                    smfIOS++;
                    break;
                  case "ASM":
                    ANDROID_SMARTFLYER_SENDER.send(androidMsg, session.getNotificationToken(), 1);
                    smfAnd++;
                    break;
                  case "ILO":
                    IOS_LOZANO_SERVICE.push(session.getNotificationToken(), iosPayload);
                    lzIOS++;
                    break;
                  case "ALO":
                    ANDROID_LOZANO_SENDER.send(androidMsg, session.getNotificationToken(), 1);
                    lzAnd++;
                    break;
                  case "IPT":
                    IOS_PROTRIP_SERVICE.push(session.getNotificationToken(), iosPayload);
                    protripIOS++;
                    break;
                  case "APT":
                    ANDROID_PROTRIP_SENDER.send(androidMsg, session.getNotificationToken(), 1);
                    protripAnd++;
                    break;
                  case "ITZ":
                    IOS_TZELLTRIP_SERVICE.push(session.getNotificationToken(), iosPayload);
                    tzellIOS++;
                    break;
                  case "ATZ":
                    ANDROID_TZELLTRIP_SENDER.send(androidMsg, session.getNotificationToken(), 1);
                    tzellAnd++;
                    break;
                  case "IMT":
                    IOS_ENSEMBLE_SERVICE.push(session.getNotificationToken(), iosPayload);
                    mytripIOS++;
                    break;
                  case "AMT":
                    ANDROID_ENSEMBLE_SENDER.send(androidMsg, session.getNotificationToken(), 1);
                    mytripAnd++;
                    break;
                  case "ISK":
                    IOS_SKI_COM_SERVICE.push(session.getNotificationToken(), iosPayload);
                    skiIOS++;
                    break;
                  case "ASK":
                    ANDROID_SKI_COM_SENDER.send(androidMsg, session.getNotificationToken(), 1);
                    skiAnd++;
                    break;
                  case "IAL":
                    IOS_ALTOUR_SERVICE.push(session.getNotificationToken(), iosPayload);
                    altIOS++;
                    break;
                  case "AAL":
                    ANDROID_ALTOUR_SENDER.send(androidMsg, session.getNotificationToken(), 1);
                    altAnd++;
                    break;
                  case "ITP":
                    IOS_TEPLIS_SERVICE.push(session.getNotificationToken(), iosPayload);
                    tepIOS++;
                    break;
                  case "ATP":
                    ANDROID_TEPLIS_SENDER.send(androidMsg, session.getNotificationToken(), 1);
                    tepAnd++;
                    break;
                  default:
                    Log.debug("Not supported yet");
                    break;
                }
              } catch (Exception pushException) {
                Log.err("Fail to push a notification: " + session.getAppId() + "; Error: " + pushException.getMessage());
              }
            }
          }
          sw.stop();
          StringBuilder sb = new StringBuilder();
          sb.append("Umapped - IOS: ");sb.append(umappedIOS); sb.append(" Android: ");sb.append(umappedAnd);sb.append("\n");
          sb.append("SmartFlyer - IOS: ");sb.append(smfIOS); sb.append(" Android: ");sb.append(smfAnd);sb.append("\n");
          sb.append("Lozano - IOS: ");sb.append(lzIOS); sb.append(" Android: ");sb.append(lzAnd);sb.append("\n");
          sb.append("Ensemble - IOS: ");sb.append(mytripIOS); sb.append(" Android: ");sb.append(mytripAnd);sb.append("\n");
          sb.append("PROTRIP - IOS: ");sb.append(protripIOS); sb.append(" Android: ");sb.append(protripAnd);sb.append("\n");
          sb.append("TZELL - IOS: ");sb.append(tzellIOS); sb.append(" Android: ");sb.append(tzellAnd);sb.append("\n");
          sb.append("SKI.COM - IOS: ");sb.append(skiIOS); sb.append(" Android: ");sb.append(skiAnd);sb.append("\n");
          sb.append("ALTOUR - IOS: ");sb.append(altIOS); sb.append(" Android: ");sb.append(altAnd);sb.append("\n");
          sb.append("TEPLIS - IOS: ");sb.append(tepIOS); sb.append(" Android: ");sb.append(tepAnd);sb.append("\n");

          Log.log(LogLevel.INFO, className + " Sent  " + sb.toString());

          Log.log(LogLevel.INFO, className + " Time for: " + email + ": " + sw.getTime());

        }
        catch (Exception e) {
          Log.log(LogLevel.ERROR, className + "Email : " + email + " - Failed: " + e.getMessage(), e);
        }
        finally {
        }
      }
    }
  }
}
