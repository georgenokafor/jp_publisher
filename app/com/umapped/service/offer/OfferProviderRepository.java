package com.umapped.service.offer;

/**
 * Created by wei on 2017-06-13.
 */
public interface OfferProviderRepository {
  OfferResult getContentOffer(ContentFilter filter);

  OfferResult getActivityOffer(ActivityFilter filter);
}
