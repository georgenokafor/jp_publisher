package models.publisher;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by twong on 2014-12-26.
 */
@Entity @Table(name = "Flight_Alert_Booking")
public class FlightAlertBooking extends Model {
  public Long lastEventTimestamp;

  @Id
  public Long alertBookingId;

  @Constraints.Required
  public Long createdtimestamp;
  public String createdby;

  @OneToOne @JoinColumn(name = "flight_alert_id")
  public FlightAlert flightAlert;

  @OneToOne @JoinColumn(name = "details_id")
  public TripDetail tripDetail;

  @OneToOne @JoinColumn(name = "trip_id")
  public Trip trip;

  @Version
  public int version;

  public static Model.Finder<Long, FlightAlertBooking> find = new Finder(FlightAlertBooking.class);


  public static FlightAlertBooking findByBookingId (String bookingId) {
    return find.where().eq("details_id", bookingId).findUnique();
  }

  public static List<FlightAlertBooking> findAllByBookingId (String bookingId) {
    return find.where().eq("details_id", bookingId).findList();
  }

  public static FlightAlertBooking findByBookingIdAlertId (String bookingId, long flight_alert_id) {
    return find.where().eq("details_id", bookingId).eq("flight_alert_id", flight_alert_id).findUnique();
  }

  public static List<FlightAlertBooking> findByFlightAlert(FlightAlert alert) {
    return find.where().eq("flight_alert_id",alert.flightAlertId).findList();
  }

  public static void deleteByBookingId (String bookingid) {
    String s = "delete from  Flight_Alert_Booking where details_id = :bookingid;";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("bookingid", bookingid);
    Ebean.execute(update);
  }

  public Long getLastEventTimestamp() {
    return lastEventTimestamp;
  }

  public void setLastEventTimestamp(Long lastEventTimestamp) {
    this.lastEventTimestamp = lastEventTimestamp;
  }

  public Long getAlertBookingId() {
    return alertBookingId;
  }

  public void setAlertBookingId(Long alertBookingId) {
    this.alertBookingId = alertBookingId;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public FlightAlert getFlightAlert() {
    return flightAlert;
  }

  public void setFlightAlert(FlightAlert flightAlert) {
    this.flightAlert = flightAlert;
  }

  public TripDetail getTripDetail() {
    return tripDetail;
  }

  public void setTripDetail(TripDetail tripDetail) {
    this.tripDetail = tripDetail;
  }

  public Trip getTrip() {
    return trip;
  }

  public void setTrip(Trip trip) {
    this.trip = trip;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
