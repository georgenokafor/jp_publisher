package controllers;

import com.google.inject.Inject;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.common.SessionConstants;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.form.PassengerForm;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.BaseView;
import com.mapped.publisher.view.TripPassengerInfoView;
import com.mapped.publisher.view.TripPassengerView;
import com.mapped.publisher.view.UserMessage;
import models.publisher.AccountTripLink;
import models.publisher.Trip;
import models.publisher.TripDetail;
import models.publisher.TripPassenger;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-15
 * Time: 8:59 AM
 */
public class TripPassengerController
    extends Controller {

  @Inject
  FormFactory formFactory;

  @With({Authenticated.class, Authorized.class})
  public Result addPassenger() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<PassengerForm> form = formFactory.form(PassengerForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return UserMessage.formErrors(form);
    }

    PassengerForm       passenger     = form.get();
    Trip trip = Trip.findByPK(passenger.getInTripId());
    if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).lt(SecurityMgr.AccessLevel.OWNER)) {
      BaseView baseView = new BaseView();
      baseView.message = "You are not authorized to edit this trip";
      return ok(views.html.common.message.render(baseView));
    }
    else {
      //check if email address is already registered
      List<AccountTripLink> tripLinks = AccountTripLink.findByEmailTrip(passenger.getInEmail(), trip.getTripid());
      if (tripLinks != null && tripLinks.size() > 0) {
        BaseView baseView = new BaseView();
        baseView.message = "This email address has already been added to this trip";
        return ok(views.html.common.message.render(baseView));
      }

      try {

        TripPassenger pm = new TripPassenger();
        pm.email = passenger.getInEmail();
        pm.name = passenger.getInName();
        pm.pk = Utils.getUniqueId();
        pm.tripid = passenger.getInTripId();
        pm.status = APPConstants.STATUS_ACTIVE;

        pm.createdtimestamp = System.currentTimeMillis();
        pm.lastupdatedtimestamp = System.currentTimeMillis();
        pm.createdby = sessionMgr.getUserId();
        pm.modifiedby = sessionMgr.getUserId();
        pm.save();
        flash(SessionConstants.SESSION_PARAM_MSG, passenger.getInName() + " Added");
        return passenger();
      }
      catch (Exception e) {
        BaseView baseView = new BaseView();
        baseView.message = "A system error has occurred";
        return ok(views.html.common.message.render(baseView));
      }
    }
  }

  @With({Authenticated.class, Authorized.class})
  public Result passenger() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    String     tripId     = sessionMgr.getParam(SessionConstants.SESSION_PARAM_TRIPID);
    if (tripId == null) {
      final DynamicForm form = formFactory.form().bindFromRequest();
      tripId = form.get("tripId");
    }
    if (tripId != null) {
      Trip trip = Trip.find.byId(tripId);
      if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.READ)) {
        TripPassengerInfoView view = new TripPassengerInfoView();
        String                msg  = flash(SessionConstants.SESSION_PARAM_MSG);
        if (msg != null) {
          view.message = msg;
        }
        view.tripId = trip.tripid;
        String           dateFormat    = "MMM dd, yyyy";
        SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);

        if (trip.starttimestamp > 0) {
          Date d = new Date(trip.starttimestamp);
          view.tripStartDate = dateFormatter.format(d);
        }

        if (trip.endtimestamp > 0) {
          Date d = new Date(trip.endtimestamp);
          view.tripEndDate = dateFormatter.format(d);
        }
        view.tripName = trip.name;
        view.tripStatus = trip.status;

        List<TripPassenger> passengers = TripPassenger.findActiveByTrip(tripId);
        if (passengers != null) {
          ArrayList<TripPassengerView> passengerViews = new ArrayList<TripPassengerView>();
          for (TripPassenger p : passengers) {
            TripPassengerView tv = new TripPassengerView();
            tv.id = p.pk;
            tv.name = p.name;
            tv.email = p.email;
            tv.note = p.comments;
            passengerViews.add(tv);
          }
          view.passengers = passengerViews;
        }
        view.numberOfBookings = TripDetail.numberOfBookings(trip.tripid);
        return ok(views.html.trip.passenger.render(view));
      }
    }
    BaseView view = new BaseView();
    view.message = "System Error";
    return ok(views.html.common.message.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result deletePassenger() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<PassengerForm> passengerForm = formFactory.form(PassengerForm.class);
    PassengerForm       passenger     = passengerForm.bindFromRequest().get();
    Trip                trip          = Trip.findByPK(passenger.getInTripId());

    if (passenger != null && passenger.getInPassengerId() != null &&
        SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.OWNER)) {
      TripPassenger pm = TripPassenger.find.byId(passenger.getInPassengerId());
      if (pm.tripid.equals(passenger.getInTripId())) {
        pm.setStatus(APPConstants.STATUS_DELETED);
        pm.setModifiedby(sessionMgr.getUserId());
        pm.setLastupdatedtimestamp(System.currentTimeMillis());
        pm.save();
        flash(SessionConstants.SESSION_PARAM_MSG, pm.name + " removed from the trip.");
        return passenger();
      }
    }

    BaseView baseView = new BaseView();
    baseView.message = "Error Deleting the traveller";
    return ok(views.html.common.message.render(baseView));

  }
}
