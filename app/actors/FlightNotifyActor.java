package actors;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.mapped.common.CoreConstants;
import com.mapped.common.EmailMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.Capability;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.common.SecurityMgr;
import com.mapped.publisher.msg.FlightNotifyMsg;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.FlightAlertView;
import com.umapped.helper.TripPublisherHelper;
import com.umapped.persistence.reservation.flight.UmFlightReservation;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import models.publisher.Account;
import models.publisher.AccountTripLink;
import models.publisher.FlightAlert;
import models.publisher.FlightAlertBooking;
import models.publisher.FlightAlertEvent;
import models.publisher.Trip;
import models.publisher.TripDetail;
import models.publisher.TripGroup;
/**
 * Created by twong on 2015-01-15.
 * <p/>
 * This actor can be invoked from the FlightAlertActor and the FlightTrackActor
 * It is responsible for sending out the actual alert notifications
 * <p/>
 * Try to batch the notification so as not to bombard the user...
 */
public class FlightNotifyActor
    extends UntypedActor {

  @Inject
  TripPublisherHelper tripPublisherHelper;

  public interface Factory {
    public Actor create();
  }
  /**
   * Akka Actor Properties
   */
  public final static Props props = Props.create(FlightNotifyActor.class);

  @Override
  public void onReceive(Object message)
      throws Exception {

    if (message instanceof FlightNotifyMsg) {
      FlightNotifyMsg msg = (FlightNotifyMsg) message;
      FlightAlert flightAlert = FlightAlert.find.byId(msg.flightAlertId);

      //get all notifications since the last events
      if (flightAlert != null && flightAlert.getStatus() >= 0) {
        try {
          if (msg.alertBookingId > 0) {
            //find last events if any
            FlightAlertBooking flightAlertBooking = FlightAlertBooking.find.byId(msg.alertBookingId);
            FlightAlertEvent event = FlightAlertEvent.findLatestByAlertId(msg.flightAlertId);
            List<FlightAlertEvent> events = new ArrayList<>();
            events.add(event);

            if (event != null) {
              TripDetail td = flightAlertBooking.getTripDetail();
              processTripBooking(td, events, flightAlert);
            }
          }
          else {
            //get all notifications to send
            List<FlightAlertEvent> events = FlightAlertEvent.findPending(msg.flightAlertId);
            if (events != null && events.size() > 0) {
              //get the alerts
              List<FlightAlertBooking> bookings = FlightAlertBooking.findByFlightAlert(flightAlert);
              if (bookings != null && bookings.size() > 0) {
                for (FlightAlertBooking booking: bookings) {
                  TripDetail td = booking.getTripDetail();
                  processTripBooking(td, events, flightAlert);
                }
              }

              List<Long> eventIds = new ArrayList<>();
              for (FlightAlertEvent fae : events) {
                eventIds.add(fae.eventId);
              }
              FlightAlertEvent.setEventProcessed(msg.flightAlertId, eventIds);
            }
          }
        }
        catch (Exception e) {
          e.printStackTrace();
          Log.err("FlightNotifyActor: Exception: AlertId: " + msg.flightAlertId, e);
        }
      }
    }
  }

  public void processTripBooking (TripDetail td, List<FlightAlertEvent> events, FlightAlert alert) {
    if (td != null && td.status == APPConstants.STATUS_ACTIVE) {
      Trip t = Trip.find.byId(td.tripid);
      if (t != null && t.status == APPConstants.STATUS_ACTIVE && SecurityMgr.hasCapability(t, Capability.FLIGHT_NOTIFICATIONS)) {
        //get all passengers to send to
        List<AccountTripLink> passengers = AccountTripLink.findByTrip(t.tripid);
        if (passengers != null) {
          //send the last notification to the users if applicable
          List<Account> travelers = new ArrayList<>();
          for (AccountTripLink tripLink: passengers) {
            Account traveler = Account.find.byId(tripLink.getPk().getUid());
            if (traveler != null) {
              travelers.add(traveler);
            }
          }
          //update the trip detail
          try {
            UmFlightReservation reservation = cast(td.getReservation(), UmFlightReservation.class);
            if (reservation != null) {
              FlightAlertEvent alertEvent = null;
              if (events != null && !events.isEmpty()) {
                alertEvent = events.get(events.size() - 1);
              }
              String newComments = FlightAlertsHelper.generateComment(reservation, alertEvent , alert);
              reservation.setNotesPlainText(newComments);
            }
            td.setStarttimestamp(alert.newDepartTime);

            td.setEndtimestamp(alert.newArriveTime);
            td.setModifiedby("system");
            td.setLastupdatedtimestamp(Instant.now().toEpochMilli());
            td.save();


          } catch (Exception e) {
            //in case of concurrent modification
          }

          //auto publish the trip
          List<TripGroup> tripgroups = TripGroup.findActiveByTripId(t.getTripid());
          if (tripgroups != null && tripgroups.size() > 0) {
            for (TripGroup tripGroup: tripgroups) {
              tripPublisherHelper.autoPublishTrip(t, tripGroup.getGroupid());
            }
          }
          tripPublisherHelper.autoPublishTrip(t, null);

          //notify the user
          sendEmailNotification(travelers, events, alert, t);
        }
      }
    }
  }

  public void sendEmailNotification(List<Account> travelers,
                                    List<FlightAlertEvent> alertEvents,
                                    FlightAlert alert,
                                    Trip trip) {
    //process all events and then process all passengers

    if (travelers == null || travelers.size() == 0 || alertEvents == null || alertEvents.size() == 0) {
      return;
    }
    //send email notifications
    //send email
    try {
      FlightAlertView view = new FlightAlertView();

      String sender = "Umapped";

      String smtp      = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_HOST);
      String user      = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_USER_ID);
      String pwd       = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_SMTP_PASSWORD);
      String fromEmail = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.EMAIL_TRIP_NOTIFICATION_FROM);

      String emailEnabled = ConfigMgr.getAppParameter(CoreConstants.EMAIL_ENABLED);

      if (emailEnabled != null && emailEnabled.equalsIgnoreCase("Y")) {
        view.airlineCode = alert.airlineCode;
        view.airlineName = alert.airlineName;
        view.departAirportCode = alert.departAirport;
        view.departAirportName = alert.departAirportName;
        view.arriveAirportCode = alert.arrivalAirport;
        view.arriveAirportName = alert.arrivalAirportName;
        view.arriveGate = alert.newArriveGate;
        view.arriveTerminal = alert.newArriveTerminal;
        view.departGate = alert.newDepartGate;
        view.departTerminal = alert.newDepartTerminal;
        view.origDepartTime = Utils.formatDateTimePrint(alert.newDepartTime);
        view.flightId = String.valueOf(alert.flightId);
        view.tripName = trip.getName();

        //initialize email mgr
        EmailMgr emailMgr = new EmailMgr(smtp, user, pwd);
        emailMgr.init();

        List<String> toEmails = new ArrayList<>();
        toEmails.add("thierry@umapped.com");

        List<Map<String, String>> body = new ArrayList<>();
        Map<String, String> token;


        view.title = alert.airlineName + " -  " + alert.airlineCode + " " + alert.flightId + " departing " + alert
            .departAirportName;

        List<String> bccEmails = new ArrayList<>();
        for (Account traveler : travelers) {
          if (traveler != null && traveler.getEmail() != null && Utils.isValidEmailAddress(traveler.getEmail() ) && !bccEmails.contains(traveler.getEmail() )) {
            bccEmails.add(traveler.getEmail() );
          }
        }

        view.events = new ArrayList<>();
        for (FlightAlertEvent event : alertEvents) {
          FlightAlertView.FlightAlertEventView eventView = view.new FlightAlertEventView();
          eventView.alertType = event.alertType;
          eventView.departTime = Utils.formatDateTimePrint(event.departTimestamp);
          eventView.arriveTime = Utils.formatDateTimePrint(event.arriveTimestamp);
          eventView.timestamp = Utils.formatDateTimePagePrint(event.alertTimestamp);
          view.events.add(eventView);

          if (event.alertType == FlightAlertEvent.FlightAlertType.CANCELLED ||
              event.alertType == FlightAlertEvent.FlightAlertType.DEPARTURE_DELAY ||
              event.alertType == FlightAlertEvent.FlightAlertType.GATE_ADJUSTMENT) {
            view.alertType = event.alertType;
          }
          else if (view.alertType == null || (view.alertType != FlightAlertEvent.FlightAlertType.CANCELLED &&
                                              view.alertType != FlightAlertEvent.FlightAlertType.DEPARTURE_DELAY &&
                                              view.alertType != FlightAlertEvent.FlightAlertType.GATE_ADJUSTMENT)) {
            view.alertType = event.alertType;
          }
        }

        String subject = alert.airlineName + " - " + alert.airlineCode + " " + alert.flightId + " from " + alert
            .departAirportName;
        if (view.alertType == null) {
          subject = "Flight Update: " + subject;
        }
        else if (view.alertType == FlightAlertEvent.FlightAlertType.CANCELLED) {
          subject = "Flight Cancelled: " + subject;
        }
        else if (view.alertType == FlightAlertEvent.FlightAlertType.DEPARTURE_DELAY) {
          subject = "Flight Delayed: " + subject;
        }
        else {
          subject = "Flight Update: " + subject;
        }


        String emailBody = views.html.email.flightNotification.render(view).toString();

        token = new HashMap<>();
        token.put(com.mapped.common.CoreConstants.HTML, emailBody);
        body.add(token);

        emailMgr.send(fromEmail, sender, toEmails, bccEmails, subject, body, null);
      }
      Log.info("FlightNotifyActor : Notifications sent: " + alert.getFlightAlertId());

    }
    catch (Exception e) {
      e.printStackTrace();
      Log.err("Cannot send trip email. AlertId: " + alert.getFlightAlertId(), e);
    }
  }
}
