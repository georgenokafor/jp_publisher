package com.mapped.publisher.form;

import models.publisher.BillingEntity;
import models.publisher.BillingPmntMethod;
import models.publisher.BillingSchedule;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-18
 * Time: 6:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserForm
    extends UserProfileForm {
  /*
   * BILLING
   */
  public BillingEntity.Type     billingBilledTo;
  public BillingSchedule.Type   billingSchedule;
  public Long                   billingPlan;
  public BillingPmntMethod.Type billingPmntMethod;
  public String                 billingStartDate;
  public String                 serviceCutoffDate;
  public Boolean                billingRecurrent;
  public String                 billingCmpyId;
  public boolean                sendEmail;


  public String billingAgentId;
  private String inCmpyId;
  private String inCmpyMode;
  private String inEditMode;
  private String inUserType;
  private String monthlyBasicPlanId;
  private String monthlyProPlanId;
  private String yearlyProPlanId;
  private String yearlyBasicPlanId;

  public UserForm() {
    billingBilledTo = BillingEntity.Type.USER;
    billingSchedule = BillingSchedule.Type.NONE;
    billingPmntMethod = BillingPmntMethod.Type.CREDIT_CARD;
  }

  public String getBillingAgentId() {
    return billingAgentId;
  }

  public void setBillingAgentId(String billingAgentId) {
    this.billingAgentId = billingAgentId;
  }

  public BillingEntity.Type getBillingBilledTo() {
    return billingBilledTo;
  }

  public void setBillingBilledTo(BillingEntity.Type billingBilledTo) {
    this.billingBilledTo = billingBilledTo;
  }

  public BillingSchedule.Type getBillingSchedule() {
    return billingSchedule;
  }

  public void setBillingSchedule(BillingSchedule.Type billingSchedule) {
    this.billingSchedule = billingSchedule;
  }

  public Long getBillingPlan() {
    return billingPlan;
  }

  public void setBillingPlan(Long billingPlan) {
    this.billingPlan = billingPlan;
  }

  public BillingPmntMethod.Type getBillingPmntMethod() {
    return billingPmntMethod;
  }

  public void setBillingPmntMethod(BillingPmntMethod.Type billingPmntMethod) {
    this.billingPmntMethod = billingPmntMethod;
  }

  public String getBillingStartDate() {
    return billingStartDate;
  }

  public void setBillingStartDate(String billingStartDate) {
    this.billingStartDate = billingStartDate;
  }

  public String getServiceCutoffDate() {
    return serviceCutoffDate;
  }

  public void setServiceCutoffDate(String serviceCutoffDate) {
    this.serviceCutoffDate = serviceCutoffDate;
  }

  public Boolean getBillingRecurrent() {
    return billingRecurrent;
  }

  public void setBillingRecurrent(Boolean billingRecurrent) {
    this.billingRecurrent = billingRecurrent;
  }

  public String getBillingCmpyId() {
    return billingCmpyId;
  }

  public void setBillingCmpyId(String billingCmpyId) {
    this.billingCmpyId = billingCmpyId;
  }

  public String getInCmpyId() {
    return inCmpyId;
  }

  public void setInCmpyId(String inCmpyId) {
    this.inCmpyId = inCmpyId;
  }

  public String getInCmpyMode() {
    return inCmpyMode;
  }

  public void setInCmpyMode(String inCmpyMode) {
    this.inCmpyMode = inCmpyMode;
  }

  public String getInEditMode() {
    return inEditMode;
  }

  public void setInEditMode(String inEditMode) {
    this.inEditMode = inEditMode;
  }

  public String getInUserType() {
    return inUserType;
  }

  public void setInUserType(String inUserType) {
    this.inUserType = inUserType;
  }

  public boolean getSendEmail() {
    return sendEmail;
  }

  public void setSendEmail(boolean sendEmail) {
    this.sendEmail = sendEmail;
  }

  public String getMonthlyBasicPlanId() {
    return monthlyBasicPlanId;
  }

  public void setMonthlyBasicPlanId(String monthlyBasicPlanId) {
    this.monthlyBasicPlanId = monthlyBasicPlanId;
  }

  public String getMonthlyProPlanId() {
    return monthlyProPlanId;
  }

  public void setMonthlyProPlanId(String monthlyProPlanId) {
    this.monthlyProPlanId = monthlyProPlanId;
  }

  public String getYearlyProPlanId() {
    return yearlyProPlanId;
  }

  public void setYearlyProPlanId(String yearlyProPlanId) {
    this.yearlyProPlanId = yearlyProPlanId;
  }

  public String getYearlyBasicPlanId() {
    return yearlyBasicPlanId;
  }

  public void setYearlyBasicPlanId(String yearlyBasicPlanId) {
    this.yearlyBasicPlanId = yearlyBasicPlanId;
  }
}
