package com.mapped.publisher.view.preferences;

import com.mapped.publisher.parse.classicvacations.datesearch.Booking;
import com.mapped.publisher.view.BaseView;
import com.umapped.persistence.accountprop.TripPrefs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 14/03/16.
 */
public class PreferencesView extends BaseView {
    public TripPrefs.TRIP_PDF_TMPLT pdfTemplate = TripPrefs.TRIP_PDF_TMPLT.DEF;
    public boolean printFriendlyPdf = false;
    public boolean display24Hr = false;

    public List<CapabilityPrefs> capabilities = new ArrayList<>();

   public class CapabilityPrefs {
     public String name;
     public String description;
     public Integer capabilityPK;
     public boolean enabled;
   }

}
