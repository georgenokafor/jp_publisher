package com.mapped.publisher.actions;

import com.mapped.publisher.utils.Log;
import com.umapped.mobile.api.model.MobileAPIResponse;
import com.umapped.mobile.api.model.MobileAPIResponseCode;
import models.publisher.Account;
import models.publisher.AccountSession;
import org.apache.commons.lang3.StringUtils;
import play.libs.Json;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by wei on 2017-08-16.
 */
public class MobileSessionAuth extends Action.Simple {
  @Override public CompletionStage<Result> call(Http.Context ctx) {
    String sessionId = ctx.request().getHeader("X-UM-SESSION-ID");

    if (StringUtils.isEmpty(sessionId)) {
      return CompletableFuture.completedFuture(unauthorized(Json.toJson(new MobileAPIResponse(MobileAPIResponseCode.MISSING_SESSION_ID))));
    }

    try {
      Long sessionUid = Long.parseLong(sessionId);
      AccountSession session = AccountSession.findById(sessionUid);
      ctx.args.put("AccountSession", session);
      return delegate.call(ctx);
    } catch (Exception ex) {
      Log.err("Failed to get session: " + sessionId, ex);
    }
    return CompletableFuture.completedFuture(unauthorized(Json.toJson(new MobileAPIResponse(MobileAPIResponseCode.MISSING_SESSION_ID))));
  }
}
