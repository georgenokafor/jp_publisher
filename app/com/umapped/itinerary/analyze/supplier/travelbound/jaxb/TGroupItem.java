//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.04.17 at 04:55:33 PM EDT 
//


package com.umapped.itinerary.analyze.supplier.travelbound.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_GroupItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_GroupItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Item" type="{}t_Item"/>
 *         &lt;element name="RoomCategory" type="{}t_Item"/>
 *       &lt;/sequence>
 *       &lt;attribute name="PrimaryHotel" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_GroupItem", propOrder = {
    "item",
    "roomCategory"
})
public class TGroupItem {

    @XmlElement(name = "Item", required = true)
    protected TItem item;
    @XmlElement(name = "RoomCategory", required = true)
    protected TItem roomCategory;
    @XmlAttribute(name = "PrimaryHotel")
    protected Boolean primaryHotel;

    /**
     * Gets the value of the item property.
     * 
     * @return
     *     possible object is
     *     {@link TItem }
     *     
     */
    public TItem getItem() {
        return item;
    }

    /**
     * Sets the value of the item property.
     * 
     * @param value
     *     allowed object is
     *     {@link TItem }
     *     
     */
    public void setItem(TItem value) {
        this.item = value;
    }

    /**
     * Gets the value of the roomCategory property.
     * 
     * @return
     *     possible object is
     *     {@link TItem }
     *     
     */
    public TItem getRoomCategory() {
        return roomCategory;
    }

    /**
     * Sets the value of the roomCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link TItem }
     *     
     */
    public void setRoomCategory(TItem value) {
        this.roomCategory = value;
    }

    /**
     * Gets the value of the primaryHotel property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPrimaryHotel() {
        return primaryHotel;
    }

    /**
     * Sets the value of the primaryHotel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrimaryHotel(Boolean value) {
        this.primaryHotel = value;
    }

}
