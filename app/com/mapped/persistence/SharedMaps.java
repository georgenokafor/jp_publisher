package com.mapped.persistence;
 
import java.util.*;
	 import com.mapped.persistence.util.*;
 import java.sql.*; 
 import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class SharedMaps { 
 
	 public final static String SharedMapsTypeId = "220"; 
	 protected static String pkTypeId =  "221"; 
	 private String pk; 
	 protected static String mapsIdTypeId =  "222"; 
	 private String mapsId; 
	 protected static String ownerTypeId =  "223"; 
	 private String owner; 
	 protected static String shareUserIdTypeId =  "224"; 
	 private String shareUserId; 
	 protected static String shareTypeTypeId =  "225"; 
	 private String shareType; 
	 protected static String statusTypeId =  "226"; 
	 private long status; 
	 protected static String lastUpdateTimestampTypeId =  "227"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "228"; 
	 private long createTimestamp; 
	 protected static String shareEmailTypeId =  "229"; 
	 private String shareEmail; 
	 protected static String syncTimestampTypeId =  "230"; 
	 private long syncTimestamp; 
	 protected static String msgTypeId =  "231"; 
	 private String msg; 

 	 // Contructor 
	 public SharedMaps () {} 
	 public SharedMaps ( String pk,  String mapsId,  String owner,  String shareUserId,  String shareType,  long status,  long lastUpdateTimestamp,  long createTimestamp,  String shareEmail,  long syncTimestamp,  String msg ) { 
		 this.pk=pk; 
		 this.mapsId=mapsId; 
		 this.owner=owner; 
		 this.shareUserId=shareUserId; 
		 this.shareType=shareType; 
		 this.status=status; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.shareEmail=shareEmail; 
		 this.syncTimestamp=syncTimestamp; 
		 this.msg=msg; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getMapsId () { 
	 	 return mapsId; 
 	 } 
 
 	 public void setMapsId(String mapsId) { 
	 	 this.mapsId = mapsId; 
 	 } 
 
 	 public  String getOwner () { 
	 	 return owner; 
 	 } 
 
 	 public void setOwner(String owner) { 
	 	 this.owner = owner; 
 	 } 
 
 	 public  String getShareUserId () { 
	 	 return shareUserId; 
 	 } 
 
 	 public void setShareUserId(String shareUserId) { 
	 	 this.shareUserId = shareUserId; 
 	 } 
 
 	 public  String getShareType () { 
	 	 return shareType; 
 	 } 
 
 	 public void setShareType(String shareType) { 
	 	 this.shareType = shareType; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  String getShareEmail () { 
	 	 return shareEmail; 
 	 } 
 
 	 public void setShareEmail(String shareEmail) { 
	 	 this.shareEmail = shareEmail; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  String getMsg () { 
	 	 return msg; 
 	 } 
 
 	 public void setMsg(String msg) { 
	 	 this.msg = msg; 
 	 } 
 
 	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: SharedMaps"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 MapsId: ").append(this.mapsId );  
		 sb.append("	 Owner: ").append(this.owner );  
		 sb.append("	 ShareUserId: ").append(this.shareUserId );  
		 sb.append("	 ShareType: ").append(this.shareType );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 ShareEmail: ").append(this.shareEmail );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 Msg: ").append(this.msg );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", SharedMapsTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (mapsId != null) {
			 memberNode.put(mapsIdTypeId, mapsId);
		 }
		 if (owner != null) {
			 memberNode.put(ownerTypeId, owner);
		 }
		 if (shareUserId != null) {
			 memberNode.put(shareUserIdTypeId, shareUserId);
		 }
		 if (shareType != null) {
			 memberNode.put(shareTypeTypeId, shareType);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (shareEmail != null) {
			 memberNode.put(shareEmailTypeId, shareEmail);
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (msg != null) {
			 memberNode.put(msgTypeId, msg);
		 }
		 return classNode;
	 } 
	 public static SharedMaps parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 SharedMaps  c= new SharedMaps(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.mapsId = fields.findPath(mapsIdTypeId).textValue();
			 c.owner = fields.findPath(ownerTypeId).textValue();
			 c.shareUserId = fields.findPath(shareUserIdTypeId).textValue();
			 c.shareType = fields.findPath(shareTypeTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 c.shareEmail = fields.findPath(shareEmailTypeId).textValue();
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 c.msg = fields.findPath(msgTypeId).textValue();
			 return c;
		} else
			 return null;
	 } 
} 
 
