package com.mapped.publisher.common;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2012-10-11
 * Time: 11:13 AM
 * To change this template use File | Settings | File Templates.
 */
public class CoreConstants {

  public final static String DB_URL = "DB_URL";
  public final static String DB_DRIVER = "DB_DRIVER";
  public final static String DB_USERID = "DB_USERID";
  public final static String DB_PWD = "DB_PWD";
  public final static String LOG_LEVEL = "logLevel";
  public final static String APP_NAME = "APP_NAME";
  public final static String ENVIRONMENT = "ENV";
  public final static String DEV = "DEV";
  public final static String PROD = "PROD";
  public final static String SESSION_TIMEOUT = "SESSION_TIMEOUT"; //mins
  public final static String PUBLISHER_APP = "WEB_PUBLISHER";


  public final static String TEXT = "TEXT";
  public final static String HTML = "HTML";

  public final static String VIDEO = "Video";
  public final static String AUDIO = "Audio";
  public final static String IMAGE = "Image";
  public final static String AUTHENTICATED_ = "Authenticated";
}
