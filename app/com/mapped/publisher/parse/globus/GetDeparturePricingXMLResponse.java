
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetDeparturePricingXMLResult" type="{http://webapi.globusandcosmos.com/}ArrayOfDeparturePricing" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDeparturePricingXMLResult"
})
@XmlRootElement(name = "GetDeparturePricingXMLResponse")
public class GetDeparturePricingXMLResponse {

    @XmlElement(name = "GetDeparturePricingXMLResult")
    protected ArrayOfDeparturePricing getDeparturePricingXMLResult;

    /**
     * Gets the value of the getDeparturePricingXMLResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDeparturePricing }
     *     
     */
    public ArrayOfDeparturePricing getGetDeparturePricingXMLResult() {
        return getDeparturePricingXMLResult;
    }

    /**
     * Sets the value of the getDeparturePricingXMLResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDeparturePricing }
     *     
     */
    public void setGetDeparturePricingXMLResult(ArrayOfDeparturePricing value) {
        this.getDeparturePricingXMLResult = value;
    }

}
