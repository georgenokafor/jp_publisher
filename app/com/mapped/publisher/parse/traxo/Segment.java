package com.mapped.publisher.parse.traxo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.List;

/**
 * Created by twong on 10/06/17.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Segment {
    public String confirmation_no;
    public String created;
    public String currency;
    public String first_name;
    public String last_name;
    public String price;
    public String source;
    public String status;
    public List<Traveler> travelers;
    public List<PriceDetail> price_details;
}
