package com.mapped.publisher.parse.extractor.booking.valueObject;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.utils.Log;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-10-25
 * Time: 9:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class TripVO
    extends BaseVO {

  private List<NoteVO> notes = new ArrayList<>();
  private List<PassengerVO> passengers;
  private List<FlightVO> flights;
  private List<HotelVO> hotels;
  private List<CruiseVO> cruises;
  private List<ActivityVO> activities;
  private List<TransportVO> transport;
  private Timestamp startTimestamp;
  private Timestamp endTimestamp;

  private BookingSrc.ImportSrc importSrc;
  private String importSrcId;
  private long importTs;

  private boolean noPOIMatch = false;

  public enum ValObjects {
    VO_FLIGHT,
    VO_ACTIVITY,
    VO_HOTEL,
    VO_CRUISE,
    VO_TRANSPORT,
    VO_PASSENGER
  }

  public boolean hasValidBookings() {
    int count = 0;

    for(FlightVO vo : flights) {
      if (vo.getCode() != null || vo.getNumber() != null) {
        count++;
      }
    }

    for(CruiseVO vo : cruises) {
      if (vo.getName() != null) {
        count++;
      }
    }

    for(HotelVO vo : hotels) {
      if (vo.getName() != null) {
        count++;
      }
    }

    for(ActivityVO vo : activities) {
      if (vo.getName() != null) {
        count++;
      }
    }

    for(TransportVO vo : transport) {
      if (vo.getName() != null) {
        count++;
      }
    }

    return count > 0;
  }

  public TripVO() {
    /* Constructing all elements with initial capacity half of the default 10 */
    passengers = new ArrayList<>(5);
    flights = new ArrayList<>(5);
    hotels = new ArrayList<>(5);
    cruises = new ArrayList<>(5);
    activities = new ArrayList<>(5);
    transport = new ArrayList<>(5);
    startTimestamp = new Timestamp(0);
    endTimestamp = new Timestamp(0);
  }

  public TripVO setNoPOIMatch() {
    this.noPOIMatch = true;
    return this;
  }

  public void addCruise(CruiseVO cruise) {
    if (cruise != null) {
      cruises.add(cruise);
      updateStartEndTimestamps(cruise.getTimestampDepart(), cruise.getTimestampArrive());
    }
  }

  private void updateStartEndTimestamps(Timestamp start, Timestamp stop) {
    if (start != null && (startTimestamp.getTime() > start.getTime() || startTimestamp.getTime() == 0)) {
      startTimestamp.setTime(start.getTime());
    }

    if (stop != null && endTimestamp.getTime() < stop.getTime()) {
      endTimestamp.setTime(stop.getTime());
    }
    else if (start != null && endTimestamp.getTime() < start.getTime()) {
      endTimestamp.setTime(start.getTime());
    }
  }

  public void addPassenger(PassengerVO passenger) {
    if (passenger != null) {
      if (!passengers.contains(passenger)) {
        passengers.add(passenger);
      }
      else {
        Log.debug("Skipping existing passenger:" + passenger.toString());
      }
    }
  }

  public void addFlight(FlightVO flight) {
    if (flight != null) {
      flights.add(flight);
      updateStartEndTimestamps(flight.getDepatureTime(), flight.getArrivalTime());
    }
  }

  public void addHotel(HotelVO hotel) {
    if (hotel != null) {
      hotels.add(hotel);
      updateStartEndTimestamps(hotel.getCheckin(), hotel.getCheckout());
    }
  }

  public void addTransport(TransportVO transportVO) {
    if (transport != null) {
      transport.add(transportVO);
      updateStartEndTimestamps(transportVO.getPickupDate(), transportVO.getDropoffDate());
    }
  }

  public void addActivity(ActivityVO activityVO) {
    if (activityVO != null) {
      activities.add(activityVO);
      updateStartEndTimestamps(activityVO.getStartDate(), activityVO.getEndDate());
    }
  }

  public List<PassengerVO> getPassengers() {
    return passengers;
  }

  public void setPassengers(List<PassengerVO> passengers) {
    this.passengers = passengers;
  }

  public List<FlightVO> getFlights() {
    return flights;
  }

  public void setFlights(List<FlightVO> flights) {
    this.flights = flights;
  }

  public List<HotelVO> getHotels() {
    return hotels;
  }

  public List<CruiseVO> getCruises() {
    return cruises;
  }

  public void setHotels(List<HotelVO> hotels) {
    this.hotels = hotels;
  }

  public List<ActivityVO> getActivities() {
    return activities;
  }

  public void setActivities(List<ActivityVO> activities) {
    this.activities = activities;
  }

  public List<TransportVO> getTransport() {
    return transport;
  }

  public void setTransport(List<TransportVO> transport) {
    this.transport = transport;
  }

  public Timestamp getStartTimestamp() {
    return startTimestamp;
  }

  public Timestamp getEndTimestamp() {
    return endTimestamp;
  }

  public BookingSrc.ImportSrc getImportSrc() {
    return importSrc;
  }

  public void setImportSrc(BookingSrc.ImportSrc importSrc) {
    this.importSrc = importSrc;
  }

  public String getImportSrcId() {
    return importSrcId;
  }

  public void setImportSrcId(String importSrcId) {
    this.importSrcId = importSrcId;
  }

  public long getImportTs() {
    return importTs;
  }

  public void setImportTs(long importTs) {
    this.importTs = importTs;
  }

  public String toString() {
    String nl = System.getProperty("line.separator");
    StringBuilder strBuilder = new StringBuilder("Trip (Value Object)" + nl);

    strBuilder.append(nl + "<<< Trip Passengers" + nl);
    for (PassengerVO pvo : passengers) {
      strBuilder.append(pvo.toString());
    }

    strBuilder.append(nl + "<<< Trip Flights" + nl);
    for (FlightVO fvo : flights) {
      strBuilder.append(fvo.toString());
    }

    strBuilder.append(nl + "<<< Trip Hotels" + nl);
    for (HotelVO hvo : hotels) {
      strBuilder.append(hvo.toString());
    }

    strBuilder.append(nl + "<<< Trip Cruises" + nl);
    for (CruiseVO cvo : cruises) {
      strBuilder.append(cvo.toString());
    }


    strBuilder.append(nl + "<<< Trip Activities" + nl);
    for (ActivityVO avo : activities) {
      strBuilder.append(avo.toString());
    }


    strBuilder.append(nl + "<<< Trip Transportation" + nl);
    for (TransportVO tvo : transport) {
      strBuilder.append(tvo.toString());
    }

    strBuilder.append(nl + "<<< Trip Notes" + nl);
    for (NoteVO tvo : notes) {
      strBuilder.append(tvo.toString());
    }

    return strBuilder.toString();
  }

  public List<NoteVO> getNotes() {
    return notes;
  }

  public void setNotes(List<NoteVO> notes) {
    this.notes = notes;
  }

  public void addNoteVO (NoteVO noteVO) {

    this.notes.add(noteVO);
  }

  public boolean isNoPOIMatch () {
    return noPOIMatch;
  }
}
