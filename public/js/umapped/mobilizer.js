/**
 * Created by surge on 2014-12-15.
 */

var initMobilizer = function () {
  $('#mobilizerSearchSubmit').unbind("click").click(function(){
    if($("#webSearchKeywords").val() && $("#webSearchKeywords").val().length > 1) {

      mobilizerSearch();
    } else {
      alert("Please enter a keyword");
    }
    return false;
  });

  $('#travel42Submit').unbind("click").click(function(){
    mobilizeTravel42();
    return false;
  });

  $('#travel42LoginSubmit').unbind("click").click(function(){
    if($("#travel42UserId").val() && $("#travel42UserId").val().length > 1 && $("#travel42Password").val() && $("#travel42Password").val().length > 1 ) {
      mobilizerTravel42Login();
    } else {
      alert("Please enter a username and password");
    }
    return false;
  });

  $('#webSearchForm').submit(function(){
    mobilizerSearch();
    return false;
  });

  $('#travel42Form').submit(function(){
    mobilizeTravel42();
    return false;
  });

  $('#mobilizerAddSubmit').unbind("click").click(function(){
    mobilizerAddPage();
    return false;
  });


  $('#travel42SelectReport').unbind("click").click(function(){
    mobilizeTravel42Report();
    return false;
  });


  //if there are travel 42 reports, hide the login and display the travel 42 list
  var numOfReports = $('#travel42ReportList option').size();
  if (numOfReports > 1) {
    $('#Travel42Report').show();
    $('#Travel42Login').hide();

  } else {
    $('#Travel42Report').hide();
    $('#Travel42Login').show();
  }

  $('#travel42RefreshReport').unbind("click").click(function(){
    $('#Travel42Report').hide();
    $('#Travel42Login').show();
  });


};

var mobilizerRequest = null;

var mobilizeTravel42Report = function() {
  var url = $('#travel42ReportList').val();

  if (url.indexOf('travel-42.com') == -1) {
    $('#travel42Url').val('');
    $('#travel42Alert').text("Only Travel42 urls are accepted in this mode").show();
    return false;
  }

  $('#webSearchSpinner').show();
  //console.log("Process URL ID:"+  resultId);
  var request     = new ApiMessage();
  request.header  = new ApiRequestHeader();
  request.body    = new WebSearch(EnumOperations.UPLOAD);
  request['@type'] = 'WEBSEARCH';

  request.body.cmpyid = $('#travel42ReportCmpyId').val();
  request.body.id =  $('#travel42ReportDocId').val();


  request.body.addPage(null, null, url, null, null);

  ExecuteCommand(request, "/mobilizer/execute.json", function(data){
    if(data.body.jobId) {
      setTimeout(function() {
        mobilizerTrave42PlanCheck(data.body.jobId);
      }, 5000);
    }
  }, function(data){
    $('#webSearchSpinner').hide();
    $('#mobilizerModal').modal("hide");
    $('#mobilizerResultModal').modal("hide");
  });
};


var mobilizerTrave42PlanCheck = function(jobId) {
  $('#webSearchSpinner').show();

  var request     = new ApiMessage();
  request.header  = new ApiRequestHeader();
  request.body    = new WebSearch(EnumOperations.STATE);
  request['@type'] = 'WEBSEARCH';

  request.body.cmpyid = $('#travel42ReportCmpyId').val();
  request.body.id =  $('#travel42ReportDocId').val();
  request.body.jobId = jobId;

  ExecuteCommand(request, "/mobilizer/execute.json", function(data){

    switch (data.body.state) {
      case 'SUCCESS':
        $('#travel42Url').val('');
        if (data.body.pages && data.body.pages.length > 0) {
          $('#pagesAccordion').children().remove();
          for (var idx = 0; idx < data.body.pages.length; idx++) {
            addAccordionPage(data.body.pages[idx], (data.body.pages.length == 1 && idx == 0) ? true : false);
            var idInputField = $('<input type="hidden" id="mobilizedLibraryId[' + idx + ']">');
            idInputField.val(data.body.pages[idx].resultId);
            $('#mobilizerForm').append(idInputField);
          }

          $('#mobilizedDocId').val(data.body.id);
          $('#mobilizedCmpyId').val(data.body.cmpyid);
        }
        else {
          $('#mobilizedResults').append('<div class="alert alert-error">Failed to parse page</div>');
        }
        $('#webSearchSpinner').hide();
        $('#mobilizerResultModal').modal();
        break;
      case 'ERROR':
        $('#webSearchSpinner').hide();
        $('#mobilizerModal').modal("hide");
        $('#mobilizerResultModal').modal("hide");
        headLineAlertDisplay("travel42.com connection failure. Please try again later.");
        break;
      default:
        setTimeout(function() {
          mobilizerTrave42PlanCheck(jobId);
        }, 5000);
    }
  }, function(data){
    $('#webSearchSpinner').hide();
    $('#mobilizerModal').modal("hide");
    $('#mobilizerResultModal').modal("hide");
  });

};


var mobilizeTravel42 = function() {
  var url = $('#travel42Url').val();

  if (url.indexOf('travel-42.com') == -1) {
    $('#travel42Url').val('');
    $('#travel42Alert').text("Only Travel42 urls are accepted in this mode").show();
    return false;
  }

  $('#webSearchSpinner').show();
  //console.log("Process URL ID:"+  resultId);
  var request     = new ApiMessage();
  request.header  = new ApiRequestHeader();
  request.body    = new WebSearch(EnumOperations.UPLOAD);
  request['@type'] = 'WEBSEARCH';

  request.body.cmpyid = $('#travel42CmpyId').val();
  request.body.id =  $('#travel42DocId').val();


  request.body.addPage(null, null, url, null, null);

  ExecuteCommand(request, "/mobilizer/execute.json", function(data){
    $('#travel42Url').val('');
    if (data.body.pages && data.body.pages.length > 0) {
      $('#pagesAccordion').children().remove();
      for(var idx = 0; idx < data.body.pages.length; idx++) {
        addAccordionPage(data.body.pages[idx], (data.body.pages.length == 1 && idx == 0)?true:false);
        var idInputField = $('<input type="hidden" id="mobilizedLibraryId['+idx+']">');
        idInputField.val(data.body.pages[idx].resultId);
        $('#mobilizerForm').append(idInputField);
      }

      $('#mobilizedDocId').val(data.body.id);
      $('#mobilizedCmpyId').val(data.body.cmpyid);
    } else {
      $('#mobilizedResults').append('<div class="alert alert-error">Failed to parse page</div>');
    }
    $('#webSearchSpinner').hide();
    $('#mobilizerResultModal').modal();
  }, function(data){
    $('#webSearchSpinner').hide();
    $('#mobilizerModal').modal("hide");
    $('#mobilizerResultModal').modal("hide");
  });
};

var addAccordionPage = function(page, expand) {
  var accordionGroup    = $('<div class="accordion-group">');
  var accordionHeading  = $('<div class="accordion-heading pageTitle">');
  var accordionToggle   = $('<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#pagesAccordion">');
  accordionToggle.attr("href", "#pageContent" + page.resultId);
  accordionToggle.text(page.title);
  accordionHeading.append(accordionToggle);

  var accordionContent = $('<div class="accordion-body collapse">');
  accordionContent.attr("id", "pageContent" + page.resultId);
  if (expand) {
    accordionContent.addClass("in")
  }
  var accordionInner = $('<div class="accordion-inner">');
  if(page.title.toLowerCase() != 'null') {
    accordionInner.append(page.content);
  } else {
    accordionInner.append("This page's content cannot be retrieved.  Please try another page.");
  }
  accordionContent.append(accordionInner);
  accordionGroup.append(accordionHeading);
  accordionGroup.append(accordionContent);

  $('#pagesAccordion').append(accordionGroup);
};

/**
 * Gather parameters from the search form and send it to find results.
 * On success populate
 */
var mobilizerSearch = function () {
  var request = new ApiMessage();
  request.header = new ApiRequestHeader();
  request.body = new WebSearch(EnumOperations.SEARCH);
  request['@type'] = 'WEBSEARCH';

  if(mobilizerRequest) {
    //console.log("Aborting prev req status:" + poiSearchRequest.status + " code:" + poiSearchRequest.statusCode());
    mobilizerRequest.abort();
    $('#webSearchSpinner').hide();
  }

  request.body.search_term = $('#webSearchKeywords').val().trim();
  request.body.cmpyid = $('#webSearchCmpyId').val();
  request.body.id =  $('#webSearchDocId').val();
  request.body.tripId =  $('#webSearchTripId').val();
  request.body.date = $('#webSearchDate').val();


  $('#mobilizedDomains input:checked').each(function() {
    request.body.addDomain($(this).attr('id'));
  });

  //console.log("mobilizerSearch term:" + request.body.search_term);

  $('#webSearchSpinner').show();
  mobilizerRequest = ExecuteCommand(request, "/mobilizer/execute.json", function(data){
    mobilizerRenderWebSearch(data.body.pages);
    $('#webSearchSpinner').hide();
  });
};

var mobilizerClearWebSearch = function() {
  var mobilizerResultsArea = $('#mobilizerSearchResults');
  mobilizerResultsArea.children().remove();
  $('#travel42Alert').hide();
};

var mobilizerRenderWebSearch = function(pages) {
  var mobilizerResultsArea = $('#mobilizerSearchResults');
  mobilizerClearWebSearch();

  var table = $('<table>',{
    "class":"table table-striped"
  });

  var buildWebSearchResult = function(page) {
    var line = $('<tr>', {
      'id':page.resultId,
      'class':'hide searchResult'
    });

    var btnArea = $('<td>',{
      "width":"20%",
      'style':'vertical-align:middle;'
    });

    var titleArea = $('<td>',{
      "width":"80%"
    });

    var previewBtn = $('<button>',{
      'type':'button',
      'class':'btn btn-info input-block-level',
    }).append('<i class="fa fa-newspaper-o"></i>&nbsp;').append('View Content');
    previewBtn.click(function() {
      $('#webSearchSpinner').show();
      mobilizerProcessUrl(page.resultId)
    });
    btnArea.append(previewBtn);

    var urlTitleAndLink = $('<a>', {
      'href':page.url,
      'target':'_blank'
    }).text(page.title).append('&nbsp;<i class="fa fa-external-link-square"></i>');
    titleArea.append(urlTitleAndLink);
    titleArea.append($('<br>'));
    var descrArea = $('<span>').text(page.description);
    titleArea.append(descrArea);

    line.append(titleArea).append(btnArea);
    return line;
  };

  if(pages && pages.length > 0) {

    $.each(pages, function(k,v){
      var resEntry = buildWebSearchResult(v);
      table.append(resEntry);
    });

    mobilizerResultsArea.append(table);
    mobilizerResultsArea.append('<div id="mobilizerPages">');

    //Pagination
    var options = {
      currentPage: 1,
      totalPages: Math.ceil(pages.length / 10),
      useBootstrapTooltip: true,
      onPageClicked: function (e, originalEvent, type, page) {
        mobilizerDisplaySearchResultPage(page);
      },
      onPageChanged: function (e, oldPage, newPage) {
        mobilizerDisplaySearchResultPage(newPage);
      }
    };
    $('#mobilizerPages').bootstrapPaginator(options);
    mobilizerDisplaySearchResultPage(1);
  } else {
    mobilizerResultsArea.append('<div class="alert alert-info">No Results!</div>');
  }
};

var mobilizerDisplaySearchResultPage = function(pageNum) {
  var results = $('#mobilizerSearchResults .searchResult');
  results.each(function(k,v){
    if (k >= (pageNum - 1) * 10 && k < (pageNum * 10)) {
      $(this).show();
    } else {
      $(this).hide();
    }

  });
};

/**
 * Process result id
 * @param resultId
 */
var mobilizerProcessUrl = function(resultId){
  //console.log("Process URL ID:"+  resultId);
  var request = new ApiMessage();
  request.header = new ApiRequestHeader();
  request.body = new WebSearch(EnumOperations.UPLOAD);
  request['@type'] = 'WEBSEARCH';

  request.body.search_term = $('#webSearchKeywords').val().trim();
  request.body.cmpyid = $('#webSearchCmpyId').val();
  request.body.id =  $('#webSearchDocId').val();
  request.body.tripId =  $('#webSearchTripId').val();
  request.body.date = $('#webSearchDate').val();


  var title = $('#' + resultId + ' a').text().trim();
  var url = $('#' + resultId + ' a').attr('href');
  var desc = $('#' + resultId + ' div').last().text();
  request.body.addPage(resultId, title, url, null, desc);

  ExecuteCommand(request, "/mobilizer/execute.json", function(data){
    $('#mobilizedResults').children().remove();
    $('#mobilizedResults').text('');
    if (data.body.pages && data.body.pages.length > 0) {

      $('#pagesAccordion').children().remove();
      $("input[id*='mobilizedLibraryId']").remove();
      for(var idx = 0; idx < data.body.pages.length; idx++) {
        addAccordionPage(data.body.pages[idx], (data.body.pages.length == 1 && idx == 0)?true:false);
        var idInputField = $('<input type="hidden" id="mobilizedLibraryId['+idx+']">');
        idInputField.val(data.body.pages[idx].resultId);
        $('#mobilizerForm').append(idInputField);
      }

      $('#mobilizedTripId').val(data.body.tripId);
      $('#mobilizedDate').val(data.body.date);
      $('#mobilizedDocId').val(data.body.id);
      $('#mobilizedCmpyId').val(data.body.cmpyid);
    } else {
      $('#mobilizedResults').append('<div class="alert alert-error">Failed to parse page</div>');
    }
    $('#webSearchSpinner').hide();
    $('#mobilizerResultModal').modal();
  }, function(data){
    $('#webSearchSpinner').hide();
    $('#mobilizerModal').modal("hide");
    $('#mobilizerResultModal').modal("hide");
  });
};

/**
 * Process result id
 * @param resultId
 */
var mobilizerAddPage = function(){
  var request = new ApiMessage();
  request.header = new ApiRequestHeader();
  request.body = new WebSearch(EnumOperations.ADD);
  request['@type'] = 'WEBSEARCH';

  request.body.search_term = $('#webSearchKeywords').val();
  if (request.body.search_term && request.body.search_term.length > 0) {
    request.body.search_term = request.body.search_term.trim();
  }
  request.body.cmpyid = $('#mobilizedCmpyId').val();
  request.body.id =  $('#mobilizedDocId').val();
  request.body.tripId =  $('#mobilizedTripId').val();
  request.body.date =  $('#mobilizedDate').val();


  $("input[id*='mobilizedLibraryId']").each(function() {
    var id    = $(this).val();
    var title = $("a[href='#pageContent"+id+"']").text();
    request.body.addPage(id, title, null, null, null);
  });

  ExecuteCommand(request, "/mobilizer/execute.json", function(data){
    $('#mobilizerModal').modal("hide");
    $('#mobilizerResultModal').modal("hide");
    var docId = data.body.id;
    var tripId = data.body.tripId;
    if (docId != null && docId.length > 0) {
      //Simply reloading page
      var reqParam = "inDestId=" + data.body.id;
      reloadPage(reqParam, {'ATTACHMENTS': 'CUSTOM_DOC'}); //TODO: Very ugly hack, create better solution.
    } else if (tripId != null && tripId.length > 0) {
      getPage("/trips/bookings/"+ data.body.tripId +"/ITINERARY")
    }
  });
};


/**
 * API Based login request login happens in the background
 */
var mobilizerTravel42Login = function() {
  var request     = new ApiMessage();
  request.header  = new ApiRequestHeader();
  request.body    = new AuthBody(EnumOperations.AUTHENTICATE);
  request['@type'] = 'T42LOGIN';

  request.body.username = $('#travel42UserId').val();
  request.body.password = $('#travel42Password').val();

  $('#webSearchSpinner').show();
  ExecuteCommand(request, "/mobilizer/t42login.json", function(data){
    setTimeout(function() {
      mobilizerTrave42LoginCheck(data.body.jobId);
    }, 5000);
  });
};


var mobilizerTrave42LoginCheck = function(jobId) {
  $('#webSearchSpinner').show();

  var request = new ApiMessage();
  request.header = new ApiRequestHeader();
  request.body = new AuthBody(EnumOperations.STATE);
  request['@type'] = 'T42LOGIN';

  request.body.jobId = jobId;

  ExecuteCommand(request, "/mobilizer/t42login.json", function(data){
    switch (data.body.state) {
      case 'SUCCESS':
        //clean up the select box
        $('#travel42ReportList option[value!="_default"]').remove();
        $.each(data.body.links, function(url,name){
          $("#travel42ReportList").append("<option value='" + url + "'>" + name + "</option>");
        });

        //hide the login and show the report list
        $('#Travel42Report').show();
        $('#Travel42Login').hide();
        $('#webSearchSpinner').hide();
        break;
      case 'NO_DATA':
        alert("Login was successful, but we were unable to find any trip plans on Travel 42 for your account.");
        break;
      case 'INVALID_LOGIN':
        alert("Login credentials that you've provided were not accepted by Travel 42.");
        $('#webSearchSpinner').hide();
        break;
      case 'ERROR':
        alert('Failed to Login to Travel 42.');
        $('#webSearchSpinner').hide();
        break;
      default:
        setTimeout(function() {
          mobilizerTrave42LoginCheck(jobId);
        }, 5000);
    }
  }, function(data){
    $('#webSearchSpinner').hide();
    $('#mobilizerModal').modal("hide");
    $('#mobilizerResultModal').modal("hide");
  });
};