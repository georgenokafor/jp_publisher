package com.mapped.publisher.view;


import models.publisher.EmailLog;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by george on 2016-11-21.
 */
public class EmailLogView extends BaseView {

  public String toEmail;
  public Long pk;
  public String subject;
  public String type;
  public String status;
  public String tripId;
  public int clickCounter;
  public int openCounter;
  public String timestamp;
  public String sentTs;
  public boolean archived;
}
