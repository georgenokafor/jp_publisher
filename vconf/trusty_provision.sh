#Fail on any error
set -e

#Define Vagrant Name
VAGRANT_NAME="trusty"
#PGSQL DUMPs
PGSQL_WEB_DUMP="https://www.dropbox.com/s/w0txlp7ubu0jdrg/umapped_web.dump.7z?dl=0"
PGSQL_PUB_DUMP="https://www.dropbox.com/s/ov10ct164yj49tj/umapped_pub.dump.7z?dl=0"
#Define Java Version
JAVA_VERSION=8
#Define PostgreSQL Version
PGSQL_VERSION=9.5
#Define Play Version
ACTIVATOR_VERSION=1.3.6
ACTIVATOR_DIST="-minimal"
#Define log file
LOG_FILE="provision.log"
#Define Current Ubuntu Version
UBUNTU_VERSION=`lsb_release -s -c`
#Define PGSQL Repository to use
PGSQL_REPOSITORY="deb http://apt.postgresql.org/pub/repos/apt/ $UBUNTU_VERSION-pgdg main"

echo "Setting up UTC Timezone"
echo "Etc/UTC" > /etc/timezone
dpkg-reconfigure --frontend noninteractive tzdata

#Cleaning log file
cat /dev/null > $LOG_FILE
echo "Provisioning $VAGRANT_NAME" > $LOG_FILE

# Configure apt for java
# http://www.webupd8.org/2012/01/install-oracle-java-jdk-7-in-ubuntu-via.html
echo "Configuring Repositories"
apt-get update &>> $LOG_FILE
apt-get install -y software-properties-common &>> $LOG_FILE
add-apt-repository ppa:webupd8team/java &>> $LOG_FILE
add-apt-repository ppa:git-core/ppa &>> $LOG_FILE
echo oracle-java$JAVA_VERSION-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
# Update apt sources
apt-get update &>> $LOG_FILE

# Install stuff available through apt-get
echo "Installing Ubuntu system packages"
apt-get install -y unzip p7zip-full wget git vim &>> $LOG_FILE

# Installing Memcached, Redis and PGSQL
echo "Installing Memcached and PostgreSQL servers"
echo $PGSQL_REPOSITORY >> /etc/apt/sources.list.d/pgsql.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - &>> $LOG_FILE
apt-get update &>> $LOG_FILE
apt-get install -y redis-server redis-tools memcached \
	postgresql-$PGSQL_VERSION \
	postgresql-client-$PGSQL_VERSION \
	postgresql-contrib-$PGSQL_VERSION \
	postgresql-$PGSQL_VERSION-postgis-scripts \
	postgresql-doc-$PGSQL_VERSION &>> $LOG_FILE

#Allowing connections from any IP to REDIS
sed -i 's:^bind:#bind:g' /etc/redis/redis.conf
service redis-server restart

#Allowing connections from any IP to memcached
sed -i 's:^-l:#-l:g' /etc/memcached.conf
service memcached restart


# Install Typesafe Play Framework Activator manually
if [ ! -f /vagrant/vconf/.skip-java ]
then
  echo "Installing Java"
  apt-get install -y oracle-java$JAVA_VERSION-installer oracle-java$JAVA_VERSION-set-default &>> $LOG_FILE

  echo "Installing Play Framework"
  cd /opt
  wget --quiet http://downloads.typesafe.com/typesafe-activator/$ACTIVATOR_VERSION/typesafe-activator-${ACTIVATOR_VERSION}${ACTIVATOR_DIST}.zip
  unzip -qq typesafe-activator-${ACTIVATOR_VERSION}${ACTIVATOR_DIST}.zip
  chmod +x /opt/activator-${ACTIVATOR_VERSION}${ACTIVATOR_DIST}/activator
  chmod -R a+rw /opt/activator-${ACTIVATOR_VERSION}${ACTIVATOR_DIST}/*
  echo 'PATH=${PATH}:/opt/activator-'${ACTIVATOR_VERSION}${ACTIVATOR_DIST} >> /etc/profile
fi

#Linking Environment file
ln -sf /vagrant/vconf/${VAGRANT_NAME}_env.sh /etc/profile.d/umapped.sh

############################################################################
#Working with PostgreSQL
############################################################################

cd /vagrant/vconf/

#Changing connection permissions to allow localhost without password
sed -i".bak" "s:md5$:trust:g" /etc/postgresql/${PGSQL_VERSION}/main/pg_hba.conf
echo "host  all all all trust" >> /etc/postgresql/${PGSQL_VERSION}/main/pg_hba.conf
echo "listen_addresses = '*'" >> /etc/postgresql/${PGSQL_VERSION}/main/postgresql.conf
service postgresql stop &>> $LOG_FILE
service postgresql start &>> $LOG_FILE

#Creating a new PosgreSQL user - vagrant
sudo -u postgres createuser -U postgres -d -e -l -r -s vagrant &>> $LOG_FILE

if [ -f /vagrant/vconf/db/pub.dump -a -f /vagrant/vconf/db/mob.dump ]
then
  echo "Found local PostgreSQL database backup. Using it to restore database"
  DB_USER="vagrant"
  DB_PASS=""
  DB_HOST="localhost"
  DB_PORT="5432"
  DB_NAME_PUB="pub_db"
  DB_NAME_MOB="mob_db"
  
  psql "postgres://$DB_USER:$DB_PASS@$DB_HOST:$DB_PORT/postgres" \
      -c "CREATE DATABASE $DB_NAME_PUB WITH OWNER='$DB_USER' ENCODING='UTF8' "\
         "LC_COLLATE='en_US.UTF-8' LC_CTYPE='en_US.UTF-8' CONNECTION LIMIT = -1;"

  psql "postgres://$DB_USER:$DB_PASS@$DB_HOST:$DB_PORT/postgres" \
    -c "CREATE DATABASE $DB_NAME_MOB WITH OWNER='$DB_USER' ENCODING='UTF8' "\
        "LC_COLLATE='en_US.UTF-8' LC_CTYPE='en_US.UTF-8' CONNECTION LIMIT = -1;"
    
  psql "postgres://$DB_USER:$DB_PASS@$DB_HOST:$DB_PORT/$DB_NAME_PUB" \
    -c "CREATE EXTENSION fuzzystrmatch WITH SCHEMA public;"

  psql "postgres://$DB_USER:$DB_PASS@$DB_HOST:$DB_PORT/$DB_NAME_PUB" \
    -c "CREATE EXTENSION hstore WITH SCHEMA public;"

  psql "postgres://$DB_USER:$DB_PASS@$DB_HOST:$DB_PORT/$DB_NAME_PUB" \
    -c "CREATE EXTENSION btree_gin WITH SCHEMA public;"

  psql "postgres://$DB_USER:$DB_PASS@$DB_HOST:$DB_PORT/$DB_NAME_PUB" \
    -c "CREATE EXTENSION pg_trgm WITH SCHEMA public;"

  pg_restore -O --schema=public \
      -d "postgres://$DB_USER:$DB_PASS@$DB_HOST:$DB_PORT/$DB_NAME_PUB" /vagrant/vconf/db/pub.dump
  pg_restore -O --schema=public \
      -d "postgres://$DB_USER:$DB_PASS@$DB_HOST:$DB_PORT/$DB_NAME_MOB" /vagrant/vconf/db/mob.dump
else 
  #Downloading and loading PostgreSQL dumps if no local exist
  echo "Downloading and restoring PostgreSQL databases (DEPRECATED METHOD)"
  wget -qq $PGSQL_WEB_DUMP -O umapped_web.dump.7z 
  wget -qq $PGSQL_PUB_DUMP -O umapped_pub.dump.7z
  7z e umapped_web.dump.7z 
  7z e umapped_pub.dump.7z
  rm umapped_web.dump.7z
  rm umapped_pub.dump.7z

  echo "Restoring database dumps"
  for dump in *.dump
  do
    echo "Restoring pgsql dump file: $dump"
    pg_restore -U vagrant --create --clean --no-acl --no-owner -h localhost -d template1 $dump &>> $LOG_FILE; true
  done
fi

#Ensuring end users will not receive any notifications
PGSQL_DUMP_FIXER=/tmp/um_pgsql_fixer.sql
cat > $PGSQL_DUMP_FIXER << EOL
UPDATE trip_participants SET email = 'user.notifications.dev@umapped.com';

UPDATE account SET email = concat(uid::varchar, '+user.notifications.dev@umapped.com') 
WHERE  email NOT LIKE '%@umapped.com';

UPDATE user_profile SET email = concat(userid, '+user.notifications.dev@umapped.com') 
WHERE  email NOT LIKE '%@umapped.com';
EOL
psql -U vagrant -h localhost -d $DB_NAME_PUB -f $PGSQL_DUMP_FIXER
rm $PGSQL_DUMP_FIXER


#Load all deltas that are found in the db subfolder of vconf
echo "Applying database deltas"
for sql_file in /vagrant/vconf/db/*.sql
do
  echo "Applying: $sql_file" 
  psql -U vagrant -h localhost -d $DB_NAME_PUB -f $sql_file
done

exit 0
