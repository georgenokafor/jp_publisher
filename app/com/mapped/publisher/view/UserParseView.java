package com.mapped.publisher.view;

import com.mapped.publisher.view.billing.BillingInfoView;
import com.mapped.publisher.view.billing.CreditCardInfoView;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ryan on 27/04/17.
 */
public class UserParseView
    extends BaseView
    implements Serializable{

    public String userId;
    public String cmpyId;
    public String cmpyName;

    public String userEmail;

    public boolean pageMode;
    public boolean isExistingUser;
}
