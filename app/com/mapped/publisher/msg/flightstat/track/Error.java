package com.mapped.publisher.msg.flightstat.track;

/**
 * Created by twong on 2015-01-26.
 */
public class Error {
  private String httpStatusCode;
  private String errorCode;
  private String errorId;
  private String errorMessage;

  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("Http Code: " + httpStatusCode);sb.append("\n");
    sb.append("Error Code: " + errorCode);sb.append("\n");
    sb.append("Error Msg: " + errorMessage);sb.append("\n");
    sb.append("Error Id: " + errorId);

    return sb.toString();
  }

  public String getHttpStatusCode() {
    return httpStatusCode;
  }

  public void setHttpStatusCode(String httpStatusCode) {
    this.httpStatusCode = httpStatusCode;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getErrorId() {
    return errorId;
  }

  public void setErrorId(String errorId) {
    this.errorId = errorId;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
}
