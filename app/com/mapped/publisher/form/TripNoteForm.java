package com.mapped.publisher.form;

import models.publisher.TripNote;

import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by twong on 15-04-25.
 */
public class TripNoteForm {

  /**************************************************************************
   * For General Notes
   **************************************************************************/
  private String inTripId;
  private Long inTripNoteId;
  private String inTripNoteName;
  private String inTripNoteIntro;
  private String inTripNoteDesc;
  private String inTripNoteLandmark;
  private String inTripNoteAddr;
  private String inTripNoteCity;
  private String inTripNoteState;
  private String inTripNoteCountry;
  private String inTripNoteLocLat;
  private String inTripNoteLocLong;
  private String inTripNoteTag;
  private String inTripNoteDate;
  private String inTripNoteTime;
  private String inTripNoteZip;
  private String inTripDetailsId;

  private int inTripNoteRecommendationType;

  public TripNote.NoteType inNoteType;

  /**************************************************************************
   * For Insurance Notes
   **************************************************************************/
  public String inProvider;
  public String inPolicyNum;
  public String inPolicyType;
  public String inEmergencyNum;
  public String inImportantInfo;
  public String inPolicyDesc;

  public List<Passenger> passengers;

  public static class Passenger {
    public String firstName;
    public String lastName;
    public String startDate;
    public String endDate;

    public boolean isEmpty() {
      return StringUtils.isEmpty(firstName) && StringUtils.isEmpty(lastName)
          && StringUtils.isEmpty(startDate) && StringUtils.isEmpty(endDate);
    }
  }


  /**************************************************************************
   * For Other Notes
   **************************************************************************/

  public String getInTripId() {
    return inTripId;
  }

  public void setInTripId(String inTripId) {
    this.inTripId = inTripId;
  }

  public Long getInTripNoteId() {
    return inTripNoteId;
  }

  public void setInTripNoteId(Long inTripNoteId) {
    this.inTripNoteId = inTripNoteId;
  }

  public String getInTripNoteName() {
    return inTripNoteName;
  }

  public void setInTripNoteName(String inTripNoteName) {
    this.inTripNoteName = inTripNoteName;
  }

  public String getInTripNoteIntro() {
    return inTripNoteIntro;
  }

  public void setInTripNoteIntro(String inTripNoteIntro) {
    this.inTripNoteIntro = inTripNoteIntro;
  }

  public String getInTripNoteDesc() {
    return inTripNoteDesc;
  }

  public void setInTripNoteDesc(String inTripNoteDesc) {
    this.inTripNoteDesc = inTripNoteDesc;
  }

  public String getInTripNoteLandmark() {
    return inTripNoteLandmark;
  }

  public void setInTripNoteLandmark(String inTripNoteLandmark) {
    this.inTripNoteLandmark = inTripNoteLandmark;
  }

  public String getInTripNoteAddr() {
    return inTripNoteAddr;
  }

  public void setInTripNoteAddr(String inTripNoteAddr) {
    this.inTripNoteAddr = inTripNoteAddr;
  }

  public String getInTripNoteCity() {
    return inTripNoteCity;
  }

  public void setInTripNoteCity(String inTripNoteCity) {
    this.inTripNoteCity = inTripNoteCity;
  }

  public String getInTripNoteState() {
    return inTripNoteState;
  }

  public void setInTripNoteState(String inTripNoteState) {
    this.inTripNoteState = inTripNoteState;
  }

  public String getInTripNoteCountry() {
    return inTripNoteCountry;
  }

  public void setInTripNoteCountry(String inTripNoteCountry) {
    this.inTripNoteCountry = inTripNoteCountry;
  }

  public String getInTripNoteLocLat() {
    return inTripNoteLocLat;
  }

  public void setInTripNoteLocLat(String inTripNoteLocLat) {
    this.inTripNoteLocLat = inTripNoteLocLat;
  }

  public String getInTripNoteLocLong() {
    return inTripNoteLocLong;
  }

  public void setInTripNoteLocLong(String inTripNoteLocLong) {
    this.inTripNoteLocLong = inTripNoteLocLong;
  }

  public String getInTripNoteTag() {
    return inTripNoteTag;
  }

  public void setInTripNoteTag(String inTripNoteTag) {
    this.inTripNoteTag = inTripNoteTag;
  }

  public String getInTripNoteDate() {
    return inTripNoteDate;
  }

  public void setInTripNoteDate(String inTripNoteDate) {
    this.inTripNoteDate = inTripNoteDate;
  }

  public String getInTripNoteTime() {
    return inTripNoteTime;
  }

  public void setInTripNoteTime(String inTripNoteTime) {
    this.inTripNoteTime = inTripNoteTime;
  }

  public String getInTripNoteZip() {
    return inTripNoteZip;
  }

  public void setInTripNoteZip(String inTripNoteZip) {
    this.inTripNoteZip = inTripNoteZip;
  }

  public String getInTripDetailsId() {
    return inTripDetailsId;
  }

  public void setInTripDetailsId(String inTripDetailsId) {
    this.inTripDetailsId = inTripDetailsId;
  }

  public int getInTripNoteRecommendationType() {
    return inTripNoteRecommendationType;
  }

  public void setInTripNoteRecommendationType(int inTripNoteRecommendationType) {
    this.inTripNoteRecommendationType = inTripNoteRecommendationType;
  }
}
