package com.umapped.external.afar;

import java.util.List;

/**
 * Created by george on 2017-11-01.
 */
public class CategoryList {
  private List<String> categories;

  public List<String> getCategories() {
    return categories;
  }

  public void setCategories(List<String> categories) {
    this.categories = categories;
  }
}
