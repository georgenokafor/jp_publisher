package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import models.publisher.ApiCmpyToken;
import models.publisher.BkApiSrc;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by george on 2015-11-24.
 */
public class CmpyTokenController
    extends Controller {

  @Inject
  FormFactory formFactory;

  public class CmpyTokenSrcMap {


    private long expiryTs;

    private String srcCmpyId;
    private String uCmpyId;
    private String token;

    private int version;

       /* public static List<CmpyTokenSrcMap> findBySrcCmpyId (String srcCmpyId) {
            return find.where().eq("src_cmpy_id", srcCmpyId).findList();
        }*/

    public long getExpiryTs() {
      return expiryTs;
    }

    public void setExpiryTs(long expiryTs) {
      this.expiryTs = expiryTs;
    }

    public String getSrcCmpyId() {
      return srcCmpyId;
    }

    public void setSrcCmpyId(String srcCmpyId) {
      this.srcCmpyId = srcCmpyId;
    }

    public String getuCmpyId() {
      return uCmpyId;
    }

    public void setuCmpyId(String uCmpyId) {
      this.uCmpyId = uCmpyId;
    }

    public String getToken() {
      return token;
    }

    public void setToken(String token) {
      this.token = token;
    }

    public int getVersion() {
      return version;
    }

    public void setVersion(int version) {
      this.version = version;
    }
  }

  @BodyParser.Of(BodyParser.Json.class)
  public Result getTokens(String srcId) {

    String authToken = request().getHeader("Authorization");
    if (authToken != null && authToken.contains("Bearer")) {
      authToken = authToken.replace("Bearer", "").trim();
    }
    if (authToken != null && authToken.contains("iL6VjE-IBMLIy-ztmHU5-oHEG1p")) {
      JsonNode jn = request().body().asJson();
      //TokenForm token = Json.fromJson(jn, TokenForm.class);
      BkApiSrc bkApiSrc = null;
      int      intSrcId = Integer.parseInt(srcId);
      boolean  valid    = false;

      if (BkApiSrc.find.byId(intSrcId) != null) {
        bkApiSrc = BkApiSrc.find.byId(intSrcId);
        if (bkApiSrc.getState() == 0) {
          valid = true;
        }
      }
      else {
        return ok("ID does not exist in 'bk_api_src' table!!");
      }

      List<ApiCmpyToken> act = null;

      long                  currentTime = System.currentTimeMillis();
      List<CmpyTokenSrcMap> tokenList   = new ArrayList<>();


      if (bkApiSrc != null && valid) {

        act = ApiCmpyToken.findByBkApiSrcId(intSrcId);

        for (ApiCmpyToken a : act) {

          if (a.getExpiryTs() - currentTime >= 0) {

            CmpyTokenSrcMap cmpyTokenSrc = new CmpyTokenController().new CmpyTokenSrcMap();
            cmpyTokenSrc.setSrcCmpyId(a.getSrcCmpyId());
            cmpyTokenSrc.setToken(a.getToken());
            cmpyTokenSrc.setuCmpyId(a.getuCmpyId());
            cmpyTokenSrc.setVersion(a.getVersion());
            cmpyTokenSrc.setExpiryTs(a.getExpiryTs());

            tokenList.add(cmpyTokenSrc);
          }

        }
      }


      return ok(Json.toJson(tokenList));
    }
    else {
      return ok();
    }


  }

}
