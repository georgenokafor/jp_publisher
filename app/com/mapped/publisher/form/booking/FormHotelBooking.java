package com.mapped.publisher.form.booking;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by surge on 2016-01-04.
 */
public class FormHotelBooking
    extends FormBookingBase {
  public String inHotelName;
  public Long   inHotelProviderId;
  public String inHotelNote;
  public String inHotelBooking;
  public String inHotelResStatus;
  public String inHotelPayStatus;
  public String inHotelCheckInDate;
  public String inHotelCheckOutDate;
  public String inHotelCheckInTime;
  public String inHotelCheckOutTime;


  public String inHotelImportant;
  public String inHotelCancellation;
  public String inHotelStartTimezone;
  public String inHotelFinishTimezone;

  public List<Passenger> passengers;


  public static class Passenger {
    public String firstName;
    public String lastName;
    public String roomType;
    public String amenities;
    public String status;
    public String membershipID;
    public String bedding;

    public boolean isEmpty() {
      return StringUtils.isEmpty(firstName) && StringUtils.isEmpty(lastName) &&
              StringUtils.isEmpty(roomType) && StringUtils.isEmpty(amenities) && StringUtils.isEmpty(status)
              && StringUtils.isEmpty(membershipID) && StringUtils.isEmpty(bedding);
    }
  }
}
