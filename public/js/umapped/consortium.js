/**
 * Created by george on 2017-09-07.
 */
var allConsortiaUI = {
  init: function(jsonUrl) {
    this.tblCompanies = $('#allConsortiaTable');
    this.tblJsonUrl   = jsonUrl;

    var allConsortiaListUI = this;
    this.tblCompanies
        .DataTable({
      "processing": true,
      "serverSide": true,
      "sDom": 'ltipr',
      "oLanguage": {
               "sInfoFiltered": "",
               "sLengthMenu": "Show _MENU_ Consortia",
               "sInfo": "",
               "sInfoEmpty": ""
             },
      "columnDefs": [
        { "data": "companyName",   "targets" : 0, "searchable" : false, "orderable": true },
        { "data": "consortiumName",   "targets" : 1, "searchable" : false, "orderable": true },
        { "data": "delete",         "targets" : 2, "searchable" : false, "orderable": false,
          "render" : function(data, type, full, meta) {
            if (type === 'display' ) {
              var res = allConsortiaListUI.renderCmpyDeleteButton(data);
              return res;
            } else {
              return data;
            }
          }
        }
      ],
      "language": {
        "emptyTable":     "No Companies Found",
        "lengthMenu": "Show _MENU_ trips",
        "info": ""
      },
      "ajax": {
        url: allConsortiaListUI.tblJsonUrl,
        type: 'POST',
        contentType: "application/json",
        data: function (d) {
          d.consortium = $('#filterConsortium').val();
          d.keyword = $('#searchTerm').val();

          console.log(JSON.stringify(d));
          return  JSON.stringify(d);
        },
        error: function (xhr, textStatus, errorThrown) {
          window.location.replace("/login");
        }
      }
    });

  },
  renderCmpyDeleteButton: function(id) {
    btn = '<a role="button" class="btn btn-danger input-block-level" style="width:60%; float: right;" onclick="doPost(\'' + id + '\');return false;"> Delete </a>' ;
    return btn;
  }

};

var initAllConsortia = function(jsonUrl) {
  var allConsortiaTable = Object.create(allConsortiaUI);
  allConsortiaTable.init(jsonUrl);
};


var editConsortium = function() {
    $("#inConsortiumId").val($('#consortiumSelect option:selected').val());
    $("#inConsortiumName").val($('#consortiumSelect option:selected').text());
    $('#deleteConsortiumButton').show();
    $('#consortiumList').hide();
    $('#editConsortiumModal h3').text("Edit Consortium");
    $('#editConsortiumModal').modal('show');
};



var initConsortiaAdmin = function() {
  $('#newConsortiumButton').click(function(){
    $("#inConsortiumId").val(null);
    $("#inConsortiumName").val("");
    $("#deleteConsortiumButton").hide();
    $('#editConsortiumModal h3').text("Add Consortium");
    $('#editConsortiumModal').modal('show');
  });


  /** COMPANY SEARCH MODAL **/
  /* Initialize company names auto completion box */
  $('#companyNameSearchTerm').autocomplete({
    source: function( request, response ) {

      companySearch(request.term, function(data){
        var result = [];
        $.each(data.body.companies, function(idx, val){
          //console.log("Found company:" + val.name);
          var c = {value:val.name, id:val.id};
          result.push(c);
        });
        response(result);
      });
    },
    minLength: 3 ,
    select: function (event, ui) {
      $("#companyNameSearchTerm").val(ui.item.value);
      $("#companyNameSearchId").val(ui.item.id);
      $("#addFoundCompanyButton").show();
    }
  });

  $('#addFoundCompanyButton').click(function(){
    if ($('#modalConsortiumSelect').val() > 0) {
      if ($("#companyNameSearchId").val() > 0) {
        consortiumAddCompany($('#modalConsortiumSelect').val(),
                             $("#companyNameSearchId").val(),
                             $("#companyNameSearchTerm").val(),
                             function(data) {
                               $('#consortiumAddCompanyModal').modal('hide');
                               $('#consortiumList').show();
                               $('#editConsortiumModal').modal('hide');
                               $("#companyNameSearchTerm").val("");
                               $("#companyNameSearchId").val(null);
                               $("#addFoundCompanyButton").hide();
                               getPage('/consortia');
                             });
      } else {
        alert("Missing company information. Please contact web developer.");
      }
    } else {
      alert("Company has not been created yet");
    }
  });

  $('#deleteConsortiumButton').click(function(){
    consortiumDelete($("#inConsortiumId").val(), $("#inConsortiumName").val());
    return false;
  });
};

var consortiumDelete = function(consortium_id, consortium_name) {
  var request = new ApiMessage();
  request.header = new ApiRequestHeader();
  request.body = new ConsortiumBody('DELETE');
  request['@type']  = 'CONSORTIUM';
  request.body.addConsortium(consortium_id, consortium_name);

  ExecuteCommand(request, "/consortia/execute.json", function(data) {
        $('#editConsortiumModal').modal('hide');
        getPage('/consortia');
      },
      function(data) {
        alert("Problem removing company from company (ERROR:" + data.header.resp_code + ")");
      });
};

/**
 * Requests company data and populates 3-column table with returned data.
 * @param term - search term
 * @param cbSuccess - callback function accepting single parameter (data)
 */
var companySearch = function(term, cbSucccess) {
  var request = new ApiMessage();
  request.header = new ApiRequestHeader();
  request.body = new CompanyBody('LIST');
  request['@type']  = 'COMPANY';
  if (term != undefined && term != null) {
    request.body.search_term = term;
  }

  ExecuteCommand(request, "/companies/execute.json", cbSucccess);
};


/**
 * Take consortium information from the form package into UMapped API and send
 * to the server
 * @param formName - name of the form with new consortium information
 */
var consortiumEdit = function(formName) {
  var consortiumName = $("#inConsortiumName").val();
  var consortiumId = $("#inConsortiumId").val();
  var request = new ApiMessage();
  request.header = new ApiRequestHeader();
  if ($('#inConsortiumId').val() > 0) {
    request.body = new ConsortiumBody('UPDATE');
  } else {
    request.body = new ConsortiumBody('ADD');
  }


  request['@type']  = 'CONSORTIUM';
  request.body.addConsortium(consortiumId, consortiumName);

  ExecuteCommand(request, "/consortia/execute.json", function(data) {
    $('#editConsortiumModal').modal('hide');
    getPage('/consortia');
  },
  function(data) {
    alert("Problem creating new consortium (ERROR:" + data.header.resp_code + ")");
  });
};

var consortiumAddCompany = function(consortiumId, companyId, companyName, cbSuccess, cbFail) {
  var request = new ApiMessage();
  request.header = new ApiRequestHeader();
  request.body = new ConsortiumBody('ADD');
  request['@type']  = 'CONSORTIUM';
  request.body.addConsortiumCompany(consortiumId, companyId, companyName);

  ExecuteCommand(request, "/consortia/execute.json", cbSuccess,
      function(data) {
        alert("Problem adding new company to a consortium (ERROR:" + data.header.resp_code + ")");
      });
};

var consortiumDeleteCompany = function(consortiumCompany) {
  var request = new ApiMessage();
  request.header = new ApiRequestHeader();
  request.body = new ConsortiumBody('DELETE');
  request['@type']  = 'CONSORTIUM';
  request.body.companies.push(consortiumCompany);

  ExecuteCommand(request, "/consortia/execute.json", function(data) {
        getPage('/consortia');
      },
      function(data) {
        alert("Problem removing company from company (ERROR:" + data.header.resp_code + ")");
      });
};

