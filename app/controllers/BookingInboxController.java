package controllers;

import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.inbox.AddBookingsForm;
import com.mapped.publisher.form.inbox.SearchBookingsForm;
import com.mapped.publisher.parse.extractor.booking.email.ReservationsHolder;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.parse.schemaorg.*;
import com.mapped.publisher.persistence.TripBrandingMgr;
import com.mapped.publisher.persistence.VOModeller;
import com.mapped.publisher.persistence.bookingapi.BookingAPIMgr;
import com.mapped.publisher.persistence.bookingapi.BookingRS;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.umapped.helper.TripPublisherHelper;
import com.umapped.persistence.enums.ReservationType;
import models.publisher.*;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by twong on 15-08-18.
 */
public class BookingInboxController
    extends Controller {

  @Inject
  FormFactory formFactory;

  @Inject
  TripPublisherHelper tripPublisherHelper;

  @With({Authenticated.class, Authorized.class})
  public Result searchBooking(String tripId, String term) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Trip trip = Trip.find.byId(tripId);
    if (trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.READ)) {
      //lets find any bookings
      if (term == null) {
        term = "%";
      } else {
        term = "%" + term + "%";
      }

      List<BookingRS> bookings = BookingAPIMgr.findByDateAndUserAndTerm(trip.starttimestamp, trip.endtimestamp, sessionMgr.getUserId(), term);
      if (bookings != null && bookings.size() > 0) {

      }

    }

    return ok("Hello");
  }

  @With({Authenticated.class, Authorized.class})
  public Result addBookings(String tripId) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    Trip trip = Trip.find.byId(tripId);
    if (trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.APPEND)) {
      //get the bookings, then let

    }

    return ok("Hello");
  }

  @With({Authenticated.class, Authorized.class})
  public Result bookingInbox(String tripId, boolean fromTrip) {
    SessionMgr               sessionMgr            = SessionMgr.fromContext(ctx());
    Credentials              cred                  = sessionMgr.getCredentials();
    Form<SearchBookingsForm> pForm                 = formFactory.form(SearchBookingsForm.class);
    SearchBookingsForm       form                  = pForm.bindFromRequest().get();
    final long               localTimeOffsetMillis = sessionMgr.getTimezoneOffsetMins() * 60 * 1000;

    if (request().method().equals("GET")) {
      form.inNewBookingsOnly =true;
    }
    List<String> cmpies = new ArrayList<>();
    if(cred.hasCompany()) {
      cmpies.add(cred.getCmpyId());

      //add extra check to see if the cmpy the user belongs is linked to a parent cmpy
      Company c = cred.getCompany();
      if (c.getParentCmpyId() != null && c.getParentCmpyId().trim().length() > 0) {
        cmpies.add(c.getParentCmpyId());
      }
    }

    BookingInboxView v = executeSearch(form, sessionMgr.getUserId(), cmpies , localTimeOffsetMillis, cred);

    return ok(views.html.bookings.inbox.render(v, tripId, fromTrip));
  }

  @With({Authenticated.class, Authorized.class})
  public Result bookingInboxMore() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Credentials cred = sessionMgr.getCredentials();
    final long localTimeOffsetMillis = sessionMgr.getTimezoneOffsetMins() * 60 * 1000;

    Form<SearchBookingsForm> pForm = formFactory.form(SearchBookingsForm.class);
    SearchBookingsForm form = pForm.bindFromRequest().get();
    List<String> cmpies = new ArrayList<>();
    if(cred.hasCompany()) {
      cmpies.add(cred.getCmpyId());

      //add extra check to see if the cmpy the user belongs is linked to a parent cmpy
      Company c = cred.getCompany();
      if (c.getParentCmpyId() != null && c.getParentCmpyId().trim().length() > 0) {
        cmpies.add(c.getParentCmpyId());
      }
    }

    BookingInboxView v = executeSearch(form, sessionMgr.getUserId(), cmpies, localTimeOffsetMillis, cred);
    return ok(views.html.bookings.inboxMore.render(v));
  }

  @With({Authenticated.class, Authorized.class})
  public Result copyTrip(boolean fromTrip) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Form<AddBookingsForm> pForm      = formFactory.form(AddBookingsForm.class);
    AddBookingsForm       form       = pForm.bindFromRequest().get();
    List<Long>            pks        = AddBookingsForm.getBookingPks(form.inBookings);
    Trip                  trip       = Trip.findByPK(form.inTripId);
    BaseView              v          = new BaseView();
    List<ReservationPackage> reservationPackages = new ArrayList<>();

    UserProfile up = UserProfile.findByPK(sessionMgr.getUserId());
    List<BookingRS> results = BookingAPIMgr.findByPks(pks);

    if (trip == null) {
      v.message = "Please make sure you have selected a trip from the autocomlete drop down";

    } else if (results != null && results.size() > 0 && trip != null && SecurityMgr.getTripAccessLevel(trip, sessionMgr.getCredentials()).ge(SecurityMgr.AccessLevel.APPEND)) {
      HashMap<String, ArrayList<BookingRS>> resultsByPK = new HashMap<>();


      //check to see how many bookings we need to import and how many are needed for updates.
      VOModeller voModeller = new VOModeller(trip, up, false);
      TripVO tripVO = null;
      for (BookingRS result: results) {
        if (result.bookingType != ReservationType.PACKAGE.ordinal()) {
          if (result.getSrcPk() != null) {
            //we group all the bookings that have the same src PK
            ArrayList<BookingRS> bookings = resultsByPK.get(result.getSrcPk());
            if (bookings == null) {
              bookings =  new ArrayList<>();
            }
            bookings.add(result);
            resultsByPK.put(result.getSrcPk(), bookings);
          }
          ReservationsHolder rh = TripPublisherHelper.parseJson(result.getData(), BkApiSrc.getName(result.getSrcId()), result.pk);
          if (tripVO == null) {
            tripVO = new TripVO();
            tripVO.setSrc(BkApiSrc.getName(result.getSrcId()));
            tripVO.setSrcId(result.pk);
            tripVO.setImportSrc(BookingSrc.ImportSrc.API);
            tripVO.setImportSrcId(result.getSrcRecLocator());
          }

          if (rh.count()  >  0) {
            tripVO = rh.toTripVO(tripVO);
          }
        } else {
          ReservationsHolder rh = TripPublisherHelper.parseJson(result.getData(), BkApiSrc.getName(result.getSrcId()), result.pk);
          if (rh != null && rh.reservationPackage != null) {
            reservationPackages.add(rh.reservationPackage);
          }
        }
      }

      if (tripVO != null) {
        voModeller.buildTripVOModels(tripVO);
        //before we save, we try to link notes to bookings where the notes src pk is equal to the booking src pk
        TripPublisherHelper.linkNotesToBookings(voModeller, resultsByPK);
      }



      try {
        voModeller.saveAllModels();
        BookingAPIMgr.updateAddToTrip(up.userid, pks);
        //let see if we need to add the reservation package to the trip
        for (ReservationPackage rp: reservationPackages) {
          ApiBookingController.processTripNoteForReservationPackage(rp, trip.tripid, sessionMgr.getUserId());

          List<Destination> destinations = TripPublisherHelper.getDocuments(rp, sessionMgr.getCredentials().getCompany());
          //see if we need to automatically link a doc to this trip
          if (destinations != null) {
            TripPublisherHelper.addDocuments(destinations, trip, sessionMgr.getCredentials().getAccount());
          }
          if (trip.getFileImage() == null && rp.image != null && !rp.image.isEmpty()) {
            Trip t = Trip.findByPK(trip.getPk());
            TripPublisherHelper.addCover(t, rp, sessionMgr.getCredentials().getAccount());
            t.save();
          }
        }
        v.message = (results.size() -1) + " imported to " + trip.getName();
        v.message = (results.size() -1) + " bookings imported to " + trip.getName();
        v.linkId = "<a onclick=\"javascript:getPage('/trips/bookings/" + trip.tripid + "/ITINERARY')\" class='btn btn-info'> Edit Trip </a>";

      } catch (Exception e) {
        e.printStackTrace();
        v.message = "A error occurred during the import. Please try again.";
      }

    } else {
      v.message = "Unauthorized Access";

    }

    if (fromTrip) {
      return redirect(routes.BookingController.bookings(trip.tripid, new TabType.Bound(TabType.ITINERARY), null));
    }
    return ok(views.html.common.message.render(v));
  }

  @With({Authenticated.class, Authorized.class})
  public Result createTrip() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Form<AddBookingsForm> pForm = formFactory.form(AddBookingsForm.class);
    AddBookingsForm form = pForm.bindFromRequest().get();
    List<Long> pks = AddBookingsForm.getBookingPks(form.inBookings);
    UserProfile up = UserProfile.findByPK(sessionMgr.getUserId());
    BaseView v = new BaseView();

    List<BookingRS> results = BookingAPIMgr.findByPks(pks);
    if (results != null && results.size() > 0 && form.inTripName != null && form.inTripName.trim().length() > 0) {
      Company c = Company.find.byId(sessionMgr.getCredentials().getMainCmpy());

      if (c == null) {
        BaseView baseView = new BaseView();
        baseView.message = "Invalid agency - please resubmit";
        return ok(views.html.common.message.render(baseView));
      }

      if (c.maxtrips != APPConstants.CMPY_MAX_NUM_TRIPS) {
        //check to see if we exceed the limit
        int numOfTrips = Trip.numOfTrips(c.cmpyid);
        if (numOfTrips > c.getMaxtrips()) {
          BaseView baseView = new BaseView();
          baseView.message = "You have exceeded your trial limits - please contact us to extend your demo";
          return ok(views.html.common.message.render(baseView));
        }
      }
      if (!SecurityMgr.canCreateTrip(c, sessionMgr)) {
        BaseView baseView = new BaseView();
        baseView.message = "Unauthorized access";
        return ok(views.html.common.message.render(baseView));
      }

      List<Trip> trips = Trip.findPendingByCmpyNameType(form.inTripName,
                                                        c.getCmpyid(),
                                                        APPConstants.TOUR_TYPE);
      if (trips != null && trips.size() > 0) {
        BaseView baseView = new BaseView();
        baseView.message = "Duplicate Trip Name - A pending trip with this name already exists.";
        return ok(views.html.common.message.render(baseView));
      }
      //create the trip
      Trip t = TripPublisherHelper.createTripFromReservationPackage(c, form.inTripName, results, sessionMgr.getCredentials().getAccount());
      if (t == null) {
        v.message = "A error occured during the import. Please try again.";
      } else {
        v.message = (results.size() -1) + " bookings imported to " + t.getName();
        v.linkId = "<a onclick=\"javascript:getPage('/trips/bookings/" + t.tripid + "/ITINERARY')\" class='btn btn-info'> Edit Trip </a>";
      }
    } else if (form.inTripName == null || form.inTripName.trim().length() == 0) {
      v.message = "Please enter a trip name";
    } else {
      v.message = "Unauthorized Access";

    }

    return ok(views.html.common.message.render(v));
  }




  public static BookingInboxView testView() {
    BookingInboxView v = new BookingInboxView();
    v.newBookingsOnly = true;
    v.selectedSrc = "All";
    v.startPos = 1;
    v.term = "Keywords";

    v.sourceList = new ArrayList<>();
    v.sourceList.add("All");
    v.sourceList.add("Sabre");
    v.sourceList.add("Clientbase");
    v.sourceList.add("Email");

    v.reservations= new ArrayList<>();
    BookingReservationView reservationView = new BookingReservationView();
    reservationView.pk = "reservationPK1";
    reservationView.details = "6 Flights, 2 hotels and 1 car rental reservations";
    reservationView.src = "Sabre";
    reservationView.receivedTS = "Dec 10, 2015 10:23 AM";
    reservationView.travelDate = "Dec 10 - Dec 20 2015";
    reservationView.travelerName = "Smith/Joe";
    reservationView.recLocator = "QDRF12";
    v.reservations.add(reservationView);

    reservationView.bookings =  new ArrayList<>();
    BookingDetailView bookingDetailView = new BookingDetailView();
    bookingDetailView.pk = "1";
    bookingDetailView.bookingType = ReservationType.FLIGHT;
    bookingDetailView.details = "Air Canada (AC 101) from YYZ to LHR on Dec 01, 2015 10:15 AM";
    reservationView.bookings.add(bookingDetailView);

    bookingDetailView = new BookingDetailView();
    bookingDetailView.pk = "2";
    bookingDetailView.bookingType = ReservationType.CRUISE;
    bookingDetailView.details = "Cruise: Disney Pnincess (7 nights) from Miami on Dec 02, 2015 10:15 AM";
    reservationView.bookings.add(bookingDetailView);

    bookingDetailView = new BookingDetailView();
    bookingDetailView.pk = "3";
    bookingDetailView.bookingType = ReservationType.HOTEL;
    bookingDetailView.details = "Hotel: Mandarin Oriental Toronto (3 nights) check-in Dec 01, 2015 10:15 AM";
    reservationView.bookings.add(bookingDetailView);

    bookingDetailView = new BookingDetailView();
    bookingDetailView.pk = "4";
    bookingDetailView.bookingType = ReservationType.TOUR;
    bookingDetailView.details = "Tour: Toronto Walking Tour on Dec 01, 2015 10:15 AM";
    reservationView.bookings.add(bookingDetailView);

    bookingDetailView = new BookingDetailView();
    bookingDetailView.pk = "5";
    bookingDetailView.bookingType = ReservationType.CAR_RENTAL;
    bookingDetailView.details = "Car Rental: Hertz Intermediate Car from YYZ pickup on Dec 01, 2015 10:15 AM";
    reservationView.bookings.add(bookingDetailView);

    reservationView = new BookingReservationView();
    reservationView.pk = "reservationPK2";
    reservationView.details = "2 Flights, 2 hotels and 12 car rental reservations";
    reservationView.src = "Clientbase";
    reservationView.receivedTS = "Dec 10, 2015 10:23 AM";
    reservationView.travelDate = "Jan 10 - Jan 20 2015";
    reservationView.travelerName = "Wong/Tom";
    reservationView.recLocator = "32DSEF";
    v.reservations.add(reservationView);

    reservationView.bookings =  new ArrayList<>();
    bookingDetailView = new BookingDetailView();
    bookingDetailView.pk = "1";
    bookingDetailView.bookingType = ReservationType.RESTAURANT;
    bookingDetailView.details = "Air Canada (AC 101) from YYZ to LHR on Dec 01, 2015 10:15 AM";
    reservationView.bookings.add(bookingDetailView);

    bookingDetailView = new BookingDetailView();
    bookingDetailView.pk = "2";
    bookingDetailView.bookingType = ReservationType.RAIL;
    bookingDetailView.details = "Air Canada (AC 101) from YYZ to LHR on Dec 01, 2015 10:15 AM";
    reservationView.bookings.add(bookingDetailView);

    bookingDetailView = new BookingDetailView();
    bookingDetailView.pk = "3";
    bookingDetailView.bookingType = ReservationType.TAXI;
    bookingDetailView.details = "Air Canada (AC 101) from YYZ to LHR on Dec 01, 2015 10:15 AM";
    reservationView.bookings.add(bookingDetailView);

    bookingDetailView = new BookingDetailView();
    bookingDetailView.pk = "4";
    bookingDetailView.bookingType = ReservationType.CRUISE_STOP;
    bookingDetailView.details = "Air Canada (AC 101) from YYZ to LHR on Dec 01, 2015 10:15 AM";
    reservationView.bookings.add(bookingDetailView);

    bookingDetailView = new BookingDetailView();
    bookingDetailView.pk = "5";
    bookingDetailView.bookingType = ReservationType.SHORE_EXCURSION;
    bookingDetailView.details = "Air Canada (AC 101) from YYZ to LHR on Dec 01, 2015 10:15 AM";
    reservationView.bookings.add(bookingDetailView);
    return v;
  }

  public static BookingDetailView getReservationView(BookingRS rs) {

    BookingDetailView bookingDetailView = new BookingDetailView();
    ReservationsHolder rh = TripPublisherHelper.parseJson(rs.data, BkApiSrc.getName(rs.srcId), rs.pk);
    bookingDetailView.pk = String.valueOf(rs.pk);
    if (rh.count() > 0) {
      StringBuilder sb = new StringBuilder();
      if (rh.activities.size() == 1) {
        ActivityReservation r = rh.activities.get(0);
        bookingDetailView.bookingType = ReservationType.ACTIVITY;
        if (r.reservationFor != null && r.reservationFor.name != null) {
          sb.append("Activity: ");

          sb.append(r.reservationFor.name);
        } else {
          sb.append("Activity");
        }

        if (r.startTime != null) {
          sb.append(" on ");
          sb.append(Utils.formatTimestamp(r.startTime.getTimestamp().getTime(), "MMM dd, YYYY"));
          sb.append(" ");
          sb.append(Utils.getTimeString(r.startTime.getTimestamp().getTime()));
        }
        if (r.reservationNumber != null && !r.reservationNumber.equals(rs.getSrcRecLocator())) {
          sb.append(" (Confirmation#: ");sb.append(r.reservationNumber); sb.append(")");
        }
      }

      if (rh.cars.size() == 1) {
        bookingDetailView.bookingType = ReservationType.CAR_RENTAL;
        RentalCarReservation r = rh.cars.get(0);
        if (r.reservationFor.name != null) {
          sb.append(r.reservationFor.name);
        } else {
          sb.append("Car Rental ");
        }
        if (r.reservationFor.rentalCompany != null && r.reservationFor.rentalCompany.name != null) {
          sb.append(" ("); sb.append(r.reservationFor.rentalCompany.name); sb.append(")");
        }
        if (r.pickupLocation != null) {
          sb.append(" - Pickup ");
          if (r.pickupLocation.name != null) {
            sb.append(" from ");
            sb.append(r.pickupLocation.name);
          } else if (r.pickupLocation.address != null && r.pickupLocation.address.streetAddress != null) {
            sb.append(" from ");
            sb.append(r.pickupLocation.address.streetAddress);
          }
        }
        if (r.pickupTime != null && r.pickupTime.getTimestamp() != null) {
          sb.append(" on ");
          sb.append(Utils.formatTimestamp(r.pickupTime.getTimestamp().getTime(), "MMM dd, YYYY"));
          sb.append(" ");
          sb.append(Utils.getTimeString(r.pickupTime.getTimestamp().getTime()));
        }
        if (r.reservationNumber != null && !r.reservationNumber.equals(rs.getSrcRecLocator())) {
          sb.append(" (Confirmation#: ");sb.append(r.reservationNumber); sb.append(")");
        }
        if (r.underName != null) {
          sb.append(" for ");
          sb.append(r.underName.givenName);
          sb.append(" ");
          sb.append(r.underName.familyName);
        }
      }

      if (rh.cruises.size() == 1) {
        CruiseShipReservation r = rh.cruises.get(0);
        bookingDetailView.bookingType = ReservationType.CRUISE;
        if (r.reservationFor.name != null) {
          sb.append("Cruise: ");

          sb.append(r.reservationFor.name);
        } else {
          sb.append("Cruise");
        }
        if (r.reservationFor.provider != null && r.reservationFor.provider.name != null) {
          sb.append(" ("); sb.append(r.reservationFor.provider.name); sb.append(") ");
        }
        if (r.reservationFor.departPort != null && r.reservationFor.departPort.name != null) {
          sb.append("departs from ");
          sb.append(r.reservationFor.departPort.name);
        }
        if (r.reservationFor.departTime != null) {
          sb.append(" on ");
          sb.append(Utils.formatTimestamp(r.reservationFor.departTime.getTimestamp().getTime(), "MMM dd, YYYY"));
          sb.append(" ");
          sb.append(Utils.getTimeString(r.reservationFor.departTime.getTimestamp().getTime()));
        }
        if (r.reservationNumber != null && !r.reservationNumber.equals(rs.getSrcRecLocator())) {
          sb.append(" (Confirmation#: ");sb.append(r.reservationNumber); sb.append(")");
        }
        if (r.underName != null) {
          sb.append(" for ");
          sb.append(r.underName.givenName);
          sb.append(" ");
          sb.append(r.underName.familyName);
        }
      }

      if (rh.events.size() == 1) {
        EventReservation r = rh.events.get(0);
        bookingDetailView.bookingType = ReservationType.EVENT;
        if (r.reservationFor.name != null) {
          sb.append("Event: ");
          sb.append(r.reservationFor.name);
        } else {
          sb.append("Event");
        }
        if (r.reservationFor.startDate != null) {
          sb.append(" start ");
          sb.append(Utils.formatTimestamp(r.reservationFor.startDate.getTimestamp().getTime(), "MMM dd, YYYY"));
          sb.append(" ");
          sb.append(Utils.getTimeString(r.reservationFor.startDate.getTimestamp().getTime()));
        }
        if (r.reservationNumber != null && !r.reservationNumber.equals(rs.getSrcRecLocator())) {
          sb.append(" (Confirmation#: ");sb.append(r.reservationNumber); sb.append(")");
        }
        if (r.underName != null) {
          sb.append(" for ");
          sb.append(r.underName.givenName);
          sb.append(" ");
          sb.append(r.underName.familyName);
        }
      }

      if (rh.flights.size() == 1) {
        sb.append("Flight: ");
        FlightReservation r = rh.flights.get(0);
        bookingDetailView.bookingType = ReservationType.FLIGHT;

        if (r.reservationFor.airline != null && r.reservationFor.airline.name != null) {
          sb.append(r.reservationFor.airline.name);sb.append(" ");
        }

        if (r.reservationFor.airline != null && r.reservationFor.airline.iataCode != null) {
          sb.append(r.reservationFor.airline.iataCode);sb.append(" ");
        }

        if (r.reservationFor.flightNumber != null) {
          sb.append(r.reservationFor.flightNumber); sb.append(" ");
        }

        if (r.reservationFor.departureAirport != null) {
          sb.append("depart from ");
          if (r.reservationFor.departureAirport.iataCode != null) {
            sb.append(r.reservationFor.departureAirport.iataCode);
          } else if (r.reservationFor.departureAirport.name != null) {
            sb.append(r.reservationFor.departureAirport.name);
          }
          sb.append(" ");
        }

        if (r.reservationFor.departureTime != null) {
          try {
            sb.append(" on ");
            sb.append(Utils.formatTimestamp(r.reservationFor.departureTime.getTimestamp().getTime(), "MMM dd, YYYY"));
            sb.append(" ");
            sb.append(Utils.getTimeString(r.reservationFor.departureTime.getTimestamp().getTime()));
          } catch (Exception e) {

          }
        }
        if (r.reservationNumber != null && !r.reservationNumber.equals(rs.getSrcRecLocator())) {
          sb.append(" (Confirmation#: ");sb.append(r.reservationNumber); sb.append(")");
        }
        /*
        if (r.underName != null) {
          sb.append(" for ");
          sb.append(r.underName.givenName);
          sb.append(" ");
          sb.append(r.underName.familyName);
        }
        */
      }

      if (rh.hotels.size() == 1) {
        sb.append("Accommodations: ");
        LodgingReservation r = rh.hotels.get(0);
        bookingDetailView.bookingType = ReservationType.HOTEL;
        sb.append(" Check-in ");
        if (r.reservationFor.name != null) {
          sb.append(r.reservationFor.name);
        } else {
          sb.append("Accommodations");
        }
        if (r.reservationFor.address != null && r.reservationFor.address.addressLocality != null) {
          sb.append(" ("); sb.append(r.reservationFor.address.addressLocality);sb.append(") ");
        }
        if (r.checkinDate != null) {
          try {
            sb.append(" on ");
            sb.append(Utils.formatTimestamp(r.checkinDate.getTimestamp().getTime(), "MMM dd, YYYY"));
            sb.append(" ");
            sb.append(Utils.getTimeString(r.checkinDate.getTimestamp().getTime()));
          }catch (Exception e) {
            
          }
        }
        if (r.reservationNumber != null && !r.reservationNumber.equals(rs.getSrcRecLocator())) {
          sb.append(" (Confirmation#: ");sb.append(r.reservationNumber); sb.append(")");
        }
        if (r.underName != null) {
          sb.append(" for ");
          sb.append(r.underName.givenName);
          sb.append(" ");
          sb.append(r.underName.familyName);
        }
      }

      if (rh.restaurants.size() == 1) {
        FoodEstablishmentReservation r = rh.restaurants.get(0);
        bookingDetailView.bookingType = ReservationType.RESTAURANT;
        if (r.reservationFor.name != null) {
          sb.append("Restaurant Reservation at ");
          sb.append(r.reservationFor.name);
        } else {
          sb.append("Restaurant");
        }
        if (r.startTime != null) {
          sb.append(" for ");
          sb.append(Utils.formatTimestamp(r.startTime.getTimestamp().getTime(), "MMM dd, YYYY"));
          sb.append(" ");
          sb.append(Utils.getTimeString(r.startTime.getTimestamp().getTime()));
        }
        if (r.reservationNumber != null && !r.reservationNumber.equals(rs.getSrcRecLocator())) {
          sb.append(" (Confirmation#: ");sb.append(r.reservationNumber); sb.append(")");
        }
        if (r.underName != null) {
          sb.append(" for ");
          sb.append(r.underName.givenName);
          sb.append(" ");
          sb.append(r.underName.familyName);
        }
      }

      if (rh.trains.size() == 1) {
        TrainReservation r = rh.trains.get(0);
        bookingDetailView.bookingType = ReservationType.RAIL;
        if (r.reservationFor.name != null) {
          sb.append("Train: ");
          sb.append(r.reservationFor.name);
        } else {
          sb.append("Train");
        }
        if (r.reservationFor.departStation != null && r.reservationFor.departStation.name != null) {
          sb.append(" departs from ");
          sb.append(r.reservationFor.departStation.name);
          if (r.reservationFor.departStation.address != null && r.reservationFor.departStation.address.addressLocality != null) {
            sb.append(" ("); sb.append(r.reservationFor.departStation.address.addressLocality);sb.append(") ");
          }
        }

        if (r.reservationFor.departTime != null && r.reservationFor.departTime.getTimestamp() != null) {
          sb.append(" on ");
          sb.append(Utils.formatTimestamp(r.reservationFor.departTime.getTimestamp().getTime(), "MMM dd, YYYY"));
          sb.append(" ");
          sb.append(Utils.getTimeString(r.reservationFor.departTime.getTimestamp().getTime()));
        }
        if (r.reservationNumber != null && !r.reservationNumber.equals(rs.getSrcRecLocator())) {
          sb.append(" (Confirmation#: ");sb.append(r.reservationNumber); sb.append(")");
        }
        if (r.underName != null) {
          sb.append(" for ");
          sb.append(r.underName.givenName);
          sb.append(" ");
          sb.append(r.underName.familyName);
        }
      }

      if (rh.transfers.size() == 1) {
        TransferReservation r = rh.transfers.get(0);
        bookingDetailView.bookingType = ReservationType.TRANSPORT;

        if (r.reservationFor.name != null) {
          sb.append("Transfer");
          sb.append(r.reservationFor.name);
        } else {
          sb.append("Transport");
        }
        if (r.reservationFor.pickupLocation != null) {
          sb.append(" pickup at ");
          sb.append(r.reservationFor.pickupLocation.name);
        }
        if (r.reservationFor.pickupTime != null) {
          sb.append(" on ");
          sb.append(Utils.formatTimestamp(r.reservationFor.pickupTime.getTimestamp().getTime(), "MMM dd, YYYY"));
          sb.append(" ");
          sb.append(Utils.getTimeString(r.reservationFor.pickupTime.getTimestamp().getTime()));
        }
        if (r.reservationNumber != null && !r.reservationNumber.equals(rs.getSrcRecLocator())) {
          sb.append(" (Confirmation#: ");sb.append(r.reservationNumber); sb.append(")");
        }
        if (r.underName != null) {
          sb.append(" for ");
          sb.append(r.underName.givenName);
          sb.append(" ");
          sb.append(r.underName.familyName);
        }
      }

      if (rh.notes.size() == 1) {
        Note r = rh.notes.get(0);
        bookingDetailView.bookingType = ReservationType.NOTE;

        if (r.name != null) {
          sb.append(r.name);
        } else {
          sb.append("Remarks");
        }
        /*
        if (r.description != null) {
          sb.append(": ");
          if (r.description.substring(0, r.description.indexOf()))
          sb.append(r.description);
        }
        */
      }


      bookingDetailView.details = sb.toString();


      return bookingDetailView;
    } else {
      return null;
    }

  }



  public static BookingInboxView executeSearch (SearchBookingsForm form, String userId, List<String> cmpyId, long msOffset, Credentials cred) {
    int maxResults = 10;
    Map<Integer, String> sources = new HashMap<>();
    sources.putAll(BkApiSrc.getAllPublic()); //make a copy since we will potentially modify it
    if (!cred.hasCapability(Capability.INBOX_CLIENTBASE)) {
      sources.remove(BkApiSrc.getId("CLIENTBASE"));
    }
    if (!cred.hasCapability(Capability.INBOX_SABRE)) {
      sources.remove(BkApiSrc.getId("SABRE"));
    }

    String term = form.inKeyword;
    if (term == null || term.trim().length() == 0) {
      term = "%";
    } else {
      term = "%" + term + "%";
    }
    List<Integer> srcIds = new ArrayList();
    if (form.inSource != null && !form.inSource.equalsIgnoreCase("All")) {
      int i  = BkApiSrc.getId(form.inSource);
      if (i > -1) {
        srcIds.add(i);
      }
    }
    if (srcIds.size() == 0) {
      srcIds.addAll(sources.keySet());
    }



    List<Boolean> flags = new ArrayList<>();
    if (!form.inNewBookingsOnly) {
      flags.add(true);
    }
    flags.add(false);

    if (form.inStartPos < 0) {
      form.inStartPos = 0;
    }

    List<BookingRS> results =  BookingAPIMgr.findByTerm(term, srcIds, maxResults, form.inStartPos, flags,userId, cmpyId);
    Map<String, BookingReservationView> reservations = new HashMap<>();
    //get all reservations
    for (BookingRS rs : results) {
      if (rs.bookingType == ReservationType.PACKAGE.ordinal() && rs.getSrcRecLocator() != null && !reservations.containsKey(rs.getSrcRecLocator() + rs.getSrcCmpyId())) {
        BookingReservationView reservationView = new BookingReservationView();
        reservationView.pk = rs.getSrcPk() + rs.getSrcCmpyId();
        if (reservationView.pk != null) {
          reservationView.pk = reservationView.pk.replace(" ", "");
        }
        reservationView.reservationPackagePK = String.valueOf(rs.getPk());

        ReservationsHolder rh = TripPublisherHelper.parseJson(rs.data, BkApiSrc.getName(rs.srcId), rs.pk);
        if (rh.reservationPackage != null && rh.reservationPackage.name != null && !rh.reservationPackage.name.isEmpty()) {
          reservationView.details = rh.reservationPackage.name;
          reservationView.reservationName = rh.reservationPackage.name;
        } else {
          reservationView.details = "";
          reservationView.reservationName = "";
        }
        reservationView.src = sources.get(rs.getSrcId());

        reservationView.receivedTS = Utils.formatOffsetDateTimePrint(rs.getModifiedTS(), msOffset);
        String startDate = "";
        StringBuilder sb = new StringBuilder();
        if (rs.startTS > 0) {
          sb.append(Utils.formatTimestamp(rs.startTS, "MMM dd"));
        }
        String endDate = "";
        if (rs.endTS > 0) {
          if (rs.startTS > 0) {
            sb.append(" to ");
          }
          sb.append(Utils.formatTimestamp(rs.endTS, "MMM dd YYYY"));
        }
        reservationView.travelDate = sb.toString();
        reservationView.travelerName = rs.getMainTraveler();
        reservationView.recLocator = rs.getSrcRecLocator();
        reservationView.bookings =  new ArrayList<>();
        reservations.put(rs.getSrcRecLocator()+rs.getSrcCmpyId(), reservationView);
      }
    }

    //group all bookings for each reservation
    for (BookingRS rs : results) {
      if (rs.bookingType != ReservationType.PACKAGE.ordinal()  && rs.getSrcRecLocator() != null && reservations.containsKey(rs.getSrcRecLocator()+rs.getSrcCmpyId())) {
        BookingReservationView reservationView = reservations.get(rs.getSrcRecLocator()+rs.getSrcCmpyId());
        BookingDetailView rv = getReservationView(rs);

        if (rv != null && rv.details != null) {
          reservationView.bookings.add(rv);
        }
      }
    }


    BookingInboxView v = new BookingInboxView();
    v.newBookingsOnly = form.inNewBookingsOnly;

    if (reservations.size() > maxResults) {
      if (form.inStartPos == 0)
        v.startPos = maxResults + 1;
      else
        v.startPos += maxResults;
    } else
      v.startPos = 0;

    v.term = form.inKeyword;

    if (srcIds.size() > 1) {
      v.selectedSrc = "All";
    } else
      v.selectedSrc = form.inSource;

    v.sourceList = new ArrayList<>();
    v.sourceList.add("All");
    for (String src: sources.values()) {
      v.sourceList.add(src);
    }


    v.reservations= new ArrayList<>();
    for (String key : reservations.keySet()) {
      BookingReservationView  bv = reservations.get(key);

      if (bv.bookings != null && bv.bookings.size() > 0) {
        //lets create the summary line for the reservations
        int flights = 0; int hotels = 0; int cruises = 0; int rails = 0; int cars = 0; int events = 0; int restaurants = 0; int transfers = 0; int activities = 0;
        for (BookingDetailView dv: bv.bookings) {
          if (dv.bookingType == ReservationType.ACTIVITY) {
            activities++;
          }
          else if (dv.bookingType == ReservationType.FLIGHT) {
            flights++;
          }
          else if (dv.bookingType == ReservationType.HOTEL) {
            hotels++;
          }
          else if (dv.bookingType == ReservationType.CRUISE) {
            cruises++;
          }
          else if (dv.bookingType == ReservationType.RAIL) {
            rails++;
          }
          else if (dv.bookingType == ReservationType.CAR_RENTAL) {
            cars++;
          }
          else if (dv.bookingType == ReservationType.EVENT) {
            events++;
          }
          else if (dv.bookingType == ReservationType.RESTAURANT) {
            restaurants++;
          }
          else if (dv.bookingType == ReservationType.TRANSPORT) {
            transfers++;
          }
        }

          StringBuilder sb = new StringBuilder();
          if (flights == 1)
            sb.append(flights + " flight");
          else if (flights > 1)
            sb.append(flights + " flights");

          if (hotels > 0) {
            if (sb.toString().length() > 0)
              sb.append(", ");
            if (hotels == 1)
              sb.append(hotels + " hotel");
            else if (hotels > 1)
              sb.append(hotels + " hotels");
          }

          if (cars > 0) {
            if (sb.toString().length() > 0)
              sb.append(", ");
            if (cars == 1)
              sb.append(cars + " car rental");
            else if (cars > 1)
              sb.append(cars + " car rentals");
          }

          if (rails > 0) {
            if (sb.toString().length() > 0)
              sb.append(", ");
            if (rails == 1)
              sb.append(rails + " train");
            else if (rails > 1)
              sb.append(rails + " trains");
          }

          if (cruises > 0) {
            if (sb.toString().length() > 0)
              sb.append(", ");
            if (cruises == 1)
              sb.append(cruises + " cruise");
            else if (cruises > 1)
              sb.append(cruises + " cruises");
          }

          if (transfers > 0) {
            if (sb.toString().length() > 0)
              sb.append(", ");
            if (transfers == 1)
              sb.append(transfers + " transfer");
            else if (transfers > 1)
              sb.append(transfers + " transfers");
          }

          if (activities > 0) {
            if (sb.toString().length() > 0)
              sb.append(", ");
            if (activities == 1)
              sb.append(activities + " activity");
            else if (activities > 1)
              sb.append(activities + " activities");
          }

          if (events > 0) {
            if (sb.toString().length() > 0)
              sb.append(", ");
            if (events == 1)
              sb.append(events + " event ");
            else if (events > 1)
              sb.append(events + " events ");
          }

          if (restaurants > 0) {
            if (sb.toString().length() > 0)
              sb.append(", ");
            if (restaurants == 1)
              sb.append(restaurants + " restaurant ");
            else if (restaurants > 1)
              sb.append(restaurants + " restaurants ");
          }

          sb.append(" booking(s)");
          if (bv.details != null && !bv.details.isEmpty()) {
            bv.details = bv.details + " - " + sb.toString();
          } else {
            bv.details = sb.toString();
          }
          if (bv.details.contains(",")) {
            int index  = bv.details.lastIndexOf(",");
            bv.details = bv.details.substring(0, index) + " and" + bv.details.substring(index + 1);
          }


        v.reservations.add(bv);
      }
    }
    return v;
  }

  public  static String getReservation() {
    String flight = "{\n" +
                    "  \"@context\": \"http://schema.org\",\n" +
                    "  \"@type\": \"FlightReservation\",\n" +
                    "  \"reservationNumber\": \"RXJ34P\",\n" +
                    "  \"reservationStatus\": \"http://schema.org/Confirmed\",\n" +
                    "  \"url\": \"http://cheapair.com/view/RXJ34P\",\n" +
                    "  \"underName\": {\n" +
                    "    \"@type\": \"Person\",\n" +
                    "    \"name\": \"Eva Green\",\n" +
                    "    \"email\": \"eva@mail.com\"\n" +
                    "  },\n" +
                    "  \"bookingAgent\": {\n" +
                    "    \"@type\": \"Organization\",\n" +
                    "    \"name\": \"Cheap Air Travel\",\n" +
                    "    \"url\": \"http://cheapair.com/\"\n" +
                    "  },\n" +
                    "  \"bookingTime\": \"2013-01-14T13:05:00-05:00\",\n" +
                    "  \"modifiedTime\": \"2013-03-14T13:05:00-05:00\",\n" +
                    "  \"programMembership\": {\n" +
                    "    \"@type\": \"ProgramMembership\",\n" +
                    "    \"memberNumber\": \"4BY123111\",\n" +
                    "    \"program\": \"StarAlliance\"\n" +
                    "  },\n" +
                    "  \"confirmReservationUrl\": \"http://cheapair.com/confirm?id=RXJ34P\",\n" +
                    "  \"cancelReservationUrl\": \"http://cheapair.com/cancel?id=RXJ34P\",\n" +
                    "  \"modifyReservationUrl\": \"http://cheapair.com/edit?id=RXJ34P\",\n" +
                    "  \"checkinUrl\": \"http://united.com/onlinecheckin.html\",\n" +
                    "  \"reservationFor\": {\n" +
                    "    \"@type\": \"Flight\",\n" +
                    "    \"flightNumber\": \"110\",\n" +
                    "    \"airline\": {\n" +
                    "      \"@type\": \"Airline\",\n" +
                    "      \"name\": \"United\",\n" +
                    "      \"iataCode\": \"UA\"\n" +
                    "    },\n" +
                    "    \"operatedBy\": {\n" +
                    "      \"@type\": \"Airline\",\n" +
                    "      \"name\": \"Continental Airlines\",\n" +
                    "      \"iataCode\": \"CO\"\n" +
                    "    },\n" +
                    "    \"departureAirport\": {\n" +
                    "      \"@type\": \"Airport\",\n" +
                    "      \"name\": \"San Francisco Airport\",\n" +
                    "      \"iataCode\": \"SFO\"\n" +
                    "    },\n" +
                    "    \"departureTime\": \"2015-09-23T20:15:00-08:00\",\n" +
                    "    \"departureGate\": \"11\",\n" +
                    "    \"departureTerminal\": \"B\",\n" +
                    "    \"arrivalAirport\": {\n" +
                    "      \"@type\": \"Airport\",\n" +
                    "      \"name\": \"John F. Kennedy International Airport\",\n" +
                    "      \"iataCode\": \"JFK\"\n" +
                    "    },\n" +
                    "    \"arrivalTime\": \"2015-09-24T06:30:00-05:00\",\n" +
                    "    \"arrivalGate\": \"32\",\n" +
                    "    \"arrivalTerminal\": \"B\",\n" +
                    "    \"webCheckinTime\": \"2015-09-23T20:00:00-08:00\",\n" +
                    "    \"boardingTime\": \"2015-09-24T19:15:00-08:00\"\n" +
                    "  },\n" +
                    "  \"ticketNumber\": \"123XYZ\",\n" +
                    "  \"ticketDownloadUrl\": \"http://cheapair.com/download/RXJ34P.pdf\",\n" +
                    "  \"ticketPrintUrl\": \"http://cheapair.com/print/RXJ34P.html\",\n" +
                    "  \"ticketToken\": \"qrCode:123456789\",\n" +
                    "  \"additionalTicketText\": \"Some ticket details, terms and conditions...\",\n" +
                    "  \"airplaneSeat\": \"9A\",\n" +
                    "  \"airplaneSeatClass\": {\n" +
                    "    \"@type\": \"AirplaneSeatClass\",\n" +
                    "    \"name\": \"Business\"\n" +
                    "  },\n" +
                    "  \"boardingGroup\": \"B\",\n" +
                    "  \"remarks\": \"\",\n" +
                    "  \"cancellationPolicy\": \"\"\n" +
                    "}";


    String s = "{\n" +
               "  \"@context\": \"http://schema.org\",\n" +
               "  \"@type\": \"ReservationPackage\",\n" +
               "  \"reservation\": [" + flight + ", " + flight + "],\n" +
               "  \"bookingAgent\": {\n" +
               "    \"@type\": \"Organization\",\n" +
               "    \"name\": \"Car Rentals Internationaly\",\n" +
               "    \"url\": \"http://carrentals.com/\"\n" +
               "  },\n" +
               "  \"bookingTime\": \"2013-01-14T13:05:00-05:00\",\n" +
               "  \"modifiedTime\": \"2013-03-14T13:05:00-05:00\",\n" +
               "  \"reservationFor\": {\n" +
               "    \"@type\": \"note\",\n" +
               "    \"name\": \"Activity name\",\n" +
               "    \"description\": \"Fun Times\"\n" +
               "  },\n" +
               "  \"startLocation\": {\n" +
               "    \"@type\": \"Place\",\n" +
               "    \"name\": \"Hertz San Diego Airport\",\n" +
               "    \"address\": {\n" +
               "      \"@type\": \"PostalAddress\",\n" +
               "      \"streetAddress\": \"1500 Orange Avenue\",\n" +
               "      \"addressLocality\": \"San Diego\",\n" +
               "      \"addressRegion\": \"CA\",\n" +
               "      \"postalCode\": \"94043\",\n" +
               "      \"addressCountry\": \"US\"\n" +
               "    },\n" +
               "    \"telephone\": \"+1-800-123-4567\",\n" +
               "    \"geo\": {\n" +
               " \t  \"@type\": \" GeoCoordinates\",\n" +
               "  \t  \"elevation\": \"\",\n" +
               "      \"latitude\": \"\",\n" +
               "      \"longitude\": \"\"\n" +
               "    }\n" +
               "  },\n" +
               "  \"time\": \"2017-08-05T16:00:00-07:00\",\n" +
               "  \"remarks\": \"\"\n" +
               "}";




    return s;
  }


}
