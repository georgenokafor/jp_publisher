package com.umapped.persistence.reservation.transfer;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.umapped.persistence.reservation.UmDateTime;
import com.umapped.persistence.reservation.UmOrganization;
import com.umapped.persistence.reservation.UmPlace;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmTransfer
    implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private UmPlace pickupLocation;
  private UmDateTime pickupTime;
  private UmPlace dropoffLocation;
  private UmDateTime dropoffTime;
  private UmOrganization provider;
  public Integer partySize;

  public UmPlace getPickupLocation() {
    if (pickupLocation == null) {
      pickupLocation = new UmPlace();
    }
    return pickupLocation;
  }

  public void setPickupLocation(UmPlace pickupLocation) {
    this.pickupLocation = pickupLocation;
  }

  public UmDateTime getPickupTime() {
    return pickupTime;
  }

  public void setPickupTime(UmDateTime pickupTime) {
    this.pickupTime = pickupTime;
  }

  public UmPlace getDropoffLocation() {
    if (dropoffLocation == null) {
      dropoffLocation = new UmPlace();
    }
    return dropoffLocation;
  }

  public void setDropoffLocation(UmPlace dropoffLocation) {
    this.dropoffLocation = dropoffLocation;
  }

  public UmDateTime getDropoffTime() {
    return dropoffTime;
  }

  public void setDropoffTime(UmDateTime dropoffTime) {
    this.dropoffTime = dropoffTime;
  }

  public UmOrganization getProvider() {
    if (provider == null) {
      provider = new UmOrganization();
    }
    return provider;
  }

  public void setProvider(UmOrganization provider) {
    this.provider = provider;
  }
}
