package com.umapped.api.schema.local;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Created by wei on 2017-05-22.
 */
@JsonTypeName("TRAVELBOUNDPHOTOS")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TravelboundPhotosJson extends PoiPhotosJson {

}
