
package com.mapped.publisher.msg.flightstat.status;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mapped.publisher.msg.flightstat.event.UpdatedAt;
import com.mapped.publisher.msg.flightstat.event.UpdatedDateFields;
import com.mapped.publisher.msg.flightstat.event.UpdatedTextFields;

@JsonIgnoreProperties({"updatedDateFields","updatedTextFields"})

public class FlightStatusUpdate{
   	private String source;
   	private UpdatedAt updatedAt;
   	private UpdatedDateFields updatedDateFields;
  private UpdatedTextFields updatedTextFields;

 	public String getSource(){
		return this.source;
	}
	public void setSource(String source){
		this.source = source;
	}
 	public UpdatedAt getUpdatedAt(){
		return this.updatedAt;
	}
	public void setUpdatedAt(UpdatedAt updatedAt){
		this.updatedAt = updatedAt;
	}
 	public UpdatedDateFields getUpdatedDateFields(){
		return this.updatedDateFields;
	}
	public void setUpdatedDateFields(UpdatedDateFields updatedDateFields){
		this.updatedDateFields = updatedDateFields;
	}

  public UpdatedTextFields getUpdatedTextFields() {
    return updatedTextFields;
  }

  public void setUpdatedTextFields(UpdatedTextFields updatedTextFields) {
    this.updatedTextFields = updatedTextFields;
  }
}
