package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by surge on 2016-06-29.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ImageObject
    extends MediaObject {
  /**
   * The caption for this object.
   */
  public String caption;
  /**
   * Exif data for this object
   */
  List<PropertyValue> exifData;
  /**
   * Thumbnail image for an image or video.
   */
  ImageObject         thumbnail;
  /**
   * Indicates whether this image is representative of the content of the page.
   * (IGNORED)
   */
  //public Boolean representativeOfPage;
}
