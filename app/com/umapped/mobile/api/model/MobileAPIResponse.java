package com.umapped.mobile.api.model;

/**
 * Created by wei on 2017-08-16.
 */
public class MobileAPIResponse {
  private MobileAPIResponseCode code;
  private String message;
  private Object payload;

  public MobileAPIResponse() {

  }

  public MobileAPIResponse(MobileAPIResponseCode code) {
    this.code = code;
  }

  public MobileAPIResponse(MobileAPIResponseCode code, String message) {
    this.code = code;
    this.message = message;
  }

  public MobileAPIResponse(MobileAPIResponseCode code, String message, Object payload) {
    this.code = code;
    this.message = message;
    this.payload = payload;
  }

  public MobileAPIResponseCode getCode() {
    return code;
  }

  public void setCode(MobileAPIResponseCode code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Object getPayload() {
    return payload;
  }

  public void setPayload(Object payload) {
    this.payload = payload;
  }
}
