package com.mapped.publisher.form;

import play.data.validation.Constraints;

/**
 *
 */
public class ExperimentalPDFUploadForm
{
    public String inUserId;
    public String inPDFFileName;
    public boolean uploaded = false;

}
