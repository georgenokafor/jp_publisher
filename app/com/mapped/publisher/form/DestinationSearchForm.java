package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-15
 * Time: 2:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class DestinationSearchForm {
    private String inTripId;
    private boolean inTripNote;
    private String inTripDetailId;
    private String inDestIds;
    private String inCmpyId;
    private String inDestGuideId;
    private int inStartPos;
    private String inTripNoteDate;
    private String inNoteId;



    private String inKeyword;
    private String type_1;
    private String type_2;
    private String type_3;
    private String type_4;
    private String type_5;

    private boolean inSearchTitleOnly;

  public boolean isInTripNote() {
    return inTripNote;
  }

  public void setInTripNote(boolean inTripNote) {
    this.inTripNote = inTripNote;
  }

  public String getInTripId() {
        return inTripId;
    }

    public void setInTripId(String inTripId) {
        this.inTripId = inTripId;
    }

    public String getInKeyword() {
        return inKeyword;
    }

    public void setInKeyword(String inKeyword) {
        this.inKeyword = inKeyword;
    }

    public String getType_1() {
        return type_1;
    }

    public void setType_1(String type_1) {
        this.type_1 = type_1;
    }

    public String getType_2() {
        return type_2;
    }

    public void setType_2(String type_2) {
        this.type_2 = type_2;
    }

    public String getType_3() {
        return type_3;
    }

    public void setType_3(String type_3) {
        this.type_3 = type_3;
    }

    public String getType_4() {
        return type_4;
    }

    public void setType_4(String type_4) {
        this.type_4 = type_4;
    }

    public String getType_5() {
        return type_5;
    }

    public void setType_5(String type_5) {
        this.type_5 = type_5;
    }

    public String getInDestIds() {
        return inDestIds;
    }

    public void setInDestIds(String inDestIds) {
        this.inDestIds = inDestIds;
    }

    public String getInCmpyId() {
        return inCmpyId;
    }

    public void setInCmpyId(String inCmpyId) {
        this.inCmpyId = inCmpyId;
    }


    public String getInDestGuideId() {
        return inDestGuideId;
    }

    public void setInDestGuideId(String inDestGuideId) {
        this.inDestGuideId = inDestGuideId;
    }

    public int getInStartPos() {
        return inStartPos;
    }

    public void setInStartPos(int inStartPos) {
        this.inStartPos = inStartPos;
    }

  public boolean isInSearchTitleOnly() {
    return inSearchTitleOnly;
  }

  public void setInSearchTitleOnly(boolean inSearchTitleOnly) {
    this.inSearchTitleOnly = inSearchTitleOnly;
  }

  public String getInTripDetailId() {
    return inTripDetailId;
  }

  public void setInTripDetailId(String inTripDetailId) {
    this.inTripDetailId = inTripDetailId;
  }

  public String getInTripNoteDate() {
    return inTripNoteDate;
  }

  public void setInTripNoteDate(String inTripNoteDate) {
    this.inTripNoteDate = inTripNoteDate;
  }

    //Added by George
    public String getInNoteId() {
        return inNoteId;
    }

    public void setInNoteId(String inNoteId) {
        this.inNoteId = inNoteId;
    }

}
