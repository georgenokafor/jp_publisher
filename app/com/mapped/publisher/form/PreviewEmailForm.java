package com.mapped.publisher.form;

/**
 * Created by twong on 15-11-17.
 */
public class PreviewEmailForm {
  private String inTripId;
  private String inGroupId;
  private String inPassengerId;
  private String inLang;

  public String getInTripId() {
    return inTripId;
  }

  public void setInTripId(String inTripId) {
    this.inTripId = inTripId;
  }

  public String getInGroupId() {
    return inGroupId;
  }

  public void setInGroupId(String inGroupId) {
    this.inGroupId = inGroupId;
  }

  public String getInPassengerId() {
    return inPassengerId;
  }

  public void setInPassengerId(String inPassengerId) {
    this.inPassengerId = inPassengerId;
  }

  public String getInLang() { return inLang; }

  public void setInLang(String inLang) { this.inLang = inLang; }
}
