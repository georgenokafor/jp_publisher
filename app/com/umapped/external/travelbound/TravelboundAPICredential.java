package com.umapped.external.travelbound;

/**
 * Created by wei on 2017-05-12.
 */
public class TravelboundAPICredential {
  private String site;
  private int clientId;
  private String email;
  private String password;

  public TravelboundAPICredential(int clientId, String site, String email, String password) {
    this.clientId = clientId;
    this.site = site;
    this.email = email;
    this.password = password;
  }

  public String getSite() {
    return site;
  }

  public int getClientId() {
    return clientId;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }
}
