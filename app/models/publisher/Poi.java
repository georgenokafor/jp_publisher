package models.publisher;

import com.avaje.ebean.*;
import com.avaje.ebean.annotation.DbJson;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.persistence.PoiTypeInfo;
import com.mapped.publisher.persistence.RecordState;
import com.mapped.publisher.utils.Log;
import com.umapped.api.schema.local.PoiJson;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.postgresql.util.PGobject;

import javax.persistence.*;
import javax.persistence.Version;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by surge on 2016-02-17.
 * <p>
 * CREATE TABLE
 * poi
 * (
 * poi_id BIGINT NOT NULL,
 * consortium_id INTEGER DEFAULT 0 NOT NULL,
 * cmpy_id INTEGER DEFAULT 0 NOT NULL,
 * src_id INTEGER DEFAULT 0 NOT NULL,
 * type_id INTEGER DEFAULT 0 NOT NULL,
 * name CHARACTER VARYING NOT NULL,
 * code CHARACTER VARYING,
 * tags CHARACTER VARYING,
 * src_reference CHARACTER VARYING,
 * country_code CHARACTER VARYING(3) DEFAULT NULL::CHARACTER VARYING,
 * loc_lat REAL,
 * loc_long REAL,
 * file_count INTEGER DEFAULT 0,
 * data JSON NOT NULL,
 * state USER-DEFINED DEFAULT 'ACTIVE'::um_type_state NOT NULL,
 * search_words TEXT,
 * createdtimestamp BIGINT NOT NULL,
 * lastupdatedtimestamp BIGINT NOT NULL,
 * createdby CHARACTER VARYING NOT NULL,
 * modifiedby CHARACTER VARYING NOT NULL,
 * version INTEGER DEFAULT 0,
 * name_metaphone CHARACTER VARYING,
 * name_dmetaphone CHARACTER VARYING,
 * name_dmetaphone_alt CHARACTER VARYING,
 * CONSTRAINT poi_pk PRIMARY KEY (poi_id, consortium_id, cmpy_id, src_id),
 * CONSTRAINT country_code FOREIGN KEY (country_code) REFERENCES lst_country (alpha3),
 * CONSTRAINT poi_cmpy_fk FOREIGN KEY (cmpy_id) REFERENCES company (cmpy_id),
 * CONSTRAINT poi_consortium_fk FOREIGN KEY (consortium_id) REFERENCES consortium (cons_id),
 * CONSTRAINT poi_src_id_fk FOREIGN KEY (src_id) REFERENCES feed_src (src_id),
 * CONSTRAINT poi_type_fk FOREIGN KEY (type_id) REFERENCES poi_type (type_id)
 * );
 */
@Entity
@Table(name = "poi")
public class Poi
    extends Model {

  @Transient
  public static final ObjectMapper om;


  public enum DeleteMode {
    HARD,
    SOFT,
    AMENITIES
  }
  public static Model.Finder<PoiPk, Poi> find = new Finder(Poi.class);

  static {
    om = new ObjectMapper();
    //Perform any kind of configuration for object mapper in this area
  }

  @Id
  PoiPk pk;
  Integer typeId;
  String  name;
  String  code;
  String  srcReference;
  String  countryCode;
  Float   locLat;
  Float   locLong;
  Integer fileCount;
  @DbJson
  JsonNode    data;
  @Transient
  PoiJson     json;
  @Enumerated(value = EnumType.STRING)
  RecordState state;
  String searchWords;

  public String getTags() {
    return tags;
  }

  public void setTags(String tags) {
    this.tags = tags;
  }

  String tags;
  Long   createdtimestamp;
  Long   lastupdatedtimestamp;
  String modifiedby;
  String createdby;
  @Version
  int version;

  @Embeddable
  public static class PoiPk {

    @Column(name = "poi_id")
    Long    poiId;
    @Column(name = "consortium_id")
    Integer consortiumId;
    @Column(name = "cmpy_id")
    Integer cmpy_id;
    @Column(name = "src_id")
    Integer srcId;

    public static PoiPk build(Long poiId, Integer consortiumId, Integer cmpyId, Integer srcId) {
      PoiPk ppk = new PoiPk();
      ppk.poiId = poiId;
      ppk.consortiumId = consortiumId;
      ppk.cmpy_id = cmpyId;
      ppk.srcId = srcId;
      return ppk;
    }

    @Override
    public int hashCode() {
      return poiId.hashCode() * consortiumId.hashCode() * cmpy_id.hashCode() * srcId.hashCode();
    }

    @Override
    public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof PoiPk)) {
        return false;
      }
      PoiPk ok = (PoiPk) o;

      return poiId.equals(ok.poiId) && consortiumId.equals(ok.consortiumId) &&
             cmpy_id.equals(ok.cmpy_id) && srcId.equals(srcId);
    }

    public Integer getSrcId() {
      return srcId;
    }

    public void setSrcId(Integer srcId) {
      this.srcId = srcId;
    }

    public Long getPoiId() {
      return poiId;
    }

    public void setPoiId(Long poiId) {
      this.poiId = poiId;
    }

    public Integer getConsortiumId() {
      return consortiumId;
    }

    public void setConsortiumId(Integer consortiumId) {
      this.consortiumId = consortiumId;
    }

    public Integer getCmpy_id() {
      return cmpy_id;
    }

    public void setCmpy_id(Integer cmpy_id) {
      this.cmpy_id = cmpy_id;
    }
  }

  public static Poi build(int consortiumId, int cmpyId, int importSrc, int typeId, String userId) {
    Poi   p   = new Poi();
    PoiPk ppk = new PoiPk();
    ppk.setConsortiumId(consortiumId);
    ppk.setCmpy_id(cmpyId);
    ppk.setSrcId(importSrc);
    ppk.setPoiId(DBConnectionMgr.getUniqueLongId());
    p.setPk(ppk);
    p.setTypeId(typeId);
    p.setCreatedby(userId);
    p.setModifiedby(userId);
    p.setCreatedtimestamp(System.currentTimeMillis());
    p.setLastupdatedtimestamp(p.createdtimestamp);

    PoiJson pj = new PoiJson();
    p.setJson(pj);
    p.setState(RecordState.ACTIVE);
    p.setFileCount(0);
    return p;
  }


  public static Poi findByPk(Long poiId, Integer consortiumId, Integer cmpyId, Integer srcId) {
    return find.byId(PoiPk.build(poiId, consortiumId, cmpyId, srcId));
  }

  public static Map<Long, List<Poi>> smartSearch(String term,
                                                 Collection<Integer> poiTypeIds,
                                                 Collection<Integer> companyIds,
                                                 int limit) {

    if (term != null) {
      String[] searchTerms = StringUtils.split(term, ',');
      if (searchTerms.length == 1) {
        String cleanTerm = searchTerms[0].trim();
        if (poiTypeIds.size() == 1 &&
            cleanTerm.length() == 3 &&
            poiTypeIds.contains(PoiTypeInfo.Instance().byName("Airport").getId())) {
          return groupSearchResults(findByCode(cleanTerm, poiTypeIds, companyIds, limit));
        }
        return groupSearchResults(findByTerm(cleanTerm, poiTypeIds, companyIds, limit));
      }

      if (searchTerms.length > 1) {
        String keywords = "";
        for (int idx = 1; idx < searchTerms.length; idx++) {
          keywords += " " + searchTerms[idx];
        }
        keywords = keywords.trim();
        if (keywords.length() == 0) { //Just some junk in keywords
          return groupSearchResults(findByTerm(searchTerms[0], poiTypeIds, companyIds, limit));
        }
        else {
          return groupSearchResults(findByTermAndKeywords(searchTerms[0], keywords, poiTypeIds, companyIds, limit));
        }
      }
      else {
        return groupSearchResults(findByTerm(null, poiTypeIds, companyIds, limit));
      }
    }
    else {
      return groupSearchResults(findByTerm(null, poiTypeIds, companyIds, limit));
    }
  }

  public static Map<Long, List<Poi>> groupSearchResults(List<Poi> found) {
    Map<Long, List<Poi>> group = new TreeMap<>();

    if (found == null) { //Gracefully returning empty group
      return group;
    }

    for (Poi in : found) {
      Long      poiId   = in.getPk().getPoiId();
      List<Poi> poiCake = group.get(poiId);

      if (poiCake == null) {
        poiCake = new ArrayList<>(1);
        group.put(poiId, poiCake);
      }

      poiCake.add(in);
    }

    return group;
  }

  /**
   * Looking for pois by their code field
   * <p>
   * "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, " +
   * "        src_reference, country_code, loc_lat, loc_long, file_count, data, state, " +
   * "        createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, version " +
   * "FROM poi p INNER JOIN (SELECT DISTINCT poi_id " +
   * "                       FROM poi " +
   * "                       WHERE state = 'ACTIVE' AND " +
   * "                             cmpy_id = ANY(?) AND " +
   * "                             type_id = ANY(?) AND " +
   * "                             code ILIKE ?" +
   * "                       LIMIT ?) r " +
   * "           ON p.poi_id = r.poi_id " +
   * "WHERE (p.cmpy_id = ANY(?) AND consortium_id = 0) OR " +
   * "      p.consortium_id IN (SELECT cons_id " +
   * "                          FROM consortium_company " +
   * "                          WHERE cmpy_id = ANY(?)) " +
   * "ORDER BY name ASC";
   *
   * @param code
   * @param poiTypeIds
   * @param companyIds
   * @param limit
   * @return
   */
  public static List<Poi> findByCode(String code,
                                     Collection<Integer> poiTypeIds,
                                     Collection<Integer> companyIds,
                                     int limit) {
    com.avaje.ebean.Query<Poi> matchingIds = Ebean
        .createQuery(Poi.class)
        .select("pk.poiId")
        .setDistinct(true)
        .where()
        .raw("state = cast(? as um_type_state)", RecordState.ACTIVE)
        .in("type_id", poiTypeIds)
        .raw("code ilike ?", "%" + code + "%")
        .query()
        .setMaxRows(limit);

    com.avaje.ebean.Query<ConsortiumCompany> cmpyConsortiumIds = Ebean
        .createQuery(ConsortiumCompany.class)
        .select("consCmpyId.consId")
        .where()
        .in("cmpy_id", companyIds)
        .query();

    return find
        .where()
        .in("pk.poiId", matchingIds)
        .or(Expr.and(Expr.in("cmpy_id", companyIds), Expr.eq("consortium_id", 0)),
            Expr.in("consortium_id", cmpyConsortiumIds))
        .findList();
  }

  /**
   * Searching for term, if term is null or empty returning recent records
   *
   * @param term       - word to look for in the name of the point of interest
   * @param poiTypeIds Collection of POI types
   * @param companyIds Company Integer identifiers (warning not old string ids)
   * @param limit      maximum number of records to return (it is possible that not all tuples of the same vendor will
   *                   be returned)
   * @return Returns a list of poi records, can return null
   * <p>
   * <p>
   * "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, " +
   * "        src_reference, country_code, loc_lat, loc_long, file_count, data, state, " +
   * "        createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, version  " +
   * "FROM poi p INNER JOIN (SELECT DISTINCT poi_id  " +
   * "                       FROM poi " +
   * "                       WHERE state = 'ACTIVE' AND " +
   * "                             type_id = ANY(?) AND  " +
   * "                             (name ILIKE ? OR " +
   * "                              (to_tsvector('english',name) @@ plainto_tsquery('english', ?)) OR " +
   * "                               code = ? OR tags ilike ?)   " +
   * "                       LIMIT ?) r  " +
   * "           ON p.poi_id = r.poi_id " +
   * "WHERE (p.cmpy_id = ANY(?) AND consortium_id = 0) OR " +
   * "      p.consortium_id IN (SELECT cons_id " +
   * "                          FROM consortium_company " +
   * "                          WHERE cmpy_id = ANY(?)) " +
   * "ORDER BY name ASC";
   */
  public static List<Poi> findByTerm(String term,
                                     Collection<Integer> poiTypeIds,
                                     Collection<Integer> companyIds,
                                     int limit) {
    if (term == null || term.trim().length() == 0) {
      return findRecent(companyIds, limit);
    }

    String[] parts    = StringUtils.split(term);
    String   likeTerm = "%" + StringUtils.join(parts, '%') + "%";

    com.avaje.ebean.Query<Poi> matchingIds = Ebean
        .createQuery(Poi.class)
        .select("pk.poiId")
        .setDistinct(true)
        .where()
        .raw("state = cast(? as um_type_state)", RecordState.ACTIVE)
        .in("type_id", poiTypeIds)
        .or(Expr.or(Expr.raw("name ilike ?", likeTerm),
                    Expr.raw("to_tsvector('english',name) @@ plainto_tsquery('english', ?)", term)),
            Expr.or(Expr.eq("code", term.toUpperCase()), Expr.raw("tags ilike ?", likeTerm)))
        .query()
        .setMaxRows(limit);

    com.avaje.ebean.Query<ConsortiumCompany> cmpyConsortiumIds = Ebean
        .createQuery(ConsortiumCompany.class)
        .select("consCmpyId.consId")
        .where()
        .in("cmpy_id", companyIds)
        .query();

    return find
        .where()
        .in("pk.poiId", matchingIds)
        .or(Expr.and(Expr.in("cmpy_id", companyIds), Expr.eq("consortium_id", 0)),
            Expr.in("consortium_id", cmpyConsortiumIds))
        .findList();
  }

  /**
   * "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, " +
   * "        src_reference, country_code, loc_lat, loc_long, file_count, data, state, " +
   * "        createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, version " +
   * "FROM poi p INNER JOIN (SELECT DISTINCT poi_id " +
   * "                       FROM poi " +
   * "                       WHERE state = 'ACTIVE' AND " +
   * "                             type_id = ANY(?) AND " +
   * "                             (name ILIKE ? OR " +
   * "                              to_tsvector('english',name) @@ plainto_tsquery('english', ?) OR " +
   * "                              code = ?) AND" +
   * "                              (search_words ILIKE ? OR " +
   * "                               (to_tsvector('english', search_words) @@ plainto_tsquery('english', ?))) " +
   * "                       LIMIT ?) r " +
   * "           ON p.poi_id = r.poi_id " +
   * "WHERE (p.cmpy_id = ANY(?) AND consortium_id = 0) OR " +
   * "      p.consortium_id IN (SELECT cons_id " +
   * "                          FROM consortium_company " +
   * "                          WHERE cmpy_id = ANY(?)) " +
   * "ORDER BY name ASC";
   *
   * @param term
   * @param keywords
   * @param poiTypeIds
   * @param companyIds
   * @param limit
   * @return
   */
  public static List<Poi> findByTermAndKeywords(String term,
                                                String keywords,
                                                Collection<Integer> poiTypeIds,
                                                Collection<Integer> companyIds,
                                                int limit) {
    String[] termParts = StringUtils.split(term);
    String   likeTerm  = "%" + StringUtils.join(termParts, '%') + "%";
    String[] kwParts   = StringUtils.split(keywords);
    String   likeKW    = "%" + StringUtils.join(kwParts, '%') + "%";

    com.avaje.ebean.Query<Poi> matchingIds = Ebean
        .createQuery(Poi.class)
        .select("pk.poiId")
        .setDistinct(true)
        .where()
        .raw("state = cast(? as um_type_state)", RecordState.ACTIVE)
        .in("type_id", poiTypeIds)
        .and(Expr.or(Expr.or(Expr.raw("name ilike ?", likeTerm),
                             Expr.raw("to_tsvector('english',name) @@ plainto_tsquery('english', ?)", term)),
                     Expr.eq("code", term)),
             Expr.or(Expr.raw("search_words ilike ?", likeKW),
                     Expr.raw("to_tsvector('english', search_words) @@ plainto_tsquery('english', ?)", keywords)))
        .query()
        .setMaxRows(limit);

    com.avaje.ebean.Query<ConsortiumCompany> cmpyConsortiumIds = Ebean
        .createQuery(ConsortiumCompany.class)
        .select("consCmpyId.consId")
        .where()
        .in("cmpy_id", companyIds)
        .query();

    return find
        .where()
        .in("pk.poiId", matchingIds)
        .or(Expr.and(Expr.in("cmpy_id", companyIds), Expr.eq("consortium_id", 0)),
            Expr.in("consortium_id", cmpyConsortiumIds))
        .findList();
  }

  public PoiPk getPk() {
    return pk;
  }

  public void setPk(PoiPk pk) {
    this.pk = pk;
  }

  /**
   * "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
   * "country_code, loc_lat, loc_long, file_count, data, state, createdtimestamp, " +
   * "lastupdatedtimestamp, createdby, modifiedby, version " +
   * "FROM poi p INNER JOIN (SELECT poi_id " +
   * "                       FROM poi " +
   * "                       WHERE state = 'ACTIVE' " +
   * "                       ORDER BY lastupdatedtimestamp DESC " +
   * "                       LIMIT ?) r " +
   * "            ON p.poi_id = r.poi_id" +
   * "WHERE (p.cmpy_id = ANY(?) AND consortium_id = 0) OR " +
   * "      p.consortium_id IN (SELECT cons_id " +
   * "                          FROM consortium_company " +
   * "                          WHERE cmpy_id = ANY(?)) ";
   *
   * @param companyIds
   * @param limit
   * @return
   */
  public static List<Poi> findRecent(Collection<Integer> companyIds, int limit) {

    com.avaje.ebean.Query<Poi> matchingIds = Ebean
        .createQuery(Poi.class)
        .select("pk.poiId")
        .setDistinct(true)
        .where()
        .raw("state = cast(? as um_type_state)", RecordState.ACTIVE)
        .query()
        .orderBy("lastupdatedtimestamp DESC")
        .setMaxRows(limit);

    com.avaje.ebean.Query<ConsortiumCompany> cmpyConsortiumIds = Ebean
        .createQuery(ConsortiumCompany.class)
        .select("consCmpyId.consId")
        .where()
        .in("cmpy_id", companyIds)
        .query();

    return find
        .where()
        .in("pk.poiId", matchingIds)
        .or(Expr.and(Expr.in("cmpy_id", companyIds), Expr.eq("consortium_id", 0)),
            Expr.in("consortium_id", cmpyConsortiumIds))
        .findList();
  }

  public static Poi findForCmpy(Long poiId, Integer cmpyId) {
    return find.where().eq("pk.poiId", poiId).eq("cmpy_id", cmpyId).findUnique();
  }

  public static List<Poi> findById(Long poiId, Collection<Integer> companyIds) {
    if (companyIds == null || companyIds.size() == 0) {
      return findById(poiId);
    }

    com.avaje.ebean.Query<ConsortiumCompany> cmpyConsortiumIds = Ebean
        .createQuery(ConsortiumCompany.class)
        .select("consCmpyId.consId")
        .where()
        .in("cmpy_id", companyIds)
        .query();

    return find
        .where()
        .eq("pk.poiId", poiId)
        .or(Expr.and(Expr.in("cmpy_id", companyIds), Expr.eq("consortium_id", 0)),
            Expr.in("consortium_id", cmpyConsortiumIds))
        .findList();
  }

  public static List<Poi> findById(Long poiId) {
    return find.where().eq("pk.poiId", poiId).findList();
  }

  /**
   * "SELECT poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
   * "country_code, loc_lat, loc_long, file_count, data, state, createdtimestamp, " +
   * "lastupdatedtimestamp, createdby, modifiedby, version " +
   * "FROM poi p " +
   * "WHERE p.cmpy_id = 0 and p.consortium_id = 0 and p.state = 'ACTIVE' and p.type_id = 7 " +
   * "     and p.code is not null and length(code) = 3 " +
   * "ORDER BY p.name asc";
   *
   * @return Returns a list of poi records. Can return null
   */
  public static List<Poi> findPortWithCode() {
    return find
        .where()
        .raw("state = cast(? as um_type_state)", RecordState.ACTIVE)
        .eq("cmpy_id", 0)
        .eq("consortium_id", 0)
        .eq("type_id", 7)
        .isNotNull("code")
        .raw("length(code) = ", 3)
        .orderBy("name asc")
        .findList();
  }

  /**
   * "SELECT poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
   * "country_code, loc_lat, loc_long, file_count, data, state, createdtimestamp, " +
   * "lastupdatedtimestamp, createdby, modifiedby, version " +
   * "FROM poi p " +
   * "WHERE p.cmpy_id = 0 and p.consortium_id = 0 and
   * "      p.state = 'ACTIVE' and p.type_id = 7 " +
   * " and ( p.name = ? or  " +
   * "      ((p.loc_lat between ? and ?) " +
   * "      and (p.loc_long between ? and ?)) " +
   * " )"
   *
   * @param name
   * @param locLat
   * @param locLong
   * @param delta
   * @return Returns a list of poi records.
   */
  public static List<Poi> findPortByNameorGPS(String name, float locLat, float locLong, float delta) {
    float minLocLat  = locLat - delta;
    float maxLocLat  = locLat + delta;
    float minLocLong = locLong - delta;
    float maxLocLong = locLong + delta;


    return find
        .where()
        .eq("cmpy_id", 0)
        .eq("consortium_id", 0)
        .raw("state = cast(? as um_type_state)", RecordState.ACTIVE)
        .eq("type_id", 7)
        .or(Expr.eq("name", name),
            Expr.and(Expr.between("loc_lat", minLocLat, maxLocLat), Expr.between("loc_long", minLocLong, maxLocLong)))
        .findList();

  }

  @PostLoad
  public void postLoad() {
    try {
      ObjectReader or = om.readerFor(PoiJson.class);
      json = or.treeToValue(data, PoiJson.class);
      //json = om.treeToValue(data, PoiJson.class);
    }
    catch (JsonProcessingException e) {
      Log.err("Poi: Failed to parse JSON tree");
    }
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public Float getLocLat() {
    return locLat;
  }

  public void setLocLat(Float locLat) {
    this.locLat = locLat;
  }

  public Float getLocLong() {
    return locLong;
  }

  public void setLocLong(Float locLong) {
    this.locLong = locLong;
  }

  public JsonNode getData() {
    return data;
  }

  public void setData(JsonNode data) {
    this.data = data;
  }

  public RecordState getState() {
    return state;
  }

  public void setState(RecordState state) {
    this.state = state;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public void saveManual() {
    if (version == 0) {
      insertManual();
    }
    else {
      updateManual();
    }
  }

  static final String SQL_INSERT_POI = "INSERT INTO poi (poi_id, consortium_id, cmpy_id, type_id, name, code, tags, " +
                                       "src_id, src_reference, " +
                                       "country_code, loc_lat, loc_long, file_count, data, state, search_words, " +
                                       "createdtimestamp," +
                                       "lastupdatedtimestamp, createdby, modifiedby, version) " +
                                       "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, cast(? as um_type_state), " +
                                       "?, ?, ?, ?, ?, ?)";

  public boolean insertManual() {
    Transaction transaction = Ebean.beginTransaction();
    Connection conn = transaction.getConnection();

    boolean        result = false;
    if (conn == null) {
      return result;
    }

    try {
      prePersist();
      PreparedStatement query        = conn.prepareStatement(SQL_INSERT_POI);
      int               paramCounter = 0;
      query.setLong(++paramCounter, getPk().getPoiId());
      query.setInt(++paramCounter, getPk().getConsortiumId());
      query.setInt(++paramCounter, getPk().getCmpy_id());
      query.setInt(++paramCounter, getTypeId());
      query.setString(++paramCounter, getName());
      query.setString(++paramCounter, getCode());
      query.setString(++paramCounter, getTags());
      query.setInt(++paramCounter, getPk().getSrcId());
      query.setString(++paramCounter, getSrcReference());
      query.setString(++paramCounter, getCountryCode());
      query.setDouble(++paramCounter, getLocLat());
      query.setDouble(++paramCounter, getLocLong());
      query.setInt(++paramCounter, getFileCount());
      String json = om.writeValueAsString(getData());
      PGobject jsonPGSQLObject = new PGobject();
      jsonPGSQLObject.setType("json");
      jsonPGSQLObject.setValue(json);
      query.setObject(++paramCounter, jsonPGSQLObject);
      query.setString(++paramCounter, getState().name());
      query.setString(++paramCounter, getSearchWords());
      query.setLong(++paramCounter, getCreatedtimestamp());
      query.setLong(++paramCounter, getLastupdatedtimestamp());
      query.setString(++paramCounter, getCreatedby());
      query.setString(++paramCounter, getModifiedby());
      query.setInt(++paramCounter, getVersion());

      if (query.executeUpdate() == 0){
        Log.info("Unexpected result from insert query :" + query.toString());
      } else  {
        result = true;
      }

      transaction.addModification("poi", true, false, false); //Flagging new "insert"
      Ebean.commitTransaction();
    }
    catch(SQLException e){
      Log.err("SQL Exception while inserting poi:" + e.getMessage());
      e.printStackTrace();
    }
    catch (JsonProcessingException jpe) {
      Log.err("Failed to generate POI JSON:" + jpe.getMessage());
    }
    finally {
      Ebean.endTransaction();
    }
    return result;
  }

  static final String SQL_UPDATE_POI = "UPDATE poi " +
                                       "SET poi_id = ?, consortium_id = ?, cmpy_id = ?, type_id = ?, name = ?, code =" +
                                       " ?, tags = ?, " +
                                       "src_id = ?, src_reference = ?, country_code = ?, loc_lat = ?, loc_long = ?, " +
                                       "file_count = ?, " +
                                       "data = ?, state = cast(? as um_type_state), search_words = ?, " +
                                       "lastupdatedtimestamp = ?, " +
                                       "modifiedby = ?, version = ? " +
                                       "WHERE poi_id = ? and consortium_id = ? and cmpy_id = ? and src_id = ?";

  public boolean updateManual() {
    Transaction transaction = Ebean.beginTransaction();
    Connection conn = transaction.getConnection();

    boolean result = false;
    if (conn == null) {
      return result;
    }

    try {
      prePersist();
      PreparedStatement query = conn.prepareStatement(SQL_UPDATE_POI);
      int paramCounter = 0;
      //poi_id = ?, consortium_id = ?, cmpy_id = ?, type_id = ?, name = ?, code = ?, tags = ?,
      //src_id = ?, src_reference = ?, country_code = ?, loc_lat = ?, loc_long = ?, file_count = ?,
      //data = ?, state = cast(? as um_type_state), search_words = ?, lastupdatedtimestamp = ?,
      //modifiedby = ?, version = ?
      //WHERE poi_id = ? and consortium_id = ? and cmpy_id = ?
      query.setLong(++paramCounter, getPk().getPoiId());
      query.setInt(++paramCounter, getPk().getConsortiumId());
      query.setInt(++paramCounter, getPk().getCmpy_id());
      query.setInt(++paramCounter, getTypeId());
      query.setString(++paramCounter, getName());
      query.setString(++paramCounter, getCode());
      query.setString(++paramCounter, getTags());
      query.setInt(++paramCounter, getPk().getSrcId());
      query.setString(++paramCounter, getSrcReference());

      query.setString(++paramCounter, getCountryCode());
      query.setDouble(++paramCounter, getLocLat());
      query.setDouble(++paramCounter, getLocLong());
      query.setInt(++paramCounter, getFileCount());
      String json = om.writeValueAsString(getData());
      PGobject jsonPGSQLObject = new PGobject();
      jsonPGSQLObject.setType("json");
      jsonPGSQLObject.setValue(json);
      query.setObject(++paramCounter, jsonPGSQLObject);
      query.setString(++paramCounter, getState().name());
      query.setString(++paramCounter, getSearchWords());
      query.setLong(++paramCounter, getLastupdatedtimestamp());
      query.setString(++paramCounter, getModifiedby());
      query.setInt(++paramCounter, getVersion());

      query.setLong(++paramCounter, getPk().getPoiId());
      query.setInt(++paramCounter, getPk().getConsortiumId());
      query.setInt(++paramCounter, getPk().getCmpy_id());
      query.setInt(++paramCounter, getPk().getSrcId());

      if (query.executeUpdate() == 0){
        Log.info("Unexpected result from update query :" + query.toString());
      } else  {
        result = true;
      }

      transaction.addModification("poi", true, false, false); //Flagging new "insert"
      Ebean.commitTransaction();
    }
    catch(SQLException e){
      Log.err("SQL Exception while update poi:" + e.getMessage());
      e.printStackTrace();
    }
    catch (JsonProcessingException jpe) {
      Log.err("Failed to generate POI JSON:" + jpe.getMessage());
      jpe.printStackTrace();
    }
    finally {
      Ebean.endTransaction();
    }
    return result;
  }

  public static int mergeCmpy(Integer srcCmpyId, Integer targetCmpyId, String userId) {

    int result;
    String s = "UPDATE poi SET cmpy_id = ?, lastupdatedtimestamp = ?, modifiedby = ?, " +
               "version = version + 1 " +
               "WHERE cmpy_id = ? AND poi_id IN (SELECT poi_id " +
               "     FROM poi " +
               "     WHERE cmpy_id = ? AND poi_id NOT IN (SELECT poi_id " +
               "     FROM poi " +
               "     WHERE cmpy_id = ?))";

    SqlUpdate update = Ebean.createSqlUpdate(s);

    int               paramCounter = 0;
    long              currTime     = System.currentTimeMillis();
    update.setParameter(++paramCounter, targetCmpyId);
    update.setParameter(++paramCounter, currTime);
    update.setParameter(++paramCounter, userId);
    update.setParameter(++paramCounter, srcCmpyId);
    update.setParameter(++paramCounter, srcCmpyId);
    update.setParameter(++paramCounter, targetCmpyId);

    result = update.execute();
    if (result == 0){
      Log.info("Unexpected result from merge query :" + update.getSql());
    }

    return result;
  }

  public void markUpdated(String userId) {
    setModifiedby(userId);
    setLastupdatedtimestamp(System.currentTimeMillis());
  }

  public boolean markDeleted(String userId) {
    setState(RecordState.DELETED);
    markUpdated(userId);
    return updateManual();
  }

  public Integer getTypeId() {
    return typeId;
  }

  public void setTypeId(Integer typeId) {
    this.typeId = typeId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getSrcReference() {
    return srcReference;
  }

  public void setSrcReference(String srcReference) {
    this.srcReference = srcReference;
  }

  public Integer getFileCount() {
    return fileCount;
  }

  @PrePersist
  public void prePersist() {
    json.prepareForDb();
    data = om.valueToTree(json);
  }

  public PoiJson getJson() {
    return json;
  }

  public void setJson(PoiJson json) {
    this.json = json;
  }

  public String getSearchWords() {
    return searchWords;
  }

  public void setSearchWords(String searchWords) {
    this.searchWords = searchWords;
  }

  public void setFileCount(Integer fileCount) {
    this.fileCount = fileCount;
  }

  @Override
  public void save() {
    throw new NotImplementedException("Poi can't use Ebean save to persist the model");
  }

  @Override
  public void update() {
    throw new NotImplementedException("Poi can't use Ebean update to persist the model");
  }

  @Override
  public void insert() {
    throw new NotImplementedException("Poi can't use Ebean insert to persist the model");
  }


  /**
   * "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id," +
   * "       src_reference, country_code, loc_lat, loc_long, file_count, data, state, " +
   * "       createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, version " +
   * "FROM poi p INNER JOIN (SELECT DISTINCT poi_id " +
   * "                       FROM poi " +
   * "                       WHERE type_id = ? AND " +
   * "                             (name ILIKE ? OR" +
   * "                              metaphone(name, 5) = metaphone(?, 5)) " +
   * "                      ) r " +
   * "           ON p.poi_id = r.poi_id"
   * @param name
   * @param poiType
   * @return
   */
  public static List<Poi> findForMerge(String name, int poiType){
    com.avaje.ebean.Query<Poi> matchingIds = Ebean
        .createQuery(Poi.class)
        .select("pk.poiId")
        .setDistinct(true)
        .where()
        .eq("type_id", poiType)
        .or(Expr.raw("name ilike ?", name),
            Expr.raw("metaphone(name, 5) = metaphone(?, 5))", name))
        .query();

    return find.where().in("pk.poiId", matchingIds).findList();
  }

  /**
   * "SELECT p.poi_id as poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference," +
   * "       country_code, loc_lat, loc_long, file_count, data, state, " +
   * "       createdtimestamp, lastupdatedtimestamp, createdby, modifiedby, version " +
   * "FROM poi p INNER JOIN (SELECT DISTINCT poi_id " +
   * "                       FROM poi " +
   * "                       WHERE type_id = ? AND country_code = ? AND " +
   * "                             (name ILIKE ? OR" +
   * "                              metaphone(name, 5) = metaphone(?, 5)) " +
   * "                      ) r " +
   * "           ON p.poi_id = r.poi_id"
   * @param name
   * @param countryA3
   * @param poiType
   * @return
   */
  public static List<Poi> findForMerge(String name, String countryA3, int poiType) {
    if(countryA3 == null) {
      findForMerge(name, poiType);
    }

    com.avaje.ebean.Query<Poi> matchingIds = Ebean
        .createQuery(Poi.class)
        .select("pk.poiId")
        .setDistinct(true)
        .where()
        .eq("type_id", poiType)
        .eq("country_code", countryA3)
        .or(Expr.raw("name ilike ?", name),
            Expr.raw("metaphone(name, 5) = metaphone(?, 5))", name))
        .query();

    return find.where().in("pk.poiId", matchingIds).findList();
  }


  static final String SQL_DELETE_DANGLING_POI_HARD =
      "DELETE FROM poi " +
      "WHERE src_id = ? AND poi_id IN (SELECT poi_id" +
      "                                  FROM poi" +
      "                                  WHERE poi_id NOT IN (SELECT poi_id" +
      "                                                       FROM poi_src_feed" +
      "                                                       WHERE src_id = ?) AND" +
      "                                        src_id = ?)";


  static final String     SQL_DELETE_DANGLING_POI_SOFT =
      "UPDATE poi SET  state = 'DELETED', lastupdatedtimestamp = ?, modifiedby = ? " +
      "WHERE src_id = ? AND poi_id IN  (SELECT poi_id " +
      "                                    FROM poi " +
      "                                    WHERE poi_id NOT IN (SELECT poi_id " +
      "                                                         FROM poi_src_feed " +
      "                                                         WHERE src_id = ?) AND " +
      "                                          src_id = ?)";

  /**
   * Deletes all POI records which are no longer present in the feed.
   *
   * @param srcId - source id to clean
   * @param mode  - mode to delete orphans (SOFT or HARD supported)
   * @param userId - when soft deleting this will populate modifiedBy field
   * @return number of removed records
   */
  public static int deleteOrphanPoi(int srcId, DeleteMode mode, String userId) {
    SqlUpdate update;
    int               paramCounter = 0;
    switch (mode) {
      case HARD:
        update  = Ebean.createSqlUpdate(SQL_DELETE_DANGLING_POI_HARD);
        break;
      case SOFT:
        update  = Ebean.createSqlUpdate(SQL_DELETE_DANGLING_POI_SOFT);
        update.setParameter(++paramCounter, System.currentTimeMillis());
        update.setParameter(++paramCounter, userId);
        break;
      default:
        throw new NotImplementedException("Deleting orphan POI records in " + mode + " is not supported.");
    }

    update.setParameter(++paramCounter, srcId);
    update.setParameter(++paramCounter, srcId);
    update.setParameter(++paramCounter, srcId);

    int result = update.execute();
    if (result == 0) {
      Log.info("No dangling POI found for source id: " + srcId);
    }
    return result;
  }

  /**
   * Find cruise ships with no photos
   *
   * "SELECT poi_id, consortium_id, cmpy_id, type_id, name, code, src_id, src_reference, " +
   * "country_code, loc_lat, loc_long, file_count, data, state, createdtimestamp, " +
   * "lastupdatedtimestamp, createdby, modifiedby, version " +
   * "FROM poi p " +
   * "WHERE p.cmpy_id = 0 and p.consortium_id = 0 and p.state = 'ACTIVE' and p.type_id = 6 " +
   * " and p.file_count = 0;";
   * @return
   */
  public static List<Poi> findCruiseShipsNoFiles(){
    return find.where()
               .eq("pk.cmpy_id", 0)
               .eq("pk.consortium_id", 0)
               .raw("state = cast(? as um_type_state)", RecordState.ACTIVE)
               .eq("type_id", 6).eq("fileCount", 0).findList();
  }

}
