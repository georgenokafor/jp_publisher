package com.mapped.publisher.parse.extractor.booking.valueObject;

import com.umapped.persistence.enums.ReservationType;

import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-10-25
 * Time: 9:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class TransportVO
    extends ReservationVO {

  private String    cmpyName;
  //private String    contact;
  private String    confirmationNumber;
  private String    pickupLocation;
  private String    dropoffLocation;
  private Timestamp pickupDate;
  private Timestamp dropoffDate;
  private ReservationType bookingType = ReservationType.TRANSPORT;

  /*
  public String getContact() {
    return contact;
  }

  public TransportVO setContact(String contact) {
    this.contact = contact;
    return this;
  }
  */

  public boolean isValid() {
    if (name != null && name.trim().length() > 0 && pickupDate != null) {
      return true;
    }
    return false;
  }


  public ReservationType getBookingType() {
    return bookingType;
  }

  public void setBookingType(ReservationType bookingType) {
    this.bookingType = bookingType;
  }

  public String getCmpyName() {
    return cmpyName;
  }

  public void setCmpyName(String cmpyName) {
    this.cmpyName = cmpyName;
  }

  public String getConfirmationNumber() {
    return confirmationNumber;
  }

  public void setConfirmationNumber(String confirmationNumber) {
    this.confirmationNumber = confirmationNumber;
  }

  public String getPickupLocation() {
    return pickupLocation;
  }

  public void setPickupLocation(String pickupLocation) {
    this.pickupLocation = pickupLocation;
  }

  public String getDropoffLocation() {
    return dropoffLocation;
  }

  public void setDropoffLocation(String dropoffLocation) {
    this.dropoffLocation = dropoffLocation;
  }

  public Timestamp getPickupDate() {
    return pickupDate;
  }

  public void setPickupDate(Timestamp pickupDate) {
    this.pickupDate = pickupDate;
  }

  public Timestamp getDropoffDate() {
    return dropoffDate;
  }

  public void setDropoffDate(Timestamp dropoffDate) {
    this.dropoffDate = dropoffDate;
  }

  public String toString() {
    String        nl         = System.getProperty("line.separator");
    StringBuilder strBuilder = new StringBuilder(">>> Transport (Value Object)" + nl);
    strBuilder.append("Type            :" + bookingType.name() + nl);
    strBuilder.append("Company         :" + cmpyName + nl);
    strBuilder.append("Confirmation#   :" + confirmationNumber + nl);
    strBuilder.append("Pickup  Location:" + pickupLocation + nl);
    strBuilder.append("Dropoff Location:" + dropoffLocation + nl);
    strBuilder.append("Pickup  Date    :" + pickupDate + nl);
    strBuilder.append("Dropoff Date    :" + dropoffDate + nl);
    strBuilder.append("Note            :" + nl + getFullNotes() + nl);
    return strBuilder.toString();
  }
}
