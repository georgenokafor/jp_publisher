
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class UpdatedAt{
   	private String dateUtc;

 	public String getDateUtc(){
		return this.dateUtc;
	}
	public void setDateUtc(String dateUtc){
		this.dateUtc = dateUtc;
	}
}
