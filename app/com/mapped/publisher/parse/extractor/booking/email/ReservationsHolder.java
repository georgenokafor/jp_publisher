package com.mapped.publisher.parse.extractor.booking.email;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.parse.schemaorg.*;
import com.mapped.publisher.utils.Log;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.StringUtils;
import org.owasp.html.Sanitizers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for all things Schema.org schema
 * Created by surge on 2015-05-12.
 */
public class ReservationsHolder {

  /**
   * Object mapper "trained" for Schema.org JSON
   */
  private static final ObjectMapper om;

  static {
    om = new ObjectMapper();
    //Perform any kind of configuration for object mapper in this area

    om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    //om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
    //om.enableDefaultTypingAsProperty(ObjectMapper.DefaultTyping.NON_FINAL, "type");
    om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
    om.registerSubtypes(new NamedType(LodgingReservation.class, "LodgingReservation"),
                        new NamedType(RentalCarReservation.class, "RentalCarReservation"),
                        new NamedType(FlightReservation.class, "FlightReservation"),
                        new NamedType(EventReservation.class, "EventReservation"),
                        new NamedType(BusReservation.class, "BusReservation"),
                        new NamedType(TaxiReservation.class, "TaxiReservation"),
                        new NamedType(ReservationPackage.class, "ReservationPackage"),
                        new NamedType(ActivityReservation.class, "ActivityReservation"),
                        new NamedType(CruiseShipReservation.class, "CruiseShipReservation"),
                        new NamedType(TrainReservation.class, "TrainReservation"),
                        new NamedType(FoodEstablishmentReservation.class, "FoodEstablishmentReservation"),
                        new NamedType(TransferReservation.class, "TransferReservation"),
                        new NamedType(Rate.class, "Rate"),
                        new NamedType(RoomType.class, "RoomType"),
                        new NamedType(FlightPassenger.class, "FlightPassenger"),
                        new NamedType(Reservation.class, "Reservation"),
                        new NamedType(Note.class, "Note"));
    om.enable(SerializationFeature.INDENT_OUTPUT); //Pretty-print for debugging
  }

  public List<FlightReservation> flights;
  public List<LodgingReservation> hotels;
  public List<EventReservation> events;
  public List<RentalCarReservation> cars;
  public List<ActivityReservation> activities;
  public List<CruiseShipReservation> cruises;
  public List<TrainReservation> trains;
  public List<FoodEstablishmentReservation> restaurants;
  public List<TransferReservation> transfers;
  public List<Note> notes;
  public ReservationPackage reservationPackage;

  public long startTimestamp = Long.MAX_VALUE;
  public long endTimestamp = Long.MIN_VALUE;
  public String keywords = null;

  public Map<String, String> travelers = new HashMap<>();

  public String src = null;
  public long srcPK = 0;

  public boolean includeReservationStatus = false;

  /**
   * Returns statically configured version of the object mapper.
   *
   * Be careful !!! none of the configuration parameters must ever be modified for this instance
   * @return
   */
  public static ObjectReader getObjectReader() {
    return om.readerFor(Reservation.class);
  }

  public boolean parseJson(String jsonText) {
    boolean result = true;
    try {
      JsonFactory f  = new JsonFactory();
      JsonParser  jp = f.createParser(jsonText);

      if (jp.nextToken() == JsonToken.START_OBJECT) {
        parseReservation(jp);
      }
      else {
        // and then each time, advance to opening START_OBJECT
        while (jp.nextToken() == JsonToken.START_OBJECT) {
          parseReservation(jp);
        }
      }
    } catch (Exception e) {
      Log.err("ReservationHolder: Error parsing JSON", e);
      result = false;
    }
    return result;
  }

  public void parseReservation(JsonParser jp) {
    ObjectReader or = getObjectReader();
    if (jp.getCurrentToken() != JsonToken.START_OBJECT) {
      Log.err("Wrong parser state: " + jp.getCurrentToken().name());
      return;
    }

    try {
      Object reservation = or.readValue(jp, Reservation.class);
      assignReservation(reservation);
    } catch (Exception e) {
      Log.err("ReservationHolder: Can't de-serialize: ", e);
    }
  }

  public int count() {
    return flights.size() + hotels.size() + events.size() + cars.size() + activities.size() + cruises.size() + trains.size() + restaurants.size() + transfers.size() + notes.size();
  }

  public boolean hasValidReservations() {
    int count = 0;
    StringBuilder sb = new StringBuilder();

    for(FlightReservation r: flights) {
      if(r.reservationFor != null &&
         (r.reservationFor.flightNumber != null || r.reservationNumber != null)) {
        ++count;

        if (r.underName != null && r.underName.email != null) {
          StringBuilder sb1 = new StringBuilder();
          if (r.underName != null) {
            if (r.underName.honorificPrefix != null) {
              sb1.append(r.underName.honorificPrefix);
              sb1.append(" ");
            }
            if (r.underName.givenName != null) {
              sb1.append(r.underName.givenName);
              sb1.append(" ");
            }
            if (r.underName.familyName != null) {
              sb1.append(r.underName.familyName);
              sb1.append(" ");
            }
          }
          travelers.put(r.underName.email, sb1.toString());
        }

        //get overall trip date
        if (r.reservationFor.departureTime != null && r.reservationFor.departureTime.value != null ) {
          Timestamp ts = r.reservationFor.departureTime.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }

        if (r.reservationFor.arrivalTime != null && r.reservationFor.arrivalTime.value != null ) {
          Timestamp ts = r.reservationFor.arrivalTime.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }
      }
    }

    for(LodgingReservation r: hotels) {
      if((r.reservationFor != null && r.reservationFor.name != null) || r.name != null) {
        ++count;

        if (r.checkinDate != null && r.checkinDate.value != null) {
          Timestamp ts = r.checkinDate.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }

        if (r.checkoutDate != null && r.checkoutDate.value != null) {
          Timestamp ts = r.checkoutDate.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }
      }
    }


    for(EventReservation r: events) {
      if((r.reservationFor != null && r.reservationFor.name != null) || r.name != null || r.description != null) {
        ++count;


        //process keywords
        appendKeyword(r.name, sb);
        appendKeyword(r.reservationNumber, sb);
        if (r.underName != null) {
          appendKeyword(r.underName.givenName, sb);
          appendKeyword(r.underName.familyName, sb);
        }

        if (r.reservationFor.startDate != null && r.reservationFor.startDate.value != null) {
          Timestamp ts = r.reservationFor.startDate.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }
      }

      if (r.reservationFor.endDate != null && r.reservationFor.endDate.value != null ) {
        Timestamp ts = r.reservationFor.endDate.getTimestamp();
        if (ts != null) {
          long tsl = ts.getTime();
          if (tsl < startTimestamp) {
            startTimestamp = tsl;
          }
          if (tsl > endTimestamp) {
            endTimestamp = tsl;
          }
        }
      }
    }

    for(RentalCarReservation r: cars) {
      if((r.reservationFor != null && r.reservationFor.name != null) || r.name != null) {
        ++count;


        if (r.pickupTime != null && r.pickupTime.value != null) {
          Timestamp ts = r.pickupTime.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }

        if (r.dropoffTime != null && r.dropoffTime.value != null) {
          Timestamp ts = r.dropoffTime.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }
      }
    }

    for(ActivityReservation r: activities) {
      if((r.reservationFor != null && r.reservationFor.name != null) || r.name != null || r.description != null) {
        ++count;


        if (r.startTime != null && r.startTime.value != null) {
          Timestamp ts = r.startTime.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }

        if (r.finishTime != null && r.finishTime.value != null) {
          Timestamp ts = r.finishTime.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }
      }
    }

    for(CruiseShipReservation r: cruises) {
      if((r.reservationFor != null && r.reservationFor.name != null) || r.name != null) {
        ++count;


        if (r.reservationFor.departTime != null && r.reservationFor.departTime.value != null) {
          Timestamp ts = r.reservationFor.departTime.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }

        if (r.reservationFor.arrivalTime != null && r.reservationFor.arrivalTime.value != null) {
          Timestamp ts = r.reservationFor.arrivalTime.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }
      }
    }

    for(TrainReservation r: trains) {
      if((r.reservationFor != null && r.reservationFor.name != null) || r.name != null) {
        ++count;


        if (r.reservationFor.departTime != null && r.reservationFor.departTime.value != null) {
          Timestamp ts = r.reservationFor.departTime.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }

        if (r.reservationFor.arrivalTime != null && r.reservationFor.arrivalTime.value != null) {
          Timestamp ts = r.reservationFor.arrivalTime.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }
      }
    }

    for(FoodEstablishmentReservation r: restaurants) {
      if((r.reservationFor != null && r.reservationFor.name != null) || r.name != null || r.description != null) {
        ++count;


        if (r.startTime != null && r.startTime.value != null) {
          Timestamp ts = r.startTime.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }

        if (r.endTime != null && r.endTime.value != null) {
          Timestamp ts = r.endTime.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }
      }
    }

    for(TransferReservation r: transfers) {
      if((r.reservationFor != null && r.reservationFor.name != null) || r.name != null) {
        ++count;


        if (r.reservationFor.pickupTime != null && r.reservationFor.pickupTime.value != null) {
          Timestamp ts = r.reservationFor.pickupTime.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }

        if (r.reservationFor.dropoffTime != null && r.reservationFor.dropoffTime.value != null) {
          Timestamp ts = r.reservationFor.dropoffTime.getTimestamp();
          if (ts != null) {
            long tsl = ts.getTime();
            if (tsl < startTimestamp) {
              startTimestamp = tsl;
            }
            if (tsl > endTimestamp) {
              endTimestamp = tsl;
            }
          }
        }
      }
    }

    for(Note r: notes) {
      if(r.name != null || r.description != null) {
        ++count;
      }


    }

    keywords = sb.toString();

    return count > 0;
  }

  public String getKeywords (FlightReservation r) {
    StringBuilder sb = new StringBuilder();
    //process keywords
    appendKeyword(r.name, sb);
    appendKeyword(r.reservationNumber, sb);
    if (r.underName != null) {
      appendKeyword(r.underName.givenName, sb);
      appendKeyword(r.underName.familyName, sb);
    }

    if (r.reservationFor != null) {
      if (r.reservationFor.airline != null) {
        appendKeyword(r.reservationFor.airline.iataCode, sb);
        appendKeyword(r.reservationFor.airline.name, sb);
      }
      if (r.reservationFor.departureAirport != null) {
        appendKeyword(r.reservationFor.departureAirport.iataCode, sb);
        appendKeyword(r.reservationFor.departureAirport.name, sb);
      }
      if (r.reservationFor.arrivalAirport != null) {
        appendKeyword(r.reservationFor.arrivalAirport.iataCode, sb);
        appendKeyword(r.reservationFor.arrivalAirport.name, sb);
      }
    }

    if (reservationPackage != null) {
      if (reservationPackage.name  != null) {
        appendKeyword(reservationPackage.name, sb);
      }
      if (reservationPackage.underName != null) {
        appendKeyword(reservationPackage.underName.givenName, sb);
        appendKeyword(reservationPackage.underName.familyName, sb);
      }
      if (reservationPackage.reservationNumber != null) {
        appendKeyword(reservationPackage.reservationNumber, sb);
      }
    }

    return sb.toString();
  }

  public String getKeywords (Reservation r) {
    StringBuilder sb = new StringBuilder();
    //process keywords
    appendKeyword(r.name, sb);
    appendKeyword(r.reservationNumber, sb);
    if (r.underName != null) {
      appendKeyword(r.underName.givenName, sb);
      appendKeyword(r.underName.familyName, sb);
    }
    if (reservationPackage != null) {
      if (reservationPackage.name != null) {
        appendKeyword(reservationPackage.name, sb);
      }
      if (reservationPackage.underName != null) {
        appendKeyword(reservationPackage.underName.givenName, sb);
        appendKeyword(reservationPackage.underName.familyName, sb);
      }
      if (reservationPackage.reservationNumber != null) {
        appendKeyword(reservationPackage.reservationNumber, sb);
      }
    }
    return sb.toString();
  }

  public String getKeywords (CruiseShipReservation r) {
    StringBuilder sb = new StringBuilder();
    //process keywords
    appendKeyword(r.name, sb);
    appendKeyword(r.reservationNumber, sb);
    if (r.underName != null) {
      appendKeyword(r.underName.givenName, sb);
      appendKeyword(r.underName.familyName, sb);
    }
    if (r.reservationFor != null) {
      if (r.reservationFor.departPort != null) {
        appendKeyword(r.reservationFor.departPort.name, sb);
      }
      if (r.reservationFor.arrivalPort != null) {
        appendKeyword(r.reservationFor.arrivalPort.name, sb);
      }
    }

    if (reservationPackage != null) {
      if (reservationPackage.name  != null) {
        appendKeyword(reservationPackage.name, sb);
      }
      if (reservationPackage.underName != null) {
        appendKeyword(reservationPackage.underName.givenName, sb);
        appendKeyword(reservationPackage.underName.familyName, sb);
      }
      if (reservationPackage.reservationNumber != null) {
        appendKeyword(reservationPackage.reservationNumber, sb);
      }
    }
    return sb.toString();
  }

  public String getKeywords (TrainReservation r) {
    StringBuilder sb = new StringBuilder();
    //process keywords
    appendKeyword(r.name, sb);
    appendKeyword(r.reservationNumber, sb);
    if (r.underName != null) {
      appendKeyword(r.underName.givenName, sb);
      appendKeyword(r.underName.familyName, sb);
    }
    if (r.reservationFor != null) {
      if (r.reservationFor.departStation != null) {
        appendKeyword(r.reservationFor.departStation.name, sb);
      }
      if (r.reservationFor.arrivalStation != null) {
        appendKeyword(r.reservationFor.arrivalStation.name, sb);
      }
    }

    if (reservationPackage != null) {
      if (reservationPackage.name  != null) {
        appendKeyword(reservationPackage.name, sb);
      }
      if (reservationPackage.underName != null) {
        appendKeyword(reservationPackage.underName.givenName, sb);
        appendKeyword(reservationPackage.underName.familyName, sb);
      }
      if (reservationPackage.reservationNumber != null) {
        appendKeyword(reservationPackage.reservationNumber, sb);
      }

    }
    return sb.toString();
  }

  public String getKeywords (RentalCarReservation r) {
    StringBuilder sb = new StringBuilder();
    //process keywords
    appendKeyword(r.name, sb);
    appendKeyword(r.reservationNumber, sb);
    if (r.underName != null) {
      appendKeyword(r.underName.givenName, sb);
      appendKeyword(r.underName.familyName, sb);
    }
    if (r.reservationFor != null) {
      if (r.reservationFor.rentalCompany != null) {
        appendKeyword(r.reservationFor.rentalCompany.name, sb);
      }
      appendKeyword(r.reservationFor.model, sb);
    }

    if (reservationPackage != null) {
      if (reservationPackage.name  != null) {
        appendKeyword(reservationPackage.name, sb);
      }
      if (reservationPackage.underName != null) {
        appendKeyword(reservationPackage.underName.givenName, sb);
        appendKeyword(reservationPackage.underName.familyName, sb);
      }
      if (reservationPackage.reservationNumber != null) {
        appendKeyword(reservationPackage.reservationNumber, sb);
      }

    }
    return sb.toString();
  }

  private void appendKeyword (String s, StringBuilder sb) {
    if (s != null && s.trim().length() > 0 && !sb.toString().contains(s)) {
      sb.append(s);
      sb.append(" ");
    }
  }

  // private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

  public ReservationsHolder() {
    flights = new ArrayList<>(1);
    events = new ArrayList<>(1);
    hotels = new ArrayList<>(1);
    cars = new ArrayList<>(1);
    activities = new ArrayList<>(1);
    cruises = new ArrayList<>(1);
    trains = new ArrayList<>(1);
    restaurants = new ArrayList<>(1);
    transfers = new ArrayList<>(1);
    notes = new ArrayList<>(1);
  }

  public ReservationsHolder(String srcId, long srcPK) {
    this(); //Calling 'default' constructor
    this.src = srcId;
    this.srcPK = srcPK;
  }

  public String toString() {
    String nl = System.getProperty("line.separator");
    StringBuilder sb = new StringBuilder();

    sb.append("Flights  : ");
    sb.append(flights.size());
    sb.append(nl);

    sb.append("Hotels   : ");
    sb.append(hotels.size());
    sb.append(nl);
    sb.append("Cars     : ");
    sb.append(cars.size());
    sb.append(nl);
    sb.append("Events   : ");
    sb.append(events.size());
    sb.append(nl);
    sb.append("Activities   : ");
    sb.append(activities.size());
    sb.append(nl);
    sb.append("Cruises   : ");
    sb.append(cruises.size());
    sb.append(nl);
    sb.append("Trains   : ");
    sb.append(trains.size());
    sb.append(nl);
    sb.append("Restaurants   : ");
    sb.append(restaurants.size());
    sb.append(nl);
    sb.append("Transfers   : ");
    sb.append(transfers.size());
    sb.append(nl);
    sb.append("Notes   : ");
    sb.append(notes.size());
    sb.append(nl);

    return sb.toString();
  }

  public void updateBase(Reservation from, ReservationVO bvo) {
    bvo.setId(from.umId);
    StringBuilder sb = new StringBuilder();
    if (includeReservationStatus && from.reservationStatus != null && !from.reservationStatus.trim().isEmpty()) {
      sb.append("Reservation Status: ");
      sb.append(from.reservationStatus);
      sb.append("\n\n");
    }
    if (from.description != null) {
      sb.append(from.description);
    }
    if (from != null && from.remarksAndSpecialRequests != null && !from.remarksAndSpecialRequests.isEmpty()) {
      sb.append("\n\nRemarks and Special Requests: ");
      sb.append(from.remarksAndSpecialRequests);
    }
    bvo.setNote(sb.toString());
    bvo.setSrc(src);
    bvo.setSrcId(srcPK);
    bvo.setOrigSrcPK(from.reservationId);
    if (from.reservationId != null && !from.reservationId.isEmpty()) {
      bvo.setRecordLocator(from.reservationId);
    } else {
      bvo.setRecordLocator(from.reservationNumber);
    }
    bvo.setName(from.name);
    //let's update price
    bvo.setSubtotal(from.subtotal);
    bvo.setTaxes(from.taxes);
    bvo.setCurrency(from.currency);
    bvo.setFees(from.fees);
    bvo.setTotal(from.total);

    //let's update rates
    if (from.rates != null && !from.rates.isEmpty()) {
      List<ReservationVO.Rate> rates = new ArrayList<>();
      for (Reservation r: from.rates) {
        ReservationVO.Rate rate = new ReservationVO.Rate();
        rate.setDescription(r.rateDescription);
        rate.setDisplayName(r.rateType);
        rate.setPrice(r.ratePrice);
        rates.add(rate);
      }
      if (rates.size() > 0) {
        bvo.setRates(rates);
      }
    }
    bvo.setCancellationPolicy(from.cancellationPolicy);
    if (from.alternateName != null && from.alternateName.trim().length() > 0) {
      bvo.setServiceType(from.alternateName);
    }


  }

  public NoteVO note2VO(TripVO tvo, Note note) {
    NoteVO noteVO = new NoteVO();
    noteVO.setSrcId(srcPK);
    noteVO.setSrc(src);
    noteVO.setOrigSrcPK(note.reservationId);
    noteVO.setPlace(note.reservationFor);
    noteVO.setId(note.umId);
    if (note.name == null) {
      noteVO.setName(" " + note.reservationId);
    }
    else {
      noteVO.setName(note.name);
    }
    if (note.position != null && note.position > 0) {
      noteVO.setRank(note.position);
    }
    if (note.noteTimestamp != null && note.noteTimestamp.getTimestamp() != null) {
      noteVO.setTimestamp(note.noteTimestamp.getTimestamp());
    }
    if (note.description != null) {
      noteVO.setNote(Sanitizers.FORMATTING.and(Sanitizers.LINKS)
                                          .and(Sanitizers.STYLES)
                                          .and(Sanitizers.BLOCKS)
                                          .and(Sanitizers.TABLES)
                                          .sanitize(note.description.replaceAll("\n", "<br/>")));
    }
    if (note.image != null) {
      noteVO.setImages(note.image);
    }
    if (note.potentialAction != null && note.potentialAction.contains(APPConstants.NOTE_TAG_LAST_ITEM)) {
      noteVO.setTag(APPConstants.NOTE_TAG_LAST_ITEM);
    }
    tvo.addNoteVO(noteVO);
    return noteVO;
  }

  public ActivityVO event2VO(TripVO tvo, EventReservation event) {
    ActivityVO avo = new ActivityVO();
    updateBase(event, avo);
    avo.setConfirmation(event.reservationNumber);
    avo.setBookingType(ReservationType.EVENT);

    if (event.name != null && !event.name.isEmpty()) {
      avo.setName(event.name);
    } else {
      avo.setName("Event");
    }

    if(event.underName != null) {
      avo.addProperty("Booking For", event.underName.name);
    }

    if (event.reservationFor != null) {
      if (event.reservationFor.startDate != null) {
        avo.setStartDate(event.reservationFor.startDate.getTimestamp());
      }
      if (event.reservationFor.endDate != null) {
        avo.setEndDate(event.reservationFor.endDate.getTimestamp());
      }
      if (event.reservationFor.location != null) {
        avo.addProperty("Venue", event.reservationFor.location.name);
        if (event.reservationFor.location.faxNumber != null && event.reservationFor.location.faxNumber.trim().length() > 0) {
          avo.addProperty("Fax Number", event.reservationFor.location.faxNumber);
        }

        if (event.reservationFor.location.telephone != null && event.reservationFor.location.telephone.trim().length() > 0) {
          avo.addProperty("Phone", event.reservationFor.location.telephone);
        }

        if (event.reservationFor.location != null && event.reservationFor.location.url != null) {
          avo.addProperty("Website", event.reservationFor.location.url);
        }
        if (event.reservationFor.location.address != null) {
          avo.addProperty("Address", event.reservationFor.location.address.toString());
          if (event.reservationFor.location.name != null) {
            avo.setEndLocation(event.reservationFor.location.address.toString());
          }

        }
        if (event.reservationFor.location.name != null) {
          avo.setEndLocation(event.reservationFor.location.name);
        }
      }

      if (event.reservationFor.performer != null) {
        avo.addProperty("Performer", event.reservationFor.performer.name);
      }
    }
    avo.addProperty("Ticket Number", event.ticketNumber);
    if (event.numSeats != null) {
      avo.addProperty("Number of Seats", event.numSeats.toString());
    }
    avo.addProperty("Seat Number", event.venueSeat);
    avo.addProperty("Seat Row", event.venueRow);
    avo.addProperty("Seat Section", event.venueSection);
    addPrice(avo, event);

    tvo.addActivity(avo);
    return avo;
  }

  public ActivityVO activity2VO(TripVO tvo, ActivityReservation activity) {
    ActivityVO avo = new ActivityVO();
    updateBase(activity, avo);
    avo.setConfirmation(activity.reservationNumber);

    if (activity.name != null && !activity.name.isEmpty()) {
      avo.setName(activity.name);
    } else {
      avo.setName("Activity");
    }

    avo.setBookingType(ReservationType.ACTIVITY); //TODO: Not clear how to determine correct event type yet
    if (activity.reservationFor != null && activity.reservationFor.activityType != null) {
      //set the activity subtype based on the name
      String activityType = activity.reservationFor.activityType.toLowerCase();
      switch (activityType) {
        case "liftticket":
          avo.setBookingType(ReservationType.SKI_LIFT);
          break;
        case "equipmentrental":
          avo.setBookingType(ReservationType.SKI_RENTALS);
          break;
        case "skilesson":
          avo.setBookingType(ReservationType.SKI_LESSONS);
          break;
        case "tour":
          avo.setBookingType(ReservationType.TOUR);
          break;
        case "shoreexcursion":
          avo.setBookingType(ReservationType.SHORE_EXCURSION);
          break;
        case "cruise_stop":
        case "cruisestop":
          avo.setBookingType(ReservationType.CRUISE_STOP);
          break;
      }
    }

    if(activity.underName != null) {
      avo.addProperty("Booking For", activity.underName.name);
    }
    if (activity.startTime != null) {
      avo.setStartDate(activity.startTime.getTimestamp());
    }
    if (activity.finishTime != null) {
      avo.setEndDate(activity.finishTime.getTimestamp());
    }
    if (activity.startLocation != null) {
      if (activity.startLocation.name != null) {
        avo.setStartLocation(activity.startLocation.name);
      } else if (activity.startLocation.address != null){
        avo.setStartLocation(activity.startLocation.address.toString());
      }
      if (activity.startLocation.address != null) {
        avo.addProperty("Start", activity.startLocation.address.toString());
      }
    }

    if (activity.finishLocation != null) {
      if (activity.finishLocation.name != null) {
        avo.setEndLocation(activity.finishLocation.name);
      } else if (activity.finishLocation.address != null){
        avo.setEndLocation(activity.finishLocation.address.toString());
      }
      if (activity.startLocation.address != null) {
        avo.addProperty("Finish", activity.startLocation.address.toString());
      }
    }


    if (activity.provider != null) {
      if (activity.provider.name != null) {
        avo.addProperty("Organizer", activity.provider.name);
        avo.setContact(activity.provider.name);
      } else if(activity.reservationFor != null && activity.reservationFor.organizedBy != null && activity.reservationFor.organizedBy.name != null) {
        avo.addProperty("Organizer", activity.reservationFor.organizedBy.name);
        avo.setContact(activity.reservationFor.organizedBy.name);
      }

      if (activity.provider.address != null) {
        avo.addProperty("Address", activity.provider.address.toString());

      }

      if (activity.provider.faxNumber != null && activity.provider.faxNumber.trim().length() > 0) {
        avo.addProperty("Fax Number", activity.provider.faxNumber);
      }

      if (activity.provider.telephone != null && activity.provider.telephone.trim().length() > 0) {
        avo.addProperty("Phone", activity.provider.telephone);
      }

      if (activity.provider.location != null && activity.provider.location.url != null) {
        avo.addProperty("Website", activity.provider.location.url);
      }
    }
    if (activity.reservationFor != null && activity.reservationFor.organizedBy != null  && activity.reservationFor.organizedBy.name != null) {
      avo.addProperty("Organizer", activity.reservationFor.organizedBy.name);
    }
    if (activity.reservationFor != null && activity.reservationFor.url != null) {
      avo.addProperty("Website", activity.reservationFor.url);
    }
    addPrice(avo, activity);

    tvo.addActivity(avo);
    return avo;
  }

  public ActivityVO restaurant2VO(TripVO tvo, FoodEstablishmentReservation event) {
    ActivityVO avo = new ActivityVO();
    updateBase(event, avo);
    avo.setBookingType(ReservationType.RESTAURANT);

    if (event.name != null && !event.name.isEmpty()) {
      avo.setName(event.name);
    } else {
      avo.setName("Restaurant");
    }
    avo.setConfirmation(event.reservationNumber);


    if(event.underName != null) {
      avo.addProperty("Booking For", event.underName.name);
    }

    if (event.startTime != null) {
      avo.setStartDate(event.startTime.getTimestamp());
    }

    if(event.provider != null && event.provider.name != null && event.provider.name.length() > 0) {
      avo.setContact(event.provider.name);
    }

    if (event.reservationFor != null) {
      if (event.reservationFor.name != null) {
        avo.setStartLocation(event.reservationFor.name);
      } else if (event.reservationFor.address != null){
        avo.setStartLocation(event.reservationFor.address.toString());
      }
      if (event.reservationFor.address != null && !event.reservationFor.address.toString().isEmpty()) {
        avo.addProperty("Start", event.reservationFor.address.toStringSingleLine());
      }
      if (event.reservationFor.url != null) {
        avo.addProperty("Website", event.reservationFor.url);
      }
      if (event.reservationFor.faxNumber != null && event.reservationFor.faxNumber.trim().length() > 0) {
        avo.addProperty("Fax Number", event.reservationFor.faxNumber);
      }

      if (event.reservationFor.telephone != null && event.reservationFor.telephone.trim().length() > 0) {
        avo.addProperty("Phone", event.reservationFor.telephone);
      }
    }

    if(event.location != null) {
      if (event.location.name != null) {
        avo.setStartLocation(event.location.name);
      } else if (event.location.address != null && !event.location.address.toString().isEmpty()){
        avo.addProperty("Start", event.location.address.toStringSingleLine());
      }
      if (event.location.faxNumber != null && event.location.faxNumber.trim().length() > 0) {
        avo.addProperty("Fax Number", event.location.faxNumber);
      }

      if (event.location.telephone != null && event.location.telephone.trim().length() > 0) {
        avo.addProperty("Phone", event.location.telephone);
      }

      if (event.location != null && event.location.url != null) {
        avo.addProperty("Website", event.location.url);
      }
    }

    if(event.partySize > 0) {
      avo.addProperty("Party Size", String.valueOf(event.partySize));
    }
    addPrice(avo, event);

    tvo.addActivity(avo);
    return avo;
  }

  public TransportVO train2Vo(TripVO tvo, TrainReservation train) {
    TransportVO trvo = new TransportVO();
    updateBase(train, trvo);
    trvo.setBookingType(ReservationType.RAIL);

    trvo.setConfirmationNumber(train.reservationNumber);
    if (train.name != null && !train.name.isEmpty()) {
      trvo.setName(train.name);
    } else {
      trvo.setName("Train");
    }
    if(train.underName != null) {
      trvo.addProperty("Booking For", train.underName.name);
    }

    if (train.reservationFor != null) {
      if (train.reservationFor.trainName != null && train.reservationFor.trainNumber != null) {
        trvo.setName(train.reservationFor.trainName + " " + train.reservationFor.trainNumber);
      } else if(train.reservationFor.name != null && train.reservationFor.name.length() > 0) {
        trvo.setName(train.reservationFor.name);
      }

      if (train.reservationFor.provider != null && train.reservationFor.provider.name != null) {
        trvo.setCmpyName(train.reservationFor.provider.name);
      }

      if (train.reservationFor.departStation != null &&  train.reservationFor.departStation.name != null) {
        trvo.setPickupLocation(train.reservationFor.departStation.name);
        if (train.reservationFor.departStation.address != null &&
            train.reservationFor.departStation.address.toString().length() > 0) {
          trvo.addProperty("Departs", train.reservationFor.departStation.name + "\n" + train.reservationFor.departStation.address.toString());
        }
      }
      if (train.reservationFor.departPlatform != null) {
        trvo.addProperty("Depart Platform", train.reservationFor.departPlatform);
      }
      if (train.reservationFor.arrivalStation != null && train.reservationFor.arrivalStation.name != null) {
        trvo.setDropoffLocation(train.reservationFor.arrivalStation.name);
        if (train.reservationFor.arrivalStation.address != null &&
            train.reservationFor.arrivalStation.address.toString().length() > 0) {
          trvo.addProperty("Arrives", train.reservationFor.arrivalStation.name + "\n" + train.reservationFor.arrivalStation.address.toString());
        }
      }
      if (train.reservationFor.arrivalPlatform != null) {
        trvo.addProperty("Arrival Platform", train.reservationFor.arrivalPlatform);
      }

      if (train.reservationFor.departTime != null) {
        trvo.setPickupDate(train.reservationFor.departTime.getTimestamp());
      }

      if (train.reservationFor.arrivalTime != null) {
        trvo.setDropoffDate(train.reservationFor.arrivalTime.getTimestamp());
      }
    }
    if (train.programMembership != null) {
      trvo.addProperty("Membership Program", train.programMembership.program);
      trvo.addProperty("Membership Program", train.programMembership.programName);
      trvo.addProperty("Membership Number", train.programMembership.membershipNumber);
    }

    if(train.ticketNumber != null) {
      trvo.addProperty("Ticket", train.ticketNumber);
    }
    addPrice(trvo, train);

    tvo.addTransport(trvo);
    return trvo;
  }

  public TransportVO transfer2Vo(TripVO tvo, TransferReservation transfer) {
    TransportVO trvo = new TransportVO();
    updateBase(transfer, trvo);

    if (transfer.name != null && !transfer.name.isEmpty()) {
      trvo.setName(transfer.name);
    } else {
      trvo.setName("Transport");
    }

    trvo.setConfirmationNumber(transfer.reservationNumber);

    if(transfer.underName != null) {
      trvo.addProperty("Booking For", transfer.underName.name);
    }

    if (transfer.reservationFor != null) {
      if (transfer.reservationFor.provider != null) {
        trvo.setCmpyName(transfer.reservationFor.provider.name);
        trvo.addProperty("Company", transfer.reservationFor.provider.name);
      }

      if (transfer.reservationFor.pickupLocation != null) {
        if (transfer.reservationFor.pickupLocation.address != null) {
          trvo.setPickupLocation(transfer.reservationFor.pickupLocation.address.toString());
          trvo.addProperty("Pickup", transfer.reservationFor.pickupLocation.name + "\n" +
                                     transfer.reservationFor.pickupLocation.address.toString());
        }
        if (transfer.reservationFor.pickupLocation.name != null) {
          trvo.setPickupLocation(transfer.reservationFor.pickupLocation.name);
        }
      }

      if (transfer.reservationFor.dropoffLocation != null) {
        if (transfer.reservationFor.dropoffLocation.address != null) {
          trvo.setDropoffLocation(transfer.reservationFor.dropoffLocation.address.toString());

          trvo.addProperty("Drop off",
                           transfer.reservationFor.dropoffLocation.name + "\n" +
                           transfer.reservationFor.dropoffLocation.address.toString());
        }
        if (transfer.reservationFor.dropoffLocation.name != null) {
          trvo.setDropoffLocation(transfer.reservationFor.dropoffLocation.name);
        }
      }

      if (transfer.reservationFor.pickupTime != null) {
        trvo.setPickupDate(transfer.reservationFor.pickupTime.getTimestamp());
      }

      if (transfer.reservationFor.dropoffTime != null) {
        trvo.setDropoffDate(transfer.reservationFor.dropoffTime.getTimestamp());
      }

      if (transfer.reservationFor.url != null) {
        trvo.addProperty("Website", transfer.reservationFor.url);
      }
    }

    if (transfer.programMembership != null) {
      trvo.addProperty("Membership Program", transfer.programMembership.program);
      trvo.addProperty("Membership Program", transfer.programMembership.programName);
      trvo.addProperty("Membership Number", transfer.programMembership.membershipNumber);
    }

    if(transfer.ticketNumber != null) {
      trvo.addProperty("Ticket", transfer.ticketNumber);
    }
    addPrice(trvo, transfer);

    tvo.addTransport(trvo);
    return trvo;
  }

  public CruiseVO cruise2Vo(TripVO tvo, CruiseShipReservation cruise) {
    CruiseVO trvo = new CruiseVO();
    updateBase(cruise, trvo);
    trvo.setConfirmationNo(cruise.reservationNumber);

    if (cruise.name != null && !cruise.name.isEmpty()) {
      trvo.setName(cruise.name);
    } else {
      trvo.setName("Cruise");
    }

    if(cruise.underName != null) {
      trvo.addProperty("Booking For", cruise.underName.name);
    }

    if (cruise.reservationFor != null) {
      if (cruise.reservationFor.name != null) {
        trvo.setName(cruise.reservationFor.name);
      }
      if (cruise.reservationFor.provider != null) {
        trvo.setCompanyName(cruise.reservationFor.provider.name);
      }

      if (cruise.reservationFor.departPort != null) {
        trvo.setPortDepart(cruise.reservationFor.departPort.name);
        if (cruise.reservationFor.departPort.address != null) {
          trvo.addProperty("Departs", cruise.reservationFor.departPort.name + "\n" + cruise.reservationFor.departPort.address.toString());
        }
      }
      if (cruise.reservationFor.departPier != null) {
        trvo.addProperty("Depart Pier", cruise.reservationFor.departPier);
      }
      if (cruise.reservationFor.arrivalPort != null) {
        trvo.setPortArrive(cruise.reservationFor.arrivalPort.name);
        if (cruise.reservationFor.arrivalPort.address != null) {
          trvo.addProperty("Arrives", cruise.reservationFor.arrivalPort.name + "\n" + cruise.reservationFor.arrivalPort.address.toString());
        }
      }
      if (cruise.reservationFor.arrivalPier != null) {
        trvo.addProperty("Arrival Pier", cruise.reservationFor.arrivalPier);
      }

      if (cruise.reservationFor.departTime != null) {
        trvo.setTimestampDepart(cruise.reservationFor.departTime.getTimestamp());
      }

      if (cruise.reservationFor.arrivalTime != null) {
        trvo.setTimestampArrive(cruise.reservationFor.arrivalTime.getTimestamp());
      }
      if (cruise.reservationFor.url != null) {
        trvo.addProperty("Website", cruise.reservationFor.url);
      }
    }
    if (cruise.cabinNumber != null) {
      trvo.setCabinNumber(cruise.cabinNumber);
    }
    if (cruise.cabinType != null) {
      trvo.setBedding(cruise.cabinType);
    }
    if (cruise.category != null) {
      trvo.setCategory(cruise.category);
    }
    if (cruise.deckNumber != null) {
      trvo.setDeck(cruise.deckNumber);
    }
    if (cruise.diningPlan != null) {
      trvo.setMeal(cruise.diningPlan);
    }
    if (cruise.amenities != null) {
      trvo.addProperty("Amenities", cruise.amenities);
    }

    if (cruise.programMembership != null) {
      trvo.addProperty("Membership Program", cruise.programMembership.program);
      trvo.addProperty("Membership Program", cruise.programMembership.programName);
      trvo.addProperty("Membership Number", cruise.programMembership.membershipNumber);
    }

    if(cruise.ticketNumber != null) {
      trvo.addProperty("Ticket", cruise.ticketNumber);
    }
    addPrice(trvo, cruise);

    tvo.addCruise(trvo);
    return trvo;
  }

  public AirportVO airport2VO(Airport a) {
    if (a == null) {
      return null;
    }

    AirportVO avo = new AirportVO();

    avo.setId(a.umId);

    if (a.iataCode != null) {
      avo.setCode(a.iataCode);
    } else {
      avo.setCode(a.icaoCode);
    }

    avo.setName(a.name);
    if(a.address != null) {
      if (a.address.addressCountry != null) {
        avo.setCountry(a.address.addressCountry);
      }

      if (a.address.addressLocality != null) {
        avo.setCity(a.address.addressLocality);
      }
    }

    return avo;
  }

  public PassengerVO person2VO(Person p, ProgramMembership pm) {
    PassengerVO pvo = new PassengerVO();

    if (pm != null) {
      pvo.setFrequentFlyer(pm.membershipNumber);
    }

    boolean validObject = false;
    if (p.name != null) {
      pvo.setFullName(p.name);
      validObject = true;
    }

    if (p.givenName != null) {
      pvo.setFirstName(p.givenName);
      validObject = true;
    }
    if(p.familyName != null) {
      pvo.setLastName(p.familyName);
      validObject = true;
    }
    if (validObject) {
      return pvo;
    }
    return null;
  }

  public FlightVO flight2VO(TripVO tvo, FlightReservation flight) {
    //check to see if we already have a flight
    FlightVO fvo = null;
    boolean isNew = false;
    if (tvo.getFlights() != null && tvo.getFlights().size() > 0) {
      for (FlightVO f: tvo.getFlights()) {
        String reservationNumber = flight.reservationNumber;
        String airlineCode = flight.reservationFor.airline.iataCode;
        String flightNum = flight.reservationFor.flightNumber;
        Timestamp departureDate = flight.reservationFor.departureTime.getTimestamp();

        if (reservationNumber != null && airlineCode != null && flightNum != null && departureDate != null &&
            f.getReservationNumber() != null && f.getCode() != null && f.getNumber() != null &&
            f.getDepatureTime() != null) {
          if (f.getReservationNumber().equals(reservationNumber) && f.getCode().equals(airlineCode) &&
              f.getNumber().equals(flightNum) && f.getDepatureTime().equals(departureDate)) {
            fvo = f;
          }
        }
      }
    }

    if (fvo == null) {
      isNew = true;
      fvo = new FlightVO();
      updateBase(flight, fvo);
      if(flight.reservationId == null) {
        fvo.setRecordLocator(flight.reservationNumber);
      } else {
        fvo.setRecordLocator(flight.reservationId);
      }
      fvo.setReservationNumber(flight.reservationNumber);

      //this is a fix for an issue introduced in the Clientbase gateway when sometimes the wrong reservation id was used
      //since it is impossible or very hard to correct the already imported data, it is easier to hack this and place the correct reservation number in an unused field
      if (src != null && src.contains("CLIENTBASE") && flight.additionalTicketText != null && !flight.additionalTicketText.isEmpty()) {
        fvo.setReservationNumber(flight.additionalTicketText);
      }

      if (flight.reservationFor != null) {
        if (flight.reservationFor.airline != null) {
          fvo.setCode(flight.reservationFor.airline.iataCode);
          fvo.setAirlinePoiId(flight.reservationFor.airline.umId);
        }

        fvo.setNumber(flight.reservationFor.flightNumber);

        if (flight.reservationFor.arrivalTime != null) {
          fvo.setArrivalTime(flight.reservationFor.arrivalTime.getTimestamp());
        }

        if (flight.reservationFor.departureTime != null) {
          fvo.setDepatureTime(flight.reservationFor.departureTime.getTimestamp());
        }

        AirportVO davo = airport2VO(flight.reservationFor.departureAirport);
        AirportVO aavo = airport2VO(flight.reservationFor.arrivalAirport);

        if (flight.reservationFor.departureTerminal != null) {
          davo.setTerminal(flight.reservationFor.departureTerminal);
        }

        if (flight.reservationFor.arrivalTerminal != null) {
          aavo.setTerminal(flight.reservationFor.arrivalTerminal);
        }

        fvo.setDepartureAirport(davo);
        fvo.setArrivalAirport(aavo);

        fvo.addProperty("Seat", flight.airplaneSeat);
        if (flight.airplaneSeatClass != null) {
          fvo.addProperty("Class", flight.airplaneSeatClass.name);
        }

        fvo.setDepartureGate(flight.reservationFor.departureGate);
        fvo.setArrivalGate(flight.reservationFor.arrivalGate);

        if (flight.reservationFor.airline != null && flight.reservationFor.airline.name != null) {
          fvo.setAirline(flight.reservationFor.airline.name);
        }
        if (flight.reservationFor.operatedBy != null && flight.reservationFor.operatedBy.name != null) {
          fvo.setOperatedBy(flight.reservationFor.operatedBy.name);
        }
        if (flight.reservationFor.aircraft != null && flight.reservationFor.aircraft.name != null) {
          fvo.setAirCraft(flight.reservationFor.aircraft.name);
        }
      }

      boolean passengerFound = false;
      if (flight.passengers != null && flight.passengers.size() > 0) {
        for (FlightPassenger fp: flight.passengers) {
          if (fp.underName != null && fp.underName.familyName != null && !fp.underName.familyName.trim().isEmpty()) {
            PassengerVO pvo = person2VO(fp.underName, fp.programMembership);
            if (fp.seat != null) {
              pvo.setSeat(fp.seat);
            } else {
              pvo.setSeat(fp.airplaneSeat);
            }
            pvo.seteTicketNumber(fp.ticketNumber);

            if (fp.airplaneSeatClass != null) {
              pvo.setSeatClass(fp.airplaneSeatClass.name);
            }
            if (fp.meal != null) {
              pvo.setMeal(fp.meal);
            }
            tvo.addPassenger(pvo);
            fvo.addPassenger(pvo);
          }
        }
      } else {
        addFlightPassenger(fvo, flight, tvo);
      }
    } else {
      addFlightPassenger(fvo, flight, tvo);
    }
    addPrice(fvo, flight);

    if (isNew) {
      tvo.addFlight(fvo);
    }
    return fvo;
  }

  public void addFlightPassenger (FlightVO fvo, FlightReservation flight, TripVO tvo) {
    boolean passengerFound = false;
    if (flight.underName != null) {
      PassengerVO pvo = person2VO(flight.underName, flight.programMembership);
      if (pvo != null && !(fvo.getNote() != null && fvo.getNote().contains(pvo.getFirstName()) && fvo.getNote().contains(pvo.getLastName()))) {
        if (flight.seat != null) {
          pvo.setSeat(flight.seat);
        } else {
          pvo.setSeat(flight.airplaneSeat);
        }
        pvo.seteTicketNumber(flight.ticketNumber);

        if (flight.airplaneSeatClass != null) {
          pvo.setSeatClass(flight.airplaneSeatClass.name);
        }
        tvo.addPassenger(pvo);
        fvo.addPassenger(pvo);
        passengerFound = true;
      }
    }
    if (!passengerFound) {
      if (flight.seat != null) {
        fvo.addProperty("Seat", flight.seat);
      } else {
        fvo.addProperty("Seat", flight.airplaneSeat);
      }
      fvo.addProperty("eTicket", flight.ticketNumber);

      if (flight.airplaneSeatClass != null) {
        fvo.addProperty("Class", flight.airplaneSeatClass.name);
      }
    }
  }

  public HotelVO lodging2VO(TripVO tvo, LodgingReservation hotel) {
    HotelVO hvo = new HotelVO(false);
    updateBase(hotel, hvo);
    hvo.setConfirmation(hotel.reservationNumber);

    // TODO: move to proper field when the front end support traveler information for hotel
    if(hotel.underName != null) {
      hvo.addProperty("Booking For", hotel.underName.name);
    }

    if (hotel.programMembership != null) {

      if (hotel.programMembership.membershipNumber != null) {
        hvo.setProgramMemberhsip(hotel.programMembership.program, hotel.programMembership.membershipNumber);
      }
    }


    if (hotel.reservationFor != null) {
      if (hotel.reservationFor.umId != null && hotel.reservationFor.umId.trim().length() > 0) {
        hvo.setTag(hotel.reservationFor.umId);
      }
      hvo.setHotelName(hotel.reservationFor.name);

      if (hotel.reservationFor.address != null && !hotel.reservationFor.address.toStringSingleLine().isEmpty()) {
        PostalAddress hoteAddress = hotel.reservationFor.address;
        hvo.setAddress(hoteAddress.streetAddress, hoteAddress.addressLocality, hoteAddress.addressRegion, hoteAddress.addressCountry);
      }

      hvo.setPhone(StringUtils.trimToNull(hotel.reservationFor.telephone));
      hvo.setUrl(StringUtils.trimToNull(hotel.reservationFor.url));
      hvo.setFax(StringUtils.trimToNull(hotel.reservationFor.faxNumber));
    }


    /*
    PassengerVO pvo = person2VO(hotel.underName, null);
    if (pvo != null) {
      tvo.addPassenger(pvo);
    }
    */

    /**
     * Checkin and Checkout Date and Time names are specified differently in Google and schema.org references
     */
    if (hotel.checkinDate != null) {
      hvo.setCheckin(hotel.checkinDate.getTimestamp());
    }

    if(hotel.checkinTime != null) {
      hvo.setCheckin(hotel.checkinTime.getTimestamp());
    }

    if (hotel.checkoutDate != null) {
      hvo.setCheckout(hotel.checkoutDate.getTimestamp());
    }

    if(hotel.checkoutTime != null) {
      hvo.setCheckout(hotel.checkoutTime.getTimestamp());
    }
    addPrice(hvo, hotel);

    //add room types
    List<HotelVO.HotelTravelerVO> travelerVOS = new ArrayList<>();
    if (hotel.rooms != null && !hotel.rooms.isEmpty()) {
      for (LodgingReservation r: hotel.rooms) {
        HotelVO.HotelTravelerVO hotelTravelerVO = new HotelVO.HotelTravelerVO();
        hotelTravelerVO.amenities = r.amenities;
        hotelTravelerVO.bedding = r.bedding;
        hotelTravelerVO.roomType = r.lodgingUnitType;
        hotelTravelerVO.status = r.confirmStatus;
        if (r.programMembership != null && r.programMembership.membershipNumber != null) {
          hotelTravelerVO.membershipNumber = r.programMembership.membershipNumber;
        }
        if (r.underName != null) {
          hotelTravelerVO.firstName = r.underName.givenName;
          hotelTravelerVO.lastName = r.underName.familyName;
        } else if (hotel.underName != null){
          hotelTravelerVO.firstName = hotel.underName.givenName;
          hotelTravelerVO.lastName = hotel.underName.familyName;
        }
        travelerVOS.add(hotelTravelerVO);
      }
    } else if ((hotel.lodgingUnitType != null && !hotel.lodgingUnitType.trim().isEmpty()) || (hotel.programMembership != null &&  hotel.programMembership.membershipNumber != null && !hotel.programMembership.membershipNumber.trim().isEmpty())) {
      HotelVO.HotelTravelerVO hotelTravelerVO = new HotelVO.HotelTravelerVO();
      if (hotel.programMembership != null && hotel.programMembership.membershipNumber != null) {
        hotelTravelerVO.membershipNumber = hotel.programMembership.membershipNumber;
      }
      hotelTravelerVO.roomType = hotel.lodgingUnitType;

      if (hotel.underName != null) {
        hotelTravelerVO.firstName = hotel.underName.givenName;
        hotelTravelerVO.lastName = hotel.underName.familyName;
      }

      travelerVOS.add(hotelTravelerVO);
    }
    if (!travelerVOS.isEmpty()) {
      hvo.setTravelers(travelerVOS);
    }

    tvo.addHotel(hvo);
    return hvo;
  }

  public TransportVO car2VO(TripVO tvo, RentalCarReservation car) {
    TransportVO trvo = new TransportVO();
    updateBase(car, trvo);
    trvo.setBookingType(ReservationType.CAR_RENTAL);

    if(car.name != null && !car.name.isEmpty()) {
      trvo.setName(car.name);
    } else  if (car.reservationFor.name != null && !car.reservationFor.name.isEmpty()) {
      trvo.setName(car.reservationFor.name);
    } else {
      trvo.setName("Car Rental");
    }
    trvo.setConfirmationNumber(car.reservationNumber);

    if(car.underName != null) {
      trvo.addProperty("Booking For", car.underName.name);
    }

    if (car.reservationFor != null) {
      if (car.reservationFor.rentalCompany != null) {
        trvo.setCmpyName(car.reservationFor.rentalCompany.name);
        if (car.reservationFor.rentalCompany.url != null) {
          trvo.addProperty("Website", car.reservationFor.rentalCompany.url);
        }

      }
      trvo.addProperty("Car Model", car.reservationFor.model);
      if(car.reservationFor.brand != null) {
        trvo.addProperty("Car Brand", car.reservationFor.brand.name);
      }
    }

    if (car.pickupTime != null) {
      trvo.setPickupDate(car.pickupTime.getTimestamp());
    }

    if (car.dropoffTime != null) {
      trvo.setDropoffDate(car.dropoffTime.getTimestamp());
    }

    if (car.programMembership != null) {
      trvo.addProperty("Membership Program", car.programMembership.program);
      trvo.addProperty("Membership Program", car.programMembership.programName);
      trvo.addProperty("Membership Number", car.programMembership.membershipNumber);
    }

    if (car.pickupLocation != null) {
      StringBuilder sb = new StringBuilder();
      if (car.pickupLocation.telephone != null && !car.pickupLocation.telephone.isEmpty()) {
        trvo.addProperty("Pickup Location Phone", car.pickupLocation.telephone);
      }
      if (car.pickupLocation.name != null && !car.pickupLocation.name.isEmpty()) {
        sb.append(car.pickupLocation.name);
      }
      if (car.pickupLocation.address != null && !car.pickupLocation.address.toString().isEmpty()) {
        if (sb.toString().length() > 0) {
          sb.append(": ");
        }
        sb.append(car.pickupLocation.address.toStringSingleLine());
      }
      trvo.setPickupLocation(sb.toString());
    }

    if (car.dropoffLocation != null) {
      StringBuilder sb = new StringBuilder();
      if (car.dropoffLocation.telephone != null && !car.dropoffLocation.telephone.isEmpty()) {
        trvo.addProperty("Dropoff Location Phone", car.dropoffLocation.telephone);
      }
      if (car.dropoffLocation.name != null && !car.dropoffLocation.name.isEmpty()) {
        sb.append(car.dropoffLocation.name);
      }
      if (car.dropoffLocation.address != null && !car.dropoffLocation.address.toString().isEmpty()) {
        if (sb.toString().length() > 0) {
          sb.append(": ");
        }
        sb.append(car.dropoffLocation.address.toStringSingleLine());
      }
      trvo.setDropoffLocation(sb.toString());
    }

    addPrice(trvo, car);

    tvo.addTransport(trvo);
    return trvo;
  }

  public void addPrice (ReservationVO vo, Reservation reservation) {
    String cur = "";
    if (reservation.currency != null && !reservation.currency.trim().isEmpty()) {
      vo.setCurrency(reservation.currency);
    }
    if (reservation.priceCurrency != null && !reservation.priceCurrency.trim().isEmpty()) {
      vo.setCurrency(reservation.priceCurrency);
    }
    if (reservation.price != null && !reservation.price.trim().isEmpty()) {
      vo.setSubtotal(reservation.price);
    }
    if (reservation.totalPrice != null && !reservation.totalPrice.trim().isEmpty()) {
      vo.setTotal(reservation.totalPrice);
    }

  }

  public AirportVO airportToVO(Airport a) {
    AirportVO avo = new AirportVO();

    if (a.iataCode != null) {
      avo.setCode(a.iataCode);
    } else if (a.icaoCode != null) {
      avo.setCity(a.icaoCode);
    }

    avo.setName(a.name);

    return avo;
  }

  public TripVO toTripVO(TripVO tvo) {
    if (tvo == null) {
      tvo = new TripVO();
    }

    for (FlightReservation fr : flights) {
      flight2VO(tvo, fr);
    }

    for (LodgingReservation lr : hotels) {
      lodging2VO(tvo, lr);
    }

    for (EventReservation er : events) {
      event2VO(tvo, er);
    }

    for (RentalCarReservation cr : cars) {
      car2VO(tvo, cr);
    }

    for (TransferReservation tr : transfers) {
      transfer2Vo(tvo, tr);
    }

    for (TrainReservation r : trains) {
      train2Vo(tvo, r);
    }

    for (CruiseShipReservation cr : cruises) {
      cruise2Vo(tvo, cr);
    }

    for (ActivityReservation at : activities) {
      activity2VO(tvo, at);
    }

    for (FoodEstablishmentReservation r : restaurants) {
      restaurant2VO(tvo, r);
    }

    for (Note n : notes) {
      note2VO(tvo, n);
    }

    if (src != null) {
      tvo.setImportSrc(BookingSrc.ImportSrc.API);
    }
    else {
      tvo.setImportSrc(BookingSrc.ImportSrc.SCHEMA_ORG);
    }

    tvo.setImportTs(System.currentTimeMillis());

    return tvo;
  }

  public boolean assignReservationFromJson(JsonNode data) {
    Reservation reservation = om.convertValue(data, Reservation.class);
    if (reservation != null) {
      assignReservation(reservation);
    }
    return reservation != null;
  }

  public void assignReservation(Object reservation) {
    if (reservation instanceof FlightReservation) {
      flights.add((FlightReservation) reservation);
    }
    else if (reservation instanceof LodgingReservation) {
      hotels.add((LodgingReservation) reservation);
    }
    else if (reservation instanceof EventReservation) {
      events.add((EventReservation) reservation);
    }
    else if (reservation instanceof RentalCarReservation) {
      cars.add((RentalCarReservation) reservation);
    }
    else if (reservation instanceof ActivityReservation) {
      activities.add((ActivityReservation) reservation);
    }
    else if (reservation instanceof CruiseShipReservation) {
      cruises.add((CruiseShipReservation) reservation);
    }
    else if (reservation instanceof TrainReservation) {
      trains.add((TrainReservation) reservation);
    }
    else if (reservation instanceof FoodEstablishmentReservation) {
      restaurants.add((FoodEstablishmentReservation) reservation);
    }
    else if (reservation instanceof TransferReservation) {
      transfers.add((TransferReservation) reservation);
    }
    else if (reservation instanceof Note) {
      notes.add((Note) reservation);
    }
    else if (reservation instanceof ReservationPackage) {
      reservationPackage = (ReservationPackage) reservation;
      if (reservationPackage != null) {
        //convert all emails to lower case
        if (reservationPackage.underName != null && reservationPackage.underName.email != null && reservationPackage.underName.email.trim().length() > 0) {
          reservationPackage.underName.email =  reservationPackage.underName.email.toLowerCase();
        }
        if (reservationPackage.additionalTravelers != null && reservationPackage.additionalTravelers.size() > 0) {
          for (Person p: reservationPackage.additionalTravelers) {
            if (p.email != null && p.email.trim().length() > 0) {
              p.email = p.email.toLowerCase();
            }
          }
        }
        if (reservationPackage.broker != null && reservationPackage.broker.email != null && reservationPackage.broker.email.trim().length() > 0) {
          reservationPackage.broker.email = reservationPackage.broker.email.toLowerCase();
        }
      }
      for (Reservation r : reservationPackage.reservation) {
        assignReservation(r);
      }
      for (Reservation r : reservationPackage.subReservation) {
        assignReservation(r);
      }
    }
  }
}
