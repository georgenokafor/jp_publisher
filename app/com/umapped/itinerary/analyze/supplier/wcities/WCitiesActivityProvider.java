package com.umapped.itinerary.analyze.supplier.wcities;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.recommendation.RecommendedItem;
import com.umapped.itinerary.analyze.supplier.musement.MusementWebService;
import com.umapped.itinerary.analyze.supplier.musement.response.MusementCityLookup;
import com.umapped.service.offer.ActivityFilter;
import com.umapped.service.offer.ActivityOfferProvider;
import com.umapped.service.offer.OfferResult;
import com.umapped.service.offer.UMappedCity;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 10/10/17.
 */
@Singleton
public class WCitiesActivityProvider implements ActivityOfferProvider {
    @Inject
    private WCitiesWebService wCitiesWebService;

    @Inject
    WCitiesCityLookup cityLookup;

    @Override public OfferResult getActivityOffers(ActivityFilter filter) {
        UMappedCity city = filter.getLocation();
        if (city != null) {
            String cityId = cityLookup.getCityId(city.getId());
            if (cityId != null) {
                List<RecommendedItem> items = new ArrayList<>();
                if (filter.getStayPeriods() != null && filter.getStayPeriods().size() > 0) {
                    for (StayPeriod s: filter.getStayPeriods()) {
                        //check to make sure that the stay period is greater than 1 day
                        if (s.getEnd().isAfter(s.getStart())) {
                            RequestPreset preset = new RequestPreset();
                            List<RecommendedItem> items1 = wCitiesWebService.getEvents(s, city.getGeoLocation(), preset);
                            if (items1 != null && !items1.isEmpty()) {
                                items.addAll(items1);
                            }
                        }

                    }
                }
                return new OfferResult(filter.getStayPeriods(), items);
            }
        }
        return null;
    }

}
