package com.mapped.publisher.persistence.communicator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.firebase.client.Firebase;
import com.mapped.publisher.utils.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by surge on 2015-07-16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Notification
    extends FirebaseObject{
  /* YAML Spec:
    notifications:
      type: object
      properties:
        ~$confId:
          type: object
          properties:
            ~$roomId:
              type: object
              properties:
                ~$notificationId:
                  type: object
                  properties:
                    msgId:            {type: string} #ID of the message in /messages structure
                    fromUserId:
                      type: string
                      constraint: isMe()
                    fromUserName:     {type: string} # Name of the user i.e. full name
                    timestamp:        {type: number} # UTC Epoch in seconds
                    notificationType: {type: string} # "message", "change"
                    priority:         {type: string} # "high", "normal"
                    status:           {type: string} # "read", "new"
                  required: [ fromUserId, fromUserName, timestamp, notificationType ]
   */

  public String fromUserId;
  public String fromUserName;
  public Long timestamp;
  public NotificationType notificationType;
  public RoomMessageType priority;
  public RoomMessageStatus status;
  public Map<String, String> data;

  public Notification() {

  }

  public Notification(Notification other) {
    fromUserId        = other.fromUserId;
    fromUserName      = other.fromUserName;
    timestamp         = other.timestamp;
    notificationType  = other.notificationType;
    status            = other.status;
    priority          = other.priority;
  }

  public void setMsgId(String msgId) {
    if(data == null) {
      data = new HashMap<>();
    }
    data.put("msgId", msgId);
  }

  public Firebase update(Firebase ref) {
    Map<String, Object> uVals = new HashMap<>();
    uVals.put("fromUserId",fromUserId);
    uVals.put("fromUserName",fromUserName);
    uVals.put("timestamp", timestamp);
    uVals.put("notificationType", notificationType.name());
    uVals.put("priority", priority.name());
    uVals.put("status", status.name());
    ref.updateChildren(uVals);
    Log.debug("Firebase requested record update: " + ref.getPath().toString());
    return ref;
  }
}
