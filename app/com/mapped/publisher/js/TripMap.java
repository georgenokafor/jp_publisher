package com.mapped.publisher.js;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.umapped.api.schema.types.ReturnCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2016-01-25.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TripMap
    extends BaseJsonResponse {

  public List<Location> locations;

  public static class Location
      implements Serializable {
    public Float  latitude;
    public Float  longitude;
    public String name;
  }

  public TripMap(ReturnCode rc) {
    super(rc);
    locations = new ArrayList<>();
  }

  public void addLocation(String name, Float latitude, Float longitude) {
    Location l = new Location();
    l.latitude = latitude;
    l.longitude = longitude;
    l.name = name;
    locations.add(l);
  }
}
