package com.mapped.publisher.actions;

import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.view.BaseView;
import com.umapped.common.BkApiSrcIdConstants;
import com.umapped.external.travelbound.TravelboundAPICredential;
import models.publisher.ApiCmpyToken;
import models.publisher.BkApiSrc;
import models.publisher.Company;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by wei on 2017-05-11.
 */
public class TravelboundAuthorized
    extends Action.Simple {
  public static final String TRAVELBOUND_CREDENTIAL = "TRAVELBOUND_CREDENTIAL";

  @Override public CompletionStage<Result> call(Http.Context ctx) {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx);
    List<ApiCmpyToken> tokens = getTravelboundAPITokens(sessionMgr.getCredentials().getCmpyId());
    List<TravelboundAPICredential> credentials = new ArrayList<>();
    if (!CollectionUtils.isEmpty(tokens)) {
      for (ApiCmpyToken token : tokens) {
        if (token != null) {
          try {
            int clientId = Integer.parseInt(token.getSrcCmpyId());
            String[] values = StringUtils.split(token.getToken(), ':');
            if (values != null && values.length == 3) {
              TravelboundAPICredential credential = new TravelboundAPICredential(clientId,
                                                                                 values[0],
                                                                                 values[1],
                                                                                 values[2]);
              credentials.add(credential);
            }
          }
          catch (Exception e) {
            Log.err("Fail to parse Travelbound token", e);
          }
        }
      }
    }
    if (CollectionUtils.isEmpty(credentials)) {
      BaseView view = new BaseView();
      view.message = "TravelBound is not configured properly for your company - please contact Umapped Support";
      return CompletableFuture.completedFuture(ok(views.html.common.message.render(view)));
    }
    else {
      ctx.args.put(TRAVELBOUND_CREDENTIAL, credentials);
      return delegate.call(ctx);
    }
  }

  private List<ApiCmpyToken> getTravelboundAPITokens(String cmpyId) {
    List<ApiCmpyToken> tokens = ApiCmpyToken.findCmpyTokensByType(cmpyId,
                                                                  BkApiSrc.getId(BkApiSrcIdConstants.TRAVELBOUND));

    if (CollectionUtils.isEmpty(tokens)) {
      Company parent = Company.findParentCompany(cmpyId);
      if (parent != null) {
        tokens = ApiCmpyToken.findCmpyTokensByType(parent.getCmpyid(), BkApiSrc.getId(BkApiSrcIdConstants.TRAVELBOUND));
      }
    }
    return tokens;
  }
}
