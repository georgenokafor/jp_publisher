package models.publisher;

import com.avaje.ebean.BeanState;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.persistence.IUmappedModel;
import com.mapped.publisher.persistence.IUmappedModelWithImage;
import com.mapped.publisher.persistence.PageAttachType;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import static com.mapped.publisher.common.APPConstants.STATUS_ACTIVE;

/**
 * Created by twong on 15-04-24.
 */
@Entity
@Table(name = "trip_note_attach")
public class TripNoteAttach
    extends UmappedModelWithImage
    implements IUmappedModel<Long, TripNoteAttach> {

  public static Model.Finder<Long, TripNoteAttach> find = new Finder(TripNoteAttach.class);

  public String   tag;
  public String   comments;
  @Id
  public Long     fileId;
  @Constraints.Required
  @ManyToOne
  @JoinColumn(name = "note_id", nullable = false)
  public TripNote tripNote;
  public String   name;
  public int      rank;
  public int      status;
  public Long     createdTimestamp;
  public Long     lastUpdatedTimestamp;
  public String   createdBy;
  public String   modifiedBy;

  String         attachName;
  String         attachUrl;
  PageAttachType attachType;
  @ManyToOne
  @JoinColumn(name = "image_pk", nullable = true, referencedColumnName = "pk")
  FileImage image;
  @ManyToOne
  @JoinColumn(name = "file_pk", nullable = true, referencedColumnName = "pk")
  FileInfo  file;


  /**
   * Takes this object makes it possible to re-insert as new record
   * @return
   */
  public TripNoteAttach prepareForReuse(String createdBy, String modifiedBy) {
    BeanState state = Ebean.getBeanState(this);
    state.resetForInsert();
    setFileId(DBConnectionMgr.getUniqueLongId());
    setModifiedBy(modifiedBy);
    setCreatedBy(createdBy);
    setLastUpdatedTimestamp(System.currentTimeMillis());
    setCreatedTimestamp(lastUpdatedTimestamp);
    return this;
  }


  public TripNoteAttach markModified(final String userId){
    setModifiedBy(userId);
    setLastUpdatedTimestamp(System.currentTimeMillis());
    return this;
  }

  public static TripNoteAttach build(final TripNote tn, final String createdBy) {
    TripNoteAttach tna = new TripNoteAttach();
    tna.setFileId(DBConnectionMgr.getUniqueLongId());
    tna.setCreatedTimestamp(System.currentTimeMillis());
    tna.setLastUpdatedTimestamp(tna.createdTimestamp);
    tna.setCreatedBy(createdBy);
    tna.setModifiedBy(createdBy);
    tna.setTripNote(tn);
    tna.setStatus(STATUS_ACTIVE);
    return tna;
  }

  public static TripNoteAttach duplicateFromGuideAttach(DestinationGuideAttach dga,
                                                        TripNote tripNote,
                                                        String modifiedBy) {
    TripNoteAttach newGuideFile = new TripNoteAttach();
    newGuideFile.setAttachName(dga.getAttachname());
    newGuideFile.setAttachType(dga.getAttachtype());
    newGuideFile.setAttachUrl(dga.getAttachurl());
    newGuideFile.setComments(dga.comments);
    newGuideFile.setName(dga.name);
    newGuideFile.setRank(dga.rank);
    newGuideFile.setTag(dga.tag);

    newGuideFile.setFileId(DBConnectionMgr.getUniqueLongId());
    newGuideFile.setTripNote(tripNote);
    newGuideFile.setModifiedBy(modifiedBy);
    newGuideFile.setCreatedBy(modifiedBy);
    newGuideFile.setStatus(STATUS_ACTIVE);
    newGuideFile.setCreatedTimestamp(System.currentTimeMillis());
    newGuideFile.setLastUpdatedTimestamp(newGuideFile.createdTimestamp);
    newGuideFile.save();
    return newGuideFile;
  }

  public static TripNoteAttach findByNoteIdFileUrl(Long noteId, String attachUrl) {
    return find.where().eq("note_Id", noteId).eq("attachUrl", attachUrl).eq("status", 0).findUnique();
  }

  public static List<TripNoteAttach> findByNoteId(Long noteId) {
    return find.where().eq("note_Id", noteId).eq("status", 0).order("attach_type, rank").findList();
  }

  public static int maxPageRankByNoteId(Long noteId, String attachType) {
    return find.where().eq("note_Id", noteId).eq("attach_type", attachType).findCount();

  }



  public static List<TripNoteAttach> findPhotosByNoteId(Long noteId) {
    return find.where()
               .eq("note_Id", noteId)
               .eq("status", 0)
               .eq("attach_type", PageAttachType.PHOTO_LINK.getStrVal())
               .orderBy("rank")
               .findList();

  }

  public String getTag() {
    return tag;
  }

  public TripNoteAttach setTag(String tag) {
    this.tag = tag;
    return this;
  }

  public String getComments() {
    return comments;
  }

  public TripNoteAttach setComments(String comments) {
    this.comments = comments;
    return this;
  }

  public Long getFileId() {
    return fileId;
  }

  public TripNoteAttach setFileId(Long fileId) {
    this.fileId = fileId;
    return this;
  }

  public PageAttachType getAttachType() {
    return attachType;
  }

  public TripNoteAttach setAttachType(PageAttachType attachType) {
    this.attachType = attachType;
    return this;
  }

  public TripNote getTripNote() {
    return tripNote;
  }

  public TripNoteAttach setTripNote(TripNote tripNote) {
    this.tripNote = tripNote;
    return this;
  }

  public String getName() {
    return name;
  }

  public TripNoteAttach setName(String name) {
    this.name = name;
    return this;
  }

  public String getAttachName() {
    return attachName;
  }

  public TripNoteAttach setAttachName(String attachName) {
    this.attachName = attachName;
    return this;
  }

  public String getAttachUrl() {
    return attachUrl;
  }

  public TripNoteAttach setAttachUrl(String attachUrl) {
    this.attachUrl = attachUrl;
    return this;
  }

  public FileImage getImage() {
    return image;
  }

  public TripNoteAttach setImage(FileImage image) {
    this.image = image;
    return this;
  }

  public FileInfo getFile() {
    return file;
  }

  public TripNoteAttach setFile(FileInfo file) {
    this.file = file;
    return this;
  }

  public int getRank() {
    return rank;
  }

  public TripNoteAttach setRank(int rank) {
    this.rank = rank;
    return this;
  }

  public int getStatus() {
    return status;
  }

  public TripNoteAttach setStatus(int status) {
    this.status = status;
    return this;
  }

  public Long getCreatedTimestamp() {
    return createdTimestamp;
  }

  public TripNoteAttach setCreatedTimestamp(Long createdTimestamp) {
    this.createdTimestamp = createdTimestamp;
    return this;
  }

  public Long getLastUpdatedTimestamp() {
    return lastUpdatedTimestamp;
  }

  public TripNoteAttach setLastUpdatedTimestamp(Long lastUpdatedTimestamp) {
    this.lastUpdatedTimestamp = lastUpdatedTimestamp;
    return this;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public TripNoteAttach setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public TripNoteAttach setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
    return this;
  }

  public int getVersion() {
    return version;
  }

  public TripNoteAttach setVersion(int version) {
    this.version = version;
    return this;
  }

  @Override
  public String getImageUrl() {
    if(getFileImage() == null) {
      return getAttachUrl();
    } else {
      return getFileImage().getUrl();
    }
  }

  @Override
  public FileImage getFileImage() {
    if (getImage() != null) {
      return getImage();
    } else {
      FileImage fi = new FileImage();
      FileInfo fileInfo = new FileInfo();
      fileInfo.setFilename(this.getAttachName());
      fileInfo.setDescription(this.getComments());
      fileInfo.setUrl(this.getAttachUrl());
      fi.setFile(fileInfo);
      return fi;
    }
  }

  @Override
  public IUmappedModelWithImage setFileImage(FileImage image) {
    setImage(image);
    if(image != null) {
      setName(image.getFilename());
      setAttachName(image.getFilename());
      setAttachType(PageAttachType.PHOTO_LINK);
      setAttachUrl(image.getUrl());
    }
    return this;
  }



  public FileInfo getFileInfo() {
    return getFile();
  }


  public TripNoteAttach setFileInfo(FileInfo file) {
    setFile(file);

    if(file != null) {
      setName(file.getFilename());
      setAttachName(file.getFilename());
      setAttachType(PageAttachType.FILE_LINK);
      setAttachUrl(file.getUrl());
    }
    return this;
  }

  @Override
  public TripNoteAttach getByPk(Long pk) {
    return find.byId(pk);
  }

  @Override
  public Long getPk() {
    return getFileId();
  }

  @Override
  public String getPkAsString() {
    return getPk().toString();
  }

  @Override
  public Long getCreatedByPk() {
    return Account.findByLegacyId(getCreatedBy()).getUid();
  }

  @Override
  public Long getModifiedByPk() {
    return Account.findByLegacyId(getModifiedBy()).getUid();
  }

  @Override
  public String getCreatedByLegacy() {
    return getCreatedBy();
  }

  @Override
  public String getModifiedByLegacy() {
    return getModifiedBy();
  }

  @Override
  public Long getCreatedEpoch() {
    return getCreatedTimestamp();
  }

  @Override
  public Timestamp getCreatedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getCreatedTimestamp()));
  }

  @Override
  public Long getModifiedEpoch() {
    return getLastUpdatedTimestamp();
  }

  @Override
  public Timestamp getModifiedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getLastUpdatedTimestamp()));
  }
}
