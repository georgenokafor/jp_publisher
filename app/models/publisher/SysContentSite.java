package models.publisher;

import com.mapped.publisher.persistence.RecordState;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Table of sites that can be used to search for content for destination guides (document pages)
 * Created by surge on 2014-12-11.
 */
@Entity @Table(name = "sys_content_site")
public class SysContentSite
    extends Model {

  public static Finder<String, SysContentSite> find = new Finder<>(SysContentSite.class);

  @Id
  private int siteId;
  /**
   * Domain (this is different from URL in that sometimes it might be desirable to
   * have access to various sections of the website)
   */
  private String domain;
  private String url;             // Full URL of the section hosting content
  private String name;            // Name of the website
  /**
   * In anticipation of Five Filter's version 3.4 storing config in the database would allow adding new sites without
   * touching servers: http://blog.fivefilters.org/post/96990228032/full-text-rss-3-4
   */
  private String parseConfig;
  @Column(name = "state")
  @Enumerated(EnumType.STRING)
  private RecordState state;
  @Version
  private int version;

  public static List<SysContentSite> findAllActive() {
    return find.where().eq("state", RecordState.ACTIVE.name()).orderBy("site_id asc").findList();
  }

  public static SysContentSite findByDomain(String domain) {
    if (domain == null) {
      return null;
    }
    return find.where().like("domain", '%'+domain+'%').findUnique();
  }

  public String getParseConfig() {
    return parseConfig;
  }

  public void setParseConfig(String parseConfig) {
    this.parseConfig = parseConfig;
  }

  public int getSiteId() {
    return siteId;
  }

  public void setSiteId(int siteId) {
    this.siteId = siteId;
  }

  public String getDomain() {
    return domain;
  }

  public void setDomain(String domain) {
    this.domain = domain;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public RecordState getState() {
    return state;
  }

  public void setState(RecordState state) {
    this.state = state;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

}
