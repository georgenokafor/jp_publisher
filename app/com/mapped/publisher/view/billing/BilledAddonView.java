package com.mapped.publisher.view.billing;

import models.publisher.BillingAddonsForUser;
import models.publisher.BillingPlan;

/**
 * Created by surge on 2015-12-17.
 */
public class BilledAddonView {
  public BillingAddonsForUser state;
  public BillingPlan          addon;
  public float   cost         = 0.0f;
  public float   proRatedCost = 0.0f;
  public boolean isProRated   = false;
}
