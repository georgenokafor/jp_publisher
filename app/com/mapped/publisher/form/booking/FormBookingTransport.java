package com.mapped.publisher.form.booking;

import org.apache.commons.lang3.StringUtils;
import play.data.validation.Constraints;

import java.util.List;

/**
 * Created by surge on 2016-01-04.
 */
public class FormBookingTransport
    extends FormBookingBase {
  @Constraints.Required
  public String inBookingType;
  public String inTransferCmpy;
  public Long   inTransferProviderId;
  public String inTransferContact;
  @Constraints.Required
  public String inTransferDate;
  public String inTransferTime;
  public String inTransferBooking;
  public String inTransferNote;

  public String inTransferEndDate;
  public String inTransferEndTime;
  public String inTransferPickUp;
  public String inTransferDropOff;


  public String inTransferImportant;
  public String inTransferCancellation;
  public String inTransferStartTimezone;
  public String inTransferFinishTimezone;
  public String inTransferServiceType;


}
