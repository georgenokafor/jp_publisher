package com.mapped.publisher.msg.flightstat.status;

import java.util.List;

/**
 * Created by twong on 2015-01-25.
 */
public class IrregularOperations {

  public List<IrregularOperation> irregularOperation;

  public List<IrregularOperation> getIrregularOperation() {
    return irregularOperation;
  }

  public void setIrregularOperation(List<IrregularOperation> irregularOperation) {
    this.irregularOperation = irregularOperation;
  }
}
