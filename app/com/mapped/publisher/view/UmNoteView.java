package com.mapped.publisher.view;

import com.mapped.publisher.view.TripBookingDetailView;
import com.mapped.publisher.view.UmReservationView;
import com.umapped.persistence.enums.ReservationType;

import java.io.Serializable;

/**
 * Created by ryan on 17/03/17.
 */
public class UmNoteView extends TripBookingDetailView
    implements Serializable{

    public UmNoteView() {
        this.detailsTypeId = ReservationType.NOTE;
    }
}
