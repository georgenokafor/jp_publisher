package com.umapped.itinerary.analyze;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.umapped.itinerary.analyze.segment.TripSegment;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.segment.decisiontree.impl.AccommodationDecisionTree;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeatureDetectorRegistry;
import modules.ItineraryAnalyzeModule;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by wei on 2017-03-09.
 */
public class AccommodationRecommendationTester extends AbstractRecommendationTester {
  private static String DATE ="2017-03-08";

  @Inject public AccommodationRecommendationTester(ObjectMapper om, SegmentFeatureDetectorRegistry registry) {
    super(om, registry, new AccommodationDecisionTree());
  }

  @Override protected String getOutputDir() {
    return DATE;
  }

  public void outputCsv(String fileName, String outputDir, List<TripSegment> segments, List<List<DecisionResult>> results)
      throws IOException {
    String header = "Nature, Start Date, End Date, Recommend, Duration, RecommendedAccommodation, RecommendedActivity, Transfer\n";
    String format = "%s, %s, %s, %s, %s, %s, %s, %s\n";

    FileWriter w = new FileWriter(new File(outputDir, fileName.replace("json", "csv")));
    w.write(header);
    for (int i = 0; i < segments.size(); i++) {
      TripSegment segment = segments.get(i);
      List<DecisionResult> r = results.get(i);
      w.write(String.format(format,
                            getNatures(segment.getNatures()),
                            segment.getStartDateTime(),
                            segment.getEndDateTime(),
                            r.isEmpty() ? "No" : "Yes",
                            segment.getDurationHours(),
                            getIds(segment.getAccommodations()),
                            getIds(segment.getActivities()),
                            getIds(segment.getTransfers())));
      if (!r.isEmpty()) {
        System.out.println("Positive: " + fileName);
        System.out.println(r);
      }
    }
    w.close();
  }


  public static void main(String[] args)
      throws Exception {
    Injector container = Guice.createInjector(new ItineraryAnalyzeModule());

    AccommodationRecommendationTester tester = container.getInstance(AccommodationRecommendationTester.class);

    File dir = new File(SEGMENT_ROOT + DATE);

    File[] files = dir.listFiles((direction, name) -> name.endsWith(".json"));


    for (File f : files) {
      //if (f.getName().contains("my_test")) {
         tester.analyze(f);
      //}
    }
  }
}
