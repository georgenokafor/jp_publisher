name := """publisher"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  filters,
  // Umapped project dependencies:
  "org.avaje.ebean" % "ebean" % "9.2.1",
  "org.avaje.ebean" % "ebean-postgis" % "0.1.2",        //PostGIS support is currently a separate asset, specifies all dependencies
  //"io.dropwizard.metrics" % "metrics-core" % "3.1.2", //Metrix to profile with VisualVM
  "de.micromata.jak" % "JavaAPIforKml" % "2.2.1",       //KML Helper classes
  "org.postgresql" % "postgresql" % "9.4.1212",
  "com.amazonaws" % "aws-java-sdk-s3" % "1.11.55",      //updated to s3 sdk only
  "com.amazonaws" % "aws-java-sdk-sqs" % "1.11.55",     //updated to sqs sdk only

  "org.jsoup" % "jsoup" % "1.10.1",
  "net.spy" % "spymemcached" % "2.12.0",
  "com.googlecode.owasp-java-html-sanitizer" % "owasp-java-html-sanitizer" % "20160924.1",
  "it.innove" % "play2-pdf" % "1.5.1",
  "org.apache.pdfbox" % "pdfbox" % "1.8.11",
  "com.freshbooks" % "freshbooksApiClient" % "0.5.1",
  "org.apache.commons" % "commons-csv" % "1.4",
  "commons-collections" % "commons-collections" % "3.2.2",
  "org.apache.any23" % "apache-any23-core" % "1.1",
  "org.apache.httpcomponents" % "httpcomponents-core" % "4.4.5",
  "com.firebase" % "firebase-token-generator" % "2.0.0",
  "com.firebase" % "firebase-client-jvm" % "2.5.0",
  "com.google.oauth-client" % "google-oauth-client" %"1.22.0",
  "com.google.http-client" % "google-http-client-jackson2" % "1.22.0",
  "com.google.maps" % "google-maps-services" % "0.1.17",
  "redis.clients" % "jedis" % "2.9.0",
  //JWT Tokens library
  "io.jsonwebtoken" % "jjwt" % "0.7.0",
  //Apache Mail Helpers
  "org.apache.commons" % "commons-email" % "1.4",
  //Very fast markdown implementation: http://stackoverflow.com/questions/19784525/markdown-to-html-with-java-scala
  "com.github.rjeschke" % "txtmark" % "0.13" ,
  //IOS app notification
  "com.notnoop.apns" % "apns" % "1.0.0.Beta6",
  //ical4J
  "org.mnode.ical4j" % "ical4j" % "1.0.7",
  //ical4J - outlook
  "org.mnode.ical4j" % "ical4j-zoneinfo-outlook" % "1.0.4",
  //Log Entries appender
    "com.logentries" % "logentries-appender" % "1.1.35",
  //Braintree
  "com.braintreepayments.gateway" % "braintree-java" % "2.67.0",
  "com.google.firebase" % "firebase-admin" % "5.4.0",
  //UMapped Temporarily Disabled Libraries
  //"com.newrelic.agent.java" % "newrelic-agent" % "3.33.0"
  //https://mvnrepository.com/artifact/net.postgis/postgis-jdbc
  "net.postgis" % "postgis-jdbc" % "2.2.1"
  //https://mvnrepository.com/artifact/org.avaje.ebean/ebean-postgis
  //"org.avaje.ebean" % "ebean-postgis" % "0.1.2"
)

includeFilter in (Assets, LessKeys.less) := "*.less"
excludeFilter in (Assets, LessKeys.less) := "_*.less"

TwirlKeys.templateImports += "org.apache.commons.lang3.StringEscapeUtils.escapeEcmaScript"

resolvers += "Freshbooks Java API Client" at "https://raw.github.com/mutovkin/freshbooks-api-java-client/mvn-repo/"

//javacOptions ++= Seq("-Xlint:unchecked")
//javacOptions += "-Xlint:deprecation"

routesGenerator := InjectedRoutesGenerator
playEnhancerEnabled := true

//Debugging in test - Disabling forking
Keys.fork in Test := false
//Debugging in test  - setting up socket parameters
//Keys.javaOptions in (Test) += "-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=9999"
//Disabling parallel exectuion
parallelExecution in Test := false

//fixed depency thast was broken by some ebean module to 1.8 alpha
dependencyOverrides += "org.slf4j" % "slf4j-api" % "1.7.25"
