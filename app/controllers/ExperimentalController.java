package controllers;

import actors.*;
import akka.NotUsed;
import akka.actor.Status;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Sender;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.AdminAccess;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.AuditActorType;
import com.mapped.publisher.audit.AuditModuleType;
import com.mapped.publisher.audit.event.AuditTripTraveller;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.ExperimentalPDFUploadForm;
import com.mapped.publisher.js.BaseJsonResponse;
import com.mapped.publisher.parse.extractor.booking.ClientbaseExtractor;
import com.mapped.publisher.parse.extractor.booking.QueenOfClubsExtractor;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.parse.worldmate.*;
import com.mapped.publisher.persistence.*;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.persistence.redis.RedisKeys;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.*;
import com.mapped.publisher.view.*;
import com.mapped.publisher.view.email.CommunicatorMessageView;
import com.umapped.api.schema.local.PoiJson;
import com.umapped.api.schema.types.*;

import com.umapped.api.schema.types.Address;
import com.umapped.common.BkApiSrcIdConstants;
import com.umapped.external.virtuoso.hotel.Root;
import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;
import com.umapped.itinerary.analyze.model.*;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.segment.TripSegmentAnalyzeResult;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.service.offer.OfferService;
import com.umapped.service.offer.UMappedCity;
import com.umapped.service.offer.UMappedCityLookup;
import it.innove.play.pdf.PdfGenerator;
import models.publisher.*;

import models.publisher.Destination;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripperByArea;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.postgis.*;
import org.postgis.Point;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.awt.*;
import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalUnit;
import java.util.*;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

import static com.mapped.publisher.utils.Utils.pojo2Xml;
import static play.db.DB.getConnection;
import play.libs.ws.*;

/**
 * Created by Serguei Moutovkin on 2014-05-02.
 */
public class ExperimentalController
    extends Controller {

  @Inject
  PdfGenerator pdfGenerator;

  @Inject
  RedisMgr redis;

  @Inject
  FormFactory formFactory;

  @Inject
  UMappedCityLookup umCity;

  @Inject
  OfferService offerService;

  @Inject
  WSClient ws;

  private enum VENDOR_COL {
    TYPE,
    NAME,
    DESC,
    ADDR1,
    ADDR2,
    CITY,
    STATE,
    ZIP,
    COUNTRY,
    LAT,
    LONG,
    EMAIL,
    WEB,
    PHONE,
    FAX,
    FACEBOOK,
    TWITTER,
    IMG,
    TAG
  }

  private enum DOC_COL {
    TYPE,
    DOC_NAME,
    PAGE_NAME,
    LANDMARK,
    ADDR1,
    CITY,
    ZIP,
    STATE,
    COUNTRY,
    LAT,
    LONG,
    RECOMMEND,
    INTRO,
    DESC,
    IMG,
    TAG,
    URL
  }

  private enum AIRPORT_COL {
    FS,
    IATA,
    ICAO,
    NAME,
    CITY,
    CITY_CODE,
    COUNTRY_CODE,
    COUNTRY_NAME,
    REGION_NAME,
    TIMEZONE_REGION_NAME,
    LOCAL_TIME,
    UTC_OFFSET_HOURS,
    LATITUDE,
    LONGITUDE,
    ELEVATION_FEET,
    CLASSIFICATION,
    ACTIVE,
    WEATHER_URL,
    DELAY_INDEX_URL,
    FAA,
    STATE_CODE,
    STREET1,
    STREET2,
    POSTALCODE
  }

  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public Result parsePdf() {
    ExpPDFParserView view = new ExpPDFParserView();

    Form<ExperimentalPDFUploadForm> pdfForm = formFactory.form(ExperimentalPDFUploadForm.class);

    ExperimentalPDFUploadForm pdfFileInfo = pdfForm.bindFromRequest().get();

    Log.log(LogLevel.DEBUG, "ParsePDF Controller: Filename:", pdfFileInfo.toString());


    view.isUMappedAdmin = true;                     //TODO: Figure out what this actually does and how it gets populated
    view.userId = "admin";                          //TODO: Figure out how to properly populate this property
    view.isUploaded = pdfFileInfo.uploaded;         //TODO: This has to be switching upload vs parse view
    view.message = "Work in progress";

    try {
      S3Object s3Obj = S3Util.getS3File(pdfFileInfo.inPDFFileName);


      QueenOfClubsExtractor extractor = new QueenOfClubsExtractor();
      TripVO tvo = extractor.extractData(s3Obj.getObjectContent());
      view.pdfText = extractor.toString();
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Failed to grab file from S3");
      Log.log(LogLevel.ERROR, e.getMessage());
      StackTraceElement trace[] = e.getStackTrace();
      for (StackTraceElement t : trace) {
        Log.log(LogLevel.ERROR, t.toString());
      }
    }

    return ok(views.html.experimental.pdfparser.render(view));
  }

  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public Result updateS3Index() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    final String tokenName = form.get("inS3");

    IndexS3View view = new IndexS3View();
    view.s3Token = tokenName;

    List<ImageView> results = new ArrayList<>();
    List<S3ObjectSummary> images = null;
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    ApiCmpyToken cmpyToken = ApiCmpyToken.findByCmpySrcId(tokenName, BkApiSrc.getId(BkApiSrcIdConstants.S3_IMG_SEARCH));
    if(cmpyToken != null) {
      String token = cmpyToken.getToken(); //userid:pwd@bucketname/prefixes with bucketname containing slashes and colon being delimiter between access key & secret key.  anything before 1st slash is bucketname.  anything after is prefix
      String s3Bucket = token.substring(token.indexOf("@") + 1, token.indexOf("/")).trim();
      String s3Key = token.substring(0, token.indexOf(":")).trim();
      String s3Pwd = token.substring(token.indexOf(":") + 1, token.indexOf("@")).trim();
      String s3Prefix = token.substring(token.indexOf("/") + 1, token.length()).trim();

      BasicAWSCredentials cre = new BasicAWSCredentials(s3Key, s3Pwd);
      AmazonS3 s3 = new AmazonS3Client(cre);

      ObjectListing listing   = s3.listObjects(s3Bucket, s3Prefix);
      ZoneId zoneId = ZoneId.systemDefault();
      long currentTime = LocalDateTime.now().atZone(zoneId).toEpochSecond();

      if(!listing.isTruncated()) {
        for(S3ObjectSummary s3o: listing.getObjectSummaries()) {
          String fileName = s3o.getKey().toLowerCase();
          if((fileName.endsWith("jpg") || fileName.endsWith("jpeg") || fileName.endsWith("png"))
                  && (!s3o.getBucketName().toLowerCase().contains("indagare")
                  || (s3o.getBucketName().toLowerCase().contains("indagare") && !s3o.getKey().matches(".*[0-9]{2,4}x[0-9]{2,4}.(jpg|png|jpeg)")))) {
            if (S3ImgIndex.findByFilenameOrig(s3o.getBucketName(), s3o.getKey()).size() > 0) {
              S3ImgIndex imgInfo = S3ImgIndex.findByFilenameOrig(s3o.getBucketName(), s3o.getKey()).get(0);
              imgInfo.setModifiedTs(currentTime);
              imgInfo.update();
            } else {
              S3ImgIndex newImgInfo = new S3ImgIndex();
              newImgInfo.setPk(DBConnectionMgr.getUniqueLongId());
              newImgInfo.setBucket(s3o.getBucketName());
              newImgInfo.setFileNameOrig(s3o.getKey());
              newImgInfo.setFileNameLower(s3o.getKey().toLowerCase());
              newImgInfo.setModifiedTs(currentTime);
              newImgInfo.insert();
            }
          }
        }
      } else {
        while (listing.isTruncated()) {
          listing = s3.listNextBatchOfObjects (listing);
          for(S3ObjectSummary s3o: listing.getObjectSummaries()) {
            String fileName = s3o.getKey().toLowerCase();
            if((fileName.endsWith("jpg") || fileName.endsWith("jpeg") || fileName.endsWith("png"))
                    && (!s3o.getBucketName().toLowerCase().contains("indagare")
                    || (s3o.getBucketName().toLowerCase().contains("indagare") && !s3o.getKey().matches(".*[0-9]{2,4}x[0-9]{2,4}.(jpg|png|jpeg)")))) {
              if (S3ImgIndex.findByFilenameOrig(s3o.getBucketName(), s3o.getKey()).size() > 0) {
                S3ImgIndex imgInfo = S3ImgIndex.findByFilenameOrig(s3o.getBucketName(), s3o.getKey()).get(0);
                imgInfo.setModifiedTs(currentTime);
                imgInfo.update();
              } else {
                S3ImgIndex newImgInfo = new S3ImgIndex();
                newImgInfo.setPk(DBConnectionMgr.getUniqueLongId());
                newImgInfo.setBucket(s3o.getBucketName());
                newImgInfo.setFileNameOrig(s3o.getKey());
                newImgInfo.setFileNameLower(s3o.getKey().toLowerCase());
                newImgInfo.setModifiedTs(currentTime);
                newImgInfo.insert();
              }
            }
          }
        }
      }

      int result = S3ImgIndex.deleteByTimestamp(s3Bucket, currentTime);
      Log.debug("# of deleted img indexes: " + result);
    }

    return ok(views.html.experimental.indexS3.render(view));
  }

  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public Result parseTxt() {
    ExpPDFParserView view = new ExpPDFParserView();

    //Form<ExperimentalPDFUploadForm> pdfForm = formFactory.form(ExperimentalPDFUploadForm.class);

    //ExperimentalPDFUploadForm pdfFileInfo = pdfForm.bindFromRequest().get();

    //Log.log(LogLevel.DEBUG, "ParsePDF Controller: Filename:", pdfFileInfo.toString());


    view.isUMappedAdmin = true;                     //TODO: Figure out what this actually does and how it gets populated
    view.userId = "admin";                          //TODO: Figure out how to properly populate this property
    view.isUploaded = true;                        //TODO: This has to be switching upload vs parse view
    view.message = "Work in progress";

    try {

      ClientbaseExtractor extractor = new ClientbaseExtractor();
      TripVO tvo = extractor.extractData(null);
      view.pdfText = extractor.toString();
      view.pdfText += tvo.toString();
    }
    catch (Exception e) {
      Log.log(LogLevel.ERROR, "Failed to grab file from S3");
      Log.log(LogLevel.ERROR, e.getMessage());
      StackTraceElement trace[] = e.getStackTrace();
      for (StackTraceElement t : trace) {
        Log.log(LogLevel.ERROR, t.toString());
      }
    }

    return ok(views.html.experimental.pdfparser.render(view));
  }

  @With({Authenticated.class, Authorized.class})
  public Result uploadPDF() {

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    //bind html form to form bean
    Form<ExperimentalPDFUploadForm> form = formFactory.form(ExperimentalPDFUploadForm.class).bindFromRequest();
    if(form.hasErrors()) {
      return BaseJsonResponse.formError(form);
    }

    ExperimentalPDFUploadForm pdfFileInfo = form.get();
    if (pdfFileInfo.inUserId == null ||
        pdfFileInfo.inPDFFileName == null ||
        pdfFileInfo.inPDFFileName.length() == 0) {
      return BaseJsonResponse.codeResponse(ReturnCode.HTTP_REQ_MISSING_DATA);
    }

    UserProfile u = UserProfile.find.byId(pdfFileInfo.inUserId);
    if (u != null) {
      if (sessionMgr.getUserId().equals(u.getUserid()) || SecurityMgr.isUmappedAdmin(sessionMgr)) {
        try {
          String id = Utils.getUniqueId();
          String filename = "pdf/" + id + "_" + pdfFileInfo.inPDFFileName;

          String signedPutUrl = S3Util.authorizeS3Put(filename);
          S3Data s3Data = S3Util.authorizeS3(filename);

          String nextUrl = "/exp/pdfparser?inUserId=" + pdfFileInfo.inUserId + "&uploaded=true&inPDFFileName=" +
                           filename; //TODO: Must be done via reverse route

          ObjectNode result = Json.newObject();
          result.put("signed_request", signedPutUrl);
          result.put("url", nextUrl);
          result.put("policy", s3Data.policy);
          result.put("accessKey", s3Data.accessKey);
          result.put("key", s3Data.key);
          result.put("s3Bucket", s3Data.s3Bucket);
          result.put("signature", s3Data.signature);

          Log.log(LogLevel.INFO, "JSON Reply: ", result.toString());

          return ok(result);

        }
        catch (Exception e) {
          //TODO: What kind of error handling should happen here?
          BaseView baseView = new BaseView();
          baseView.message = "System Error - S3 Error. Please report to Umapped.";
          return ok(views.html.common.message.render(baseView));
        }
      }
    }

    ObjectNode result = Json.newObject();
    result.put("msg", "Error - Unknown Error");
    return ok(result);
  }

  @With({Authenticated.class, Authorized.class})
  public Result audit() {
    //TODO: Deprecate since trip passenger is now different
    ExpAuditView eav = new ExpAuditView();

    String getInTripId = "434714411305032714";
    String getInEmail = "msergei+umapped@gmail.com";
    String getInName = "Sergei";

    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());
    Trip trip = Trip.findByPK(getInTripId);
    TripPassenger passenger;

    if (SecurityMgr.getTripAccessLevel(trip, sessionMgr).ge(SecurityMgr.AccessLevel.ADMIN)) {
      //check for group
      TripGroup group = null;
      List<TripPassenger> passengers = TripPassenger.findActiveByTripEmail(getInTripId, getInEmail);

      if (passengers == null || passengers.size() == 0) {
        passenger = new TripPassenger();
        passenger.setComments("");
        passenger.setCreatedby(sessionMgr.getUserId());
        passenger.setCreatedtimestamp(System.currentTimeMillis());
        passenger.setEmail(getInEmail);
        passenger.setLastupdatedtimestamp(System.currentTimeMillis());
        passenger.setModifiedby(sessionMgr.getUserId());
        passenger.setName(getInName);
        passenger.setPk(DBConnectionMgr.getUniqueId());
        passenger.setStatus(APPConstants.STATUS_ACTIVE);
        passenger.setTripid(trip.tripid);
        passenger.save();
      }
    }

    TripAudit ta = TripAudit.buildRecord(AuditModuleType.TRIP_TRAVELLER, AuditActionType.ADD, AuditActorType.WEB_USER)
                            .withTrip(trip)
                            .withCmpyid(trip.cmpyid)
                            .withUserid(sessionMgr.getUserId());

    ((AuditTripTraveller) ta.getDetails()).withEmail(getInEmail)
                                          .withName(getInName)
                                          .withId("Unset")
                                          .withText("User Added");
    ta.save();;

    return ok(views.html.experimental.audit.render(eav));
  }

  @With({Authenticated.class, Authorized.class})
  public Result colabEmail() {
    SessionMgr sessionMgr = SessionMgr.fromContext(ctx());

    String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);

    final DynamicForm form = formFactory.form().bindFromRequest();

    Trip trip = new Trip();
    trip.setName("Demo Trip Name For Email Test Purposes");

    UserProfile invitedUser = new UserProfile();
    invitedUser.setFirstname("John");
    invitedUser.setLastname("Doe");

    String note = form.get("inNote");
    if (note == null) {
      note = "";
    }
    TripShare tripShare = TripShare.buildTripShare(sessionMgr.getUserId(), trip, invitedUser, note, SecurityMgr.AccessLevel.READ);

    if(true) {
      throw new NotImplementedException("Old Version - needs to be converted to Account");
    }
    UserProfile senderUserProfile = new UserProfile();
    senderUserProfile.setFirstname("Jane");
    senderUserProfile.setLastname("Roe");

    return ok(views.html.trip.collaborate.emailBodyShare.render(trip,
                                                                tripShare,
                                                                senderUserProfile,
                                                                hostUrl));
  }

  @With({Authenticated.class, Authorized.class})
  public Result colabInviteEmail() {

    String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);

    final DynamicForm form = formFactory.form().bindFromRequest();

    Trip trip = new Trip();
    trip.setName("Demo Trip Name For Email Test Purposes");

    SessionMgr sessionMgr = new SessionMgr(session());
    UserInvite userInvite = new UserInvite();
    userInvite.setFirstname("John");
    userInvite.setLastname("Doe");

    String note = form.get("inNote");
    if (note == null) {
      note = "";
    }

    TripInvite tripInvite = TripInvite.buildTripInvite(sessionMgr.getUserId(),
                                                       trip,
                                                       userInvite,
                                                       SecurityMgr.AccessLevel.READ,
                                                       note);

    UserProfile senderUserProfile = new UserProfile();
    senderUserProfile.setFirstname("Jane");
    senderUserProfile.setLastname("Roe");


    BusinessCardView bcv = new BusinessCardView();
    bcv.setFirstName("Jane");
    bcv.setLastName("Roe");
    bcv.companyLogoUrl = "https://s3.amazonaws" +
                         ".com/umapped_prd/124addde1d98b85e44846ac93d2a33c8cc1492861892ec0bc864fa6109fff326_B%20R" +
                         "%20logo.png?Expires=2005579011&AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Signature=SaIjHfSj3" +
                         "%2FCwJP5e1hc%2FX5bRxJ0%3D";
    bcv.companyUrl = "http://www.umapped.com";
    bcv.email = "jane@umapped.com";
    bcv.phone = "+1.416.987.1234";
    bcv.socialTwitterUrl = "https://twitter.com/microsoft";
    bcv.socialGoogleUrl = "https://plus.google.com/111221966647232053570/posts";
    bcv.socialInstagramUrl = "http://instagram.com/xbox";

    if(true) {
      throw new NotImplementedException("Old Version - needs to be converted to Account");
    }

    return ok(views.html.trip.collaborate.emailBodyInvite.render(bcv,
                                                                 trip,
                                                                 tripInvite,
                                                                 senderUserProfile,
                                                                 "Megacorp Inc.",
                                                                 hostUrl));
  }

  @With({Authenticated.class, Authorized.class})
  public Result newUserEmail() {

    String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);

    final DynamicForm form = formFactory.form().bindFromRequest();

    NewRegistrationView newRegistrationView = new NewRegistrationView();
    newRegistrationView.userId = "john.doe";
    newRegistrationView.pwdResetLink = hostUrl;


    return ok(views.html.email.registrationconfirm.render(newRegistrationView));
  }

  @With({Authenticated.class, Authorized.class})
  public Result msgEmail() {
    CommunicatorMessageView cmv = new CommunicatorMessageView();

    cmv.fromEmail = "serguei@umapped.com";
    cmv.fromName = "Serguei Moutovkin";
    cmv.id = "i.123123asdasd.t.akljkjASD";
    cmv.message = "Hello everyone.<br/>This is a long message with some <b>bold</b> text.";
    cmv.time = "12:30PM UTC";
    cmv.webItineraryUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL) +
                          controllers.routes.WebItineraryController.index("986773789630065410","986786259650065410", 0L)
                                                                 .url();
    cmv.tripName = "Very long trip name when people taravel for long time in November 1, 2017";


    return ok(views.html.email.communicatorMessage.render(cmv));
  }

  @With({Authenticated.class, Authorized.class})
  public Result publishEmail() {

    String hostUrl = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.HOST_URL);

    final DynamicForm form = formFactory.form().bindFromRequest();

    EmailView ev = new EmailView();
    ev.tripInfo = new TripInfoView();
    ev.tripInfo.tripName = "CaLiFoRnIa DrEamInG";
    ev.tripInfo.tripStartDatePrint = Utils.formatDatePrint(1396765795327l);
    ev.tripInfo.tripEndDatePrint = Utils.formatDatePrint(1416765795327l);
    ev.tripInfo.cover = new ImageView();
    ev.tripInfo.url = "http://static-umapped-com.s3.amazonaws" +
                           ".com/public/lib/web-itinerary-splash/images/default_splash.jpg";
    ev.tripInfo.agentName = "John Doe";
    String note = form.get("inNote");
    if (note != null) {
      ev.note = note;
    }

    String updated = form.get("updated");
    if (updated != null && updated.equalsIgnoreCase("true")) {
      ev.updated = true;
    }
    else {
      ev.updated = false;
    }

    ev.businessCard = new BusinessCardView();
    ev.businessCard.setFirstName("Jane");
    ev.businessCard.setLastName("Roe");
    ev.businessCard.companyName = "Megacorp Inc.";
    ev.businessCard.companyLogoUrl = "https://s3.amazonaws" + "" +
                                     ".com/umapped_prd/124addde1d98b85e44846ac93d2a33c8cc1492861892ec0bc864fa6109fff326_B%20R%20logo.png?Expires=2005579011&AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Signature=SaIjHfSj3%2FCwJP5e1hc%2FX5bRxJ0%3D";
    ev.businessCard.companyUrl = "http://www.umapped.com";
    ev.businessCard.email = "jane@umapped.com";
    ev.businessCard.phone = "+1.416.987.1234";
    ev.businessCard.socialTwitterUrl = "https://twitter.com/microsoft";
    ev.businessCard.socialGoogleUrl = "https://plus.google.com/111221966647232053570/posts";
    ev.businessCard.socialInstagramUrl = "http://instagram.com/xbox";
    ev.businessCard.socialYouTubeUrl = "https://www.youtube.com/channel/UChZZdw7e8WLOGJdRXgTUOYQ";
    ev.businessCard.socialFacebookUrl = "https://www.facebook.com/ApocalypseNowMovie";
    ev.businessCard.socialLinkedInUrl = "http://www.linkedin.com/in/williamhgates";


    String type = form.get("type");
    if (type != null && type.equalsIgnoreCase("agent")) {
      return ok(views.html.email.agent.render(ev));
    }
    else {
      return ok(views.html.email.trip.render(ev));
    }
  }

  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public CompletionStage<Result> loadPoi() {
    boolean DEBUG = false;

    final DynamicForm form = formFactory.form().bindFromRequest();
    final String url = form.get("inUrl");
    final String cmpyId = form.get("inCmpyId");
    String mode = form.get("inMode");
    final String nl = System.getProperty("line.separator");
    Company cmpy1 = null;
    if (cmpyId != null) {
      cmpy1 = Company.find.byId(cmpyId);
    }
    final Company cmpy = cmpy1;

    if (url != null && url.toLowerCase().startsWith("http") && cmpy != null && mode != null && mode.equals("YES")) {

      CompletionStage<UploadView> promiseOfView = CompletableFuture.supplyAsync(() -> {
        UploadView view = new UploadView();
        view.cmpyId = cmpyId;
        view.url = url;
        StringBuffer sb = new StringBuffer();

        try {

          InputStream input = new URL(url).openStream();
          Reader reader = new InputStreamReader(input);
          CSVReader csvReader = new CSVReader(reader);
          String[] nextLine;
          HashMap<String, String> imgFiles = null;

          int i = 0;
          int insertCount = 0;
          int updateCount = 0;
          int failedCount = 0;

          while ((nextLine = csvReader.readNext()) != null) {

            i++;
            if (nextLine.length != VENDOR_COL.values().length) {
              sb.append("SKIP LINE - " + nextLine.toString() + " - ERROR: WRONG COLUMN COUNT" + nl);
            }

            String type = Utils.trim(nextLine[VENDOR_COL.TYPE.ordinal()]);
            PoiTypeInfo.PoiType poiType = PoiTypeInfo.Instance().byName(type);
            String name = Utils.trim(nextLine[VENDOR_COL.NAME.ordinal()]);

            if (poiType == null || name == null || name.length() == 0) {
              sb.append("SKIP LINE - " + nextLine.toString() + "\n");
              continue;
            }
            boolean removeAllFiles = false;

            String addr1 = Utils.trim(nextLine[VENDOR_COL.ADDR1.ordinal()]);      //Set
            String addr2 = Utils.trim(nextLine[VENDOR_COL.ADDR2.ordinal()]);      //Set
            String city = Utils.trim(nextLine[VENDOR_COL.CITY.ordinal()]);        //Set
            String zip = Utils.trim(nextLine[VENDOR_COL.ZIP.ordinal()]);          //Set
            String state = Utils.trim(nextLine[VENDOR_COL.STATE.ordinal()]);      //Set
            String country = Utils.trim(nextLine[VENDOR_COL.COUNTRY.ordinal()]);  //Set
            String lat = Utils.trim(nextLine[VENDOR_COL.LAT.ordinal()]);          //Set
            String lng = Utils.trim(nextLine[VENDOR_COL.LONG.ordinal()]);         //Set
            String email = Utils.trim(nextLine[VENDOR_COL.EMAIL.ordinal()]);
            String web = Utils.trim(nextLine[VENDOR_COL.WEB.ordinal()]);
            String phone = Utils.trim(nextLine[VENDOR_COL.PHONE.ordinal()]);
            String fax = Utils.trim(nextLine[VENDOR_COL.FAX.ordinal()]);
            String facebook = Utils.trim(nextLine[VENDOR_COL.FACEBOOK.ordinal()]);
            String twitter = Utils.trim(nextLine[VENDOR_COL.TWITTER.ordinal()]);
            String imgs = Utils.trim(nextLine[VENDOR_COL.IMG.ordinal()]);
            String tag = Utils.trim(nextLine[VENDOR_COL.TAG.ordinal()]);
            String note = Utils.trim(nextLine[VENDOR_COL.DESC.ordinal()]);
            //for ski.com, we explicity ignore everything not set
            addr1 = " ";// (addr1.isEmpty()?" " : addr1);
            addr2 = "";// (addr2.isEmpty()?" " : addr2);
            city = (city.isEmpty()?"" : city);
            zip = "";//(zip.isEmpty()?" " : zip);
            state = (state.isEmpty()?"" : state);
            country = (country.isEmpty()?"UNK" : country);
            email = " ";// (email.isEmpty()?" " : email);
            web = " ";// (web.isEmpty()?" " : web);
            phone = " ";// (phone.isEmpty()?" " : phone);
            fax = " ";// (fax.isEmpty()?" " : fax);
            facebook = " ";// (facebook.isEmpty()?" " : facebook);
            twitter = " ";// (twitter.isEmpty()?" " : twitter);
            note = " ";

            imgFiles = getFiles(imgs);

            try {
              Ebean.beginTransaction();
              PoiRS prs = null;
              PoiRS defaultPrs = null;

              boolean isNewPoi = true;

              Set<Integer> mergeCmpy = new HashSet<>();
              if (cmpy.getCmpyId() != Company.PUBLIC_COMPANY_ID) {
                mergeCmpy.add(cmpy.getCmpyId());
              }
             // mergeCmpy.add(Company.PUBLIC_COMPANY_ID);

              List<Integer> types = new ArrayList<Integer>();
              types.add(poiType.getId());

              String term = name. replace(",", " ");
              if (city != null && city.trim().length() > 0) {
                term += ", " + city;
              }

              List<PoiRS> poisQuery = PoiController.findMergedByTag(tag, poiType.getId(), 883, 1000, false) ;//PoiController.findMergedByTerm(name,types, mergeCmpy, 2);//PoiController.findMergedByTermForCmpy(name, types.get(0), cmpy.getCmpyId(), 2); //PoiController.findMergedByTerm(name,types, mergeCmpy, 2);

              PoiRS poi = null;
              if (poisQuery != null) {
                for (PoiRS p: poisQuery) {
                  if (p.data != null && p.data.getTags() != null && p.getCmpyId() == 883) {
                    for (String t: p.data.getTags()) {
                      if (t.equals(tag)) {
                        poi = p;
                        break;
                      }
                    }
                  }
                }
              }
              /*
              if (poi == null) {
                PoiSearchHelper psh = PoiSearchHelper.build()
                        .setTermSmart(term)
                        .setType(poiType.getId())
                        .setWithPublic(false) // Only false for ski.com
                        .setCompanyId(cmpy.getCmpyId())
                        .setLimit(30)
                        .search()
                        .rank();

                poi = psh.getBestFit();
              }
              */
              List<PoiRS> pois = new ArrayList<>();
              if (poi != null) {
                pois.add(poi);
                if (city != null && city.trim().length() > 0 && poi.getMainAddress() != null && poi.getMainAddress().getLocality() != null && poi.getMainAddress().getLocality().trim().compareToIgnoreCase(city.trim()) != 0) {
                  Log.debug("SKI-LOAD: Matched - " + name + " Line: " + i + " City: " + city + " --- " + tag + " :: " + poi.getId() + " - " + poi.getName() + " - " + poi.getTags());
                } else {
                  Log.debug("SKI-LOAD: Matched - " + name + " Line: " + i + " City: " + city + " --- " + tag + " :: " + poi.getId() + " - " + poi.getName() + " - " + poi.getMainAddress().getLocality() + " - " + poi.getTags());
                }
              } else {
                Log.debug("SKI-LOAD: NEW - " + name + " Line: " + i + " City: " + city + " --- " + tag);
              }

/*
              if (poi != null) {
                if (name.compareToIgnoreCase(poi.getName()) != 0) {
                  if (poi.getMainAddress() != null && poi.getMainAddress().getLocality() != null) {
                    Log.debug("SKI-LOAD: MisMatched - " + name + " Line: " + i + " City: " + city + " --- " + poi.getId() + " - " + poi.getName() + " - " + poi.getMainAddress().getLocality());
                  } else {
                    Log.debug("SKI-LOAD: MisMatched - " + name + " Line: " + i + " City: " + city + " --- " + poi.getId() + " - " + poi.getName());

                  }
                } else if(poi.data != null && poi.data.getTags() != null && poi.data.getTags().size() == 1 && poi.data.getTags().contains(tag.trim())) {
                  if (city != null && city.trim().length() > 0 && poi.getMainAddress() != null && poi.getMainAddress().getLocality() != null && poi.getMainAddress().getLocality().trim().compareToIgnoreCase(city.trim()) != 0) {
                    Log.debug("SKI-LOAD: Matched - " + name + " Line: " + i + " City: " + city + " --- " + poi.getId() + " - " + poi.getName());
                  } else {
                    Log.debug("SKI-LOAD: Matched - " + name + " Line: " + i + " City: " + city + " --- " + poi.getId() + " - " + poi.getName() + " - " + poi.getMainAddress().getLocality());
                  }
                  pois.add(poi);
                } else if (city != null && city.trim().length() > 0) {
                  //make sure we match the city
                  if (poi.getMainAddress() != null && poi.getMainAddress().getLocality() != null && poi.getMainAddress().getLocality().trim().compareToIgnoreCase(city.trim()) == 0) {
                    if (poi.data != null && poi.data.getTags() != null && poi.data.getTags().size() > 1) {
                      removeAllFiles = true;
                      if (poi.getMainAddress() != null && poi.getMainAddress().getLocality() != null) {
                        Log.debug("SKI-LOAD: Matched - " + name + " Line: " + i + " City: " + city + " --- " + poi.getId() + " - " + poi.getName() + " - " + poi.getMainAddress().getLocality());
                      } else {
                        Log.debug("SKI-LOAD: Matched - " + name + " Line: " + i + " City: " + city + " --- " + poi.getId() + " - " + poi.getName());

                      }
                    }
                    pois.add(poi);
                  }

                } else {
                  if (poi.data != null && poi.data.getTags() != null && poi.data.getTags().size() > 1) {
                    removeAllFiles = true;
                    if (poi.getMainAddress() != null && poi.getMainAddress().getLocality() != null) {
                      Log.debug("SKI-LOAD: Matched - " + name + " Line: " + i + " City: " + city + " --- " + poi.getId() + " - " + poi.getName() + " - " + poi.getMainAddress().getLocality());
                    } else {
                      Log.debug("SKI-LOAD: Matched - " + name + " Line: " + i + " City: " + city + " --- " + poi.getId() + " - " + poi.getName());

                    }
                  }
                  pois.add(poi);
                }

              }
*/

              if (pois.size() > 0) {
                for (PoiRS p: pois) {
                  if (p.getCmpyId() == cmpy.getCmpyId() ) {
                    prs = p;
                    isNewPoi = false;
                    break;
                  } else if (p.getCmpyId() == Company.PUBLIC_COMPANY_ID && p.getName() != null && p.getName().trim().compareToIgnoreCase(name) == 0) {
                    defaultPrs = p;
                  }

                }
              }

              if (isNewPoi) {
                if (defaultPrs == null) {
                  prs = PoiRS.buildRecord(Consortium.INDEPENDENT_CONSORTIUM_ID,
                                          cmpy.getCmpyId(),
                                          poiType.getId(),
                                          FeedSourcesInfo.byName("Web"),
                                          "system");
                } else {
                  prs = PoiRS.buildRecord(defaultPrs.data, Consortium.INDEPENDENT_CONSORTIUM_ID, cmpy.getCmpyId(), poiType.getId(), FeedSourcesInfo.byName("Web"), "system");
                }
              }
              prs.updateLastUpdatedTS("system");
              prs.setName(name);
              prs.data.setName(name);
              prs.data.deleteTags();
              prs.setTags(tag);
              prs.data.addTags(tag);
              Address address = new Address();
              address.setAddressType(AddressType.MAIN);

              if (lat != null && lat.length() > 1 &&
                  lng != null && lng.length() > 1 &&
                  !((lat.equals("0") || lat.equals("-1")) && (lng.equals("0") || lng.equals("-1")))) {
                try {
                  float locLat = Float.parseFloat(lat);
                  float locLng = Float.parseFloat(lng);
                  Coordinates coords = new Coordinates();
                  coords.setLatitude(locLat);
                  coords.setLongitude(locLng);
                  address.setCoordinates(coords);
                }
                catch (NumberFormatException ne) {
                  Log.err("Missing coordinates for import poi:" + name);
                }
              }

              if (addr2 != null && addr2.trim().length() > 0) {
                address.setStreetAddress(addr1 + "\n" + addr2);
              } else {
                address.setStreetAddress(addr1);
              }
              address.setLocality(city);
              address.setRegion(state);
              address.setPostalCode(zip);
              CountriesInfo.Country cntry = CountriesInfo.Instance().searchByName(country);
              if (cntry != null) {
                address.setCountryCode(cntry.getAlpha3());
              }
              else {
                address.setCountryCode("UNK");
                Log.info("Failed to covert country name to country code. Name is:" + country);
              }

              prs.setMainAddress(address);
              if (phone != null) {
                PhoneNumber pn = new PhoneNumber();
                pn.setPhoneType(PhoneNumber.PhoneType.MAIN);
                pn.setNumber(phone);
                prs.setMainPhoneNumber(pn);
              }

              if (web != null) {
                if (prs.data.getUrls() != null) {
                  for (Iterator<String> it = prs.data.getUrls().iterator(); it.hasNext(); ) {
                    String currItem = it.next();
                    it.remove();
                  }
                }
                prs.data.addUrl(web);
              }

              if (twitter != null) {
                Set<SocialLink>  links       = prs.data.getSocialLinks();
                List<SocialLink> toberemoved = new ArrayList<SocialLink>();
                for (SocialLink s:  links) {
                  if (s.getServiceName() != null && s.getServiceName().equals("Twitter")) {
                    toberemoved.add(s);
                  }
                }
                for (SocialLink s: toberemoved) {
                  links.remove(s);
                }


                SocialLink link = new SocialLink();
                link.setServiceName("Twitter");
                link.setServiceDirectUrl(twitter);
                link.setServiceUsername("");

                links.add(link);

                prs.data.setSocialLinks(links);
              }

              if (facebook != null) {
                Set<SocialLink> links = prs.data.getSocialLinks();
                List<SocialLink> toberemoved = new ArrayList<SocialLink>();
                for (SocialLink s:  links) {
                  if (s.getServiceName() != null && s.getServiceName().equals("Facebook")) {
                    toberemoved.add(s);
                  }
                }
                for (SocialLink s: toberemoved) {
                  links.remove(s);
                }

                SocialLink link = new SocialLink();
                link.setServiceName("Facebook");
                link.setServiceDirectUrl(facebook);
                link.setServiceUsername("");

                links.add(link);

                prs.data.setSocialLinks(links);
              }

              if (email != null) {
                Email email1 = new Email();
                email1.setAddress(email);
                email1.setEmailType(Email.EmailType.MAIN);
                prs.data.addEmail(email1);
              }

              if (note != null) {
                prs.data.setDesc(note);
              }

              if (!DEBUG) {
                if (isNewPoi) {
                  Log.debug("SKI-LOAD: New - " + name + " Line: " + i + " City: " +  city);
                  PoiMgr.save(prs);
                  insertCount++;
                } else {
                  if (poi.getMainAddress() != null && poi.getMainAddress().getLocality() != null) {
                    Log.debug("SKI-LOAD: Matched - " + name + " Line: " + i + " City: " +  city + " --- " + poi.getId() + " - " + poi.getName() + " - " + poi.getMainAddress().getLocality());
                  } else {
                    Log.debug("SKI-LOAD: Matched - " + name + " Line: " + i + " City: " +  city + " --- " + poi.getId() + " - " + poi.getName());

                  }
                  PoiMgr.update(prs);
                  updateCount++;
                }
              } else {
                if (isNewPoi) {
                  System.out.println("New - " + name + " Line: " + i + " City: " +  city);
                  insertCount++;
                } else {

                  updateCount++;
                }
              }
              Account account = Account.find.byId(Account.AID_PUBLISHER); //Since admin is loading, it is system

              List<PoiFile> poiFiles = PoiFile.getCmpyForPoi(prs.getId(), prs.getCmpyId());
              if (poiFiles != null && poiFiles.size() > 0) {
                if (poiFiles.size() > 16) {
                  removeAllFiles = true;
                  Log.debug("SKI-LOAD: REMOVE FILES - " + poiFiles.size() + " - " + name + " Line: " + i + " City: " + city + " -Tag: " + tag + " --- " + poi.getId() + " - " + poi.getName() + "\n");
                }
                if (!DEBUG && removeAllFiles) {
                  for (PoiFile poiFile: poiFiles) {
                    if (poiFile.getCmpyId() == 883 && removeAllFiles) {
                      poiFile.setModifiedby("system");
                      poiFile.setState(RecordState.DELETED);
                      poiFile.setLastupdatedtimestamp(Instant.now().toEpochMilli());
                      poiFile.save();
                      PoiController.decrementFileCount(prs.getId(), prs.getCmpyId(), "system");
                    }
                  }
                  poiFiles = PoiFile.getCmpyForPoi(prs.getId(), prs.getCmpyId());
                }
              }

              if (imgFiles != null && !imgFiles.isEmpty()) {
                for (String img : imgFiles.keySet()) {
                  String fileName = imgFiles.get(img);
                  boolean found = false;
                  if (poiFiles != null) {
                    for (PoiFile poiFile: poiFiles) {
                      if (poiFile.getName() != null && poiFile.getName().compareToIgnoreCase(img) == 0) {
                        found = true;
                        break;
                      }
                    }
                  }
                  if (found) {
                    Log.debug("SKI-LOAD: LOAD::Existing image: " + prs.getId() + " == " +   prs.getName() + " == "+ img );
                  } else {
                    Log.debug("SKI-LOAD: LOAD::NEW IMAGE: " + prs.getId() + " == " +   prs.getName() + " == "+ img);
                  }


                  if (fileName != null && !found && !DEBUG) {
                    try {
                      //save the image to s3 and set as
                      // cover

                      FileImage image = ImageController.downloadAndSaveImage(img, fileName,
                                                                             FileSrc.FileSrcType.IMG_VENDOR_UMAPPED,
                                                                             null, account, false);


                      if (image != null) {
                        PoiFile pf = PoiFile.buildPoiFile(prs.getId(), prs.getCmpyId(), img, "",
                                                          Account.LEGACY_PUBLISHER_USERID);
                        pf.setFileImage(image);
                        pf.setNameSys(S3Util.normalizeFilename(image.getFile().getFilename()));
                        pf.setSrcId(FeedSourcesInfo.byName("Web"));
                        pf.setState(RecordState.ACTIVE);
                        pf.setExtension(image.getFile().getFiletype());
                        pf.setUrl(image.getUrl());
                        PoiController.incrementFileCount(prs.getId(), prs.getCmpyId(), "system");
                        pf.save();
                      }
                    }
                    catch (Exception e1) {
                      sb.append("Error Saving Photo - " + nextLine.toString() + "\n");
                      Log.err("VENDOR LOAD::Error save Photo: Line " +
                                         "number: " + i);
                      e1.printStackTrace();
                    }
                  }
                }
              }

              Ebean.commitTransaction();

            }
            catch (Exception e) {
              Ebean.rollbackTransaction();
              failedCount++;
              sb.append("Error: " + nextLine.toString() + "\n");
              e.printStackTrace();
            }
          }
          sb.append("Read Count: " + i + "\n");
          sb.append("Insert Count: " + insertCount +
                    "\n");
          sb.append("Update Count: " + updateCount +
                    "\n");
          sb.append("Failed Count: " + failedCount +
                    "\n");
          view.message = sb.toString();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        return view;
      });
      return promiseOfView.thenApplyAsync((UploadView view) -> ok(views.html.experimental.parseVendor.render(view)));
    }
    else {
      UploadView view = new UploadView();
      view.cmpyId = cmpyId;
      view.url = url;
      return CompletableFuture.completedFuture(ok(views.html.experimental.parseVendor.render(view)));
    }
  }

  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public CompletionStage<Result> loadVirtuosoPoi() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    final String url = form.get("inUrl");
    final String cmpyId = form.get("inCmpyId");
    String mode = form.get("inMode");
    final String nl = System.getProperty("line.separator");

    Company cmpy1 = null;
    if (cmpyId != null) {
      cmpy1 = Company.find.byId(cmpyId);
    }
    final Company cmpy = cmpy1;

    if (url != null && url.toLowerCase().startsWith("http") && cmpy != null && mode != null && mode.equals("YES")) {

      CompletionStage<UploadView> promiseOfView =  CompletableFuture.supplyAsync(() -> {

        UploadView view = new UploadView();
        view.cmpyId = cmpyId;
        view.url = url;
        StringBuffer sb = new StringBuffer();

        try {
          InputStream input = new URL(url).openStream();
          Reader reader = new InputStreamReader(input);
          CSVReader csvReader = new CSVReader(reader);
          String[] nextLine;
          HashMap<String, String> imgFiles = null;

          int i = 0;
          int insertCount = 0;
          int updateCount = 0;
          int failedCount = 0;


          while ((nextLine = csvReader.readNext()) != null) {

            i++;
            if (nextLine.length != VENDOR_COL.values().length) {
              sb.append("SKIP LINE - " + nextLine.toString() + " - ERROR: WRONG COLUMN COUNT" + nl);
            }


            String type = Utils.trim(nextLine[VENDOR_COL.TYPE.ordinal()]);
            PoiTypeInfo.PoiType poiType = PoiTypeInfo.Instance().byName(type);
            String name = Utils.trim(nextLine[VENDOR_COL.NAME.ordinal()]);

            if (name == null || name.length() == 0) {
              sb.append("SKIP LINE - " + nextLine.toString() + "\n");
              continue;
            }


            String addr1 = Utils.trim(nextLine[VENDOR_COL.ADDR1.ordinal()]);      //Set
            String addr2 = Utils.trim(nextLine[VENDOR_COL.ADDR2.ordinal()]);      //Set
            String city = Utils.trim(nextLine[VENDOR_COL.CITY.ordinal()]);        //Set
            String zip = Utils.trim(nextLine[VENDOR_COL.ZIP.ordinal()]);          //Set
            String state = Utils.trim(nextLine[VENDOR_COL.STATE.ordinal()]);      //Set
            String country = Utils.trim(nextLine[VENDOR_COL.COUNTRY.ordinal()]);  //Set
            String lat = Utils.trim(nextLine[VENDOR_COL.LAT.ordinal()]);          //Set
            String lng = Utils.trim(nextLine[VENDOR_COL.LONG.ordinal()]);         //Set
            String email = Utils.trim(nextLine[VENDOR_COL.EMAIL.ordinal()]);
            String web = Utils.trim(nextLine[VENDOR_COL.WEB.ordinal()]);
            String phone = Utils.trim(nextLine[VENDOR_COL.PHONE.ordinal()]);
            String fax = Utils.trim(nextLine[VENDOR_COL.FAX.ordinal()]);
            String facebook = Utils.trim(nextLine[VENDOR_COL.FACEBOOK.ordinal()]);
            String twitter = Utils.trim(nextLine[VENDOR_COL.TWITTER.ordinal()]);
            String imgs = Utils.trim(nextLine[VENDOR_COL.IMG.ordinal()]);
            String tag = Utils.trim(nextLine[VENDOR_COL.TAG.ordinal()]);
            String note = Utils.trim(nextLine[VENDOR_COL.DESC.ordinal()]);

            imgFiles = getVirtuosoFiles(imgs);

            try {
              Ebean.beginTransaction();
              PoiRS prs = null;
              PoiRS defaultPrs = null;

              boolean isNewPoi = true;

              Set<Integer> mergeCmpy = new HashSet<>();
              if (cmpy.getCmpyId() != Company.PUBLIC_COMPANY_ID) {
                mergeCmpy.add(cmpy.getCmpyId());
              }
              mergeCmpy.add(Company.PUBLIC_COMPANY_ID);

              List<Integer> types = new ArrayList<Integer>();
              //types.add(poiType.getId());

              List<PoiRS> virtuosoPois = PoiMgr.findBy(name, 0, 2, 3, 2);
              List<PoiRS> basePois = new ArrayList<PoiRS>();
              List<String> missingPois = new ArrayList<String>();

              PoiRS virtuosoPoi = new PoiRS();
              if (virtuosoPois.size() > 0) {
                virtuosoPoi = virtuosoPois.get(0);
                basePois = PoiMgr.findByPk(virtuosoPoi.getId(), 0, 0, 2);


                if (basePois.size() < 1) {
                  prs = virtuosoPoi;
                  prs.consortiumId = 0;
                  prs.setSrcId(2);

                }
                else {
                  for (PoiRS p: basePois) {
                    if (p.getId() == virtuosoPoi.getId() && p.getCmpyId() == virtuosoPoi.getCmpyId()) {
                      prs = p;
                      isNewPoi = false;
                      break;
                    }
                    else {
                      break;

                    }
                  }

                }

                prs.updateLastUpdatedTS("system");
                prs.setName(name);
                prs.data.setName(name);
                Address address = new Address();
                address.setAddressType(AddressType.MAIN);

                if (lat != null && lat.length() > 1 &&
                    lng != null && lng.length() > 1) {
                  try {
                    float locLat = Float.parseFloat(lat);
                    float locLng = Float.parseFloat(lng);
                    Coordinates coords = new Coordinates();
                    coords.setLatitude(locLat);
                    coords.setLongitude(locLng);
                    address.setCoordinates(coords);
                  }
                  catch (NumberFormatException ne) {
                    Log.err("Missing coordinates for import poi:" + name);
                  }
                }

                address.setStreetAddress(addr1 + "\n" + addr2);
                address.setLocality(city);
                address.setRegion(state);
                address.setPostalCode(zip);
                CountriesInfo.Country cntry = CountriesInfo.Instance().searchByName(country);
                if (cntry != null) {
                  address.setCountryCode(cntry.getAlpha3());
                }
                else {
                  address.setCountryCode("UNK");
                  Log.info("Failed to covert country name to country code. Name is:" + country);
                }

                prs.setMainAddress(address);
                if (phone != null && !phone.isEmpty()) {
                  PhoneNumber pn = new PhoneNumber();
                  pn.setPhoneType(PhoneNumber.PhoneType.BUSINESS);
                  pn.setNumber(phone);
                  prs.setMainPhoneNumber(pn);
                }

                if (web != null && !web.isEmpty()) {
                  prs.data.addUrl(web);
                }

                if (twitter != null && !twitter.isEmpty()) {
                  Set<SocialLink>  links       = prs.data.getSocialLinks();
                  List<SocialLink> toberemoved = new ArrayList<SocialLink>();
                  for (SocialLink s:  links) {
                    if (s.getServiceName() != null && s.getServiceName().equals("Twitter")) {
                      toberemoved.add(s);
                    }
                  }
                  for (SocialLink s: toberemoved) {
                    links.remove(s);
                  }


                  SocialLink link = new SocialLink();
                  link.setServiceName("Twitter");
                  link.setServiceDirectUrl(twitter);
                  link.setServiceUsername("");

                  links.add(link);

                  prs.data.setSocialLinks(links);
                }

                if (facebook != null && !facebook.isEmpty()) {
                  Set<SocialLink> links = prs.data.getSocialLinks();
                  List<SocialLink> toberemoved = new ArrayList<SocialLink>();
                  for (SocialLink s:  links) {
                    if (s.getServiceName() != null && s.getServiceName().equals("Facebook")) {
                      toberemoved.add(s);
                    }
                  }
                  for (SocialLink s: toberemoved) {
                    links.remove(s);
                  }

                  SocialLink link = new SocialLink();
                  link.setServiceName("Facebook");
                  link.setServiceDirectUrl(facebook);
                  link.setServiceUsername("");

                  links.add(link);

                  prs.data.setSocialLinks(links);
                }

                if (email != null && !email.isEmpty()) {
                  Email email1 = new Email();
                  email1.setAddress(email);
                  email1.setEmailType(Email.EmailType.BUSINESS);
                  prs.data.addEmail(email1);
                }

                prs.data.setFeatures(null);
                prs.data.setDesc(null);

                if (isNewPoi) {
                  PoiMgr.save(prs);
                  insertCount++;
                } else {
                  PoiMgr.update(prs);
                  updateCount++;
                }
                Account account = Account.find.byId(Account.AID_PUBLISHER);

                if (imgFiles != null && !imgFiles.isEmpty()) {
                  for (String img : imgFiles.keySet()) {
                    String fileName = imgFiles.get(img);
                    if (fileName != null) {
                      try {
                        //save the image to s3 and set as
                        // cover
                        String       urlPK = Utils.hash(img);
                        ImageLibrary il    = ImageLibrary.find.byId(urlPK);
                        if (il == null) {

                          FileImage image = ImageController.downloadAndSaveImage(img,
                                                                                 fileName,
                                                                                 FileSrc.FileSrcType
                                                                                     .IMG_VENDOR_VIRTUOSO,
                                                                                 null,
                                                                                 account,
                                                                                 false);

                          if (image != null) {
                            PoiFile pf = PoiFile.buildPoiFile(prs.getId(),
                                                              prs.getCmpyId(),
                                                              img,
                                                              "",
                                                              Account.LEGACY_PUBLISHER_USERID);
                            pf.setFileImage(image);
                            pf.setSrcId(2);
                            pf.setNameSys(S3Util.normalizeFilename(image.getFile().getFilename()));

                            pf.setState(RecordState.ACTIVE);
                            pf.setExtension(image.getFile().getFiletype());
                            pf.setUrl(image.getUrl());
                            pf.save();
                            PoiController.incrementFileCount(prs.getId(),
                                                             prs.getCmpyId(),
                                                             2,
                                                             0,
                                                             Account.LEGACY_PUBLISHER_USERID);
                          }
                        }
                      }
                      catch (Exception e1) {
                        sb.append("Error Saving Photo - " + nextLine.toString() + "\n");
                        System.out.println("Error save Photo: Line " + "number: " + i);
                        e1.printStackTrace();
                      }
                    }
                  }
                }

              }
              else {
                missingPois.add(name);
                Log.log(LogLevel.ERROR, "SKIPPING: '"+name +"' could not be Found!!");
              }

              Ebean.commitTransaction();

            }
            catch (Exception e) {
              Ebean.rollbackTransaction();
              failedCount++;
              sb.append("Error: " + nextLine.toString() + "\n");
              e.printStackTrace();
            }
          }

          sb.append("Read Count: " + i + "\n");
          sb.append("Insert Count: " + insertCount +
              "\n");
          sb.append("Update Count: " + updateCount +
              "\n");
          sb.append("Failed Count: " + failedCount +
              "\n");
          view.message = sb.toString();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        return view;
      });

      return promiseOfView.thenApplyAsync(view -> ok(views.html.experimental.parseVirtuosoVendor.render(view)));
    }
    else {
      UploadView view = new UploadView();
      view.cmpyId = cmpyId;
      view.url = url;
      return CompletableFuture.completedFuture(ok(views.html.experimental.parseVirtuosoVendor.render(view)));
    }
  }

  public static HashMap<String, String> getFiles(String imgs) {
    HashMap<String, String> files = new HashMap<>();
    String[] imgList = imgs.split("====");
    if (imgList != null) {

      for (String imgUrl : imgList) {
        imgUrl = imgUrl.trim();
        String fileName = null;
        if (!imgUrl.toLowerCase().contains("missing.png")) {
          int index = imgUrl.lastIndexOf("/") + 1;
          if (index > 0 && index < imgUrl.length()) {
            String s = imgUrl.substring(index);
            if (s != null && s.contains("?")) {
              s = s.substring(0, s.indexOf("?"));
            }

            if (s != null && (s.endsWith("jpg") || s.endsWith("png") || s.endsWith("jpeg"))) {
              fileName = s;
            }
          }
        }

        if (imgUrl != null && fileName != null) {
          files.put(imgUrl, fileName);
        }
      }

    }
    return files;
  }

  public static HashMap<String, String> getVirtuosoFiles(String imgs) {
    HashMap<String, String> files = new HashMap<>();
    String[] imgList = imgs.split("====");
    if (imgList != null) {
      int i = 0;
      for (String imgUrl : imgList) {
        if (i > 10) {
          break;
        }
        imgUrl = imgUrl.trim();
        String fileName = null;
        if (!imgUrl.toLowerCase().contains("missing.png")) {
          if (imgUrl.indexOf("publicid=") > 0 && imgUrl.indexOf("instanceid") > 0) {
            fileName = imgUrl.substring(imgUrl.indexOf("publicid=") + 9, imgUrl.indexOf("instanceid") - 1) + ".jpeg";

          } else {
            fileName = "iceportal_" + i + ".jpeg";
          }
          i++;
        }

        if (imgUrl != null && fileName != null) {
          files.put(imgUrl, fileName);
        }
      }

    }
    return files;
  }

  public static HashMap<String, String> getWebUrls(String urls) {
    HashMap<String, String> webUrls = new HashMap<String, String>();
    String[] urlList = urls.split("====");
    if (urlList != null) {
      for (String url : urlList) {
        if (url.contains("http")) {

          String web = url.substring(url.indexOf("http"));
          String name = url.substring(0, url.indexOf("http"));
          if (name == null || name.trim().length() == 0) {
            name = web;
          }
          webUrls.put(web, name);
        }
      }

    }
    return webUrls;

  }

  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public CompletionStage<Result> loadDoc() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    final String url = form.get("inUrl");
    final String cmpyId = form.get("inCmpyId");
    final String mode = form.get("inMode");

    Company cmpy1 = null;
    if (cmpyId != null) {
      cmpy1 = Company.find.byId(cmpyId);
    }
    final Company cmpy = cmpy1;

    Account account = Account.find.byId(Account.AID_PUBLISHER);

    if (url != null && url.toLowerCase().startsWith("http") && cmpy != null && mode != null && mode.equals("YES")) {

      CompletionStage<UploadView> promiseOfView = CompletableFuture.supplyAsync(() -> {

        UploadView view = new UploadView();
        view.cmpyId = cmpyId;
        view.url = url;
        HashMap<String, String> imgFiles = null;
        StringBuffer sb = new StringBuffer();

        try {
          InputStream input = new URL(url).openStream();
          Reader reader = new InputStreamReader(input);
          CSVReader csvReader = new CSVReader(reader);
          String[] nextLine;
          int i = 0;
          int insertCount = 0;
          int updateCount = 0;
          int failedCount = 0;

          while ((nextLine = csvReader.readNext()) != null) {

            i++;
            if (nextLine.length >= (DOC_COL.values().length - 1)) {

              String type = Utils.trim(nextLine[DOC_COL.TYPE.ordinal()]);
              int activityType = -1;
              boolean isPage = false;
              String docName = Utils.trim(nextLine[DOC_COL.DOC_NAME.ordinal()]);
              String pageName = Utils.trim(nextLine[DOC_COL.PAGE_NAME.ordinal()]);

              if (type != null && type.length() > 0) {
                if (type.toLowerCase().equals("country")) {
                  activityType = APPConstants.COUNTRY_DESTINATION_TYPE;
                }
                else if (type.toLowerCase().equals("city")) {
                  activityType = APPConstants.CITY_DESTINATION_TYPE;
                }

                else if (type.toLowerCase().equals("tour")) {
                  activityType = APPConstants.TOUR_DESTINATION_TYPE;
                }
                else if (type.toLowerCase().equals("activity")) {
                  activityType = APPConstants.ACTIVITY_DESTINATION_TYPE;
                }
                else if (type.toLowerCase().equals("page") && docName != null && docName.trim().length() > 0 &&
                         pageName != null && pageName.trim().length() > 0) {
                  isPage = true;
                  activityType = 0;
                }

              }


              if (activityType > -1 && docName != null &&
                  docName.length() > 0) {
                String addr1 = Utils.trim(nextLine[DOC_COL.ADDR1.ordinal()]);
                String city = Utils.trim(nextLine[DOC_COL.CITY.ordinal()]);
                String zip = Utils.trim(nextLine[DOC_COL.ZIP.ordinal()]);
                String state = Utils.trim(nextLine[DOC_COL.STATE.ordinal()]);
                String country = Utils.trim(nextLine[DOC_COL.COUNTRY.ordinal()]);
                String lat = Utils.trim(nextLine[DOC_COL.LAT.ordinal()]);
                String lng = Utils.trim(nextLine[DOC_COL.LONG.ordinal()]);

                String landmark = Utils.trim(nextLine[DOC_COL.LANDMARK.ordinal()]);
                String recommend = Utils.trim(nextLine[DOC_COL.RECOMMEND.ordinal()]);
                String tag = Utils.trim(nextLine[DOC_COL.TAG.ordinal()]);


                String imgs = Utils.trim(nextLine[DOC_COL.IMG.ordinal()]);
                String intro = Utils.trim(nextLine[DOC_COL.INTRO.ordinal()]);
                intro = intro.replaceAll("\n", "<br/>");

                String note = Utils.trim(nextLine[DOC_COL.DESC.ordinal()]);
                note = note.replaceAll("\n", "<br/>");


                imgFiles = getFiles(imgs);

                HashMap<String, String> webUrls = null;
                if (nextLine.length == DOC_COL.values().length) {
                  String urls = Utils.trim(nextLine[DOC_COL.URL.ordinal()]);
                  webUrls = getWebUrls(urls);
                }


                try {
                  Ebean.beginTransaction();
                  if (isPage) {
                    //add this to an exisitng document
                    List<Destination> documents = Destination.findActiveByNameCmpy(docName, cmpy.cmpyid);
                    Destination document = null;
                    if (documents != null && documents.size() == 1) {
                      document = documents.get(0);
                      List<DestinationGuide> pages = DestinationGuide.findDestGuidesByName(pageName,
                                                                                           document.destinationid);
                      DestinationGuide page = null;
                      if (pages != null && pages.size() == 1) {
                        page = pages.get(0);
                        updateCount++;
                      }
                      int pageRank = DestinationGuide.activeCount(document.destinationid);
                      if (page == null) {
                        page = new DestinationGuide();
                        page.setCreatedby("system");
                        page.setCreatedtimestamp(System.currentTimeMillis());
                        page.setDestinationguideid(Utils.getUniqueId());
                        page.setDestinationid(document.destinationid);
                        page.setRank(pageRank + 1);
                        page.setStatus(APPConstants.STATUS_ACTIVE);
                        page.setName(pageName);
                        insertCount++;
                      }

                      if (addr1 != null && addr1.length() > 0) {
                        page.setStreetaddr1(addr1);
                      }

                      if (zip != null && zip.length() > 0) {
                        page.setZipcode(zip);
                      }
                      if (lat != null && lat.length() > 1) {
                        try {
                          float locLat = Float.parseFloat(lat);
                          page.setLoclat(locLat);
                        }
                        catch (NumberFormatException ne) {
                          ne.printStackTrace();
                        }
                      }
                      if (lng != null && lng.length() > 1) {
                        try {
                          float locLng = Float.parseFloat(lng);
                          page.setLoclong(locLng);
                        }
                        catch (NumberFormatException ne) {
                          ne.printStackTrace();
                        }
                      }
                      if (city != null && city.length() > 0) {
                        page.setCity(city);
                      }
                      if (country != null && country.length() > 0) {
                        page.setCountry(country);
                      }
                      if (state != null && state.length() > 0) {
                        page.setState(state);
                      }
                      if (landmark != null && landmark.length() > 0) {
                        page.setLandmark(landmark);
                      }
                      if (tag != null && tag.length() > 0) {
                        page.setTag(tag);
                      }
                      if (intro != null && intro.length() > 0) {
                        page.setIntro(intro);
                      }
                      if (note != null && note.length() > 0) {
                        page.setDescription(note);
                      }

                      int recomendType = APPConstants.RECOMMENDATION_TYPE_UNDEFINED;

                      if (recommend != null && recommend.length() > 0) {
                        if (recommend.toLowerCase().equals("food")) {
                          recomendType = APPConstants.RECOMMENDATION_TYPE_FOOD;
                        }
                        else if (recommend.toLowerCase().equals("hotel")) {
                          recomendType = APPConstants.RECOMMENDATION_TYPE_HOTEL;
                        }
                        else if (recommend.toLowerCase().equals("sights")) {
                          recomendType = APPConstants.RECOMMENDATION_TYPE_SIGHTS;
                        }
                        else if (recommend.toLowerCase().equals("shopping")) {
                          recomendType = APPConstants.RECOMMENDATION_TYPE_SHOPPING;
                        }
                        else if (recommend.toLowerCase().equals("other")) {
                          recomendType = APPConstants.RECOMMENDATION_TYPE_OTHER;
                        }
                        else if (recommend.toLowerCase().equals("goingson")) {
                          recomendType = APPConstants.RECOMMENDATION_TYPE_GOINGS_ON;
                        }


                      }
                      page.setRecommendationtype(recomendType);

                      page.setLastupdatedtimestamp(System.currentTimeMillis());
                      page.setModifiedby("system");

                      page.save();
                      DestinationPostController.invalidateCache(document.destinationid);

                      List<DestinationGuideAttach> attachedUrls = DestinationGuideAttach.findActiveByDestGuide(page.destinationguideid);
                      int rank = 0;
                      if (attachedUrls != null) {
                        rank = attachedUrls.size();
                      }

                      for (String img : imgFiles.keySet()) {
                        String fileName = imgFiles.get(img);
                        boolean insert = true;

                        if (attachedUrls != null) {
                          for (DestinationGuideAttach d : attachedUrls) {
                            if (d.getAttachtype() == PageAttachType.PHOTO_LINK&&
                                d.name.equals(fileName)) {
                              insert = false;
                              break;

                            }
                          }
                        }

                        if (fileName != null && insert) {
                          try {
                            //save the image to s3 and
                            // set as cover

                            FileImage image = ImageController.downloadAndSaveImage(img, fileName,
                                                                                   FileSrc.FileSrcType
                                                                                       .IMG_VENDOR_UMAPPED, null,
                                                                                   account, false);
                            if (image != null) {
                              List<DestinationGuideAttach> files = DestinationGuideAttach.findActiveByDestGuide
                                  (page.destinationguideid);
                              if (files != null && files.size() > 0) {
                                for (DestinationGuideAttach pf : files) {
                                  if (pf.getFileImage().getPk().equals(image.getPk())) {
                                    insert = false;
                                    break;
                                  }
                                }
                              }

                              if (insert) {
                                DestinationGuideAttach pf = DestinationGuideAttach
                                    .buildGuideAttachment(page.destinationguideid, Account.LEGACY_PUBLISHER_USERID);
                                pf.setFileImage(image);
                                pf.setAttachname(image.getFile().getFilename());
                                pf.setAttachtype(PageAttachType.PHOTO_LINK);
                                pf.setAttachurl(image.getUrl());
                                pf.setName(fileName);
                                pf.setRank(rank++);
                                pf.save();
                              }
                            }
                          }
                          catch (Exception e1) {
                            sb.append("Error Saving Photo - " + nextLine.toString() + "\n");
                            System.out.println("Error save Photo: Line number: " + i);
                            e1.printStackTrace();
                          }
                        }
                      }


                      if (webUrls != null && webUrls.size() > 0) {
                        for (String web : webUrls.keySet()) {
                          boolean insert = true;

                          if (attachedUrls != null) {
                            for (DestinationGuideAttach d : attachedUrls) {
                              if (d.getAttachtype() == PageAttachType.WEB_LINK &&
                                  d.getAttachurl().equals(web)) {
                                insert = false;
                                break;

                              }
                            }
                          }

                          if (insert) {
                            String caption = (String) webUrls.get(web);
                            DestinationGuideAttach pf = new DestinationGuideAttach();
                            pf.setCreatedby("system");
                            pf.setCreatedtimestamp(System.currentTimeMillis());
                            pf.setFileid(Utils.getUniqueId());
                            pf.setAttachtype(PageAttachType.WEB_LINK);
                            pf.setAttachurl(web);
                            pf.setLastupdatedtimestamp(System.currentTimeMillis());
                            pf.setName(caption);
                            pf.setAttachname(caption);
                            pf.setModifiedby("system");
                            pf.setDestinationguideid(page.destinationguideid);
                            pf.setRank(rank++);

                            pf.setStatus(APPConstants.STATUS_ACTIVE);
                            pf.save();

                          }
                        }
                      }

                    }
                    else {
                      failedCount++;
                      System.out.println("Failed - " + docName + " " + pageName + "\n");

                    }
                  }
                  else {
                    //this is a new document
                    List<Destination> documents = Destination.findActiveByNameCmpy(docName, cmpy.cmpyid);
                    Destination document = null;
                    if (documents != null && documents.size() == 1) {
                      document = documents.get(0);
                      updateCount++;
                    }
                    if (document == null) {
                      document = new Destination();
                      document.setCmpyid(cmpy.cmpyid);
                      document.setCreatedby("system");
                      document.setCreatedtimestamp(System.currentTimeMillis());
                      document.setDestinationid(Utils.getUniqueId());
                      document.setDestinationtypeid(activityType);
                      document.setName(docName);
                      insertCount++;
                    }

                    if (intro != null && intro.length() > 0) {
                      document.setIntro(intro);
                    }
                    if (country != null && country.length() > 0) {
                      document.setCountry(country);
                    }
                    if (city != null && city.length() > 0) {
                      document.setCity(city);
                    }
                    if (note != null && note.length() > 0) {
                      document.setDescription(note);
                    }

                    if (lat != null && lat.length() > 1) {
                      try {
                        float locLat = Float.parseFloat(lat);
                        document.setLoclat(locLat);
                      }
                      catch (NumberFormatException ne) {
                        ne.printStackTrace();
                      }
                    }
                    if (lng != null && lng.length() > 1) {
                      try {
                        float locLng = Float.parseFloat(lng);
                        document.setLoclong(locLng);
                      }
                      catch (NumberFormatException ne) {
                        ne.printStackTrace();
                      }
                    }

                    document.setLastupdatedtimestamp(System.currentTimeMillis());
                    document.setStatus(APPConstants.STATUS_ACTIVE);
                    document.setModifiedby("system");
                    for (String img : imgFiles.keySet()) {
                      String fileName = imgFiles.get(img);

                      if (fileName != null) {
                        try {
                          //save the image to s3 and set as cover
                          FileImage image = ImageController.downloadAndSaveImage(img, fileName,
                                                                                 FileSrc.FileSrcType
                                                                                     .IMG_VENDOR_UMAPPED, null,
                                                                                 account, false);
                          if (image != null) {
                            document.setFileImage(image);
                            document.setCovername(image.getFile().getFilename());
                            document.setCoverurl(image.getUrl());
                          }
                        }
                        catch (Exception e) {
                          sb.append("Error Saving Photo - " +
                                    nextLine.toString() + "\n");
                          e.printStackTrace();
                        }
                      }
                    }
                    document.save();
                    DestinationPostController.invalidateCache(document.destinationid);

                  }

                  Ebean.commitTransaction();
                }
                catch (Exception e) {
                  Ebean.rollbackTransaction();
                  sb.append("Error Processing - " + nextLine.toString() + "\n");
                  e.printStackTrace();
                  failedCount++;
                }

              }
              else {
                sb.append("SKIP LINE 1- " + nextLine.toString() + "\n");
                System.out.println("SKIP1 - " + docName + " " + pageName + "\n");


              }
            }
            else {
              sb.append("SKIP LINE 2- " + nextLine.toString() + "\n");
              System.out.println("SKIP2 - " + nextLine.toString() + "\n");


            }
          }
          sb.append("Read Count: " + i + "\n");
          sb.append("Insert Count: " + insertCount + "\n");
          sb.append("Update Count: " + updateCount + "\n");
          sb.append("Failed Count: " + failedCount + "\n");
          view.message = sb.toString();

        }
        catch (Exception e) {
          e.printStackTrace();
        }
        return view;
      });

      return promiseOfView.thenApplyAsync(view -> ok(views.html.experimental.parseDocument.render(view)));
    }
    else {
      final UploadView view = new UploadView();
      view.cmpyId = cmpyId;
      view.url = url;
      return CompletableFuture.completedFuture(ok(views.html.experimental.parseDocument.render(view)));
    }
  }

  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public CompletionStage<Result> loadAirportCity() {
    boolean DEBUG = false;

    final DynamicForm form = formFactory.form().bindFromRequest();
    final String url = form.get("inUrl");
    final String cmpyId = form.get("inCmpyId");
    String mode = form.get("inMode");
    final String nl = System.getProperty("line.separator");
    Company cmpy1 = null;
    if (cmpyId != null) {
      cmpy1 = Company.find.byId(cmpyId);
    }
    final Company cmpy = cmpy1;

    if (url != null && url.toLowerCase().startsWith("http") && cmpy != null && mode != null && mode.equals("YES")) {

      CompletionStage<UploadView> promiseOfView = CompletableFuture.supplyAsync(() -> {
        UploadView view = new UploadView();
        view.cmpyId = cmpyId;
        view.url = url;
        StringBuffer sb = new StringBuffer();

        try {

          InputStream input = new URL(url).openStream();
          Reader reader = new InputStreamReader(input);
          CSVReader csvReader = new CSVReader(reader);
          String[] nextLine;
          int i = 0;
          int insertCount = 0;
          int updateCount = 0;
          int failedCount = 0;
          int skipped = 0;
          int matchedCount = 0;

          while ((nextLine = csvReader.readNext()) != null) {

            i++;
            if (nextLine.length < AIRPORT_COL.values().length) {
              sb.append("SKIP LINE - " + nextLine.toString() + " - ERROR: WRONG COLUMN COUNT" + nl + " 00 " + AIRPORT_COL.values().length);
            }

            PoiTypeInfo.PoiType poiType = PoiTypeInfo.Instance().byName("AIRPORT");
            String name = Utils.trim(nextLine[AIRPORT_COL.NAME.ordinal()]);
            String code = Utils.trim(nextLine[AIRPORT_COL.IATA.ordinal()]);

            if (poiType == null || name == null || name.length() == 0 || code.length() != 3) {
              Log.debug("AIRPORT-SKIPPED: - " + name + " Line: " + i + " Code: " +  code );
              skipped++;
              continue;
            }

            String addr1 = Utils.trim(nextLine[AIRPORT_COL.STREET1.ordinal()]);      //Set
            String addr2 = Utils.trim(nextLine[AIRPORT_COL.STREET2.ordinal()]);      //Set
            String city = Utils.trim(nextLine[AIRPORT_COL.CITY.ordinal()]);        //Set
            String zip = Utils.trim(nextLine[AIRPORT_COL.POSTALCODE.ordinal()]);          //Set
            String state = Utils.trim(nextLine[AIRPORT_COL.STATE_CODE.ordinal()]);      //Set
            String country = Utils.trim(nextLine[AIRPORT_COL.COUNTRY_NAME.ordinal()]);  //Set
            String lat = Utils.trim(nextLine[AIRPORT_COL.LATITUDE.ordinal()]);          //Set
            String lng = Utils.trim(nextLine[AIRPORT_COL.LONGITUDE.ordinal()]);         //Set

            if (city != null && city.trim().length() > 0) {
              name += ", " +  city;
            }
            if (country != null && country.trim().length() > 0) {
              if (country.toLowerCase().contains("united states"))
                name += ", " + "USA";
              else
                name += ", " + country;
            }
            try {
              Ebean.beginTransaction();
              PoiRS prs = null;
              PoiRS defaultPrs = null;

              boolean isNewPoi = true;
              boolean hasPublicPoi = false;


              Set<Integer> mergeCmpy = new HashSet<>();
              mergeCmpy.add(Company.PUBLIC_COMPANY_ID);

              if (cmpy.getCmpyId() != Company.PUBLIC_COMPANY_ID) {
                mergeCmpy.add(cmpy.getCmpyId());
              }

              List<Integer> types = new ArrayList<Integer>();
              types.add(poiType.getId());

              PoiRS p = PoiController.findAndMergeByCode(code, poiType.getId(), mergeCmpy);


              if (p != null && p.getCmpyId() == cmpy.getCmpyId() ) {
                prs = p;
                isNewPoi = false;
              } else if (p != null && p.getCmpyId() == Company.PUBLIC_COMPANY_ID) {
                defaultPrs = p;
              }

              if (isNewPoi) {
                if (defaultPrs == null) {
                  prs = PoiRS.buildRecord(Consortium.INDEPENDENT_CONSORTIUM_ID,
                                          cmpy.getCmpyId(),
                                          poiType.getId(),
                                          FeedSourcesInfo.byName("Web"),
                                          "system");
                } else {
                  hasPublicPoi = true;
                  prs = PoiRS.buildRecord(defaultPrs.data, Consortium.INDEPENDENT_CONSORTIUM_ID, cmpy.getCmpyId(), poiType.getId(), FeedSourcesInfo.byName("Web"), "system");
                }
              }

              prs.updateLastUpdatedTS("system");
              prs.setName(name);
              prs.data.setName(name);
              prs.data.deleteTags();
              prs.setCode(code);
              Address address = new Address();
              address.setAddressType(AddressType.MAIN);

              if (lat != null && lat.length() > 1 &&
                  lng != null && lng.length() > 1 &&
                  !((lat.equals("0") || lat.equals("-1")) && (lng.equals("0") || lng.equals("-1")))) {
                try {
                  float locLat = Float.parseFloat(lat);
                  float locLng = Float.parseFloat(lng);
                  Coordinates coords = new Coordinates();
                  coords.setLatitude(locLat);
                  coords.setLongitude(locLng);
                  address.setCoordinates(coords);
                }
                catch (NumberFormatException ne) {
                  Log.err("Missing coordinates for import poi:" + name);
                }
              }

              if (addr2 != null && addr2.trim().length() > 0) {
                address.setStreetAddress(addr1 + "\n" + addr2);
              } else {
                address.setStreetAddress(addr1);
              }
              address.setLocality(city);
              address.setRegion(state);
              address.setPostalCode(zip);
              CountriesInfo.Country cntry = CountriesInfo.Instance().searchByName(country);
              if (cntry != null) {
                address.setCountryCode(cntry.getAlpha3());
              }
              else {
                address.setCountryCode("UNK");
                Log.info("Failed to covert country name to country code. Name is:" + country);
              }

              prs.setMainAddress(address);

              if (!DEBUG) {
                if (isNewPoi) {
                  PoiMgr.save(prs);
                  if (hasPublicPoi) {
                    Log.debug("AIRPORT-LOAD: Public New Matched- " + name + " Line: " + i + " Code: " + code);
                    matchedCount++;
                  } else {
                    Log.debug("AIRPORT-LOAD: New - " + name + " Line: " + i + " Code: " +  code);
                    insertCount++;
                  }
                } else {
                  Log.debug("AIRPORT-LOAD: Matched - " + name + " Line: " + i + " Code: " +  code + " --- " + p.getId() + " - " + p.getName() + " - " + p.getCode());
                  PoiMgr.update(prs);
                  updateCount++;
                }
              } else {
                if (isNewPoi) {
                  if (hasPublicPoi) {
                    Log.debug("AIRPORT-LOAD: Public New - " + name + " Line: " + i + " Code: " + code);
                    matchedCount++;

                  } else {
                    Log.debug("AIRPORT-LOAD: New - " + name + " Line: " + i + " Code: " + code);
                  }
                  insertCount++;
                } else {
                  Log.debug("AIRPORT-LOAD: Matched - " + name + " Line: " + i + " Code: " +  code + " --- " + p.getId() + " - " + p.getName() + " - " + p.getCode());
                  updateCount++;
                }
              }

              Ebean.commitTransaction();

            }
            catch (Exception e) {
              Ebean.rollbackTransaction();
              failedCount++;
              sb.append("Error: " + nextLine.toString() + "\n");
              e.printStackTrace();
            }
          }
          sb.append("Read Count: " + i + "\n");
          sb.append("Insert Count: " + insertCount +
                    "\n");
          sb.append("Update Count: " + updateCount +
                    "\n");
          sb.append("Failed Count: " + failedCount +
                    "\n");
          sb.append("Skipped Count: " + skipped +
                    "\n");
          sb.append("Matched Count: " + matchedCount);
          view.message = sb.toString();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        return view;
      });
      return promiseOfView.thenApplyAsync((UploadView view) -> ok(views.html.experimental.parseAiportCity.render(view)));
    }
    else {
      UploadView view = new UploadView();
      view.cmpyId = cmpyId;
      view.url = url;
      return CompletableFuture.completedFuture(ok(views.html.experimental.parseAiportCity.render(view)));
    }
  }

  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public Result loadAirlineCheckinUrl() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    final String url = form.get("inUrl");
    final String mode = form.get("inMode");




    if (url != null && url.toLowerCase().startsWith("http")  && mode.equals("YES")) {

      UploadView view = new UploadView();
      view.url = url;
      HashMap<String, String> imgFiles = null;
      StringBuffer sb = new StringBuffer();

      try {
        InputStream input = new URL(url).openStream();
        Reader reader = new InputStreamReader(input);
        CSVReader csvReader = new CSVReader(reader);
        String[] nextLine;
        int i = 0;
        int insertCount = 0;
        int updateCount = 0;
        int failedCount = 0;
        List<Integer> poiTypes = new ArrayList<Integer>();
        poiTypes.add(PoiTypeInfo.Instance().byName("Airline").getId());
        Set<Integer> cmpyIds = new HashSet<>();
        cmpyIds.add(0);
        while ((nextLine = csvReader.readNext()) != null) {

          i++;
          if (nextLine.length == 4) {

            String airlineCode = Utils.trim(nextLine[0]);
            String airlineName = Utils.trim(nextLine[1]);
            String checkinUrl = Utils.trim(nextLine[2]);
            String baggageUrl = Utils.trim(nextLine[3]);

            Map<Long, List<PoiRS>> airlines = PoiMgr.findByCode(airlineCode, poiTypes, cmpyIds, 2);
            boolean found =  false;
            if (airlines != null) {
              for (List<PoiRS> list: airlines.values()) {
                for (PoiRS rs: list) {
                  found = true;
                  PoiJson             data               = rs.data;
                  Map<String, String> additionalParams   = data.getAdditionalProperties();
                  String              existingBaggageUrl = additionalParams.get(APPConstants.BAGGAGE_URL);
                  String              existingCheckinUrl = additionalParams.get(APPConstants.CHECKIN_URL);

                  System.out.println("FOUND: " + airlineName + " -- " + rs.getName());
                  System.out.println("----- Existing: " + existingBaggageUrl + " -- " + existingCheckinUrl);

                  if (baggageUrl != null && baggageUrl.trim().length() > 0) {
                    data.setAdditionalProperty(APPConstants.BAGGAGE_URL, baggageUrl );
                  } else if (existingBaggageUrl != null) {
                    System.out.println("----- Removing Existing Baggage URL: " + existingBaggageUrl);

                    data.getAdditionalProperties().remove(APPConstants.BAGGAGE_URL);
                  }

                  if (checkinUrl != null && checkinUrl.trim().length() > 0) {
                    data.setAdditionalProperty(APPConstants.CHECKIN_URL, checkinUrl );
                  } else if (existingCheckinUrl != null) {
                    System.out.println("----- Removing Existing Checkin URL: " + existingCheckinUrl);

                    data.getAdditionalProperties().remove(APPConstants.CHECKIN_URL);
                  }

                  rs.setData(data);
                  PoiMgr.update(rs);
                  System.out.println("----- Updated: " + additionalParams.get(APPConstants.BAGGAGE_URL) + " -- " +
                                     additionalParams.get(APPConstants.CHECKIN_URL));
                }
              }
            }
            if (!found) {
              System.out.println("NOT FOUND: " + airlineCode + " " + airlineName);
            }
          }
          else {
            sb.append("SKIP LINE 2- " + nextLine.toString() + "\n");
            System.out.println("SKIP2 - " + nextLine.toString() + "\n");
            failedCount++;
          }
        }
        sb.append("Read Count: " + i + "\n");
        sb.append("Insert Count: " + insertCount + "\n");
        sb.append("Update Count: " + updateCount + "\n");
        sb.append("Failed Count: " + failedCount + "\n");
        view.message = sb.toString();

      }
      catch (Exception e) {
        e.printStackTrace();
      }
      return ok(views.html.experimental.parseAirlineUrls.render(view));
    }
    final UploadView view = new UploadView();
    view.url = url;
    return ok(views.html.experimental.parseAirlineUrls.render(view));

  }

  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public Result memcache() {
    StringBuilder sb         = new StringBuilder();
    StopWatch     sw         = new StopWatch();
    SessionMgr    sessionMgr = SessionMgr.fromContext(ctx());

    sw.start();
    //insert credentials 10000 times
    for (int i = 0; i < 10000; i++) {
      Credentials cred = Credentials.getFromCache(sessionMgr.getAccountId());
      cred.updateCache();
    }
    sw.stop();
    sb.append("10000 get/set credentials: " + sw.toString());
    sb.append("\n");
    sw.reset();
    System.out.println(sb.toString());

    List<TripDetail> details = TripDetail.findAll();

    for (int i = 0; i < 5; i++) {
      sw.start();
      for (TripDetail t : details) {
        BookingController.buildBookingView(t.tripid, t, true);
      }
      sw.stop();
      sb.append("Cycle " + i + " Size: " + details.size() + " build Trip Details: " + sw.toString());
      sb.append("\n");
      sw.reset();
      System.out.println(sb.toString());

      try {
        TimeUnit.MILLISECONDS.sleep(500);
      } catch (InterruptedException ie) {}

      sw.start();
      for (TripDetail t : details) {
        BookingController.buildBookingView(t.tripid, t, true);
      }
      sw.stop();
      sb.append("Cycle " + i + " Size: " + details.size() + " get Trip Details: " + sw.toString());
      sb.append("\n");
      sw.reset();
      System.out.println(sb.toString());

      sw.start();
      for (TripDetail t : details) {
        BookingController.invalidateCache(t.tripid, t.detailsid);
      }
      sw.stop();
      sb.append("Cycle " + i + " Size: " + details.size() + " remove Trip Details: " + sw.toString());
      sb.append("\n");
      sw.reset();
      System.out.println(sb.toString());

    }

    sw.start();
    Random r = new Random();
    for (int i = 0; i < 200000; i++) {
      int max = r.nextInt(100) + 1;
      StringBuilder tempS = new StringBuilder();
      for (int j = 0; j < max; j++) {
        tempS.append("a");
      }
      CacheMgr.setBlocking("STR_" + String.valueOf(i), tempS.toString(), RedisMgr.NO_EXPIRE);
    }
    sw.stop();
    sb.append("200000 random Strings put: " + sw.toString());
    sb.append("\n");
    sw.reset();
    System.out.println(sb.toString());

    sw.start();
    for (int i = 0; i < 20000; i++) {
      CacheMgr.get("STR_" + String.valueOf(i));
    }
    sw.stop();
    sb.append("200000 random Strings get: " + sw.toString());
    sb.append("\n");
    sw.reset();
    System.out.println(sb.toString());

    sw.start();

    for (int i = 0; i < 20000; i++) {
      CacheMgr.setBlocking("STR_" + String.valueOf(i), null, RedisMgr.NO_EXPIRE);
    }
    sw.stop();
    sb.append("200000 random Strings remove: " + sw.toString());
    sb.append("\n");
    sw.reset();
    System.out.println(sb.toString());

    BaseView view = new BaseView();
    view.message = sb.toString();

    return ok(views.html.common.message.render(view));
  }


  /**
   * Tests Redis List functionality
   */
  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public Result redisListTest() {
    Trip.find.all().forEach((t)->{
      StopWatch sw = new StopWatch();
      sw.start();
      Set<TripDetail> tripDetails = new HashSet<>();
      TripDetail.find.where().eq("tripid", t.getTripid()).query().findEach((td)->{
        tripDetails.add(td);
        redis.listPush(RedisKeys.L_LIST_TEST, t.getTripid(), td, RedisMgr.Direction.LEFT, RedisMgr.WriteMode.BLOCKING);
      });

      //Since we have no details - there will be no records to read back
      if(tripDetails.size() == 0) {
        return;
      }

      Optional<List<TripDetail>> fromCache = redis.listRange(RedisKeys.L_LIST_TEST, t.getTripid(), 0, -1,
                                                             TripDetail.class);
      if(!fromCache.isPresent()) {
        Log.err("Empty list cache error");
        return;
      }

      List<TripDetail> cacheDetails = fromCache.get();

      if(cacheDetails.size() != tripDetails.size()) {
        Log.err("Trip ID: " + t.getTripid() + " Number of objects does not match. Actual: " + cacheDetails.size() +
                " vs Expected:" + tripDetails.size());
      }

      for(TripDetail td: cacheDetails) {
        if(!tripDetails.contains(td)) {
          Log.err("Trip ID: " + t.getTripid() + " Didn't find an object (range)");
        }
      }

      //Remove all one by one
      Optional<TripDetail> rtd = redis.listPop(RedisKeys.L_LIST_TEST, t.getTripid(), RedisMgr.Direction.LEFT,
                                               TripDetail.class);
      int count = 0;
      while(rtd.isPresent()) {
        ++count;
        if(!tripDetails.contains(rtd.get())) {
          Log.err("Trip ID: " + t.getTripid() + ": Didn't find an object (pop)");
        }

        rtd = redis.listPop(RedisKeys.L_LIST_TEST, t.getTripid(), RedisMgr.Direction.LEFT,
                            TripDetail.class);
      }

      if(count != tripDetails.size()) {
        Log.err("Popping didn't result in the same number of pops as pushes actual: " + count + " vs expected: " +
                tripDetails.size());
      }

      sw.stop();

      Log.info(String.format("Processed tripid: %1$20s in %2$4dms for %3$3d bookings",
                             t.getTripid(), sw.getTime(),
                             tripDetails.size()));
      sw.reset();
    });

    return UserMessage.message(ReturnCode.SUCCESS);
  }

  /**
   * Helper action to debug html/pdf issues
   *
   * @return
   */
  @With({Authenticated.class, Authorized.class})
  public Result documentDebug(String destId, boolean htmlView, String tripId, String tmplt) {
    List<DestinationType> destinationTypes = DestinationType.findActive();
    HashMap<Integer, String> destinationTypeDesc = new HashMap<>();

    if (destinationTypes != null) {
      for (DestinationType u : destinationTypes) {
        destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
      }
    }

    boolean mergeBookings = false;

    final DestinationView destView = DestinationController.getDestView(destId, destinationTypeDesc);
    if (destView != null) {
      if (tripId != null) {
        Trip trip = Trip.findByPK(tripId);
        if (trip != null && trip.status != APPConstants.STATUS_DELETED) {
          TripDestination tripDestination = TripDestination.find.byId(tripId);
          if (tripDestination != null) {
            mergeBookings = tripDestination.isMergebookings();
          }

          destView.tripStartDate = trip.starttimestamp;
          destView.tripStartDatePrint = Utils.formatDatePrint(trip.starttimestamp);
          destView.tripEndDate = trip.endtimestamp;
          destView.tripEndDatePrint = Utils.formatDatePrint(trip.endtimestamp);
          destView.tripId = trip.tripid;

          //destView.agent = UserController.getUserView(trip.createdby);

          TripPublishHistory tp = TripPublishHistory.getLastPublished(trip.tripid, null);
          if (tp != null) {
            destView.agent = UserController.getUserView(tp.createdby);
          }
          else {
            destView.agent = UserController.getUserView(trip.createdby);
          }

          List<TripBrandingView> brandingViews = TripBrandingMgr.findActiveBrandedCmpies(trip);
          if (brandingViews != null && brandingViews.size() > 0) {
            Company c = TripBrandingMgr.getMainContact(brandingViews);
            if (!c.cmpyid.equals(destView.cmpyInfo.cmpyId)) {
              destView.cmpyInfo = CompanyController.getCmpyView(c.cmpyid);
            }
            destView.tripCoBranding = brandingViews;
          }
        }

        if (mergeBookings) {
          //get all the trip bookings also
          TripBookingView tripBookingView = new TripBookingView();
          tripBookingView.lang = ctx().lang();
          List<TripDetail> tripDetails = TripDetail.findActiveByTripId(tripId);
          if (tripDetails != null) {
            for (TripDetail tripDetail : tripDetails) {
              TripBookingDetailView bookingDetails = BookingController.buildBookingView(tripId, tripDetail, true);
              tripBookingView.addBookingDetailView(tripDetail.getDetailtypeid(), bookingDetails);
            }
            destView.tripBookingView = tripBookingView;
          }

          // figure out all the chronological items
          destView.chronologicalBookingsPages = new HashMap<>();
          destView.chronologicalPagesBookings = new ArrayList<>();

          ArrayList<BookingAndPageView> bookingsPages = new ArrayList<>();

          //get all pages that have dates
          for (DestinationGuideView gv : destView.guides) {
            if (gv.timestamp != null && gv.timestamp > 0 && gv.groupedViews != null && gv.groupedViews.size() > 0) {
              for (DestinationGuideView gv1 : gv.groupedViews) {
                BookingAndPageView v = new BookingAndPageView();
                v.destinationGuideView = gv1;
                v.timestamp = gv1.timestamp;
                bookingsPages.add(v);
              }
            }
          }
          destView.chronologicalPagesBookings = new ArrayList<>();
          //get all the bookings that have date
          List<TripBookingDetailView> allBookings = new ArrayList<>();

          for (ReservationType type : destView.tripBookingView.bookings.keySet()) {
            allBookings.addAll(destView.tripBookingView.bookings.get(type));
          }
          for (TripBookingDetailView b : allBookings) {
            BookingAndPageView v = new BookingAndPageView();
            v.tripBookingDetailView = b;
            v.timestamp = b.startDateMs;
            bookingsPages.add(v);
          }

          //sort all the timestamps and group by date
          Collections.sort(bookingsPages, BookingAndPageView.BookingAndPageViewComparator);
          for (BookingAndPageView v : bookingsPages) {
            String d = Utils.formatDatePrint(v.timestamp);
            if (!destView.chronologicalPagesBookings.contains(d)) {
              destView.chronologicalPagesBookings.add(d);
              ArrayList<BookingAndPageView> l = new ArrayList<>();
              l.add(v);
              destView.chronologicalBookingsPages.put(d, l);
            }
            else {
              destView.chronologicalBookingsPages.get(d).add(v);
            }
          }
        }
      }
      CmpyCustomTemplate.TEMPLATE_TYPE customTemplateType1 = CmpyCustomTemplate.TEMPLATE_TYPE.DOC_PDF;
      if (destView.chronologicalPagesBookings != null && destView.chronologicalPagesBookings.size() > 0) {
        customTemplateType1 = CmpyCustomTemplate.TEMPLATE_TYPE.BOOKINGS_DOC_PDF;
      }

      final CmpyCustomTemplate.TEMPLATE_TYPE customTemplateType = customTemplateType1;

      try {
        java.lang.reflect.Method render = null;
        if (tmplt == null) {
          CmpyCustomTemplate customTemplate = CmpyCustomTemplate.findActiveByCmpyType(destView.cmpyId,
                                                                                      customTemplateType.ordinal());
          if (customTemplate != null &&
              customTemplate.isfiletemplate &&
              customTemplate.getPath() != null &&
              customTemplate.getPath().startsWith("views")) {

            final Class<?> clazz = Class.forName(customTemplate.getPath());
            render = clazz.getDeclaredMethod("render", DestinationView.class);
          }
        }
        else {
          final Class<?> clazz = Class.forName(tmplt);
          render = clazz.getDeclaredMethod("render", DestinationView.class);
        }

        if (render != null) {
          Html html = (Html) render.invoke(null, destView);
          if (htmlView) {
            return ok(html);
          }
          else {
            return pdfGenerator.ok(html, "http://umapped.com");
          }
        }
        else {
          if (htmlView) {
            return ok(views.html.guide.destinationPdf.render(destView));
          }
          else {
            return pdfGenerator.ok(views.html.guide.destinationPdf.render(destView), "http://umapped.com");
          }
        }

      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    return internalServerError("Why did you do this to me?");
  }

  @With({Authenticated.class, Authorized.class})
  public Result tripDebug(String tripId, boolean htmlView, String tmplt) {
    Trip trip = Trip.findByPK(tripId);

    if (trip == null) {
      return badRequest("Who are you? What are you? Stop this.");
    }

    TripPreviewView previewView = new TripPreviewView();
    Company cmpy = null;

    previewView.tripCoverUrl = trip.getImageUrl();
    previewView.wasPublished = (TripPublishHistory.countPublished(trip.tripid) > 0); //George: toggle for draft B/G in PDF


    //get agent info
    UserProfile up = null;
    TripPublishHistory tp = TripPublishHistory.getLastPublished(trip.tripid, null);
    if (tp != null) {
      up = UserProfile.findByPK(tp.createdby);
    }
    else {
      up = UserProfile.find.byId(trip.createdby);
    }

    if (up != null) {
      AgentView agentView = new AgentView();
      previewView.agent = agentView;
      StringBuffer sb = new StringBuffer();
      if (up.firstname != null) {
        sb.append(up.firstname);
      }
      if (sb.length() > 0 && up.lastname != null) {
        sb.append(" ");
        sb.append(up.lastname);
      }
      agentView.agentName = sb.toString();
      agentView.agentEmail = up.email;
      agentView.agentPhone = up.phone;
      agentView.agentMobile = up.mobile;

      List<TripBrandingView> brandingViews = TripBrandingMgr.findActiveBrandedCmpies(trip);
      if (brandingViews != null && brandingViews.size() > 0) {
        cmpy = TripBrandingMgr.getMainContact(brandingViews);
        previewView.tripBranding = brandingViews;
      }

      if (cmpy == null) {
        cmpy = Company.find.byId(trip.cmpyid);
      }

      if (cmpy != null) {
        agentView.cmpyName = cmpy.name;
        if (cmpy.getLogoname() != null &&
            cmpy.getLogoname().length() > 0 &&
            cmpy.getLogourl() != null &&
            cmpy.getLogourl().length() > 0) {
          agentView.cmpyLogo = Utils.escapeS3Umapped_Prd(cmpy.getLogourl());
        }
      }
      List<CmpyAddress> cmpyAddrs = CmpyAddress.findMainActiveByCmpyId(cmpy.cmpyid);
      if (cmpyAddrs != null && cmpyAddrs.size() > 0) {
        CmpyAddress cmpyAddress = cmpyAddrs.get(0);
        agentView.cmpyEmail = cmpyAddress.email;
        agentView.cmpyPhone = cmpyAddress.phone;
        agentView.cmpyWeb = cmpyAddress.web;
      }
    }

    ArrayList<TripBookingDetailView> bookingLocations = new ArrayList<>();
    previewView.tripName = trip.name;
    previewView.tripStatus = String.valueOf(trip.status);
    previewView.tripStartDate = Utils.getDateString(trip.starttimestamp);
    previewView.tripEndDate = Utils.getDateString(trip.endtimestamp);
    previewView.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
    previewView.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
    previewView.tripTag = trip.tag;

    TripBookingView view = new TripBookingView();
    previewView.bookings = view;
    view.lang = ctx().lang();

    previewView.tripId = trip.tripid;
    previewView.tripStatus = String.valueOf(trip.status);
    view.comments = Utils.escapeHtml(trip.comments);

    view.tripId = trip.tripid;
    view.tripCmpyId = trip.cmpyid;

    if (trip.starttimestamp > 0) {
      view.tripStartDatePrint = Utils.getDateStringPrint(trip.starttimestamp);
      view.tripStartDate = String.valueOf(trip.starttimestamp);


      Calendar cal = Calendar.getInstance();
      cal.setTimeInMillis(trip.starttimestamp);
      previewView.startYear = String.valueOf(cal.get(Calendar.YEAR));
      previewView.startMonth = String.valueOf(cal.get(Calendar.MONTH));
      previewView.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
    }

    if (trip.endtimestamp > 0) {
      view.tripEndDatePrint = Utils.getDateStringPrint(trip.endtimestamp);
      view.tripEndDate = Utils.getDateString(trip.endtimestamp);
    }
    view.tripName = trip.name;
    view.tripStatus = trip.status;

    List<TripDetail> tripDetails = TripDetail.findActiveByTripId(trip.tripid);
    if (tripDetails != null) {
      for (TripDetail tripDetail : tripDetails) {
        TripBookingDetailView bookingDetails = BookingController.buildBookingView(trip.tripid, tripDetail, true);
        if (bookingDetails != null) {
          if (bookingDetails.locStartLat != null &&
              bookingDetails.locStartLat != 0.0 &&
              bookingDetails.locStartLong != null &&
              bookingDetails.locStartLong != 0.0) {
            bookingLocations.add(bookingDetails);
          }

          if (tripDetail.starttimestamp > 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(tripDetail.starttimestamp);
            bookingDetails.startYear = String.valueOf(cal.get(Calendar.YEAR));
            bookingDetails.startMonth = String.valueOf(cal.get(Calendar.MONTH));
            bookingDetails.startDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
            bookingDetails.startHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
            bookingDetails.startMin = String.valueOf(cal.get(Calendar.MINUTE));
          }

          if (tripDetail.endtimestamp > 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(tripDetail.endtimestamp);
            bookingDetails.endYear = String.valueOf(cal.get(Calendar.YEAR));
            bookingDetails.endMonth = String.valueOf(cal.get(Calendar.MONTH));
            bookingDetails.endDay = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
            bookingDetails.endHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
            bookingDetails.endMin = String.valueOf(cal.get(Calendar.MINUTE));
          }

          view.addBookingDetailView(tripDetail.getDetailtypeid(), bookingDetails);
        }
      }

      if (!bookingLocations.isEmpty()) {
        previewView.bookingsLocations = bookingLocations;
      }
    }
    HashMap<Integer, String> destinationTypeDesc = new HashMap<>();
    List<DestinationType> destinationTypes = DestinationType.findActive();
    if (destinationTypes != null) {
      ArrayList<GenericTypeView> destinationTypeList = new ArrayList<>();
      for (DestinationType u : destinationTypes) {
        GenericTypeView typeView = new GenericTypeView();
        typeView.id = String.valueOf(u.getDestinationtypeid());
        typeView.name = u.getName();
        destinationTypeList.add(typeView);
        destinationTypeDesc.put(u.getDestinationtypeid(), u.getName());
      }
      previewView.destinationTypeList = destinationTypeList;
    }

    ArrayList<DestinationView> guides = new ArrayList<>();
    DestinationSearchView dests = TripController.getGuides(view);
    previewView.destinations = dests;
    if (previewView.destinations != null && previewView.destinations.destinationList != null) {
      for (DestinationView v : previewView.destinations.destinationList) {
        DestinationView destView = DestinationController.getDestView(v.id, destinationTypeDesc);
        guides.add(destView);
      }
      previewView.destinations.destinationList = guides;
    }

    //handle cross timezone booking
    if (previewView.bookings != null &&
        previewView.bookings.flights != null &&
        previewView.bookings.flights.size() > 0) {
      previewView.bookings.handleCrosTimezoneFlights();
    }

    try {
      java.lang.reflect.Method render = null;
      if (tmplt == null) {
        CmpyCustomTemplate customTemplate = CmpyCustomTemplate.findActiveByCmpyType(trip.cmpyid,
                                                                                    CmpyCustomTemplate.TEMPLATE_TYPE
                                                                                        .TRIP_PDF
                                                                                        .ordinal());
        if (customTemplate != null &&
            customTemplate.isfiletemplate &&
            customTemplate.getPath() != null &&
            customTemplate.getPath().startsWith("views")) {

          final Class<?> clazz = Class.forName(customTemplate.getPath());
          render = clazz.getDeclaredMethod("render", TripPreviewView.class);
        }
      }
      else {
        final Class<?> clazz = Class.forName(tmplt);
        render = clazz.getDeclaredMethod("render", TripPreviewView.class);
      }

      if (render != null) {
        Html html = (Html) render.invoke(null, previewView);
        if (htmlView) {
          return ok(html);
        }
        else {
          return pdfGenerator.ok(html, "http://umapped.com");
        }
      }
      else {
        if (htmlView) {
          return ok(views.html.trip.tripPdf.render(previewView, "en", false));
        }
        else {
          return pdfGenerator.ok(views.html.trip.tripPdf.render(previewView, "en", false), "http://umapped.com");
        }
      }

    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return internalServerError("Why did you do this to me?");
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result virtuosoFiles(final String folder) {
      String                prefix = "virtuoso/" + folder;
      List<S3ObjectSummary> files  = S3Util.getObjectList(S3Util.getPDFBucketName(), prefix);

      ObjectNode result = Json.newObject();
      ArrayNode  an     = result.putArray("files");
      for (S3ObjectSummary f : files) {
        ObjectNode on = Json.newObject();
        String fn = FilenameUtils.getBaseName(f.getKey());
        on.put("name", fn);
        on.put("size", f.getSize());
        on.put("date", Utils.getISO8601Timestamp(f.getLastModified()));
        if (FilenameUtils.getExtension(f.getKey()).equals("xml")) {
          an.add(on);
        }
      }
      return ok(result);
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result virtuosoData(final String folder, final String fileName) {
    try {
    String       path             = FilenameUtils.concat("virtuoso/" + folder, fileName + ".xml");
    S3Object     s3Obj            = S3Util.getS3File(S3Util.getPDFBucketName(), path);
    JAXBContext  jaxbContext      = JAXBContext.newInstance(Root.class);//TODO: 2016-11-17: Which Root class to
                                                                        // import for this?
    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
    Root         pois             = (Root) jaxbUnmarshaller.unmarshal(s3Obj.getObjectContent());
    return ok(Json.toJson(pois));
    } catch (Exception e) {
      Log.err("ExperimentalController::virtuosoData: Error: ", e);
    }
    return internalServerError();
  }

  @With({Authenticated.class, Authorized.class, AdminAccess.class})
  public Result virtuosoCruiseAmenities() {
        int inserted = 0;
        int read = 0;
        int updated = 0;
        int unchanged = 0;
        int skipped =0;
        try {
          final DynamicForm form = formFactory.form().bindFromRequest();
          String fileName = form.get("filename");

          SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

          String path = FilenameUtils.concat("virtuoso/Cruise/", fileName + ".xml");
          S3Object s3Obj = S3Util.getS3File(S3Util.getPDFBucketName(), path);
          JAXBContext jaxbContext = JAXBContext.newInstance(com.mapped.publisher.parse.virtuoso.cruise.amenities.Root
                                                                .class);
          Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
          com.mapped.publisher.parse.virtuoso.cruise.amenities.Root cruises = (com.mapped.publisher.parse.virtuoso
              .cruise.amenities.Root) jaxbUnmarshaller
              .unmarshal(s3Obj.getObjectContent());
          Log.log(LogLevel.DEBUG, "virtuosoCruiseAmenities - loaded: " + path);

          List<CruiseAmenities> amenitiesList = new ArrayList<CruiseAmenities>();


          for (com.mapped.publisher.parse.virtuoso.cruise.amenities.Root.Cruise.CruiseInfo c : cruises.getCruise()
                                                                                                      .getCruiseInfo
                                                                                                          ()) {
            CruiseAmenities amenities = CruiseAmenities.findBySource_ConsortiumId(String.valueOf(c.getMasterEntityID()),
                                                                                  Long.valueOf(2));

            read++;
            String cruiseInfo = null;
            String newHash = null;

            if (c.getCruiseShip() != null) {

              boolean updateRec = false;
              cruiseInfo = pojo2Xml(c, jaxbContext);
              newHash = Utils.hash(cruiseInfo);

              if (amenities == null) {
                amenities = new CruiseAmenities();
                amenities.setPk(DBConnectionMgr.getUniqueLongId());
                amenities.setCreated_ts(System.currentTimeMillis());
                amenities.setCreated_by("System");
                updateRec = true;
                inserted++;
              }
              else {
                if (!newHash.equals(amenities.getHash())) {
                  Log.log(LogLevel.DEBUG, " " + " New Hash:" + newHash + " Old Hash" + amenities.getHash());
                  updateRec = true;
                  updated++;
                }
                else {
                  Log.log(LogLevel.DEBUG, " " + " Master Entity:" + c.getMasterEntityID());

                  unchanged++;
                }
              }

              if (updateRec) {
                amenities.setShip_name(c.getCruiseShip());
                amenities.setHash(newHash.getBytes());
                amenities.setRaw(cruiseInfo);
                amenities.setSrc_reference_id(String.valueOf(c.getMasterEntityID()));
                amenities.setConsortium_id((long) 2);
                amenities.setCruise_name(c.getProductName());
                amenities.setCmpy_name(c.getCompanyName());
                amenities.setStart_ts(df.parse(c.getTravelStartDate().toString()).getTime());
                amenities.setEnd_ts(df.parse(c.getTravelEndDate().toString()).getTime());
                amenities.setLast_updated_ts(System.currentTimeMillis());
                amenities.setModified_by("System");

                List<Integer> poiTypes = new ArrayList<Integer>();
                poiTypes.add(6);
                Set<Integer> cmpyIds = new HashSet<Integer>();
                cmpyIds.add(0);

                Map<Long, List<PoiRS>> poiMap = PoiMgr.findByTerm(c.getCruiseShip(), poiTypes, cmpyIds, 1);

                if (!poiMap.keySet().isEmpty()) {
                  amenities.setPoi_id(poiMap.keySet().iterator().next());
                }
                else {
                  amenities.setPoi_id(0);
                }

                StringBuffer sb = new StringBuffer();
                String terms = null;
                if (c.getSpecialAmenities() != null &&c.getSpecialAmenities().getTermsAndConditions() != null) {
                  terms = c.getSpecialAmenities().getTermsAndConditions().trim();

                  for (com.mapped.publisher.parse.virtuoso.cruise.amenities.Root.Cruise.CruiseInfo.SpecialAmenities
                      .SpecialAmenity sa : c
                      .getSpecialAmenities()
                      .getSpecialAmenity()) {

                    if (sa.getBenefit() != null) {
                      sb.append(sa.getBenefit().trim() + "\n");
                    }
                    if (sa.getBenefitDescription() != null) {
                      sb.append(sa.getBenefitDescription().trim() + "\n");
                    }
                  }

                  amenities.setAmenities(sb.toString());
                }
                else {
                  if (c.getVoyagerClub() != null && c.getVoyagerClub().getVoyagerClubOption() != null) {
                    for (com.mapped.publisher.parse.virtuoso.cruise.amenities.Root.Cruise.CruiseInfo.VoyagerClub.VoyagerClubOption vco : c

                        .getVoyagerClub()
                        .getVoyagerClubOption()) {
                      sb.append(vco.getBenefit().trim() + "\n");
                      sb.append(vco.getBenefitDescription().trim() + "\n");
                    }
                  }
                  if (c.getVoyagerClub() != null && c.getVoyagerClub().getVoyagerClubEvent() != null) {

                    for (com.mapped.publisher.parse.virtuoso.cruise.amenities.Root.Cruise.CruiseInfo.VoyagerClub.VoyagerClubEvent vce : c

                        .getVoyagerClub()
                        .getVoyagerClubEvent()) {
                      sb.append(vce.getEventActivityType().trim() + "\n");
                      sb.append(vce.getEventActivityLevel().trim() + "\n");
                      sb.append(vce.getEventAge().trim() + "\n");
                      sb.append(vce.getEventDate().getMillisecond() + "\n");
                      sb.append(vce.getEventDescription().trim() + "\n");
                      sb.append(vce.getEventLength().trim() + "\n");
                      sb.append(vce.getEventLocation().trim() + "\n");
                      sb.append(vce.getEventName().trim() + "\n");
                      sb.append(vce.getEventNote().trim() + "\n");
                      sb.append(vce.getMeals() + "\n");
                    }
                  }
                  if (c.getVoyagerClub() != null && c.getVoyagerClub().getAllGuestsReceiveNote() != null) {
                    sb.append(c.getVoyagerClub().getAllGuestsReceiveNote().trim() + "\n");
                  }
                  if (c.getVoyagerClub() != null && c.getVoyagerClub().getTermsAndConditions() != null) {
                    sb.append(c.getVoyagerClub().getTermsAndConditions().trim() + "\n");
                  }


                }
                if (terms != null && terms.trim().length() > 0) {
                  sb.append("\n\n");sb.append(terms);
                }
                String s = sb.toString();
                s = s.replaceAll("<br.*./>","\n");
                amenities.setAmenities(s);
                amenities.save();
              }
            } else {
              skipped++;
            }
          }


        }
        catch (Exception e) {
          e.printStackTrace();
        }
        BaseView view = new BaseView();
        view.message = "Read: " + read + " Inserted: " + inserted + " Updated: " + updated + " Unchanged: " + unchanged + " Skipped: " + skipped;
        return ok(views.html.common.message.render(view));
  }

  /**
   * Do not use this function, it causes major screwup with Firebase subscriptions
   * @return
   */
  @With({Authenticated.class, Authorized.class,  AdminAccess.class})
  public Result testFlightMessages(){
    if (!ConfigMgr.getInstance().isProd()) {
      Communicator.Instance().testFlightMessages();
    }
    return TODO;
  }

  @With({Authenticated.class, Authorized.class,  AdminAccess.class})
  public Result communicatorDeleteAll(){
    if (!ConfigMgr.getInstance().isProd()) {
      Communicator.Instance().fbDeleteAllData();
    }
    return TODO;
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result processICBellagioDocs() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    String inTripId = form.get("inTripId");
    Trip trip = Trip.find.byId(inTripId);

    int newDocs = 0;
    int newPages = 0;

    if (trip != null) {
      List<TripAttachment> attachments = TripAttachment.findByTripId(inTripId);
      if (attachments != null && attachments.size() > 0) {
        for (TripAttachment attachment : attachments) {
          if (attachment.getOrigfilename().contains("RESTAURANT")) {

            try {
              S3Object s3Obj = S3Util.getS3File(attachment.getFilename());
              Destination doc = null;
              List<DestinationGuide> pages = new ArrayList<DestinationGuide>();
              InputStream           input    = s3Obj.getObjectContent();
              PDDocument            document = PDDocument.load(input);
              PDFTextStripperByArea stripper = new PDFTextStripperByArea();
              stripper.setSortByPosition(true);
              Rectangle rect = new Rectangle(0, 100, 612, 650);
              stripper.addRegion("body", rect);
              List<?> allPages = document.getDocumentCatalog().getAllPages();

              StringBuffer buffer = new StringBuffer();
              for (Object pdPage : allPages) {
                stripper.extractRegions((PDPage) pdPage);
                buffer.append(stripper.getTextForRegion("body"));
              }

              String[] lines = buffer.toString().split("\n");
              int lineCount = 0;
              int rank = 0;
              while (lineCount < lines.length) {

                String line = lines[lineCount];
                String title = "";
                if (line.indexOf("COLLECTION") > 0) {
                  String city = lines[lineCount + 1];
                  if (city != null) {
                    city = city.trim();
                    city = city.replaceAll("\"", "");
                    city = city.replaceAll("“", "");
                    city = city.replaceAll("”", "");

                  }
                  title = lines[lineCount];

                  title = title + " FOR " + city;

                  doc = new Destination();
                  doc.setCity(city);
                  doc.setCountry("Italy");
                  doc.setCmpyid(trip.cmpyid);
                  doc.setCreatedby("system");
                  doc.setCreatedtimestamp(System.currentTimeMillis());
                  doc.setDescription("");
                  doc.setDestinationid(DBConnectionMgr.getUniqueId());
                  doc.setDestinationtypeid(APPConstants.CITY_DESTINATION_TYPE);
                  doc.setIntro("");
                  doc.setLastupdatedtimestamp(System.currentTimeMillis());
                  doc.setModifiedby("system");
                  doc.setName(title);
                  doc.setStatus(APPConstants.STATUS_ACTIVE);
                  doc.setTag("Restaurant");
                }


                if (line.contains("+")) {
                  //found a phone number, skip lines until we find the next phone number - must be greater than 1
                  // line apart
                  int startingLine = lineCount - 2;
                  int endLine = lineCount;

                  //go backwards until we get to a blank line
                  for (int i = lineCount; i > 0; i--) {
                    String parsedLine = lines[i];
                    if (parsedLine.trim().isEmpty()) {
                      startingLine = i + 1;
                      break;
                    }
                  }

                  String restaurantName = lines[startingLine];
                  String address = lines[startingLine + 1];
                  StringBuffer contactInfo = new StringBuffer();
                  contactInfo.append(address);
                  contactInfo.append("\n \n");
                  //add everything between title and phone number
                  for (int i = startingLine + 2; i < lineCount; i++) {
                    String parsedLine = lines[i];
                    if (!parsedLine.trim().isEmpty()) {
                      contactInfo.append(parsedLine.trim());
                      contactInfo.append("\n");
                    }
                  }
                  startingLine = lineCount;

                  StringBuffer description = new StringBuffer();

                  for (int i = lineCount; i < lines.length; i++) {
                    String parsedLine = lines[i];
                    if ((parsedLine.contains("+") && (i - lineCount) > 6) || i == (lines.length - 1)) {
                      //break out
                      lineCount = i;
                      endLine = i - 3;
                      break;
                    }
                  }
                  boolean linebreak = false;

                  for (int i = startingLine; i < endLine + 1; i++) {
                    String parsedLine = lines[i];

                    if (parsedLine.contains("+") || parsedLine.contains("@") || parsedLine.contains("www") || parsedLine
                        .toUpperCase()
                        .contains("contact:")) {
                      //phone
                      contactInfo.append(parsedLine);
                      contactInfo.append("\n");
                    }
                    else if (!parsedLine.trim().isEmpty()) {
                      if (parsedLine.contains(":")) {
                        if (!linebreak) {
                          description.append("\n\n");
                          linebreak = true;
                        }
                        description.append("\n");
                      }
                      description.append(parsedLine);
                    }
                  }

                  if (doc != null) {
                    DestinationGuide page = new DestinationGuide();
                    page.setCreatedby("system");
                    page.setCreatedtimestamp(System.currentTimeMillis());
                    page.setDescription(description.toString());
                    page.setDestinationguideid(DBConnectionMgr.getUniqueId());
                    page.setDestinationid(doc.destinationid);
                    page.setIntro(contactInfo.toString());
                    page.setLastupdatedtimestamp(System.currentTimeMillis());
                    page.setModifiedby("system");
                    page.setName(restaurantName);
                    page.setRank(rank);
                    page.setCity(doc.getCity());
                    page.setCountry(doc.getCountry());
                    page.setStatus(APPConstants.STATUS_ACTIVE);
                    page.setStreetaddr1(address);
                    pages.add(page);

                    rank++;
                  }
                }
                else {
                  lineCount++;
                }
              }
              if (doc != null && pages.size() > 0) {
                Ebean.beginTransaction();
                doc.save();
                newDocs++;
                for (DestinationGuide page : pages) {
                  newPages++;
                  page.save();
                }

                Ebean.commitTransaction();
              }

              document.close();
              input.close();

            }
            catch (Exception e) {
              e.printStackTrace();
            }
          }
        }
      }
    }
    return ok("New Docs: " + newDocs + "  New Pages: " + newPages);
  }

  @With({Authenticated.class, AdminAccess.class})
  public Result testAkka() {
    try {
      Thread.sleep(4);
    }catch (InterruptedException e) {
      Log.err("My sleep was interrupted");
    }
    StopWatch sw = new StopWatch();
    sw.start();
    for (int msgCounter = 0; msgCounter < 100; ++msgCounter) {

      final FirebaseActor.CommunicatorCommand cmd = (msgCounter % 2 == 0) ?
                                                    new FirebaseActor.CommunicatorCommand(FirebaseActor
                                                                                              .CommunicatorCommand.Type.AKKA_TEST_2,
                                                                          "Src Msg #: " + msgCounter) :
                                                    new FirebaseActor.CommunicatorCommand(FirebaseActor
                                                                                              .CommunicatorCommand.Type.AKKA_TEST_1,
                                                                          "Src Msg #: " + msgCounter);

      ActorsHelper.tell(SupervisorActor.UmappedActor.FIREBASE, cmd);
    }
    sw.stop();
    Log.info("Sent 100 message  to communicator in: " + sw.getTime() + "ms");
    return TODO;
  }


  @With({Authenticated.class, AdminAccess.class})
  public Result testMobileNotifications() {

//    final MobileNotificationActor.Command cmd1 = new MobileNotificationActor.Command("thierry@umapped.com", "This is the actor test\n\nHere we go.", DBConnectionMgr.getUniqueLongId());
//    SupervisorActor.tell(SupervisorActor.UmappedActor.MOBILE_NOTIFY, cmd1);
    //String device_id = "APA91bEO2jpkEQOnu8n1V_9S_8kTJrOce84L4NFbqwqpeyrcCbXAIefmkTKBuZal3YNPbR2UMML_0ZEoJtL-O3kVjE-FcarDgg-9hUVFtV-jP9K31tJg57kwHvguBR9HA4BIbGxIwvMo";

    String umapped_key="AIzaSyCzxHweHy__gl5YgpyB9VPgrikcty9cj9g";
   // String device_id = "APA91bGAspFexlkx-t40vfNmohh2PZS4-DB_oTQcCv1_L3kxkfe8D909-Z5FzZ" +
   //                    "-WMhLa4hVUNxGoFjy92Dg2VQlpVdc2FeePTn26eCqWHrUiP_IzcMVSKCTG1NLL6P9bLuCAikS3Kv59";

    String device_id = "APA91bGhe-pssmQ1cNsVn1k3Q0Y2ka1EE1vmVGEATjE2WbTnbpwOA0a14c0_V8RBsy6VKmUOrYZLd4o6GOz3J5L3RhjxxzKIHXkdemJwYTBGjHcVlvw2Q600Xi5u-PqqrjYgBXuIP-O9";

    String top_key="AIzaSyBZ0BXq2uYm-P2kLVeFfghIuRM-fA7YjHg";
    try {
      Sender s = new Sender(umapped_key);
      Message msg = new Message.Builder()
          .addData("title", "Notification Title 2")
          .addData("msg", "Hello World 4")
          .build();
      s.send(msg,
             device_id,
             1);
    } catch (Exception e) {
      e.printStackTrace();
    }

/*
    String email = "thierry@umapped.com";
    String msg = "New Test NOW I am ALIVE... Can't be simpler than this!\nThis is a line break";
    String payload = APNS.newPayload().alertBody(msg).build();

    try {
      List<UserSession> userSessions = UserSessionMgrExt.getSessionByEmail(email);
      if (userSessions != null) {
        for (UserSession session: userSessions) {
          if (session.getDev() != null && session.getDev().contains("TOP-ATLANTICO") && session.getDev().contains("iPhone")) {
            //top atlantico ios
            Utils.IOS_TOP_ATLANTICO_SERVICE.push(session.getDevId(),payload);
          } else if (session.getDev() != null && session.getDev().contains("SKI-COM") && session.getDev().contains("iPhone")) {
            //ski.com iphone
            Utils.IOS_SKI_COM_SERVICE.push(session.getDevId(),payload);

          } else if (session.getDev() != null && session.getDev().contains("iPhone")) {
            //ios umapped
            Utils.IOS_UMAPPED_SERVICE.push(session.getDevId(),payload);

          } else if (session.getDev() != null && session.getDev().contains("TOP-ATLANTICO") && !session.getDev().contains("iPhone")) {
            //android top-atlantico
          } else if (session.getDev() != null && session.getDev().contains("SKI-COM") && !session.getDev().contains("iPhone")) {
            //android ski.com
          } else if (session.getDev() != null && !session.getDev().contains("iPhone")) {
            //android umapped
          }
        }
      }
    } catch (Exception e) {
      Log.err("Error sending notification", e);
    }
*/

    return TODO;
  }

  //@With({Authenticated.class, AdminAccess.class})
  public Result poimodel(String termi) {
    /*
    if(term == null) {
      return UserMessage.message(ReturnCode.HTTP_REQ_MISSING_DATA);
    }*/
    String[] terms = {"moscow", "york", "barbados", "st. lucia", "san", "royal", "paris", "london", "town", "city"};


    long oldTime = 0;
    long newTime = 0;


    StringBuilder sb = new StringBuilder();
    StopWatch     sw = new StopWatch();
    try {
      Set<Integer> cmpyIds = new HashSet<>();
      cmpyIds.add(0);
      List<Integer> poiTypes = new ArrayList<>();
      for (int tId = 1; tId < 10; tId++) {
        poiTypes.add(tId);
      }

      for (String term : terms) {
        sw.reset();
        sw.start();


        Map<Long, List<PoiRS>> recs = PoiMgr.findByTerm(term, poiTypes, cmpyIds, 1000);
        sw.stop();
        int recCount = 0;
        for (List<PoiRS> lrs : recs.values()) {
          recCount += lrs.size();
        }

        oldTime += sw.getTime();

        sb.append("Found ")
            .append(recCount)
            .append(" records in ")
            .append(sw.getTime())
            .append(" ms for term:").append(term)
            .append(System.lineSeparator());
      }

      for (String term : terms) {
        sw.reset();
        sw.start();
        List<Poi> models = Poi.findByTerm(term, poiTypes, cmpyIds, 1000);
        sw.stop();

        newTime += sw.getTime();

        sb
            .append("Found ")
            .append(models.size())
            .append(" models in ")
            .append(sw.getTime())
            .append(" ms for term:").append(term)
            .append(System.lineSeparator());
      }

      sb.append(terms.length).append(" term search. Old style: ").append(oldTime).append(" eBean: ").append(newTime)
        .append(System.lineSeparator());

    }
    catch (Exception e) {
      e.printStackTrace();
    }

    try {
      Poi p = Poi.build(0, 0, 0, 1, "sergei");

      p.setName("Test POI");
      p.setCode("CODE");
      p.setCountryCode("RUS");
      p.setLocLat(11.11f);
      p.setLocLong(33.33f);
      p.setSearchWords("no search words");

      sw.reset();
      sw.start();
      p.insertManual();
      sw.stop();
      sb.append("Inserted record in ").append(sw.getTime()).append("ms").append(System.lineSeparator());

      sw.reset();
      sw.start();
      p.setCode("CODE2");
      p.updateManual();
      sw.stop();
      sb.append("Updated record in ").append(sw.getTime()).append("ms").append(System.lineSeparator());


      sw.reset();
      sw.start();
      Poi fp = Poi.findByPk(p.getPk().getPoiId(),
                            p.getPk().getConsortiumId(),
                            p.getPk().getCmpy_id(),
                            p.getPk().getSrcId());
      sw.stop();
      sb.append("Found record in ").append(sw.getTime()).append("ms").append(System.lineSeparator());

    } catch (Exception e) {
      Log.err("Failed to create eBean POI record", e);
    }

    return ok(sb.toString());
  }

  public Result testAnalysis() {
    TripAnalysisActor.Command command = new TripAnalysisActor.Command();
    ActorsHelper.tell(SupervisorActor.UmappedActor.TRIP_ANALYZE, command);
    return ok("Actor triggered -" + command.getRunInstant().toEpochMilli());
  }

  public Result testChunk() {
    StringBuilder sb = new StringBuilder();
    sb.append("\n     <--alertMsg-->\n");
    sb.append("    <div id=\"alertMsg\">\n");
    sb.append("    <h3>\n");
    sb.append("        17 bookings imported\n\n");
    sb.append("    </h3>\n");
    sb.append("    </div\n\n");
    sb.append("    <--endAlertMsg-->\n");

    StringBuilder sb1 = new StringBuilder();
    sb1.append("Hello");

    Source<ByteString, ?> source = Source.<ByteString>actorRef(65535, OverflowStrategy.dropNew())
            .mapMaterializedValue(sourceActor -> {

              StopWatch sw = new StopWatch();
              sw.start();
              boolean timeout = true;
              while (sw.getTime() < 150000) {
                try {
                  Thread.sleep(1000);
                  if (sw.getTime() > 5000) {
                    sourceActor.tell(ByteString.fromString(sb.toString()), null);

                    timeout = false;
                    break;
                  }
                  else {
                    sourceActor.tell(ByteString.fromString(" "), null);
                  }
                }
                catch (Exception e) {
                }
              }
              if (timeout) {
                BaseView baseView = new BaseView();
                baseView.message = "Error Retrieving the Record Locator - please contact support@umapped.com if the problem persists";
                String s = views.html.common.message.render(baseView).toString();
                sourceActor.tell(ByteString.fromString(s), null);
              }

              sourceActor.tell(new Status.Success(NotUsed.getInstance()), null);
              return null;
            });
    // Serves this stream with 200 OK
    return ok().chunked(source);

    /*
    try {
      RecordLocatorForm formInfo = new RecordLocatorForm();
      formInfo.setInLastName("faust");
      formInfo.setInRecordLocator("MIPN3W");
      formInfo.setInTripId("1245767883490000008");
      formInfo.setUserId("thierry");
      formInfo.setInProvider(RecordLocatorForm.Provider.WORLDSPAN);
      final String recLocator = formInfo.getInRecordLocator();


      CacheMgr.set(APPConstants.CACHE_REC_LOCATOR_WEB + formInfo.getInRecordLocator()+ formInfo.getUserId(), null);

      final RecLocatorActor.Command cmd = new RecLocatorActor.Command(formInfo);
      ActorsHelper.tell(SupervisorActor.UmappedActor.RECORD_LOCATOR, cmd);

      Source<ByteString, ?> source = Source.<ByteString>actorRef(65535, OverflowStrategy.dropNew())
              .mapMaterializedValue(sourceActor -> {

                StopWatch sw = new StopWatch();
                sw.start();
                boolean timeout = true;
                while (sw.getTime() < 150000) {
                  try {
                    Thread.sleep(1000);
                    Object o = CacheMgr.get(APPConstants.CACHE_REC_LOCATOR_WEB + recLocator + formInfo.getUserId());
                    if (o != null) {
                      if (o instanceof BaseView) {
                        BaseView baseView = (BaseView) o;
                        String s = views.html.common.message.render(baseView).toString();
                        System.out.println(s);
                        sourceActor.tell(ByteString.fromString(s), null);
                      }
                      else {
                        BaseView baseView = new BaseView();
                        baseView.message = "Error Retrieving the Record Locator - please contact support@umapped.com if the problem persists";
                        String s = views.html.common.message.render(baseView).toString();
                        sourceActor.tell(ByteString.fromString(s), null);
                      }
                      timeout = false;
                      break;
                    }
                    else {
                      sourceActor.tell(ByteString.fromString(""), null);
                    }
                  }
                  catch (Exception e) {
                  }
                }
                if (timeout) {
                  BaseView baseView = new BaseView();
                  baseView.message = "Error Retrieving the Record Locator - please contact support@umapped.com if the problem persists";
                  String s = views.html.common.message.render(baseView).toString();
                  sourceActor.tell(ByteString.fromString(s), null);
                }
                sourceActor.tell(new Status.Success(NotUsed.getInstance()), null);
                return null;
              });
      // Serves this stream with 200 OK
      return ok().chunked(source);

    }
    catch (Exception e) {
      e.printStackTrace();
      BaseView baseView = new BaseView();
      baseView.message = "Error Retrieving the Record Locator - please contact support@umapped.com if the problem persists";
      return ok(views.html.common.message.render(baseView));
    }
    */
  }

  @With({Authenticated.class, Authorized.class})
  public Result updateCityPhotos() {
    final DynamicForm form = formFactory.form().bindFromRequest();
    final String url = form.get("inUrl");
    final String mode = form.get("inMode");


    int count = 0;
    int failed = 0;
    int success = 0;
    UploadView view = new UploadView();


    if (url != null && url.toLowerCase().startsWith("http") && mode.equals("YES")) {

      view.url = url;
      HashMap<String, String> imgFiles = null;
      StringBuffer sb = new StringBuffer();

      try {
        InputStream input = new URL(url).openStream();
        Reader reader = new InputStreamReader(input);
        CSVReader csvReader = new CSVReader(reader);
        String[] nextLine;
        while ((nextLine = csvReader.readNext()) != null) {
          failed++;
          count++;
          if (nextLine.length > 2) {
            String city = nextLine[0].trim();
            if (city.indexOf(",") > 1) {
              city = city.substring(0, city.indexOf(","));
            }
            String country = nextLine[1].trim();
            UMappedCityLookup.ExtraDetail details = new UMappedCityLookup.ExtraDetail();
            for (int i = 3; i < nextLine.length; i++) {
              String purl = nextLine[i];
              if (purl != null && !purl.isEmpty() && purl.toLowerCase().startsWith("http")) {
                details.addPhoto(purl.trim());
              }
            }
            if (details.photos != null) {
              try {
                UMappedCity rec = umCity.getCityCountry(city, country);
                if (rec != null) {
                  int r = umCity.updatePhoto(details, rec.getId());
                  if (r == 1) {
                   // Log.info("Processing City Photo: " + city + " " + country + " Number of Photos: " + details.photos.size());
                    success++;
                    failed--;
                  }
                } else {
                  Log.info("Processing City Photo - no city found: " + city + " " + country + " Number of Photos: " + details.photos.size());
                }
              } catch (Exception e) {
                Log.info("Error Processing City Photo: " + city + " " + country + " Number of Photos: " + details.photos.size());

              }
            } else {
              Log.info("Processing City Photo - no Photos: " + city + " " + country);
            }
          } else {
            Log.info("Processing City Photo - no city found: " + nextLine[0]);

          }

        }
      } catch (Exception e) {
        Log.err("Error Processing city photos ", e);
      }

      String s = umCity.getPhotoByCityCountry("Abu Dhabi", "United Arab Emirates");
      System.out.println("3333 " + s);
      view.message = "Loaded " + count + " Updated " + success + " cities" + " Failed: " + failed;
    }
    return ok(views.html.experimental.uploadCityPhoto.render(view));

  }

  public Result tripAnalysis (String tripId) {
    Trip trip = Trip.findByPK(tripId);
    if (trip != null) {
      ItineraryAnalyzeResult r = offerService.analyzeTrip(tripId);
      if (r != null) {
        for (TripSegmentAnalyzeResult s: r.getSegmentResults()) {
          if (s.getSegment().getStartDateTime() != null) {
            System.out.println("Start offset: " + s.getSegment().getStartDateTime().getOffset());
          }
          if (s.getSegment().getEndDateTime() != null) {
            System.out.println("End offset: " + s.getSegment().getEndDateTime().getOffset());
          }
          if (s.getSegment().getStartLocation() != null) {
            System.out.println("Start Location: " + s.getSegment().getStartLocation().getGeoLocation());
          }
          if(s.getSegment().getEndLocation() != null) {
            System.out.println("End Location: " + s.getSegment().getEndLocation().getGeoLocation());
          }


        }
      }
    }
    return ok("Not found");
  }


  public Result skitripname(){
    List<String> cmpies = new ArrayList<>();
    cmpies.add("920269968500066832");

    List<Trip> activeTrips = Trip.findActiveByCmpyIds("thierry111", 10000,cmpies, "%", 0);
    StringBuilder sb = new StringBuilder();
    int processed = 0;
    int skipped = 0;

    if (activeTrips != null) {
      sb.append("Processing - " + activeTrips.size() + "\n");
      for (Trip t: activeTrips) {
        String tripName = t.getName().replaceAll("-*.[0-9]{2}/[0-9]{2}/[0-9]{4}-[0-9]{2}/[0-9]{2}/[0-9]{4}","");
        if (!tripName.equals(t.getName())) {
          try {
            System.out.println("Trip: " + t.getTripid() + " From: " + t.getName() + " to: " + tripName);
            sb.append("Processed: (" + t.getTag() + ":" + t.getTripid() + ") From: " + t.getName() + " to " + tripName + "\n");
            processed++;
            t.setName(tripName);
            t.setModifiedby("system");
            t.setLastupdatedtimestamp(1510858800000L);
            t.save();
          } catch (Exception e) {
            e.printStackTrace();
          }
        } else {
          skipped++;
          sb.append("Skipped: (" + t.getTag() + ":" + t.getTripid() + ") From: " + t.getName() + " to " + tripName + "\n");

        }
        /*
        String[] tokens = t.name.split("\\s-\\s");
        StringBuilder tripName = new StringBuilder();
        if (tokens != null && tokens.length == 5) {
          tripName.append(tokens[0].trim());
          tripName.append(" - ");
          tripName.append(tokens[3].trim());
          tripName.append(" - ");
          tripName.append(tokens[4].trim());
          sb.append("Processed: " + t.getName() + " - " + t.getTag() + " - " + t.getTripid() + " :: " + tripName.toString());
          sb.append("\n");
          System.out.println("Processed: " + t.getName() + " - " + t.getTag() + " - " + t.getTripid() + " :: " + tripName.toString());
        } else {
          sb.append("SKIPPING: " + t.getName() + " - " + t.getTag() + " - " + t.getTripid());
          sb.append("\n");
          System.out.println("SKIPPING: " + t.getName() + " - " + t.getTag() + " - " + t.getTripid());
        }*/
      }
      sb.append("\n\n Total: " + activeTrips.size() + " Processed: " + processed + " Skipped: " + skipped);
    } else {
      sb.append("NO TRIPS");
    }
    return ok(sb.toString());

  }

  public Result skitripnote(){
    List<String> cmpies = new ArrayList<>();
    cmpies.add("920269968500066832");

    List<Trip> activeTrips = Trip.findActiveByCmpyIds("thierry111", 10000,cmpies, "%", 0);
    StringBuilder sb = new StringBuilder();
    int processed = 0;
    int skipped = 0;
    List<String> names = new ArrayList<>();
    /*names.add("Transfer General Information");
    names.add("Lifts General Information");
    names.add("Equipment General Information");
    names.add("Packing Tips");
    names.add("Travel Tips");
    names.add("Terms and Conditions");
    names.add("Important Phone Numbers and Links");*/
    names.add("Ski School General Information");

    if (activeTrips != null) {
      sb.append("Processing - " + activeTrips.size() + "\n");
      for (Trip t: activeTrips) {
        List<TripNote> tobedeleted = new ArrayList<>();
        Map<String, TripNote> tokeep = new HashMap<>();

        List<TripNote> allNotes = TripNote.getNotesByTripIdNames(t.getTripid(), names);

        if (allNotes != null) {
          for (TripNote tn: allNotes) {
            switch (tn.getName()) {
              case "Transfer General Information" :
              case "Lifts General Information" :
              case "Equipment General Information" :
              case "Packing Tips" :
              case "Travel Tips" :
              case "Terms and Conditions" :
              case "Important Phone Numbers and Links" :
              case "Ski School General Information":
                if (tokeep.containsKey(tn.getName())) {
                  tobedeleted.add(tn);
                } else {
                  tokeep.put(tn.getName(), tn);
                }
                break;
            }
          }
          if (tobedeleted.size() > 0) {
            System.out.println("\n\nProcessing: " + t.getTripid() + " - " + t.getName());
            sb.append("\n\nProcessing: " + t.getTripid() + " - " + t.getName());

            for (String key : tokeep.keySet()) {
              TripNote tn = tokeep.get(key);
              System.out.println("Keeping: " + tn.getNoteId() + " - " + tn.getName() + " created on " + Utils.formatDateTimePrint(tn.getCreatedTimestamp()) + " updated on: " + Utils.formatDateTimePrint(tn.getLastUpdatedTimestamp()));
              sb.append("\nKeeping: " + tn.getNoteId() + " - " + tn.getName() + " created on " + Utils.formatDateTimePrint(tn.getCreatedTimestamp()) + " updated on: " + Utils.formatDateTimePrint(tn.getLastUpdatedTimestamp()));

            }

            for (TripNote tn : tobedeleted) {
              tn.setStatus(-1);
              tn.setLastUpdatedTimestamp(1510858800000L);
              tn.setModifiedBy("system");
              tn.save();
              System.out.println("DELETING: " + tn.getNoteId() + " - " + tn.getName() + " created on " + Utils.formatDateTimePrint(tn.getCreatedTimestamp()) + " updated on: " + Utils.formatDateTimePrint(tn.getLastUpdatedTimestamp()));
              sb.append("\nDELETING: " + tn.getNoteId() + " - " + tn.getName() + " created on " + Utils.formatDateTimePrint(tn.getCreatedTimestamp()) + " updated on: " + Utils.formatDateTimePrint(tn.getLastUpdatedTimestamp()));

            }
            BookingNoteController.invalidateCache(t.getTripid());
            processed++;
          }  else {
            System.out.println("\n\nSkipped: " + t.getTripid() + " - " + t.getName());
            sb.append("\n\n**** Skipped: " + t.getTripid() + " - " + t.getName());

            skipped++;
          }

        }


      }
      sb.append("\n\n Total: " + activeTrips.size() + " Processed: " + processed + " Skipped: " + skipped);

    } else {
      sb.append("NO TRIPS");
    }
    return ok(sb.toString());

  }

  public Result resetPoi(Integer cmpyId) {
    List<PoiRS> pois = PoiMgr.findBy("%", cmpyId, 0, 3, 0);
    StringBuilder sb = new StringBuilder();
    int processed =0;
    int updated =0;
    if (pois != null) {
      for (PoiRS poiRS: pois) {
        System.out.println("Processing: " + poiRS.getId() + " " + poiRS.getName());
        sb.append("Processing: " + poiRS.getId() + " " + poiRS.getName() +"\n");
        processed++;
        PoiJson data = poiRS.data;
        Coordinates coordinates = null;
        if (data.getAddresses() != null) {
          for (Iterator<Address> it = data.getAddresses().iterator(); it.hasNext(); ) {
            Address currItem = it.next();
            if (currItem.getAddressType() == AddressType.MAIN) {
              coordinates = currItem.getCoordinates();
              it.remove();
            }
          }
        }
        if (data.getPhoneNumbers() != null) {
          for (Iterator<PhoneNumber> it = data.getPhoneNumbers().iterator(); it.hasNext(); ) {
            PhoneNumber currItem = it.next();
            it.remove();
          }
        }

        if (data.getEmails() != null) {
          for (Iterator<Email> it = data.getEmails().iterator(); it.hasNext(); ) {
            Email currItem = it.next();
            it.remove();
          }
        }
        if (data.getUrls() != null) {
          for (Iterator<String> it = data.getUrls().iterator(); it.hasNext(); ) {
            String currItem = it.next();
            it.remove();
          }
        }
        if (data.getSocialLinks() != null) {
          for (Iterator<SocialLink> it = data.getSocialLinks().iterator(); it.hasNext(); ) {
            SocialLink currItem = it.next();
            it.remove();
          }
        }

        Address address = new Address();
        address.setAddressType(AddressType.MAIN);
        address.setCoordinates(coordinates);
        address.setCountryCode("UNK");
        data.addAddress(address);

        Email email = new Email();
        email.setEmailType(Email.EmailType.MAIN);
        email.setAddress(" ");
        data.addEmail(email);

        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setPhoneType(PhoneNumber.PhoneType.MAIN);
        phoneNumber.setNumber(" ");
        data.addPhoneNumber(phoneNumber);

        PhoneNumber fax = new PhoneNumber();
        fax.setPhoneType(PhoneNumber.PhoneType.FAX);
        fax.setNumber(" ");
        data.addPhoneNumber (fax);

        data.addUrl(" ");
        poiRS.countryCode = "UNK";

        poiRS.modifiedby = "system";
        poiRS.lastupdatedtimestamp = 1510929836000L;
          PoiMgr.update(poiRS);
          sb.append("Updated: " + poiRS.getId() + " " + poiRS.getName() +"\n");
          updated++;


      }
    }
    sb.append("\n\n Processed: " + processed + " Updated: " + updated);
    return ok(sb.toString());
  }

  @With({AdminAccess.class, Authenticated.class, Authorized.class})
  public Result prosperworks(String auth) {
    if (auth != null && auth.equals("i7J368GEa49W7EagKnuZJZ1spFf9EZYY")) {
      //start generating the data
      StringWriter sw = new StringWriter();
      CSVWriter csvWriter = new CSVWriter(sw);
      String[] header = {"umid", "First Name", "Last Name", "Company", "Street", "City", "State", "Postal Code", "Country",
                         "Contact Type", "Work Phone", "Email", "Work Website", "Publisher User Id", "Consortia Affiliation", "plan type", "plan currency", "plan amount", "plan duration", "plan bill date", "pymt type", "bill-to","paid by","last login", "create date", "cutoff date", "parent cmpy"};
      csvWriter.writeNext(header);
      Map<String, Company> companies = new HashMap<>();
      Map<String, CmpyAddress> companyAddress = new HashMap<>();
      Map<String, Boolean> hasConsortia = new HashMap<>();
      Map<String, String> consortia = new HashMap<>();
      Map<Integer, Consortium> consortiums = new HashMap<>();
      ArrayList<String>planNames = new ArrayList<>();

      long timestamp = Instant.now().minus(Duration.ofDays(60)).toEpochMilli();

      List<AccountCmpyLink> accountCmpyLink = AccountCmpyLink.find.all();
      for (AccountCmpyLink acl: accountCmpyLink) {
        String umid;
        String fName;
        String lName;
        String cName;
        String street = "";
        String city = "";
        String state = "";
        String postalCode = "";
        String country = "";
        String type;
        String phone ="";
        String email ="";
        String web = "";
        String userId;
        String consortium = "";
        String planName = "";
        String planCurrency = "";
        String planAmt = "";
        String planDuration = "";
        String planBillDate = "";
        String billingType = "";
        String billTo = "";
        String paidBy = "";
        String lastLogin = "";
        String createDate = "";
        String cutoffDate = "";
        String parentCmpy = "";


        Account a = Account.find.byId(acl.getUid());
        AccountProp contact = AccountProp.findByPk(a.getUid(), AccountProp.PropertyType.CONTACT);
        AccountProp billing = AccountProp.findByPk(a.getUid(), AccountProp.PropertyType.BILLING_INFO);
        AccountAuth aa = AccountAuth.getByType(a.getUid(), AccountAuth.AAuthType.PASSWORD);

        UserProfile up = UserProfile.findByPK(a.getLegacyId());
        if (aa == null || aa.getSuccessLastTs() == null || aa.getSuccessLastTs().getTime() <  timestamp) {
          type = "UM-INACTIVE";
        } else {
          type = "UM-ACTIVE";
        }



        Company c = companies.get(acl.getCmpyid());
        if (c == null) {
          c = Company.find.byId(acl.getCmpyid());
          companies.put(c.getCmpyid(), c);
        }
        if (c.getParentCmpyId() != null && !c.getParentCmpyId().isEmpty()) {
          Company p = companies.get(c.getParentCmpyId());
          if (p == null) {
            p = Company.find.byId(c.getParentCmpyId());
            if (p != null) {
              companies.put(p.getCmpyid(), p);
            }
          }
          if (p != null) {
            parentCmpy = p.getName();
          }
        }
        if (a.getAccountType() == Account.AccountType.PUBLISHER && a.getState() != RecordStatus.DELETED && !a.getEmail().contains("umapped.com") && !a.getEmail().contains("whatsyou")
            && !c.getName().toLowerCase().contains(" demo company")&& !c.getName().toLowerCase().contains("umapped") && !c.getName().toLowerCase().contains("your travel company")) {

          CmpyAddress ca = companyAddress.get(c.getCmpyid());
          if (ca == null) {
            List<CmpyAddress> addrs = CmpyAddress.findActiveByCmpyId(c.getCmpyid());
            if (addrs != null && addrs.size() > 0) {
              ca = addrs.get(0);
              companyAddress.put(c.getCmpyid(), ca);
            }
          }
          BillingSettingsForUser bsu = BillingSettingsForUser.findSettingsByUserId(a.getLegacyId());


          umid = String.valueOf(a.getUid());
          fName = a.getFirstName();
          lName = a.getLastName();
          cName = c.getName();
          if (ca != null) {
            street = (ca.streetaddr1 == null ? "" : ca.streetaddr1);
            city = (ca.city == null ? "" : ca.city);
            state = (ca.state == null ? "" : ca.state);
            postalCode = (ca.zipcode == null ? "" : ca.zipcode);
            country = (ca.country == null ? "" : ca.country);
          }
          if (contact != null && contact.accountContact != null) {
            if (contact.accountContact.phones != null && contact.accountContact.phones.get(PhoneNumber.PhoneType.BUSINESS) != null) {
              phone = contact.accountContact.phones.get(PhoneNumber.PhoneType.BUSINESS).getNumber();
            }
            if (contact.accountContact.phones != null && contact.accountContact.phones.get(PhoneNumber.PhoneType.MAIN) != null) {
              phone = contact.accountContact.phones.get(PhoneNumber.PhoneType.MAIN).getNumber();
            }
            if (contact.accountContact.urls != null && !contact.accountContact.urls.isEmpty()) {
              web = contact.accountContact.urls.iterator().next();
            }
          }
          if (ca != null && ca.getWeb() != null && web.isEmpty()) {
            web = ca.getWeb();
          }
          email = a.getEmail();
          userId = a.getLegacyId();

          if (phone != null && phone.length() >= 30) {
            phone = phone.substring(0,29);
          }

          if (web != null && (!web.startsWith("http") || !web.contains("."))) {
            web = "";
          }

          if (!hasConsortia.containsKey(c.getCmpyid())) {
            List<ConsortiumCompany> cc = ConsortiumCompany.findByCmpyId(c.getCmpyId());
            StringBuilder sb = new StringBuilder();
            for (ConsortiumCompany consortiumCompany: cc) {
              Consortium co = consortiums.get(consortiumCompany.getConsId());
              if (co == null) {
                co = Consortium.find.byId(consortiumCompany.getConsId());
                consortiums.put(consortiumCompany.getConsId(), co);
              }
              if (!sb.toString().isEmpty()) {
                sb.append("/");
              }
              sb.append(co.getName());
            }
            if (!sb.toString().isEmpty()) {
              consortia.put(c.getCmpyid(), sb.toString());
              hasConsortia.put(c.getCmpyid(),Boolean.TRUE);
              consortium = sb.toString();
            }
          } else if (hasConsortia.get(c.getCmpyid()) == Boolean.TRUE) {
            consortium = consortia.get(c.getCmpyid());
          }

          if (bsu != null) {
            BillingPlan bp = null;
            if (bsu.getCutoffTs() != null && bsu.getCutoffTs() > 0) {
              cutoffDate = Utils.formatTimestamp(bsu.getCutoffTs(),"MM/dd/yyyy");
            }
            if (bsu.getBillTo().getName() == BillingEntity.Type.USER) {
              bp = bsu.getPlan();
              billTo = "USER";
              paidBy = a.getLegacyId();
            } else if (bsu.getBillTo().getName() == BillingEntity.Type.COMPANY) {
              BillingSettingsForCmpy bsc = BillingSettingsForCmpy.find.byId(bsu.getCmpyid());
              if (bsc != null) {
                billTo = "CMPY";
                paidBy = c.getName();
                bp = bsc.getPlan();
              }
            } else if (bsu.getBillTo().getName() == BillingEntity.Type.AGENT) {
              BillingSettingsForUser bsu1 = BillingSettingsForUser.findSettingsByUserId(bsu.getAgent());
              if (bsu1 != null) {
                billTo = "AGENT";
                paidBy = bsu1.getUserId();
                bp = bsu1.getPlan();
              }
            }

            if (bp != null) {
              planName = bp.getName().trim();
              planName = planName.replace(" ", "_")
                                 .replace("(CAD)", "")
                                 .replace("(USD)", "")
                                 .replace("(AUD)", "")
                                 .replace("Monthly", "")
                                 .replace("Annual", "")
                                 .replace("Prepaid", "")
                                 .replace("Invoice", "")
                                 .replace("INVOICE", "")
                                 .replace("per Month", "")
                                 .replace("USD","")
                                 .replace("CAD","")
                                 .trim();
              planNames.add(planName);
              planCurrency = bp.getCurrency().name();
              planAmt = String.valueOf(bp.getCost());
              if (billing != null) {
                billingType = "CC";
              }
              else {
                billingType = "INV";
              }
              if (bsu.getSchedule().getName() == BillingSchedule.Type.TRIAL) {
                if (bsu.getCutoffTs() != null && bsu.getCutoffTs() > timestamp) {
                  type = "UM-TRIAL";
                } else {
                  type = "UM-LOST";
                }

              }
              planDuration = bp.getSchedule().getScheduleName();
              planBillDate = Utils.formatTimestamp(bsu.getStartTs(), "MM/dd/yyyy");
            }
          }
          if (aa != null && aa.getSuccessLastTs() != null) {
            lastLogin = Utils.formatTimestamp(aa.getSuccessLastTs().getTime(), "MM/dd/yyyy");
          }
          createDate = Utils.formatTimestamp(up.createdtimestamp,"MM/dd/yyyy");
          String[] line = {umid, fName, lName, cName, street, city, state, postalCode, country, type, phone, email, web, userId, consortium, planName, planCurrency, planAmt, planDuration, planBillDate, billingType,billTo, paidBy, lastLogin, createDate, cutoffDate, parentCmpy};
          csvWriter.writeNext(line);
        }
      }
      try {
        for (String s: planNames) {
          System.out.println(s);
        }
        csvWriter.flush();
        csvWriter.close();
        S3Util.saveFile(sw.toString().getBytes(), "report/lA31Iksuwg/prosperworks.csv", "text/csv");
      } catch (Exception e) {

      }
    }
    BaseView view = new BaseView("https://s3.amazonaws.com/" + S3Util.getPDFBucketName()+"/report/lA31Iksuwg/prosperworks.csv");
    return ok(views.html.common.message.render(view));
  }
}
