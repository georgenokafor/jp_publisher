package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * An airport.
 * https://schema.org/Airport
 * Created by surge on 2015-05-11.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Airport
    extends Place {
  /**
   * 	Text 	IATA identifier for an airline or airport.
   */
  public String iataCode;

  /**
   * Text 	IACO identifier for an airport.
   */
  public String icaoCode;
}
