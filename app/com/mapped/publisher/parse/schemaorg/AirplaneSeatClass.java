package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Type name: http://schema.org/AirplaneSeatClass
 * Created by surge on 2015-05-11.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AirplaneSeatClass
    extends Thing {
  /**
   * [Text]	The IATA class code for the seat.
   */
  public String iataClassCode;
  /**
   * [Text]	The seat category.
   */
  public String seatCategory;
}
