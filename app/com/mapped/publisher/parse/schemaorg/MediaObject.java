package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by surge on 2016-06-29.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MediaObject
    extends CreativeWork {
  /**
   * A NewsArticle associated with the Media Object. (IGNORED)
   */
  //public NewsArticle associatedArticle;

  /**
   * 	The bitrate of the media object.
   * 	Text
   */
  public String bitrate;
  /**
   * 	Text 	File size in (mega/kilo) bytes.
   */
  public String contentSize;
  /**
   * Actual bytes of the media object, for example the image file or video file.
   * URL
   */
  public String contentUrl;

  /**
   * The duration of the item (movie, audio recording, event, etc.) in ISO 8601 date format.
   */
  //Duration duration;

  /**
   * A URL pointing to a player for a specific video. In general, this is the information in the src element of an embed tag and should not be the same as the content of the loc tag.
   *	URL
   */
  public String embedUrl;

  /**
   * The CreativeWork encoded by this media object. (IGNORED)
   */
  //CreativeWork encodesCreativeWork;

  /**
   * mp3, mpeg4, etc.
   * 	Text
   */
  public String encodingFormat;
  /**
   * Date the content expires and is no longer useful or available. Useful for videos.
   * Date
   */
  public Date expires;

  /**
   * The height of the item.
   * Distance  or QuantitativeValue
   */
  //Distance height;

  /**
   * Date when this media object was uploaded to this site.
   * Date
   */
  public Date uploadDate;

  /**
   * Distance  or QuantitativeValue 	The width of the item.
   */
  //public Distance width;


/*
  playerType	Text 	Player type required—for example, Flash or Silverlight.
  productionCompany	Organization 	The production company or studio responsible for the item e.g. series, video game, episode etc.
  regionsAllowed	Place 	The regions where the media is allowed. If not specified, then it's assumed to be allowed everywhere. Specify the countries in ISO 3166 format.
  requiresSubscription	Boolean 	Indicates if use of the media require a subscription (either paid or free). Allowed values are true or false (note that an earlier version had 'yes', 'no').
 */
}
