
package com.mapped.publisher.parse.globus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfHotelAmenity complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ArrayOfHotelAmenity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HotelAmenity" type="{http://webapi.globusandcosmos.com/}HotelAmenity" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfHotelAmenity", propOrder = {
    "hotelAmenities"
})
public class ArrayOfHotelAmenity {

    @XmlElement(name = "HotelAmenity", nillable = true)
    protected ArrayList<HotelAmenity> hotelAmenities;

    /**
     * Gets the value of the hotelAmenities property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hotelAmenities property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHotelAmenities().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HotelAmenity }
     *
     *
     */
    public ArrayList<HotelAmenity> getHotelAmenities() {
        if (hotelAmenities == null) {
            hotelAmenities = new ArrayList<HotelAmenity>();
        }
        return this.hotelAmenities;
    }

}
