package com.mapped.publisher.parse.extractor;

import com.mapped.publisher.parse.valueObject.TravelDetails;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-26
 * Time: 8:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class BigFiveTourExtractorDocx extends BigFiveTourExtractor {

    protected TravelDetails parse(InputStream fis) {
        try {
            XWPFDocument doc = new XWPFDocument(fis);
            XWPFWordExtractor wordxExtractor = new XWPFWordExtractor(doc);
            // extract all paragraphs
            String allParagraphs = wordxExtractor.getText();

            //get the intro
            if (allParagraphs != null) {
                return parseFile(allParagraphs);
            }
        } catch (Exception exep) {
            exep.printStackTrace();
        }


        return null;
    }
}
