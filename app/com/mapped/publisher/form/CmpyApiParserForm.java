package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-09-20
 * Time: 3:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class CmpyApiParserForm {
    private String inCmpyId;
    public String inParserId;
    public String inName;
    public String inFilePattern;
    public String inParser;
    public String inParams;
    public int inParserType;


    public String getInCmpyId() {
        return inCmpyId;
    }

    public void setInCmpyId(String inCmpyId) {
        this.inCmpyId = inCmpyId;
    }

    public String getInParserId() {
        return inParserId;
    }

    public void setInParserId(String inParserId) {
        this.inParserId = inParserId;
    }

    public String getInName() {
        return inName;
    }

    public void setInName(String inName) {
        this.inName = inName;
    }

    public String getInFilePattern() {
        return inFilePattern;
    }

    public void setInFilePattern(String inFilePattern) {
        this.inFilePattern = inFilePattern;
    }

    public String getInParser() {
        return inParser;
    }

    public void setInParser(String inParser) {
        this.inParser = inParser;
    }

    public String getInParams() {
        return inParams;
    }

    public void setInParams(String inParams) {
        this.inParams = inParams;
    }

    public int getInParserType() {
        return inParserType;
    }

    public void setInParserType(int inParserType) {
        this.inParserType = inParserType;
    }
}
