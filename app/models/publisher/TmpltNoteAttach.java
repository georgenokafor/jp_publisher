package models.publisher;

import com.avaje.ebean.Model;
import com.mapped.publisher.persistence.PageAttachType;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.List;

@Entity @Table(name = "tmplt_note_attach")
public class TmpltNoteAttach
    extends Model {
  public static Finder<Long, TmpltNoteAttach> find = new Finder(TmpltNoteAttach.class);

  @Id
  public Long fileId;
  @Constraints.Required

  @OneToOne @JoinColumn(name = "note_id")
  public TmpltNote tmpltNote;

  public String name;
  public String attachName;
  public String attachUrl;
  public String attachType;
  public int rank;
  public int status;
  public String tag;
  public String comments;
  public Long   createdTs;
  public Long   modifiedTs;
  public String createdBy;
  public String modifiedBy;
  @Version
  public int version;

  public static List<TmpltNoteAttach> findByNoteId(Long noteId) {
    return find.where().eq("note_Id", noteId).eq("status", 0).findList();
  }

  public static int maxPageRankByNoteId(Long noteId, String attachType) {
    return find.where().eq("note_Id", noteId).eq("attach_type", attachType).findCount();

  }

  public static List<TmpltNoteAttach> findPhotosByNoteId(Long noteId) {
    return find.where()
               .eq("note_Id", noteId)
               .eq("status", 0)
               .eq("attach_type", PageAttachType.PHOTO_LINK.getStrVal())
               .findList();

  }

  public TmpltNote getTmpltNote() {
    return tmpltNote;
  }

  public void setTmpltNote(TmpltNote tmpltNote) {
    this.tmpltNote = tmpltNote;
  }

  public Long getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Long createdTs) {
    this.createdTs = createdTs;
  }

  public Long getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Long modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public Long getFileId() {
    return fileId;
  }

  public void setFileId(Long fileId) {
    this.fileId = fileId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAttachName() {
    return attachName;
  }

  public void setAttachName(String attachName) {
    this.attachName = attachName;
  }

  public String getAttachUrl() {
    return attachUrl;
  }

  public void setAttachUrl(String attachUrl) {
    this.attachUrl = attachUrl;
  }

  public String getAttachType() {
    return attachType;
  }

  public void setAttachType(String attachType) {
    this.attachType = attachType;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
