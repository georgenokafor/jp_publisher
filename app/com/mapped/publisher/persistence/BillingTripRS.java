package com.mapped.publisher.persistence;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-03-26
 * Time: 11:04 AM
 * To change this template use File | Settings | File Templates.
 */
public class BillingTripRS {

  private String tripId;
  private String groupId;
  private String tripName;
  private String groupName;
  private String cmpyName;
  private int numPublished;
  private long createdTimestamp;
  private String createdBy;
  private String firstName;
  private String lastName;

  public String getTripId() {
    return tripId;
  }

  public void setTripId(String tripId) {
    this.tripId = tripId;
  }

  public String getGroupId() {
    return groupId;
  }

  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }

  public String getTripName() {
    return tripName;
  }

  public void setTripName(String tripName) {
    this.tripName = tripName;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public String getCmpyName() {
    return cmpyName;
  }

  public void setCmpyName(String cmpyName) {
    this.cmpyName = cmpyName;
  }

  public int getNumPublished() {
    return numPublished;
  }

  public void setNumPublished(int numPublished) {
    this.numPublished = numPublished;
  }

  public long getCreatedTimestamp() {
    return createdTimestamp;
  }

  public void setCreatedTimestamp(long createdTimestamp) {
    this.createdTimestamp = createdTimestamp;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
}
