package com.umapped.api.schema.local;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.umapped.api.schema.common.IBodyJson;
import com.umapped.api.schema.types.Operation;

import java.util.ArrayList;
import java.util.List;

/**
 * JSON Message body for all operations related to consortia.
 *
 * Created by surge on 2014-10-28.
 */
@JsonTypeName("CONSORTIUM")

public class ConsortiumJson
    implements IBodyJson {

  @JsonProperty(required = false, value = "operation")
  public Operation operation;

  @JsonProperty(required = false, value = "search_term")
  public String searchTerm;

  @JsonProperty(required = false, value = "search_id")
  public Integer searchId;

  /**
   * Start location for search results
   */
  @JsonProperty(required = false, value = "cursor")
  public Integer cursor;

  /**
   * Maximum number of results
   */
  @JsonProperty(required = false, value = "count")
  public Integer count;

  public static class ConsortiumDetails {
    public Integer id;
    public String name;
  }

  public static class ConsortiumCompanyDetails {
    @JsonProperty(value = "consortium_id")
    public Integer consortiumId;
    @JsonProperty(value = "company_id")
    public Integer companyId;
    @JsonProperty(value = "company_name")
    public String  companyName;
  }

  /**
   * List of consortia to which operation will be performed.
   * For example:
   *  - Create operation would create new consortia if IDs are not set,
   *  - Delete operation would delete all consortia with ids in the list
   *  - Update operation would update names for the items in the list that have both ID and Name
   */
  public List<ConsortiumDetails> consortia;


  /**
   * List of companies belonging to consortia.
   * Operations will apply to every item in the list, i.e.:
   *  - "ADD" - Creates new association of the consortium to the company
   */
  public List<ConsortiumCompanyDetails> companies;


  public void addConsortium(Integer id, String name) {
    ConsortiumDetails cd = new ConsortiumDetails();
    cd.id = id;
    cd.name = name;

    if (consortia == null) {
      consortia = new ArrayList<>();
    }
    consortia.add(cd);
  }

  public void addConsortiumCompany(Integer consortiumId, Integer companyId, String companyName) {
    ConsortiumCompanyDetails ccd = new ConsortiumCompanyDetails();
    ccd.companyId = companyId;
    ccd.consortiumId = consortiumId;
    ccd.companyName = companyName;

    if (companies == null) {
      companies = new ArrayList<>();
    }
    companies.add(ccd);
  }

}
