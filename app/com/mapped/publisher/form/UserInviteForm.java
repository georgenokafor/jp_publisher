package com.mapped.publisher.form;

/**
 * Created by twong on 2014-05-23.
 */
public class UserInviteForm {

  private String inInviteId;
  private String inUserId;

  public String getInInviteId() {
    return inInviteId;
  }

  public void setInInviteId(String inInviteId) {
    this.inInviteId = inInviteId;
  }

  public String getInUserId() {
    return inUserId;
  }

  public void setInUserId(String inUserId) {
    this.inUserId = inUserId;
  }
}
