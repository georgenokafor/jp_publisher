package com.mapped.publisher.parse.extractor.booking.valueObject;

import com.mapped.publisher.parse.schemaorg.ImageObject;
import com.mapped.publisher.parse.schemaorg.VideoObject;
import com.umapped.api.schema.types.Address;
import com.mapped.publisher.parse.schemaorg.Place;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 15-10-04.
 */
public class NoteVO
    extends BaseVO {
  private Timestamp timestamp;
  private String tripDetailId;

  private float locLat;
  private float locLong;
  private String tag;
  private String intro;
  private String description;
  private String streetAddr;
  private String city;
  private String zipcode;
  private String state;
  private String country;
  private String landmark;
  private Place place;
  private List<ImageObject> images;

  public Place getPlace() {
    return place;
  }

  public NoteVO setPlace(Place place) {
    this.place = place;
    return this;
  }



  public Timestamp getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Timestamp timestamp) {
    this.timestamp = timestamp;
  }

  public String getTripDetailId() {
    return tripDetailId;
  }

  public void setTripDetailId(String tripDetailId) {
    this.tripDetailId = tripDetailId;
  }

  public float getLocLat() {
    return locLat;
  }

  public void setLocLat(float locLat) {
    this.locLat = locLat;
  }

  public float getLocLong() {
    return locLong;
  }

  public void setLocLong(float locLong) {
    this.locLong = locLong;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public String getIntro() {
    return intro;
  }

  public void setIntro(String intro) {
    this.intro = intro;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getStreetAddr() {
    return streetAddr;
  }

  public void setStreetAddr(String streetAddr) {
    this.streetAddr = streetAddr;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getLandmark() {
    return landmark;
  }

  public void setLandmark(String landmark) {
    this.landmark = landmark;
  }

  public void addImage(ImageObject vo) {
    if(images == null) {
      images = new ArrayList<>(1);
    }
    images.add(vo);
  }

  public void setImages(List<ImageObject> images) {
    this.images = images;
  }

  public List<ImageObject> getImages() {
    return this.images;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(
    "NoteVO{" +
        "timestamp=" + timestamp +
        ", origSrcPK='" + origSrcPK + '\'' +
        ", tripDetailId='" + tripDetailId + '\'' +
        ", locLat=" + locLat +
        ", locLong=" + locLong +
        ", tag='" + tag + '\'' +
        ", intro='" + intro + '\'' +
        ", description='" + description + '\'' +
        ", streetAddr='" + streetAddr + '\'' +
        ", city='" + city + '\'' +
        ", zipcode='" + zipcode + '\'' +
        ", state='" + state + '\'' +
        ", country='" + country + '\'' +
        ", landmark='" + landmark + '\'' +
        '}');

    sb.append("Name: ");
    sb.append(name);
    sb.append("\n");
    sb.append("Timestamp: ");
    sb.append(timestamp);
    sb.append("\n");
    sb.append("Note: ");
    sb.append(note);
    sb.append("\n");
    return sb.toString();
  }
}
