package com.umapped.itinerary.analyze.segment;

/**
 * Created by wei on 2017-03-08.
 */
public enum TransitSegmentFeature {
  PreviousSegmentNature,
  PreviousSegmentDuration,
  NextSegmentNature,
  NextSegmentDuration
}
