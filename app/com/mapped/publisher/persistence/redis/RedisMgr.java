package com.mapped.publisher.persistence.redis;

import com.mapped.common.CoreConstants;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.utils.Log;
import org.apache.commons.lang3.SerializationUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;
import redis.clients.util.SafeEncoder;

import javax.inject.Singleton;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.BiConsumer;

import static java.util.Optional.ofNullable;

/**
 * Singleton Redis Manager
 * Created by surge on 2016-02-01.
 */
@Singleton
public class RedisMgr {

  /**
   * Defines method used for writing to Redis data store
   */
  public enum WriteMode {
    /**
     * Fastest method to write to Redis.
     * <b>Important! objects written via this method must be immutable otherwise what is sent over the wire is not
     * defined</b>
     * In this method serialization of the object and the write are all scheduled to happen in the future.
     * Do not rely on this mode of write if you know someone might need to read data immediately
     */
    UNSAFE,
    /**
     * This method is slower than unsafe.
     * In this method serialization of the object happens before scheduling future write request.
     * This method is safe to be used with mutable data as by the time function returns binary representation of the
     * object is already obtained.
     */
    FAST,
    /**
     * Slow, sometimes very slow method to write to Redis.
     * In this method no operation is schedule in the future and compared to previous methods object is serialized and
     * then sent over the wire waiting for the write to return.
     */
    BLOCKING
  }

  /**
   * Command Direction LEFT or RIGHT, modifier for the type of command to use, specifically in lists
   */
  public enum Direction {
    LEFT,
    RIGHT
  }

  final public static int NO_EXPIRE = -1;
  JedisPool pool;
  Map<JedisPubSub, RedisPubSubThread> subscriptions;
  ExecutorService executor;

  public RedisMgr() {
    executor = Executors.newSingleThreadExecutor();

    String redisUrl = ConfigMgr.getAppParameter(CoreConstants.REDIS_URL);
    String[] tokens = redisUrl.split(":");
    if (redisUrl.startsWith("redis://") && tokens.length == 4 && tokens[2].contains("@")) {
      //extract host, port and password from the url
      redisUrl = redisUrl.replace("redis://","");
      // redis://rediscloud:Kwre1Z9eLYJff8qP@pub-redis-11618.us-east-1-3.7.ec2.redislabs.com:11618
      String hostname = tokens[2].substring(tokens[2].indexOf("@") + 1);
      String port = tokens[3];
      String password = tokens[2].substring(0,tokens[2].indexOf("@"));
      int timeout = 30000; //30 secs

      pool = new JedisPool(new JedisPoolConfig(), hostname,Integer.parseInt(port), timeout, password, 0);
    } else {
      JedisPoolConfig jpc = new JedisPoolConfig();
      /*
      jpc.setMaxTotal(20);
      jpc.setMaxIdle(20);
      jpc.setMinIdle(15);
      */
      pool = new JedisPool(new JedisPoolConfig(), ConfigMgr.getAppParameter(CoreConstants.REDIS_URL));
    }
    subscriptions = new HashMap<>();
  }

  /**
   * Dangerous method - wipes everything
   * @return
   */
  public boolean flushAll() {
    try (Jedis jedis = pool.getResource()) {
      jedis.flushAll();
      return true;
    }
    catch (Exception e) {
      Log.err("RedisMgr: FLUSHALL failed", e);
      return false;
    }
  }


  /**
   * Binary Blocking (synced) version of the Set operation
   */
  public boolean setBlocking(final String key, final Object value, final int expire) {
    try(ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = new ObjectOutputStream(bos);
        Jedis jedis = pool.getResource()) {

      out.writeObject(value);
      if(expire == NO_EXPIRE) {
        jedis.set(SafeEncoder.encode(key), bos.toByteArray());
      } else {
        jedis.setex(SafeEncoder.encode(key), expire, bos.toByteArray());
      }
      return true;
    }catch (IOException ioe){
      Log.err("RedisMgr: Setting " + key + " failed", ioe);
    }
    return false;
  }

  /**
   * Binary default "safe" async Set operation
   * @param key
   * @param value
   * @return
   */
  public boolean set(final String key, final Object value) {
    try(ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = new ObjectOutputStream(bos)) {
      out.writeObject(value);
      final byte[] dataToSend = bos.toByteArray();

      Future<Boolean> future = executor.submit(() -> {
        /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
        try (Jedis jedis = pool.getResource()){
          jedis.set(SafeEncoder.encode(key), dataToSend);
          return true;
        }
        catch (Exception e) {
          Log.err("RedisMgr: Setting " + key + " failed", e);
          return false;
        }
      });
      return true;
    } catch (IOException ioe) {
      Log.err("RedisMgr: Object serialization IO error", ioe);
    }
    return true;
  }


  /**
   * Unsafe, turbo-fast async Set operation on immutable objects, without expiration
   * @param key
   * @param value
   * @return
   */
  public Future<Boolean> setUnsafe(final String key, final Object value) {
    return setUnsafe(key, value, NO_EXPIRE);
  }


    /**
     * Unsafe, turbo-fast async Set operation on immutable objects
     * @param key
     * @param value
     * @param expire expire (delete) value after this many seconds
     * @return
     */
  public Future<Boolean> setUnsafe(final String key, final Object value, final int expire) {
    Future<Boolean> future = executor.submit(() -> {
      try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
           ObjectOutput out = new ObjectOutputStream(bos);
           /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
           Jedis jedis = pool.getResource()) {
        out.writeObject(value);
        if(expire == NO_EXPIRE) {
          jedis.set(SafeEncoder.encode(key), bos.toByteArray());
        } else {
          jedis.setex(SafeEncoder.encode(key), expire, bos.toByteArray());
        }
        return true;
      }
      catch (Exception e) {
        Log.err("RedisMgr: Setting " + key + " failed", e);
        return false;
      }
    });
    return future;
  }

  public Object getBin(final String key) {
    /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
    try (Jedis jedis = pool.getResource()) {
      byte[] a = jedis.get(key.getBytes(StandardCharsets.US_ASCII));
      if(a == null) {
        return null;
      }
      try (ByteArrayInputStream bis = new ByteArrayInputStream(a);
           ObjectInput in = new ObjectInputStream(bis)) {
        return in.readObject();
      }
    }
    catch (Exception e) {
      Log.err("RedisMgr: Getting " + key + " failed", e);
      return null;
    }
  }

  public Jedis subscribe(JedisPubSub pubSub, String... channels) {
    try{
      Jedis jedis = pool.getResource();
      RedisPubSubThread thread = new RedisPubSubThread(jedis, pubSub, channels);
      thread.start();
      subscriptions.put(pubSub, thread);
      return jedis;
    } catch (Exception e) {
      Log.err("RedisMgr: Failed to subscribe to pubsub channels", e);
      return null;
    }
  }

  public void unsubscribe(JedisPubSub pubSub) {
    Log.info("RedisMgr - unsubscribing");
    RedisPubSubThread thread = subscriptions.remove(pubSub);
    if(thread == null) {
      Log.err("RedisMgr: Trying to unsubscribe from non-existed connection");
      return;
    }
    thread.unsubscribe();
  }

  /**
   * Publish String message on PubSub channel
   */
  public boolean publish(String channel, String message) {
    try (Jedis jedis = pool.getResource()) {
      jedis.publish(channel, message);
      return true;
    }
    catch (Exception e) {
      Log.err("RedisMgr: Publishing on " + channel + " failed", e);
      return false;
    }
  }

  /**
   * Publish String message on PubSub channel
   */
  public boolean publish(String channel, Object message) {
    throw new UnsupportedOperationException("Binary publishing currently not supported");
    /*
    try (Jedis jedis = pool.getResource()) {
      jedis.publish(SafeEncoder.encode(channel), SerializationUtils.serialize((Serializable) message));
      return true;
    }
    catch (Exception e) {
      Log.err("RedisMgr: Publishing on " + channel + " failed", e);
      return false;
    }
    */
  }


  public  boolean set(String key, String value) {
    /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
    try (Jedis jedis = pool.getResource()) {
      jedis.set(key, value);
      return true;
    }
    catch (Exception e) {
      Log.err("RedisMgr: Setting " + key + " failed", e);
      return false;
    }
  }

  public Object get(String key) {
    /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
    try (Jedis jedis = pool.getResource()) {
      return jedis.get(key);
    }
    catch (Exception e) {
      Log.err("RedisMgr: Getting " + key + " failed", e);
      return null;
    }
  }

  //append the object to the tail of the list. If there is no list for this key a list will be created
  public boolean appendToList(String key, Object value) {
    /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
    try (Jedis jedis = pool.getResource()) {
      jedis.rpush(SafeEncoder.encode(key), SerializationUtils.serialize((Serializable) value));
      return true;
    }
    catch (Exception e) {
      Log.err("RedisMgr: Append " + key + " failed", e);
      return false;
    }
  }

  //append the string to the tail of the list. If there is no list for this key a list will be created
  public boolean appendToList(String key, String value) {
    /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
    try (Jedis jedis = pool.getResource()) {
      jedis.rpush(key, value);
      return true;
    }
    catch (Exception e) {
      Log.err("RedisMgr: Append " + key + " failed", e);
      return false;
    }
  }

  //get all in a list
  public List getBList(String key) {
    /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
    List results = new ArrayList<>();
    try (Jedis jedis = pool.getResource()) {
      List<byte[]> a =  jedis.lrange(SafeEncoder.encode(key), 0, -1);
      if (a != null && a.size() > 0) {
        for (byte[] b : a) {
          ByteArrayInputStream bis = new ByteArrayInputStream(b);
          ObjectInput in = new ObjectInputStream(bis);

          results.add(in.readObject());
        }
      }
    }
    catch (Exception e) {
      Log.err("RedisMgr: Get List " + key + " failed", e);

    }
    return results;
  }

  //get all in a list
  public List<String> getList(String key) {
    /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
    try (Jedis jedis = pool.getResource()) {
      return  jedis.lrange(key, 0, -1);

    }
    catch (Exception e) {
      Log.err("RedisMgr: Get List " + key + " failed", e);

    }
    return null;
  }

  //get all in a list
  public boolean clearList(String key) {
    /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
    try (Jedis jedis = pool.getResource()) {
      jedis.ltrim(SafeEncoder.encode(key),1,0);
      return true;
    }
    catch (Exception e) {
      Log.err("RedisMgr: Get List " + key + " failed", e);

    }
    return false;
  }

  //add a string to a set - the set enforces uniqueness
  public boolean addToSet(String key, String value) {
    /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
    try (Jedis jedis = pool.getResource()) {
      jedis.sadd(key, value);
      return true;
    }
    catch (Exception e) {
      Log.err("RedisMgr: Add Key " + key + " failed", e);
      return false;
    }
  }

  //remove a string from a set
  public boolean removeFromSet(String key, String value) {
    /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
    try (Jedis jedis = pool.getResource()) {
      jedis.srem(key, value);
      return true;
    }
    catch (Exception e) {
      Log.err("RedisMgr: Remove value " + value + " failed", e);
      return false;
    }
  }


  //get all members of a set
  public Set<String> getSet(String key) {
    /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
    try (Jedis jedis = pool.getResource()) {
      return jedis.smembers(key);
    }
    catch (Exception e) {
      Log.err("RedisMgr: Getting " + key + " failed", e);
      return null;
    }
  }

  /**
   * Blocking key delete
   * @param key
   * @return
   */
  public boolean delKey(String key) {
    /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
    try (Jedis jedis = pool.getResource()) {
      jedis.del(key);
      return true;
    }
    catch (Exception e) {
      Log.err("RedisMgr: Getting " + key + " failed", e);
      return false;
    }
  }

  //********************************************************************************************************************
  // Key/Value Operations
  //********************************************************************************************************************

  /**
   * SETs Key/Value pair. If Key has Expiry Timeout associated, expiry will be set.
   *
   * @param keyPrefix
   * @param key
   * @param value
   * @param mode
   * @return
   */
  public Future<Boolean> set(final RedisKeys keyPrefix, final String key, final String value, final WriteMode mode) {
    final String exactKey = keyPrefix.getKey(key);
    switch (mode) {
      //Unsafe and fast are the same as there is no explicit de-serialization into byte array
      case UNSAFE:
      case FAST:
        return executor.submit(() -> {
          /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
          try (Jedis jedis = pool.getResource()) {
            if (keyPrefix.getExpire() > 0) {
              jedis.setex(exactKey, keyPrefix.getExpire(), value); //Updating expiry for the main key
            }
            else {
              jedis.set(exactKey, value);
            }
            return true;
          }
          catch (Exception e) {
            Log.err("RedisMgr: SET failed. Key: " + exactKey, e);
            return false;
          }
        });
      case BLOCKING:
      default: //Added default to satisfy compiler not to complain about main function not returning a value
        CompletableFuture<Boolean> result = new CompletableFuture<>();
        /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
        try (Jedis jedis = pool.getResource()) {
          if (keyPrefix.getExpire() > 0) {
            jedis.setex(exactKey, keyPrefix.getExpire(), value); //Updating expiry for the main key
          }
          else {
            jedis.set(exactKey, value);
          }
          result.complete(true);
          return result;
        }
        catch (Exception e) {
          Log.err("RedisMgr: SET failed. Key: " + exactKey, e);
          result.complete(false);
          return result;
        }
    }
  }

  /**
   * SETs Key/Value pair for binary Value. If Key has Expiry Timeout associated, expiry will be set.
   *
   * @param keyPrefix
   * @param key
   * @param value
   * @param mode
   * @return
   */
  public Future<Boolean> set(final RedisKeys keyPrefix, final String key, final Object value, final WriteMode mode) {
    final String exactKey = keyPrefix.getKey(key);
    switch (mode) {
      //Unsafe and fast are the same as there is no explicit de-serialization into byte array
      case UNSAFE:
        return executor.submit(() -> {
          try (ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutput out = new ObjectOutputStream(bos);
               /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
               Jedis jedis = pool.getResource()) {
            out.writeObject(value);
            if (keyPrefix.getExpire() > 0) {
              jedis.setex(SafeEncoder.encode(exactKey), keyPrefix.getExpire(), bos.toByteArray());
            }
            else {
              jedis.set(SafeEncoder.encode(exactKey), bos.toByteArray());
            }
            return true;
          }
          catch (Exception e) {
            Log.err("RedisMgr: SET failed. Key: " + exactKey, e);
            return false;
          }
        });
      case FAST:
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutput out = new ObjectOutputStream(bos)) {
          out.writeObject(value);
          final byte[] dataToSend = bos.toByteArray();
          return executor.submit(() -> {
            /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
            try (Jedis jedis = pool.getResource()) {
              if (keyPrefix.getExpire() > 0) {
                jedis.setex(SafeEncoder.encode(exactKey), keyPrefix.getExpire(), dataToSend);
              }
              else {
                jedis.set(SafeEncoder.encode(exactKey), bos.toByteArray());
              }
              return true;
            }
            catch (Exception e) {
              Log.err("RedisMgr: SET failed. Key: " + exactKey, e);
              return false;
            }
          });
        }
        catch (IOException ioe) {
          Log.err("RedisMgr: SET Object serialization IO error. Key: " + exactKey, ioe);
          CompletableFuture<Boolean> result = new CompletableFuture<>();
          result.complete(Boolean.FALSE);
          return result;
        }
      case BLOCKING:
      default: //Added default to satisfy compiler not to complain about main function not returning a value
        CompletableFuture<Boolean> result = new CompletableFuture<>();
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos);
             Jedis jedis = pool.getResource()) {
          out.writeObject(value);
          if (keyPrefix.getExpire() > 0) {
            jedis.setex(SafeEncoder.encode(exactKey), keyPrefix.getExpire(), bos.toByteArray());
          }
          else {
            jedis.set(SafeEncoder.encode(exactKey), bos.toByteArray());
          }
          result.complete(Boolean.TRUE);
          return result;
        }
        catch (Exception e) {
          Log.err("RedisMgr: SET failed. Key: " + exactKey, e);
          result.complete(Boolean.FALSE);
          return result;
        }
    }
  }

  /**
   * @param keyPrefix
   * @param key (Optional) Sub Key
   * @param valueType
   * @param <T>
   * @return
   */
  public <T> Optional<T> getBin(final RedisKeys keyPrefix, final String key, Class<T> valueType) {
    final String exactKey = keyPrefix.getKey(key);
    try (Jedis jedis = pool.getResource()) {
      byte[] a = jedis.get(SafeEncoder.encode(exactKey));
      if (a == null || a.length == 0) {
        return Optional.ofNullable(null);
      }
      T val;
      try (ByteArrayInputStream bis = new ByteArrayInputStream(a); ObjectInput in = new ObjectInputStream(bis)) {
        val = (T) in.readObject();
      }
      return Optional.ofNullable(val);
    }
    catch (Exception e) {
      Log.err("RedisMgr: GET Failed. Key: " + exactKey, e);
      return ofNullable(null);
    }
  }

  /**
   * Retrieves (GET) Textual value for a Key.
   *
   * @param keyPrefix
   * @param key (Optional) sub key
   * @return
   */
  public Optional<String> get(final RedisKeys keyPrefix, final String key) {
    final String exactKey = keyPrefix.getKey(key);
    try (Jedis jedis = pool.getResource()) {
      return ofNullable(jedis.get(exactKey));
    }
    catch (Exception e) {
      Log.err("RedisMgr: GET Failed. Key: " + exactKey, e);
      return Optional.empty();
    }
  }

  //********************************************************************************************************************
  // HASH Operations                                                                                                   *
  //********************************************************************************************************************

  /**
   * Adds/sets Set key/value for the specified key
   * @param keyPrefix namespace/prefix for the key, prefix and key will be delimited with semicolon (Redis convention)
   * @param key (optional) key
   * @param field Set's key
   * @param value Set's value
   * @param mode Write mode to use for this set operation
   */
  public Future<Boolean> hashSet(final RedisKeys keyPrefix, final String key, final String field,
                      final String value, final WriteMode mode) {
    final String exactKey = keyPrefix.getKey(key);
    switch (mode){
      //Unsafe and fast are the same as there is no explicit de-serialization into byte array
      case UNSAFE:
      case FAST:
        return executor.submit(() -> {
          /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
          try (Jedis jedis = pool.getResource()){
            jedis.hset(exactKey, field, value);
            if(keyPrefix.getExpire() > 0) {
              jedis.expire(exactKey, keyPrefix.getExpire()); //Updating expiry for the main key
            }
            return true;
          }
          catch (Exception e) {
            Log.err("RedisMgr: HashSet failed. Key: " + exactKey + " field: " + field, e);
            return false;
          }
        });
      case BLOCKING:
      default: //Added default to satisfy compiler not to complain about main function not returning a value
        CompletableFuture<Boolean> result = new CompletableFuture<>();
        /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
        try (Jedis jedis = pool.getResource()) {
          jedis.hset(exactKey, field, value);
          if(keyPrefix.getExpire() > 0) {
            jedis.expire(exactKey, keyPrefix.getExpire()); //Updating expiry for the main key
          }
          result.complete(true);
          return result;
        }
        catch (Exception e) {
          Log.err("RedisMgr: Setting " + exactKey + " failed", e);
          result.complete(false);
          return result;
        }
    }
  }

  /**
   * Adds/sets Set key/value for the specified key
   * @param keyPrefix namespace/prefix for the key, prefix and key will be delimited with semicolon (Redis convention)
   * @param key (optional) key
   * @param field Set's key
   * @param value Set's value
   * @param mode Write mode to use for this set operation
   */
  public Future<Boolean> hashSet(final RedisKeys keyPrefix, final String key, final String field,
                                 final Object value, final WriteMode mode) {
    final String exactKey = keyPrefix.getKey(key);
    switch (mode){
      //Unsafe and fast are the same as there is no explicit de-serialization into byte array
      case UNSAFE:
        return executor.submit(() -> {
          try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
               ObjectOutput out = new ObjectOutputStream(bos);
               /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
               Jedis jedis = pool.getResource()) {
            out.writeObject(value);
            jedis.hset(SafeEncoder.encode(exactKey), SafeEncoder.encode(field), bos.toByteArray());
            if(keyPrefix.getExpire() > 0) {
              jedis.expire(exactKey, keyPrefix.getExpire()); //Updating expiry for the main key
            }
            return true;
          }
          catch (Exception e) {
            Log.err("RedisMgr: HashSet failed. Key: " + exactKey + " field: " + field, e);
            return false;
          }
        });
      case FAST:
        try(ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos)) {
          out.writeObject(value);
          final byte[] dataToSend = bos.toByteArray();
          return executor.submit(() -> {
            /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
            try (Jedis jedis = pool.getResource()){
              jedis.hset(SafeEncoder.encode(exactKey), SafeEncoder.encode(field), dataToSend);
              if(keyPrefix.getExpire() > 0) {
                jedis.expire(exactKey, keyPrefix.getExpire()); //Updating expiry for the main key
              }
              return true;
            }
            catch (Exception e) {
              Log.err("RedisMgr: HashSet failed. Key: " + exactKey + " field: " + field, e);
              return false;
            }
          });
        } catch (IOException ioe) {
          Log.err("RedisMgr: HashSet Object serialization IO error", ioe);
          CompletableFuture<Boolean> result = new CompletableFuture<>();
          result.complete(Boolean.FALSE);
          return result;
        }
      case BLOCKING:
      default: //Added default to satisfy compiler not to complain about main function not returning a value
        CompletableFuture<Boolean> result = new CompletableFuture<>();
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos);
             Jedis jedis = pool.getResource()) {
          out.writeObject(value);
          jedis.hset(SafeEncoder.encode(exactKey), SafeEncoder.encode(field), bos.toByteArray());
          if(keyPrefix.getExpire() > 0) {
            jedis.expire(exactKey, keyPrefix.getExpire()); //Updating expiry for the main key
          }
          result.complete(Boolean.TRUE);
          return result;
        }
        catch (Exception e) {
          Log.err("RedisMgr: HashSet failed. Key: " + exactKey + " field: " + field, e);
          result.complete(Boolean.FALSE);
          return result;
        }
    }
  }

  public <T> Optional<T> hashGetBin(final RedisKeys keyPrefix, final String key, final String field, Class<T> valueType) {
    final String exactKey = keyPrefix.getKey(key);
    try (Jedis jedis = pool.getResource()) {
      byte[] a = jedis.hget(SafeEncoder.encode(exactKey), SafeEncoder.encode(field));
      if(a == null || a.length == 0) {
        return Optional.empty();
      }
      T val;
      try (ByteArrayInputStream bis = new ByteArrayInputStream(a);
           ObjectInput in = new ObjectInputStream(bis)) {
        val = (T) in.readObject();
      }
      return Optional.ofNullable(val);
    }
    catch (Exception e) {
      Log.err("RedisMgr: HGET Failed. Key: " + exactKey + " Field:" + field, e);
      return ofNullable(null);
    }
  }


  /**
   * Returns all elements of the map, assumes that all objects are of the same common type
   * @param keyPrefix
   * @param key (optional) subkey
   * @param valueType common type of all objects, objects not of this type will be dropped out
   * @param <T>
   * @return
   */
  public <T> Optional<Map<String, T>> hashGetAll(final RedisKeys keyPrefix, final String key, Class<T> valueType) {
    final String exactKey = keyPrefix.getKey(key);

    try (Jedis jedis = pool.getResource()) {
      Map<byte[], byte[]> a = jedis.hgetAll(SafeEncoder.encode(exactKey));
      if (a == null || a.size() == 0) {
        return Optional.ofNullable(null);
      }

      Map<String, T> result = new HashMap<>();

      boolean isString = valueType.getClass().equals(String.class);
      a.forEach((keyBytes, valBytes) -> {
        String keyStr = SafeEncoder.encode(keyBytes);
        T      val;
        if(isString) {
          val = (T) SafeEncoder.encode(valBytes);
        } else {
          try (ByteArrayInputStream bis = new ByteArrayInputStream(valBytes); ObjectInput in = new ObjectInputStream(bis)) {
            val = (T) in.readObject();
            result.put(keyStr, val);
          }
          catch (IOException | ClassNotFoundException e) {
          } //Ignoring to be quiet if objects changed
        }
      });
      return Optional.of(result);
    }
    catch (Exception e) {
      Log.err("RedisMgr: HGETALL Failed. Key: " + exactKey, e);
      return ofNullable(null);
    }
  }


  /**
   * Deletes one or keys
   * @param keyPrefix
   * @param key
   * @param field
   * @return never null
   */
  public Optional<String> hashGet(final RedisKeys keyPrefix, final String key, final String field) {
    final String exactKey = keyPrefix.getKey(key);
    try (Jedis jedis = pool.getResource()) {
      return ofNullable(jedis.hget(exactKey, field));
    }
    catch (Exception e) {
      Log.err("RedisMgr: HGET Failed. Key: " + exactKey + " Field:" + field, e);
      return Optional.empty();
    }
  }

  /**
   * Deletes one specific field from the set
   * @param keyPrefix
   * @param key
   * @param fields
   * @param mode
   * @return future whether or not operation completed
   */
  public Future<Boolean> hashDel(final RedisKeys keyPrefix, final String key, final WriteMode mode,
                                 String... fields) {

    final String exactKey = keyPrefix.getKey(key);
    switch (mode){
      case UNSAFE:
      case FAST:
        return executor.submit(() -> {
          try (Jedis jedis = pool.getResource()) {
            jedis.hdel(exactKey, fields);
            return true;
          }
          catch (Exception e) {
            Log.err("RedisMgr: HDEL failed. Key: " + exactKey + " field: " + fields, e);
            return false;
          }
        });
      case BLOCKING:
      default:
        CompletableFuture<Boolean> result = new CompletableFuture<>();
        try (Jedis jedis = pool.getResource()) {
          jedis.hdel(exactKey, fields);
          result.complete(Boolean.TRUE);
        }
        catch (Exception e) {
          Log.err("RedisMgr: HDEL Failed. Key: " + exactKey + " Field:" + fields, e);
          result.complete(Boolean.FALSE);
        }
        return result;
    }
  }


  //********************************************************************************************************************
  // List Operations
  //********************************************************************************************************************

  /**
   * Pushes item to the list from the specified side.
   * Common function which will automatically detect if value is a string and use optimized call for it.
   * @param keyPrefix namespace/prefix for the key, prefix and key will be delimited with semicolon (Redis convention)
   * @param key (optional) key
   * @param value Item value
   * @param wMode Write mode to use for this set operation
   * @param dir direction from which to append items: LEFT (head) or RIGHT (tail)
   */
  public Future<Boolean> listPush(final RedisKeys keyPrefix, final String key,
                                  final Object value,  final Direction dir, final WriteMode wMode) {

    final String exactKey = keyPrefix.getKey(key);

    BiConsumer<Jedis, byte[]> pusher = (jedis, byteValue) -> {
      boolean isString = value.getClass().equals(String.class);
      switch (dir) {
        case LEFT:
          if (isString) {
            jedis.lpush(exactKey, (String) value);
          }
          else {
            jedis.lpush(SafeEncoder.encode(exactKey), byteValue);
          }
          break;
        case RIGHT:
          if(isString) {
            jedis.lpush(exactKey, (String) value);
          } else {
            jedis.rpush(SafeEncoder.encode(exactKey), byteValue);
          }
          break;
      }
      if(keyPrefix.getExpire() > 0) {
        jedis.expire(exactKey, keyPrefix.getExpire()); //Updating expiry for the main key
      }
    };

    switch (wMode){
      //Unsafe and fast are the same as there is no explicit de-serialization into byte array
      case UNSAFE:
        return executor.submit(() -> {
          try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
               ObjectOutput out = new ObjectOutputStream(bos);
               Jedis jedis = pool.getResource()) {
            out.writeObject(value);
            pusher.accept(jedis, bos.toByteArray());
            return true;
          }
          catch (Exception e) {
            Log.err("RedisMgr: List Push failed. Key: " + exactKey + " dir: " + dir, e);
            return false;
          }
        });
      case FAST:
        try(ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos)) {
          out.writeObject(value);
          final byte[] dataToSend = bos.toByteArray();
          return executor.submit(() -> {
            try (Jedis jedis = pool.getResource()){
              pusher.accept(jedis, dataToSend);
              return true;
            }
            catch (Exception e) {
              Log.err("RedisMgr: List Push failed. Key: " + exactKey + " dir: " + dir, e);
              return false;
            }
          });
        } catch (IOException ioe) {
          Log.err("RedisMgr: List Push Object serialization IO error", ioe);
          CompletableFuture<Boolean> result = new CompletableFuture<>();
          result.complete(Boolean.FALSE);
          return result;
        }
      case BLOCKING:
      default: //Added default to satisfy compiler not to complain about main function not returning a value
        CompletableFuture<Boolean> result = new CompletableFuture<>();
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos);
             Jedis jedis = pool.getResource()) {
          out.writeObject(value);
          pusher.accept(jedis, bos.toByteArray());
          result.complete(Boolean.TRUE);
          return result;
        }
        catch (Exception e) {
          Log.err("RedisMgr: List Push failed. Key: " + exactKey + " dir: " + dir, e);
          result.complete(Boolean.FALSE);
          return result;
        }
    }
  }

  /**
   * Pops item from the list from the specified end
   * @param keyPrefix namespace/prefix for the key, prefix and key will be delimited with semicolon (Redis convention)
   * @param key (optional) key
   * @param dir which side to pop element from LEFT(head) or RIGHT(tail)
   * @param valueType Type of elements in the list and the type will be used for return value
   * @param <T>
   * @return
   */
  public <T> Optional<T> listPop(final RedisKeys keyPrefix, final String key, final Direction dir, Class<T> valueType) {
    final String exactKey = keyPrefix.getKey(key);
    try (Jedis jedis = pool.getResource()) {
      boolean isString = (valueType.equals(String.class));
      if(isString) {
        String s;
        switch (dir) {
          default:
          case LEFT:
            s = jedis.lpop(exactKey);
            break;
          case RIGHT:
            s = jedis.rpop(exactKey);
            break;
        }
        return Optional.ofNullable((T) s);
      } else {
        byte[] a;
        switch (dir) {
          default:
          case LEFT:
            a = jedis.lpop(SafeEncoder.encode(exactKey));
            break;
          case RIGHT:
            a = jedis.rpop(SafeEncoder.encode(exactKey));
            break;
        }
        if(a == null || a.length == 0) {
          return Optional.ofNullable(null);
        }
        T val;
        try (ByteArrayInputStream bis = new ByteArrayInputStream(a);
             ObjectInput in = new ObjectInputStream(bis)) {
          val = (T) in.readObject();
        }
        return Optional.ofNullable(val);
      }
    }
    catch (Exception e) {
      Log.err("RedisMgr: List GET Failed. Key: " + exactKey + " dir: " + dir, e);
      return ofNullable(null);
    }
  }

  /**
   * List range operation. Wraps LRANGE Redis command.
   * http://redis.io/commands/lrange
   * @param keyPrefix namespace/prefix for the key, prefix and key will be delimited with semicolon (Redis convention)
   * @param key (optional) key
   * @param fromIdx   start index of the range
   * @param toIdx     stop index of the range (can be negative)
   * @param valueType Type of elements in the list
   * @param <T>
   * @return
   */
  public <T> Optional<List<T>> listRange(final RedisKeys keyPrefix, final String key,
                                         final long fromIdx, final long toIdx, Class<T> valueType) {
    final String exactKey = keyPrefix.getKey(key);
    try (Jedis jedis = pool.getResource()) {

      boolean isString = (valueType.equals(String.class));
      if(isString) {
        List<String> r = jedis.lrange(exactKey, fromIdx, toIdx);
        return Optional.ofNullable((List<T>) r);
      } else {
        List<byte[]> r = jedis.lrange(SafeEncoder.encode(exactKey), fromIdx, toIdx);
        List<T> output = new ArrayList<>(r.size());
        for(byte[] i: r) {
          T val;
          try (ByteArrayInputStream bis = new ByteArrayInputStream(i);
               ObjectInput in = new ObjectInputStream(bis)) {
            val = (T) in.readObject();
            output.add(val);
          }
        }
        return (output.size() == 0)?Optional.empty():Optional.of(output);
      }
    }
    catch (Exception e) {
      Log.err("RedisMgr: List RANGE Failed. Key: " + exactKey + " fromIdx: " + fromIdx + " toIdx:" + toIdx, e);
      return ofNullable(null);
    }
  }

  //********************************************************************************************************************
  // Other Operations                                                                                                  *
  //********************************************************************************************************************

  public Future<Boolean> expire(final RedisKeys keyPrefix, final String key) {
    return executor.submit(() -> {
      if(!keyPrefix.isField()) {
        Log.err("RedisMgr: Failed attempt to set expiry on a 'field'. Field prefix: " + keyPrefix);
        return false;
      }
      final String exactKey = keyPrefix.getKey(key);
      try (Jedis jedis = pool.getResource()){
        return jedis.expire(exactKey, keyPrefix.getExpire()) == 1;
      }
      catch (Exception e) {
        Log.err("RedisMgr: Expire failed. Key: " + exactKey + " seconds: " + keyPrefix.getExpire(), e);
        return false;
      }
    });
  }


  /**
   * Blocking key delete
   * @param key
   * @return
   */
  public Future<Boolean> delete(final RedisKeys keyPrefix, final String key, final WriteMode mode) {
    final String exactKey = keyPrefix.getKey(key);
    switch (mode){
      case UNSAFE:
      case FAST:
        return executor.submit(() -> {
          try (Jedis jedis = pool.getResource()) {
            jedis.del(exactKey);
            return true;
          }
          catch (Exception e) {
            Log.err("RedisMgr: DEL failed. Key: " + exactKey , e);
            return false;
          }
        });
      case BLOCKING:
      default:
        CompletableFuture<Boolean> result = new CompletableFuture<>();
        try (Jedis jedis = pool.getResource()) {
          jedis.del(exactKey);
          result.complete(Boolean.TRUE);
        }
        catch (Exception e) {
          Log.err("RedisMgr: DEL Failed. Key: " + exactKey, e);
          result.complete(Boolean.FALSE);
        }
        return result;
    }
  }
}
