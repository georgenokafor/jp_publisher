/**
 * The Globus family of brands WebAPI allows agencies and partners to utilize the same information we do when building their Web sites.  It is built in .Net SOAP Web Services, and can be called from any programming language that supports SOAP with calls available in XML and data sets.
 * 	<br><br>
 * 	Below are the Web calls you will need to directly access the tour/vacation content,
 * 	including dates, prices and itineraries. For explanations and questions, please
 * 	read the <a href="http://www.globusfamily.com/webapi">WebAPI Information.</a>
 * <br><br>
 * 	For questions or technical support, please e-mail <a href="mailto:webtechsupport@globusfamily.com">webtechsupport@globusfamily.com</a>.
 * <br><br>
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://webapi.globusandcosmos.com/", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.mapped.publisher.parse.globus;
