package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.google.common.collect.ImmutableMap;
import com.mapped.publisher.parse.virtuoso.cruise.CruiseDownload;
import com.mapped.publisher.utils.Log;
import org.apache.commons.lang3.time.StopWatch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by twong on 15-05-19.
 */
public class VirtuosoCruisesActor
    extends UntypedActor {

  public interface Factory {
    public Actor create();
  }

  /**
   * Akka Actor Properties
   */
  public final static Props props = Props.create(VirtuosoCruisesActor.class);

  private static String DOWNLOAD_URL = "https://www.virtuoso.com/search/ajax/getsearchview";
  private static int MAX_ROWS_PER_PAGE = 200;
  private static int NUM_ACTORS = 5;

  public static class Command
      implements Serializable {

    private final String startDate;
    private final String endDate;

    private final String term;

    public Command(String startDate, String endDate, String term) {
      this.startDate = startDate;
      this.endDate = endDate;
      this.term = term;
    }

    public String getStartDate() {
      return startDate;
    }

    public String getEndDate() {
      return endDate;
    }


    public String getTerm() {
      return term;
    }
  }

  public void onReceive(Object message)
      throws Exception {

    if (message instanceof Command) {
      Command cmd = (Command) message;
      StopWatch sw = new StopWatch();
      sw.start();

      Map<String, String> fullUrls = CruiseDownload.parseCruises(DOWNLOAD_URL, cmd.startDate, cmd.endDate, cmd.term, MAX_ROWS_PER_PAGE);

      sw.stop();
      Log.debug("VirtuosoCruisesActor - " + cmd.getStartDate() + " to " + cmd.getEndDate() +" - finished download in " + sw.getTime());

      if (fullUrls != null && fullUrls.size() > 0) {
        //distribute the ids so all the actors are put to work
        int tripPerActor = fullUrls.size() / NUM_ACTORS;
        ArrayList<String> ids = new ArrayList<>(fullUrls.keySet());
        if (tripPerActor < 1) {
          NUM_ACTORS = 1;
          tripPerActor = ids.size();
        }

        for (int i = 0; i < NUM_ACTORS; i++) {
          int startPos = (i * tripPerActor);
          int endPos = startPos + tripPerActor;
          if (i == NUM_ACTORS - 1) {
            //for the last actor, make sure we get everything
            endPos = ids.size();
          }

          HashMap<String, String> process = new HashMap<>();
          for (int j = startPos; j < endPos; j++) {
            String key = ids.get(j);
            process.put(key, fullUrls.get(key));
          }

          if (process.size() > 0) {
            //send to the actor
            final ImmutableMap<String, String> msg = ImmutableMap.copyOf(process);
            ActorsHelper.tell(SupervisorActor.UmappedActor.VIRTUOSO_CRUISE, msg);
          }
        }
      }
    }
  }
}
