package com.mapped.publisher.msg.flightstat.event;

/**
 * Created by twong on 2015-01-25.
 */
public class AirportResources {
  private String departureTerminal;
  private String departureGate;
  private String arrivalTerminal;
  private String arrivalGate;
  private String baggage;

  public String getDepartureTerminal() {
    return departureTerminal;
  }

  public void setDepartureTerminal(String departureTerminal) {
    this.departureTerminal = departureTerminal;
  }

  public String getDepartureGate() {
    return departureGate;
  }

  public void setDepartureGate(String departureGate) {
    this.departureGate = departureGate;
  }

  public String getArrivalTerminal() {
    return arrivalTerminal;
  }

  public void setArrivalTerminal(String arrivalTerminal) {
    this.arrivalTerminal = arrivalTerminal;
  }

  public String getArrivalGate() {
    return arrivalGate;
  }

  public void setArrivalGate(String arrivalGate) {
    this.arrivalGate = arrivalGate;
  }

  public String getBaggage() {
    return baggage;
  }

  public void setBaggage(String baggage) {
    this.baggage = baggage;
  }
}
