
package com.umapped.external.wetu.itinerary.details;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "day",
    "room_basis",
    "drinks_basis",
    "notes",
    "consultant_notes",
    "included",
    "excluded",
    "routes",
    "activities",
    "day_tours"
})
public class Day {

    @JsonProperty("day")
    private int day;
    @JsonProperty("room_basis")
    private String roomBasis;
    @JsonProperty("drinks_basis")
    private String drinksBasis;
    @JsonProperty("notes")
    private String notes;
    @JsonProperty("consultant_notes")
    private String consultantNotes;
    @JsonProperty("included")
    private String included;
    @JsonProperty("excluded")
    private String excluded;
    @JsonProperty("routes")
    private List<Route> routes = new ArrayList<Route>();
    @JsonProperty("activities")
    private List<Activity> activities = new ArrayList<Activity>();
    @JsonProperty("day_tours")
    private List<DayTour> dayTours = new ArrayList<DayTour>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The day
     */
    @JsonProperty("day")
    public int getDay() {
        return day;
    }

    /**
     * 
     * @param day
     *     The day
     */
    @JsonProperty("day")
    public void setDay(int day) {
        this.day = day;
    }

    /**
     * 
     * @return
     *     The roomBasis
     */
    @JsonProperty("room_basis")
    public String getRoomBasis() {
        return roomBasis;
    }

    /**
     * 
     * @param roomBasis
     *     The room_basis
     */
    @JsonProperty("room_basis")
    public void setRoomBasis(String roomBasis) {
        this.roomBasis = roomBasis;
    }

    /**
     * 
     * @return
     *     The drinksBasis
     */
    @JsonProperty("drinks_basis")
    public String getDrinksBasis() {
        return drinksBasis;
    }

    /**
     * 
     * @param drinksBasis
     *     The drinks_basis
     */
    @JsonProperty("drinks_basis")
    public void setDrinksBasis(String drinksBasis) {
        this.drinksBasis = drinksBasis;
    }

    /**
     * 
     * @return
     *     The notes
     */
    @JsonProperty("notes")
    public String getNotes() {
        return notes;
    }

    /**
     * 
     * @param notes
     *     The notes
     */
    @JsonProperty("notes")
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * 
     * @return
     *     The consultantNotes
     */
    @JsonProperty("consultant_notes")
    public String getConsultantNotes() {
        return consultantNotes;
    }

    /**
     * 
     * @param consultantNotes
     *     The consultant_notes
     */
    @JsonProperty("consultant_notes")
    public void setConsultantNotes(String consultantNotes) {
        this.consultantNotes = consultantNotes;
    }

    /**
     * 
     * @return
     *     The included
     */
    @JsonProperty("included")
    public String getIncluded() {
        return included;
    }

    /**
     * 
     * @param included
     *     The included
     */
    @JsonProperty("included")
    public void setIncluded(String included) {
        this.included = included;
    }

    /**
     * 
     * @return
     *     The excluded
     */
    @JsonProperty("excluded")
    public String getExcluded() {
        return excluded;
    }

    /**
     * 
     * @param excluded
     *     The excluded
     */
    @JsonProperty("excluded")
    public void setExcluded(String excluded) {
        this.excluded = excluded;
    }

    /**
     * 
     * @return
     *     The activities
     */
    @JsonProperty("activities")
    public List<Activity> getActivities() {
        return activities;
    }

    /**
     * 
     * @param activities
     *     The activities
     */
    @JsonProperty("activities")
    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(day).append(roomBasis).append(drinksBasis).append(notes).append(consultantNotes).append(included).append(excluded).append(activities).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Day) == false) {
            return false;
        }
        Day rhs = ((Day) other);
        return new EqualsBuilder().append(day, rhs.day).append(roomBasis, rhs.roomBasis).append(drinksBasis, rhs.drinksBasis).append(notes, rhs.notes).append(consultantNotes, rhs.consultantNotes).append(included, rhs.included).append(excluded, rhs.excluded).append(activities, rhs.activities).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
