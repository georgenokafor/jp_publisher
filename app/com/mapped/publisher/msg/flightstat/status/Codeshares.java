
package com.mapped.publisher.msg.flightstat.status;

import java.util.List;

public class Codeshares{
   	private List<Codeshare> codeshare;

 	public List<Codeshare> getCodeshare(){
		return this.codeshare;
	}
	public void setCodeshare(List<Codeshare> codeshare){
		this.codeshare = codeshare;
	}
}
