package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by surge on 2016-06-28.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ServiceChannel
    extends Thing {
  /**
   * A language someone may use with the item.
   *
   * @note Simplified from Language type to Thing
   */
  Thing         availableLanguage;
  /**
   * Estimated processing time for the service using this channel.
   *
   * @note Simplified from Duration type to Thing
   */
  Thing         processingTime;
  /**
   * The service provided by this channel.
   */
  Service       providesService;
  /**
   * The location (e.g. civic structure, local business, etc.) where a person can go to access the service.
   */
  Place         serviceLocation;
  /**
   * The phone number to use to access the service.
   */
  ContactPoint  servicePhone;
  /**
   * The address for accessing the service by mail.
   */
  PostalAddress servicePostalAddress;
  /**
   * The number to access the service by text message.
   */
  ContactPoint  serviceSmsNumber;
  /**
   * URL	The website to access the service.
   */
  String        serviceUrl;

}
