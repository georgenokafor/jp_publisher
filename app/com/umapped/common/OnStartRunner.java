package com.umapped.common;

import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.persistence.CountriesInfo;
import com.mapped.publisher.persistence.FeedSourcesInfo;
import com.mapped.publisher.persistence.PoiTypeInfo;
import com.mapped.publisher.persistence.communicator.Communicator;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.umapped.helper.UmappedEmailFactory;
import models.publisher.*;
import org.apache.commons.lang3.time.StopWatch;
import play.Configuration;
import play.Logger;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.inject.Singleton;
import java.util.*;

/**
 * Created by surge on 2016-11-18.
 */
@Singleton
public class OnStartRunner {

  @Inject
  public OnStartRunner(Configuration configuration) {

    Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName("JPEG");
    while (readers.hasNext()) {
      System.out.println("reader: " + readers.next());
    }

    try {
      int instanceId = DBConnectionMgr.initClusterId();

        /*
   L E G A C Y     S U P P O R T  for Memcached for extenal monitoring of processes...
   Processes running out of Play keep their state in Memcached
   And the Application:healthcheck service is called externally to monitor the state of these processes
   This legacy support will be temporary until Memcached is replaced in all the external processes
   */
      //init cache to use our own cache implementation
      if ((configuration.getString("memcachedplugin") == null || configuration.getString("memcachedplugin")
                                                                              .contains("disabled")) &&
          (configuration.getString(
          "ehcacheplugin") == null || configuration.getString("ehcacheplugin").contains("disabled"))) {
        //iniialize our own cache
        String server   = ConfigMgr.getAppParameter("MEMCACHIER_SERVERS");
        String userName = ConfigMgr.getAppParameter("MEMCACHIER_USERNAME");
        String password = ConfigMgr.getAppParameter("MEMCACHIER_PASSWORD");

        if (!CacheMgr.init(server, userName, password)) {
          Log.debug("Catastrophic failure while initializing memcached");
          System.exit(1);
        }

        String cacheKey = String.valueOf(instanceId) + "_SYSTEM_TEST_REDIS";
        //CacheMgr.set(cacheKey, "ALL GOOD");
        //Log.log(LogLevel.INFO,"Test CacheMgr: " + CacheMgr.get(cacheKey));
        //CacheMgr.set(cacheKey, null);
        cacheKey = String.valueOf(instanceId) + "_SYSTEM_TEST_MEMCACHED";
        CacheMgr.setMemcached(cacheKey, "ALL GOOD");
        Log.info("Test CacheMgr: " + CacheMgr.getMemcached(cacheKey));
        CacheMgr.setMemcached(cacheKey, null);
      }

      TimeZone tz = TimeZone.getDefault();
      Logger.info("Java timezone before: " + tz.getDisplayName());

      System.setProperty("user.timezone", "UTC");
      TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

      tz = TimeZone.getDefault();
      Logger.info("Java timezone after : " + tz.getDisplayName());

      if (instanceId > 0) {
        Logger.info("Application has started - " + DBConnectionMgr.getUniqueId());
        //TO DO send email notification
      }
      else {
        Log.debug("Start Error - cannot get instance id ");
        System.exit(-1);
      }

      /* Loading all countries */
      try {
        StopWatch sw                 = new StopWatch();
        int       countryNameCounter = 0;
        sw.start();
        TreeMap<String, List<String>> names = new TreeMap<>();
        for (LstCountrySynonyms s : LstCountrySynonyms.getAllForLocale(CountriesInfo.LangCode.EN.name())) {
          List<String> cntryNames = names.get(s.getAlpha3());
          if (cntryNames == null) {
            cntryNames = new ArrayList<>(5);
            names.put(s.getAlpha3(), cntryNames);
          }
          cntryNames.add(Utils.curlyQuotesToAscii(s.getName()));
          countryNameCounter++;
        }

        CountriesInfo countriesInfo = CountriesInfo.Instance();
        for (LstCountry c : LstCountry.getAll()) {
          List<String> cntryNames = names.get(c.getAlpha3());
          if (cntryNames == null) {
            cntryNames = new ArrayList<>(1);
            names.put(c.getAlpha3(), cntryNames);
          }
          cntryNames.add(0, Utils.curlyQuotesToAscii(c.getName()));
          countryNameCounter++;
          countriesInfo.addCountry(c.getAlpha2(), c.getAlpha3(), cntryNames);
        }
        sw.stop();
        Log.info(countryNameCounter + " county names loaded in " + sw.getTime() + "ms");
        Log.debug(sw.toString());
      }
      catch (Exception e) {
        Log.debug("Catastrophic failure while loading countries");
        e.printStackTrace();
      }

      /* Loading all poi types */
      StopWatch sw = new StopWatch();
      sw.start();
      List<PoiType> types = PoiType.findAll();
      PoiTypeInfo   pti   = PoiTypeInfo.Instance();
      for (PoiType pt : types) {
        pti.addTypeInfo(pt.getTypeId(), pt.getName(), pt.getPriority(), pt.getState());
      }
      //Iterating again to add children and parents
      for (PoiType pt : types) {
        pti.byId(pt.getParentTypeId()).addChild(pti.byId(pt.getTypeId()));
        pti.byId(pt.getTypeId()).setParent(pti.byId(pt.getParentTypeId()));
      }

      sw.stop();
      Log.info(types.size() + " poi types loaded in " + sw.getTime() + "ms");


       /* Loading all poi sources */
      sw.reset();
      sw.start();
      List<FeedSrc>   srcs = FeedSrc.find.all();
      FeedSourcesInfo psi  = FeedSourcesInfo.Instance();
      for (FeedSrc ps : srcs) {
        psi.addSource(ps.getSrcId(), ps.getName());
      }
      sw.stop();
      Log.info(srcs.size() + " poi sources loaded in " + sw.getTime() + "ms");

      //Syncing Capabilities in the enum with database
      SysCapability.syncEnumToDb();

      Communicator.Instance(); //Kicking communicator initialization

      /* load any smtp white label */
      UmappedEmailFactory.load();
    }
    catch (Exception e) {
      Log.err("Start Error - ", e);
      e.printStackTrace();
      System.exit(-1);
    }
  }
}
