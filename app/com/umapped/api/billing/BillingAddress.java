package com.umapped.api.billing;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * Created by twong on 2016-10-18.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BillingAddress implements Serializable {
  public String fName;
  public String lName;

  public String company;
  public String streetAddress;
  public String extendedAddress;
  public String locality;
  public String region;
  public String postalCode;
  public String countryCodeAlpha2;
  public String country;
}
