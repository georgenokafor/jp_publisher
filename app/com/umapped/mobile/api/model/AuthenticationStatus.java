package com.umapped.mobile.api.model;

/**
 * Created by wei on 2017-07-05.
 */
public enum AuthenticationStatus {
  USER_NOT_FOUND,
  USER_PASSWORD_NOT_MATCH,
  USER_ROLE_INVALID,
  UNEXPECTED_ERROR,
  SUCCESSFUL
}
