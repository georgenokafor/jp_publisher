package com.mapped.publisher.parse.extractor.booking.valueObject;

import java.util.List;

/**
 * Created by wei on 2017-05-19.
 */
public class ReservationVO extends BaseVO {
  private String              taxes;
  private String              fees;
  private String              total;
  private String              currency;
  private String              important;
  private String              subtotal;
  private List<Rate>          rates;

  private String              cancellationPolicy;
  private String              serviceType;



  public ReservationVO() {
    super();
  }

  public ReservationVO(boolean commentAsContainer) {
    super(commentAsContainer);
  }

  public String getServiceType() {
    return serviceType;
  }

  public void setServiceType(String serviceType) {
    this.serviceType = serviceType;
  }

  public String getTaxes() {
    return taxes;
  }

  public ReservationVO setTaxes(String taxes) {
    this.taxes = taxes;
    return this;
  }

  public String getFees() {
    return fees;
  }

  public ReservationVO setFees(String fees) {
    this.fees = fees;
    return this;
  }

  public String getTotal() {
    return total;
  }

  public ReservationVO setTotal(String total) {
    this.total = total;
    return this;
  }

  public String getCurrency() {
    return currency;
  }

  public ReservationVO setCurrency(String currency) {
    this.currency = currency;
    return this;
  }

  public String getImportant() {
    return important;
  }

  public ReservationVO setImportant(String important) {
    this.important = important;
    return this;
  }

  public String getSubtotal() {
    return subtotal;
  }

  public ReservationVO setSubtotal(String subtotal) {
    this.subtotal = subtotal;
    return this;
  }

  public List<Rate> getRates() {
    return rates;
  }

  public ReservationVO setRates(List<Rate> rates) {
    this.rates = rates;
    return this;
  }

  public String getCancellationPolicy() {
    return cancellationPolicy;
  }

  public ReservationVO setCancellationPolicy(String cancellationPolicy) {
    this.cancellationPolicy = cancellationPolicy;
    return this;
  }

  public static class Rate {
    private String description;
    private String name;
    private String price;
    private String displayName;

    public String getName() {
      return this.name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getDisplayName() {
      return this.displayName;
    }

    public void setDisplayName(String displayName) {
      this.displayName = displayName;
    }

    public String getDescription() {
      return this.description;
    }

    public void setDescription(String description) {
      this.description = description;
    }

    public String getPrice() {
      return this.price;
    }

    public void setPrice(String price) {
      this.price = price;
    }
  }
}
