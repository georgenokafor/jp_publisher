package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by surge on 2015-11-24.
 */
@Entity @Table(name = "t42_guide") public class T42Guide
    extends Model {
  public static Model.Finder<GuideId, T42Guide> find = new Finder(T42Guide.class);
  @Id GuideId id;
  @ManyToOne  @JoinColumn(name="poi_place_key", nullable = false, referencedColumnName = "poi_place_key")
  T42Poi       poiPlaceKey;
  String        rowType;
  @ManyToOne  @JoinColumn(name="class_id", nullable = false, referencedColumnName = "class_id")
  T42GuideIndex classId;
  @ManyToOne  @JoinColumn(name="ref_class_id", nullable = true, referencedColumnName = "class_id")
  T42GuideIndex refClassId;
  @ManyToOne  @JoinColumn(name="top_class_id", nullable = true, referencedColumnName = "class_id")
  T42GuideIndex topClassId;
  Integer       levelNbr;
  String        content;
  Boolean       active;
  @Version int version;

  @Transient //Temp
  String afarKey;

  /**
   * Helper to see if guide is a category 3 overview
   * @return
   */
  public boolean isOverview(){
    return this.getClassId().getClassId().endsWith("-OVW#");
  }


  public static T42Guide rootLevelGuide(Integer geoPlaceKey, String classId) {
    return find.where().eq("geo_place_key", geoPlaceKey).eq("class_id", classId).eq("level_nbr", 1).findUnique();
  }

  /**
   * Searching all guides that are available for this destination
   */
  public static List<T42Guide> findByPlaceKey(Integer geoPlaceKey) {
    return find.where().eq("geo_place_key", geoPlaceKey).orderBy("sequence_nbr").findList();
  }

  public static T42Guide findByKey(String placeKey, String seqNumber) {
    GuideId gid = new GuideId(placeKey, seqNumber);
    return find.byId(gid);
  }

  public static T42Guide findByKey(Integer placeKey, Integer seqNumber) {
    GuideId gid = new GuideId(placeKey, seqNumber);
    return find.byId(gid);
  }

  public T42Poi getPoiPlaceKey() {
    return poiPlaceKey;
  }

  public void setPoiPlaceKey(String poiPlaceKey) {
    if (poiPlaceKey.length() == 0) {
      return;
    }
    try {
      Integer pk = Integer.parseInt(poiPlaceKey);
      T42Poi p = T42Poi.find.byId(pk);
      setPoiPlaceKey(p);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public void setPoiPlaceKey(T42Poi poiPlaceKey) {
    this.poiPlaceKey = poiPlaceKey;
  }

  public T42GuideIndex getClassId() {
    return classId;
  }

  public void setClassId(T42GuideIndex classId) {
    this.classId = classId;
  }

  public T42GuideIndex getRefClassId() {
    return refClassId;
  }

  public void setRefClassId(T42GuideIndex refClassId) {
    this.refClassId = refClassId;
  }

  public T42GuideIndex getTopClassId() {
    return topClassId;
  }

  public void setTopClassId(T42GuideIndex topClassId) {
    this.topClassId = topClassId;
  }

  public GuideId getId() {
    return id;
  }

  public void setId(GuideId id) {
    this.id = id;
  }

  public void setId(String placeKey, String seqNumber) {
    GuideId gid = new GuideId(placeKey, seqNumber);
    setId(gid);
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public String getRowType() {
    return rowType;
  }

  public void setRowType(String rowType) {
    this.rowType = rowType;
  }

  public Integer getLevelNbr() {
    return levelNbr;
  }

  public void setLevelNbr(String levelNbr) {
    if (levelNbr.length() == 0) {
      return;
    }
    try {
      this.levelNbr = Integer.parseInt(levelNbr);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public void setLevelNbr(Integer levelNbr) {
    this.levelNbr = levelNbr;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getAfarKey() {
    return afarKey;
  }

  public void setAfarKey(String afarKey) {
    this.afarKey = afarKey;
  }

  @Embeddable public static class GuideId {
    @Column(name = "geo_place_key") Integer geoPlaceKey;
    @Column(name = "sequence_nbr")  Integer sequenceNbr;

    public GuideId() {
      geoPlaceKey = 0;
      sequenceNbr = 0;
    }


    public GuideId(Integer placeKey, Integer seqNbr) {
      setGeoPlaceKey(placeKey);
      setSequenceNbr(seqNbr);
    }

    public GuideId(String placeKey, String seqNbr) {
      setGeoPlaceKey(placeKey);
      setSequenceNbr(seqNbr);
    }

    public void setGeoPlaceKey(String geoPlaceKey) {
      if (geoPlaceKey.length() == 0) {
        return;
      }
      try {
        this.geoPlaceKey = Integer.parseInt(geoPlaceKey);
      }
      catch (NumberFormatException e) {
        e.printStackTrace();
      }
    }

    public void setSequenceNbr(String sequenceNbr) {
      if (sequenceNbr.length() == 0) {
        return;
      }
      try {
        this.sequenceNbr = Integer.parseInt(sequenceNbr);
      }
      catch (NumberFormatException e) {
        e.printStackTrace();
      }
    }

    public Integer getSequenceNbr() {
      return sequenceNbr;
    }

    public void setSequenceNbr(Integer sequenceNbr) {
      this.sequenceNbr = sequenceNbr;
    }

    public Integer getGeoPlaceKey() {
      return geoPlaceKey;
    }

    public void setGeoPlaceKey(Integer geoPlaceKey) {
      this.geoPlaceKey = geoPlaceKey;
    }

    @Override public int hashCode() {
      return geoPlaceKey.hashCode() * sequenceNbr.hashCode();
    }

    @Override public boolean equals(Object o) {
      if (o == null) {
        return false;
      }
      if (!(o instanceof GuideId)) {
        return false;
      }
      GuideId guideId = (GuideId) o;
      if (!(guideId.geoPlaceKey.intValue() == geoPlaceKey.intValue())) {
        return false;
      }
      if (!(guideId.sequenceNbr.intValue() == sequenceNbr.intValue())) {
        return false;
      }
      return true;
    }
  }
}
