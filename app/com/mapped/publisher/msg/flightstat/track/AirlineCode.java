
package com.mapped.publisher.msg.flightstat.track;

import java.util.List;

public class AirlineCode{
   	private String fsCode;
   	private String requestedCode;

 	public String getFsCode(){
		return this.fsCode;
	}
	public void setFsCode(String fsCode){
		this.fsCode = fsCode;
	}
 	public String getRequestedCode(){
		return this.requestedCode;
	}
	public void setRequestedCode(String requestedCode){
		this.requestedCode = requestedCode;
	}
}
