package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by surge on 2015-05-12.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RentalCar
    extends Product {

  public Thing brand;

  /**
   * [Organization]	The rental company, e.g. 'Hertz'.
   */
  public Organization rentalCompany;

}
