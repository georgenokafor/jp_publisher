package com.mapped.publisher.parse.traxo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.parse.extractor.booking.email.ReservationsHolder;
import com.mapped.publisher.parse.schemaorg.*;
import com.mapped.publisher.parse.schemaorg.DateTime;
import com.mapped.publisher.parse.worldmate.CarRental;
import com.mapped.publisher.parse.worldmate.HotelReservation;
import com.mapped.publisher.persistence.bookingapi.BookingRS;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.enums.ReservationType;
import controllers.ApiBookingController;
import models.publisher.BkApiSrc;
import org.joda.time.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 10/06/17.
 */
public class TraxoHelper {
    private static final ObjectMapper om;
    long startTimestamp = 0;
    long endTimestamp = 0;

    static {
        om = new ObjectMapper();
        //Perform any kind of configuration for object mapper in this area

        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        //om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        //om.enableDefaultTypingAsProperty(ObjectMapper.DefaultTyping.NON_FINAL, "type");
        om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        om.registerSubtypes(new NamedType(Air.class, "Air"),
                new NamedType(Car.class, "Car"),
                new NamedType(Hotel.class, "Hotel"),
                new NamedType(Rail.class, "Rail"),
                new NamedType(Activity.class, "Activity")
        );
        om.enable(SerializationFeature.INDENT_OUTPUT); //Pretty-print for debugging
    }

    public List<Air> flights;
    public List<Hotel> hotels;
    public List<Rail> trains;
    public List<Car> cars;
    public List<Activity> activities;
    public Response response;
    public DataObject dataObject;

    public TraxoHelper() {
        flights = new ArrayList<>(1);
        hotels = new ArrayList<>(1);
        cars = new ArrayList<>(1);
        activities = new ArrayList<>(1);
        trains = new ArrayList<>(1);
    }

    public boolean hasReservations() {
        if (!flights.isEmpty() || !hotels.isEmpty() || !trains.isEmpty() || !cars.isEmpty() || !activities.isEmpty()) {
            return true;
        }
        return false;
    }

    public static ObjectReader getObjectReader() {
        return om.readerFor(Response.class);
    }

    public boolean parseJson(String jsonText) {
        boolean result = true;
        try {
            JsonFactory f = new JsonFactory();
            JsonParser jp = f.createParser(jsonText);
            this.response = getObjectReader().readValue(jp, Response.class);
            if (response.data != null && response.data.object != null) {

                dataObject = response.data.object;
                parseReservation(dataObject);
            }

        } catch (Exception e) {
            Log.err("ReservationHolder: Error parsing JSON", e);
            result = false;
        }
        return result;
    }

    public void parseReservation(DataObject ob) {
        if (ob != null && ob.segments != null) {
            for (Object s : ob.segments) {
                assignReservation(s);
            }
        }
    }

    public void assignReservation(Object reservation) {
        if (reservation instanceof Air) {
            flights.add((Air) reservation);
        } else if (reservation instanceof Hotel) {
            hotels.add((Hotel) reservation);
        } else if (reservation instanceof Activity) {

            activities.add((Activity) reservation);
        } else if (reservation instanceof Rail) {
            trains.add((Rail) reservation);
        } else if (reservation instanceof Car) {
            cars.add((Car) reservation);
        }

    }

    public ReservationPackage toReservationPacakage(String userId, String cmpyId, String tripName) {
        ReservationPackage rp = new ReservationPackage();
        rp.reservationNumber = Utils.shortenNumber(DBConnectionMgr.getUniqueKey());
        rp.bookingAgent = new Organization();
        rp.bookingAgent.umId = userId;
        rp.umId = "EMAIL";
        rp.name = tripName;

        rp.reservation.addAll(toFlightReservations());
        rp.reservation.addAll(toActivityReservations());
        rp.reservation.addAll(toCarReservations());
        rp.reservation.addAll(toRailReservations());
        rp.reservation.addAll(toHotelReservations());

        if (rp.reservation.size() > 0) {

            return rp;
        }
        return null;
    }

    public List<FlightReservation> toFlightReservations() {
        List<FlightReservation> flightReservations = new ArrayList<>();
        for (Air air: flights) {
            FlightReservation fr = new FlightReservation();
            fr.underName = new Person();
            if (air.first_name != null && !air.first_name.isEmpty() && air.last_name !=null && !air.last_name.isEmpty()) {
                fr.underName.familyName = air.last_name;
                fr.underName.givenName = air.first_name;
            } else if (air.travelers != null && air.travelers.size() > 1) {
                Traveler t = air.travelers.get(0);
                fr.underName.givenName = t.first_name;
                fr.underName.familyName = t.last_name;
                if ((fr.underName.givenName == null || fr.underName.givenName.isEmpty()) && (fr.underName.familyName == null || fr.underName.familyName.isEmpty())
                        &&t.name != null && t.name.contains("/")) {
                    fr.underName.givenName = t.name.substring(t.name.indexOf("/") + 1);
                    fr.underName.familyName = t.name.substring(0, t.name.indexOf("/"));
                }
            }

            fr.reservationId = DBConnectionMgr.getUniqueId();
            fr.reservationNumber = air.confirmation_no;
            fr.reservationStatus = "Confirmed";
            fr.reservationFor = new com.mapped.publisher.parse.schemaorg.Flight();
            fr.reservationFor.airline = new Airline();
            fr.reservationFor.airline.name = air.airline;
            fr.reservationFor.airline.iataCode = air.iata_code;
            fr.reservationFor.flightNumber = air.flight_number;

            fr.reservationFor.arrivalAirport = new Airport();
            fr.reservationFor.arrivalAirport.iataCode = air.destination;
            fr.reservationFor.arrivalAirport.name = air.destination_name;

            fr.reservationFor.departureAirport = new Airport();
            fr.reservationFor.departureAirport.iataCode = air.origin;
            fr.reservationFor.departureAirport.name = air.origin_name;

            fr.reservationFor.arrivalTime = getDateTime(air.arrival_datetime);
            fr.reservationFor.departureTime = getDateTime(air.departure_datetime);

            if (air.travelers != null) {
                for (int i = 0; i < air.travelers.size(); i++) {
                    FlightPassenger sub = new FlightPassenger();
                    sub.underName = new Person();
                    sub.underName.givenName = air.travelers.get(i).first_name;
                    sub.underName.familyName = air.travelers.get(i).last_name;
                    if ((sub.underName.givenName == null || sub.underName.givenName.isEmpty()) && (sub.underName.familyName == null || sub.underName.familyName.isEmpty())
                            && air.travelers.get(i).name != null && air.travelers.get(i).name.contains("/")) {
                        sub.underName.givenName = air.travelers.get(i).name.substring(air.travelers.get(i).name.indexOf("/") + 1);
                        sub.underName.familyName = air.travelers.get(i).name.substring(0, air.travelers.get(i).name.indexOf("/"));
                    }
                    if (air.seats != null && air.seats.size() > 0 && i < air.seats.size()) {
                        sub.seat = air.seats.get(i);
                    }
                    if (air.tickets != null && air.tickets.size() > 0 && i < air.tickets.size()) {
                        sub.ticketNumber = air.tickets.get(i);
                    }
                    sub.airplaneSeatClass = new AirplaneSeatClass();
                    sub.airplaneSeatClass.name = air.class_of_service;

                    fr.addPassenger(sub);

                }
            }

            double total = 0.0;
            double tax = 0.0;
            String cur = "";
            if (air.currency != null && !air.currency.isEmpty() && !air.currency.equals("null")) {
                cur = air.currency;
                fr.priceCurrency = air.currency;
            }

            if(air.price_details != null) {
                for (PriceDetail p : air.price_details) {
                    FlightReservation sub = new FlightReservation();
                    sub.rateName = p.name;
                    sub.priceCurrency = p.units;
                    sub.ratePrice = String.valueOf(p.value);
                    sub.rateDescription = p.type;
                    if (p.type.equals("total")) {
                        total = Double.valueOf(p.value);
                    } else if (p.type.equals("tax")) {
                        tax = Double.valueOf(p.value);
                    }
                    fr.addRates(sub);
                }
            }
            
            StringBuilder sb = new StringBuilder();
            if (air.number_of_pax != null && !air.number_of_pax.isEmpty() && !air.number_of_pax.equals("null") 
                && Integer.valueOf(air.number_of_pax) > 0) {
                sb.append("No. of Passengers: ");
                sb.append(air.number_of_pax);
                sb.append("\n");
            }

            if (total > 0.0) {
                sb.append("\n\nPrice: ");
                sb.append(total);
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                sb.append("Tax: ");
                sb.append(tax);
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                sb.append("Total: ");
                sb.append(String.valueOf(total + tax));
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                fr.price = String.valueOf(total);
                fr.taxes = String.valueOf(tax);
                fr.totalPrice = String.valueOf(total + tax);
            }
            fr.description = sb.toString();

            flightReservations.add(fr);
        }
        return flightReservations;
    }

    public List<LodgingReservation> toHotelReservations() {
        List<LodgingReservation> lodgingReservations = new ArrayList<>();
        for (Hotel hotel : hotels) {
            LodgingReservation hr = new LodgingReservation();
            hr.underName = new Person();
            if (hotel.first_name != null && !hotel.first_name.isEmpty() && hotel.last_name !=null && !hotel.last_name.isEmpty()) {
                hr.underName.familyName = hotel.last_name;
                hr.underName.givenName = hotel.first_name;
            } else if (hotel.travelers != null && hotel.travelers.size() > 1) {
                Traveler t = hotel.travelers.get(0);
                hr.underName.givenName = t.first_name;
                hr.underName.familyName = t.last_name;
                if ((hr.underName.givenName == null || hr.underName.givenName.isEmpty()) && (hr.underName.familyName == null || hr.underName.familyName.isEmpty())
                        &&t.name != null && t.name.contains("/")) {
                    hr.underName.givenName = t.name.substring(t.name.indexOf("/") + 1);
                    hr.underName.familyName = t.name.substring(0, t.name.indexOf("/"));
                }
            }
            hr.checkinTime = getDateTime(hotel.checkin_date);
            hr.checkinDate = getDateTime(hotel.checkin_date);
            hr.checkoutTime = getDateTime(hotel.checkout_date);
            hr.checkoutDate = getDateTime(hotel.checkout_date);
            
            hr.reservationId = DBConnectionMgr.getUniqueId();
            hr.reservationStatus = "Confirmed";
            hr.reservationFor = new LodgingBusiness();
            hr.reservationFor.name = hotel.hotel_name;
            if (hotel.address1 != null) {
                hr.reservationFor.address = new PostalAddress();
                hr.reservationFor.address.streetAddress = hotel.address1;
                hr.reservationFor.address.postalCode = hotel.postal_code;
                hr.reservationFor.address.addressCountry = hotel.country;
            }

            hr.reservationNumber = hotel.confirmation_no;
            hr.lodgingUnitType = hotel.room_type;
            hr.lodgingUnitDescription = hotel.room_description;
            hr.cancellationPolicy = hotel.cancellation_policy;

            StringBuilder sb = new StringBuilder();

            if (hotel.room_type != null && !hotel.room_type.isEmpty() && !hotel.room_type.equals("null")) {
                hr.lodgingUnitType= hotel.room_type;
                sb.append("Room Type: ");
                sb.append(hotel.room_type);
                sb.append("\n");

            }
            
            if (hotel.room_description != null && !hotel.room_description.isEmpty() && !hotel.room_description.equals("null")) {
                hr.lodgingUnitDescription= hotel.room_description;
                sb.append("Room description: ");
                sb.append(hotel.room_description);
                sb.append("\n");

            }
            if (hotel.price != null) {
                String cur = "";
                if (hotel.currency != null  && !hotel.currency.isEmpty() && !hotel.currency.equals("null")) {
                    cur = hotel.currency;
                    hr.priceCurrency = hotel.currency;
                }

                if (hotel.price != null  && !hotel.price.isEmpty() && !hotel.price.equals("null")) {
                    sb.append("Total: ");
                    sb.append(hotel.price);
                    sb.append(" ");
                    sb.append(cur);
                    sb.append("\n");
                }

            }

            if (hotel.travelers != null && hotel.travelers.size() > 0) {
                hotel.travelers.stream().forEach((t) -> {
                        if (t != null) {
                            RoomType sub = new RoomType();
                            sub.underName = new Person();
                            sub.underName.givenName = t.first_name;
                            sub.underName.familyName = t.last_name;
                            if ((sub.underName.givenName == null || sub.underName.givenName.isEmpty()) && (sub.underName.familyName == null || sub.underName.familyName.isEmpty())
                                    &&t.name != null && t.name.contains("/")) {
                                sub.underName.givenName = t.name.substring(t.name.indexOf("/") + 1);
                                sub.underName.familyName = t.name.substring(0, t.name.indexOf("/"));
                            }

                            sub.lodgingUnitType = hotel.room_type;
                            sub.confirmStatus = hotel.status;
                            hr.addRoom(sub);
                        }
                    }
                );
            }

            if(hotel.price_details != null) {
                hotel.price_details.stream().forEach((p) -> {
                    LodgingReservation sub = new LodgingReservation();
                    sub.rateName = p.name;
                    sub.priceCurrency = p.units;
                    sub.ratePrice = String.valueOf(p.value);
                    sub.rateDescription = p.type;
                    hr.addRates(sub);
                });
            }

            double total = 0.0;
            double tax = 0.0;
            String cur = "";
            if (hotel.currency != null && !hotel.currency.isEmpty() && !hotel.currency.equals("null")) {
                cur = hotel.currency;
                hr.priceCurrency = hotel.currency;
            }

            if(hotel.price_details != null) {
                for (PriceDetail p : hotel.price_details) {
                    LodgingReservation sub = new LodgingReservation();
                    sub.rateName = p.name;
                    sub.priceCurrency = p.units;
                    sub.ratePrice = String.valueOf(p.value);
                    sub.rateDescription = p.type;
                    if (p.type.equals("total")) {
                        total = Double.valueOf(p.value);
                    } else if (p.type.equals("tax")) {
                        tax = Double.valueOf(p.value);
                    }
                    hr.addRates(sub);
                }
            }

            if (total > 0.0) {
                sb.append("\n\nDaily Rate: ");
                sb.append(total);
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                sb.append("Tax: ");
                sb.append(tax);
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                sb.append("Total: ");
                sb.append(String.valueOf(total + tax));
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                hr.price = String.valueOf(total);
                hr.taxes = String.valueOf(tax);
                hr.totalPrice = String.valueOf(total + tax);
            }
            hr.description = sb.toString();
            
            lodgingReservations.add(hr);
            
        }
        return lodgingReservations;
    }

    public List<RentalCarReservation> toCarReservations() {
        List<RentalCarReservation> carReservations = new ArrayList<>();
        for (Car car : cars) {
            RentalCarReservation cr = new RentalCarReservation();
            cr.reservationId = DBConnectionMgr.getUniqueId();
            cr.reservationNumber = car.confirmation_no;
            cr.reservationStatus="Confirmed";
            cr.underName = new Person();
            if (car.first_name != null && !car.first_name.isEmpty() && car.last_name !=null && !car.last_name.isEmpty()) {
                cr.underName.familyName = car.last_name;
                cr.underName.givenName = car.first_name;
            } else if (car.travelers != null && car.travelers.size() > 1) {
                Traveler t = car.travelers.get(0);
                cr.underName.givenName = t.first_name;
                cr.underName.familyName = t.last_name;
                if ((cr.underName.givenName == null || cr.underName.givenName.isEmpty()) && (cr.underName.familyName == null || cr.underName.familyName.isEmpty())
                        &&t.name != null && t.name.contains("/")) {
                    cr.underName.givenName = t.name.substring(t.name.indexOf("/") + 1);
                    cr.underName.familyName = t.name.substring(0, t.name.indexOf("/"));
                }
            }
            cr.pickupTime = getDateTime(car.pickup_datetime);
            cr.pickupLocation = new Place();
            //cr.pickupLocation.name = car.getPickup().getAirportCode();
            if (car.pickup_address1 != null && !car.pickup_address1.isEmpty() && !car.pickup_address1.equals("null")) {
                cr.pickupLocation.address = new PostalAddress();
                StringBuilder sb = new StringBuilder();
                sb.append(car.pickup_address1);
                if (car.pickup_address2 != null && !car.pickup_address2.isEmpty() && !car.pickup_address2.equals("null")) {
                    sb.append(", ");
                    sb.append(car.pickup_address2);
                }
                cr.pickupLocation.address.streetAddress = sb.toString();
                cr.pickupLocation.address.addressLocality = car.pickup_city_name;
                cr.pickupLocation.address.addressRegion = car.pickup_admin_code;
                cr.pickupLocation.address.postalCode = car.pickup_postal_code;
                cr.pickupLocation.address.addressCountry = car.pickup_country;
            }

            cr.dropoffTime = getDateTime(car.dropoff_datetime);
            cr.dropoffLocation = new Place();
            if (car.dropoff_address1 != null && !car.dropoff_address1.isEmpty() && !car.dropoff_address1.equals("null")) {
                cr.dropoffLocation.address = new PostalAddress();
                StringBuilder sb = new StringBuilder();
                sb.append(car.dropoff_address1);
                if (car.dropoff_address2 != null && !car.dropoff_address2.isEmpty() && !car.dropoff_address2.equals("null")) {
                    sb.append(", ");
                    sb.append(car.dropoff_address2);
                }
                cr.dropoffLocation.address.streetAddress = sb.toString();
                cr.dropoffLocation.address.addressLocality = car.dropoff_city_name;
                cr.dropoffLocation.address.addressRegion = car.dropoff_admin_code;
                cr.dropoffLocation.address.postalCode = car.dropoff_postal_code;
                cr.dropoffLocation.address.addressCountry = car.dropoff_country;
            }

            cr.reservationFor = new RentalCar();
            if (car.car_type != null && !car.car_type.isEmpty() && !car.car_type.equals("null")) {
                cr.reservationFor.model = car.car_type;
            }
            cr.reservationFor.rentalCompany = new Organization();
            cr.reservationFor.rentalCompany.name = car.car_company;

            StringBuilder sb = new StringBuilder();
            if (car.car_description != null && !car.car_description.isEmpty() && !car.car_description.equals("null")) {
                sb.append("Car description: ");
                sb.append(car.car_description);
                sb.append("\n");
            }
            if (car.price != null && !car.price.isEmpty() && !car.price.equals("null")) {
                String cur = "";
                if (car.currency != null && !car.currency.isEmpty() && !car.currency.equals("null")) {
                    cur = car.currency;
                    cr.priceCurrency = car.currency;
                }
                
                sb.append("Total: ");
                sb.append(car.price);
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
            }


            
            double total = 0.0;
            double tax = 0.0;
            String cur = "";
            if (car.currency != null && !car.currency.isEmpty() && !car.currency.equals("null")) {
                cur = car.currency;
                cr.priceCurrency = car.currency;
            }
            
            if(car.price_details != null) {
                for (PriceDetail p : car.price_details) {
                    RentalCarReservation sub = new RentalCarReservation();
                    sub.rateName = p.name;
                    sub.priceCurrency = p.units;
                    sub.ratePrice = String.valueOf(p.value);
                    sub.rateDescription = p.type;
                    if (p.type.equals("total")) {
                        total = Double.valueOf(p.value);
                    } else if (p.type.equals("tax")) {
                        tax = Double.valueOf(p.value);
                    }
                    cr.addRates(sub);
                }
            }
            
            if (total > 0.0) {
                sb.append("\n\nDaily Rate: ");
                sb.append(total);
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                sb.append("Tax: ");
                sb.append(tax);
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                sb.append("Total: ");
                sb.append(String.valueOf(total + tax));
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                cr.price = String.valueOf(total);
                cr.taxes = String.valueOf(tax);
                cr.totalPrice = String.valueOf(total + tax);
            }
            
            cr.description = sb.toString();

            carReservations.add(cr);

        }
        return carReservations;
    }

    public List<TrainReservation> toRailReservations() {
        List<TrainReservation> trainReservations = new ArrayList<>();
        for (Rail rail : trains) {
            TrainReservation tr = new TrainReservation();
            tr.reservationId = DBConnectionMgr.getUniqueId();
            tr.reservationNumber = rail.confirmation_no;//getProviderDetails().getConfirmationNumber();
            tr.reservationStatus = "Confirmed";
            tr.reservationFor = new TrainTrip();
            tr.underName = new Person();
            if (rail.first_name != null && !rail.first_name.isEmpty() && rail.last_name !=null && !rail.last_name.isEmpty()) {
                tr.underName.familyName = rail.last_name;
                tr.underName.givenName = rail.first_name;
            } else if (rail.travelers != null && rail.travelers.size() > 1) {
                Traveler t = rail.travelers.get(0);
                tr.underName.givenName = t.first_name;
                tr.underName.familyName = t.last_name;
                if ((tr.underName.givenName == null || tr.underName.givenName.isEmpty()) && (tr.underName.familyName == null || tr.underName.familyName.isEmpty())
                        &&t.name != null && t.name.contains("/")) {
                    tr.underName.givenName = t.name.substring(t.name.indexOf("/") + 1);
                    tr.underName.familyName = t.name.substring(0, t.name.indexOf("/"));
                }
            }
            tr.reservationFor.trainName = rail.rail_line;
            tr.reservationFor.trainNumber = rail.train_number;
            if (rail.origin != null) {
                tr.reservationFor.departTime = getDateTime(rail.departure_datetime);
                tr.reservationFor.departStation = new Place();
                tr.reservationFor.departStation.name = rail.origin;
                tr.reservationFor.departStation.address = new PostalAddress();
                tr.reservationFor.departStation.address.addressLocality = rail.origin_city_name;
                tr.reservationFor.departStation.address.addressRegion = rail.origin_admin_code;
                tr.reservationFor.departStation.address.addressCountry = rail.origin_country;
            }

            if (rail.destination != null) {
                tr.reservationFor.arrivalTime = getDateTime(rail.arrival_datetime);
                tr.reservationFor.arrivalStation = new Place();
                tr.reservationFor.arrivalStation.name = rail.destination;
                tr.reservationFor.arrivalStation.address = new PostalAddress();
                tr.reservationFor.arrivalStation.address.addressLocality = rail.destination_city_name;
                tr.reservationFor.arrivalStation.address.addressRegion = rail.destination_admin_code;
                tr.reservationFor.arrivalStation.address.addressCountry = rail.destination_country;
            }
/*
            if (rail.travelers != null) {
                for (int i = 0; i < rail.travelers.size(); i++) {
                    TrainReservation sub = new TrainReservation();
                    sub.underName = new Person();
                    sub.underName = new Person();
                    sub.underName.givenName = rail.travelers.get(i).first_name;
                    sub.underName.familyName = rail.travelers.get(i).last_name;
                    if ((sub.underName.givenName == null || sub.underName.givenName.isEmpty()) && (sub.underName.familyName == null || sub.underName.familyName.isEmpty())
                            && rail.travelers.get(i).name != null && rail.travelers.get(i).name.contains("/")) {
                        sub.underName.givenName = rail.travelers.get(i).name.substring(rail.travelers.get(i).name.indexOf("/") + 1);
                        sub.underName.familyName = rail.travelers.get(i).name.substring(0, rail.travelers.get(i).name.indexOf("/"));
                    }
                    if (rail.seats != null && rail.seats.size() > 0 && i < rail.seats.size()) {
                        sub.seat = rail.seats.get(i);
                    }
                    if (rail.tickets != null && rail.tickets.size() > 0 && i < rail.tickets.size()) {
                        sub.ticketNumber = rail.tickets.get(i);
                    }
                    tr.addSubReservation(sub);

                }
            }*/

            double total = 0.0;
            double tax = 0.0;
            String cur = "";
            if (rail.currency != null && !rail.currency.isEmpty() && !rail.currency.equals("null")) {
                cur = rail.currency;
                tr.priceCurrency = rail.currency;
            }

            if(rail.price_details != null) {
                for (PriceDetail p : rail.price_details) {
                    FlightReservation sub = new FlightReservation();
                    sub.rateName = p.name;
                    sub.priceCurrency = p.units;
                    sub.ratePrice = String.valueOf(p.value);
                    sub.rateDescription = p.type;
                    if (p.type.equals("total")) {
                        total = Double.valueOf(p.value);
                    } else if (p.type.equals("tax")) {
                        tax = Double.valueOf(p.value);
                    }
                    tr.addRates(sub);
                }
            }

            StringBuilder sb = new StringBuilder();
            if (rail.number_of_pax != null && !rail.number_of_pax.isEmpty() && !rail.number_of_pax.equals("null")
                && Integer.valueOf(rail.number_of_pax) > 0) {
                sb.append("No. of Passengers: ");
                sb.append(rail.number_of_pax);
                sb.append("\n");
            }

            if (total > 0.0) {
                sb.append("\n\nPrice: ");
                sb.append(total);
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                sb.append("Tax: ");
                sb.append(tax);
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                sb.append("Total: ");
                sb.append(String.valueOf(total + tax));
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                tr.price = String.valueOf(total);
                tr.taxes = String.valueOf(tax);
                tr.totalPrice = String.valueOf(total + tax);
            }
            tr.description = sb.toString();

            trainReservations.add(tr);
        }
        return trainReservations;
    }

    public List<ActivityReservation> toActivityReservations() {
        List<ActivityReservation> activityReservations = new ArrayList<>();
        for (Activity act : activities) {
            ActivityReservation ar = new ActivityReservation();

            ar.reservationId = DBConnectionMgr.getUniqueId();
            ar.reservationStatus = "Confirmed";
            ar.reservationNumber = act.confirmation_no;
            ar.name = act.activity_name;
            ar.underName = new Person();
            if (act.first_name != null && !act.first_name.isEmpty() && act.last_name !=null && !act.last_name.isEmpty()) {
                ar.underName.familyName = act.last_name;
                ar.underName.givenName = act.first_name;
            } else if (act.travelers != null && act.travelers.size() > 1) {
                Traveler t = act.travelers.get(0);
                ar.underName.givenName = t.first_name;
                ar.underName.familyName = t.last_name;
                if ((ar.underName.givenName == null || ar.underName.givenName.isEmpty()) && (ar.underName.familyName == null || ar.underName.familyName.isEmpty())
                        &&t.name != null && t.name.contains("/")) {
                    ar.underName.givenName = t.name.substring(t.name.indexOf("/") + 1);
                    ar.underName.familyName = t.name.substring(0, t.name.indexOf("/"));
                }
            }

            ar.startTime = getDateTime(act.start_datetime);
            ar.startLocation = new Place();
            ar.startLocation.name = act.start_name;

            if (act.start_address1 != null && !act.start_address1.isEmpty() && !act.start_address1.equals("null")) {
                ar.startLocation.address = new PostalAddress();
                StringBuilder sb = new StringBuilder();
                sb.append(act.start_address1);
                if (act.start_address2 != null && !act.start_address2.isEmpty() && !act.start_address2.equals("null")) {
                    sb.append(", ");
                    sb.append(act.start_address2);
                }
                ar.startLocation.address.streetAddress = sb.toString();
                ar.startLocation.address.addressLocality = act.start_city_name;
                ar.startLocation.address.addressRegion = act.start_admin_code;
                ar.startLocation.address.postalCode = act.start_postal_code;
                ar.startLocation.address.addressCountry = act.start_country;
            }


            ar.finishTime = getDateTime(act.end_datetime);
            ar.finishLocation = new Place();

            if (act.end_address1 != null && !act.end_address1.isEmpty() && !act.end_address1.equals("null")) {
                ar.finishLocation.address = new PostalAddress();
                StringBuilder sb = new StringBuilder();
                sb.append(act.end_address1);
                if (act.end_address2 != null && !act.end_address2.isEmpty() && !act.end_address2.equals("null")) {
                    sb.append(", ");
                    sb.append(act.end_address2);
                }
                ar.finishLocation.address.streetAddress = sb.toString();
                ar.finishLocation.address.addressLocality = act.end_city_name;
                ar.finishLocation.address.addressRegion = act.end_admin_code;
                ar.finishLocation.address.postalCode = act.end_postal_code;
                ar.finishLocation.address.addressCountry = act.end_country;
            }


            

            double total = 0.0;
            double tax = 0.0;
            String cur = "";
            if (act.currency != null && !act.currency.isEmpty() && !act.currency.equals("null")) {
                cur = act.currency;
                ar.priceCurrency = act.currency;
            }

            if(act.price_details != null) {
                for (PriceDetail p : act.price_details) {
                    FlightReservation sub = new FlightReservation();
                    sub.rateName = p.name;
                    sub.priceCurrency = p.units;
                    sub.ratePrice = String.valueOf(p.value);
                    sub.rateDescription = p.type;
                    if (p.type.equals("total")) {
                        total = Double.valueOf(p.value);
                    } else if (p.type.equals("tax")) {
                        tax = Double.valueOf(p.value);
                    }
                    ar.addRates(sub);
                }
            }

            StringBuilder sb = new StringBuilder();
            if (total > 0.0) {
                sb.append("\n\nPrice: ");
                sb.append(total);
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                sb.append("Tax: ");
                sb.append(tax);
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                sb.append("Total: ");
                sb.append(String.valueOf(total + tax));
                sb.append(" ");
                sb.append(cur);
                sb.append("\n");
                ar.price = String.valueOf(total);
                ar.taxes = String.valueOf(tax);
                ar.totalPrice = String.valueOf(total + tax);
            }
            ar.description = sb.toString();

            activityReservations.add(ar);
        }
        return activityReservations;
    }

    public static void main(String[] args) {
        String s = "{\"created\": \"2015-04-10T04:49:34+00:00\"," +
                "    \"data\": {" +
                "        \"object\": {" +
                "            \"created\": \"2015-04-10T04:49:21+0000\"," +
                "            \"from_address\": \"testuser@example.com\"," +
                "            \"id\": \"405612237952923800\"," +
                "            \"mailbox_id\": \"405621341795997700\"," +
                "            \"modified\": \"2015-04-10T04:49:34+0000\"," +
                "            \"segments\": [" +
                "                {" +
                "    \"activity_type\": \"General\"," +
                "    \"activity_name\": \"Van Halen\"," +
                "    \"first_name\": \"Don\"," +
                "    \"last_name\": \"Draper\"," +
                "    \"start_name\": \"Park City Ampitheater\"," +
                "    \"start_admin_code\": \"UT\"," +
                "    \"start_address1\": \"1895 Sidewinder Drive\"," +
                "    \"start_address2\": null," +
                "    \"start_city_name\": \"Park City\"," +
                "    \"start_country\": \"US\"," +
                "    \"start_lat\": \"40.6460609436035\"," +
                "    \"start_lon\": \"-111.497970581055\"," +
                "    \"start_postal_code\": \"84060\"," +
                "    \"end_name\": \"Park City Ampitheater\"," +
                "    \"end_admin_code\": \"UT\"," +
                "    \"end_address1\": \"1895 Sidewinder Drive\"," +
                "    \"end_address2\": null," +
                "    \"end_city_name\": \"Park City\"," +
                "    \"end_country\": \"US\"," +
                "    \"end_lat\": \"40.6460609436035\"," +
                "    \"end_lon\": \"-111.497970581055\"," +
                "    \"end_postal_code\": \"84060\"," +
                "    \"start_datetime\": \"2015-03-06 18:00:00\"," +
                "    \"end_datetime\": \"2015-03-10 22:00:00\"," +
                "    \"confirmation_no\": \"12345678\"," +
                "    \"created\": \"2015-04-10T04:49:35+0000\"," +
                "    \"currency\": \"USD\"," +
                "    \"price\": \"1282.58\"," +
                "    \"source\": \"Email\"," +
                "    \"status\": \"Active\"," +
                "    \"time_zone_id\": \"America/Denver\"," +
                "    \"type\": \"Activity\"," +
                "    \"travelers\": [" +
                "      {" +
                "        \"name\": \"Mr. Don Draper\"," +
                "        \"first_name\": \"Don\"," +
                "        \"last_name\": \"Draper\"" +
                "      }" +
                "    ]," +
                "    \"price_details\": [" +
                "      {" +
                "        \"type\": \"total\"," +
                "        \"name\": \"Total\"," +
                "        \"value\": \"50.00\"," +
                "        \"units\": \"USD\"" +
                "      }," +
                "      {" +
                "        \"type\": \"tax\"," +
                "        \"name\": \"Tax\"," +
                "        \"value\": \"8.25\"," +
                "        \"units\": \"USD\"" +
                "      }" +
                "    ]" +
                "}" +
                "            ]," +
                "            \"source\": \"Email\"," +
                "            \"status\": \"Processed\"," +
                "            \"subject\": \"Fwd: Reservation Confirmation #12345678 for Park City Marriott\"," +
                "            \"to_address\": \"plans@example-travelapp.com\"," +
                "            \"user_address\": \"testuser@example.com\"" +
                "        }" +
                "    }," +
                "    \"type\": \"email.updated\"" +
                "}";

        String s1 = "{\"id\":\"673076204042204868\",\"type\":\"email.updated\"," +
                    "\"created\":\"2017-07-17T16:02:15+00:00\",\"data\":{\"object\":{\"id\":\"673076167550126253\"," +
                    "\"mailbox_id\":\"638966011000394425\",\"mailbox_type\":\"Developer\",\"status\":\"Processed\"," +
                    "\"source\":null,\"user_address\":\"thierry.wong@gmail.com\",\"from_address\":\"thierry" +
                    ".wong@gmail.com\",\"to_address\":\"plans+592ed3bc04bc4f6d87aa0db80a011b56@d.us1.traxo.com\"," +
                    "\"cc_address\":null,\"subject\":\"Fwd: Delta to Cincy\"," +
                    "\"msg_id\":\"CAA45sXvVqrxaFKsBEp6pvz2T94=2pK27a4aQ1-m-7gO5u37usA@mail.gmail.com\"," +
                    "\"msg_date\":\"Mon, 17 Jul 2017 12:02:02 -0400\",\"created\":\"2017-07-17T16:02:11+0000\"," +
                    "\"modified\":\"2017-07-17T16:02:15+0000\",\"metadata\":null,\"includes\":[]," +
                    "\"segments\":[{\"type\":\"Air\",\"status\":\"Active\",\"source\":\"Delta\",\"first_name\":null," +
                    "\"last_name\":null,\"airline\":\"Delta Air Lines\",\"iata_code\":\"DL\"," +
                    "\"normalized_airline\":\"DAL\",\"flight_number\":\"5289\",\"seat_assignment\":\"16C\"," +
                    "\"origin\":\"YUL\",\"origin_name\":\"Montreal-pierre Elliott Trudeau International Airport\"," +
                    "\"origin_city_name\":\"Montreal\",\"origin_admin_code\":\"QC\",\"origin_country\":\"CA\"," +
                    "\"origin_lat\":\"45.468055725097656\",\"origin_lon\":\"-73.74138641357422\"," +
                    "\"destination\":\"LGA\",\"destination_name\":\"La Guardia\",\"destination_city_name\":\"New " +
                    "York\",\"destination_admin_code\":\"NY\",\"destination_country\":\"US\"," +
                    "\"destination_lat\":\"40.78333282470703\",\"destination_lon\":\"-73.86666870117188\"," +
                    "\"departure_datetime\":\"2017-07-18T07:00:00\"," +
                    "\"departure_time_zone_id\":\"America\\/Montreal\",\"arrival_datetime\":\"2017-07-18T08:38:00\"," +
                    "\"arrival_time_zone_id\":\"America\\/New_York\",\"confirmation_no\":\"JMPXFZ\",\"phone\":null," +
                    "\"price\":\"\",\"currency\":null,\"number_of_pax\":1,\"ticket_number\":null," +
                    "\"fare_basis_code\":null,\"class_of_service\":null,\"created\":\"2017-07-17T16:02:15+0000\"," +
                    "\"travelers\":null,\"tickets\":null,\"seats\":[\"16C\",\"11B\"],\"price_details\":null," +
                    "\"metadata\":null},{\"type\":\"Air\",\"status\":\"Active\",\"source\":\"Delta\"," +
                    "\"first_name\":null,\"last_name\":null,\"airline\":\"Delta Air Lines\",\"iata_code\":\"DL\"," +
                    "\"normalized_airline\":\"DAL\",\"flight_number\":\"5895\",\"seat_assignment\":\"11B\"," +
                    "\"origin\":\"LGA\",\"origin_name\":\"La Guardia\",\"origin_city_name\":\"New York\"," +
                    "\"origin_admin_code\":\"NY\",\"origin_country\":\"US\",\"origin_lat\":\"40.78333282470703\"," +
                    "\"origin_lon\":\"-73.86666870117188\",\"destination\":\"SDF\",\"destination_name\":\"Louisville " +
                    "International (Standiford Field)\",\"destination_city_name\":\"Louisville\"," +
                    "\"destination_admin_code\":\"KY\",\"destination_country\":\"US\"," +
                    "\"destination_lat\":\"38.18333435058594\",\"destination_lon\":\"-85.73332977294922\"," +
                    "\"departure_datetime\":\"2017-07-18T09:30:00\"," +
                    "\"departure_time_zone_id\":\"America\\/New_York\",\"arrival_datetime\":\"2017-07-18T11:59:00\"," +
                    "\"arrival_time_zone_id\":\"America\\/Kentucky\\/Louisville\",\"confirmation_no\":\"JMPXFZ\"," +
                    "\"phone\":null,\"price\":\"\",\"currency\":null,\"number_of_pax\":1,\"ticket_number\":null," +
                    "\"fare_basis_code\":null,\"class_of_service\":null,\"created\":\"2017-07-17T16:02:15+0000\"," +
                    "\"travelers\":null,\"tickets\":null,\"seats\":[\"11B\",\"11C\"],\"price_details\":null," +
                    "\"metadata\":null},{\"type\":\"Air\",\"status\":\"Active\",\"source\":\"Delta\"," +
                    "\"first_name\":null,\"last_name\":null,\"airline\":\"Delta Air Lines\",\"iata_code\":\"DL\"," +
                    "\"normalized_airline\":\"DAL\",\"flight_number\":\"5957\",\"seat_assignment\":\"15C\"," +
                    "\"origin\":\"SDF\",\"origin_name\":\"Louisville International (Standiford Field)\"," +
                    "\"origin_city_name\":\"Louisville\",\"origin_admin_code\":\"KY\",\"origin_country\":\"US\"," +
                    "\"origin_lat\":\"38.18333435058594\",\"origin_lon\":\"-85.73332977294922\"," +
                    "\"destination\":\"LGA\",\"destination_name\":\"La Guardia\",\"destination_city_name\":\"New " +
                    "York\",\"destination_admin_code\":\"NY\",\"destination_country\":\"US\"," +
                    "\"destination_lat\":\"40.78333282470703\",\"destination_lon\":\"-73.86666870117188\"," +
                    "\"departure_datetime\":\"2017-07-19T17:57:00\"," +
                    "\"departure_time_zone_id\":\"America\\/Kentucky\\/Louisville\"," +
                    "\"arrival_datetime\":\"2017-07-19T20:08:00\",\"arrival_time_zone_id\":\"America\\/New_York\"," +
                    "\"confirmation_no\":\"JMPXFZ\",\"phone\":null,\"price\":\"\",\"currency\":null," +
                    "\"number_of_pax\":1,\"ticket_number\":null,\"fare_basis_code\":null,\"class_of_service\":null," +
                    "\"created\":\"2017-07-17T16:02:15+0000\",\"travelers\":null,\"tickets\":null,\"seats\":[\"15C\"," +
                    "\"17B\"],\"price_details\":null,\"metadata\":null},{\"type\":\"Air\",\"status\":\"Active\"," +
                    "\"source\":\"Delta\",\"first_name\":null,\"last_name\":null,\"airline\":\"Delta Air Lines\"," +
                    "\"iata_code\":\"DL\",\"normalized_airline\":\"DAL\",\"flight_number\":\"5288\"," +
                    "\"seat_assignment\":\"15C\",\"origin\":\"LGA\",\"origin_name\":\"La Guardia\"," +
                    "\"origin_city_name\":\"New York\",\"origin_admin_code\":\"NY\",\"origin_country\":\"US\"," +
                    "\"origin_lat\":\"40.78333282470703\",\"origin_lon\":\"-73.86666870117188\"," +
                    "\"destination\":\"YUL\",\"destination_name\":\"Montreal-pierre Elliott Trudeau International " +
                    "Airport\",\"destination_city_name\":\"Montreal\",\"destination_admin_code\":\"QC\"," +
                    "\"destination_country\":\"CA\",\"destination_lat\":\"45.468055725097656\"," +
                    "\"destination_lon\":\"-73.74138641357422\",\"departure_datetime\":\"2017-07-19T20:45:00\"," +
                    "\"departure_time_zone_id\":\"America\\/New_York\",\"arrival_datetime\":\"2017-07-19T22:22:00\"," +
                    "\"arrival_time_zone_id\":\"America\\/Montreal\",\"confirmation_no\":\"JMPXFZ\",\"phone\":null," +
                    "\"price\":\"\",\"currency\":null,\"number_of_pax\":1,\"ticket_number\":null," +
                    "\"fare_basis_code\":null,\"class_of_service\":null,\"created\":\"2017-07-17T16:02:15+0000\"," +
                    "\"travelers\":null,\"tickets\":null,\"seats\":[\"15C\",\"16C\"],\"price_details\":null," +
                    "\"metadata\":null}]}}}\n";

        String s2 = "{\"id\":\"755249611399695763\",\"type\":\"email.updated\",\"created\":\"2017-11-08T01:06:09+00:00\",\"data\":{\"object\":{\"id\":\"755249594738198362\",\"mailbox_id\":\"638966011000394425\",\"mailbox_type\":\"Developer\",\"status\":\"Processed\",\"source\":null,\"user_address\":\"teresas@ema-eda.com\",\"from_address\":\"teresas@ema-eda.com\",\"to_address\":\"plans+592ed3bc04bc4f6d87aa0db80a011b56@d.us1.traxo.com\",\"cc_address\":null,\"subject\":\"FW: eTicket Itinerary and Receipt for Confirmation NJ87HQ\",\"msg_id\":\"DM2PR0501MB826B39FB6BF5675249C2643F6560@DM2PR0501MB826.namprd05.prod.outlook.com\",\"msg_date\":\"Wed, 8 Nov 2017 01:05:59 +0000\",\"created\":\"2017-11-08T01:06:07+0000\",\"modified\":\"2017-11-08T01:06:09+0000\",\"metadata\":null,\"includes\":[],\"segments\":[{\"type\":\"Air\",\"status\":\"Active\",\"source\":\"United\",\"first_name\":\"MICHAELJOHN\",\"last_name\":\"MERRIMAN\",\"airline\":\"United Airlines\",\"iata_code\":\"UA\",\"normalized_airline\":\"UAL\",\"flight_number\":\"1539\",\"seat_assignment\":\"36C\",\"origin\":\"NAS\",\"origin_name\":\"Nassau International\",\"origin_city_name\":\"Nassau\",\"origin_admin_code\":\"23\",\"origin_country\":\"BS\",\"origin_lat\":\"25.038888931274414\",\"origin_lon\":\"-77.46611022949219\",\"destination\":\"EWR\",\"destination_name\":\"Newark Liberty International\",\"destination_city_name\":\"Newark\",\"destination_admin_code\":\"NJ\",\"destination_country\":\"US\",\"destination_lat\":\"40.70000076293945\",\"destination_lon\":\"-74.16666412353516\",\"departure_datetime\":\"2017-11-25T14:35:00\",\"departure_time_zone_id\":\"America\\/Nassau\",\"arrival_datetime\":\"2017-11-25T17:45:00\",\"arrival_time_zone_id\":\"America\\/New_York\",\"confirmation_no\":\"NJ87HQ\",\"phone\":null,\"price\":\"\",\"currency\":null,\"number_of_pax\":5,\"ticket_number\":\"0162358481893\",\"fare_basis_code\":null,\"class_of_service\":\"E\",\"created\":\"2017-11-08T01:06:09+0000\",\"travelers\":[{\"name\":\"MERRIMAN\\/MICHAELJOHN\",\"first_name\":\"MICHAELJOHN\",\"last_name\":\"MERRIMAN\"},{\"name\":\"MERRIMAN\\/TRACEYANN\",\"first_name\":\"TRACEYANN\",\"last_name\":\"MERRIMAN\"},{\"name\":\"MERRIMAN\\/NATALIEMARIE\",\"first_name\":\"NATALIEMARIE\",\"last_name\":\"MERRIMAN\"},{\"name\":\"MERRIMAN\\/SEANMICHAEL\",\"first_name\":\"SEANMICHAEL\",\"last_name\":\"MERRIMAN\"},{\"name\":\"MERRIMAN\\/BRENDANROBERT\",\"first_name\":\"BRENDANROBERT\",\"last_name\":\"MERRIMAN\"}],\"tickets\":[\"0162358481893\"],\"seats\":[\"36C\"],\"price_details\":null,\"metadata\":null},{\"type\":\"Air\",\"status\":\"Active\",\"source\":\"United\",\"first_name\":\"MICHAELJOHN\",\"last_name\":\"MERRIMAN\",\"airline\":\"United Airlines\",\"iata_code\":\"UA\",\"normalized_airline\":\"UAL\",\"flight_number\":\"4048\",\"seat_assignment\":\"8C\",\"origin\":\"EWR\",\"origin_name\":\"Newark Liberty International\",\"origin_city_name\":\"Newark\",\"origin_admin_code\":\"NJ\",\"origin_country\":\"US\",\"origin_lat\":\"40.70000076293945\",\"origin_lon\":\"-74.16666412353516\",\"destination\":\"ROC\",\"destination_name\":\"Greater Rochester International\",\"destination_city_name\":\"Rochester\",\"destination_admin_code\":\"NY\",\"destination_country\":\"US\",\"destination_lat\":\"43.11666488647461\",\"destination_lon\":\"-77.66666412353516\",\"departure_datetime\":\"2017-11-25T21:27:00\",\"departure_time_zone_id\":\"America\\/New_York\",\"arrival_datetime\":\"2017-11-25T22:49:00\",\"arrival_time_zone_id\":\"America\\/New_York\",\"confirmation_no\":\"NJ87HQ\",\"phone\":null,\"price\":\"\",\"currency\":null,\"number_of_pax\":5,\"ticket_number\":\"0162358481893\",\"fare_basis_code\":null,\"class_of_service\":\"E\",\"created\":\"2017-11-08T01:06:09+0000\",\"travelers\":[{\"name\":\"MERRIMAN\\/MICHAELJOHN\",\"first_name\":\"MICHAELJOHN\",\"last_name\":\"MERRIMAN\"},{\"name\":\"MERRIMAN\\/TRACEYANN\",\"first_name\":\"TRACEYANN\",\"last_name\":\"MERRIMAN\"},{\"name\":\"MERRIMAN\\/NATALIEMARIE\",\"first_name\":\"NATALIEMARIE\",\"last_name\":\"MERRIMAN\"},{\"name\":\"MERRIMAN\\/SEANMICHAEL\",\"first_name\":\"SEANMICHAEL\",\"last_name\":\"MERRIMAN\"},{\"name\":\"MERRIMAN\\/BRENDANROBERT\",\"first_name\":\"BRENDANROBERT\",\"last_name\":\"MERRIMAN\"}],\"tickets\":[\"0162358481893\"],\"seats\":[\"8C\"],\"price_details\":null,\"metadata\":null}]}}}\n";
        String s3 = "{\"id\":\"759311638710325305\",\"type\":\"email.updated\",\"created\":\"2017-11-13T15:36:40+00:00\",\"data\":{\"object\":{\"id\":\"759311605154490153\",\"mailbox_id\":\"638966011000394425\",\"mailbox_type\":\"Developer\",\"status\":\"Processed\",\"source\":null,\"user_address\":\"travelumapped@gmail.com\",\"from_address\":\"travelumapped@gmail.com\",\"to_address\":\"import+apidemo@umapped.com\",\"cc_address\":null,\"subject\":\"Traxo Email - Your Nov 14, 2017 Confirmation #83933229\",\"msg_id\":\"CAGy-Q-5ik1eO2iWqY6O8eCGtS4V2o_dgW5Z0vU7qppridJGgkw@mail.gmail.com\",\"msg_date\":\"Mon, 13 Nov 2017 10:36:29 -0500\",\"created\":\"2017-11-13T15:36:37+0000\",\"modified\":\"2017-11-13T15:36:40+0000\",\"metadata\":null,\"includes\":[],\"segments\":[{\"type\":\"Hotel\",\"status\":\"Active\",\"source\":\"Hilton\",\"first_name\":\"David\",\"last_name\":\"Palmieri\",\"hotel_name\":\"Hampton by Hilton\",\"address1\":\"202 Evert van de Beekstraat\",\"address2\":null,\"city_name\":\"Schiphol\",\"admin_code\":\"NH\",\"country\":\"NL\",\"postal_code\":\"1118 CP\",\"lat\":\"52.306499\",\"lon\":\"4.75386\",\"checkin_date\":\"2017-11-14\",\"checkout_date\":\"2017-11-15\",\"time_zone_id\":\"Europe\\/Amsterdam\",\"price\":\"153.4\",\"currency\":\"EUR\",\"number_of_rooms\":\"1\",\"confirmation_no\":\"83933229\",\"phone\":null,\"room_type\":\"QUEEN ROOM NON SMOKING\",\"room_description\":null,\"rate_description\":null,\"cancellation_policy\":null,\"created\":\"2017-11-13T15:36:40+0000\",\"travelers\":[{\"name\":\"David Palmieri\",\"first_name\":\"David\",\"last_name\":\"Palmieri\"}],\"price_details\":[{\"type\":\"total\",\"name\":\"Total\",\"value\":\"153.4\",\"units\":\"EUR\"},{\"type\":\"tax\",\"name\":\"Tax\",\"value\":\"8.75\",\"units\":\"EUR\"}],\"metadata\":null}]}}}";
        try {
            TraxoHelper t = new TraxoHelper();
            t.parseJson(s3);
            ObjectMapper om = new ObjectMapper();
            System.out.println(om.writeValueAsString(t.toReservationPacakage("thierry", "1", "22")));

            for (Activity a : t.activities) {
                System.out.println(t.getDateTime(a.created));
                System.out.println(t.getDateTime(a.start_datetime));
                System.out.printf(a.price_details.get(0).value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<BookingRS> getBookingRS(String userId, String cmpyId, String tripName) {
        ReservationPackage reservationPackage = toReservationPacakage(userId, cmpyId, tripName);
        ReservationsHolder rh = new ReservationsHolder();
        List<BookingRS> bookings = new ArrayList<>();
        if (reservationPackage == null) {
            return bookings;
        }
        ObjectMapper om = new ObjectMapper();
        int srcId = BkApiSrc.getId("EMAIL");
        BookingRS rp = ApiBookingController.getPackageBookingRS(srcId,
                cmpyId,
                userId,
                cmpyId,
                userId,
                reservationPackage,
                startTimestamp,
                endTimestamp);
        try {
            rp.setData(om.writeValueAsString(reservationPackage));
            rp.setHash(Utils.hash(rp.getData()));
            bookings.add(rp);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (Reservation r : reservationPackage.reservation) {
            try {
                BookingRS rs = ApiBookingController.getBaseBookingRS(BkApiSrc.getId("EMAIL"),
                        cmpyId,
                        userId,
                        cmpyId,
                        userId,
                        reservationPackage.reservationNumber,
                        r.reservationStatus);
                rs.setSrcPk(r.reservationId);
                rs.setHash(Utils.hash(rs.getData()));


                if (r instanceof com.mapped.publisher.parse.schemaorg.FlightReservation) {
                    com.mapped.publisher.parse.schemaorg.FlightReservation f = (com.mapped.publisher.parse.schemaorg.FlightReservation) r;
                    if (f.reservationFor != null && f.reservationFor.arrivalTime != null && f.reservationFor.arrivalTime.getTimestamp() != null) {
                        rs.setEndTS(f.reservationFor.arrivalTime.getTimestamp().getTime());
                    }
                    if (f.reservationFor != null && f.reservationFor.departureTime != null && f.reservationFor.departureTime.getTimestamp() != null) {
                        rs.setStartTS(f.reservationFor.departureTime.getTimestamp().getTime());
                    }
                    rs.setSearchWords(rh.getKeywords(f));
                    rs.setBookingType(ReservationType.FLIGHT.ordinal());
                    rs.setData(om.writeValueAsString(f));
                    rs.setMainTraveler(ApiBookingController.getMainTraveler(f));
                } else if (r instanceof com.mapped.publisher.parse.schemaorg.LodgingReservation) {
                    LodgingReservation f = (LodgingReservation) r;
                    if (f.reservationFor != null && f.checkoutDate != null && f.checkoutDate.getTimestamp() != null) {
                        rs.setEndTS(f.checkoutDate.getTimestamp().getTime());
                    }
                    if (f.reservationFor != null && f.checkinDate != null && f.checkinDate.getTimestamp() != null) {
                        rs.setStartTS(f.checkinDate.getTimestamp().getTime());
                    }
                    rs.setSearchWords(rh.getKeywords(f));
                    rs.setBookingType(ReservationType.HOTEL.ordinal());
                    rs.setData(om.writeValueAsString(f));
                    rs.setMainTraveler(ApiBookingController.getMainTraveler(f));
                } else if (r instanceof com.mapped.publisher.parse.schemaorg.RentalCarReservation) {
                    RentalCarReservation f = (RentalCarReservation) r;
                    if (f.dropoffTime != null && f.dropoffTime.getTimestamp() != null) {
                        rs.setEndTS(f.dropoffTime.getTimestamp().getTime());
                    }
                    if (f.pickupTime != null && f.pickupTime.getTimestamp() != null) {
                        rs.setStartTS(f.pickupTime.getTimestamp().getTime());
                    }
                    rs.setSearchWords(rh.getKeywords(f));
                    rs.setBookingType(ReservationType.CAR_RENTAL.ordinal());
                    rs.setData(om.writeValueAsString(f));
                    rs.setMainTraveler(ApiBookingController.getMainTraveler(f));
                } else if (r instanceof com.mapped.publisher.parse.schemaorg.TrainReservation) {
                    TrainReservation f = (TrainReservation) r;
                    if (f.reservationFor != null && f.reservationFor.arrivalTime != null && f.reservationFor.arrivalTime.getTimestamp() != null) {
                        rs.setEndTS(f.reservationFor.arrivalTime.getTimestamp().getTime());
                    }
                    if (f.reservationFor != null && f.reservationFor.departTime != null && f.reservationFor.departTime.getTimestamp() != null) {
                        rs.setStartTS(f.reservationFor.departTime.getTimestamp().getTime());
                    }
                    rs.setSearchWords(rh.getKeywords(f));
                    rs.setBookingType(ReservationType.RAIL.ordinal());
                    rs.setData(om.writeValueAsString(f));
                    rs.setMainTraveler(ApiBookingController.getMainTraveler(f));
                } else if (r instanceof com.mapped.publisher.parse.schemaorg.TransferReservation) {
                    TransferReservation f = (TransferReservation) r;
                    if (f.reservationFor != null && f.reservationFor.dropoffTime != null) {
                        rs.setEndTS(f.reservationFor.dropoffTime.getTimestamp().getTime());
                    }
                    if (f.reservationFor != null && f.reservationFor.pickupTime != null) {
                        rs.setStartTS(f.reservationFor.pickupTime.getTimestamp().getTime());
                    }
                    rs.setSearchWords(rh.getKeywords(f));
                    rs.setBookingType(ReservationType.TRANSPORT.ordinal());
                    rs.setData(om.writeValueAsString(f));
                    rs.setMainTraveler(ApiBookingController.getMainTraveler(f));
                } else if (r instanceof com.mapped.publisher.parse.schemaorg.ActivityReservation) {
                    ActivityReservation f = (ActivityReservation) r;
                    if (f.finishTime != null) {
                        rs.setEndTS(f.finishTime.getTimestamp().getTime());
                    }
                    if (f.startTime != null) {
                        rs.setStartTS(f.startTime.getTimestamp().getTime());
                    }
                    if (f.finishTime != null) {
                        rs.setEndTS(f.finishTime.getTimestamp().getTime());
                    }
                    if (f.startTime != null) {
                        rs.setStartTS(f.startTime.getTimestamp().getTime());
                    }
                    rs.setSearchWords(rh.getKeywords(f));
                    rs.setBookingType(ReservationType.ACTIVITY.ordinal());
                    rs.setSrcPk(f.reservationId);
                    rs.setData(om.writeValueAsString(f));
                    rs.setMainTraveler(ApiBookingController.getMainTraveler(f));
                }
                if (rs.searchWords != null) {
                    rs.setSearchWords(rs.getSearchWords() + " " + tripName);
                }


                bookings.add(rs);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return bookings;
    }

    public DateTime getDateTime(String timestamp) {
        if (timestamp != null) {
            if(!timestamp.contains("T") && timestamp.length() >11) {
                String c = timestamp.substring(0,10);
                String c1 = timestamp.substring(10);
                timestamp = c.trim() + "T" + c1.trim();
            }
            timestamp = timestamp .replace("+0000","");

            DateTime dateTime = null;
            if (!timestamp.endsWith("Z")) {
                timestamp +="Z";
            }

            if (timestamp.contains("T") && timestamp.contains("Z")) {
                dateTime = new DateTime(timestamp);
            } else if (timestamp.contains("Z")) {
                dateTime =  new DateTime(timestamp.replace("Z","T00:00:00Z"));
            }
            long t = 0;
            if (dateTime != null) {
                t = dateTime.getTimestamp().toInstant().toEpochMilli();
            }
            if (startTimestamp == 0 || t < startTimestamp) {
                startTimestamp = t;
            }

            if (endTimestamp == 0 || t > endTimestamp) {
                endTimestamp = t;
            }
            return dateTime;

        }
        return null;
    }
}
