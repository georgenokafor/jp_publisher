package com.mapped.publisher.persistence;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.mapped.persistence.util.DBConnectionMgr;
import com.umapped.api.schema.local.PoiJson;
import com.umapped.api.schema.types.*;
import models.publisher.Company;
import models.publisher.PoiFile;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Version;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


/**
 * Model for Poi table.
 * <p/>
 * Created by surge on 2014-10-14.
 */
public class PoiRS
    implements Cloneable, Serializable {

  public int consortiumId;          //Merge skipped
  public String countryCode;        //Merged
  public float locLat;              //Merged
  public float locLong;             //Merged
  /* Deserialized from JSON */
  public PoiJson data;              //Merged
  public long createdtimestamp;     //Merge skipped
  public long lastupdatedtimestamp; //Merge skipped
  public String createdby;          //Merge skipped
  public String modifiedby;         //Merge skipped
  public RecordState state;         //Merge skipped
  @Version
  public int version;               //Merge skipped

  @JsonSerialize(using=ToStringSerializer.class)
  private long id;                  //Merged
  private int fileCount;            //Merged
  private int cmpyId;               //Merged
  private int typeId;               //Merged
  private String srcReference;      //Merge skipped
  private int srcId;                //Merge skipped
  private String code;              //Merged
  private String tags;              //Merged
  private String name;              //Merged
  private Set<PoiFile> attachments; //Merged

  public PoiRS() {
    data = new PoiJson();
    attachments = new HashSet<>();
  }

  public static PoiRS buildRecord(int consortiumId, int cmpyId, int typeId, int importSrc, String userId) {
    PoiJson pjs = new PoiJson();
    return buildRecord(pjs, consortiumId, cmpyId, typeId, importSrc, userId);
  }

  public static PoiRS buildRecord(PoiJson data,
                                  int consortiumId,
                                  int cmpyId,
                                  int typeId,
                                  int importSrc,
                                  String userId) {
    PoiRS prs = new PoiRS();
    prs.setData(data);
    prs.state = RecordState.ACTIVE;

    if (prs.id == 0) {
      prs.setId(DBConnectionMgr.getUniqueLongId());
    }

    prs.consortiumId = consortiumId;
    prs.setSrcId(importSrc);
    prs.setCmpyId(cmpyId);
    prs.setTypeId(typeId);
    prs.setFileCount(0);

    prs.createdtimestamp = System.currentTimeMillis();
    prs.lastupdatedtimestamp = prs.createdtimestamp;
    prs.createdby = userId;
    prs.modifiedby = prs.createdby;
    return prs;
  }

  public String getSearchWords() {
    StringBuilder sb = new StringBuilder();
    Address a = getMainAddress();
    if (a != null && a.getLocality() != null && a.getLocality().length() > 0) {
      sb.append(a.getLocality());
      sb.append(" ");
    }

    if (tags != null) {
      sb.append(tags);
      sb.append(" ");

    }

    if (srcId != FeedSourcesInfo.byName("Web") && srcId != FeedSourcesInfo.byName("System")) {
      sb.append(FeedSourcesInfo.byId(srcId));
      sb.append(" ");
    }

    if (countryCode != null && !countryCode.equals("UNK")) {
      sb.append(CountriesInfo.Instance().byAlpha3(countryCode).getName(CountriesInfo.LangCode.EN));
    }

    return sb.toString();
  }

  public void setData(PoiJson poi) {
    data = poi;
    name = poi.getName();
    code = poi.getCode();
    tags = poi.getTagsString();
    if (poi.getPoiId() != null) {
      id = poi.getPoiId();
    }
    if (poi.getAddresses() != null) {
      for (Address a : poi.getAddresses()) {
        if (a.getAddressType() == AddressType.MAIN) {
          Coordinates coords = a.getCoordinates();
          if (coords != null && coords.getLatitude() != null && coords.getLongitude() != null) {
            locLat = a.getCoordinates().getLatitude();
            locLong = a.getCoordinates().getLongitude();
          } else if (coords != null) {
            if (coords.getLatitude() == null)
              locLat = 0;
            else
              locLat = coords.getLatitude();

            if (coords.getLongitude() == null)
              locLong = 0;
            else
              locLong = coords.getLongitude();
          } else {
            locLat = 0;
            locLong = 0;
          }
          countryCode = a.getCountryCode();
        }
      }
    }
  }

  public String getTags() {
    return tags;
  }

  public void setTags(String tags) {
    if (tags != null && tags.length() > 0) {
      data.addTags(tags);
      this.tags = data.getTagsString();
    }
  }

  public void setTagsFromData() {
    tags = data.getTagsString();
  }

  public PhoneNumber getMainPhone() {
    PhoneNumber nonMain = null;
    for (PhoneNumber f : data.getPhoneNumbers()) {
      if (f.getPhoneType() == PhoneNumber.PhoneType.MAIN) {
        return f;
      }
      else {
        nonMain = f;
      }
    }

    return nonMain;
  }

  public void clearFeatures() {
    data.getFeatures().clear();
  }

  public void addFeature(Feature f) {
    data.getFeatures().add(f);
  }

  /**
   * Removes any previous MAIN address information and adds new record
   * @param address
   */
  public void setMainAddress(Address address) {
    for (Iterator<Address> it = data.getAddresses().iterator(); it.hasNext();) {
      Address currItem = it.next();
      if (currItem.getAddressType() == AddressType.MAIN) {
        it.remove();
      }
    }
    address.setAddressType(AddressType.MAIN);
    data.addAddress(address);
    if (address.getCoordinates() != null &&
        address.getCoordinates().getLatitude() != null &&
        address.getCoordinates().getLongitude() != null) {
      locLat = address.getCoordinates().getLatitude();
      locLong = address.getCoordinates().getLongitude();
    }
    countryCode = address.getCountryCode();
  }

  public void setMainPhoneNumber(PhoneNumber phoneNumber) {
    for (Iterator<PhoneNumber> it = data.getPhoneNumbers().iterator(); it.hasNext();) {
      PhoneNumber currItem = it.next();
      if (currItem.getPhoneType()== PhoneNumber.PhoneType.MAIN) {
        it.remove();
      }
    }

    phoneNumber.setPhoneType(PhoneNumber.PhoneType.MAIN);
    data.addPhoneNumber(phoneNumber);
  }

  public String getMainUrl() {
    for (String u : data.getUrls()) {
      return u;
    }
    return null;
  }

  public Email getMainEmail() {
    Email nonMain = null;
    for (Email e : data.getEmails()) {
      if (e.getEmailType() == Email.EmailType.MAIN) {
        return e;
      }
      else {
        nonMain = e;
      }
    }
    return nonMain;
  }

  public String getMainAddressString() {
    StringBuffer sb = new StringBuffer();
    Address a = getMainAddress();
    if (a == null) {
      return sb.toString();
    }

    //Street address
    if (a.getStreetAddress() != null && !a.getStreetAddress().isEmpty()) {
      sb.append(a.getStreetAddress());
    }

    if (a.getLocality() != null && a.getLocality().trim().length() > 0) {
      if (sb.length() > 0) {
        sb.append(", ");
      }
      sb.append(a.getLocality());
    }

    //Region name (State/Province/County)
    if (a.getRegion() != null && a.getRegion().trim().length() > 0) {
      if (sb.length() > 0) {
        sb.append(", ");
      }
      sb.append(a.getRegion());
    }


    //zip
    if (a.getPostalCode() != null && a.getPostalCode().trim().length() > 0) {
      if (sb.length() > 0) {
        sb.append(", ");
      }
      sb.append(a.getPostalCode());
    }


    CountriesInfo.Country cntry = CountriesInfo.Instance().byAlpha3(countryCode);
    if (cntry != null && cntry.getAlpha3() != null && !cntry.getAlpha3().equals("UNK")) {
      if (sb.length() > 0) {
        sb.append(", ");
      }
      sb.append(cntry.getName(CountriesInfo.LangCode.EN));
    }

    return sb.toString();
  }

  /**
   * Returns main address for the poi, if main type is not specified returns any other one
   *
   * @return null if no address for poi
   */
  public Address getMainAddress() {
    Address nonMain = null;
    for (Address a : data.getAddresses()) {
      if (a.getAddressType() == AddressType.MAIN) {
        return a;
      }
      else {
        nonMain = a;
      }
    }
    return nonMain;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    if (data != null) {
      data.setPoiId(id);
    }
    this.id = id;
  }

  public Long getIdLong() {
    return id;
  }

  public int getFileCount() {
    return fileCount;
  }

  public void setFileCount(int fileCount) {
    this.fileCount = fileCount;
    if (data != null) {
      data.setFileCount(fileCount);
    }
  }

  public int getCmpyId() {
    return cmpyId;
  }

  public void setCmpyId(int cmpyId) {
    this.cmpyId = cmpyId;
    this.data.setCmpyId(cmpyId);
  }

  public void updateLastUpdatedTS(String userId) {
    modifiedby = userId;
    lastupdatedtimestamp = System.currentTimeMillis();
  }

  public void addAttachments(List<PoiFile> files) {
    attachments.addAll(files);
  }

  public int getTypeId() {
    return typeId;
  }

  public void setTypeId(int typeId) {
    this.typeId = typeId;
    data.setPoiTypeId(typeId);
    data.setPoiTypeName(PoiTypeInfo.Instance().byId(typeId).getName());
  }

  @Override
  public Object clone()
      throws CloneNotSupportedException {
    PoiRS prs = new PoiRS();

    prs.id = id;
    prs.consortiumId = consortiumId;
    prs.cmpyId = cmpyId;
    prs.typeId = typeId;
    prs.countryCode = countryCode;
    prs.locLat = locLat;
    prs.locLong = locLong;
    prs.data = (PoiJson) data.clone();
    prs.createdby = createdby;
    prs.createdtimestamp = createdtimestamp;
    prs.modifiedby = modifiedby;
    prs.lastupdatedtimestamp = lastupdatedtimestamp;
    prs.state = state;
    prs.version = version;
    prs.srcReference = srcReference;
    prs.srcId = srcId;
    prs.code = code;
    prs.name = name;
    prs.fileCount = fileCount;
    prs.tags = tags;
    return prs;
  }

  @Override
  public String toString() {
    String nl = System.getProperty("line.separator");
    StringBuilder sb = new StringBuilder();

    sb.append(nl + "############ POIRS ############" + nl);

    sb.append("Consortium :" + consortiumId + nl);
    sb.append("Company    :" + cmpyId + nl);
    sb.append("Source     :" + srcId + nl);
    sb.append("Source Ref :" + srcReference + nl);
    sb.append("State      :" + state.name() + nl);
    sb.append("Tags       :" + tags);

    sb.append(data.toString());

    sb.append("Files    (" + fileCount + ") : ");
    for (PoiFile f : attachments) {
      sb.append(f.toString());
    }
    sb.append(nl);

    sb.append("@@@@@@@@@@@@ POIRS @@@@@@@@@@@@" + nl);

    return sb.toString();
  }

  public void overwriteWith(PoiRS prs) {
    if (prs == null) {
      return;
    }

    id = prs.id;
    if ((cmpyId == Company.PUBLIC_COMPANY_ID && prs.cmpyId != Company.PUBLIC_COMPANY_ID) ||
        (cmpyId != prs.cmpyId && lastupdatedtimestamp < prs.lastupdatedtimestamp)) {
      cmpyId = prs.cmpyId;
    }

    typeId = prs.typeId;
    countryCode = prs.countryCode;
    //workaround for now to always keep a loc if there is one
    if ( prs.locLat != 0 && prs.locLong != 0) {
      locLat = prs.locLat;
      locLong = prs.locLong;
    }

    state = RecordState.ACTIVE;
    name = prs.name;
    if (prs.code != null) {
      code = prs.code;
    }

    if(prs.tags != null && tags == null) {
      tags = prs.tags;
    } else if (prs.tags != null) {
      String[] prsTags = StringUtils.split(prs.tags, ',');
      String[] lTags = StringUtils.split(prs.tags, ',');
      Set<String> tagSet = new HashSet<>();
      for(String t: prsTags) {
        tagSet.add(t.trim());
      }

      for (String t : lTags) {
        tagSet.add(t.trim());
      }

      tags = StringUtils.join(tagSet, ", ");
    }

    if (data == null) {
      data = new PoiJson();
    }
    data.overwriteWith(prs.data);
    attachments.addAll(prs.attachments);
    if (prs.getCmpyId() != Company.PUBLIC_COMPANY_ID && prs.fileCount > 0) {
      setFileCount(prs.fileCount);
    } else {
      setFileCount(getFileCount() + prs.fileCount);
    }
  }

  public int getSrcId() {
    return srcId;
  }

  public void setSrcId(int srcId) {
    this.srcId = srcId;
  }

  public String getSrcReference() {
    return srcReference;
  }

  public void setSrcReference(String srcReference) {
    this.srcReference = srcReference;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
    data.setCode(code);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
    data.setName(name);
  }
}
