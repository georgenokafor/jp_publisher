
package com.umapped.external.wetu.content;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "general_description",
    "extended_description",
    "contact_information",
    "images",
    "youtube_videos",
    "panoramas",
    "logo"
})
public class Content {

    @JsonProperty("general_description")
    private String generalDescription;
    @JsonProperty("extended_description")
    private String extendedDescription;
    @JsonProperty("contact_information")
    private ContactInformation contactInformation;
    @JsonProperty("images")
    private List<Image> images = null;
    @JsonProperty("youtube_videos")
    private List<YoutubeVideo> youtubeVideos = null;
    @JsonProperty("panoramas")
    private List<Panorama> panoramas = null;
    @JsonProperty("logo")
    private Logo logo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("general_description")
    public String getGeneralDescription() {
        return generalDescription;
    }

    @JsonProperty("general_description")
    public void setGeneralDescription(String generalDescription) {
        this.generalDescription = generalDescription;
    }

    @JsonProperty("extended_description")
    public String getExtendedDescription() {
        return extendedDescription;
    }

    @JsonProperty("extended_description")
    public void setExtendedDescription(String extendedDescription) {
        this.extendedDescription = extendedDescription;
    }

    @JsonProperty("contact_information")
    public ContactInformation getContactInformation() {
        return contactInformation;
    }

    @JsonProperty("contact_information")
    public void setContactInformation(ContactInformation contactInformation) {
        this.contactInformation = contactInformation;
    }

    @JsonProperty("images")
    public List<Image> getImages() {
        return images;
    }

    @JsonProperty("images")
    public void setImages(List<Image> images) {
        this.images = images;
    }

    @JsonProperty("youtube_videos")
    public List<YoutubeVideo> getYoutubeVideos() {
        return youtubeVideos;
    }

    @JsonProperty("youtube_videos")
    public void setYoutubeVideos(List<YoutubeVideo> youtubeVideos) {
        this.youtubeVideos = youtubeVideos;
    }

    @JsonProperty("panoramas")
    public List<Panorama> getPanoramas() {
        return panoramas;
    }

    @JsonProperty("panoramas")
    public void setPanoramas(List<Panorama> panoramas) {
        this.panoramas = panoramas;
    }

    @JsonProperty("logo")
    public Logo getLogo() {
        return logo;
    }

    @JsonProperty("logo")
    public void setLogo(Logo logo) {
        this.logo = logo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("generalDescription", generalDescription).append("extendedDescription", extendedDescription).append("contactInformation", contactInformation).append("images", images).append("youtubeVideos", youtubeVideos).append("panoramas", panoramas).append("logo", logo).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(logo).append(generalDescription).append(contactInformation).append(panoramas).append(additionalProperties).append(images).append(youtubeVideos).append(extendedDescription).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Content) == false) {
            return false;
        }
        Content rhs = ((Content) other);
        return new EqualsBuilder().append(logo, rhs.logo).append(generalDescription, rhs.generalDescription).append(contactInformation, rhs.contactInformation).append(panoramas, rhs.panoramas).append(additionalProperties, rhs.additionalProperties).append(images, rhs.images).append(youtubeVideos, rhs.youtubeVideos).append(extendedDescription, rhs.extendedDescription).isEquals();
    }

}
