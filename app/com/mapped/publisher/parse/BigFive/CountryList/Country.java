
package com.mapped.publisher.parse.BigFive.CountryList;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}country_name"/>
 *         &lt;element ref="{}country_description"/>
 *         &lt;element ref="{}country_currency" minOccurs="0"/>
 *         &lt;element ref="{}country_flight" minOccurs="0"/>
 *         &lt;element ref="{}country_time" minOccurs="0"/>
 *         &lt;element ref="{}country_voltage" minOccurs="0"/>
 *         &lt;element ref="{}country_weather" minOccurs="0"/>
 *         &lt;element ref="{}country_images"/>
 *         &lt;element ref="{}link"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "countryName",
    "countryDescription",
    "countryCurrency",
    "countryFlight",
    "countryTime",
    "countryVoltage",
    "countryWeather",
    "countryImages",
    "link"
})
@XmlRootElement(name = "country")
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
public class Country {

    @XmlElement(name = "country_name", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    protected String countryName;
    @XmlElement(name = "country_description", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    protected String countryDescription;
    @XmlElement(name = "country_currency")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    protected String countryCurrency;
    @XmlElement(name = "country_flight")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    protected String countryFlight;
    @XmlElement(name = "country_time")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    protected String countryTime;
    @XmlElement(name = "country_voltage")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    protected String countryVoltage;
    @XmlElement(name = "country_weather")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    protected String countryWeather;
    @XmlElement(name = "country_images", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    protected CountryImages countryImages;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    protected String link;

    /**
     * Gets the value of the countryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public String getCountryName() {
        return countryName;
    }

    /**
     * Sets the value of the countryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public void setCountryName(String value) {
        this.countryName = value;
    }

    /**
     * Gets the value of the countryDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public String getCountryDescription() {
        return countryDescription;
    }

    /**
     * Sets the value of the countryDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public void setCountryDescription(String value) {
        this.countryDescription = value;
    }

    /**
     * Gets the value of the countryCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public String getCountryCurrency() {
        return countryCurrency;
    }

    /**
     * Sets the value of the countryCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public void setCountryCurrency(String value) {
        this.countryCurrency = value;
    }

    /**
     * Gets the value of the countryFlight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public String getCountryFlight() {
        return countryFlight;
    }

    /**
     * Sets the value of the countryFlight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public void setCountryFlight(String value) {
        this.countryFlight = value;
    }

    /**
     * Gets the value of the countryTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public String getCountryTime() {
        return countryTime;
    }

    /**
     * Sets the value of the countryTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public void setCountryTime(String value) {
        this.countryTime = value;
    }

    /**
     * Gets the value of the countryVoltage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public String getCountryVoltage() {
        return countryVoltage;
    }

    /**
     * Sets the value of the countryVoltage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public void setCountryVoltage(String value) {
        this.countryVoltage = value;
    }

    /**
     * Gets the value of the countryWeather property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public String getCountryWeather() {
        return countryWeather;
    }

    /**
     * Sets the value of the countryWeather property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public void setCountryWeather(String value) {
        this.countryWeather = value;
    }

    /**
     * Gets the value of the countryImages property.
     * 
     * @return
     *     possible object is
     *     {@link CountryImages }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public CountryImages getCountryImages() {
        return countryImages;
    }

    /**
     * Sets the value of the countryImages property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryImages }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public void setCountryImages(CountryImages value) {
        this.countryImages = value;
    }

    /**
     * Gets the value of the link property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public String getLink() {
        return link;
    }

    /**
     * Sets the value of the link property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-09-22T08:07:47-04:00", comments = "JAXB RI v2.2.4-2")
    public void setLink(String value) {
        this.link = value;
    }

}
