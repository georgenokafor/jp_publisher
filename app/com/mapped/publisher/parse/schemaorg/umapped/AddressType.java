package com.mapped.publisher.parse.schemaorg.umapped;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by surge on 2016-02-26.
 */
@Generated("org.jsonschema2pojo")
public enum AddressType {

  MAIN("main"),
  BUSINESS("business"),
  BILLING("billing"),
  MAILING("mailing"),
  HOME("home"),
  ENTRANCE("entrance"),
  PICKUP("pickup"),
  DROPOFF("dropoff");
  private static Map<String, AddressType> constants = new HashMap<String, AddressType>();

  static {
    for (AddressType c : values()) {
      constants.put(c.value, c);
    }
  }

  private final String value;

  private AddressType(String value) {
    this.value = value;
  }

  @JsonCreator
  public static AddressType fromValue(String value) {
    AddressType constant = constants.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    }
    else {
      return constant;
    }
  }

  @JsonValue
  @Override
  public String toString() {
    return this.value;
  }
}
