package models.publisher;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Model;
import com.mapped.publisher.persistence.IUmappedModel;
import com.mapped.publisher.persistence.IUmappedModelWithImage;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-04-09
 * Time: 10:48 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "image_library")
public class ImageLibrary
    extends UmappedModelWithImage
    implements IUmappedModel<String, ImageLibrary> {
  public enum ImgSrc {
    UMAPPED,
    WIKIPEDIA,
    WEB,
    MOBILIZER,
    VIRTUOSO,
    BIG_FIVE,
    WETU;

    public static ImgSrc fromFileSrc(FileSrc.FileSrcType type) {
      switch (type) {
        case IMG_MOBILIZER:
          return MOBILIZER;
        case IMG_VENDOR_WIKIPEDIA:
          return WIKIPEDIA;
        case IMG_USER_DOWNLOAD:
          return WEB;
        case IMG_VENDOR_VIRTUOSO:
          return VIRTUOSO;
        case IMG_VENDOR_BIGFIVE:
          return BIG_FIVE;
        case IMG_VENDOR_WETU:
          return WETU;
        default:
          return UMAPPED;
      }
    }
  }

  public static Model.Finder<String, ImageLibrary> find = new Finder(ImageLibrary.class);
  @Id
  public  String imgid;
  public  String thumburl;
  public  String checksum;
  @Constraints.Required
  public  String s3filename;
  public  String s3url;
  public  String origurl;
  @Column(name = "imgsrc")
  @Enumerated(EnumType.ORDINAL)
  public  ImgSrc imgsrc;
  public  int    status;
  public  Long   createdtimestamp;
  @ManyToOne
  @JoinColumn(name = "image_pk", nullable = true, referencedColumnName = "pk")
  FileImage image;
  private String pageId;
  private String searchTerm;

  public static List<ImageLibrary> findByPageId(String pageId) {
    return find.where().eq("page_id", pageId).findList();
  }

  public static List<ImageLibrary> findAll() {
    return find.all();
  }

  public static List<ImageLibrary> findAllActive() {
    return find.where().eq("status", 0).findList();
  }

  public static ImageLibrary findByS3Url(String s3Url) {
    return find.where().eq("s3url", s3Url).findUnique();
  }

  public static ImageLibrary findByUrlOrName(final String url, final String name) {
    return find.where().or(Expr.eq("s3url", url), Expr.eq("s3filename", name)).setMaxRows(1).findUnique();
  }

  public static ImageLibrary findByPK(String pk) {
    return find.byId(pk);
  }

  public String getPageId() {
    return pageId;
  }

  public void setPageId(String pageId) {
    this.pageId = pageId;
  }

  public String getSearchTerm() {
    return searchTerm;
  }

  public void setSearchTerm(String searchTerm) {
    this.searchTerm = searchTerm;
  }

  public String getThumburl() {
    return thumburl;
  }

  public void setThumburl(String thumburl) {
    this.thumburl = thumburl;
  }

  public String getChecksum() {
    return checksum;
  }

  public void setChecksum(String checksum) {
    this.checksum = checksum;
  }

  public String getImgid() {
    return imgid;
  }

  public void setImgid(String imgid) {
    this.imgid = imgid;
  }

  public String getS3filename() {
    return s3filename;
  }

  public void setS3filename(String s3filename) {
    this.s3filename = s3filename;
  }

  public String getOrigurl() {
    return origurl;
  }

  public void setOrigurl(String origurl) {
    this.origurl = origurl;
  }

  public ImgSrc getImgsrc() {
    return imgsrc;
  }

  public void setImgsrc(ImgSrc imgsrc) {
    this.imgsrc = imgsrc;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  @Override
  public String getImageUrl() {
    if (getFileImage() == null) {
      return getS3url();
    }
    else {
      return getFileImage().getUrl();
    }
  }

  public String getS3url() {
    return s3url;
  }

  public FileImage getImage() {
    return image;
  }

  public ImageLibrary setImage(FileImage image) {
    this.image = image;
    return this;
  }

  public void setS3url(String s3url) {
    this.s3url = s3url;
  }

  @Override
  public FileImage getFileImage() {
    return getImage();
  }

  @Override
  public IUmappedModelWithImage setFileImage(FileImage image) {
    setImage(image);
    return this;
  }

  @Override
  public ImageLibrary getByPk(String pk) {
    return find.byId(pk);
  }

  @Override
  public String getPk() {
    return getImgid();
  }

  @Override
  public String getPkAsString() {
    return getPk().toString();
  }

  @Override
  public Long getCreatedByPk() {
    return Account.find.byId(Account.AID_PUBLISHER).getUid();
  }

  @Override
  public Long getModifiedByPk() {
    return Account.find.byId(Account.AID_PUBLISHER).getUid();
  }

  @Override
  public String getCreatedByLegacy() {
    return "system";
  }

  @Override
  public String getModifiedByLegacy() {
    return "system";
  }

  @Override
  public Long getCreatedEpoch() {
    return getCreatedtimestamp();
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  @Override
  public Timestamp getCreatedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getCreatedtimestamp()));
  }

  @Override
  public Long getModifiedEpoch() {
    return getCreatedtimestamp();
  }

  @Override
  public Timestamp getModifiedTs() {
    return Timestamp.from(Instant.ofEpochMilli(getCreatedtimestamp()));
  }
}
