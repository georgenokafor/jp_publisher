package com.mapped.publisher.form.sharedTrip;

/**
 * Created by surge on 2016-01-04.
 */
public class FormRevokeTripShareFromUser {
  public String inShareUserId;
  public String inOriginPage;

  public boolean isValid() {
    return inShareUserId != null && inShareUserId.length() > 0;
  }
}
