
package com.mapped.publisher.parse.globus;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfTourKeyword complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ArrayOfTourKeyword">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TourKeyword" type="{http://webapi.globusandcosmos.com/TourInformation.xsd}TourKeyword" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfTourKeyword", namespace = "http://webapi.globusandcosmos.com/TourInformation.xsd", propOrder = {
    "tourKeywords"
})
public class ArrayOfTourKeyword {

    @XmlElement(name = "TourKeyword", nillable = true)
    protected ArrayList<TourKeyword> tourKeywords;

    /**
     * Gets the value of the tourKeywords property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tourKeywords property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTourKeywords().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TourKeyword }
     *
     *
     */
    public ArrayList<TourKeyword> getTourKeywords() {
        if (tourKeywords == null) {
            tourKeywords = new ArrayList<TourKeyword>();
        }
        return this.tourKeywords;
    }

}
