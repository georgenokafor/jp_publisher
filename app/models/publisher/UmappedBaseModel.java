package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * Common Umapped base model
 * Created by surge on 2016-11-03.
 */
@MappedSuperclass
public abstract  class UmappedBaseModel
    extends Model {

  @Version
  int version;

  public int getVersion() {
    return version;
  }

  public UmappedBaseModel setVersion(int version) {
    this.version = version;
    return this;
  }
}
