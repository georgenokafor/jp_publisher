package com.mapped.publisher.common;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-14
 * Time: 10:59 AM
 * To change this template use File | Settings | File Templates.
 */
public class APPConstants {
  public final static String BING_KEY = "UPvdnbddhaz6pJ2iRDoN54HIPlt8DyITnDgld0Q+48A";
  public final static String BING_KEY_2 = "971216b2cbf34f48b1134242c1d31e90";

  //Trip state/status machine PENDING->DELETED or PENDING->PUBLISHED
  public final static int STATUS_DELETED = -1;
  public final static int STATUS_ACTIVE = 0;
  public final static int STATUS_PUBLISHED = STATUS_ACTIVE;
  public final static int STATUS_PENDING = 1;
  public final static int STATUS_PUBLISH_PENDING = STATUS_PENDING;

  public final static int STATUS_ACCOUNT_LOCKED = 2;
  public final static int STATUS_COMPLETED = 2;
  public final static int STATUS_FAILED = 3;

  public final static int STATUS_ACCEPTED = 0;
  public final static int STATUS_DECLINED = 1;
  public final static int STATUS_UNAUTHORIZED_CMPY = 10;
  public final static int STATUS_UNAUTHORIZED_USER = 11;
  public final static int STATUS_VALIDATION_FAILED = 12;
  public final static int STATUS__SYSTEM_FAILED = 13;
  public final static String TRUE = "yes";

  public final static String MAIN_ADDRESS = "1";
  public final static int STATUS_PUBLISHED_REVIEW = 1;

  public final static int COUNTRY_DESTINATION_TYPE = 1;
  public final static int CITY_DESTINATION_TYPE = 2;
  public final static int TOUR_DESTINATION_TYPE = 3;
  public final static int PLACE_DESTINATION_TYPE = 4;
  public final static int ACTIVITY_DESTINATION_TYPE = 5;
  public final static int GENERAL_DESTINATION_TYPE = 8;
  public final static int CMPY_INFO_DESTINATION_TYPE = 9;

  public final static int TRIP_TYPE = 0;
  public final static int TOUR_TYPE = 1;
  public final static int PRIVATE_VISIBILITY = 0;
  public final static int PUBLIC_VISIBILITY = 1;
  public final static String NOT_APPLICABLE = "N/A";
  public final static int TRIP_DOC_DESTINATION_TYPE = 6;
  public final static int TOUR_DOC_DESTINATION_TYPE = 7;

  public final static String DEFAULT_PROVIDER_ID = "1";
  public final static String DEFAULT_CMPY_ID = "0";

  public final static String PAGE_BOOKING_TYPE = "1";
  public final static String PAGE_DAY_TYPE = "2";
  public final static String PAGE_GUIDE_TYPE = "3";

  public final static String MAP_TRIP_TYPE = "1";
  public final static String MAP_PREF_TRAVEL_CMPYID = "CmpyId";
  public final static String MAP_PREF_TRAVEL_CMPY = "Cmpy";
  public final static String MAP_PREF_TRAVEL_AGENTID = "AgentId";
  public final static String MAP_PREF_TRAVEL_AGENT = "Agent";
  public final static String MAP_PREF_TRAVEL_LOGO = "LogoId";
  public final static String MAP_PREF_TRAVEL_COVER = "Cover";
  public final static String MAP_PREF_TRAVEL_AGENT_PHONE = "APhone";
  public final static String MAP_PREF_TRAVEL_AGENT_EMAIL = "AEmail";
  public final static String MAP_PREF_TRAVEL_TRAVEL_WEB = "AWeb";
  public final static String MAP_PREF_TRAVEL_COBRANDING = "CoBranding";

  public final static String STATUS_PUBLISHED_REVIEW_DESC = "Draft sent for review";
  public final static String STATUS_PUBLISHED_DESC = "Final copy published";

  public final static int STATUS_INVITE_PENDING = 3;
  public final static int STATUS_INVITE_SENT = 4;
  public final static int STATUS_INVITE_ACCEPTED = 5;
  public final static int STATUS_NOTIFICATION_PENDING = 6;
  public final static int STATUS_NOTIFICATION_SENT = 7;
  public final static int STATUS_INVITE_REMINDER = 8;
  public final static int STATUS_INVITE_WELCOME = 9;

  public final static String SHARED_TYPE_OWNER = "3";

  public final static String SHARE_TYPE_READONLY = "2";
  public final static String SHARE_TYPE_TRIP_READONLY = "4";

  public final static String MP_HOTEL = "Hotel";
  public final static String MP_RESTAURANT = "Restaurant";
  public final static String MP_NIGHTLIFE = "Nightlife";
  public final static String MP_OTHER = "Other";
  public final static String MP_THINGSTODO = "Things To Do";
  public final static String MP_THINGSTOSEE = "Things To See";
  public final static String MP_SHOPPING = "Shopping";
  public final static String MP_HAPPENING = "Happening";

  public final static String MP_HOTEL_TEXT = "Hotel";
  public final static String MP_RESTAURANT_TEXT = "Food & Drinks";
  public final static String MP_OTHER_TEXT = "Other";
  public final static String MP_THINGSTODO_TEXT = "Things To Do";
  public final static String MP_THINGSTOSEE_TEXT = "Sights";
  public final static String MP_SHOPPING_TEXT = "Shopping";
  public final static String MP_HAPPENING_TEXT = "Goings On";

  public final static String WEB_TYPE = "Web";
  public final static String WEB_IMG_TYPE = "WebImg";
  public final static String DOC_TYPE = "Doc";
  public final static String AUDIO_TYPE = "Audio";
  public final static String IMAGE_TYPE = "Image";
  public final static String Video_TYPE = "Video";
  public final static String MP_PAGE_TYPE = "PAGE";

  public final static int LINK_TYPE_OWNER = 1;
  public final static int LINK_TYPE_ASSOCIATED = 2;

  public final static String CMPY_TYPE_TRAVEL = "1";
  public final static String CMPY_TYPE_DESTINATION = "2";
  public final static String CMPY_TYPE_TRIAL = "3";

  public final static int CMPY_MAX_NUM_ACCOUNTS = 100;
  public final static int CMPY_MAX_NUM_TRIPS = -1;

  public final static int CMPY_DEFAULT_NUM_ACCOUNTS = 5;
  public final static int CMPY_DEFAULT_NUM_TRIPS = 10;


  public final static int USER_ACCOUNT_TYPE_UMAPPED = 0;
  public final static int USER_ACCOUNT_TYPE_REGULAR = 1;

  public final static int PWD_RESET_EXPIRY = 4 * 60 * 60 * 1000; //4 hours
  public final static int PWD_SETUP_EXPIRY = 14 * 24 * 60 * 60 * 1000; //14 days

  public final static int SESSION_EXPIRY              = 4 * 60 * 60 * 1000; //4 hour
  public final static int SESSION_EXPIRY_SECS         = 4 * 60 * 60; //4 hour
  public final static int CACHE_EXPIRY_SECS           = 1 * 60 * 60; //1 hour
  public final static int CACHE_EXPIRY_T42_COOKIE     = 24 * 60 * 60; //For now 1 day
  public final static int CACHE_TRANSIENT_EXPIRY_SECS = 10 * 60; //10 mins
  public final static int CACHE_FRESHDESK_EXPIRY_SECS = 10 * 60; // 600 seconds => 10 min
  public final static int CACHE_MOBILIZER_EXPIRY_SECS = 10 * 60; //10 minutes is enough
  public final static int CACHE_TRIP_ANALYSIS_EXPIRY_SECS = 1 * 60; //1 minutes is enough

  public final static int CACHE_CLASSIC_VACATIONS_SECS = 6 * 24 * 60 * 60; //6 days


  //namespace for cache objects
  public final static String CACHE_CLASSIC_VACATIONS_AUTH_PREFIX = "CLASSICVACATIONS.AUTH.";
  public final static String CACHE_CLASSIC_VACATIONS_BOOKING_PREFIX = "CLASSICVACATIONS.BOOKING.";

  public final static String CACHE_USER_VIEW_PREFIX = "USERVIEW.";
  public final static String CACHE_EVENT_VIEW_PREFIX = "EVENTVIEW.";
  public final static String CACHE_DEST_VIEW_PREFIX = "DESTVIEW.";
  public final static String CACHE_MOBILIZED_DOMAINS = "MOBDOMAIN.";
  public final static String CACHE_T42_COOKIES = "T42.COOKIE.";
  public final static String CACHE_T42_REPORT_USER = "T42.REPORTS.";
  public final static String CACHE_REC_LOCATOR_WEB = "REC_LOCATOR_WEB.";
  public final static String CACHE_WETU_ITINERARY = "WETU_ITIN.";

  public final static String CACHE_FRESHDESK_GETTING_STARTED = "FRESHDESK.GS.";
  public final static String CACHE_DEST_VIEW_PDF_PREFIX = "DESTVIEWPDF.";
  public final static String CACHE_CMPY_VIEW_PREFIX = "CMPYVIEW.";
  public final static String CACHE_SESSION_PREFIX = "SESSION.";
  public final static String CACHE_USER_API_PARSERS = "USERAPIPARSERS.";
  public final static String CACHE_WEB_ITINERARY_PREFX = "WEB_ITINERARY.";

  public final static String CACHE_SYS_ACTIVE_PROCESSES = "SYS_ACTIVE_PROCESSES";
  public final static String CACHE_SYS_HEARTBEAT_PREFX = "SYS_HEARTBEAT.";
  public final static String CACHE_SYS_EXCEPTION_PREFX = "SYS_EXCEPTION.";
  public final static String CACHE_SYS_STATUS_PREFX = "SYS_STATUS.";

  public final static String CACHE_TRIP_PDF = "TRIPPDF.";
  public final static String CACHE_DOC_PDF = "DOCPDF.";
  public final static String CACHE_POI_FEED_STATE = "POIFEED.";
  public final static String CACHE_BILLING_STATE = "BILLING.";
  public final static String CACHE_MOBILIZER_STATE = "MOBILIZER.";
  public final static int CACHE_PDF_EXPIRY_SECS = 5 * 60; //10 mins

  public final static String CACHE_PUB_ACTOR = "PUB_ACTOR.";
  public final static String CACHE_TRIP_PUB_ACTOR = "TRIP_PUB_ACTOR.";

  public final static String CACHE_TRIP_PREFS = "TRIP_PREFS.";
  public final static String CACHE_ACCOUNT_PREFS = "ACT_PREFS.";
  public final static String CACHE_PREFS_CHECKED = "PREFS_CHECKED.";

  public final static String CACHE_SHORE_TRIP_PREFIX = "SHORETRIP.";

  public final static String CACHE_API_CMPY_TOKEN_PREFIX = "API_CMPY_TOKEN."; //prefix + token id
  public final static String CACHE_API_USER_LINK_PREFIX = "API_USER_LINK."; //prefix + cmpy token id + external userid

  public final static String CACHE_EMAIL_RAW_PREFIX = "EMAIL_RAW."; //prefix + Email Parse Log Primary Key
  public final static int    CACHE_EMAIL_RAW_EXPIRY_SEC = 5*60; //10 minutes

  public final static String CACHE_TRIP_CAP_PREFIX = "TRIP_CAP.";

  public final static String CACHE_DIGEST_ACCOUNT_LIST_KEY = "DIGEST_ACCOUNTS.";
  public final static String CACHE_DIGEST_MSGS_BY_ACCOUNT_PREFIX = "ACCOUNT_MSG_LIST.";

  public final static String CACHE_TRIP_OFFER = "TRIP_OFFER.";

  public final static String CACHE_GOGO_USERID = "GOGO.USERID.";
  public final static String CACHE_TRIP_ANALYSIS_TRIPID = "TRIP_ANALYSIS.TRIPID.";

  public final static String CACHE_AFAR_LIST_SELECTIONS = "AFAR_LIST_FOR_USER.";

  public final static String CACHE_CLIENTBASE_BOOKINGS_GROUP = "CLIENTBASE_BOOKINGS_GROUP_FOR_USER.";



  public final static String MP_PLACEHOLDER_NOTE = "<Enter Description>";
  public final static String MP_PLACEHOLDER_TAG = "<Enter a Tag e.g a neighbourhood, etc>";
  public final static String MP_PLACEHOLDER_TAG1 = "<Enter a Tag>";

  public final static int USER_AUDIT_LOGON = 0;
  public final static int USER_AUDIT_PASSWORD_RESET_REQ = 1;
  public final static int USER_AUDIT_PASSWORD_CHANGED = 2;
  public final static int USER_AUDIT_LOGON_FAILED = 3;
  public final static int USER_AUDIT_PASSWORD_RESET_FAILED = 4;
  public final static int USER_AUDIT_PASSWORD_RESET = 5;
  public final static int USER_AUDIT_PASSWORD_CHANGED_FAILED = 6;
  public final static int USER_AUDIT_LOGON_LOCKED = 7;
  public final static int USER_AUDIT_PASSWORD_RESET_ADMIN = 8;
  public final static int USER_AUDIT_LOGON_SUBSCRIPTION = 9;

  public final static int TRIP_ATTACH_FILE = 0;

  public final static String PDF_END_TOKEN = "PDF_END_TOKEN";
  public final static String PDF_SAVE_COVER = "PDF_SAVE_COVER";

  public final static String WORD_START_TOKEN = "WORD_START_TOKEN";
  public final static String WORD_END_TOKEN_1 = "WORD_END_TOKEN_1";
  public final static String WORD_END_TOKEN_2 = "WORD_END_TOKEN_2";
  public final static String DOC_PARSE_ACTIVITIES = "DOC_PARSE_ACTIVITIES";

  public final static String DOC_LINE_TOKEN = "DOC_LINE_TOKEN";
  public final static String DOC_MORNING_TOKEN = "DOC_MORNING_TOKEN";
  public final static String DOC_AFTERNOON_TOKEN = "DOC_AFTERNOON_TOKEN";

  public final static int SEARCH_MY_TRIPS = 0;
  public final static int SEARCH_MY_PENDING_TRIPS = 1;
  public final static int SEARCH_OTHER_TRIPS = 2;
  public final static int SEARCH_OTHER_PENDING_TRIPS = 3;
  public final static int MAX_RESULT_COUNT = 15; //Changed to 15 from 50 on 2014-05-28 by Serguei
  public final static int MAX_AUDIT_RESULT_COUNT = 5; //Changed to 15 from 50 on 2014-05-28 by Serguei

  public final static int ALLOW_SELF_REGISTRATION = 1;
  public final static int PREVENT_SELF_REGISTRATION = 0;

  public final static String TRIP_GROUP_SEPARATOR = "___";

  public final static int TAB_ADDITIONAL_DOC = 1;
  public final static int TAB_UPLOADED_FILE = 2;
  public final static int TAB_TRIP_DOCUMENT = 3;

  public final static String MSG_FILE_UPLOAD_SUCCESS = "File uploaded successfully";

  public final static int CUSTOM_TEMPLATE_DOC_PDF = 0;
  public final static int CUSTOM_TEMPLATE_BOOKING_PDF = 1;
  public final static int CUSTOM_TEMPLATE_WEB_ITINERARY_LOGIN = 2;
  public final static int CUSTOM_TEMPLATE_WEB_ITINERARY_MAIN = 3;
  public final static int CUSTOM_TEMPLATE_EMAIL_TRIP_PASSENGER = 4;
  public final static int CUSTOM_TEMPLATE_EMAIL_TRIP_AGENCY = 5;

  public final static int RECOMMENDATION_TYPE_UNDEFINED = -1;
  public final static int RECOMMENDATION_TYPE_HOTEL = 1;
  public final static int RECOMMENDATION_TYPE_FOOD = 2;
  public final static int RECOMMENDATION_TYPE_SIGHTS = 3;
  public final static int RECOMMENDATION_TYPE_GOINGS_ON = 4;
  public final static int RECOMMENDATION_TYPE_SHOPPING = 5;
  public final static int RECOMMENDATION_TYPE_OTHER = 6;

  public final static String RECOMMENDATION_TYPE_HOTEL_TAG = "Hotel";
  public final static String RECOMMENDATION_TYPE_FOOD_TAG = "Restaurant";
  public final static String RECOMMENDATION_TYPE_SIGHTS_TAG = "Things To See";
  public final static String RECOMMENDATION_TYPE_GOINGS_ON_TAG = "Happening";
  public final static String RECOMMENDATION_TYPE_SHOPPING_TAG = "Shopping";
  public final static String RECOMMENDATION_TYPE_OTHER_TAG = "Other";

  public final static String WIKIMEDIA_URL = "commons.wikimedia.org";
  public final static int ADD_TRIP_COVER = 1;
  public final static int ADD_DOC_COVER = 2;
  public final static int ADD_PAGE_IMG = 3;
  public final static int ADD_VENDOR_IMG = 4;
  public final static int ADD_TEMPLATE_COVER = 5;
  public final static int ADD_NOTE_IMG = 6;

  public final static String BAGGAGE_URL = "BaggageUrl";
  public final static String CHECKIN_URL = "CheckinUrl";


  public final static String MP_UNTAGGED_PAGE = "Untagged";
  public final static Long PDF_TIMEOUT = (long)5 * 60 * 1000;

  public final static String WHITE_LABEL_PRO_TRAVEL = "ProTrip App: Protravel International + ICs";
  public final static String WHITE_LABEL_TZELL = "TzellTrip App: Tzell Travel Group + ICs";

  public final static String CMPY_TAG_API_AUTOPUBLISH = "API-AUTO-PUBLISH"; //used at a cmpy or api token level
  public final static String CMPY_TAG_API_NO_POI_MATCH = "API-NO-POI-MATCH"; //used at a cmpy level
  public final static String CMPY_TAG_API_AUTO_TRIP_COVER = "API-AUTO-TRIP-COVER"; //used at a cmpy level

  public final static String CMPY_TAG_API_AUTOPUBLISH_NO_EMAIL = "API-AUTO-PUBLISH-NO-EMAIL"; //used at a cmpy or api token level
  public final static String CMPY_TAG_API_WEB_ITINERARY_AUTO_LOGIN = "API-WEB-ITINERARY-AUTO-LOGIN"; //used at a cmpy level
  public final static String CMPY_TAG_API_NO_MOBILE_LINKS = "API-NO-MOBILE-LINKS"; //used at a cmpy level
  public final static String CMPY_TAG_API_NOTIFY_AGENT_PUBLISH = "API-NOTIFY-AGENT-PUBLISH-EVENT"; //used at a cmpy level
  public final static String CMPY_TAG_API_NOTIFY_AGENT_REPUBLISH = "API-NOTIFY-AGENT-REPUBLISH-EVENT"; //used at a cmpy level
  public final static String CMPY_TAG_API_ATTACH_ITINERARY_PDF_EMAIL = "API-ATTACH-PDF-IN-EMAIL"; //used at a cmpy or api token level
  public final static String CMPY_TAG_API_CONTROL_TRAVELERS = "API-CONTROL-TRAVELERS"; //travelers can only be added and removed from API
  public final static String CMPY_TAG_API_UPDATE_TRIP_NAME = "API-UPDATE-TRIP-NAME"; //update trip name from API
  public final static String CMPY_TAG_API_NOTIFY_AGENT_PUBLISH_ERROR = "API-NOTIFY-AGENT-PUBLISH-ERROR";

  public final static String CMPY_TAG_API_FULL_RESERVATION_PACKAGE = "API_FULL_RESERVATION_PACKAGE"; //used at a cmpy or api token level

  public final static String CMPY_TAG_SUMMARY_NOTE = "API-SUMMARY-NOTE"; //used at a cmpy or api token level
  public final static String CMPY_TAG_SUMMARY_NOTE_1 = "API-SUMMARY-NOTE-1"; //used at a cmpy or api token level

  public final static String CMPY_TAG_AFAR_GUIDE = "API-AUTO-AFAR-GUIDE"; //used at a cmpy or api token level
  public final static String CMPY_TAG_WCITIES = "API-AUTO-WCITIES"; //used at a cmpy or api token level
  public final static String CMPY_TAG_DEFAULT_PHOTO = "DEFAULT-PHOTO"; //used at a cmpy or api token level


  public final static String CMPY_AUTOPUBLISH_DEFAULT_USER = "default";

  public final static String NOTE_TAG_LAST_ITEM = "LAST-ITEM";
  public final static String TAG_PAGE_BREAK_BEFORE = "page break before";
  public final static String TAG_PAGE_BREAK_AFTER = "page break after";

  public final static String AFAR_NOTE_TAG = "Afar";
  public final static String WCITIES_NOTE_TAG = "Wcities";

  public final static String TRIP_TAG_CROSS_DATE_FLIGHT = "TAG_CROSS_DATE_FLIGHT";
  public final static String TRIP_TAG_DISABLE_FLIGHT_NOTIFICATION = "DISABLE-FLIGHT-NOTIFICATION";

  public final static String CMPY_TAG_WCITIES_FILTER_FLIGHTCENTER = "FC-WCITIES-FILTER";

  public final static String CMPY_TAG_ITINERARY_GUEST_LOGIN = "ITINERARY-GUEST-LOGIN";
  public final static String CMPY_TAG_FILTER_MANGNATECH_NOTE = "FILTER-MANGNATECH-NOTE";

  public final static int INSERT_AFAR_NOTE_ERROR = -1;
  public final static int UPDATE_AFAR_NOTE_SUCCESS = 1;
  public final static int INSERT_AFAR_NOTE_SUCCESS = 0;

  public final static int PROCESS_WCITIES_NOTE_ERROR = -1;
  public final static int PROCESS_WCITIES_NOTE_SUCCESS = 0;


}
