package com.umapped.api.schema.types;

/**
 * All error codes returned by JSON return codes.
 *
 * Please add all new error conditions to the end of the list to preserve error numbering.
 * Created by surge on 2014-09-24.
 */
public enum ReturnCode {
  /** 0 - Success */
  SUCCESS(0, SeverityLevel.DEBUG, "Success"),

  /* 1-99 SERIES ERRORS - SYSTEM ERRORS */
  NOT_IMPLEMENTED(1, SeverityLevel.INFO, "Requested feature is not implemented"),
  NOT_SUPPORTED(2, SeverityLevel.ERROR, "Operation is not supported"),
  HTTP_REQ_FAIL(3, SeverityLevel.ERROR, "Error in submitted data"),
  HTTP_REQ_MISSING_DATA(4, SeverityLevel.ERROR, "Not all required data was submitted"),
  HTTP_REQ_WRONG_DATA(5, SeverityLevel.ERROR, "Request contains wrong data"),
  LOGICAL_ERROR(6, SeverityLevel.SEVERE, "System error. Please try again later."),
  UNHANDLED_EXCEPTION(7, SeverityLevel.CRITICAL, "System error. Please try again later."),
  STORAGE_S3_SIGN(8, SeverityLevel.CRITICAL, "Storage subsystem error. Please contact Umapped."),
  STORAGE_S3_UPLOAD(9, SeverityLevel.ERROR, "Storage subsystem error. Please contact Umapped."),
  RESOURCE_LIMIT_REACHED(10, SeverityLevel.ERROR, "Resource limit reached."),

  /* 100 ERROR SERIES - AUTHENTICATION and PERMISSIONS ERRORS */
  AUTH_TRIP_FAIL(100, SeverityLevel.WARNING, "Unauthorized to Access the trip"),
  AUTH_TRIP_EDIT_FAIL(101, SeverityLevel.WARNING, "Unauthorized to Edit the trip"),
  AUTH_TRIP_DEL_FAIL(102, SeverityLevel.WARNING, "Unauthorized to Delete the trip"),
  AUTH_BOOKING_FAIL(103, SeverityLevel.WARNING, "Unauthorized to access the booking"),
  AUTH_BOOKING_EDIT_FAIL(104, SeverityLevel.WARNING, "Unauthorized to Edit the booking"),
  AUTH_BOOKING_DEL_FAIL(105, SeverityLevel.WARNING, "Unauthorized to Delete the booking"),
  AUTH_TEMPLATE_FAIL(106, SeverityLevel.WARNING, "Unauthorized Access to the template"),
  AUTH_VENDOR_FAIL(107, SeverityLevel.WARNING, "Unauthorized Access to the vendor"),
  AUTH_DOCUMENT_FAIL(108, SeverityLevel.WARNING, "Unauthorized Access to the document"),
  AUTH_ADMIN_FAIL(109, SeverityLevel.WARNING, "Unauthorized Admin Access"),
  AUTH_ACCOUNT_EDIT_FAIL(110, SeverityLevel.WARNING, "Unauthorized Account Access"),
  AUTH_ACCOUNT_PWD_FAIL(111, SeverityLevel.WARNING, "Authentication Failed"),
  AUTH_ACCOUNT_PWD_NOT_SET(112, SeverityLevel.WARNING, "Account Password Not Configured"),
  AUTH_ACCOUNT_PWD_EXPIRED(113, SeverityLevel.WARNING, "Account Password Expired"),
  AUTH_ACCOUNT_SESSION_EXPIRED(114, SeverityLevel.WARNING, "Logged-in Session Expired"),

  /**
   * Used when request does not contain "Authorization" header
   */
  AUTH_ACCOUNT_FAIL(114, SeverityLevel.INFO, "Account Authentication Failed"),

  /* 200 ERROR SERIES - DATABASE / CACHE / PERSISTENCE ERRORS */
  DB_RECORD_NOT_FOUND(200, SeverityLevel.ERROR, "Requested record is not found"),
  DB_TRANSACTION_FAIL(201, SeverityLevel.SEVERE, "System error. Please try again later."),
  DB_DELETE_FAIL_CASCADE(202, SeverityLevel.ERROR, "Error cannot delete this vendor - it is being used in trips/templates"),
  DB_DELETE_FAIL_IN_USE(203, SeverityLevel.ERROR, "This Vendor cannot be deleted - it is being used in trips/templates"),
  DB_BILLING_PLAN_NOT_FOUND(204, SeverityLevel.ERROR, "Your billing settings are not configured properly - please contact support@umapped.com")  ;


  private String userMsg;
  private SeverityLevel severityLevel;
  private int idx;

  ReturnCode(int idx, SeverityLevel level, String userMsg) {
    this.idx = idx;
    this.severityLevel = level;
    this.userMsg = userMsg;
  }

  public String msg() {
    return userMsg;
  }

  public SeverityLevel level() {
    return severityLevel;
  }

  public int getIdx() {
    return this.idx;
  }
}
