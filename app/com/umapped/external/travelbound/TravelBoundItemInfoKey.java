package com.umapped.external.travelbound;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * Created by wei on 2017-11-16.
 */
public class TravelBoundItemInfoKey implements Serializable {
  private String cityCode;
  private String itemCode;

  public TravelBoundItemInfoKey() {

  }

  public TravelBoundItemInfoKey(String cityCode, String itemCode) {
    this.cityCode = cityCode;
    this.itemCode = itemCode;
  }

  public String getCityCode() {
    return cityCode;
  }

  public void setCityCode(String cityCode) {
    this.cityCode = cityCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  @Override public int hashCode() {
    return (cityCode != null ? cityCode.hashCode()  : 0 ) +
           (itemCode != null ? itemCode.hashCode() : 0);
  }

  @Override public boolean equals(Object obj) {
    if (obj != null && obj instanceof TravelBoundItemInfoKey) {
      TravelBoundItemInfoKey other = (TravelBoundItemInfoKey) obj;
      return (StringUtils.equals(cityCode, other.cityCode) && StringUtils.equals(itemCode, other.itemCode));
    }
    return false;
  }
}
