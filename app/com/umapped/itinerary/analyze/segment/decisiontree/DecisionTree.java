package com.umapped.itinerary.analyze.segment.decisiontree;

import com.umapped.itinerary.analyze.segment.AnalyzerContext;

import java.util.List;

/**
 * Created by wei on 2017-03-10.
 */
public interface DecisionTree {
  List<DecisionResult> decide(AnalyzerContext context);
}
