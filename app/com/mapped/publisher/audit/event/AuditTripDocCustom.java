package com.mapped.publisher.audit.event;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Created by Serguei Moutovkin on 2014-05-21.
 */
@JsonTypeName("TRIP_DOC_CUSTOM")
public class AuditTripDocCustom
    extends AuditEvent {

  //In existing system guides are called "destination"
  public String guideId;
  public String name;
  public String coverImageFileName = "";

  public AuditTripDocCustom withGuideId(String guideId) {
    this.guideId = guideId;
    return this;
  }

  public AuditTripDocCustom withName(String name) {
    this.name = name;
    return this;
  }

  public AuditTripDocCustom withCoverImage(String coverName){
    this.coverImageFileName = coverName;
    return this;
  }

}
