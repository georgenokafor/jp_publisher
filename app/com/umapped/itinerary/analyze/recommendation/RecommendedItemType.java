package com.umapped.itinerary.analyze.recommendation;

import java.io.Serializable;

/**
 * Created by wei on 2017-06-09.
 */
public enum RecommendedItemType implements Serializable {
  Accommodation,
  Activity,
  Transfer,
  Content,
  ContentHotel,
  ContentRestaurant,
  ContentAttraction,
  ContentNightlife,
  ContentShopping,
  Events,
}
