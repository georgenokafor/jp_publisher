
package com.umapped.external.wetu.pins.list;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "map_object_id",
    "name",
    "last_modified",
    "type",
    "category",
    "position",
    "content",
    "documentation",
    "specials"
})
public class Pin {

    @JsonProperty("map_object_id")
    private int mapObjectId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("last_modified")
    private String lastModified;
    @JsonProperty("type")
    private String type;
    @JsonProperty("category")
    private String category;
    @JsonProperty("position")
    private Position position;
    @JsonProperty("content")
    private Content content;
    @JsonProperty("documentation")
    private List<Documentation> documentation = new ArrayList<Documentation>();
    @JsonProperty("specials")
    private List<Specials> specials = new ArrayList<Specials>();

    private String status;
    @JsonProperty("subcategory")
    private String subCategory;
    private String affiliateName;
    private String affiliateCode;
    private String affiliateInterface;




    /**
     * 
     * @return
     *     The mapObjectId
     */
    @JsonProperty("map_object_id")
    public int getMapObjectId() {
        return mapObjectId;
    }

    /**
     * 
     * @param mapObjectId
     *     The map_object_id
     */
    @JsonProperty("map_object_id")
    public void setMapObjectId(int mapObjectId) {
        this.mapObjectId = mapObjectId;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The lastModified
     */
    @JsonProperty("last_modified")
    public String getLastModified() {
        return lastModified;
    }

    /**
     * 
     * @param lastModified
     *     The last_modified
     */
    @JsonProperty("last_modified")
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * 
     * @return
     *     The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The category
     */
    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    /**
     * 
     * @param category
     *     The category
     */
    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * 
     * @return
     *     The position
     */
    @JsonProperty("position")
    public Position getPosition() {
        return position;
    }

    /**
     * 
     * @param position
     *     The position
     */
    @JsonProperty("position")
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     * 
     * @return
     *     The content
     */
    @JsonProperty("content")
    public Content getContent() {
        return content;
    }

    /**
     * 
     * @param content
     *     The content
     */
    @JsonProperty("content")
    public void setContent(Content content) {
        this.content = content;
    }

    /**
     * 
     * @return
     *     The documentation
     */
    @JsonProperty("documentation")
    public List<Documentation> getDocumentation() {
        return documentation;
    }

    /**
     * 
     * @param documentation
     *     The documentation
     */
    @JsonProperty("documentation")
    public void setDocumentation(List<Documentation> documentation) {
        this.documentation = documentation;
    }

    /**
     * 
     * @return
     *     The specials
     */
    @JsonProperty("specials")
    public List<Specials> getSpecials() {
        return specials;
    }

    /**
     * 
     * @param specials
     *     The specials
     */
    @JsonProperty("specials")
    public void setSpecials(List<Specials> specials) {
        this.specials = specials;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("subcategory")
    public String getSubCategory() {
        return subCategory;
    }

    @JsonProperty("subcategory")
    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getAffiliateName() {
        return affiliateName;
    }

    public void setAffiliateName(String affiliateName) {
        this.affiliateName = affiliateName;
    }

    public String getAffiliateCode() {
        return affiliateCode;
    }

    public void setAffiliateCode(String affiliateCode) {
        this.affiliateCode = affiliateCode;
    }

    public String getAffiliateInterface() {
        return affiliateInterface;
    }

    public void setAffiliateInterface(String affiliateInterface) {
        this.affiliateInterface = affiliateInterface;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(mapObjectId).append(name).append(lastModified).append(type).append(category).append(position).append(content).append(documentation).append(specials).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Pin) == false) {
            return false;
        }
        Pin rhs = ((Pin) other);
        return new EqualsBuilder().append(mapObjectId, rhs.mapObjectId).append(name, rhs.name).append(lastModified, rhs.lastModified).append(type, rhs.type).append(category, rhs.category).append(position, rhs.position).append(content, rhs.content).append(documentation, rhs.documentation).append(specials, rhs.specials).isEquals();
    }

}
