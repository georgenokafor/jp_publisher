package controllers;

import com.google.inject.Inject;
import com.mapped.common.CoreConstants;
import com.mapped.common.EmailMgr;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.form.WebContactForm;
import com.mapped.publisher.view.WebContactView;
import models.publisher.WebContact;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-06-17
 * Time: 12:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class WebContactController
    extends Controller {

  @Inject
  FormFactory formFactory;

  public Result newContact() {
    Form<WebContactForm> webForm = formFactory.form(WebContactForm.class);
    WebContactForm webInfoForm = webForm.bindFromRequest().get();

    WebContactView view = new WebContactView();
    if (webInfoForm.getInName() != null &&
        webInfoForm.getInName().trim().length() > 0 && webInfoForm.getInPhone() != null &&
        webInfoForm.getInPhone().trim().length() > 0 && webInfoForm.getInEmail() != null &&
        webInfoForm.getInEmail().trim().length() > 0 && webInfoForm.getInNote() != null &&
        webInfoForm.getInNote().trim().length() > 0) {
      try {
        WebContact webContact = new WebContact();
        webContact.setPk(DBConnectionMgr.getUniqueId());
        webContact.setName(webInfoForm.getInName());
        webContact.setPhone(webInfoForm.getInPhone());
        webContact.setEmail(webInfoForm.getInEmail());

        StringBuilder noteBuffer = new StringBuilder();
        if (webInfoForm.getInCmpy() != null && webInfoForm.getInCmpy().trim().length() > 0) {
          noteBuffer.append("Company: ");
          noteBuffer.append(webInfoForm.getInCmpy());
          noteBuffer.append("\n");
        }
        if (webInfoForm.getInWeb() != null && webInfoForm.getInWeb().trim().length() > 0) {
          noteBuffer.append("Website: ");
          noteBuffer.append(webInfoForm.getInWeb());
          noteBuffer.append("\n");
        }
        if (webInfoForm.getInNote() != null && webInfoForm.getInNote().trim().length() > 0) {
          noteBuffer.append("Note:");
          noteBuffer.append("\n");
          noteBuffer.append(webInfoForm.getInNote());
        }


        webContact.setNote(noteBuffer.toString());

        webContact.setStatus(APPConstants.STATUS_ACTIVE);
        webContact.setCreatedby("SYSTEM");
        webContact.setModifiedby("SYSTEM");
        webContact.setCreatedtimestamp(System.currentTimeMillis());
        webContact.setLastupdatedtimestamp(System.currentTimeMillis());

        webContact.save();
        view.msg = "Thank you for your interest - we will contact you shortly.";
        view.success = true;


        try {
          String smtp = ConfigMgr.getAppParameter(CoreConstants.EMAIL_SMTP_HOST);
          String user = ConfigMgr.getAppParameter(CoreConstants.EMAIL_SMTP_USER_ID);
          String pwd = ConfigMgr.getAppParameter(CoreConstants.EMAIL_SMTP_PASSWORD);

          EmailMgr emailMgr = new EmailMgr(smtp, user, pwd);
          emailMgr.init();
          List<Map<String, String>> body = new ArrayList<Map<String, String>>();
          Map<String, String> token = new HashMap<String, String>();

          StringBuffer subject = new StringBuffer("Web Contact: ");
          if (webInfoForm.getInName() != null && webInfoForm.getInName().length() > 0) {
            subject.append(webInfoForm.getInName());
          }
          else if (webInfoForm.getInEmail() != null && webInfoForm.getInEmail().length() > 0) {
            subject.append(webInfoForm.getInEmail());
          }

          token.put(CoreConstants.TEXT, subject.toString());
          //body.add(token);

          StringBuffer sb = new StringBuffer();
          sb.append("Website info request.\n");
          if (webInfoForm.getInName() != null && webInfoForm.getInName().length() > 0) {
            sb.append("Name: ");
            sb.append(webInfoForm.getInName());
            sb.append("\n");
          }
          if (webInfoForm.getInEmail() != null && webInfoForm.getInEmail().length() > 0) {
            sb.append("Email: ");
            sb.append(webInfoForm.getInEmail());
            sb.append("\n");
          }
          if (webInfoForm.getInPhone() != null && webInfoForm.getInPhone().length() > 0) {
            sb.append("Phone: ");
            sb.append(webInfoForm.getInPhone());
            sb.append("\n");
          }
          if (webInfoForm.getInNote() != null && webInfoForm.getInNote().length() > 0) {
            sb.append("Note: ");
            sb.append(noteBuffer.toString());
            sb.append("\n");
          }

          token = new HashMap<>();
          token.put(CoreConstants.TEXT, sb.toString());
          body.add(token);

          emailMgr.sendBCC("info@umapped.com",
                           "UMapped Website",
                           "info@umapped.com",
                           "dropbox@83184387.umappedinc.highrisehq.com",
                           subject.toString(),
                           body);
        }
        catch (Exception e) {
          e.printStackTrace();
        }

      }
      catch (Exception e) {
        e.printStackTrace();
        view.msg = "A system error has occurred - please try again.";
        view.success = false;
      }
    }
    else {
      view.msg = "Some mandatory fields are missing.";
      view.success = false;
    }

    response().setHeader("Access-Control-Allow-Origin",
                         "http://www.umapped.com");       // Need to add the correct domain in here!!
    response().setHeader("Access-Control-Allow-Methods", "POST");   // Only allow POST
    response().setHeader("Access-Control-Max-Age", "1");          // Cache response for 5 minutes
    response().setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    return ok(views.html.web.webcontact.render(view));
  }
}
