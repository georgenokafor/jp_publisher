package com.mapped.publisher.parse.extractor;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.parse.valueObject.DateDetails;
import com.mapped.publisher.parse.valueObject.DayDetails;
import com.mapped.publisher.parse.valueObject.TravelDetails;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class DataExtractor {

    private String lineToken = null;
    private String morningToken = null;
    private String afternoonToken = null;
    private boolean parseActivities = false;

    private static String[] dateTimeFormat = {"MMM dd yyyy HH:mm z", "MMM dd yyyy h:mm a z", "MMM dd yyyy HH mm z", "MMM dd yyyy h mm a z", "MMM dd yyyy HH.mm z", "MMM dd yyyy h.mm a z", "MMM dd yyyy ha z","MMM dd yyyy h a z" };
    private static String defaultDateFormat = "MMM dd yyyy";

    private final static String tre2 = "(\\d+)"; // Integer Number 1
    private final static String tre3 = "(\\.)"; // Any Single Character 3
    private final static String tre4 = "(\\d+)"; // Integer Number 2
    private final static String tre5 = "(\\s+)"; // Any Single Character 4
    private final static String tre6 = "((?:am|AM|pm|PM))"; // Word 1

    private final static String re1 = "((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9])):(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)(?:\\s+)(-)(?:\\s+)((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9])):(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)(?:\\s+)|(\\d+)(?:\\s+)(?:am|AM|pm|PM)(?:\\s+)(-)(?:\\s+)(\\d+)(?:\\s+)(?:am|AM|pm|PM)|((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9])):(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)(?:\\s+)(-)(?:\\s+)(\\d+)(?:\\s+)(?:am|AM|pm|PM)|(\\d+)(?:\\s+)(?:am|AM|pm|PM)(?:\\s+)(-)(?:\\s+)((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9])):(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)|((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9])):(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)|(\\d+)(?:\\s+)(?:am|AM|pm|PM)";
    //(?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9]))((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9]))(:|\.)(?:[0-5][0-9])(?:(1|\.)[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)(?:\\s+)(-)(?:\\s+)((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9]))(:|\.)(?:[0-5][0-9])(?:(:|\.)[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)(?:\\s+)|(\\d+)(?:\\s+)(?:am|AM|pm|PM)(?:\\s+)(-)(?:\\s+)(\\d+)(?:\\s+)(?:am|AM|pm|PM)|((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9]))(:|\.)(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)(?:\\s+)(-)(?:\\s+)(\\d+)(?:\\s+)(?:am|AM|pm|PM)|(\\d+)(?:\\s+)(?:am|AM|pm|PM)(?:\\s+)(-)(?:\\s+)((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9]))(:|\.)(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)|((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9]))(:|\.)(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)|(\\d+)(?:\\s+)(?:am|AM|pm|PM).

    private final static String e = "(([0-9]|[0-2][0-9])\\s*(am|Am|AM|pm|Pm|PM)|([0-9]|[0-2][0-9])(:|\\.)[0-5][0-9]\\s*(am|Am|AM|pm|Pm|PM)|([0-9]|[0-2][0-9])(:)[0-5][0-9])";


    private static Pattern timePattern = Pattern.compile(e,Pattern.DOTALL); //Pattern.compile(tre2 + tre3 + tre4 + tre5 + tre6, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);



    public void init(Map<String, String> params) {
        if (params != null) {
            lineToken = params.get(APPConstants.DOC_LINE_TOKEN);
            morningToken = params.get(APPConstants.DOC_MORNING_TOKEN);
            afternoonToken = params.get(APPConstants.DOC_AFTERNOON_TOKEN);
            String parseA = params.get(APPConstants.DOC_PARSE_ACTIVITIES);
            if (parseA != null && (parseA.trim().toLowerCase().startsWith("y") || parseA.trim().toLowerCase().startsWith("t"))) {
                parseActivities = true;
            }
        }
        this.initImpl(params);

    }


    public TravelDetails extractData(InputStream input) throws Exception {
        try {
            return this.extractDataImpl(input);
        } finally {
            if (input != null) {
                input.close();
            }
        }
    }

    protected abstract TravelDetails extractDataImpl(InputStream input) throws Exception;

    protected abstract void initImpl(Map<String, String> params);

    protected Map<Date, DayDetails> parseDailyActivities(DateDetails details) {
        if (!parseActivities)
            return null;


        Map<Date, DayDetails> activities = new TreeMap<Date, DayDetails>();


        boolean timeFound = false;
        //try to extract by time of date
        if (lineToken != null && !lineToken.isEmpty()) {
            String[] a = details.getFulText().split(lineToken);
            if (a != null) {
                for (String line : a) {
                    try {
                        DayDetails detail = getTime(line, details.getDate());
                        if (detail != null) {
                            Date timestamp = new Date(detail.getTimestamp());
                            activities.put(timestamp, detail);
                            timeFound = true;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }


        if (!timeFound && morningToken != null && afternoonToken != null) {
            //try to extract by morning/afternoon

            String am = "";
            String pm = "";
            int startAm = -1;
            int startPm = -1;

            if (details.getFulText().toLowerCase().indexOf(morningToken) > 0) {
                if (details.getFulText().toLowerCase().indexOf(afternoonToken) > 0 && details.getFulText().toLowerCase().indexOf(afternoonToken) > details.getFulText().toLowerCase().indexOf(morningToken)) {
                    am = details.getFulText().substring(details.getFulText().toLowerCase().indexOf(morningToken), details.getFulText().toLowerCase().indexOf(afternoonToken));
                    //find the start of the sentence that contains the token
                    if (!Character.isUpperCase(am.codePointAt(0))) {
                        int startIndex = details.getFulText().indexOf(am);
                        if (startIndex > 0) {
                            for (int i = startIndex - 1; i >= 0; i--) {
                                if (Character.isUpperCase(details.getFulText().codePointAt(i))) {
                                    startAm = i;
                                    break;
                                }
                            }
                        }

                    }

                    pm = details.getFulText().substring(details.getFulText().toLowerCase().indexOf(afternoonToken));
                    //find the start of the sentence that contains the token
                    if (!Character.isUpperCase(pm.codePointAt(0))) {
                        int startIndex = details.getFulText().indexOf(pm);
                        if (startIndex > 0) {
                            for (int i = startIndex - 1; i >= 0; i--) {
                                if (Character.isUpperCase(details.getFulText().codePointAt(i))) {
                                    startPm = i;
                                    break;
                                }
                            }
                        }

                    }

                    if (startAm != -1 && startPm < details.getFulText().length()) {
                        am = details.getFulText().substring(startAm, startPm);
                    }
                    if (startPm != -1) {
                        pm = details.getFulText().substring(startPm);
                    }
                } else {
                    am = details.getFulText().substring(details.getFulText().toLowerCase().indexOf(morningToken));
                    if (!Character.isUpperCase(am.codePointAt(0))) {
                        int startIndex = details.getFulText().indexOf(am);

                        if (startIndex > 0) {
                            for (int i = startIndex - 1; i >= 0; i--) {
                                if (Character.isUpperCase(details.getFulText().codePointAt(i))) {
                                    startAm = i;
                                    break;
                                }
                            }
                        }
                        if (startAm >= 0) {
                            am = details.getFulText().substring(startAm);
                        }

                    }
                }
            } else
                am = details.getFulText();


            if (am != null && !am.isEmpty()) {
                am = am.substring(0, 1).toUpperCase() + am.substring(1);

                DayDetails d = new DayDetails();
                String intro = am;
                String body = "";
                if (intro.indexOf(".") > 0) {
                    intro = intro.substring(0, intro.indexOf("."));
                    if ((intro.indexOf(".") + 1) < intro.length()) {
                        body = am.substring(am.indexOf(".") + 1);
                        body = body.substring(0, 1).toUpperCase() + body.substring(1);
                    }

                }
                d.setIntro(intro);
                d.setBody(body);
                DateTime dateTime = new DateTime(details.getDate());
                dateTime = dateTime.withTime(9, 0, 0, 0);
                d.setTimestamp(dateTime.getMillis());

                Date date = new Date(dateTime.getMillis());
                activities.put(date, d);

            }

            if (pm != null && !pm.isEmpty()) {
                pm = pm.substring(0, 1).toUpperCase() + pm.substring(1);

                DayDetails d = new DayDetails();
                String intro = pm;
                String body = "";
                if (intro.indexOf(".") > 0) {
                    intro = intro.substring(0, intro.indexOf("."));

                    if (intro.indexOf(".") + 1 < intro.length())
                        body = pm.substring(pm.indexOf(".") + 1);
                    body = body.substring(0, 1).toUpperCase() + body.substring(1);

                }
                d.setIntro(intro);
                d.setBody(body);
                DateTime dateTime = new DateTime(details.getDate());
                dateTime = dateTime.withTime(13, 0, 0, 0);
                d.setTimestamp(dateTime.getMillis());

                Date date = new Date(dateTime.getMillis());
                activities.put(date, d);

            }
        }
        return activities;
    }


    /**
     * Returns the time extracted from the text
     *
     * @param txt
     * @return
     */
    private DayDetails getTime(String txt, Date date) {
       try {
        Matcher m = timePattern.matcher(txt);
        if(m.find()) {
            String startTime = m.group();
            String subString = txt.substring(txt.indexOf(startTime) + startTime.length());

            DayDetails dayDetails = new DayDetails();
            Date timestamp = formatDateTime(date, startTime, dateTimeFormat);

            dayDetails.setTimestamp(timestamp.getTime());

            String text = "";
            //check to see if there is an end time
            Matcher m1 = timePattern.matcher(subString);
            if (m1.find()) {//end time found
                String endTime = m1.group();
                String subString1 = subString.substring(subString.indexOf(endTime) + endTime.length());
                Date endTimestamp = formatDateTime(date, endTime, dateTimeFormat);

                dayDetails.setEndTimestamp(endTimestamp.getTime());
                text = subString1;
            } else {  //n end time  - assume the rest of the text is the body
                text = subString;
            }

            text = cleanText(text);
            //remove any non -alpha from the beginning
            for (int i = 0; i < text.length() - 1; i++) {
                if (StringUtils.isAlphanumeric(text.substring(i, i + 1))) {
                    text = text.substring(i);
                    break;
                }
            }

            String[] title = text.split("\\.");
            StringBuffer sb = new StringBuffer();
            String intro = "";
            int count = 0;
            for (String s : title) {
                if (count == 0) {
                    intro = s;
                } else {
                    sb.append(s);
                }
                count++;
            }

            dayDetails.setIntro(intro);
            dayDetails.setBody(sb.toString());



            return dayDetails;

        }
       } catch (Exception e) {
          e.printStackTrace();
       }

        return null;




        /*
        Matcher m = timePattern.matcher(txt);

        while (m.find()) {
            String s = m.group();
            System.out.println(s);
        }


        if (m.find()) {

            return m.group();
        }

        return "";
        */
    }


    private Date formatDateTime(Date date, String time, String[] format) throws ParseException {
        //covert all timestamp to GMT
        SimpleDateFormat sdf = new SimpleDateFormat(defaultDateFormat);
        StringBuffer dateTime = new StringBuffer();
        dateTime.append(sdf.format(date));
        dateTime.append(" ");
        dateTime.append(time);
        dateTime.append(" GMT");


        DateUtils du = new DateUtils();
        return du.parseDate(dateTime.toString(), format);
    }

    public String cleanText(String s) {
        //handle windows cr
        s = s.replace("\r", "\n");
        s = s.replace("\r\n", "\n");

        String[] lines = s.split("\n");
        StringBuffer sb = new StringBuffer();
        if (lines != null && lines.length > 0) {
            String previousLine = "";
            int numOfCR = 0;
            for (String line : lines) {
                line = line.trim();
                if (line.isEmpty() && previousLine.isEmpty()) {
                    numOfCR++;
                } else
                    numOfCR = 0;


                if (line.isEmpty() && previousLine.isEmpty() && sb.length() > 0 && numOfCR < 2) { //empty lines
                    sb.append("\n");
                } else if (!previousLine.isEmpty()) {
                    sb.append(previousLine);
                    if (previousLine.endsWith(".") || previousLine.endsWith(":") || previousLine.endsWith(";") || previousLine.endsWith("!") || previousLine.endsWith("?")) {
                        sb.append("\n");
                    } else if (previousLine.length() > 0 && Character.isUpperCase(previousLine.codePointAt(previousLine.length() - 1))) {
                        sb.append("\n");
                    } else if (line.isEmpty() ) {  //|| (line.length() > 0 && Character.isUpperCase(line.codePointAt(0)))
                        sb.append("\n");
                    } else if (!line.isEmpty() && !StringUtils.isAlphanumeric(line.substring(0, 1)) && !line.substring(0,1).equals("‘") && !line.substring(0,1).equals("'") && !line.substring(0,1).equals("`") && !line.substring(0,1).equals("\"") &&  !(line.substring(0,1).equals("(") && line.length() > 1 && Character.isLowerCase(line.codePointAt(1)) ) ) {
                        sb.append("\n");
                    } else
                        sb.append(" ");
                }
                previousLine = line;
            }

            //process last line
            if (previousLine != null && !previousLine.isEmpty()) {
                sb.append(previousLine);
            }

            return sb.toString();
        }
        return s;

    }


}
