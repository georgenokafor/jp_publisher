package com.umapped.persistence.accountprop;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;

/**
 * Created by george on 2017-09-22.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EligiblePlans implements Serializable {
  public Long monthlyBasicPlanId;
  public Long monthlyProPlanId;
  public Long yearlyBasicPlanId;
  public Long yearlyProPlanId;

  public boolean hasEligblePlan() {
    if (monthlyBasicPlanId != null || monthlyProPlanId != null || yearlyBasicPlanId != null || yearlyProPlanId != null) {
      return true;
    }
    return false;
  }

}
