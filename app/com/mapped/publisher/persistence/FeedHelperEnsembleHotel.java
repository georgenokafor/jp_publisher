package com.mapped.publisher.persistence;

import au.com.bytecode.opencsv.CSVReader;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.S3Util;
import com.mapped.publisher.utils.Utils;
import com.umapped.api.schema.types.Address;
import com.umapped.api.schema.types.Feature;
import models.publisher.FeedSrc;
import models.publisher.PoiSrcFeed;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.language.Metaphone;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import javax.xml.bind.DatatypeConverter;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by surge on 2015-10-06.
 */
public class FeedHelperEnsembleHotel
    extends FeedHelperPOI {
  public final static String ENSEMBLE_HOTEL_FEED_NAME = "Ensemble Hotel";
  public final static String ENSEMBLE_S3_PREFIX = "feeds/Ensemble/";
  private final static int METAPHONE_LEN = 4;

  private static Pattern feedFile = Pattern.compile("Ensemble_(2[0-9][0-9][0-9])([0-1][0-9])([0-3][0-9])_hotel.*.csv",
                                                      Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

  @Override public List<FeedResource> getResources() {
    List<FeedResource> result = new ArrayList<>();
    List<S3ObjectSummary> objs = S3Util.getObjectList(S3Util.getPDFBucketName(), ENSEMBLE_S3_PREFIX);
    Map<Date, S3ObjectSummary> amenities = new HashMap<>();
    Map<Date, S3ObjectSummary> hotels = new HashMap<>();

    for(S3ObjectSummary o : objs) {
      Matcher m = feedFile.matcher(o.getKey());
      if(m.find()) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)) - 1, Integer.parseInt(m.group(3)), 0, 0, 0);
        Date d = cal.getTime();
        if (o.getKey().contains("hotel_amenities")) {
          amenities.put(d, o);
        } else {
          hotels.put(d, o);
        }
      }
    }

    for(Date d: amenities.keySet()) {
      S3ObjectSummary amen = amenities.get(d);
      S3ObjectSummary hotl = hotels.get(d);
      if(hotl != null) {
        FeedResource fr = new FeedResource();
        fr.setDate(d);
        fr.setName(amen.getKey());
        fr.setOrigin(FeedResource.Origin.AWS_S3);
        fr.setSize(amen.getSize());
        result.add(fr);
      }
    }
    return result;
  }

  public static class EnsembleHotelData {
    public String ensembleId;
    public String phone;
    public String chain;
    public String name;
    public String address;
    public String city;
    public String state;
    public String postalCode;
    public String country;
    public String seasonStart;
    public String seasonEnd;
    public String rate;
    public String currency;
    public String sabre;
    public String sabreAudit;
    public String apolloGalileo;
    public String apolloGalileoAudit;
    public String chainCode;
    public String amadeus;
    public String amadeusAudit;
    public String worldspan;
    public String worldspanAudit;
    public String comments;
    public String airport;
    public String contact;
    public String contactEmail;
    public String namName;
    public String namEmail;
    public String amenities;
    public String nameAlias;
  }

  private enum AmenitiesColumns {
    COUNTRY,
    STATE,
    CITY,
    PROPERTY,
    AMENITIES,
    COLUMN_COUNT;
  }

  private enum HotelsColumns {
    Ensemble_ID,
    Phone,
    Chain,
    Hotel_Name,
    Address,
    City,
    State,
    Postal_Code,
    Country,
    Season_1_Start,
    Season_1_End,
    Rate_1,
    Currency,
    Sabre,
    Sabre_audit,
    Apollo_Galileo,
    Apollo_Galileo_audit,
    Chain_code,
    Amadeus,
    Amadeus_audit,
    Worldspan,
    Worldspan_audit,
    Comments_and_rate_notes,
    Airport,
    Hotel_Contact,
    Hotel_Contact_Email,
    NAM_NAME_OR_EMAIL,
    NAM_EMAIL,
    COLUMN_COUNT;
  }

  public String getFeedRef(EnsembleHotelData  hd) {
    return DigestUtils.md5Hex(simplifyName(hd.name));
  }

  @Override public Map<PoiFeedUtil.FeedResult, Integer> load(String filePath) {
    Map<PoiFeedUtil.FeedResult, Integer> results = new HashMap<>();
    int errors  = 0;
    int success = 0;
    try {
      String hotelsFilepath = "";
      Matcher m = feedFile.matcher(filePath);
      if(m.find()) {
        hotelsFilepath = ENSEMBLE_S3_PREFIX +  "Ensemble_"+m.group(1) + m.group(2) + m.group(3) + "_hotels.csv";
      }
      else {
        return null;
      }

      S3Object amenities = S3Util.getS3File(S3Util.getPDFBucketName(), filePath);
      S3Object hotels    = S3Util.getS3File(S3Util.getPDFBucketName(), hotelsFilepath);
      if(hotels == null || amenities == null) {
        return null;
      }

      Map<String, EnsembleHotelData> data = new HashMap<>();
      Map<String, List<EnsembleHotelData>> byCity = new HashMap<>();
      Reader hotelsReader = new InputStreamReader(hotels.getObjectContent());
      CSVReader hotelsCSV = new CSVReader(hotelsReader);

      String[] line;
      line = hotelsCSV.readNext(); //Skipping labels line
      Metaphone metaphone = new Metaphone();
      metaphone.setMaxCodeLen(METAPHONE_LEN);

      while ((line = hotelsCSV.readNext()) != null) {
        if(line.length < HotelsColumns.COLUMN_COUNT.ordinal()) {
          Log.err("Ensemble hotels CSV line has fewer columns than expected");
          continue;
        }
        EnsembleHotelData hd = new EnsembleHotelData();
        hd.ensembleId =  Utils.trim(line[HotelsColumns.Ensemble_ID.ordinal()]);
        hd.phone =  Utils.trim(line[HotelsColumns.Phone.ordinal()]);
        hd.chain =  Utils.trim(line[HotelsColumns.Chain.ordinal()]);
        hd.name =  Utils.trim(line[HotelsColumns.Hotel_Name.ordinal()]);
        hd.address =  Utils.trim(line[HotelsColumns.Address.ordinal()]);
        hd.city =  Utils.trim(line[HotelsColumns.City.ordinal()]);
        hd.state =  Utils.trim(line[HotelsColumns.State.ordinal()]);
        hd.postalCode =  Utils.trim(line[HotelsColumns.Postal_Code.ordinal()]);
        hd.country =  Utils.trim(line[HotelsColumns.Country.ordinal()]);
        hd.seasonStart =  Utils.trim(line[HotelsColumns.Season_1_Start.ordinal()]);
        hd.seasonEnd =  Utils.trim(line[HotelsColumns.Season_1_End.ordinal()]);
        hd.rate =  Utils.trim(line[HotelsColumns.Rate_1.ordinal()]);
        hd.currency =  Utils.trim(line[HotelsColumns.Currency.ordinal()]);
        hd.sabre =  Utils.trim(line[HotelsColumns.Sabre.ordinal()]);
        hd.sabreAudit =  Utils.trim(line[HotelsColumns.Sabre_audit.ordinal()]);
        hd.apolloGalileo =  Utils.trim(line[HotelsColumns.Apollo_Galileo.ordinal()]);
        hd.apolloGalileoAudit =  Utils.trim(line[HotelsColumns.Apollo_Galileo_audit.ordinal()]);
        hd.chainCode =  Utils.trim(line[HotelsColumns.Chain_code.ordinal()]);
        hd.amadeus =  Utils.trim(line[HotelsColumns.Amadeus.ordinal()]);
        hd.amadeusAudit =  Utils.trim(line[HotelsColumns.Amadeus_audit.ordinal()]);
        hd.worldspan =  Utils.trim(line[HotelsColumns.Worldspan.ordinal()]);
        hd.worldspanAudit =  Utils.trim(line[HotelsColumns.Worldspan_audit.ordinal()]);
        hd.comments =  Utils.trim(line[HotelsColumns.Comments_and_rate_notes.ordinal()]);
        hd.airport =  Utils.trim(line[HotelsColumns.Airport.ordinal()]);
        hd.contact =  Utils.trim(line[HotelsColumns.Hotel_Contact.ordinal()]);
        hd.contactEmail =  Utils.trim(line[HotelsColumns.Hotel_Contact_Email.ordinal()]);
        hd.namName =  Utils.trim(line[HotelsColumns.NAM_NAME_OR_EMAIL.ordinal()]);
        hd.namEmail =  Utils.trim(line[HotelsColumns.NAM_EMAIL.ordinal()]);
        if(hd.namName.indexOf('@') > 0) {
          hd.namEmail = hd.namName;
          hd.namName = "";
        }
        hd.name = hd.name.replaceAll(" +", " ");

        if(data.get(hd.name) != null) {
          Log.err("{.}|{.} WTF these Ensemble are doing????");
        }
        data.put(simplifyName(hd.name), hd);

        List<EnsembleHotelData> cityHotels = byCity.get(hd.city);
        if(cityHotels == null) {
          cityHotels = new ArrayList<>();
          cityHotels.add(hd);
          byCity.put(hd.city, cityHotels);
        } else {
          cityHotels.add(hd);
        }
      }

      Reader amenitiesReader = new InputStreamReader(amenities.getObjectContent());
      CSVReader amenitiesCSV = new CSVReader(amenitiesReader);
      line = amenitiesCSV.readNext(); //Skipping labels
      int unmatched = 0;
      while ((line = amenitiesCSV.readNext()) != null) {
        if(line.length < AmenitiesColumns.COLUMN_COUNT.ordinal()) {
          Log.err("Ensemble amenities CSV line has fewer columns than expected");
          continue;
        }

        String name = Utils.trim(line[AmenitiesColumns.PROPERTY.ordinal()]);
        String city = Utils.trim(line[AmenitiesColumns.CITY.ordinal()]);
        name = name.replaceAll(" +", " ");
        if(name.length() == 0) {
          continue; //Just a label line
        }
        EnsembleHotelData hd = data.get(simplifyName(name));
        if(hd == null) {
          List<EnsembleHotelData> cityHotels = byCity.get(city);
          if(cityHotels != null) {
            int matches = 0;
            EnsembleHotelData pick = null;
            for(EnsembleHotelData h: cityHotels) {
              int cmp = compareNames(name, h.name);
              if(cmp == 0) {
                pick = h;
                matches++;
              }
            }
            if(matches == 1) {
              hd = pick;
            }
          }
        }

        if(hd != null) {
          //Log.debug("SUCCESS: " + name + " ~IS~ " + hd.name);
          hd.amenities = Utils.trim(line[AmenitiesColumns.AMENITIES.ordinal()]);
          hd.nameAlias = name;
          success++;
        } else {
          errors++;
          Log.err("ENSEMBLE HOTEL MISMATCH " + (++unmatched) + ". Name: " + name );
        }
      }

      ObjectMapper jsonMapper = new ObjectMapper();
      for(String name: data.keySet()) {
        EnsembleHotelData hd = data.get(name);
        if(hd.amenities == null) {
          continue;
        }

        String json = jsonMapper.writeValueAsString(hd);
        PoiImportRS pirs = new PoiImportRS();
        pirs.state = PoiImportRS.ImportState.UNPROCESSED;
        pirs.srcId = FeedSourcesInfo.byName(ENSEMBLE_HOTEL_FEED_NAME);
        pirs.poiId = null;
        pirs.typeId = PoiTypeInfo.Instance().byName("Accommodations").getId();
        pirs.timestamp = System.currentTimeMillis();
        pirs.srcReference = getFeedRef(hd);
        pirs.srcFilename = hotelsFilepath;
        pirs.name = hd.name;
        pirs.countryName = hd.country;

        CountriesInfo.Country c = CountriesInfo.Instance().searchByName(hd.country);
        if (c == null) {
          Log.err("Failed to find matching country: >>" + hd.country + "<< !!!");
          pirs.countryCode = "UNK";
          errors++;
        }
        else {
          pirs.countryCode = c.getAlpha3();
        }

        pirs.phone = hd.phone;
        pirs.region = hd.state;
        pirs.postalCode = hd.postalCode;
        pirs.city = hd.city;
        pirs.address = hd.address;
        pirs.raw = json;
        pirs.hash = DigestUtils.md5Hex(pirs.raw);
        PoiImportMgr.save(pirs);
      }
    }
    catch (Exception e) {
      Log.err("Failure during Ensemble hotels feed processing");
      e.printStackTrace();
    }
    results.put(PoiFeedUtil.FeedResult.SUCCESS, success);
    results.put(PoiFeedUtil.FeedResult.ERROR, errors);
    return results;
  }

  @Override public PoiImportRS.ImportState associate(PoiImportRS importRS) {
    List<Pair<PoiRS, Integer>> scores = getRankedPois(importRS, false);

    if (scores.size() == 0 || scores.get(0).getRight() <= 0) {
      Log.debug("Found no matches for:" + importRS.name + " from: " + importRS.countryCode);
      importRS.state = PoiImportRS.ImportState.MISMATCH;
      importRS.poiId = null;
    }
    else {
      importRS.state = (scores.get(0).getRight() >= 40) ?
                       PoiImportRS.ImportState.MATCH :
                       PoiImportRS.ImportState.UNSURE;
      importRS.poiId = scores.get(0).getLeft().getId();
    }

    PoiImportMgr.update(importRS);
    return importRS.state;
  }

  @Override public PoiSrcFeed.FeedSyncResult synchronize(PoiSrcFeed psf) {
    PoiSrcFeed.FeedSyncResult result = PoiSrcFeed.FeedSyncResult.FAIL;
    try {
      ObjectMapper jsonMapper = new ObjectMapper();
      com.fasterxml.jackson.databind.ObjectReader r = jsonMapper.reader(EnsembleHotelData.class);
      EnsembleHotelData data = r.readValue(psf.getRaw());
      FeedSrc feedSrc = FeedSrc.find.byId(psf.getFeedId().srcId);
      List<PoiRS> poiRecs = PoiMgr.findAllById(psf.getFeedId().poiId);

      PoiRS rsToUpdate = null;
      for (PoiRS p : poiRecs) {
        if (p.getCmpyId() == feedSrc.getCmpyId() &&
            p.consortiumId == feedSrc.getConsortiumId() &&
            p.getSrcId() == feedSrc.getSrcId()) {
          rsToUpdate = p;
        }
      }

      if (rsToUpdate == null) { //Creating a new POI record
        rsToUpdate = PoiRS.buildRecord(feedSrc.getConsortiumId(),
                                       feedSrc.getCmpyId(),
                                       feedSrc.getTypeId(),
                                       feedSrc.getSrcId(),
                                       "system");
        updatePoi(rsToUpdate, psf, data, feedSrc);
        rsToUpdate.setId(psf.getFeedId().poiId);
        PoiMgr.save(rsToUpdate);
        result = PoiSrcFeed.FeedSyncResult.NEW;
      }
      else { //updating existing one
        updatePoi(rsToUpdate, psf, data, feedSrc);
        PoiMgr.update(rsToUpdate);
        result = PoiSrcFeed.FeedSyncResult.UPDATED;
      }
      psf.setSyncTs(System.currentTimeMillis());
      psf.setSynced(true);
      psf.update();

    }
    catch (Exception e) {
      Log.err("Unexpected Error - not handling gracefully:", e.getMessage());
      e.printStackTrace();
    }
    return result;
  }

  private PoiRS updatePoi(PoiRS poi, PoiSrcFeed feedRec, EnsembleHotelData hotelInfo, FeedSrc src) {
    poi.setSrcReference(feedRec.getSrcReference());
    poi.setName(hotelInfo.name);
    Address address = new Address();
    address.setStreetAddress(hotelInfo.address);
    address.setLocality(hotelInfo.city);
    address.setRegion(hotelInfo.state);
    address.setCountryCode(CountriesInfo.Instance().searchByName(hotelInfo.country).getAlpha3());
    address.setPostalCode(hotelInfo.postalCode);
    poi.setMainAddress(address);
    poi.clearFeatures(); //Clearing all previously listed features for this feed
    if(hotelInfo.amenities != null) {
      Feature f = new Feature();
      f.setName("EnsembleAmenities");
      f.setDesc(hotelInfo.amenities);
      f.addTag(src.getName());
      poi.addFeature(f);
    }
    return poi;
  }

  @Override public List<Pair<PoiRS, Integer>> getRankedPois(PoiImportRS importRS, boolean ignoreCountry) {
    String cleanName = importRS.name;
    List<PoiRS> pois;
    if (ignoreCountry) {
      pois = PoiMgr.findForMerge(cleanName, null, importRS.typeId);
    }
    else {
      pois = PoiMgr.findForMerge(cleanName, importRS.countryCode, importRS.typeId);
    }

    List<Pair<PoiRS, Integer>> scores = new ArrayList<>();
    for (PoiRS prs : pois) {
      int currScore = rankPoi(cleanName, importRS, prs);
      scores.add(new ImmutablePair<>(prs, currScore));
    }

    Collections.sort(scores, (o1, o2) -> o2.getRight() - o1.getRight());

    return scores;
  }

  @Override public PoiSrcFeed.FeedSyncResult clearAmenities(PoiSrcFeed psf) {
    PoiSrcFeed.FeedSyncResult result = PoiSrcFeed.FeedSyncResult.FAIL;

    ObjectMapper jsonMapper = new ObjectMapper();
    com.fasterxml.jackson.databind.ObjectReader r = jsonMapper.reader(EnsembleHotelData.class);
    try {
      EnsembleHotelData data = r.readValue(psf.getRaw());
      if (data.amenities != null) {
        data.amenities = null;
        psf.setSynced(false);
        psf.setRaw(jsonMapper.writeValueAsString(data));
        psf.setHash(DatatypeConverter.parseHexBinary(DigestUtils.md5Hex(psf.getRaw())));
        psf.update();
        result = PoiSrcFeed.FeedSyncResult.UPDATED;
      } else {
        result = PoiSrcFeed.FeedSyncResult.UNCHANGED;
      }
    } catch(Exception e) {
      Log.err("Failure to remove amenities for Ensemble feed", e);
      e.printStackTrace();
    }
    return result;
  }
}
