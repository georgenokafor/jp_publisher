//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.04.17 at 04:55:33 PM EDT 
//


package com.umapped.itinerary.analyze.supplier.travelbound.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for t_SearchHotelPricePaxRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="t_SearchHotelPricePaxRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemDestination" type="{}t_ItemDestinationPax"/>
 *         &lt;element name="ImmediateConfirmationOnly" type="{}t_Empty" minOccurs="0"/>
 *         &lt;element name="ItemName" type="{}t_SearchName" minOccurs="0"/>
 *         &lt;sequence>
 *           &lt;choice>
 *             &lt;element name="ItemCode" type="{}t_ItemCode" minOccurs="0"/>
 *             &lt;element name="ItemCodes" type="{}t_ItemCodes" minOccurs="0"/>
 *           &lt;/choice>
 *         &lt;/sequence>
 *         &lt;element name="PeriodOfStay" type="{}t_PeriodOfStay"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="IncludeRecommended" type="{}t_Empty" minOccurs="0"/>
 *           &lt;element name="RecommendedOnly" type="{}t_Empty" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="IncludePriceBreakdown" type="{}t_Empty" minOccurs="0"/>
 *         &lt;element name="IncludeChargeConditions" type="{}t_IncludeChargeConditions" minOccurs="0"/>
 *         &lt;element name="ExcludeChargeableItems" type="{}t_ExcludeChargeableItems" minOccurs="0"/>
 *         &lt;element name="PaxRooms" type="{}t_PaxRooms"/>
 *         &lt;element name="StarRatingRange" type="{}t_StarRatingRange" minOccurs="0"/>
 *         &lt;element name="HotelTypeCode" type="{}t_HotelType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "t_SearchHotelPricePaxRequest", propOrder = {
    "itemDestination",
    "immediateConfirmationOnly",
    "itemName",
    "itemCode",
    "itemCodes",
    "periodOfStay",
    "includeRecommended",
    "recommendedOnly",
    "includePriceBreakdown",
    "includeChargeConditions",
    "excludeChargeableItems",
    "paxRooms",
    "starRatingRange",
    "hotelTypeCode"
})
public class TSearchHotelPricePaxRequest {

    @XmlElement(name = "ItemDestination", required = true)
    protected TItemDestinationPax itemDestination;
    @XmlElement(name = "ImmediateConfirmationOnly")
    protected TEmpty immediateConfirmationOnly;
    @XmlElement(name = "ItemName")
    protected String itemName;
    @XmlElement(name = "ItemCode")
    protected String itemCode;
    @XmlElement(name = "ItemCodes")
    protected TItemCodes itemCodes;
    @XmlElement(name = "PeriodOfStay", required = true)
    protected TPeriodOfStay periodOfStay;
    @XmlElement(name = "IncludeRecommended")
    protected TEmpty includeRecommended;
    @XmlElement(name = "RecommendedOnly")
    protected TEmpty recommendedOnly;
    @XmlElement(name = "IncludePriceBreakdown")
    protected TEmpty includePriceBreakdown;
    @XmlElement(name = "IncludeChargeConditions")
    protected TIncludeChargeConditions includeChargeConditions;
    @XmlElement(name = "ExcludeChargeableItems")
    protected TExcludeChargeableItems excludeChargeableItems;
    @XmlElement(name = "PaxRooms", required = true)
    protected TPaxRooms paxRooms;
    @XmlElement(name = "StarRatingRange")
    protected TStarRatingRange starRatingRange;
    @XmlElement(name = "HotelTypeCode")
    protected String hotelTypeCode;

    /**
     * Gets the value of the itemDestination property.
     * 
     * @return
     *     possible object is
     *     {@link TItemDestinationPax }
     *     
     */
    public TItemDestinationPax getItemDestination() {
        return itemDestination;
    }

    /**
     * Sets the value of the itemDestination property.
     * 
     * @param value
     *     allowed object is
     *     {@link TItemDestinationPax }
     *     
     */
    public void setItemDestination(TItemDestinationPax value) {
        this.itemDestination = value;
    }

    /**
     * Gets the value of the immediateConfirmationOnly property.
     * 
     * @return
     *     possible object is
     *     {@link TEmpty }
     *     
     */
    public TEmpty getImmediateConfirmationOnly() {
        return immediateConfirmationOnly;
    }

    /**
     * Sets the value of the immediateConfirmationOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link TEmpty }
     *     
     */
    public void setImmediateConfirmationOnly(TEmpty value) {
        this.immediateConfirmationOnly = value;
    }

    /**
     * Gets the value of the itemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * Sets the value of the itemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemName(String value) {
        this.itemName = value;
    }

    /**
     * Gets the value of the itemCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * Sets the value of the itemCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemCode(String value) {
        this.itemCode = value;
    }

    /**
     * Gets the value of the itemCodes property.
     * 
     * @return
     *     possible object is
     *     {@link TItemCodes }
     *     
     */
    public TItemCodes getItemCodes() {
        return itemCodes;
    }

    /**
     * Sets the value of the itemCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link TItemCodes }
     *     
     */
    public void setItemCodes(TItemCodes value) {
        this.itemCodes = value;
    }

    /**
     * Gets the value of the periodOfStay property.
     * 
     * @return
     *     possible object is
     *     {@link TPeriodOfStay }
     *     
     */
    public TPeriodOfStay getPeriodOfStay() {
        return periodOfStay;
    }

    /**
     * Sets the value of the periodOfStay property.
     * 
     * @param value
     *     allowed object is
     *     {@link TPeriodOfStay }
     *     
     */
    public void setPeriodOfStay(TPeriodOfStay value) {
        this.periodOfStay = value;
    }

    /**
     * Gets the value of the includeRecommended property.
     * 
     * @return
     *     possible object is
     *     {@link TEmpty }
     *     
     */
    public TEmpty getIncludeRecommended() {
        return includeRecommended;
    }

    /**
     * Sets the value of the includeRecommended property.
     * 
     * @param value
     *     allowed object is
     *     {@link TEmpty }
     *     
     */
    public void setIncludeRecommended(TEmpty value) {
        this.includeRecommended = value;
    }

    /**
     * Gets the value of the recommendedOnly property.
     * 
     * @return
     *     possible object is
     *     {@link TEmpty }
     *     
     */
    public TEmpty getRecommendedOnly() {
        return recommendedOnly;
    }

    /**
     * Sets the value of the recommendedOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link TEmpty }
     *     
     */
    public void setRecommendedOnly(TEmpty value) {
        this.recommendedOnly = value;
    }

    /**
     * Gets the value of the includePriceBreakdown property.
     * 
     * @return
     *     possible object is
     *     {@link TEmpty }
     *     
     */
    public TEmpty getIncludePriceBreakdown() {
        return includePriceBreakdown;
    }

    /**
     * Sets the value of the includePriceBreakdown property.
     * 
     * @param value
     *     allowed object is
     *     {@link TEmpty }
     *     
     */
    public void setIncludePriceBreakdown(TEmpty value) {
        this.includePriceBreakdown = value;
    }

    /**
     * Gets the value of the includeChargeConditions property.
     * 
     * @return
     *     possible object is
     *     {@link TIncludeChargeConditions }
     *     
     */
    public TIncludeChargeConditions getIncludeChargeConditions() {
        return includeChargeConditions;
    }

    /**
     * Sets the value of the includeChargeConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link TIncludeChargeConditions }
     *     
     */
    public void setIncludeChargeConditions(TIncludeChargeConditions value) {
        this.includeChargeConditions = value;
    }

    /**
     * Gets the value of the excludeChargeableItems property.
     * 
     * @return
     *     possible object is
     *     {@link TExcludeChargeableItems }
     *     
     */
    public TExcludeChargeableItems getExcludeChargeableItems() {
        return excludeChargeableItems;
    }

    /**
     * Sets the value of the excludeChargeableItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link TExcludeChargeableItems }
     *     
     */
    public void setExcludeChargeableItems(TExcludeChargeableItems value) {
        this.excludeChargeableItems = value;
    }

    /**
     * Gets the value of the paxRooms property.
     * 
     * @return
     *     possible object is
     *     {@link TPaxRooms }
     *     
     */
    public TPaxRooms getPaxRooms() {
        return paxRooms;
    }

    /**
     * Sets the value of the paxRooms property.
     * 
     * @param value
     *     allowed object is
     *     {@link TPaxRooms }
     *     
     */
    public void setPaxRooms(TPaxRooms value) {
        this.paxRooms = value;
    }

    /**
     * Gets the value of the starRatingRange property.
     * 
     * @return
     *     possible object is
     *     {@link TStarRatingRange }
     *     
     */
    public TStarRatingRange getStarRatingRange() {
        return starRatingRange;
    }

    /**
     * Sets the value of the starRatingRange property.
     * 
     * @param value
     *     allowed object is
     *     {@link TStarRatingRange }
     *     
     */
    public void setStarRatingRange(TStarRatingRange value) {
        this.starRatingRange = value;
    }

    /**
     * Gets the value of the hotelTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelTypeCode() {
        return hotelTypeCode;
    }

    /**
     * Sets the value of the hotelTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelTypeCode(String value) {
        this.hotelTypeCode = value;
    }

}
