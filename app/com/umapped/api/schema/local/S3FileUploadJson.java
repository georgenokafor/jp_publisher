package com.umapped.api.schema.local;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * S3 Direct File Upload.
 *
 * Created by surge on 2014-11-24.
 */
@JsonTypeName("S3FILE")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class S3FileUploadJson {

  @JsonProperty(required = true, value = "signed_request")
  public String signed_request;
  @JsonProperty(required = true, value = "policy")
  public String policy;
  @JsonProperty(required = true, value = "accessKey")
  public String accessKey;
  @JsonProperty(required = true, value = "key")
  public String key;
  @JsonProperty(required = true, value = "s3Bucket")
  public String s3Bucket;
  @JsonProperty(required = true, value = "signature")
  public String signature;
}
