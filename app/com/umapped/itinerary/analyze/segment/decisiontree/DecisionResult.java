package com.umapped.itinerary.analyze.segment.decisiontree;

import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.recommendation.RecommendedItemType;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by wei on 2017-03-09.
 */
public class DecisionResult implements Serializable {
  private RecommendedItemType type;

  private List<Location> locations;
  private LocalDate startDate;
  private LocalDate endDate;

  public List<Location> getLocations() {
    return locations;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public DecisionResult() {

  }

  public DecisionResult(RecommendedItemType type, Location location, LocalDate date) {
    this.type = type;
    this.locations = new ArrayList<>();
    locations.add(location);
    this.startDate = date;
    this.endDate = date;
  }

  public DecisionResult(RecommendedItemType type, List<Location> locations, LocalDate date) {
    this(type, locations, date, date);
  }

  public DecisionResult(RecommendedItemType type, List<Location> locations, LocalDate start, LocalDate end) {
    this.type = type;
    this.locations = new ArrayList<>(locations);
    startDate = start;
    endDate = end;
  }

  public DecisionResult addLocation(Location location) {
   this.locations.add(location);
   return this;
  }

  public RecommendedItemType getType() {
    return type;
  }

  public DecisionResult setType(RecommendedItemType type) {
    this.type = type;
    return this;
  }

  public DecisionResult setLocations(List<Location> locations) {
    this.locations = locations;
    return this;
  }

  public DecisionResult setStartDate(LocalDate startDate) {
    this.startDate = startDate;
    return this;
  }

  public DecisionResult setEndDate(LocalDate endDate) {
    this.endDate = endDate;
    return this;
  }

  public String toString() {
    StringBuilder loc = new StringBuilder();
    for (Location location : locations) {
      loc.append(location == null ? ""  : location.getAddress().getLocality());
      loc.append("/");
    }
    return String.format("{ startDate = %s, endDate = %s, Location = %s }", startDate, endDate, loc.toString());
  }
}
