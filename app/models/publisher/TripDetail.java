package models.publisher;

import com.avaje.ebean.*;
import com.avaje.ebean.annotation.DbJsonB;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.utils.Log;
import com.umapped.persistence.enums.ReservationType;
import com.umapped.persistence.reservation.UmReservation;
import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;
import com.umapped.persistence.reservation.activity.UmActivityReservation;
import com.umapped.persistence.reservation.cruise.UmCruiseReservation;
import com.umapped.persistence.reservation.flight.UmFlightReservation;
import com.umapped.persistence.reservation.transfer.UmTransferReservation;

import models.publisher.utils.reservation.migration.TripDetailMigrator;
import models.utils.PersistenceUtilFactory;
import models.utils.migration.Migrator;
import play.data.validation.Constraints;

import javax.persistence.*;
import javax.persistence.Version;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-15
 * Time: 11:07 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "trip_detail")
public class TripDetail
    extends Model 
    implements JsonSupport
    {
  public static Model.Finder<String, TripDetail> find = new Finder<String, TripDetail>(TripDetail.class);

  public static Comparator<TripDetail> TripDetailComparator = (t1, t2) -> {
    long timestamp1 = t1.starttimestamp;
    long timestamp2 = t2.starttimestamp;

    if (timestamp1 > 0) {
      Calendar cal = Calendar.getInstance();
      cal.setTimeInMillis(timestamp1);
      if (cal.get(Calendar.HOUR_OF_DAY) == 0 && cal.get(Calendar.MINUTE) == 0) {
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        timestamp1 = cal.getTimeInMillis();
      }
    }

    if (timestamp2 > 0) {
      Calendar cal = Calendar.getInstance();
      cal.setTimeInMillis(timestamp2);
      if (cal.get(Calendar.HOUR_OF_DAY) == 0 && cal.get(Calendar.MINUTE) == 0) {
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        timestamp2 = cal.getTimeInMillis();
      }
    }

    if (timestamp1 == timestamp2) {
      return 0;
    }
    else if (timestamp1 > timestamp2) {
      return 1;
    }
    else {
      return -1;
    }
  };
  public String               comments;
  public  String               bookingnumber;
  public  int                  rank;
  @Enumerated(value = EnumType.STRING)
  public  BookingSrc.ImportSrc importSrc;
  public  String               importSrcId;
  public  long                 importTs;
  public  Long                 bkApiSrcId;
  @Id
  public  String               detailsid;
  @Constraints.Required
  public  String               tripid;
  public  int                  status;
  public  int                  triptype;
  @Enumerated(EnumType.ORDINAL)
  public  ReservationType      detailtypeid;
  public  Long                 starttimestamp;
  public  Long                 endtimestamp;
  public  Long                 createdtimestamp;
  public  Long                 lastupdatedtimestamp;
  public  String               createdby;
  public  String               modifiedby;
  public  String               tag;
  @Version
  public  int                  version;
  private String               recordLocator;
  private int                  passengerCount;
  private String               locStartName;
  private float                locStartLong;
  private float                locStartLat;
  private String               locFinishName;
  private float        locFinishLong;
  private float        locFinishLat;
  private Long         poiId;
  private Integer      poiCmpyId;
  private Long         locStartPoiId;
  private Integer      locStartPoiCmpyId;
  private Long         locFinishPoiId;
  private Integer      locFinishPoiCmpyId;
  private String       name;
  private String       extraDetail;
  @Transient
  private ExtraDetails extraDetails;

  @DbJsonB
  private Map<String, Object>    reservationData;

  @Transient
  private UmReservation reservation;

  /**
   * Takes this object makes it possible to re-insert as new record
   * @return
   */
  public TripDetail prepareForReuse(String createdBy, String modifiedBy) {
    BeanState state = Ebean.getBeanState(this);
    state.resetForInsert();
    setDetailsid(DBConnectionMgr.getUniqueId());
    setModifiedby(modifiedBy);
    setCreatedby(createdBy);
    setLastupdatedtimestamp(System.currentTimeMillis());
    setCreatedtimestamp(lastupdatedtimestamp);
    return this;
  }


  /**
   * Extra Details to a booking reservation
   */
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class ExtraDetails {
    /**
     * Disable photos hides all
     */
    public Boolean showPhotos = null;
  }

  public int getPoiUseCount(Long poiId, Integer cmpyId) {
    return find.where()
               .or(Expr.or(Expr.and(Expr.eq("poi_id", poiId), Expr.eq("poi_cmpy_id", cmpyId)),
                           Expr.and(Expr.eq("loc_start_poi_id", poiId), Expr.eq("loc_start_poi_cmpy_id", cmpyId))),
                   Expr.and(Expr.eq("loc_finish_poi_id", poiId), Expr.eq("loc_finish_poi_cmpy_id", cmpyId)))
               .findCount();
  }

  public static int mergeCmpy(Integer srcCmpyId, Integer targetCmpyId, String userId) {
    int recUpdated = 0;
    String poiCmpyMain = "UPDATE trip_detail set poi_cmpy_id = :targetCmpyId, lastupdatedtimestamp = :timestamp, " +
                         "" + "modifiedby = :userid  where poi_cmpy_id = :srcCmpyId and status = 0";
    String poiCmpyStart = "UPDATE trip_detail set loc_start_poi_cmpy_id = :targetCmpyId, " +
                          "lastupdatedtimestamp = :timestamp, " + "modifiedby = :userid  where loc_start_poi_cmpy_id " +
                          "= :srcCmpyId and status = 0";
    String poiCmpyFinish = "UPDATE trip_detail set loc_finish_poi_cmpy_id = :targetCmpyId, " +
                           "lastupdatedtimestamp = :timestamp, " + "modifiedby = :userid  where " +
                           "loc_finish_poi_cmpy_id = :srcCmpyId and status = 0";

    SqlUpdate upMain = Ebean.createSqlUpdate(poiCmpyMain);
    upMain.setParameter("timestamp", System.currentTimeMillis());
    upMain.setParameter("userid", userId);
    upMain.setParameter("srcCmpyId", srcCmpyId);
    upMain.setParameter("targetCmpyId", targetCmpyId);

    recUpdated += Ebean.execute(upMain);

    SqlUpdate upStart = Ebean.createSqlUpdate(poiCmpyStart);
    upStart.setParameter("timestamp", System.currentTimeMillis());
    upStart.setParameter("userid", userId);
    upStart.setParameter("srcCmpyId", srcCmpyId);
    upStart.setParameter("targetCmpyId", targetCmpyId);

    recUpdated += Ebean.execute(upStart);

    SqlUpdate upFinish = Ebean.createSqlUpdate(poiCmpyFinish);
    upFinish.setParameter("timestamp", System.currentTimeMillis());
    upFinish.setParameter("userid", userId);
    upFinish.setParameter("srcCmpyId", srcCmpyId);
    upFinish.setParameter("targetCmpyId", targetCmpyId);

    recUpdated += Ebean.execute(upFinish);

    return recUpdated;
  }

  /**
   * Get all trip_details records to be converted
   *
   * @return
   */
  public static List<TripDetail> findAll() {
    return find.all();
  }

  public static TripDetail findByPK(String pk) {
    return find.byId(pk);
  }

  /**
   * Creates a new active TripDetail record and initializes it based on incoming parameters
   *
   * @param userProfileId Existing user profile id
   * @param tripId        Existing trip id (trip must exist by the time save() function is called)
   * @param bookingType   Booking type (i.e. APPConstants.FLIGHT_BOOKING_TYPE)
   * @return new record that is not yet persisted
   */
  public static TripDetail build(String userProfileId, String tripId, ReservationType bookingType) {
    TripDetail tripDetail = new TripDetail();
    tripDetail.setCreatedby(userProfileId);
    tripDetail.setCreatedtimestamp(System.currentTimeMillis());
    tripDetail.setDetailsid(DBConnectionMgr.getUniqueId());
    tripDetail.setLastupdatedtimestamp(tripDetail.createdtimestamp);
    tripDetail.setModifiedby(tripDetail.createdby);
    tripDetail.setDetailtypeid(bookingType);
    tripDetail.setLocStartLat(0.0f);
    tripDetail.setLocStartLong(0.0f);
    tripDetail.setLocFinishLat(0.0f);
    tripDetail.setLocFinishLong(0.0f);
    tripDetail.setStatus(APPConstants.STATUS_ACTIVE);
    tripDetail.setTripid(tripId);
    tripDetail.setTriptype(0);
    switch (bookingType) {
      case FLIGHT:
        tripDetail.setReservation(new UmFlightReservation());
        break;
      case HOTEL:
        tripDetail.setReservation(new UmAccommodationReservation());
        break;
      case CRUISE:
        tripDetail.setReservation(new UmCruiseReservation());
        break;
      case ACTIVITY:
        tripDetail.setReservation(new UmActivityReservation());
        break;
      case TRANSPORT:
        tripDetail.setReservation(new UmTransferReservation());
        break;
    }
    // the type is a sub type, therefore needs to be set properly
    if (tripDetail.getReservation() == null) {
      if (ReservationType.ACTIVITY == bookingType.getParent()) {
        tripDetail.setReservation(new UmActivityReservation());
      } else if (ReservationType.TRANSPORT == bookingType.getParent()) {
        tripDetail.setReservation(new UmTransferReservation());
      }
    }
    return tripDetail;
  }

  public static List<TripDetail> findCruiseStopsByDateRange(String tripId, long starttimestamp, long endtimestamp) {
    List<TripDetail> details = find.where()
                                   .eq("tripid", tripId)
                                   .eq("detailtypeid", ReservationType.CRUISE_STOP)
                                   .ge("starttimestamp",starttimestamp)
                                   .le("starttimestamp",endtimestamp)
                                   .eq("status", 0)
                                   .orderBy("starttimestamp asc")
                                   .findList();
    Collections.sort(details, TripDetail.TripDetailComparator);
    return details;
  }


  public static List<TripDetail> findActiveByTripId(String tripId) {
    List<TripDetail> details = find.where()
                                   .eq("tripid", tripId)
                                   .eq("status", 0)
                                   .orderBy("starttimestamp asc")
                                   .findList();
    Collections.sort(details, TripDetail.TripDetailComparator);
    return details;
  }


  public static List<TripDetail> findActiveByTripIdandDateRange(String tripId, Long timeStart, Long timeEnd) {
    ExpressionList<TripDetail> sqlExpression = find.where()
                                                   .eq("tripid", tripId)
                                                   .eq("status", APPConstants.STATUS_ACTIVE);

    if (timeStart != null && timeStart > 0) {
      sqlExpression.ge("starttimestamp", timeStart);
    }

    if (timeEnd != null && timeEnd > 0) {
      sqlExpression.le("endtimestamp", timeEnd);
    }

    return sqlExpression.orderBy("starttimestamp asc").findList();
  }

  public static List<TripDetail> findActiveByTripIdandStartTimestamp(String tripId, long starttimestamp) {
    return find.where()
               .eq("tripid", tripId)
               .eq("status", 0)
               .eq("starttimestamp", starttimestamp)
               .orderBy("createdtimestamp desc")
               .findList();
  }


  public static List<TripDetail> findActiveByTripIdAndConfirmation(String tripId, String bookingnumber,
                                                                   ReservationType triptype) {

    if (triptype == ReservationType.ACTIVITY) {
      List<Integer> types = new ArrayList<>();
      types.add(ReservationType.ACTIVITY.getIdx());
      types.add(ReservationType.CRUISE_STOP.getIdx());
      types.add(ReservationType.EVENT.getIdx());
      types.add(ReservationType.RESTAURANT.getIdx());
      types.add(ReservationType.SHORE_EXCURSION.getIdx());
      types.add(ReservationType.TOUR.getIdx());
      types.add(ReservationType.SKI_LESSONS.getIdx());
      types.add(ReservationType.SKI_RENTALS.getIdx());
      types.add(ReservationType.SKI_LIFT.getIdx());



      return find.where()
                 .eq("tripid", tripId)
                 .eq("status", APPConstants.STATUS_ACTIVE)
                 .eq("bookingnumber", bookingnumber)
                 .in("detailtypeid", types)
                 .orderBy("createdtimestamp desc")
                 .findList();
    } else if (triptype == ReservationType.TRANSPORT) {
      List<Integer> types = new ArrayList<>();
      types.add(ReservationType.TRANSPORT.getIdx());
      types.add(ReservationType.TAXI.getIdx());
      types.add(ReservationType.PRIVATE_TRANSFER.getIdx());
      types.add(ReservationType.PUBLIC_TRANSPORT.getIdx());
      types.add(ReservationType.BUS.getIdx());
      types.add(ReservationType.CAR_RENTAL.getIdx());
      types.add(ReservationType.CAR_DROPOFF.getIdx());
      types.add(ReservationType.RAIL.getIdx());

      return find.where()
                 .eq("tripid", tripId)
                 .eq("status", APPConstants.STATUS_ACTIVE)
                 .eq("bookingnumber", bookingnumber)
                 .in("detailtypeid", types)
                 .orderBy("createdtimestamp desc")
                 .findList();

    } else {
      return find.where()
                 .eq("tripid", tripId)
                 .eq("status", APPConstants.STATUS_ACTIVE)
                 .eq("bookingnumber", bookingnumber)
                 .eq("detailtypeid", triptype.getIdx())
                 .orderBy("createdtimestamp desc")
                 .findList();
    }
  }


      public static List<TripDetail> findActiveByTripIdandSrcID(String tripId,
                                                                       Long bkApiSrcId,
                                                                       ReservationType triptype) {

        if (triptype == ReservationType.ACTIVITY) {
          List<Integer> types = new ArrayList<>();
          types.add(ReservationType.ACTIVITY.getIdx());
          types.add(ReservationType.CRUISE_STOP.getIdx());
          types.add(ReservationType.EVENT.getIdx());
          types.add(ReservationType.RESTAURANT.getIdx());
          types.add(ReservationType.SHORE_EXCURSION.getIdx());
          types.add(ReservationType.TOUR.getIdx());
          types.add(ReservationType.SKI_LESSONS.getIdx());
          types.add(ReservationType.SKI_RENTALS.getIdx());
          types.add(ReservationType.SKI_LIFT.getIdx());


          return find.where()
                     .eq("tripid", tripId)
                     .eq("status", APPConstants.STATUS_ACTIVE)
                     .eq("bkApiSrcId", bkApiSrcId)
                     .in("detailtypeid", types)
                     .orderBy("createdtimestamp desc")
                     .findList();
        }
        else if (triptype == ReservationType.TRANSPORT) {
          List<Integer> types = new ArrayList<>();
          types.add(ReservationType.TRANSPORT.getIdx());
          types.add(ReservationType.TAXI.getIdx());
          types.add(ReservationType.PRIVATE_TRANSFER.getIdx());
          types.add(ReservationType.PUBLIC_TRANSPORT.getIdx());
          types.add(ReservationType.BUS.getIdx());
          types.add(ReservationType.CAR_RENTAL.getIdx());
          types.add(ReservationType.CAR_DROPOFF.getIdx());
          types.add(ReservationType.RAIL.getIdx());

          return find.where()
                     .eq("tripid", tripId)
                     .eq("status", APPConstants.STATUS_ACTIVE)
                     .eq("bkApiSrcId", bkApiSrcId)
                     .in("detailtypeid", types)
                     .orderBy("createdtimestamp desc")
                     .findList();

        }
        else {
          return find.where()
                     .eq("tripid", tripId)
                     .eq("status", APPConstants.STATUS_ACTIVE)
                     .eq("bkApiSrcId", bkApiSrcId)
                     .eq("detailtypeid", triptype.getIdx())
                     .orderBy("createdtimestamp desc")
                     .findList();
        }
      }




      public static List<TripDetail> findActiveByStartTimestampAndType(String tripId,
                                                                   long starttimestamp,
                                                                   ReservationType triptype) {
    if (triptype == ReservationType.ACTIVITY) {
      List<Integer> types = new ArrayList<>();
      types.add(ReservationType.ACTIVITY.getIdx());
      types.add(ReservationType.CRUISE_STOP.getIdx());
      types.add(ReservationType.EVENT.getIdx());
      types.add(ReservationType.RESTAURANT.getIdx());
      types.add(ReservationType.SHORE_EXCURSION.getIdx());
      types.add(ReservationType.TOUR.getIdx());
      types.add(ReservationType.SKI_LESSONS.getIdx());
      types.add(ReservationType.SKI_RENTALS.getIdx());
      types.add(ReservationType.SKI_LIFT.getIdx());



      return find.where()
                 .eq("tripid", tripId)
                 .eq("status", APPConstants.STATUS_ACTIVE)
                 .eq("starttimestamp", starttimestamp)
                 .in("detailtypeid", types)
                 .orderBy("createdtimestamp desc")
                 .findList();
    } else if (triptype == ReservationType.TRANSPORT) {
      List<Integer> types = new ArrayList<>();
      types.add(ReservationType.TRANSPORT.getIdx());
      types.add(ReservationType.TAXI.getIdx());
      types.add(ReservationType.PRIVATE_TRANSFER.getIdx());
      types.add(ReservationType.PUBLIC_TRANSPORT.getIdx());
      types.add(ReservationType.BUS.getIdx());
      types.add(ReservationType.CAR_RENTAL.getIdx());
      types.add(ReservationType.CAR_DROPOFF.getIdx());
      types.add(ReservationType.RAIL.getIdx());

      return find.where()
                 .eq("tripid", tripId)
                 .eq("status", APPConstants.STATUS_ACTIVE)
                 .eq("starttimestamp", starttimestamp)
                 .in("detailtypeid", types)
                 .orderBy("createdtimestamp desc")
                 .findList();

    } else {
      return find.where()
                 .eq("tripid", tripId)
                 .eq("status", APPConstants.STATUS_ACTIVE)
                 .eq("starttimestamp", starttimestamp)
                 .eq("detailtypeid", triptype.getIdx())
                 .orderBy("createdtimestamp desc")
                 .findList();
    }
  }

  public static int numberOfBookings(String tripId) {
    return find.where().eq("tripid", tripId).eq("status", APPConstants.STATUS_ACTIVE).findCount();
  }

  @Transient
  public ExtraDetails getExtraDetails() {
    if (extraDetails == null && extraDetail != null && extraDetail.length() > 0) {
      ObjectMapper om = new ObjectMapper();
      try {
        extraDetails = om.readValue(extraDetail, ExtraDetails.class);
      }
      catch (IOException e) {
        Log.err("Trip Detail failure unmarshaling JSON extra details");
        e.printStackTrace();
      }
    }
    return extraDetails;
  }

  @Transient
  public void setExtraDetails(ExtraDetails extraDetails) {
    this.extraDetails = extraDetails;
    saveExtraDetails();
  }

  public void saveExtraDetails() {
    ObjectMapper jsonMapper = new ObjectMapper();
    try {
      extraDetail = jsonMapper.writeValueAsString(extraDetails);
    } catch (Exception e) {
      Log.err("Failed to marshal Booking Extra Details to String");
      e.printStackTrace();
    }
  }

  public static List<TripDetail> findByAPIPk (String tripid, List<Long> bookingPks) {
    return find.where().eq("tripid", tripid)
        .in("bk_api_src_id", bookingPks)
        .ne("status", -1)
        .findList();
  }

  public String getExtraDetail() {
    return extraDetail;
  }

  public void setExtraDetail(String extraDetail) {
    this.extraDetail = extraDetail;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getLocFinishPoiCmpyId() {
    return locFinishPoiCmpyId;
  }

  public void setLocFinishPoiCmpyId(Integer locFinishPoiCmpyId) {
    this.locFinishPoiCmpyId = locFinishPoiCmpyId;
  }

  public Long getLocStartPoiId() {
    return locStartPoiId;
  }

  public void setLocStartPoiId(Long locStartPoiId) {
    this.locStartPoiId = locStartPoiId;
  }

  public Integer getLocStartPoiCmpyId() {
    return locStartPoiCmpyId;
  }

  public void setLocStartPoiCmpyId(Integer locStartPoiCmpyId) {
    this.locStartPoiCmpyId = locStartPoiCmpyId;
  }

  public Long getLocFinishPoiId() {
    return locFinishPoiId;
  }

  public void setLocFinishPoiId(Long locFinishPoiId) {
    this.locFinishPoiId = locFinishPoiId;
  }

  public Long getPoiId() {
    return poiId;
  }

  public void setPoiId(Long poiId) {
    this.poiId = poiId;
  }

  public Integer getPoiCmpyId() {
    return poiCmpyId;
  }

  public void setPoiCmpyId(Integer poiCmpyId) {
    this.poiCmpyId = poiCmpyId;
  }

  public float getLocFinishLong() {
    return locFinishLong;
  }

  public void setLocFinishLong(float locFinishLong) {
    this.locFinishLong = locFinishLong;
  }

  public float getLocFinishLat() {
    return locFinishLat;
  }

  public void setLocFinishLat(float locFinishLat) {
    this.locFinishLat = locFinishLat;
  }

  public String getLocStartName() {
    return locStartName;
  }

  public void setLocStartName(String locStartName) {
    this.locStartName = locStartName;
  }

  public String getLocFinishName() {
    return locFinishName;
  }

  public void setLocFinishName(String locFinishName) {
    this.locFinishName = locFinishName;
  }

  public String getRecordLocator() {
    return recordLocator;
  }

  public void setRecordLocator(String recordLocator) {
    this.recordLocator = recordLocator;
  }

  public int getPassengerCount() {
    return passengerCount;
  }

  public void setPassengerCount(int passengerCount) {
    this.passengerCount = passengerCount;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getBookingnumber() {
    return bookingnumber;
  }

  public void setBookingnumber(String bookingnumber) {
    this.bookingnumber = bookingnumber;
  }

  public String getDetailsid() {
    return detailsid;
  }

  public void setDetailsid(String detailsid) {
    this.detailsid = detailsid;
  }

  public String getTripid() {
    return tripid;
  }

  public void setTripid(String tripid) {
    this.tripid = tripid;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public int getTriptype() {
    return triptype;
  }

  public void setTriptype(int triptype) {
    this.triptype = triptype;
  }

  public ReservationType getDetailtypeid() {
    return detailtypeid;
  }

  public void setDetailtypeid(ReservationType detailtypeid) {
    this.detailtypeid = detailtypeid;
  }

  public float getLocStartLong() {
    return locStartLong;
  }

  public void setLocStartLong(float loclong) {
    this.locStartLong = loclong;
  }

  public float getLocStartLat() {
    return locStartLat;
  }

  public void setLocStartLat(float loclat) {
    this.locStartLat = loclat;
  }

  public Long getStarttimestamp() {
    return starttimestamp;
  }

  public void setStarttimestamp(Long starttimestamp) {
    this.starttimestamp = starttimestamp;
  }

  /**
   * Convenience method
   *
   * @param ts
   */
  public void setStartTimestamp(Timestamp ts) {
    if (ts == null) {
      this.starttimestamp = 0l;
    }
    else {
      this.starttimestamp = ts.getTime();
    }
  }

  /**
   * Convenience method with logical checks to ensure proper data is written
   *
   * @param ts
   */
  public void setEndTimestamp(Timestamp ts) {
    if (ts == null) {
      this.endtimestamp = starttimestamp;
    }
    else {
      this.endtimestamp = ts.getTime();
    }
  }

  public Long getEndtimestamp() {
    return endtimestamp;
  }

  public void setEndtimestamp(Long endtimestamp) {
    this.endtimestamp = endtimestamp;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  public BookingSrc.ImportSrc getImportSrc() {
    return importSrc;
  }

  public void setImportSrc(BookingSrc.ImportSrc importSrc) {
    this.importSrc = importSrc;
  }

  public String getImportSrcId() {
    return importSrcId;
  }

  public void setImportSrcId(String importSrcId) {
    this.importSrcId = importSrcId;
  }

  public long getImportTs() {
    return importTs;
  }

  public void setImportTs(long importTs) {
    this.importTs = importTs;
  }

  public Long getBkApiSrcId() {
    return bkApiSrcId;
  }

  public void setBkApiSrcId(Long bkApiSrcId) {
    this.bkApiSrcId = bkApiSrcId;
  }

  public Map<String, Object> getReservationData() {
    return reservationData;
  }

  public void setReservationData(Map<String, Object> reservationData) {
    this.reservationData = reservationData;
  }
  
  public UmReservation getReservation() {
    if (reservation == null) {
      if (reservationData != null) {
        reservation = unmarshal(reservationData, UmReservation.class);
      }
    }
    return reservation;
  }
  
  public void setReservation(UmReservation reservation) {
    this.reservation = reservation;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
  
  /**
   * This is sync the transient reservation back to the persisted field reservationData
   * before the persiste happends
   * 
   * @throws IOException
   */
  @PrePersist
  @PreUpdate
  public void preparePersist() throws IOException { 
    if (reservation != null) {
      reservation.setStartDateTime(getStarttimestamp());
      reservation.setEndDateTime(getEndtimestamp());
      reservationData = marshal(reservation);
    }
  }
    
  @SuppressWarnings("unchecked")
  @PostLoad
  public void migrationIfNeeded() throws IOException {
   Migrator<TripDetail> m = (Migrator<TripDetail>) PersistenceUtilFactory.getMigrator(TripDetail.class);
   m.migrate(this);
  }
}

