package com.mapped.publisher.parse.virtuoso.cruise.amenities.cruiseinfo;
//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Any modifications to this file will be lost upon recompilation of the source schema.
// Generated on: 2015.06.23 at 01:14:06 PM EDT
//


import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.mapped.publisher.parse.virtuoso.cruise.cruiseinfo package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {


  /**
   * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com
   * .mapped.publisher.parse.virtuoso.cruise.cruiseinfo
   */
  public ObjectFactory() {
  }

  /**
   * Create an instance of {@link CruiseInfo }
   */
  public CruiseInfo createCruiseInfo() {
    return new CruiseInfo();
  }

  /**
   * Create an instance of {@link CruiseInfo.SpecialAmenities }
   */
  public CruiseInfo.SpecialAmenities createCruiseInfoSpecialAmenities() {
    return new CruiseInfo.SpecialAmenities();
  }

  /**
   * Create an instance of {@link CruiseInfo.VoyagerClub }
   */
  public CruiseInfo.VoyagerClub createCruiseInfoVoyagerClub() {
    return new CruiseInfo.VoyagerClub();
  }

  /**
   * Create an instance of {@link CruiseInfo.SpecialAmenities.SpecialAmenity }
   */
  public CruiseInfo.SpecialAmenities.SpecialAmenity createCruiseInfoSpecialAmenitiesSpecialAmenity() {
    return new CruiseInfo.SpecialAmenities.SpecialAmenity();
  }

  /**
   * Create an instance of {@link CruiseInfo.VoyagerClub.VoyagerClubOption }
   */
  public CruiseInfo.VoyagerClub.VoyagerClubOption createCruiseInfoVoyagerClubVoyagerClubOption() {
    return new CruiseInfo.VoyagerClub.VoyagerClubOption();
  }

  /**
   * Create an instance of {@link CruiseInfo.VoyagerClub.VoyagerClubEvent }
   */
  public CruiseInfo.VoyagerClub.VoyagerClubEvent createCruiseInfoVoyagerClubVoyagerClubEvent() {
    return new CruiseInfo.VoyagerClub.VoyagerClubEvent();
  }

}