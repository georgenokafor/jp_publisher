package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.InputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Serguei Moutovkin on 2014-07-21.
 * <p/>
 * Parsers below are based on the following assumptions
 * <p/>
 */
public class ClientbaseExtractor
    extends BookingExtractor {
  private static final boolean DEBUG = false; //Compiler will remove all if(DEBUG) statements while it is set to false

  public enum SectionHeaderKeyword implements Comparable<SectionHeaderKeyword> {
    CONFIRMATION_NO("Confirmation No."),
    RECORD_LOCATOR("Record Locator"),
    BOOKING_STATUS("Booking Status"),
    NO_OF_PASSENGER("No. of Passengers"),
    START_DATE("Start Date"),
    END_DATE("End Date"),
    PASSENGERS("Passengers"),
    NO_OF_CARS("No. of Cars"), //Cars specific
    VENDOR("Vendor"),
    DURATION("Duration"), //Cruise specific?
    NO_OF_UNITS("No. of Units"),
    NO_OF_CABINS("No. of Cabins"),
    NO_OF_NIGHTS("No. of Nights"),
    NO_OF_ROOMS("No. of Rooms"),
    PARSER_COMMENTS("XYZ_COMMENT_123"); //Keyword which will store notes for the header

    private static SectionHeaderKeyword printableValues[] = {VENDOR, CONFIRMATION_NO, BOOKING_STATUS, NO_OF_PASSENGER,
                                                             NO_OF_CARS, NO_OF_CABINS, NO_OF_NIGHTS, NO_OF_ROOMS,
                                                             NO_OF_UNITS, RECORD_LOCATOR};
    private static List<SectionHeaderKeyword> printableList = new ArrayList<SectionHeaderKeyword>(Arrays.asList(printableValues));
    private String token;
    private SectionHeaderKeyword(String token) {
      this.token = token;
    }

    public static SectionHeaderKeyword isKeyword(String kw) {
      for(SectionHeaderKeyword kWord : SectionHeaderKeyword.values()) {
        if (kWord.token.compareTo(kw) == 0) {
          return kWord;
        }
      }
      return null;
    }

    public String getKeywordText() {
      return this.token;
    }

    public static Comparator<SectionHeaderKeyword> keywordComparator = (o1, o2) -> o2.getKeywordText().length() - o1.getKeywordText().length();

    public static List<SectionHeaderKeyword> getPrintableValues() {
      return printableList;
    }
  };

  public enum CarKeyword {
    CAR_PICK_UP("Pick-up"),
    CAR_DROP_OFF("Drop-off"),
    CAR_CATEGORY("Category"),
    CAR_PICK_UP_CITY("Pick-up City"),
    CAR_DROP_OFF_CITY("Drop-off City"),
    CAR_DESCRIPTION("Description");

    private String token;
    private CarKeyword(String token) {
      this.token = token;
    }

    public static CarKeyword isKeyword(String kw) {
      for(CarKeyword kWord : CarKeyword.values()) {
        if (kWord.token.compareTo(kw) == 0) {
          return kWord;
        }
      }
      return null;
    }

    public String getKeywordText() {
      return this.token;
    }
  };

  public enum HotelKeyword {
    H_CHECK_IN("Check-in"),
    H_CHECK_OUT("Check-out"),
    H_DESCRIPTION("Description"),
    H_ROOM_TYPE("Room Type"),
    H_BEDDING("Bedding"),
    H_SMOKING("Smoking");

    private String token;
    private static HotelKeyword[] starterArray = {H_CHECK_IN, H_CHECK_OUT};
    private static List<HotelKeyword> starterList = new ArrayList<HotelKeyword>(Arrays.asList(starterArray));

    private HotelKeyword(String token) {
      this.token = token;
    }

    public static HotelKeyword isKeyword(String kw) {
      for(HotelKeyword kWord : HotelKeyword.values()) {
        if (kWord.token.compareTo(kw) == 0) {
          return kWord;
        }
      }
      return null;
    }

    public String getKeywordText() {
      return this.token;
    }

    public static List<HotelKeyword> getStarters() {
      return starterList;
    }
  }


  public enum CruiseKeyword {
    CR_REAL_START_DATE("Start Date"),
    CR_REAL_END_DATE("End Date"),
    CR_CATEGORY("Category"),
    CR_CABIN_ROOM("Cabin/Room"),
    CR_DINING("Dining"),
    CR_BEDDING("Bedding"),
    CR_DECK("Deck"),
    CR_DESCRIPTION("Description");

    private static CruiseKeyword[] starterArray = {CR_REAL_END_DATE, CR_REAL_START_DATE};
    private static List<CruiseKeyword> starterList = new ArrayList<CruiseKeyword>(Arrays.asList(starterArray));
    private String token;

    private CruiseKeyword(String token) {
      this.token = token;
    }

    public static CruiseKeyword isKeyword(String kw) {
      for (CruiseKeyword kWord : CruiseKeyword.values()) {
        if (kWord.token.compareTo(kw) == 0) {
          return kWord;
        }
      }
      return null;
    }

    public static List<CruiseKeyword> getStarters() {
      return starterList;
    }

    public String getKeywordText() {
      return this.token;
    }
  }

  public enum FlightKeyword {
    FL_AIRCRAFT_TYPE("Aircraft Type"),
    FL_MEAL("Meal"),
    FL_FLIGHT_DURATION("Flight Duration"),
    FL_LOCATOR("Locator"),
    FL_MILEAGE("Mileage"),
    FL_CLASS("Class"),      //Will not treat them as keywords for now as they go to the comment
    FL_STATUS("Status"),    //Will not treat them as keywords for now as they go to the comment
    FL_SEAT_NO("Seat No."), //Will not treat them as keywords for now as they go to the comment
    FL_SEAT_TYPE("Seat Type"),
    FL_DESCRIPTION("Description"),
    FL_MISCELLANEOUS("Miscellaneous");

    private String token;
    private FlightKeyword(String token) {
      this.token = token;
    }

    public static FlightKeyword isKeyword(String kw) {
      for(FlightKeyword kWord : FlightKeyword.values()) {
        if (kWord.token.compareTo(kw) == 0) {
          return kWord;
        }
      }
      return null;
    }

    public String getKeywordText() {
      return this.token;
    }
  };

  public enum RailKeyword {
    RAIL("UNKNOWN"); //Keywords are not defined for Rail

    private String token;
    private RailKeyword(String token) {
      this.token = token;
    }

    public static RailKeyword isKeyword(String kw) {
      for(RailKeyword kWord : RailKeyword.values()) {
        if (kWord.token.compareTo(kw) == 0) {
          return kWord;
        }
      }
      return null;
    }

    public String getKeywordText() {
      return this.token;
    }
  };

  public enum MiscKeyword {
    MISC_REAL_START_DATE("Start Date"),
    MISC_REAL_END_DATE("End Date"),
    MISC_DESCRIPTION("Description");

    private String token;
    private static MiscKeyword[] starterArray = {MISC_REAL_START_DATE, MISC_REAL_END_DATE};
    private static List<MiscKeyword> starterList = new ArrayList<MiscKeyword>(Arrays.asList(starterArray));

    private MiscKeyword(String token) {
      this.token = token;
    }

    public static MiscKeyword isKeyword(String kw) {
      for(MiscKeyword kWord : MiscKeyword.values()) {
        if (kWord.token.compareTo(kw) == 0) {
          return kWord;
        }
      }
      return null;
    }

    public String getKeywordText() {
      return this.token;
    }

    public static List<MiscKeyword> getStarters() {
      return starterList;
    }
  };


  public enum TourKeyword {
    TR_REAL_START_DATE("Start Date"),
    TR_REAL_END_DATE("End Date"),
    TR_TOUR_NAME("Tour Name"),
    TR_DEPART("Depart"),
    TR_ARRIVE("Arrive"),
    TR_DESCRIPTION("Description");

    private String token;
    private static TourKeyword[] starterArray = {TR_REAL_END_DATE, TR_REAL_START_DATE};
    private static List<TourKeyword> starterList = new ArrayList<TourKeyword>(Arrays.asList(starterArray));


    private TourKeyword(String token) {
      this.token = token;
    }

    public static TourKeyword isKeyword(String kw) {
      for(TourKeyword kWord : TourKeyword.values()) {
        if (kWord.token.compareTo(kw) == 0) {
          return kWord;
        }
      }
      return null;
    }

    public String getKeywordText() {
      return this.token;
    }

    public static List<TourKeyword> getStarters() {
      return starterList;
    }
  };

  public enum TransportKeyword {
    TRANS_PICK_UP("Pick-up"),
    TRANS_DROP_OFF("Drop-off"),
    TRANS_PICK_UP_CITY("Pick-up City"),
    TRANS_DROP_OFF_CITY("Drop-off City"),
    TRANS_DESCRIPTION("Description"),
    TRANS_CATEGORY("Category");

    private String token;
    private static TransportKeyword[] starterArray = {TRANS_PICK_UP, TRANS_DROP_OFF};
    private static List<TransportKeyword> starterList = new ArrayList<TransportKeyword>(Arrays.asList(starterArray));

    private TransportKeyword(String token) {
      this.token = token;
    }

    public static TransportKeyword isKeyword(String kw) {
      for(TransportKeyword kWord : TransportKeyword.values()) {
        if (kWord.token.compareTo(kw) == 0) {
          return kWord;
        }
      }
      return null;
    }

    public String getKeywordText() {
      return this.token;
    }

    public static List<TransportKeyword> getStarters() {
      return starterList;
    }
  };


  public enum ClientbaseInvoiceCategory {
    HOTEL_RESERVATION,
    AIR_RESERVATION,
    TOUR_RESERVATION,
    RAIL_RESERVATION,
    TRANSPORT_RESERVATION,
    CRUISE_RESERVATION,
    MISCELLANEOUS,
    INSURANCE,
    CAR_RESERVATION,
    AGENCY_FEE
  };

  private static String nl = System.getProperty("line.separator");
  private static final Map<String, ClientbaseInvoiceCategory> sectionsMap = new HashMap<>();
  private static final Map<String, ClientbaseInvoiceCategory> sectionBookingsMap = new HashMap<>();
  private static final ArrayList<String> sectionEndersList = new ArrayList<>();
  private static Pattern sectionsSpitterPattern;
  private static Pattern sectionBookingsPattern;
  /**
   * Pattern defines works that end description part of the section usually invoice items and amounts
   */
  private static Pattern sectionEndPattern;


  private static String[] hotelNameHotWords = {"hotel", "inn", "suites", "&", "resort", "best", "holiday", "western",
                                               "marriott", "spa", "express", "beach", "comfort", "airport", "lodge",
                                               "super", "city", "hampton", "park", "grand", "hilton", "motel",
                                               "plaza", "quality", "garden", "center", "villa", "ibis", "palace",
                                               "residence", "house", "san", "international", "le", "courtyard",
                                               "boutique", "econo", "quinta", "americas", "royal", "ramada",
                                               "fairfield", "mercure", "wyndham", "downtown", "travelodge",
                                               "conference", "sheraton", "del", "america", "red", "village",
                                               "business", "villas", "radisson", "golf", "hyatt", "place", "golden",
                                               "valley", "rodeway", "casa", "novotel"};
  private static Pattern hotelHotWordPattern;
  /**
   * Static Element Initialization
   */
  static {

    sectionsMap.put("Hotel Reservation", ClientbaseInvoiceCategory.HOTEL_RESERVATION);
    sectionsMap.put("Air Reservation", ClientbaseInvoiceCategory.AIR_RESERVATION);
    sectionsMap.put("Tour Reservation", ClientbaseInvoiceCategory.TOUR_RESERVATION);
    sectionsMap.put("Rail Reservation", ClientbaseInvoiceCategory.RAIL_RESERVATION);
    sectionsMap.put("Transportation Reservation", ClientbaseInvoiceCategory.TRANSPORT_RESERVATION);
    sectionsMap.put("Cruise Reservation", ClientbaseInvoiceCategory.CRUISE_RESERVATION);
    sectionsMap.put("Miscellaneous", ClientbaseInvoiceCategory.MISCELLANEOUS);
    //sectionsMap.put("Insurance", ClientbaseInvoiceCategory.INSURANCE); //May be needs to be skipped
    sectionsMap.put("Car Reservation", ClientbaseInvoiceCategory.CAR_RESERVATION); //From new invoices
    sectionsMap.put("Agency Fee", ClientbaseInvoiceCategory.AGENCY_FEE); //From new invoices

    sectionBookingsMap.put("Hotel", ClientbaseInvoiceCategory.HOTEL_RESERVATION);
    sectionBookingsMap.put("Flights", ClientbaseInvoiceCategory.AIR_RESERVATION);
    sectionBookingsMap.put("Tour", ClientbaseInvoiceCategory.TOUR_RESERVATION);
    sectionBookingsMap.put("Cruise", ClientbaseInvoiceCategory.CRUISE_RESERVATION);
    sectionBookingsMap.put("Car", ClientbaseInvoiceCategory.CAR_RESERVATION);
    sectionBookingsMap.put("Transportation", ClientbaseInvoiceCategory.TRANSPORT_RESERVATION);
    sectionBookingsMap.put("Rail", ClientbaseInvoiceCategory.RAIL_RESERVATION);
    sectionBookingsMap.put("Insurance", ClientbaseInvoiceCategory.INSURANCE);
    sectionBookingsMap.put("Miscellaneous", ClientbaseInvoiceCategory.MISCELLANEOUS);

    sectionEndersList.add("Amount Details");
    sectionEndersList.add("Base Tax Total");
    sectionEndersList.add("Total");
    sectionEndersList.add("\\*\\*Amounts in USD Base Tax Total");
    sectionEndersList.add("\\*\\*Amounts in USD Total");
    sectionEndersList.add("\\*\\*Amounts in USD");
    sectionEndersList.add("Pricing USD.*");
    sectionEndersList.add("Pricing EUR.*");

    String sectionsSplitterRE = "";
    for (String sectionIdentifier : sectionsMap.keySet()) {
      sectionsSplitterRE += "(^" + sectionIdentifier + "$)|";
    }
    sectionsSplitterRE = sectionsSplitterRE.substring(0, sectionsSplitterRE.length() - 1);
    sectionsSpitterPattern = Pattern.compile(sectionsSplitterRE, Pattern.MULTILINE | Pattern.DOTALL);

    String bookingsSplitterRE = "";
    for (String bookingIdentifier : sectionBookingsMap.keySet()) {
      bookingsSplitterRE += "(^" + bookingIdentifier + "$)|";
    }
    bookingsSplitterRE = bookingsSplitterRE.substring(0, bookingsSplitterRE.length() - 1);
    sectionBookingsPattern = Pattern.compile(bookingsSplitterRE, Pattern.MULTILINE | Pattern.DOTALL);


    StringBuilder sectionEnderRE = new StringBuilder("^(");
    for (String sectionEnder : sectionEndersList) {
      sectionEnderRE.append("(" + sectionEnder + ")|");
    }
    sectionEnderRE.setLength(sectionEnderRE.length() - 1);
    sectionEnderRE.append(")$");
    sectionEndPattern = Pattern.compile(sectionEnderRE.toString(), Pattern.MULTILINE | Pattern.DOTALL);

    StringBuilder hhw = new StringBuilder("(^|\\s)(");
    for (String hw: hotelNameHotWords) {
      hhw.append( hw +"|");
    }
    hhw.setLength(hhw.length() - 1);
    hhw.append(")(s|es)?(\\s|$)");
    hotelHotWordPattern = Pattern.compile(hhw.toString(), Pattern.CASE_INSENSITIVE);
  }

  public static class CBSection {
    ClientbaseInvoiceCategory category;
    CBSection() {
      bookings = new ArrayList<>();
    }
    public String headerText;
    public Map<SectionHeaderKeyword, String> headerData;
    public List<Pair<ClientbaseInvoiceCategory, String>> bookings;

    public String buildNote() {
      StringBuilder sb = new StringBuilder();

      for(SectionHeaderKeyword kw : headerData.keySet()) {
        if (SectionHeaderKeyword.printableList.contains(kw) && headerData.get(kw) != null) {
          sb.append(kw.getKeywordText() + " : " + headerData.get(kw).trim() + nl);
        }
      }
      //Passenger always last element before comments and data must continue
      if (headerData.get(SectionHeaderKeyword.PASSENGERS) != null) {
        sb.append(SectionHeaderKeyword.PASSENGERS.getKeywordText() + " : " +
                  headerData.get(SectionHeaderKeyword.PASSENGERS).trim() + nl);
      }
      sb.append(headerData.get(SectionHeaderKeyword.PARSER_COMMENTS).trim());
      return sb.toString();
    }
  }


  private static Pattern dateTimeCheckinPattern = Pattern.compile("Check-in\\s*:\\s*"+
                                                                  "([0-2]?[0-9])/([0-3]?[0-9])/([0-9]{4})" + //Date
                                                                  "(\\s+([0-2]?[0-9]):([0-5][0-9])\\s+(PM|AM))?", //Optional time
                                                                  Pattern.CASE_INSENSITIVE);

  private static Pattern dateTimeCheckoutPattern = Pattern.compile("Check-out\\s*:\\s*"+
                                                                   "([0-2]?[0-9])/([0-3]?[0-9])/([0-9]{4})" + //Date
                                                                   "(\\s+([0-2]?[0-9]):([0-5][0-9])\\s+(PM|AM))?", //Optional time
                                                                   Pattern.CASE_INSENSITIVE);

  private static Pattern flightDepartPattern = Pattern.compile("(.*?)\\s*" + //Airline Name (G#1)
                                                               "(Mon|Tues|Wed|Thurs|Fri|Sat|Sun)\\s" + //Day of the week (G#2)
                                                               "(\\d\\d)\\s*" + //Departure Day (G#3)
                                                               "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)" + //Departure Mnth (G#4)
                                                               "\\sDepart\\s" + //Main keyword that shows that this is a new flight
                                                               "(.*?)\\s\\((\\w{3})\\)\\s" + //City and Airport code where Departs from (G#5) and (G#6)
                                                               "([0-1]?[0-9]):([0-5][0-9])\\s(AM|PM)", //Departure time (G#7) (G#8) (G#9)
                                                               Pattern.CASE_INSENSITIVE);
  private static Pattern flightArrivePattern = Pattern.compile("Flight\\s(.*?)\\s" + //Flight Name
                                                               "(Mon|Tues|Wed|Thurs|Fri|Sat|Sun)\\s" + //Day of the week (don't care)
                                                               "(\\d\\d)\\s" + //Arrive Day
                                                               "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)" + //Arrive Month
                                                               "\\sArrive\\s" + //Main keyword that shows that this is a new flight
                                                               "(.*?)\\s\\((\\w{3})\\)\\s" + //City and Airport code where Arrives from
                                                               "([0-1]?[0-9]):([0-5][0-9])\\s(AM|PM)", //Arrive time
                                                               Pattern.CASE_INSENSITIVE);
  private static Pattern flightLocatorPattern = Pattern.compile("Locator\\s:\\s(.*)");


  private static Pattern tourNamePattern = Pattern.compile("Tour Name\\s:\\s+(.*)");

  private static Pattern dateUSPattern = Pattern.compile("([0-2]?[0-9])/([0-3]?[0-9])/([0-9]{4})");

  private static Pattern dateTimeCarPattern = Pattern.compile("(Pick-up|Drop-off)\\s:\\s"+
                                                              "([0-2]?[0-9])/([0-3]?[0-9])/([0-9]{4})" + //Date
                                                              "(\\s+([0-2]?[0-9]):([0-5][0-9])\\s+(PM|AM))?", //Optional time
                                                              Pattern.CASE_INSENSITIVE);
  private static Pattern carRentalCityPattern = Pattern.compile("City\\s:\\s(.*)");




  protected String rawPDFText;
  protected  List<Pair<ClientbaseInvoiceCategory, String>> documentSections;
  private ArrayList<ParseError> parseErrors = new ArrayList<ParseError>();

  private boolean confSloganEnabled = false;
  private String confSlogan = "";

  protected void initImpl(Map<String, String> params) {
    if (params.containsKey(CONF_SLOGAN)) {
      confSlogan = params.get(CONF_SLOGAN);
      confSloganEnabled = true;
    }
  }

  public TripVO extractDataImpl(InputStream input)
      throws Exception {
    TripVO tvo = new TripVO();


    PDDocument pdfDoc = PDDocument.load(input);

    int pdfPageCount = pdfDoc.getNumberOfPages();

    ArrayList<String> pages = new ArrayList<String>();
    try {
      PDFTextStripper stripper = new PDFTextStripper();
      stripper.setSortByPosition(true);
      for (int p = 1; p <= pdfPageCount; p++) {
        stripper.setStartPage(p);
        stripper.setEndPage(p);

        String pageText = stripper.getText(pdfDoc);
        pages.add(pageText);

      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    pdfDoc.close();

    StringBuilder rawTextBuilder = new StringBuilder();
    for (String page : cleanPages(pages)) {
      rawTextBuilder.append(page);
    }
    rawPDFText = rawTextBuilder.toString();

    //try to parse using section titles like Air Reservations, etc
    documentSections = getSectionsText(rawPDFText);
    List<CBSection> parsedSections = new ArrayList<CBSection>();

    //detect if there are no sction titles or if this is a new version of CB with Vendor:
    int vendorCount =  StringUtils.countMatches(rawPDFText, "Vendor :");
    if (documentSections == null || documentSections.size() == 0 || vendorCount > documentSections.size()) {
      documentSections = getSectionsTextNoHeadings(rawPDFText);
    }

    for (Pair<ClientbaseInvoiceCategory, String> typedText : documentSections) {
      CBSection section = processSection(typedText.getLeft(), typedText.getRight());
      parsedSections.add(section);
    }

    for (CBSection section : parsedSections) {
      for (Pair<ClientbaseInvoiceCategory, String> booking : section.bookings) {
        switch (booking.getLeft()) {
          case TOUR_RESERVATION:
            parseTour(tvo, booking.getRight(), section);
            break;
          case AIR_RESERVATION:
            parseFlight(tvo, booking.getRight(), section);
            break;
          case HOTEL_RESERVATION:
            parseHotel(tvo, booking.getRight(), section);
            break;
          case CRUISE_RESERVATION:
            parseCruise(tvo, booking.getRight(), section);
            break;
          case TRANSPORT_RESERVATION:
            parseTransport(tvo, booking.getRight(), section);
            break;
          case CAR_RESERVATION:
            parseCar(tvo, booking.getRight(), section);
            break;
          case RAIL_RESERVATION:
            parseRail(tvo, booking.getRight(), section);
            break;
          case MISCELLANEOUS:
            parseMisc(tvo, booking.getRight(), section);
            break;
        }
      }
    }
    tvo.setImportSrc(BookingSrc.ImportSrc.CLIENT_BASE_PDF);
    tvo.setImportTs(System.currentTimeMillis());
    return tvo;
  }


  protected CBSection processSection(ClientbaseInvoiceCategory type, String text) {
    CBSection section = new CBSection();
    section.category = type;

    Matcher sm = sectionBookingsPattern.matcher(text);
    int start = 0;
    int end = 0;
    ClientbaseInvoiceCategory bookingType = null;
    String match = "";
    while(sm.find()) {
      end = sm.start();
      match = sm.group(0);

      if (bookingType == null) { //The first match
        section.headerText = text.substring(0, end);
      }
      else {
        section.bookings.add(new ImmutablePair<>(bookingType, text.substring(start, end)));
      }

      bookingType = sectionBookingsMap.get(match);
      start = end + match.length();
    }

    if (bookingType != null) {
      section.bookings.add(new ImmutablePair<>(bookingType, text.substring(start, text.length())));
    } else {
      section.headerText = text; // We found no bookings, so everything goes to header (and later to comments)
    }

    section.headerData = parseSectionHeader(section.headerText);

    return section;
  }

  /**
   * @param rawText text string extracted from the PDF Document
   * @return Map of various bookings of the document
   */
  protected List<Pair<ClientbaseInvoiceCategory, String>> getSectionsTextNoHeadings(String rawText) {
    List<Pair<ClientbaseInvoiceCategory, String>> bookingSections = new ArrayList<>();

    rawText = rawText.replaceAll("Vendor :","____ Vendor :");
    String[] tokens  = rawText.split("____ ");
    for (String section: tokens) {
      Pair<ClientbaseInvoiceCategory, String> typeAndText = guessTypeAndCleanSection(section);
      if (typeAndText != null) {
        bookingSections.add(typeAndText);
      }
    }
    return bookingSections;
  }


  /**
   * @param rawText text string extracted from the PDF Document
   * @return Map of various bookings of the document
   */
  protected List<Pair<ClientbaseInvoiceCategory, String>> getSectionsText(String rawText) {
    List<Pair<ClientbaseInvoiceCategory, String>> bookingSections = new ArrayList<>();

    Matcher bm = sectionsSpitterPattern.matcher(rawText);

    /* Finding where booking blocks start end and and cutting them out of the raw pdf text*/
    boolean matchedBooking = false;
    int start = 0;
    int end = 0;
    while (bm.find()) {
      start = end;
      end = bm.start();
      addBookingSection(bookingSections, rawText,  start, end);
      matchedBooking = true;
    }
    if (matchedBooking) {
      start = end;
      end = rawText.length();
      addBookingSection(bookingSections, rawText, start, end);
    }

    return bookingSections;
  }


  private void addBookingSection(List<Pair<ClientbaseInvoiceCategory, String>> bookingSections, String rawText, int start, int end) {
    Pair<ClientbaseInvoiceCategory, String> typeAndText = getTypeAndCleanString(rawText.substring(start, end));

    //Handling exceptional case of Misc sections.
    //TODO: This will not handle multiple real Misc sections one after another
    //If previous section was Misc, concatenate new section to the previous
    if (bookingSections.size() > 0 && typeAndText.getLeft() == ClientbaseInvoiceCategory.MISCELLANEOUS &&
        bookingSections.get(bookingSections.size() - 1).getLeft() == ClientbaseInvoiceCategory.MISCELLANEOUS) {
      StringBuilder sb = new StringBuilder(bookingSections.get(bookingSections.size() - 1).getRight());
      sb.append(nl + "Miscellaneous" + nl);
      sb.append(typeAndText.getRight());
      typeAndText = new ImmutablePair<>(ClientbaseInvoiceCategory.MISCELLANEOUS, sb.toString());
      bookingSections.remove(bookingSections.size() - 1);
    }

    if (typeAndText != null) {
      bookingSections.add(typeAndText);
      if (DEBUG) {
        Log.log(LogLevel.DEBUG, "Section: " + typeAndText.getLeft().name() + " ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Log.log(LogLevel.DEBUG, typeAndText.getRight());
        Log.log(LogLevel.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
      }
    }
  }

  public ArrayList<String> cleanPages(ArrayList<String> pages) {
    ArrayList<String> cleanPages = new ArrayList<String>();

    int pageCount = pages.size();
    int firstPageIdx = 0;
    int midlePageIdx = 0;
    int lastPageIdx = 0;
    boolean findCommonHeader = true;
    int headerLineCount = 0;

    //Depending on document length selected pages for analysis of the headers are different
    switch(pageCount) {
      case 1:
        findCommonHeader = false;
        break;
      case 2:
        firstPageIdx = 0;
        midlePageIdx = 1;
        lastPageIdx = 1;
        break;
      default:
        firstPageIdx = 0;
        midlePageIdx = (int) Math.floor(pages.size() / 2.0);
        lastPageIdx = pageCount - 1;
        break;
    }

    if (findCommonHeader) {
      String pageOne = pages.get(firstPageIdx);
      String pageMid = pages.get(midlePageIdx);
      String pageLast = pages.get(lastPageIdx);

      Scanner scannerOne = new Scanner(pageOne);
      Scanner scannerMid = new Scanner(pageMid);
      Scanner scannerLast = new Scanner(pageLast);

      //Looking for the header
      while (scannerOne.hasNextLine() && scannerMid.hasNextLine() && scannerLast.hasNextLine()) {
        String pOneLine = scannerOne.nextLine();
        String pMidLine = scannerMid.nextLine();
        String pLastLine = scannerLast.nextLine();

        if (pOneLine.equals(pMidLine) && pOneLine.equals(pLastLine)) {
          headerLineCount++;
        }
        else {
          break;
        }
      }
    }


    for (String page: pages) {
      int currLine = 0;

      //Remove slogans
      if (confSloganEnabled){
        page = StringUtils.replace(page, confSlogan + "\n", "");
        page = StringUtils.replace(page, confSlogan, "");
      }

      Scanner currScanner = new Scanner(page);
      StringBuilder cleanPage = new StringBuilder();
      boolean addAllLinesFromHere = false;
      while(currScanner.hasNextLine()) {
        String currLineString = currScanner.nextLine().trim();
        if (currLineString.length() == 0) {
          continue;
        }
        Matcher bm = sectionsSpitterPattern.matcher(currLineString);
        if (addAllLinesFromHere){
          cleanPage.append(currLineString.trim() + nl);
        } else if (bm.find()) { //So we found header, should not attempt to filter anymore
          addAllLinesFromHere = true;
          cleanPage.append(currLineString.trim() + nl);
        }
        else if (currLineString != null && ++currLine > headerLineCount &&
                 !(currLineString.contains("Page No") || currLineString.contains("Invoice No") ||
                   currLineString.contains("Invoice Date") || currLineString.contains("Travel Consultant") ||
                   currLineString.contains("Group Name") || currLineString.contains("Group No"))) {
          addAllLinesFromHere = true;
          cleanPage.append(currLineString.trim() + nl);
        }
      }
      cleanPages.add(cleanPage.toString());
    }

    return cleanPages;
  }

  /**
   * Helper function that determines type of the bookings and shortens the text to cut
   * information pricing
   *
   * @param parsedText a section of text extracted from the PDF
   * @return
   */
  private static Pair<ClientbaseInvoiceCategory, String> guessTypeAndCleanSection(String parsedText) {

    ClientbaseInvoiceCategory vo;

    String s = parsedText.toLowerCase();
    if (s.contains("no. of nights")) {
      vo = ClientbaseInvoiceCategory.HOTEL_RESERVATION;
    } else if (s.contains("no. of cabins")) {
      vo = ClientbaseInvoiceCategory.CRUISE_RESERVATION;
    } else if (s.contains("record locator")) {
      vo = ClientbaseInvoiceCategory.AIR_RESERVATION;
    } else if (s.contains("duration")) {
      vo = ClientbaseInvoiceCategory.TOUR_RESERVATION;
    } else {
      vo = ClientbaseInvoiceCategory.CAR_RESERVATION;
    }

    //Find end of booking details and start of prices
    int endOfText = parsedText.length();
    Matcher secEnder = sectionEndPattern.matcher(parsedText);
    if (secEnder.find()) {
      endOfText = secEnder.start();
    }

    String cleanedText = parsedText.substring(0, endOfText);
    return new ImmutablePair<>(vo, cleanedText);
  }


  /**
   * Helper function that determines type of the bookings and shortens the text to cut
   * information pricing
   *
   * @param parsedText a section of text extracted from the PDF
   * @return
   */
  private static Pair<ClientbaseInvoiceCategory, String> getTypeAndCleanString(String parsedText) {
    int nlIndex = parsedText.indexOf('\n');
    ClientbaseInvoiceCategory vo;
    if (nlIndex > 0) {
      String title = parsedText.substring(0, nlIndex);
      vo = sectionsMap.get(title);
      if (vo == null) {
        return null;
      }
    }
    else {
      return null;
    }

    //Find end of booking details and start of prices
    int endOfText = parsedText.length();
    Matcher secEnder = sectionEndPattern.matcher(parsedText);
    if (secEnder.find()) {
      endOfText = secEnder.start();
      if(DEBUG) {
        System.out.println("CHOPPING OFF  #!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!");
        System.out.println(parsedText.substring(endOfText));
        System.out.println("CHOPPING OFF END ^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^");
      }
    }

    String cleanedText = parsedText.substring(nlIndex + 1, endOfText);

    return new ImmutablePair<>(vo, cleanedText);
  }


  /**
   * Checks string for presence of any of the keywords
   * @param line
   * @return
   */
  private static List<Pair<HotelKeyword, Integer>> findHotelTokens(String line) {
    List<Pair<HotelKeyword, Integer>> result = new ArrayList<>();

    for (HotelKeyword kw : HotelKeyword.values()) {
      int offset = line.indexOf(kw.getKeywordText() + " : ");
      if (offset != -1) {
        Pair<HotelKeyword, Integer> kOffset = new ImmutablePair<>(kw, offset);
        result.add(kOffset);
      }
    }

    // Sorting list
    Collections.sort(result, new Comparator<Pair<HotelKeyword, Integer>>() {
      @Override public int compare(Pair<HotelKeyword, Integer> o1, Pair<HotelKeyword, Integer> o2) {
        return o2.getRight().intValue() - o1.getRight().intValue(); //Reverse order
      }
    });

    return result;
  }

  public void parseHotel(TripVO tvo, String hotelText, CBSection section) {
    Scanner lineScanner = new Scanner(hotelText);
    ArrayDeque<String> lineQueue = new ArrayDeque<String>();

    String currLine;
    StringBuilder headerComment = new StringBuilder();
    StringBuilder currItemComment = new StringBuilder();
    String confirmatioNo = null;
    String[] bookingNumbers = null;
    HotelVO hvo = null;
    boolean skipLine = true;
    boolean isReusableAddress = false;
    int currHotel = 0; //Hotel Counter to properly find all data in the bookings
    String hotelName = "";
    StringBuilder hotelAddress = new StringBuilder();


    //Copy header information into comments only if document section is for Hotel bookings
    if (section.category == ClientbaseInvoiceCategory.HOTEL_RESERVATION) {
      headerComment.append(section.buildNote());
      headerComment.append(nl);
      confirmatioNo = section.headerData.get(SectionHeaderKeyword.CONFIRMATION_NO);
      if (confirmatioNo != null) {
        bookingNumbers = StringUtils.split(confirmatioNo.trim(), "&,/");
      }
    }

    while(lineScanner.hasNextLine()) {
      currLine = lineScanner.nextLine();
      List<Pair<HotelKeyword, Integer>> lineTokens = findHotelTokens(currLine);

      //If line has no tokens, just put on stack and continue
      if (lineTokens.size() == 0 && !skipLine) {
        lineQueue.addLast(currLine);
        continue;
      }

      if (skipLine) {
        skipLine = false;
        continue;
      }

      String lineParsed = currLine;
      String currSubstr = "";
      boolean addLineToComment = true;
      boolean newSetOfTokens = true;

      /* Need to check for new transport start line */
      for (Pair<HotelKeyword, Integer> currToken : lineTokens) {
        if (HotelKeyword.getStarters().contains(currToken.getLeft())) {
          isReusableAddress = false;
          if (++currHotel != 1) { //First hotel in the section - no need to bother searching around
            //1. All lines above that are higher than line 5 in hotels go to desc
            while (lineQueue.size() > 5 && hvo != null) {
              currItemComment.append(lineQueue.removeFirst() + nl);
            }

            //2. Looking closely at all lines
            int currLineIdx = 0;
            for (String candidate : lineQueue) {
              // A. Any line longer than 72 chars is not a hotel name
              if (candidate.length() > 72) {
                ++currLineIdx;
                continue;
              }
              // B. If current line equals to hotel name from the last tern - it is a hotel name
              if (hotelName.length() > 0 && hotelName.equals(candidate)) {
                isReusableAddress = true;
                break; //Found exactly the same hotel as before
              }

              // C. If the line contains one of the hotel hot words, *probably* a hotel name - mistakes can happen here
              Matcher hotNameMatcher = hotelHotWordPattern.matcher(candidate);
              if (hotNameMatcher.find()) {

                break; //Found hotel name !!!
              }

              ++currLineIdx;
            }

            //3. Hotel name is not found on one of the lines, drop all other lines to comments
            if (currLineIdx != lineQueue.size()) {
              while (--currLineIdx >= 0) {
                currItemComment.append(lineQueue.removeFirst() + nl);
              }
            }
          }

          //4. Extract name and address, if present
          hotelName = lineQueue.removeFirst();
          if (!lineQueue.isEmpty()) {
            hotelAddress.setLength(0);
            isReusableAddress = true;
          }
          while (!lineQueue.isEmpty()) {
            hotelAddress.append(lineQueue.removeFirst() + nl);
          }

          if (hvo != null) {
            hvo.setNote(headerComment.toString() + nl + currItemComment.toString());
            currItemComment.setLength(0);
          }

          hvo = new HotelVO();
          hvo.setHotelName(hotelName);
          tvo.addHotel(hvo);
          if (bookingNumbers != null && bookingNumbers.length >= currHotel) {
            hvo.setConfirmation(bookingNumbers[currHotel - 1]);
          }
          if (hotelAddress.length() > 0 && isReusableAddress) {
            currItemComment.append("Hotel Address:" + nl);
            currItemComment.append(hotelAddress);
          }
          break;
        }
      }

      for (Pair<HotelKeyword, Integer> currToken : lineTokens) {
        HotelKeyword hkw = currToken.getLeft();
        int offset = currToken.getRight();
        currSubstr = lineParsed.substring(offset);
        lineParsed = lineParsed.substring(0, offset); // current line is left only with earlier token

        switch (hkw) {
          case H_CHECK_IN:
            Matcher dtcim = dateTimeCheckinPattern.matcher(currSubstr);
            if (dtcim.find()) {
              int hours = 15;
              int minutes = 0;
              if (dtcim.group(4) != null) {
                if (DEBUG) {
                  Log.log(LogLevel.DEBUG, "Hotel Check-in time: " + dtcim.group(1) + " " + dtcim.group(2) + " " +
                                          dtcim.group(3) + " " + dtcim.group(4) + " " + dtcim.group(5) + " " +
                                          dtcim.group(6) + " ");
                }
                hours = Integer.parseInt(dtcim.group(5));
                minutes = Integer.parseInt(dtcim.group(6));
              }

              hvo.setCheckin(getTimestamp(dtcim.group(3), dtcim.group(1), dtcim.group(2), hours, minutes, dtcim.group(7)));
            }
            addLineToComment = false;
            break;
          case H_CHECK_OUT:
            Matcher dtcom = dateTimeCheckoutPattern.matcher(currSubstr);
            if(dtcom.find()) {
              int hours = 11;
              int minutes = 0;
              if (dtcom.group(4) != null) {
                hours = Integer.parseInt(dtcom.group(5));
                minutes = Integer.parseInt(dtcom.group(6));
              }
              hvo.setCheckout(getTimestamp(dtcom.group(3), dtcom.group(1), dtcom.group(2), hours, minutes, dtcom.group(7)));
            }
            addLineToComment = false;
            break;
          case H_DESCRIPTION:
            addLineToComment = true;
            break;
          case H_ROOM_TYPE:
            addLineToComment = true;
            break;
          case H_BEDDING:
            addLineToComment = true;
            break;
          case H_SMOKING:
            addLineToComment = true;
            break;
        }

        if (addLineToComment && newSetOfTokens) {
          currItemComment.append(currLine + nl);
        }
        newSetOfTokens = false;

        if (DEBUG) {
          Log.log(LogLevel.DEBUG, "Found token:" + currToken.getLeft() +
                                  " at offset " + currToken.getRight() + " in line: " + currLine);
        }
      }
    }
    if (hvo != null) { //Final comments need to be written
      while(!lineQueue.isEmpty()) {
        while (!lineQueue.isEmpty()) {
          currItemComment.append(lineQueue.removeFirst() + nl);
        }
      }
      hvo.setNote(headerComment.toString() + nl +  currItemComment.toString());
    }
  }

  /**
   * Checks string for presence of any of the keywords
   *
   * @param line
   * @return
   */
  private static List<Pair<FlightKeyword, Integer>> findFlightTokens(String line) {
    List<Pair<FlightKeyword, Integer>> result = new ArrayList<Pair<FlightKeyword, Integer>>();

    for (FlightKeyword kw : FlightKeyword.values()) {
      int offset = line.indexOf(kw.getKeywordText() + " : ");
      if (offset != -1) {
        Pair<FlightKeyword, Integer> kOffset = new ImmutablePair<FlightKeyword, Integer>(kw, offset);
        result.add(kOffset);
      }
    }


    /* Sorting list */
    Collections.sort(result, new Comparator<Pair<FlightKeyword, Integer>>() {
      @Override public int compare(Pair<FlightKeyword, Integer> o1, Pair<FlightKeyword, Integer> o2) {
        return o2.getRight().intValue() - o1.getRight().intValue(); //Reverse order
      }
    });

    return result;
  }


  private static FlightVO buildFlightVO(String airline, String flight,
                                        String departCity, String departAirportCode,
                                        String arriveCity, String arriveAirportCode,
                                        Timestamp departTime, Timestamp arriveTime) {
    FlightVO fvo = new FlightVO();
    AirportVO departAVO = new AirportVO();
    AirportVO arriveAVO = new AirportVO();
    departAVO.setCity(departCity);
    departAVO.setCode(departAirportCode);
    arriveAVO.setCity(arriveCity);
    arriveAVO.setCode(arriveAirportCode);

    fvo.setDepartureAirport(departAVO);
    fvo.setArrivalAirport(arriveAVO);
    fvo.setName(airline);
    fvo.setNumber(flight);
    fvo.setDepatureTime(departTime);
    fvo.setArrivalTime(arriveTime);

    return fvo;
  }

  public Timestamp getUndefinedYearTimestamp(int bookinsYearStart,
                                                     int bookingsYearEnd,
                                                     String month,
                                                     String day,
                                                     String hours,
                                                     String minutes,
                                                     String ampm){
    if(month == null || day == null) {
      return null;
    }

    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(0);
    int flightMonth = 0;
    int flightYear = bookinsYearStart;
    try {
      Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(month);
      cal.setTime(date);
      flightMonth = cal.get(Calendar.MONTH);
    }
    catch (ParseException e) {
      System.out.println("Failed to parse month :" + month);
    }

    //If trip starts in one year and ends in another since trips can be long any month before june is in the new year
    if (bookinsYearStart != bookingsYearEnd && flightMonth < 6) {
      flightYear = bookingsYearEnd;
    }

    int metricHour = 0;
    int convertedMinutes = 0;
    if (hours != null) {
      metricHour = Integer.parseInt(hours);
    }
    if (minutes != null) {
      convertedMinutes =  Integer.parseInt(minutes);
    }

    if (ampm != null && ampm.compareTo("PM") == 0) {
      if (metricHour < 12) {
        metricHour += 12;
      }
    }

    cal.set(flightYear, flightMonth, Integer.parseInt(day),
            metricHour,convertedMinutes);

    return new Timestamp(cal.getTimeInMillis());

  }

  public void parseFlight(TripVO tvo, String flightString, CBSection section) {
    Scanner lineScanner = new Scanner(flightString);
    lineScanner.useDelimiter(" ");

    Queue<String> lineQueue = new ArrayDeque<String>();

    String currLine;
    StringBuilder headerComment = new StringBuilder();
    StringBuilder currFlightComment = new StringBuilder();
    FlightVO fvo = null;
    int flightsYearStart = 0;
    int flightsYearEnd = 0;
    boolean skipLine = true;

    /* 1. Section common information setup */
    String startDate = section.headerData.get(SectionHeaderKeyword.START_DATE);
    if (startDate != null) {
      Matcher sdtm = dateUSPattern.matcher(startDate);
      if (sdtm.find()) {
        flightsYearStart = Integer.parseInt(sdtm.group(3));
      }
    }
    String endDate = section.headerData.get(SectionHeaderKeyword.END_DATE);
    if (endDate != null) {
      Matcher sdtm = dateUSPattern.matcher(endDate);
      if (sdtm.find()) {
        flightsYearEnd = Integer.parseInt(sdtm.group(3));
      }
    }

    //Copy header information into comments only if document section is for Flight bookings
    if (section.category == ClientbaseInvoiceCategory.AIR_RESERVATION) {
      headerComment.append(section.buildNote());
      headerComment.append(nl);
    }

    /* 2. Parse all flights under flights sub-section */
    while(lineScanner.hasNextLine()) {
      currLine = lineScanner.nextLine();
      List<Pair<FlightKeyword, Integer>> lineTokens = findFlightTokens(currLine);

      /* Once we are in the body, each line can should be checked for new flight information */

      Matcher dfm = flightDepartPattern.matcher(currLine);
      if (dfm.find()) {
        String arriveLine = lineScanner.nextLine();
        Matcher afm = flightArrivePattern.matcher(arriveLine);
        if (afm.find()) {
          if (fvo != null) {
            fvo.setNotes(headerComment.toString() + nl + currFlightComment.toString());
            currFlightComment.setLength(0); //Need to clear for new flight
          }

          Timestamp departTS = getUndefinedYearTimestamp(flightsYearStart,
                                                         flightsYearEnd,
                                                         dfm.group(4),
                                                         dfm.group(3),
                                                         dfm.group(7),
                                                         dfm.group(8),
                                                         dfm.group(9));
          Timestamp arriveTS = getUndefinedYearTimestamp(flightsYearStart,
                                                         flightsYearEnd,
                                                         afm.group(4),
                                                         afm.group(3),
                                                         afm.group(7),
                                                         afm.group(8),
                                                         afm.group(9));
          fvo = buildFlightVO(dfm.group(1), afm.group(1), dfm.group(5), dfm.group(6),
                              afm.group(5),afm.group(6), departTS, arriveTS);
          tvo.addFlight(fvo);
          continue;
        }
      }

      //If line has no tokens, just put on stack and continue
      if (lineTokens.size() == 0 && !skipLine) {
        lineQueue.add(currLine);
        continue;
      }

      if (skipLine) {
        skipLine = false;
        continue;
      }

      String lineParsed = currLine;
      String currSubstr = "";
      boolean addLineToComment = true;
      boolean newSetOfTokens = true;
      for (Pair<FlightKeyword, Integer> currToken : lineTokens) {

        FlightKeyword fkw = currToken.getLeft();
        int offset = currToken.getRight();
        currSubstr = lineParsed.substring(offset);
        lineParsed = lineParsed.substring(0, offset); //current line is left only with earlier token

        while(!lineQueue.isEmpty()) {
          currFlightComment.append(lineQueue.remove() + nl);
        }

        switch (fkw) {
          case FL_AIRCRAFT_TYPE:
            addLineToComment = true;
            break;
          case FL_MEAL:
            addLineToComment = true;
            break;
          case FL_FLIGHT_DURATION:
            addLineToComment = true;
            break;
          case FL_LOCATOR:
            Matcher lm = flightLocatorPattern.matcher(currSubstr);
            if (fvo != null && lm.find()) {
              fvo.setReservationNumber(lm.group(1));
            }
            addLineToComment = true;
            break;
          case FL_MILEAGE:
            addLineToComment = true;
            break;
          case FL_CLASS:
            addLineToComment = true;
            break;
          case FL_STATUS:
            addLineToComment = true;
            break;
          case FL_SEAT_NO:
            addLineToComment = true;
            break;
          case FL_SEAT_TYPE:
            addLineToComment = true;
            break;
          case FL_DESCRIPTION:
            addLineToComment = true;
            break;
        }

        if (addLineToComment && newSetOfTokens) {
          currFlightComment.append(currLine + nl);
        }
        newSetOfTokens = false;

        if (DEBUG) {
          Log.log(LogLevel.DEBUG, "Found token:" + currToken.getLeft() +
                                  " at offset " + currToken.getRight() + " in line: " + currLine);
        }
      }
    }
    if (fvo != null) { //Final comments need to be written
      while(!lineQueue.isEmpty()) {
        currFlightComment.append(lineQueue.remove() + nl);
      }
      fvo.setNotes(headerComment.toString() + nl +  currFlightComment.toString());
    }
  }


  /**
   * Checks string for presence of any of the keywords
   * @param line
   * @return
   */
  private static List<Pair<TourKeyword, Integer>> findTourTokens(String line) {
    List<Pair<TourKeyword, Integer>> result = new ArrayList<Pair<TourKeyword, Integer>>();

    for (TourKeyword kw : TourKeyword.values()) {
      int offset = line.indexOf(kw.getKeywordText());
      if (offset != -1) {
        Pair<TourKeyword, Integer> kOffset = new ImmutablePair<TourKeyword, Integer>(kw, offset);
        result.add(kOffset);
      }
    }

    // Sorting list
    Collections.sort(result, new Comparator<Pair<TourKeyword, Integer>>() {
      @Override public int compare(Pair<TourKeyword, Integer> o1, Pair<TourKeyword, Integer> o2) {
        return o2.getRight().intValue() - o1.getRight().intValue(); //Reverse order
      }
    });

    return result;
  }

  public void parseTour(TripVO tvo, String tourText, CBSection section) {
    Scanner lineScanner = new Scanner(tourText);
    ArrayDeque<String> lineQueue = new ArrayDeque<String>();
    String currLine;
    StringBuilder headerComment = new StringBuilder();
    StringBuilder currItemComment = new StringBuilder();
    String confirmatioNo = null;
    ActivityVO avo = null;
    boolean skipLine = false;

    //Copy header information into comments only if document section is for Flight bookings
    if (section.category == ClientbaseInvoiceCategory.TOUR_RESERVATION) {
      headerComment.append(section.buildNote());
      headerComment.append(nl);
      confirmatioNo = section.headerData.get(SectionHeaderKeyword.CONFIRMATION_NO);
    }

    while(lineScanner.hasNextLine()) {
      currLine = lineScanner.nextLine();
      List<Pair<TourKeyword, Integer>> lineTokens = findTourTokens(currLine);

      //If line has no tokens, just put on stack and continue
      if (lineTokens.size() == 0 && !skipLine) {
        lineQueue.add(currLine);
        continue;
      }

      if (skipLine) {
        skipLine = false;
        continue;
      }

      for (Pair<TourKeyword, Integer> currToken : lineTokens) {
        if (TourKeyword.getStarters().contains(currToken.getLeft())) {
          String contact = lineQueue.pollLast();
          if (avo != null) {
            //Write all Queue to the comments/note
            while(!lineQueue.isEmpty()) {
              currItemComment.append(lineQueue.pop() + nl);
            }
            avo.setNote(headerComment.toString() + nl +  currItemComment.toString());
            if (avo.getName() == null && avo.getContact() != null) {
              avo.setName(avo.getContact());
              avo.setContact(null);
            }
            currItemComment.setLength(0);
          }

          avo = new ActivityVO();
          avo.setBookingType(ReservationType.TOUR);
          avo.setContact(contact);
          avo.setConfirmation(confirmatioNo);
          tvo.addActivity(avo);
          break;
        }
      }

      String lineParsed = currLine;
      String currSubstr = "";
      boolean addLineToComment = true;
      boolean newSetOfTokens = true;
      for (Pair<TourKeyword, Integer> currToken : lineTokens) {

        TourKeyword tkw = currToken.getLeft();
        int offset = currToken.getRight();
        currSubstr = lineParsed.substring(offset);
        lineParsed = lineParsed.substring(0, offset); // current line is left only with earlier token

        switch (tkw) {
          case TR_REAL_START_DATE:
            Matcher sdtm = dateUSPattern.matcher(currSubstr);
            if (sdtm.find()) {
              Timestamp ts = getTimestamp(sdtm.group(3), sdtm.group(1), sdtm.group(2), 0, 0, null);
              avo.setStartDate(ts);
            }

            addLineToComment = false;
            break;
          case TR_REAL_END_DATE:
            Matcher edtm = dateUSPattern.matcher(currSubstr);
            if (edtm.find()) {
              Timestamp ts = getTimestamp(edtm.group(3), edtm.group(1), edtm.group(2), 0, 0, null);
              avo.setEndDate(ts);
            }
            addLineToComment = false;
            break;
          case TR_TOUR_NAME:
            Matcher tnm = tourNamePattern.matcher(currSubstr);
            if (tnm.find()) {
              if (avo != null) {
                avo.setName(tnm.group(1));
              }
              else {
                Log.log(LogLevel.INFO, "Tour Name without End Date - no activity can be created");
              }
            }
            addLineToComment = false;
            break;
          case TR_DEPART:
            addLineToComment = true;
            break;
          case TR_ARRIVE:
            addLineToComment = true;
            break;
          case TR_DESCRIPTION:
            addLineToComment = true;
            break;
        }

        if (addLineToComment && newSetOfTokens) {
          currItemComment.append(currLine + nl);
        }
        newSetOfTokens = false;

        if (DEBUG) {
          Log.log(LogLevel.DEBUG, "Found token:" + currToken.getLeft() +
                                  " at offset " + currToken.getRight() + " in line: " + currLine);
        }
      }
    }
    if (avo != null) { //Final comments need to be written
      while(!lineQueue.isEmpty()) {
        currItemComment.append(lineQueue.remove() + nl);
      }
      avo.setNote(headerComment.toString() + nl +  currItemComment.toString());

      if (avo.getName() == null && avo.getContact() != null) {
        avo.setName(avo.getContact());
        avo.setContact(null);
      }
    }
  }

  /**
   * Checks string for presence of any of the keywords
   * @param line
   * @return
   */
  private static List<Pair<CruiseKeyword, Integer>> findCruiseTokens(String line) {
    List<Pair<CruiseKeyword, Integer>> result =  new ArrayList<>();
    for (CruiseKeyword kw : CruiseKeyword.values()) {
      int offset = line.indexOf(kw.getKeywordText() + " : ");
      if (offset != -1) {
        Pair<CruiseKeyword, Integer> kOffset = new ImmutablePair<CruiseKeyword, Integer>(kw, offset);
        result.add(kOffset);
      }
    }

    // Sorting list
    Collections.sort(result, new Comparator<Pair<CruiseKeyword, Integer>>() {
      @Override public int compare(Pair<CruiseKeyword, Integer> o1, Pair<CruiseKeyword, Integer> o2) {
        return o2.getRight().intValue() - o1.getRight().intValue(); //Reverse order
      }
    });

    return result;
  }

  private static Pattern portDetectPattern = Pattern.compile("(^Day\\s+Port\\s+Arrive\\s+Depart$)|(^Depart\\s+Arrive$)");
  private static Pattern portPatternTypeA = Pattern.compile(
      "^(Mon|Tues|Wed|Thurs|Fri|Sat|Sun)\\s([0-9]+)\\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\\s*" +
      "(([0-2]?[0-9]):([0-5][0-9])\\s(AM|PM)?)?\\s*(.*?)" +
      "(Mon|Tues|Wed|Thurs|Fri|Sat|Sun)\\s([0-9]+)\\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\\s*" +
      "(([0-2]?[0-9]):([0-5][0-9])\\s(AM|PM)?)?\\s*" +
      "(.*?)$");


  private static Pattern portPatternTypeB = Pattern.compile(
      "^(Mon|Tues|Wed|Thurs|Fri|Sat|Sun)\\s([0-9]+)\\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\\s(.*?)\\s" +
      "((([0-2]?[0-9]):([0-5][0-9])\\s(AM|PM)?)|(---))\\s?((([0-2]?[0-9]):([0-5][0-9])\\s?(AM|PM)?)|(---))?$");

  public void parseCruise(TripVO tvo, String cruiseText, CBSection section) {
    Scanner lineScanner = new Scanner(cruiseText);
    ArrayDeque<String> lineQueue = new ArrayDeque<>();
    String currLine;
    StringBuilder headerComment = new StringBuilder();
    StringBuilder currItemComment = new StringBuilder();
    CruiseVO cvo = null;

    ArrayDeque<ActivityVO> ports = new ArrayDeque<>();


    boolean portListStarted = false;
    int     portListCounter = 0;
    int cruiseStartYear = 0;
    int cruiseFinishYear = 0;

    /* 1. Section common information setup */
    String startDate = section.headerData.get(SectionHeaderKeyword.START_DATE);
    if (startDate != null) {
      Matcher sdtm = dateUSPattern.matcher(startDate);
      if (sdtm.find()) {
        cruiseStartYear = Integer.parseInt(sdtm.group(3));
      }
    }

    String endDate = section.headerData.get(SectionHeaderKeyword.END_DATE);
    if (endDate != null) {
      Matcher sdtm = dateUSPattern.matcher(endDate);
      if (sdtm.find()) {
        cruiseFinishYear = Integer.parseInt(sdtm.group(3));
      }
    }

    // 2. Copy header information into comments only if document section is for Cruise bookings
    if (section.category == ClientbaseInvoiceCategory.CRUISE_RESERVATION) {
      headerComment.append(section.buildNote());
      headerComment.append(nl);
    }

    while(lineScanner.hasNextLine()) {
      currLine = lineScanner.nextLine();
      String lineParsed = currLine;
      String currSubstr = "";
      String currValue = "";
      boolean addLineToComment = true;
      boolean newSetOfTokens = true;
      List<Pair<CruiseKeyword, Integer>> lineTokens = findCruiseTokens(currLine);

      //If line has no tokens, just put on stack and continue
      if (lineTokens.size() == 0) {
        Matcher portStarter = portDetectPattern.matcher(currLine);
        if (portStarter.find()) {
          portListStarted = true;
        }

        if (portListStarted && cvo != null) {
          //1. Trying to match first type of port definitions
          Matcher pm= portPatternTypeA.matcher(currLine);
          if (pm.find()) {
            ActivityVO avo = new ActivityVO();
            avo.setBookingType(ReservationType.CRUISE_STOP);
            Timestamp ts = getUndefinedYearTimestamp(cruiseStartYear,
                                                           cruiseFinishYear,
                                                           pm.group(3),
                                                           pm.group(2),
                                                           pm.group(5),
                                                           pm.group(6),
                                                           pm.group(7));

            avo.setName(pm.group(8).trim());
            if(ports.size() == 0) { //Then depart
              avo.setEndDate(ts);
            } else {
              avo.setStartDate(ts);
            }
            ports.add(avo);

            avo = new ActivityVO();
            avo.setBookingType(ReservationType.CRUISE_STOP);
            ts = getUndefinedYearTimestamp(cruiseStartYear,
                                           cruiseFinishYear,
                                           pm.group(11),
                                           pm.group(10),
                                           pm.group(13),
                                           pm.group(14),
                                           pm.group(15));

            avo.setName(pm.group(16).trim());
            avo.setStartDate(ts);
            ports.add(avo);

            addLineToComment = false;
          }

          pm = portPatternTypeB.matcher(currLine);
          if (pm.find()) {
            ActivityVO avo = new ActivityVO();
            avo.setBookingType(ReservationType.CRUISE_STOP);
            avo.setName(pm.group(4));

            Timestamp arriveTS = getUndefinedYearTimestamp(cruiseStartYear,
                                                           cruiseFinishYear,
                                                           pm.group(3),
                                                           pm.group(2),
                                                           pm.group(7),
                                                           pm.group(8),
                                                           pm.group(9));
            avo.setStartDate(arriveTS);



            Timestamp departTs = getUndefinedYearTimestamp(cruiseStartYear,
                                                           cruiseFinishYear,
                                                           pm.group(3),
                                                           pm.group(2),
                                                           pm.group(13),
                                                           pm.group(14),
                                                           pm.group(15));
            avo.setEndDate(departTs);
            ports.add(avo);
          }
        }

        if (addLineToComment && currLine.length() > 0) {
          lineQueue.add(currLine);
        }
        continue;
      }

      /* Need to check for new cruise start line */
      for (Pair<CruiseKeyword, Integer> currToken : lineTokens) {
        if (CruiseKeyword.getStarters().contains(currToken.getLeft())) {
          String cruiseName = lineQueue.pollLast();
          if (cvo != null) {
            //Empty ports
            portsForCruise(ports, cvo, tvo);
            //Write all Queue to the comments/note
            while(!lineQueue.isEmpty()) {
              currItemComment.append(lineQueue.pop() + nl);
            }
            cvo.setNote(headerComment.toString() + nl +  currItemComment.toString());
            currItemComment.setLength(0);
          }
          cvo = new CruiseVO();
          cvo.setName(cruiseName);
          cvo.setConfirmationNo(section.headerData.get(SectionHeaderKeyword.CONFIRMATION_NO));
          cvo.setRecordLocator(section.headerData.get(SectionHeaderKeyword.RECORD_LOCATOR));
          cvo.setCompanyName(section.headerData.get(SectionHeaderKeyword.VENDOR));

          tvo.addCruise(cvo);
          break;
        }
      }

      for (Pair<CruiseKeyword, Integer> currToken : lineTokens) {
        CruiseKeyword crkw = currToken.getLeft();
        int offset = currToken.getRight();
        currSubstr = lineParsed.substring(offset);
        currValue = currSubstr.substring(crkw.getKeywordText().length() + 3);
        lineParsed = lineParsed.substring(0, offset); // current line is left only with earlier token

        switch (crkw) {
          case CR_REAL_START_DATE:
            Matcher sdtm = dateUSPattern.matcher(currSubstr);
            if (sdtm.find()) {
              Timestamp ts = getTimestamp(sdtm.group(3), sdtm.group(1), sdtm.group(2), 0, 0, null);
              cvo.setTimestampDepart(ts);
            }
            addLineToComment = false;
            break;
          case CR_REAL_END_DATE:
            Matcher edtm = dateUSPattern.matcher(currSubstr);
            if (edtm.find()) {
              Timestamp ts = getTimestamp(edtm.group(3), edtm.group(1), edtm.group(2), 0, 0, null);
              cvo.setTimestampArrive(ts);
            }
            addLineToComment = false;
            break;
          case CR_CATEGORY:
            cvo.setCategory(currValue);
            addLineToComment = false;
            break;
          case CR_BEDDING:
            cvo.setBedding(currValue);
            addLineToComment = false;
            break;
          case CR_DINING:
            cvo.setMeal(currValue);
            addLineToComment = false;
            break;
          case CR_DECK:
            cvo.setDeck(currValue);
            addLineToComment = false;
            break;
          case CR_CABIN_ROOM:
            cvo.setCabinNumber(currValue);
            addLineToComment = false;
            break;
          case CR_DESCRIPTION:
            addLineToComment = true;
            break;
        }

        if (addLineToComment && newSetOfTokens) {
          currItemComment.append(currLine + nl);
        }
        newSetOfTokens = false;

        if (DEBUG) {
          Log.log(LogLevel.DEBUG, "Found token:" + currToken.getLeft() +
                                  " at offset " + currToken.getRight() + " in line: " + currLine);
        }
      }
    }
    if (cvo != null) { //Final comments need to be written
      portsForCruise(ports, cvo, tvo);
      while(!lineQueue.isEmpty()) {
        currItemComment.append(lineQueue.pop() + nl);
      }
      cvo.setNote(headerComment.toString() + nl +  currItemComment.toString());
    }
  }

  private static void portsForCruise(ArrayDeque<ActivityVO> ports, CruiseVO cvo, TripVO tvo) {
    if(ports.size() == 0) {
      return;
    }
    ActivityVO avo = ports.removeFirst();
    cvo.setPortDepart(avo.getName());
    cvo.setTimestampDepart(avo.getEndDate());

    avo = ports.removeLast();
    cvo.setPortArrive(avo.getName());
    cvo.setTimestampArrive(avo.getStartDate());

    //Removing duplicates
    Iterator<ActivityVO> iter = ports.iterator();
    while(iter.hasNext()) {
      ActivityVO currAvo = iter.next();
      if(currAvo.getName().equals(avo.getName())) { //Multi-day stays or duplicate entries
        avo.setEndDate(currAvo.getEndDate());
        iter.remove();
        continue;
      }
      if( currAvo.getName().equals("At Sea") || currAvo.getName().equals("Sea Day")) { //Nothing to do, just watch water
        iter.remove();
        continue;
      }
      avo = currAvo;
    }

    while(ports.size() != 0) {
      avo = ports.removeFirst();
      avo.setName("Cruise Stop: " + avo.getName());
      tvo.addActivity(avo);
    }
  }


  /**
   * Checks string for presence of any of the keywords
   *
   * @param line
   * @return
   */
  private static List<Pair<SectionHeaderKeyword, Integer>> findHeaderTokens(String line) {
    List<Pair<SectionHeaderKeyword, Integer>> result = new ArrayList<Pair<SectionHeaderKeyword, Integer>>();

    for (SectionHeaderKeyword kw : SectionHeaderKeyword.values()) {
      //Handling exception case where Passenger keyword is wrongly detected on No. of Passengers line
      if (kw == SectionHeaderKeyword.PASSENGERS) {
        if (line.indexOf(SectionHeaderKeyword.NO_OF_PASSENGER.getKeywordText()) > -1) {
          continue;
        }
      }

      int offset = line.indexOf(kw.getKeywordText() + " : ");
      if (offset != -1) {
        Pair<SectionHeaderKeyword, Integer> kOffset = new ImmutablePair<SectionHeaderKeyword, Integer>(kw, offset);
        result.add(kOffset);
      }
    }

    // Sorting list
    Collections.sort(result, new Comparator<Pair<SectionHeaderKeyword, Integer>>() {
      @Override public int compare(Pair<SectionHeaderKeyword, Integer> o1, Pair<SectionHeaderKeyword, Integer> o2) {
        return o2.getRight().intValue() - o1.getRight().intValue(); //Reverse order
      }
    });

    return result;
  }

  public Map<SectionHeaderKeyword, String> parseSectionHeader(String headerText) {
    Map<SectionHeaderKeyword, String> result = new HashMap<>();

    headerText = headerText.replaceAll("\\s+", " "); //Brownell invoices somehow got 2 spaces
    Scanner lineScanner = new Scanner(headerText);
    String currLine;
    StringBuilder headerComment = new StringBuilder();

    while(lineScanner.hasNextLine()) {
      currLine = lineScanner.nextLine();
      List<Pair<SectionHeaderKeyword, Integer>> lineTokens = findHeaderTokens(currLine);

      //If line has no tokens, just put on stack and continue
      if (lineTokens.size() == 0) {
        headerComment.append(currLine + nl);
        continue;
      }

      String lineParsed = currLine;
      String currSubstr;

      for (Pair<SectionHeaderKeyword, Integer> currToken : lineTokens) {
        SectionHeaderKeyword shkw = currToken.getLeft();
        int offset = currToken.getRight();
        if (shkw.getKeywordText().length() >= lineParsed.length()) {
          continue;
        }
        currSubstr = lineParsed.substring(offset + shkw.getKeywordText().length() + 3).trim(); //Magic number 3 is for " : "
        lineParsed = lineParsed.substring(0, offset); // current line is left only with earlier token
        result.put(shkw, currSubstr);

        if (DEBUG) {
          Log.log(LogLevel.DEBUG, "Found token:" + currToken.getLeft() + " at offset " +
                                  currToken.getRight() + " in line: " + currLine);
        }
      }
    }
    result.put(SectionHeaderKeyword.PARSER_COMMENTS, headerComment.toString());
    return result;
  }


  /**
   * Checks string for presence of any of the keywords
   * @param line
   * @return
   */
  private static List<Pair<CarKeyword, Integer>> findCarTokens(String line) {
    List<Pair<CarKeyword, Integer>> result = new ArrayList<Pair<CarKeyword, Integer>>();

    for (CarKeyword kw : CarKeyword.values()) {
      int offset = line.indexOf(kw.getKeywordText() + " : ");

      if (offset != -1) {
        Pair<CarKeyword, Integer> kOffset = new ImmutablePair<CarKeyword, Integer>(kw, offset);
        result.add(kOffset);
      }
    }

    // Sorting list
    Collections.sort(result, new Comparator<Pair<CarKeyword, Integer>>() {
      @Override public int compare(Pair<CarKeyword, Integer> o1, Pair<CarKeyword, Integer> o2) {
        return o2.getRight().intValue() - o1.getRight().intValue(); //Reverse order
      }
    });

    return result;
  }

  public void parseCar(TripVO tvo, String carText, CBSection section) {
    Scanner lineScanner = new Scanner(carText);
    ArrayDeque<String> lineQueue = new ArrayDeque<String>();
    String currLine;
    StringBuilder headerComment = new StringBuilder();
    StringBuilder currItemComment = new StringBuilder();

    TransportVO trvo = null;
    boolean skipLine = true;


    String confirmatioNo = null;

    //Copy header information into comments only if document section is for Hotel bookings
    if (section.category == ClientbaseInvoiceCategory.CAR_RESERVATION) {
      headerComment.append(section.buildNote());
      headerComment.append(nl);
      confirmatioNo = section.headerData.get(SectionHeaderKeyword.CONFIRMATION_NO);
    }

    while(lineScanner.hasNextLine()) {
      currLine = lineScanner.nextLine();
      List<Pair<CarKeyword, Integer>> lineTokens = findCarTokens(currLine);

      //If line has no tokens, just put on stack and continue
      if (lineTokens.size() == 0 && !skipLine) {
        lineQueue.add(currLine);
        continue;
      }

      if (skipLine) {
        skipLine = false;
        continue;
      }
      String lineParsed = currLine;
      String currSubstr = "";
      boolean addLineToComment = true;
      boolean newSetOfTokens = true;
      for (Pair<CarKeyword, Integer> currToken : lineTokens) {

        CarKeyword ckw = currToken.getLeft();
        int offset = currToken.getRight();
        currSubstr = lineParsed.substring(offset);
        lineParsed = lineParsed.substring(0, offset); // current line is left only with earlier token

        switch (ckw) {
          case CAR_PICK_UP:
            Matcher dtcm = dateTimeCarPattern.matcher(currSubstr);
            if (dtcm.find()) {
              int hours;
              int minutes;
              if (dtcm.group(5) != null) {
                hours = Integer.parseInt(dtcm.group(6));
                minutes = Integer.parseInt(dtcm.group(7));
              }
              else {
                hours = 0;
                minutes = 0;
              }
              Timestamp ts = getTimestamp(dtcm.group(4), dtcm.group(2), dtcm.group(3), hours, minutes, dtcm.group(8));

              if (trvo == null) {
                String carRentalName = "Car Rental";
                if (!lineQueue.isEmpty()) {
                  lineQueue.pop();
                }
                trvo = new TransportVO();
                trvo.setBookingType(ReservationType.CAR_RENTAL);
                tvo.addTransport(trvo);
                trvo.setConfirmationNumber(confirmatioNo);
                trvo.setCmpyName(carRentalName);
              }

              trvo.setPickupDate(ts);
            }
            addLineToComment = false;
            break;

          case CAR_DROP_OFF:
            if (lineQueue.isEmpty()) {
              addLineToComment = false;
              break;
            }
            String carRentalName = lineQueue.pop();
            if (trvo != null) {
              //Write all Queue to the comments/note
              while(!lineQueue.isEmpty()) {
                currItemComment.append(lineQueue.remove() + nl);
              }
              trvo.setNote(headerComment.toString() + nl +  currItemComment.toString());
              currItemComment.setLength(0);
            }

            trvo = new TransportVO();
            trvo.setBookingType(ReservationType.CAR_RENTAL);
            tvo.addTransport(trvo);

            Matcher dotcm = dateTimeCarPattern.matcher(currSubstr);
            if (dotcm.find()) {
              int hours;
              int minutes;
              if (dotcm.group(5) != null) {
                hours = Integer.parseInt(dotcm.group(6));
                minutes = Integer.parseInt(dotcm.group(7));
              }
              else {
                hours = 0;
                minutes = 0;
              }
              trvo.setDropoffDate(getTimestamp(dotcm.group(4), dotcm.group(2), dotcm.group(3), hours, minutes, dotcm.group(8)));
            }
            trvo.setConfirmationNumber(confirmatioNo);
            trvo.setCmpyName(carRentalName);
            addLineToComment = false;
            break;
          case CAR_PICK_UP_CITY:
            Matcher crcm = carRentalCityPattern.matcher(currSubstr);
            if (crcm.find()) {
              trvo.setPickupLocation(crcm.group(1));
            }
            addLineToComment = false;
            break;
          case CAR_DROP_OFF_CITY:

            Matcher dfcm = carRentalCityPattern.matcher(currSubstr);
            if (dfcm.find()) {
              trvo.setDropoffLocation(dfcm.group(1));
            }
            addLineToComment = false;
            break;

          case CAR_CATEGORY:
          case CAR_DESCRIPTION:
            addLineToComment = true;
            break;
        }

        if (addLineToComment && newSetOfTokens) {
          currItemComment.append(currLine + nl);
        }
        newSetOfTokens = false;

        if (DEBUG) {
          Log.log(LogLevel.DEBUG, "Found token:" + currToken.getLeft() +
                                  " at offset " + currToken.getRight() + " in line: " + currLine);
        }
      }
    }
    if (trvo != null) { //Final comments need to be written
      while(!lineQueue.isEmpty()) {
        currItemComment.append(lineQueue.remove() + nl);
      }
      trvo.setNote(headerComment.toString() + nl +  currItemComment.toString());
    }
  }

  /**
   * Checks string for presence of any of the keywords
   * @param line
   * @return
   */
  private static List<Pair<TransportKeyword, Integer>> findTransportTokens(String line) {
    List<Pair<TransportKeyword, Integer>> result = new ArrayList<Pair<TransportKeyword, Integer>>();

    for (TransportKeyword kw : TransportKeyword.values()) {
      int offset = line.indexOf(kw.getKeywordText() + " : ");
      if (offset != -1) {
        Pair<TransportKeyword, Integer> kOffset = new ImmutablePair<TransportKeyword, Integer>(kw, offset);
        result.add(kOffset);
      }
    }

    // Sorting list
    Collections.sort(result, new Comparator<Pair<TransportKeyword, Integer>>() {
      @Override public int compare(Pair<TransportKeyword, Integer> o1, Pair<TransportKeyword, Integer> o2) {
        return o2.getRight().intValue() - o1.getRight().intValue(); //Reverse order
      }
    });

    return result;
  }

  public void parseTransport(TripVO tvo, String transportText, CBSection section) {
    Scanner lineScanner = new Scanner(transportText);
    ArrayDeque<String> lineQueue = new ArrayDeque<String>();
    String currLine;
    StringBuilder headerComment = new StringBuilder();
    StringBuilder currItemComment = new StringBuilder();
    String confirmatioNo = null;
    TransportVO trvo = null;
    boolean skipLine = true;

    //Copy header information into comments only if document section is for Flight bookings
    if (section.category == ClientbaseInvoiceCategory.TRANSPORT_RESERVATION) {
      confirmatioNo = section.headerData.get(SectionHeaderKeyword.CONFIRMATION_NO);
      headerComment.append(section.buildNote());
      headerComment.append(nl);
    }

    while(lineScanner.hasNextLine()) {
      currLine = lineScanner.nextLine();
      List<Pair<TransportKeyword, Integer>> lineTokens = findTransportTokens(currLine);

      //If line has no tokens, just put on stack and continue
      if (lineTokens.size() == 0 && !skipLine) {
        lineQueue.add(currLine);
        continue;
      }

      if (skipLine) {
        skipLine = false;
        continue;
      }
      String lineParsed = currLine;
      String currSubstr = "";
      boolean addLineToComment = true;
      boolean newSetOfTokens = true;

      /* Need to check for new transport start line */
      for (Pair<TransportKeyword, Integer> currToken : lineTokens) {
        if (TransportKeyword.getStarters().contains(currToken.getLeft()) && !lineQueue.isEmpty()) {
          String carRentalName = lineQueue.remove();

          if (trvo != null) {
            //Write all Queue to the comments/note
            while (!lineQueue.isEmpty()) {
              currItemComment.append(lineQueue.remove() + nl);
            }
            trvo.setNote(headerComment.toString() + nl + currItemComment.toString());
            currItemComment.setLength(0);
          }

          trvo = new TransportVO();
          trvo.setBookingType(ReservationType.TRANSPORT);
          tvo.addTransport(trvo);

          trvo.setConfirmationNumber(confirmatioNo);
          trvo.setCmpyName(carRentalName);
          break;
        }
      }
      if (trvo != null) {


        for (Pair<TransportKeyword, Integer> currToken : lineTokens) {
          TransportKeyword tkw = currToken.getLeft();
          int offset = currToken.getRight();
          currSubstr = lineParsed.substring(offset);
          lineParsed = lineParsed.substring(0, offset); // current line is left only with earlier token

          switch (tkw) {
            case TRANS_PICK_UP:
              Matcher dtcm = dateTimeCarPattern.matcher(currSubstr);
              if (dtcm.find()) {
                int hours;
                int minutes;
                if (dtcm.group(5) != null) {
                  hours = Integer.parseInt(dtcm.group(6));
                  minutes = Integer.parseInt(dtcm.group(7));
                } else {
                  hours = 0;
                  minutes = 0;
                }
                trvo.setPickupDate(getTimestamp(dtcm.group(4), dtcm.group(2), dtcm.group(3), hours, minutes, null));
              }
              addLineToComment = false;
              break;
            case TRANS_PICK_UP_CITY:
              trvo.setPickupLocation(currSubstr.substring(tkw.getKeywordText().length() + 3).trim());
              addLineToComment = false;
              break;
            case TRANS_DROP_OFF:
              Matcher dotcm = dateTimeCarPattern.matcher(currSubstr);
              if (dotcm.find()) {
                int hours;
                int minutes;
                if (dotcm.group(5) != null) {
                  hours = Integer.parseInt(dotcm.group(6));
                  minutes = Integer.parseInt(dotcm.group(7));
                } else {
                  hours = 0;
                  minutes = 0;
                }
                trvo.setDropoffDate(getTimestamp(dotcm.group(4), dotcm.group(2), dotcm.group(3), hours, minutes, null));
              }

              addLineToComment = false;
              break;
            case TRANS_DROP_OFF_CITY:
              trvo.setDropoffLocation(currSubstr.substring(tkw.getKeywordText().length() + 3).trim());
              addLineToComment = false;
              break;
            case TRANS_CATEGORY:
            case TRANS_DESCRIPTION:
              addLineToComment = true;
              break;
          }

          if (addLineToComment && newSetOfTokens) {
            currItemComment.append(currLine + nl);
          }
          newSetOfTokens = false;

          if (DEBUG) {
            Log.log(LogLevel.DEBUG, "Found token:" + currToken.getLeft() +
                    " at offset " + currToken.getRight() + " in line: " + currLine);
          }
        }
      }
    }
    if (trvo != null) { //Final comments need to be written
      while(!lineQueue.isEmpty()) {
        currItemComment.append(lineQueue.remove() + nl);
      }
      trvo.setNote(headerComment.toString() + nl +  currItemComment.toString());
    }
  }

  /**
   * Checks string for presence of any of the keywords
   * @param line
   * @return
   */
  private static List<Pair<RailKeyword, Integer>> findRailTokens(String line) {
    List<Pair<RailKeyword, Integer>> result = new ArrayList<Pair<RailKeyword, Integer>>();

    for (RailKeyword kw : RailKeyword.values()) {
      int offset = line.indexOf(kw.getKeywordText() + " : ");

      if (offset != -1) {
        Pair<RailKeyword, Integer> kOffset = new ImmutablePair<RailKeyword, Integer>(kw, offset);
        result.add(kOffset);
      }

    }

    // Sorting list
    Collections.sort(result, new Comparator<Pair<RailKeyword, Integer>>() {
      @Override public int compare(Pair<RailKeyword, Integer> o1, Pair<RailKeyword, Integer> o2) {
        return o2.getRight().intValue() - o1.getRight().intValue(); //Reverse order
      }
    });

    return result;
  }


  private static Pattern departRailPattern = Pattern.compile("(.*?)\\s" + //Airline Name (G#1)
                                                             "(Mon|Tues|Wed|Thurs|Fri|Sat|Sun)\\s" + //Day of the week (G#2)
                                                             "(\\d\\d)\\s" + //Departure Day (G#3)
                                                             "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)" + //Departure Mnth (G#4)
                                                             "\\sDepart\\s" + //Main keyword that shows that this is a new train
                                                             "(.*?)\\s" + //City name (G#5)
                                                             "([0-1]?[0-9]):([0-5][0-9])\\s(AM|PM)", //Departure time (G#6) (G#7) (G#8)
                                                             Pattern.CASE_INSENSITIVE);
  private static Pattern arriveRailPattern = Pattern.compile("(Train\\s(.*?)\\s)?" + //Train Name
                                                             "(Mon|Tues|Wed|Thurs|Fri|Sat|Sun)\\s" + //Day of the week (don't care)
                                                             "(\\d\\d)\\s" + //Arrive Day
                                                             "(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)" + //Arrive Month
                                                             "\\sArrive\\s" + //Main keyword that shows that this is a new train
                                                             "(.*?)\\s" + //Arrive city name
                                                             "([0-1]?[0-9]):([0-5][0-9])\\s(AM|PM)", //Arrive time
                                                             Pattern.CASE_INSENSITIVE);


  private static TransportVO buildTransportVO(String railwayName,
                                              String trainNumber,
                                              String departCity,
                                              String arriveCity,
                                              Timestamp departTime,
                                              Timestamp arriveTime) {
    TransportVO trvo = new TransportVO();
    if (trainNumber != null) {
      trvo.setCmpyName(railwayName + " " + trainNumber);
    }
    else {
      trvo.setCmpyName(railwayName);
    }
    trvo.setPickupLocation(departCity);
    trvo.setDropoffLocation(arriveCity);
    trvo.setPickupDate(departTime);
    trvo.setDropoffDate(arriveTime);

    return trvo;
  }


  public void parseRail(TripVO tvo, String railString, CBSection section) {
    Scanner lineScanner = new Scanner(railString);

    ArrayDeque<String> lineQueue = new ArrayDeque<String>();

    String currLine;

    StringBuilder headerComment = new StringBuilder("Train Booking" + nl);
    StringBuilder currFlightComment = new StringBuilder();
    String confirmationNo = null;
    TransportVO rvo = null;
    int railYearStart = 0; //Used to determine the year the train booking happens
    int railYearEnd = 0;   //Used to determine the year the train booking happens
    boolean skipLine = true;

    /* 1. Section common information setup */
    String startDate = section.headerData.get(SectionHeaderKeyword.START_DATE);
    if (startDate != null) {
      Matcher sdtm = dateUSPattern.matcher(startDate);
      if (sdtm.find()) {
        railYearStart = Integer.parseInt(sdtm.group(3));
      }
    }
    String endDate = section.headerData.get(SectionHeaderKeyword.END_DATE);
    if (endDate != null) {
      Matcher sdtm = dateUSPattern.matcher(endDate);
      if (sdtm.find()) {
        railYearEnd = Integer.parseInt(sdtm.group(3));
      }
    }

    //Copy header information into comments only if document section is for train bookings
    if (section.category == ClientbaseInvoiceCategory.RAIL_RESERVATION) {
      headerComment.append(section.buildNote());
      headerComment.append(nl);
      confirmationNo = section.headerData.get(SectionHeaderKeyword.CONFIRMATION_NO);
    }

    while(lineScanner.hasNextLine()) {
      currLine = lineScanner.nextLine();
      List<Pair<RailKeyword, Integer>> lineTokens = findRailTokens(currLine);

      /* Once we are in the body, each line can should be checked for new flight information */

      Matcher dtm = departRailPattern.matcher(currLine);
      if (dtm.find()) {
        String arriveLine = lineScanner.nextLine();
        Matcher atm = arriveRailPattern.matcher(arriveLine);
        if (atm.find()) {
          if (rvo != null) {
            rvo.setNote(headerComment.toString() + nl + currFlightComment.toString());
            currFlightComment.setLength(0); //Need to clear for new flight
          }

          Timestamp departTS = getUndefinedYearTimestamp(railYearStart,
                                                         railYearEnd,
                                                         dtm.group(4),
                                                         dtm.group(3),
                                                         dtm.group(6),
                                                         dtm.group(7),
                                                         dtm.group(8));

          Timestamp arriveTS = getUndefinedYearTimestamp(railYearStart,
                                                         railYearEnd,
                                                         atm.group(5),
                                                         atm.group(4),
                                                         atm.group(7),
                                                         atm.group(8),
                                                         atm.group(9));
          rvo = buildTransportVO(dtm.group(1), atm.group(2), dtm.group(5), atm.group(6), departTS, arriveTS);
          rvo.setBookingType(ReservationType.RAIL);
          rvo.setConfirmationNumber(confirmationNo);
          tvo.addTransport(rvo);
          continue;
        }
      }

      //If line has no tokens, just put on stack and continue
      if (lineTokens.size() == 0 && !skipLine) {
        lineQueue.add(currLine);
        continue;
      }

      if (skipLine) {
        skipLine = false;
        continue;
      }
      String lineParsed = currLine;
      String currSubstr = "";
      boolean addLineToComment = true;
      boolean newSetOfTokens = true;
      for (Pair<RailKeyword, Integer> currToken : lineTokens) {

        RailKeyword rkw = currToken.getLeft();
        int offset = currToken.getRight();
        currSubstr = lineParsed.substring(offset);
        lineParsed = lineParsed.substring(0, offset); //current line is left only with earlier token

        while(!lineQueue.isEmpty()) {
          currFlightComment.append(lineQueue.remove() + nl);
        }

        switch (rkw) {
          //NO KEYWORDS SEEN IN ANY OF THE SAMPLES
        }


        if (addLineToComment && newSetOfTokens) {
          currFlightComment.append(currLine + nl);
        }
        newSetOfTokens = false;

        if (DEBUG) {
          Log.log(LogLevel.DEBUG, "Found token:" + currToken.getLeft() +
                                  " at offset " + currToken.getRight() + " in line: " + currLine);
        }
      }
    }
    if (rvo != null) { //Final comments need to be written
      while(!lineQueue.isEmpty()) {
        currFlightComment.append(lineQueue.pop() + nl);
      }
      rvo.setNote(headerComment.toString() + nl +  currFlightComment.toString());
    }
  }

  /**
   * Checks string for presence of any of the keywords
   * @param line
   * @return
   */
  private static List<Pair<MiscKeyword, Integer>> findMiscTokens(String line) {
    List<Pair<MiscKeyword, Integer>> result = new ArrayList<Pair<MiscKeyword, Integer>>();

    for (MiscKeyword kw : MiscKeyword.values()) {
      int offset = line.indexOf(kw.getKeywordText() + " : ");
      if (offset != -1) {
        Pair<MiscKeyword, Integer> kOffset = new ImmutablePair<MiscKeyword, Integer>(kw, offset);
        result.add(kOffset);
      }
    }

    // Sorting list
    Collections.sort(result, new Comparator<Pair<MiscKeyword, Integer>>() {
      @Override public int compare(Pair<MiscKeyword, Integer> o1, Pair<MiscKeyword, Integer> o2) {
        return o2.getRight().intValue() - o1.getRight().intValue(); //Reverse order
      }
    });

    return result;
  }

  public void parseMisc(TripVO tvo, String miscText, CBSection section) {
    Scanner lineScanner = new Scanner(miscText);
    ArrayDeque<String> lineQueue = new ArrayDeque<String>();
    String currLine;
    StringBuilder headerComment = new StringBuilder();
    StringBuilder currItemComment = new StringBuilder();
    String confirmatioNo = null;
    String recordLocator = null;
    ActivityVO avo = null;
    boolean skipLine = false;

    //Copy header information into comments only if document section is for Misc bookings
    if (section.category == ClientbaseInvoiceCategory.MISCELLANEOUS) {
      headerComment.append(section.buildNote());
      headerComment.append(nl);
      confirmatioNo = section.headerData.get(SectionHeaderKeyword.CONFIRMATION_NO);
      recordLocator = section.headerData.get(SectionHeaderKeyword.RECORD_LOCATOR);
    }

    while(lineScanner.hasNextLine()) {
      currLine = lineScanner.nextLine();
      List<Pair<MiscKeyword, Integer>> lineTokens = findMiscTokens(currLine);

      //If line has no tokens, just put on stack and continue
      if (lineTokens.size() == 0 && !skipLine) {
        lineQueue.add(currLine);
        continue;
      }

      if (skipLine) {
        skipLine = false;
        continue;
      }

      for (Pair<MiscKeyword, Integer> currToken : lineTokens) {
        if (MiscKeyword.getStarters().contains(currToken.getLeft())) {
          String contact = lineQueue.pollLast();
          if (avo != null) {
            //Write all Queue to the comments/note
            while(!lineQueue.isEmpty()) {
              currItemComment.append(lineQueue.pop() + nl);
            }
            avo.setNote(headerComment.toString() + nl +  currItemComment.toString());
            if (avo.getName() == null && avo.getContact() != null) {
              avo.setName(avo.getContact());
              avo.setContact(null);
            }
            currItemComment.setLength(0);
          }

          avo = new ActivityVO();
          avo.setBookingType(ReservationType.ACTIVITY);
          avo.setContact(contact);
          avo.setConfirmation(confirmatioNo);
          avo.setRecordLocator(recordLocator);
          tvo.addActivity(avo);
          break;
        }
      }

      String lineParsed = currLine;
      String currSubstr = "";
      boolean addLineToComment = true;
      boolean newSetOfTokens = true;
      for (Pair<MiscKeyword, Integer> currToken : lineTokens) {

        MiscKeyword tkw = currToken.getLeft();
        int offset = currToken.getRight();
        currSubstr = lineParsed.substring(offset);
        lineParsed = lineParsed.substring(0, offset); // current line is left only with earlier token

        switch (tkw) {
          case MISC_REAL_START_DATE:
            Matcher sdtm = dateUSPattern.matcher(currSubstr);
            if (sdtm.find()) {
              Timestamp ts = getTimestamp(sdtm.group(3), sdtm.group(1), sdtm.group(2), 0, 0, null);
              avo.setStartDate(ts);
            }

            addLineToComment = false;
            break;
          case MISC_REAL_END_DATE:
            Matcher edtm = dateUSPattern.matcher(currSubstr);
            if (edtm.find()) {
              Timestamp ts = getTimestamp(edtm.group(3), edtm.group(1), edtm.group(2), 0, 0, null);
              avo.setEndDate(ts);
            }
            addLineToComment = false;
            break;
          case MISC_DESCRIPTION:
            addLineToComment = true;
            break;
        }

        if (addLineToComment && newSetOfTokens) {
          currItemComment.append(currLine + nl);
        }
        newSetOfTokens = false;

        if (DEBUG) {
          Log.log(LogLevel.DEBUG, "Found token:" + currToken.getLeft() +
                                  " at offset " + currToken.getRight() + " in line: " + currLine);
        }
      }
    }
    if (avo != null) { //Final comments need to be written
      while(!lineQueue.isEmpty()) {
        currItemComment.append(lineQueue.remove() + nl);
      }
      avo.setNote(headerComment.toString() + nl +  currItemComment.toString());

      if (avo.getName() == null && avo.getContact() != null) {
        avo.setName(avo.getContact());
        avo.setContact(null);
      }
    }
  }


  /**
   * Access to the list of parse errors.
   *
   * @return list of parse errors,
   * @note must be called only after calling @see extractData() method
   */
  @Override
  public ArrayList<ParseError> getParseErrors() {
    return parseErrors;
  }

  @Override
  public void postProcessingImpl(String tripId, String userId) {
  }

  public Timestamp getTimestamp(String year, String month, String day, int hours, int minutes, String ampm) {
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(0);

    int metricHour = hours;
    if (ampm != null && ampm.compareTo("PM") == 0) {
      if (metricHour < 12) {
        metricHour += 12;
      }
    }

    cal.set(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(day), metricHour, minutes);
    return new Timestamp(cal.getTimeInMillis());
  }

  public Timestamp getTimestamp(String month, int day, int hh, int mm) {
    //Getting current time
    Calendar cal = Calendar.getInstance();
    int currYY = cal.get(Calendar.YEAR);
    int currMM = cal.get(Calendar.MONTH);

    if (DEBUG) {
      Log.log(LogLevel.DEBUG, "getTimestamp(" + month + ", " + day + ", " + hh + ", " + mm + ")");
    }
    //Convert month to computer understandable value
    try {
      Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(month);
      cal.setTime(date);
    }
    catch (ParseException e) {
      Log.log(LogLevel.ERROR, "Failed to parse month :" + month);
    }

    int itemMonth = cal.get(Calendar.MONTH);

    if (itemMonth - currMM < -2) //If up to three months old, still the same year, otherwise next
    {
      ++currYY;
    }

    cal.setTimeInMillis(0); //Eliminate seconds and milliseconds from getInstance() call
    cal.set(currYY, itemMonth, day, hh, mm);

    return new Timestamp(cal.getTimeInMillis());
  }

  public String toString() {
    StringBuilder strBuilder = new StringBuilder("Split Document\n");

    for (Pair<ClientbaseInvoiceCategory, String> typedText : documentSections) {
      strBuilder.append("Type: " + typedText.getLeft() + "\n");
      strBuilder.append("Details: " + typedText.getRight() + "\n");
      strBuilder.append("==========================================================================================\n");
    }

    //strBuilder.append("\nRAW\n" + rawPDFText);
    return strBuilder.toString();
  }

  private void addParseError(ParseError pe) {
    if (parseErrors.size() == 0) {
      parseErrors.add(pe);
      return;
    }
    //Only adding more severe parse errors for the same element
    ParseError prevError = parseErrors.get(parseErrors.size() - 1);
    if (prevError.text.compareTo(pe.text) == 0) {
      if (prevError.severity.getLevel() < pe.severity.getLevel()) {
        parseErrors.set(parseErrors.size() - 1, pe);
      }
    }
    else {
      parseErrors.add(pe);
    }
  }
}
