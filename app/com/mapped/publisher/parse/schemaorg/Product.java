package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by surge on 2015-05-12.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Product
    extends Thing {

  /**
   * [Brand] or [Organization] The brand(s) associated with a product or service, or the brand(s) maintained by an
   * organization or business person.
   */
  public Brand brand;

  /**
   * color	Text 	The color of the product.
   */
  public String color;

  /**
   * [ReviewAction]	Actions supported by Product.
   */
  public String action;

  /**
   * Text 	The Stock Keeping Unit (SKU), i.e. a merchant-specific identifier for a product or service,
   * or the product to which the offer refers.
   */
  public String sku;


  /**
   * [ProductModel] or [Text]
   * The model of the product. Use with the URL of a ProductModel or a textual representation of the model identifier.
   * The URL of the ProductModel can be from an external source. It is recommended to additionally provide strong
   * product identifiers via the gtin8/gtin13/gtin14 and mpn properties.
   */
  public String model;


  /*

  aggregateRating	AggregateRating 	The overall rating, based on a collection of reviews or ratings, of the item.
audience	Audience 	The intended audience of the item, i.e. the group for whom the item was created.

depth	Distance  or
QuantitativeValue 	The depth of the item.
gtin13	Text 	The GTIN-13 code of the product, or the product to which the offer refers. This is equivalent to 13-digit ISBN codes and EAN UCC-13. Former 12-digit UPC codes can be converted into a GTIN-13 code by simply adding a preceeding zero. See GS1 GTIN Summary for more details.
gtin14	Text 	The GTIN-14 code of the product, or the product to which the offer refers. See GS1 GTIN Summary for more details.
gtin8	Text 	The GTIN-8 code of the product, or the product to which the offer refers. This code is also known as EAN/UCC-8 or 8-digit EAN. See GS1 GTIN Summary for more details.
height	Distance  or
QuantitativeValue 	The height of the item.
isAccessoryOrSparePartFor	Product 	A pointer to another product (or multiple products) for which this product is an accessory or spare part.
isConsumableFor	Product 	A pointer to another product (or multiple products) for which this product is a consumable.
isRelatedTo	Product 	A pointer to another, somehow related product (or multiple products).
isSimilarTo	Product 	A pointer to another, functionally similar product (or multiple products).
itemCondition	OfferItemCondition 	A predefined value from OfferItemCondition or a textual description of the condition of the product or service, or the products or services included in the offer.
logo	URL  or
ImageObject 	An associated logo.
manufacturer	Organization 	The manufacturer of the product.

mpn	Text 	The Manufacturer Part Number (MPN) of the product, or the product to which the offer refers.
offers	Offer 	An offer to provide this item—for example, an offer to sell a product, rent the DVD of a movie, or give away tickets to an event.
productID	Text 	The product identifier, such as ISBN. For example: <meta itemprop='productID' content='isbn:123-456-789'/>.
releaseDate	Date 	The release date of a product or product model. This can be used to distinguish the exact variant of a product.
review	Review 	A review of the item. Supersedes reviews.
weight	QuantitativeValue 	The weight of the product or person.
width	Distance  or
QuantitativeValue 	The width of the item.


   */
}
