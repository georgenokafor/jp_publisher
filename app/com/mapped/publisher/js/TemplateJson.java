package com.mapped.publisher.js;

import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.umapped.persistence.enums.ReservationType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by surge on 2014-09-23.
 */
public class TemplateJson
    extends BaseJsonResponse {

  public String templateId;
  public String templateName;
  public String cmpyId;
  public String cmpyName;
  public boolean canEdit;
  public String tripId;

  public Map<ReservationType, List<BookingDetailsJson>> bookings;

  public TemplateJson(String templateId, String templateName) {
    this.templateId = templateId;
    this.templateName = templateName;
    this.returnCode = 0;
    bookings = new HashMap<>();
  }

  public void addBookingDetails(BookingDetailsJson tbdj) {
    if (tbdj == null || tbdj.type == null) {
      Log.log(LogLevel.ERROR, "Failed to add template booking detail view either type or view is null");
      return;
    }
    List<BookingDetailsJson> tbdjl = bookings.get(tbdj.type);
    if (tbdjl == null) {
      tbdjl = new ArrayList<>();
      bookings.put(tbdj.type, tbdjl);
    }
    tbdjl.add(tbdj);
  }
}
