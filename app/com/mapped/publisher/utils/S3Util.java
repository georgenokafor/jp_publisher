package com.mapped.publisher.utils;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.io.Files;
import com.mapped.common.CoreConstants;
import com.mapped.publisher.common.ConfigMgr;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.io.FilenameUtils;
import play.libs.Json;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;
import javax.management.timer.Timer;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-08
 * Time: 1:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class S3Util {
  public final static String S3_DOMAIN = "s3.amazonaws.com/";

  public final static String BUCKET_NAME = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.S3_BUCKET);
  private final static String ACCESS_KEY = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.S3_ACCESS_KEY);
  private final static String SECRET_KEY = ConfigMgr.getAppParameter(com.mapped.common.CoreConstants.S3_SECRET);
  private static BasicAWSCredentials cre = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
  private static AmazonS3 s3 = new AmazonS3Client(cre);


  public static String authorizeS3Put(String bucket, String key) {

    //set expiry time
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.SECOND, 1800); //allow 30mins
    Date expDate = cal.getTime();

    try {
      GeneratePresignedUrlRequest presignedUrlRequest = new GeneratePresignedUrlRequest(bucket,
                                                                                        key,
                                                                                        HttpMethod.PUT);
      presignedUrlRequest.setExpiration(expDate);
      presignedUrlRequest.setContentType(nameToMime(key));
      presignedUrlRequest.addRequestParameter("x-amz-acl", "public-read");
      String url = null;
      url = s3.generatePresignedUrl(presignedUrlRequest).toString();
      return url;
    }
    catch (Exception e) {
      Log.err("authorizeS3Put(): Fail " + bucket + " - " + key, e);
    }

    return null;
  }


  public static String authorizeS3Put(String fileName) {
    return authorizeS3Put(BUCKET_NAME, fileName);
  }

  public static S3Data authorizeS3(String bucket, String fileName)
      throws Exception {
    Date             now            = new Date();
    Date             oneHourFromNow = new Date(now.getTime() + Timer.ONE_HOUR);
    TimeZone         tz             = TimeZone.getTimeZone("UTC");
    SimpleDateFormat dfm            = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    dfm.setTimeZone(tz);
    String formattedExpirationDate = dfm.format(oneHourFromNow);

    String policy_document = "{\"expiration\": \"" + formattedExpirationDate + "\",\"conditions\": [{\"bucket\": \""
                             + bucket + "\" },[\"starts-with\", \"$key\", \"\"]," +
                             "{'success_action_status': '201'},[\"content-length-range\", 1048, 10485760]]}";

    S3Data s3Data = new S3Data();
    s3Data.policy = new String(Base64.encodeBase64(policy_document.getBytes("UTF-8")), "ASCII").replaceAll("\n", "")
                                                                                               .replaceAll("\r", "");


    Mac hmac = Mac.getInstance("HmacSHA1");
    hmac.init(new SecretKeySpec(SECRET_KEY.getBytes("UTF-8"), "HmacSHA1"));
    s3Data.signature = new String(Base64.encodeBase64(hmac.doFinal(s3Data.policy.getBytes("UTF-8"))),
                                  "ASCII").replaceAll("\n", "");

    s3Data.accessKey = ACCESS_KEY;
    s3Data.key = fileName;
    s3Data.s3Bucket = "https://" + bucket + ".s3.amazonaws.com";

    return s3Data;
  }

  public static S3Data authorizeS3(String fileName)
      throws Exception {
    return authorizeS3(BUCKET_NAME, fileName);
  }

  public static String authorizeS3Get(String fileName) {
    return authorizeS3Get(BUCKET_NAME, fileName);
  }

  public static String authorizeS3Get(String bucket, String fileName) {

    return "https://" + bucket + "." + S3_DOMAIN + fileName;

  }

  public static String authorizeS3GetSigned(String bucket, String fileName) {
//still used for signed s3 from the mobile app

    //set expiry time
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.SECOND, 5 * 24 * 60 * 60); //5 days  expiry
    Date expDate = cal.getTime();
    try {
      String url = null;
      url = s3.generatePresignedUrl(bucket, fileName, expDate, HttpMethod.GET).toString();
      return url;
    }
    catch (Exception e) {
      Log.err("authorizeS3Get():  " + fileName, e);
    }
    return null;
  }

  public static long uploadS3Text(String data, String fileName) {
    return uploadS3Text(BUCKET_NAME, fileName, data, "text/html");
  }


  public static String generatePublicUrl(final String bucket, final String key){
    return "https://" + bucket + "." + S3_DOMAIN + key;
  }

  //Copy S3 object from full URL
  //Example: https://s3.amazonaws.com/umapped_prd/672309608685033085_Profile%20pic.jpg?Expires=2055002620&AWSAccessKeyId=AKIAIWSVOJAGXUUUXGBA&Signature=Z4ASGimk3wy5lPpyb5vzcgefLWM%3D
  public static CopyObjectResult s3CopyObject(String url, String toBucket, String toKey, ObjectMetadata om) {
    URLCodec urlCodec = new URLCodec();
    try {
      String cleanUrl = urlCodec.decode(url);

      int domain = cleanUrl.indexOf("s3.amazonaws.com/");

      if(domain == -1) {
        return null;
      }

      if (domain == "https://".length()) {
        String cleaned = cleanUrl.substring("https://s3.amazonaws.com/".length());
        int    params  = cleaned.indexOf('?');
        if (params != -1) {
          cleaned = cleaned.substring(0, params);
        }

        int bucketSplit = cleaned.indexOf('/');
        String bucket = cleaned.substring(0, bucketSplit);
        String key    = cleaned.substring(bucketSplit + 1);
        return s3CopyObject(bucket, key, toBucket, toKey, om);
      }


      String bucket = cleanUrl.substring("https://".length(), domain - 1);
      String cleaned = cleanUrl.substring(domain + "s3.amazonaws.com/".length());
      int    params  = cleaned.indexOf('?');
      if (params != -1) {
        cleaned = cleaned.substring(0, params);
      }

      int bucketSplit = cleaned.indexOf('/');
      String key    = cleaned.substring(bucketSplit + 1);
      return s3CopyObject(bucket, key, toBucket, toKey, om);
    } catch (DecoderException de) {
      Log.err("S3Utils: Failed to decode S3 URL: ", de);
      return null;
    }
  }

    /**
     * Copies object from one bucket to another as a single atomic command.
     *
     * @param fromBucket
     * @param fromKey
     * @param toBucket
     * @param toKey
     * @param objectMetadata - if not null uses filled-in metadata fields from this object,
     *                         while others will be copied
     * @return S3 API Copy Result, null if there is a failure
     */
  public static CopyObjectResult s3CopyObject(String fromBucket, String fromKey, String toBucket, String toKey,
                                              ObjectMetadata objectMetadata) {
    try {
      // Copying object
      CopyObjectRequest copyObjRequest = new CopyObjectRequest(fromBucket, fromKey, toBucket, toKey);
      Log.debug("Copying S3 object from " + fromBucket + "/" + fromKey + " to " + toBucket + "/" + toKey);
      if(objectMetadata != null) {
        copyObjRequest.withNewObjectMetadata(objectMetadata);
      }
      return s3.copyObject(copyObjRequest);
    }
    catch (AmazonServiceException ase) {
      Log.err("Caught an AmazonServiceException, which means your request made it " +
              "to Amazon S3, but was rejected with an error response for some reason.");
      Log.err("Error Message:    " + ase.getMessage());
      Log.err("HTTP Status Code: " + ase.getStatusCode());
      Log.err("AWS Error Code:   " + ase.getErrorCode());
      Log.err("Error Type:       " + ase.getErrorType());
      Log.err("Request ID:       " + ase.getRequestId());
    }
    catch (AmazonClientException ace) {
      Log.err("Caught an AmazonClientException, which means the client encountered " +
              "an internal error while trying to  communicate with S3, such as not being able to access the network.");
      Log.err("Error Message: " + ace.getMessage());
    }
    return null;
  }

  public static class S3FileInfo {
    public String  bucket;
    public String  key;
    public String  mime;
    public byte[]  hash;
    public long    size;
    public byte[]  binData;
    public String  filename;
    public long    cacheControlTimeout;
    public boolean isCacheControlPublic;
    public boolean isCacheControlNoTransform;


    public S3FileInfo(){
      cacheControlTimeout = -1; //Not set
    }

    public static S3FileInfo buildInfo(String bucket, String filePath, String data, String mime) {
      S3FileInfo s3fi = new S3FileInfo();
      s3fi.binData = data.getBytes();
      s3fi.bucket = bucket;
      s3fi.key = filePath;
      s3fi.mime = mime;
      s3fi.hash = DigestUtils.md5(s3fi.binData);
      s3fi.size = s3fi.binData.length;
      return s3fi;
    }

    public static S3FileInfo buildInfo(String bucket, String filePath, File file, String mime) {
      try {
        S3FileInfo s3fi = new S3FileInfo();
        s3fi.binData = Files.toByteArray(file);
        s3fi.bucket = bucket;
        s3fi.key = filePath;
        s3fi.mime = mime;
        s3fi.hash = DigestUtils.md5(s3fi.binData);
        s3fi.size = s3fi.binData.length;
        return s3fi;
      }
      catch (IOException e) {
        Log.err("Failed to read file to be saved to S3:" + filePath, e);
      }
      return null;
    }

    public static S3FileInfo buildInfo(String bucket, String filePath, InputStream is, String mime) {
      try {
        S3FileInfo s3fi = new S3FileInfo();
        s3fi.bucket = bucket;
        s3fi.key = filePath;
        s3fi.mime = mime;
        s3fi.binData = new byte[is.available()];
        s3fi.size = is.read(s3fi.binData);
        s3fi.hash = DigestUtils.md5(s3fi.binData);
        return s3fi;
      }
      catch (IOException e) {
        Log.err("Failed to read file to be saved to S3:" + filePath, e);
      }
      return null;
    }
  }

  public static long uploadS3Text(String bucket, String filePath, String data, String mime) {
    S3FileInfo info = S3FileInfo.buildInfo(bucket, filePath, data, mime);
    if (uploadS3Data(info)) {
      return info.size;
    }
    return -1;
  }

  public static boolean uploadS3Data(final S3FileInfo info) {
    ObjectMetadata metaData = new ObjectMetadata();
    try(InputStream inputStream = new ByteArrayInputStream(info.binData)) {
      metaData.setContentLength(info.size);
      metaData.setContentType(info.mime);
      if(info.hash != null && info.hash.length > 0) { //User calculated content MD5
        metaData.setContentMD5(java.util.Base64.getEncoder().encodeToString(info.hash));
      }
      if(info.cacheControlTimeout != -1) {
        setCacheControl(metaData, info.cacheControlTimeout, info.isCacheControlNoTransform, info.isCacheControlPublic);
      }
      PutObjectResult result = s3.putObject(info.bucket, info.key, inputStream, metaData);
      setObjectPublic(info.bucket, info.key);
      return true;
    }
    catch (Exception e) {
      Log.err("Unable to save to S3 - " + info.key + " in bucket " + info.bucket, e);
      return false;
    }
  }

  public static String nameToMime(String filename) {
    String ext = FilenameUtils.getExtension(filename).toLowerCase();
    switch (ext) {
      case "txt":
        return "text/plain";
      case "eml":
        return "text/plain";
      case "html":
        return "text/html";
      case "xhtml":
        return "application/xhtml+xml";
      case "pdf":
        return "application/pdf";
      case "doc":
        return "application/msword";
      case "xls":
        return "application/vnd.ms-excel";
      case "xlsx":
        return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
      case "docx":
        return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
      case "png":
        return "image/png";
      case "jpg":
        return "image/jpeg";
      case "jpeg":
        return "image/jpeg";
      case "gif":
        return "image/gif";
      default:
        return "text/html";
    }
  }

  public static void uploadS3File(File file, String fileName)
      throws Exception {
    ObjectMetadata       metaData    = new ObjectMetadata();
    byte[]               b           = Files.toByteArray(file);
    ByteArrayInputStream inputStream = new ByteArrayInputStream(b);
    metaData.setContentLength(b.length);
    metaData.setContentType(nameToMime(fileName));
    PutObjectResult result = s3.putObject(BUCKET_NAME, fileName, inputStream, metaData);
    inputStream.close();
  }

  public static void deleteS3File(String fileName)
      throws Exception {
    deleteS3File(BUCKET_NAME, fileName);
  }

  public static void deleteS3File(String bucket, String key)
      throws Exception {
    s3.deleteObject(bucket, key);
  }

  public static S3Object getS3File(String fileName)
      throws Exception {
    return s3.getObject(BUCKET_NAME, fileName);
  }

  public static S3Object getS3File(String bucket, String fileName)
      throws Exception {
    return s3.getObject(bucket, fileName);
  }

  public static ObjectMetadata getMetadata(String url) {
    if(url == null){
      return null;
    }
    URLCodec urlCodec = new URLCodec();
    try {
      String cleanUrl = urlCodec.decode(url);

      int domain = cleanUrl.indexOf("s3.amazonaws.com/");

      if(domain == -1) {
        return null;
      }

      if (domain == "https://".length()) {
        String cleaned = cleanUrl.substring("https://s3.amazonaws.com/".length());
        int    params  = cleaned.indexOf('?');
        if (params != -1) {
          cleaned = cleaned.substring(0, params);
        }

        int bucketSplit = cleaned.indexOf('/');
        String bucket = cleaned.substring(0, bucketSplit);
        String key    = cleaned.substring(bucketSplit + 1);
        return getMetadata(bucket, key);
      }


      String bucket = cleanUrl.substring("https://".length(), domain - 1);
      String cleaned = cleanUrl.substring(domain + "s3.amazonaws.com/".length());
      int    params  = cleaned.indexOf('?');
      if (params != -1) {
        cleaned = cleaned.substring(0, params);
      }

      int bucketSplit = cleaned.indexOf('/');
      String key    = cleaned.substring(bucketSplit + 1);
      return getMetadata(bucket, key);


    } catch (DecoderException de) {
      Log.err("S3Utils: Failed to decode S3 URL: ", de);
      return null;
    }
  }

  public static ObjectMetadata getMetadata(String bucket, String key) {
    try {
      return s3.getObjectMetadata(bucket, key);
    } catch (AmazonS3Exception e) {
      Log.err("Failed to obtain metadata for bucket: " + bucket + " key: " + key, e);
      return null;
    }
  }

  public static AmazonS3 getS3Client()
      throws Exception {
    return s3;
  }

  public static S3Object getS3File(AmazonS3Client s3, String fileName)
      throws Exception {
    return s3.getObject(BUCKET_NAME, fileName);
  }

  public static void saveImg(BufferedImage img, String fileName)
      throws IOException {
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    ImageIO.write(img, "png", os);
    byte[] buffer = os.toByteArray();
    InputStream is = new ByteArrayInputStream(buffer);
    ObjectMetadata metaData = new ObjectMetadata();
    metaData.setContentLength(buffer.length);
    metaData.setContentType("image/png");

    PutObjectResult result = s3.putObject(BUCKET_NAME, fileName, is, metaData);
    os.close();
    is.close();
    setObjectPublic(BUCKET_NAME, fileName);
  }


  public static void savePDF(byte[] pdf, String fileName)
      throws IOException {
    InputStream is = new ByteArrayInputStream(pdf);
    ObjectMetadata metaData = new ObjectMetadata();
    metaData.setContentLength(pdf.length);
    metaData.setContentType("application/pdf");

    PutObjectResult result = s3.putObject(new PutObjectRequest(getPDFBucketName(), fileName, is, metaData).withCannedAcl(CannedAccessControlList.PublicRead));
    is.close();
  }


  public static void saveHtml(byte[]  html, String fileName)
      throws IOException {
    saveFile(html, fileName, "text/html");
  }

  public static void saveFile(byte[]  html, String fileName, String contentType)
      throws IOException {
    InputStream is = new ByteArrayInputStream(html);
    ObjectMetadata metaData = new ObjectMetadata();
    metaData.setContentLength(html.length);
    if (contentType != null) {
      metaData.setContentType(contentType);
    } else {
      metaData.setContentType("text/html");
    }
    PutObjectResult result = s3.putObject(new PutObjectRequest(getPDFBucketName(), fileName, is, metaData).withCannedAcl(CannedAccessControlList.PublicRead));
    is.close();
  }

  public static boolean doesPDFexist (String fileName) {
    try {



      s3.getObjectMetadata(getPDFBucketName(), fileName);
    } catch(AmazonServiceException e) {
      return false;
    }
    return true;

  }

  public static String getPDFBucketName () {
    String pdf_bucket = ConfigMgr.getAppParameter(CoreConstants.S3_PDF_BUCKET);
    if (!ConfigMgr.getInstance().isProd()) {
      pdf_bucket = "umapped-pdf-test";
    }
    return pdf_bucket;
  }

  public static String getPDFUrl (String fileName) {
    StringBuilder sb = new StringBuilder();
    sb.append("https://");
    sb.append(getPDFBucketName());
    sb.append(".s3.amazonaws.com/");
    sb.append(fileName);

    return sb.toString();
  }

  public static  List<S3ObjectSummary> getObjectList(String bucketName, String prefix) {
    return getObjectList(s3, bucketName, prefix);
  }

  public static List<S3ObjectSummary> getObjectList(AmazonS3 s3, String bucketName, String prefix, int limit) {
    ObjectListing         listing   = s3.listObjects( bucketName, prefix );
    List<S3ObjectSummary> summaries = listing.getObjectSummaries();

    while (listing.isTruncated()) {
      listing = s3.listNextBatchOfObjects (listing);
      summaries.addAll(listing.getObjectSummaries());
      if (limit > 0 && summaries.size() > limit) {
        break;
      }
    }
    return summaries;
  }

  public static List<S3ObjectSummary> getObjectList(AmazonS3 s3, String bucketName, String prefix) {

    return getObjectList(s3, bucketName, prefix, 0);
  }

  public static  List<String> getCommonPrefixes(String bucketName, String prefix) {
    ListObjectsRequest request = new ListObjectsRequest();
    request.setBucketName(bucketName);
    request.setPrefix(prefix);
    request.setDelimiter("/");
    ObjectListing         listing   = s3.listObjects( request );
    return listing.getCommonPrefixes();
  }

  public static long getObjecCount(String bucketName, String prefix) {
    ObjectListing         listing   = s3.listObjects( bucketName, prefix );

    long count = 0;
    boolean done = false;
    while (!done) {
      List<S3ObjectSummary> summaries = listing.getObjectSummaries();
      count += summaries.size();
      if (listing.isTruncated()) {
        listing = s3.listNextBatchOfObjects(listing);
      } else {
        done = true;
      }
    }
    return count;
  }

  /**
   * Memory friendly way to list objects under a bucket prefix and run a consumer on them
   * @param bucketName
   * @param prefix
   * @param consumer
   */
  public static void forEachObject(final String bucketName, final String prefix, Consumer<S3ObjectSummary> consumer) {
    ObjectListing listing = s3.listObjects( bucketName, prefix );

    boolean done = false;
    while (!done) {
      List<S3ObjectSummary> summaries = listing.getObjectSummaries();
      summaries.forEach(consumer);
      if (listing.isTruncated()) {
        listing = s3.listNextBatchOfObjects(listing);
      } else {
        done = true;
      }
    }
  }

  public static boolean setObjectPublic(String bucket, String key) {
    try {
      s3.setObjectAcl(bucket, key, CannedAccessControlList.PublicRead);
      return true;
    } catch (AmazonClientException e) {
      Log.err("S3Utils: Failed to change object permission to Public Read", e);
      return false;
    }
  }

  /**
   * Configures CacheControl on the object. S3 limits ability to change metadata,
   * therefore it has to be set at the time of the upload or copy.
   * The only way to change metadata after is using Web Console or by performing copy.
   *
   * https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control
   *
   * @param timeoutSeconds number of seconds for timeout, 0 - means never cache, for images and other static content
   *                       recommended value is 31536000 (1 year)
   * @param isNoTransform indicates that no compression or encoding must be used by CDNs or Proxies
   * @param isPublic - Public indicates that browser can cache for all users
   * @return
   */
  public static boolean setCacheControl(ObjectMetadata om,
                                        Long timeoutSeconds,
                                        boolean isNoTransform,
                                        boolean isPublic) {
    try {
      StringBuilder sb = new StringBuilder();
      sb.append("max-age=").append(timeoutSeconds).append(", ");
      if(isNoTransform) {
        sb.append("no-transform, ");
      }
      if(isPublic) {
        sb.append("public");
      }else {
        sb.append("private");
      }

      om.setCacheControl(sb.toString());
      return true;
    }
    catch (AmazonClientException e) {
      Log.err("S3Utils: Failed to change object permission to Public Read", e);
      return false;
    }
  }


  /**
   * Uses setCacheControl method above while copying object to itself.
   * @param bucket
   * @param key
   * @param timeoutSeconds
   * @param isNoTransform
   * @param isPublic
   * @return
   */
  public static boolean setCacheControlViaCopy(String bucket,
                                               String key,
                                               Long timeoutSeconds,
                                               boolean isNoTransform,
                                               boolean isPublic) {
    try {
      // Copying object
      CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucket, key, bucket, key);
      ObjectMetadata    om             = new ObjectMetadata();
      setCacheControl(om, timeoutSeconds, isNoTransform, isPublic);
      copyObjRequest.withNewObjectMetadata(om);
      CopyObjectResult res = s3.copyObject(copyObjRequest);
      if (res != null) {
        return true;
      }
      Log.err("S3Util: setCacheControlViaCopy(): unknown failure for bucket: " + bucket + " key: " + key);
      return false;
    }
    catch (AmazonServiceException ase) {
      Log.err("Caught an AmazonServiceException, which means your request made it " + "to Amazon S3, but was rejected" +
              " with an error response for some reason.");
      Log.err("Error Message:    " + ase.getMessage());
      Log.err("HTTP Status Code: " + ase.getStatusCode());
      Log.err("AWS Error Code:   " + ase.getErrorCode());
      Log.err("Error Type:       " + ase.getErrorType());
      Log.err("Request ID:       " + ase.getRequestId());
    }
    catch (AmazonClientException ace) {
      Log.err("Caught an AmazonClientException, which means the client encountered " + "an internal error while " +
              "trying to  communicate with S3, such as not being able to access the network.");
      Log.err("Error Message: " + ace.getMessage());
    }
    return false;
  }


  public static boolean doesFileexist (String bucket, String fileName) {
    try {
      s3.getObjectMetadata(bucket, fileName);
    } catch(AmazonServiceException e) {
      return false;
    }
    return true;
  }

  /**
   * Add all files from the list to the JSON tree
   * @param files
   * @param filterName  if not null - string that must be present in each path
   * @param filterExtension if not null - string describing file extension (without dot)
   * @return
   */
  public static ObjectNode fileListToJson(List<S3ObjectSummary> files, String filterName, String filterExtension) {
    ObjectNode result = Json.newObject();
    ArrayNode an = result.putArray("files");
    for (S3ObjectSummary f : files) {
      if (filterName != null && !f.getKey().contains(filterName)) {
        continue;
      }
      if (filterExtension != null && ! FilenameUtils.getExtension(f.getKey()).equals(filterExtension)) {
        continue;
      }
      ObjectNode on = Json.newObject();
      String fn = FilenameUtils.getBaseName(f.getKey());
      on.put("name", fn);
      on.put("size", f.getSize());
      on.put("date", Utils.getISO8601Timestamp(f.getLastModified()));
      an.add(on);
    }
    return result;
  }


  public static String getRealBucketName(String bucket) {
    if (ConfigMgr.getInstance().isProd()) {
      return bucket;
    }
    return bucket + "-test";
  }


  public static void debugToS3(String stuffToStore, String typeName, String mimeType) {
    if(stuffToStore == null) {
      return;
    }
    String isoTimestamp = Utils.getISO8601Timestamp(System.currentTimeMillis());
    StringBuilder s3Path = new StringBuilder();
    s3Path.append(typeName);
    s3Path.append('/');
    s3Path.append(isoTimestamp);
    s3Path.append(".debug");

    S3FileInfo s3Info = S3FileInfo.buildInfo(S3Util.getRealBucketName("um-debug"),
                                                           s3Path.toString(),
                                                           stuffToStore,
                                                           mimeType);
    try {
      boolean rc = S3Util.uploadS3Data(s3Info);
      if (rc) {
        Log.info("DEBUG UPLOADED. Type: " + typeName + " Path:" + s3Path);
      }
    } catch (Exception e) {
      Log.err("Failed to save debug data to S3 for type: " + typeName, e);
    }
  }

  public static void replaceFile (File newFile, String bucket, String key) throws IOException{
    ObjectMetadata metaData = getMetadata(bucket, key);
    if (metaData != null) {
      byte[] b = Files.toByteArray(newFile);
      ByteArrayInputStream inputStream = null;
      try {
        inputStream = new ByteArrayInputStream(b);
        metaData.setContentLength(b.length);
        PutObjectResult result = s3.putObject(BUCKET_NAME, key, inputStream, metaData);
      } finally {
        if (inputStream != null) {
          inputStream.close();
        }
      }
    }
  }


  /**
   * Files uploaded to S3 should be easily queried and or downloaded
   *
   * Normalization involves:
   *
   *  # Decoding any filename from any URL encoded sequences
   *  # Converting filename to lower case
   *  # Replacing all non latin characters and arabic digits to underscores
   *
   * @param filename
   * @return
   */
  public static String normalizeFilename(final String filename) {
    String result = null;
    try {
      //Un-url encoding filename
      result = java.net.URLDecoder.decode(filename, "UTF-8");
      //For consistency filenames will be lower case
      result = result.toLowerCase();

      String ext = null;
      int    i   = result.lastIndexOf('.');
      if (i > 0) {
        ext = result.substring(i);
        result = result.substring(0, i);
      }

      //Replacing all unknown characters with underscores
      result = result.replaceAll("[\\W&&[^-]]", "_");

      if(ext != null) {
        result = result + ext;
      }
    }
    catch (UnsupportedEncodingException uee) {
    }
    return result;
  }

  //Valid for older filenames
  private static Pattern umappedFile = Pattern.compile("/([0-9]{5,})_(.*)");

  /**
   * Given the S3 url to one of the Umapped objects figures out bucket, key and filename
   * @param url
   * @return
   */
  public static S3FileInfo parseS3Url(final String url) {
    //Work-In-Progress url
    String wip = url;

    int domain = wip.lastIndexOf(S3_DOMAIN);
    if (domain == -1) {
      return null;
    }

    //Removing all parameters (new urls should not have any)
    int params = url.lastIndexOf('?');
    if (params != -1) {
      wip = url.substring(0, params);
    }

    if (wip.indexOf("https://") == 0) {
      wip = wip.substring(8);
      domain -= 8;
    }
    else if (wip.indexOf("http://") == 0) {
      wip = wip.substring(7);
      domain -= 7;
    }

    S3FileInfo sfi = new S3FileInfo();

    //Extracting bucket name
    if (domain == 0) {
      //S3 Domain is at the beginning of the url like this:
      //https://s3.amazonaws.com/umapped_prd/410089939600065410_Pantheon_Rome___281__29.jpg
      wip = wip.substring(domain + S3_DOMAIN.length());
      int firstSlash = wip.indexOf('/');
      sfi.bucket = wip.substring(0, firstSlash);
      sfi.key = wip.substring(firstSlash + 1);
      wip = wip.substring(sfi.bucket.length());
    }
    else {
      //S3 Domain is a parent domain of the bucket like this:
      //https://umapped-test.s3.amazonaws.com/963137651295032708_article_img_1
      sfi.bucket = wip.substring(0, domain - 1);
      sfi.key = wip.substring(domain + S3_DOMAIN.length());
      wip = wip.substring(domain + S3_DOMAIN.length() - 1);
    }

    Matcher m = umappedFile.matcher(wip);
    if (m.find()) {
      sfi.filename = m.group(2);
    }
    else {
      int lastSlash = wip.lastIndexOf('/');
      if (lastSlash != -1) {
        wip = wip.substring(lastSlash + 1);
      }
      int lastUnderscore = wip.lastIndexOf('_');
      if (lastUnderscore != -1) {
        wip = wip.substring(lastUnderscore + 1);
      }
      sfi.filename = wip;
    }

    //Crazy characters get URL encoded - we need pure filename
    try {
      sfi.key = java.net.URLDecoder.decode(sfi.key, "UTF-8");
      sfi.filename = java.net.URLDecoder.decode(sfi.filename, "UTF-8");
    } catch (UnsupportedEncodingException uee) {}

    return sfi;
  }
}
