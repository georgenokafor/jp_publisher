
package com.mapped.publisher.msg.flightstat.event;

import java.util.List;

public class FlightEquipment{
   	private String actualEquipmentIataCode;
   	private String scheduledEquipmentIataCode;
    private String tailNumber;


 	public String getActualEquipmentIataCode(){
		return this.actualEquipmentIataCode;
	}
	public void setActualEquipmentIataCode(String actualEquipmentIataCode){
		this.actualEquipmentIataCode = actualEquipmentIataCode;
	}
 	public String getScheduledEquipmentIataCode(){
		return this.scheduledEquipmentIataCode;
	}
	public void setScheduledEquipmentIataCode(String scheduledEquipmentIataCode){
		this.scheduledEquipmentIataCode = scheduledEquipmentIataCode;
	}

  public String getTailNumber() {
    return tailNumber;
  }

  public void setTailNumber(String tailNumber) {
    this.tailNumber = tailNumber;
  }
}
