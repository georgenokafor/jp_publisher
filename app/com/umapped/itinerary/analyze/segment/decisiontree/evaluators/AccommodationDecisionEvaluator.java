package com.umapped.itinerary.analyze.segment.decisiontree.evaluators;

import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.recommendation.RecommendedItemType;
import com.umapped.itinerary.analyze.segment.decisiontree.Evaluator;
import com.umapped.itinerary.analyze.segment.AnalyzerContext;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeature;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wei on 2017-03-09.
 */
public class AccommodationDecisionEvaluator
    implements Evaluator {
  @Override public Object evaluate(AnalyzerContext context) {
    List<StayPeriod> datesWithoutAccomodation = new ArrayList<>((List<StayPeriod>) context.getFeature(SegmentFeature
                                                                                                          .DatesWithoutAccommodation));
    StayPeriod existingBooking = (StayPeriod) context.getFeature(SegmentFeature.AccommodationStay);

    if (existingBooking != null) {
      Boolean cancellableAccomomdation = (Boolean) context.getFeature(SegmentFeature.HasCancellableAccomodation);
      if (cancellableAccomomdation) {
        datesWithoutAccomodation.add(existingBooking);
      }
    }

    // TODO: simplify it with the starting location for now
    List<DecisionResult> results = new ArrayList<>();

    if (!datesWithoutAccomodation.isEmpty()) {
      for (StayPeriod p : datesWithoutAccomodation) {
       results.add(new DecisionResult(RecommendedItemType.Accommodation, context.getPossibleLocation(p), p.getStart(), p.getEnd()));
      }
    }
    return results;
  }

}
