package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbJsonB;
import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.databind.JsonNode;
import com.mapped.persistence.util.DBConnectionMgr;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * Created by surge on 2016-02-24.
 */
@Entity
@Table(name = "account_link")
public class AccountLink
    extends Model {
  public enum LinkType {
    @EnumValue("TRV")
    TRAVELER
  }
  public static Model.Finder<Long, AccountLink> find = new Finder(AccountLink.class);
  @Id
  Long pk;
  Long uidFrom;
  Long uidTo;
  @Enumerated
  LinkType linkType;

  @DbJsonB
  JsonNode meta;

  Timestamp createdTs;
  Long      createdBy;
  Timestamp modifiedTs;
  Long      modifiedBy;
  @Version
  int version;

  public static AccountLink build(Long createdBy) {
    AccountLink al = new AccountLink();
    al.setPk(DBConnectionMgr.getUniqueLongId());
    al.setCreatedTs(Timestamp.from(Instant.now()));
    al.setModifiedTs(al.createdTs);
    al.setCreatedBy(createdBy);
    al.setModifiedBy(createdBy);
    return al;
  }

  public static boolean doesLinkExists(Long uidFrom, Long uidTo) {
    int row =  find.where().eq("uidFrom", uidFrom)
                            .eq("uidTo", uidTo)
                            .findCount();
    return ((row > 0)?true : false);
  }

  public Long getPk() {
    return pk;
  }

  public void setPk(Long pk) {
    this.pk = pk;
  }

  public Long getUidFrom() {
    return uidFrom;
  }

  public void setUidFrom(Long uidFrom) {
    this.uidFrom = uidFrom;
  }

  public Long getUidTo() {
    return uidTo;
  }

  public void setUidTo(Long uidTo) {
    this.uidTo = uidTo;
  }

  public LinkType getLinkType() {
    return linkType;
  }

  public void setLinkType(LinkType linkType) {
    this.linkType = linkType;
  }

  public JsonNode getMeta() {
    return meta;
  }

  public void setMeta(JsonNode meta) {
    this.meta = meta;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }


}
