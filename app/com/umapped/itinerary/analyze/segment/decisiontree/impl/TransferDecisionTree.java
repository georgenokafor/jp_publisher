package com.umapped.itinerary.analyze.segment.decisiontree.impl;

import com.umapped.itinerary.analyze.segment.SegmentNature;
import com.umapped.itinerary.analyze.segment.decisiontree.AbstractDecisionTree;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionNode;
import com.umapped.itinerary.analyze.segment.decisiontree.EndDecisionNode;
import com.umapped.itinerary.analyze.segment.decisiontree.SplitDecisionNode;
import com.umapped.itinerary.analyze.segment.decisiontree.evaluators.TransferDecisionEvaluator;

import java.util.ArrayList;

import static com.umapped.itinerary.analyze.segment.feature.SegmentFeature.*;

/**
 * Created by wei on 2017-03-10.
 */
public class TransferDecisionTree extends AbstractDecisionTree {

  @Override protected DecisionNode initTree() {
    EndDecisionNode noRecommendationResult = new EndDecisionNode((context) -> new ArrayList<>(0));
    EndDecisionNode recommendationResult = new EndDecisionNode(new TransferDecisionEvaluator());

    SplitDecisionNode isTransitSegment = new SplitDecisionNode((context) -> context.getCurrent()
                                                                                   .hasNature(SegmentNature.Transit));

    SplitDecisionNode longDuration = new SplitDecisionNode((context) -> ((Integer) context.getFeature(DurationHours)) >= 24);

    SplitDecisionNode hasTransfers = new SplitDecisionNode((context) -> ((Boolean)context.getFeature(HasTransfers)));

    SplitDecisionNode hasActivities = new SplitDecisionNode((context) -> ((Boolean)context.getFeature(HasActivities)));

    SplitDecisionNode hasAccommodations = new SplitDecisionNode((context) -> ((Boolean)context.getFeature(HasAccommondations)));

    isTransitSegment.addNode(Boolean.TRUE, noRecommendationResult).addNode(Boolean.FALSE, longDuration);

    longDuration.addNode(Boolean.TRUE, hasTransfers)
                .addNode(Boolean.FALSE, hasAccommodations);

    hasTransfers.addNode(Boolean.TRUE, noRecommendationResult)
                .addNode(Boolean.FALSE, recommendationResult);

    hasAccommodations.addNode(Boolean.TRUE, hasTransfers)
                     .addNode(Boolean.FALSE, hasActivities);

    hasActivities.addNode(Boolean.TRUE, hasTransfers);
    hasActivities.addNode(Boolean.FALSE, noRecommendationResult);

    return isTransitSegment;
  }
}
