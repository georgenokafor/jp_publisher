package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbJsonB;
import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by george on 2016-11-15.
 */
@Entity
@Table(name = "email_log")
public class EmailLog extends Model {

  public enum EmailStatus {
    @EnumValue("SNT")
    SENT,
    @EnumValue("DEL")
    DELIVERED,
    @EnumValue("PRO")
    PROCESSED,
    @EnumValue("OPN")
    OPENED,
    @EnumValue("CLK")
    CLICKED,
    @EnumValue("BNC")
    BOUNCED,
    @EnumValue("DRP")
    DROPPED,
    @EnumValue("DEF")
    DEFERRED
  }


  public enum EmailTypes {
    @EnumValue("TIT")
    TRAVELLER_ITINERARY,
    @EnumValue("CIT")
    CC_ITINERARY,
    @EnumValue("CIT")
    COLLABORATOR_ITINERARY,
    @EnumValue("PWD")
    PASSWORD_RESET,
    @EnumValue("COL")
    COLLABORATOR_INVITE,
    @EnumValue("CSU")
    COLLABORATOR_SIGN_UP,
    @EnumValue("INV")
    NEW_USER_INVITE,
    @EnumValue("WEL")
    WELCOME,
    @EnumValue("MSG")
    MESSENGER,
    @EnumValue("WRN")
    WARNING,
    @EnumValue("UNL")
    UNLINK_ACCOUNT,
    @EnumValue("LNK")
    LINK_ACCOUNT,
    @EnumValue("NWA")
    NEW_ACCOUNT,
    @EnumValue("FLT")
    FLIGHT_NOTIFICIATION,
    @EnumValue("DIG")
    DIGEST,
    @EnumValue("TPN")
    TRIP_PUBLISH_NOTIFICATION
  }

  public static Model.Finder<Long, EmailLog> find = new Finder(EmailLog.class);


  @Id
  Long pk;
  Long account_uid;
  String to_email;
  String subject;
  @Enumerated(value = EnumType.STRING)
  EmailTypes type;
  @Enumerated(value = EnumType.STRING)
  EmailStatus status;
  String meta;
  Timestamp modified_ts;
  Timestamp sent_ts;
  Timestamp delivered_ts;
  Timestamp failed_ts;
  Timestamp processed_ts;
  Timestamp opened_ts;
  Timestamp clicked_ts;
  int click_counter;
  int open_counter;
  Boolean archive;
  @Version
  public int version;

  public static List<EmailLog> findByTrip(String tripId, EmailTypes emailType) {
    return find.where()
               .eq("meta", tripId)
               .eq("type", emailType)
               .orderBy("sent_ts desc")
               .findList();
  }
  

  public Long getPk() {
    return pk;
  }

  public void setPk(Long pk) {
    this.pk = pk;
  }

  public Long getAccount_uid() {
    return account_uid;
  }

  public void setAccount_uid(Long account_uid) {
    this.account_uid = account_uid;
  }

  public String getTo_email() {
    return to_email;
  }

  public void setTo_email(String to_email) {
    this.to_email = to_email;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public EmailTypes getType() {
    return type;
  }

  public void setType(EmailTypes type) {
    this.type = type;
  }

  public EmailStatus getStatus() {
    return status;
  }

  public void setStatus(EmailStatus status) {
    this.status = status;
  }


  public Timestamp getModified_ts() {
    return modified_ts;
  }

  public void setModified_ts(Timestamp modified_ts) {
    this.modified_ts = modified_ts;
  }

  public Timestamp getSent_ts() {
    return sent_ts;
  }

  public void setSent_ts(Timestamp sent_ts) {
    this.sent_ts = sent_ts;
  }

  public Timestamp getDelivered_ts() {
    return delivered_ts;
  }

  public void setDelivered_ts(Timestamp delivered_ts) {
    this.delivered_ts = delivered_ts;
  }

  public Timestamp getFailed_ts() {
    return failed_ts;
  }

  public void setFailed_ts(Timestamp failed_ts) {
    this.failed_ts = failed_ts;
  }

  public Timestamp getProcessed_ts() {
    return processed_ts;
  }

  public void setProcessed_ts(Timestamp processed_ts) {
    this.processed_ts = processed_ts;
  }

  public Timestamp getOpened_ts() {
    return opened_ts;
  }

  public void setOpened_ts(Timestamp opened_ts) {
    this.opened_ts = opened_ts;
  }

  public Timestamp getClicked_ts() {
    return clicked_ts;
  }

  public void setClicked_ts(Timestamp clicked_ts) {
    this.clicked_ts = clicked_ts;
  }

  public int getClick_counter() {
    return click_counter;
  }

  public void setClick_counter(int click_counter) {
    this.click_counter = click_counter;
  }

  public int getOpen_counter() {
    return open_counter;
  }

  public void setOpen_counter(int open_counter) {
    this.open_counter = open_counter;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public String getMeta() {
    return meta;
  }

  public void setMeta(String meta) {
    this.meta = meta;
  }

  public Boolean getArchive() {
    return archive;
  }

  public boolean isArchive() {
    if (archive != null) {
      return archive;
    }
    return false;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
