
package com.umapped.external.bing.image;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "pivot",
    "suggestions"
})
public class PivotSuggestion {

    @JsonProperty("pivot")
    private String pivot;
    @JsonProperty("suggestions")
    private List<Suggestion> suggestions = new ArrayList<Suggestion>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The pivot
     */
    @JsonProperty("pivot")
    public String getPivot() {
        return pivot;
    }

    /**
     * 
     * @param pivot
     *     The pivot
     */
    @JsonProperty("pivot")
    public void setPivot(String pivot) {
        this.pivot = pivot;
    }

    /**
     * 
     * @return
     *     The suggestions
     */
    @JsonProperty("suggestions")
    public List<Suggestion> getSuggestions() {
        return suggestions;
    }

    /**
     * 
     * @param suggestions
     *     The suggestions
     */
    @JsonProperty("suggestions")
    public void setSuggestions(List<Suggestion> suggestions) {
        this.suggestions = suggestions;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "PivotSuggestion{" +
            "pivot='" + pivot + '\'' +
            ", suggestions=" + suggestions +
            ", additionalProperties=" + additionalProperties +
            '}';
    }
}
