package com.umapped.itinerary.analyze.model.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Created by wei on 2017-03-29.
 */
public class ZoneIdJsonMarshaller {
  public static class ZoneIdDeserializer extends StdDeserializer<ZoneId> {
    public ZoneIdDeserializer() {
      super(ZoneId.class);
    }

    public ZoneIdDeserializer(Class<ZoneId> vc) {
      super(vc);
    }

    @Override public ZoneId deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
        throws IOException, JsonProcessingException {
      String text = jsonParser.getText();
      if (StringUtils.isEmpty(text)) {
        return null;
      }
      else {
        return ZoneId.of(text);
      }
    }
  }

  /**
   * Created by wei on 2017-02-28.
   */
  public static class ZoneIdSerializer
      extends StdSerializer<ZoneId> {

    public ZoneIdSerializer() {
      super(ZoneId.class);
    }

    public ZoneIdSerializer(Class<ZoneId> t) {
      super(t);
    }

    @Override public void serialize(ZoneId zoneId, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
        throws IOException {
      if (zoneId != null) {
        jsonGenerator.writeString(zoneId.toString());
      }
    }
  }
}
