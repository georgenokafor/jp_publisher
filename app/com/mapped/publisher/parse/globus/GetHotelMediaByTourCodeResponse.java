
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetHotelMediaByTourCodeResult" type="{http://webapi.globusandcosmos.com/}ArrayOfHotelMedia" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getHotelMediaByTourCodeResult"
})
@XmlRootElement(name = "GetHotelMediaByTourCodeResponse")
public class GetHotelMediaByTourCodeResponse {

    @XmlElement(name = "GetHotelMediaByTourCodeResult")
    protected ArrayOfHotelMedia getHotelMediaByTourCodeResult;

    /**
     * Gets the value of the getHotelMediaByTourCodeResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfHotelMedia }
     *     
     */
    public ArrayOfHotelMedia getGetHotelMediaByTourCodeResult() {
        return getHotelMediaByTourCodeResult;
    }

    /**
     * Sets the value of the getHotelMediaByTourCodeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfHotelMedia }
     *     
     */
    public void setGetHotelMediaByTourCodeResult(ArrayOfHotelMedia value) {
        this.getHotelMediaByTourCodeResult = value;
    }

}
