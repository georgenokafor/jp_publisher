package models.publisher;

import java.util.Map;

import models.utils.JsonObjectMapper;
import models.utils.PersistenceUtilFactory;

/**
 * Mixin interface to support json in the model
 * 
 * @author wei
 *
 */
public interface JsonSupport {
  default JsonObjectMapper getJsonObjectMapper() {
    return PersistenceUtilFactory.get(JsonObjectMapper.class);
  }
  
  default public <T> T unmarshal(Map<String, Object> jsonTree, Class<T> type) {
    return getJsonObjectMapper().unmarshal(jsonTree, type);
  }
  
  default public Map<String, Object> marshal(Object value) {
    return getJsonObjectMapper().marshalToMap(value);
  }
}
