package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.topatlantico.NortravelParser;
import com.mapped.publisher.parse.extractor.booking.topatlantico.SolferiasParser;
import com.mapped.publisher.parse.extractor.booking.topatlantico.SoltourParser;
import com.mapped.publisher.parse.extractor.booking.valueObject.AirportVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.FlightVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.PassengerVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.InputStream;
import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2014-04-03
 * Time: 8:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopAtlanticoExtractor extends BookingExtractor {
    private static HashMap<String, String> months = null;
    private static String[] dateTimeFormat = {"dd MMM yyyy", "E dd MMM yyyy h:mma",  "E dd MMM yyyy", "E dd MMMMM yyyy h:mm a" , "dd MMM yyyy h:mm a", "E dd MMM yyyy h:mm a" , "dd MMMMM yyyy h:mm a", "dd-MMM yyyy HH:mm"} ;
    private static String[] dateFormat = {"dd MMMMM yyyy", "E dd MMM yyyy"};

    private static Pattern dayDatePattern = Pattern.compile("[0-9]?[0-9]-(JAN|FEB|MAR|ABR|MAI|JUN|JUL|AGO|SET|OUT|NOV|DEZ)", Pattern.CASE_INSENSITIVE);
    private static Pattern timePattern = Pattern.compile("[0-9][0-9]:[0-9][0-9]", Pattern.CASE_INSENSITIVE);

    private static Pattern airportPattern = Pattern.compile(" [A-Z][A-Z][A-Z] ");
    private DateUtils du = new DateUtils();

    protected void initImpl(Map<String, String> params) {
         if (months == null) {
             months = new HashMap<String, String>();
             months.put ("JAN", "JAN");
             months.put ("FEB", "FEB");
             months.put ("MAR", "MAR");
             months.put ("ABR", "APR");
             months.put ("MAI", "MAY");
             months.put ("JUN", "JUN");
             months.put ("JUL", "JUL");
             months.put ("AGO", "AUG");
             months.put ("SET", "SEP");
             months.put ("OUT", "OCT");
             months.put ("NOV", "NOV");
             months.put ("DEZ", "DEC");

         }
    }


    protected TripVO extractDataImpl(InputStream input) throws Exception {
        try {
            PDDocument document = PDDocument.load(input);
            PDFTextStripper textStripper = new PDFTextStripper();
            textStripper.setSortByPosition(true);

          String s1 = textStripper.getText(document);

          if (s1.contains("Soliférias") || s1.contains("SOLIFÉRIAS")) {
            SolferiasParser solferiasParser =  new SolferiasParser(s1);
            return solferiasParser.getTrip();

          } else if (s1.contains("Soltour")) {
            SoltourParser soltourParser = new SoltourParser(s1);
            return soltourParser.getTrip();

          } else if (s1.contains("NORTRAVEL")) {
            NortravelParser nortravelParser = new NortravelParser(s1);
            return nortravelParser.getTrip();

          } else {
            return parseFlightNonuis(s1);
          }


        } catch (Exception e) {
          e.printStackTrace();
        } finally {
            if (input != null) {
                input.close();
            }
        }

        return null;
    }

    private TripVO parseFlightNonuis (String s1) {
      TripVO trip = new TripVO();
      ArrayList<String> passengers = new ArrayList<String>();
      String reservationCode = "";
      StringBuffer buffer = new StringBuffer();

      String[] data = s1.split("\n");

      for (int i = 0; i < data.length; i++) {
        String s = data[i];


        boolean foundFlight = false;
        String departDate = null;
        if (s.contains("Airline Reservation Number")) {
          i++;
          if (i < data.length) {
            while (true) {
              String airlineInfo = data[i];
              if (airlineInfo != null) {
                int startIndex = airlineInfo.indexOf(":");
                if (startIndex > 0) {
                  airlineInfo = airlineInfo.substring(startIndex);

                }
              }
              if (airlineInfo != null) {
                airlineInfo = airlineInfo.trim();
                if (!reservationCode.contains(airlineInfo)) {
                  reservationCode += airlineInfo + "\n";
                }
              }

              if (i == data.length || (i + 1) == data.length) {
                break;
              } else {
                String nextLine = data[i + 1];
                if (nextLine.startsWith("Informa")) {

                  break;
                } else
                  i++;
              }
            }
          }

        } else if (s.contains("Passenger Name")) {
          i++;
          if (i < data.length ) {
            String [] tokens = data[i].split("-");
            if (tokens != null) {
              String name = tokens[tokens.length - 1].trim();
              int startIndex = name.indexOf(" ");
              if (startIndex > 0) {
                String passengerName = name.substring(startIndex);
                if (passengerName != null && passengerName.trim().length() > 0 && !passengers.contains(passengerName.trim())) {
                  passengers.add(passengerName.trim());
                }
              }
            }
          }
        } else if (s.contains("Partida:")) {
          //process flight Info
          departDate = parseDate(s);

          // process flight info
          if (departDate != null) {
            //process flight info
            String duration = null;
            String departAirport = null;
            String arriveAirport = null;
            String departTime = null;
            String arriveTime = null;
            String departTerminal = null;
            String arriveTerminal = null;
            String airline = null;
            String flightNum = null;
            String bookingCode = null;
            String departCity = null;
            String departCountry = null;
            String arriveCity = null;
            String arriveCountry = null;


            String arriveDate = departDate;

            while (true) {

              String flightInfo = data[i];
              if (flightInfo.contains("Partida:")) {
                departTime = parseTime(flightInfo);
                if (departTime != null) {
                  departTime = departTime.trim();
                }
                //get airport
                departAirport = parseAirportCode(flightInfo);


              } else if (flightInfo.contains("Chegada:")) {
                if (flightInfo.indexOf("Chegada:") > 0) {
                  flightNum = flightInfo.substring(0, flightInfo.indexOf("Chegada:"));
                }
                arriveDate = parseDate(flightInfo);
                arriveTime = parseTime(flightInfo);
                arriveAirport = parseAirportCode(flightInfo);


              }


              if (i == data.length || (i + 1) == data.length) {
                break;
              } else {
                String nextLine = data[i + 1];
                if (nextLine.startsWith("Partida:") || nextLine.startsWith("Recibo Bilhete Elect")) {

                  break;
                } else
                  i++;
              }


            }

            try {
              FlightVO flightVO = new FlightVO();
              flightVO.setPassengers(new ArrayList<PassengerVO>());

              flightVO.setArrivalAirport(new AirportVO());
              flightVO.setDepartureAirport(new AirportVO());
              flightVO.setArrivalTime(getTimestamp(arriveDate, arriveTime));
              flightVO.setDepatureTime(getTimestamp(departDate, departTime));
              flightVO.setReservationNumber(bookingCode);
              if (flightNum != null && flightNum.length() > 3) {
                flightVO.setCode(flightNum.substring(0, 2));
                flightVO.setNumber(flightNum.substring(2));
              } else
                flightVO.setNumber(flightNum);

              flightVO.setName(airline);
              flightVO.getArrivalAirport().setName(arriveAirport);
              flightVO.getArrivalAirport().setTerminal(arriveTerminal);
              flightVO.getArrivalAirport().setCode(arriveAirport);
              flightVO.getDepartureAirport().setName(departAirport);
              flightVO.getDepartureAirport().setTerminal(departTerminal);
              flightVO.getDepartureAirport().setCode(departAirport);
              flightVO.setReservationNumber(reservationCode);

              if (flightVO.getDepartureAirport() != null && flightVO.getDepatureTime() != null && flightVO.getCode() != null) {
                //check  to make sure we don't have duplicates
                boolean isDuplicate = false;
                if (trip.getFlights() != null) {
                  for (FlightVO f : trip.getFlights()) {
                    if (f.getDepartureAirport().getCode() != null && flightVO.getDepartureAirport().getCode() != null && f.getDepartureAirport().getCode().equals(flightVO.getDepartureAirport().getCode()) && f.getDepatureTime().equals(flightVO.getDepatureTime()) && f.getCode().equals(flightVO.getCode())) {
                      isDuplicate = true;
                      break;
                    }
                  }
                }

                if (!isDuplicate) {
                  trip.addFlight(flightVO);
                }
              }

            } catch (Exception e) {
              e.printStackTrace();
            }


          }
        }



      }

      //check to see if we need to add passengers to the flights
      for (String name : passengers) {
        PassengerVO passengerVO = new PassengerVO();
        passengerVO.setNote(name);
        if (trip.getFlights() != null) {
          for (FlightVO flightVO: trip.getFlights()) {
            flightVO.getPassengers().add(passengerVO);

          }
        }
      }
      trip.setImportSrc(BookingSrc.ImportSrc.TA_NONIUS_PDF);
      trip.setImportSrcId(reservationCode);
      trip.setImportTs(System.currentTimeMillis());
      return trip;
    }


    private String parseDate(String s) {
        if (s != null) {

            int count = 0;
            Matcher m = dayDatePattern.matcher(s);
            while (m.find()) {
                if (count == 0) {
                    String departDate = m.group();

                    return departDate;
                }
                count++;
            }
        }

        return null;

    }

    private String parseTime (String s) {
        if (s != null) {

            int count = 0;
            Matcher m = timePattern.matcher(s);
            while (m.find()) {
                if (count ==0 ) {
                    String departDate = m.group();

                    return departDate;
                }
                count++;
            }
        }

        return null;

    }
    private String parseAirportCode (String s) {
        if (s != null) {

            int count = 0;
            Matcher m = airportPattern.matcher(s);
            while (m.find()) {
                if (count ==0 ) {
                    String airportCode = m.group();
                    if (airportCode != null)
                        airportCode = airportCode.trim();

                    return airportCode;
                }
                count++;
            }
        }

        return null;

    }

    private Timestamp getTimestamp (String date, String time) {
        try {
            int monthIndex = 0;
            for (String m1 : months.keySet()) {
                if (date.toUpperCase().contains(m1)) {
                    String m = months.get(m1);
                    date = date.toUpperCase().replace(m1, m);
                    break;
                }
                monthIndex++;

            }
            int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
            int currentYear = Calendar.getInstance().get(Calendar.YEAR);


            if (currentMonth > monthIndex) {
                currentYear++;
            }
            date  =  date + " " +  currentYear;

            Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime() );

            return t;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<ParseError> getParseErrors()
    {
        return null;
    }

  @Override
  public void postProcessingImpl(String tripId, String userId) {
  }
}
