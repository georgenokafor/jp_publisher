
package com.mapped.publisher.parse.globus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShipMedia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipMedia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HotelCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CrewMembers" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NumberOfStaterooms" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NumberOfSuites" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="LengthInFeet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="YearBuilt" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="YearLastRenovated" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Images" type="{http://webapi.globusandcosmos.com/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="Amenities" type="{http://webapi.globusandcosmos.com/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="PublicAreas" type="{http://webapi.globusandcosmos.com/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="Optionals" type="{http://webapi.globusandcosmos.com/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="Dining" type="{http://webapi.globusandcosmos.com/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="StateRoomTypes" type="{http://webapi.globusandcosmos.com/}ArrayOfRoomMedia" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipMedia", propOrder = {
    "name",
    "hotelCode",
    "description",
    "crewMembers",
    "numberOfStaterooms",
    "numberOfSuites",
    "lengthInFeet",
    "yearBuilt",
    "yearLastRenovated",
    "images",
    "amenities",
    "publicAreas",
    "optionals",
    "dining",
    "stateRoomTypes"
})
public class ShipMedia {

    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "HotelCode")
    protected String hotelCode;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "CrewMembers")
    protected int crewMembers;
    @XmlElement(name = "NumberOfStaterooms")
    protected int numberOfStaterooms;
    @XmlElement(name = "NumberOfSuites")
    protected int numberOfSuites;
    @XmlElement(name = "LengthInFeet")
    protected String lengthInFeet;
    @XmlElement(name = "YearBuilt")
    protected int yearBuilt;
    @XmlElement(name = "YearLastRenovated")
    protected int yearLastRenovated;
    @XmlElement(name = "Images")
    protected ArrayOfString images;
    @XmlElement(name = "Amenities")
    protected ArrayOfString amenities;
    @XmlElement(name = "PublicAreas")
    protected ArrayOfString publicAreas;
    @XmlElement(name = "Optionals")
    protected ArrayOfString optionals;
    @XmlElement(name = "Dining")
    protected ArrayOfString dining;
    @XmlElement(name = "StateRoomTypes")
    protected ArrayOfRoomMedia stateRoomTypes;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the hotelCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotelCode() {
        return hotelCode;
    }

    /**
     * Sets the value of the hotelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotelCode(String value) {
        this.hotelCode = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the crewMembers property.
     * 
     */
    public int getCrewMembers() {
        return crewMembers;
    }

    /**
     * Sets the value of the crewMembers property.
     * 
     */
    public void setCrewMembers(int value) {
        this.crewMembers = value;
    }

    /**
     * Gets the value of the numberOfStaterooms property.
     * 
     */
    public int getNumberOfStaterooms() {
        return numberOfStaterooms;
    }

    /**
     * Sets the value of the numberOfStaterooms property.
     * 
     */
    public void setNumberOfStaterooms(int value) {
        this.numberOfStaterooms = value;
    }

    /**
     * Gets the value of the numberOfSuites property.
     * 
     */
    public int getNumberOfSuites() {
        return numberOfSuites;
    }

    /**
     * Sets the value of the numberOfSuites property.
     * 
     */
    public void setNumberOfSuites(int value) {
        this.numberOfSuites = value;
    }

    /**
     * Gets the value of the lengthInFeet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLengthInFeet() {
        return lengthInFeet;
    }

    /**
     * Sets the value of the lengthInFeet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLengthInFeet(String value) {
        this.lengthInFeet = value;
    }

    /**
     * Gets the value of the yearBuilt property.
     * 
     */
    public int getYearBuilt() {
        return yearBuilt;
    }

    /**
     * Sets the value of the yearBuilt property.
     * 
     */
    public void setYearBuilt(int value) {
        this.yearBuilt = value;
    }

    /**
     * Gets the value of the yearLastRenovated property.
     * 
     */
    public int getYearLastRenovated() {
        return yearLastRenovated;
    }

    /**
     * Sets the value of the yearLastRenovated property.
     * 
     */
    public void setYearLastRenovated(int value) {
        this.yearLastRenovated = value;
    }

    /**
     * Gets the value of the images property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getImages() {
        return images;
    }

    /**
     * Sets the value of the images property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setImages(ArrayOfString value) {
        this.images = value;
    }

    /**
     * Gets the value of the amenities property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getAmenities() {
        return amenities;
    }

    /**
     * Sets the value of the amenities property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setAmenities(ArrayOfString value) {
        this.amenities = value;
    }

    /**
     * Gets the value of the publicAreas property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getPublicAreas() {
        return publicAreas;
    }

    /**
     * Sets the value of the publicAreas property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setPublicAreas(ArrayOfString value) {
        this.publicAreas = value;
    }

    /**
     * Gets the value of the optionals property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getOptionals() {
        return optionals;
    }

    /**
     * Sets the value of the optionals property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setOptionals(ArrayOfString value) {
        this.optionals = value;
    }

    /**
     * Gets the value of the dining property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getDining() {
        return dining;
    }

    /**
     * Sets the value of the dining property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setDining(ArrayOfString value) {
        this.dining = value;
    }

    /**
     * Gets the value of the stateRoomTypes property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfRoomMedia }
     *     
     */
    public ArrayOfRoomMedia getStateRoomTypes() {
        return stateRoomTypes;
    }

    /**
     * Sets the value of the stateRoomTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfRoomMedia }
     *     
     */
    public void setStateRoomTypes(ArrayOfRoomMedia value) {
        this.stateRoomTypes = value;
    }

}
