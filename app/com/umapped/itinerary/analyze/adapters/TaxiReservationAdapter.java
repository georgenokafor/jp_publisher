package com.umapped.itinerary.analyze.adapters;

import com.mapped.publisher.parse.schemaorg.Reservation;
import com.mapped.publisher.parse.schemaorg.TaxiReservation;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.Location;

/**
 * Created by wei on 2017-03-22.
 */
public class TaxiReservationAdapter
    extends AbstractReservationItinearyItemAdapter {

  @Override protected ItineraryItemType getItemType(Reservation reservation) {
    return ItineraryItemType.ShortTransfer;
  }

  @Override protected Location getStartLocation(Reservation reservation) {
    TaxiReservation r = (TaxiReservation) reservation;
    return getLocationFromPlace(r.pickupLocation);
  }
}
