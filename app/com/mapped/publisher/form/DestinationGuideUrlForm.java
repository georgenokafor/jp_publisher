package com.mapped.publisher.form;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-03-07
 * Time: 5:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class DestinationGuideUrlForm {
    private String inDestGuideUrlType;
    private String inDestGuideUrl;
    private String inDestGuideUrlCaption;
    private String inDestGuideUrlTitle;

    private String inDestId;
    private String inDestGuideId;
    private String inDestGuideUrlId;

    public String getInDestGuideUrlType() {
        return inDestGuideUrlType;
    }

    public void setInDestGuideUrlType(String inDestGuideUrlType) {
        this.inDestGuideUrlType = inDestGuideUrlType;
    }

    public String getInDestGuideUrl() {
        return inDestGuideUrl;
    }

    public void setInDestGuideUrl(String inDestGuideUrl) {
        this.inDestGuideUrl = inDestGuideUrl;
    }

    public String getInDestGuideUrlCaption() {
        return inDestGuideUrlCaption;
    }

    public void setInDestGuideUrlCaption(String inDestGuideUrlCaption) {
        this.inDestGuideUrlCaption = inDestGuideUrlCaption;
    }

    public String getInDestId() {
        return inDestId;
    }

    public void setInDestId(String inDestId) {
        this.inDestId = inDestId;
    }

    public String getInDestGuideId() {
        return inDestGuideId;
    }

    public void setInDestGuideId(String inDestGuideId) {
        this.inDestGuideId = inDestGuideId;
    }

    public String getInDestGuideUrlId() {
        return inDestGuideUrlId;
    }

    public void setInDestGuideUrlId(String inDestGuideUrlId) {
        this.inDestGuideUrlId = inDestGuideUrlId;
    }

    public String getInDestGuideUrlTitle() {
        return inDestGuideUrlTitle;
    }

    public void setInDestGuideUrlTitle(String inDestGuideUrlTitle) {
        this.inDestGuideUrlTitle = inDestGuideUrlTitle;
    }
}
