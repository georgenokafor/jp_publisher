package parsers;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.ResultSet;

import au.com.bytecode.opencsv.CSVWriter;
import com.umapped.external.virtuoso.hotel.HotelInfo;
import com.umapped.external.virtuoso.hotel.Root;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 * Created by george on 2016-02-18.
 */
public class ParseVirtuosoCSV {
  public static void main(String[] args) throws Exception {

    //CSV file header

    String[] header = {"Type", "Name", "Description", "Addr1", "Addr2", "City", "Zip", "State", "Country",
        "Lat", "Long", "Email", "Web", "Phone", "Fax", "Facebook", "Twitter", "Img", "Tag"};


    try {

      File file = new File("/volumes/data2/Downloads/HotelExport_20160517_071432_V4_0.xml");
      JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);

      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      //jaxbUnmarshaller.setProperty("jaxb.encoding", "UTF-8");
      Root root = (Root) jaxbUnmarshaller.unmarshal(file);


      //FileWriter fileWriter = null;
      FileWriter fileWriter = null;

      String outputFile = "/volumes/data2/Downloads/virtuoso_hotel_may_17.csv";

      boolean alreadyExists = new File(outputFile).exists();


      try {
        fileWriter = new FileWriter(outputFile, true);

        CSVWriter csvWriter = new CSVWriter(fileWriter);

        // if the file didn't already exist then we need to write out the header line
        if (!alreadyExists) {
          //Write the CSV file header
          csvWriter.writeNext(header);
        }

        //Write a new student object list to the CSV file

        int j = 0;
        int imgs = 0;
        for (HotelInfo r : root.getHotel().getHotelInfo()){
        //  System.out.println(r);
          if (r.getProductImages() != null && r.getProductImages().getImage().size() > 0) {

            StringBuffer imageURL = new StringBuffer();
            int i;

            for (i = 0; i < r.getProductImages().getImage().size(); i++) {
              if (r.getProductImages().getImage().get(i).getProductImageURL().contains("photo.aspx")) {
                imageURL.append(r.getProductImages().getImage().get(i).getProductImageURL());
                imageURL.append("====");
                imgs++;
              }
            }


            String[] line = {"Hotel", r.getCompanyName(), "", r.getAddress(), "", r.getCity(), r.getPostalCode(), r.getState(), r

                .getCountry(), "", "", "", "", "", "", "", "", imageURL.toString(), ""};

            csvWriter.writeNext(line);

            j++;
          }


          //String rs = r.getNoteContent().replace("\r", "");
          //System.out.println(rs);
          //System.out.println(r.getNoteContent());
        }

        System.out.println("CSV file was created successfully !!! " + j + " imgs: " + imgs);

      } catch (Exception e) {
        System.out.println("Error in CsvFileWriter !!!");
        e.printStackTrace();
      } finally {

        try {
          fileWriter.flush();
          fileWriter.close();
        } catch (IOException e) {
          System.out.println("Error while flushing/closing fileWriter !!!");
          e.printStackTrace();
        }

      }

    } catch (JAXBException e) {
      e.printStackTrace();
    }
  }
}
