package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.google.inject.Inject;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.persistence.TripBrandingMgr;
import com.mapped.publisher.persistence.redis.RedisKeys;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.view.AgentView;
import com.umapped.helper.TripPublisherHelper;
import controllers.TourController;
import models.publisher.*;
import org.apache.commons.lang3.time.StopWatch;
import play.i18n.Lang;

import java.io.Serializable;
import java.util.Optional;

/**
 * Created by twong on 2016-08-09.
 * Actor to republish a trip automatically - gets invoked automatically when a trip has been changed without the agent
 * intervention and needs to be republished
 */
public class TripPublisherActor
    extends UntypedActor {
  /**
   * Akka Actor Properties
   */
  public final static Props props = Props.create(TripPublisherActor.class);

  @Inject
  RedisMgr redisMgr;

  public static class Command
      implements Serializable {
    private String tripId;
    private String groupId;

    public Command(String tripId, String groupId) {
      this.tripId = tripId;
      this.groupId = groupId;
    }

    public String getTripId() {
      return tripId;
    }

    public void setTripId(String tripId) {
      this.tripId = tripId;
    }

    public String getGroupId() {
      return groupId;
    }

    public void setGroupId(String groupId) {
      this.groupId = groupId;
    }
  }

  @Override
  public void onReceive(Object message)
      throws Exception {

    if (message instanceof Command) {
      StopWatch sw = new StopWatch();
      sw.start();

      String tripId  = ((Command) message).getTripId();
      String groupId = ((Command) message).getGroupId();
      Log.info("Auto publishing the trip: " + tripId + " Group: " + groupId);

      String key = tripId;
      if (groupId != null) {
        key += groupId;
      }

      redisMgr.hashDel(RedisKeys.H_TRIPS_TO_PUBLISH, null, RedisMgr.WriteMode.FAST, key);

      Trip               trip          = Trip.find.byId(((Command) message).getTripId());
      TripPublishHistory lastPublished = null;
      lastPublished = TripPublishHistory.getLastPublished(tripId, groupId);

      if (trip != null && lastPublished != null) {
        TripGroup group                 = null;
        boolean   allowSelfRegistration = false;

        long endtimestamp = trip.endtimestamp + (23 * 60 * 60 * 1000) + (59 * 60 * 1000) + (59 * 1000);

        if (endtimestamp < (System.currentTimeMillis() - (8 * 60 * 60 * 1000))) {   //fudge the validation by 8
          // hours to account for west coast time
          Log.info("TripPublisherActor - cannot publish old trip: " + trip.getTripid());
          return;
        }
        String mapPk = trip.tripid;
        //check for group
        if (groupId != null && groupId.trim().length() > 0) {
          group = TripGroup.findByPK(groupId);
          if (group == null || !group.getTripid().equals(tripId)) {
            group = null;
          }
          else {
            if (group.getAllowselfregistration() == APPConstants.ALLOW_SELF_REGISTRATION) {
              allowSelfRegistration = true;
            }
            mapPk = TourController.getCompositePK(tripId, group.groupid);
          }
        }
        if (group == null) {
          if (trip.getVisibility() == APPConstants.ALLOW_SELF_REGISTRATION) {
            allowSelfRegistration = true;
          }
        }

        AgentView   agentView = new AgentView();
        UserProfile up        = UserProfile.find.byId(lastPublished.getCreatedby());
        up.setEmail(TripBrandingMgr.getWhieLabelEmail(trip,
                                                      up.getEmail())); //allow overwrite of email domain for ski.com);
        Company cmpy = Company.find.byId(trip.cmpyid);
        agentView.agentName = up.firstname + " " + up.lastname;

        //call common publish routine
        try {
          TripPublisherHelper.publishTrip(trip,
                                          up,
                                          mapPk,
                                          agentView,
                                          cmpy,
                                          true,
                                          true,
                                          true,
                                          allowSelfRegistration,
                                          null,
                                          group,
                                          Lang.forCode("en-US"),
                                          true);
        }
        catch (Exception e) {
          e.printStackTrace();
          Log.err("Error publishing the trip: " + tripId + " Group: " + groupId, e);
        }
      }
      else {
        Log.err("Cannot publish the trip  " + tripId + " Group: " + groupId);
      }
    }
  }

  public interface Factory {
    public Actor create();
  }
}
