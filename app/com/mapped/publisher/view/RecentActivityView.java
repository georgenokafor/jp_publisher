package com.mapped.publisher.view;

import com.mapped.publisher.audit.AuditActionType;
import com.mapped.publisher.audit.event.*;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.LogLevel;
import com.mapped.publisher.utils.Utils;
import controllers.routes;
import models.publisher.Trip;
import models.publisher.TripAudit;

import java.util.List;

/**
 * Created by surge on 2014-05-26.
 */
public class RecentActivityView
    extends BaseView {

  public Trip tripModel;
  public List<TripAudit> auditEvents;
  public int nextStartPos;
  public int nextMaxResults;
  public int prevStartPos;
  public long localTimeOffset = 0;

  /**
   * Has more does not guarantee that here are more results, but
   * indicates that previous search returned maximum number of records.
   */
  public boolean hasMore;


  /**
   * Return localized text representation of audit event time
   */
  public String getTime(TripAudit ta){
    return Utils.formatOffsetDateTimePrint(ta.eventtime, localTimeOffset);
  }

  public String getAgentName(TripAudit ta){
    return ta.userid;
  }

  public String getAction(TripAudit ta){
    switch(ta.action){
      case ADD:
        return "New";
      case MODIFY:
        return "Modified";
      case DELETE:
        return "Removed";
      case COPY:
        return "Copied";
      case MERGE:
        return "Merged";
      default:
        return "";
    }
  }

  public String getDescription(TripAudit ta) {
    StringBuilder sb = new StringBuilder();
    if (ta.auditRecord != null && ta.auditRecord.details != null) {
      switch (ta.module) {
        case TRIP:
          AuditTrip tDetails = (AuditTrip) ta.auditRecord.details;
          if (tDetails.currentAction != null && tDetails.group !=null) {
            switch (tDetails.currentAction) {
              case NEW:
                sb.append("New Trip: ").append(tDetails.name);
                break;
              case MODIFIED:
                sb.append("Trip: ").append(tDetails.name);
                break;
              case SENT_FOR_REVIEW:
                sb.append("Sent for Review Trip: ")
                  .append(tDetails.name)
                  .append(" to Travel Group: ")
                  .append(tDetails.group.name);
                break;
              case PUBLISHED:
                sb.append("Published Trip: ")
                  .append(tDetails.name)
                  .append(" for Travel Group: ")
                  .append(tDetails.group.name);
                break;
              case REPUBLISHED:
                sb.append("Republished Trip: ")
                  .append(tDetails.name)
                  .append(" for Travel Group: ")
                  .append(tDetails.group.name);
            }
          }
          else {
            sb.append("Trip: ").append(tDetails.name);
          }
          break;
        case TRIP_TRAVELLER:
          AuditTripTraveller trDetails = (AuditTripTraveller) ta.auditRecord.details;

          switch(ta.auditRecord.action){
            case ADD:
              sb.append("Added Traveller: ");
              break;
            case DELETE:
              sb.append("Removed Traveller: ");
              break;
            default:
              sb.append("Traveller: ");
          }

          sb.append(trDetails.tripParticipantName)
            .append(" with e-mail: ")
            .append(trDetails.tripParticipantEmail);

          if (trDetails != null && trDetails.group != null) {
            sb.append(" to Group: ").append(trDetails.group.name);
          }
          break;
        case TRIP_AGENT:
          AuditTripAgent aDetails = (AuditTripAgent) ta.auditRecord.details;

          switch(ta.auditRecord.action){
            case ADD:
              sb.append("Added Agent: ");
              break;
            case DELETE:
              sb.append("Removed Agent: ");
              break;
            default:
              sb.append("Agent: ");
          }

          sb.append(aDetails.tripAgentName)
            .append(" with e-mail: ")
            .append(aDetails.tripAgentEmail);;

          if (aDetails != null && aDetails.group != null) {
            sb.append(" to Group: ").append(aDetails.group.name);
          }


          break;
        case TRIP_GROUP:
          AuditTripGroup gDetails = (AuditTripGroup) ta.auditRecord.details;
          sb.append("Group: ").append(gDetails.name);
          break;
        case TRIP_BOOKING_FLIGHT:
          AuditTripBookingFlight bfDetails = (AuditTripBookingFlight) ta.auditRecord.details;
          sb.append("Flight: ")
            .append(bfDetails.number)
            .append(" on ")
            .append(Utils.formatDateTimePrint(bfDetails.departure.getTime()));
          break;
        case TRIP_BOOKING_HOTEL:
          AuditTripBookingHotel bhDetails = (AuditTripBookingHotel) ta.auditRecord.details;
          sb.append("Hotel: ").append(bhDetails.name).append(" Check-in: ")
            .append(Utils.formatDateTimePrint(bhDetails.checkIn.getTime()))
            .append(" Check-out: ").append(Utils.formatDateTimePrint(bhDetails.checkOut.getTime()));
          break;
        case TRIP_BOOKING_CRUISE:
          AuditTripBookingCruise bcDetails = (AuditTripBookingCruise) ta.auditRecord.details;
          sb.append("Cruise: ").append(bcDetails.name).append(" Depart: ")
            .append(Utils.formatDateTimePrint(bcDetails.depart.getTime()))
            .append(" Arrive: ").append(Utils.formatDateTimePrint(bcDetails.arrive.getTime()));
          break;
        case TRIP_BOOKING_TRANSPORT:
          AuditTripBookingTransport btDetails = (AuditTripBookingTransport) ta.auditRecord.details;
          sb.append("Transportation: ")
            .append(btDetails.name)
            .append(" at ")
            .append(Utils.formatDateTimePrint(btDetails.startTime.getTime()));
          break;
        case TRIP_BOOKING_ACTIVITY:
          AuditTripBookingActivity baDetails = (AuditTripBookingActivity) ta.auditRecord.details;
          sb.append("Activity: ").append(baDetails.name);
          break;
        case TRIP_DOC_ATTACHMENT:
          AuditTripDocAttachment daDetails = (AuditTripDocAttachment) ta.auditRecord.details;
          sb.append("Document: Filename: ").append(daDetails.origFileName);
          break;
        case TRIP_DOC_GUIDE:
          AuditTripDocGuide dgDetails = (AuditTripDocGuide) ta.auditRecord.details;
          sb.append("Guide: ").append(dgDetails.name);
          break;
        case TRIP_DOC_CUSTOM:
          AuditTripDocCustom dcDetails = (AuditTripDocCustom) ta.auditRecord.details;
          sb.append("Custom Document: ").append(dcDetails.name);
          break;
        case TRIP_BOOKING_NOTE:
          try {
            AuditTripDocCustom dcDetails1 = (AuditTripDocCustom) ta.auditRecord.details;
            sb.append("Trip Note: ").append(dcDetails1.name);
          } catch (Exception e) {

          }
          break;
        case TRIP_COLLABORATION:
          AuditCollaboration colDetails = (AuditCollaboration) ta.auditRecord.details;
          sb.append("Collaboration: ");
          switch(colDetails.currentAction) {
            case INVITE:
              sb.append("Sent new user invite to (");
              break;
            case RESEND_INVITE:
              sb.append("Resent new user invite to (");
              break;
            case SHARE:
              sb.append("Sent UMapped user collaboration request to (");
              break;
            case RESEND_SHARE:
              sb.append("Resent UMapped user collaboration request to (");
              break;
            case REVOKE_INVITE:
              sb.append("Removed invite for user (");
              break;
            case REVOKE_SHARE:
              sb.append("Removed share for user (");
              break;
            case CHANGE_INVITE_ACCESS:
              sb.append("Changed access for invite (");
              break;
            case CHANGE_SHARE_ACCESS:
              sb.append("Changed access for user (");
              break;

            default:
              break;
          }

          sb.append(colDetails.invitedFirstName + " " + colDetails.invitedLastName);
          if (colDetails.email != null &&
              (colDetails.currentAction == AuditCollaboration.CollaboraionAction.INVITE ||
               colDetails.currentAction == AuditCollaboration.CollaboraionAction.RESEND_INVITE)) {
            sb.append(" Email:<"+colDetails.email+">");
          }
          sb.append(") Access Level: ");
          switch(colDetails.accessLevel) {
            case READ:
              sb.append("Review");
              break;
            case APPEND:
              sb.append("Contribute");
              break;
            case APPEND_N_PUBLISH:
              sb.append("Contribute & Publish");
              break;
            case NONE:
              sb.append("No access");
              break;
            case OWNER:
            default:
              sb.append("Full Access");
              break;
          }
          break;
        default:
          Log.log(LogLevel.ERROR, "No description for Audit module:" + ta.module);
          break;
      }
    } else {
      if (ta != null && ta.auditRecord != null) {
        Log.log(LogLevel.ERROR, "No audit details to show for trip audit #:" + ta.pk + " AR:" + ta.auditRecord + " ARD:" + ta.auditRecord.details);
      }
    }
    return sb.toString();
  }

  public String getEventReviewUrl(TripAudit ta) {
    StringBuilder sb = new StringBuilder();
    if (ta.auditRecord != null && ta.auditRecord.details != null) {
      switch (ta.module) {
        case TRIP:
          AuditTrip tDetails = (AuditTrip) ta.auditRecord.details;
          sb.append(routes.TripController.tripInfo(tDetails.tripId));
          break;
        case TRIP_TRAVELLER:
        case TRIP_AGENT:
        case TRIP_GROUP:
          sb.append(routes.TourController.reviewTour()).append("?");
          sb.append("tripId=").append(ta.auditRecord.tripId);
          break;
        case TRIP_BOOKING_FLIGHT:
          if (ta.action != AuditActionType.DELETE) {
            AuditTripBookingFlight bfDetails = (AuditTripBookingFlight) ta.auditRecord.details;
            sb.append(routes.BookingController.bookings(ta.auditRecord.tripId,
                                                        new TabType.Bound(TabType.FLIGHTS),
                                                        bfDetails.bookingId));
          } else {
            sb.append(routes.BookingController.bookings(ta.auditRecord.tripId,
                                                        new TabType.Bound(TabType.ITINERARY),
                                                        null));
          }
          break;
        case TRIP_BOOKING_HOTEL:
          if (ta.action != AuditActionType.DELETE) {
            AuditTripBookingHotel bhDetails = (AuditTripBookingHotel) ta.auditRecord.details;
            sb.append(routes.BookingController.bookings(ta.auditRecord.tripId,
                                                        new TabType.Bound(TabType.HOTELS),
                                                        bhDetails.bookingId));
          }  else {
            sb.append(routes.BookingController.bookings(ta.auditRecord.tripId,
                                                        new TabType.Bound(TabType.ITINERARY),
                                                        null));
          }
          break;
        case TRIP_BOOKING_CRUISE:
          if (ta.action != AuditActionType.DELETE) {
            AuditTripBookingCruise bcDetails = (AuditTripBookingCruise) ta.auditRecord.details;
            sb.append(routes.BookingController.bookings(ta.auditRecord.tripId,
                                                        new TabType.Bound(TabType.CRUISES),
                                                        bcDetails.bookingId));
          }  else {
            sb.append(routes.BookingController.bookings(ta.auditRecord.tripId,
                                                        new TabType.Bound(TabType.ITINERARY),
                                                        null));
          }
          break;
        case TRIP_BOOKING_TRANSPORT:
          if (ta.action != AuditActionType.DELETE) {
            AuditTripBookingTransport btDetails = (AuditTripBookingTransport) ta.auditRecord.details;
            sb.append(routes.BookingController.bookings(ta.auditRecord.tripId,
                                                        new TabType.Bound(TabType.TRANSPORTS),
                                                        btDetails.bookingId));
          }  else {
            sb.append(routes.BookingController.bookings(ta.auditRecord.tripId,
                                                        new TabType.Bound(TabType.ITINERARY),
                                                        null));
          }
          break;
        case TRIP_BOOKING_ACTIVITY:
          if (ta.action != AuditActionType.DELETE) {
            AuditTripBookingActivity baDetails = (AuditTripBookingActivity) ta.auditRecord.details;
            sb.append(routes.BookingController.bookings(ta.auditRecord.tripId,
                                                        new TabType.Bound(TabType.ACTIVITIES),
                                                        baDetails.bookingId));
          }  else {
            sb.append(routes.BookingController.bookings(ta.auditRecord.tripId,
                                                        new TabType.Bound(TabType.ITINERARY),
                                                        null));
          }
          break;
        case TRIP_DOC_ATTACHMENT:
        case TRIP_DOC_GUIDE:
          sb.append(routes.TripController.guides(ta.auditRecord.tripId, new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.ATTACHMENTS)));
          break;
        case TRIP_DOC_CUSTOM:
          sb.append(routes.TripController.guides(ta.auditRecord.tripId, new DestinationSearchView.Tab.Bound(DestinationSearchView.Tab.CUSTOM_DOC)));
          break;
        case TRIP_COLLABORATION:
          AuditCollaboration colDetails = (AuditCollaboration) ta.auditRecord.details;
          sb.append(routes.TripController.tripInfo(colDetails.tripId));
          break;
        case TRIP_BOOKING_NOTE:
          try {
            AuditTripDocCustom bnDetails = (AuditTripDocCustom) ta.auditRecord.details;
            if (bnDetails != null && bnDetails.guideId != null && ta.action != AuditActionType.DELETE) {
              sb.append(routes.BookingNoteController.getNote(ta.auditRecord.tripId, Long.parseLong(bnDetails.guideId),
                                                             null, null));
            } else {
              sb.append(routes.BookingController.bookings(ta.auditRecord.tripId,
                                                          new TabType.Bound(TabType.ITINERARY),
                                                          null));
            }
          } catch (Exception e) {
          }
          break;
        default:
          Log.log(LogLevel.ERROR, "No URL for Audit module:" + ta.module);
          break;
      }
    }
    else {
      if (ta != null && ta.auditRecord != null) {
        Log.log(LogLevel.ERROR,
                "No audit details to show for trip audit #:" + ta.pk + " AR:" + ta.auditRecord + " ARD:" + ta.auditRecord.details);
      }
    }
    return sb.toString();
  }
}
