package models.publisher.utils.reservation.migration;

import org.apache.commons.lang.StringUtils;

import com.umapped.persistence.reservation.UmPostalAddress;
import com.umapped.persistence.reservation.accommodation.UmAccommodationReservation;
import com.umapped.persistence.reservation.accommodation.UmLodgingBusiness;

import models.publisher.HotelBooking;
import models.publisher.TmpltDetails;
import models.publisher.TmpltHotel;
import models.publisher.TripDetail;

public class UmAccommodationReservationBuilder {
  public UmAccommodationReservation build(TripDetail trip, HotelBooking hotel) {
    UmAccommodationReservation reservation = new UmAccommodationReservation();
    
    if (hotel != null) {
      reservation.getAccommodation().name = hotel.name;
    }

    parseComments(reservation, trip.getComments());

    reservation.setStartDateTime(trip.getStarttimestamp());
    reservation.setEndDateTime(trip.getEndtimestamp());
    
    return reservation;
  }
  
  public UmAccommodationReservation build(TmpltDetails trip, TmpltHotel hotel) {
    UmAccommodationReservation reservation = new UmAccommodationReservation();
    
    if (hotel != null) {
      reservation.getAccommodation().name = hotel.name;
    }
    parseComments(reservation, trip.getComments());

    reservation.setStartDateTime(trip.getStarttimestamp());
    reservation.setEndDateTime(trip.getEndtimestamp());
    
    return reservation;
  }
  
  /**
   * Short term solution before the hotel information is separated outside of comments
   * @param hotel
   */
  private void clearHotelInformation(UmLodgingBusiness hotel) {
    UmPostalAddress address= hotel.getAddress();
    address.streetAddress = null;
    address.addressLocality = null;
    address.addressRegion = null;
    address.addressCountry = null;
    hotel.url = null;
    hotel.telephone = null;
    hotel.faxNumber = null;
  }
  
  public void parseComments(UmAccommodationReservation reservation, String comments) {
    UmLodgingBusiness hotel = reservation.getAccommodation();
    
    clearHotelInformation(hotel);
    
    if (comments == null || comments.trim().length() == 0) {
      reservation.setNotesPlainText("");
      return;
    }
    
   if (comments.contains("Address:")) {
        String s  = comments.substring(comments.indexOf("Address:"));
        String[] tokens = s.split("\n");
        int i =0 ;
        int j = 0;
        for (String token:tokens) {
          if (token.startsWith("Address:")) {
            i++;
          } else if (i > 0) {
            if (token.trim().length() == 0 && i > 0) {
              break;
            }
            else if (j == 0 && token.trim().length() > 1) {
              hotel.getAddress().streetAddress = token;
              j++;
            }
            else if (j == 1 && token.trim().length() > 1 ) {
              if (!token.toLowerCase().contains("phone")&& !token.toLowerCase().contains("web:")){
                hotel.getAddress().addressRegion = token;
              }
              break;
            }
          }
        }
      }
    

    if (comments.contains("Phone:")) {
      String s = comments.substring(comments.indexOf("Phone:") + 6);
      if (s.contains("\n")) {
        hotel.telephone = StringUtils.trim(s.substring(0, s.indexOf("\n")));
      }
    }
    if (comments.contains("Web:")) {
      String s = comments.substring(comments.indexOf("Web:") + 6);
      if (s.contains("\n")) {
        String url = s.substring(0, s.indexOf("\n"));
        if (!url.startsWith("http") && url.contains("http")) {
          url = url.substring(url.indexOf("http"));
        }
        hotel.url = StringUtils.trim(url);
      }
    }
    if (comments.contains("Fax:")) {
      String s = comments.substring(comments.indexOf("Fax:") + 6);
      if (s.contains("\n")) {
        hotel.faxNumber = StringUtils.trim(s.substring(0, s.indexOf("\n")));
      }
    }

    if (comments != null ) {
      //sanitize comments by removing any duplicate info
      String s  = comments;
      String[] tokens = s.split("\n");
      StringBuilder sb = new StringBuilder();

      UmPostalAddress hotelAddress = hotel.getAddress();
      String streetAddress = hotelAddress == null ? "" : StringUtils.trimToEmpty(hotelAddress.streetAddress);
      String addressLocality = hotelAddress == null ? "" : StringUtils.trimToEmpty(hotelAddress.addressLocality);
      String addressRegion = hotelAddress == null ? "" : StringUtils.trimToEmpty(hotelAddress.addressRegion);
      String addressCountry = hotelAddress == null ? "" : StringUtils.trimToEmpty(hotelAddress.addressCountry);

      String faxNumber = StringUtils.trimToEmpty(hotel.faxNumber);
      String telephone = StringUtils.trimToEmpty(hotel.telephone);
      String url = StringUtils.trimToEmpty(hotel.url);

      for (String s1: tokens) {
        boolean add = true;
        if (s1.startsWith("Fax:") || s1.startsWith("Address:") || s1.startsWith("Web:") || s1.startsWith("Phone:") || s1.startsWith("Fax:")) {
          add = false;
        } else if ( s1.trim().length() > 0 && hotelAddress != null && (
            (streetAddress.length() == s1.trim().length() && s1.contains(streetAddress) )
            || (addressLocality.length() == s1.trim().length() && s1.contains(addressLocality))
            || (addressRegion.length() == s1.trim().length() && s1.contains(addressRegion))
            || (addressCountry.length() == s1.trim().length() && s1.contains(addressCountry))
            || (faxNumber.length() == s1.trim().length() && s1.contains(faxNumber))
            || (telephone.length() == s1.trim().length() && s1.contains(telephone))
            || (url.length() == s1.trim().length() && s1.contains(url)))) {
          add = false;
        }

        if (add) {
          if (s1.trim().length() == 0) {
            if (sb.toString().length() > 0) {
              sb.append(s1);
              sb.append("\n");
            }
          } else {
            sb.append(s1);
            sb.append("\n");
          }
        }
      }

      reservation.setNotesPlainText(sb.toString());
    }
  }
}
