package actors;

import akka.actor.Actor;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.mapped.common.CoreConstants;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.msg.FlightNotifyMsg;
import com.mapped.publisher.msg.FlightTrackMsg;
import com.mapped.publisher.msg.flightstat.track.Airports;
import com.mapped.publisher.msg.flightstat.track.AlertResponse;
import com.mapped.publisher.persistence.PoiRS;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.umapped.persistence.reservation.flight.UmFlightReservation;

import controllers.PoiController;
import models.publisher.*;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;

import static com.umapped.persistence.reservation.utils.UmReservationUtils.cast;

import java.util.List;


/**
 * Created by twong on 2015-01-15.
 * This actor will be called for each individual flight when a trip is published
 * The processing is as follows:
 * - check if this flight is already being tracked
 * - if flight is already being tracked:
 * - add this trip to the tracking list
 * - check and send any notification that might already be there
 * <p/>
 * - if flight is not being tracked:
 * - call flightstats to register an alert for this flight
 * - if success, create the alert rec and add the trip
 * <p/>
 * The following notifications will be subscribed:
 * - flight reminder 24 hours
 * - any delays
 */
public class FlightTrackActor
    extends UntypedActor {

  @Inject
  WSClient ws;

  public interface Factory {
    public Actor create();
  }

  /**
   * Akka Actor Properties
   */
  public final static Props props = Props.create(FlightTrackActor.class);

  public static void trackFlight(TripDetail bookingDetails) {
    try {
      DateTime currentTime = new DateTime(System.currentTimeMillis());
      if (bookingDetails.getStarttimestamp() < currentTime.minusDays(2).getMillis()) {
        Log.info("FlightTrackActor:trackFlight - old flight - cannot track flight for booking id: " + bookingDetails.getDetailsid());

        return;
      }

      boolean tracking = false;
      StringBuilder flightId = new StringBuilder();
      StringBuilder airlineId = new StringBuilder();

      String departAirportCode = null;
      String arriveAirportCode = null;

      if (bookingDetails.getLocStartPoiId() != null) {
        int poiCmpyId = Company.PUBLIC_COMPANY_ID;
        if (bookingDetails.getLocStartPoiCmpyId() != null) {
          poiCmpyId = bookingDetails.getLocStartPoiCmpyId();
        }

        PoiRS poiMain = PoiController.getMergedPoi(bookingDetails.getLocStartPoiId(), poiCmpyId);

        if (poiMain != null && poiMain.getIdLong() != null) {
          departAirportCode = poiMain.getCode();
        }
      }
      else if (bookingDetails.getLocStartName() != null && bookingDetails.getLocStartName().trim().length() == 3) {
        departAirportCode = bookingDetails.getLocStartName();
      }

      if (bookingDetails.getLocFinishPoiId() != null) {
        int poiCmpyId = Company.PUBLIC_COMPANY_ID;
        if (bookingDetails.getLocFinishPoiCmpyId() != null) {
          poiCmpyId = bookingDetails.getLocFinishPoiCmpyId();
        }
        PoiRS poiMain = PoiController.getMergedPoi(bookingDetails.getLocFinishPoiId(), poiCmpyId);
        if (poiMain != null && poiMain.getIdLong() != null) {
          arriveAirportCode = poiMain.getCode();
        }
      }
      else if (bookingDetails.getLocFinishName() != null && bookingDetails.getLocFinishName().trim().length() == 3) {
        arriveAirportCode = bookingDetails.getLocFinishName();
      }

      UmFlightReservation reservation = cast(bookingDetails.getReservation(), UmFlightReservation.class);
      String flightNumber = reservation != null ? reservation.getFlight().flightNumber : "" ;
      
      if (departAirportCode != null && arriveAirportCode != null &&
          !StringUtils.isEmpty(flightNumber)) {
        char[] flightid = flightNumber.trim().replaceAll(" ", "").toCharArray();
        for (int i = 0; i < flightid.length; i++) {
          if (i < 2) {
            airlineId.append(flightid[i]);
          }
          else {
            flightId.append(flightid[i]);
          }
        }

        if (flightId.length() > 0 && airlineId.length() > 0) {
          try {
            DateTime startDate = new DateTime(bookingDetails.getStarttimestamp());
            //send to Actor
            final FlightTrackMsg msg = new FlightTrackMsg();
            msg.airlineCode = airlineId.toString().trim().toUpperCase();
            msg.arriveAirport = arriveAirportCode.trim().toUpperCase();
            msg.departAirport = departAirportCode.trim().toUpperCase();
            msg.departTimestamp = startDate;
            msg.flightId = Integer.parseInt(flightId.toString());
            msg.tripDetailsId = bookingDetails.detailsid;
            msg.tripId = bookingDetails.tripid;
            msg.userId = bookingDetails.getModifiedby();

            ActorsHelper.tell(SupervisorActor.UmappedActor.FLIGHT_TRACK, msg);

            tracking = true;
            Log.debug("FlightTrackActor:trackFlight - tracking flight for booking id: " + bookingDetails.getDetailsid
                ());

          }
          catch (Exception e) {
            Log.info("FlightTrackActor:trackFlight Wrong flight number for booking id: " + bookingDetails.getDetailsid());
          }
        }
      }

      if (!tracking) {
        Log.info("FlightTrackActor:trackFlight - cannot track flight for booking id: " + bookingDetails.getDetailsid());
      }
    } catch (Exception e) {
      Log.err("FlightTrackActor:trackFlight Exception ", e);
    }
  }

  @Override
  public void onReceive(Object message)
      throws Exception {

    //TODO ADD logic to not subscribe in TEST environments unless explicitly overridden
    if (message instanceof FlightTrackMsg) {
      FlightTrackMsg msg = (FlightTrackMsg) message;
      if (msg.isValid()) {
        try {
          //check if this trip detail is already being tracked
          FlightAlert alert = null;
          //there is a possibility that we can have a single flight id departing from 1 airport that can have 2
          // arrival airports
          //so take arrival airport into consideration
          if (msg.arriveAirport == null || msg.arriveAirport.trim().length() != 3) {
            alert = FlightAlert.findAlert(msg.airlineCode, msg.flightId, msg.departAirport, msg.departTimestamp);
          }
          else {
            alert = FlightAlert.findAlert(msg.airlineCode,
                                          msg.flightId,
                                          msg.departAirport,
                                          msg.arriveAirport,
                                          msg.departTimestamp);
          }


          //make this is only 1 - if a booking has been updated to a different flight  make sure we remove them
          List<FlightAlertBooking> existingBookings = FlightAlertBooking.findAllByBookingId(msg.tripDetailsId);

          if (alert != null) {
            //an alert already exists
            //check to see if this trip booking is already being monitored for this alert
            FlightAlertBooking booking = null;
            if (existingBookings != null) {
              for (FlightAlertBooking b : existingBookings) {
                if (b.getFlightAlert().getFlightAlertId() == alert.getFlightAlertId()) {
                  booking = b;
                  break;
                }
              }
            }

            if (booking == null) {
              //no recs yet - start tracking the flight
              Trip t = Trip.find.byId(msg.tripId);
              TripDetail td = TripDetail.findByPK(msg.tripDetailsId);
              if (t != null && td != null) {


                booking = new FlightAlertBooking();
                booking.setAlertBookingId(DBConnectionMgr.getUniqueLongId());
                booking.setCreatedby(msg.userId);
                booking.setCreatedtimestamp(System.currentTimeMillis());
                booking.setFlightAlert(alert);
                booking.setTrip(t);
                booking.setTripDetail(td);
                booking.save();

                //check to see if we missed any notifications
                int countNotifications = FlightAlertEvent.countNotificationByAlert(alert.flightAlertId);
                if (countNotifications > 0) {
                  final FlightNotifyMsg flightNotifyMsg = new FlightNotifyMsg();
                  flightNotifyMsg.flightAlertId = alert.flightAlertId;
                  flightNotifyMsg.alertBookingId = booking.alertBookingId;
                  //send msg to notification actor so missed notification can be sent out
                  //TODO send msg to notification actor
                  //send to Actor
                  ActorsHelper.tell(SupervisorActor.UmappedActor.FLIGHT_NOTIFY, msg);
                }
              }
            }
            else {
              //remove existing bookings from the list
              existingBookings.remove(booking);
            }
          }
          else {
            //this is for the flightstat pull alert
            // no alert yet - register the alert with Flight stats and create all the tracking recs.
            //registerAlert(msg);

            //register a poll alert
            registerPollAlert(msg);
          }

          //delete any only references for this booking
          if (existingBookings != null) {
            for (FlightAlertBooking b : existingBookings) {
              try {
                b.refresh();
              } catch (Exception e) {
                Log.err("FlightTrackActor - Exception Refresh alert for booking bookingId: " + msg.tripDetailsId + " alert:" + b.getAlertBookingId(), e);
              }
              b.delete();
            }
          }
        }
        catch (Exception e) {
          e.printStackTrace();
          Log.err("FlightTrackActor - Exception Cannot Track: BookingId: " + msg.tripDetailsId, e);
        }
      }
    }

  }

  /* register a poll alert - alert will be polled at 30 mins interval */
  private void registerPollAlert(FlightTrackMsg finalMsg)
      throws Exception {
    try {
      Ebean.beginTransaction();
      FlightAlert alert = new FlightAlert();
      alert.setFlightAlertId(DBConnectionMgr.getUniqueLongId());
      alert.setAirlineCode(finalMsg.airlineCode);
      alert.setRuleId(""); //make sure ruleid is empty so we can distinguish between push alerts (has an id) and poll
      // alerts (empty id)
      alert.setFlightId(finalMsg.flightId);
      alert.setArrivalAirport(finalMsg.arriveAirport);
      alert.setDepartAirport(finalMsg.departAirport);

      if (finalMsg.departTimestamp != null) {
        alert.setOrigDepartTime(finalMsg.departTimestamp.getMillis());
      }

      if (finalMsg.arriveTimestamp != null) {
        alert.setOrigArriveTime(finalMsg.arriveTimestamp.getMillis());
      }

      alert.setCreatedby(finalMsg.userId);
      alert.setCreatedtimestamp(System.currentTimeMillis());
      alert.setLastupdatedtimestamp(System.currentTimeMillis());
      alert.setStatus(FlightAlert.FlightAlertStatus.PENDING.ordinal());
      alert.setResponse("");
      alert.save();

      //no recs yet - start tracking the flight
      Trip t = Trip.find.byId(finalMsg.tripId);
      TripDetail td = TripDetail.findByPK(finalMsg.tripDetailsId);

      FlightAlertBooking booking = new FlightAlertBooking();
      booking.setAlertBookingId(DBConnectionMgr.getUniqueLongId());
      booking.setCreatedby(finalMsg.userId);
      booking.setCreatedtimestamp(System.currentTimeMillis());
      booking.setFlightAlert(alert);
      booking.setTrip(t);
      booking.setTripDetail(td);
      booking.save();
      Log.info("FlightTrackActor - Flight Tracked: " + finalMsg.airlineCode + " " + finalMsg.flightId + " from " +
               finalMsg.departAirport + " on " + Utils.formatDateTimePrint(finalMsg.departTimestamp.getMillis()));
      Ebean.commitTransaction();
    }
    catch (Exception e) {
      Ebean.rollbackTransaction();
      throw e;
    }
  }

  /* register a push alert using the flighstats alert service */
  public void registerAlert(FlightTrackMsg msg)
      throws Exception {
    final FlightTrackMsg finalMsg = msg;
    //flight alert iurl https://api.flightstats
    // .com/flex/alerts/rest/v1/json/create/<FLIGHT_CODE>/<FLIGHT_ID>/from/<AIRPORT_CODE>/departing/<YYYY>/<MM>/<DD
    // >?appId=aeae0c80&appKey=8106152200b485839efba94368e7fb36&type=JSON&deliverTo=<CALLBACK URL>"
    StringBuilder urlQuery = new StringBuilder("https://api.flightstats.com/flex/alerts/rest/v1/json/create/");
    urlQuery.append(msg.airlineCode);
    urlQuery.append("/");
    urlQuery.append(msg.flightId);
    urlQuery.append("/from/");
    urlQuery.append(msg.departAirport);
    urlQuery.append("/departing/");
    urlQuery.append(msg.departTimestamp.getYear());
    urlQuery.append("/");
    urlQuery.append(msg.departTimestamp.getMonthOfYear());
    urlQuery.append("/");
    urlQuery.append(msg.departTimestamp.getDayOfMonth());

    String url = urlQuery.toString();// URLEncoder.encode( urlQuery.toString(),"ISO-8859-1");


    //make the GET request


    ws.url(url)
      .setQueryParameter("appId", "aeae0c80")
      .setQueryParameter("appKey", "8106152200b485839efba94368e7fb36")
      .setQueryParameter("type", "JSON")
      .setQueryParameter("events", "preDep360, depDelay,depDelayDelta15,depDelayWindow360,can, depGate60")
      .setQueryParameter("deliverTo", ConfigMgr.getAppParameter(CoreConstants.HOST_URL) + "/flightstats/alert")
      .get()
      .thenApplyAsync((WSResponse response) -> {
        JsonNode json = response.asJson();
        try {
          processResponse(finalMsg, json);
          return json;
        }
        catch (Exception e) {
          Log.err("FlightTrackActor - Error parsing response BookingId: " + finalMsg.tripDetailsId, e);
          Log.err("FlightTrackActor - Error parsing response" + json.toString());
        }
        return null;
      });



       /* test code
        //test code
      String s =  "{\"request\":{\"airport\":{\"requestedCode\":\"LHR\",\"fsCode\":\"LHR\"},
      \"url\":\"https://api.flightstats.com/flex/alerts/rest/v1/json/create/AA/100/to/LHR/arriving/2013/2/27\",
      \"airlineCode\":{\"requestedCode\":\"AA\",\"fsCode\":\"AA\"},\"flightNumber\":{\"requested\":\"100\",
      \"interpreted\":\"100\"},\"date\":{\"year\":\"2013\",\"month\":\"2\",\"day\":\"27\",
      \"interpreted\":\"2013-02-27\"},\"name\":{\"requested\":\"Example alert name\",
      \"interpreted\":\"Example alert name\"},\"description\":{\"requested\":\"Example alert description\",
      \"interpreted\":\"Example alert description\"},\"type\":{\"requested\":\"JSON\",\"interpreted\":\"JSON\"},
      \"deliverTo\":{\"requested\":\"https://example.com/your_post_url\",\"interpreted\":\"https://example
      .com/your_post_url\"},\"events\":[{\"interpreted\":\"all\"}],\"nameValues\":[],\"codeType\":{},
      \"extendedOptions\":{\"requested\":\"includeNewFields\",\"interpreted\":\"includeNewFields\"}},
      \"rule\":{\"id\":\"125524541\",\"name\":\"Example alert name\",\"description\":\"Example alert description\",
      \"carrierFsCode\":\"AA\",\"flightNumber\":\"100\",\"departureAirportFsCode\":\"JFK\",
      \"arrivalAirportFsCode\":\"LHR\",\"departure\":\"2013-02-26T18:15:00.000\",\"arrival\":\"2013-02-27T06:20:00
      .000\",\"ruleEvents\":[{\"type\":\"ALL_CHANGES\"}],\"nameValues\":[],\"delivery\":{\"format\":\"json\",
      \"destination\":\"https://example.com/your_post_url\"}},\"appendix\":{\"airlines\":[{\"fs\":\"AA\",
      \"iata\":\"AA\",\"icao\":\"AAL\",\"name\":\"American Airlines\",\"phoneNumber\":\"1-800-433-7300\",
      \"active\":true}],\"airports\":[{\"fs\":\"JFK\",\"iata\":\"JFK\",\"icao\":\"KJFK\",\"faa\":\"JFK\",
      \"name\":\"John F. Kennedy International Airport\",\"street1\":\"JFK Airport\",\"city\":\"New York\",
      \"cityCode\":\"NYC\",\"stateCode\":\"NY\",\"postalCode\":\"11430\",\"countryCode\":\"US\",
      \"countryName\":\"United States\",\"regionName\":\"North America\",\"timeZoneRegionName\":\"America/New_York\",
      \"weatherZone\":\"NYZ178\",\"localTime\":\"2013-02-26T19:41:43.187\",\"utcOffsetHours\":-5.0,
      \"latitude\":40.642335,\"longitude\":-73.78817,\"elevationFeet\":13,\"classification\":1,\"active\":true},
      {\"fs\":\"LHR\",\"iata\":\"LHR\",\"icao\":\"EGLL\",\"name\":\"London Heathrow Airport\",\"city\":\"London\",
      \"cityCode\":\"LON\",\"stateCode\":\"EN\",\"countryCode\":\"GB\",\"countryName\":\"United Kingdom\",
      \"regionName\":\"Europe\",\"timeZoneRegionName\":\"Europe/London\",\"localTime\":\"2013-02-27T00:41:42.834\",
      \"utcOffsetHours\":0.0,\"latitude\":51.469603,\"longitude\":-0.453566,\"elevationFeet\":80,
      \"classification\":1,\"active\":true}]},\"alertCapabilities\":{\"baggage\":true,\"departureGateChange\":true,
      \"arrivalGateChange\":true,\"gateDeparture\":true,\"gateArrival\":true,\"runwayDeparture\":true,
      \"runwayArrival\":true}}" +
                  "";

      try {
        ObjectMapper obj = new ObjectMapper();
        JsonNode json =  obj.readTree(s); //response.asJson();

        processResponse(finalMsg, json);

      } catch (Exception e) {
        Log.err( "FlightTrackActor - Error parsing response BookingId: " + finalMsg.tripDetailsId);

      }
        */

  }

  public static void processResponse(FlightTrackMsg finalMsg, JsonNode json)
      throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    com.fasterxml.jackson.databind.ObjectReader r = mapper.reader(AlertResponse.class);
    AlertResponse jsonResp = r.readValue(json);

    if (jsonResp != null && jsonResp.getError() != null) {
      Log.err("FlightTrackActor - Cannot Track: Error BookingId: " +
              finalMsg.tripDetailsId + "\n" +
              jsonResp.getError().toString());

    }

    if (jsonResp != null && jsonResp.getRule() != null && jsonResp.getRule().getId() != null) {
      //save the alert
      FlightAlert alert = new FlightAlert();
      alert.setFlightAlertId(DBConnectionMgr.getUniqueLongId());
      alert.setAirlineCode(finalMsg.airlineCode);
      alert.setRuleId(jsonResp.getRule().getId());

      if (jsonResp.getRule().getFlightNumber() != null && StringUtils.isNumeric(jsonResp.getRule().getFlightNumber())) {
        alert.setFlightId(Integer.parseInt(jsonResp.getRule().getFlightNumber()));
      }
      else {
        alert.setFlightId(finalMsg.flightId);
      }

      if (jsonResp.getRule().getArrivalAirportFsCode() != null) {
        alert.setArrivalAirport(jsonResp.getRule().getArrivalAirportFsCode());
      }
      else {
        alert.setArrivalAirport(finalMsg.arriveAirport);
      }

      if (jsonResp.getRule().getDepartureAirportFsCode() != null) {
        alert.setDepartAirport(jsonResp.getRule().getDepartureAirportFsCode());
      }
      else {
        alert.setDepartAirport(finalMsg.departAirport);
      }

      DateTime departDate = Utils.getDateTimeFromISO(jsonResp.getRule().getDeparture());
      if (departDate != null) {
        alert.setOrigDepartTime(departDate.getMillis());
      }
      else {
        alert.setOrigDepartTime(finalMsg.departTimestamp.getMillis());
      }

      DateTime arriveDate = Utils.getDateTimeFromISO(jsonResp.getRule().getArrival());
      if (arriveDate != null) {
        alert.setOrigArriveTime(arriveDate.getMillis());
      }
      else if (finalMsg.arriveTimestamp != null) {
        alert.setOrigArriveTime(finalMsg.arriveTimestamp.getMillis());
      }


      alert.setCreatedby(finalMsg.userId);
      alert.setCreatedtimestamp(System.currentTimeMillis());
      alert.setLastupdatedtimestamp(System.currentTimeMillis());
      alert.setStatus(APPConstants.STATUS_ACTIVE);
      alert.setResponse(json.toString());

      //optional stuff
      if (jsonResp.getAppendix() != null) {
        if (jsonResp.getAppendix().getAirlines() != null && jsonResp.getAppendix().getAirlines().size() > 0) {
          alert.setAirlineName(jsonResp.getAppendix().getAirlines().get(0).getName());
        }
        if (jsonResp.getAppendix().getAirports() != null && jsonResp.getAppendix().getAirports().size() > 0) {
          for (Airports airport : jsonResp.getAppendix().getAirports()) {
            if (airport.getFs().equalsIgnoreCase(alert.getDepartAirport())) {
              alert.setDepartAirportName(airport.getName());
              alert.setDepartTimeZone(airport.getTimeZoneRegionName());

            }
            else if (alert.getArrivalAirport() != null && airport.getFs().equalsIgnoreCase(alert.getArrivalAirport())) {
              alert.setArrivalAirportName(airport.getName());
              alert.setArriveTimeZone(airport.getTimeZoneRegionName());
            }
          }
        }
      }

      alert.save();

      //no recs yet - start tracking the flight
      Trip t = Trip.find.byId(finalMsg.tripId);
      TripDetail td = TripDetail.findByPK(finalMsg.tripDetailsId);

      FlightAlertBooking booking = new FlightAlertBooking();
      booking.setAlertBookingId(DBConnectionMgr.getUniqueLongId());
      booking.setCreatedby(finalMsg.userId);
      booking.setCreatedtimestamp(System.currentTimeMillis());
      booking.setFlightAlert(alert);
      booking.setTrip(t);
      booking.setTripDetail(td);
      booking.save();
      Log.info("FlightTrackActor - Flight Tracked: " + finalMsg.airlineCode + " " + finalMsg.flightId + " from " +
               finalMsg.departAirport + " on " + Utils.formatDateTimePrint(finalMsg.departTimestamp.getMillis()));
    }
    else {
      Log.err("FlightTrackActor - Cannot Track: BookingId: " + finalMsg.tripDetailsId + " JSON: " + json.toString());

    }
  }
}
