package com.umapped.itinerary.analyze.location;

import com.google.inject.Inject;
import com.mapped.publisher.persistence.redis.RedisKeys;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.mapped.publisher.utils.Log;
import com.umapped.itinerary.analyze.db.model.publisher.PoiLookupService;
import com.umapped.itinerary.analyze.location.google.GoogleLocationLookupSevice;
import com.umapped.itinerary.analyze.model.Location;

import javax.inject.Singleton;
import java.time.ZoneId;
import java.util.Optional;

/**
 * Created by wei on 2017-03-29.
 */
@Singleton
public class LocationEnhanceServiceImpl implements LocationEnhanceService {

  @Inject
  RedisMgr redis;

  private TimeZoneLookupService timeZoneLookupService;
  private LocationLookupService poiLookupService;
  private GoogleLocationLookupSevice googleLocationLookupSevice;

  @Inject
  public LocationEnhanceServiceImpl(TimeZoneLookupService timeZoneLookupService) {
    this.timeZoneLookupService = timeZoneLookupService;
    poiLookupService = new PoiLookupService();
    googleLocationLookupSevice = new GoogleLocationLookupSevice();
  }

  @Override public Location enhanceLocation(Location location, boolean important) {
    if (location == null) {
      return null;
    }
    Location enhanced = lookupFromCache(location);
    if (enhanced == null) {
      if (location.getGeoLocation() == null) {
        enhanced = lookupGeoLocation(location, important);
      } else {
        enhanced = location;
      }

      if (enhanced.getTimezone() == null) {
        ZoneId zoneId = lookupTimeZone(enhanced.getGeoLocation());
        enhanced.setTimezone(zoneId);
      }
      // if it is enhanced, put it into cache
      if (enhanced.getGeoLocation() != null) {
        redis.set(RedisKeys.K_POI_LOCATION, getLocationKey(location), enhanced, RedisMgr.WriteMode.FAST);
      }
    }
    return enhanced;
  }

  private Location lookupGeoLocation(Location location, boolean important) {
    Location enhanced = null;
    if (location.getUmPoidId() != null) {
      enhanced = poiLookupService.lookup(location);
    }
    // try google
    if ((enhanced == null || enhanced.getGeoLocation() == null) && important) {
      enhanced = googleLocationLookupSevice.lookup(location);
      if (enhanced != null) {
        enhanced.setUmPoidId(location.getUmPoidId());
      }
    }

    if (enhanced == null) {
      enhanced = location;
    }
    // still not found
    return enhanced;
  }

  private ZoneId lookupTimeZone(Location.GeoLocation geoLocation) {
    if (geoLocation == null) {
      return null;
    }
    return timeZoneLookupService.lookup(geoLocation);
  }

  private Location lookupFromCache(Location location) {
    Optional<Location> enhanced = redis.getBin(RedisKeys.K_POI_LOCATION, getLocationKey(location), Location.class);
    if (enhanced.isPresent()) {
      return enhanced.get();
    } else {
      return null;
    }
  }

  private String getLocationKey(Location location) {
    if (location.getUmPoidId() != null) {
      return location.getUmPoidId();
    } else {
      return location.getDescription();
    }
  }
}
