#!/bin/bash

# Helper script to generate Java Keystore and OpenSSL X509 server SSL certificates
# Note configure parameters down in the script if you want some custom claims
#
#Resources I found useful
# - https://devcenter.heroku.com/articles/ssl-certificate-self
# - http://stackoverflow.com/questions/4294689/how-to-generate-a-key-with-passphrase-from-the-command-line
# - http://www.shellhacks.com/en/HowTo-Create-CSR-using-OpenSSL-Without-Prompt-Non-Interactive
# - http://stackoverflow.com/a/8224863/194624


# Password to use to encrypt keys and keystores
PASSWORD=123456
# Destination Folder
DEST=cert
# Entry name in the keystore
STORE_NAME=umapped
# How long key is valid for
SHELF_LIFE_DAYS=365

CLAIM_COUNTRY="CA"
CLAIM_STATE="Ontario"
CLAIM_LOCATION="Vaughan"
CLAIM_ORG="Umapped Inc"
CLAIM_ORG_DEPT="H4x0rZ"
CLAIM_NAME="umdev"
CLAIM_EMAIL="serguei@umapped.com"

# subject=/C=CA/ST=Ontario/L=Vaughan/O=Umapped Inc/OU=H4x0rZ/CN=192.168.1.100/emailAddress=serguei@umapped.com
SUBJECT="/C=$CLAIM_COUNTRY/ST=$CLAIM_STATE/L=$CLAIM_LOCATION/O=$CLAIM_ORG/OU=$CLAIM_ORG_DEPT/CN=$CLAIM_NAME/emailAddress=$CLAIM_EMAIL"

#Info
echo "Generating keystore for Subject:"
echo $SUBJECT

echo "Step 1: Deleting and Re-Creating destination folder..."
rm -rf $DEST
mkdir -p $DEST

echo "Step 2: Generating encryption key..."
openssl genrsa -des3 -passout pass:$PASSWORD -out $DEST/server.pass.key 2048

echo "Step 3: Generating Server Key..."
openssl rsa -passin pass:$PASSWORD -in $DEST/server.pass.key -out $DEST/server.key

echo "Step 4: Generating request..."
openssl req -new -key $DEST/server.key -out $DEST/server.csr -subj "$SUBJECT"

echo "Step 5: Generating X509 SSL Certificate..."
openssl x509 -req -days $SHELF_LIFE_DAYS -in $DEST/server.csr -signkey $DEST/server.key -out $DEST/server.crt

###############################################################################
# Generating Java JKS keystore                                                #
###############################################################################

echo "Step 6:  Convert x509 Cert and Key to a pkcs12 file..."
openssl pkcs12 -export -passout pass:$PASSWORD -in $DEST/server.crt -inkey $DEST/server.key -out $DEST/server.p12 -name $STORE_NAME -CAfile ca.crt -caname root

echo "Step 7: Convert the pkcs12 file to a Java JSK keystore..."
keytool -importkeystore -deststorepass $PASSWORD -destkeypass $PASSWORD \
        -destkeystore $DEST/server.keystore -srckeystore $DEST/server.p12 \
        -srcstoretype PKCS12 -srcstorepass $PASSWORD -alias $STORE_NAME
