package models.publisher;

import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-08-26
 * Time: 11:08 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity @Table(name = "Tmplt_Guide")
public class TmpltGuide
    extends Model {

  public static Model.Finder<String, TmpltGuide> find = new Finder(TmpltGuide.class);
  @Id
  public String pk;
  @Constraints.Required
  public String templateid;
  public String destinationid;
  public int status;
  public String createdby;
  public Long createdtimestamp;
  @Version
  public int version;

  public static TmpltGuide buildGuide(String templateid, String userId) {
    TmpltGuide tmpltGuide = new TmpltGuide();
    tmpltGuide.setPk(DBConnectionMgr.getUniqueId());
    tmpltGuide.setTemplateid(templateid);
    tmpltGuide.setCreatedtimestamp(System.currentTimeMillis());
    tmpltGuide.setCreatedby(userId);
    tmpltGuide.version = 0;
    tmpltGuide.status = APPConstants.STATUS_ACTIVE;
    return tmpltGuide;
  }

  public static List<TmpltGuide> findByTemplate(String templateid) {
    return find.where().eq("templateid", templateid).eq("status", 0).findList();
  }

  public static List<TmpltGuide> findByTemplateDestination(String templateid, String destId) {
    return find.where().eq("templateid", templateid).eq("destinationId", destId).eq("status", 0).findList();
  }


  public String getPk() {
    return pk;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public String getTemplateid() {
    return templateid;
  }

  public void setTemplateid(String templateid) {
    this.templateid = templateid;
  }

  public String getDestinationid() {
    return destinationid;
  }

  public void setDestinationid(String destinationid) {
    this.destinationid = destinationid;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
