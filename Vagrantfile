# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"
Vagrant.require_version ">= 1.6.5"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  #Configuring Parallels environment that uses boot2docker image
  config.vm.define "boot2docker", primary: false do | boot2docker |
    boot2docker.vm.box = "parallels/boot2docker"
    boot2docker.vm.network "private_network", type: "dhcp"
    #boot2docker.vm.synced_folder ".", "/vagrant", type: "nfs"
    # Fix busybox/udhcpc issue
    boot2docker.vm.provision :shell do |s|
      s.inline = <<-EOT
      if ! grep -qs ^nameserver /etc/resolv.conf; then
        sudo /sbin/udhcpc
      fi
      cat /etc/resolv.conf
      EOT
    end
    # Adjust datetime after suspend and resume
    boot2docker.vm.provision :shell do |s|
      s.inline = <<-EOT
        sudo /usr/local/bin/ntpclient -s -h pool.ntp.org
        date
      EOT
    end
    boot2docker.vm.provision :docker do |d|
      d.pull_images "yungsang/busybox"
      d.run "simple-echo",
      image: "yungsang/busybox",
      args: "-p 8080:8080",
      cmd: "nc -p 8080 -l -l -e echo hello world!"
    end
  end

  config.ssh.insert_key = false

  config.vm.define "trusty_parallels", primary: true do | trusty_parallels |
    trusty_parallels.vm.box  = "parallels/ubuntu-14.04"
    trusty_parallels.vm.network "private_network", type: "dhcp"
    trusty_parallels.vm.synced_folder ".", "/vagrant"
    trusty_parallels.vm.network :forwarded_port, guest: 9000, host: 7000
    trusty_parallels.vm.network :forwarded_port, guest: 9999, host: 7999
    trusty_parallels.vm.network :forwarded_port, guest: 5432, host: 5432
    trusty_parallels.vm.network :forwarded_port, guest: 5433, host: 5433
    trusty_parallels.vm.network :forwarded_port, guest: 6379, host: 6379    #Redis
    trusty_parallels.vm.network :forwarded_port, guest: 11211, host: 11211  #Memcached
    trusty_parallels.vm.provision "shell", path: "vconf/trusty_provision.sh"
  end 

  config.vm.define "trusty_docker", primary: false do | trusty_docker |
    trusty_docker.vm.box  = "box-cutter/ubuntu1404"
    trusty_docker.vm.network "private_network", type: "dhcp"
    trusty_docker.vm.synced_folder ".", "/vagrant", type: "nfs"
    #trusty_docker.vm.network :forwarded_port, guest: 9000, host: 9000
    #trusty_docker.vm.network :forwarded_port, guest: 9999, host: 9999
    trusty_docker.vm.network :forwarded_port, guest: 5432, host: 5432
    trusty_docker.vm.network :forwarded_port, guest: 5433, host: 5433
    trusty_docker.vm.network :forwarded_port, guest: 6379, host: 6379   #Redis
    trusty_docker.vm.network :forwarded_port, guest: 11211, host: 11211 #Memcached
    trusty_docker.vm.provision "shell", path: "vconf/trusty_provision.sh"
  end 

  config.vm.provider :virtualbox do |v, override|
      v.gui = false
      v.customize ["modifyvm", :id, "--memory", 2048]
      v.customize ["modifyvm", :id, "--cpus", 2]
      v.customize ["modifyvm", :id, "--vram", "128"]
      v.customize ["setextradata", "global", "GUI/MaxGuestResolution", "any"]
      v.customize ["setextradata", :id, "CustomVideoMode1", "1024x768x32"]
      v.customize ["modifyvm", :id, "--ioapic", "on"]
      v.customize ["modifyvm", :id, "--rtcuseutc", "on"]
      v.customize ["modifyvm", :id, "--accelerate3d", "off"]
      v.customize ["modifyvm", :id, "--clipboard", "bidirectional"]
      v.customize ["modifyvm", :id, "--nictype1", "virtio"]
  end

  config.vm.provider "parallels" do |v|
    #v.name = "boot2docker"
    v.update_guest_tools = true
    v.customize ["set", :id, "--longer-battery-life", "off"]
    v.memory = 4096
    v.cpus = 4
  end
end

