package com.umapped.service.offer;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;


/**
 * Created by wei on 2017-06-13.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ItineraryOffer implements Serializable {
  private String tripId;
  private List<LocationOffer> locationOffers;

  public String getTripId() {
    return tripId;
  }

  public ItineraryOffer setTripId(String tripId) {
    this.tripId = tripId;
    return this;
  }

  public List<LocationOffer> getLocationOffers() {
    return locationOffers;
  }

  public ItineraryOffer setLocationOffers(List<LocationOffer> locationOffers) {
    this.locationOffers = locationOffers;
    return this;
  }

  public ItineraryOffer(String tripId, List<LocationOffer> locationOffers) {

    this.tripId = tripId;
    this.locationOffers = locationOffers;
  }
}
