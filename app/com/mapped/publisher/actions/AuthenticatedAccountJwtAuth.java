package com.mapped.publisher.actions;

import com.mapped.publisher.js.BaseJsonResponse;
import com.mapped.publisher.utils.Log;
import com.umapped.api.schema.types.ReturnCode;
import io.jsonwebtoken.Claims;
import org.apache.commons.lang3.time.StopWatch;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by twong on 17/10/16.
 */
public class AuthenticatedAccountJwtAuth extends AccountJwtAuth {
    @Override
    public CompletionStage<Result> call(Http.Context ctx) {
        StopWatch sw = new StopWatch();
        sw.start();
        try {
            Claims claims = parseFromContext(ctx);
            ctx.args.put("uid", claims.get("uid", Long.class));
            if (!claims.containsKey("sec")) {
                return CompletableFuture.completedFuture(BaseJsonResponse.codeResponse(ReturnCode.AUTH_ACCOUNT_FAIL));
            }
        }
        catch (Exception e) {
            Log.err("JWT Authenticated Validation Failure: " + e.getMessage()); //Only message printed, no stack plz
            return CompletableFuture.completedFuture(BaseJsonResponse.codeResponse(ReturnCode.AUTH_ACCOUNT_FAIL));
        }
        sw.stop();
        Log.debug("JWT Authenticated Authorization took: " + sw.getTime() + "ms");
        return delegate.call(ctx);
    }
}
