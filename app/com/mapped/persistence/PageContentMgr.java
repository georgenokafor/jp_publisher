package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class PageContentMgr {

	 private static final String SELECT_ALL = "Select Pk, PageId, ContentId, ContentType, Rank, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from PageContent;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, PageId, ContentId, ContentType, Rank, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from PageContent where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from PageContent where Pk = ?;";
	 private static final String INSERT = "insert into PageContent (Pk, PageId, ContentId, ContentType, Rank, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update PageContent SET PageId=?, ContentId=?, ContentType=?, Rank=?, ModifiedBy=?, Status=?, SyncTimestamp=?, LastUpdateTimestamp=? WHERE Pk = ?; ";
	 private static final String SELECT_ALL_BY_PAGE_ID = "Select Pk, PageId, ContentId, ContentType, Rank, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from PageContent where PageId=? ";
	 private static final String DELETE_BY_PAGE_ID = "delete from PageContent where PageId=? ";
	 public static ArrayList <PageContent> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return PageContentMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static PageContent getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<PageContent> results = PageContentMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (PageContent c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getPageId());
				 prep.setString(3, c.getContentId());
				 prep.setString(4, c.getContentType());
				 prep.setString(5, c.getRank());
				 prep.setString(6, c.getModifiedBy());
				 prep.setString(7, c.getCreatedBy());
				 prep.setLong(8, c.getStatus());
				 prep.setLong(9, c.getSyncTimestamp());
				 prep.setLong(10, c.getLastUpdateTimestamp());
				 prep.setLong(11, c.getCreateTimestamp());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (PageContent c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getPageId());
				 prep.setString(2, c.getContentId());
				 prep.setString(3, c.getContentType());
				 prep.setString(4, c.getRank());
				 prep.setString(5, c.getModifiedBy());
				 prep.setLong(6, c.getStatus());
				 prep.setLong(7, c.getSyncTimestamp());
				 prep.setLong(8, c.getLastUpdateTimestamp());
				 prep.setString(9, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <PageContent> getAllByPageId(String id) throws SQLException {
		 ResultSet rs = null;
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PAGE_ID);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 return PageContentMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void deleteByPageId (String id) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PAGE_ID);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <PageContent> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <PageContent> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 PageContent c = new PageContent();
			 c.setPk(rs.getString("Pk"));
			 c.setPageId(rs.getString("PageId"));
			 c.setContentId(rs.getString("ContentId"));
			 c.setContentType(rs.getString("ContentType"));
			 c.setRank(rs.getString("Rank"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setStatus(rs.getLong("Status"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 results.add(c);
		 }
		 return results;
	 }
}
