
package com.mapped.publisher.parse.ensemble;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="day" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="destination" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="arrival_time" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="departure_time" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "value"
})
@XmlRootElement(name = "itinerary")
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
public class Itinerary {

    @XmlValue
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String value;
    @XmlAttribute(name = "day")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String day;
    @XmlAttribute(name = "destination")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String destination;
    @XmlAttribute(name = "arrival_time")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String arrivalTime;
    @XmlAttribute(name = "departure_time")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    protected String departureTime;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the day property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getDay() {
        return day;
    }

    /**
     * Sets the value of the day property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setDay(String value) {
        this.day = value;
    }

    /**
     * Gets the value of the destination property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Gets the value of the arrivalTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Sets the value of the arrivalTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setArrivalTime(String value) {
        this.arrivalTime = value;
    }

    /**
     * Gets the value of the departureTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public String getDepartureTime() {
        return departureTime;
    }

    /**
     * Sets the value of the departureTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2015-10-08T11:49:34-04:00", comments = "JAXB RI v2.2.4-2")
    public void setDepartureTime(String value) {
        this.departureTime = value;
    }

}
