package com.mapped.publisher.view.util;

import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * Created by george on 2015-12-07.
 */
public class PiqueTripPdf extends Pique{

  public String seats = null;
  public String eticket = null;
  public String fClass = null;
  public EnhancedTripBookingDetailView details;

  public static boolean hasBooking(List<TripBookingDetailView> tripBookingDetailViewList) {
    for (TripBookingDetailView tbdv : tripBookingDetailViewList) {
      if (tbdv.noteId == null || tbdv.noteId == 0) {
        return true;
      }
    }

    return false;

  }

  public static Pique parseFlight(TripBookingDetailView view) {
    EnhancedTripBookingDetailView v = EnhancedTripBookingDetailView.parseFlight(view);
    Pique p = new Pique();
    if (v != null && v.passengers != null) {
      p.details = v;
      for (EnhancedTripBookingDetailView.PassengerInfo pi : v.passengers) {
        if (pi.seat != null && pi.seat.trim().length() > 0) {
          if (p.seats == null) {
            p.seats = pi.seat;
          }
          else {
            p.seats = p.seats + "/" + pi.seat;
          }
        }

        if (pi.eticket != null && pi.eticket.trim().length() > 0) {
          if (p.eticket == null) {
            p.eticket = pi.name + " " + pi.eticket;
          }
          else {
            p.eticket = p.eticket + " | " + pi.name + " " + pi.eticket;
          }
        }

        if (pi.fClass != null && pi.fClass.trim().length() > 0) {
          if (p.fClass == null) {
            p.fClass = pi.fClass;
          }
          else if (!p.fClass.contains(pi.fClass)) {
            p.fClass = p.fClass + " | " + pi.fClass;
          }
        }
      }
    }


    return p;

  }


  public static void updateChronologicalBookings(TripBookingView m) {


    //insert new card dropoff
    if (m.chronologicalBookings != null && m.chronologicalDates != null && m.transfers != null) {
      ArrayList<TripBookingDetailView> car = new ArrayList<TripBookingDetailView>();

      for (TripBookingDetailView v : m.transfers) {
        if (v.detailsTypeId == ReservationType.CAR_RENTAL && v.startDate != null && v.endDate != null && !v.startDate
            .equals(v.endDate)) {
          UmTransferReservationView n = new UmTransferReservationView();
          n.name = v.name;
          n.startDate = v.endDate;
          n.startDateMs = v.endDateMs;
          n.startDatePrint = v.endDatePrint;
          n.startDay = v.startDay;
          n.startHour = v.startHour;
          n.startMin = v.startMin;
          n.startMonth = v.startMonth;
          n.startTime = v.startTime;
          n.startYear = v.startYear;
          n.bookingNumber = v.bookingNumber;
          n.locStartName = v.locStartName;
          n.locFinishName = v.locFinishName;
          n.contact = v.contact;
          n.endDate = v.endDate;
          n.endDateMs = v.endDateMs;
          n.endDatePrint = v.endDatePrint;
          n.endDay = v.endDay;
          n.endHour = v.endHour;
          n.endMin = v.endHour;
          n.endMonth = v.endMonth;
          n.endTime = v.endTime;
          n.endYear = v.endYear;
          n.detailsTypeId = v.detailsTypeId;
          n.detailsId = v.detailsId;
          n.providerName = v.providerName;
          car.add(n);
        }
      }

      boolean sortNeeded = false;

      for (TripBookingDetailView v : car) {
        String date = Utils.formatDatePrint(v.endDateMs);
        if (m.chronologicalDates.contains(date)) {
          m.chronologicalBookings.get(date).add(0,v);
          //resort the chronological bookings
          //Collections.sort(m.chronologicalBookings.get(date), PiqueComparator);

        }
        else {
          //insert new date
          m.chronologicalDates.add(date);
          ArrayList<TripBookingDetailView> newRentals = new ArrayList<TripBookingDetailView>();
          newRentals.add(v);
          m.chronologicalBookings.put(date, newRentals);
          sortNeeded = true;
        }
      }

      if (sortNeeded) {
        Collections.sort(m.chronologicalDates, new Comparator<String>() {
          @Override public int compare(String o1, String o2) {
            long sDate = getMillis(o1);
            long eDate = getMillis(o2);
            if (sDate > eDate) {
              return 1;
            }
            else if (sDate < eDate) {
              return -1;
            }
            else {
              return 0;
            }
          }
        });
      }
    }

    if (m.chronologicalBookings != null && m.chronologicalDates != null && m.hotels != null) {
      ArrayList<TripBookingDetailView> hotels = new ArrayList<TripBookingDetailView>();

      for (TripBookingDetailView v : m.hotels) {
        if (v.detailsTypeId == ReservationType.HOTEL && v.startDate != null && v.endDate != null && !v.startDate.equals(
            v.endDate)) {
          UmAccommodationReservationView n = new UmAccommodationReservationView();
          n.name = v.name;
          n.startDate = v.endDate;
          n.startDateMs = v.endDateMs;
          n.startDatePrint = v.endDatePrint;
          n.endDate = v.endDate;
          n.endDateMs = v.endDateMs;
          n.endDatePrint = v.endDatePrint;
          n.detailsTypeId = v.detailsTypeId;
          n.detailsId = v.detailsId;
          n.providerName = v.providerName;
          hotels.add(n);
        }
        else if (v.detailsTypeId == ReservationType.HOTEL && v.startDate != null && v.endDate != null && v.startDate
            .equals(v.endDate)) {
          //same day checkin - we need to insert another record but this time set the start time to the chekckin time so it sorts properly

          UmAccommodationReservationView n = new UmAccommodationReservationView();
          n.name = v.name;
          n.startDate = v.endDate;
          n.startDateMs = v.endDateMs;
          n.startDatePrint = v.endDatePrint;
          n.endDate = v.endDate;
          n.endDateMs = v.endDateMs;
          n.endDatePrint = v.endDatePrint;
          n.detailsTypeId = v.detailsTypeId;
          n.detailsId = v.detailsId;
          n.providerName = v.providerName;
          hotels.add(n);
        }
      }

      boolean sortNeeded = false;

      for (TripBookingDetailView v : hotels) {
        String date = Utils.formatDatePrint(v.endDateMs);
        if (m.chronologicalDates.contains(date)) {
          m.chronologicalBookings.get(date).add(0,v);
          //resort the chronological bookings
          //Collections.sort(m.chronologicalBookings.get(date), PiqueComparator);

        }
        else {
          //insert new date
          m.chronologicalDates.add(date);
          ArrayList<TripBookingDetailView> newRentals = new ArrayList<TripBookingDetailView>();
          newRentals.add(v);
          m.chronologicalBookings.put(date, newRentals);
          sortNeeded = true;
        }
      }

      if (sortNeeded) {
        Collections.sort(m.chronologicalDates, new Comparator<String>() {
          @Override public int compare(String o1, String o2) {
            long sDate = getMillis(o1);
            long eDate = getMillis(o2);
            if (sDate > eDate) {
              return 1;
            }
            else if (sDate < eDate) {
              return -1;
            }
            else {
              return 0;
            }
          }
        });
      }
    }
  }


  public static String getDocCoverForTrip(TripPreviewView v) {
    if (v.tripTag != null && v.tripTag.trim().length() > 0 && StringUtils.isAlphanumeric(v.tripTag.replaceAll(" ", ""))) {
      StringBuilder sb = new StringBuilder("https://s3.amazonaws.com/static-umapped-com/public/external/pique/img/cover/");
      sb.append(v.tripTag.toLowerCase().replaceAll(" ", ""));
      sb.append(".png");
      return sb.toString();
    }

    return v.tripCoverUrl;
  }

  public static boolean hasTripCover(TripPreviewView v) {
    if (v.tripTag != null && v.tripTag.trim().length() > 0 && StringUtils.isAlphanumeric(v.tripTag.replaceAll(" ", ""))) {
      return true;
    }

    return false;
  }

  public static AttachmentView getNotePhoto(List<TripBookingDetailView> viewList) {
    for (TripBookingDetailView v : viewList) {
      if (v.noteId != null && v.noteId > 0){
        if (v.photos != null && v.photos.size() > 0) {
          return v.photos.get(0);
        }
      }
    }
    return null;
  }


  public static List<String> removeNotesOnly (List<String> chronologicalDates,  HashMap<String, List<TripBookingDetailView>> chronologicalBookings) {
    List<String> newDates = new ArrayList<>();
    if (chronologicalDates != null && chronologicalBookings != null) {
      for (String day : chronologicalDates) {
        if (day != null && day.trim().length() > 0 && hasBooking(chronologicalBookings.get(day))) {
          newDates.add(day);
        }
      }
    }


    return newDates;
  }


}
