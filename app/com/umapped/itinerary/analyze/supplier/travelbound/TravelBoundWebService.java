package com.umapped.itinerary.analyze.supplier.travelbound;

import com.google.inject.Guice;
import com.google.inject.Injector;
import modules.ItineraryAnalyzeModule;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.recommendation.RecommendedItem;

import com.umapped.itinerary.analyze.supplier.Supplier;
import com.umapped.itinerary.analyze.supplier.travelbound.jaxb.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.*;

/**
 * Created by wei on 2017-04-17.
 */
@Singleton public class TravelBoundWebService {
  private static final Logger LOG = LoggerFactory.getLogger(TravelBoundWebService.class);

  private TravelBoundWebServiceRequestHandler requestHandler = new TravelBoundWebServiceRequestHandler();
  private TravelBoundAPIRequestBuilder requestBuilder = new TravelBoundAPIRequestBuilder();

  public List<RecommendedItem> getRecommendedHotels(StayPeriod period, Location location) {
    try {
      if (location.getGeoLocation() != null && period.getStart() != null && period.getEnd() != null) {
        TRequest request = requestBuilder.createSearchHotelRequest(period, location.getGeoLocation());

        JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        StringWriter writer = new StringWriter();
        marshaller.marshal(request, writer);
        LOG.debug("Search hotel: " + writer.toString());
        List<RecommendedItem> items = requestHandler.post("", writer.toString(), TravelBoundWebService::getHotels);

        if (!items.isEmpty()) {
          List<String> ids = new ArrayList<>();
          for (RecommendedItem item : items) {
            ids.add(item.id);
          }

          TRequest hotelRequest = requestBuilder.getHotelsInformation(ids);
          writer = new StringWriter();
          marshaller.marshal(hotelRequest, writer);
          LOG.debug(writer.toString());
          Map<String, RecommendedItem> hotels = requestHandler.post("",
                                                                    writer.toString(),
                                                                    TravelBoundWebService::getHotelInformation);

          for (RecommendedItem item : items) {
            RecommendedItem info = hotels.get(item.id);
            if (info != null) {
              item.address = info.address;
              item.description = info.description;
            }
          }
          return items;
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      LOG.error("Fail to invoke travel bound service", e);
    }
    return Collections.emptyList();
  }


  public static List<RecommendedItem> getHotels(InputStream inputStream) {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);

      Unmarshaller umarshaller = jaxbContext.createUnmarshaller();

      TResponse response = (TResponse) umarshaller.unmarshal(inputStream);
      TSearchHotelPriceResponse priceResponse = (TSearchHotelPriceResponse) response.getResponseDetails()
                                                                                    .getSearchHotelPricePaxResponseOrApartmentPriceBreakdownResponseOrAvailabilityCacheResponse()
                                                                                    .get(0)
                                                                                    .getValue();

      List<RecommendedItem> items = new ArrayList<>();
      if (priceResponse.getHotelDetails() != null) {
        List<THotel> hotels = priceResponse.getHotelDetails().getHotel();
        for (THotel h : hotels) {
          items.add(getHotelInfo(h));
        }
      }
      return items;
    }
    catch (Exception e) {
      LOG.error("Fail to parse response from travel bound service", e);
    }
    return Collections.emptyList();

  }

  public static Map<String, RecommendedItem> getHotelInformation(InputStream inputStream) {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);

      Unmarshaller umarshaller = jaxbContext.createUnmarshaller();

      Map<String, RecommendedItem> results = new HashMap<>();
      TResponse response = (TResponse) umarshaller.unmarshal(inputStream);
      List<JAXBElement<?>> hotelResponses = response.getResponseDetails()
                                                    .getSearchHotelPricePaxResponseOrApartmentPriceBreakdownResponseOrAvailabilityCacheResponse();
      for (JAXBElement<?> hotel : hotelResponses) {
        RecommendedItem ri = new RecommendedItem();
        TSearchItemInformationResponse itemResponse = (TSearchItemInformationResponse) hotel.getValue();
        TItemDetail itemDetail = itemResponse.getItemDetails().getItemDetail().get(0);
        ri.id = itemDetail.getCity().getCode() + "-" + itemDetail.getItem().getCode();

        ri.address = new RecommendedItem.Address();
        TAddressLines addressLines = itemDetail.getHotelInformation().getAddressLines();

        ri.address.streetAddress = addressLines.getAddressLine1();
        ri.address.locality = addressLines.getAddressLine2();
        ri.address.region = addressLines.getAddressLine3();
        ri.address.country = addressLines.getAddressLine4();
        ri.description = itemDetail.getHotelInformation().getCategory() + ":" + itemDetail.getHotelInformation()
                                                                                          .getStarRating()
                                                                                          .getValue() + " Stars";
        results.put(ri.id, ri);
      }
      return results;
    }
    catch (Exception e) {
      LOG.error("Fail to parse response from travel bound service", e);
    }

    return Collections.emptyMap();
  }

  static private RecommendedItem getHotelInfo(THotel hotel) {
    String cityCode = hotel.getCity().getCode();
    String itemCode = hotel.getItem().getCode();
    String name = hotel.getItem().getValue();

    List<TRoomCategory> rcs = hotel.getRoomCategories().getRoomCategory();

    RecommendedItem item = new RecommendedItem();
    item.supplier = Supplier.Travelbound;
    item.id = cityCode + "-" + itemCode;
    item.name = name;
    List<RecommendedItem.Price> prices = new ArrayList<>();
    item.prices = prices;
    for (TRoomCategory r : rcs) {
      String description = r.getDescription();
      switch (r.getMeals().getBasis().getCode()) {
        case "N":
          break;
        case "B":
          description = description + "(" + r.getMeals().getBreakfast().getValue() + " breakfast included)";
          break;
        case "L":
          description = description + "(" + r.getMeals().getLunch().getValue() + " lunch included)";
          break;
        case "D":
          description = description + "(" + r.getMeals().getDinner().getValue() + " dinner included)";
          break;
      }


      RecommendedItem.Price price = new RecommendedItem.Price(String.format("%.2f", r.getItemPrice().getValue()),
                                                              "USD",
                                                              description);
      item.prices.add(price);
    }
    return item;
  }

  public static void main(String[] args) {
    Injector container = Guice.createInjector(new ItineraryAnalyzeModule());

    TravelBoundWebService service = container.getInstance(TravelBoundWebService.class);

    LocalDate start = LocalDate.now().plusDays(10);
    LocalDate end = start.plusDays(10);

    StayPeriod p = new StayPeriod(start, end);

    Location location = new Location();
    location.setGeoLocation(new Location.GeoLocation(51.4775, -0.461389));

    List<RecommendedItem> hotels = service.getRecommendedHotels(p, location);
    System.out.println(hotels);

  }
}
