package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class MapsProviderMgr {

	 private static final String SELECT_ALL = "Select Pk, MapsId, OrgId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from MapsProvider;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, MapsId, OrgId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from MapsProvider where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from MapsProvider where Pk = ?;";
	 private static final String INSERT = "insert into MapsProvider (Pk, MapsId, OrgId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp) values (?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update MapsProvider SET MapsId=?, OrgId=?, ModifiedBy=?, Status=?, SyncTimestamp=?, LastUpdateTimestamp=? WHERE Pk = ?; ";
	 private static final String SELECT_ALL_BY_MAPS_ID = "Select Pk, MapsId, OrgId, ModifiedBy, CreatedBy, Status, SyncTimestamp, LastUpdateTimestamp, CreateTimestamp from MapsProvider where MapsId=? ";
	 private static final String DELETE_BY_MAPS_ID = "delete from MapsProvider where MapsId=? ";
	 public static ArrayList <MapsProvider> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return MapsProviderMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static MapsProvider getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<MapsProvider> results = MapsProviderMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (MapsProvider c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getMapsId());
				 prep.setString(3, c.getOrgId());
				 prep.setString(4, c.getModifiedBy());
				 prep.setString(5, c.getCreatedBy());
				 prep.setLong(6, c.getStatus());
				 prep.setLong(7, c.getSyncTimestamp());
				 prep.setLong(8, c.getLastUpdateTimestamp());
				 prep.setLong(9, c.getCreateTimestamp());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (MapsProvider c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getMapsId());
				 prep.setString(2, c.getOrgId());
				 prep.setString(3, c.getModifiedBy());
				 prep.setLong(4, c.getStatus());
				 prep.setLong(5, c.getSyncTimestamp());
				 prep.setLong(6, c.getLastUpdateTimestamp());
				 prep.setString(7, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <MapsProvider> getAllByMapsId(String id) throws SQLException {
		 ResultSet rs = null;
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_MAPS_ID);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 return MapsProviderMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void deleteByMapsId (String id) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_MAPS_ID);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <MapsProvider> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <MapsProvider> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 MapsProvider c = new MapsProvider();
			 c.setPk(rs.getString("Pk"));
			 c.setMapsId(rs.getString("MapsId"));
			 c.setOrgId(rs.getString("OrgId"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setStatus(rs.getLong("Status"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 results.add(c);
		 }
		 return results;
	 }
}
