package com.umapped.persistence.reservation.cruise;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.umapped.persistence.reservation.UmDateTime;
import com.umapped.persistence.reservation.UmPlace;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmCruiseStop
    implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  public String pier;
  public UmPlace port;
  public UmDateTime arrivalTime;
  public UmDateTime boardingTime;
  public UmDateTime departTime;
}

