package com.mapped.publisher.utils;

public enum LogLevel {
    ERROR, INFO, DEBUG 
}
