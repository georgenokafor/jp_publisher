package com.umapped.external.wetu.itinerary.details;

/**
 * Created by george on 2017-06-05.
 */

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "collection",
    "drop_off",
    "collection_content_entityId",
    "drop_off_content_entity_id",
    "collection_address",
    "drop_off_address",
    "agency",
    "vehicle",
    "vehicle_class",
    "reference_codes",
    "collection_date",
    "collection_time",
    "drop_off_date",
    "drop_off_time",
    "included",
    "contact_numbers"
})
public class CarHire {

  @JsonProperty("collection")
  private String collection;
  @JsonProperty("drop_off")
  private String dropOff;
  @JsonProperty("collection_content_entityId")
  private int collectionContentEntityId;
  @JsonProperty("drop_off_content_entity_id")
  private int dropOffContentEntityId;
  @JsonProperty("collection_address")
  private String collectionAddress;
  @JsonProperty("drop_off_address")
  private String dropOffAddress;
  @JsonProperty("agency")
  private String agency;
  @JsonProperty("vehicle")
  private String vehicle;
  @JsonProperty("vehicle_class")
  private String vehicleClass;
  @JsonProperty("reference_codes")
  private String referenceCodes;
  @JsonProperty("collection_date")
  private String collectionDate;
  @JsonProperty("collection_time")
  private String collectionTime;
  @JsonProperty("drop_off_date")
  private String dropOffDate;
  @JsonProperty("drop_off_time")
  private String dropOffTime;
  @JsonProperty("included")
  private String included;
  @JsonProperty("contact_numbers")
  private String contactNumbers;


  @JsonProperty("collection")
  public String getCollection() {
    return collection;
  }

  @JsonProperty("collection")
  public void setCollection(String collection) {
    this.collection = collection;
  }

  @JsonProperty("drop_off")
  public String getDropOff() {
    return dropOff;
  }

  @JsonProperty("drop_off")
  public void setDropOff(String dropOff) {
    this.dropOff = dropOff;
  }

  @JsonProperty("collection_content_entityId")
  public int getCollectionContentEntityId() {
    return collectionContentEntityId;
  }

  @JsonProperty("collection_content_entityId")
  public void setCollectionContentEntityId(int collectionContentEntityId) {
    this.collectionContentEntityId = collectionContentEntityId;
  }

  @JsonProperty("drop_off_content_entity_id")
  public int getDropOffContentEntityId() {
    return dropOffContentEntityId;
  }

  @JsonProperty("drop_off_content_entity_id")
  public void setDropOffContentEntityId(int dropOffContentEntityId) {
    this.dropOffContentEntityId = dropOffContentEntityId;
  }

  @JsonProperty("collection_address")
  public String getCollectionAddress() {
    return collectionAddress;
  }

  @JsonProperty("collection_address")
  public void setCollectionAddress(String collectionAddress) {
    this.collectionAddress = collectionAddress;
  }

  @JsonProperty("drop_off_address")
  public String getDropOffAddress() {
    return dropOffAddress;
  }

  @JsonProperty("drop_off_address")
  public void setDropOffAddress(String dropOffAddress) {
    this.dropOffAddress = dropOffAddress;
  }

  @JsonProperty("agency")
  public String getAgency() {
    return agency;
  }

  @JsonProperty("agency")
  public void setAgency(String agency) {
    this.agency = agency;
  }

  @JsonProperty("vehicle")
  public String getVehicle() {
    return vehicle;
  }

  @JsonProperty("vehicle")
  public void setVehicle(String vehicle) {
    this.vehicle = vehicle;
  }

  @JsonProperty("vehicle_class")
  public String getVehicleClass() {
    return vehicleClass;
  }

  @JsonProperty("vehicle_class")
  public void setVehicleClass(String vehicleClass) {
    this.vehicleClass = vehicleClass;
  }

  @JsonProperty("reference_codes")
  public String getReferenceCodes() {
    return referenceCodes;
  }

  @JsonProperty("reference_codes")
  public void setReferenceCodes(String referenceCodes) {
    this.referenceCodes = referenceCodes;
  }

  @JsonProperty("collection_date")
  public String getCollectionDate() {
    return collectionDate;
  }

  @JsonProperty("collection_date")
  public void setCollectionDate(String collectionDate) {
    this.collectionDate = collectionDate;
  }

  @JsonProperty("collection_time")
  public String getCollectionTime() {
    return collectionTime;
  }

  @JsonProperty("collection_time")
  public void setCollectionTime(String collectionTime) {
    this.collectionTime = collectionTime;
  }

  @JsonProperty("drop_off_date")
  public String getDropOffDate() {
    return dropOffDate;
  }

  @JsonProperty("drop_off_date")
  public void setDropOffDate(String dropOffDate) {
    this.dropOffDate = dropOffDate;
  }

  @JsonProperty("drop_off_time")
  public String getDropOffTime() {
    return dropOffTime;
  }

  @JsonProperty("drop_off_time")
  public void setDropOffTime(String dropOffTime) {
    this.dropOffTime = dropOffTime;
  }

  @JsonProperty("included")
  public String getIncluded() {
    return included;
  }

  @JsonProperty("included")
  public void setIncluded(String included) {
    this.included = included;
  }

  @JsonProperty("contact_numbers")
  public String getContactNumbers() {
    return contactNumbers;
  }

  @JsonProperty("contact_numbers")
  public void setContactNumbers(String contactNumbers) {
    this.contactNumbers = contactNumbers;
  }
}
