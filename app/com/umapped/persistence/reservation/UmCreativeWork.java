package com.umapped.persistence.reservation;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmCreativeWork
    extends UmThing {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Place  The location of the content.
   */
  public UmPlace contentLocation;

  /**
   * Media type (aka MIME format, see IANA site) of the content e.g. application/zip of a SoftwareApplication binary.
   * In cases where a CreativeWork has several media type representations, 'encoding' can be used to indicate each
   * MediaObject alongside particular fileFormat information.
   * Text
   */
  public String fileFormat;

  /**
   * Keywords or tags used to describe this content. Multiple entries in a keywords list are typically delimited by
   * commas.
   * Text
   */
  public String keywords;

  /**
   * The position of an item in a series or sequence of items.
   *  Integer or Text
   */
  public Integer position;

}
