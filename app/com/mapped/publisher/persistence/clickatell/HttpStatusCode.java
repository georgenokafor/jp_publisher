package com.mapped.publisher.persistence.clickatell;

/**
 * Status codes that Clickatell might reply with when connection is attempted
 * https://www.clickatell.com/developers/api-docs/http-status-codes-rest/
 * Created by surge on 2016-02-10.
 */
public enum HttpStatusCode {
  OK(200,
     false,
     "OK",
     "The request was successfully completed."),
  ACCEPTED(202, false,
           "Accepted",
           "The message(s) will be processed."),
  MULTI_STATUS(207,
               false,
               "Multi-status",
               "More than  one message was submitted to the API; however, not all messages have " + "the same status."),
  BAD_REQUEST(400,
              true,
              "Bad request",
              "Validation failure (such as missing/invalid parameters or headers)"),
  UNAUTHORIZED(401,
               true,
               "Unauthorized",
               "Authentication failure. This can also be caused by IP lockdown settings"),
  PAYMENT_REQUIRED(402,
                   true,
                   "Payment required",
                   "You do not have enough credits to send messages"),
  NOT_FOUND(404,
            true,
            "Not found",
            "The action was performed on a resource that does not exist"),
  NOT_ALLOWED(405,
              true,
              "Method not allowed",
              "HTTP method is not supported on the resource"),
  GONE(410,
       true,
       "Gone",
       "The mobile number is blocked, delisted or cannot be routed"),
  TOO_MANY_REQUESTS(429,
                    true,
                    "Too many requests",
                    "Generic rate limiting error"),
  SERVICE_UNAVAILABLE(503,
                      true,
                      "Service unavailable",
                      "A temporary error has occurred on our platform - please " + "retry"),
  UNDEFINED_ERROR(0, true, "Undefined Error", "");


  int     code;
  String  desc;
  String  detail;
  boolean error;

  HttpStatusCode(int code, boolean error, String desc, String detail) {
    this.code = code;
    this.desc = desc;
    this.detail = detail;
    this.error = error;
  }

  public boolean isError() {
    return error;
  }

  public String getDetail() {
    return detail;
  }

  public String getDesc() {
    return desc;
  }

  public int getCode() {
    return code;
  }

  public static HttpStatusCode fromCode(int code) {
    for(HttpStatusCode sc : HttpStatusCode.values()) {
      if(sc.code == code) {
        return sc;
      }
    }
    return UNDEFINED_ERROR;
  }
}
