package parsers;

import com.mapped.publisher.parse.extractor.booking.email.ReservationsHolder;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by surge on 2016-07-14.
 */
public class SchemaDotOrgTest
    extends Assert {

  @Test
  public void clientbaseInjest_sample1() {
    String data = getResourceFile("json/schemaorg/cb_Sample_40410.json");
    assertNotNull(data);
    ReservationsHolder rh = new ReservationsHolder();
    boolean            rc = rh.parseJson(data);
    assertTrue(rc);
    assertEquals(17, rh.count());
  }

  @Test
  public void clientbaseInjest_sample2() {
    String data = getResourceFile("json/schemaorg/CB-1-28407.json");
    assertNotNull(data);
    ReservationsHolder rh = new ReservationsHolder();
    boolean            rc = rh.parseJson(data);
    assertTrue(rc);
    assertEquals(1, rh.count());
    assertEquals(1, rh.cruises.size());

    /*
    Trip trip = Trip.buildTrip("", "", "");
    VOModeller voModeller =  new VOModeller(trip, (Account) null, false);
    voModeller.setGenerateNotes(false); //Never generate comments in API mode
    TripVO tvo = new TripVO();
    rh.toTripVO(tvo);
    voModeller.buildTripVOModels(tvo);

    assertEquals(1, voModeller.getCruises().size());
    */
  }

  @Test
  public void skiInjest() {
    String data = getResourceFile("json/schemaorg/testgus1.json");
    assertNotNull(data);
    ReservationsHolder rh = new ReservationsHolder();
    boolean            rc = rh.parseJson(data);
    assertTrue(rc);
    assertEquals(23, rh.count());
  }


  @Test
  public void apiInjest() {
    String data = getResourceFile("json/schemaorg/reservations.json");
    assertNotNull(data);
    ReservationsHolder rh = new ReservationsHolder();
    boolean            rc = rh.parseJson(data);
    assertTrue(rc);
    assertEquals(38, rh.count());
  }

  private String getResourceFile(String fileName) {
    String result = null;
    ClassLoader classLoader = getClass().getClassLoader();
    try {
      result = IOUtils.toString(classLoader.getResourceAsStream(fileName));
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    return result;
  }
}
