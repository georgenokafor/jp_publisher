package com.umapped.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.name.Named;
import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.segment.TripSegment;
import com.umapped.itinerary.analyze.segment.TripSegmentAnalyzeResult;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesCityLookup;
import com.umapped.service.offer.LocationOffer;
import com.umapped.service.offer.OfferService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.ApplicationLoader;
import play.Environment;
import play.inject.guice.GuiceApplicationBuilder;
import play.inject.guice.GuiceApplicationLoader;
import play.test.Helpers;

import java.util.List;

/**
 * Created by wei on 2017-06-09.
 */
public class OfferServiceTest {
  @Inject Application application;

  @Inject
  private OfferService service;

  @Inject
  private WCitiesCityLookup cityLookup;

  @Inject
  @Named("ItineraryAnalayzeObjectMapper")
  private ObjectMapper om;

  @Before
  public void setup() {
    Module testModule = new AbstractModule() {
      @Override
      public void configure() {
        // Install custom test binding here
      }
    };

    GuiceApplicationBuilder builder = new GuiceApplicationLoader()
        .builder(new ApplicationLoader.Context(Environment.simple()))
        .overrides(testModule);
    Guice.createInjector(builder.applicationModule()).injectMembers(this);

    Helpers.start(application);
  }

  @After
  public void teardown() {
    Helpers.stop(application);
  }

  @Test
  public void testAnalyze() throws Exception {
    ItineraryAnalyzeResult result = service.analyzeTrip("1348336475290000004"); //"1338395454040002491");//"1320929702090001363");
    for (TripSegmentAnalyzeResult r : result.getSegmentResults()) {
      TripSegment segment = r.getSegment();
      System.out.println(toString(segment));
      List<DecisionResult> decisions = r.getDecisionResults();
      System.out.println("\t" + toString(decisions));
    }

    System.out.println("===================================");

    List<LocationOffer> offers = service.getLocationOffers(result);
    for (LocationOffer offer : offers) {
      System.out.println(offer);
    }
  }


  private String toString(TripSegment segment) {
    StringBuilder builder = new StringBuilder();
    builder.append("Start: (");
    builder.append(segment.getStartDateTime());
    builder.append(",");
    builder.append(toString(segment.getStartLocation()));
    builder.append(");End: (");
    builder.append(segment.getEndDateTime());
    builder.append(",");
    builder.append(toString(segment.getEndLocation()));
    builder.append("); Category: ");
    builder.append(segment.getCategory());
    builder.append("; Natures: ");
    builder.append(segment.getNatures());
    builder.append("; Duration: ");
    builder.append(segment.getDurationHours());
    builder.append("; accomodationSize: ");
    builder.append(segment.getAccommodationCount());

    return builder.toString();
  }

  private String toString(Location location) {
    if (location == null) {
      return "";
    }
    if (location.getGeoLocation() != null) {
      Pair<String, String> city = cityLookup.getCity(location.getGeoLocation());
      if (city != null)
       return "UMappedCity: " + cityLookup.getCity(location.getGeoLocation()).getRight();
    }
    if (StringUtils.isNotEmpty(location.getDescription())) {
      return location.getDescription();
    } else if (location.getAddress() != null) {
      Location.Address address = location.getAddress();
      return "" + address.getLocality() + ", " + address.getCountry();
    }
    return "";
  }

  private String toString(List<DecisionResult> results) {
    StringBuilder builder = new StringBuilder();
    for (DecisionResult r : results) {
      builder.append(r.getType());
      builder.append(",");
    }
    return builder.toString();
  }

  private String toString(DecisionResult dr) {
    StringBuilder builder = new StringBuilder();
    builder.append("Type: ");
    builder.append(dr.getType());
    builder.append("; Date: ");
    builder.append(dr.getStartDate());
    builder.append(" - ");
    builder.append(dr.getEndDate());
    builder.append("; Locations: [");
    for (Location loc : dr.getLocations()) {
      builder.append(toString(loc));
      builder.append(";");
    }
    builder.append("]");
    return builder.toString();
  }
}
