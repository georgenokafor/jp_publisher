package models.publisher;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbJsonB;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.mapped.publisher.utils.Utils;
import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;
import com.umapped.itinerary.analyze.model.json.ObjectMapperProvider;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Map;

/**
 * Created by wei on 2017-03-30.
 */
@Entity
@Table(name = "trip_analysis")
public class TripAnalysis extends Model {
    @Transient
    public static final ObjectMapperProvider om;
    static {
        om = new ObjectMapperProvider();
        //Perform any kind of configuration for object mapper in this area
    }

    @DbJsonB
    @Column(name = "reservation_package")
    private JsonNode reservationPackage;

    @DbJsonB
    @Column(name = "result")
    private JsonNode itineraryAnalyzeResult;

    @DbJsonB
    @Column(name = "performance")
    private JsonNode performance;

    @Id
    @Column(name = "analyze_id")
    private Long analyzeId;

    @Column(name = "trip_id")
    private String tripId;

    @Constraints.Required
    @Column(name = "created_ts")
    private Timestamp createdTs;


    public static Model.Finder<Long, TripAnalysis> find                 = new Finder(TripAnalysis.class);

    public long getAnalyzeId() {
        return analyzeId;
    }

    public TripAnalysis setAnalyzeId(long analyzeId) {
        this.analyzeId = analyzeId;
        return this;
    }

    public String getTripId() {
        return tripId;
    }

    public TripAnalysis setTripId(String tripId) {
        this.tripId = tripId;
        return this;
    }

    public JsonNode getReservationPackage() {
        return reservationPackage;
    }

    public TripAnalysis setReservationPackage(JsonNode reservationPackage) {
        this.reservationPackage = reservationPackage;
        return this;
    }

    public JsonNode getItineraryAnalyzeResult() {
        return itineraryAnalyzeResult;
    }

    public TripAnalysis setItineraryAnalyzeResult(JsonNode itineraryAnalyzeResult) {
        this.itineraryAnalyzeResult = itineraryAnalyzeResult;
        return this;
    }

    public JsonNode getPerformance() {
        return performance;
    }

    public TripAnalysis setPerformance(JsonNode performance) {
        this.performance = performance;
        return this;
    }

    public Timestamp getCreated_ts() {
        return createdTs;
    }

    public TripAnalysis setCreated_ts(Timestamp created_ts) {
        this.createdTs = created_ts;
        return this;
    }

    public static TripAnalysis build (ReservationPackage reservationPackage, ItineraryAnalyzeResult result, Map<String, Long> performance, Instant startTime) {
        TripAnalysis tripAnalysis = new TripAnalysis();

        tripAnalysis.setAnalyzeId(DBConnectionMgr.getUniqueLongId())
                .setTripId(result.getItineraryId())
                .setReservationPackage(om.get().valueToTree(reservationPackage))
                .setItineraryAnalyzeResult(om.get().valueToTree(result))
                .setPerformance(om.get().valueToTree(performance))
                .setCreated_ts(Timestamp.from(startTime));
        return tripAnalysis;
    }

    public static TripAnalysis findLastGoodAnalysis (String tripId) {
        //let's look at the last 5 analysis - if there is nothing good, then return null
        List<TripAnalysis> trips =  find.where().eq("trip_id", tripId).orderBy("created_ts desc").setMaxRows(5).findList();
        if (trips != null) {
            for (TripAnalysis tripAnalysis: trips) {
                if (tripAnalysis.getItineraryAnalyzeResult() != null) {
                    return tripAnalysis;
                }
            }
        }
        return null;
    }
}
