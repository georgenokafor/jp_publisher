package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by surge on 2016-07-05.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class VideoObject
    extends MediaObject {
  /**
   * An actor, e.g. in tv, radio, movie, video games etc., or in an event. Actors can be associated with individual
   * items or with a series, episode, clip. Supersedes actors.
   */
  public List<Person> actor;
  /**
   * The caption for this object.
   */
  public String       caption;
  /**
   * A director of e.g. tv, radio, movie, video gaming etc. content, or of an event. Directors
   * can be associated with individual items or with a series, episode, clip. Supersedes directors.
   */
  public List<Person> director;
  /**
   * The composer of the soundtrack.
   */
  public List<Person> musicBy;
  /**
   * Thumbnail image for an image or video.
   */
  public ImageObject  thumbnail;
  /**
   * If this MediaObject is an AudioObject or VideoObject, the transcript of that object.
   */
  public String       transcript;
  /**
   * The frame size of the video.
   */
  public String       videoFrameSize;
  /**
   * The quality of the video.
   */
  public String       videoQuality;

}
