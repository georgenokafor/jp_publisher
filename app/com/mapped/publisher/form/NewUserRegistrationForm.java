package com.mapped.publisher.form;

import com.mapped.publisher.utils.Utils;

/**
 * Created by twong on 2014-05-20.
 */
public class NewUserRegistrationForm {
  private String inCmpyName;
  private String inFirstName;
  private String inLastName;
  private String inEmail;
  private String inConfirmEmail;
  private String inAuthCode;
  private String inPhoneNumber;


  public String getInCmpyName() {
    return inCmpyName;
  }

  public void setInCmpyName(String inCmpyName) {
    this.inCmpyName = inCmpyName;
  }

  public String getInFirstName() {
    return inFirstName;
  }

  public void setInFirstName(String inFirstName) {
    this.inFirstName = inFirstName;
  }

  public String getInLastName() {
    return inLastName;
  }

  public void setInLastName(String inLastName) {
    this.inLastName = inLastName;
  }

  public String getInEmail() {
    return inEmail;
  }

  public void setInEmail(String inEmail) {
    this.inEmail = inEmail;
  }

  public String getInConfirmEmail() {
    return inConfirmEmail;
  }

  public void setInConfirmEmail(String inConfirmEmail) {
    this.inConfirmEmail = inConfirmEmail;
  }

  public String getInAuthCode() {
    return inAuthCode;
  }

  public void setInAuthCode(String inAuthCode) {
    this.inAuthCode = inAuthCode;
  }

  public String getInPhoneNumber() {
    return inPhoneNumber;
  }

  public void setInPhoneNumber(String inPhoneNumber) {
    this.inPhoneNumber = inPhoneNumber;
  }

  public boolean hasErrors () {
    if(inAuthCode == null || inAuthCode.trim().length() == 0) {
      return true;
    } else if(inCmpyName == null || inCmpyName.trim().length() == 0) {
      return true;
    } else if(inFirstName == null || inFirstName.trim().length() == 0) {
      return true;
    } else if(inLastName == null || inLastName.trim().length() == 0) {
      return true;
    } else if(inEmail == null || inEmail.trim().length() == 0) {
      return true;
    } else if (!Utils.isValidEmailAddress(inEmail)) {
      return true;
    } else if (inConfirmEmail == null || inConfirmEmail.trim().length() == 0) {
      return true;
    } else if (!inEmail.equalsIgnoreCase(inConfirmEmail)) {
      return true;

    }
    return false;
  }
}
