package models.publisher;


import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.common.APPConstants;
import play.data.validation.Constraints;
import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-02-14
 * Time: 11:19 AM
 */
@Entity @Table(name = "user_cmpy_link")
public class UserCmpyLink
    extends Model {

  public static Model.Finder<String, UserCmpyLink> find = new Finder(UserCmpyLink.class);

  @Id
  public String pk;
  public String comment;
  public int isowner;

  @Constraints.Required @Enumerated(EnumType.ORDINAL)
  private LinkType linktype;
  public int status;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @OneToOne @JoinColumn(name = "cmpyid")
  public Company cmpy;
  @OneToOne @JoinColumn(name = "userid")
  public UserProfile user;
  @Version
  public int version;

  public enum LinkType {
    SUSPENDED(0),
    //No real meaning for this value yet, may be in the future, placeholder for now
    ADMIN(1),
    MEMBER(2),
    POWER(3);
    private int dbVal;

    private LinkType(int dbVal) {
      this.dbVal = dbVal;
    }

    public int getIdx() {
      return dbVal;
    }

    public static LinkType fromInt(int val) {
      for (LinkType lt : LinkType.values()) {
        if (lt.dbVal == val) {
          return lt;
        }
      }
      return SUSPENDED;
    }

    public static  LinkType fromAccountLinkType(AccountCmpyLink.LinkType type) {
      switch (type){
        case ADMIN:
          return ADMIN;
        case MEMBER:
          return MEMBER;
        case POWER:
          return POWER;
        default:
        case SUSPENDED:
          return SUSPENDED;
      }
    }
  }

  public static UserCmpyLink build(UserProfile user, Company cmpy, LinkType type, String adminId) {
    UserCmpyLink ucl = new UserCmpyLink();
    ucl.pk = DBConnectionMgr.getUniqueId();
    ucl.user = user;
    ucl.cmpy = cmpy;
    ucl.linktype = type;
    ucl.createdtimestamp = System.currentTimeMillis();
    ucl.createdby = adminId;
    ucl.modifiedby= adminId;
    ucl.lastupdatedtimestamp = ucl.createdtimestamp;
    ucl.status = APPConstants.STATUS_ACTIVE;
    ucl.isowner = 1;
    return ucl;
  }

  public static Model findByPK(String pk) {
    return find.byId(pk);
  }

  public static List<UserCmpyLink> findByNameAndCmpy(String term, String cmpyId) {
    return find.where().eq("status", 0).eq("cmpyid", cmpyId).findList();
  }

  public static List<UserCmpyLink> findAll() {
    return find.all();
  }

  public static List<UserCmpyLink> findActiveByUserId(String userId) {
    return find.where().eq("userid", userId).eq("status", 0).findList();
  }

  public static List<UserCmpyLink> findActiveByUserIdAndCmpyName(String userId, String cmpyName) {

    com.avaje.ebean.Query<Company> subQuery = Ebean.createQuery(Company.class)
                                                           .select("cmpyid")
                                                           .where()
                                                           .icontains("name", cmpyName)
                                                           .query();

    return find.where().eq("userid", userId).eq("status", 0).in("cmpyid", subQuery).findList();
  }


  public static List<UserCmpyLink> findActiveByCmpyId(String cmpyId) {
    return find.where().eq("cmpyid", cmpyId).eq("status", 0).findList();
  }

  public static int activeCountByCmpyId(String cmpyId) {
    return find.where().eq("cmpyid", cmpyId).eq("status", 0).findCount();
  }

  public static List<UserCmpyLink> findActiveByUserIdCmpyId(String userId, String cmpyId) {
    return find.where().eq("userid", userId).eq("cmpyid", cmpyId).eq("status", 0).findList();
  }

  public static List<UserCmpyLink> findByUserIdCmpyId(String userId, String cmpyId) {
    return find.where().eq("userid", userId).eq("cmpyid", cmpyId).findList();
  }


  public static int hardDeleteForUser(String userId) {
    String s = "DELETE FROM user_cmpy_link WHERE userid = :userId;";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("userId", userId);
    return Ebean.execute(update);
  }

  public static int mergeCmpy(String srcCmpyId, String targetCmpyId, String userId) {
    String s = "UPDATE user_cmpy_Link set cmpyid = :targetCmpyId, lastupdatedtimestamp = :timestamp, " +
               "" + "modifiedby = :userid  where cmpyid = :srcCmpyId and status = 0";
    SqlUpdate update = Ebean.createSqlUpdate(s);
    update.setParameter("timestamp", System.currentTimeMillis());
    update.setParameter("userid", userId);
    update.setParameter("srcCmpyId", srcCmpyId);
    update.setParameter("targetCmpyId", targetCmpyId);

    return Ebean.execute(update);
  }

  public static List<UserCmpyLink> getCompanyAdministrators(String cmpyid) {
    return find.where().eq("cmpyid", cmpyid).eq("linktype", UserCmpyLink.LinkType.ADMIN.getIdx()).findList();
  }

  //Helper to retrieve AccessLevel for each UserId within target company
  public static String getUserAccessLevel(String userId, String cmpyId) {
    List<UserCmpyLink> userCmpyLink = findActiveByUserIdCmpyId(userId, cmpyId);
    String accessLevel = null;
    if (userCmpyLink != null && userCmpyLink.size() > 0) {
      accessLevel = userCmpyLink.get(0).getLinktype().name();
    }
    return accessLevel;
  }

  public LinkType getLinktype() {
    return linktype;
  }

  public void setLinktype(LinkType linktype) {
    this.linktype = linktype;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getPk() {
    return pk;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }


  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public Company getCmpy() {
    return cmpy;
  }

  public void setCmpy(Company cmpy) {
    this.cmpy = cmpy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public UserProfile getUser() {
    return user;
  }

  public void setUser(UserProfile user) {
    this.user = user;
  }

  public int getIsowner() {
    return isowner;
  }

  public void setIsowner(int isowner) {
    this.isowner = isowner;
  }
}
