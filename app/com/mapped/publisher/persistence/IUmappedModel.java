package com.mapped.publisher.persistence;

import java.beans.Transient;

/**
 * Created by surge on 2016-11-03.
 */
public interface IUmappedModel<PKT, T extends IUmappedModel<PKT, T>>
    extends IUmappedBaseModel {
  @Transient
  T getByPk(PKT pk);

  @Transient
  PKT getPk();
}
