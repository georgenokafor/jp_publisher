package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class MapDirectionMgr {

	 private static final String SELECT_ALL = "Select Pk, MapsId, StartMilestonesId, EndMilestonesId, Rank, Tag, Type, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, ModifiedBy, CreatedBy, Note from MapDirection;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, MapsId, StartMilestonesId, EndMilestonesId, Rank, Tag, Type, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, ModifiedBy, CreatedBy, Note from MapDirection where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from MapDirection where Pk = ?;";
	 private static final String INSERT = "insert into MapDirection (Pk, MapsId, StartMilestonesId, EndMilestonesId, Rank, Tag, Type, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp, ModifiedBy, CreatedBy, Note) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update MapDirection SET MapsId=?, StartMilestonesId=?, EndMilestonesId=?, Rank=?, Tag=?, Type=?, LastUpdateTimestamp=?, SyncTimestamp=?, ModifiedBy=?, Note=? WHERE Pk = ?; ";
	 public static ArrayList <MapDirection> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return MapDirectionMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static MapDirection getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<MapDirection> results = MapDirectionMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (MapDirection c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getMapsId());
				 prep.setString(3, c.getStartMilestonesId());
				 prep.setString(4, c.getEndMilestonesId());
				 prep.setString(5, c.getRank());
				 prep.setString(6, c.getTag());
				 prep.setString(7, c.getType());
				 prep.setLong(8, c.getLastUpdateTimestamp());
				 prep.setLong(9, c.getCreateTimestamp());
				 prep.setLong(10, c.getSyncTimestamp());
				 prep.setString(11, c.getModifiedBy());
				 prep.setString(12, c.getCreatedBy());
				 prep.setString(13, c.getNote());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (MapDirection c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getMapsId());
				 prep.setString(2, c.getStartMilestonesId());
				 prep.setString(3, c.getEndMilestonesId());
				 prep.setString(4, c.getRank());
				 prep.setString(5, c.getTag());
				 prep.setString(6, c.getType());
				 prep.setLong(7, c.getLastUpdateTimestamp());
				 prep.setLong(8, c.getSyncTimestamp());
				 prep.setString(9, c.getModifiedBy());
				 prep.setString(10, c.getNote());
				 prep.setString(11, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <MapDirection> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <MapDirection> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 MapDirection c = new MapDirection();
			 c.setPk(rs.getString("Pk"));
			 c.setMapsId(rs.getString("MapsId"));
			 c.setStartMilestonesId(rs.getString("StartMilestonesId"));
			 c.setEndMilestonesId(rs.getString("EndMilestonesId"));
			 c.setRank(rs.getString("Rank"));
			 c.setTag(rs.getString("Tag"));
			 c.setType(rs.getString("Type"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setModifiedBy(rs.getString("ModifiedBy"));
			 c.setCreatedBy(rs.getString("CreatedBy"));
			 c.setNote(rs.getString("Note"));
			 results.add(c);
		 }
		 return results;
	 }
}
