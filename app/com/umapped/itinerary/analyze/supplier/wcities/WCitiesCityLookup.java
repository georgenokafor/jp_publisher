package com.umapped.itinerary.analyze.supplier.wcities;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;
import com.umapped.itinerary.analyze.model.Location;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import javax.inject.Singleton;
import java.util.HashMap;
import java.util.List;

/**
 * Created by wei on 2017-03-31.
 */
@Singleton
public class WCitiesCityLookup {

  private static final String City_Lookup = "SELECT wcities_city.wcities_city_id as city_id, um_city.name as name, "
  + "ST_Distance(um_city.geo_location, ST_MakePoint(:lng, :lat)) distance "
  + "FROM um_city, wcities_city WHERE um_city.id=wcities_city.um_city_id and "
  + "ST_Distance(um_city.geo_location, ST_MakePoint(:lng, :lat)) <= :radius * 1000 order by distance asc";

  private static final String WCITY_City_ID_Lookup = "SELECT wcities_city_id as city_id, um_city.name as name " +
                                            " FROM wcities_city, um_city WHERE wcities_city.um_city_id=:um_city_id and um_city.id=wcities_city.um_city_id ";

  private static final String City_Lookup_name = "SELECT um_city_id as city_id, um_city.name as name, um_city.country as country" +
          " FROM wcities_city, um_city WHERE um_city.id=wcities_city.um_city_id and upper(unaccent(um_city.name)) like unaccent(:name) ";

  public WCitiesCityLookup() {
  }

  private List<SqlRow> getCities(Location.GeoLocation location) {
    if (location == null) {
      return null;
    }
    List<SqlRow> rows = Ebean.createSqlQuery(City_Lookup)
                             .setParameter("lng", location.getLongitude())
                             .setParameter("lat", location.getLatitude())
                             .setParameter("radius", 50).findList();

    return rows;
  }

  public String getCityId(Location.GeoLocation location) {
    List<SqlRow> rows = getCities(location);
    if (rows != null && rows.size() > 0) {
      return rows.get(0).getString("city_id");
    } else {
      return null;
    }
  }

  public Pair<String, String> getCity(Location.GeoLocation location) {
    List<SqlRow> rows = getCities(location);
    if (rows != null && rows.size() > 0) {
      return new ImmutablePair<>(rows.get(0).getString("city_id"), rows.get(0).getString("name"));
    } else {
      return null;
    }
  }

  public Pair<String, String> getCity(String umappedCityId) {
    SqlRow row = Ebean.createSqlQuery(WCITY_City_ID_Lookup).setParameter("um_city_id", umappedCityId).findUnique();
    if (row != null) {
      return new ImmutablePair<>(row.getString("city_id"), row.getString("name"));
    } else {
      return null;
    }

  }

  public String getCityId(String umappedCityId) {
    SqlRow row = Ebean.createSqlQuery(WCITY_City_ID_Lookup).setParameter("um_city_id", umappedCityId).findUnique();
    if (row != null) {
      return row.getString("city_id");
    } else {
      return null;
    }
  }

  public HashMap<String, String> getCities(String name) {
    HashMap <String, String> cities = new HashMap();
    if (name == null) {
      return null;
    }
    List<SqlRow> rows = Ebean.createSqlQuery(City_Lookup_name)
            .setParameter("name", name.toUpperCase())
            .findList();

    if (rows != null && rows.size() > 0) {
      for (SqlRow row: rows) {
        String key = row.getString("city_id");
        String value = row.getString("name") + "," + row.getString("country");
        cities.put(key, value);
      }
    }
    return cities;
  }
}
