package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.actions.Authenticated;
import com.mapped.publisher.actions.Authorized;
import com.mapped.publisher.common.*;
import com.mapped.publisher.form.TripSearchForm;
import com.mapped.publisher.parse.schemaorg.Organization;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import com.mapped.publisher.parse.tramada.TramadaHelper;
import com.mapped.publisher.persistence.PoiMgr;
import com.mapped.publisher.persistence.PoiRS;
import com.mapped.publisher.persistence.PoiTypeInfo;
import com.mapped.publisher.persistence.TripMgr;
import com.mapped.publisher.utils.Log;
import com.mapped.publisher.utils.Utils;
import com.mapped.publisher.view.*;
import com.umapped.BodyParsers.TolerantText1Mb;
import com.umapped.common.BkApiSrcIdConstants;
import com.umapped.persistence.enums.ReservationType;
import models.publisher.*;
import org.apache.commons.lang3.time.StopWatch;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;
import play.mvc.BodyParser;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.SystemDefaultCredentialsProvider;

import java.sql.Connection;
import java.util.*;

/**
 * Created by george on 2017-08-24.
 */
public class TramadaController extends Controller {

  @BodyParser.Of(TolerantText1Mb.class)
  public Result importBooking() {
    try {
        StopWatch sw = new StopWatch();
        sw.start();
        String authToken = request().getHeader("Authorization");
        if (authToken != null && authToken.contains("Bearer")) {
          authToken = authToken.replace("Bearer", "").trim();
        }
        ApiCmpyToken cmpyToken = null;
        //security validation
        cmpyToken = (ApiCmpyToken) CacheMgr.get(APPConstants.CACHE_API_CMPY_TOKEN_PREFIX + authToken);
        if (cmpyToken == null) {
          cmpyToken = ApiCmpyToken.findByToken(authToken);
          if (cmpyToken != null) {
            CacheMgr.set(APPConstants.CACHE_API_CMPY_TOKEN_PREFIX + cmpyToken.getToken(), cmpyToken);
          }
        }
        if (cmpyToken == null) {
            Log.debug("TramadaController:booking Authentication error: " + authToken);
            return status(403,  "<ResponseCode>403</ResponseCode>").as("application/xml");
        }
        Company company = Company.find.byId(cmpyToken.getuCmpyId());
        String agencyCode = cmpyToken.getSrcCmpyId();
        if (company != null && cmpyToken.getBkApiSrcId() == BkApiSrc.getId(BkApiSrcIdConstants.TRAMADA)) {
          String requestText = request().body().asText();// Log the request
          Log.debug("TramadaController:booking original request: " + requestText);

          if (requestText != null && requestText.trim().contains("<DocumentInformation>") && requestText.trim().contains("</DocumentInformation>")) {
              String s = requestText.substring(requestText.indexOf("<DocumentInformation>"), requestText.indexOf("</DocumentInformation>") + 22);
              requestText = s.trim();
              Log.debug("TramadaController:booking corrected request: " + requestText);
          }
            TramadaHelper tramadaHelper = new TramadaHelper();
            boolean parsed = tramadaHelper.parseXMLPayload(requestText);
            ReservationPackage rp = null;
            if (parsed) {
                rp = tramadaHelper.toReservationPackage();
                if (rp != null && rp.reservation != null && rp.reservation.size() > 0) {
                    ApiCmpyToken token = null;
                    if (tramadaHelper.docInfo.getAgency() != null && tramadaHelper.docInfo.getAgency().getAgencyCode().getValue() != null) {
                        token = ApiCmpyToken.findCmpyTokenByTypSrcCmpy(tramadaHelper.docInfo.getAgency().getAgencyCode().getValue(), BkApiSrc.getId(BkApiSrcIdConstants.TRAMADA));
                        if (token != null && (token.getToken().equals(authToken) || agencyCode.equals("TRAMADA"))) { //make sure the token matches the agency code or TRAMADA
                            //set the umid for the rp and the agent id
                            rp.umId = token.getSrcCmpyId();
                            rp.bookingAgent = new Organization();
                            if (tramadaHelper.docInfo.getBooking().getBookingDetails().getBookingConsultant1EmailAddress() != null && !tramadaHelper.docInfo.getBooking().getBookingDetails().getBookingConsultant1EmailAddress().getValue().isEmpty()) {
                              rp.bookingAgent.umId = tramadaHelper.docInfo.getBooking().getBookingDetails().getBookingConsultant1EmailAddress().getValue();
                            } else if (tramadaHelper.docInfo.getBooking().getBookingDetails().getBookingConsultant1() != null && !tramadaHelper.docInfo.getBooking().getBookingDetails().getBookingConsultant1().getValue().isEmpty()) {
                              rp.bookingAgent.umId = tramadaHelper.docInfo.getBooking().getBookingDetails().getBookingConsultant1().getValue();
                            } else if (tramadaHelper.docInfo.getBooking().getBookingDetails().getBookedBy() != null && !tramadaHelper.docInfo.getBooking().getBookingDetails().getBookedBy().getValue().isEmpty()) {
                              rp.bookingAgent.umId = tramadaHelper.docInfo.getBooking().getBookingDetails().getBookedBy().getValue();
                            } else {
                              rp.bookingAgent.umId = "";
                            }
                            int code = tramadaHelper.callBookingApi(rp, token.getToken());
                            return status(code, "<ResponseCode>" + code + "</ResponseCode>").as("application/xml");
                        } else {
                          Log.debug("TramadaController: Agent Authentication Error - Agency Code does not exist as a token within Umapped. Contact Umapped Support");
                          return status(403, "<ResponseCode>403</ResponseCode>").as("application/xml");
                        }
                    } else {
                      Log.debug("TramadaController: Error - AgencyCode required.");
                    }
                } else {
                  Log.debug("TramadaController: Error creating Reservation Package.");
                }
            } else {
              Log.debug("TramadaController: Error - XML parsing failed");
            }
        } else {
          Log.debug("TramadaController: Authentication Error - Request is not from Tramada");
          return status(403, "<ResponseCode>403</ResponseCode>").as("application/xml");
        }

    } catch (Exception e) {
        Log.err("TramadaController:importBooking ", e);
    }
    Log.debug("TramadaController: Error Processing the request");
    return status(500, "<ResponseCode>500</ResponseCode>").as("application/xml");

  }
}


