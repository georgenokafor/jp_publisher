package com.umapped.itinerary.analyze.supplier.travelbound;

import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.supplier.travelbound.jaxb.*;
import com.umapped.itinerary.analyze.webservice.WebAPIServiceRequest;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wei on 2017-04-17.
 */
public class TravelBoundAPIRequestBuilder
    extends WebAPIServiceRequest {
  private static Logger LOG = LoggerFactory.getLogger(TravelBoundAPIRequestBuilder.class);

  private static int CLIENT_ID = 46607;
  private static String EMAIL_ADDRESS = "API@UMAPPED.COM";
  private static String PASSWORD = "PASS";

  public TRequest createSearchHotelRequest(StayPeriod stayPeriod, Location.GeoLocation location) {
    TRequest request = createRequestWrapper();
    TRequestDetails requestDetails = request.getRequestDetails();

    TSearchHotelPriceRequest hotelRequest = new TSearchHotelPriceRequest();
    JAXBElement<TSearchHotelPriceRequest> element = new JAXBElement<TSearchHotelPriceRequest>(new QName("",
                                                                                                        "SearchHotelPriceRequest"),
                                                                                              TSearchHotelPriceRequest.class,
                                                                                              hotelRequest);
    requestDetails.getSearchHotelPricePaxRequestOrAddBookingItemRequestOrAddBookingRequest().add(element);
    TItemDestination destination = new TItemDestination();
    destination.setDestinationType(TDestinationType.GEOCODE);
    destination.setLatitude(location.getLatitude());
    destination.setLongitude(location.getLongitude());
    destination.setRadiusKm(30.0);
    hotelRequest.setItemDestination(destination);
    hotelRequest.setIncludePriceBreakdown(new TEmpty());
    hotelRequest.setIncludeRecommended(new TEmpty());
    hotelRequest.setImmediateConfirmationOnly(new TEmpty());
    TPeriodOfStay periodOfStay = new TPeriodOfStay();
    periodOfStay.setCheckInDate(fromLocalDate(stayPeriod.getStart()));
    periodOfStay.setCheckOutDate(fromLocalDate(stayPeriod.getEnd()));
    hotelRequest.setPeriodOfStay(periodOfStay);
    hotelRequest.setPeriodOfStay(periodOfStay);
    TRooms rooms = new TRooms();
    TRoom room = new TRoom();
    room.setCode("SB");
    room.setNumberOfRooms(1);

    rooms.getRoom().add(room);
    hotelRequest.setRooms(rooms);
    TStarRating rating = new TStarRating();
    rating.setValue("3");
    rating.setMinimumRating(true);
    hotelRequest.setStarRating(rating);
    hotelRequest.setOrderBy("pricelowtohigh");
    hotelRequest.setNumberOfReturnedItems(5);
    return request;
  }

  public TRequest getHotelsInformation(List<String> hotelIds) {
    TRequest request = createRequestWrapper();
    TRequestDetails requestDetails = request.getRequestDetails();

    for (String id : hotelIds) {
      String[] ids = StringUtils.split(id, "-");
      TSearchItemInformationRequest r = new TSearchItemInformationRequest();
      r.setItemType(TItemType.HOTEL);
      TItemDestination destination = new TItemDestination();
      destination.setDestinationType(TDestinationType.CITY);
      destination.setDestinationCode(ids[0]);
      r.setItemDestination(destination);
      r.setItemCode(ids[1]);
      requestDetails.getSearchHotelPricePaxRequestOrAddBookingItemRequestOrAddBookingRequest()
                    .add(new JAXBElement<>(new QName("", "SearchItemInformationRequest"),
                                           TSearchItemInformationRequest.class,
                                           r));
    }
    return request;
  }

  private XMLGregorianCalendar fromLocalDate(LocalDate date) {
    try {
      XMLGregorianCalendar calendar = DatatypeFactory.newInstance()
                                                     .newXMLGregorianCalendar(date.getYear(),
                                                                              date.getMonthValue(),
                                                                              date.getDayOfMonth(),
                                                                              DatatypeConstants.FIELD_UNDEFINED,
                                                                              DatatypeConstants.FIELD_UNDEFINED,
                                                                              DatatypeConstants.FIELD_UNDEFINED,
                                                                              DatatypeConstants.FIELD_UNDEFINED,
                                                                              DatatypeConstants.FIELD_UNDEFINED);
      return calendar;
    }
    catch (DatatypeConfigurationException e) {
      LOG.error("Fail to create xml data", e);
      throw new RuntimeException(e);
    }
  }

  private TRequest createRequestWrapper() {
    TRequest request = new TRequest();
    TSource source = new TSource();

    request.setSource(source);

    source.setRequestorID(getRequestorID());
    source.setRequestorPreferences(getRequestorPreferences());

    TRequestDetails details = new TRequestDetails();
    request.setRequestDetails(details);
    return request;
  }

  private TRequestorID getRequestorID() {
    TRequestorID requestorId = new TRequestorID();
    requestorId.setClient(CLIENT_ID);
    requestorId.setEMailAddress(EMAIL_ADDRESS);
    requestorId.setPassword(PASSWORD);
    return requestorId;
  }

  private TRequestorPreferences getRequestorPreferences() {
    TRequestorPreferences pref = new TRequestorPreferences();
    pref.setCountry("US");
    pref.setCurrency("USD");
    pref.setLanguage("en");
    pref.setRequestMode(TRequestMode.SYNCHRONOUS);
    return pref;
  }
}
