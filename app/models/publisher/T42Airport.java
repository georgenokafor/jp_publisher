package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created by surge on 2015-11-24.
 */
@Entity @Table(name = "t42_airport")
public class T42Airport
    extends Model {
  public static Model.Finder<Integer, T42Airport> find = new Finder(T42Airport.class);

  @Id Integer airportPlaceKey;
  String airportCode;
  String name;
  Integer geoPlaceKey;
  String geoDisplay;
  Float latitude;
  Float longitude;
  Boolean active;
  @Version int version;

  public static T42Airport findByKey(String placeKey) {
    Integer key = null;
    try {
      key = Integer.parseInt(placeKey);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
      return null;
    }
    return find.byId(key);
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public Integer getAirportPlaceKey() {
    return airportPlaceKey;
  }

  public void setAirportPlaceKey(Integer airportPlaceKey) {
    this.airportPlaceKey = airportPlaceKey;
  }

  public void setAirportPlaceKey(String airportPlaceKey) {
    try {
      this.airportPlaceKey = Integer.parseInt(airportPlaceKey);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public String getAirportCode() {
    return airportCode;
  }

  public void setAirportCode(String airportCode) {
    this.airportCode = airportCode;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getGeoPlaceKey() {
    return geoPlaceKey;
  }

  public void setGeoPlaceKey(Integer geoPlaceKey) {
    this.geoPlaceKey = geoPlaceKey;
  }

  public void setGeoPlaceKey(String geoPlaceKey) {
    try {
      this.geoPlaceKey = Integer.parseInt(geoPlaceKey);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public String getGeoDisplay() {
    return geoDisplay;
  }

  public void setGeoDisplay(String geoDisplay) {
    this.geoDisplay = geoDisplay;
  }

  public Float getLatitude() {
    return latitude;
  }

  public void setLatitude(Float latitude) {
    this.latitude = latitude;
  }

  public void setLatitude(String latitude) {
    if(latitude == null || latitude.length() == 0) {
      return;
    }
    try {
      this.latitude = Float.parseFloat(latitude);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }

  public Float getLongitude() {
    return longitude;
  }

  public void setLongitude(Float longitude) {
    this.longitude = longitude;
  }

  public void setLongitude(String longitude) {
    if(longitude == null || longitude.length() == 0) {
      return;
    }
    try {
      this.longitude = Float.parseFloat(longitude);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }
}
