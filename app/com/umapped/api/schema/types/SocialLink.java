package com.umapped.api.schema.types;

import com.fasterxml.jackson.annotation.*;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Social link service parameters
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder({ "service_name", "service_username", "service_direct_url" })
public class SocialLink implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  /**
   * Name like 'YouTube', 'Facebook', 'MySpace', etc without with spaces removed
   */
  @JsonProperty("service_name")
  private String serviceName;
  @JsonProperty("service_username")
  private String serviceUsername;
  /**
   * Direct URL for the service in case service is not natively handled by the
   * platform
   */
  @JsonProperty("service_direct_url")
  private String serviceDirectUrl;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  public SocialLink() {

  }

  public SocialLink(String serviceName, String serviceUsername, String serviceDirectUrl) {
    this.serviceName = serviceName;
    this.serviceUsername = serviceUsername;
    this.serviceDirectUrl = serviceDirectUrl;
  }

  @Override
  public String toString() {
    return "[" + serviceName + "@" + getServiceUsername() + "]" + serviceDirectUrl + ";;";
  }

  /**
   * Name like 'YouTube', 'Facebook', 'MySpace', etc without with spaces removed
   *
   * @return The serviceName
   */
  @JsonProperty("service_name")
  public String getServiceName() {
    return serviceName;
  }

  @Override
  public int hashCode() {
    int hash = 1;

    if (serviceName != null && serviceName.hashCode() != 0) {
      hash *= serviceName.hashCode();
    }

    if (serviceUsername != null && serviceUsername.hashCode() != 0) {
      hash *= serviceUsername.hashCode();
    }

    if (serviceDirectUrl != null && serviceDirectUrl.hashCode() != 0) {
      hash *= serviceDirectUrl.hashCode();
    }

    return hash;
  }

  /**
   * Name like 'YouTube', 'Facebook', 'MySpace', etc without with spaces removed
   *
   * @param serviceName
   *          The service_name
   */
  @JsonProperty("service_name")
  public void setServiceName(String serviceName) {
    this.serviceName = serviceName;
  }

  public SocialLink withServiceName(String serviceName) {
    this.serviceName = serviceName;
    return this;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if ((obj == null) || (obj.getClass() != this.getClass())) {
      return false;
    }

    return this.hashCode() == obj.hashCode();
  }

  public SocialLink withServiceUsername(String serviceUsername) {
    this.serviceUsername = serviceUsername;
    return this;
  }

  /**
   * Direct URL for the service in case service is not natively handled by the
   * platform
   *
   * @return The serviceDirectUrl
   */
  @JsonProperty("service_direct_url")
  public String getServiceDirectUrl() {
    return serviceDirectUrl;
  }

  /**
   * Direct URL for the service in case service is not natively handled by the
   * platform
   *
   * @param serviceDirectUrl
   *          The service_direct_url
   */
  @JsonProperty("service_direct_url")
  public void setServiceDirectUrl(String serviceDirectUrl) {
    this.serviceDirectUrl = serviceDirectUrl;
  }

  public SocialLink withServiceDirectUrl(String serviceDirectUrl) {
    this.serviceDirectUrl = serviceDirectUrl;
    return this;
  }

  /**
   * @return The serviceUsername
   */
  @JsonProperty("service_username")
  public String getServiceUsername() {
    return serviceUsername;
  }

  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  /**
   * @param serviceUsername
   *          The service_username
   */
  @JsonProperty("service_username")
  public void setServiceUsername(String serviceUsername) {
    this.serviceUsername = serviceUsername;
  }

  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

  public SocialLink withAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
    return this;
  }
}
