package com.umapped.itinerary.analyze.segment.feature;

import java.util.Optional;

/**
 * Created by wei on 2017-03-10.
 */

public interface SegmentFeatureDetectorRegistry {
  Optional<SegmentFeatureDetector> getDetector(SegmentFeature feature);
}
