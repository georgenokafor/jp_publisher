package com.mapped.publisher.view;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.Comparator;

/**
 * Created by twong on 2014-10-16.
 */
public class BookingAndPageView {

  public TripBookingDetailView tripBookingDetailView;
  public DestinationGuideView destinationGuideView;

  public long timestamp;

  public static Comparator<BookingAndPageView> BookingAndPageViewComparator = new
      Comparator<BookingAndPageView>() {

        public int compare(BookingAndPageView t1, BookingAndPageView t2) {

          if (t1.tripBookingDetailView != null && t2.tripBookingDetailView != null) {
            DateTime t1DateTime = new DateTime(t1.timestamp);
            DateTime t2DateTime = new DateTime(t2.timestamp);
            if (t1DateTime.withTimeAtStartOfDay().equals(t2DateTime.withTimeAtStartOfDay())) {
              if (t1.tripBookingDetailView.rank > 0 && t2.tripBookingDetailView.rank > 0) {
                if (t1.tripBookingDetailView.rank > t2.tripBookingDetailView.rank) {
                  return 1;
                } else if (t1.tripBookingDetailView.rank < t2.tripBookingDetailView.rank) {
                  return -1;
                } else {
                  return 0;
                }
              } else if (t1.tripBookingDetailView.rank > 0) {
                return 1;
              } else if (t2.tripBookingDetailView.rank > 0) {
                return -1;
              } else {
                if (t1.timestamp > t2.timestamp) {
                  return 1;
                }
                else if (t1.timestamp < t2.timestamp) {
                  return -1;
                }
                else {
                  return 0;
                }
              }
            } else {
              if (t1.timestamp > t2.timestamp) {
                return 1;
              }
              else if (t1.timestamp < t2.timestamp) {
                return -1;
              }
              else {
                return 0;
              }
            }
          } else {
            if (t1.tripBookingDetailView != null && t1.tripBookingDetailView.rank > 0) {
              return -1;
            } else if (t2.tripBookingDetailView != null && t2.tripBookingDetailView.rank > 0) {
              return 1;
            }

            if (t1.timestamp > t2.timestamp) {
              return 1;
            }
            else if (t1.timestamp < t2.timestamp) {
              return -1;
            }
            else {
              return 0;
            }
          }
        }

      };
}
