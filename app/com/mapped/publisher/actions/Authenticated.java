package com.mapped.publisher.actions;

import com.mapped.publisher.common.ConfigMgr;
import com.mapped.publisher.common.Credentials;
import com.mapped.publisher.common.SessionConstants;
import com.mapped.publisher.common.SessionMgr;
import com.mapped.publisher.utils.Log;
import org.apache.commons.lang3.time.StopWatch;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class Authenticated
    extends Action.Simple {

  public CompletionStage<Result> call(Http.Context ctx) {
    //set no cache
    ctx.response().setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    StopWatch sw = new StopWatch();
    sw.start();
    SessionMgr sessionMgr = new SessionMgr(ctx.session());
    if (sessionMgr.isSessionValid()) {
      Credentials cred = sessionMgr.getCredentials();
      if (cred != null && cred.getBillingStatus() != null) {
        if (cred.getBillingStatus() == Credentials.BillingStatus.AGENT_SUBSCRIPTION_NOT_SETUP) {
          sessionMgr.setParam(SessionConstants.SESSION_PARAM_MSG, "Your billing information is incomplete - please contact your company administrator or support@umapped.com");
          if(ConfigMgr.isProduction() || ConfigMgr.isLocalDev()) {
            return CompletableFuture.completedFuture(redirect("https://" + ctx.request().host() + "/login"));
          } else {
            return CompletableFuture.completedFuture(redirect("http://" + ctx.request().host() + "/login"));
          }
        }
        else if(cred.getUserId() != null && !cred.isSubscribedToBilling()) {
          if(ConfigMgr.isProduction() || ConfigMgr.isLocalDev()) {
            return CompletableFuture.completedFuture(redirect("https://" + ctx.request().host() + "/user/subscription"));
          } else {
            return CompletableFuture.completedFuture(redirect("http://" + ctx.request().host() + "/user/subscription"));
          }
        } else if (cred.getBillingStatus() == Credentials.BillingStatus.CREDIT_CARD_SETUP_NEEDED
                 || cred.getBillingStatus() == Credentials.BillingStatus.CREDIT_CARD_UPDATE_NEEDED) {
          if(ConfigMgr.isProduction() || ConfigMgr.isLocalDev()) {
            return CompletableFuture.completedFuture(redirect("https://" + ctx.request().host() + "/billing/setup"));
          } else {
            return CompletableFuture.completedFuture(redirect("http://" + ctx.request().host() + "/billing/setup"));
          }
        }
      }

      ctx.args.put(SessionMgr.CONTEXT_KEY, sessionMgr);
      Log.debug("Authentication SUCCESS in " + sw.getTime() + "ms for context: ", ctx.toString());
      return delegate.call(ctx);
    }
    else {
      if (sessionMgr.isSessionIdMismatch()) {
        sessionMgr.setParam(SessionConstants.SESSION_PARAM_MSG, "Your session has expired - please log in again and" +
                                                                " make sure you are not logged in from two locations.");
        Log.info("Authentication FAILED in " + sw.getTime() + "ms - multiple login - for context: ", ctx.toString());
      } else if (sessionMgr.isSessionExpired()){
        sessionMgr.setParam(SessionConstants.SESSION_PARAM_MSG, "Your session has expired - please log in again.");
        Log.info("Authentication FAILED in " + sw.getTime() + "ms - session expired - for context: ", ctx.toString());
      } else {
        Log.info("Authentication FAILED in " + sw.getTime() + "ms for context: ", ctx.toString());
      }

      if(ConfigMgr.isProduction() || ConfigMgr.isLocalDev()) {
        return CompletableFuture.completedFuture(redirect("https://" + ctx.request().host() + "/login"));
      } else {
        return CompletableFuture.completedFuture(redirect("http://" + ctx.request().host() + "/login"));
      }
    }
  }
}
