package com.umapped.api.billing;

import com.mapped.publisher.common.Credentials;
import com.mapped.publisher.view.BaseView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ryan on 31/10/16.
 */
public class BillingInfo extends BaseView {
    public BillingAddress bilAddr;
    public boolean hasPymt;

    public BillingPlan myBilPlan;

    public List<BillingPlan> billedToMe;

    public String btreeToken; //braintree JS token for authentication
    public String custId;

    public BillingInfo() {
      billedToMe = new ArrayList<>();
    }

    public BillingCCInfo ccInfo;

    public Credentials.BillingStatus billingStatus;
}
