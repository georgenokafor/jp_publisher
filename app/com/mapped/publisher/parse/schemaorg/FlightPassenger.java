package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.mapped.publisher.parse.schemaorg.FlightReservation;

/**
 * Created by twong on 20/06/17.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
public class FlightPassenger extends FlightReservation {
}
