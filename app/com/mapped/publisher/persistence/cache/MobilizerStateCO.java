package com.mapped.publisher.persistence.cache;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.CacheMgr;
import models.publisher.LibraryPage;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by surge on 2015-04-30.
 */
public class MobilizerStateCO
    implements Serializable {

  /**
   * After how many next() calls should cache be updated
   */
  private static final int DEFAULT_UPDATE_RATE = 2;
  public State currState;
  public long lastUpdateTs;
  public int currItem;
  public int updateRate;

  public List<LibraryPage>    pages;
  public Map<String, String>  links;

  /**
   * -1 indicates unknown number of items. 0 for stop all positive number are legal.
   */
  public int totalItems;
  @Transient
  private String uuid;

  public static enum State {
    STOPPED,
    CONNECTING,
    INVALID_LOGIN,
    NO_DATA,
    PARSING,
    SUCCESS,
    UNKNOWN_ERROR
  }

  public MobilizerStateCO(String requestUUID) {
    uuid = requestUUID;
    currState = State.STOPPED;
    lastUpdateTs = System.currentTimeMillis();
    currItem = 0;
    totalItems = 0;
    updateRate = DEFAULT_UPDATE_RATE;
  }

  public static MobilizerStateCO getFromCache(String uuid) {
    MobilizerStateCO state = (MobilizerStateCO) CacheMgr.get(APPConstants.CACHE_MOBILIZER_STATE + uuid);
    if (state == null) {
      state = new MobilizerStateCO(uuid);
    }
    return state;
  }

  public boolean start(State state, int totalItems) {
    currState = state;
    lastUpdateTs = System.currentTimeMillis();
    currItem = 0;
    this.totalItems = totalItems;
    updateCache();
    return true;
  }

  public boolean start(State state, int totalItems, int updateRate) {
    this.updateRate = updateRate;
    return start(state, totalItems);
  }

  private void updateCache() {
    CacheMgr.set(APPConstants.CACHE_MOBILIZER_STATE + uuid, this, APPConstants.CACHE_MOBILIZER_EXPIRY_SECS);
  }

  /**
   * Increments the running state and updates it every 50th item
   */
  public void next() {
    currItem++;
    if (currItem % updateRate == 0) {
      update(currItem);
    }
  }

  public void update(int currItemId) {
    lastUpdateTs = System.currentTimeMillis();
    currItem = currItemId;
    updateCache();
  }


  public void success() {
    finish(State.SUCCESS);
  }

  public void error() {
    finish(State.UNKNOWN_ERROR);
  }

  public void finish(State state) {
    currState = state;
    lastUpdateTs = System.currentTimeMillis();
    currItem = 0;
    totalItems = 0;
    updateCache();
  }

}
