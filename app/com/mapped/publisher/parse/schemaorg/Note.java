package com.mapped.publisher.parse.schemaorg;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ryan on 15-08-20.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Note
    extends Reservation {

  public Place    reservationFor;
  public DateTime noteTimestamp;

  public List<Thing>       urls;
  public List<VideoObject> videos;

  @Override
  public DateTime getStartDateTime() {
    return noteTimestamp;
  }

  @Override
  public DateTime getFinishDateTime() {
    return null;
  }

  public void addVideo(VideoObject vo) {
    if(videos == null) {
      videos = new ArrayList<>(1);
    }
    videos.add(vo);
  }

  public void addUrl(Thing t) {
    if(urls == null) {
      urls = new ArrayList<>(1);
    }
    urls.add(t);
  }
}
