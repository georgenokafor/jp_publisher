package models.publisher;

import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Created by surge on 2016-02-24.
 */
@Entity
@Table(name = "account_note")
public class AccountNote
    extends Model {

  public static Finder<Long, AccountNote> find = new Finder<>(AccountNote.class);

  @Id
  Long pk;
  Long      uid;
  String    note;
  Timestamp createdTs;
  Long      createdBy;
  Timestamp modifiedTs;
  Long      modifiedBy;
  @Version
  int version;

  public static AccountNote build(Long createdBy) {
    AccountNote an = new AccountNote();
    an.setPk(DBConnectionMgr.getUniqueLongId());
    an.setCreatedTs(Timestamp.from(Instant.now()));
    an.setModifiedTs(an.createdTs);
    an.setCreatedBy(createdBy);
    an.setModifiedBy(createdBy);
    return an;
  }

  public static List<AccountNote> accountNotes(Long uid) {
    return find.where().eq("uid", uid).findList();
  }

  public Long getPk() {
    return pk;
  }

  public void setPk(Long pk) {
    this.pk = pk;
  }

  public Long getUid() {
    return uid;
  }

  public void setUid(Long uid) {
    this.uid = uid;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public void setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
