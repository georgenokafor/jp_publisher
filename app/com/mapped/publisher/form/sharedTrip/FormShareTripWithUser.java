package com.mapped.publisher.form.sharedTrip;

import com.mapped.publisher.common.SecurityMgr;

/**
 * Created by surge on 2016-01-04.
 */
public class FormShareTripWithUser {
  public SecurityMgr.AccessLevel inAccessLevel;
  public String                  inShareNote;
  public String                  inShareUserId;

  public boolean isValid() {
    if (inAccessLevel == null || inShareNote == null || inShareUserId == null) {
      return false;
    }

    switch (inAccessLevel) {
      case READ:
      case APPEND:
      case APPEND_N_PUBLISH:
      case OWNER:
        break; //Continue access level is proper
      default:
        return false;
    }

    if (inShareUserId.length() <= 0) {
      return false;
    }

    return true;
  }
}
