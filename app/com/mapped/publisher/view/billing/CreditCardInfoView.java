package com.mapped.publisher.view.billing;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by twong on 2016-10-27.
 */
public class CreditCardInfoView {
  public boolean isCreditCardEligible = false;
  public boolean hasPlanSub = false;
  public List<CreditCardSubView> subscriptions;
  public String userBillingInfo;
  public String agentBillingInfo;

  public void addCreditCardSubView (CreditCardSubView subView) {
    if (subscriptions == null) {
      subscriptions = new ArrayList<>();
    }
    subscriptions.add(subView);
  }

}
