package com.umapped.persistence.notes;

import com.mapped.publisher.form.TripNoteForm;
import com.mapped.publisher.form.booking.FormBookingBase;
import com.mapped.publisher.form.booking.FormInsuranceNote;
import models.publisher.TripNote;

import java.util.Map;

/**
 * Created by george on 2017-03-26.
 */
public class DefaultNote extends StructuredNote {

  protected DefaultNote populateNote(TripNote tripNote, TripNoteForm tripNoteInfo) {
    return null;
  }

  @Override
  protected String renderIntro(StructuredNote noteData) {
    return null;
  }


}