package com.umapped.mobile.api.model;

import java.io.Serializable;

/**
 * Created by wei on 2017-06-28.
 */
public class AuthenticationRequest implements Serializable {
  protected String email;
  protected String password;
  protected String pnToken;
  protected AppDeviceInfo appDevice;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPnToken() {
    return pnToken;
  }

  public void setPnToken(String pnToken) {
    this.pnToken = pnToken;
  }

  public AppDeviceInfo getAppDevice() {
    return appDevice;
  }

  public void setAppDevice(AppDeviceInfo appDevice) {
    this.appDevice = appDevice;
  }
}
