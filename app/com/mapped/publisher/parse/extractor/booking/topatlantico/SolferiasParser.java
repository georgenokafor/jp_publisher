package com.mapped.publisher.parse.extractor.booking.topatlantico;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.time.DateUtils;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ryan on 15-04-04.
 */
public class SolferiasParser {
  private String file;
  private String fileLower = "";

  private TripVO trip;
  private static String[] dateTimeFormat = {"dd-MM-yyyy h:mm", "EEE ddMMM yyyy h:mm", "dd MMM yyyy h:mm", "yyyy-MM-dd h:mm", "ddMMM yyyy h:mm", "MMM dd, yyyy h:mm", "dd/MM/yyyy h:mm", "dd MMM yy kkmm", "ddMMMyy kkmm", "M/dd/yyyy h:mm:ss a", "dd MMM yyyy", "dd MMM yyyy ", "E dd MMM yyyy h:mma",  "E dd MMM yyyy", "E dd MMM yyyy ", "E dd MMMMM yyyy HH:mm" , "dd MMM yyyy HH:mm a", "E dd MMM yyyy HH:mm" , "dd MMMMM yyyy HH:mm", "E, MMMMM dd, yyyy HH:mm", "E, MMMMM dd, yyyy", "E, MMMMM dd, yyyy hh:mm a", "EEE dd MMM yyyy h:mm a", "EEE dd MMM yyyy 0h:mm a"} ;

  //private static String[] dateTimeFormat = {"dd MMM yyyy h:mm", "dd-MM-yyyy H:mm", "yyyy-MM-dd H:mm", "ddMMM yyyy h:mm", "MMM dd, yyyy h:mm", "dd/MM/yyyy H:mm", "dd/MM/yyyy H:mm:ss", "dd MMM yy kkmm", "ddMMMyy kkmm", "M/dd/yyyy h:mm:ss a", "dd MMM yyyy", "dd MMM yyyy ", "E dd MMM yyyy h:mma",  "E dd MMM yyyy", "E dd MMM yyyy ", "E dd MMMMM yyyy HH:mm" , "dd MMM yyyy HH:mm a", "E dd MMM yyyy HH:mm" , "dd MMMMM yyyy HH:mm", "E, MMMMM dd, yyyy HH:mm", "E, MMMMM dd, yyyy", "E, MMMMM dd, yyyy hh:mm a", "EEE dd MMM yyyy h:mm a", "EEE dd MMM yyyy 0h:mm a", "EEE ddMMM yyyy h:mm"} ;
  private static DateUtils du = new DateUtils();
  private static Calendar now = Calendar.getInstance();

  public SolferiasParser(String file) {
    this.file = file;
    if (file != null) {
     fileLower = file.toLowerCase();
    }
    trip = new TripVO();
  }

  public TripVO getTrip() {
    Matcher hotelNames = Pattern.compile("Sede.*.Head\\sOffice.*.\n.*.").matcher(file);
    boolean isHotel = false;
    if (hotelNames.find()) {
      String s = hotelNames.group();
      if (hotelNames.group().toLowerCase().contains("otel") || hotelNames.group().toLowerCase().contains("inn") || hotelNames.group().toLowerCase().contains("resort") || hotelNames.group().toLowerCase().contains("lodge")) {
        isHotel = true;
      }
    }

    if (fileLower.contains("aeroportos")) {
      parseFlight();
    } else if (fileLower.contains("serviços") && file.contains("De") && fileLower.contains("para") && ( fileLower.contains("transfer") || fileLower.contains("voo"))) {
      parseCar();
    } else if (isHotel || fileLower.contains("hotel voucher")) {
      if (fileLower.contains("hotel voucher")) {
        parseHotel2();
      } else {
        parseHotel();
      }
    } else {
      parseActivity();
    }
    trip.setImportSrc(BookingSrc.ImportSrc.TA_SOLFERIAS_PDF);
    trip.setImportTs(System.currentTimeMillis());
    return trip;
  }

  //Used with bilhete aviÃ£o megeve 1_solferias.pdf, bilhete aviÃ£o megeve 2_solferias.pdf
  public void parseFlight() {
    Matcher dates = Pattern.compile("[A-Z]{3}\\s[0-9]{1,2}[A-Za-z]{3}").matcher(file);
    Matcher departureLocationTimes = Pattern.compile("Departure/Partida:\\s[0-9]{2}:[0-9]{2}.*.").matcher(file);
    Matcher arrivalLocationTimes = Pattern.compile("Arrival/Chegada:.*.[0-9]{2}:[0-9]{2}.*.").matcher(file);
    Matcher terminals = Pattern.compile("Terminal:\\s[A-Z0-9]{1,2}").matcher(file);
    Matcher confirmationNumber = Pattern.compile("Confirmation/Código\\sde\\sReserva:\\s[A-Z0-9]{3,6}").matcher(file);
    Matcher flightNumbers = Pattern.compile("Flight/V.*o:.*.").matcher(file);

    Matcher eTicket = Pattern.compile("E\\s-Ticket.*.").matcher(file);
    Matcher eTicket1 = Pattern.compile("Ticket\\sNº\\s:.*.").matcher(file);
    Matcher passgengerName = Pattern.compile("Passenger\\sName\\s:.*.").matcher(file);


    PassengerVO passengerVO = new PassengerVO();
    if (eTicket.find()) {
      String ticket = eTicket.group();
      ticket = ticket.substring(ticket.indexOf("Ticket") + 6).trim();
      passengerVO.seteTicketNumber(ticket);
    } else if (eTicket1.find()) {
      String ticket = eTicket1.group();
      ticket = ticket.substring(ticket.indexOf(": ") + 2).trim();
      passengerVO.seteTicketNumber(ticket);
    }


    if (passgengerName.find()) {
      String name = passgengerName.group();
      name = name.substring(name.indexOf(": ") + 2);
      passengerVO.setLastName(name);
    }


    while(true) {
      FlightVO flight = new FlightVO();

      flight.setDepartureAirport(new AirportVO());
      flight.setArrivalAirport(new AirportVO());

      String date = "";
      if(dates.find())
        date = dates.group() + " " + now.get(Calendar.YEAR);

      if(departureLocationTimes.find()) {
        String[] timeAndLocation = departureLocationTimes.group().split(" ");
        flight.getDepartureAirport().setCity(timeAndLocation[2]);
        flight.getDepartureAirport().setName(timeAndLocation[2]);

        if(!date.equals(""))
          flight.setDepatureTime(getTimestamp(date, timeAndLocation[1]));
      }

      if(arrivalLocationTimes.find()) {
        String[] timeAndLocation = arrivalLocationTimes.group().split(" ");
        flight.getArrivalAirport().setCity(timeAndLocation[4]);
        flight.getArrivalAirport().setName(timeAndLocation[4]);


        if(!date.equals(""))
          flight.setArrivalTime(getTimestamp(date, timeAndLocation[3]));
      }

      if(terminals.find()) {
        flight.getDepartureAirport().setTerminal(terminals.group());
        flight.getArrivalAirport().setTerminal(terminals.group());
      }

      if(confirmationNumber.find()) {
        String[] confirmationNumberArray = confirmationNumber.group().split(" ");
        flight.setReservationNumber(confirmationNumberArray[3]);
      }

      if(flightNumbers.find()) {
        String f = flightNumbers.group();
        f = f.substring(f.indexOf(": ") + 1).trim();

        flight.setCode(f.substring(0,2));
        flight.setNumber(f.substring(2).trim());
      }

      if(dates.hitEnd() && departureLocationTimes.hitEnd() && arrivalLocationTimes.hitEnd() && terminals.hitEnd() &&
         confirmationNumber.hitEnd() && flightNumbers.hitEnd())
        break;

      if (flight.isValid()) {
        if (flight.getReservationNumber() != null && passengerVO.getLastName() != null) {
          flight.setReservationNumber(flight.getReservationNumber() + " " + passengerVO.getLastName());
        }
        if (passengerVO.getLastName() != null) {
          flight.addPassenger(passengerVO);
        }
        //set the code to lisboa airport by default
        if (flight.getArrivalAirport().getCity() != null && flight.getArrivalAirport().getCity().toLowerCase().equals("lisboa")) {
          flight.getArrivalAirport().setCode("LIS");
        }
        if (flight.getDepartureAirport().getCity() != null && flight.getDepartureAirport().getCity().toLowerCase().equals("lisboa")) {
          flight.getDepartureAirport().setCode("LIS");
        }

        trip.addFlight(flight);
      }
    }
  }

  //voucher hotel megeve_solferias.pdf, voucher hotel pdl_solferias.pdf
  public void parseHotel() {
    Matcher checkinDates = Pattern.compile("De/From:\\s[0-9]{2}/[0-9]{2}/[0-9]{4}").matcher(file);
    Matcher checkoutDates = Pattern.compile("A/To:\\s[0-9]{2}/[0-9]{2}/[0-9]{4}").matcher(file);
    Matcher hotelNames = Pattern.compile("Sede\\s/\\sHead\\sOffice:.*.").matcher(file);//Pattern.compile("Sede\\s/\\sHead\\sOffice:\\s[A-Z0-9.,\\s]+").matcher(file);
    Matcher hotelNames1= Pattern.compile("Sede.*.Head\\sOffice.*.\n.*.").matcher(file);

    while(true) {
      HotelVO hotel = new HotelVO();

      if (hotelNames.find())
        hotel.setHotelName(hotelNames.group().replace("Sede / Head Office:", "").trim());

      if (hotel.getHotelName() == null || hotel.getHotelName().trim().length() == 0) {
        if (hotelNames1.find()) {
          String s = hotelNames1.group();
          s = s.replace("Sede / Head Office:", "").replaceAll("\n","").trim();
          hotel.setHotelName(s);
        }
      }

      if(checkinDates.find()) {
        String[] checkinDate = checkinDates.group().split(" ");
        hotel.setCheckin(getTimestamp(checkinDate[1], "15:00"));
      }

      if(checkoutDates.find()) {
        String[] checkoutDate = checkoutDates.group().split(" ");
        hotel.setCheckout(getTimestamp(checkoutDate[1], "12:00"));
      }

      if(checkinDates.hitEnd() && checkoutDates.hitEnd() && hotelNames.hitEnd())
        break;

      if (hotel.isValid()) {
        trip.addHotel(hotel);
      }
    }
  }

  //Used with voucher hotel e entradas_solferias.pdf
  public void parseHotel2() {
    //Matcher dates = Pattern.compile("[0-9]{2}-[0-9]{2}-[0-9]{4}").matcher(file);
    Matcher dates = Pattern.compile("[0-9]{2}(-|/)[0-9]{2}(-|/)[0-9]{4}").matcher(file);
    Matcher confirmationNum = Pattern.compile("NÂº\\s[0-9]{6}").matcher(file);
    Matcher hotelName = Pattern.compile("Hotel:\n.*.\n").matcher(file);

    while(true) {
      HotelVO hotel = new HotelVO();
      if (hotelName.find()) {
        String h = "";
        h = hotelName.group().substring(hotelName.group().indexOf(":") + 2);
        hotel.setHotelName(h.substring(0, h.indexOf("\n")));
      }

      if(dates.find())
        hotel.setCheckin(getTimestamp(dates.group(), "15:00"));

      if(dates.find())
        hotel.setCheckout(getTimestamp(dates.group(), "12:00"));

      if(confirmationNum.find())
        hotel.setConfirmation(confirmationNum.group().substring(3));

      if(hotelName.hitEnd() && dates.hitEnd() && confirmationNum.hitEnd())
        break;

      if (hotel.isValid()) {
        trip.addHotel(hotel);
      }
    }
  }

  //USed with voucher escola de ski_solferias.pdf, voucher forfait 2 pax_solferias.pdf, voucher packs ski_solferias.pdf
  public void parseActivity() {
    Matcher startDates = Pattern.compile("De/From:\\s[0-9]{2}/[0-9]{2}/[0-9]{4}").matcher(file);
    Matcher endDates = Pattern.compile("A/To:\\s[0-9]{2}/[0-9]{2}/[0-9]{4}").matcher(file);
    Matcher locations = Pattern.compile("Lisboa\\s[0-9A-Za-z.,\\s]+1600-207").matcher(file);
    Matcher activityNames = Pattern.compile("Sede\\s/\\sHead\\sOffice:.*.").matcher(file);

    StringBuilder notes = new StringBuilder();
    String [] tokens = file.split("\n");
    for (int i=0; i < tokens.length; i++) {
      String line = tokens[i];
      if (line.contains("Os seguintes serviços")) {

        while (true) {
          i++;
          if (i < tokens.length) {
            String l = tokens[i];
            if (l.contains("Carimbo")) {
              notes.append(l.replaceAll("Carimbo",""));notes.append("\n");
              break;
            } else if (l.contains("Stamp") || l.contains("Pagamento")) {
              break;
            } else {
              notes.append(l);notes.append("\n");
            }
          } else {
            break;
          }
        }
      }
    }

    while(true) {
      ActivityVO activity = new ActivityVO();

      if (activityNames.find()) {
        activity.setName(activityNames.group().replace("Sede / Head Office:", "").trim());
      }
      if (activity.getName() == null) {
        activity.setName("Activity");
      }

      if(startDates.find()) {
        String[] dateArray = startDates.group().split(" ");
        activity.setStartDate(getTimestamp(dateArray[1], "00:00"));
      }

      if(endDates.find()) {
        String[] dateArray = endDates.group().split(" ");
        activity.setEndDate(getTimestamp(dateArray[1], "00:00:01"));
      }

      if(locations.find()) {
        String location = locations.group().replace("Lisboa", "").replace("1600-207", "").trim();
        activity.setStartLocation(location);
        activity.setEndLocation(location);
      }

      if(startDates.hitEnd() && endDates.hitEnd() && locations.hitEnd())
        break;

      if (activity.isValid()) {
        activity.setNote(notes.toString());
        trip.addActivity(activity);
      }
    }
  }

  //Used with voucher transfer in megeve_solferias.pdf, voucher transfer out megeve_solferias.pdf, voucher transfer in pdl_solferias.pdf, voucher transfer out pdl_solferias.pdf
  public void parseCar() {
    Matcher dates = Pattern.compile("Data/Date:\\s[0-9]{2}/[0-9]{2}/[0-9]{4}").matcher(file);
    Matcher pickupLocations = Pattern.compile("De/From:.*.").matcher(file);
    Matcher dropoffLocations = Pattern.compile("Para/To:.*.").matcher(file);


    StringBuilder notes = new StringBuilder();
    String [] tokens = file.split("\n");
    for (int i=0; i < tokens.length; i++) {
      String line = tokens[i];
      if (line.contains("Os seguintes serviços")) {

        while (true) {
          i++;
          if (i < tokens.length) {
            String l = tokens[i];
            if (l.contains("Carimbo")) {
              notes.append(l.replaceAll("Carimbo",""));notes.append("\n");
              break;
            } else if (l.contains("Stamp") || l.contains("Pagamento")) {
              break;
            } else {
              notes.append(l);notes.append("\n");
            }
          } else {
            break;
          }
        }
      }
    }



    while(true) {
      TransportVO car = new TransportVO();
      car.setBookingType(ReservationType.PRIVATE_TRANSFER);

      if(dates.find()) {
        String[] dateArray = dates.group().split(" ");
        car.setPickupDate(getTimestamp(dateArray[1], "00:00:00"));
        car.setDropoffDate(getTimestamp(dateArray[1], "00:00:01"));
      }

      if(pickupLocations.find())
        car.setPickupLocation(pickupLocations.group().replace("De/From:", "").trim());

      if(dropoffLocations.find())
        car.setDropoffLocation(dropoffLocations.group().replace("Para/To:", "").trim());



      car.setNote(notes.toString());

      if(dates.hitEnd() && pickupLocations.hitEnd() && dropoffLocations.hitEnd())
        break;

      if (car.getDropoffDate() == null) {
        car.setDropoffDate(car.getPickupDate());
      }

      if (car.getName() == null) {
        StringBuilder sb = new StringBuilder();
        sb.append("Transfer");

        if (car.getPickupLocation() != null) {
          sb.append(" de ");sb.append(car.getPickupLocation());
        }
        car.setName(sb.toString());
      }
      trip.addTransport(car);
    }
  }

  private Timestamp getTimestamp (String date, String time) {
    try {
      Timestamp t = new Timestamp(du.parseDate(date + " " + time, dateTimeFormat).getTime() );
      return t;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
