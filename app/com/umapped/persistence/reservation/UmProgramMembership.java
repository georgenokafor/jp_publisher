package com.umapped.persistence.reservation;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmProgramMembership
    implements Serializable {
  /**
  * 
  */
  private static final long serialVersionUID = 1L;

  public UmOrganization hostingOrganization;

  /**
   * A unique identifier for the membership.
   */
  public String membershipNumber;

  /**
   * The program providing the membership.
   */
  public String programName;
  public String program;
}