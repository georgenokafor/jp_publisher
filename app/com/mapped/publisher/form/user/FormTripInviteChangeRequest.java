package com.mapped.publisher.form.user;

/**
 * Created by surge on 2016-01-04.
 */
public class FormTripInviteChangeRequest {
  public String inInviteId;
  public String inInviteNote;

  public boolean isValid() {
    return inInviteId != null && inInviteId.length() > 0;
  }
}
