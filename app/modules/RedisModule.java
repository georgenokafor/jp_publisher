package modules;

import com.google.inject.AbstractModule;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.persistence.redis.RedisMgr;

/**
 * Created by surge on 2016-02-02.
 */
public class RedisModule
    extends AbstractModule {

  @Override
  protected void configure() {
    bind(RedisMgr.class).asEagerSingleton();
    requestStaticInjection(CacheMgr.class);
  }
}
