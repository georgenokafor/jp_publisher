package com.umapped.itinerary.analyze.recommendation;

import com.umapped.itinerary.analyze.ItineraryAnalyzeResult;
import com.umapped.itinerary.analyze.TripFeature;
import com.umapped.itinerary.analyze.model.ItineraryItemType;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.segment.SegmentNature;
import com.umapped.itinerary.analyze.segment.TripSegmentAnalyzeResult;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.supplier.ProviderRepository;
import com.umapped.itinerary.analyze.supplier.shoretrip.ShoreTripWebService;
import com.umapped.itinerary.analyze.supplier.travelbound.TravelBoundWebService;
import com.umapped.itinerary.analyze.supplier.wcities.RequestPreset;
import com.umapped.itinerary.analyze.supplier.wcities.WCitiesWebService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * Created by wei on 2017-03-31.
 */
public class RecommendationServiceImpl
    implements RecommendationService {
  private static Logger LOG = LoggerFactory.getLogger(RecommendationServiceImpl.class);
  private ProviderRepository providerRepository;

  @Inject public RecommendationServiceImpl(ProviderRepository providerRepository) {
    this.providerRepository = providerRepository;
  }

  @Override public ItineraryRecommendation recommend(ItineraryAnalyzeResult result) {
    List<TripSegmentRecommendation> recommendations = new ArrayList<>();

    long t1 = System.currentTimeMillis();

    if (result.getSegmentResults() == null) {
      return null;
    }

    List<CompletableFuture<TripSegmentRecommendation>> segments = new ArrayList<>();
    for (TripSegmentAnalyzeResult r : result.getSegmentResults()) {
      if (!CollectionUtils.isEmpty(r.getDecisionResults())) {

        segments.add(CompletableFuture.supplyAsync(() -> getSegmentRecommendation(r)));

      }
    }
    CompletableFuture[] futures = new CompletableFuture[segments.size()];
    segments.toArray(futures);
    CompletableFuture.allOf(futures).thenAccept((ignore) -> {
      for (CompletableFuture<TripSegmentRecommendation> s : segments) {
        recommendations.add(s.join());
      }
    }).join();
    ItineraryRecommendation rec = new ItineraryRecommendation(result.getItineraryId());
    rec.setSegmentRecommendations(recommendations);

    long t2 = System.currentTimeMillis();
    LOG.debug("Search took = " + (t2 - t1));
    return rec;
  }

  private TripSegmentRecommendation getSegmentRecommendation(final TripSegmentAnalyzeResult r) {
    final TripSegmentRecommendation segmentRecommendation = new TripSegmentRecommendation();
    List<CompletableFuture> drs = new ArrayList<>();
    for (DecisionResult dr : r.getDecisionResults()) {
     drs.add(CompletableFuture.runAsync(() ->  getRecommendationForPeriod(segmentRecommendation, dr, r)));
    }
    CompletableFuture[] futures = new CompletableFuture[drs.size()];
    drs.toArray(futures);
    CompletableFuture.allOf(futures).join();
    segmentRecommendation.setSegmentResult(r);
    return segmentRecommendation;
  }

  private void getRecommendationForPeriod(TripSegmentRecommendation segmentRecommendation, DecisionResult dr, TripSegmentAnalyzeResult r) {
    switch (dr.getType()) {
      case Activity:
        segmentRecommendation.addActivity(dr.getStartDate(),
                                          dr.getEndDate(),
                                          providerRepository.getActivityProvider(dr,
                                                                                 r.getSegment(),
                                                                                 null,
                                                                                 r.getFeatureValues())
                                                            .findActivities(dr, null, r.getFeatureValues()));

        segmentRecommendation.addGuide(dr.getStartDate(),
                                       dr.getEndDate(),
                                       providerRepository.getContentProvider(dr,
                                                                             r.getSegment(),
                                                                             null,
                                                                             r.getFeatureValues())
                                                         .findContents(dr, null, r.getFeatureValues()));

        break;
      case Accommodation:
        segmentRecommendation.addHotel(dr.getStartDate(),
                                       dr.getEndDate(),
                                       providerRepository.getAccommodationProvider(dr,
                                                                                   r.getSegment(),
                                                                                   null,
                                                                                   r.getFeatureValues())
                                                         .findAccommondations(dr, null, r.getFeatureValues()));
        break;
    }
  }
}
