package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.common.APPConstants;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.BigFiveTourExtractorPdf;
import com.mapped.publisher.parse.extractor.booking.valueObject.NoteVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.parse.valueObject.DateDetails;
import com.mapped.publisher.parse.valueObject.DayDetails;
import com.mapped.publisher.parse.valueObject.TravelDetails;

import java.io.InputStream;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Created by twong on 2017-02-24.
 */
public class BigFiveTourExtractor extends BookingExtractor {
  protected void initImpl(Map<String, String> params) {

  }


  protected TripVO extractDataImpl(InputStream input)
      throws Exception {
    TripVO tripVO = new TripVO();
    try {
      com.mapped.publisher.parse.extractor.BigFiveTourExtractorPdf b  = new BigFiveTourExtractorPdf();
      TravelDetails td = b.parse(input);
      if (td != null  && td.getDateWiseDetails() != null) {
        for (Date date : td.getDateWiseDetails().keySet()) {
          DateDetails dateDetails = td.getDateWiseDetails().get(date);
          if (dateDetails.getFulText() != null) {
            tripVO.addNoteVO(getNote(dateDetails, date));
          }
          if (dateDetails != null && dateDetails.getActivities() != null && dateDetails.getActivities().size() > 0) {
            for (Date date1 : dateDetails.getActivities().keySet()) {
              DayDetails dayDetails = dateDetails.getActivities().get(date);
              if (dayDetails.getIntro() != null) {
                tripVO.addNoteVO(getNote(dayDetails, date));
              }
            }
          }
        }
      }


    } catch (Exception e) {

    }
    tripVO.setImportSrc(BookingSrc.ImportSrc.BIG_FIVE);
    tripVO.setImportTs(Instant.now().toEpochMilli());
    return tripVO;

  }

  public NoteVO getNote (DateDetails dateDetails, Date date) {
    NoteVO tripNote = new NoteVO();
    tripNote.setTimestamp(new Timestamp(date.getTime()));
    tripNote.setNote(dateDetails.getFulText().replace("\n","<br/>"));
    if (dateDetails.getTitle() != null) {
      tripNote.setName(dateDetails.getTitle());
    } else {
      tripNote.setName("Note");
    }
    return tripNote;
  }

  public NoteVO getNote (DayDetails dateDetails, Date date) {
    NoteVO tripNote = new NoteVO();
    tripNote.setTimestamp(new Timestamp(date.getTime()));
    StringBuffer sb = new StringBuffer();
    if (dateDetails.getIntro() != null) {
      sb.append(dateDetails.getIntro());
      sb.append("\n");
    }
    if (dateDetails.getBody() != null) {
      sb.append(dateDetails.getBody());
    }
    tripNote.setNote(sb.toString().replace("\n","<br/>"));
    if (dateDetails.getTitle() != null) {
      tripNote.setName(dateDetails.getTitle());
    } else {
      tripNote.setName("Note");
    }
    return tripNote;
  }

  @Override
  public ArrayList<ParseError> getParseErrors()
  {
    return null;
  }

  @Override
  public void postProcessingImpl(String tripId, String userId) {
  }


}