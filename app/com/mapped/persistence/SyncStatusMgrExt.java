package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;

import java.sql.*;
import java.util.ArrayList;


public class SyncStatusMgrExt extends  SyncStatusMgr {

   private static final String SELECT_NEWEST_BY_USER = "Select Pk, UserId, DevId, StartTimestamp, EndTimestamp, ClientTimestamp, ServerTimestamp, Status, LastUpdateTimestamp, CreateTimestamp, SyncTimestamp from SyncStatus where userid = ? and createtimestamp in (select max(a.createtimestamp) from syncstatus a where a.userid = ?);";

    public static SyncStatus getNewestByUser (String userId) throws SQLException {
        ResultSet rs = null;
        PreparedStatement prep= null;
        Connection conn= null;
        Statement stat= null;
        try {
            conn = DBConnectionMgr.getConnection4Mobile();
             stat = conn.createStatement();
             prep = conn.prepareStatement(SELECT_NEWEST_BY_USER);
            prep.setString(1, userId);
            prep.setString(2, userId);
            rs = prep.executeQuery();
            ArrayList<SyncStatus> results = SyncStatusMgr.handleResults(rs);
            if (results != null && results.size() > 0)
                return results.get(0);
        } finally {
            if (prep != null)
                prep.close();
            if (stat != null)
                stat.close();
            if (rs != null) {
                rs.close();
            }
            DBConnectionMgr.closeConnection(conn);
        }
        return null;
    }



}
