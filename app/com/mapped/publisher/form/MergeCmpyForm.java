package com.mapped.publisher.form;

/**
 * Created by twong on 2014-05-23.
 */
public class MergeCmpyForm {
  private String inCmpyId;
  private String inTargetCmpyId;
  private String inTerm;

  public String getInCmpyId() {
    return inCmpyId;
  }

  public void setInCmpyId(String inCmpyId) {
    this.inCmpyId = inCmpyId;
  }

  public String getInTargetCmpyId() {
    return inTargetCmpyId;
  }

  public void setInTargetCmpyId(String inTargetCmpyId) {
    this.inTargetCmpyId = inTargetCmpyId;
  }

  public String getInTerm() {
    return inTerm;
  }

  public void setInTerm(String inTerm) {
    this.inTerm = inTerm;
  }
}

