//     Underscore.js 1.7.0
//     http://underscorejs.org
//     (c) 2009-2014 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.
(function(){var n=this,t=n._,r=Array.prototype,e=Object.prototype,u=Function.prototype,i=r.push,a=r.slice,o=r.concat,l=e.toString,c=e.hasOwnProperty,f=Array.isArray,s=Object.keys,p=u.bind,h=function(n){return n instanceof h?n:this instanceof h?void(this._wrapped=n):new h(n)};"undefined"!=typeof exports?("undefined"!=typeof module&&module.exports&&(exports=module.exports=h),exports._=h):n._=h,h.VERSION="1.7.0";var g=function(n,t,r){if(t===void 0)return n;switch(null==r?3:r){case 1:return function(r){return n.call(t,r)};case 2:return function(r,e){return n.call(t,r,e)};case 3:return function(r,e,u){return n.call(t,r,e,u)};case 4:return function(r,e,u,i){return n.call(t,r,e,u,i)}}return function(){return n.apply(t,arguments)}};h.iteratee=function(n,t,r){return null==n?h.identity:h.isFunction(n)?g(n,t,r):h.isObject(n)?h.matches(n):h.property(n)},h.each=h.forEach=function(n,t,r){if(null==n)return n;t=g(t,r);var e,u=n.length;if(u===+u)for(e=0;u>e;e++)t(n[e],e,n);else{var i=h.keys(n);for(e=0,u=i.length;u>e;e++)t(n[i[e]],i[e],n)}return n},h.map=h.collect=function(n,t,r){if(null==n)return[];t=h.iteratee(t,r);for(var e,u=n.length!==+n.length&&h.keys(n),i=(u||n).length,a=Array(i),o=0;i>o;o++)e=u?u[o]:o,a[o]=t(n[e],e,n);return a};var v="Reduce of empty array with no initial value";h.reduce=h.foldl=h.inject=function(n,t,r,e){null==n&&(n=[]),t=g(t,e,4);var u,i=n.length!==+n.length&&h.keys(n),a=(i||n).length,o=0;if(arguments.length<3){if(!a)throw new TypeError(v);r=n[i?i[o++]:o++]}for(;a>o;o++)u=i?i[o]:o,r=t(r,n[u],u,n);return r},h.reduceRight=h.foldr=function(n,t,r,e){null==n&&(n=[]),t=g(t,e,4);var u,i=n.length!==+n.length&&h.keys(n),a=(i||n).length;if(arguments.length<3){if(!a)throw new TypeError(v);r=n[i?i[--a]:--a]}for(;a--;)u=i?i[a]:a,r=t(r,n[u],u,n);return r},h.find=h.detect=function(n,t,r){var e;return t=h.iteratee(t,r),h.some(n,function(n,r,u){return t(n,r,u)?(e=n,!0):void 0}),e},h.filter=h.select=function(n,t,r){var e=[];return null==n?e:(t=h.iteratee(t,r),h.each(n,function(n,r,u){t(n,r,u)&&e.push(n)}),e)},h.reject=function(n,t,r){return h.filter(n,h.negate(h.iteratee(t)),r)},h.every=h.all=function(n,t,r){if(null==n)return!0;t=h.iteratee(t,r);var e,u,i=n.length!==+n.length&&h.keys(n),a=(i||n).length;for(e=0;a>e;e++)if(u=i?i[e]:e,!t(n[u],u,n))return!1;return!0},h.some=h.any=function(n,t,r){if(null==n)return!1;t=h.iteratee(t,r);var e,u,i=n.length!==+n.length&&h.keys(n),a=(i||n).length;for(e=0;a>e;e++)if(u=i?i[e]:e,t(n[u],u,n))return!0;return!1},h.contains=h.include=function(n,t){return null==n?!1:(n.length!==+n.length&&(n=h.values(n)),h.indexOf(n,t)>=0)},h.invoke=function(n,t){var r=a.call(arguments,2),e=h.isFunction(t);return h.map(n,function(n){return(e?t:n[t]).apply(n,r)})},h.pluck=function(n,t){return h.map(n,h.property(t))},h.where=function(n,t){return h.filter(n,h.matches(t))},h.findWhere=function(n,t){return h.find(n,h.matches(t))},h.max=function(n,t,r){var e,u,i=-1/0,a=-1/0;if(null==t&&null!=n){n=n.length===+n.length?n:h.values(n);for(var o=0,l=n.length;l>o;o++)e=n[o],e>i&&(i=e)}else t=h.iteratee(t,r),h.each(n,function(n,r,e){u=t(n,r,e),(u>a||u===-1/0&&i===-1/0)&&(i=n,a=u)});return i},h.min=function(n,t,r){var e,u,i=1/0,a=1/0;if(null==t&&null!=n){n=n.length===+n.length?n:h.values(n);for(var o=0,l=n.length;l>o;o++)e=n[o],i>e&&(i=e)}else t=h.iteratee(t,r),h.each(n,function(n,r,e){u=t(n,r,e),(a>u||1/0===u&&1/0===i)&&(i=n,a=u)});return i},h.shuffle=function(n){for(var t,r=n&&n.length===+n.length?n:h.values(n),e=r.length,u=Array(e),i=0;e>i;i++)t=h.random(0,i),t!==i&&(u[i]=u[t]),u[t]=r[i];return u},h.sample=function(n,t,r){return null==t||r?(n.length!==+n.length&&(n=h.values(n)),n[h.random(n.length-1)]):h.shuffle(n).slice(0,Math.max(0,t))},h.sortBy=function(n,t,r){return t=h.iteratee(t,r),h.pluck(h.map(n,function(n,r,e){return{value:n,index:r,criteria:t(n,r,e)}}).sort(function(n,t){var r=n.criteria,e=t.criteria;if(r!==e){if(r>e||r===void 0)return 1;if(e>r||e===void 0)return-1}return n.index-t.index}),"value")};var m=function(n){return function(t,r,e){var u={};return r=h.iteratee(r,e),h.each(t,function(e,i){var a=r(e,i,t);n(u,e,a)}),u}};h.groupBy=m(function(n,t,r){h.has(n,r)?n[r].push(t):n[r]=[t]}),h.indexBy=m(function(n,t,r){n[r]=t}),h.countBy=m(function(n,t,r){h.has(n,r)?n[r]++:n[r]=1}),h.sortedIndex=function(n,t,r,e){r=h.iteratee(r,e,1);for(var u=r(t),i=0,a=n.length;a>i;){var o=i+a>>>1;r(n[o])<u?i=o+1:a=o}return i},h.toArray=function(n){return n?h.isArray(n)?a.call(n):n.length===+n.length?h.map(n,h.identity):h.values(n):[]},h.size=function(n){return null==n?0:n.length===+n.length?n.length:h.keys(n).length},h.partition=function(n,t,r){t=h.iteratee(t,r);var e=[],u=[];return h.each(n,function(n,r,i){(t(n,r,i)?e:u).push(n)}),[e,u]},h.first=h.head=h.take=function(n,t,r){return null==n?void 0:null==t||r?n[0]:0>t?[]:a.call(n,0,t)},h.initial=function(n,t,r){return a.call(n,0,Math.max(0,n.length-(null==t||r?1:t)))},h.last=function(n,t,r){return null==n?void 0:null==t||r?n[n.length-1]:a.call(n,Math.max(n.length-t,0))},h.rest=h.tail=h.drop=function(n,t,r){return a.call(n,null==t||r?1:t)},h.compact=function(n){return h.filter(n,h.identity)};var y=function(n,t,r,e){if(t&&h.every(n,h.isArray))return o.apply(e,n);for(var u=0,a=n.length;a>u;u++){var l=n[u];h.isArray(l)||h.isArguments(l)?t?i.apply(e,l):y(l,t,r,e):r||e.push(l)}return e};h.flatten=function(n,t){return y(n,t,!1,[])},h.without=function(n){return h.difference(n,a.call(arguments,1))},h.uniq=h.unique=function(n,t,r,e){if(null==n)return[];h.isBoolean(t)||(e=r,r=t,t=!1),null!=r&&(r=h.iteratee(r,e));for(var u=[],i=[],a=0,o=n.length;o>a;a++){var l=n[a];if(t)a&&i===l||u.push(l),i=l;else if(r){var c=r(l,a,n);h.indexOf(i,c)<0&&(i.push(c),u.push(l))}else h.indexOf(u,l)<0&&u.push(l)}return u},h.union=function(){return h.uniq(y(arguments,!0,!0,[]))},h.intersection=function(n){if(null==n)return[];for(var t=[],r=arguments.length,e=0,u=n.length;u>e;e++){var i=n[e];if(!h.contains(t,i)){for(var a=1;r>a&&h.contains(arguments[a],i);a++);a===r&&t.push(i)}}return t},h.difference=function(n){var t=y(a.call(arguments,1),!0,!0,[]);return h.filter(n,function(n){return!h.contains(t,n)})},h.zip=function(n){if(null==n)return[];for(var t=h.max(arguments,"length").length,r=Array(t),e=0;t>e;e++)r[e]=h.pluck(arguments,e);return r},h.object=function(n,t){if(null==n)return{};for(var r={},e=0,u=n.length;u>e;e++)t?r[n[e]]=t[e]:r[n[e][0]]=n[e][1];return r},h.indexOf=function(n,t,r){if(null==n)return-1;var e=0,u=n.length;if(r){if("number"!=typeof r)return e=h.sortedIndex(n,t),n[e]===t?e:-1;e=0>r?Math.max(0,u+r):r}for(;u>e;e++)if(n[e]===t)return e;return-1},h.lastIndexOf=function(n,t,r){if(null==n)return-1;var e=n.length;for("number"==typeof r&&(e=0>r?e+r+1:Math.min(e,r+1));--e>=0;)if(n[e]===t)return e;return-1},h.range=function(n,t,r){arguments.length<=1&&(t=n||0,n=0),r=r||1;for(var e=Math.max(Math.ceil((t-n)/r),0),u=Array(e),i=0;e>i;i++,n+=r)u[i]=n;return u};var d=function(){};h.bind=function(n,t){var r,e;if(p&&n.bind===p)return p.apply(n,a.call(arguments,1));if(!h.isFunction(n))throw new TypeError("Bind must be called on a function");return r=a.call(arguments,2),e=function(){if(!(this instanceof e))return n.apply(t,r.concat(a.call(arguments)));d.prototype=n.prototype;var u=new d;d.prototype=null;var i=n.apply(u,r.concat(a.call(arguments)));return h.isObject(i)?i:u}},h.partial=function(n){var t=a.call(arguments,1);return function(){for(var r=0,e=t.slice(),u=0,i=e.length;i>u;u++)e[u]===h&&(e[u]=arguments[r++]);for(;r<arguments.length;)e.push(arguments[r++]);return n.apply(this,e)}},h.bindAll=function(n){var t,r,e=arguments.length;if(1>=e)throw new Error("bindAll must be passed function names");for(t=1;e>t;t++)r=arguments[t],n[r]=h.bind(n[r],n);return n},h.memoize=function(n,t){var r=function(e){var u=r.cache,i=t?t.apply(this,arguments):e;return h.has(u,i)||(u[i]=n.apply(this,arguments)),u[i]};return r.cache={},r},h.delay=function(n,t){var r=a.call(arguments,2);return setTimeout(function(){return n.apply(null,r)},t)},h.defer=function(n){return h.delay.apply(h,[n,1].concat(a.call(arguments,1)))},h.throttle=function(n,t,r){var e,u,i,a=null,o=0;r||(r={});var l=function(){o=r.leading===!1?0:h.now(),a=null,i=n.apply(e,u),a||(e=u=null)};return function(){var c=h.now();o||r.leading!==!1||(o=c);var f=t-(c-o);return e=this,u=arguments,0>=f||f>t?(clearTimeout(a),a=null,o=c,i=n.apply(e,u),a||(e=u=null)):a||r.trailing===!1||(a=setTimeout(l,f)),i}},h.debounce=function(n,t,r){var e,u,i,a,o,l=function(){var c=h.now()-a;t>c&&c>0?e=setTimeout(l,t-c):(e=null,r||(o=n.apply(i,u),e||(i=u=null)))};return function(){i=this,u=arguments,a=h.now();var c=r&&!e;return e||(e=setTimeout(l,t)),c&&(o=n.apply(i,u),i=u=null),o}},h.wrap=function(n,t){return h.partial(t,n)},h.negate=function(n){return function(){return!n.apply(this,arguments)}},h.compose=function(){var n=arguments,t=n.length-1;return function(){for(var r=t,e=n[t].apply(this,arguments);r--;)e=n[r].call(this,e);return e}},h.after=function(n,t){return function(){return--n<1?t.apply(this,arguments):void 0}},h.before=function(n,t){var r;return function(){return--n>0?r=t.apply(this,arguments):t=null,r}},h.once=h.partial(h.before,2),h.keys=function(n){if(!h.isObject(n))return[];if(s)return s(n);var t=[];for(var r in n)h.has(n,r)&&t.push(r);return t},h.values=function(n){for(var t=h.keys(n),r=t.length,e=Array(r),u=0;r>u;u++)e[u]=n[t[u]];return e},h.pairs=function(n){for(var t=h.keys(n),r=t.length,e=Array(r),u=0;r>u;u++)e[u]=[t[u],n[t[u]]];return e},h.invert=function(n){for(var t={},r=h.keys(n),e=0,u=r.length;u>e;e++)t[n[r[e]]]=r[e];return t},h.functions=h.methods=function(n){var t=[];for(var r in n)h.isFunction(n[r])&&t.push(r);return t.sort()},h.extend=function(n){if(!h.isObject(n))return n;for(var t,r,e=1,u=arguments.length;u>e;e++){t=arguments[e];for(r in t)c.call(t,r)&&(n[r]=t[r])}return n},h.pick=function(n,t,r){var e,u={};if(null==n)return u;if(h.isFunction(t)){t=g(t,r);for(e in n){var i=n[e];t(i,e,n)&&(u[e]=i)}}else{var l=o.apply([],a.call(arguments,1));n=new Object(n);for(var c=0,f=l.length;f>c;c++)e=l[c],e in n&&(u[e]=n[e])}return u},h.omit=function(n,t,r){if(h.isFunction(t))t=h.negate(t);else{var e=h.map(o.apply([],a.call(arguments,1)),String);t=function(n,t){return!h.contains(e,t)}}return h.pick(n,t,r)},h.defaults=function(n){if(!h.isObject(n))return n;for(var t=1,r=arguments.length;r>t;t++){var e=arguments[t];for(var u in e)n[u]===void 0&&(n[u]=e[u])}return n},h.clone=function(n){return h.isObject(n)?h.isArray(n)?n.slice():h.extend({},n):n},h.tap=function(n,t){return t(n),n};var b=function(n,t,r,e){if(n===t)return 0!==n||1/n===1/t;if(null==n||null==t)return n===t;n instanceof h&&(n=n._wrapped),t instanceof h&&(t=t._wrapped);var u=l.call(n);if(u!==l.call(t))return!1;switch(u){case"[object RegExp]":case"[object String]":return""+n==""+t;case"[object Number]":return+n!==+n?+t!==+t:0===+n?1/+n===1/t:+n===+t;case"[object Date]":case"[object Boolean]":return+n===+t}if("object"!=typeof n||"object"!=typeof t)return!1;for(var i=r.length;i--;)if(r[i]===n)return e[i]===t;var a=n.constructor,o=t.constructor;if(a!==o&&"constructor"in n&&"constructor"in t&&!(h.isFunction(a)&&a instanceof a&&h.isFunction(o)&&o instanceof o))return!1;r.push(n),e.push(t);var c,f;if("[object Array]"===u){if(c=n.length,f=c===t.length)for(;c--&&(f=b(n[c],t[c],r,e)););}else{var s,p=h.keys(n);if(c=p.length,f=h.keys(t).length===c)for(;c--&&(s=p[c],f=h.has(t,s)&&b(n[s],t[s],r,e)););}return r.pop(),e.pop(),f};h.isEqual=function(n,t){return b(n,t,[],[])},h.isEmpty=function(n){if(null==n)return!0;if(h.isArray(n)||h.isString(n)||h.isArguments(n))return 0===n.length;for(var t in n)if(h.has(n,t))return!1;return!0},h.isElement=function(n){return!(!n||1!==n.nodeType)},h.isArray=f||function(n){return"[object Array]"===l.call(n)},h.isObject=function(n){var t=typeof n;return"function"===t||"object"===t&&!!n},h.each(["Arguments","Function","String","Number","Date","RegExp"],function(n){h["is"+n]=function(t){return l.call(t)==="[object "+n+"]"}}),h.isArguments(arguments)||(h.isArguments=function(n){return h.has(n,"callee")}),"function"!=typeof/./&&(h.isFunction=function(n){return"function"==typeof n||!1}),h.isFinite=function(n){return isFinite(n)&&!isNaN(parseFloat(n))},h.isNaN=function(n){return h.isNumber(n)&&n!==+n},h.isBoolean=function(n){return n===!0||n===!1||"[object Boolean]"===l.call(n)},h.isNull=function(n){return null===n},h.isUndefined=function(n){return n===void 0},h.has=function(n,t){return null!=n&&c.call(n,t)},h.noConflict=function(){return n._=t,this},h.identity=function(n){return n},h.constant=function(n){return function(){return n}},h.noop=function(){},h.property=function(n){return function(t){return t[n]}},h.matches=function(n){var t=h.pairs(n),r=t.length;return function(n){if(null==n)return!r;n=new Object(n);for(var e=0;r>e;e++){var u=t[e],i=u[0];if(u[1]!==n[i]||!(i in n))return!1}return!0}},h.times=function(n,t,r){var e=Array(Math.max(0,n));t=g(t,r,1);for(var u=0;n>u;u++)e[u]=t(u);return e},h.random=function(n,t){return null==t&&(t=n,n=0),n+Math.floor(Math.random()*(t-n+1))},h.now=Date.now||function(){return(new Date).getTime()};var _={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;"},w=h.invert(_),j=function(n){var t=function(t){return n[t]},r="(?:"+h.keys(n).join("|")+")",e=RegExp(r),u=RegExp(r,"g");return function(n){return n=null==n?"":""+n,e.test(n)?n.replace(u,t):n}};h.escape=j(_),h.unescape=j(w),h.result=function(n,t){if(null==n)return void 0;var r=n[t];return h.isFunction(r)?n[t]():r};var x=0;h.uniqueId=function(n){var t=++x+"";return n?n+t:t},h.templateSettings={evaluate:/<%([\s\S]+?)%>/g,interpolate:/<%=([\s\S]+?)%>/g,escape:/<%-([\s\S]+?)%>/g};var A=/(.)^/,k={"'":"'","\\":"\\","\r":"r","\n":"n","\u2028":"u2028","\u2029":"u2029"},O=/\\|'|\r|\n|\u2028|\u2029/g,F=function(n){return"\\"+k[n]};h.template=function(n,t,r){!t&&r&&(t=r),t=h.defaults({},t,h.templateSettings);var e=RegExp([(t.escape||A).source,(t.interpolate||A).source,(t.evaluate||A).source].join("|")+"|$","g"),u=0,i="__p+='";n.replace(e,function(t,r,e,a,o){return i+=n.slice(u,o).replace(O,F),u=o+t.length,r?i+="'+\n((__t=("+r+"))==null?'':_.escape(__t))+\n'":e?i+="'+\n((__t=("+e+"))==null?'':__t)+\n'":a&&(i+="';\n"+a+"\n__p+='"),t}),i+="';\n",t.variable||(i="with(obj||{}){\n"+i+"}\n"),i="var __t,__p='',__j=Array.prototype.join,"+"print=function(){__p+=__j.call(arguments,'');};\n"+i+"return __p;\n";try{var a=new Function(t.variable||"obj","_",i)}catch(o){throw o.source=i,o}var l=function(n){return a.call(this,n,h)},c=t.variable||"obj";return l.source="function("+c+"){\n"+i+"}",l},h.chain=function(n){var t=h(n);return t._chain=!0,t};var E=function(n){return this._chain?h(n).chain():n};h.mixin=function(n){h.each(h.functions(n),function(t){var r=h[t]=n[t];h.prototype[t]=function(){var n=[this._wrapped];return i.apply(n,arguments),E.call(this,r.apply(h,n))}})},h.mixin(h),h.each(["pop","push","reverse","shift","sort","splice","unshift"],function(n){var t=r[n];h.prototype[n]=function(){var r=this._wrapped;return t.apply(r,arguments),"shift"!==n&&"splice"!==n||0!==r.length||delete r[0],E.call(this,r)}}),h.each(["concat","join","slice"],function(n){var t=r[n];h.prototype[n]=function(){return E.call(this,t.apply(this._wrapped,arguments))}}),h.prototype.value=function(){return this._wrapped},"function"==typeof define&&define.amd&&define("underscore",[],function(){return h})}).call(this);

/* ===========================================================
 * bootstrap-tooltip.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#tooltips
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ===========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* TOOLTIP PUBLIC CLASS DEFINITION
  * =============================== */

  var Tooltip = function (element, options) {
    this.init('tooltip', element, options)
  }

  Tooltip.prototype = {

    constructor: Tooltip

  , init: function (type, element, options) {
      var eventIn
        , eventOut
        , triggers
        , trigger
        , i

      this.type = type
      this.$element = $(element)
      this.options = this.getOptions(options)
      this.enabled = true

      triggers = this.options.trigger.split(' ')

      for (i = triggers.length; i--;) {
        trigger = triggers[i]
        if (trigger == 'click') {
          this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
        } else if (trigger != 'manual') {
          eventIn = trigger == 'hover' ? 'mouseenter' : 'focus'
          eventOut = trigger == 'hover' ? 'mouseleave' : 'blur'
          this.$element.on(eventIn + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
          this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
        }
      }

      this.options.selector ?
        (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
        this.fixTitle()
    }

  , getOptions: function (options) {
      options = $.extend({}, $.fn[this.type].defaults, this.$element.data(), options)

      if (options.delay && typeof options.delay == 'number') {
        options.delay = {
          show: options.delay
        , hide: options.delay
        }
      }

      return options
    }

  , enter: function (e) {
      var defaults = $.fn[this.type].defaults
        , options = {}
        , self

      this._options && $.each(this._options, function (key, value) {
        if (defaults[key] != value) options[key] = value
      }, this)

      self = $(e.currentTarget)[this.type](options).data(this.type)

      if (!self.options.delay || !self.options.delay.show) return self.show()

      clearTimeout(this.timeout)
      self.hoverState = 'in'
      this.timeout = setTimeout(function() {
        if (self.hoverState == 'in') self.show()
      }, self.options.delay.show)
    }

  , leave: function (e) {
      var self = $(e.currentTarget)[this.type](this._options).data(this.type)

      if (this.timeout) clearTimeout(this.timeout)
      if (!self.options.delay || !self.options.delay.hide) return self.hide()

      self.hoverState = 'out'
      this.timeout = setTimeout(function() {
        if (self.hoverState == 'out') self.hide()
      }, self.options.delay.hide)
    }

  , show: function () {
      var $tip
        , pos
        , actualWidth
        , actualHeight
        , placement
        , tp
        , e = $.Event('show')

      if (this.hasContent() && this.enabled) {
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        $tip = this.tip()
        this.setContent()

        if (this.options.animation) {
          $tip.addClass('fade')
        }

        placement = typeof this.options.placement == 'function' ?
          this.options.placement.call(this, $tip[0], this.$element[0]) :
          this.options.placement

        $tip
          .detach()
          .css({ top: 0, left: 0, display: 'block' })

        this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)

        pos = this.getPosition()

        actualWidth = $tip[0].offsetWidth
        actualHeight = $tip[0].offsetHeight

        switch (placement) {
          case 'bottom':
            tp = {top: pos.top + pos.height, left: pos.left + pos.width / 2 - actualWidth / 2}
            break
          case 'top':
            tp = {top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2}
            break
          case 'left':
            tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth}
            break
          case 'right':
            tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width}
            break
        }

        this.applyPlacement(tp, placement)
        this.$element.trigger('shown')
      }
    }

  , applyPlacement: function(offset, placement){
      var $tip = this.tip()
        , width = $tip[0].offsetWidth
        , height = $tip[0].offsetHeight
        , actualWidth
        , actualHeight
        , delta
        , replace

      $tip
        .offset(offset)
        .addClass(placement)
        .addClass('in')

      actualWidth = $tip[0].offsetWidth
      actualHeight = $tip[0].offsetHeight

      if (placement == 'top' && actualHeight != height) {
        offset.top = offset.top + height - actualHeight
        replace = true
      }

      if (placement == 'bottom' || placement == 'top') {
        delta = 0

        if (offset.left < 0){
          delta = offset.left * -2
          offset.left = 0
          $tip.offset(offset)
          actualWidth = $tip[0].offsetWidth
          actualHeight = $tip[0].offsetHeight
        }

        this.replaceArrow(delta - width + actualWidth, actualWidth, 'left')
      } else {
        this.replaceArrow(actualHeight - height, actualHeight, 'top')
      }

      if (replace) $tip.offset(offset)
    }

  , replaceArrow: function(delta, dimension, position){
      this
        .arrow()
        .css(position, delta ? (50 * (1 - delta / dimension) + "%") : '')
    }

  , setContent: function () {
      var $tip = this.tip()
        , title = this.getTitle()

      $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
      $tip.removeClass('fade in top bottom left right')
    }

  , hide: function () {
      var that = this
        , $tip = this.tip()
        , e = $.Event('hide')

      this.$element.trigger(e)
      if (e.isDefaultPrevented()) return

      $tip.removeClass('in')

      function removeWithAnimation() {
        var timeout = setTimeout(function () {
          $tip.off($.support.transition.end).detach()
        }, 500)

        $tip.one($.support.transition.end, function () {
          clearTimeout(timeout)
          $tip.detach()
        })
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        removeWithAnimation() :
        $tip.detach()

      this.$element.trigger('hidden')

      return this
    }

  , fixTitle: function () {
      var $e = this.$element
      if ($e.attr('title') || typeof($e.attr('data-original-title')) != 'string') {
        $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
      }
    }

  , hasContent: function () {
      return this.getTitle()
    }

  , getPosition: function () {
      var el = this.$element[0]
      return $.extend({}, (typeof el.getBoundingClientRect == 'function') ? el.getBoundingClientRect() : {
        width: el.offsetWidth
      , height: el.offsetHeight
      }, this.$element.offset())
    }

  , getTitle: function () {
      var title
        , $e = this.$element
        , o = this.options

      title = $e.attr('data-original-title')
        || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

      return title
    }

  , tip: function () {
      return this.$tip = this.$tip || $(this.options.template)
    }

  , arrow: function(){
      return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }

  , validate: function () {
      if (!this.$element[0].parentNode) {
        this.hide()
        this.$element = null
        this.options = null
      }
    }

  , enable: function () {
      this.enabled = true
    }

  , disable: function () {
      this.enabled = false
    }

  , toggleEnabled: function () {
      this.enabled = !this.enabled
    }

  , toggle: function (e) {
      var self = e ? $(e.currentTarget)[this.type](this._options).data(this.type) : this
      self.tip().hasClass('in') ? self.hide() : self.show()
    }

  , destroy: function () {
      this.hide().$element.off('.' + this.type).removeData(this.type)
    }

  }


 /* TOOLTIP PLUGIN DEFINITION
  * ========================= */

  var old = $.fn.tooltip

  $.fn.tooltip = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tooltip')
        , options = typeof option == 'object' && option
      if (!data) $this.data('tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.tooltip.Constructor = Tooltip

  $.fn.tooltip.defaults = {
    animation: true
  , placement: 'top'
  , selector: false
  , template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
  , trigger: 'hover focus'
  , title: ''
  , delay: 0
  , html: false
  , container: false
  }


 /* TOOLTIP NO CONFLICT
  * =================== */

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(window.jQuery);

// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.

/**
 * marked - a markdown parser
 * Copyright (c) 2011-2014, Christopher Jeffrey. (MIT Licensed)
 * https://github.com/chjj/marked
 */

;(function() {

/**
 * Block-Level Grammar
 */

var block = {
  newline: /^\n+/,
  code: /^( {4}[^\n]+\n*)+/,
  fences: noop,
  hr: /^( *[-*_]){3,} *(?:\n+|$)/,
  heading: /^ *(#{1,6}) *([^\n]+?) *#* *(?:\n+|$)/,
  nptable: noop,
  lheading: /^([^\n]+)\n *(=|-){2,} *(?:\n+|$)/,
  blockquote: /^( *>[^\n]+(\n(?!def)[^\n]+)*\n*)+/,
  list: /^( *)(bull) [\s\S]+?(?:hr|def|\n{2,}(?! )(?!\1bull )\n*|\s*$)/,
  html: /^ *(?:comment *(?:\n|\s*$)|closed *(?:\n{2,}|\s*$)|closing *(?:\n{2,}|\s*$))/,
  def: /^ *\[([^\]]+)\]: *<?([^\s>]+)>?(?: +["(]([^\n]+)[")])? *(?:\n+|$)/,
  table: noop,
  paragraph: /^((?:[^\n]+\n?(?!hr|heading|lheading|blockquote|tag|def))+)\n*/,
  text: /^[^\n]+/
};

block.bullet = /(?:[*+-]|\d+\.)/;
block.item = /^( *)(bull) [^\n]*(?:\n(?!\1bull )[^\n]*)*/;
block.item = replace(block.item, 'gm')
  (/bull/g, block.bullet)
  ();

block.list = replace(block.list)
  (/bull/g, block.bullet)
  ('hr', '\\n+(?=\\1?(?:[-*_] *){3,}(?:\\n+|$))')
  ('def', '\\n+(?=' + block.def.source + ')')
  ();

block.blockquote = replace(block.blockquote)
  ('def', block.def)
  ();

block._tag = '(?!(?:'
  + 'a|em|strong|small|s|cite|q|dfn|abbr|data|time|code'
  + '|var|samp|kbd|sub|sup|i|b|u|mark|ruby|rt|rp|bdi|bdo'
  + '|span|br|wbr|ins|del|img)\\b)\\w+(?!:/|[^\\w\\s@]*@)\\b';

block.html = replace(block.html)
  ('comment', /<!--[\s\S]*?-->/)
  ('closed', /<(tag)[\s\S]+?<\/\1>/)
  ('closing', /<tag(?:"[^"]*"|'[^']*'|[^'">])*?>/)
  (/tag/g, block._tag)
  ();

block.paragraph = replace(block.paragraph)
  ('hr', block.hr)
  ('heading', block.heading)
  ('lheading', block.lheading)
  ('blockquote', block.blockquote)
  ('tag', '<' + block._tag)
  ('def', block.def)
  ();

/**
 * Normal Block Grammar
 */

block.normal = merge({}, block);

/**
 * GFM Block Grammar
 */

block.gfm = merge({}, block.normal, {
  fences: /^ *(`{3,}|~{3,})[ \.]*(\S+)? *\n([\s\S]*?)\s*\1 *(?:\n+|$)/,
  paragraph: /^/,
  heading: /^ *(#{1,6}) +([^\n]+?) *#* *(?:\n+|$)/
});

block.gfm.paragraph = replace(block.paragraph)
  ('(?!', '(?!'
    + block.gfm.fences.source.replace('\\1', '\\2') + '|'
    + block.list.source.replace('\\1', '\\3') + '|')
  ();

/**
 * GFM + Tables Block Grammar
 */

block.tables = merge({}, block.gfm, {
  nptable: /^ *(\S.*\|.*)\n *([-:]+ *\|[-| :]*)\n((?:.*\|.*(?:\n|$))*)\n*/,
  table: /^ *\|(.+)\n *\|( *[-:]+[-| :]*)\n((?: *\|.*(?:\n|$))*)\n*/
});

/**
 * Block Lexer
 */

function Lexer(options) {
  this.tokens = [];
  this.tokens.links = {};
  this.options = options || marked.defaults;
  this.rules = block.normal;

  if (this.options.gfm) {
    if (this.options.tables) {
      this.rules = block.tables;
    } else {
      this.rules = block.gfm;
    }
  }
}

/**
 * Expose Block Rules
 */

Lexer.rules = block;

/**
 * Static Lex Method
 */

Lexer.lex = function(src, options) {
  var lexer = new Lexer(options);
  return lexer.lex(src);
};

/**
 * Preprocessing
 */

Lexer.prototype.lex = function(src) {
  src = src
    .replace(/\r\n|\r/g, '\n')
    .replace(/\t/g, '    ')
    .replace(/\u00a0/g, ' ')
    .replace(/\u2424/g, '\n');

  return this.token(src, true);
};

/**
 * Lexing
 */

Lexer.prototype.token = function(src, top, bq) {
  var src = src.replace(/^ +$/gm, '')
    , next
    , loose
    , cap
    , bull
    , b
    , item
    , space
    , i
    , l;

  while (src) {
    // newline
    if (cap = this.rules.newline.exec(src)) {
      src = src.substring(cap[0].length);
      if (cap[0].length > 1) {
        this.tokens.push({
          type: 'space'
        });
      }
    }

    // code
    if (cap = this.rules.code.exec(src)) {
      src = src.substring(cap[0].length);
      cap = cap[0].replace(/^ {4}/gm, '');
      this.tokens.push({
        type: 'code',
        text: !this.options.pedantic
          ? cap.replace(/\n+$/, '')
          : cap
      });
      continue;
    }

    // fences (gfm)
    if (cap = this.rules.fences.exec(src)) {
      src = src.substring(cap[0].length);
      this.tokens.push({
        type: 'code',
        lang: cap[2],
        text: cap[3] || ''
      });
      continue;
    }

    // heading
    if (cap = this.rules.heading.exec(src)) {
      src = src.substring(cap[0].length);
      this.tokens.push({
        type: 'heading',
        depth: cap[1].length,
        text: cap[2]
      });
      continue;
    }

    // table no leading pipe (gfm)
    if (top && (cap = this.rules.nptable.exec(src))) {
      src = src.substring(cap[0].length);

      item = {
        type: 'table',
        header: cap[1].replace(/^ *| *\| *$/g, '').split(/ *\| */),
        align: cap[2].replace(/^ *|\| *$/g, '').split(/ *\| */),
        cells: cap[3].replace(/\n$/, '').split('\n')
      };

      for (i = 0; i < item.align.length; i++) {
        if (/^ *-+: *$/.test(item.align[i])) {
          item.align[i] = 'right';
        } else if (/^ *:-+: *$/.test(item.align[i])) {
          item.align[i] = 'center';
        } else if (/^ *:-+ *$/.test(item.align[i])) {
          item.align[i] = 'left';
        } else {
          item.align[i] = null;
        }
      }

      for (i = 0; i < item.cells.length; i++) {
        item.cells[i] = item.cells[i].split(/ *\| */);
      }

      this.tokens.push(item);

      continue;
    }

    // lheading
    if (cap = this.rules.lheading.exec(src)) {
      src = src.substring(cap[0].length);
      this.tokens.push({
        type: 'heading',
        depth: cap[2] === '=' ? 1 : 2,
        text: cap[1]
      });
      continue;
    }

    // hr
    if (cap = this.rules.hr.exec(src)) {
      src = src.substring(cap[0].length);
      this.tokens.push({
        type: 'hr'
      });
      continue;
    }

    // blockquote
    if (cap = this.rules.blockquote.exec(src)) {
      src = src.substring(cap[0].length);

      this.tokens.push({
        type: 'blockquote_start'
      });

      cap = cap[0].replace(/^ *> ?/gm, '');

      // Pass `top` to keep the current
      // "toplevel" state. This is exactly
      // how markdown.pl works.
      this.token(cap, top, true);

      this.tokens.push({
        type: 'blockquote_end'
      });

      continue;
    }

    // list
    if (cap = this.rules.list.exec(src)) {
      src = src.substring(cap[0].length);
      bull = cap[2];

      this.tokens.push({
        type: 'list_start',
        ordered: bull.length > 1
      });

      // Get each top-level item.
      cap = cap[0].match(this.rules.item);

      next = false;
      l = cap.length;
      i = 0;

      for (; i < l; i++) {
        item = cap[i];

        // Remove the list item's bullet
        // so it is seen as the next token.
        space = item.length;
        item = item.replace(/^ *([*+-]|\d+\.) +/, '');

        // Outdent whatever the
        // list item contains. Hacky.
        if (~item.indexOf('\n ')) {
          space -= item.length;
          item = !this.options.pedantic
            ? item.replace(new RegExp('^ {1,' + space + '}', 'gm'), '')
            : item.replace(/^ {1,4}/gm, '');
        }

        // Determine whether the next list item belongs here.
        // Backpedal if it does not belong in this list.
        if (this.options.smartLists && i !== l - 1) {
          b = block.bullet.exec(cap[i + 1])[0];
          if (bull !== b && !(bull.length > 1 && b.length > 1)) {
            src = cap.slice(i + 1).join('\n') + src;
            i = l - 1;
          }
        }

        // Determine whether item is loose or not.
        // Use: /(^|\n)(?! )[^\n]+\n\n(?!\s*$)/
        // for discount behavior.
        loose = next || /\n\n(?!\s*$)/.test(item);
        if (i !== l - 1) {
          next = item.charAt(item.length - 1) === '\n';
          if (!loose) loose = next;
        }

        this.tokens.push({
          type: loose
            ? 'loose_item_start'
            : 'list_item_start'
        });

        // Recurse.
        this.token(item, false, bq);

        this.tokens.push({
          type: 'list_item_end'
        });
      }

      this.tokens.push({
        type: 'list_end'
      });

      continue;
    }

    // html
    if (cap = this.rules.html.exec(src)) {
      src = src.substring(cap[0].length);
      this.tokens.push({
        type: this.options.sanitize
          ? 'paragraph'
          : 'html',
        pre: !this.options.sanitizer
          && (cap[1] === 'pre' || cap[1] === 'script' || cap[1] === 'style'),
        text: cap[0]
      });
      continue;
    }

    // def
    if ((!bq && top) && (cap = this.rules.def.exec(src))) {
      src = src.substring(cap[0].length);
      this.tokens.links[cap[1].toLowerCase()] = {
        href: cap[2],
        title: cap[3]
      };
      continue;
    }

    // table (gfm)
    if (top && (cap = this.rules.table.exec(src))) {
      src = src.substring(cap[0].length);

      item = {
        type: 'table',
        header: cap[1].replace(/^ *| *\| *$/g, '').split(/ *\| */),
        align: cap[2].replace(/^ *|\| *$/g, '').split(/ *\| */),
        cells: cap[3].replace(/(?: *\| *)?\n$/, '').split('\n')
      };

      for (i = 0; i < item.align.length; i++) {
        if (/^ *-+: *$/.test(item.align[i])) {
          item.align[i] = 'right';
        } else if (/^ *:-+: *$/.test(item.align[i])) {
          item.align[i] = 'center';
        } else if (/^ *:-+ *$/.test(item.align[i])) {
          item.align[i] = 'left';
        } else {
          item.align[i] = null;
        }
      }

      for (i = 0; i < item.cells.length; i++) {
        item.cells[i] = item.cells[i]
          .replace(/^ *\| *| *\| *$/g, '')
          .split(/ *\| */);
      }

      this.tokens.push(item);

      continue;
    }

    // top-level paragraph
    if (top && (cap = this.rules.paragraph.exec(src))) {
      src = src.substring(cap[0].length);
      this.tokens.push({
        type: 'paragraph',
        text: cap[1].charAt(cap[1].length - 1) === '\n'
          ? cap[1].slice(0, -1)
          : cap[1]
      });
      continue;
    }

    // text
    if (cap = this.rules.text.exec(src)) {
      // Top-level should never reach here.
      src = src.substring(cap[0].length);
      this.tokens.push({
        type: 'text',
        text: cap[0]
      });
      continue;
    }

    if (src) {
      throw new
        Error('Infinite loop on byte: ' + src.charCodeAt(0));
    }
  }

  return this.tokens;
};

/**
 * Inline-Level Grammar
 */

var inline = {
  escape: /^\\([\\`*{}\[\]()#+\-.!_>])/,
  autolink: /^<([^ >]+(@|:\/)[^ >]+)>/,
  url: noop,
  tag: /^<!--[\s\S]*?-->|^<\/?\w+(?:"[^"]*"|'[^']*'|[^'">])*?>/,
  link: /^!?\[(inside)\]\(href\)/,
  reflink: /^!?\[(inside)\]\s*\[([^\]]*)\]/,
  nolink: /^!?\[((?:\[[^\]]*\]|[^\[\]])*)\]/,
  strong: /^__([\s\S]+?)__(?!_)|^\*\*([\s\S]+?)\*\*(?!\*)/,
  em: /^\b_((?:[^_]|__)+?)_\b|^\*((?:\*\*|[\s\S])+?)\*(?!\*)/,
  code: /^(`+)\s*([\s\S]*?[^`])\s*\1(?!`)/,
  br: /^ {2,}\n(?!\s*$)/,
  del: noop,
  text: /^[\s\S]+?(?=[\\<!\[_*`]| {2,}\n|$)/
};

inline._inside = /(?:\[[^\]]*\]|[^\[\]]|\](?=[^\[]*\]))*/;
inline._href = /\s*<?([\s\S]*?)>?(?:\s+['"]([\s\S]*?)['"])?\s*/;

inline.link = replace(inline.link)
  ('inside', inline._inside)
  ('href', inline._href)
  ();

inline.reflink = replace(inline.reflink)
  ('inside', inline._inside)
  ();

/**
 * Normal Inline Grammar
 */

inline.normal = merge({}, inline);

/**
 * Pedantic Inline Grammar
 */

inline.pedantic = merge({}, inline.normal, {
  strong: /^__(?=\S)([\s\S]*?\S)__(?!_)|^\*\*(?=\S)([\s\S]*?\S)\*\*(?!\*)/,
  em: /^_(?=\S)([\s\S]*?\S)_(?!_)|^\*(?=\S)([\s\S]*?\S)\*(?!\*)/
});

/**
 * GFM Inline Grammar
 */

inline.gfm = merge({}, inline.normal, {
  escape: replace(inline.escape)('])', '~|])')(),
  url: /^(https?:\/\/[^\s<]+[^<.,:;"')\]\s])/,
  del: /^~~(?=\S)([\s\S]*?\S)~~/,
  text: replace(inline.text)
    (']|', '~]|')
    ('|', '|https?://|')
    ()
});

/**
 * GFM + Line Breaks Inline Grammar
 */

inline.breaks = merge({}, inline.gfm, {
  br: replace(inline.br)('{2,}', '*')(),
  text: replace(inline.gfm.text)('{2,}', '*')()
});

/**
 * Inline Lexer & Compiler
 */

function InlineLexer(links, options) {
  this.options = options || marked.defaults;
  this.links = links;
  this.rules = inline.normal;
  this.renderer = this.options.renderer || new Renderer;
  this.renderer.options = this.options;

  if (!this.links) {
    throw new
      Error('Tokens array requires a `links` property.');
  }

  if (this.options.gfm) {
    if (this.options.breaks) {
      this.rules = inline.breaks;
    } else {
      this.rules = inline.gfm;
    }
  } else if (this.options.pedantic) {
    this.rules = inline.pedantic;
  }
}

/**
 * Expose Inline Rules
 */

InlineLexer.rules = inline;

/**
 * Static Lexing/Compiling Method
 */

InlineLexer.output = function(src, links, options) {
  var inline = new InlineLexer(links, options);
  return inline.output(src);
};

/**
 * Lexing/Compiling
 */

InlineLexer.prototype.output = function(src) {
  var out = ''
    , link
    , text
    , href
    , cap;

  while (src) {
    // escape
    if (cap = this.rules.escape.exec(src)) {
      src = src.substring(cap[0].length);
      out += cap[1];
      continue;
    }

    // autolink
    if (cap = this.rules.autolink.exec(src)) {
      src = src.substring(cap[0].length);
      if (cap[2] === '@') {
        text = cap[1].charAt(6) === ':'
          ? this.mangle(cap[1].substring(7))
          : this.mangle(cap[1]);
        href = this.mangle('mailto:') + text;
      } else {
        text = escape(cap[1]);
        href = text;
      }
      out += this.renderer.link(href, null, text);
      continue;
    }

    // url (gfm)
    if (!this.inLink && (cap = this.rules.url.exec(src))) {
      src = src.substring(cap[0].length);
      text = escape(cap[1]);
      href = text;
      out += this.renderer.link(href, null, text);
      continue;
    }

    // tag
    if (cap = this.rules.tag.exec(src)) {
      if (!this.inLink && /^<a /i.test(cap[0])) {
        this.inLink = true;
      } else if (this.inLink && /^<\/a>/i.test(cap[0])) {
        this.inLink = false;
      }
      src = src.substring(cap[0].length);
      out += this.options.sanitize
        ? this.options.sanitizer
          ? this.options.sanitizer(cap[0])
          : escape(cap[0])
        : cap[0]
      continue;
    }

    // link
    if (cap = this.rules.link.exec(src)) {
      src = src.substring(cap[0].length);
      this.inLink = true;
      out += this.outputLink(cap, {
        href: cap[2],
        title: cap[3]
      });
      this.inLink = false;
      continue;
    }

    // reflink, nolink
    if ((cap = this.rules.reflink.exec(src))
        || (cap = this.rules.nolink.exec(src))) {
      src = src.substring(cap[0].length);
      link = (cap[2] || cap[1]).replace(/\s+/g, ' ');
      link = this.links[link.toLowerCase()];
      if (!link || !link.href) {
        out += cap[0].charAt(0);
        src = cap[0].substring(1) + src;
        continue;
      }
      this.inLink = true;
      out += this.outputLink(cap, link);
      this.inLink = false;
      continue;
    }

    // strong
    if (cap = this.rules.strong.exec(src)) {
      src = src.substring(cap[0].length);
      out += this.renderer.strong(this.output(cap[2] || cap[1]));
      continue;
    }

    // em
    if (cap = this.rules.em.exec(src)) {
      src = src.substring(cap[0].length);
      out += this.renderer.em(this.output(cap[2] || cap[1]));
      continue;
    }

    // code
    if (cap = this.rules.code.exec(src)) {
      src = src.substring(cap[0].length);
      out += this.renderer.codespan(escape(cap[2], true));
      continue;
    }

    // br
    if (cap = this.rules.br.exec(src)) {
      src = src.substring(cap[0].length);
      out += this.renderer.br();
      continue;
    }

    // del (gfm)
    if (cap = this.rules.del.exec(src)) {
      src = src.substring(cap[0].length);
      out += this.renderer.del(this.output(cap[1]));
      continue;
    }

    // text
    if (cap = this.rules.text.exec(src)) {
      src = src.substring(cap[0].length);
      out += this.renderer.text(escape(this.smartypants(cap[0])));
      continue;
    }

    if (src) {
      throw new
        Error('Infinite loop on byte: ' + src.charCodeAt(0));
    }
  }

  return out;
};

/**
 * Compile Link
 */

InlineLexer.prototype.outputLink = function(cap, link) {
  var href = escape(link.href)
    , title = link.title ? escape(link.title) : null;

  return cap[0].charAt(0) !== '!'
    ? this.renderer.link(href, title, this.output(cap[1]))
    : this.renderer.image(href, title, escape(cap[1]));
};

/**
 * Smartypants Transformations
 */

InlineLexer.prototype.smartypants = function(text) {
  if (!this.options.smartypants) return text;
  return text
    // em-dashes
    .replace(/---/g, '\u2014')
    // en-dashes
    .replace(/--/g, '\u2013')
    // opening singles
    .replace(/(^|[-\u2014/(\[{"\s])'/g, '$1\u2018')
    // closing singles & apostrophes
    .replace(/'/g, '\u2019')
    // opening doubles
    .replace(/(^|[-\u2014/(\[{\u2018\s])"/g, '$1\u201c')
    // closing doubles
    .replace(/"/g, '\u201d')
    // ellipses
    .replace(/\.{3}/g, '\u2026');
};

/**
 * Mangle Links
 */

InlineLexer.prototype.mangle = function(text) {
  if (!this.options.mangle) return text;
  var out = ''
    , l = text.length
    , i = 0
    , ch;

  for (; i < l; i++) {
    ch = text.charCodeAt(i);
    if (Math.random() > 0.5) {
      ch = 'x' + ch.toString(16);
    }
    out += '&#' + ch + ';';
  }

  return out;
};

/**
 * Renderer
 */

function Renderer(options) {
  this.options = options || {};
}

Renderer.prototype.code = function(code, lang, escaped) {
  if (this.options.highlight) {
    var out = this.options.highlight(code, lang);
    if (out != null && out !== code) {
      escaped = true;
      code = out;
    }
  }

  if (!lang) {
    return '<pre><code>'
      + (escaped ? code : escape(code, true))
      + '\n</code></pre>';
  }

  return '<pre><code class="'
    + this.options.langPrefix
    + escape(lang, true)
    + '">'
    + (escaped ? code : escape(code, true))
    + '\n</code></pre>\n';
};

Renderer.prototype.blockquote = function(quote) {
  return '<blockquote>\n' + quote + '</blockquote>\n';
};

Renderer.prototype.html = function(html) {
  return html;
};

Renderer.prototype.heading = function(text, level, raw) {
  return '<h'
    + level
    + ' id="'
    + this.options.headerPrefix
    + raw.toLowerCase().replace(/[^\w]+/g, '-')
    + '">'
    + text
    + '</h'
    + level
    + '>\n';
};

Renderer.prototype.hr = function() {
  return this.options.xhtml ? '<hr/>\n' : '<hr>\n';
};

Renderer.prototype.list = function(body, ordered) {
  var type = ordered ? 'ol' : 'ul';
  return '<' + type + '>\n' + body + '</' + type + '>\n';
};

Renderer.prototype.listitem = function(text) {
  return '<li>' + text + '</li>\n';
};

Renderer.prototype.paragraph = function(text) {
  return '<p>' + text + '</p>\n';
};

Renderer.prototype.table = function(header, body) {
  return '<table>\n'
    + '<thead>\n'
    + header
    + '</thead>\n'
    + '<tbody>\n'
    + body
    + '</tbody>\n'
    + '</table>\n';
};

Renderer.prototype.tablerow = function(content) {
  return '<tr>\n' + content + '</tr>\n';
};

Renderer.prototype.tablecell = function(content, flags) {
  var type = flags.header ? 'th' : 'td';
  var tag = flags.align
    ? '<' + type + ' style="text-align:' + flags.align + '">'
    : '<' + type + '>';
  return tag + content + '</' + type + '>\n';
};

// span level renderer
Renderer.prototype.strong = function(text) {
  return '<strong>' + text + '</strong>';
};

Renderer.prototype.em = function(text) {
  return '<em>' + text + '</em>';
};

Renderer.prototype.codespan = function(text) {
  return '<code>' + text + '</code>';
};

Renderer.prototype.br = function() {
  return this.options.xhtml ? '<br/>' : '<br>';
};

Renderer.prototype.del = function(text) {
  return '<del>' + text + '</del>';
};

Renderer.prototype.link = function(href, title, text) {
  if (this.options.sanitize) {
    try {
      var prot = decodeURIComponent(unescape(href))
        .replace(/[^\w:]/g, '')
        .toLowerCase();
    } catch (e) {
      return '';
    }
    if (prot.indexOf('javascript:') === 0 || prot.indexOf('vbscript:') === 0) {
      return '';
    }
  }
  var out = '<a href="' + href + '"';
  if (title) {
    out += ' title="' + title + '"';
  }
  out += '>' + text + '</a>';
  return out;
};

Renderer.prototype.image = function(href, title, text) {
  var out = '<img src="' + href + '" alt="' + text + '"';
  if (title) {
    out += ' title="' + title + '"';
  }
  out += this.options.xhtml ? '/>' : '>';
  return out;
};

Renderer.prototype.text = function(text) {
  return text;
};

/**
 * Parsing & Compiling
 */

function Parser(options) {
  this.tokens = [];
  this.token = null;
  this.options = options || marked.defaults;
  this.options.renderer = this.options.renderer || new Renderer;
  this.renderer = this.options.renderer;
  this.renderer.options = this.options;
}

/**
 * Static Parse Method
 */

Parser.parse = function(src, options, renderer) {
  var parser = new Parser(options, renderer);
  return parser.parse(src);
};

/**
 * Parse Loop
 */

Parser.prototype.parse = function(src) {
  this.inline = new InlineLexer(src.links, this.options, this.renderer);
  this.tokens = src.reverse();

  var out = '';
  while (this.next()) {
    out += this.tok();
  }

  return out;
};

/**
 * Next Token
 */

Parser.prototype.next = function() {
  return this.token = this.tokens.pop();
};

/**
 * Preview Next Token
 */

Parser.prototype.peek = function() {
  return this.tokens[this.tokens.length - 1] || 0;
};

/**
 * Parse Text Tokens
 */

Parser.prototype.parseText = function() {
  var body = this.token.text;

  while (this.peek().type === 'text') {
    body += '\n' + this.next().text;
  }

  return this.inline.output(body);
};

/**
 * Parse Current Token
 */

Parser.prototype.tok = function() {
  switch (this.token.type) {
    case 'space': {
      return '';
    }
    case 'hr': {
      return this.renderer.hr();
    }
    case 'heading': {
      return this.renderer.heading(
        this.inline.output(this.token.text),
        this.token.depth,
        this.token.text);
    }
    case 'code': {
      return this.renderer.code(this.token.text,
        this.token.lang,
        this.token.escaped);
    }
    case 'table': {
      var header = ''
        , body = ''
        , i
        , row
        , cell
        , flags
        , j;

      // header
      cell = '';
      for (i = 0; i < this.token.header.length; i++) {
        flags = { header: true, align: this.token.align[i] };
        cell += this.renderer.tablecell(
          this.inline.output(this.token.header[i]),
          { header: true, align: this.token.align[i] }
        );
      }
      header += this.renderer.tablerow(cell);

      for (i = 0; i < this.token.cells.length; i++) {
        row = this.token.cells[i];

        cell = '';
        for (j = 0; j < row.length; j++) {
          cell += this.renderer.tablecell(
            this.inline.output(row[j]),
            { header: false, align: this.token.align[j] }
          );
        }

        body += this.renderer.tablerow(cell);
      }
      return this.renderer.table(header, body);
    }
    case 'blockquote_start': {
      var body = '';

      while (this.next().type !== 'blockquote_end') {
        body += this.tok();
      }

      return this.renderer.blockquote(body);
    }
    case 'list_start': {
      var body = ''
        , ordered = this.token.ordered;

      while (this.next().type !== 'list_end') {
        body += this.tok();
      }

      return this.renderer.list(body, ordered);
    }
    case 'list_item_start': {
      var body = '';

      while (this.next().type !== 'list_item_end') {
        body += this.token.type === 'text'
          ? this.parseText()
          : this.tok();
      }

      return this.renderer.listitem(body);
    }
    case 'loose_item_start': {
      var body = '';

      while (this.next().type !== 'list_item_end') {
        body += this.tok();
      }

      return this.renderer.listitem(body);
    }
    case 'html': {
      var html = !this.token.pre && !this.options.pedantic
        ? this.inline.output(this.token.text)
        : this.token.text;
      return this.renderer.html(html);
    }
    case 'paragraph': {
      return this.renderer.paragraph(this.inline.output(this.token.text));
    }
    case 'text': {
      return this.renderer.paragraph(this.parseText());
    }
  }
};

/**
 * Helpers
 */

function escape(html, encode) {
  return html
    .replace(!encode ? /&(?!#?\w+;)/g : /&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#39;');
}

function unescape(html) {
  return html.replace(/&([#\w]+);/g, function(_, n) {
    n = n.toLowerCase();
    if (n === 'colon') return ':';
    if (n.charAt(0) === '#') {
      return n.charAt(1) === 'x'
        ? String.fromCharCode(parseInt(n.substring(2), 16))
        : String.fromCharCode(+n.substring(1));
    }
    return '';
  });
}

function replace(regex, opt) {
  regex = regex.source;
  opt = opt || '';
  return function self(name, val) {
    if (!name) return new RegExp(regex, opt);
    val = val.source || val;
    val = val.replace(/(^|[^\[])\^/g, '$1');
    regex = regex.replace(name, val);
    return self;
  };
}

function noop() {}
noop.exec = noop;

function merge(obj) {
  var i = 1
    , target
    , key;

  for (; i < arguments.length; i++) {
    target = arguments[i];
    for (key in target) {
      if (Object.prototype.hasOwnProperty.call(target, key)) {
        obj[key] = target[key];
      }
    }
  }

  return obj;
}


/**
 * Marked
 */

function marked(src, opt, callback) {
  if (callback || typeof opt === 'function') {
    if (!callback) {
      callback = opt;
      opt = null;
    }

    opt = merge({}, marked.defaults, opt || {});

    var highlight = opt.highlight
      , tokens
      , pending
      , i = 0;

    try {
      tokens = Lexer.lex(src, opt)
    } catch (e) {
      return callback(e);
    }

    pending = tokens.length;

    var done = function(err) {
      if (err) {
        opt.highlight = highlight;
        return callback(err);
      }

      var out;

      try {
        out = Parser.parse(tokens, opt);
      } catch (e) {
        err = e;
      }

      opt.highlight = highlight;

      return err
        ? callback(err)
        : callback(null, out);
    };

    if (!highlight || highlight.length < 3) {
      return done();
    }

    delete opt.highlight;

    if (!pending) return done();

    for (; i < tokens.length; i++) {
      (function(token) {
        if (token.type !== 'code') {
          return --pending || done();
        }
        return highlight(token.text, token.lang, function(err, code) {
          if (err) return done(err);
          if (code == null || code === token.text) {
            return --pending || done();
          }
          token.text = code;
          token.escaped = true;
          --pending || done();
        });
      })(tokens[i]);
    }

    return;
  }
  try {
    if (opt) opt = merge({}, marked.defaults, opt);
    return Parser.parse(Lexer.lex(src, opt), opt);
  } catch (e) {
    e.message += '\nPlease report this to https://github.com/chjj/marked.';
    if ((opt || marked.defaults).silent) {
      return '<p>An error occured:</p><pre>'
        + escape(e.message + '', true)
        + '</pre>';
    }
    throw e;
  }
}

/**
 * Options
 */

marked.options =
marked.setOptions = function(opt) {
  merge(marked.defaults, opt);
  return marked;
};

marked.defaults = {
  gfm: true,
  tables: true,
  breaks: false,
  pedantic: false,
  sanitize: false,
  sanitizer: null,
  mangle: true,
  smartLists: false,
  silent: false,
  highlight: null,
  langPrefix: 'lang-',
  smartypants: false,
  headerPrefix: '',
  renderer: new Renderer,
  xhtml: false
};

/**
 * Expose
 */

marked.Parser = Parser;
marked.parser = Parser.parse;

marked.Renderer = Renderer;

marked.Lexer = Lexer;
marked.lexer = Lexer.lex;

marked.InlineLexer = InlineLexer;
marked.inlineLexer = InlineLexer.output;

marked.parse = marked;

if (typeof module !== 'undefined' && typeof exports === 'object') {
  module.exports = marked;
} else if (typeof define === 'function' && define.amd) {
  define(function() { return marked; });
} else {
  this.marked = marked;
}

}).call(function() {
  return this || (typeof window !== 'undefined' ? window : global);
}());

this["FirechatDefaultTemplates"] = this["FirechatDefaultTemplates"] || {};

this["FirechatDefaultTemplates"]["templates/card-context-menu.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape, __j = Array.prototype.join;function print() { __p += __j.call(arguments, '') }with (obj) {__p += '<div data-toggle=\'firechat-contextmenu\' class=\'contextmenu\' data-message-id=\'' +__e( id ) +'\'>\n<ul>\n<li><a href=\'#!\' data-event=\'firechat-user-warn\'>Warn User</a></li>\n'; if (allowKick) { ;__p += '\n<li><a href=\'#!\' data-event=\'firechat-user-kick\'>Kick User</a></li>\n'; } ;__p += '\n<li><a href=\'#!\' data-event=\'firechat-user-suspend-hour\'>Suspend User (1 Hour)</a></li>\n<li><a href=\'#!\' data-event=\'firechat-user-suspend-day\'>Suspend User (1 Day)</a></li>\n<li><a href=\'#!\' data-event=\'firechat-message-delete\'>Delete Message</a></li>\n</ul>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/card.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape;with (obj) {__p += '<div class=\'card card-' +__e( type ) +' clearfix midParent\'\ndata-card-id=\'' +__e( id ) +'\' data-class="firechat-card"><div class="fifth  right-divider height-full">\n<img class=\'logo centerMiddle\' src=\'' +__e( logo ) +'\'>\n</div><div class="threefifth">\n<label>\n<strong class=\'name\'>' +__e( name ) +'</strong>\n</label>\n<div class=\'desc\'>\n' +((__t = ( desc )) == null ? '' : __t) +'\n</div>\n</div>\n<div class="fifth height-full">\n<a class="btn centerBottom" href=\'' +__e( url ) +'\' style="color: black;" target="_blank">' +__e( btn ) +'</a>\n</div>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/header.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape, __j = Array.prototype.join;function print() { __p += __j.call(arguments, '') }with (obj) {__p += '<div id="firechat-header-back" class="aligncenter">\n'; if(backVisible) { ;__p += '\n<i class="fa fa-arrow-left fa-lg"></i>\n'; } ;__p += '\n</div>\n<div id="firechat-header-title"  class="aligncenter">\n'; if(type === 'NOTIFICATIONS_ALL') { ;__p += ' Welcome ' +__e( userName ); }if (type === 'NOTIFICATIONS_ROOM') { ;__p +=__e( confName  );}if (type === 'CHAT') { ;__p +=__e( roomName ); } ;__p += '\n</div>\n<div id="firechat-header-notification"  class="aligncenter">\n<i class="fa fa-bell fa-lg"></i>\n<span id="notificationBellCount" class="badge badge-notify ' +__e( badge ) +'">' +__e( counter ) +'</span>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/layout-full.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape;with (obj) {__p += '<div id=\'firechat\' class="maximized">\n<div class=\'vflex maximized\'>\n<div id=\'firechat-header\' class="flex-header vflex-row-stiff"></div><!-- This is where notifications will go -->\n<div id="firechat-notifications" class="vflex-row-expanding">\n<h2>No New Messages</h2>\n</div><div id=\'firechat-footer\' class=\'vflex-row-stiff\'></div>\n</div>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/message-context-menu.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape, __j = Array.prototype.join;function print() { __p += __j.call(arguments, '') }with (obj) {__p += '<div data-toggle=\'firechat-contextmenu\' class=\'contextmenu\' data-message-id=\'' +__e( id ) +'\'>\n<ul>\n<li><a href=\'#!\' data-event=\'firechat-user-warn\'>Warn User</a></li>\n'; if (allowKick) { ;__p += '\n<li><a href=\'#!\' data-event=\'firechat-user-kick\'>Kick User</a></li>\n'; } ;__p += '\n<li><a href=\'#!\' data-event=\'firechat-user-suspend-hour\'>Suspend User (1 Hour)</a></li>\n<li><a href=\'#!\' data-event=\'firechat-user-suspend-day\'>Suspend User (1 Day)</a></li>\n<li><a href=\'#!\' data-event=\'firechat-message-delete\'>Delete Message</a></li>\n</ul>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/message.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape, __j = Array.prototype.join;function print() { __p += __j.call(arguments, '') }with (obj) {__p += '<div class=\'message message-' +__e( type ) +' '; if (isSelfMessage) { ;__p += ' message-self '; } ;__p += '\'\ndata-message-id=\'' +__e( id ) +'\' data-user-id=\'' +__e( userId ) +'\'\ndata-user-name=\'' +__e( name ) +'\' data-class="firechat-message">\n<div class=\'clearfix\'>\n<label class=\'fourfifth\'>\n<strong class=\'name\' title=\'' +__e( name ) +'\'>' +__e( name ) +'</strong>\n<em>(' +__e( localdate ) +' at ' +__e( localtime ) +')</em>:\n</label>'; if (!disableActions) { ;__p += '\n<label class=\'fifth alignright\'>\n<a href=\'#!\' data-event=\'firechat-user-chat\' class=\'icon user-chat\' title=\'Invite to Private Chat\'>&nbsp;</a>\n<a href=\'#!\' data-event=\'firechat-user-mute-toggle\' class=\'icon user-mute\' title=\'Mute User\'>&nbsp;</a>\n</label>\n'; } ;__p += '</div>\n<div class=\'clearfix content\'>\n'; if (type === 'SUCCESS') { ;__p += '\n<i class="fa fa-clock-o fa-2x"></i>\n'; } ; if (type === 'CRITICAL' || type === 'WARNING') { ;__p += '\n<i class="fa fa-exclamation-triangle fa-2x"></i>\n'; } ;__p +=((__t = ( message )) == null ? '' : __t) +'</div>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/notification-confs.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape, __j = Array.prototype.join;function print() { __p += __j.call(arguments, '') }with (obj) {__p += '<div class=\'message\' data-conf-id=\'' +__e( confId ) +'\'\ndata-conf-name=\'' +__e( confName ) +'\'\ndata-class="firechat-message" data-event="firechat-notification-conf">\n<div class=\'clearfix notificationItem\'>\n<label class=\'fourfifth notificationTitle\'>\n<strong class=\'name\'>' +__e( confName ) +'</strong>\n</label>\n<div class="fifth">'; if (msgCount > 0) { ;__p += '\n<div class="right">\n<i class="fa fa-comments fa-lg"></i>\n<span class="badge badge-warning">' +__e( msgCount ) +'</span>\n</div>\n'; } ; if (alertCount > 0) { ;__p += '\n<br/>\n<div class="right">\n<i class="fa fa-exclamation-triangle fa-lg"></i>\n<span class="badge badge-warning">' +__e( alertCount ) +'</span>\n</div>\n'; } ;__p += '\n</div>\n</div>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/notification-rooms.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape, __j = Array.prototype.join;function print() { __p += __j.call(arguments, '') }with (obj) {__p += '<div class=\'message\' data-conf-id=\'' +__e( confId ) +'\'\ndata-room-id=\'' +__e( roomId ) +'\' data-room-name=\'' +__e( roomName ) +'\'\ndata-class="firechat-message" data-msg-count=\'' +__e( msgCount ) +'\'\ndata-event="firechat-notification-room">\n<div class=\'clearfix notificationItem\'>\n<label class=\'fourfifth notificationTitle\'>\n<i class="fa fa-comments-o fa-lg"></i>&nbsp;<strong class=\'name\'>' +__e( roomName ) +'</strong>\n</label>\n<div class="fifth">'; if (msgCount > 0) { ;__p += '\n<div class="right">\n<i class="fa fa-comments fa-lg"></i>\n<span class="badge badge-warning">' +__e( msgCount ) +'</span>\n</div>\n'; } ; if (alertCount > 0) { ;__p += '\n<br/>\n<div class="right">\n<i class="fa fa-exclamation-triangle fa-lg"></i>\n<span class="badge badge-warning">' +__e( alertCount ) +'</span>\n</div>\n'; } ;__p += '\n</div>\n</div>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/prompt-alert.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape;with (obj) {__p += '<div class=\'aligncenter clearfix\'>\n<h6>' +__e( message ) +'</h6>\n<p class=\'clearfix\'>\n<button type=\'button\' class=\'btn quarter right close\'>Close</button>\n</p>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/prompt-create-room.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape;with (obj) {__p += '<div class=\'clearfix\'>\n<h6>Give your chat room a name:</h6>\n<input data-input=\'firechat-room-name\' type=\'text\' placeholder=\'Room name...\' style=\'margin-bottom: 5px;\' maxlength=\'' +__e( maxLengthRoomName ) +'\'>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/prompt-invitation.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape;with (obj) {__p += '<div class=\'aligncenter clearfix\'>\n<h5>' +__e( fromUserName ) +'</h5>\n<p>invited you to join</p>\n<h5>' +__e( toRoomName ) +'</h5>\n<p class=\'clearfix\'>\n<button data-toggle=\'accept\' type=\'button\' class=\'btn\'>Accept</button>\n<button data-toggle=\'decline\' type=\'button\' class=\'btn\'>Decline</button>\n</p>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/prompt-invite-private.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape;with (obj) {__p += '<div class=\'aligncenter clearfix\'>\n<h6>Invite <strong>' +__e( userName ) +'</strong> to ' +__e( roomName ) +'?</h6>\n<p class=\'clearfix\'>\n<button data-toggle=\'accept\' type=\'button\' class=\'btn\'>Invite</button>\n<button data-toggle=\'decline\' type=\'button\' class=\'close btn\'>Cancel</button>\n</p>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/prompt-invite-reply.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape, __j = Array.prototype.join;function print() { __p += __j.call(arguments, '') }with (obj) {__p += '<div class=\'aligncenter clearfix\'>\n<h5>' +__e( toUserName ) +'</h5>\n<p>\n'; if (status === 'accepted') { ;__p += ' accepted your invite. '; } else { ;__p += ' declined your invite. '; } ;__p += '\n</p>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/prompt-user-mute.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape;with (obj) {__p += '<div class=\'aligncenter clearfix\'>\n<h5>' +__e( userName ) +'</h5>\n<p class=\'clearfix\'>\n<button data-toggle=\'accept\' type=\'button\' class=\'btn\'>Mute</button>\n<button data-toggle=\'decline\' type=\'button\' class=\'btn\'>Cancel</button>\n</p>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/prompt.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape;with (obj) {__p += '<div class=\'prompt hidden\'>\n<div class=\'prompt-header\'>\n' +__e( title ) +'\n<a href=\'#!\' class=\'close right\'>X</a>\n</div>\n<div class=\'prompt-body clearfix\'>\n' +((__t = ( content )) == null ? '' : __t) +'\n</div>\n<div class=\'prompt-footer\'></div>\n</div>';}return __p};

this["FirechatDefaultTemplates"]["templates/room-content.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape, __j = Array.prototype.join;function print() { __p += __j.call(arguments, '') }with (obj) {__p += '<div id="firechat-chat-controls" class=\'vflex-row-stiff room-content\'><div id="firechat-chat-users">\n</div><!--\n<div class=\'firechat-dropdown twofifth\'>\n<a data-event=\'firechat-user-room-list-btn\' class=\'full btn firechat-dropdown-toggle\'\ndata-toggle="firechat-dropdown" href=\'#\' data-target=\'firechat-room-user-list-' +__e( roomId ) +'\'>\n<i class="fa fa-users"></i>\nIn Room\n<span class=\'caret\'></span>\n</a><div class=\'firechat-dropdown-menu\' role=\'menu\'>\n<ul id=\'firechat-room-user-list-' +__e( roomId ) +'\' class=\'full\'></ul>\n</div>\n</div>\n-->\n</div><div id=\'firechat-chat-messages\' class=\'vflex-row-expanding room-content\'></div>'; if (talkback) { ;__p += '\n<div id=\'firechat-chat-response\' class=\'vflex-row-stiff room-content\'>\n<span>Remaining:&nbsp;<span id="remainingChars">' +__e(maxMsgLength) +'</span></span>\n<label>Your message:</label>\n<textarea rows=4 id=\'textarea' +__e( roomId ) +'\' placeholder=\'Type your message here...\' data-provide=\'limit\'\ndata-counter=\'#remainingChars\'\nmaxlength=' +__e(maxMsgLength) +' >\n</textarea>\n</div>\n'; } ;}return __p};

this["FirechatDefaultTemplates"]["templates/room-user-list-item.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape;with (obj) {__p += '<a href="#" class=\'firechat-chat-user ' +__e( userClass ) +'\' data-toggle=\'tooltip\' data-user-id=\'' +__e( id ) +'\' title=\'' +__e( name ) +'\'>\n' +__e( initials ) +'\n</a>';}return __p};

this["FirechatDefaultTemplates"]["templates/room-user-search-list-item.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape, __j = Array.prototype.join;function print() { __p += __j.call(arguments, '') }with (obj) {__p += '<li data-user-id=\'' +__e( id ) +'\' data-user-name=\'' +__e( name ) +'\'>\n<a href=\'#!\' class=\'clearfix\'>\n'; if (disableActions) { ;__p += '\n<span class=\'left fourfifth clipped\' title=\'' +__e( name ) +'\'>' +__e( name ) +'</span>\n'; } else { ;__p += '\n<span data-event=\'firechat-user-invite\' class=\'left fourfifth clipped\' title=\'' +__e( name ) +'\'>' +__e( name ) +'</span>\n<span data-event=\'firechat-user-invite\' class=\'icon plus right\' title=\'Invite to Room\'>+</span>\n'; } ;__p += '\n</a>\n</li>';}return __p};

this["FirechatDefaultTemplates"]["templates/user-search-list-item.html"] = function(obj) {obj || (obj = {});var __t, __p = '', __e = _.escape, __j = Array.prototype.join;function print() { __p += __j.call(arguments, '') }with (obj) {__p += '<li data-user-id=\'' +__e( id ) +'\' data-user-name=\'' +__e( name ) +'\'>\n<a href=\'#!\' class=\'clearfix\'>\n'; if (disableActions) { ;__p += '\n<span class=\'left fivesixth clipped\' title=\'' +__e( name ) +'\'>' +__e( name ) +'</span>\n'; } else { ;__p += '\n<span data-event=\'firechat-user-chat\' class=\'left fivesixth clipped\' title=\'' +__e( name ) +'\'>' +__e( name ) +'</span>\n<span data-event=\'firechat-user-chat\' class=\'icon user-chat right\' title=\'Invite to Private Chat\'>&nbsp;</span>\n'; } ;__p += '\n</a>\n</li>';}return __p};
(function($) {

  // Shim for Function.bind(...) - (Required by IE < 9, FF < 4, SF < 6)
  if (!Function.prototype.bind) {
    Function.prototype.bind = function(oThis) {
      if (typeof this !== "function") {
        throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
      }
  
      var aArgs = Array.prototype.slice.call(arguments, 1), 
          fToBind = this, 
          fNOP = function() {},
          fBound = function() {
            return fToBind.apply(this instanceof fNOP && oThis ? this : oThis,
                                 aArgs.concat(Array.prototype.slice.call(arguments)));
          };
   
      fNOP.prototype = this.prototype;
      fBound.prototype = new fNOP();
      return fBound;
    };
  }

  // Shim for Object.keys(...) - (Required by IE < 9, FF < 4)
  Object.keys = Object.keys || function(oObj) {  
    var result = [];  
    for (var name in oObj) {  
      if (oObj.hasOwnProperty(name)) {
        result.push(name);  
      }
    }  
    return result;  
  };

})();

// Firechat is a simple, easily-extensible data layer for multi-user,
// multi-room chat, built entirely on [Firebase](https://firebase.com).
//
// The Firechat object is the primary conduit for all underlying data events.
// It exposes a number of methods for binding event listeners, creating,
// entering, or leaving chat rooms, initiating chats, sending messages,
// and moderator actions such as warning, kicking, or suspending users.
//
//     Firechat.js 0.0.0
//     https://firebase.com
//     (c) 2014 Firebase
//     License: MIT

// Setup
// --------------
(function(Firebase) {

  // Establish a reference to the `window` object, and save the previous value
  // of the `Firechat` variable.
  var root = this,
      previousFirechat = root.Firechat;

  function Firechat(firebaseRef, options) {
    // Instantiate a new connection to Firebase.
    this._firebase = firebaseRef;

    // User-specific instance variables.
    this._user = null;
    this._userId = null;
    this._userName = null;
    this._isModerator = false;

    // A unique id generated for each session.
    this._sessionId = null;

    // A mapping of event IDs to an array of callbacks.
    this._events = {};

    // A mapping of room IDs to a boolean indicating presence.
    this._rooms = {};

    // A structure of conference -> room -> events - > map of events
    this._notifications = {
      alertCount: 0,
      msgCount: 0,
      confs: {}
    };

    // Unique list of room user sessions
    this._roomUsers = {};

    // A helper list of messages that have been read, but notification was late
    this._readMsgIds = [];

    // A mapping of operations to re-queue on disconnect.
    this._presenceBits = {};

    // Commonly-used Firebase references.
    this._userRef        = null;
    this._messageRef     = this._firebase.child('messages');
    this._confRef        = this._firebase.child('conferences');
    this._moderatorsRef  = this._firebase.child('moderators');
    this._suspensionsRef = this._firebase.child('suspensions');

    // Setup and establish default options.
    this._options = options || {};

    // The number of historical messages to load per room.
    this._options.numMaxMessages          = this._options.numMaxMessages || 200;

    //How long a notification stays marked as read before it is being cleaned, default 72 hours
    this._options.readNotificationLifespan = this._options.readNotificationLifespan || 3 * 24 * 3600 * 1000;
  }

  // Run Firechat in *noConflict* mode, returning the `Firechat` variable to
  // its previous owner, and returning a reference to the Firechat object.
  Firechat.noConflict = function noConflict() {
    root.Firechat = previousFirechat;
    return Firechat;
  };

  // Export the Firechat object as a global.
  root.Firechat = Firechat;

  // Firechat Internal Methods
  // --------------
  Firechat.prototype = {
    // Load the initial metadata for the user's account and set initial state.
    _loadUserMetadata: function(onComplete) {
      var self = this;

      // Update the user record with a default name on user's first visit.
      this._userRef.transaction(function(current) {
        if (!current || !current.id || !current.name) {
          return {
            id: self._userId,
            name: self._userName
          };
        }
      }, function(error, committed, snapshot) {
        self._user = snapshot.val();
        self._moderatorsRef.child(self._userId).once('value', function(snapshot) {
          self._isModerator = !!snapshot.val();
          root.setTimeout(onComplete, 0);
        });
      });
    },

    // Initialize Firebase listeners and callbacks for the supported bindings.
    _setupDataEvents: function() {
      // Monitor connection state so we can requeue disconnect operations if need be.
      this._firebase.root().child('.info/connected').on('value', function(snapshot) {
        if (snapshot.val() === true) {
          // We're connected (or reconnected)! Set up our presence state.
          for (var i = 0; i < this._presenceBits; i++) {
            var op = this._presenceBits[i],
                ref = this._firebase.root().child(op.ref);

            ref.onDisconnect().set(op.offlineValue);
            ref.set(op.onlineValue);
          }
        }
      }, this);

      this._userRef.child('notifications').once('value', this._onNotification, this);

      // Generate a unique session id for the visit.
      var sessionRef = this._userRef.child('sessions').push();
      this._sessionId = sessionRef.key();
      this._queuePresenceOperation(sessionRef, true, null);

      // Listen for state changes for the given user.
      this._userRef.on('value', this._onUpdateUser, this);

      // Listen for chat invitations from other users.
      this._userRef.child('invites').on('child_added', this._onFirechatInvite, this);

      // Listen for new event notifications
      this._userRef.child('notifications').on('child_changed', this._onNotification, this);
      this._userRef.child('notifications').on('child_removed', this._onNotificationRemove, this);
    },

    // Append the new callback to our list of event handlers.
    _addEventCallback: function(eventId, callback) {
      this._events[eventId] = this._events[eventId] || [];
      this._events[eventId].push(callback);
    },

    // Retrieve the list of event handlers for a given event id.
    _getEventCallbacks: function(eventId) {
      if (this._events.hasOwnProperty(eventId)) {
        return this._events[eventId];
      }
      return [];
    },

    // Invoke each of the event handlers for a given event id with specified data.
    _invokeEventCallbacks: function(eventId) {
      var args = [],
          callbacks = this._getEventCallbacks(eventId);
      //debugger; //TODO: Understand what this does
      Array.prototype.push.apply(args, arguments);
      args = args.slice(1);

      for (var i = 0; i < callbacks.length; i += 1) {
        callbacks[i].apply(null, args);
      }
    },

    // Keep track of on-disconnect events so they can be requeued if we disconnect the reconnect.
    _queuePresenceOperation: function(ref, onlineValue, offlineValue) {
      if(offlineValue=== null) {
        ref.onDisconnect().remove();
      } else {
        ref.onDisconnect().set(offlineValue);
      }
      ref.set(onlineValue);
      this._presenceBits[ref.toString()] = {
        ref: ref,
        onlineValue: onlineValue,
        offlineValue: offlineValue
      };
    },

    // Remove an on-disconnect event from firing upon future disconnect and reconnect.
    _removePresenceOperation: function(path, value) {
      var ref = new Firebase(path);
      ref.onDisconnect().cancel();
      ref.set(value);
      delete this._presenceBits[path];
    },

    // Event to monitor current user state.
    _onUpdateUser: function(snapshot) {
      this._user = snapshot.val();
      this._invokeEventCallbacks('user-update', this._user);
    },

    // Event to monitor current auth + user state.
    _onAuthRequired: function() {
      this._invokeEventCallbacks('auth-required');
    },

    // Events to monitor room entry / exit and messages additional / removal.
    _onEnterRoom: function(room) {
      this._invokeEventCallbacks('room-enter', room);
    },

    //Expects snapshot from room-users
    _onRoomUsersChange: function(users) {
      var usernames = users.val() || {};
      this._roomUsers = {};
      for (var username in usernames) {
        for (var session in usernames[username]) {
          // Skip all other sessions for this user as we only need one.
          this._roomUsers[username] = usernames[username][session];
          break;
        }
      }

      this._invokeEventCallbacks('room-users-change', this._roomUsers);
    },
    _onNewMessage: function(confId, roomId, snapshot) {
      var message = snapshot.val();
      message.id = snapshot.key();
      if(message.fromUserId != this._userId) {
        this._invokeEventCallbacks('message-add', confId, roomId, message);
      }
    },
    _onRemoveMessage: function(confId, roomId, snapshot) {
      var messageId = snapshot.key();
      this._invokeEventCallbacks('message-remove', confId, roomId, messageId);
    },

    _onLeaveRoom: function(confId, roomId) {
      this._invokeEventCallbacks('room-exit', confId, roomId);
    },

    // Event to listen for notifications about events on the platform
    _onNotification: function(snapshot) {
      var self = this,
          notification = snapshot.val();

      if(snapshot.key() === 'notifications') {
        console.log("Full Notification Object Received");
        if(notification === null) {
          console.log("Ignoring notifications...");
          return; //On first visit we might get nothing in value
        }
        for(var confId in notification) {
          self._notificationsProcess(confId, notification[confId]);
        }
      } else {
        console.log("Received single notification object");
        self._notificationsProcess(snapshot.key(), notification);
      }

      //Passing information on unread
      self._invokeEventCallbacks('notification', self._notifications);
    },

    _onNotificationRemove: function(snapshot) {
      var self = this;
      if (snapshot.key() !== 'notifications') {
        var emptyNotification = {};
        self._notificationsProcess(snapshot.key(), emptyNotification);
        delete self._notifications.confs[snapshot.key()];
        console.log("Deleting conference id:" + snapshot.key());
        //Passing information on unread
        self._invokeEventCallbacks('notification', self._notifications);
      } else {
        console.log("Got message that all notifications are gone");
      }
    },

    /**
     *
     * @param confId
     * @param roomId
     * @param upToKey What is the
     * @private
     */
    _notificationsCleanup: function(confId, roomId, upToKey, cutoffTime) {
      var self = this;

      var roomEvents = self._userRef
          .child("notifications")
          .child(confId).child("rooms")
          .child(roomId).child("events");

      roomEvents.orderByKey().endAt(upToKey).once("value",function(dataSnapshot){
        dataSnapshot.forEach(function(childSnapshot){
          if(cutoffTime && childSnapshot.val().timestamp > cutoffTime) {
            console.warn("Deleting new key:" + confId + " room:" + roomId + " event:" + childSnapshot.key() + " ts:" + childSnapshot.val().timestamp);
          }
          else {
            roomEvents.child(childSnapshot.key()).remove();
          }
        });
      });
    },

    eventCutoffTimestamp: function() {
      var now = new Date();
      var utc_now = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),
          now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
      return utc_now - this._options.readNotificationLifespan;
    },

    _notificationsProcess: function(confId, notification) {
      var self = this;

      console.log("Notification Event for: " + confId);
      var cutoffTime = self.eventCutoffTimestamp();
      notification.alertCount = 0;
      notification.msgCount = 0;
      notification.eventCount = 0;

      for (var roomId in notification.rooms) {
        var currRoom = notification.rooms[roomId];
        currRoom.alertCount = 0;
        currRoom.msgCount = 0;

        var lastOldEvent = null; //EventId up to which all notifications should be removed
        if(notification.rooms[roomId].events) {
          for (var eventId in notification.rooms[roomId].events) {
            notification.eventCount++;

            var event = notification.rooms[roomId].events[eventId];
            //If notification is read and older than cutoff time we are removing it
            if (event.status === 'READ' && event.timestamp < cutoffTime) {
              lastOldEvent = eventId;
              delete notification.rooms[roomId].events[eventId];
            }

            if (!event.status || event.status === 'UNREAD') {
              switch (event.notificationType) {
                case 'MESSAGE': //General message
                case 'ALERT':   //Alert/notification message
                  var rIdx = self._readMsgIds.indexOf(event.data.msgId);
                  if (rIdx === -1) {
                    if(event.notificationType === 'MESSAGE') {
                      currRoom.msgCount++;
                    } else {
                      currRoom.alertCount++;
                    }
                  }
                  else { //If message was previously shown but notification just arrived
                    console.log("Read message from here...345");
                    self._readMsgIds.splice(rIdx, 1);
                    self.fbMessageRead(confId, roomId, event.data.msgId);
                    self.fbNotificationRead(confId, roomId, eventId);
                  }
                  break;
                case 'CARD':
                case 'UPSELL':
                case 'COUPON':
                case 'SUPPLIER':
                  console.log("Received " + event.notificationType + " message");
                  var card = self.notificationToCard(eventId, event);
                  this._invokeEventCallbacks('card-add', confId, roomId, card);
                  break;
                default :
                  break;
              }
            }
          }
        }

        notification.alertCount += currRoom.alertCount;
        notification.msgCount += currRoom.msgCount;

        if(lastOldEvent) {
          self._notificationsCleanup(confId, roomId, lastOldEvent, cutoffTime);
        }
      }

      //Updating globals
      if(self._notifications.confs[confId]) {
        self._notifications.alertCount -=  self._notifications.confs[confId].alertCount;
        self._notifications.msgCount -=  self._notifications.confs[confId].msgCount ;
      }

      self._notifications.alertCount  += notification.alertCount;
      self._notifications.msgCount    += notification.msgCount;

      self._notifications.confs[confId] = notification;
      console.log("Notification totals messages: " + self._notifications.msgCount +
                  " alerts: " + self._notifications.alertCount );
    },

    // Events to monitor chat invitations and invitation replies.
    _onFirechatInvite: function(snapshot) {
      var self = this,
          invite = snapshot.val();

      // Skip invites we've already responded to.
      if (invite.status) {
        return;
      }

      invite.id = invite.id || snapshot.key();
      self.getRoom(invite.confId, invite.roomId, function(room) {
        invite.toRoomName = room.name;
        self._invokeEventCallbacks('room-invite', invite);
      });
    },
    _onFirechatInviteResponse: function(snapshot) {
      var self = this,
          invite = snapshot.val();

      invite.id = invite.id || snapshot.key();
      this._invokeEventCallbacks('room-invite-response', invite);
    },

    _getRefMessage: function(confId, roomId, msgId) {
      var ref = this._messageRef
          .child(confId)
          .child(roomId)
          .child(msgId);
      return ref;
    }
  };

  // Firechat External Methods
  // --------------

  /**
   * Looks for notification cards in range
   * @param confId
   * @param roomId
   * @param startTs
   * @param endTs
   */
  Firechat.prototype.getCardsInRange = function(confId, roomId, startTs, endTs) {
    var self = this,
        events = self._notifications.confs[confId].rooms[roomId].events,
        result = [];
    if(events) {
      for (var eventId in events) {
        var event = events[eventId];
        if (event.timestamp >= startTs && event.timestamp < endTs) {
          var card = self.notificationToCard(eventId, event);
          result.push(card);
        }
      }
    }
    return result;
  };

  /**
   * Helper method to convert notification object to a card
   * @param notificiation
   */
  Firechat.prototype.notificationToCard = function(eventId, event) {
    var card = {
      id:   eventId,
      type: event.notificationType,
      ts:   event.timestamp
    };

    for (var prop in event.data) {
      if (event.data.hasOwnProperty(prop)) {
        card[prop] = event.data[prop];
      }
    }
    return card;
  };

  /**
   * Returns cached conference name
   * @param confId
   */
  Firechat.prototype.getConferenceName = function(confId) {
    var self = this;
    if(self._notifications.confs[confId] !== undefined) {
      return self._notifications.confs[confId].name;
    }
    return null;
  };

  /**
   * Returns cached room name
   */
  Firechat.prototype.getConferenceRoomName = function(confId, roomId) {
    var self = this;
    if(self._notifications.confs[confId] !== undefined && self._notifications.confs[confId].rooms[roomId] !== undefined) {
      return self._notifications.confs[confId].rooms[roomId].name;
    }
    return null;
  };

  // Initialize the library and setup data listeners.
  Firechat.prototype.setUser = function(userId, userName, callback) {
    var self = this;

    self._firebase.onAuth(function(authData) {
      if (authData) {
        self._userId = userId.toString();
        self._userName = userName.toString();
        self._userRef = self._firebase.child('users').child(self._userId);
        self._loadUserMetadata(function() {
          root.setTimeout(function() {
            if(callback) {
              callback(self._user);
            }
            self._setupDataEvents();
          }, 0);
        });
      } else {
        self.warn('Firechat requires an authenticated Firebase reference. Pass an authenticated reference before loading.');
      }
    });
  };

  // Resumes the previous session by automatically entering rooms.
  Firechat.prototype.resumeSession = function() {
    this._userRef.child('joined').once('value', function(snapshot) {
      var confs = snapshot.val();

      for(var confId in confs) {
        var currConf = confs[confId];
        for (var roomId in currConf) {
          var currRoom = currConf[roomId];
          if(currRoom.joinedAt) { //joined at means room was not disconnected...//TODO: May be better approach
            this.joinRoom(confId, roomId);
          }
        }
      }
    }, /* onError */ function(err){
      console.error("Failed to get list of open chat rooms: " + err.message);
    }, /* context */ this);
  };

  // Callback registration. Supports each of the following events:
  Firechat.prototype.on = function(eventType, cb) {
    this._addEventCallback(eventType, cb);
  };

  Firechat.prototype.sendMessage = function(confId, roomId, messageContent, messageType, cb) {
    var self = this,
        message = {
          userId: self._userId,
          name: self._userName,
          timestamp: Firebase.ServerValue.TIMESTAMP,
          message:  messageContent,
          type: messageType || 'NORMAL' //By default no decorations
        },
        room = self._rooms[roomId],
        newMessageRef;

    if (!self._user) {
      self._onAuthRequired();
      if (cb) {
        cb(new Error('Not authenticated or user not set!'));
      }
      return;
    }

    newMessageRef = self._messageRef.child(confId).child(roomId).push();
    newMessageRef.setWithPriority(message, Firebase.ServerValue.TIMESTAMP, cb);

    //Update own last activity
    self.fbLastActivityUpdate(confId, roomId);

    //Notifications for all users
    self.sendConfRoomNewMessageNotification(confId, roomId, newMessageRef.key(), message.type);

    //Issue notification that a message was sent
    var eventData = {
      confId: confId,
      roomId: roomId,
      from: self._userId,
      message: messageContent,
      type: messageType,
      roomUsers: Object.keys(this._roomUsers).map(function(v,idx){
        return v;
      }),
      roomNotify: Object.keys(room.notifyUsers).map(function(v,idx){
        return v;
      })
    };

    this._invokeEventCallbacks('message-sent', eventData);
  };

  /**
   * For all users registered for notifications for the room deliver notification
   *
   * @param confId Conference ID
   * @param roomId Room ID
   */
  Firechat.prototype.sendConfRoomNewMessageNotification = function(confId, roomId, msgId, messageType) {
    var self = this,
        room = self._rooms[roomId];

    if(!room) {
      return;
    }

    var data = {
      msgId : msgId
    };

    for(var userId in room.notifyUsers) {
      if (userId !== self._userId) {
        console.log("Notifying user: " + userId + " about new message:" + msgId);
        self.sendConfRoomUserNotification(userId, confId, roomId, 'MESSAGE', messageType, data);
      }
    }
  };

  Firechat.prototype.fbLastActivityUpdate = function(confId, roomId) {
    var self = this;
    //1. Conference update
    self._userRef
        .child('notifications')
        .child(confId)
        .child('lastActivity')
        .set(Firebase.ServerValue.TIMESTAMP);
    //2. Room update
    self._userRef
        .child('notifications')
        .child(confId)
        .child('rooms')
        .child(roomId)
        .child('lastActivity')
        .set(Firebase.ServerValue.TIMESTAMP);
  };

  /**
   * @param confId
   * @param roomId
   * @param msgId (Optional) message id that caused room to clear
   */
  Firechat.prototype.readRoom = function(confId, roomId, msgId) {
    var self = this,
        currRoom = self._notifications.confs[confId].rooms[roomId];
    //console.log("Marking conf" + confId + " room: " + roomId + " READ");
    if(currRoom.events) {
      for (var eventId in currRoom.events) {
        var currEvent = currRoom.events[eventId];
        if (currEvent.data && currEvent.data.msgId === msgId) {
          msgId = null;
        }
        if (currEvent.status === 'UNREAD') {
          if (currEvent.notificationType === 'MESSAGE' || currEvent.notificationType === 'ALERT') {
            self.fbMessageRead(confId, roomId, currEvent.data.msgId);
          }
          self.fbNotificationRead(confId, roomId, eventId);
          currEvent.status = 'READ';
        }
      }
    }

    if(msgId) {
      self._readMsgIds.push(msgId);
    }
  };

  /**
   * Marks message as read in user's notification list and adds message reading
   * update to message's metadata
   * @param confId Message conference ID
   * @param roomId Message room ID
   * @param msgId  Message ID
   */
  Firechat.prototype.fbMessageRead = function(confId, roomId, msgId) {
    var self = this;
    var messageReadBy = self._getRefMessage(confId, roomId, msgId).child('readBy').child(self._userId);
    //Update message status
    messageReadBy.set(Firebase.ServerValue.TIMESTAMP, function(err){
      if(err !== null) { //On success err should be null
        console.error("Failed to update message read list: " + err.message);
      } else {
        console.log("Marking messages as read /" + confId + "/" + roomId + "/" + msgId);
      }
    });
  };

  /**
   * Marks specified event as "READ"
   * @param confId
   * @param roomId
   * @param eventId
   */
  Firechat.prototype.fbNotificationRead = function(confId, roomId, eventId) {
    var self = this,
        userNotificationsRef = self._firebase.child('users')
        .child(self._userId)
        .child('notifications')
        .child(confId)
        .child('rooms')
        .child(roomId)
        .child('events')
        .child(eventId)
        .child('status');

    userNotificationsRef.set('READ', function(err){
      if (err !== null) { //On success err should be null
        console.error("Failed to update notification status to READ: " + err.message);
      } else {
        console.log("Cleared notification event: /" + confId + "/rooms/" + roomId + "/events/" + eventId);
      }
    });
  };

  Firechat.prototype.deleteMessage = function(roomId, messageId, cb) {
    var self = this;

    self._messageRef.child(roomId).child(messageId).remove(cb);
  };

  // Mute or unmute a given user by id. This list will be stored internally and
  // all messages from the muted clients will be filtered client-side after
  // receipt of each new message.
  Firechat.prototype.toggleUserMute = function(userId, cb) {
    var self = this;

    if (!self._user) {
      self._onAuthRequired();
      if (cb) {
        cb(new Error('Not authenticated or user not set!'));
      }
      return;
    }

    self._userRef.child('muted').child(userId).transaction(function(isMuted) {
      return (isMuted) ? null : true;
    }, cb);
  };

  /**
   * Invites system is BROKEN
   */
  Firechat.prototype.acceptInvite = function(inviteId, cb) {
    var self = this;

    self._userRef.child('invites').child(inviteId).once('value', function(snapshot) {
      var invite = snapshot.val();
      if (invite === null && cb) {
        return cb(new Error('acceptInvite(' + inviteId + '): invalid invite id'));
      } else {
        self.joinRoom(invite.roomId);
        self._userRef.child('invites').child(inviteId).update({
          'status': 'accepted',
          'toUserName': self._userName
        }, cb);
      }
    }, self);
  };

  Firechat.prototype.declineInvite = function(inviteId, cb) {
    var self = this,
        updates = {
          'status': 'declined',
          'toUserName': self._userName
        };

    self._userRef.child('invites').child(inviteId).update(updates, cb);
  };

  Firechat.prototype.getConferenceRoomList = function(cb) {
    var self = this;

    self._userRef.child('notifications').once('value', function(snapshot) {
      var confsSnapshot = snapshot.val();
      var roomList = {};
      for (var confId in confsSnapshot) {
        var currConference = confsSnapshot[confId];
        console.log('Conference:' + confId);
        for(var roomId in currConference.rooms) {
          var room = {
            name  : currConference.rooms[roomId].name,
            id    : roomId,
            confid: confId,
            type  : "public"
          };
          roomList[roomId] = room;
        }
      }
      cb(roomList);
    });

  };

  Firechat.prototype.getUsersByRoom = function() {
    var self = this,
        confId = arguments[0],
        roomId = arguments[1],
        query = self.getRoomUsersRef(confId, roomId),
        cb = arguments[arguments.length - 1],
        limit = null;

    if (arguments.length > 3) {
      limit = arguments[2];
    }

    query = (limit) ? query.limitToLast(limit) : query;

    query.once('value', function(snapshot) {
      var usernames = snapshot.val() || {},
          usernamesUnique = {};

      for (var username in usernames) {
        for (var session in usernames[username]) {
          // Skip all other sessions for this user as we only need one.
          usernamesUnique[username] = usernames[username][session];
          break;
        }
      }

      root.setTimeout(function() {
        cb(usernamesUnique);
      }, 0);
    });
  };

  // Miscellaneous helper methods.
  Firechat.prototype.userIsModerator = function() {
    return this._isModerator;
  };

  Firechat.prototype.warn = function(msg) {
    if (console) {
      msg = 'Firechat Warning: ' + msg;
      if (typeof console.warn === 'function') {
        console.warn(msg);
      } else if (typeof console.log === 'function') {
        console.log(msg);
      }
    }
  };

  /**
   * REFACTORED
   */


  /**
   * Deposits a new message notification to the user
   *
   * @param userId Id of the user who should get notification
   * @param confId Id of the conference where message was deposited
   * @param roomId Id of the room in the conference where message was deposited
   * @param notificationType Currently loosely defined what notification type is.
   * @param data Additional data to be stored (TODO: Probably should be removed)
   * @param cb Callback function to execute when notification was delivered
   */
  Firechat.prototype.sendConfRoomUserNotification = function(userId, confId, roomId, notificationType, messageType, data, cb) {

    var self = this,
        userNotificationsRef = self._firebase.child('users')
            .child(userId)
            .child('notifications')
            .child(confId)
            .child('rooms')
            .child(roomId)
            .child('events');

    var notification = {
      fromUserId:       self._userId,
      fromUserName:     self._userName,
      timestamp:        Firebase.ServerValue.TIMESTAMP,
      status:           'UNREAD',
      notificationType: notificationType,
      priority:         messageType || 'NORMAL', //Backwards compatible name 'priority' storing message type
      data: data || {}
    };
    userNotificationsRef.push(notification, cb);
  };

  // Create and automatically enter a new chat room. TODO: See how this is used
  Firechat.prototype.createRoom = function(roomName, confId, roomType, callback) {
    var self = this,
        newRoomRef = this._confRef.child(confId).push();

    var newRoom = {
      id: newRoomRef.key(),
      name: roomName,
      type: roomType || 'PUBLIC',
      createdByUserId: this._userId,
      createdAt: Firebase.ServerValue.TIMESTAMP
    };

    if (roomType === 'PRIVATE') {
      newRoom.authorizedUsers = {};
      newRoom.authorizedUsers[this._userId] = true;
    }

    newRoomRef.set(newRoom, function(error) {
      if (!error) {
        self.joinRoom(confId, newRoomRef.key());
      }
      if (callback) {
        callback(confId, newRoomRef.key());
      }
    });
  };

  // Enter a chat room.
  Firechat.prototype.joinRoom = function(confId, roomId) {
    console.log("### Joining Conf: " + confId + " room: "+ roomId);
    var self = this;

    //Mark that we've entered room in the conference and we will get updates when room changes
    self.getRoomRef(confId, roomId).on('value',function(snapshot){
      var room = snapshot.val();
      var roomName = room.name;

      if (!roomId || !roomId || !roomName) return;

      room.confId = confId;
      // Skip if we're already in this room.
      if (self._rooms[roomId]) {
        //Just updating existing snapshot of the room data
        self._rooms[roomId] = room;
        console.log("Returning... already there.");
        return;
      }
      self._rooms[roomId] = room;
      if (self._user) {
        // Save entering this room to resume the session again later.
        var joinedRef = self._userRef.child('joined').child(confId).child(roomId);
        self._queuePresenceOperation(joinedRef,
            {joinedAt:  Firebase.ServerValue.TIMESTAMP},
            {leftAt:  Firebase.ServerValue.TIMESTAMP});

        // Set presence bit for the room and queue it for removal on disconnect.
        var presenceRef = self.getRoomUsersRef(confId, roomId).child(self._userId).child(self._sessionId);
        self._queuePresenceOperation(presenceRef, {
          id: self._userId,
          name: self._userName
        }, null);
      }

      // Invoke our callbacks before we start listening for new messages.
      var confName = self._notifications.confs[confId].name;
      self._onEnterRoom({confId: confId, roomId: roomId, confName: confName, roomName: roomName});

      // Setup message listeners
      self._messageRef.child(confId).child(roomId).limitToLast(self._options.numMaxMessages).on('child_added', function(snapshot) {
        self._onNewMessage(confId, roomId, snapshot);
      }, /* onCancel */ function(err) {
        console.error("Failure to subscribe for new messages: " + err.message);
        // Turns out we don't have permission to access these messages.
        self.leaveRoom(roomId);
      }, /* context */ self);

      self._messageRef.child(confId).child(roomId).limitToLast(self._options.numMaxMessages).on('child_removed', function(snapshot) {
        self._onRemoveMessage(confId, roomId, snapshot);
      }, /* onCancel */ function(){}, /* context */ self);


      self.getRoomUsersRef(confId, roomId).on('value', function(dataSnapshot){
        self._onRoomUsersChange(dataSnapshot);
      });
    }, /* onFailure to get the room to join */ function(err){
      console.error("Failure to enter room ConfId: " + confId + " RoomId: " + roomId);
      console.error("Failure to enter room: " + err.message);
    }, self);
  };

  Firechat.prototype.leaveAllRooms = function() {
    var self = this;
    for(var roomId in self._rooms) {
      var r = self._rooms[roomId];
      self.leaveRoom(r.confId, roomId);
    }
  };

  // Leave a chat room.
  Firechat.prototype.leaveRoom = function(confId, roomId) {
    console.log('### Leaving Conf :' + confId + " roomId :" + roomId);
    var self = this,
        userRoomRef = self.getRoomUsersRef(confId, roomId),
        roomRef = self.getRoomRef(confId, roomId);

    //Removing room updates listener
    roomRef.off();

    // Remove listener for new messages to this room.
    self._messageRef.child(confId).child(roomId).off();

    if (self._user) {
      var presenceRef = userRoomRef.child(self._userId).child(self._sessionId);

      // Remove presence bit for the room and cancel on-disconnect removal.
      self._removePresenceOperation(presenceRef.toString(), null);

      // Remove session bit for the room.
      var joinedRef = self._userRef.child('joined').child(confId).child(roomId);
      self._removePresenceOperation(joinedRef.toString(), {leftAt:  Firebase.ServerValue.TIMESTAMP});
    }

    userRoomRef.off();

    delete self._rooms[roomId];
    self._roomUsers = {};

    // Invoke event callbacks for the room-exit event.
    self._onLeaveRoom(confId, roomId);
  };

  Firechat.prototype.getRoomUsersRef = function(confId, roomId) {
    return this._firebase.child('room-users').child(confId).child(roomId);
  };

  Firechat.prototype.getRoomRef = function(confId, roomId) {
    return  this._confRef.child(confId).child('rooms').child(roomId);
  };

  Firechat.prototype.getRoom = function(confId, roomId, callback) {
    this.getRoomRef(confId, roomId).once('value', function(snapshot) {
      callback(snapshot.val());
    }, function(err) {
      console.error("Failed to access confId:" + confId + " roomId:"+ roomId + " err: " + err.message);
    });
  };

  // Invite a user to a specific chat room.
  Firechat.prototype.inviteUser = function(userId, confId, roomId) {
    var self = this,
        sendInvite = function() {
          var inviteRef = self._firebase.child('users').child(userId).child('invites').push();
          inviteRef.set({
            id: inviteRef.key(),
            fromUserId: self._userId,
            fromUserName: self._userName,
            confId: confId,
            roomId: roomId
          });

          // Handle listen unauth / failure in case we're kicked.
          inviteRef.on('value', self._onFirechatInviteResponse, function(){}, self);
        };

    if (!self._user) {
      self._onAuthRequired();
      return;
    }

    self.getRoom(confId, roomId, function(room) {
      if (room.type === 'PRIVATE') {
        var authorizedUserRef = self.getRoomRef(confId, roomId).child('authorizedUsers');
        authorizedUserRef.child(userId).set(true, function(error) {
          if (!error) {
            sendInvite();
          }
        });
      } else {
        sendInvite();
      }
    });
  };

  Firechat.prototype.logout = function() {
    this._firebase.unauth();
    Firebase.goOffline();
  };

  /**
   * TODO: REFACTOR OR DELETE FUNCTIONALITY
   */
    // Warn a user for violating the terms of service or being abusive.
  Firechat.prototype.warnUser = function(userId) {
    var self = this;

    self.sendSuperuserNotification(userId, 'warning');
  };

  // Suspend a user by putting the user into read-only mode for a period.
  Firechat.prototype.suspendUser = function(userId, timeLengthSeconds, cb) {
    var self = this,
        suspendedUntil = new Date().getTime() + 1000*timeLengthSeconds;

    self._suspensionsRef.child(userId).set(suspendedUntil, function(error) {
      if (error && cb) {
        return cb(error);
      } else {
        self.sendSuperuserNotification(userId, 'suspension', {
          suspendedUntil: suspendedUntil
        });
        return cb(null);
      }
    });
  };
})(Firebase);

(function($) {


  if (!$ || (parseInt($().jquery.replace(/\./g, ""), 10) < 170)) {
    throw new Error("jQuery 1.7 or later required!");
  }

  var root = this,
      previousFirechatUI = root.FirechatUI;

  root.FirechatUI = FirechatUI;

  var EnumViewStates = {
    NOTIFICATIONS_ALL:    "NOTIFICATIONS_ALL",
    NOTIFICATIONS_ROOM:   "NOTIFICATIONS_ROOM", //Conference specific notifications
    CHAT:                 "CHAT"
  };

  if (!self.FirechatDefaultTemplates) {
    throw new Error("Unable to find chat templates!");
  }

  function FirechatUI(firebaseRef, el, options) {
    var self = this;

    if (!firebaseRef) {
      throw new Error('FirechatUI: Missing required argument `firebaseRef`');
    }

    if (!el) {
      throw new Error('FirechatUI: Missing required argument `el`');
    }

    options = options || {};
    this._options = options;

    this.isVisible = false;
    this.isVisibleMain = true;
    this._el = el;
    this._user = null;
    this._chat = new Firechat(firebaseRef, options);

    //Talkback controls whether a chat stream has reply text area
    if(options.talkback === undefined) {
      this.talkback = true;
    } else {
      this.talkback = options.talkback;
    }

    //Previous rendered message timestamp (sent timestamp)
    this.prevMessageTs = 0;

    // The room in focus
    this._roomInFocus = {
      confId: null,
      roomId: null,
      confName: null,
      roomName: null
    };

    // A list of rooms to enter once we've made room for them (once we've hit the max room limit).
    this._roomQueue = [];

    // Define some constants regarding maximum lengths, client-enforced.
    this.maxLengthUsername = 15;
    this.maxLengthUsernameDisplay = 15;
    this.maxLengthMessage = 512;
    this.maxUserSearchResults = 100;
    this.maxLengthHeader = 60;    //These values just limit size of text, but ellipses are inserted via CSS
    this.maxLengthRoomName = 60;  //These values just limit size of text, but ellipses are inserted via CSS

    this._viewState = EnumViewStates.NOTIFICATIONS_ALL;

    // Define some useful regexes.
    this.urlPattern = /\b(?:https?|ftp):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|]/gim;
    this.pseudoUrlPattern = /(^|[^\/])(www\.[\S]+(\b|$))/gim;

    this._renderLayout();

    // Grab shortcuts to commonly used jQuery elements.
    this.$wrapper = $('#firechat');
    this.$header  = $('#firechat-header');
    this.$notifications = $('#firechat-notifications');
    this.$messages = {};

    // Rate limit messages from a given user with some defaults.
    this.$rateLimit = {
      limitCount: 10,         // max number of events
      limitInterval: 10000,   // max interval for above count in milliseconds
      limitWaitTime: 30000,   // wait time if a user hits the wait limit
      history: {}
    };

    // Setup UI bindings for chat controls.
    this._bindUIEvents();

    // Setup bindings to internal methods
    this._bindDataEvents();
  }

  // Run FirechatUI in *noConflict* mode, returning the `FirechatUI` variable to
  // its previous owner, and returning a reference to the FirechatUI object.
  FirechatUI.noConflict = function noConflict() {
    root.FirechatUI = previousFirechatUI;
    return FirechatUI;
  };

  FirechatUI.prototype = {
    _bindUIEvents: function() {
      // Chat-specific custom interactions and functionality.
      this._bindForHeightChange();
      this._bindForUserRoomList();
      this._bindForUserMuting();
      this._bindForChatInvites();
      this._bindForRoomListing();
      this._bindNotifications();
      this._bindHeader();

      // Generic, non-chat-specific interactive elements.
      this._syncUI();
      this._setupDropdowns();
      this._bindTextInputFieldLimits();
    },

    _bindDataEvents: function() {
      this._chat.on('user-update', this._onUpdateUser.bind(this));

      // Bind events for new messages, enter / leaving rooms, and user metadata.
      this._chat.on('room-enter', this._onEnterRoom.bind(this));
      this._chat.on('room-exit', this._onLeaveRoom.bind(this));
      this._chat.on('message-add', this._onNewMessage.bind(this));
      this._chat.on('card-add', this._onNewCard.bind(this));
      this._chat.on('message-remove', this._onRemoveMessage.bind(this));

      // Bind events related to chat invitations.
      this._chat.on('room-invite', this._onChatInvite.bind(this));
      this._chat.on('room-invite-response', this._onChatInviteResponse.bind(this));

      // Binds events related to admin or moderator notifications.
      this._chat.on('notification', this._onNotification.bind(this));
      this._chat.on('room-users-change', this._onRoomUsersChange.bind(this));
    },

    _renderLayout: function() {
      var template = FirechatDefaultTemplates["templates/layout-full.html"];
      $(this._el).html(template({
        maxLengthUsername: this.maxLengthUsername
      }));
    },
    _onUpdateUser: function(user) {
      // Update our current user state and render latest user name.
      this._user = user;

      // Update our interface to reflect which users are muted or not.
      var mutedUsers = this._user.muted || {};
      $('[data-event="firechat-user-mute-toggle"]').each(function(i, el) {
        var userId = $(this).closest('[data-user-id]').data('user-id');
        $(this).toggleClass('red', !!mutedUsers[userId]);
      });

      // Ensure that all messages from muted users are removed.
      for (var userId in mutedUsers) {
        $('.message[data-user-id="' + userId + '"]').fadeOut();
      }
    },
    _onEnterRoom: function(room) {
      this.joinRoom(room.confId, room.confName, room.roomId, room.roomName);
    },
    _onLeaveRoom: function(confId, roomId) {
      this.exitRoom(confId, roomId);
    },
    _onNewMessage: function(confId, roomId, message) {
      var userId = message.userId;
      if (!this._user || !this._user.muted || !this._user.muted[userId]) {
        this.showMessage(confId, roomId, message);
      }
    },
    _onNewCard: function(confId, roomId, card) {
      this.showCard(confId, roomId, card);
    },
    _onRemoveMessage: function(roomId, messageId) {
      this.removeMessage(roomId, messageId);
    },

    // Events related to chat invitations.
    _onChatInvite: function(invitation) {
      var self = this;
      var template = FirechatDefaultTemplates["templates/prompt-invitation.html"];
      var $prompt = this.prompt('Invite', template(invitation));
      $prompt.find('a.close').click(function() {
        $prompt.remove();
        self._chat.declineInvite(invitation.id);
        return false;
      });

      $prompt.find('[data-toggle=accept]').click(function() {
        $prompt.remove();
        self._chat.acceptInvite(invitation.id);
        return false;
      });

      $prompt.find('[data-toggle=decline]').click(function() {
        $prompt.remove();
        self._chat.declineInvite(invitation.id);
        return false;
      });
    },
    _onChatInviteResponse: function(invitation) {
      if (!invitation.status) return;

      var self = this,
          template = FirechatDefaultTemplates["templates/prompt-invite-reply.html"],
          $prompt;

      if (invitation.status && invitation.status === 'accepted') {
        $prompt = this.prompt('Accepted', template(invitation));
        this._chat.getRoom(invitation.roomId, function(room) {
          self._chat.joinRoom(invitation.confId, invitation.roomId, room.name);
        });
      } else {
        $prompt = this.prompt('Declined', template(invitation));
      }

      $prompt.find('a.close').click(function() {
        $prompt.remove();
        return false;
      });
    },

    // Unread messages notification status change
    _onNotification: function(notifications) {
      console.log('UI: Notification Handler');
      this.renderNotificationBadge(notifications);
      this.renderNotifications(notifications);
    },
    _onRoomUsersChange: function(users){
      console.log('UI: Room users changed');
      this.renderOnlineUsers(users);
    }
  };

  /**
   * Enables or disable ability to reply to the chat
   */
  FirechatUI.prototype.setTalkback = function(isEnabled) {
    this.talkback = isEnabled;
  };

  /**
   * Get backend chat service
   */
  FirechatUI.prototype.getChat = function() {
    return this._chat;
  };

  /**
   * Initialize an authenticated session with a user id and name.
   * This method assumes that the underlying Firebase reference has
   * already been authenticated.
   */
  FirechatUI.prototype.setUser = function(userId, userName) {
    var self = this;
    // Initialize data events
    self._chat.setUser(userId, userName, function(user) {
      self._user = user;

      if (self._chat.userIsModerator()) {
        self._bindSuperuserUIEvents();
      }

      //In the context of a single chat window resuming session does not make sense
      //self._chat.resumeSession();
    });
  };

  /**
   * Exposes internal chat bindings via this external interface.
   */
  FirechatUI.prototype.on = function(eventType, cb) {
    var self = this;
    this._chat.on(eventType, cb);
  };

  /**
   * Binds a custom context menu to messages for superusers to warn or ban
   * users for violating terms of service.
   */
  FirechatUI.prototype._bindSuperuserUIEvents = function() {
    var self = this,
        parseMessageVars = function(event) {
          var $this = $(this),
          messageId = $this.closest('[data-message-id]').data('message-id'),
          userId = $('[data-message-id="' + messageId + '"]').closest('[data-user-id]').data('user-id'),
          roomId = $('[data-message-id="' + messageId + '"]').closest('[data-room-id]').data('room-id');

          return { messageId: messageId, userId: userId, roomId: roomId };
        },
        clearMessageContextMenus = function() {
          // Remove any context menus currently showing.
          $('[data-toggle="firechat-contextmenu"]').each(function() {
            $(this).remove();
          });

          // Remove any messages currently highlighted.
          $('#firechat .message.highlighted').each(function() {
            $(this).removeClass('highlighted');
          });
        },
        showMessageContextMenu = function(event) {
          var $this = $(this),
              $message = $this.closest('[data-message-id]'),
              template = FirechatDefaultTemplates["templates/message-context-menu.html"],
              messageVars = parseMessageVars.call(this, event),
              $template;

          event.preventDefault();

          // Clear existing menus.
          clearMessageContextMenus();

          // Highlight the relevant message.
          $this.addClass('highlighted');

          self._chat.getRoom(messageVars.roomId, function(room) {
            // Show the context menu.
            $template = $(template({
              id: $message.data('message-id')
            }));
            $template.css({
              left: event.clientX,
              top: event.clientY
            }).appendTo(self.$wrapper);
          });
        },
        showCardContextMenu = function(event) { //TODO: Needs to be revisited
          var $this = $(this),
              $message = $this.closest('[data-card-id]'),
              template = FirechatDefaultTemplates["templates/card-context-menu.html"],
              messageVars = parseMessageVars.call(this, event),
              $template;
          event.preventDefault();

          // Clear existing menus.
          clearMessageContextMenus();
          // Highlight the relevant message.
          $this.addClass('highlighted');
          self._chat.getRoom(messageVars.roomId, function(room) {
            // Show the context menu.
            $template = $(template({
              id: $message.data('card-id')
            }));
            $template.css({
              left: event.clientX,
              top: event.clientY
            }).appendTo(self.$wrapper);
          });
        };

    // Handle dismissal of message context menus (any non-right-click click event).
    $(document).bind('click', { self: this }, function(event) {
      if (!event.button || event.button != 2) {
        clearMessageContextMenus();
      }
    });

    // Handle display of message context menus (via right-click on a message).
    $(document).delegate('[data-class="firechat-message"]', 'contextmenu', showMessageContextMenu);
    $(document).delegate('[data-class="firechat-card"]',    'contextmenu', showCardContextMenu);

    // Handle click of the 'Warn User' contextmenu item.
    $(document).delegate('[data-event="firechat-user-warn"]', 'click', function(event) {
      var messageVars = parseMessageVars.call(this, event);
      self._chat.warnUser(messageVars.userId);
    });

    // Handle click of the 'Delete Message' contextmenu item.
    $(document).delegate('[data-event="firechat-message-delete"]', 'click', function(event) {
      var messageVars = parseMessageVars.call(this, event);
      self._chat.deleteMessage(messageVars.roomId, messageVars.messageId);
    });
  };

  /**
   * Binds to height changes in the surrounding div.
   */
  FirechatUI.prototype._bindForHeightChange = function() {
    var self = this,
        $el = $(this._el),
        lastHeight = null;

    setInterval(function() {
      var height = $el.height();
      if (height != lastHeight) {
        lastHeight = height;
        $('.chat').each(function(i, el) {

        });
      }
    }, 500);
  };

  /**
   * Binds user list dropdown per room to populate user list on-demand.
   */
  FirechatUI.prototype._bindForUserRoomList = function() {
    var self = this;

    // Upon click of the dropdown, autofocus the input field and trigger list population.
    $(document).delegate('[data-event="firechat-user-room-list-btn"]', 'click', function(event) {
      event.stopPropagation();
      var $this = $(this),
          template = FirechatDefaultTemplates["templates/room-user-list-item.html"],
          targetId = $this.data('target'),
          $target = $('#' + targetId);

      $target.empty();
      self._chat.getUsersByRoom(self._roomInFocus.confId,self._roomInFocus.roomId, function(users) {
        for (var username in users) {
          user = users[username];
          user.disableActions = true;//(!self._user || user.id === self._user.id);
          user.nameTrimmed = self.trimWithEllipsis(user.name, self.maxLengthUsernameDisplay);
          user.isMuted = (self._user && self._user.muted && self._user.muted[user.id]);
          $target.append($(template(user)));
        }
        self.sortListLexicographically('#' + targetId);
      });
    });
  };

  /**
   * Binds user mute toggles and removes all messages for a given user upon mute.
   */
  FirechatUI.prototype._bindForUserMuting = function() {
    var self = this;
    $(document).delegate('[data-event="firechat-user-mute-toggle"]', 'click', function(event) {
      var $this = $(this),
          userId = $this.closest('[data-user-id]').data('user-id'),
          userName = $this.closest('[data-user-name]').data('user-name'),
          isMuted = $this.hasClass('red'),
          template = FirechatDefaultTemplates["templates/prompt-user-mute.html"];

      event.preventDefault();

      // Require user confirmation for muting.
      if (!isMuted) {
        var $prompt = self.prompt('Mute User?', template({
          userName: userName
        }));

        $prompt.find('a.close').first().click(function() {
          $prompt.remove();
          return false;
        });

        $prompt.find('[data-toggle=decline]').first().click(function() {
          $prompt.remove();
          return false;
        });

        $prompt.find('[data-toggle=accept]').first().click(function() {
          self._chat.toggleUserMute(userId);
          $prompt.remove();
          return false;
        });
      } else {
        self._chat.toggleUserMute(userId);
      }
    });
  };

  /**
   * Binds to elements with the data-event='firechat-user-(private)-invite' and
   * handles invitations as well as room creation and entering.
   */
  FirechatUI.prototype._bindForChatInvites = function() {
    var self = this,
        renderInvitePrompt = function(event) {
          var $this = $(this),
              userId = $this.closest('[data-user-id]').data('user-id'),
              roomId = $this.closest('[data-room-id]').data('room-id'),
              userName = $this.closest('[data-user-name]').data('user-name'),
              template = FirechatDefaultTemplates["templates/prompt-invite-private.html"],
              $prompt;

          self._chat.getRoom(roomId, function(room) {
            $prompt = self.prompt('Invite', template({
              userName: userName,
              roomName: room.name
            }));

            $prompt.find('a.close').click(function() {
              $prompt.remove();
              return false;
            });

            $prompt.find('[data-toggle=decline]').click(function() {
              $prompt.remove();
              return false;
            });

            $prompt.find('[data-toggle=accept]').first().click(function() {
              $prompt.remove();
              self._chat.inviteUser(userId, roomId, room.name);
              return false;
            });
            return false;
          });
          return false;
        },
        renderPrivateInvitePrompt = function(event) {
          var $this = $(this),
              userId = $this.closest('[data-user-id]').data('user-id'),
              userName = $this.closest('[data-user-name]').data('user-name'),
              template = FirechatDefaultTemplates["templates/prompt-invite-private.html"],
              $prompt;

          if (userId && userName) {
            $prompt = self.prompt('Private Invite', template({
              userName: userName,
              roomName: 'Private Chat'
            }));

            $prompt.find('a.close').click(function() {
              $prompt.remove();
              return false;
            });

            $prompt.find('[data-toggle=decline]').click(function() {
              $prompt.remove();
              return false;
            });

            $prompt.find('[data-toggle=accept]').first().click(function() {
              $prompt.remove();
              var roomName = 'Private Chat';
              //TODO: Specify Conference Number, to create private groups
              self._chat.createRoom(roomName, confId, 'PRIVATE', function(confId, roomId) {
                self._chat.inviteUser(userId, confId, roomId, roomName);
              });
              return false;
            });
          }
          return false;
        };

    $(document).delegate('[data-event="firechat-user-chat"]', 'click', renderPrivateInvitePrompt);
    $(document).delegate('[data-event="firechat-user-invite"]', 'click', renderInvitePrompt);
  };

  /**
   * Binds to room dropdown button, menu items, and create room button.
   */
  FirechatUI.prototype._bindForRoomListing = function() {
    var self = this,
        $createRoomPromptButton = $('#firechat-btn-create-room-prompt'),
        $createRoomButton = $('#firechat-btn-create-room'),
        renderRoomList = function(event) {
          var type = $(this).data('room-type');

          self.sortListLexicographically('#firechat-room-list');
        };

    // Handle click of the create new room prompt-button.
    $createRoomPromptButton.bind('click', function(event) {
      self.promptCreateRoom();
      return false;
    });

    // Handle click of the create new room button.
    $createRoomButton.bind('click', function(event) {
      var roomName = $('#firechat-input-room-name').val();
      $('#firechat-prompt-create-room').remove();
      self._chat.createRoom(roomName);
      return false;
    });
  };

  /**
   * A stripped-down version of bootstrap-dropdown.js.
   *
   * Original bootstrap-dropdown.js Copyright 2012 Twitter, Inc., licensed under the Apache v2.0
   */
  FirechatUI.prototype._setupDropdowns = function() {
    var self = this,
        toggle = '[data-toggle=firechat-dropdown]',
        toggleDropdown = function(event) {
          var $this = $(this),
              $parent = getParent($this),
              isActive = $parent.hasClass('open');

          if ($this.is('.disabled, :disabled')) return;

          clearMenus();

          if (!isActive) {
            $parent.toggleClass('open');
          }

          $this.focus();

          return false;
        },
        clearMenus = function() {
          $('[data-toggle=firechat-dropdown]').each(function() {
            getParent($(this)).removeClass('open');
          });
        },
        getParent = function($this) {
          var selector = $this.attr('data-target'),
              $parent;

          if (!selector) {
            selector = $this.attr('href');
            selector = selector && /#/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '');
          }

          $parent = selector && $(selector);

          if (!$parent || !$parent.length) $parent = $this.parent();

          return $parent;
        };

      $(document)
        .bind('click', clearMenus)
        .delegate('.firechat-dropdown-menu', 'click', function(event) { event.stopPropagation(); })
        .delegate('[data-toggle=firechat-dropdown]', 'click', toggleDropdown);
  };

  /**
   * Binds to any text input fields with data-provide='limit' and
   * data-counter='<selector>', and upon value change updates the selector
   * content to reflect the number of characters remaining, as the 'maxlength'
   * attribute less the current value length.
   */
  FirechatUI.prototype._bindTextInputFieldLimits = function() {
    $('body').delegate('input[data-provide="limit"], textarea[data-provide="limit"]', 'keyup', function(event) {
      var $this = $(this),
          $target = $($this.data('counter')),
          limit = $this.attr('maxlength'),
          count = $this.val().length;
      $target.html(Math.max(0, limit - count));
    });
  };

  /**
   * Binding
   * @private
   */
  FirechatUI.prototype._bindNotifications = function() {
    var self = this;

    // Handle transition from conference notifications to room notifications
    $(document).delegate('[data-event="firechat-notification-conf"]', 'click', function(event) {
      //1. Switching to NOTIFICATION ROOM view
      self._viewState = EnumViewStates.NOTIFICATIONS_ROOM;

      //2. Setting current state params
      var $this     = $(this),
          confId    = $this.closest('[data-conf-id]').data('conf-id'),
          confName  = $this.closest('[data-conf-name]').data('conf-name');
      self._roomInFocus.confId = confId;
      self._roomInFocus.confName = confName;

      //3. Render room
      self.renderNotifications();

    });

    // Handle transition from conference notifications to room notifications
    $(document).delegate('[data-event="firechat-notification-room"]', 'click', function(event) {
      //1. Switching to NOTIFICATION ROOM view
      self._viewState = EnumViewStates.CHAT;

      //2. Conference ID
      var $this = $(this),
          confId    = $this.closest('[data-conf-id]').data('conf-id'),
          roomId    = $this.closest('[data-room-id]').data('room-id');

      self._syncUI();
      self._chat.joinRoom(confId,roomId);
    });
  };

  /**
   * Binding header clicks
   * @private
   */
  FirechatUI.prototype._bindHeader = function() {
    var self = this;

    // Handle transition from conference notifications to room notifications
    $(document).delegate('#firechat-header-back', 'click', function(event) {

      switch(self._viewState) {
        //Switching to NOTIFICATION ROOM view
        case EnumViewStates.CHAT:
          //a. New view state
          self._viewState = EnumViewStates.NOTIFICATIONS_ROOM;
          //b. Leave room
          self._chat.leaveRoom(self._roomInFocus.confId, self._roomInFocus.roomId);
          //c. Rendering notifications
          self.renderNotifications(self._roomInFocus.confId);
          break;
        //Switching to ALL NOTIFICATIONS PER CONFERENCE view
        case EnumViewStates.NOTIFICATIONS_ROOM:
          self._viewState = EnumViewStates.NOTIFICATIONS_ALL;
          self.renderNotifications();
          break;
      }
      self._syncUI();
    });
  };

  FirechatUI.prototype._syncUI = function() {
    var self = this;
    self._updateHeader();
    if(self._viewState === EnumViewStates.CHAT) {
      $('#firechat-notifications').hide();
    } else {
      $('#firechat-notifications').show();
      if(self._roomInFocus.confId !== null && self._roomInFocus.roomId !== null) {
        this._chat.leaveRoom(this._roomInFocus.confId, this._roomInFocus.roomId);
      }
    }
  };

  /**
   * Renders unread message notifications based on Firechat defined unread object
   * @param unread
   * @param confIdToShow (optional) conference id to show in ROOM list mode
   */
  FirechatUI.prototype.renderNotificationBadge = function (notifications) {
    var self = this;
    var count = notifications.msgCount + notifications.alertCount;
    var bellCount = $('#firechat-header-notification #notificationBellCount');

    var badgeClass = 'badge-success';
    if(count > 0 && count < 10) {
      badgeClass = 'badge-warning';
    } else  if (count >= 10){
      badgeClass = 'badge-important';
    }

    bellCount
        .removeClass('badge-success badge-important badge-warning')
        .addClass(badgeClass)
        .text(count);
  };

  FirechatUI.prototype._updateHeader = function() {
    var self = this;
    var count = self._chat._notifications.msgCount + self._chat._notifications.alertCount;
    var badgeClass = 'badge-success';
    if(count > 0 && count < 10) {
      badgeClass = 'badge-warning';
    } else  if (count >= 10) {
      badgeClass = 'badge-important';
    }

    var backVisible = true;

    switch (self._viewState) {
      case EnumViewStates.CHAT:
        backVisible = true;
        break;
      case EnumViewStates.NOTIFICATIONS_ALL:
        backVisible = false;
        break;
      case EnumViewStates.NOTIFICATIONS_ROOM:
        backVisible = self.isVisibleMain;
        console.log("Rendering rooms header");
        if ((!self._roomInFocus.confName || self._roomInFocus.confName.length === 0) &&
            self._roomInFocus.confId && self._roomInFocus.confId > 0) {
          self._roomInFocus.confName = self._chat.getConferenceName(self._roomInFocus.confId); //Setting room name
        }
        break;
    }

    // Populate and render the message template
    var headerData = {
      backVisible: backVisible,
      type:     self._viewState,
      userName: self._chat._userName,
      confName: self.trimWithEllipsis(self._roomInFocus.confName, self.maxLengthHeader),
      roomName: self.trimWithEllipsis(self._roomInFocus.roomName, self.maxLengthHeader),
      counter:  count,
      badge:    badgeClass
    };
    var template = FirechatDefaultTemplates["templates/header.html"];
    var header = $(template(headerData));
    if (header) {
      self.$header.children().remove();
      self.$header.append(header);
    }
  };

  /** If currently in chat room, read all messages */
  FirechatUI.prototype.setVisible = function(isVisible) {
    this.isVisible = isVisible;
    if(isVisible && this._viewState === EnumViewStates.CHAT) {
      this._chat.readRoom(this._roomInFocus.confId, this._roomInFocus.roomId);
    }
  };

  FirechatUI.prototype.setVisbleMain = function(isVisible) {
    this.isVisibleMain = isVisible;
  };

  /**
   * Given a title and message content, show an alert prompt to the user.
   *
   * @param    {string}    title
   * @param    {string}    message
   */
  FirechatUI.prototype.renderAlertPrompt = function(title, message) {
    var template = FirechatDefaultTemplates["templates/prompt-alert.html"],
        $prompt = this.prompt(title, template({ message: message }));

      $prompt.find('.close').click(function() {
        $prompt.remove();
        return false;
      });
      return;
  };

  /**
   * External function to force switch communicator to chat window
   * @param confId
   * @param roomId
   */
  FirechatUI.prototype.activateConferenceRoom = function(confId, roomId) {
    this._chat.joinRoom(confId, roomId);
  };

  FirechatUI.prototype.setConfRoom = function(confId) {
    var self = this;
    //a. New view state
    self._viewState = EnumViewStates.NOTIFICATIONS_ROOM;
    //b. Leave room
    if (self._roomInFocus.confId && self._roomInFocus.roomId) {
      self._chat.leaveRoom(self._roomInFocus.confId, self._roomInFocus.roomId);
    }
    //c. Rendering notifications
    self._roomInFocus.confId = confId;
    self._roomInFocus.confName = self._chat.getConferenceName(confId);
    self.renderNotifications(self._roomInFocus.confId);
  };

  /**
   * Resets communicator into initial state
   * @param leaveRooms boolean indicating if all rooms must be closed
   */
  FirechatUI.prototype.resetCommunicator = function(leaveRooms) {
    this._viewState = EnumViewStates.NOTIFICATIONS_ALL;
    this._renderNotificationConfs();

    if(leaveRooms && this._roomInFocus.roomId !== null) {
      this._chat.leaveRoom(this._roomInFocus.confId, this._roomInFocus.roomId);
    }

    this._syncUI();
  };

  /**
   * Generates chat room view
   * @param confId
   * @param confName
   * @param roomId
   * @param roomName
   */
  FirechatUI.prototype.joinRoom = function(confId, confName, roomId, roomName) {
    console.log("UI: Rendering conf: " + confId + " room: " + roomId);
    var self = this;
    //0. Switching state
    self._viewState = EnumViewStates.CHAT;
    self.prevMessageTs = 0;

    //1. If currently chatting in another room, leave that room gracefully
    if(self._roomInFocus.roomId !== null) {
      self._chat.leaveRoom(self._roomInFocus.confId, self._roomInFocus.roomId);
    }

    //2. If already in the room => do nothing
    if(self._roomInFocus.roomId === roomId) {
      return;
    }

    //3. Joining new room
    self._roomInFocus.confId   = confId;
    self._roomInFocus.confName = confName;
    self._roomInFocus.roomId   = roomId;
    self._roomInFocus.roomName = roomName;

    var roomData = {
      confId   : confId,
      confName : confName,
      roomId   : roomId,
      roomName : roomName,
      maxMsgLength: self.maxLengthMessage,
      talkback: self.talkback
    };

    //4. Update Header
    self._updateHeader();

    //5. Populate and render chat room content template.
    var chatRoomTemplate = FirechatDefaultTemplates["templates/room-content.html"];
    var roomContent = $(chatRoomTemplate(roomData));
    this.$header.after(roomContent);
    self.$messages = $('#firechat-chat-messages');

    //6. Attach on-enter event to textarea.
    var $textarea = roomContent.find('textarea').first();
    $textarea.bind('keydown', function(e) {
      var message = self.trimWithEllipsis($textarea.val(), self.maxLengthMessage);
      if ((e.which === 13) && (message !== '') && !e.shiftKey) {
        $textarea.val('');
        self._chat.sendMessage(confId, roomId, message);
        return false;
      }
    });

    //7. Scroll messages to the bottom //TODO: Verify this actually works
    self.$messages.scrollTop(self.$messages[0].scrollHeight);

    //8. Sort each item in the user list alphabetically on click of the dropdown.
    $('#firechat-btn-room-user-list-' + roomId).bind('click', function() {
      self.sortListLexicographically('#firechat-room-user-list-' + roomId);
      return false;
    });

    //9. If dialog is visible read all messages in the room
    if(self.isVisible === true) {
      self._chat.readRoom(confId, roomId);
    }

    //10. Sync UI
    this._syncUI();
  };

  /**
   * Leaves room and cleans up all resources
   * @param confId
   * @param roomId
   */
  FirechatUI.prototype.exitRoom = function(confId, roomId) {
    //1. Deleting selector
    delete this.$messages;

    //2. Room In focus is no longer in focus but conference can be, so it is being kept
    this._roomInFocus.roomId = null;
    this._roomInFocus.roomName = null;

    //3. Remove room messages from DOM
    this.$wrapper.find('.room-content').remove();
  };

  /**
   * Renders unread message notifications based on Firechat defined unread object
   * @param unread
   * @param confIdToShow (optional) conference id to show in ROOM list mode
   */
  FirechatUI.prototype.renderNotifications = function () {
    var self = this;

    switch (self._viewState) {
      case  EnumViewStates.NOTIFICATIONS_ALL:
        self._renderNotificationConfs();
        self._syncUI();
        break;
      case EnumViewStates.NOTIFICATIONS_ROOM:
        self.renderNotificationRooms(self._roomInFocus.confId);
        self._syncUI();
        break;
      default:
        break;
    }
  };

  /**
   * Compare two conferences which one is more important
   * @param a
   * @param b
   * @returns {number}
   */
  FirechatUI.prototype.compareConferences = function (a, b) {
    if (a.alertCount > b.alertCount) {
      return -1;
    }
    else if (b.alertCount > 0) {
      return 1;
    }

    if (a.msgCount > b.msgCount) {
      return -1;
    }
    else if (b.msgCount > 0) {
      return 1;
    }

    if (a.lastActivity) {
      if (b.lastActivity) {
        if (a.lastActivity > b.lastActivity) {
          return -1;
        }
        else {
          return 1;
        }
      }
      else {
        return -1;
      }
    }
    else {
      if (b.lastActivty) {
        return 1;
      }
    }

    if(a.id > b.id) {
      return -1;
    }

    return 1;
  };


  /**
   * Compare two conferences which one is more important
   * @param a
   * @param b
   * @returns {number}
   */
  FirechatUI.prototype.compareRooms = function (a, b) {
    if (a.id && b.id) {
      //console.warn('A ID: ' + a.id + ' (' + a.alertCount + ',' + a.msgCount + ')');
      //console.warn('B ID: ' + b.id + ' (' + b.alertCount + ',' + b.msgCount + ')');
      var isAGeneral = a.id.indexOf('general') > 0;
      var isAInternal = a.id.indexOf('internal') > 0;
      var isBGeneral = b.id.indexOf('general') > 0;
      var isBInternal = b.id.indexOf('internal') > 0;
      if (isAGeneral) {
        return -1;
      }
      if (isBGeneral) {
        return 1;
      }
      if (isAInternal) {
        return -1;
      }
      if (isBInternal) {
        return 1;
      }
    }

    if (a.alertCount > b.alertCount) {
      return -1;
    }
    else if (b.alertCount > 0) {
      return 1;
    }

    if (a.msgCount > b.msgCount) {
      return -1;
    }
    else if (b.msgCount > 0) {
      return 1;
    }

    if (a.lastActivity) {
      if (b.lastActivity) {
        if (a.lastActivity > b.lastActivity) {
          return -1;
        }
        else {
          return 1;
        }
      }
      else {
        return -1;
      }
    }
    else {
      if (b.lastActivty) {
        return 1;
      }
    }

    if(a.id > b.id) {
      return -1;
    }

    return 1;
  };


  FirechatUI.prototype._renderNotificationConfs = function () {
    var self = this,
        notifications = self._chat._notifications;
    console.log("UI: Conference list rendering");

    var cuttoffTime = self._chat.eventCutoffTimestamp();

    if (self._viewState !== EnumViewStates.NOTIFICATIONS_ALL) {
      return;
    }

    var $notifications = self.$notifications;
    if ($notifications) {
      $notifications.children().remove();
    }

    var cns = [];

    for(var confId in notifications.confs) {
      var currConf = notifications.confs[confId];
      currConf.id = confId;
      cns.push(currConf);
    }
    cns.sort(self.compareConferences);

    for(var idx = 0; idx < cns.length; idx++) {
      var confCurr = cns[idx];

      //If conference has no unread events and in fact no events at all and last activity
      //is either not set or expired, then skip the conference
      if(confCurr.eventCount === 0 &&
         (!confCurr.lastActivity || confCurr.lastActivity < cuttoffTime)) {
        continue;
      }

      var notification = {
        confId: confCurr.id,
        confName: self.trimWithEllipsis(confCurr.name, self.maxLengthRoomName),
        msgCount: confCurr.msgCount,
        alertCount: confCurr.alertCount
      };

      // Populate and render the message template.
      var template = FirechatDefaultTemplates["templates/notification-confs.html"];
      var $notification = $(template(notification));

      if ($notifications) {
        $notifications.append($notification);
      }
    }
  };

  FirechatUI.prototype.renderNotificationRooms = function (confIdToShow) {
    var self = this;
    if (self._viewState !== EnumViewStates.NOTIFICATIONS_ROOM) {
      return;
    }
    console.log("UI: Room list rendering for conf: " + confIdToShow);
    var $notifications = self.$notifications;
    if ($notifications) {
      $notifications.children().remove();
    }

    var currConf = self._chat._notifications.confs[confIdToShow];
    if (currConf && currConf.rooms) {
      var roomsForDisplay = [];
      for (var roomId in currConf.rooms) {
        var currRoom = currConf.rooms[roomId];
        currRoom.id = roomId;
        roomsForDisplay.push(currRoom);
      }
      roomsForDisplay.sort(self.compareRooms);
      for (var r4dIdx in roomsForDisplay) {
        var r4d = roomsForDisplay[r4dIdx];
        var notification = {
          confId: confIdToShow,
          roomId: r4d.id,
          roomName: self.trimWithEllipsis(r4d.name, self.maxLengthRoomName),
          msgCount: r4d.msgCount,
          alertCount: r4d.alertCount
        };

        // Populate and render the message template.
        var template = FirechatDefaultTemplates["templates/notification-rooms.html"];
        var $notification = $(template(notification));
        if ($notifications) {
          $notifications.append($notification);
        }
      }
    } else {
      //Messenger is not available for some reason for this conference
      var $notice = $('<h2>Messenger is not enabled</h2>');
      $notifications.append($notice);
    }
  };

  FirechatUI.prototype.renderOnlineUsers = function(users) {
    var self = this,
        template = FirechatDefaultTemplates["templates/room-user-list-item.html"],
        $target = $('#firechat-chat-users');

    $target.children().remove();
    for (var username in users) {
      user = users[username];
      user.initials = self.nameInitials(user.name);
      user.userClass = isNaN(user.id)?'agent':'traveller';
      $target.append($(template(user)));
    }
    $target.children().tooltip();
  };

  /**
   * Render a new message in the specified chat room.
   *
   * @param    {string}    roomId
   * @param    {string}    message
   */
  FirechatUI.prototype.showMessage = function(confId, roomId, rawMessage) {
    var self = this,
        cards = self._chat.getCardsInRange(confId, roomId, self.prevMessageTs, rawMessage.timestamp);

    self.prevMessageTs = rawMessage.timestamp;
    for(var cardId in cards) {
      if (cards[cardId].logo) {
        self.showCard(confId, roomId, cards[cardId]);
      }
    }
// Setup defaults
    var message = {
      id              : rawMessage.id,
      localdate       : self.formatDate(rawMessage.timestamp),
      localtime       : self.formatTime(rawMessage.timestamp),
      message         : rawMessage.message || '',
      userId          : rawMessage.userId,
      name            : rawMessage.name,
      type            : rawMessage.type || 'NORMAL',
      isSelfMessage   : (self._user && rawMessage.userId == self._user.id),
      disableActions  : true //(!self._user || rawMessage.userId == self._user.id)
    };


    // While other data is escaped in the Underscore.js templates, escape and
    // process the message content here to add additional functionality (add links).
    // Also trim the message length to some client-defined maximum.
    message.message = _.map(message.message.split(' '), function(token) {
      if (self.urlPattern.test(token) || self.pseudoUrlPattern.test(token)) {
        return self.linkify(encodeURI(token));
      } else {
        return _.escape(token);
      }

    }).join(' ');
    //Just in case allowing 2x length of the max human entered message
    message.message = self.trimWithEllipsis(message.message, self.maxLengthMessage * 2);
    message.message = marked(message.message);
    // Populate and render the message template.
    var template = FirechatDefaultTemplates["templates/message.html"];
    var $message = $(template(message));
    var $messages = self.$messages;
    if ($messages) {
      var scrollToBottom = false;
      if ($messages.scrollTop() / ($messages[0].scrollHeight - $messages[0].offsetHeight) >= 0.95) {
        // Pinned to bottom
        scrollToBottom = true;
      } else if ($messages[0].scrollHeight <= $messages.height()) {
        // Haven't added the scrollbar yet
        scrollToBottom = true;
      }

      $messages.append($message);

      if (scrollToBottom) {
        $messages.scrollTop($messages[0].scrollHeight);
      }
    }

    //We can easily clear all unread messages in the room
    if(self.isVisible === true &&
       self._roomInFocus.roomId === roomId) {
      self._chat.readRoom(confId, roomId, rawMessage.id);
    }
  };

  FirechatUI.prototype.showCard = function(confId, roomId, card) {
    var self = this,
        $messages = self.$messages;

    //Checking if card was already rendered, then skip it
    if($('[data-card-id="'+card.id+'"]').length > 0) {
      return;
    }

    var template = FirechatDefaultTemplates["templates/card.html"];
    var $card = $(template(card));

    if ($messages && Object.keys(self.$messages).length !== 0) {
      var scrollToBottom = false;
      if ($messages.scrollTop() / ($messages[0].scrollHeight - $messages[0].offsetHeight) >= 0.95) {
        // Pinned to bottom
        scrollToBottom = true;
      } else if ($messages[0].scrollHeight <= $messages.height()) {
        // Haven't added the scrollbar yet
        scrollToBottom = true;
      }
      $messages.append($card);
      if (scrollToBottom) {
        $messages.scrollTop($messages[0].scrollHeight);
      }
    }

    //We can easily clear all unread messages in the room
    if(self.isVisible === true &&
       self._roomInFocus.roomId === roomId) {
      self._chat.readRoom(confId, roomId);
      //TODO: Mark Card as READ here
    }
  };

  /**
   * Remove a message by id.
   *
   * @param    {string}    roomId
   * @param    {string}    messageId
   */
  FirechatUI.prototype.removeMessage = function(roomId, messageId) {
    $('.message[data-message-id="' + messageId + '"]').remove();
  };

  /**
   * Given a selector for a list element, sort the items alphabetically.
   *
   * @param    {string}    selector
   */
  FirechatUI.prototype.sortListLexicographically = function(selector) {
    $(selector).children("li").sort(function(a, b) {
        var upA = $(a).text().toUpperCase();
        var upB = $(b).text().toUpperCase();
        return (upA < upB) ? -1 : (upA > upB) ? 1 : 0;
    }).appendTo(selector);
  };

  /**
   * Remove leading and trailing whitespace from a string and shrink it, with
   * added ellipsis, if it exceeds a specified length.
   *
   * @param    {string}    str
   * @param    {number}    length
   * @return   {string}
   */
  FirechatUI.prototype.trimWithEllipsis = function(str, length) {
    if(!str){
      return '';
    }

    str = str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    return (length && str.length > length) ? str.substring(0, length) + '...' :  str ;
  };

  /**
   * Remove leading and trailing whitespace from a string and shrink it, with
   * added ellipsis, if it exceeds a specified length.
   *
   * @param    {string}    str
   * @param    {number}    length
   * @return   {string}
   */
  FirechatUI.prototype.nameInitials = function(name) {
    return name.replace(/\W*(\w)\w*/g, '$1').toUpperCase();
  };

  /**
   * Given a timestamp, format it in the form hh:mm am/pm. Defaults to now
   * if the timestamp is undefined.
   *
   * @param    {Number}    timestamp
   * @param    {string}    date
   */
  FirechatUI.prototype.formatTime = function(timestamp) {
    var date = (timestamp) ? new Date(timestamp) : new Date(),
        hours = date.getHours() || 12,
        minutes = '' + date.getMinutes(),
        ampm = (date.getHours() >= 12) ? 'pm' : 'am';

    hours = (hours > 12) ? hours - 12 : hours;
    minutes = (minutes.length < 2) ? '0' + minutes : minutes;
    return '' + hours + ':' + minutes + ampm;
  };

  /**
   * Formats date to display within the message
   * @param timestamp
   * @returns {string}
   */
  FirechatUI.prototype.formatDate = function(timestamp) {
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var date = (timestamp) ? new Date(timestamp) : new Date(),
        month = months[date.getMonth()],
        day = date.getDate(),
        year = date.getFullYear();

    return month + ' ' + day + ', ' + year;
  };

  /**
   * Launch a prompt to allow the user to create a new room.
   */
  FirechatUI.prototype.promptCreateRoom = function() {
    var self = this;
    var template = FirechatDefaultTemplates["templates/prompt-create-room.html"];

    var $prompt = this.prompt('Create Public Room', template({
      maxLengthRoomName: this.maxLengthRoomName,
      isModerator: self._chat.userIsModerator()
    }));
    $prompt.find('a.close').first().click(function() {
      $prompt.remove();
      return false;
    });


    $prompt.find('[data-toggle=submit]').first().click(function() {
      var name = $prompt.find('[data-input=firechat-room-name]').first().val();
      if (name !== '') {
        self._chat.createRoom(name, 'PUBLIC');
        $prompt.remove();
      }
      return false;
    });

    $prompt.find('[data-input=firechat-room-name]').first().focus();
    $prompt.find('[data-input=firechat-room-name]').first().bind('keydown', function(e) {
      if (e.which === 13) {
        var name = $prompt.find('[data-input=firechat-room-name]').first().val();
        if (name !== '') {
          self._chat.createRoom(name, 'PUBLIC');
          $prompt.remove();
          return false;
        }
      }
    });
  };

  /**
   * Inner method to launch a prompt given a specific title and HTML content.
   * @param    {string}    title
   * @param    {string}    content
   */
  FirechatUI.prototype.prompt = function(title, content) {
    var template = FirechatDefaultTemplates["templates/prompt.html"],
        $prompt;

    $prompt = $(template({
      title: title,
      content: content
    })).css({
      top: this.$wrapper.position().top + (0.333 * this.$wrapper.height()),
      left: this.$wrapper.position().left + (0.125 * this.$wrapper.width()),
      width: 0.75 * this.$wrapper.width()
    });
    this.$wrapper.append($prompt.removeClass('hidden'));
    return $prompt;
  };

  // see http://stackoverflow.com/questions/37684/how-to-replace-plain-urls-with-links
  FirechatUI.prototype.linkify = function(str) {
    var self = this;
    return str
      .replace(self.urlPattern, '<a target="_blank" href="$&">$&</a>')
      .replace(self.pseudoUrlPattern, '$1<a target="_blank" href="http://$2">$2</a>');
  };

})(jQuery);


var initCommunicator = function(options){
  console.log("Initializing communicator");
  var comm = Object.create(Communicator);
  comm.init(options);
  return comm;
};

var Communicator = {

  init: function(options) {
    var self = this;
    self._options = options || {};

    self.fb = new Firebase(self._options.dbUri);
    self.auth = null;
    self.chat = null;
    self.notificator = self._options.notificator;
    self.updateUrl = self._options.msgSentUpdateUrl;

    self.chat = new FirechatUI(self.fb, document.getElementById('communicator'), {
      talkback: self._options.talkback
    });

    this.fb.authWithCustomToken(self._options.authToken, function(error, authData) {
      if (error) {
        console.log("Login Failed!", error);
      } else {
        console.log("Login Succeeded!", authData);
        self.chat.setUser(authData.uid, authData.auth.displayName);
        this.auth = authData;
      }
    });

    self.chat.on("notification", function(notifications){
      if(self.notificator) {
        self.notificator.setCount(notifications.msgCount + notifications.alertCount);
      }
    });

    if(self.updateUrl) {
      self.chat.getChat().on("message-sent", function(msgData) {
        $.ajax({
           method: "POST",
           url: self.updateUrl,
           data: JSON.stringify(msgData),
           contentType: "application/json; charset=utf-8"
         })
         .always(function(data, textStatus, jqXHR) {
           if (textStatus == 'success' && data) {
             //Do nothing for now
           }
           else if (textStatus != 'abort') {
             console.error('Communicator: new message server update failure: ' + textStatus + ". Code: " + data.status);
           }
         });
      });
    }

    //debugger;
    var height = document.body.clientHeight;
    var width = document.body.clientWidth;
    console.log("Width: " + width + " height:" + height);
  },

  logout: function() {
    this.fb.unauth();
  },

  setConfRoomList: function(confId) {
    var self = this;

    self.chat.setConfRoom(confId);
  },

  setConfRoom: function(confId, roomId) {
    var self = this;

    self.chat.activateConferenceRoom(confId, roomId);
  },

  resetUI: function(){
    var self = this;
    self.chat.resetCommunicator(true);
  }
};


