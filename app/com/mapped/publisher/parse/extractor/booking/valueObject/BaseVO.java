package com.mapped.publisher.parse.extractor.booking.valueObject;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

/**
 * Base class for different value objects.
 * <p/>
 * Created by surge on 2015-05-18.
 */
public class BaseVO
    implements Serializable {

  private final static String PROPERTIES_DELIMITER = ": ";

  protected String src; //Foreign system
  protected long   srcId; //pk of the booking in the import table

  /**
   * Main name of the value object
   */
  protected String name;
  /**
   * Notes for the value object
   */
  protected String note;
  protected String recordLocator;
  protected int rank = -1;

  protected boolean commentAsContainer;

  protected String    origSrcPK;

  /**
   * A variety of properties that can be later saved as a note or some type of Key-Value store.
   */
  protected Map<String, String> properties;
  private String id;

  private String tag;

  public BaseVO() {
    this(true);
  }

  public BaseVO(boolean commentAsContainer) {
    properties = new TreeMap<>();
    this.commentAsContainer = commentAsContainer;
  }

  public boolean isCommentAsContainer() {
    return commentAsContainer;
  }

  public String getId() {
    return id;
  }

  public BaseVO setId(String id) {
    this.id = id;
    return this;
  }

  public Map<String, String> getProperties() {
    return properties;
  }

  public void addProperty(String name, String value) {
    if (name == null || value == null) {
      return;
    }

    properties.put(name, value);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public void appendNote(String note) {
    this.note += (" \n" + note);
  }

  public String getFullNotes() {
    StringBuilder sb = new StringBuilder();

    if (note != null && note.trim().length() > 0) {
      sb.append(StringUtils.trim(note));
      sb.append('\n');
    }
    if (properties.size() > 0) {
      sb.append("\n");
      sb.append(propertiesAsNote());
    }

    return sb.toString();
  }

  /**
   * Prepares properties as delimiter separated lines of text
   *
   * @return All properties, one propertly/value pair per line, no headers or footers
   */
  public String propertiesAsNote() {
    StringBuilder sb = new StringBuilder();

    for (String name : properties.keySet()) {
      String value = properties.get(name);
      if (name.endsWith(":")) {
        sb.append(name.substring(0,name.lastIndexOf(":")));
      } else {
        sb.append(name);
      }
      sb.append(PROPERTIES_DELIMITER);
      sb.append(value);
      sb.append('\n');
    }

    return sb.toString();
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  public String getSrc() {
    return src;
  }

  public void setSrc(String src) {
    this.src = src;
  }

  public long getSrcId() {
    return srcId;
  }

  public void setSrcId(long srcId) {
    this.srcId = srcId;
  }

  public String getRecordLocator() {
    return recordLocator;
  }

  public void setRecordLocator(String recordLocator) {
    this.recordLocator = recordLocator;
  }

  public String getOrigSrcPK() {
    return origSrcPK;
  }

  public void setOrigSrcPK(String origSrcPK) {
    this.origSrcPK = origSrcPK;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }
}
