package com.umapped.external.wetu.itinerary.details;

/**
 * Created by george on 2017-06-05.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "content_entity_id"
})
public class AlternateAccommodation {
  @JsonProperty("content_entity_id")
  private int contentEntityId;
  @JsonIgnore
  private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  @JsonProperty("content_entity_id")
  public int getContentEntityId() {
    return contentEntityId;
  }

  @JsonProperty("content_entity_id")
  public void setContentEntityId(int contentEntityId) {
    this.contentEntityId = contentEntityId;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(contentEntityId).append(additionalProperties).toHashCode();
  }

  @Override
  public boolean equals(Object other) {
    if (other == this) {
      return true;
    }
    if ((other instanceof AlternateAccommodation) == false) {
      return false;
    }
    AlternateAccommodation rhs = ((AlternateAccommodation) other);
    return new EqualsBuilder().append(contentEntityId, rhs.contentEntityId).append(additionalProperties, rhs.additionalProperties).isEquals();
  }
}
