
package com.mapped.publisher.parse.iceportal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="brochure" type="{http://services.iceportal.com/service}PropertyBrochure" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "brochure"
})
@XmlRootElement(name = "CreateBrochure")
public class CreateBrochure {

    protected PropertyBrochure brochure;

    /**
     * Gets the value of the brochure property.
     * 
     * @return
     *     possible object is
     *     {@link PropertyBrochure }
     *     
     */
    public PropertyBrochure getBrochure() {
        return brochure;
    }

    /**
     * Sets the value of the brochure property.
     * 
     * @param value
     *     allowed object is
     *     {@link PropertyBrochure }
     *     
     */
    public void setBrochure(PropertyBrochure value) {
        this.brochure = value;
    }

}
