package com.mapped.publisher.parse.extractor.booking.valueObject;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-10-25
 * Time: 9:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class AirportVO
    extends BaseVO {
  private String code;
  private String terminal;
  private String city;
  private String country;

  /**
   * @return the city
   */
  public String getCity() {
    return city;
  }

  /**
   * @param city the city to set
   */
  public void setCity(String city) {
    this.city = city;
  }

  /**
   * @return the country
   */
  public String getCountry() {
    return country;
  }

  /**
   * @param country the country to set
   */
  public void setCountry(String country) {
    this.country = country;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getTerminal() {
    return terminal;
  }

  public void setTerminal(String terminal) {
    this.terminal = terminal;
  }

  public String toString() {
    String nl = System.getProperty("line.separator");
    StringBuilder strBuilder = new StringBuilder(">>> Airport (Value Object)" + nl);
    strBuilder.append("Code:" + code + nl);
    strBuilder.append("Terminal:" + terminal + nl);
    strBuilder.append("Name:" + name + nl);
    strBuilder.append("City:" + city + nl);
    strBuilder.append("Country:" + country + nl);

    return strBuilder.toString();
  }

}

