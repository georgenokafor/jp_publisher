package com.umapped.itinerary.analyze;

import com.umapped.itinerary.analyze.model.Itinerary;
import com.umapped.itinerary.analyze.segment.TripSegment;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionTree;
import com.umapped.itinerary.analyze.segment.decisiontree.impl.AccommodationDecisionTree;
import com.umapped.itinerary.analyze.segment.decisiontree.impl.ActivityDecisionTree;
import com.umapped.itinerary.analyze.segment.decisiontree.impl.ContentDecisionTree;
import com.umapped.itinerary.analyze.segment.decisiontree.impl.TransferDecisionTree;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeatureDetectorRegistry;
import com.umapped.itinerary.analyze.segment.AnalyzerContext;
import com.umapped.itinerary.analyze.segment.TripSegmentAnalyzeResult;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by wei on 2017-03-29.
 */
public class ItineraryAnalyzer {
  private List<DecisionTree> decisionTrees;

  protected SegmentFeatureDetectorRegistry segmentFeatureDetectorRegistry;

  public ItineraryAnalyzer(SegmentFeatureDetectorRegistry registry) {
    this.segmentFeatureDetectorRegistry = registry;
    decisionTrees = new ArrayList<>();
    decisionTrees.add(new AccommodationDecisionTree());
    decisionTrees.add(new TransferDecisionTree());
    decisionTrees.add(new ActivityDecisionTree());
    decisionTrees.add(new ContentDecisionTree());
  }

  public ItineraryAnalyzeResult analyze(Itinerary itinerary, List<TripSegment> tripSegments) {
    LinkedList<TripSegmentAnalyzeResult> segmentResults = new LinkedList<>();

    TripSegment p = null, c = null, n = null;

    int total = tripSegments.size();
    for (int i = 0; i < total; i++) {
      p = c;
      c = tripSegments.get(i);
      n = (i == total - 1) ? null : tripSegments.get(i + 1);
      AnalyzerContext context = new AnalyzerContext(segmentFeatureDetectorRegistry);
      List<DecisionResult> decisions = new ArrayList<>();
      context.setup(c, p, n);
      for (DecisionTree dt : decisionTrees) {
        decisions.addAll(dt.decide(context));
      }
      TripSegmentAnalyzeResult segmentResult = new TripSegmentAnalyzeResult();
      segmentResult.setSegment(c)
                   .setDecisionResults(decisions)
                   .setFeatureValues(context.getFeatureValues());
      segmentResults.add(segmentResult);
    }
    ItineraryAnalyzeResult itineraryAnalyzeResult = new ItineraryAnalyzeResult();
    itineraryAnalyzeResult.setItinerary(itinerary);
    itineraryAnalyzeResult.setSegmentResults(segmentResults);
    itineraryAnalyzeResult.setItineraryId(itinerary.getUmItineraryId());
    return itineraryAnalyzeResult;
  }
}
