package com.umapped.itinerary.analyze.supplier.shoretrip;

import com.google.inject.Inject;
import com.umapped.itinerary.analyze.TripFeature;
import com.umapped.itinerary.analyze.model.Location;
import com.umapped.itinerary.analyze.model.StayPeriod;
import com.umapped.itinerary.analyze.recommendation.RecommendedItem;
import com.umapped.itinerary.analyze.segment.decisiontree.DecisionResult;
import com.umapped.itinerary.analyze.segment.feature.SegmentFeature;
import com.umapped.itinerary.analyze.supplier.ActivityProvider;
import com.umapped.itinerary.analyze.supplier.ProviderHelper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Created by wei on 2017-04-20.
 */
public class ShoreTripProvider implements ActivityProvider {

  private ShoreTripWebService shoreTripWebService;
  private ProviderHelper providerHelper;

  @Inject
  public ShoreTripProvider(ShoreTripWebService shoreTripWebService, ProviderHelper providerHelper) {
    this.shoreTripWebService = shoreTripWebService;
    this.providerHelper = providerHelper;
  }

  @Override
  public List<RecommendedItem> findActivities(DecisionResult decisionResult,
                                              EnumMap<TripFeature, Object> tripFeatures,
                                              EnumMap<SegmentFeature, Object> segmentFeatures) {
    Location location = providerHelper.findLocation(decisionResult);
    if (location == null) {
      return Collections.emptyList();
    }


    LocalDate startDate = decisionResult.getStartDate();
    LocalDate endDate = decisionResult.getEndDate();
    StayPeriod period = new StayPeriod(startDate, endDate);

    return shoreTripWebService.getRecommnededActivity(period, location);
  }
}
