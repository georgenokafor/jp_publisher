package com.mapped.publisher.msg.flightstat.status;

/**
 * Created by twong on 2015-02-10.
 */
public class RequestField {
  private String requestedCode;
  private String fsCode;

  private String requested;
  private String interpreted;

  private String year;
  private String month;
  private String day;

  private String error;

  public String getRequestedCode() {
    return requestedCode;
  }

  public void setRequestedCode(String requestedCode) {
    this.requestedCode = requestedCode;
  }

  public String getFsCode() {
    return fsCode;
  }

  public void setFsCode(String fsCode) {
    this.fsCode = fsCode;
  }

  public String getRequested() {
    return requested;
  }

  public void setRequested(String requested) {
    this.requested = requested;
  }

  public String getInterpreted() {
    return interpreted;
  }

  public void setInterpreted(String interpreted) {
    this.interpreted = interpreted;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public String getMonth() {
    return month;
  }

  public void setMonth(String month) {
    this.month = month;
  }

  public String getDay() {
    return day;
  }

  public void setDay(String day) {
    this.day = day;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }
}
