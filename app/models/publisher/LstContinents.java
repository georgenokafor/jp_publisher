package models.publisher;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by surge on 2014-10-21.
 */
@Entity @Table(name = "lst_continents")
public class LstContinents
    extends Model {
  public static Model.Finder<String, LstContinents> find = new Finder(LstContinents.class);

  @Id @Column(name = "continent_id")
  private Integer continentId;

  @Column(name = "names")
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getContinentId() {
    return continentId;
  }

  public void setContinentId(Integer continentId) {
    this.continentId = continentId;
  }
}
