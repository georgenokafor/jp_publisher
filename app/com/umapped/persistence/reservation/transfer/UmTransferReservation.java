package com.umapped.persistence.reservation.transfer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.umapped.persistence.reservation.UmPerson;
import com.umapped.persistence.reservation.UmRentalCar;
import com.umapped.persistence.reservation.UmReservation;

/**
 * PUB-1147 Requirements for Rental Car
 * <p>
 * Voucher (ex: booked through auto europe there will be a voucher number with auto europe and then a confirmation with
 * the car rental agency)
 * Company:
 * Car Class:
 * Automatic/Standard:
 * GPS:
 * AC:
 * Doors (2 or 4):
 * Seats (fits number of passengers):
 * Fits number of Luggage pieces:
 * Daily Cost:
 * Taxes & Fees:
 * Total Cost:
 * Currency:
 * Paid or Pay at Rental Counter:
 * Rental Terms & Conditions:
 * Additional Instructions:
 * <p>
 * <p>
 * PUB-1148 Requirements for general transfers:
 * Private or Shared
 * Passengers (adults and children)
 * Cost
 * Taxes
 * Total
 * Gratuity Included
 * Type of Vehicle
 * Prepaid or Payable there
 * Additional Instructions
 * re pub-1360
 * Driver Name
 * Driver Phone Number -
 * Transportation Company -
 * Transportation Contact Info (Phone Number) -
 * <p>
 * Created by surge on 2016-11-25.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmTransferReservation
    extends UmReservation {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  /**
   * For types of transfer other than car Rental
   */
  private UmTransfer transfer;
  /****************************************************************************
   * Fields from ticket: PUB-1148 (Car Rental Specific)                       *
   ****************************************************************************/
  /**
   * Information about the driver
   */
  private UmPerson   driver;
  private Boolean  shared;
  private Integer  adults;
  private Integer  children;
  private Boolean  gratuityIncluded;
  private String   vehicleType;


  /****************************************************************************
   * Fields from ticket: PUB-1147 (Car Rental Specific)                       *
   ****************************************************************************/
  private UmRentalCar rental;
  private String    voucher;
  private String    carClass;
  private Boolean   automatic;
  private Boolean   gps;
  private Boolean   ac;
  private Integer   doorCount;
  private Integer   seatCount;
  private Integer   luggage;
  private Boolean   prepaid; //Common with PUB-1148
  private String    terms;
  private String    instructions; //Common with PUB-1147

  public Boolean getShared() {
    return shared;
  }

  public UmTransferReservation setShared(Boolean shared) {
    this.shared = shared;
    return this;
  }

  public Integer getAdults() {
    return adults;
  }

  public UmTransferReservation setAdults(Integer adults) {
    this.adults = adults;
    return this;
  }

  public Integer getChildren() {
    return children;
  }

  public UmTransferReservation setChildren(Integer children) {
    this.children = children;
    return this;
  }

  public Boolean getGratuityIncluded() {
    return gratuityIncluded;
  }

  public UmTransferReservation setGratuityIncluded(Boolean gratuityIncluded) {
    this.gratuityIncluded = gratuityIncluded;
    return this;
  }

  public String getVehicleType() {
    return vehicleType;
  }

  public UmTransferReservation setVehicleType(String vehicleType) {
    this.vehicleType = vehicleType;
    return this;
  }

  public UmRentalCar getRental() {
    return rental;
  }

  public UmTransferReservation setRental(UmRentalCar rental) {
    this.rental = rental;
    return this;
  }

  public String getVoucher() {
    return voucher;
  }

  public UmTransferReservation setVoucher(String voucher) {
    this.voucher = voucher;
    return this;
  }

  public String getCarClass() {
    return carClass;
  }

  public UmTransferReservation setCarClass(String carClass) {
    this.carClass = carClass;
    return this;
  }

  public Boolean getAutomatic() {
    return automatic;
  }

  public UmTransferReservation setAutomatic(Boolean automatic) {
    this.automatic = automatic;
    return this;
  }

  public Boolean getGps() {
    return gps;
  }

  public UmTransferReservation setGps(Boolean gps) {
    this.gps = gps;
    return this;
  }

  public Boolean getAc() {
    return ac;
  }

  public UmTransferReservation setAc(Boolean ac) {
    this.ac = ac;
    return this;
  }

  public Integer getDoorCount() {
    return doorCount;
  }

  public UmTransferReservation setDoorCount(Integer doorCount) {
    this.doorCount = doorCount;
    return this;
  }

  public Integer getSeatCount() {
    return seatCount;
  }

  public UmTransferReservation setSeatCount(Integer seatCount) {
    this.seatCount = seatCount;
    return this;
  }

  public Integer getLuggage() {
    return luggage;
  }

  public UmTransferReservation setLuggage(Integer luggage) {
    this.luggage = luggage;
    return this;
  }

  public Boolean getPrepaid() {
    return prepaid;
  }

  public UmTransferReservation setPrepaid(Boolean prepaid) {
    this.prepaid = prepaid;
    return this;
  }

  public String getTerms() {
    return terms;
  }

  public UmTransferReservation setTerms(String terms) {
    this.terms = terms;
    return this;
  }

  public String getInstructions() {
    return instructions;
  }

  public UmTransferReservation setInstructions(String instructions) {
    this.instructions = instructions;
    return this;
  }

  public UmPerson getDriver() {
    return driver;
  }

  public UmTransferReservation setDriver(UmPerson driver) {
    this.driver = driver;
    return this;
  }

  public UmTransfer getTransfer() {
    if (transfer == null) {
      transfer = new UmTransfer();
    }
    return transfer;
  }

  public UmTransferReservation setTransfer(UmTransfer transfer) {
    this.transfer = transfer;
    return this;
  }
}
