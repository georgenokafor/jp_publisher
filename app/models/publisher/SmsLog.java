package models.publisher;

import com.avaje.ebean.Model;
import com.mapped.persistence.util.DBConnectionMgr;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * SMS Messages sent and received
 * <p>
 * Created by surge on 2016-02-09.
 */
@Entity
@Table(name = "sms_log")
public class SmsLog
    extends Model {
  public static Model.Finder<Long, SmsLog> find = new Finder(SmsLog.class);

  @Id
  Long pk;
  /**
   * True for outgoing, False for incoming
   */
  Boolean   outgoing;
  /**
   * Account of the source message
   */
  Long      fromUid;
  /**
   * Source MSISDN
   */
  String    fromMsisdn;
  /**
   * Account of the destination message
   */
  Long      destUid;
  /**
   * Destination MSISDN
   */
  String    destMsisdn;
  /**
   * Which external API was used to submit SMS (ids as used by Clickatell)
   */
  String    apiId;
  /**
   * Unique message identifier
   */
  String    apiMessageId;
  /**
   * For incoming SMS message
   */
  String    cliMessageId;
  /**
   * Cost of delivery for this message
   */
  Float     cost;
  /**
   * Contents of the text message
   */
  String    contents;
  /**
   * Delivery status
   */
  String    status;
  /**
   * Time when status was updated by SMS service provider
   */
  Long      statusTs;
  /**
   * Trip associated with the SMS
   */
  String    tripId;
  /**
   * NULL for general trip groups, detailsid for specific group
   */
  String    tripDetailId;
  /**
   * 8-byte (same as long) timestamp, but actually can be easily visible in DB
   */
  Timestamp createdTs;
  /**
   * Account who originated creation of this SMS message
   */
  Long      createdBy;
  /**
   * Human friendly timestamp
   */
  Timestamp modifiedTs;
  /**
   * Account that modified the record
   */
  Long      modifiedBy;
  @Version
  int version;

  public static SmsLog buildReply(SmsLog orig, Long accountId) {
    SmsLog sl = build(accountId);
    sl.setOutgoing(false);
    sl.setTripId(orig.getTripId());
    sl.setTripDetailId(orig.getTripDetailId());
    sl.setFromMsisdn(orig.getDestMsisdn());
    sl.setFromUid(orig.destUid);
    return sl;
  }

  public static SmsLog build(Long accountId) {
    SmsLog sl = new SmsLog();
    sl.setPk(DBConnectionMgr.getUniqueLongId());
    sl.setCreatedTs(Timestamp.from(Instant.now()));
    sl.setCreatedBy(accountId);
    sl.setModifiedBy(accountId);
    sl.setModifiedTs(sl.createdTs);
    return sl;
  }

  public String getTripId() {
    return tripId;
  }

  public void setTripId(String tripId) {
    this.tripId = tripId;
  }

  public String getTripDetailId() {
    return tripDetailId;
  }

  public void setTripDetailId(String tripDetailId) {
    this.tripDetailId = tripDetailId;
  }

  /**
   * Tries to find the very last message sent to specified MSISDN
   *
   * @param msisdn
   * @return
   */
  public static SmsLog findLastTo(String msisdn) {
    return find.where().eq("dest_msisdn", msisdn).orderBy("created_ts DESC").setMaxRows(1).findUnique();
  }

  public static SmsLog findByMessageId(String apiMessageId) {
    return find.where().eq("api_message_id", apiMessageId).findUnique();
  }

  public Long getFromUid() {
    return fromUid;
  }

  public SmsLog setFromUid(Long fromUid) {
    this.fromUid = fromUid;
    return this;
  }

  public String getFromMsisdn() {
    return fromMsisdn;
  }

  public SmsLog setFromMsisdn(String fromMsisdn) {
    this.fromMsisdn = fromMsisdn;
    return this;
  }

  public Long getDestUid() {
    return destUid;
  }

  public SmsLog setDestUid(Long destUid) {
    this.destUid = destUid;
    return this;
  }

  public String getDestMsisdn() {
    return destMsisdn;
  }

  public SmsLog setDestMsisdn(String destMsisdn) {
    this.destMsisdn = destMsisdn;
    return this;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public SmsLog setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
    return this;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public SmsLog setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public SmsLog setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
    return this;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public SmsLog setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
    return this;
  }

  public void markModified(Long accountId) {
    setModifiedBy(accountId);
    setModifiedTs(Timestamp.from(Instant.now()));
  }

  public Long getStatusTs() {
    return statusTs;
  }

  public void setStatusTs(Long statusTs) {
    this.statusTs = statusTs;
  }

  public Long getPk() {
    return pk;
  }

  public void setPk(Long pk) {
    this.pk = pk;
  }

  public Boolean getOutgoing() {
    return outgoing;
  }

  public void setOutgoing(Boolean outgoing) {
    this.outgoing = outgoing;
  }

  public SmsLog setDestMsisdnAndClean(String msisdn) {
    setDestMsisdn(msisdn.replaceAll("[^0-9]", ""));
    return this;
  }

  public String getApiId() {
    return apiId;
  }

  public void setApiId(String apiId) {
    this.apiId = apiId;
  }

  public String getApiMessageId() {
    return apiMessageId;
  }

  public void setApiMessageId(String apiMessageId) {
    this.apiMessageId = apiMessageId;
  }

  public String getCliMessageId() {
    return cliMessageId;
  }

  public void setCliMessageId(String cliMessageId) {
    this.cliMessageId = cliMessageId;
  }

  public Float getCost() {
    return cost;
  }

  public void setCost(Float cost) {
    this.cost = cost;
  }

  public String getContents() {
    return contents;
  }

  public void setContents(String contents) {
    this.contents = contents;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
