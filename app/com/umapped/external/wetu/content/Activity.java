
package com.umapped.external.wetu.content;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "start_time",
    "duration",
    "end_time",
    "time_slot",
    "content_entity_id",
    "simple_item_id",
    "sequence",
    "name",
    "is_highlight",
    "type",
    "reference",
    "prevent_voucher"
})
public class Activity {

    //Day Activity fields
    @JsonProperty("start_time")
    private String startTime;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("end_time")
    private String endTime;
    @JsonProperty("time_slot")
    private String timeSlot;
    @JsonProperty("content_entity_id")
    private int contentEntityId;
    @JsonProperty("simple_item_id")
    private String simpleItemId;
    @JsonProperty("sequence")
    private int sequence;
    @JsonProperty("name")
    private String name;
    @JsonProperty("is_highlight")
    private boolean isHighlight;
    @JsonProperty("type")
    private String type;
    @JsonProperty("reference")
    private String reference;
    @JsonProperty("prevent_voucher")
    private boolean preventVoucher;

    //Pin Activity Fields below (including "name" above)
    @JsonProperty("id")
    private String id;
    @JsonProperty("description")
    private String description;
    @JsonProperty("images")
    private List<Image> images = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("start_time")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("start_time")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    @JsonProperty("end_time")
    public String getEndTime() {
        return endTime;
    }

    @JsonProperty("end_time")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @JsonProperty("time_slot")
    public String getTimeSlot() {
        return timeSlot;
    }

    @JsonProperty("time_slot")
    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    @JsonProperty("content_entity_id")
    public int getContentEntityId() {
        return contentEntityId;
    }

    @JsonProperty("content_entity_id")
    public void setContentEntityId(int contentEntityId) {
        this.contentEntityId = contentEntityId;
    }

    @JsonProperty("simple_item_id")
    public String getSimpleItemId() {
        return simpleItemId;
    }

    @JsonProperty("simple_item_id")
    public void setSimpleItemId(String simpleItemId) {
        this.simpleItemId = simpleItemId;
    }

    @JsonProperty("sequence")
    public int getSequence() {
        return sequence;
    }

    @JsonProperty("sequence")
    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("is_highlight")
    public boolean isIsHighlight() {
        return isHighlight;
    }

    @JsonProperty("is_highlight")
    public void setIsHighlight(boolean isHighlight) {
        this.isHighlight = isHighlight;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("reference")
    public String getReference() {
        return reference;
    }

    @JsonProperty("reference")
    public void setReference(String reference) {
        this.reference = reference;
    }

    @JsonProperty("prevent_voucher")
    public boolean isPreventVoucher() {
        return preventVoucher;
    }

    @JsonProperty("prevent_voucher")
    public void setPreventVoucher(boolean preventVoucher) {
        this.preventVoucher = preventVoucher;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("images")
    public List<Image> getImages() {
        return images;
    }

    @JsonProperty("images")
    public void setImages(List<Image> images) {
        this.images = images;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("startTime", startTime).append("duration", duration).append("endTime", endTime).append("timeSlot", timeSlot).append("contentEntityId", contentEntityId).append("simpleItemId", simpleItemId).append("sequence", sequence).append("name", name).append("isHighlight", isHighlight).append("type", type).append("reference", reference).append("preventVoucher", preventVoucher).append("id", id).append("description", description).append("images", images).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(isHighlight).append(type).append(endTime).append(reference).append(timeSlot).append(startTime).append(preventVoucher).append(simpleItemId).append(duration).append(contentEntityId).append(additionalProperties).append(sequence).append(name).append(id).append(description).append(images).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Activity) == false) {
            return false;
        }
        Activity rhs = ((Activity) other);
        return new EqualsBuilder().append(isHighlight, rhs.isHighlight).append(type, rhs.type).append(endTime, rhs.endTime).append(reference, rhs.reference).append(timeSlot, rhs.timeSlot).append(startTime, rhs.startTime).append(preventVoucher, rhs.preventVoucher).append(simpleItemId, rhs.simpleItemId).append(duration, rhs.duration).append(contentEntityId, rhs.contentEntityId).append(additionalProperties, rhs.additionalProperties).append(sequence, rhs.sequence).append(name, rhs.name).append(id, rhs.id).append(description, rhs.description).append(images, rhs.images).isEquals();
    }

}
