package com.umapped.external.travelbound;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.umapped.external.travelbound.api.TBookingReference;
import com.umapped.external.travelbound.api.TReferenceSource;
import com.umapped.external.travelbound.api.TSearchBookingItemResponse;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.ApplicationLoader;
import play.Environment;
import play.inject.guice.GuiceApplicationBuilder;
import play.inject.guice.GuiceApplicationLoader;
import play.test.Helpers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.running;

/**
 * Created by wei on 2017-05-08.
 */
public class TravelboundWebServiceTest {
  @Inject TravelboundWebService service;

  @Inject Application application;

  private TravelboundBookingMapper mapper;

  private TravelboundAPICredential credential;
  @Before
  public void setup() {
    mapper = new TravelboundBookingMapper();
    Module testModule = new AbstractModule() {
      @Override
      public void configure() {
        // Install custom test binding here
      }
    };

    GuiceApplicationBuilder builder = new GuiceApplicationLoader()
        .builder(new ApplicationLoader.Context(Environment.simple()))
        .overrides(testModule);
    Guice.createInjector(builder.applicationModule()).injectMembers(this);

    Helpers.start(application);

    credential = new TravelboundAPICredential(46607, "044","API@UMAPPED.COM", "PASS");
  }

  @After
  public void teardown() {
    Helpers.stop(application);
  }

//  @Test
//  public void testSearchBookingItem() throws Exception {
//    TBookingReference reference = new TBookingReference();
//    reference.setValue("919563_0");
//    reference.setReferenceSource(TReferenceSource.CLIENT);
//    TSearchBookingItemResponse result = service.getBookingItems(credential, reference); //("2468369");
//    Pair<String, List<Integer>> mapResult = mapper.map(result);
//    System.out.println(mapResult.getRight());
//  }

  @Test
  public void testGetAllCities() throws Exception {
    Map<String, List<String>> countryCityMap = new HashMap<>();
    long t1 = System.currentTimeMillis();
    int cityCount = 0;
    List<String> countries = service.getCountries(credential);

    File cityMap = new File("tb_city_map.csv");
    BufferedWriter writer = new BufferedWriter(new FileWriter(cityMap));
    for (String c : countries) {
      List<String> cities = service.getCities(credential, c);
      countryCityMap.put(c, cities);
      cityCount += cities.size();
      for (String city: cities) {
        writer.write(String.format("%s,%s\n", c, city));
      }
    }
    writer.close();
    long t2 = System.currentTimeMillis();

    System.out.println("Total load time: " + (t2-t1));
    System.out.println("Country Count: " + countries.size());
    System.out.println("city Count: " + cityCount);

  }
}
