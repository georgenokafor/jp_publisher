package com.mapped.publisher.audit.event;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Created by Serguei Moutovkin on 2014-05-21.
 */
@JsonTypeName("TRIP_DOC_ATTACHMENT")
public class AuditTripDocAttachment
    extends AuditEvent {

  //In existing system guides are called "destination"
  public String attachmentId;
  public String sysFileName;
  public String origFileName;

  public AuditTripDocAttachment withAttachementId(String attachementId) {
    this.attachmentId = attachementId;
    return this;
  }

  /**
   * @param fileName system filename (internal name as used by Umapped Publisher)
   */
  public AuditTripDocAttachment withSysFileName(String fileName) {
    this.sysFileName = fileName;
    return this;
  }

  public AuditTripDocAttachment withFileName(String fileName) {
    this.origFileName = fileName;
    return this;
  }
}
