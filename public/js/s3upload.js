(function () {

  window.S3Upload = (function () {

    function S3Upload(options) {
      if (options == null) {
        options = {};
      }
      _.extend(this, options);
      var a = document.getElementById(this.file_dom_selector);
      this.handleFileSelect(a);
    }

    S3Upload.prototype.s3_object_name = 'default_name';

    S3Upload.prototype.s3_sign_put_url = '/signS3put';

    S3Upload.prototype.s3_object_file_name = 'default_file_name';

    S3Upload.prototype.s3_object_caption = 'default_caption';

    S3Upload.prototype.file_dom_selector = '#file_upload';

    S3Upload.prototype.onFinishS3Put = function (public_url) {
      return console.log('base.onFinishS3Put()', public_url);
    };

    S3Upload.prototype.onProgress = function (percent, status) {
      return console.log('base.onProgress()', percent, status);
    };

    S3Upload.prototype.onError = function (status) {
      return console.log('base.onError()', status);
    };

    S3Upload.prototype.getMimeType = function(fileName) {
      var ext = fileName.split('.').pop().toLowerCase();
      switch (ext) {
        case "png":
          return  "image/png";
        case "jpg":
          return  "image/jpeg";
        case "jpeg":
          return  "image/jpeg";
        case "gif":
          return  "image/gif";
        case "doc":
          return  "application/msword";
        case "pdf":
          return  "application/pdf";
        case "docx":
          return  "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        default:
          return null;
      }
    };

    S3Upload.prototype.handleFileSelect = function (file_element) {
      var self = this,
          f, files, output, _i, _len, _results;
      this.onProgress(0, 'Upload started.');
      files = file_element.files;
      output = [];
      _results = [];
      if (files != null && files.length > 0) {
        for (_i = 0, _len = files.length; _i < _len; _i++) {
          f = files[_i];
          _results.push(this.uploadFile(f));
        }
      }
      else {
        f = {name: file_element.value.substring(file_element.value.lastIndexOf("\\") + 1),
          size: 0,  // it's not possible to get file size w/o flash or so
          type: file_element.value.substring(file_element.value.lastIndexOf(".") + 1)
        };

        _results.push(this.uploadFile(f));
      }
      return _results;
    };

    S3Upload.prototype.executeOnSignedUrl = function (file, callback) {
      var this_s3upload, xhr;
      this_s3upload = this;
      xhr = new XMLHttpRequest();
      xhr.open('POST', this.s3_sign_put_url, true);

      xhr.onreadystatechange = function (e) {
        var result;
        if (this.readyState === 4 && this.status === 200) {
          try {
            result = JSON.parse(this.responseText);
          }
          catch (error) {
            this_s3upload.onError('Signing server returned some ugly/empty JSON: "' + this.responseText + '"');
            window.location.href = "/home";
            return false;
          }
          console.log("S3: " + result.signed_request);

          return callback(result.signed_request, decodeURIComponent(result.url));
        }
        else if (this.readyState === 4 && this.status !== 200) {
          return this_s3upload.onError('Could not contact request signing server. Status = ' + this.status);
        }
      };
      return xhr.send();
    };

    S3Upload.prototype.createCORSRequest = function (method, url) {
      var xhr = new XMLHttpRequest();
      if (xhr.withCredentials != null) {
        xhr.open(method, url, true);
      }
      else if (typeof XDomainRequest !== "undefined") {
        xhr = new XDomainRequest();
        xhr.open(method, url);
      }
      else {
        xhr = null;
      }
      return xhr;
    };


    S3Upload.prototype.uploadToS3 = function (file, url, public_url) {
      console.log("S3-1: " + url);
      var self = this,
          xhr = self.createCORSRequest('PUT', url);

      if (!xhr) {
        alert("CORS not supported");
        this.onError('CORS not supported');
      }
      else {
        xhr.onload = function (e) {
          if (xhr.status === 200) {
            self.onProgress(100, 'Upload completed.');
            return self.onFinishS3Put(public_url);
          }
          else {
            return self.onError('Upload error: ' + e.target.status + ' Desc:' + e.target.statusText);
          }
        };
        xhr.onerror = function (e) {
          return self.onError('XHR error:' + e.target.status);
        };
        xhr.upload.onprogress = function (e) {
          var percentLoaded;
          if (e.lengthComputable) {
            percentLoaded = Math.round((e.loaded / e.total) * 100);
            return self.onProgress(percentLoaded, percentLoaded === 100 ? 'Finalizing.' : 'Uploading.');
          }
        };
      }

      xhr.setRequestHeader('Content-Type', file.type || self.getMimeType(file.name));
      return xhr.send(file);
    };

    S3Upload.prototype.uploadFile = function (file) {
      var self = this;
      return this.executeOnSignedUrl(file, function (signedURL, publicURL) {
        return self.uploadToS3(file, signedURL, publicURL);
      });
    };

    return S3Upload;

  })();

}).call(this);
