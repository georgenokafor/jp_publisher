package com.mapped.publisher.parse.extractor;

import com.mapped.publisher.parse.extractor.booking.GoldmanParser;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import com.mapped.publisher.parse.pdf.AreaBasedPDFParser;
import com.mapped.publisher.parse.pdf.PDFParser;
import com.mapped.publisher.parse.valueObject.TravelDetails;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-10-14
 * Time: 4:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class BigFiveTourExtractorPdf extends BigFiveTourExtractor {

    public TravelDetails parse(InputStream fis) {

        try {
            PDFParser parser = null;
            TravelDetails details = new TravelDetails();
            Rectangle rect = new Rectangle(0, 100, 612, 630);
            parser = new AreaBasedPDFParser(rect);
            // Parser and get the raw text data
            String txt = parser.parse(fis);

            //get the intro
            if (txt != null) {
                return parseFile(cleanText(txt));
            }

        } catch (Exception exep) {
            exep.printStackTrace();
        }


        return null;
    }

    public static void main (String[] args) {
        try {
            String dir = "/users/twong/Dropbox/sync/wip/pnrs/bigfive/";//"/Users/twong/Dropbox/UMapped_Share/Companies/Goldman Travel/Tramada Invoices/"; //"/volumes/data2/downloads/";
            String filename = "AustraliaDymottproposal.pdf";// "B66753-DEUEL-LIZA-MS-Itinerary---Full.pdf"; //"B53398-SUTTON_ANDREW-Itinerary---Full.pdf"
            File file = new File(dir + filename);



            InputStream ins = new FileInputStream(file);

            BigFiveTourExtractor b  = new BigFiveTourExtractorPdf();
            TravelDetails td = b.parse(ins);
            System.out.println(td);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
