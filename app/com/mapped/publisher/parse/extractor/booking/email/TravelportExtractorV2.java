package com.mapped.publisher.parse.extractor.booking.email;

import com.fasterxml.jackson.databind.JsonNode;
import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.msg.flightstat.track.Name;
import com.mapped.publisher.parse.extractor.booking.valueObject.*;
import com.umapped.persistence.enums.ReservationType;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import play.libs.Json;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by twong on 15-08-27.
 */
public class TravelportExtractorV2 {
  private static String[] dateTimeFormat = {"yyyy-MM-dd'T'HH:mm:ss"} ;

  private static DateUtils du = new DateUtils();

  public static String getItinerary(String lastName, String recLocator) {
    try {
   //   String recLocator = "N7Z2DG";
   //   String lastName = "DELANY";

      HttpClient c = HttpClientBuilder.create().build();
      HttpGet p = new HttpGet(
          "https://viewtripnextgen-api.travelport.com/api/v1/itinerary/" + recLocator + "?lName=" + lastName + "&cultureInfo=en-US");

      p.setHeader("User-Agent",
                  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36");

      p.setHeader("Pragma", "no-cache");
      p.setHeader("Origin", "https://viewtrip.travelport.com");
      p.setHeader("Referrer", "https://viewtrip.travelport.com");
      p.setHeader("Accept-Language", "en-US,en;q=0.8,pt;q=0.6,pt-BR;q=0.4,fr;q=0.2");
      p.setHeader("Accept", "text/html, /; q=0.01");
      p.setHeader("Cache-Control", "no-cache");
      p.setHeader("X-Requested-With", "XMLHttpRequest");
      p.setHeader("Content-Type", "application/json; charset=UTF-8");

      HttpResponse r = c.execute(p);

      ResponseHandler<String> handler = new BasicResponseHandler();

      String body = handler.handleResponse(r);
      if (body != null && body.contains(recLocator)) {
        return body;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public static TripVO parseItinerary (String s, String recLocator) {
    HashMap<Integer, FlightVO> flightsBySegment = new HashMap<>();
    String providerCode = null;
    try {
      JsonNode jsonNode = Json.parse(s);
      if (jsonNode != null &&  jsonNode.get("Segments") != null  &&  jsonNode.get("Segments").isArray()) {
        if (jsonNode.get("ProviderCode") != null) {
          providerCode = jsonNode.get("ProviderCode").asText();
        }
        TripVO tripVO = new TripVO();
        tripVO.setImportSrc(BookingSrc.ImportSrc.TRAVELPORT__VIEWTRIPS);
        tripVO.setImportSrcId(recLocator);
        int i = 0;
        for (JsonNode node : jsonNode.get("Segments")){
          i++;
          if (node.get("SegmentType") != null) {
            String segmentType = node.get("SegmentType").asText().trim();
            if (segmentType != null && segmentType.length() > 0 && !segmentType.equalsIgnoreCase("null")) {
              if (segmentType.equalsIgnoreCase("flight") || segmentType.equalsIgnoreCase("Tvl_AIR")) {
                FlightVO flight = parseFlight(node);
                if (flight.isValid() && flight.getCode() != null && !flight.getCode().equals("ZZ")) {
                  tripVO.addFlight(flight);
                  flightsBySegment.put(node.get("SegmentNum").asInt(), flight);
                }
              } else if (segmentType.equalsIgnoreCase("hotel")) {
                HotelVO hotel =  parseHotel(node);
                if (hotel.isValid()) {
                  tripVO.addHotel(hotel);
                }
              } else if (segmentType.equalsIgnoreCase("car")) {
                TransportVO car =  parseCar(node);
                if (car.isValid()) {
                  tripVO.addTransport(car);
                }
              } else if (segmentType.equalsIgnoreCase("tour") || segmentType.equalsIgnoreCase("Tvl_MIS")) {
                //if this is the last segment and it has the word RETAIN, then do not process
                String segmentTexts = null;
                if (i == jsonNode.get("Segments").size() && node.get("SegmentInfo") != null && node.get("SegmentInfo").get("SegmentTexts") != null){
                  StringBuilder sb = new StringBuilder();
                  for (JsonNode segNode: node.get("SegmentInfo").get("SegmentTexts")) {
                    if (segNode.asText() != null) {
                      sb.append(segNode.asText());
                      sb.append("\n");
                    }
                  }
                  segmentTexts = sb.toString();
                }
                if (segmentTexts == null || !segmentTexts.contains("RETAIN")) {
                  ActivityVO tour = parseTour(node);
                  if (tour.isValid()) {
                    tripVO.addActivity(tour);
                  }
                }
              }

            }
          }
        }
        if (tripVO.getFlights() != null) {
          for (FlightVO f : tripVO.getFlights()) {
            if (f.getReservationNumber() == null || f.getReservationNumber().trim().length() == 0) {
              f.setReservationNumber(recLocator);
            }
          }
        }

        //processRemarks
        NoteVO noteVO = getRemarks(jsonNode);
        if (noteVO != null) {
          tripVO.addNoteVO(noteVO);
        }

        //process another request to get etickets
        //for some reason, it comes in a different request
        if (providerCode != null && flightsBySegment.size() > 0) {
          try {
            String eTicketJson = getEtickets(providerCode, recLocator);
            if (eTicketJson != null) {
              JsonNode eTicketNode = Json.parse(eTicketJson);
              if (eTicketNode != null && eTicketNode.get("Segments") != null  &&  eTicketNode.get("Segments").isArray()) {
                for (JsonNode segmentNode : eTicketNode.get("Segments")){
                  FlightVO flightVO = flightsBySegment.get(segmentNode.get("SegmentNum").asInt());
                  if (flightVO != null && segmentNode.get("Travelers") != null && segmentNode.get("Travelers").isArray()) {
                    for (JsonNode travelerNode: segmentNode.get("Travelers")) {
                      String firstN = cleanFirstName(travelerNode.get("FirstName").asText());
                      String lastN = travelerNode.get("LastName").asText();
                      String eticket = travelerNode.get("EticketNumber").asText();
                      if (firstN != null && lastN != null && eticket != null && !eticket.equals("null") && !eticket.isEmpty()) {
                        boolean passengerFound = false;
                        if (flightVO.getPassengers() != null) {
                          for (PassengerVO p: flightVO.getPassengers()) {
                            if (p.getFirstName() != null && p.getFirstName().contains(firstN) && p.getLastName() != null && p.getLastName().contains(lastN)) {
                              p.seteTicketNumber(eticket);
                              passengerFound = true;
                              break;
                            }
                          }
                        }
                        if (!passengerFound) {
                          PassengerVO passengerVO = new PassengerVO();
                          passengerVO.setFirstName(firstN);
                          passengerVO.setLastName(lastN);
                          passengerVO.seteTicketNumber(eticket);
                          flightVO.addPassenger(passengerVO);
                        }

                      }

                    }
                  }
                }
              }

            }
          } catch (Exception e) {

          }
        }

        return tripVO;
      }


    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  public static FlightVO parseFlight (JsonNode node) {
    boolean seatsFound = false;
    FlightVO flightVO = new FlightVO();
    StringBuilder sb = new StringBuilder();


    flightVO.setNumber(node.get("FlightNumber").asText());
    flightVO.setCode(node.get("Logo").asText());
    flightVO.setName(node.get("AirlineName").asText());
    flightVO.setDepatureTime(getTimestamp(node.get("Depart").asText()));
    flightVO.setArrivalTime(getTimestamp(node.get("Arrive").asText()));
    flightVO.setReservationNumber(node.get("Confirmation").asText());

    AirportVO departAirport = new AirportVO();
    departAirport.setCity(node.get("DepartName").asText());
    departAirport.setCode(node.get("DepartCode").asText());
    flightVO.setDepartureAirport(departAirport);

    AirportVO arriveAirport = new AirportVO();
    arriveAirport.setCity(node.get("ArriveName").asText());
    arriveAirport.setCode(node.get("ArriveCode").asText());
    flightVO.setArrivalAirport(arriveAirport);

    if (node.get("Guests") != null && node.get("Guests").isArray()) {
      int i = 0;
      for (JsonNode pNode: node.get("Guests")) {
        StringBuilder sb1 = new StringBuilder();
        boolean pSeatsFound =  false;

        PassengerVO passenger = null;
        String firstN = cleanFirstName(pNode.get("FirstName").asText().trim());
        String lastN = pNode.get("LastName").asText().trim();
        if (flightVO.getPassengers() != null) {
          for (PassengerVO p: flightVO.getPassengers()) {
            if (p.getFirstName() != null && p.getFirstName().contains(firstN) && p.getLastName() != null && p.getLastName().contains(lastN)) {
              passenger = p;
              break;
            }
          }
        }

        if (passenger == null) {
          passenger = new PassengerVO();
          passenger.setFirstName(firstN);
          passenger.setLastName(lastN);
        }

        if (pNode.get("LoyaltyInfo") != null && pNode.get("LoyaltyInfo").asText() != null && !pNode.get("LoyaltyInfo").asText().equals("null")) {
          passenger.setFrequentFlyer(pNode.get("LoyaltyInfo").asText());
        }
        if (pNode.get("EticketNumber") != null && pNode.get("EticketNumber").asText() != null && !pNode.get("EticketNumber").asText().equals("null")) {
          passenger.seteTicketNumber(pNode.get("EticketNumber").asText());
        }
        JsonNode seats = node.get("Seats");
        if (seats != null && seats.isArray() && seats.size() > 0 && seats.size() > i && seats.get(i) != null && seats.get(i).asText() != null && !seats.get(i).asText().isEmpty()) {
          passenger.setSeat(seats.get(i).asText());
          seatsFound = true;
          pSeatsFound = true;
        }
        if (!pSeatsFound) {
          JsonNode seats1 = pNode.get("SeatInfo");
          if (seats1 != null && seats1.isArray()) {
            for (int j = 0; j < seats1.size(); j++) {
              JsonNode seat1 = seats1.get(j);
              if (seat1.get("ArrivalAirportCode") != null && seat1.get("Number") != null && !seat1.get("Number").asText().isEmpty() && seat1.get("ArrivalAirportCode").asText().contains(arriveAirport.getCode())) {
                passenger.setSeat(seat1.get("Number").asText());
                seatsFound = true;
                pSeatsFound = true;
                break;
              }
            }
          }
        }

        if (node.get("ClassOfService")!= null && node.get("ClassOfService").get("Description") != null && node.get("ClassOfService").get("Description").asText().trim().length() > 0) {
          passenger.setSeatClass(node.get("ClassOfService").get("Description").asText());

        }
        passenger.setNote(sb1.toString());
        flightVO.addPassenger(passenger);
        i++;
      }
    }

    if (node.get("SegmentDates") != null && node.get("SegmentDates").isArray() && node.get("SegmentDates").size() > 1) {
      if (node.get("SegmentDates").get(0) != null && node.get("SegmentDates").get(0).asText() != null) {
        flightVO.setDepatureTime(getTimestamp(node.get("SegmentDates").get(0).asText()));
      }
      int last = node.get("SegmentDates").size() -1;
      if (node.get("SegmentDates").get(last) != null && node.get("SegmentDates").get(last).asText() != null) {
        flightVO.setArrivalTime(getTimestamp(node.get("SegmentDates").get(last).asText()));
      }


    }

    if (node.get("Legs") != null && node.get("Legs").isArray()) {
      if (node.get("Legs").size() == 1) {
        JsonNode leg = node.get("Legs").get(0);
        if (leg.get("TravelTime") != null && leg.get("TravelTime").asText().trim().length() > 0) {
          sb.append("Travel Time: ");sb.append(leg.get("TravelTime").asText());sb.append("\n");
        }
        if (leg.get("FlightInfo") != null && leg.get("FlightInfo").isArray() &&  leg.get("FlightInfo").size() > 0) {
          sb.append("\nFlight Info:");
          for (JsonNode fi: leg.get("FlightInfo")) {
            if (fi.asText() != null) {
              if (fi.asText().toLowerCase().contains("lunch") || fi.asText().toLowerCase().contains("dinner") || fi.asText().toLowerCase().contains("meal") || fi.asText().toLowerCase().contains("breakfast") || fi.asText().toLowerCase().contains("snack") || fi.asText().toLowerCase().contains("food")) {
                if (flightVO.getPassengers() != null) {
                  for (PassengerVO p: flightVO.getPassengers()) {
                    p.setMeal(fi.asText());
                  }
                }
              } else {
                sb.append(fi.asText());sb.append("\n");
              }
            }
          }
        }
        if (leg.get("DepartAirport").get("Terminal") != null && leg.get("DepartAirport").get("Terminal").asText().trim().length() > 0 && !leg.get("DepartAirport").get("Terminal").asText().equalsIgnoreCase("null")) {
          departAirport.setTerminal(leg.get("DepartAirport").get("Terminal").asText());
        }
        if (leg.get("ArriveAirport").get("Terminal") != null && leg.get("ArriveAirport").get("Terminal").asText().trim().length() > 0 && !leg.get("ArriveAirport").get("Terminal").asText().equalsIgnoreCase("null")) {
          arriveAirport.setTerminal(leg.get("ArriveAirport").get("Terminal").asText());
        }

        if (!seatsFound && flightVO.getPassengers() != null && flightVO.getPassengers().size() > 0) {
          JsonNode seats = leg.get("Seats");
          if (seats != null && seats.isArray() && seats.size() > 0) {
            int i = 0;
            for (PassengerVO passengerVO: flightVO.getPassengers()) {
              if (i < seats.size()) {
                passengerVO.setSeat(seats.get(i).asText());
              }
              i++;
            }
          }
        }


      } else if (node.get("Legs").size() > 1) {
        int i = 1;
        for (JsonNode leg: node.get("Legs")) {
          if (departAirport.getCode().equals(leg.get("DepartAirport").get("AirportCode").asText())) {
            if (departAirport.getCode().equals(leg.get("DepartAirport").get("AirportCode").asText()) && leg.get(
                "DepartAirport").get("Terminal") != null && leg.get("DepartAirport")
                                                               .get("Terminal")
                                                               .asText()
                                                               .trim()
                                                               .length() > 0 && !leg.get("DepartAirport")
                                                                                    .get("Terminal")
                                                                                    .asText()
                                                                                    .equalsIgnoreCase("null")) {
              departAirport.setTerminal(leg.get("DepartAirport").get("Terminal").asText());
            }
            if (flightVO.getDepatureTime() == null && leg.get("Depart") != null && leg.get("Depart").asText() != null && getTimestamp(leg.get("Depart").asText()) != null  ) {
              flightVO.setDepatureTime(getTimestamp(leg.get("Depart").asText()));
            }
          }

          if (leg.get("ArriveAirport") != null  && leg.get("ArriveAirport").get("AirportCode") != null && arriveAirport.getCode().equals(leg.get("ArriveAirport").get("AirportCode").asText())) {

            if (leg.get("ArriveAirport").get("Terminal") != null
                 && leg.get("ArriveAirport").get("Terminal").asText() != null && leg.get("ArriveAirport").get("Terminal").asText().trim().length() > 0) {
              if (!leg.get("ArriveAirport").get("Terminal").asText().equalsIgnoreCase("null")) {
                arriveAirport.setTerminal(leg.get("ArriveAirport").get("Terminal").asText());
              }
            }

          }
          if (flightVO.getArrivalTime() == null && leg.get("Arrive") != null && leg.get("Arrive").asText() != null &&  getTimestamp(leg.get("Arrive").asText()) != null) {
            flightVO.setArrivalTime(getTimestamp(leg.get("Arrive").asText()));
          }

          sb.append("Leg ");sb.append(i);sb.append("\n");
          sb.append(leg.get("AirlineName").asText()); sb.append(" "); sb.append(leg.get("FlightNumber").asText());sb.append("\n");
          sb.append("Depart: "); sb.append(leg.get("DepartAirport").get("Name").asText());sb.append(" ");sb.append(leg.get("DepartAirport").get("AirportCode").asText());

          /*
          if (leg.get("Depart").asText() != null && leg.get("Depart").asText().trim().length() > 0 && getTimestamp(leg.get("Depart").asText()) != null) {
            sb.append(" ");
            sb.append(getTimestamp(leg.get("Depart").asText()));
          }*/

          sb.append("\n");
          sb.append("Arrive: "); sb.append(leg.get("ArriveAirport").get("Name").asText());sb.append(" ");sb.append(leg.get("ArriveAirport").get("AirportCode").asText());
          /*
          if (leg.get("ArriveAirport").asText()!=null && leg.get("ArriveAirport").asText().trim().length() > 0 && getTimestamp(leg.get("ArriveAirport").asText()) != null) {
            sb.append(" ");
            sb.append(getTimestamp(leg.get("ArriveAirport").asText()));
          }*/
          sb.append("\n");
          if (leg.get("FlightInfo") != null && leg.get("FlightInfo").isArray() &&  leg.get("FlightInfo").size() > 0) {
            sb.append("\nFlight Info:\n");
            for (JsonNode fi: leg.get("FlightInfo")) {
              sb.append(fi.asText());sb.append("\n");
            }
          }
          sb.append("\n");
          i++;


          if (!seatsFound && flightVO.getPassengers() != null && flightVO.getPassengers().size() > 0) {
            JsonNode seats = leg.get("Seats");
            if (seats != null && seats.isArray() && seats.size() > 0) {
              int j = 0;
              for (PassengerVO passengerVO: flightVO.getPassengers()) {
                if (j < seats.size()) {
                  String seat = passengerVO.getProperties().get("Seat");
                  if (seat == null) {
                    seat = "";
                  } else {
                    seat +=", ";
                  }
                  seat += seats.get(j).asText();
                  passengerVO.setSeat(seat);
                }
                j++;
              }
            }
          }
        }
      }
    }
    if (node.get("OperatingCarrier") != null && node.get("OperatingCarrier").asText().trim().length() > 0 &&  ! node.get("OperatingCarrier").asText().equalsIgnoreCase("null")) {
      sb.append("\nOperated by: ");sb.append(node.get("OperatingCarrier").asText());
    }
    if (node.get("AssociatedRemarks") != null) {
      for (JsonNode fi: node.get("AssociatedRemarks")) {
        String s = fi.asText();
        if (s != null && !s.isEmpty())
        sb.append(s);sb.append("\n");
      }
    }
    flightVO.setNote(sb.toString());
    JsonNode ssr = node.get("SpecialServiceRequest");
    if (flightVO.getPassengers() != null && ssr != null && ssr.size() > 0) {
      for (JsonNode n: ssr) {
        if (n.get("Description") != null && n.get("FirstName") != null && n.get("LastName") != null) {
          String desc = n.get("Description").asText();
          if (desc.toLowerCase().contains("meal")) {
            String firstN = cleanFirstName(n.get("FirstName").asText().trim());
            String lastN = n.get("LastName").asText().trim();
            for (PassengerVO p: flightVO.getPassengers()) {
              if (p.getFirstName() != null && p.getFirstName().contains(firstN) && p.getLastName() != null && p.getLastName().contains(lastN)) {
                p.setMeal(desc);
                break;
              }
            }

          }


        }
      }
    }
    return flightVO;
  }

  public static TransportVO parseCar (JsonNode node) {
    TransportVO car = new TransportVO();
    StringBuilder sb1 = new StringBuilder();

    car.setBookingType(ReservationType.CAR_RENTAL);
    car.setName(node.get("SegmentInfo").get("ProviderName").asText());
    if (car.getName() == null || car.getName().isEmpty()) {
      car.setName("Car Rental");
    }
    car.setPickupDate(getTimestamp(node.get("SegmentInfo").get("Pickup").asText()));
    car.setDropoffDate(getTimestamp(node.get("SegmentInfo").get("DropOff").asText()));
    car.setConfirmationNumber(node.get("SegmentInfo").get("Confirmation").asText());
    if (node.get("SegmentInfo").get("PickupInfo")!= null) {
      StringBuilder sb = new StringBuilder();
      StringBuilder pickupLocation = new StringBuilder();
      int i = 0;
      if (node.get("SegmentInfo").get("PickupInfo").get("Name") != null && node.get("SegmentInfo").get("PickupInfo").get("Name").isArray()) {
        for (JsonNode node1: node.get("SegmentInfo").get("PickupInfo").get("Name") ) {
          if (node1.asText() != null && !node1.asText().isEmpty()) {
            if (i > 0) {
              pickupLocation.append(", ");
              sb.append(", ");
            }
            sb.append(node1.asText());
            pickupLocation.append(node1.asText());
            i++;
          }
        }
      }
      if (node.get("SegmentInfo").get("PickupInfo").get("AirportName") != null && node.get("SegmentInfo").get("PickupInfo").get("AirportName").asText().trim().length() > 0 && !node.get("SegmentInfo").get("PickupInfo").get("AirportName").asText().equalsIgnoreCase("null")) {
        if (i > 0) {
          pickupLocation.append(", ");
          sb.append(", ");
        }
        pickupLocation.append(node.get("SegmentInfo").get("PickupInfo").get("AirportName").asText());
        sb.append(node.get("SegmentInfo").get("PickupInfo").get("AirportName").asText());sb.append("\n");
      }

      if (node.get("SegmentInfo").get("PickupInfo").get("Address") != null && node.get("SegmentInfo").get("PickupInfo").get("Address").asText().trim().length() > 0 && !node.get("SegmentInfo").get("PickupInfo").get("Address").asText().equalsIgnoreCase("null")) {
        sb.append(node.get("SegmentInfo").get("PickupInfo").get("Address").asText());
        sb.append("\n");
      }

      if (node.get("SegmentInfo").get("PickupInfo").get("Address") != null && node.get("SegmentInfo").get("PickupInfo").get("City").asText().trim().length() > 0 && !node.get("SegmentInfo").get("PickupInfo").get("City").asText().equalsIgnoreCase("null")) {
        sb.append(node.get("SegmentInfo").get("PickupInfo").get("City").asText());
        sb.append("\n");
      }
      if (node.get("SegmentInfo").get("DropoffInfo").get("ContactInfo") != null) {
        JsonNode contactNode = node.get("SegmentInfo").get("DropoffInfo").get("ContactInfo");
        if (contactNode.get("Phone") != null && contactNode.get("Phone").asText().trim().length() > 0 && ! contactNode.get(
            "Phone").asText().equalsIgnoreCase("null")) {
          sb.append("Phone: ");
          sb.append(contactNode.get("Phone").asText());
          sb.append("\n");
        }
        if (contactNode.get("Fax") != null && contactNode.get("Fax").asText().trim().length() > 0 && ! contactNode.get("Fax").asText().equalsIgnoreCase("null")) {
          sb.append("Fax: ");
          sb.append(contactNode.get("Fax").asText());
          sb.append("\n");
        }
      }
      car.setPickupLocation(pickupLocation.toString());
      if (sb.toString().trim().length() > 0) {
        sb1.append("Pickup Address:\n");
        sb1.append(sb.toString());
        sb1.append("\n");
      }

    }

    if (node.get("SegmentInfo").get("DropoffInfo")!= null) {
      StringBuilder sb = new StringBuilder();
      StringBuilder pickupLocation = new StringBuilder();
      int i = 0;
      if (node.get("SegmentInfo").get("DropoffInfo").get("Name") != null && node.get("SegmentInfo").get("DropoffInfo").get("Name").isArray()) {
        for (JsonNode node1: node.get("SegmentInfo").get("DropoffInfo").get("Name") ) {
          if (node1.asText() != null && !node1.asText().isEmpty()) {
            if (i > 0) {
              pickupLocation.append(", ");
              sb.append(", ");
            }
            sb.append(node1.asText());
            pickupLocation.append(node1.asText());
            i++;
          }
        }
      }
      if (node.get("SegmentInfo").get("DropoffInfo").get("AirportName") != null && node.get("SegmentInfo").get("DropoffInfo").get("AirportName").asText().trim().length() > 0 && !node.get("SegmentInfo").get("DropoffInfo").get("AirportName").asText().equalsIgnoreCase("null")) {
        if (i > 0) {
          pickupLocation.append(", ");
          sb.append(", ");
        }
        pickupLocation.append(node.get("SegmentInfo").get("DropoffInfo").get("AirportName").asText());
        sb.append(node.get("SegmentInfo").get("DropoffInfo").get("AirportName").asText());sb.append("\n");
      }

      if (node.get("SegmentInfo").get("DropoffInfo").get("Address") != null && node.get("SegmentInfo").get("DropoffInfo").get("Address").asText().trim().length() > 0 && !node.get("SegmentInfo").get("DropoffInfo").get("Address").asText().equalsIgnoreCase("null")) {
        sb.append(node.get("SegmentInfo").get("DropoffInfo").get("Address").asText());
        sb.append("\n");
      }

      if (node.get("SegmentInfo").get("DropoffInfo").get("Address") != null && node.get("SegmentInfo").get("DropoffInfo").get("City").asText().trim().length() > 0 && !node.get("SegmentInfo").get("DropoffInfo").get("City").asText().equalsIgnoreCase("null")) {
        sb.append(node.get("SegmentInfo").get("DropoffInfo").get("City").asText());
        sb.append("\n");
      }
      if (node.get("SegmentInfo").get("DropoffInfo").get("ContactInfo") != null) {
        JsonNode contactNode = node.get("SegmentInfo").get("DropoffInfo").get("ContactInfo");
        if (contactNode.get("Phone") != null && contactNode.get("Phone").asText().trim().length() > 0 && ! contactNode.get("Phone").asText().equalsIgnoreCase("null")) {
          sb.append("Phone: ");
          sb.append(contactNode.get("Phone").asText());
          sb.append("\n");
        }
        if (contactNode.get("Fax") != null && contactNode.get("Fax").asText().trim().length() > 0 && ! contactNode.get("Fax").asText().equalsIgnoreCase("null")) {
          sb.append("Fax: ");
          sb.append(contactNode.get("Fax").asText());
          sb.append("\n");
        }
      }
      car.setDropoffLocation(pickupLocation.toString());

      if (sb.toString().trim().length() > 0) {
        sb1.append("Dropoff Address:\n");
        sb1.append(sb.toString());
        sb1.append("\n");
      }
    }

    if (node.get("SegmentInfo").get("CarInfo") != null) {
      sb1.append("Car Info:\n");
      JsonNode carNode = node.get("SegmentInfo").get("CarInfo");
      if (carNode.get("Description") != null && carNode.get("Description").asText().trim().length() > 0 && !carNode.get("Description").asText().equalsIgnoreCase("null")) {
        sb1.append(carNode.get("Description").asText());sb1.append("\n");

      }
      if (carNode.get("Miles") != null && carNode.get("Miles").asText().trim().length() > 0 && !carNode.get("Miles").asText().equalsIgnoreCase("null")) {
        sb1.append(carNode.get("Miles").asText());sb1.append("\n");

      }
      if (carNode.get("ApproxTotal") != null && carNode.get("ApproxTotal").asText().trim().length() > 0 && !carNode.get("ApproxTotal").asText().equalsIgnoreCase("null")) {
        sb1.append("Rate: ");sb1.append(carNode.get("ApproxTotal"));sb1.append(" ");sb1.append(carNode.get("Currency").asText()); sb1.append("\n");

      }

    }

    if (node.get("SegmentInfo").get("AssociatedRemarks")!= null && node.get("SegmentInfo").get("AssociatedRemarks").isArray()) {
      for (JsonNode node1: node.get("SegmentInfo").get("AssociatedRemarks") ) {
        if (node1.asText() != null && !node1.asText().isEmpty()) {
          sb1.append(node1.asText());
          sb1.append("\n");
        }
      }
    }
    car.setNote(sb1.toString());


     return car;
  }


  public static HotelVO parseHotel (JsonNode node) {
    HotelVO hotelVO = new HotelVO();
    StringBuilder sb = new StringBuilder();

    hotelVO.setName(node.get("SegmentInfo").get("ProviderName").asText());
    hotelVO.setCheckin(getTimestamp(node.get("SegmentInfo").get("CheckIn").asText()));
    hotelVO.setCheckout(getTimestamp(node.get("SegmentInfo").get("CheckOut").asText()));
    hotelVO.setConfirmation(node.get("SegmentInfo").get("Confirmation").asText());


    if (node.get("SegmentInfo").get("PropertyInfo")!= null) {
      boolean hasAddr = false;
      if (node.get("SegmentInfo").get("PropertyInfo").get("Address") != null && !node.get("SegmentInfo").get("PropertyInfo").get("Address").asText().equals("null")) {
        sb.append("Address: \n");
        sb.append(node.get("SegmentInfo").get("PropertyInfo").get("Address").asText());
        hasAddr = true;
      }

      if (node.get("SegmentInfo").get("PropertyInfo").get("City") != null && !node.get("SegmentInfo").get("PropertyInfo").get("City").asText().equals("null")) {
        sb.append(", ");sb.append(node.get("SegmentInfo").get("PropertyInfo").get("City").asText());
        hasAddr = true;
      }
      if (node.get("SegmentInfo").get("PropertyInfo").get("Zip") != null && !node.get("SegmentInfo").get("PropertyInfo").get("Zip").asText().equals("null")) {
        sb.append(", ");sb.append(node.get("SegmentInfo").get("PropertyInfo").get("Zip").asText());
        hasAddr = true;
      }

      if (!hasAddr && node.get("SegmentInfo").get("PropertyInfo").get("Name") != null ) {
        Iterator<JsonNode> it = node.get("SegmentInfo").get("PropertyInfo").get("Name").iterator();
        int count = 0;
        while (it.hasNext()) {
          JsonNode j = it.next();
          if (j.asText() != null && !j.asText().isEmpty() && !j.asText().equals("null") && !j.asText().equalsIgnoreCase(hotelVO.getName())) {
            if (count == 0) {
              sb.append("Address: \n");
            }
            sb.append(j.asText());
            sb.append("\n");
            count++;
          }
        }
      }

      sb.append("\n");

    }
    if (node.get("SegmentInfo").get("ContactInfo")!= null) {
      if (node.get("SegmentInfo").get("ContactInfo").get("Phone") != null) {
        sb.append("Phone: ");
        sb.append(node.get("SegmentInfo").get("ContactInfo").get("Phone").asText());
        sb.append("\n");
      }
      if (node.get("SegmentInfo").get("ContactInfo").get("Email") != null) {
        sb.append("Email: ");
        sb.append(node.get("SegmentInfo").get("ContactInfo").get("Email").asText());
        sb.append("\n");
      }
      if (node.get("SegmentInfo").get("ContactInfo").get("Web") != null) {
        sb.append("Web: ");
        sb.append(node.get("SegmentInfo").get("ContactInfo").get("Web").asText());
        sb.append("\n");
      }
      if (node.get("SegmentInfo").get("ContactInfo").get("Fax") != null) {
        sb.append("Fax: ");
        sb.append(node.get("SegmentInfo").get("ContactInfo").get("Fax").asText());
        sb.append("\n");
      }

      sb.append("\n");
    }


    if (node.get("SegmentInfo").get("RoomInfo")!= null) {
      if (node.get("SegmentInfo").get("RoomInfo").get("Guests") != null) {
        sb.append("Number of guests: ");
        sb.append(node.get("SegmentInfo").get("RoomInfo").get("Guests").asText());
        sb.append("\n");
      }
      if (node.get("SegmentInfo").get("RoomInfo").get("Rooms") != null) {
        sb.append("Number of rooms: ");
        sb.append(node.get("SegmentInfo").get("RoomInfo").get("Rooms").asText());
        sb.append("\n");
      }
      if (node.get("SegmentInfo").get("RoomInfo").get("Nights") != null) {
        sb.append("Number of nights: ");
        sb.append(node.get("SegmentInfo").get("RoomInfo").get("Nights").asText());
        sb.append("\n");
      }
      if (node.get("SegmentInfo").get("RoomInfo").get("RoomTypeDescription") != null) {
        sb.append("Room Description: ");
        JsonNode n1 = node.get("SegmentInfo").get("RoomInfo").get("RoomTypeDescription");
        if (n1.isArray()) {
          for (JsonNode d : n1) {
            sb.append(d.asText());
            sb.append("\n");
          }
        }
      }
      if (node.get("SegmentInfo").get("RoomInfo").get("SpecialInformation") != null) {
        sb.append("Special Information: ");
        sb.append(node.get("SegmentInfo").get("RoomInfo").get("SpecialInformation").asText());
        sb.append("\n");
      }

      if (node.get("SegmentInfo").get("RoomInfo").get("Rate") != null) {
        sb.append("Rate: ");
        sb.append(node.get("SegmentInfo").get("RoomInfo").get("Rate").asText());
        if (node.get("SegmentInfo").get("RoomInfo").get("Currency") != null) {
          sb.append(" ");
          sb.append(node.get("SegmentInfo").get("RoomInfo").get("Currency").asText());
        }
        sb.append("\n");
      }
      if (node.get("SegmentInfo").get("RoomInfo").get("ApproxTotal") != null) {
        sb.append("Total Rate: ");
        sb.append(node.get("SegmentInfo").get("RoomInfo").get("ApproxTotal").asText());
        if (node.get("SegmentInfo").get("RoomInfo").get("Currency") != null) {
          sb.append(" ");
          sb.append(node.get("SegmentInfo").get("RoomInfo").get("Currency").asText());
        }
        sb.append("\n");
      }
      sb.append("\n");
    }

    if (node.get("SegmentInfo").get("AssociatedRemarks")!= null && node.get("SegmentInfo").get("AssociatedRemarks").isArray()) {
      for (JsonNode node1: node.get("SegmentInfo").get("AssociatedRemarks") ) {
        if (node1.asText() != null && !node1.asText().isEmpty()) {
          sb.append(node1.asText());
          sb.append("\n");
        }
      }
    }

    hotelVO.setNote(sb.toString());

    return hotelVO;
  }


  public static ActivityVO parseTour (JsonNode node) {
    ActivityVO tour = new ActivityVO();
    StringBuilder sb = new StringBuilder();

    tour.setBookingType(ReservationType.ACTIVITY);

    tour.setName("Tour");
    if (node.get("SegmentInfo") != null) {
      if (node.get("SegmentInfo").get("Title") != null && !node.get("SegmentInfo").get("Title").asText().isEmpty()) {
        tour.setName(node.get("SegmentInfo").get("Title").asText());
      } else {
        tour.setName(node.get("SegmentInfo").get("TourTypeName").asText());
        if (tour.getName().trim().equalsIgnoreCase("tour")) {
          tour.setBookingType(ReservationType.TOUR);
        }
      }

      tour.setStartDate(getTimestamp(node.get("SegmentInfo").get("StartDate").asText()));

      if (node.get("SegmentInfo").get("EndDate") != null && !node.get("SegmentInfo").get("EndDate").asText().isEmpty()) {
        tour.setEndDate(getTimestamp(node.get("SegmentInfo").get("EndDate").asText()));
      }
      if (node.get("SegmentInfo").get("Confirmation") != null && !node.get("SegmentInfo").get("Confirmation").asText().isEmpty() && !node.get("SegmentInfo").get("Confirmation").asText().equals("null")) {
        tour.setConfirmation(node.get("SegmentInfo").get("Confirmation").asText());
      }

      if (node.get("SegmentInfo").get("OriginCityName")!= null && !node.get("SegmentInfo").get("OriginCityName").asText().isEmpty()) {
        tour.setStartLocation(node.get("SegmentInfo").get("OriginCityName").asText());
      }

      if (node.get("SegmentInfo").get("DestinationCityName")!= null && !node.get("SegmentInfo").get("DestinationCityName").asText().isEmpty()) {
        tour.setEndLocation(node.get("SegmentInfo").get("DestinationCityName").asText());
      }

      if (node.get("SegmentInfo").get("SegmentTexts")!= null && node.get("SegmentInfo").get("SegmentTexts").isArray()) {
        for (JsonNode node1: node.get("SegmentInfo").get("SegmentTexts") ) {
          if (node1.asText() != null) {
            sb.append(node1.asText());
            sb.append("\n");
          }
        }
      }

      if (node.get("SegmentInfo").get("AssociatedRemarks")!= null && node.get("SegmentInfo").get("AssociatedRemarks").isArray()) {
        for (JsonNode node1: node.get("SegmentInfo").get("AssociatedRemarks") ) {
          if (node1.asText() != null && !node1.asText().isEmpty()) {
            sb.append(node1.asText());
            sb.append("\n");
          }
        }
      }

    } else {
      if (node.get("AssociateInformation") != null && node.get("AssociateInformation").get("Name") != null && !node.get("AssociateInformation").get("Name").asText().isEmpty()){
        tour.setName(node.get("AssociateInformation").get("Name").asText());
      } else if (node.get("AirlineName") != null && !node.get("AirlineName").asText().isEmpty()) {
        tour.setName(node.get("AirlineName").asText());
      }
      if (node.get("SegmentDates") != null && node.get("SegmentDates").isArray() && node.get("SegmentDates").size() > 1) {
        if (node.get("SegmentDates").get(0) != null && node.get("SegmentDates").get(0).asText() != null) {
          tour.setStartDate(getTimestamp(node.get("SegmentDates").get(0).asText()));
        }
        int last = node.get("SegmentDates").size() -1;
        if (node.get("SegmentDates").get(last) != null && node.get("SegmentDates").get(last).asText() != null) {
          tour.setEndDate(getTimestamp(node.get("SegmentDates").get(last).asText()));
        }
      }
      if (node.get("AssociatedRemarks")!= null && node.get("AssociatedRemarks").isArray()) {
        for (JsonNode node1: node.get("AssociatedRemarks") ) {
          if (node1.asText() != null && !node1.asText().isEmpty()) {
            sb.append(node1.asText());
            sb.append("\n");
          }
        }
      }
      if (node.get("PassiveType") != null && node.get("PassiveType").asText().contains("Tour")) {
        tour.setBookingType(ReservationType.TOUR);
      }

    }




    tour.setNote(sb.toString());


    return tour;
  }

  public static NoteVO getRemarks(JsonNode node) {
    if (node != null) {
      StringBuilder sb = new StringBuilder();
      if (node.get("UnassociatedRemarks") != null && node.get("UnassociatedRemarks").isArray()) {
        for (JsonNode n: node.get("UnassociatedRemarks")) {
          if (n.asText() != null && !n.asText().isEmpty()) {
            sb.append(n.asText());
            sb.append("<br/>");
          }
        }
      }
      if (node.get("CannedRemarks") != null && node.get("CannedRemarks").isArray()) {
        if (!sb.toString().isEmpty()) {
          sb.append("<br/>");
        }
        for (JsonNode n: node.get("CannedRemarks")) {
          if (n.asText() != null && !n.asText().isEmpty()) {
            sb.append(n.asText());
            sb.append("<br/>");
          }
        }
      }
      if (!sb.toString().isEmpty()) {
        NoteVO noteVO = new NoteVO();
        noteVO.setName("Remarks");
        noteVO.setNote(sb.toString());
        return noteVO;
      }
    }
    return null;
  }


  public static String getEtickets(String providerCode, String recLocator) {
    try {
      //   String recLocator = "N7Z2DG";
      //   String lastName = "DELANY";

      HttpClient c = HttpClientBuilder.create().build();
      HttpGet p = new HttpGet(
          "https://viewtripnextgen-api.travelport.com/api/v1/itinerary/ancillaries/" + recLocator + "?providerCode=" + providerCode + "&cultureInfo=en-US");

      p.setHeader("User-Agent",
                  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36");

      p.setHeader("Pragma", "no-cache");
      p.setHeader("Origin", "https://viewtrip.travelport.com");
      p.setHeader("Referrer", "https://viewtrip.travelport.com");
      p.setHeader("Accept-Language", "en-US,en;q=0.8,pt;q=0.6,pt-BR;q=0.4,fr;q=0.2");
      p.setHeader("Accept", "text/html, /; q=0.01");
      p.setHeader("Cache-Control", "no-cache");
      p.setHeader("X-Requested-With", "XMLHttpRequest");
      p.setHeader("Content-Type", "application/json; charset=UTF-8");

      HttpResponse r = c.execute(p);

      ResponseHandler<String> handler = new BasicResponseHandler();

      String body = handler.handleResponse(r);
      if (body != null ) {
        return body;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public static void main(String[] args) {



    String s6 = "{\"Locator\":\"JZGDNY\",\"LastName\":\"NOVAKOWSKI\",\"TripName\":null,\"AddedToTripList\":false," +
                "\"PseudoCityCode\":\"27XQ\",\"SubPCC\":null,\"Segments\":[{\"SegmentNum\":1,\"Stops\":0," +
                "\"Seats\":null,\"AirlineName\":\"Aeromexico\",\"FlightNumber\":\"697\",\"OperatingCarrier\":\"\"," +
                "\"Legs\":[{\"DepartAirport\":{\"AirportCode\":\"YVR\",\"Terminal\":\"M\",\"Name\":[\"Vancouver Intl " +
                "Arpt\"],\"Address\":null,\"City\":\"Vancouver\",\"State\":\"BC\",\"Zip\":null,\"Country\":\"CA\"}," +
                "\"ArriveAirport\":{\"AirportCode\":\"MEX\",\"Terminal\":\"2\",\"Name\":[\"Mexico City Juarez " +
                "Intl\"],\"Address\":null,\"City\":\"Mexico City\",\"State\":null,\"Zip\":null,\"Country\":\"MX\"}," +
                "\"Depart\":\"2018-06-01T23:20:00\",\"Arrive\":\"2018-06-01T07:00:00\",\"TravelTime\":\"5H 40M\"," +
                "\"FlightInfo\":[\"Cold meal, Alcohol no cost\"],\"LegMiles\":\"\",\"FlightNumber\":\"697\"," +
                "\"AirlineName\":\"Aeromexico\",\"OperatingCarrier\":\"\",\"Seats\":null}]," +
                "\"AssociatedRemarks\":[\"NO SPECIAL MEALS OFFERED ON OVERNIGHT FLIGHT\",\"PLUS SEATS 7B 7C\"]," +
                "\"Arrive\":\"2018-06-01T07:00:00\",\"Depart\":\"2018-06-01T23:20:00\",\"TravelTime\":\"5H 40M\"," +
                "\"IsPassive\":false,\"OnlineCheckIn\":null,\"SpecialServiceRequest\":[{\"FirstName\":\"LORENE MS\"," +
                "\"LastName\":\"NOVAKOWSKI\",\"Description\":\"Vegetarian Lacto-Ovo Meal\",\"Code\":\"VLML\"," +
                "\"AgentComment\":\"\"}],\"ChangeOfDay\":\"+1\",\"ChangeOfPlanes\":false," +
                "\"EquipmentInfo\":{\"Code\":\"7S8\",\"Description\":\"\"},\"Guests\":[{\"FirstName\":\"LORENE MS\"," +
                "\"LastName\":\"NOVAKOWSKI\",\"Type\":\"\",\"LoyaltyInfo\":\"\",\"FrequentGuestInfo\":null," +
                "\"EticketNumber\":\"1395797569003\",\"TicketNumber\":null,\"SeatInfo\":[{\"Number\":\"07B\"," +
                "\"DepartureAirportCode\":\"YVR\",\"ArrivalAirportCode\":\"MEX\",\"StatusEnum\":1,\"Coach\":null}]}," +
                "{\"FirstName\":\"ALAN MR\",\"LastName\":\"SUDEYKO\",\"Type\":\"\",\"LoyaltyInfo\":\"\"," +
                "\"FrequentGuestInfo\":null,\"EticketNumber\":\"1395797569005\",\"TicketNumber\":null," +
                "\"SeatInfo\":[]}],\"Confirmation\":\"GSIOZD\",\"Logo\":\"AM\",\"Title\":\"\"," +
                "\"ClassOfService\":{\"Code\":\"K\",\"Description\":\"Economy\"}," +
                "\"SegmentDates\":[\"2018-06-29T23:20:00\",\"2018-06-30T07:00:00\"],\"DepartName\":\"Vancouver\"," +
                "\"DepartCode\":\"YVR\",\"ArriveName\":\"Mexico City\",\"ArriveCode\":\"MEX\"," +
                "\"SegmentType\":\"Flight\",\"StatusEnum\":1},{\"SegmentNum\":2,\"Stops\":0,\"Seats\":null," +
                "\"AirlineName\":\"Aeromexico\",\"FlightNumber\":\"46\",\"OperatingCarrier\":\"\"," +
                "\"Legs\":[{\"DepartAirport\":{\"AirportCode\":\"MEX\",\"Terminal\":\"2\",\"Name\":[\"Mexico City " +
                "Juarez Intl\"],\"Address\":null,\"City\":\"Mexico City\",\"State\":null,\"Zip\":null," +
                "\"Country\":\"MX\"},\"ArriveAirport\":{\"AirportCode\":\"LIM\",\"Terminal\":\"\",\"Name\":[\"Jorge " +
                "Chavez International Arpt\"],\"Address\":null,\"City\":\"Lima\",\"State\":null,\"Zip\":null," +
                "\"Country\":\"PE\"},\"Depart\":\"2018-06-01T08:15:00\",\"Arrive\":\"2018-06-01T14:15:00\"," +
                "\"TravelTime\":\"6H 0M\",\"FlightInfo\":[\"Hot meal, Breakfast\"],\"LegMiles\":\"\"," +
                "\"FlightNumber\":\"46\",\"AirlineName\":\"Aeromexico\",\"OperatingCarrier\":\"\",\"Seats\":null}]," +
                "\"AssociatedRemarks\":[\"PLUS SEATS 7B 7C\"],\"Arrive\":\"2018-06-01T14:15:00\"," +
                "\"Depart\":\"2018-06-01T08:15:00\",\"TravelTime\":\"6H 0M\",\"IsPassive\":false," +
                "\"OnlineCheckIn\":null,\"SpecialServiceRequest\":[{\"FirstName\":\"LORENE MS\"," +
                "\"LastName\":\"NOVAKOWSKI\",\"Description\":\"Vegetarian Lacto-Ovo Meal\",\"Code\":\"VLML\"," +
                "\"AgentComment\":\"\"}],\"ChangeOfDay\":null,\"ChangeOfPlanes\":false," +
                "\"EquipmentInfo\":{\"Code\":\"7S8\",\"Description\":\"\"},\"Guests\":[{\"FirstName\":\"LORENE MS\"," +
                "\"LastName\":\"NOVAKOWSKI\",\"Type\":\"\",\"LoyaltyInfo\":\"\",\"FrequentGuestInfo\":null," +
                "\"EticketNumber\":\"1395797569003\",\"TicketNumber\":null,\"SeatInfo\":[{\"Number\":\"07C\"," +
                "\"DepartureAirportCode\":\"MEX\",\"ArrivalAirportCode\":\"LIM\",\"StatusEnum\":1,\"Coach\":null}]}," +
                "{\"FirstName\":\"ALAN MR\",\"LastName\":\"SUDEYKO\",\"Type\":\"\",\"LoyaltyInfo\":\"\"," +
                "\"FrequentGuestInfo\":null,\"EticketNumber\":\"1395797569005\",\"TicketNumber\":null," +
                "\"SeatInfo\":[]}],\"Confirmation\":\"GSIOZD\",\"Logo\":\"AM\",\"Title\":\"\"," +
                "\"ClassOfService\":{\"Code\":\"K\",\"Description\":\"Economy\"}," +
                "\"SegmentDates\":[\"2018-06-30T08:15:00\",\"2018-06-30T14:15:00\"],\"DepartName\":\"Mexico City\"," +
                "\"DepartCode\":\"MEX\",\"ArriveName\":\"Lima\",\"ArriveCode\":\"LIM\",\"SegmentType\":\"Flight\"," +
                "\"StatusEnum\":1},{\"SegmentNum\":3,\"Stops\":0,\"Seats\":null,\"AirlineName\":\"LATAM Airlines " +
                "Group\",\"FlightNumber\":\"2063\",\"OperatingCarrier\":\"LATAM AIRLINES PERU\"," +
                "\"Legs\":[{\"DepartAirport\":{\"AirportCode\":\"LIM\",\"Terminal\":\"\",\"Name\":[\"Jorge Chavez " +
                "International Arpt\"],\"Address\":null,\"City\":\"Lima\",\"State\":null,\"Zip\":null," +
                "\"Country\":\"PE\"},\"ArriveAirport\":{\"AirportCode\":\"CUZ\",\"Terminal\":\"\",\"Name\":[\"A " +
                "Velasco Astete Intl\"],\"Address\":null,\"City\":\"Cuzco\",\"State\":null,\"Zip\":null," +
                "\"Country\":\"PE\"},\"Depart\":\"2018-06-01T16:10:00\",\"Arrive\":\"2018-06-01T17:27:00\"," +
                "\"TravelTime\":\"1H 17M\",\"FlightInfo\":[\"Airbus A320\",\"Meal at cost\"],\"LegMiles\":\"\"," +
                "\"FlightNumber\":\"2063\",\"AirlineName\":\"LATAM Airlines Group\",\"OperatingCarrier\":\"LATAM " +
                "AIRLINES PERU\",\"Seats\":null}],\"AssociatedRemarks\":[],\"Arrive\":\"2018-06-01T17:27:00\"," +
                "\"Depart\":\"2018-06-01T16:10:00\",\"TravelTime\":\"1H 17M\",\"IsPassive\":false," +
                "\"OnlineCheckIn\":null,\"SpecialServiceRequest\":[{\"FirstName\":\"LORENE MS\"," +
                "\"LastName\":\"NOVAKOWSKI\",\"Description\":\"Vegetarian Lacto-Ovo Meal\",\"Code\":\"VLML\"," +
                "\"AgentComment\":\"\"}],\"ChangeOfDay\":null,\"ChangeOfPlanes\":false," +
                "\"EquipmentInfo\":{\"Code\":\"320\",\"Description\":\"Airbus A320\"}," +
                "\"Guests\":[{\"FirstName\":\"LORENE MS\",\"LastName\":\"NOVAKOWSKI\",\"Type\":\"\"," +
                "\"LoyaltyInfo\":\"\",\"FrequentGuestInfo\":null,\"EticketNumber\":\"0455797569001\"," +
                "\"TicketNumber\":null,\"SeatInfo\":[{\"Number\":\"08J\",\"DepartureAirportCode\":\"LIM\"," +
                "\"ArrivalAirportCode\":\"CUZ\",\"StatusEnum\":1,\"Coach\":null}]},{\"FirstName\":\"ALAN MR\"," +
                "\"LastName\":\"SUDEYKO\",\"Type\":\"\",\"LoyaltyInfo\":\"\",\"FrequentGuestInfo\":null," +
                "\"EticketNumber\":\"0455797569002\",\"TicketNumber\":null,\"SeatInfo\":[{\"Number\":\"08K\"," +
                "\"DepartureAirportCode\":\"LIM\",\"ArrivalAirportCode\":\"CUZ\",\"StatusEnum\":1,\"Coach\":null}]}]," +
                "\"Confirmation\":\"ALPLSW\",\"Logo\":\"LA\",\"Title\":\"\",\"ClassOfService\":{\"Code\":\"V\"," +
                "\"Description\":\"Economy\"},\"SegmentDates\":[\"2018-06-30T16:10:00\",\"2018-06-30T17:27:00\"]," +
                "\"DepartName\":\"Lima\",\"DepartCode\":\"LIM\",\"ArriveName\":\"Cuzco\",\"ArriveCode\":\"CUZ\"," +
                "\"SegmentType\":\"Flight\",\"StatusEnum\":1},{\"SegmentNum\":5,\"Stops\":0,\"Seats\":null," +
                "\"AirlineName\":\"LATAM Airlines Group\",\"FlightNumber\":\"2054\",\"OperatingCarrier\":\"LATAM " +
                "AIRLINES PERU\",\"Legs\":[{\"DepartAirport\":{\"AirportCode\":\"CUZ\",\"Terminal\":\"\"," +
                "\"Name\":[\"A Velasco Astete Intl\"],\"Address\":null,\"City\":\"Cuzco\",\"State\":null," +
                "\"Zip\":null,\"Country\":\"PE\"},\"ArriveAirport\":{\"AirportCode\":\"LIM\",\"Terminal\":\"\"," +
                "\"Name\":[\"Jorge Chavez International Arpt\"],\"Address\":null,\"City\":\"Lima\",\"State\":null," +
                "\"Zip\":null,\"Country\":\"PE\"},\"Depart\":\"2018-06-01T08:37:00\"," +
                "\"Arrive\":\"2018-06-01T10:04:00\",\"TravelTime\":\"1H 27M\",\"FlightInfo\":[\"Airbus A320\",\"Meal " +
                "at cost\"],\"LegMiles\":\"\",\"FlightNumber\":\"2054\",\"AirlineName\":\"LATAM Airlines Group\"," +
                "\"OperatingCarrier\":\"LATAM AIRLINES PERU\",\"Seats\":null}],\"AssociatedRemarks\":[]," +
                "\"Arrive\":\"2018-06-01T10:04:00\",\"Depart\":\"2018-06-01T08:37:00\",\"TravelTime\":\"1H 27M\"," +
                "\"IsPassive\":false,\"OnlineCheckIn\":null,\"SpecialServiceRequest\":[{\"FirstName\":\"LORENE MS\"," +
                "\"LastName\":\"NOVAKOWSKI\",\"Description\":\"Vegetarian Lacto-Ovo Meal\",\"Code\":\"VLML\"," +
                "\"AgentComment\":\"\"}],\"ChangeOfDay\":null,\"ChangeOfPlanes\":false," +
                "\"EquipmentInfo\":{\"Code\":\"320\",\"Description\":\"Airbus A320\"}," +
                "\"Guests\":[{\"FirstName\":\"LORENE MS\",\"LastName\":\"NOVAKOWSKI\",\"Type\":\"\"," +
                "\"LoyaltyInfo\":\"\",\"FrequentGuestInfo\":null,\"EticketNumber\":\"0455797569001\"," +
                "\"TicketNumber\":null,\"SeatInfo\":[{\"Number\":\"09B\",\"DepartureAirportCode\":\"CUZ\"," +
                "\"ArrivalAirportCode\":\"LIM\",\"StatusEnum\":1,\"Coach\":null}]},{\"FirstName\":\"ALAN MR\"," +
                "\"LastName\":\"SUDEYKO\",\"Type\":\"\",\"LoyaltyInfo\":\"\",\"FrequentGuestInfo\":null," +
                "\"EticketNumber\":\"0455797569002\",\"TicketNumber\":null,\"SeatInfo\":[{\"Number\":\"09C\"," +
                "\"DepartureAirportCode\":\"CUZ\",\"ArrivalAirportCode\":\"LIM\",\"StatusEnum\":1,\"Coach\":null}]}]," +
                "\"Confirmation\":\"ALPLSW\",\"Logo\":\"LA\",\"Title\":\"\",\"ClassOfService\":{\"Code\":\"V\"," +
                "\"Description\":\"Economy\"},\"SegmentDates\":[\"2018-07-05T08:37:00\",\"2018-07-05T10:04:00\"]," +
                "\"DepartName\":\"Cuzco\",\"DepartCode\":\"CUZ\",\"ArriveName\":\"Lima\",\"ArriveCode\":\"LIM\"," +
                "\"SegmentType\":\"Flight\",\"StatusEnum\":1},{\"SegmentNum\":6,\"Stops\":0,\"Seats\":null," +
                "\"AirlineName\":null,\"FlightNumber\":\"7389\",\"OperatingCarrier\":\"\"," +
                "\"Legs\":[{\"DepartAirport\":{\"AirportCode\":\"LIM\",\"Terminal\":\"\",\"Name\":[\"Jorge Chavez " +
                "International Arpt\"],\"Address\":null,\"City\":\"Lima\",\"State\":null,\"Zip\":null," +
                "\"Country\":\"PE\"},\"ArriveAirport\":{\"AirportCode\":\"UIO\",\"Terminal\":\"\"," +
                "\"Name\":[\"Mariscal Sucre Intl\"],\"Address\":null,\"City\":\"Quito\",\"State\":null,\"Zip\":null," +
                "\"Country\":\"EC\"},\"Depart\":\"2018-06-01T10:32:00\",\"Arrive\":\"2018-06-01T13:02:00\"," +
                "\"TravelTime\":\"\",\"FlightInfo\":[],\"LegMiles\":\"\",\"FlightNumber\":\"7389\"," +
                "\"AirlineName\":null,\"OperatingCarrier\":\"\",\"Seats\":null}],\"AssociatedRemarks\":[\"FLIGHT " +
                "OPERATED BY AVIANCA AIRLINES\",\"FLIGHT BOOKED AND PAID THRU METROPOLITAN TOURING\"]," +
                "\"Arrive\":\"2018-06-01T13:02:00\",\"Depart\":\"2018-06-01T10:32:00\",\"TravelTime\":\"\"," +
                "\"IsPassive\":false,\"OnlineCheckIn\":null,\"SpecialServiceRequest\":[],\"ChangeOfDay\":null," +
                "\"ChangeOfPlanes\":false,\"EquipmentInfo\":{\"Code\":\"\",\"Description\":\"\"}," +
                "\"Guests\":[{\"FirstName\":\"LORENE MS\",\"LastName\":\"NOVAKOWSKI\",\"Type\":\"\"," +
                "\"LoyaltyInfo\":\"\",\"FrequentGuestInfo\":null,\"EticketNumber\":null,\"TicketNumber\":null," +
                "\"SeatInfo\":[]},{\"FirstName\":\"ALAN MR\",\"LastName\":\"SUDEYKO\",\"Type\":\"\"," +
                "\"LoyaltyInfo\":\"\",\"FrequentGuestInfo\":null,\"EticketNumber\":null,\"TicketNumber\":null," +
                "\"SeatInfo\":[]}],\"Confirmation\":\"\",\"Logo\":\"YY\",\"Title\":\"\"," +
                "\"ClassOfService\":{\"Code\":\"Y\",\"Description\":\"Economy\"}," +
                "\"SegmentDates\":[\"2018-07-07T10:32:00\",\"2018-07-07T13:02:00\"],\"DepartName\":\"Lima\"," +
                "\"DepartCode\":\"LIM\",\"ArriveName\":\"Quito\",\"ArriveCode\":\"UIO\",\"SegmentType\":\"Flight\"," +
                "\"StatusEnum\":0},{\"SegmentNum\":7,\"Stops\":0,\"Seats\":null,\"AirlineName\":\"Aeromexico\"," +
                "\"FlightNumber\":\"685\",\"OperatingCarrier\":\"\"," +
                "\"Legs\":[{\"DepartAirport\":{\"AirportCode\":\"UIO\",\"Terminal\":\"\",\"Name\":[\"Mariscal Sucre " +
                "Intl\"],\"Address\":null,\"City\":\"Quito\",\"State\":null,\"Zip\":null,\"Country\":\"EC\"}," +
                "\"ArriveAirport\":{\"AirportCode\":\"MEX\",\"Terminal\":\"2\",\"Name\":[\"Mexico City Juarez " +
                "Intl\"],\"Address\":null,\"City\":\"Mexico City\",\"State\":null,\"Zip\":null,\"Country\":\"MX\"}," +
                "\"Depart\":\"2018-06-01T08:35:00\",\"Arrive\":\"2018-06-01T13:30:00\",\"TravelTime\":\"4H 55M\"," +
                "\"FlightInfo\":[\"Hot meal, Breakfast\"],\"LegMiles\":\"\",\"FlightNumber\":\"685\"," +
                "\"AirlineName\":\"Aeromexico\",\"OperatingCarrier\":\"\",\"Seats\":null}]," +
                "\"AssociatedRemarks\":[\"PLUS SEATS 7B 7C\"],\"Arrive\":\"2018-06-01T13:30:00\"," +
                "\"Depart\":\"2018-06-01T08:35:00\",\"TravelTime\":\"4H 55M\",\"IsPassive\":false," +
                "\"OnlineCheckIn\":null,\"SpecialServiceRequest\":[{\"FirstName\":\"LORENE MS\"," +
                "\"LastName\":\"NOVAKOWSKI\",\"Description\":\"Vegetarian Lacto-Ovo Meal\",\"Code\":\"VLML\"," +
                "\"AgentComment\":\"\"}],\"ChangeOfDay\":null,\"ChangeOfPlanes\":false," +
                "\"EquipmentInfo\":{\"Code\":\"7S8\",\"Description\":\"\"},\"Guests\":[{\"FirstName\":\"LORENE MS\"," +
                "\"LastName\":\"NOVAKOWSKI\",\"Type\":\"\",\"LoyaltyInfo\":\"\",\"FrequentGuestInfo\":null," +
                "\"EticketNumber\":\"1395797569003\",\"TicketNumber\":null,\"SeatInfo\":[{\"Number\":\"07B\"," +
                "\"DepartureAirportCode\":\"UIO\",\"ArrivalAirportCode\":\"MEX\",\"StatusEnum\":1,\"Coach\":null}]}," +
                "{\"FirstName\":\"ALAN MR\",\"LastName\":\"SUDEYKO\",\"Type\":\"\",\"LoyaltyInfo\":\"\"," +
                "\"FrequentGuestInfo\":null,\"EticketNumber\":\"1395797569005\",\"TicketNumber\":null," +
                "\"SeatInfo\":[]}],\"Confirmation\":\"GSIOZD\",\"Logo\":\"AM\",\"Title\":\"\"," +
                "\"ClassOfService\":{\"Code\":\"Q\",\"Description\":\"Economy\"}," +
                "\"SegmentDates\":[\"2018-07-14T08:35:00\",\"2018-07-14T13:30:00\"],\"DepartName\":\"Quito\"," +
                "\"DepartCode\":\"UIO\",\"ArriveName\":\"Mexico City\",\"ArriveCode\":\"MEX\"," +
                "\"SegmentType\":\"Flight\",\"StatusEnum\":1},{\"SegmentNum\":8,\"Stops\":0,\"Seats\":null," +
                "\"AirlineName\":\"Aeromexico\",\"FlightNumber\":\"696\",\"OperatingCarrier\":\"\"," +
                "\"Legs\":[{\"DepartAirport\":{\"AirportCode\":\"MEX\",\"Terminal\":\"2\",\"Name\":[\"Mexico City " +
                "Juarez Intl\"],\"Address\":null,\"City\":\"Mexico City\",\"State\":null,\"Zip\":null," +
                "\"Country\":\"MX\"},\"ArriveAirport\":{\"AirportCode\":\"YVR\",\"Terminal\":\"M\"," +
                "\"Name\":[\"Vancouver Intl Arpt\"],\"Address\":null,\"City\":\"Vancouver\",\"State\":\"BC\"," +
                "\"Zip\":null,\"Country\":\"CA\"},\"Depart\":\"2018-06-01T18:00:00\"," +
                "\"Arrive\":\"2018-06-01T21:55:00\",\"TravelTime\":\"5H 55M\",\"FlightInfo\":[\"Hot meal, Alcohol no " +
                "cost\"],\"LegMiles\":\"\",\"FlightNumber\":\"696\",\"AirlineName\":\"Aeromexico\"," +
                "\"OperatingCarrier\":\"\",\"Seats\":null}],\"AssociatedRemarks\":[\"PLUS SEATS 7B 7C\"]," +
                "\"Arrive\":\"2018-06-01T21:55:00\",\"Depart\":\"2018-06-01T18:00:00\",\"TravelTime\":\"5H 55M\"," +
                "\"IsPassive\":false,\"OnlineCheckIn\":null,\"SpecialServiceRequest\":[{\"FirstName\":\"LORENE MS\"," +
                "\"LastName\":\"NOVAKOWSKI\",\"Description\":\"Vegetarian Lacto-Ovo Meal\",\"Code\":\"VLML\"," +
                "\"AgentComment\":\"\"}],\"ChangeOfDay\":null,\"ChangeOfPlanes\":false," +
                "\"EquipmentInfo\":{\"Code\":\"7S8\",\"Description\":\"\"},\"Guests\":[{\"FirstName\":\"LORENE MS\"," +
                "\"LastName\":\"NOVAKOWSKI\",\"Type\":\"\",\"LoyaltyInfo\":\"\",\"FrequentGuestInfo\":null," +
                "\"EticketNumber\":\"1395797569004\",\"TicketNumber\":null,\"SeatInfo\":[{\"Number\":\"07C\"," +
                "\"DepartureAirportCode\":\"MEX\",\"ArrivalAirportCode\":\"YVR\",\"StatusEnum\":1,\"Coach\":null}]}," +
                "{\"FirstName\":\"ALAN MR\",\"LastName\":\"SUDEYKO\",\"Type\":\"\",\"LoyaltyInfo\":\"\"," +
                "\"FrequentGuestInfo\":null,\"EticketNumber\":\"1395797569006\",\"TicketNumber\":null," +
                "\"SeatInfo\":[]}],\"Confirmation\":\"GSIOZD\",\"Logo\":\"AM\",\"Title\":\"\"," +
                "\"ClassOfService\":{\"Code\":\"Q\",\"Description\":\"Economy\"}," +
                "\"SegmentDates\":[\"2018-07-14T18:00:00\",\"2018-07-14T21:55:00\"],\"DepartName\":\"Mexico City\"," +
                "\"DepartCode\":\"MEX\",\"ArriveName\":\"Vancouver\",\"ArriveCode\":\"YVR\"," +
                "\"SegmentType\":\"Flight\",\"StatusEnum\":1}],\"UnassociatedRemarks\":[\"JOHN AND KATE DIRECT PHONE " +
                "604 739 3604\",\"DIRECT TOLL FREE 1 877 739 3604\",\".\",\"PLEASE SEE COPY OF ELECTRONIC TICKET " +
                "RECEIPT.\",\"THIS MUST BE PRESENTED TO AIRLINE AT CHECK IN ALONG\",\"WITH PHOTO I.D. A BOARDING PASS" +
                " WILL BE ISSUED AT CHECK IN\",\".\",\"DUE TO HEIGHTENED SECURITY EARLIER CHECK IN TIMES ARE " +
                "SUGGESTED\",\"60 MINUTES - DOMESTIC. 2 HOURS - TRANSBORDER. 3 HOURS INTERNATIONAL\",\"PLEASE NOTE - " +
                "NO SHARP OBJECTS IN CARRY ON BAGGAGE\",\"LIQUIDS/LOTIONS/GELS PERMITTED IN LIMITED QUANTITIES.\"," +
                "\"PHOTO ID IS MANDATORY FOR TRAVEL TO ALL DESTINATIONS\",\".\",\"A VALID PASSPORT - VALID AT LEAST 6" +
                " MONTHS BEYOND RETURN\",\"DATE OF TRAVEL - IS REQUIRED FOR YOUR JOURNEY\",\".\",\"THIS FARE IS " +
                "NONREFUNDABLE - CHANGE RESTRICTIONS APPLY\",\".\",\"PLEASE RECONFIRM YOUR FLIGHT TIMES BEFORE " +
                "DEPARTURE\",\"DOMESTIC - 24 HOURS PRIOR. INTERNATIONAL - 72 HOURS PRIOR\"],\"HasETicket\":true," +
                "\"Is24HourFormat\":false,\"Agency\":{\"Phone\":\"604 739-3622 PERSONAL TRVL MGMT 6167276-3/-KATE\"," +
                "\"IsWhiteLabel\":true,\"PhoneList\":[\"604 739-3622 PERSONAL TRVL MGMT 6167276-3/-KATE\"]," +
                "\"Name\":[\"PERSONAL TRAVEL MGMT\"],\"Address\":\"221 - 8678 GREENALL AVE.\",\"City\":\"BURNABY\"," +
                "\"State\":\"BC\",\"Zip\":\"V5J3M6\",\"Country\":\"CANADA\"},\"CannedRemarks\":[],\"Core\":0," +
                "\"ProviderCode\":\"1V\",\"MarketingAgreement\":false,\"EmailAddress\":null,\"ShowExpense\":true," +
                "\"HasEReceipt\":false,\"OtherVendorConfirmation\":[],\"PassengerFilter\":null}";

    //String s6 = getItinerary("ROBINSON", "OA6QE4");
    TripVO vo = parseItinerary(s6, "TF2JSE");
    System.out.println(vo);
  }

  public static Timestamp getTimestamp (String timestamp) {
    if (timestamp.startsWith("0"))
      return null;

    try {
      Timestamp t = new Timestamp(du.parseDate(timestamp, dateTimeFormat).getTime() );
      return t;
    } catch (Exception e) {
    }
    return null;
  }

  public static  String cleanFirstName(String firstName) {
    if (firstName != null) {
      if (firstName.endsWith("MR")) {
        firstName = firstName.substring(0, firstName.lastIndexOf("MR")).trim();
      } else if (firstName.endsWith("MS")) {
        firstName = firstName.substring(0, firstName.lastIndexOf("MS")).trim();
      } else if (firstName.endsWith("MRS")) {
        firstName = firstName.substring(0, firstName.lastIndexOf("MRS")).trim();
      }
    }
    return firstName;
  }
}
