package com.umapped.persistence.reservation.flight;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UmAirplaneSeatClass
    implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * [Text] The IATA class code for the seat.
   */
  public String iataClassCode;
  /**
   * [Text] The seat category.
   */
  public String seatCategory;
}
