package com.mapped.persistence;

import com.mapped.persistence.util.DBConnectionMgr;
import com.mapped.publisher.utils.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class ChatMsgsMgr {

  private static final String SELECT_ALL       = "Select Pk, ChatId, UsersId, Msg, LastUpdateTimestamp, " +
																								 "CreateTimestamp, SyncTimestamp from ChatMsgs;";
  private static final String SELECT_ALL_BY_PK = "Select Pk, ChatId, UsersId, Msg, LastUpdateTimestamp, " +
																								 "CreateTimestamp, SyncTimestamp from ChatMsgs where Pk = ?;";
  private static final String DELETE_BY_PK     = "delete from ChatMsgs where Pk = ?;";
  private static final String INSERT           = "insert into ChatMsgs (Pk, ChatId, UsersId, Msg, " +
																								 "LastUpdateTimestamp, CreateTimestamp, SyncTimestamp) values (?, ?, " +
																								 "?, ?, ?, ?, ?);";
  private static final String UPDATE           = "update ChatMsgs SET ChatId=?, UsersId=?, Msg=?, " +
																								 "LastUpdateTimestamp=?, SyncTimestamp=? WHERE Pk = ?; ";

  public static ArrayList<ChatMsgs> getAll()
      throws SQLException {
    ResultSet         rs   = null;
    PreparedStatement prep = null;
    Connection        conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(SELECT_ALL);
      rs = prep.executeQuery();
      return ChatMsgsMgr.handleResults(rs);
    }
    catch (Exception e) {
      Log.err("ChatMsgsMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
    return null;
  }

  public static ArrayList<ChatMsgs> handleResults(ResultSet rs)
      throws SQLException {
    ArrayList<ChatMsgs> results = new ArrayList<>();
    if (rs == null) {
      return results;
    }
    while (rs.next()) {
      ChatMsgs c = new ChatMsgs();
      c.setPk(rs.getString("Pk"));
      c.setChatId(rs.getString("ChatId"));
      c.setUsersId(rs.getString("UsersId"));
      c.setMsg(rs.getString("Msg"));
      c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
      c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
      c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
      results.add(c);
    }
    return results;
  }

  public static ChatMsgs getByPk(String id)
      throws SQLException {
    ResultSet         rs   = null;
    PreparedStatement prep = null;
    Connection        conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(SELECT_ALL_BY_PK);
      prep.setString(1, id);
      rs = prep.executeQuery();
      ArrayList<ChatMsgs> results = ChatMsgsMgr.handleResults(rs);
      if (results != null && results.size() == 1) {
        return results.get(0);
      }
      else {
        return null;
      }
    }
    catch (Exception e) {
      Log.err("ChatMsgsMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      if (rs != null) {
        rs.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
    return null;
  }

  public static void deleteByPK(String id)
      throws SQLException {
    PreparedStatement prep = null;
    Connection        conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(DELETE_BY_PK);
      prep.setString(1, id);
      prep.execute();
    }
    catch (Exception e) {
      Log.err("ChatMsgsMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
  }

  public static void insert(ChatMsgs c)
      throws SQLException {
    PreparedStatement prep = null;
    Connection        conn = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(INSERT);
      prep.setString(1, c.getPk());
      prep.setString(2, c.getChatId());
      prep.setString(3, c.getUsersId());
      prep.setString(4, c.getMsg());
      prep.setLong(5, c.getLastUpdateTimestamp());
      prep.setLong(6, c.getCreateTimestamp());
      prep.setLong(7, c.getSyncTimestamp());
      prep.execute();
    }
    catch (Exception e) {
      Log.err("ChatMsgsMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
  }

  public static void update(ChatMsgs c)
      throws SQLException {
    Connection        conn = null;
    PreparedStatement prep = null;
    try {
      conn = DBConnectionMgr.getConnection4Mobile();
      prep = conn.prepareStatement(UPDATE);
      prep.setString(1, c.getChatId());
      prep.setString(2, c.getUsersId());
      prep.setString(3, c.getMsg());
      prep.setLong(4, c.getLastUpdateTimestamp());
      prep.setLong(5, c.getSyncTimestamp());
      prep.setString(6, c.getPk());
      prep.execute();
    }
    catch (Exception e) {
      Log.err("ChatMsgsMgr: Exception", e);
    }
    finally {
      if (prep != null) {
        prep.close();
      }
      DBConnectionMgr.closeConnection(conn);
    }
  }
}
