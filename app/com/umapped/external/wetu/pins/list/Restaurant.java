package com.umapped.external.wetu.pins.list;

/**
 * Created by george on 2016-06-07.
 */

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "features"
})
public class Restaurant extends Pin {
  @JsonProperty("features")
  private Features_Restaurant features;

  @JsonProperty("features")
  public Features_Restaurant getFeatures() {
    return features;
  }

  @JsonProperty("features")
  public void setFeatures(Features_Restaurant features) {
    this.features = features;
  }
}
