package com.mapped.publisher.parse.extractor.booking;

import com.mapped.publisher.common.BookingSrc;
import com.mapped.publisher.parse.ParseError;
import com.mapped.publisher.parse.extractor.booking.valueObject.AirportVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.FlightVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.PassengerVO;
import com.mapped.publisher.parse.extractor.booking.valueObject.TripVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-10-25
 * Time: 9:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class AmadeusExtractor extends BookingExtractor {
    private static final int SERVICE_COLUMN_LENGTH = 15;
    private static final int FROM_COLUMN_LENGTH = 20;
    private static final int TO_COLUMN_LENGTH = 22;
    private static final int DEPART_COLUMN_LENGTH = 9;
    private static final int ARRIVE_COLUMN_LENGTH = 7;

    private static final SimpleDateFormat ddMMMMMyy_DATE_FORMAT = new SimpleDateFormat("ddMMMMMyy");
    private static final SimpleDateFormat EEE_ddMMMhhmma_DATE_FORMAT = new SimpleDateFormat("EEE ddMMMhhmma");

    private static final String BOOKING_DATE_IDENTIFIER = "DATE ";
    private static final String RESERVATION_NUMBERS_IDENTIFIER = "RESERVATION NUMBER(S) ";
    private static final String TICKET_IDENTIFIER = "TICKET:";
    private static final String ETKT_IDENTIFIER = "ETKT ";

    private static final String EMPTY_LINES_REGEX = "(\\s+)(\\s+)(\\n)"; //  "(\\s+)(\\s+)(\\r)(\\n)"; //
    private static final String NEW_LINE_REGEX = "(\\n)"; // "(\\r)(\\n)";   //

    protected void initImpl(Map<String, String> params) {

    }

    protected TripVO extractDataImpl(InputStream input) throws Exception {
        try {
            TripVO trip = new TripVO();

            // get the entire pdf content as String
            String pdfString = getPDFContentAsString(input);

            // substring booking information from pdfString
            String bookingInformation = substringBookingInformation(pdfString);
            Calendar bookingDateAsCalendar = getBookingDate(bookingInformation);

            // substring all the flight details from pdfString
            String flightDetailsString = substringAllFlightDetails(pdfString);

            // substring passenger information from pdfString
            String passengerInformation = substringPassengerInformation(pdfString);

            // parse flight details
            Map<String, List<FlightVO>> flightsByAirlineCode = parseFlightDetails(flightDetailsString, bookingDateAsCalendar);

            // parse the passenger details
            List<PassengerVO> passengers = parsePassengerDetails(passengerInformation, flightsByAirlineCode);

            List<FlightVO> allFlights = new ArrayList<FlightVO>();

            for (String airlineCode : flightsByAirlineCode.keySet()) {
                List<FlightVO> flightList = flightsByAirlineCode.get(airlineCode);
                if (flightList != null) {
                    for (FlightVO flightVO : flightList) {
                        if (flightVO.getDepartureAirport() != null && flightVO.getDepatureTime() != null && flightVO.getCode() != null) {
                            allFlights.add(flightVO);
                        }
                    }
                }
            }

            trip.setFlights(allFlights);

            if (passengers != null)
                trip.setPassengers(passengers);


          trip.setImportSrc(BookingSrc.ImportSrc.AMADEUS_PDF);
          trip.setImportTs(System.currentTimeMillis());

            return trip;
        } finally {
            if (input != null) {
                input.close();
            }
        }


    }


    /**
     * Get the booking date
     *
     * @param bookingInformation
     * @return booking date as Calendar
     * @throws java.text.ParseException
     */
    private static Calendar getBookingDate(String bookingInformation) throws ParseException {
        Calendar cal = Calendar.getInstance();

        String identifier = BOOKING_DATE_IDENTIFIER;
        int start = bookingInformation.indexOf(identifier) + identifier.length();
        int end = start + 13;
        String bookingDateString = StringUtils.substring(bookingInformation, start, end);
        cal.setTime(ddMMMMMyy_DATE_FORMAT.parse(bookingDateString));
        return cal;
    }

    /**
     * Parse the flight details
     *
     * @param flightDetailsString
     * @param bookingDateAsCalendar
     * @return flightsByAirlineCode
     * @throws ParseException
     */
    private static Map<String, List<FlightVO>> parseFlightDetails(String flightDetailsString,
                                                                Calendar bookingDateAsCalendar) throws ParseException {

        // split the flight details based on the empty lines
        String flightDetailsArray[] = flightDetailsString.split(EMPTY_LINES_REGEX);

        FlightVO flight = null;
        AirportVO departureAirport = null;
        AirportVO arrivalAirport = null;
        List<FlightVO> flightList = null;

        // KEY: airlineCode VALUE: flightList
        Map<String, List<FlightVO>> flightsByAirlineCode = new HashMap<String, List<FlightVO>>();

        for (String flightDetails : flightDetailsArray) {


            // split each flight to lines
            String[] lineByLineFlightDetails = flightDetails.split(NEW_LINE_REGEX);
            String airlineCode = null;

            if (lineByLineFlightDetails.length > 3) {
                // we are interested only in first 3 lines of data

                flight = new FlightVO();
                departureAirport = new AirportVO();
                arrivalAirport = new AirportVO();
                for (int i = 0; i < 3; i++) {

                    switch (i + 1) {

                        // Line 1: Flight name and Flight code & number separated by "-"
                        // eg. AIR INDIA LIMITED - AI 442
                        case 1:
                            String[] flightNameAndCodeNumber = StringUtils.split(lineByLineFlightDetails[i], "-");
                            flight.setName(StringUtils.trim(flightNameAndCodeNumber[0]));

                            // split the flight code and number (eg. AI 442 -> AI is code and 442 is number)
                            String[] flightCodeAndNumber = StringUtils.split(flightNameAndCodeNumber[1]);
                            airlineCode = StringUtils.trim(flightCodeAndNumber[0]);
                            flight.setCode(airlineCode);
                            flight.setNumber(StringUtils.trim(flightCodeAndNumber[1]));
                            break;

                        // Line 2: Departure date, departure city & country code, arrival city & country code, departure time, arrival time
                        // eg. FRI 03JAN MUMBAI IN AURANGABAD IN 0300P 0350P
                        case 2:
                            String depatureAndArrivalDetails = lineByLineFlightDetails[i];
                            String departureDate = getServiceColumn(depatureAndArrivalDetails);

                            String departureCityAndCountryCode = getFromColumn(depatureAndArrivalDetails);
                            departureAirport.setCity(StringUtils.substring(
                                    departureCityAndCountryCode, 0,
                                    departureCityAndCountryCode.length() - 2).trim());
                            departureAirport.setCountry(StringUtils.substring(
                                    departureCityAndCountryCode,
                                    departureCityAndCountryCode.length() - 2).trim());

                            String arrivalCityAndCountryCode = getToColumn(depatureAndArrivalDetails);
                            arrivalAirport.setCity(StringUtils.substring(
                                    arrivalCityAndCountryCode, 0,
                                    arrivalCityAndCountryCode.length() - 2).trim());
                            arrivalAirport.setCountry(StringUtils.substring(
                                    arrivalCityAndCountryCode,
                                    arrivalCityAndCountryCode.length() - 2).trim());

                            String departureDateTime = departureDate + getDepartColumn(depatureAndArrivalDetails);
                            flight.setDepatureTime(formatFlightTime(departureDateTime, bookingDateAsCalendar));

                            String arrivalDateTime = departureDate + getArriveColumn(depatureAndArrivalDetails);
                            flight.setArrivalTime(formatFlightTime(arrivalDateTime, bookingDateAsCalendar));

                            break;

                        // Line 3: Stops, Departure airport, Arrival airport
                        // eg. NON STOP CHHATRAPATI SHIVAJI AURANGABAD
                        case 3:
                            depatureAndArrivalDetails = lineByLineFlightDetails[i];
                            departureAirport.setName(getFromColumn(depatureAndArrivalDetails));
                            arrivalAirport.setName(getToColumn(depatureAndArrivalDetails));
                            break;
                        default:
                            break;
                    }
                }
                flight.setDepartureAirport(departureAirport);
                flight.setArrivalAirport(arrivalAirport);

                // add the flight to the map by airline code
                flightList = flightsByAirlineCode.get(airlineCode);

                if (flightList == null) {
                    flightList = new ArrayList<FlightVO>();
                    flightList.add(flight);
                    flightsByAirlineCode.put(airlineCode, flightList);
                } else {
                    flightList.add(flight);
                }
            }



        }

        return flightsByAirlineCode;
    }

    /**
     * Parses the passenger details
     *
     * @param passengerInformation
     * @param flightsByAirlineCode
     * @throws Exception
     */
    private static List<PassengerVO> parsePassengerDetails (String passengerInformation,
                                               Map<String, List<FlightVO>> flightsByAirlineCode) throws Exception {

        // split lines
        String[] lineByLineDetails = passengerInformation.split(NEW_LINE_REGEX);

        // KEY: airlineCode VALUE: passengerList
        Map<String, List<PassengerVO>> passengerListByAirlineCode = new HashMap<String, List<PassengerVO>>();

        // KEY: airlineCode VALUE: reservationNumber
        Map<String, String> reservationNumnberByAirlineCode = new HashMap<String, String>();

        PassengerVO passenger = null;
        List<PassengerVO> passengerList = null;

        for (String data : lineByLineDetails) {
            if (StringUtils.isNotBlank(data)) {

                // get the reservation numbers
                if (data.startsWith(RESERVATION_NUMBERS_IDENTIFIER)) {
                    String[] reservationNumbers = StringUtils.split(
                            data.replace(RESERVATION_NUMBERS_IDENTIFIER, StringUtils.EMPTY));

                    for (String reservationNum : reservationNumbers) {
                        String[] airlineCodeAndReservationNumber = StringUtils.split(reservationNum, "/");
                        if (airlineCodeAndReservationNumber.length >= 2) {
                            String airlineCode = StringUtils.trim(airlineCodeAndReservationNumber[0]);
                            reservationNumnberByAirlineCode.put(airlineCode,StringUtils.trim(airlineCodeAndReservationNumber[1]));
                        }
                    }
                }
                // get the passenger details
                else {

                    String passengerFullName = data.substring(0, data.indexOf(TICKET_IDENTIFIER)).trim();

                    passenger = new PassengerVO();
                    passenger.setFirstName(passengerFullName.substring(passengerFullName.indexOf("/") + 1,
                            passengerFullName.length() - 2).trim());
                    passenger.setLastName(passengerFullName.substring(0, passengerFullName.indexOf("/")));
                    passenger.setTitle((passengerFullName.substring(passengerFullName.length() - 2)));

                    String eTicketDetails = data.substring(data.indexOf(TICKET_IDENTIFIER)
                            + (TICKET_IDENTIFIER).length()).trim();
                    String[] airlineCodeAndeTicketNumber = eTicketDetails.split("/");
                    String airlineCode = airlineCodeAndeTicketNumber[0];
                    passenger.seteTicketNumber(airlineCodeAndeTicketNumber[1].substring(
                            airlineCodeAndeTicketNumber[1].indexOf(ETKT_IDENTIFIER) + (ETKT_IDENTIFIER).length()));

                    passengerList = passengerListByAirlineCode.get(airlineCode);
                    if (passengerList == null) {
                        passengerList = new ArrayList<PassengerVO>();
                        passengerList.add(passenger);
                        passengerListByAirlineCode.put(airlineCode, passengerList);
                    } else {
                        passengerList.add(passenger);
                    }
                }
            }
        }

        List<FlightVO> flightList = null;

        // update the reservation numbers and passengerList in the Flight object
        for (String airlineCode : passengerListByAirlineCode.keySet()){
            flightList = flightsByAirlineCode.get(airlineCode);

            if (flightList != null && !flightList.isEmpty()) {
                for (FlightVO flight : flightList) {
                    String reservationNumber = reservationNumnberByAirlineCode.get(airlineCode);
                    if (StringUtils.isNotBlank(reservationNumber)) {
                        flight.setReservationNumber(reservationNumber);
                    } else {
                        throw new Exception("No reservation number found for airline code: " + airlineCode
                                + ". But flight and passenger details found");
                    }

                    flight.setPassengers(passengerListByAirlineCode.get(airlineCode));
                }
            } else {
                throw new Exception("No flight found for the airline code: " + airlineCode
                        + ". But passenger details found");
            }

        }

        return passengerList;
    }

    /**
     * formats the flight departure and arrival time
     *
     * @param dateTimeString
     * @param bookingDateAsCalendar
     * @return formatted flight time as Timestamp
     * @throws ParseException
     */
    private static Timestamp formatFlightTime(String dateTimeString,
                                              Calendar bookingDateAsCalendar) throws ParseException {

        Calendar cal = Calendar.getInstance();

        // psotfix "M" to complete the AM / PM
        cal.setTime(EEE_ddMMMhhmma_DATE_FORMAT.parse(dateTimeString + "M"));

        // if flight month is less than booking month then flight year is booking year + 1
        if (cal.get(Calendar.MONTH) < bookingDateAsCalendar.get(Calendar.MONTH)) {
            cal.set(Calendar.YEAR, bookingDateAsCalendar.get(Calendar.YEAR) + 1);
        } else {
            cal.set(Calendar.YEAR, bookingDateAsCalendar.get(Calendar.YEAR));
        }

        return new Timestamp(cal.getTimeInMillis());
    }

    /**
     * substring the data in the SERVICE column
     *
     * @param data
     * @return service column data
     */
    private static String getServiceColumn(String data) {
        return StringUtils.trim(StringUtils.substring(data, 0, SERVICE_COLUMN_LENGTH));
    }

    /**
     * substrings the data in the FROM column
     *
     * @param data
     * @return FROM column data
     */
    private static String getFromColumn(String data) {
        return StringUtils.trim(StringUtils.substring(data,
                SERVICE_COLUMN_LENGTH, SERVICE_COLUMN_LENGTH + FROM_COLUMN_LENGTH));
    }

    /**
     * substrings the data in the TO column
     *
     * @param data
     * @return TO column data
     */
    private static String getToColumn(String data) {
        return StringUtils.trim(StringUtils.substring(data,
                SERVICE_COLUMN_LENGTH + FROM_COLUMN_LENGTH,
                SERVICE_COLUMN_LENGTH + FROM_COLUMN_LENGTH + TO_COLUMN_LENGTH));
    }

    /**
     * substrings the data in DEPART column
     *
     * @param data
     * @return DEPART column data
     */
    private static String getDepartColumn(String data) {
        return StringUtils.trim(StringUtils.substring(data,
                SERVICE_COLUMN_LENGTH + FROM_COLUMN_LENGTH + TO_COLUMN_LENGTH,
                SERVICE_COLUMN_LENGTH + FROM_COLUMN_LENGTH + TO_COLUMN_LENGTH
                        + DEPART_COLUMN_LENGTH));
    }

    /**
     * substrings the data in ARRIVE column
     *
     * @param data
     * @return ARRIVE column data
     */
    private static String getArriveColumn(String data) {
        return StringUtils.trim(StringUtils.substring(data,
                SERVICE_COLUMN_LENGTH + FROM_COLUMN_LENGTH + TO_COLUMN_LENGTH + DEPART_COLUMN_LENGTH,
                SERVICE_COLUMN_LENGTH + FROM_COLUMN_LENGTH + TO_COLUMN_LENGTH + DEPART_COLUMN_LENGTH
                        + ARRIVE_COLUMN_LENGTH));
    }


    /**
     * substring all the flight details from the pdf string
     *
     * @param pdfString
     * @return flight details as String
     */
    private static final String substringAllFlightDetails (String pdfString) {
        String identifier = "------- \n";
        int start = pdfString.lastIndexOf(identifier) + identifier.length();
        int end = pdfString.indexOf(RESERVATION_NUMBERS_IDENTIFIER);

        return StringUtils.substring(pdfString, start, end);
    }

    /**
     * substring the booking information from the pdf string
     *
     * @param pdfString
     * @return booking information
     */
    private static String substringBookingInformation (String pdfString) {
        String identifier = "ITINERARY PASSENGER RECEIPT";
        int start = pdfString.indexOf(identifier) + identifier.length();
        int end = pdfString.indexOf("SERVICE        FROM");

        return StringUtils.substring(pdfString, start, end);

    }

    /**
     * substring the passenger information from the pdf string
     *
     * @param pdfString
     * @return booking information
     */
    private static String substringPassengerInformation (String pdfString) {
        String identifier = "RESERVATION NUMBER(S)";
        int start = pdfString.indexOf(identifier);

        return StringUtils.substring(pdfString, start);

    }

    /**
     * Get the PDF content as String
     *
     * @param fileinputstream
     * @return pdf content as String
     */
    private static final String getPDFContentAsString(InputStream fis) {
        PDDocument pdfDocument = null;
        String contents = null;
        try {
            PDFParser parser = new PDFParser(fis);
            parser.parse();
            pdfDocument = parser.getPDDocument();

            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setSortByPosition(true);
            contents = stripper.getText(pdfDocument);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return contents;
    }

    @Override
    public ArrayList<ParseError> getParseErrors()
    {
        return null;
    }

  @Override
  public void postProcessingImpl(String tripId, String userId) {
  }

}
