package com.umapped.itinerary.analyze.supplier.shoretrip;

/**
 * Created by wei on 2017-04-05.
 */
public interface Constants {
  String GET_EXCURSIONS = "LOCATIONEXCURSIONS/";

  String PARAM_PARTNER = "partner";
  String PARAM_API_KEY = "accesskey";

}
