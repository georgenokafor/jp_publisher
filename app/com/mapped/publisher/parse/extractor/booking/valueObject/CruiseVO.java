package com.mapped.publisher.parse.extractor.booking.valueObject;

import java.sql.Timestamp;

/**
 * Created by Serguei Moutovkin on 2014-08-21.
 */
public class CruiseVO
    extends ReservationVO {

  private String companyName;
  private String confirmationNo;
  private Timestamp timestampDepart;
  private Timestamp timestampArrive;
  private String portDepart;
  private String portArrive;
  private String category;
  private String cabinNumber;
  private String deck;
  private String bedding;
  private String meal;

  public String toString() {
    String nl = System.getProperty("line.separator");
    StringBuilder strBuilder = new StringBuilder(">>> Cruise (Value Object)" + nl);
    strBuilder.append("Name        : " + name + nl);
    strBuilder.append("Company     : " + companyName + nl);
    strBuilder.append("Confirmation: " + confirmationNo + nl);
    strBuilder.append("Record Loctr: " + recordLocator + nl);
    strBuilder.append("Board time  : " + timestampDepart + nl);
    strBuilder.append("Disembark ts: " + timestampArrive + nl);
    strBuilder.append("Port Depart : " + portDepart + nl);
    strBuilder.append("Port Arrive : " + portArrive + nl);
    strBuilder.append("Category    : " + category + nl);
    strBuilder.append("Cabin Numb  : " + cabinNumber + nl);
    strBuilder.append("Deck        : " + deck + nl);
    strBuilder.append("Meal        : " + meal + nl);
    strBuilder.append("Bedding     : " + bedding + nl);
    strBuilder.append("Note        : " + nl + getFullNotes() + nl);
    return strBuilder.toString();
  }

  public boolean isValid() {
    if (name != null && name.trim().length() > 0 && timestampDepart != null) {
      return true;
    }
    return false;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getConfirmationNo() {
    return confirmationNo;
  }

  public void setConfirmationNo(String confirmationNo) {
    this.confirmationNo = confirmationNo;
  }

  public Timestamp getTimestampDepart() {
    return timestampDepart;
  }

  public void setTimestampDepart(Timestamp timestampDepart) {
    this.timestampDepart = timestampDepart;
  }

  public Timestamp getTimestampArrive() {
    return timestampArrive;
  }

  public void setTimestampArrive(Timestamp timestampArrive) {
    this.timestampArrive = timestampArrive;
  }

  public String getPortDepart() {
    return portDepart;
  }

  public void setPortDepart(String portDepart) {
    this.portDepart = portDepart;
  }

  public String getPortArrive() {
    return portArrive;
  }

  public void setPortArrive(String portArrive) {
    this.portArrive = portArrive;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getCabinNumber() {
    return cabinNumber;
  }

  public void setCabinNumber(String cabinNumber) {
    this.cabinNumber = cabinNumber;
  }

  public String getDeck() {
    return deck;
  }

  public void setDeck(String deck) {
    this.deck = deck;
  }

  public String getBedding() {
    return bedding;
  }

  public void setBedding(String bedding) {
    this.bedding = bedding;
  }

  public String getMeal() {
    return meal;
  }

  public void setMeal(String meal) {
    this.meal = meal;
  }
}
