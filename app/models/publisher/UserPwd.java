package models.publisher;

import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: twong
 * Date: 2013-04-20
 * Time: 9:28 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "user_pwd")
public class UserPwd
    extends Model {

  public static Model.Finder<String, UserPwd> find = new Finder(UserPwd.class);
  @Id
  public String pk;
  @Constraints.Required
  public String userid;
  public String password;
  public int status;
  public Long createdtimestamp;
  public Long lastupdatedtimestamp;
  public String createdby;
  public String modifiedby;
  @Version
  public int version;

  public static List<UserPwd> authenticate(String userId, String pwd) {
    return find
        .where()
        .eq("userId", userId)
        .eq("password", pwd)
        .eq("status", 0)
        .orderBy("lastupdatedtimestamp desc")
        .findList();
  }

  public static List<UserPwd> getActivePwd(String userId) {
    return find.where().eq("userId", userId).eq("status", 0).orderBy("lastupdatedtimestamp desc").findList();
  }

  public static UserPwd getUserPwd(String userId) {
    return find
        .where()
        .eq("userId", userId)
        .eq("status", 0)
        .orderBy("lastupdatedtimestamp desc")
        .setMaxRows(1)
        .findUnique();
  }

  public String getPk() {
    return pk;
  }

  public void setPk(String pk) {
    this.pk = pk;
  }

  public String getUserid() {
    return userid;
  }

  public void setUserId(String userid) {
    this.userid = userid;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getCreatedtimestamp() {
    return createdtimestamp;
  }

  public void setCreatedtimestamp(Long createdtimestamp) {
    this.createdtimestamp = createdtimestamp;
  }

  public Long getLastupdatedtimestamp() {
    return lastupdatedtimestamp;
  }

  public void setLastupdatedtimestamp(Long lastupdatedtimestamp) {
    this.lastupdatedtimestamp = lastupdatedtimestamp;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedby() {
    return modifiedby;
  }

  public void setModifiedby(String modifiedby) {
    this.modifiedby = modifiedby;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }
}
