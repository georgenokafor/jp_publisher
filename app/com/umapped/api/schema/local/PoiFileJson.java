package com.umapped.api.schema.local;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.umapped.api.schema.common.IBodyJson;
import com.umapped.api.schema.types.Operation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by surge on 2014-11-13.
 */
@JsonTypeName("POIFILE")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PoiFileJson extends S3FileUploadJson
    implements IBodyJson{

  @JsonProperty(required = false, value = "operation")
  public Operation operation;

  /**
   * Poi ID
   */
  @JsonProperty(required = false, value = "poi_id")
  public String poiId;

  /**
   * Company ID of the POI
   */
  @JsonProperty(required = false, value = "cmpy_id")
  public Integer cmpyId;


  public static class PoiFileDetails {
    @JsonProperty(required = true, value = "file_id")
    public String fileId;

    /** Who the file belongs to */
    @JsonProperty(required = true, value = "cmpy_id")
    public Integer cmpyId;

    /**  File name to display to the user (uploaded file name) */
    @JsonProperty(required = true, value = "name")
    public String name;

    /** File url (where to download it from) */
    @JsonProperty(required = true, value = "url")
    public String url;

    @JsonProperty(required = false, value = "desc")
    public String desc;

    /** Can be used to sort images on the page */
    @JsonProperty(required = false, value = "priority")
    public Integer priority;
  }

  @JsonProperty(required = false, value = "files")
  public List<PoiFileDetails> files = null;

  /**
   * Creates a new file based on input parameters
   * @return created file object with fields provided filled in.
   */
  public PoiFileDetails addFile(Long fileId, Integer cmpyId, String name, String url, String desc, Integer priority) {
    if (files == null) {
      files = new ArrayList<>();
    }

    PoiFileDetails pfd = new PoiFileDetails();
    pfd.fileId = fileId.toString();
    pfd.cmpyId = cmpyId;
    pfd.name = name;
    pfd.url = url;
    pfd.desc = desc;
    pfd.priority = priority;

    files.add(pfd);

    return pfd;
  }

}
