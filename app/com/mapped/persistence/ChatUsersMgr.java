package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class ChatUsersMgr {

	 private static final String SELECT_ALL = "Select Pk, ChatId, UsersId, LastUpdateTimestamp, CreateTimestamp, Status, SyncTimestamp from ChatUsers;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, ChatId, UsersId, LastUpdateTimestamp, CreateTimestamp, Status, SyncTimestamp from ChatUsers where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from ChatUsers where Pk = ?;";
	 private static final String INSERT = "insert into ChatUsers (Pk, ChatId, UsersId, LastUpdateTimestamp, CreateTimestamp, Status, SyncTimestamp) values (?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update ChatUsers SET ChatId=?, UsersId=?, LastUpdateTimestamp=?, Status=?, SyncTimestamp=? WHERE Pk = ?; ";
	 public static ArrayList <ChatUsers> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return ChatUsersMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static ChatUsers getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<ChatUsers> results = ChatUsersMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (ChatUsers c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getChatId());
				 prep.setString(3, c.getUsersId());
				 prep.setLong(4, c.getLastUpdateTimestamp());
				 prep.setLong(5, c.getCreateTimestamp());
				 prep.setLong(6, c.getStatus());
				 prep.setLong(7, c.getSyncTimestamp());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (ChatUsers c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getChatId());
				 prep.setString(2, c.getUsersId());
				 prep.setLong(3, c.getLastUpdateTimestamp());
				 prep.setLong(4, c.getStatus());
				 prep.setLong(5, c.getSyncTimestamp());
				 prep.setString(6, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <ChatUsers> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <ChatUsers> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 ChatUsers c = new ChatUsers();
			 c.setPk(rs.getString("Pk"));
			 c.setChatId(rs.getString("ChatId"));
			 c.setUsersId(rs.getString("UsersId"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setStatus(rs.getLong("Status"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 results.add(c);
		 }
		 return results;
	 }
}
