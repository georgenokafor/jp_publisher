package com.umapped.itinerary.analyze;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.mapped.publisher.parse.schemaorg.ReservationPackage;
import modules.ItineraryAnalyzeModule;

import java.io.File;

/**
 * Created by wei on 2017-03-29.
 */
public class ItineraryAnalyzeServiceTest {
  protected static String DATA_ROOT = "/Users/wei/DevResources/data/trips/";
  protected static String SEGMENT_ROOT = DATA_ROOT + "segment/";
  protected static String RECOMMEND_ROOT = DATA_ROOT + "recommend/";
  private static String RESERVATION_ROOT = DATA_ROOT + "reservations/";
  private static String ITINERARY_ROOT = DATA_ROOT + "itinerary/";

  private static String DATE = "2017-03-09/";

  public static void main(String[] args)
      throws Exception {
    Injector container = Guice.createInjector(new ItineraryAnalyzeModule());

    ItineraryAnalyzeService service = container.getInstance(ItineraryAnalyzeService.class);

    File dir = new File(RESERVATION_ROOT + "cruise" );

    ObjectMapper om = container.getInstance(ObjectMapper.class);

    File[] files = dir.listFiles((directory, name) -> name.endsWith("1342270094300000558.json"));

    for (File f : files) {
      ReservationPackage rp = om.readValue(f, ReservationPackage.class);
      service.analyze(rp);
    }

  }
}
