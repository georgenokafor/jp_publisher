
package com.umapped.external.wetu.itinerary.details;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "itinerary_document_id",
    "name",
    "hidden",
    "element"
})
public class Document {

    @JsonProperty("itinerary_document_id")
    private int itineraryDocumentId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("hidden")
    private boolean hidden;
    @JsonProperty("element")
    private String element;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The itineraryDocumentId
     */
    @JsonProperty("itinerary_document_id")
    public int getItineraryDocumentId() {
        return itineraryDocumentId;
    }

    /**
     * 
     * @param itineraryDocumentId
     *     The itinerary_document_id
     */
    @JsonProperty("itinerary_document_id")
    public void setItineraryDocumentId(int itineraryDocumentId) {
        this.itineraryDocumentId = itineraryDocumentId;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The hidden
     */
    @JsonProperty("hidden")
    public boolean isHidden() {
        return hidden;
    }

    /**
     * 
     * @param hidden
     *     The hidden
     */
    @JsonProperty("hidden")
    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    /**
     * 
     * @return
     *     The element
     */
    @JsonProperty("element")
    public String getElement() {
        return element;
    }

    /**
     * 
     * @param element
     *     The element
     */
    @JsonProperty("element")
    public void setElement(String element) {
        this.element = element;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(itineraryDocumentId).append(name).append(hidden).append(element).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Document) == false) {
            return false;
        }
        Document rhs = ((Document) other);
        return new EqualsBuilder().append(itineraryDocumentId, rhs.itineraryDocumentId).append(name, rhs.name).append(hidden, rhs.hidden).append(element, rhs.element).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
