package com.mapped.publisher.js;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mapped.publisher.common.CacheMgr;
import com.mapped.publisher.persistence.redis.RedisKeys;
import com.mapped.publisher.persistence.redis.RedisMgr;
import com.umapped.api.schema.types.ReturnCode;

import java.io.Serializable;
import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Created by surge on 2016-08-05.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BackgroundJobStateView
    extends BaseJsonResponse implements Serializable {

  public enum Condition {
    RUNNING,
    ERROR,
    STOPPED
  }

  /**
   * Minimum progress value (Positive Integer)
   */
  public int       minVal;
  /**
   * Maximum progress value (Positive Integer)
   * If value is set to 0, UI should display indefinite progress bar
   */
  public int       maxVal;
  /**
   * Current progress value
   */
  public int       value;
  /**
   * State name
   */
  public String    state;
  /**
   * Longer state description
   */
  public String    description;
  /**
   * Current state condition
   */
  public Condition condition;
  /**
   * URL which should be used to retrieve this status
   */
  public String    stateUrl;
  /**
   * Last Updated Timestamp
   *
   * @return
   */
  public long      updatedTs;
  /**
   * Name of the job. Will also be used as a suffix for the Redis Key
   */
  @JsonIgnore
  public String    jobName;
  /**
   * How frequently to write status progress to cache
   */
  @JsonIgnore
  public int updateRate = 50;


  public BackgroundJobStateView(final String jobName) {
    super(ReturnCode.SUCCESS);
    this.jobName = jobName;
    this.condition = Condition.STOPPED;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Methods to support easy update                                                                                   //
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static BackgroundJobStateView getFromCache(final String jobName) {
    final RedisMgr rm = CacheMgr.getRedis();
    Optional<BackgroundJobStateView> oview = rm.getBin(RedisKeys.K_BACKGROUND_JOB_CACHE,
                                                       jobName,
                                                       BackgroundJobStateView.class);
    if (oview.isPresent()) {
      return oview.get();
    }
    return new BackgroundJobStateView(jobName);
  }

  public boolean start(Condition cond, int maxVal) {
    this.condition = cond;
    this.updatedTs = System.currentTimeMillis();
    this.value = 0;
    this.minVal = 0;
    this.maxVal = maxVal;
    updateCache();
    return true;
  }

  private void updateCache() {
    final RedisMgr rm = CacheMgr.getRedis();
    rm.set(RedisKeys.K_BACKGROUND_JOB_CACHE, jobName, this, RedisMgr.WriteMode.FAST);
  }

  /**
   * Increments the running state and updates cache every updateRate'th item
   */
  public void next() {
    ++value;
    if (value % updateRate == 0) {
      update(value);
    }
  }

  public void update(final int value) {
    this.updatedTs = System.currentTimeMillis();
    this.value = value;
    updateCache();
  }

  public void stop() {
    this.condition = Condition.STOPPED;
    this.updatedTs = System.currentTimeMillis();
    this.value = 0;
    this.maxVal = 0;
    updateCache();
  }

  public boolean isUpdatedInTheLastMinutes(int minutes) {
    return updatedTs >= (Instant.now().toEpochMilli() - TimeUnit.MINUTES.toMillis(minutes));
  }

}
