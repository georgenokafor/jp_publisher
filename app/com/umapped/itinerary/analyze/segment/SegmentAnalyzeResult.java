package com.umapped.itinerary.analyze.segment;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by wei on 2017-03-08.
 */
public class SegmentAnalyzeResult {
  private Map<String, Object> features;

  public SegmentAnalyzeResult() {
    this.features = new HashMap<>();
  }

  public void addFeature(String featureName, Object value) {
    features.put(featureName, value);
    return;
  }

}
