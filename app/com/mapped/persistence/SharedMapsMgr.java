package com.mapped.persistence;

import java.util.*;
 import java.sql.*;
	 import com.mapped.persistence.util.*;


public class SharedMapsMgr {

	 private static final String SELECT_ALL = "Select Pk, MapsId, Owner, ShareUserId, ShareType, Status, LastUpdateTimestamp, CreateTimestamp, ShareEmail, SyncTimestamp, Msg from SharedMaps;";
	 private static final String SELECT_ALL_BY_PK = "Select Pk, MapsId, Owner, ShareUserId, ShareType, Status, LastUpdateTimestamp, CreateTimestamp, ShareEmail, SyncTimestamp, Msg from SharedMaps where Pk = ?;";
	 private static final String DELETE_BY_PK = "delete from SharedMaps where Pk = ?;";
	 private static final String INSERT = "insert into SharedMaps (Pk, MapsId, Owner, ShareUserId, ShareType, Status, LastUpdateTimestamp, CreateTimestamp, ShareEmail, SyncTimestamp, Msg) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	 private static final String UPDATE = "update SharedMaps SET MapsId=?, Owner=?, ShareUserId=?, ShareType=?, Status=?, LastUpdateTimestamp=?, ShareEmail=?, SyncTimestamp=?, Msg=? WHERE Pk = ?; ";
	 public static ArrayList <SharedMaps> getAll() throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL);
			 rs = prep.executeQuery();
			 return SharedMapsMgr.handleResults(rs);
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static SharedMaps getByPk (String id) throws SQLException {
		 ResultSet rs = null;
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(SELECT_ALL_BY_PK);
			 prep.setString(1, id);
			 rs = prep.executeQuery();
			 ArrayList<SharedMaps> results = SharedMapsMgr.handleResults(rs);
			 if (results != null && results.size() == 1)
				 return results.get(0);
			 else
				 return null;
		} finally {
		    if (prep != null)
			  prep.close();
		    if (rs != null) {
		        rs.close();
		    }
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }
	 public static void deleteByPK (String id) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(DELETE_BY_PK);
			 prep.setString(1, id);
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void insert (SharedMaps c) throws SQLException {
		 PreparedStatement prep= null;
		 Connection conn= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(INSERT);
				 prep.setString(1, c.getPk());
				 prep.setString(2, c.getMapsId());
				 prep.setString(3, c.getOwner());
				 prep.setString(4, c.getShareUserId());
				 prep.setString(5, c.getShareType());
				 prep.setLong(6, c.getStatus());
				 prep.setLong(7, c.getLastUpdateTimestamp());
				 prep.setLong(8, c.getCreateTimestamp());
				 prep.setString(9, c.getShareEmail());
				 prep.setLong(10, c.getSyncTimestamp());
				 prep.setString(11, c.getMsg());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static void update (SharedMaps c) throws SQLException {
		 Connection conn= null;
		 PreparedStatement prep= null;
		 try {
			 conn = DBConnectionMgr.getConnection4Mobile();
			 prep = conn.prepareStatement(UPDATE);
				 prep.setString(1, c.getMapsId());
				 prep.setString(2, c.getOwner());
				 prep.setString(3, c.getShareUserId());
				 prep.setString(4, c.getShareType());
				 prep.setLong(5, c.getStatus());
				 prep.setLong(6, c.getLastUpdateTimestamp());
				 prep.setString(7, c.getShareEmail());
				 prep.setLong(8, c.getSyncTimestamp());
				 prep.setString(9, c.getMsg());
				 prep.setString(10, c.getPk());
			 prep.execute();
		} finally {
		    if (prep != null)
			  prep.close();
		    DBConnectionMgr.closeConnection(conn);
		 }
	 }

	 public static ArrayList <SharedMaps> handleResults(ResultSet rs) throws SQLException {
		 ArrayList <SharedMaps> results = new ArrayList<>();
		 if (rs == null)
			 return results;
		 while (rs.next()) {
			 SharedMaps c = new SharedMaps();
			 c.setPk(rs.getString("Pk"));
			 c.setMapsId(rs.getString("MapsId"));
			 c.setOwner(rs.getString("Owner"));
			 c.setShareUserId(rs.getString("ShareUserId"));
			 c.setShareType(rs.getString("ShareType"));
			 c.setStatus(rs.getLong("Status"));
			 c.setLastUpdateTimestamp(rs.getLong("LastUpdateTimestamp"));
			 c.setCreateTimestamp(rs.getLong("CreateTimestamp"));
			 c.setShareEmail(rs.getString("ShareEmail"));
			 c.setSyncTimestamp(rs.getLong("SyncTimestamp"));
			 c.setMsg(rs.getString("Msg"));
			 results.add(c);
		 }
		 return results;
	 }
}
