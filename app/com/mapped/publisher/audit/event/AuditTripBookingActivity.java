package com.mapped.publisher.audit.event;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.sql.Timestamp;

/**
 * Created by Serguei Moutovkin on 2014-05-21.
 */
@JsonTypeName("TRIP_BOOKING_ACTIVITY")
public class AuditTripBookingActivity
    extends AuditEvent {

  public String bookingId;
  public String name;
  public Timestamp startTime;

  public AuditTripBookingActivity withName(String name) {
    this.name = name;
    return this;
  }

  public AuditTripBookingActivity withStartTime(long startDT) {
    this.startTime = new Timestamp(startDT);
    return this;
  }

  public AuditTripBookingActivity withBookingId(String id) {
    bookingId = id;
    return this;
  }
}
