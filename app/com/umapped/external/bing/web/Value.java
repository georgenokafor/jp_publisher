
package com.umapped.external.bing.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "id",
    "name",
    "url",
    "displayUrl",
    "snippet",
    "dateLastCrawled",
    "deepLinks"
})
public class Value {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("url")
    private String url;
    @JsonProperty("displayUrl")
    private String displayUrl;
    @JsonProperty("snippet")
    private String snippet;
    @JsonProperty("dateLastCrawled")
    private String dateLastCrawled;
    @JsonProperty("deepLinks")
    private List<DeepLink> deepLinks = new ArrayList<DeepLink>();

    @JsonProperty("webSearchUrl")
    private String webSearchUrl;
    @JsonProperty("thumbnailUrl")
    private String thumbnailUrl;
    @JsonProperty("datePublished")
    private String datePublished;
    @JsonProperty("contentUrl")
    private String contentUrl;
    @JsonProperty("hostPageUrl")
    private String hostPageUrl;
    @JsonProperty("contentSize")
    private String contentSize;
    @JsonProperty("encodingFormat")
    private String encodingFormat;
    @JsonProperty("hostPageDisplayUrl")
    private String hostPageDisplayUrl;
    @JsonProperty("width")
    private int width;
    @JsonProperty("height")
    private int height;
    @JsonProperty("thumbnail")
    private Thumbnail thumbnail;

    @JsonProperty("text")
    private String text;
    @JsonProperty("displayText")
    private String displayText;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 
     * @return
     *     The displayUrl
     */
    @JsonProperty("displayUrl")
    public String getDisplayUrl() {
        return displayUrl;
    }

    /**
     * 
     * @param displayUrl
     *     The displayUrl
     */
    @JsonProperty("displayUrl")
    public void setDisplayUrl(String displayUrl) {
        this.displayUrl = displayUrl;
    }

    /**
     * 
     * @return
     *     The snippet
     */
    @JsonProperty("snippet")
    public String getSnippet() {
        return snippet;
    }

    /**
     * 
     * @param snippet
     *     The snippet
     */
    @JsonProperty("snippet")
    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    /**
     * 
     * @return
     *     The dateLastCrawled
     */
    @JsonProperty("dateLastCrawled")
    public String getDateLastCrawled() {
        return dateLastCrawled;
    }

    /**
     * 
     * @param dateLastCrawled
     *     The dateLastCrawled
     */
    @JsonProperty("dateLastCrawled")
    public void setDateLastCrawled(String dateLastCrawled) {
        this.dateLastCrawled = dateLastCrawled;
    }

    /**
     * 
     * @return
     *     The deepLinks
     */
    @JsonProperty("deepLinks")
    public List<DeepLink> getDeepLinks() {
        return deepLinks;
    }

    /**
     * 
     * @param deepLinks
     *     The deepLinks
     */
    @JsonProperty("deepLinks")
    public void setDeepLinks(List<DeepLink> deepLinks) {
        this.deepLinks = deepLinks;
    }

    /**
     *
     * @return
     *     The webSearchUrl
     */
    @JsonProperty("webSearchUrl")
    public String getWebSearchUrl() {
        return webSearchUrl;
    }

    /**
     *
     * @param webSearchUrl
     *     The webSearchUrl
     */
    @JsonProperty("webSearchUrl")
    public void setWebSearchUrl(String webSearchUrl) {
        this.webSearchUrl = webSearchUrl;
    }

    /**
     *
     * @return
     *     The thumbnailUrl
     */
    @JsonProperty("thumbnailUrl")
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    /**
     *
     * @param thumbnailUrl
     *     The thumbnailUrl
     */
    @JsonProperty("thumbnailUrl")
    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    /**
     *
     * @return
     *     The datePublished
     */
    @JsonProperty("datePublished")
    public String getDatePublished() {
        return datePublished;
    }

    /**
     *
     * @param datePublished
     *     The datePublished
     */
    @JsonProperty("datePublished")
    public void setDatePublished(String datePublished) {
        this.datePublished = datePublished;
    }

    /**
     *
     * @return
     *     The contentUrl
     */
    @JsonProperty("contentUrl")
    public String getContentUrl() {
        return contentUrl;
    }

    /**
     *
     * @param contentUrl
     *     The contentUrl
     */
    @JsonProperty("contentUrl")
    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    /**
     *
     * @return
     *     The hostPageUrl
     */
    @JsonProperty("hostPageUrl")
    public String getHostPageUrl() {
        return hostPageUrl;
    }

    /**
     *
     * @param hostPageUrl
     *     The hostPageUrl
     */
    @JsonProperty("hostPageUrl")
    public void setHostPageUrl(String hostPageUrl) {
        this.hostPageUrl = hostPageUrl;
    }

    /**
     *
     * @return
     *     The contentSize
     */
    @JsonProperty("contentSize")
    public String getContentSize() {
        return contentSize;
    }

    /**
     *
     * @param contentSize
     *     The contentSize
     */
    @JsonProperty("contentSize")
    public void setContentSize(String contentSize) {
        this.contentSize = contentSize;
    }

    /**
     *
     * @return
     *     The encodingFormat
     */
    @JsonProperty("encodingFormat")
    public String getEncodingFormat() {
        return encodingFormat;
    }

    /**
     *
     * @param encodingFormat
     *     The encodingFormat
     */
    @JsonProperty("encodingFormat")
    public void setEncodingFormat(String encodingFormat) {
        this.encodingFormat = encodingFormat;
    }

    /**
     *
     * @return
     *     The hostPageDisplayUrl
     */
    @JsonProperty("hostPageDisplayUrl")
    public String getHostPageDisplayUrl() {
        return hostPageDisplayUrl;
    }

    /**
     *
     * @param hostPageDisplayUrl
     *     The hostPageDisplayUrl
     */
    @JsonProperty("hostPageDisplayUrl")
    public void setHostPageDisplayUrl(String hostPageDisplayUrl) {
        this.hostPageDisplayUrl = hostPageDisplayUrl;
    }

    /**
     *
     * @return
     *     The width
     */
    @JsonProperty("width")
    public int getWidth() {
        return width;
    }

    /**
     *
     * @param width
     *     The width
     */
    @JsonProperty("width")
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     *
     * @return
     *     The height
     */
    @JsonProperty("height")
    public int getHeight() {
        return height;
    }

    /**
     *
     * @param height
     *     The height
     */
    @JsonProperty("height")
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     *
     * @return
     *     The thumbnail
     */
    @JsonProperty("thumbnail")
    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    /**
     *
     * @param thumbnail
     *     The thumbnail
     */
    @JsonProperty("thumbnail")
    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    /**
     *
     * @return
     *     The text
     */
    @JsonProperty("text")
    public String getText() {
        return text;
    }

    /**
     *
     * @param text
     *     The text
     */
    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    /**
     *
     * @return
     *     The displayText
     */
    @JsonProperty("displayText")
    public String getDisplayText() {
        return displayText;
    }

    /**
     *
     * @param displayText
     *     The displayText
     */
    @JsonProperty("displayText")
    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }



    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(name).append(url).append(displayUrl).append(snippet).append(dateLastCrawled).append(deepLinks).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Value) == false) {
            return false;
        }
        Value rhs = ((Value) other);
        return new EqualsBuilder().append(id, rhs.id).append(name, rhs.name).append(url, rhs.url).append(displayUrl, rhs.displayUrl).append(snippet, rhs.snippet).append(dateLastCrawled, rhs.dateLastCrawled).append(deepLinks, rhs.deepLinks).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
