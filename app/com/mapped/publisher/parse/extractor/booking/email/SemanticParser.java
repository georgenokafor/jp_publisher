package com.mapped.publisher.parse.extractor.booking.email;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.any23.extractor.html.TagSoupParser;
import org.apache.any23.extractor.microdata.ItemProp;
import org.apache.any23.extractor.microdata.ItemScope;
import org.apache.any23.extractor.microdata.MicrodataParser;
import org.apache.any23.extractor.microdata.MicrodataParserReport;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Parser for data encoded within emails or other hypertext media as JSON-LD and
 * Microdata.
 *
 * Created by surge on 2015-05-12.
 */
public class SemanticParser {

  public static ReservationsHolder parseEmbeddedJsonLD(String html) {
    ReservationsHolder rh = new ReservationsHolder();
    try {
      Document doc = Jsoup.parseBodyFragment(html);
      Elements parsedHtml = doc.select("script").attr("type", "application/ld+json");
      String jsonText = parsedHtml.html();

      rh.parseJson(jsonText);

    } catch (Exception e) {
      e.printStackTrace();
      System.err.println(e.getMessage());
      System.err.flush();
    }
    return rh;
  }


  public static ReservationsHolder parseMicrodata(String html) {
    ReservationsHolder rh = new ReservationsHolder();

    InputStream documentInputInputStream = null;
    //ByteArrayOutputStream baos = new ByteArrayOutputStream();
    //PrintStream ps = new PrintStream(baos);

    try {
      documentInputInputStream = new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8));
      final TagSoupParser   tagSoupParser = new TagSoupParser(documentInputInputStream, "schema.org");
      MicrodataParserReport report        = MicrodataParser.getMicrodata(tagSoupParser.getDOM());
      List<ObjectNode>      nodes         = SemanticParser.getJsonLD(report);
      ObjectReader          or            = ReservationsHolder.getObjectReader();

      for(ObjectNode on: nodes) {
        //System.out.println(om.writeValueAsString(on));
        JsonParser jp = or.treeAsTokens(on);
        jp.nextValue();
        rh.parseReservation(jp);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    finally {
      if (documentInputInputStream != null) {
        try {
          documentInputInputStream.close();
        }
        catch (IOException ioe) {
          System.err.println("Problem closing String input");
        }
      }
    }

    return rh;
  }

  private static ObjectNode scopeToNodes(ItemScope is, JsonNodeFactory nodeFactory) {
    ObjectNode root = nodeFactory.objectNode();
    root.put("@context", is.getType().getProtocol() + "://" + is.getType().getHost());
    root.put("@type", is.getType().getPath().substring(1));

    final Collection<List<ItemProp>> itemPropsList = is.getProperties().values();
    for (List<ItemProp> itemProps : itemPropsList) {
      ObjectNode on = propsToNodes(itemProps, nodeFactory);

      root.putAll(on);
    }
    return root;
  }

  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");

  private static ObjectNode propsToNodes(List<ItemProp> itemProps, JsonNodeFactory nodeFactory) {
    ObjectNode on = nodeFactory.objectNode();

    for (ItemProp itemProp : itemProps) {
      switch (itemProp.getValue().getType()) {
        case Plain:
          on.put(itemProp.getName(), (String) itemProp.getValue().getContent());
          break;
        case Link:
          on.put(itemProp.getName(), (String) itemProp.getValue().getContent());
          break;
        case Date:
          on.put(itemProp.getName(), sdf.format((Date) itemProp.getValue().getContent()));
          break;
        case Nested:
          if (itemProp.getName().equals("reservation") || itemProp.getName().equals("subReservation")) {
            ArrayNode an = on.withArray(itemProp.getName());
            an.add(scopeToNodes(itemProp.getValue().getAsNested(), nodeFactory));
          } else {
            on.put(itemProp.getName(), scopeToNodes(itemProp.getValue().getAsNested(), nodeFactory));
          }
          break;
      }
    }
    return on;
  }

  private static List<ObjectNode> getJsonLD(MicrodataParserReport report) {
    JsonNodeFactory nodeFactory = new JsonNodeFactory(false);
    List<ObjectNode> reservations = new ArrayList<>(1); //Frequently a single reservation per email

    for(ItemScope is : report.getDetectedItemScopes()) {
      ObjectNode root = scopeToNodes(is, nodeFactory);
      reservations.add(root);
    }
    return reservations;
  }


}
