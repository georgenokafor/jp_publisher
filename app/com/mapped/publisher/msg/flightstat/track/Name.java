
package com.mapped.publisher.msg.flightstat.track;

import java.util.List;

public class Name{
   	private String interpreted;
   	private String requested;

 	public String getInterpreted(){
		return this.interpreted;
	}
	public void setInterpreted(String interpreted){
		this.interpreted = interpreted;
	}
 	public String getRequested(){
		return this.requested;
	}
	public void setRequested(String requested){
		this.requested = requested;
	}
}
