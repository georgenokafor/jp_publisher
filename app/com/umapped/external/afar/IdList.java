package com.umapped.external.afar;

import java.util.List;

/**
 * Created by george on 2017-11-01.
 */
public class IdList {
  private List<Integer> ids;

  public List<Integer> getIds() {
    return ids;
  }

  public void setIds(List<Integer> ids) {
    this.ids = ids;
  }
}
