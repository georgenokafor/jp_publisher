package models.publisher;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbHstore;
import com.avaje.ebean.annotation.DbJsonB;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapped.publisher.form.DataTablesForm;
import com.mapped.publisher.persistence.communicator.Communicator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by surge on 2016-08-04.
 */
@Entity
@Table(name = "messenger_conference")
public class MessengerConference
    extends Model {

  public static Model.Finder<String, MessengerConference> find = new Finder(MessengerConference.class);

  @Transient
  public static final ObjectMapper om;

  static {
    om = new ObjectMapper();
    //Perform any kind of configuration for object mapper in this area
  }

  /**
   * Conferences are tightly coupled to trips, so trip IDs are PKs
   */
  @Id
  String tripid;
  /**
   * Flags if the messenger conference was removed from firebase database
   */
  boolean      removed;
  /**
   * Indicates when the cleaning (removal) of the firebase database records was performed
   */
  Timestamp    removedTs;
  /**
   * ALL Users that have/had access to the conference as of the last known time
   */
  @DbHstore
  Map<String, String> users;
  /**
   * Number of rooms in the conferece
   */
  int          roomCount;
  /**
   * Number of messages in all conferences
   */
  int          msgCount;

  /**
   * Archive of all rooms in the conference exactly as JSON appears in Firebase database
   */
  @DbJsonB
  JsonNode rooms;
  /**
   * Archive of all messages as they appear in Firebase database, in JSON format
   */
  @DbJsonB
  JsonNode messages;
  /**
   * Additional misc parameters that might be needed
   */
  @DbHstore
  Map<String, String> params;
  Timestamp           createdTs;
  Long                createdBy;
  Timestamp           modifiedTs;
  Long                modifiedBy;
  @Version
  int version;

  public static MessengerConference build(String tripid, Long createdBy) {
    MessengerConference mc = new MessengerConference();
    mc.setCreatedBy(createdBy);
    mc.setModifiedBy(createdBy);
    mc.setTripid(tripid);
    mc.setCreatedTs(Timestamp.from(Instant.now()));
    mc.setModifiedTs(mc.createdTs);
    mc.users = new HashMap<>();
    mc.setRemoved(false);
    return mc;
  }

  /**
   * Total number of conferences open in the messenger;
   * @return
   */
  public static int getRowCount() {
    return find.findCount();
  }

  public static int getRowCountFiltered(DataTablesForm dtRequest) {
    return find.where()
               .findCount();
  }

  public static List<MessengerConference> getPage(DataTablesForm dtRequest) {
    String orderBy = "roomCount desc";
    if(dtRequest.order  != null && dtRequest.order.size() >= 1) {
      orderBy = dtRequest.columns.get(dtRequest.order.get(0).column).name + " " + dtRequest.order.get(0).dir;
    }

    return find.where()
               .orderBy(orderBy)
               .setFirstRow(dtRequest.start).setMaxRows(dtRequest.length)
               .findList();
  }

  public static Optional<MessengerConference> findByPk(String tripid) {
    return Optional.ofNullable(find.byId(tripid));
  }

  public void markRemoved() {
    setRemoved(true);
    setRemovedTs(Timestamp.from(Instant.now()));
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public MessengerConference setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public MessengerConference setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
    return this;
  }

  public void markModified(Long modifiedBy) {
    setModifiedBy(modifiedBy);
    setModifiedTs(Timestamp.from(Instant.now()));
  }

  public String getTripid() {
    return tripid;
  }

  public MessengerConference setTripid(String tripid) {
    this.tripid = tripid;
    return this;
  }

  public boolean isRemoved() {
    return removed;
  }

  public MessengerConference setRemoved(boolean removed) {
    this.removed = removed;
    return this;
  }

  public Timestamp getRemovedTs() {
    return removedTs;
  }

  public MessengerConference setRemovedTs(Timestamp removedTs) {
    this.removedTs = removedTs;
    return this;
  }

  public Map<String, String> getUsers() {
    return users;
  }

  public int getUsersCount() {
    if(users == null) {
      return 0;
    }
    return users.size();
  }


  public MessengerConference setUsers(Map<String, String> users) {
    this.users = users;
    return this;
  }

  public MessengerConference addUser(String legacyId) {
    if(getUsers() == null) {
      users = new HashMap<>();
    }
    users.put(legacyId, "true");
    return this;
  }

  public MessengerConference setUserState(String legacyId, boolean state) {
    users.put(Communicator.makeUserIdSafe(legacyId), Boolean.toString(state));
    return this;
  }

  public boolean getUserState(String legacyId) {
    String bv = getUsers().get(Communicator.makeUserIdSafe(legacyId));
    return bv != null && bv.equals("true");
  }

  public MessengerConference removeUser(String legacyId) {
    users.remove(legacyId);
    return this;
  }

  public int getRoomCount() {
    return roomCount;
  }

  public MessengerConference setRoomCount(int roomCount) {
    this.roomCount = roomCount;
    return this;
  }

  public int getMsgCount() {
    return msgCount;
  }

  public MessengerConference setMsgCount(int msgCount) {
    this.msgCount = msgCount;
    return this;
  }

  public JsonNode getRooms() {
    return rooms;
  }

  public MessengerConference setRooms(Object dataSnapshotValue) {
    this.rooms = om.valueToTree(dataSnapshotValue);
    return this;
  }

  public MessengerConference setRooms(JsonNode rooms) {
    this.rooms = rooms;
    return this;
  }

  public JsonNode getMessages() {
    return messages;
  }

  public MessengerConference setMessages(Object dataSnapshotValue) {
    this.messages = om.valueToTree(dataSnapshotValue);
    return this;
  }

  public MessengerConference setMessages(JsonNode messages) {
    this.messages = messages;
    return this;
  }

  public Map<String, String> getParams() {
    return params;
  }

  public MessengerConference setParams(Map<String, String> params) {
    this.params = params;
    return this;
  }

  public Timestamp getCreatedTs() {
    return createdTs;
  }

  public MessengerConference setCreatedTs(Timestamp createdTs) {
    this.createdTs = createdTs;
    return this;
  }

  public Timestamp getModifiedTs() {
    return modifiedTs;
  }

  public MessengerConference setModifiedTs(Timestamp modifiedTs) {
    this.modifiedTs = modifiedTs;
    return this;
  }

  public int getVersion() {
    return version;
  }

  public MessengerConference setVersion(int version) {
    this.version = version;
    return this;
  }
}
