package com.mapped.persistence;
 
import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.JsonNode; 

 
import play.libs.Json;
 public class File { 
 
	 public final static String FileTypeId = "44"; 
	 protected static String pkTypeId =  "45"; 
	 private String pk; 
	 protected static String filePathTypeId =  "46"; 
	 private String filePath; 
	 protected static String fileTagTypeId =  "47"; 
	 private String fileTag; 
	 protected static String fileNoteTypeId =  "48"; 
	 private String fileNote; 
	 protected static String statusTypeId =  "49"; 
	 private long status; 
	 protected static String fileTypeTypeId =  "50"; 
	 private String fileType; 
	 protected static String lastUpdateTimestampTypeId =  "51"; 
	 private long lastUpdateTimestamp; 
	 protected static String createTimestampTypeId =  "52"; 
	 private long createTimestamp; 
	 protected static String modifiedByTypeId =  "53"; 
	 private String modifiedBy; 
	 protected static String createdByTypeId =  "54"; 
	 private String createdBy; 
	 protected static String parentIdTypeId =  "55"; 
	 private String parentId; 
	 protected static String syncTimestampTypeId =  "56"; 
	 private long syncTimestamp; 
	 protected static String rankTypeId =  "57"; 
	 private String rank; 
	 protected static String uploadedTypeId =  "58"; 
	 private String uploaded; 
	 protected static String imgWidthTypeId =  "400"; 
	 private String imgWidth; 
	 protected static String imgHeightTypeId =  "401"; 
	 private String imgHeight; 
	 protected static String latitudeTypeId =  "402"; 
	 private String latitude; 
	 protected static String longitudeTypeId =  "403"; 
	 private String longitude; 
	 protected static String bearingTypeId =  "404"; 
	 private String bearing; 

 	 // Contructor 
	 public File () {} 
	 public File ( String pk,  String filePath,  String fileTag,  String fileNote,  long status,  String fileType,  long lastUpdateTimestamp,  long createTimestamp,  String modifiedBy,  String createdBy,  String parentId,  long syncTimestamp,  String rank,  String uploaded,  String imgWidth,  String imgHeight,  String latitude,  String longitude,  String bearing ) { 
		 this.pk=pk; 
		 this.filePath=filePath; 
		 this.fileTag=fileTag; 
		 this.fileNote=fileNote; 
		 this.status=status; 
		 this.fileType=fileType; 
		 this.lastUpdateTimestamp=lastUpdateTimestamp; 
		 this.createTimestamp=createTimestamp; 
		 this.modifiedBy=modifiedBy; 
		 this.createdBy=createdBy; 
		 this.parentId=parentId; 
		 this.syncTimestamp=syncTimestamp; 
		 this.rank=rank; 
		 this.uploaded=uploaded; 
		 this.imgWidth=imgWidth; 
		 this.imgHeight=imgHeight; 
		 this.latitude=latitude; 
		 this.longitude=longitude; 
		 this.bearing=bearing; 
	 }
	 public  String getPk () { 
	 	 return pk; 
 	 } 
 
 	 public void setPk(String pk) { 
	 	 this.pk = pk; 
 	 } 
 
 	 public  String getFilePath () { 
	 	 return filePath; 
 	 } 
 
 	 public void setFilePath(String filePath) { 
	 	 this.filePath = filePath; 
 	 } 
 
 	 public  String getFileTag () { 
	 	 return fileTag; 
 	 } 
 
 	 public void setFileTag(String fileTag) { 
	 	 this.fileTag = fileTag; 
 	 } 
 
 	 public  String getFileNote () { 
	 	 return fileNote; 
 	 } 
 
 	 public void setFileNote(String fileNote) { 
	 	 this.fileNote = fileNote; 
 	 } 
 
 	 public  long getStatus () { 
	 	 return status; 
 	 } 
 
 	 public void setStatus(long status) { 
	 	 this.status = status; 
 	 } 
 
 	 public  String getFileType () { 
	 	 return fileType; 
 	 } 
 
 	 public void setFileType(String fileType) { 
	 	 this.fileType = fileType; 
 	 } 
 
 	 public  long getLastUpdateTimestamp () { 
	 	 return lastUpdateTimestamp; 
 	 } 
 
 	 public void setLastUpdateTimestamp(long lastUpdateTimestamp) { 
	 	 this.lastUpdateTimestamp = lastUpdateTimestamp; 
 	 } 
 
 	 public  long getCreateTimestamp () { 
	 	 return createTimestamp; 
 	 } 
 
 	 public void setCreateTimestamp(long createTimestamp) { 
	 	 this.createTimestamp = createTimestamp; 
 	 } 
 
 	 public  String getModifiedBy () { 
	 	 return modifiedBy; 
 	 } 
 
 	 public void setModifiedBy(String modifiedBy) { 
	 	 this.modifiedBy = modifiedBy; 
 	 } 
 
 	 public  String getCreatedBy () { 
	 	 return createdBy; 
 	 } 
 
 	 public void setCreatedBy(String createdBy) { 
	 	 this.createdBy = createdBy; 
 	 } 
 
 	 public  String getParentId () { 
	 	 return parentId; 
 	 } 
 
 	 public void setParentId(String parentId) { 
	 	 this.parentId = parentId; 
 	 } 
 
 	 public  long getSyncTimestamp () { 
	 	 return syncTimestamp; 
 	 } 
 
 	 public void setSyncTimestamp(long syncTimestamp) { 
	 	 this.syncTimestamp = syncTimestamp; 
 	 } 
 
 	 public  String getRank () { 
	 	 return rank; 
 	 } 
 
 	 public void setRank(String rank) { 
	 	 this.rank = rank; 
 	 } 
 
 	 public  String getUploaded () { 
	 	 return uploaded; 
 	 } 
 
 	 public void setUploaded(String uploaded) { 
	 	 this.uploaded = uploaded; 
 	 } 
 
 	 public  String getImgWidth () { 
	 	 return imgWidth; 
 	 } 
 
 	 public void setImgWidth(String imgWidth) { 
	 	 this.imgWidth = imgWidth; 
 	 } 
 
 	 public  String getImgHeight () { 
	 	 return imgHeight; 
 	 } 
 
 	 public void setImgHeight(String imgHeight) { 
	 	 this.imgHeight = imgHeight; 
 	 } 
 
 	 public  String getLatitude () { 
	 	 return latitude; 
 	 } 
 
 	 public void setLatitude(String latitude) { 
	 	 this.latitude = latitude; 
 	 } 
 
 	 public  String getLongitude () { 
	 	 return longitude; 
 	 } 
 
 	 public void setLongitude(String longitude) { 
	 	 this.longitude = longitude; 
 	 } 
 
 	 public  String getBearing () { 
	 	 return bearing; 
 	 } 
 
 	 public void setBearing(String bearing) { 
	 	 this.bearing = bearing; 
 	 } 
 
 	 //@Override 
	 public String toString() { 
		 StringBuffer sb = new StringBuffer(); 
		 sb.append("Class: File"); 
		 sb.append("	 Pk: ").append(this.pk );  
		 sb.append("	 FilePath: ").append(this.filePath );  
		 sb.append("	 FileTag: ").append(this.fileTag );  
		 sb.append("	 FileNote: ").append(this.fileNote );  
		 sb.append("	 Status: ").append(this.status );  
		 sb.append("	 FileType: ").append(this.fileType );  
		 sb.append("	 LastUpdateTimestamp: ").append(this.lastUpdateTimestamp );  
		 sb.append("	 CreateTimestamp: ").append(this.createTimestamp );  
		 sb.append("	 ModifiedBy: ").append(this.modifiedBy );  
		 sb.append("	 CreatedBy: ").append(this.createdBy );  
		 sb.append("	 ParentId: ").append(this.parentId );  
		 sb.append("	 SyncTimestamp: ").append(this.syncTimestamp );  
		 sb.append("	 Rank: ").append(this.rank );  
		 sb.append("	 Uploaded: ").append(this.uploaded );  
		 sb.append("	 ImgWidth: ").append(this.imgWidth );  
		 sb.append("	 ImgHeight: ").append(this.imgHeight );  
		 sb.append("	 Latitude: ").append(this.latitude );  
		 sb.append("	 Longitude: ").append(this.longitude );  
		 sb.append("	 Bearing: ").append(this.bearing );  
		 return sb.toString(); 
	 } 
	 public ObjectNode toJson() {
		 ObjectNode classNode = Json.newObject();
		 classNode.put("objID", FileTypeId ); 
		 ObjectNode memberNode = classNode.putObject("fields");
		 if (pk != null) {
			 memberNode.put(pkTypeId, pk);
		 }
		 if (filePath != null) {
			 memberNode.put(filePathTypeId, filePath);
		 }
		 if (fileTag != null) {
			 memberNode.put(fileTagTypeId, fileTag);
		 }
		 if (fileNote != null) {
			 memberNode.put(fileNoteTypeId, fileNote);
		 }
		 if (status >= -1) {
			 memberNode.put(statusTypeId, Long.toString(status));
		 }
		 if (fileType != null) {
			 memberNode.put(fileTypeTypeId, fileType);
		 }
		 if (lastUpdateTimestamp >= 0) {
			 memberNode.put(lastUpdateTimestampTypeId, Long.toString(lastUpdateTimestamp));
		 }
		 if (createTimestamp >= 0) {
			 memberNode.put(createTimestampTypeId, Long.toString(createTimestamp));
		 }
		 if (modifiedBy != null) {
			 memberNode.put(modifiedByTypeId, modifiedBy);
		 }
		 if (createdBy != null) {
			 memberNode.put(createdByTypeId, createdBy);
		 }
		 if (parentId != null) {
			 memberNode.put(parentIdTypeId, parentId);
		 }
		 if (syncTimestamp >= 0) {
			 memberNode.put(syncTimestampTypeId, Long.toString(syncTimestamp));
		 }
		 if (rank != null) {
			 memberNode.put(rankTypeId, rank);
		 }
		 if (uploaded != null) {
			 memberNode.put(uploadedTypeId, uploaded);
		 }
		 if (imgWidth != null) {
			 memberNode.put(imgWidthTypeId, imgWidth);
		 }
		 if (imgHeight != null) {
			 memberNode.put(imgHeightTypeId, imgHeight);
		 }
		 if (latitude != null) {
			 memberNode.put(latitudeTypeId, latitude);
		 }
		 if (longitude != null) {
			 memberNode.put(longitudeTypeId, longitude);
		 }
		 if (bearing != null) {
			 memberNode.put(bearingTypeId, bearing);
		 }
		 return classNode;
	 } 
	 public static File parseJson(JsonNode fields) {
		 if (fields != null ) { 
			 File  c= new File(); 
			 c.pk = fields.findPath(pkTypeId).textValue();
			 c.filePath = fields.findPath(filePathTypeId).textValue();
			 c.fileTag = fields.findPath(fileTagTypeId).textValue();
			 c.fileNote = fields.findPath(fileNoteTypeId).textValue();
			 if (fields.findPath(statusTypeId).textValue() == null) {
				 c.status = 0;
			 } else { 
				 c.status = Long.parseLong(fields.findPath(statusTypeId).textValue());
			 } 
			 c.fileType = fields.findPath(fileTypeTypeId).textValue();
			 if (fields.findPath(lastUpdateTimestampTypeId).textValue() == null) {
				 c.lastUpdateTimestamp = 0;
			 } else { 
				 c.lastUpdateTimestamp = Long.parseLong(fields.findPath(lastUpdateTimestampTypeId).textValue());
			 } 
			 if (fields.findPath(createTimestampTypeId).textValue() == null) {
				 c.createTimestamp = 0;
			 } else { 
				 c.createTimestamp = Long.parseLong(fields.findPath(createTimestampTypeId).textValue());
			 } 
			 c.modifiedBy = fields.findPath(modifiedByTypeId).textValue();
			 c.createdBy = fields.findPath(createdByTypeId).textValue();
			 c.parentId = fields.findPath(parentIdTypeId).textValue();
			 if (fields.findPath(syncTimestampTypeId).textValue() == null) {
				 c.syncTimestamp = 0;
			 } else { 
				 c.syncTimestamp = Long.parseLong(fields.findPath(syncTimestampTypeId).textValue());
			 } 
			 c.rank = fields.findPath(rankTypeId).textValue();
			 c.uploaded = fields.findPath(uploadedTypeId).textValue();
			 c.imgWidth = fields.findPath(imgWidthTypeId).textValue();
			 c.imgHeight = fields.findPath(imgHeightTypeId).textValue();
			 c.latitude = fields.findPath(latitudeTypeId).textValue();
			 c.longitude = fields.findPath(longitudeTypeId).textValue();
			 c.bearing = fields.findPath(bearingTypeId).textValue();
			 return c;
		} else
			 return null;
	 } 
} 
 
