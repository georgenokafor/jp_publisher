
package com.mapped.publisher.parse.ensemble;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mapped.publisher.parse.ensemble package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AgentAlert_QNAME = new QName("", "agent_alert");
    private final static QName _Title_QNAME = new QName("", "title");
    private final static QName _Embarkationdate_QNAME = new QName("", "embarkationdate");
    private final static QName _BookingInstruction_QNAME = new QName("", "booking_instruction");
    private final static QName _AgentDisclaimer_QNAME = new QName("", "agent_disclaimer");
    private final static QName _Promo_QNAME = new QName("", "promo");
    private final static QName _ConsumerDisclaimer_QNAME = new QName("", "consumer_disclaimer");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mapped.publisher.parse.ensemble
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Form }
     * 
     */
    public Form createForm() {
        return new Form();
    }

    /**
     * Create an instance of {@link Ship }
     * 
     */
    public Ship createShip() {
        return new Ship();
    }

    /**
     * Create an instance of {@link Image }
     * 
     */
    public Image createImage() {
        return new Image();
    }

    /**
     * Create an instance of {@link Offer }
     * 
     */
    public Offer createOffer() {
        return new Offer();
    }

    /**
     * Create an instance of {@link Supplier }
     * 
     */
    public Supplier createSupplier() {
        return new Supplier();
    }

    /**
     * Create an instance of {@link SpecialInterest }
     * 
     */
    public SpecialInterest createSpecialInterest() {
        return new SpecialInterest();
    }

    /**
     * Create an instance of {@link Itinerary }
     * 
     */
    public Itinerary createItinerary() {
        return new Itinerary();
    }

    /**
     * Create an instance of {@link EnsembletravelOffer }
     * 
     */
    public EnsembletravelOffer createEnsembletravelOffer() {
        return new EnsembletravelOffer();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "agent_alert")
    public JAXBElement<String> createAgentAlert(String value) {
        return new JAXBElement<String>(_AgentAlert_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "title")
    public JAXBElement<String> createTitle(String value) {
        return new JAXBElement<String>(_Title_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "embarkationdate")
    public JAXBElement<String> createEmbarkationdate(String value) {
        return new JAXBElement<String>(_Embarkationdate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "booking_instruction")
    public JAXBElement<String> createBookingInstruction(String value) {
        return new JAXBElement<String>(_BookingInstruction_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "agent_disclaimer")
    public JAXBElement<String> createAgentDisclaimer(String value) {
        return new JAXBElement<String>(_AgentDisclaimer_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "promo")
    public JAXBElement<String> createPromo(String value) {
        return new JAXBElement<String>(_Promo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "consumer_disclaimer")
    public JAXBElement<String> createConsumerDisclaimer(String value) {
        return new JAXBElement<String>(_ConsumerDisclaimer_QNAME, String.class, null, value);
    }

}
